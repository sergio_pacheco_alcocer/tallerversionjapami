﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using System.Text;
using JAPAMI.Psp_Presupuesto.Negocio;

namespace JAPAMI.Psp_Presupuesto.Datos
{
    public class Cls_Ope_Psp_Presupuesto_Datos
    {
        #region METODOS
            #region (GENERALES)
                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Anios
                ///DESCRIPCIÓN          : Obtiene datos de los años del estado analitico de ingresos
                ///PARAMETROS           : 
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 25/Abril/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Anios(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                    DataSet Ds_Anios = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                    DataTable Dt_Anios = new DataTable();
                    try
                    {
                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_Presupuesto.Trim()))
                        {
                            if (Negocio.P_Tipo_Presupuesto.Trim().Equals("ING"))
                            {
                                Mi_SQL.Append("SELECT DISTINCT " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " AS ANIO_ID ");
                                Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                                if (!String.IsNullOrEmpty(Negocio.P_Busqueda.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " LIKE '%" + Negocio.P_Busqueda.Trim() + "%'");
                                }
                            }
                            else
                            {
                                Mi_SQL.Append("SELECT DISTINCT " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " AS ANIO_ID ");
                                Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                if (!String.IsNullOrEmpty(Negocio.P_Busqueda.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " LIKE '%" + Negocio.P_Busqueda.Trim() + "%'");
                                }
                            }
                        }
                        else
                        {
                            Mi_SQL.Append("SELECT DISTINCT " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " AS ANIO_ID ");
                            Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                            if (!String.IsNullOrEmpty(Negocio.P_Busqueda.Trim()))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " LIKE '%" + Negocio.P_Busqueda.Trim() + "%'");
                            }
                            Mi_SQL.Append(" UNION ");
                            Mi_SQL.Append("SELECT DISTINCT " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " AS ANIO_ID ");
                            Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);

                            if (!String.IsNullOrEmpty(Negocio.P_Busqueda.Trim()))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " LIKE '%" + Negocio.P_Busqueda.Trim() + "%'");
                            }
                        }

                        Mi_SQL.Append(" ORDER BY ANIO_ID DESC");

                        Ds_Anios = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        if (Ds_Anios != null)
                        {
                            Dt_Anios = Ds_Anios.Tables[0];
                        }
                    }
                    catch (Exception Ex)
                    {
                        String Mensaje = "Error al intentar consultar los registros de los años. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                        throw new Exception(Mensaje);
                    }
                    return Dt_Anios;
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_FF
                ///DESCRIPCIÓN          : Obtiene datos de  las fuentes de financiamiento del estado analitico de ingresos
                ///PARAMETROS           : 
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 25/Abril/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_FF(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                    DataSet Ds_FF = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                    DataTable Dt_FF = new DataTable();
                    try
                    {
                        Mi_SQL.Append("SELECT DISTINCT " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + ", ");
                        Mi_SQL.Append(" UPPER(" + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                        Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + ") AS CLAVE_NOMBRE ");
                        Mi_SQL.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_Presupuesto.Trim()))
                        {
                            if (Negocio.P_Tipo_Presupuesto.Trim().Equals("ING"))
                            {
                                Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                                Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                                if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                                    Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                                }
                            }
                            else
                            {
                                Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                                Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                Mi_SQL.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                                if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                                    Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                                }
                                if (!String.IsNullOrEmpty(Negocio.P_Tipo_Usuario.Trim()))
                                {
                                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                                    {
                                        if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                        {
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                            Mi_SQL.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                        }
                                        else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                        {
                                            Mi_SQL.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                            Mi_SQL.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                        }
                                        else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                        {
                                            Mi_SQL.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                        }
                                        else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                        {
                                            Mi_SQL.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                        }
                                        else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                        {
                                            Mi_SQL.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                            Mi_SQL.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                        }
                                    }
                                    else 
                                    {
                                        if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                        {
                                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                            Mi_SQL.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                        }
                                        else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                        {
                                            Mi_SQL.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                            Mi_SQL.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                        }
                                        else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                        {
                                            Mi_SQL.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                        }
                                        else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                        {
                                            Mi_SQL.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                        }
                                        else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                        {
                                            Mi_SQL.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                            Mi_SQL.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                            Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                            Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                            Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);

                            if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                                Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                                Mi_SQL.Append(" OR " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                                Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Busqueda.Trim()))
                        {
                            if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" AND ");
                                Mi_SQL.Append(" UPPER(" + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                                Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + ")");
                                Mi_SQL.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                            }
                            else
                            {
                                Mi_SQL.Append(" WHERE ");
                                Mi_SQL.Append(" UPPER(" + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                                Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + ")");
                                Mi_SQL.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                            }
                        }

                        Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                        Ds_FF = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        if (Ds_FF != null)
                        {
                            Dt_FF = Ds_FF.Tables[0];
                        }
                    }
                    catch (Exception Ex)
                    {
                        String Mensaje = "Error al intentar consultar los registros de las fuentes de fianciamiento. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                        throw new Exception(Mensaje);
                    }
                    return Dt_FF;
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Proyectos
                ///DESCRIPCIÓN          : consulta para obtener los datos de los proyectos
                ///PARAMETROS           : 
                ///CREO                 : Leslie Gonzalez Vázquez
                ///FECHA_CREO           : 24/Mayo/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Proyectos(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_Sql = new StringBuilder();
                    try
                    {
                        if (Negocio.P_Tipo_Presupuesto.Trim().Equals("EGR"))
                        {
                            Mi_Sql.Append("SELECT DISTINCT UPPER(" + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + ") AS CLAVE_NOMBRE, ");
                            Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                            Mi_Sql.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                            Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                            Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                            Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                            Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                            Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                            Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                            Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                            Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                            Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);

                            if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                                Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Tipo_Usuario.Trim()))
                            {
                                if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                                {
                                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                    {
                                        Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                        Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                        Negocio.P_Dependencia_ID = String.Empty;
                                    }
                                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                    {
                                        Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                        Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                    }
                                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                    {
                                        Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                    }
                                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                    {
                                        Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                    }
                                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                    {
                                        Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                        Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                        Negocio.P_Fte_Financiamiento = String.Empty;
                                    }
                                }
                                else
                                {
                                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                    {
                                        Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                        Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                        Negocio.P_Dependencia_ID = String.Empty;
                                    }
                                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                    {
                                        Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                        Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                    }
                                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                    {
                                        Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                    }
                                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                    {
                                        Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                    }
                                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                    {
                                        Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                        Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                        Negocio.P_Fte_Financiamiento = String.Empty;
                                    }
                                }
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                            {
                                if (!Mi_Sql.ToString().Contains("WHERE"))
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                                }
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                            {
                                if (!Mi_Sql.ToString().Contains("WHERE"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                                }
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                            {
                                if (!Mi_Sql.ToString().Contains("WHERE"))
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                }
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Busqueda.Trim()))
                            {
                                if (!Mi_Sql.ToString().Contains("WHERE"))
                                {
                                    Mi_Sql.Append(" WHERE UPPER(" + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + ")");
                                    Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND UPPER(" + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + ")");
                                    Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                                }
                            }
                        }
                        else
                        {
                            Mi_Sql.Append("SELECT DISTINCT " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS CLAVE_NOMBRE, ");
                            Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                            Mi_Sql.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                            Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                            Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                            Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);

                            if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                                Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_Ing.Trim()))
                            {
                                if (!Mi_Sql.ToString().Contains("WHERE"))
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento_Ing.Trim() + "'");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento_Ing.Trim() + "'");
                                }
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Busqueda.Trim()))
                            {
                                if (!Mi_Sql.ToString().Contains("WHERE"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre);
                                    Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim() + "%'");
                                    Mi_Sql.Append(" OR " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre);
                                    Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre);
                                    Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim() + "%'");
                                    Mi_Sql.Append(" OR " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre);
                                    Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                                }
                            }
                        }

                        Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error al intentar consultar los registros de los proyectos. Error: [" + Ex.Message + "]");
                    }
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Gpo_Dependencia
                ///DESCRIPCIÓN          : Obtiene datos del grupo dependencia del usuario
                ///PARAMETROS           : 
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 17/Agosto/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Gpo_Dependencia(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                    DataSet Ds_Dep = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                    DataTable Dt_Dep = new DataTable();
                    DataSet Ds_Gpo = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                    DataTable Dt_Gpo = new DataTable();

                    try
                    {
                        Mi_SQL.Append("SELECT " + Cat_Organigrama.Campo_Dependencia_ID);
                        Mi_SQL.Append(" FROM " + Cat_Organigrama.Tabla_Cat_Organigrama);
                        Mi_SQL.Append(" WHERE " + Cat_Organigrama.Campo_Empleado_ID + " = '" + Negocio.P_Empleado_ID.Trim() + "'");
                        Mi_SQL.Append(" AND " + Cat_Organigrama.Campo_Tipo + " = 'COORDINADOR_DIRECCION'");
                        //Mi_SQL.Append(" AND " + Cat_Organigrama.Campo_Modulo + " = 'NOMINA'");

                        Ds_Dep = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        if (Ds_Dep != null)
                        {
                            Dt_Dep = Ds_Dep.Tables[0];

                            if (Dt_Dep != null && Dt_Dep.Rows.Count > 0)
                            {
                                Mi_SQL = new StringBuilder();
                                Mi_SQL.Append("SELECT " + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                Mi_SQL.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
                                Mi_SQL.Append(" WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Dt_Dep.Rows[0][Cat_Organigrama.Campo_Dependencia_ID] + "'");
                                Ds_Gpo = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                                if (Ds_Gpo != null)
                                {
                                    Dt_Gpo = Ds_Gpo.Tables[0];
                                    if (Dt_Gpo != null && Dt_Gpo.Rows.Count > 0)
                                    {
                                        Dt_Dep.Columns.Add(Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                        Dt_Dep.Rows[0][Cat_Dependencias.Campo_Grupo_Dependencia_ID] = Dt_Gpo.Rows[0][Cat_Dependencias.Campo_Grupo_Dependencia_ID].ToString().Trim();
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception Ex)
                    {
                        String Mensaje = "Error al intentar consultar los registros del grupo dependencia. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                        throw new Exception(Mensaje);
                    }
                    return Dt_Dep;
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Rol
                ///DESCRIPCIÓN          : consulta para obtener el rol del empleado
                ///PARAMETROS           : 
                ///CREO                 : Leslie Gonzalez Vázquez
                ///FECHA_CREO           : 8/Marzo/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static String Consultar_Rol(String Rol_ID)
                {
                    StringBuilder Mi_Sql = new StringBuilder();
                    DataTable Dt_Rol_Administrador = new DataTable();
                    String Administrador = String.Empty;

                    try
                    {
                        Mi_Sql.Append("SELECT " + Apl_Grupos_Roles.Tabla_Apl_Grupos_Roles + "." + Apl_Grupos_Roles.Campo_Nombre);
                        Mi_Sql.Append(" FROM " + Apl_Grupos_Roles.Tabla_Apl_Grupos_Roles);
                        Mi_Sql.Append(" LEFT OUTER JOIN " + Apl_Cat_Roles.Tabla_Apl_Cat_Roles);
                        Mi_Sql.Append(" ON " + Apl_Cat_Roles.Tabla_Apl_Cat_Roles + "." + Apl_Cat_Roles.Campo_Grupo_Roles_ID);
                        Mi_Sql.Append(" = " + Apl_Grupos_Roles.Tabla_Apl_Grupos_Roles + "." + Apl_Grupos_Roles.Campo_Grupo_Roles_ID);
                        Mi_Sql.Append(" WHERE " + Apl_Cat_Roles.Tabla_Apl_Cat_Roles + "." + Apl_Cat_Roles.Campo_Rol_ID + " = '" + Rol_ID + "'");

                        Dt_Rol_Administrador = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];

                        if (Dt_Rol_Administrador != null)
                        {
                            if (Dt_Rol_Administrador.Rows.Count > 0)
                            {
                                if (Dt_Rol_Administrador.Rows[0]["NOMBRE"].ToString().Trim().Equals("PRESUPUESTOS"))
                                {
                                    Administrador = "Presupuestos";
                                }
                                else if (Dt_Rol_Administrador.Rows[0]["NOMBRE"].ToString().Trim().Equals("RECURSOS HUMANOS"))
                                {
                                    Administrador = "Nomina";
                                }
                                else 
                                {
                                    Administrador = String.Empty;
                                }
                            }
                        }

                        return Administrador;
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error al intentar consultar el rol. Error: [" + Ex.Message + "]");
                    }
                }

            #endregion

            #region (PSP EGR)
                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_AF
                ///DESCRIPCIÓN          : consulta para obtener los datos de las areas funcionales
                ///PARAMETROS           : 
                ///CREO                 : Leslie Gonzalez Vázquez
                ///FECHA_CREO           : 17/Agosto/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_AF(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_Sql = new StringBuilder();
                    try
                    {
                        Mi_Sql.Append("SELECT DISTINCT UPPER(" + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " +' '+ ");
                        Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Descripcion + ") AS CLAVE_NOMBRE, ");
                        Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                        Mi_Sql.Append(" FROM " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                        Mi_Sql.Append(" ON " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                        Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                        Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                            Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_Usuario.Trim()))
                        {
                            if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                            else
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Busqueda.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE UPPER(" + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " + ' ' + ");
                                Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Descripcion + ")");
                                Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND UPPER(" + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " + ' ' + ");
                                Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Descripcion + ")");
                                Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                            }
                        }

                        Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error al intentar consultar los registros de las areas funcionales. Error: [" + Ex.Message + "]");
                    }
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Dependencias
                ///DESCRIPCIÓN          : consulta para obtener los datos de las dependencias
                ///PARAMETROS           : 
                ///CREO                 : Leslie Gonzalez Vázquez
                ///FECHA_CREO           : 24/Mayo/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Dependencias(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_Sql = new StringBuilder();
                    try
                    {
                        Mi_Sql.Append("SELECT DISTINCT UPPER(" + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " +' '+ ");
                        Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + ") AS CLAVE_NOMBRE, ");
                        Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID + ", ");
                        Mi_Sql.Append("UPPER(" + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Nombre + ") AS NOM_GPO_DEP, ");
                        Mi_Sql.Append("UPPER(" + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + ") AS NOM_DEP ");
                        Mi_Sql.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
                        Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias);
                        Mi_Sql.Append(" ON " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID);
                        Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                            Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_Usuario.Trim()))
                        {
                            if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                            else
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Busqueda.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE UPPER(" + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " + ' ' + ");
                                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + ")");
                                Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND UPPER(" + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " + ' ' + ");
                                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + ")");
                                Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                            }
                        }

                        Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error al intentar consultar los registros de las depencencias. Error: [" + Ex.Message + "]");
                    }
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Capitulo
                ///DESCRIPCIÓN          : consulta para obtener los datos de los capitulos
                ///PARAMETROS           : 
                ///CREO                 : Leslie Gonzalez Vázquez
                ///FECHA_CREO           : 24/Mayo/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Capitulo(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_Sql = new StringBuilder();
                    try
                    {
                        Mi_Sql.Append("SELECT DISTINCT UPPER(" + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + " +' '+ ");
                        Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Descripcion + ") AS CLAVE_NOMBRE, ");
                        Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                        Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas);
                        Mi_Sql.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                        Mi_Sql.Append(" ON " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                        Mi_Sql.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                            Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_Usuario.Trim()))
                        {
                            if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                            else
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Busqueda.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE UPPER(" + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + " + ' ' + ");
                                Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Descripcion + ")");
                                Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND UPPER(" + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + " + ' ' + ");
                                Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Descripcion + ")");
                                Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                            }
                        }

                        Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error al intentar consultar los registros de los capitulos. Error: [" + Ex.Message + "]");
                    }
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Concepto
                ///DESCRIPCIÓN          : consulta para obtener los datos de los conceptos
                ///PARAMETROS           : 
                ///CREO                 : Leslie Gonzalez Vázquez
                ///FECHA_CREO           : 24/Mayo/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Concepto(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_Sql = new StringBuilder();
                    try
                    {
                        Mi_Sql.Append("SELECT DISTINCT UPPER(" + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave + " +' '+ ");
                        Mi_Sql.Append(Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Descripcion + ") AS CLAVE_NOMBRE, ");
                        Mi_Sql.Append(Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                        Mi_Sql.Append(" FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas);
                        Mi_Sql.Append(" ON " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                        Mi_Sql.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                        Mi_Sql.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                            Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_Usuario.Trim()))
                        {
                            if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                            else
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Capitulo_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Busqueda.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE UPPER(" + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave + " + ' ' + ");
                                Mi_Sql.Append(Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Descripcion + ")");
                                Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND UPPER(" + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave + " + ' ' + ");
                                Mi_Sql.Append(Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Descripcion + ")");
                                Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                            }
                        }

                        Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error al intentar consultar los registros de los conceptos. Error: [" + Ex.Message + "]");
                    }
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Partida_Generica
                ///DESCRIPCIÓN          : consulta para obtener los datos de las partidas Genericas
                ///PARAMETROS           : 
                ///CREO                 : Leslie Gonzalez Vázquez
                ///FECHA_CREO           : 24/Mayo/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Partida_Generica(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_Sql = new StringBuilder();
                    try
                    {
                        Mi_Sql.Append("SELECT DISTINCT UPPER(" + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Clave + " +' '+ ");
                        Mi_Sql.Append(Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Descripcion + ") AS CLAVE_NOMBRE, ");
                        Mi_Sql.Append(Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                        Mi_Sql.Append(" ON " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                        Mi_Sql.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                        Mi_Sql.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                            Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_Usuario.Trim()))
                        {
                            if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                            else
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Capitulo_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Busqueda.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE UPPER(" + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Clave + " + ' ' + ");
                                Mi_Sql.Append(Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Descripcion + ")");
                                Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND UPPER(" + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Clave + " + ' ' + ");
                                Mi_Sql.Append(Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Descripcion + ")");
                                Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                            }
                        }

                        Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error al intentar consultar los registros de las partidas genericas Error: [" + Ex.Message + "]");
                    }
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Partida
                ///DESCRIPCIÓN          : consulta para obtener los datos de las partidas
                ///PARAMETROS           : 
                ///CREO                 : Leslie Gonzalez Vázquez
                ///FECHA_CREO           : 24/Mayo/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Partida(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_Sql = new StringBuilder();
                    try
                    {
                        Mi_Sql.Append("SELECT DISTINCT UPPER(" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " +' '+ ");
                        Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + ") AS CLAVE_NOMBRE, ");
                        Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                        Mi_Sql.Append(" FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas);
                        Mi_Sql.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                        Mi_Sql.Append(" ON " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                        Mi_Sql.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                        Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                            Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_Usuario.Trim()))
                        {
                            if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                            else
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Capitulo_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Partida_Generica_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Partida_Generica_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Partida_Generica_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Busqueda.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE UPPER(" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " + ' ' + ");
                                Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + ")"); 
                                Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND UPPER(" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " + ' ' + ");
                                Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + ")");
                                Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                            }
                        }

                        Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error al intentar consultar los registros de las partidas Error: [" + Ex.Message + "]");
                    }
                }
            #endregion

            #region (PSP ING)
                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Rubros
                ///DESCRIPCIÓN          : Obtiene datos de  las los rubros del presupuesto de ingresos
                ///PARAMETROS           : 
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 25/Abril/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Rubros(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                    DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                    DataTable Dt = new DataTable();
                    try
                    {
                        Mi_SQL.Append("SELECT DISTINCT " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID + ", ");
                        Mi_SQL.Append(" REPLACE(" + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + " AS CLAVE_NOMBRE ");
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                            Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_Ing.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_Ing.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_Ing.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Busqueda.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + " + ' ' + ");
                                Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion);
                                Mi_SQL.Append(" LIKE '%" + Negocio.P_Busqueda.Trim() + "%'");
                                Mi_SQL.Append(" OR " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + " + ' ' + ");
                                Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion);
                                Mi_SQL.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + " + ' ' + ");
                                Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion);
                                Mi_SQL.Append(" LIKE '%" + Negocio.P_Busqueda.Trim() + "%'");
                                Mi_SQL.Append(" OR " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + " + ' ' + ");
                                Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion);
                                Mi_SQL.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                            }
                        }

                        Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                        Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        if (Ds != null)
                        {
                            Dt = Ds.Tables[0];
                        }
                    }
                    catch (Exception Ex)
                    {
                        String Mensaje = "Error al intentar consultar los registros de los rubros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                        throw new Exception(Mensaje);
                    }
                    return Dt;
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Tipos
                ///DESCRIPCIÓN          : Obtiene datos de los tipos del presupuesto de ingresos
                ///PARAMETROS           : 
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 25/Abril/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Tipos(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                    DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                    DataTable Dt = new DataTable();
                    try
                    {
                        Mi_SQL.Append("SELECT DISTINCT " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID + ", ");
                        Mi_SQL.Append(" REPLACE(" + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + " AS CLAVE_NOMBRE ");
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                            Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_Ing.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_Ing.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_Ing.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Busqueda.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + " + ' ' + ");
                                Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion);
                                Mi_SQL.Append(" LIKE '%" + Negocio.P_Busqueda.Trim() + "%'");
                                Mi_SQL.Append(" OR " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + " + ' ' + ");
                                Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion);
                                Mi_SQL.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + " + ' ' + ");
                                Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion);
                                Mi_SQL.Append(" LIKE '%" + Negocio.P_Busqueda.Trim() + "%'");
                                Mi_SQL.Append(" OR " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + " + ' ' + ");
                                Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion);
                                Mi_SQL.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                            }
                        }

                        Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                        Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        if (Ds != null)
                        {
                            Dt = Ds.Tables[0];
                        }
                    }
                    catch (Exception Ex)
                    {
                        String Mensaje = "Error al intentar consultar los registros de los tipos Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                        throw new Exception(Mensaje);
                    }
                    return Dt;
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Clases
                ///DESCRIPCIÓN          : Obtiene datos de  las clases del presupuesto de ingresos
                ///PARAMETROS           : 
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 25/Abril/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Clases(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                    DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                    DataTable Dt = new DataTable();
                    try
                    {
                        Mi_SQL.Append("SELECT DISTINCT " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + ", ");
                        Mi_SQL.Append(" REPLACE(" + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + " AS CLAVE_NOMBRE ");
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                            Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_Ing.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_Ing.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_Ing.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Busqueda.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + " + ' ' + ");
                                Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion);
                                Mi_SQL.Append(" LIKE '%" + Negocio.P_Busqueda.Trim() + "%'");
                                Mi_SQL.Append(" OR " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + " + ' ' + ");
                                Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion);
                                Mi_SQL.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + " + ' ' + ");
                                Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion);
                                Mi_SQL.Append(" LIKE '%" + Negocio.P_Busqueda.Trim() + "%'");
                                Mi_SQL.Append(" OR " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + " + ' ' + ");
                                Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion);
                                Mi_SQL.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                            }
                        }

                        Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                        Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        if (Ds != null)
                        {
                            Dt = Ds.Tables[0];
                        }
                    }
                    catch (Exception Ex)
                    {
                        String Mensaje = "Error al intentar consultar los registros de las clases Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                        throw new Exception(Mensaje);
                    }
                    return Dt;
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Conceptos_Ing
                ///DESCRIPCIÓN          : Obtiene datos de  los conceptos del presupuesto de ingresos
                ///PARAMETROS           : 
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 25/Abril/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Conceptos_Ing(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                    DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                    DataTable Dt = new DataTable();
                    try
                    {
                        Mi_SQL.Append("SELECT DISTINCT " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + ", ");
                        Mi_SQL.Append(" REPLACE(" + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + " AS CLAVE_NOMBRE ");
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                            Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_Ing.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_Ing.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_Ing.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Clase_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Busqueda.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + " + ' ' + ");
                                Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                                Mi_SQL.Append(" LIKE '%" + Negocio.P_Busqueda.Trim() + "%'");
                                Mi_SQL.Append(" OR " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + " + ' ' + ");
                                Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                                Mi_SQL.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + " + ' ' + ");
                                Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                                Mi_SQL.Append(" LIKE '%" + Negocio.P_Busqueda.Trim() + "%'");
                                Mi_SQL.Append(" OR " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + " + ' ' + ");
                                Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                                Mi_SQL.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                            }
                        }

                        Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                        Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        if (Ds != null)
                        {
                            Dt = Ds.Tables[0];
                        }
                    }
                    catch (Exception Ex)
                    {
                        String Mensaje = "Error al intentar consultar los registros de los conceptos Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                        throw new Exception(Mensaje);
                    }
                    return Dt;
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_SubConceptos
                ///DESCRIPCIÓN          : Obtiene datos de  los subconceptos del presupuesto de ingresos
                ///PARAMETROS           : 
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 19/Mayo/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_SubConceptos(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                    DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                    DataTable Dt = new DataTable();
                    try
                    {
                        Mi_SQL.Append("SELECT DISTINCT " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + ", ");
                        Mi_SQL.Append(" REPLACE(" + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion + " AS CLAVE_NOMBRE ");
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                            Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_Ing.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_Ing.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_Ing.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Clase_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Busqueda.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + " + ' ' + ");
                                Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion);
                                Mi_SQL.Append(" LIKE '%" + Negocio.P_Busqueda.Trim() + "%'");
                                Mi_SQL.Append(" OR " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + " + ' ' + ");
                                Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion);
                                Mi_SQL.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + " + ' ' + ");
                                Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion);
                                Mi_SQL.Append(" LIKE '%" + Negocio.P_Busqueda.Trim() + "%'");
                                Mi_SQL.Append(" OR " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + " + ' ' + ");
                                Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion);
                                Mi_SQL.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                            }
                        }

                        Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                        Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        if (Ds != null)
                        {
                            Dt = Ds.Tables[0];
                        }
                    }
                    catch (Exception Ex)
                    {
                        String Mensaje = "Error al intentar consultar los registros de los conceptos Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                        throw new Exception(Mensaje);
                    }
                    return Dt;
                }
            #endregion

            #region (NIVEL PSP ING)
                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_FF_Ing
                ///DESCRIPCIÓN          : Obtiene datos de  las fuentes de financiamiento del estado analitico de ingresos
                ///PARAMETROS           : 
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 24/Mayo/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Nivel_FF_Ing(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                    StringBuilder Mi_SQL_Parametros = new StringBuilder();
                    DataSet Ds_FF = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                    DataTable Dt_FF = new DataTable();
                    try
                    {
                        Mi_SQL.Append("SELECT DISTINCT " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " + '_' + ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " AS ID, ");
                        Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                        Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " AS FF_ID, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ") AS ESTIMADO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ") AS AMPLIACION, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ") AS REDUCCION, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ") AS MODIFICADO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ") AS DEVENGADO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ") AS RECAUDADO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ") + ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ") AS DEV_REC, ");
                        Mi_SQL.Append("'' AS COMPROMETIDO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ") AS POR_RECAUDAR, ");
                        Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                        Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS FF ");
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                        Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                        Mi_SQL.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_SQL_Parametros.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                            Mi_SQL_Parametros.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            if (!Mi_SQL_Parametros.ToString().Contains("WHERE"))
                            {
                                Mi_SQL_Parametros.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL_Parametros.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL_Parametros.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL_Parametros.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL_Parametros.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_Ing_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_Ing_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" WHERE  " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_Ing_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_Ing_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" AND  " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID.Trim()))
                        {
                            if (!Mi_SQL_Parametros.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_RubroF_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_Rubro_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_RubroF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_RubroF_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_Rubro_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_RubroF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID.Trim()))
                        {
                            if (!Mi_SQL_Parametros.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_TipoF_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_TipoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_TipoF_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_TipoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Clase_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL_Parametros.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ClaseF_Ing_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_Clase_Ing_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_ClaseF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ClaseF_Ing_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_Clase_Ing_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_ClaseF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL_Parametros.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_Ing_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_Concepto_Ing_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_ConceptoF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_Ing_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_Concepto_Ing_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_ConceptoF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_SubConcepto_ID.Trim()))
                        {
                            if (!Mi_SQL_Parametros.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_SubConceptoF_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConcepto_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_SubConceptoF_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" AND" + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConcepto_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                                }
                            }
                        }

                        Mi_SQL.Append(" " + Mi_SQL_Parametros.ToString().Trim());

                        Mi_SQL.Append(" GROUP BY ");
                        Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + ", ");
                        Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                        Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                        Ds_FF = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        if (Ds_FF != null)
                        {
                            Dt_FF = Ds_FF.Tables[0];
                        }
                    }
                    catch (Exception Ex)
                    {
                        String Mensaje = "Error al intentar consultar los registros de las fuentes de fianciamiento. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                        throw new Exception(Mensaje);
                    }
                    return Dt_FF;
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_PP_Ing
                ///DESCRIPCIÓN          : Obtiene datos de  los proyectos programas del estado analitico de ingresos
                ///PARAMETROS           : 
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 22/Agosto/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Nivel_PP_Ing(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                    StringBuilder Mi_SQL_Parametros = new StringBuilder();
                    DataSet Ds_PP = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                    DataTable Dt_PP = new DataTable();
                    try
                    {
                        Mi_SQL.Append("SELECT DISTINCT " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " + '_' + ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        { Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " + '_' + "); }
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " AS ID, ");
                        Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                        Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        { Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " AS FF_ID, "); }
                        else { Mi_SQL.Append(" ''  AS FF_ID, "); }
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " AS PP_ID, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ") AS ESTIMADO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ") AS AMPLIACION, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ") AS REDUCCION, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ") AS MODIFICADO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ") AS DEVENGADO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ") AS RECAUDADO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ") + ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ") AS DEV_REC, ");
                        Mi_SQL.Append("'' AS COMPROMETIDO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ") AS POR_RECAUDAR, ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                            Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS FF ");
                        }
                        else { Mi_SQL.Append(" '' AS FF "); }
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                        Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                        Mi_SQL.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_SQL_Parametros.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                            Mi_SQL_Parametros.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            if (!Mi_SQL_Parametros.ToString().Contains("WHERE"))
                            {
                                Mi_SQL_Parametros.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL_Parametros.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL_Parametros.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL_Parametros.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL_Parametros.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_Ing_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_Ing_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" WHERE  " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_Ing_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_Ing_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" AND  " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID.Trim()))
                        {
                            if (!Mi_SQL_Parametros.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_RubroF_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_Rubro_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_RubroF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_RubroF_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_Rubro_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_RubroF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID.Trim()))
                        {
                            if (!Mi_SQL_Parametros.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_TipoF_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_TipoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_TipoF_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_TipoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Clase_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL_Parametros.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ClaseF_Ing_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_Clase_Ing_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_ClaseF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ClaseF_Ing_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_Clase_Ing_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_ClaseF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL_Parametros.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_Ing_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_Concepto_Ing_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_ConceptoF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_Ing_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_Concepto_Ing_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_ConceptoF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_SubConcepto_ID.Trim()))
                        {
                            if (!Mi_SQL_Parametros.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_SubConceptoF_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConcepto_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_SubConceptoF_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" AND" + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConcepto_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                                }
                            }
                        }

                        Mi_SQL.Append(" " + Mi_SQL_Parametros.ToString().Trim());

                        Mi_SQL.Append(" GROUP BY ");
                        Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + ", ");
                        Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + ", ");
                        Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion);
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            Mi_SQL.Append(", " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                            Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion);
                        }
                        Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                        Ds_PP = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        if (Ds_PP != null)
                        {
                            Dt_PP = Ds_PP.Tables[0];
                        }
                    }
                    catch (Exception Ex)
                    {
                        String Mensaje = "Error al intentar consultar los registros de los proyectos programas. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                        throw new Exception(Mensaje);
                    }
                    return Dt_PP;
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_Rubros
                ///DESCRIPCIÓN          : Obtiene datos de  las los rubros del presupuesto de ingresos
                ///PARAMETROS           : 
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 24/Mayo/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Nivel_Rubros(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                    StringBuilder Mi_SQL_Parametros = new StringBuilder();
                    DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                    DataTable Dt = new DataTable();
                    try
                    {
                        Mi_SQL.Append("SELECT DISTINCT " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID + " + '_' + ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        { Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " + '_' + "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        { Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " + '_' + "); }
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " AS ID, ");
                        Mi_SQL.Append("REPLACE(" + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        { Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " AS FF_ID, "); }
                        else { Mi_SQL.Append(" ''  AS FF_ID, "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        { Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " AS PP_ID, "); }
                        else { Mi_SQL.Append(" ''  AS PP_ID, "); }
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ") AS ESTIMADO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ") AS AMPLIACION, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ") AS REDUCCION, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ") AS MODIFICADO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ") AS DEVENGADO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ") AS RECAUDADO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ") + ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ") AS DEV_REC, ");
                        Mi_SQL.Append("'' AS COMPROMETIDO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ") AS POR_RECAUDAR, ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                            Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS FF ");
                        }
                        else { Mi_SQL.Append(" '' AS FF "); }
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                        Mi_SQL.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);


                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_SQL_Parametros.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                            Mi_SQL_Parametros.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            if (!Mi_SQL_Parametros.ToString().Contains("WHERE"))
                            {
                                Mi_SQL_Parametros.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL_Parametros.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL_Parametros.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL_Parametros.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL_Parametros.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_Ing_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_Ing_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_Ing_ID.Trim() + "')");
                                }
                                else 
                                {
                                    Mi_SQL_Parametros.Append(" WHERE  " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_Ing_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_Ing_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" AND  " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID.Trim()))
                        {
                            if (!Mi_SQL_Parametros.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_RubroF_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID+ " = '" + Negocio.P_Rubro_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_RubroF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_RubroF_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_Rubro_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_RubroF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID.Trim()))
                        {
                            if (!Mi_SQL_Parametros.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_TipoF_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_TipoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_TipoF_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_TipoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Clase_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL_Parametros.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ClaseF_Ing_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_Clase_Ing_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_ClaseF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ClaseF_Ing_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_Clase_Ing_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_ClaseF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL_Parametros.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_Ing_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_Concepto_Ing_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_ConceptoF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_Ing_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_Concepto_Ing_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_ConceptoF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_SubConcepto_ID.Trim()))
                        {
                            if (!Mi_SQL_Parametros.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_SubConceptoF_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConcepto_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_SubConceptoF_ID.Trim()))
                                {
                                    Mi_SQL_Parametros.Append(" AND" + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave);
                                    Mi_SQL_Parametros.Append(" BETWEEN (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConcepto_ID.Trim() + "')");
                                    Mi_SQL_Parametros.Append(" AND (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL_Parametros.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                                    Mi_SQL_Parametros.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                                }
                            }
                        }

                        Mi_SQL.Append(" " + Mi_SQL_Parametros.ToString().Trim());

                        Mi_SQL.Append(" GROUP BY ");
                        Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + ", ");
                        Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + ", ");
                        Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            Mi_SQL.Append(", " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                            Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion);
                        }
                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        {
                            Mi_SQL.Append(", " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                        }

                        Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                        Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        if (Ds != null)
                        {
                            Dt = Ds.Tables[0];
                        }
                    }
                    catch (Exception Ex)
                    {
                        String Mensaje = "Error al intentar consultar los registros de los rubros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                        throw new Exception(Mensaje);
                    }
                    return Dt;
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_Tipos
                ///DESCRIPCIÓN          : Obtiene datos de los tipos del presupuesto de ingresos
                ///PARAMETROS           : 
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 25/Mayo/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Nivel_Tipos(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                    DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                    DataTable Dt = new DataTable();
                    try
                    {
                        Mi_SQL.Append("SELECT DISTINCT " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " + '_' + ");
                        Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID + " + '_' + ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        { Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " + '_' + "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        { Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " + '_' + "); }
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " AS ID, ");
                        Mi_SQL.Append("REPLACE(" + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + ", ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        { Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " AS FF_ID, "); }
                        else { Mi_SQL.Append(" ''  AS FF_ID, "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        { Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " AS PP_ID, "); }
                        else { Mi_SQL.Append(" ''  AS PP_ID, "); }
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ") AS ESTIMADO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ") AS AMPLIACION, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ") AS REDUCCION, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ") AS MODIFICADO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ") AS DEVENGADO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ") AS RECAUDADO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ") + ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ") AS DEV_REC, ");
                        Mi_SQL.Append("'' AS COMPROMETIDO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ") AS POR_RECAUDAR, ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                            Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS FF ");
                        }
                        else { Mi_SQL.Append(" '' AS FF "); }
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                        Mi_SQL.Append("  LEFT OUTER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                        Mi_SQL.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                            Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE  " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND  " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_RubroF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_Rubro_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_RubroF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_RubroF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_Rubro_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_RubroF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_TipoF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_TipoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_TipoF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_TipoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Clase_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ClaseF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_Clase_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_ClaseF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ClaseF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_Clase_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_ClaseF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_Concepto_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_ConceptoF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_Concepto_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_ConceptoF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_SubConcepto_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_SubConceptoF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConcepto_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_SubConceptoF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND" + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConcepto_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                                }
                            }
                        }

                        Mi_SQL.Append(" GROUP BY ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                        Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + ", ");
                        Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + ", ");
                        Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            Mi_SQL.Append(", " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                            Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion);
                        }
                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        {
                            Mi_SQL.Append(", " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                        }

                        Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                        Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        if (Ds != null)
                        {
                            Dt = Ds.Tables[0];
                        }
                    }
                    catch (Exception Ex)
                    {
                        String Mensaje = "Error al intentar consultar los registros de los tipos Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                        throw new Exception(Mensaje);
                    }
                    return Dt;
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_Clases
                ///DESCRIPCIÓN          : Obtiene datos de  las clases del presupuesto de ingresos
                ///PARAMETROS           : 
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 25/Mayo/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Nivel_Clases(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                    DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                    DataTable Dt = new DataTable();
                    try
                    {
                        Mi_SQL.Append("SELECT DISTINCT " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " + '_' + ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " + '_' + ");
                        Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " + '_' + ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        { Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " + '_' + "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        { Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " + '_' + "); }
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " AS ID, ");
                        Mi_SQL.Append("REPLACE(" + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + ", ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        { Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " AS FF_ID, "); }
                        else { Mi_SQL.Append(" ''  AS FF_ID, "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        { Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " AS PP_ID, "); }
                        else { Mi_SQL.Append(" ''  AS PP_ID, "); }
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ") AS ESTIMADO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ") AS AMPLIACION, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ") AS REDUCCION, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ") AS MODIFICADO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ") AS DEVENGADO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ") AS RECAUDADO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ") + ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ") AS DEV_REC, ");
                        Mi_SQL.Append("'' AS COMPROMETIDO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ") AS POR_RECAUDAR, ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                            Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS FF ");
                        }
                        else { Mi_SQL.Append(" '' AS FF "); }
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                        Mi_SQL.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                            Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE  " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND  " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_RubroF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_Rubro_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_RubroF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_RubroF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_Rubro_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_RubroF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_TipoF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_TipoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_TipoF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_TipoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Clase_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ClaseF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_Clase_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_ClaseF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ClaseF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_Clase_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_ClaseF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_Concepto_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_ConceptoF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_Concepto_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_ConceptoF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_SubConcepto_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_SubConceptoF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConcepto_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_SubConceptoF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND" + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConcepto_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                                }
                            }
                        }

                        Mi_SQL.Append(" GROUP BY ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + ", ");
                        Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + ", ");
                        Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + ", ");
                        Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            Mi_SQL.Append(", " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                            Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion);
                        }
                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        {
                            Mi_SQL.Append(", " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                        }

                        Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                        Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        if (Ds != null)
                        {
                            Dt = Ds.Tables[0];
                        }
                    }
                    catch (Exception Ex)
                    {
                        String Mensaje = "Error al intentar consultar los registros de las clases Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                        throw new Exception(Mensaje);
                    }
                    return Dt;
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_Conceptos
                ///DESCRIPCIÓN          : Obtiene datos de  los conceptos del presupuesto de ingresos
                ///PARAMETROS           : 
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 25/Mayo/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Nivel_Conceptos_Ing(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                    DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                    DataTable Dt = new DataTable();
                    try
                    {
                        Mi_SQL.Append("SELECT DISTINCT " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " + '_' + ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " + '_' + ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " + '_' + ");
                        Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " + '_' + ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        { Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " + '_' + "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        { Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " + '_' + "); }
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " AS ID, ");
                        Mi_SQL.Append("REPLACE(" + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + ", ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        { Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " AS FF_ID, "); }
                        else { Mi_SQL.Append(" ''  AS FF_ID, "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        { Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " AS PP_ID, "); }
                        else { Mi_SQL.Append(" ''  AS PP_ID, "); }
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ") AS ESTIMADO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ") AS AMPLIACION, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ") AS REDUCCION, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ") AS MODIFICADO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ") AS DEVENGADO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ") AS RECAUDADO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ") + ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ") AS DEV_REC, ");
                        Mi_SQL.Append("'' AS COMPROMETIDO, ");
                        Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ") AS POR_RECAUDAR, ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                            Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS FF ");
                        }
                        else { Mi_SQL.Append(" '' AS FF "); }
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                        Mi_SQL.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                        
                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                            Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE  " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND  " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_RubroF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_Rubro_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_RubroF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_RubroF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_Rubro_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_RubroF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_TipoF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_TipoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_TipoF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_TipoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Clase_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ClaseF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_Clase_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_ClaseF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ClaseF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_Clase_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_ClaseF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_Concepto_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_ConceptoF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_Concepto_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_ConceptoF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_SubConcepto_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_SubConceptoF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConcepto_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_SubConceptoF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND" + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConcepto_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                                }
                            }
                        }

                        Mi_SQL.Append(" GROUP BY ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + ", ");
                        Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + ", ");
                        Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", ");
                        Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            Mi_SQL.Append(", " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                            Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion);
                        }
                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        {
                            Mi_SQL.Append(", " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                        }
                        Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                        Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        if (Ds != null)
                        {
                            Dt = Ds.Tables[0];
                        }
                    }
                    catch (Exception Ex)
                    {
                        String Mensaje = "Error al intentar consultar los registros de los conceptos Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                        throw new Exception(Mensaje);
                    }
                    return Dt;
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_SubConceptos
                ///DESCRIPCIÓN          : Obtiene datos de  los conceptos del presupuesto de ingresos
                ///PARAMETROS           : 
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 25/Mayo/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Nivel_SubConceptos(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                    DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                    DataTable Dt = new DataTable();
                    try
                    {
                        Mi_SQL.Append("SELECT " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " + '_' + ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " + '_' + ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " + '_' + ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + " + '_' + ");
                        Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " + '_' + ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " + '_' + ");
                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        { Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " + '_' + "); }
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " AS ID, ");
                        Mi_SQL.Append("REPLACE(" + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID+ ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID+ " AS FF_ID, ");
                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        { Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " AS PP_ID, "); }
                        else { Mi_SQL.Append(" ''  AS PP_ID, "); }
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + " AS ESTIMADO, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + " AS AMPLIACION, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + " AS REDUCCION, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + " AS MODIFICADO, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + " AS DEVENGADO, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + " AS RECAUDADO, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + " + ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + " AS DEV_REC, ");
                        Mi_SQL.Append(" '' AS COMPROMETIDO, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + " AS POR_RECAUDAR, ");
                        Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + '' +");
                        Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS FF ");
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                        Mi_SQL.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                            Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE  " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND  " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_RubroF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_Rubro_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_RubroF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_RubroF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_Rubro_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_RubroF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_TipoF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_TipoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_TipoF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_TipoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Clase_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ClaseF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_Clase_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_ClaseF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ClaseF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_Clase_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_ClaseF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_Concepto_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_ConceptoF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_Concepto_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_ConceptoF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_SubConcepto_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_SubConceptoF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConcepto_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_SubConceptoF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND" + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConcepto_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                                }
                            }
                        }

                        Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                        Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        if (Ds != null)
                        {
                            Dt = Ds.Tables[0];
                        }
                    }
                    catch (Exception Ex)
                    {
                        String Mensaje = "Error al intentar consultar los registros de los conceptos Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                        throw new Exception(Mensaje);
                    }
                    return Dt;
                }
            #endregion

            #region (NIVEL PSP EGR)
                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_FF
                ///DESCRIPCIÓN          : consulta para obtener los datos de las fuentes de financiamiento
                ///PARAMETROS           : 
                ///CREO                 : Leslie Gonzalez Vázquez
                ///FECHA_CREO           : 23/Agosto/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Nivel_FF(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_Sql = new StringBuilder();
                    try
                    {
                        Mi_Sql.Append("SELECT " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " +' '+ ");
                        Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS nombre, ");
                        Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " + '_' + ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " AS id, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " AS FF_ID, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ") AS APROBADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ") AS AMPLIACION, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ") AS REDUCCION, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ") AS MODIFICADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ") AS DEVENGADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ") AS PAGADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ") + ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ") AS DEV_PAG, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ") AS COMPROMETIDO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ") + ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ") AS DISPONIBLE, ");
                        Mi_Sql.Append("'' AS FF, '' AS UR, '' AS PP");
                        Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas);
                        Mi_Sql.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                        Mi_Sql.Append(" ON " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                        Mi_Sql.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                            Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_Usuario.Trim()))
                        {
                            if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                            else
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        {

                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_DependenciaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_DependenciaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_DependenciaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_DependenciaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                }
                            }

                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Capitulo_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_CapituloF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_Capitulo_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_CapituloF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_CapituloF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_Capitulo_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_CapituloF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_Concepto_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_ConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_Concepto_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_ConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Partida_Generica_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_Partida_GenericaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_Generica_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_GenericaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_Generica_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_Partida_GenericaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_Generica_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_GenericaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_Generica_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_PartidaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_PartidaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_PartidaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_PartidaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                                }
                            }
                        }

                        Mi_Sql.Append(" GROUP BY ");
                        Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                        Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + ", ");
                        Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);

                        Mi_Sql.Append(" ORDER BY nombre ASC");

                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error al intentar consultar los registros de las fuentes de financiamiento. Error: [" + Ex.Message + "]");
                    }
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_AF
                ///DESCRIPCIÓN          : consulta para obtener los datos de las areas funcinales
                ///PARAMETROS           : 
                ///CREO                 : Leslie Gonzalez Vázquez
                ///FECHA_CREO           : 23/Agosto/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Nivel_AF(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_Sql = new StringBuilder();
                    try
                    {
                        Mi_Sql.Append("SELECT " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " +' '+ ");
                        Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Descripcion + " AS nombre, ");
                        if(!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim())){
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " + '_' + ");}
                        Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional+ "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID + " + '_' + ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " AS id, ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim())){
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " AS FF_ID, ");}
                        else { Mi_Sql.Append(" '' AS FF_ID, "); }
                        Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID + " AS AF_ID, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ") AS APROBADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ") AS AMPLIACION, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ") AS REDUCCION, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ") AS MODIFICADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ") AS DEVENGADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ") AS PAGADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ") + ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ") AS DEV_PAG, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ") AS COMPROMETIDO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ") + ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ") AS DISPONIBLE, ");
                        Mi_Sql.Append("'' AS FF, '' AS UR, '' AS PP");
                        Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas);
                        Mi_Sql.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                        Mi_Sql.Append(" ON " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                        Mi_Sql.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                        Mi_Sql.Append(" ON " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                        Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                            Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_Usuario.Trim()))
                        {
                            if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                            else
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        {

                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_DependenciaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_DependenciaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_DependenciaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_DependenciaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                }
                            }

                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Capitulo_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_CapituloF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_Capitulo_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_CapituloF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_CapituloF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_Capitulo_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_CapituloF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_Concepto_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_ConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_Concepto_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_ConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Partida_Generica_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_Partida_GenericaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_Generica_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_GenericaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_Generica_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_Partida_GenericaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_Generica_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_GenericaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_Generica_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_PartidaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_PartidaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_PartidaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_PartidaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                                }
                            }
                        }

                        Mi_Sql.Append(" GROUP BY ");
                        Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + ", ");
                        Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Descripcion + ", ");
                        Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + ", ");
                        Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()) || Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                        {Mi_Sql.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);}

                        Mi_Sql.Append(" ORDER BY nombre ASC");

                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error al intentar consultar los registros de las areas funcionales. Error: [" + Ex.Message + "]");
                    }
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_PP
                ///DESCRIPCIÓN          : consulta para obtener los datos de los programas
                ///PARAMETROS           : 
                ///CREO                 : Leslie Gonzalez Vázquez
                ///FECHA_CREO           : 23/Agosto/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Nivel_PP(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_Sql = new StringBuilder();
                    try
                    {
                        Mi_Sql.Append("SELECT " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " +' '+ ");
                        Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS nombre, ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim())){
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " + '_' + ");}
                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim())){
                            Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID + " + '_' + ");}
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " + '_' + ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " AS id, ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim())){
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " AS FF_ID, ");}
                        else { Mi_Sql.Append(" '' AS FF_ID, "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim())){
                            Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID + " AS AF_ID, ");}
                        else { Mi_Sql.Append(" '' AS AF_ID, "); }
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " AS PP_ID, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ") AS APROBADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ") AS AMPLIACION, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ") AS REDUCCION, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ") AS MODIFICADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ") AS DEVENGADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ") AS PAGADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ") + ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ") AS DEV_PAG, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ") AS COMPROMETIDO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ") + ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ") AS DISPONIBLE, ");
                        Mi_Sql.Append("'' AS FF, '' AS UR, '' AS PP");
                        Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas);
                        Mi_Sql.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                        Mi_Sql.Append(" ON " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                        Mi_Sql.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                        Mi_Sql.Append(" ON " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                        Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                            Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_Usuario.Trim()))
                        {
                            if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                            else
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        {

                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_DependenciaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_DependenciaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_DependenciaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_DependenciaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                }
                            }

                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Capitulo_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_CapituloF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_Capitulo_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_CapituloF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_CapituloF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_Capitulo_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_CapituloF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_Concepto_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_ConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_Concepto_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_ConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Partida_Generica_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_Partida_GenericaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_Generica_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_GenericaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_Generica_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_Partida_GenericaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_Generica_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_GenericaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_Generica_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_PartidaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_PartidaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_PartidaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_PartidaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                                }
                            }
                        }

                        Mi_Sql.Append(" GROUP BY ");
                        Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + ", ");
                        Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()) || Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                        { Mi_Sql.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);}
                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        {Mi_Sql.Append(", " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);}
                        Mi_Sql.Append(" ORDER BY nombre ASC");

                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error al intentar consultar los registros de los programas. Error: [" + Ex.Message + "]");
                    }
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_UR
                ///DESCRIPCIÓN          : consulta para obtener los datos de las unidades responsables
                ///PARAMETROS           : 
                ///CREO                 : Leslie Gonzalez Vázquez
                ///FECHA_CREO           : 23/Agosto/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Nivel_UR(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_Sql = new StringBuilder();
                    try
                    {
                        Mi_Sql.Append("SELECT " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " +' '+ ");
                        Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS nombre, ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim())){
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " + '_' + ");}
                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim())){
                            Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID + " + '_' + ");}
                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        {Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " + '_' + ");}
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " + '_' + ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " AS id, ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim())){
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " AS FF_ID, ");
                        }else { Mi_Sql.Append(" '' AS FF_ID, "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim())){
                            Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID + " AS AF_ID, ");
                        }else { Mi_Sql.Append(" '' AS AF_ID, "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim())){
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " AS PP_ID, ");
                        }else { Mi_Sql.Append(" '' AS PP_ID, "); }
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " AS UR_ID, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ") AS APROBADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ") AS AMPLIACION, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ") AS REDUCCION, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ") AS MODIFICADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ") AS DEVENGADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ") AS PAGADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ") + ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ") AS DEV_PAG, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ") AS COMPROMETIDO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ") + ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ") AS DISPONIBLE, ");
                        Mi_Sql.Append("'' AS FF, '' AS UR, '' AS PP");
                        Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas);
                        Mi_Sql.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                        Mi_Sql.Append(" ON " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                        Mi_Sql.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                        Mi_Sql.Append(" ON " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                        Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                            Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_Usuario.Trim()))
                        {
                            if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                            else
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        {

                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_DependenciaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_DependenciaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_DependenciaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_DependenciaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                }
                            }

                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Capitulo_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_CapituloF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_Capitulo_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_CapituloF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_CapituloF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_Capitulo_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_CapituloF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_Concepto_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_ConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_Concepto_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_ConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Partida_Generica_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_Partida_GenericaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_Generica_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_GenericaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_Generica_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_Partida_GenericaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_Generica_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_GenericaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_Generica_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_PartidaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_PartidaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_PartidaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_PartidaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                                }
                            }
                        }

                        Mi_Sql.Append(" GROUP BY ");
                        Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + ", ");
                        Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()) || Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                        {Mi_Sql.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);}
                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        {Mi_Sql.Append(", " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);}
                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        { Mi_Sql.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);}

                        Mi_Sql.Append(" ORDER BY nombre ASC");

                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error al intentar consultar los registros de las unidades responsables. Error: [" + Ex.Message + "]");
                    }
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_Capitulos
                ///DESCRIPCIÓN          : consulta para obtener los datos de los capitulos
                ///PARAMETROS           : 
                ///CREO                 : Leslie Gonzalez Vázquez
                ///FECHA_CREO           : 25/Mayo/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Nivel_Capitulos(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_Sql = new StringBuilder();
                    try
                    {
                        Mi_Sql.Append("SELECT " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + " +' '+ ");
                        Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Descripcion + " AS nombre, ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        { Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " + '_' + ");}
                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        {Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID + " + '_' + ");}
                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        { Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " + '_' + "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        {Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " + '_' + ");}
                        Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID + " + '_' + ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " AS id, ");
                        Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID + " AS Capitulo_id, ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        { Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " AS FF_ID, ");}
                        else { Mi_Sql.Append(" '' AS FF_ID, "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        { Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID + " AS AF_ID, ");}
                        else { Mi_Sql.Append(" '' AS AF_ID, "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        {Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " AS PP_ID, ");}
                        else { Mi_Sql.Append(" '' AS PP_ID, "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        { Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " AS UR_ID, "); }
                        else { Mi_Sql.Append(" '' AS UR_ID, "); }
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ") AS APROBADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ") AS AMPLIACION, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ") AS REDUCCION, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ") AS MODIFICADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ") AS DEVENGADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ") AS PAGADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ") + ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ") AS DEV_PAG, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ") AS COMPROMETIDO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ") + ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido+ ") AS DISPONIBLE, ");
                        Mi_Sql.Append("'' AS FF, '' AS UR, '' AS PP");
                        Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas);
                        Mi_Sql.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                        Mi_Sql.Append(" ON " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                        Mi_Sql.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                       
                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                            Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_Usuario.Trim()))
                        {
                            if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                            else
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        {
                           
                                if (!Mi_Sql.ToString().Contains("WHERE"))
                                {
                                    if (!String.IsNullOrEmpty(Negocio.P_DependenciaF_ID.Trim()))
                                    {
                                        Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave);
                                        Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID.Trim()+ "')");
                                        Mi_Sql.Append(" AND (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_DependenciaF_ID.Trim() + "')");
                                    }
                                    else
                                    {
                                        Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                        Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    }
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(Negocio.P_DependenciaF_ID.Trim()))
                                    {
                                        Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave);
                                        Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID.Trim() + "')");
                                        Mi_Sql.Append(" AND (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_DependenciaF_ID.Trim() + "')");
                                    }
                                    else
                                    {
                                        Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                        Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    }
                                }
                            
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id+ " = '" + Negocio.P_ProgramaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Capitulo_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_CapituloF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_Capitulo_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_CapituloF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_CapituloF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_Capitulo_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_CapituloF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_Concepto_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_ConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_Concepto_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_ConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Partida_Generica_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_Partida_GenericaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_Generica_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_GenericaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_Generica_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_Partida_GenericaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_Generica_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_GenericaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_Generica_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_PartidaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_PartidaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_PartidaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_PartidaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                                }
                            }
                        }

                        Mi_Sql.Append(" GROUP BY " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + ", ");
                        Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Descripcion + ", ");
                        Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()) || Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                        { Mi_Sql.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID); }
                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        { Mi_Sql.Append(", " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID); }
                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        { Mi_Sql.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID); }
                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        { Mi_Sql.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID); }
                        Mi_Sql.Append(" ORDER BY nombre ASC");

                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error al intentar consultar los registros de los capitulos. Error: [" + Ex.Message + "]");
                    }
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Conceptos
                ///DESCRIPCIÓN          : consulta para obtener los datos de los conceptos
                ///PARAMETROS           : 
                ///CREO                 : Leslie Gonzalez Vázquez
                ///FECHA_CREO           : 25/Mayo/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Nivel_Conceptos(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_Sql = new StringBuilder();
                    try
                    {
                        Mi_Sql.Append("SELECT " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave + " +' '+ ");
                        Mi_Sql.Append(Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Descripcion + " AS nombre, ");
                        Mi_Sql.Append(Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID + " AS Concepto_id, ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        { Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " + '_' + "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        { Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID + " + '_' + "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        { Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " + '_' + "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        { Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " + '_' + "); }
                        Mi_Sql.Append(Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID + " + '_' + ");
                        Mi_Sql.Append(Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID + " + '_' + ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " AS id, ");
                        Mi_Sql.Append(Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID + " Capitulo_id, ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        { Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " AS FF_ID, "); }
                        else { Mi_Sql.Append(" '' AS FF_ID, "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        { Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID + " AS AF_ID, "); }
                        else { Mi_Sql.Append(" '' AS AF_ID, "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        { Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " AS PP_ID, "); }
                        else { Mi_Sql.Append(" '' AS PP_ID, "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        { Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " AS UR_ID, "); }
                        else { Mi_Sql.Append(" '' AS UR_ID, "); }
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ") AS APROBADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ") AS AMPLIACION, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ") AS REDUCCION, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ") AS MODIFICADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ") AS DEVENGADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ") AS PAGADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ") + ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ") AS DEV_PAG, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ") AS COMPROMETIDO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ") + ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ") AS DISPONIBLE, ");
                        Mi_Sql.Append("'' AS FF, '' AS UR, '' AS PP");
                        Mi_Sql.Append(" FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas);
                        Mi_Sql.Append(" ON " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                        Mi_Sql.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                        Mi_Sql.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                            Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_Usuario.Trim()))
                        {
                            if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                            else
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        {

                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_DependenciaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_DependenciaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_DependenciaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_DependenciaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                }
                            }

                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Capitulo_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_CapituloF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_Capitulo_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_CapituloF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_CapituloF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_Capitulo_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_CapituloF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_Concepto_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_ConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_Concepto_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_ConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Partida_Generica_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_Partida_GenericaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_Generica_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_GenericaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_Generica_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_Partida_GenericaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_Generica_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_GenericaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_Generica_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_PartidaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_PartidaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_PartidaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_PartidaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                                }
                            }
                        }

                        Mi_Sql.Append(" GROUP BY " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave + ", ");
                        Mi_Sql.Append(Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Descripcion + ", ");
                        Mi_Sql.Append(Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID + ", ");
                        Mi_Sql.Append(Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()) || Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                        { Mi_Sql.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID); }
                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        { Mi_Sql.Append(", " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID); }
                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        { Mi_Sql.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID); }
                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        { Mi_Sql.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID); }
                        Mi_Sql.Append(" ORDER BY " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID + " ASC");

                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error al intentar consultar los registros de los conceptos. Error: [" + Ex.Message + "]");
                    }
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Partida_Generica
                ///DESCRIPCIÓN          : consulta para obtener los datos de las partidas genericas
                ///PARAMETROS           : 
                ///CREO                 : Leslie Gonzalez Vázquez
                ///FECHA_CREO           : 25/Mayo/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Nivel_Partida_Gen(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_Sql = new StringBuilder();
                    try
                    {
                        Mi_Sql.Append("SELECT " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Clave + " +' '+ ");
                        Mi_Sql.Append(Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Descripcion + " AS nombre, ");
                        Mi_Sql.Append(Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " AS Partida_Generica_id, ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        { Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " + '_' + "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        { Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID + " + '_' + "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        { Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " + '_' + "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        { Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " + '_' + "); }
                        Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID + " + '_' + ");
                        Mi_Sql.Append(Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + " + '_' + ");
                        Mi_Sql.Append(Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " + '_' + ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " AS id, ");
                        Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID + " AS Capitulo_id, ");
                        Mi_Sql.Append(Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + " AS Concepto_id, ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        { Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " AS FF_ID, "); }
                        else { Mi_Sql.Append(" '' AS FF_ID, "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        { Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID + " AS AF_ID, "); }
                        else { Mi_Sql.Append(" '' AS AF_ID, "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        { Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " AS PP_ID, "); }
                        else { Mi_Sql.Append(" '' AS PP_ID, "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        { Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " AS UR_ID, "); }
                        else { Mi_Sql.Append(" '' AS UR_ID, "); }
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ") AS APROBADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ") AS AMPLIACION, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ") AS REDUCCION, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ") AS MODIFICADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ") AS DEVENGADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ") AS PAGADO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ") + ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ") AS DEV_PAG, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ") AS COMPROMETIDO, ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ") + ");
                        Mi_Sql.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ") AS DISPONIBLE, ");
                        Mi_Sql.Append("'' AS FF, '' AS UR, '' AS PP");
                        Mi_Sql.Append(" FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                        Mi_Sql.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                        Mi_Sql.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID);
                        Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                        Mi_Sql.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);


                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                            Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_Usuario.Trim()))
                        {
                            if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                            else
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        {

                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_DependenciaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_DependenciaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_DependenciaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_DependenciaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                }
                            }

                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Capitulo_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_CapituloF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_Capitulo_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_CapituloF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_CapituloF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_Capitulo_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_CapituloF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_Concepto_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_ConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_Concepto_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_ConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Partida_Generica_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_Partida_GenericaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_Generica_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_GenericaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_Generica_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_Partida_GenericaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_Generica_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_GenericaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_Generica_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_PartidaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_PartidaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_PartidaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_PartidaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                                }
                            }
                        }

                        Mi_Sql.Append("GROUP BY " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Clave + ", ");
                        Mi_Sql.Append(Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Descripcion + ", ");
                        Mi_Sql.Append(Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + ", ");
                        Mi_Sql.Append(Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + ", ");
                        Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()) || Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                        { Mi_Sql.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID); }
                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        { Mi_Sql.Append(", " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID); }
                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        { Mi_Sql.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID); }
                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        { Mi_Sql.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID); }
                        Mi_Sql.Append(" ORDER BY nombre ASC");

                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error al intentar consultar los registros de las partidas genericas. Error: [" + Ex.Message + "]");
                    }
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Partida
                ///DESCRIPCIÓN          : consulta para obtener los datos de las partidas especificas
                ///PARAMETROS           : 
                ///CREO                 : Leslie Gonzalez Vázquez
                ///FECHA_CREO           : 25/Mayo/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Nivel_Partida(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_Sql = new StringBuilder();
                    try
                    {
                        Mi_Sql.Append("SELECT " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " +' '+ ");
                        Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS nombre, ");
                        Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + " AS Capitulo_id, ");
                        Mi_Sql.Append(Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID + " AS Concepto_id, ");
                        Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " AS Partida_Generica_id, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " + '_' + ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID + " + '_' + "); 
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " + '_' + "); 
                        Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID + " + '_' + ");
                        Mi_Sql.Append(Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID + " + '_' + ");
                        Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " + '_' + ");
                        Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " + '_' + ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " AS id, ");
                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        { Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " AS FF_ID, "); }
                        else { Mi_Sql.Append(" '' AS FF_ID, "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        { Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID + " AS AF_ID, "); }
                        else { Mi_Sql.Append(" '' AS AF_ID, "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        { Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " AS PP_ID, "); }
                        else { Mi_Sql.Append(" '' AS PP_ID, "); }
                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        { Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " AS UR_ID, "); }
                        else { Mi_Sql.Append(" '' AS UR_ID, "); }
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + " + ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + " AS DEV_PAG, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + " + ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " AS DISPONIBLE, ");
                        Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                        Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS FF, ");
                        Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " + ' ' + ");
                        Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS UR, ");
                        Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                        Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS PP ");
                        Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado );
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas);
                        Mi_Sql.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                        Mi_Sql.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                        Mi_Sql.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);


                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                            Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_Usuario.Trim()))
                        {
                            if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                            else
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                        }


                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        {

                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_DependenciaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_DependenciaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_DependenciaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_DependenciaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                }
                            }

                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Capitulo_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_CapituloF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_Capitulo_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_CapituloF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_CapituloF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_Capitulo_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_CapituloF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_Concepto_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_ConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_Concepto_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_ConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Partida_Generica_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_Partida_GenericaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_Generica_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_GenericaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_Generica_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_Partida_GenericaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_Generica_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_GenericaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_Generica_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_PartidaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_PartidaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_PartidaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_PartidaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                                }
                            }
                        }

                        Mi_Sql.Append(" ORDER BY " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " ASC");

                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error al intentar consultar los registros de las partidas especificas. Error: [" + Ex.Message + "]");
                    }
                }
            #endregion

            #region (DETALLES ING)
                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Det_Modificacion_Ing
                ///DESCRIPCIÓN          : Obtiene datos de las solicitudes de movimientos del presupuesto 
                ///                       de ingresos (ampliacion, reduccion, modificado)
                ///PARAMETROS           : 
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 24/Agosto/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Det_Modificacion_Ing(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                    DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                    DataTable Dt = new DataTable();
                    try
                    {
                        Mi_SQL.Append("SELECT ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing + " AS NO_MODIFICACION, ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Movimiento_Ing_ID + " AS NO_MOVIMIENTO, ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Anio + ", ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Total + ", ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Movimiento + ", ");
                        Mi_SQL.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Justificacion + ", ' ') AS JUSTIFICACION, ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Fecha_Creo+ ", ");
                        Mi_SQL.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Usuario_Creo + ", ' ') AS USUARIO_CREO, ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Fecha_Modifico + ", ");
                        Mi_SQL.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Usuario_Modifico + ", ' ') AS USUARIO_MODIFICO, ");
                        Mi_SQL.Append("(CASE WHEN " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det+ "." + Ope_Psp_Movimiento_Ing_Det.Campo_SubConcepto_Ing_ID + " IS NULL THEN ");
                        Mi_SQL.Append(" REPLACE(" + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                        Mi_SQL.Append(" ELSE ");
                        Mi_SQL.Append(" REPLACE(" + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                        Mi_SQL.Append(" END) AS CONCEPTO ");
                        Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Estatus);
                        Mi_SQL.Append(" = 'AUTORIZADO'");

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Anio);
                            Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_Detalle.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Movimiento);
                            Mi_SQL.Append(" = '" + Negocio.P_Tipo_Detalle.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND  " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Proyecto_Programa_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Rubro_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Clase_Ing_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Clase_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_Ing_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Concepto_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_SubConcepto_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_SubConcepto_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fecha_Inicial.Trim()))
                        {
                            if (!String.IsNullOrEmpty(Negocio.P_Fecha_Final.Trim()))
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Fecha_Creo);
                                Mi_SQL.Append(" BETWEEN TO_DATE ('" + Negocio.P_Fecha_Inicial.Trim() + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS')");
                                Mi_SQL.Append(" AND TO_DATE('" + Negocio.P_Fecha_Final.Trim() + " 23:59:00', 'DD/MM/YYYY HH24:MI:SS')");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Fecha_Creo);
                                Mi_SQL.Append(" BETWEEN TO_DATE ('" + Negocio.P_Fecha_Inicial.Trim() + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS')");
                                Mi_SQL.Append(" AND TO_DATE('" + Negocio.P_Fecha_Inicial.Trim() + " 23:59:00', 'DD/MM/YYYY HH24:MI:SS')");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Periodo_Inicial.Trim()) && !String.IsNullOrEmpty(Negocio.P_Periodo_Final.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Fecha_Creo);
                            Mi_SQL.Append(" BETWEEN TO_DATE ('" + Negocio.P_Periodo_Inicial.Trim() + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS')");
                            Mi_SQL.Append(" AND TO_DATE('" + Negocio.P_Periodo_Final.Trim() + " 23:59:00', 'DD/MM/YYYY HH24:MI:SS')");
                        }

                        Mi_SQL.Append(" ORDER BY NO_MODIFICACION, NO_MOVIMIENTO  ASC");

                        Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        if (Ds != null)
                        {
                            Dt = Ds.Tables[0];
                        }
                    }
                    catch (Exception Ex)
                    {
                        String Mensaje = "Error al intentar consultar los registros de los detalles del presupuesto Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                        throw new Exception(Mensaje);
                    }
                    return Dt;
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Det_Recaudado_Ing
                ///DESCRIPCIÓN          : Obtiene datos de lo recaudado del presupuesto de ingresos
                ///PARAMETROS           : 
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 29/Agosto/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Det_Recaudado_Ing(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                    DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                    DataTable Dt = new DataTable();
                    try
                    {
                        Mi_SQL.Append("SELECT ");
                        Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_No_Movimiento + ", ");
                        Mi_SQL.Append("ISNULL(" + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_No_Poliza + ", ' ') AS NO_POLIZA, ");
                        Mi_SQL.Append("ISNULL(" + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_Mes_Ano + ", ' ') AS MES_ANO, ");
                        Mi_SQL.Append("ISNULL(" + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_Tipo_Poliza_ID + ", ' ') AS TIPO_POLIZA_ID, ");
                        Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_Fecha + ", ");
                        Mi_SQL.Append("ISNULL(" + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_Descripcion + ", ' ') AS DESCRIPCION, ");
                        Mi_SQL.Append("ISNULL(" + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_Referencia + ", ' ') AS REFERENCIA, ");
                        Mi_SQL.Append("ISNULL(" + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_Usuario_Creo + ", ' ') AS USUARIO_CREO, ");
                        Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_Importe + ", ");
                        Mi_SQL.Append("(CASE WHEN " + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_SubConcepto_Ing_ID + " IS NULL THEN ");
                        Mi_SQL.Append(" REPLACE(" + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                        Mi_SQL.Append(" ELSE ");
                        Mi_SQL.Append(" REPLACE(" + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                        Mi_SQL.Append(" END) AS CONCEPTO ");
                        Mi_SQL.Append(" FROM " + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_Cargo);
                        Mi_SQL.Append(" = '" + Negocio.P_Tipo_Detalle.Trim() + "'");

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_SQL.Append(" AND EXTRACT(YEAR FROM " + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_Fecha + ")");
                            Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_Fte_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND  " + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_Proyecto_Programa_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_Rubro_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_Tipo_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Clase_Ing_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_Clase_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_Ing_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_Concepto_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_SubConcepto_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_SubConcepto_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fecha_Inicial.Trim()))
                        {
                            if (!String.IsNullOrEmpty(Negocio.P_Fecha_Final.Trim()))
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_Fecha_Creo);
                                Mi_SQL.Append(" BETWEEN TO_DATE ('" + Negocio.P_Fecha_Inicial.Trim() + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS')");
                                Mi_SQL.Append(" AND TO_DATE('" + Negocio.P_Fecha_Final.Trim() + " 23:59:00', 'DD/MM/YYYY HH24:MI:SS')");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_Fecha_Creo);
                                Mi_SQL.Append(" BETWEEN TO_DATE ('" + Negocio.P_Fecha_Inicial.Trim() + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS')");
                                Mi_SQL.Append(" AND TO_DATE('" + Negocio.P_Fecha_Inicial.Trim() + " 23:59:00', 'DD/MM/YYYY HH24:MI:SS')");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Periodo_Inicial.Trim()) && !String.IsNullOrEmpty(Negocio.P_Periodo_Final.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_Fecha_Creo);
                            Mi_SQL.Append(" BETWEEN TO_DATE ('" + Negocio.P_Periodo_Inicial.Trim() + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS')");
                            Mi_SQL.Append(" AND TO_DATE('" + Negocio.P_Periodo_Final.Trim() + " 23:59:00', 'DD/MM/YYYY HH24:MI:SS')");
                        }

                        Mi_SQL.Append(" ORDER BY " + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "." + Ope_Psp_Registro_Mov_Ingreso.Campo_No_Movimiento + " ASC");

                        Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        if (Ds != null)
                        {
                            Dt = Ds.Tables[0];
                        }
                    }
                    catch (Exception Ex)
                    {
                        String Mensaje = "Error al intentar consultar los registros de los detalles del presupuesto Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                        throw new Exception(Mensaje);
                    }
                    return Dt;
                }
            #endregion

            #region (DETALLES EGR)
                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Det_Modificacion_Egr
                ///DESCRIPCIÓN          : Obtiene datos de las solicitudes de movimientos del presupuesto 
                ///                       de egresos (ampliacion, reduccion, modificado)
                ///PARAMETROS           : 
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 27/Agosto/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Det_Modificacion_Egr(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                    DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                    DataTable Dt = new DataTable();
                    try
                    {
                        Mi_SQL.Append("SELECT ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + " AS NO_MODIFICACION, ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " AS NO_MOVIMIENTO, ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Anio + ", ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Total + ", ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion + ", ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + ", ");
                        Mi_SQL.Append("ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Justificacion + ", ' ') AS JUSTIFICACION, ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Creo + ", ");
                        Mi_SQL.Append("ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Creo + ", ' ') AS USUARIO_CREO, ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Modifico + ", ");
                        Mi_SQL.Append("ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Modifico + ", ' ') AS USUARIO_MODIFICO, ");
                        Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " + ' ' + ");
                        Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS PARTIDA ");
                        Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                        Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Partida_ID);
                        Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas);
                        Mi_SQL.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                        Mi_SQL.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                        Mi_SQL.Append(" ON " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID);
                        Mi_SQL.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                        Mi_SQL.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                        Mi_SQL.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Estatus);
                        Mi_SQL.Append(" = 'AUTORIZADO'");

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Anio);
                            Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_Detalle.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion);
                            Mi_SQL.Append(" IN ('" + Negocio.P_Tipo_Detalle.Trim() + "', 'TRASPASO')");

                             if (Negocio.P_Tipo_Detalle.Trim().Equals("REDUCCION"))
                             {
                                 Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida);
                                 Mi_SQL.Append(" = 'Origen'");
                             }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND  " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Proyecto_Programa_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Area_Funcional_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Dependencia_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Capitulo_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Partida_Generica_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Partida_Generica_ID.Trim() + "'");
                        }
                        
                        if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Partida_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fecha_Inicial.Trim()))
                        {
                            if (!String.IsNullOrEmpty(Negocio.P_Fecha_Final.Trim()))
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Creo);
                                Mi_SQL.Append(" BETWEEN TO_DATE ('" + Negocio.P_Fecha_Inicial.Trim() + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS')");
                                Mi_SQL.Append(" AND TO_DATE('" + Negocio.P_Fecha_Final.Trim() + " 23:59:00', 'DD/MM/YYYY HH24:MI:SS')");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Creo);
                                Mi_SQL.Append(" BETWEEN TO_DATE ('" + Negocio.P_Fecha_Inicial.Trim() + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS')");
                                Mi_SQL.Append(" AND TO_DATE('" + Negocio.P_Fecha_Inicial.Trim() + " 23:59:00', 'DD/MM/YYYY HH24:MI:SS')");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Periodo_Inicial.Trim()) && !String.IsNullOrEmpty(Negocio.P_Periodo_Final.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Creo);
                            Mi_SQL.Append(" BETWEEN TO_DATE ('" + Negocio.P_Periodo_Inicial.Trim() + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS')");
                            Mi_SQL.Append(" AND TO_DATE('" + Negocio.P_Periodo_Final.Trim() + " 23:59:00', 'DD/MM/YYYY HH24:MI:SS')");
                        }

                        Mi_SQL.Append(" ORDER BY NO_MODIFICACION, NO_MOVIMIENTO  ASC");

                        Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        if (Ds != null)
                        {
                            Dt = Ds.Tables[0];
                        }
                    }
                    catch (Exception Ex)
                    {
                        String Mensaje = "Error al intentar consultar los registros de los detalles del presupuesto Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                        throw new Exception(Mensaje);
                    }
                    return Dt;
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Det_Movimiento_Contable_Egr
                ///DESCRIPCIÓN          : Obtiene datos de las solicitudes de movimientos contables del presupuesto 
                ///                       de egresos (devengado, pagado, comprometido)
                ///PARAMETROS           : 
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 29/Agosto/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Det_Movimiento_Contable_Egr(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                    DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                    DataTable Dt = new DataTable();
                    try
                    {
                        Mi_SQL.Append("SELECT ");
                        Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_No_Reserva + ", ");
                        Mi_SQL.Append("ISNULL(" + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_No_Poliza + ", ' ') AS NO_POLIZA, ");
                        Mi_SQL.Append("ISNULL(" + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Tipo_Poliza_ID + ", ' ') AS TIPO_POLIZA_ID, ");
                        Mi_SQL.Append("ISNULL(" + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Mes_Ano + ", ' ') AS MES_ANO, ");
                        Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Importe + " AS IMPORTE_MOV, ");
                        Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Fecha + " AS FECHA_MOV, ");
                        Mi_SQL.Append("ISNULL(" + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Usuario + ", ' ') AS USUARIO_MOV, ");
                        Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio + ", ");
                        Mi_SQL.Append("ISNULL(" + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Concepto + ", ' ') AS CONCEPTO, ");
                        Mi_SQL.Append("ISNULL(" + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Beneficiario + ", ' ') AS BENEFICIARIO, ");
                        Mi_SQL.Append(Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago + "." + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion + " AS TIPO_SOLICITUD, ");
                        Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " + ' ' + ");
                        Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA, ");
                        Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                        Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS PROGRAMA, ");
                        Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                        Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS FUENTE, ");
                        Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " + ' ' + ");
                        Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS PARTIDA ");
                        Mi_SQL.Append(" FROM " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos);
                        Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                        Mi_SQL.Append(" ON " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_No_Reserva);
                        Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                        Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                        Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                        Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago);
                        Mi_SQL.Append(" ON " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Tipo_Solicitud_Pago_ID);
                        Mi_SQL.Append(" = " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago + "." + Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                        Mi_SQL.Append(" ON " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID);
                        Mi_SQL.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID);
                        Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                        Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID);
                        Mi_SQL.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                        Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID);
                        Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas);
                        Mi_SQL.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                        Mi_SQL.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                        Mi_SQL.Append(" ON " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID);
                        Mi_SQL.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                        Mi_SQL.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                        Mi_SQL.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Estatus + " = 'GENERADA'");

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_SQL.Append(" AND EXTRACT(YEAR FROM " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Fecha + ")");
                            Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_Detalle.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Cargo);
                            Mi_SQL.Append(" = '" + Negocio.P_Tipo_Detalle.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND  " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Capitulo_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Partida_Generica_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Partida_Generica_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fecha_Inicial.Trim()))
                        {
                            if (!String.IsNullOrEmpty(Negocio.P_Fecha_Final.Trim()))
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Fecha);
                                Mi_SQL.Append(" BETWEEN TO_DATE ('" + Negocio.P_Fecha_Inicial.Trim() + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS')");
                                Mi_SQL.Append(" AND TO_DATE('" + Negocio.P_Fecha_Final.Trim() + " 23:59:00', 'DD/MM/YYYY HH24:MI:SS')");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Fecha);
                                Mi_SQL.Append(" BETWEEN TO_DATE ('" + Negocio.P_Fecha_Inicial.Trim() + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS')");
                                Mi_SQL.Append(" AND TO_DATE('" + Negocio.P_Fecha_Inicial.Trim() + " 23:59:00', 'DD/MM/YYYY HH24:MI:SS')");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Periodo_Inicial.Trim()) && !String.IsNullOrEmpty(Negocio.P_Periodo_Final.Trim()))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Fecha);
                            Mi_SQL.Append(" BETWEEN TO_DATE ('" + Negocio.P_Periodo_Inicial.Trim() + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS')");
                            Mi_SQL.Append(" AND TO_DATE('" + Negocio.P_Periodo_Final.Trim() + " 23:59:00', 'DD/MM/YYYY HH24:MI:SS')");
                        }

                        Mi_SQL.Append(" ORDER BY " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_No_Reserva  + " ASC");

                        Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        if (Ds != null)
                        {
                            Dt = Ds.Tables[0];
                        }
                    }
                    catch (Exception Ex)
                    {
                        String Mensaje = "Error al intentar consultar los registros de los detalles del presupuesto Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                        throw new Exception(Mensaje);
                    }
                    return Dt;
                }
            #endregion

            #region(Reporte)
                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Psp_Egr
                ///DESCRIPCIÓN          : consulta para obtener los datos del presupuesto de egresos
                ///PARAMETROS           : 
                ///CREO                 : Leslie Gonzalez Vázquez
                ///FECHA_CREO           : 30/Agosto/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Psp_Egr(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_Sql = new StringBuilder();
                    try
                    {
                        Mi_Sql.Append("SELECT " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                        Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS CLAVE_NOM_FF, ");
                        Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                        Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS CLAVE_NOM_PP, ");
                        Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " + ' ' + ");
                        Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Descripcion + " AS CLAVE_NOM_AF, ");
                        Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Clave + " + ' ' + ");
                        Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Nombre + " AS CLAVE_NOM_GPO_UR, ");
                        Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " + ' ' + ");
                        Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS CLAVE_NOM_UR, ");
                        Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + " +' '+ ");
                        Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Descripcion + " AS CLAVE_NOM_CAPITULO, ");
                        Mi_Sql.Append(Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave + " +' '+ ");
                        Mi_Sql.Append(Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Descripcion + " AS CLAVE_NOM_CONCEPTO, ");
                        Mi_Sql.Append(Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Clave + " +' '+ ");
                        Mi_Sql.Append(Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Descripcion + " AS CLAVE_NOM_PARTIDA_GEN, ");
                        Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " +' '+ ");
                        Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS CLAVE_NOM_PARTIDA, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " AS FF_ID, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " AS PP_ID, ");
                        Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID + " AS AF_ID, ");
                        Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID + "  AS GPO_UR_ID, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + "  AS UR_ID, ");
                        Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID + "  AS CA_ID, ");
                        Mi_Sql.Append(Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID + " AS CO_ID, ");
                        Mi_Sql.Append(Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID + " AS PG_ID, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " AS P_ID, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + " + ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + " AS DEV_PAG, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + " + ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " AS DISPONIBLE ");
                        Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas);
                        Mi_Sql.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                        Mi_Sql.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                        Mi_Sql.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                        Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                        Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias);
                        Mi_Sql.Append(" ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                        Mi_Sql.Append(" = " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                        Mi_Sql.Append(" ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                        Mi_Sql.Append(" = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);


                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                            Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_Usuario.Trim()))
                        {
                            if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                            else
                            {
                                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                    Negocio.P_Dependencia_ID = String.Empty;
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Coordinador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Gpo_Dependencia_ID.Trim() + "'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " IN ('SI', 'NO')");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Inversiones"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                                }
                                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                                    Mi_Sql.Append(" IN(" + Negocio.P_Fte_Financiamiento.Trim() + ")");
                                    Negocio.P_Fte_Financiamiento = String.Empty;
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                            }
                        }


                        if (!String.IsNullOrEmpty(Negocio.P_Area_Funcional_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        {

                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_DependenciaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_DependenciaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_DependenciaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_DependenciaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                                }
                            }

                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Capitulo_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_CapituloF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_Capitulo_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_CapituloF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_CapituloF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_Capitulo_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_SAP_Capitulos.Campo_Clave + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_CapituloF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Capitulo_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_Concepto_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_ConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_Concepto_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Concepto.Campo_Clave + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = '" + Negocio.P_ConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Partida_Generica_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_Partida_GenericaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_Generica_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_GenericaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_Generica_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_Partida_GenericaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_Generica_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Genericas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_GenericaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_Generica_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                        {
                            if (!Mi_Sql.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_PartidaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_PartidaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_PartidaF_ID.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave);
                                    Mi_Sql.Append(" BETWEEN (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_Partida_ID.Trim() + "')");
                                    Mi_Sql.Append(" AND (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Negocio.P_PartidaF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_Sql.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_Sql.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                                }
                            }
                        }

                        Mi_Sql.Append(" ORDER BY  CLAVE_NOM_FF, CLAVE_NOM_AF, CLAVE_NOM_PP, CLAVE_NOM_GPO_UR, CLAVE_NOM_UR,  ");
                        Mi_Sql.Append(" CLAVE_NOM_CAPITULO, CLAVE_NOM_CONCEPTO, CLAVE_NOM_PARTIDA_GEN, CLAVE_NOM_PARTIDA ASC");

                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error al intentar consultar los registros del presupuesto de egresos. Error: [" + Ex.Message + "]");
                    }
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Psp_Ing
                ///DESCRIPCIÓN          : consulta para obtener los datos del presupuesto de ingresos
                ///PARAMETROS           : 
                ///CREO                 : Leslie Gonzalez Vázquez
                ///FECHA_CREO           : 30/Agosto/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                internal static DataTable Consultar_Psp_Ing(Cls_Ope_Psp_Presupuesto_Negocio Negocio)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                    DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                    DataTable Dt = new DataTable();
                    try
                    {
                        Mi_SQL.Append("SELECT " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                        Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS CLAVE_NOM_FF, ");
                        Mi_SQL.Append("ISNULL(" + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                        Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + ", '') AS CLAVE_NOM_PP, ");
                        Mi_SQL.Append("REPLACE(" + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + " AS CLAVE_NOM_RUBRO, ");
                        Mi_SQL.Append("REPLACE(" + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + " AS CLAVE_NOM_TIPO, ");
                        Mi_SQL.Append("REPLACE(" + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + " AS CLAVE_NOM_CLASE, ");
                        Mi_SQL.Append("REPLACE(" + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + " AS CLAVE_NOM_CON, ");
                        Mi_SQL.Append("ISNULL(REPLACE(" + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion + ",'') AS CLAVE_NOM_SUBCON, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " AS FF_ID, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " AS PP_ID, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " AS R_ID, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " AS T_ID, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " AS CL_ID, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + " AS C_ID, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " AS SC_ID, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + " AS ESTIMADO, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + " AS AMPLIACION, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + " AS REDUCCION, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + " AS MODIFICADO, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + " AS DEVENGADO, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + " AS RECAUDADO, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + " + ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + " AS DEV_REC, ");
                        Mi_SQL.Append(" '' AS COMPROMETIDO, ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + " AS POR_RECAUDAR ");
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                        Mi_SQL.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                            Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_Ing.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_Ing.Trim() + "'");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_Ing.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE  " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ProgramaF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_ProgramaF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND  " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Programa_Ing_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_RubroF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_Rubro_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_RubroF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_RubroF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_Rubro_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Rubro.Campo_Clave + " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Negocio.P_RubroF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_TipoF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_TipoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_TipoF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Tipo.Campo_Clave + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_TipoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Clase_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ClaseF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_Clase_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_ClaseF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ClaseF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_Clase_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_ClaseF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_Ing_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_Concepto_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_ConceptoF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_ConceptoF_Ing_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_Concepto_Ing_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_Concepto_Ing.Campo_Clave + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Negocio.P_ConceptoF_Ing_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_SubConcepto_ID.Trim()))
                        {
                            if (!Mi_SQL.ToString().Contains("WHERE"))
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_SubConceptoF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConcepto_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Negocio.P_SubConceptoF_ID.Trim()))
                                {
                                    Mi_SQL.Append(" AND" + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave);
                                    Mi_SQL.Append(" BETWEEN (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConcepto_ID.Trim() + "')");
                                    Mi_SQL.Append(" AND (SELECT " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + Negocio.P_SubConceptoF_ID.Trim() + "')");
                                }
                                else
                                {
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                                    Mi_SQL.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                                }
                            }
                        }

                        Mi_SQL.Append(" ORDER BY CLAVE_NOM_FF, CLAVE_NOM_PP, CLAVE_NOM_RUBRO, CLAVE_NOM_TIPO, CLAVE_NOM_CLASE, CLAVE_NOM_CON, CLAVE_NOM_SUBCON ASC");

                        Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        if (Ds != null)
                        {
                            Dt = Ds.Tables[0];
                        }
                    }
                    catch (Exception Ex)
                    {
                        String Mensaje = "Error al intentar consultar los registros del presupuesto de ingresos Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                        throw new Exception(Mensaje);
                    }
                    return Dt;
                }
            #endregion
        #endregion
    }
}
