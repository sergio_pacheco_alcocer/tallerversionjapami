﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using System.Text;
using JAPAMI.Cat_Psp_Rubros.Negocio;

namespace JAPAMI.Cat_Psp_Rubros.Datos
{
    public class Cls_Cat_Psp_Rubros_Datos
    {
        #region Metodos
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Rubros
        ///DESCRIPCIÓN          : consulta para obtener los datos de los rubros
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 15/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Rubros(Cls_Cat_Psp_Rubros_Negocio Rubros_Negocios)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                Mi_Sql.Append("SELECT " + Cat_Psp_Rubro.Campo_Clave + " + ' ' + ");
                Mi_Sql.Append(Cat_Psp_Rubro.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                Mi_Sql.Append(Cat_Psp_Rubro.Campo_Clave + ", ");
                Mi_Sql.Append(Cat_Psp_Rubro.Campo_Descripcion + ", ");
                Mi_Sql.Append(Cat_Psp_Rubro.Campo_Estatus + ", ");
                Mi_Sql.Append(Cat_Psp_Rubro.Campo_Anio + ", ");
                Mi_Sql.Append(Cat_Psp_Rubro.Campo_Rubro_ID);
                Mi_Sql.Append(" FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);

                if (!String.IsNullOrEmpty(Rubros_Negocios.P_Clave))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Rubro.Campo_Clave + " = '" + Rubros_Negocios.P_Clave.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Rubro.Campo_Clave + " = '" + Rubros_Negocios.P_Clave.Trim() + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Rubros_Negocios.P_Descripcion))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Rubro.Campo_Descripcion + " = '" + Rubros_Negocios.P_Descripcion.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Rubro.Campo_Descripcion + " = '" + Rubros_Negocios.P_Descripcion.Trim() + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Rubros_Negocios.P_Estatus))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Rubro.Campo_Estatus + " = '" + Rubros_Negocios.P_Estatus.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Rubro.Campo_Estatus + " = '" + Rubros_Negocios.P_Estatus.Trim() + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Rubros_Negocios.P_Rubro_ID))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Rubros_Negocios.P_Rubro_ID.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Rubros_Negocios.P_Rubro_ID.Trim() + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Rubros_Negocios.P_Anio))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Rubro.Campo_Anio + " = '" + Rubros_Negocios.P_Anio.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Rubro.Campo_Anio + " = '" + Rubros_Negocios.P_Anio.Trim() + "'");
                    }
                }

                Mi_Sql.Append(" ORDER BY " + Cat_Psp_Rubro.Campo_Clave + " ASC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de los rubros. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Rubros2
        ///DESCRIPCIÓN          : Obtiene los Tipos de acuerdo a los filtros establecidos en la interfaz
        ///PARAMETROS           : Rubros, instancia de Cls_Cat_Psp_Rubros_Negocio 
        ///CREO                 : Antonio Salvador Benavides Guardado
        ///FECHA_CREO           : 21/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Rubros2(Cls_Cat_Psp_Rubros_Negocio Rubros)
        {
            DataTable Tabla = new DataTable();
            String Mi_SQL;
            String Mi_SQL_Campos_Foraneos = "";
            try
            {
                if (Rubros.P_Campos_Dinamicos != null && Rubros.P_Campos_Dinamicos != "")
                {
                    Mi_SQL = "SELECT " + Mi_SQL_Campos_Foraneos + Rubros.P_Campos_Dinamicos;
                }
                else
                {
                    Mi_SQL = "SELECT ";
                    Mi_SQL += Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID + ", ";
                    Mi_SQL += Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + ", ";
                    Mi_SQL += Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Anio + ", ";
                    Mi_SQL += Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + ", ";
                    Mi_SQL += Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Estatus + ", ";
                    if (Mi_SQL.EndsWith(", "))
                    {
                        Mi_SQL = Mi_SQL.Substring(0, Mi_SQL.Length - 2);
                    }
                }
                Mi_SQL += " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro;
                if (Rubros.P_Unir_Tablas != null && Rubros.P_Unir_Tablas != "")
                {
                    Mi_SQL += ", " + Rubros.P_Unir_Tablas;
                }
                else
                {
                    if (Rubros.P_Join != null && Rubros.P_Join != "")
                    {
                        Mi_SQL += " " + Rubros.P_Join;
                    }
                }
                if (Rubros.P_Filtros_Dinamicos != null && Rubros.P_Filtros_Dinamicos != "")
                {
                    Mi_SQL += " WHERE " + Rubros.P_Filtros_Dinamicos;
                }
                else
                {
                    Mi_SQL += " WHERE ";
                    if (Rubros.P_Rubro_ID != null && Rubros.P_Rubro_ID != "")
                    {
                        Mi_SQL += Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Rubros.P_Rubro_ID + "' AND ";
                    }
                    if (Rubros.P_Anio != null && Rubros.P_Anio != "")
                    {
                        Mi_SQL += Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Anio + " = " + Rubros.P_Anio + " AND ";
                    }
                    if (Rubros.P_Clave != null && Rubros.P_Clave != "")
                    {
                        Mi_SQL += Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + Validar_Operador_Comparacion(Rubros.P_Clave) + " AND ";
                    }
                    if (Rubros.P_Estatus != null && Rubros.P_Estatus != "")
                    {
                        Mi_SQL += Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Estatus + Validar_Operador_Comparacion(Rubros.P_Estatus) + " AND ";
                    }
                    if (Mi_SQL.EndsWith(" AND "))
                    {
                        Mi_SQL = Mi_SQL.Substring(0, Mi_SQL.Length - 5);
                    }
                    if (Mi_SQL.EndsWith(" WHERE "))
                    {
                        Mi_SQL = Mi_SQL.Substring(0, Mi_SQL.Length - 7);
                    }
                }
                if (Rubros.P_Agrupar_Dinamico != null && Rubros.P_Agrupar_Dinamico != "")
                {
                    Mi_SQL += " GROUP BY " + Rubros.P_Agrupar_Dinamico;
                }
                if (Rubros.P_Ordenar_Dinamico != null && Rubros.P_Ordenar_Dinamico != "")
                {
                    Mi_SQL += " ORDER BY " + Rubros.P_Ordenar_Dinamico;
                }
                DataSet dataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (dataSet != null)
                {
                    Tabla = dataSet.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros de la Cuentas. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Tabla;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Eliminar_Rubros
        ///DESCRIPCIÓN          : consulta para eliminar los datos de los rubros
        ///PARAMETROS           1 Rubros_Negocios: conexion con la capa de negocios
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 15/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static Boolean Eliminar_Rubros(Cls_Cat_Psp_Rubros_Negocio Rubros_Negocios)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenará la consulta.
            Boolean Operacion_Completa = false;//Estado de la operacion.

            try
            {
                Mi_SQL.Append("UPDATE " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                Mi_SQL.Append(" SET " + Cat_Psp_Rubro.Campo_Estatus + " = 'INACTIVO'");
                Mi_SQL.Append(" WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Rubros_Negocios.P_Rubro_ID + "'");

                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                Operacion_Completa = true;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al ejecutar Eliminar los rubros. Error: [" + Ex.Message + "]");
            }
            return Operacion_Completa;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Rubros
        ///DESCRIPCIÓN          : consulta para modificar los datos de los rubros
        ///PARAMETROS           1 Rubros_Negocios: conexion con la capa de negocios
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 15/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static Boolean Modificar_Rubros(Cls_Cat_Psp_Rubros_Negocio Rubros_Negocios)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenará la consulta.
            Boolean Operacion_Completa = false;//Estado de la operacion.

            try
            {
                Mi_SQL.Append("UPDATE " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                Mi_SQL.Append(" SET " + Cat_Psp_Rubro.Campo_Clave + " = '" + Rubros_Negocios.P_Clave + "', ");
                Mi_SQL.Append(Cat_Psp_Rubro.Campo_Descripcion + " = '" + Rubros_Negocios.P_Descripcion + "', ");
                Mi_SQL.Append(Cat_Psp_Rubro.Campo_Estatus + " = '" + Rubros_Negocios.P_Estatus + "', ");
                Mi_SQL.Append(Cat_Psp_Rubro.Campo_Anio + " = '" + Rubros_Negocios.P_Anio + "', ");
                Mi_SQL.Append(Cat_Psp_Rubro.Campo_Usuario_Modifico + " = '" + Rubros_Negocios.P_Usuario_Modifico + "', ");
                Mi_SQL.Append(Cat_Psp_Rubro.Campo_Fecha_Modifico + " = GETDATE()");
                Mi_SQL.Append(" WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Rubros_Negocios.P_Rubro_ID + "' ");

                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                Operacion_Completa = true;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al ejecutar los la modificación de los rubros. Error: [" + Ex.Message + "]");
            }
            return Operacion_Completa;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Rubros
        ///DESCRIPCIÓN          : consulta para guardar los datos de los rubros
        ///PARAMETROS           1 Rubros_Negocios: conexion con la capa de negocios
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 15/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static Boolean Alta_Rubros(Cls_Cat_Psp_Rubros_Negocio Rubros_Negocios)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenará la consulta.
            Boolean Operacion_Completa = false;//Estado de la operacion.
            String Id = Consecutivo_ID(Cat_Psp_Rubro.Campo_Rubro_ID, Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro, "5");

            try
            {
                Mi_SQL.Append("INSERT INTO " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "(");
                Mi_SQL.Append(Cat_Psp_Rubro.Campo_Rubro_ID + ", ");
                Mi_SQL.Append(Cat_Psp_Rubro.Campo_Clave + ", ");
                Mi_SQL.Append(Cat_Psp_Rubro.Campo_Descripcion + ", ");
                Mi_SQL.Append(Cat_Psp_Rubro.Campo_Estatus + ", ");
                Mi_SQL.Append(Cat_Psp_Rubro.Campo_Anio + ", ");
                Mi_SQL.Append(Cat_Psp_Rubro.Campo_Usuario_Creo + ", ");
                Mi_SQL.Append(Cat_Psp_Rubro.Campo_Fecha_Creo + ") VALUES( ");
                Mi_SQL.Append("'" + Id + "', ");
                Mi_SQL.Append("'" + Rubros_Negocios.P_Clave + "', ");
                Mi_SQL.Append("'" + Rubros_Negocios.P_Descripcion + "', ");
                Mi_SQL.Append("'" + Rubros_Negocios.P_Estatus + "', ");
                Mi_SQL.Append("" + Rubros_Negocios.P_Anio + ", ");
                Mi_SQL.Append("'" + Rubros_Negocios.P_Usuario_Creo + "', ");
                Mi_SQL.Append("GETDATE()) ");

                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                Operacion_Completa = true;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al ejecutar el alta de los rubros. Error: [" + Ex.Message + "]");
            }
            return Operacion_Completa;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consecutivo_ID
        ///DESCRIPCIÓN          : consulta para obtener el consecutivo de una tabla
        ///PARAMETROS           1 Campo_Id: campo del que se obtendra el consecutivo
        ///                     2 Tabla: tabla del que se obtendra el consecutivo
        ///                     3 Tamaño: longitud del campo 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 15/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static String Consecutivo_ID(String Campo_Id, String Tabla, String Tamaño)
        {
            String Consecutivo = "";
            StringBuilder Mi_SQL = new StringBuilder();
            object Id; //Obtiene el ID con la cual se guardo los datos en la base de datos

            if (Tamaño.Equals("5"))
            {
                Mi_SQL.Append("SELECT ISNULL(MAX (" + Campo_Id + "), '00000')");
                Mi_SQL.Append(" FROM " + Tabla);

                Id = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                if (Convert.IsDBNull(Id))
                {
                    Consecutivo = "00001";
                }
                else
                {
                    Consecutivo = string.Format("{0:00000}", Convert.ToInt32(Id) + 1);
                }
            }
            else if (Tamaño.Equals("10"))
            {
                Mi_SQL.Append("SELECT ISNULL(MAX (" + Campo_Id + "), '0000000000')");
                Mi_SQL.Append(" FROM " + Tabla);

                Id = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                if (Convert.IsDBNull(Id))
                {
                    Consecutivo = "0000000001";
                }
                else
                {
                    Consecutivo = string.Format("{0:0000000000}", Convert.ToInt32(Id) + 1);
                }
            }

            return Consecutivo;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Validar_Operador_Comparacion
        ///DESCRIPCIÓN          : Devuelve una cadena adecuada al operador indicado en la capa de Negocios
        ///PARAMETROS           : 
        ///CREO                 : Antonio Salvador Benavides Guardado
        ///FECHA_CREO           : 20/Agosto/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private static String Validar_Operador_Comparacion(String Filtro)
        {
            String Cadena_Validada;
            if (Filtro.Trim().StartsWith("<")
               || Filtro.Trim().StartsWith(">")
               || Filtro.Trim().StartsWith("<>")
               || Filtro.Trim().StartsWith("<=")
               || Filtro.Trim().StartsWith(">=")
               || Filtro.Trim().StartsWith("=")
               || Filtro.Trim().ToUpper().StartsWith("BETWEEN")
               || Filtro.Trim().ToUpper().StartsWith("LIKE")
               || Filtro.Trim().ToUpper().StartsWith("IN")
               || Filtro.Trim().ToUpper().StartsWith("NOT IN"))
            {
                Cadena_Validada = " " + Filtro + " ";
            }
            else
            {
                if (Filtro.Trim().ToUpper().StartsWith("NULL")
                    || Filtro.Trim().ToUpper().StartsWith("NOT NULL"))
                {
                    Cadena_Validada = " IS " + Filtro + " ";
                }
                else
                {
                    if (Filtro.Trim().ToUpper().StartsWith("(") && Filtro.Trim().ToUpper().EndsWith(")"))
                    {
                        Cadena_Validada = " = " + Filtro;
                    }
                    else
                    {
                        Cadena_Validada = " = '" + Filtro + "' ";
                    }
                }
            }
            return Cadena_Validada;
        }
        #endregion
    }
}