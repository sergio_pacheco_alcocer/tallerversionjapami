﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Presupuestos_Reporte_Calendarizacion.Negocio;

/// <summary>
/// Summary description for Cls_Rpt_Psp_Calendarizacion_Datos
/// </summary>
namespace JAPAMI.Presupuestos_Reporte_Calendarizacion.Datos
{
    public class Cls_Rpt_Psp_Calendarizacion_Datos
    {
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: consultar_Anio()
        ///DESCRIPCIÓN:          Realiza la consulta para llenar el combo de años.
        ///PROPIEDADES:     
        ///CREO:                 Luis Daniel Guzmán Malagón
        ///FECHA_CREO:           20/Septiembre/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable consultar_Anio() {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            try {
                Mi_SQL = "select distinct " + Ope_Psp_Calendarizacion_Presu .Campo_Anio+ " from " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu;

                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
                
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar Años. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: consultar_unidad_responsable()
        ///DESCRIPCIÓN:          Realiza la consulta para llenar el combo de unidad Responsable.
        ///PROPIEDADES:          El año que seleccionó el usuario atravez del objeto parametros_negocio
        ///CREO:                 Luis Daniel Guzmán Malagón
        ///FECHA_CREO:           20/Septiembre/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Unidad_Responsable(Cls_Rpt_Psp_Calendarizacion_Negocio Parametros_Negocio) {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            try {
                Mi_SQL = "SELECT DISTINCT (" + Cat_Dependencias.Campo_Clave + " + ' - ' + " + Cat_Dependencias.Campo_Nombre + ") as DEPENDENCIA, " + Cat_Dependencias.Campo_Clave + " from " + 
                    Cat_Dependencias.Tabla_Cat_Dependencias +
                        " dependencia join " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu +
                            " calen on calen." + Ope_Psp_Calendarizacion_Presu .Campo_Dependencia_ID+ " = dependencia."+Cat_Dependencias.Campo_Dependencia_ID+"";
                if(!String.IsNullOrEmpty(Parametros_Negocio.P_Anio))
                    Mi_SQL += " WHERE calen." + Ope_Psp_Calendarizacion_Presu.Campo_Anio+ " = '" + Parametros_Negocio.P_Anio + "'";
                
                
                
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al consultar Las Unidades Rsponsables. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }

            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: consultar_partida()
        ///DESCRIPCIÓN:          Realiza la consulta para llenar el combo Partida.
        ///PROPIEDADES:          El año y la unidad responsable que seleccionó el usuario atravez del objeto parametros_negocio
        ///CREO:                 Luis Daniel Guzmán Malagón
        ///FECHA_CREO:           20/Septiembre/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Partida(Cls_Rpt_Psp_Calendarizacion_Negocio Parametros_Negocio)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            try {
                Mi_SQL = "select DISTINCT(partida." + Cat_Sap_Partidas_Especificas.Campo_Clave + " + ' - ' + partida." + Cat_Sap_Partidas_Especificas.Campo_Nombre + ")as PARTIDA, partida." + Cat_Sap_Partidas_Especificas.Campo_Clave + " from " +
                    Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " partida join " +
                        Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + " calen on partida." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = calen." + Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID + " join " +
                            Cat_Dependencias.Tabla_Cat_Dependencias + " dependencia on dependencia."+Cat_Dependencias.Campo_Dependencia_ID+" = calen."+Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID+"";
               if(!String.IsNullOrEmpty(Parametros_Negocio.P_Anio))
                Mi_SQL += " where calen."+Ope_Psp_Calendarizacion_Presu.Campo_Anio+" ='" + Parametros_Negocio.P_Anio + "'";
                
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
                
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al consultar las Partidas. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }

            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: consultar_calendarizacion()
        ///DESCRIPCIÓN:          Realiza la consulta para generar la calendarizacion .
        ///PROPIEDADES:          Los filtros seleccionó el usuario atravez del objeto parametros_negocio
        ///CREO:                 Luis Daniel Guzmán Malagón
        ///FECHA_CREO:           20/Septiembre/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consulta_Calendarizacion(Cls_Rpt_Psp_Calendarizacion_Negocio Parametros_Negocio)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            try
            {
                //Mi_SQL = "SELECT * FROM "+Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu;
                Mi_SQL = " SELECT calen."+Ope_Psp_Calendarizacion_Presu.Campo_Anio+",";
                Mi_SQL += " fte." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS FTE_FINANCIAMIENTO_ID,";
                Mi_SQL += " area."+ Cat_SAP_Area_Funcional.Campo_Clave +" as AREA_FUNCIONAL_ID,";
                Mi_SQL += " pro."+ Cat_Sap_Proyectos_Programas.Campo_Clave +" as PROYECTO_PROGRAMA_ID,";
                Mi_SQL += " depe."+ Cat_Dependencias.Campo_Clave +" as DEPENDENCIA_ID,";
                Mi_SQL += " par."+ Cat_Sap_Partidas_Especificas.Campo_Clave +" as PARTIDA_ID,";
                Mi_SQL += " sub."+ Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal +" AS SUBNIVEL_PRESUPUESTAL_ID,";
                Mi_SQL += " produ."+Cat_Com_Productos.Campo_Clave +" as PRODUCTO_ID,";
                Mi_SQL += " calen." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Enero + ",";
                Mi_SQL += " calen." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Febrero + ",";
                Mi_SQL += " calen." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Marzo + ",";
                Mi_SQL += " calen." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Abril + ",";
                Mi_SQL += " calen." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Mayo + ",";
                Mi_SQL += " calen." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Junio + ",";
                Mi_SQL += " calen." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Julio + ",";
                Mi_SQL += " calen." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Agosto + ",";
                Mi_SQL += " calen." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Septiembre + ",";
                Mi_SQL += " calen." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Octubre + ",";
                Mi_SQL += " calen." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Noviembre + ",";
                Mi_SQL += " calen." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Diciembre + ",";
                Mi_SQL += " calen." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + "";
                Mi_SQL += " FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + " calen ";
                Mi_SQL += " left join "+ Cat_Dependencias.Tabla_Cat_Dependencias +" depe on calen."+Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID+" = depe."+Cat_Dependencias.Campo_Dependencia_ID;
                Mi_SQL += " left join "+ Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento +" fte on calen."+Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID+" = fte."+Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
                Mi_SQL += " left join "+ Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas +" pro on calen."+Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID+" = pro."+Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id;
                Mi_SQL += " left join "+ Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas +" par on calen."+Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID+" = par."+Cat_Sap_Partidas_Especificas.Campo_Partida_ID;
                Mi_SQL += " left join "+ Cat_Com_Productos.Tabla_Cat_Com_Productos +" produ on calen."+Ope_Psp_Calendarizacion_Presu.Campo_Producto_ID+" = produ."+Cat_Com_Productos.Campo_Producto_ID;
                Mi_SQL += " left join "+ Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional +" area on calen."+Ope_Psp_Calendarizacion_Presu.Campo_Area_Funcional_ID+" = area."+Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID;
                Mi_SQL += " left join "+ Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos +" sub on calen."+Ope_Psp_Calendarizacion_Presu.Campo_Subnivel_Presupuestal_ID+" = sub."+Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal_ID;
                
                if(!String.IsNullOrEmpty(Parametros_Negocio.P_Anio))
                    Mi_SQL += " where calen." + Ope_Psp_Calendarizacion_Presu.Campo_Anio + " ='" + Parametros_Negocio.P_Anio + "'";
                if(!String.IsNullOrEmpty(Parametros_Negocio.P_Unidad_Responsable))
                    Mi_SQL += " AND depe." + Cat_Dependencias.Campo_Clave + " = '" + Parametros_Negocio.P_Unidad_Responsable + "'";
                if(!String.IsNullOrEmpty(Parametros_Negocio.P_Partida))
                    Mi_SQL += " AND par." + Cat_Sap_Partidas_Especificas.Campo_Clave + " = '" + Parametros_Negocio.P_Partida + "'";

                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al consultar la Calendarizacion. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }

            return Dt_Datos;
        }
    }
}