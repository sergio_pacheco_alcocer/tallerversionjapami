﻿using System;
using System.Data;
using System.Text;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Rpt_Psp_Movimeitnos_Presupuesto_Ingresos.Negocio;

namespace JAPAMI.Rpt_Psp_Movimeitnos_Presupuesto_Ingresos.Datos
{
    public class Cls_Rpt_Psp_Movimeitnos_Presupuesto_Ingresos_Datos
    {
        #region(Metodos)
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Anios
            ///DESCRIPCIÓN          : Obtiene datos de los años de los movimientos
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 05/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Anios()
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds_Anios = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt_Anios = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT DISTINCT " + Ope_Psp_Movimiento_Ing.Campo_Anio);
                    Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Ing.Tabla_Ope_Psp_Movimiento_Ing);
                    Mi_SQL.Append(" ORDER BY " + Ope_Psp_Movimiento_Ing.Campo_Anio + " DESC");


                    Ds_Anios = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds_Anios != null)
                    {
                        Dt_Anios = Ds_Anios.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros de los años. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Anios;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_No_Modificacion
            ///DESCRIPCIÓN          : Obtiene datos de los movimientos de los sños de las modificaciones
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 05/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_No_Modificacion(Cls_Rpt_Psp_Movimeitnos_Presupuesto_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds_Anios = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt_Anios = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT " + Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing);
                    Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Ing.Tabla_Ope_Psp_Movimiento_Ing);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Ing.Campo_Anio + " = " + Negocio.P_Anio.Trim());
                    Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing + " IN (");
                    Mi_SQL.Append(" SELECT " + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing);
                    Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Ing.Campo_Anio + " = " + Negocio.P_Anio.Trim() + ")");
                    Mi_SQL.Append(" ORDER BY " + Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing + " DESC");

                    Ds_Anios = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds_Anios != null)
                    {
                        Dt_Anios = Ds_Anios.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros de las modificaciones. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Anios;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Movimientos_Autorizado
            ///DESCRIPCIÓN          : Obtiene datos de los movimientos
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 05/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Movimientos_Autorizado(Cls_Rpt_Psp_Movimeitnos_Presupuesto_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt = new DataTable();
                Int32 No_Movim;

                try
                {
                    Mi_SQL.Append("SELECT " + Ope_Psp_Presupuesto_Ing_Esp.Campo_No_Modificacion);
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ing_Esp.Campo_No_Modificacion + " = " + Negocio.P_No_Movimiento_Ing);
                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ing_Esp.Campo_Anio + " = " + Negocio.P_Anio.Trim());

                    No_Movim = Convert.ToInt32(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()));

                    if (No_Movim > 0)
                    {
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("SELECT ");
                        Mi_SQL.Append("(CASE WHEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_SubConcepto_Ing_ID + " IS NULL ");
                        Mi_SQL.Append(" THEN ");
                        Mi_SQL.Append(" REPLACE(" + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                        Mi_SQL.Append(" ELSE ");
                        Mi_SQL.Append(" REPLACE(" + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion);
                        Mi_SQL.Append(" END) AS CONCEPTO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Aprobado + ",0)) AS APROBADO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Ampliacion + ",0)) AS INCREMENTO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Reduccion + ",0)) AS DISMINUCION, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Modificado + ",0)) AS MODIFICADO, ");
                        Mi_SQL.Append(" 'SUBCONCEPTO' AS TIPO, '" + Cls_Sessiones.Nombre_Empleado.Trim() + "' AS ELABORO ");
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det);
                        Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_No_Modificacion);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Anio);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Anio);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Rubro_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Rubro_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Tipo_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_SubConcepto_Ing_ID + " = ");
                        Mi_SQL.Append("CASE WHEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_SubConcepto_Ing_ID + " IS NOT NULL ");
                        Mi_SQL.Append(" THEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" ELSE NULL END ");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_No_Modificacion);
                        Mi_SQL.Append(" = " + Negocio.P_No_Movimiento_Ing.Trim());
                        Mi_SQL.Append(" GROUP BY ");
                        Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", ");
                        Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + ", ");
                        Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + ", ");
                        Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_SubConcepto_Ing_ID);

                        Mi_SQL.Append(" UNION ");

                        Mi_SQL.Append("SELECT ");
                        Mi_SQL.Append(" REPLACE(" + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + " AS CONCEPTO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Aprobado + ",0)) AS APROBADO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Ampliacion + ",0)) AS INCREMENTO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Reduccion + ",0)) AS DISMINUCION, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Modificado + ",0)) AS MODIFICADO, ");
                        Mi_SQL.Append(" 'RUBRO' AS TIPO, '" + Cls_Sessiones.Nombre_Empleado.Trim() + "' AS ELABORO ");
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Rubro_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det);
                        Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_No_Modificacion);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Anio);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Anio);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Rubro_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Rubro_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Tipo_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_SubConcepto_Ing_ID + " = ");
                        Mi_SQL.Append("CASE WHEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_SubConcepto_Ing_ID + " IS NOT NULL ");
                        Mi_SQL.Append(" THEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" ELSE NULL END ");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_No_Modificacion);
                        Mi_SQL.Append(" = " + Negocio.P_No_Movimiento_Ing.Trim());
                        Mi_SQL.Append(" GROUP BY ");
                        Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + ", ");
                        Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion);


                        Mi_SQL.Append(" UNION ");

                        Mi_SQL.Append("SELECT ");
                        Mi_SQL.Append(" REPLACE(" + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + " AS CONCEPTO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Aprobado + ",0)) AS APROBADO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Ampliacion + ",0)) AS INCREMENTO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Reduccion + ",0)) AS DISMINUCION, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Modificado + ",0)) AS MODIFICADO, ");
                        Mi_SQL.Append(" 'TIPO' AS TIPO, '" + Cls_Sessiones.Nombre_Empleado.Trim() + "' AS ELABORO ");
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Tipo_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det);
                        Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_No_Modificacion);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Anio);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Anio);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Rubro_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Rubro_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Tipo_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_SubConcepto_Ing_ID + " = ");
                        Mi_SQL.Append("CASE WHEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_SubConcepto_Ing_ID + " IS NOT NULL ");
                        Mi_SQL.Append(" THEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" ELSE NULL END ");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_No_Modificacion);
                        Mi_SQL.Append(" = " + Negocio.P_No_Movimiento_Ing.Trim());
                        Mi_SQL.Append(" GROUP BY ");
                        Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + ", ");
                        Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion);

                        Mi_SQL.Append(" UNION ");

                        Mi_SQL.Append("SELECT ");
                        Mi_SQL.Append(" REPLACE(" + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + " AS CONCEPTO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Aprobado + ",0)) AS APROBADO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Ampliacion + ",0)) AS INCREMENTO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Reduccion + ",0)) AS DISMINUCION, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Modificado + ",0)) AS MODIFICADO, ");
                        Mi_SQL.Append(" 'CLASE' AS TIPO, '" + Cls_Sessiones.Nombre_Empleado.Trim() + "' AS ELABORO ");
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det);
                        Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_No_Modificacion);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Anio);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Anio);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Rubro_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Rubro_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Tipo_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_SubConcepto_Ing_ID + " = ");
                        Mi_SQL.Append("CASE WHEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_SubConcepto_Ing_ID + " IS NOT NULL ");
                        Mi_SQL.Append(" THEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" ELSE NULL END ");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_No_Modificacion);
                        Mi_SQL.Append(" = " + Negocio.P_No_Movimiento_Ing.Trim());
                        Mi_SQL.Append(" GROUP BY ");
                        Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + ", ");
                        Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion);

                        Mi_SQL.Append(" UNION ");

                        Mi_SQL.Append("SELECT ");
                        Mi_SQL.Append(" REPLACE(" + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + " AS CONCEPTO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Aprobado + ",0)) AS APROBADO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Ampliacion + ",0)) AS INCREMENTO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Reduccion + ",0)) AS DISMINUCION, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Modificado + ",0)) AS MODIFICADO, ");
                        Mi_SQL.Append(" 'CONCEPTO' AS TIPO, '" + Cls_Sessiones.Nombre_Empleado.Trim() + "' AS ELABORO ");
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det);
                        Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_No_Modificacion);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Anio);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Anio);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Rubro_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Rubro_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Tipo_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_SubConcepto_Ing_ID + " = ");
                        Mi_SQL.Append("CASE WHEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_SubConcepto_Ing_ID + " IS NOT NULL ");
                        Mi_SQL.Append(" THEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" ELSE NULL END ");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_No_Modificacion);
                        Mi_SQL.Append(" = " + Negocio.P_No_Movimiento_Ing.Trim());
                        Mi_SQL.Append(" GROUP BY ");
                        Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", ");
                        Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);

                        Mi_SQL.Append(" UNION ");

                        Mi_SQL.Append("SELECT '' AS CONCEPTO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Aprobado + ",0)) AS APROBADO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Ampliacion + ",0)) AS INCREMENTO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Reduccion + ",0)) AS DISMINUCION, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Modificado + ",0)) AS MODIFICADO, ");
                        Mi_SQL.Append(" 'TOTAL' AS TIPO, '" + Cls_Sessiones.Nombre_Empleado.Trim() + "' AS ELABORO ");
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det);
                        Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_No_Modificacion);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Anio);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Anio);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Rubro_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Rubro_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Tipo_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_SubConcepto_Ing_ID + " = ");
                        Mi_SQL.Append("CASE WHEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_SubConcepto_Ing_ID + " IS NOT NULL ");
                        Mi_SQL.Append(" THEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" ELSE NULL END ");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_No_Modificacion);
                        Mi_SQL.Append(" = " + Negocio.P_No_Movimiento_Ing.Trim());
                    }
                    else 
                    {
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("SELECT ");
                        Mi_SQL.Append("(CASE WHEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL ");
                        Mi_SQL.Append(" THEN ");
                        Mi_SQL.Append(" REPLACE(" + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                        Mi_SQL.Append(" ELSE ");
                        Mi_SQL.Append(" REPLACE(" + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion);
                        Mi_SQL.Append(" END) AS CONCEPTO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ",0)) AS APROBADO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Ampliacion + ",0)) AS INCREMENTO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Reduccion + ",0)) AS DISMINUCION, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ",0)) AS MODIFICADO, ");
                        Mi_SQL.Append(" 'SUBCONCEPTO' AS TIPO, '" + Cls_Sessiones.Nombre_Empleado.Trim() + "' AS ELABORO ");
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det);
                        Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Anio);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing);
                        Mi_SQL.Append(" = " + Negocio.P_No_Movimiento_Ing.Trim());
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Estatus);
                        Mi_SQL.Append(" IN ('GENERADO', 'ACEPTADO', 'PREAUTORIZADO')");
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Rubro_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_SubConcepto_Ing_ID + " = ");
                        Mi_SQL.Append("CASE WHEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NOT NULL ");
                        Mi_SQL.Append(" THEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" ELSE NULL END ");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        Mi_SQL.Append(" GROUP BY ");
                        Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", ");
                        Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + ", ");
                        Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + ", ");
                        Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);

                        Mi_SQL.Append(" UNION ");

                        Mi_SQL.Append("SELECT ");
                        Mi_SQL.Append(" REPLACE(" + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + " AS CONCEPTO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ",0)) AS APROBADO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Ampliacion + ",0)) AS INCREMENTO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Reduccion + ",0)) AS DISMINUCION, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ",0)) AS MODIFICADO, ");
                        Mi_SQL.Append(" 'RUBRO' AS TIPO, '" + Cls_Sessiones.Nombre_Empleado.Trim() + "' AS ELABORO ");
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det);
                        Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Anio);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing);
                        Mi_SQL.Append(" = " + Negocio.P_No_Movimiento_Ing.Trim());
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Estatus);
                        Mi_SQL.Append(" IN ('GENERADO', 'ACEPTADO', 'PREAUTORIZADO')");
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Rubro_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_SubConcepto_Ing_ID + " = ");
                        Mi_SQL.Append("CASE WHEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NOT NULL ");
                        Mi_SQL.Append(" THEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" ELSE NULL END ");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        Mi_SQL.Append(" GROUP BY ");
                        Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + ", ");
                        Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion);

                        Mi_SQL.Append(" UNION ");

                        Mi_SQL.Append("SELECT ");
                        Mi_SQL.Append(" REPLACE(" + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + " AS CONCEPTO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ",0)) AS APROBADO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Ampliacion + ",0)) AS INCREMENTO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Reduccion + ",0)) AS DISMINUCION, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ",0)) AS MODIFICADO, ");
                        Mi_SQL.Append(" 'TIPO' AS TIPO, '" + Cls_Sessiones.Nombre_Empleado.Trim() + "' AS ELABORO ");
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det);
                        Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Anio);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing);
                        Mi_SQL.Append(" = " + Negocio.P_No_Movimiento_Ing.Trim());
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Estatus);
                        Mi_SQL.Append(" IN ('GENERADO', 'ACEPTADO', 'PREAUTORIZADO')");
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Rubro_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_SubConcepto_Ing_ID + " = ");
                        Mi_SQL.Append("CASE WHEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NOT NULL ");
                        Mi_SQL.Append(" THEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" ELSE NULL END ");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        Mi_SQL.Append(" GROUP BY ");
                        Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + ", ");
                        Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion);

                        Mi_SQL.Append(" UNION ");

                        Mi_SQL.Append("SELECT ");
                        Mi_SQL.Append(" REPLACE(" + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + " AS CONCEPTO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ",0)) AS APROBADO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Ampliacion + ",0)) AS INCREMENTO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Reduccion + ",0)) AS DISMINUCION, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ",0)) AS MODIFICADO, ");
                        Mi_SQL.Append(" 'CLASE' AS TIPO, '" + Cls_Sessiones.Nombre_Empleado.Trim() + "' AS ELABORO ");
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det);
                        Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Anio);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing);
                        Mi_SQL.Append(" = " + Negocio.P_No_Movimiento_Ing.Trim());
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Estatus);
                        Mi_SQL.Append(" IN ('GENERADO', 'ACEPTADO', 'PREAUTORIZADO')");
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Rubro_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_SubConcepto_Ing_ID + " = ");
                        Mi_SQL.Append("CASE WHEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NOT NULL ");
                        Mi_SQL.Append(" THEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" ELSE NULL END ");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        Mi_SQL.Append(" GROUP BY ");
                        Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + ", ");
                        Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion);

                        Mi_SQL.Append(" UNION ");

                        Mi_SQL.Append("SELECT ");
                        Mi_SQL.Append(" REPLACE(" + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", ' ', '') + ' ' + ");
                        Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + " AS CONCEPTO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ",0)) AS APROBADO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Ampliacion + ",0)) AS INCREMENTO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Reduccion + ",0)) AS DISMINUCION, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ",0)) AS MODIFICADO, ");
                        Mi_SQL.Append(" 'CONCEPTO' AS TIPO, '" + Cls_Sessiones.Nombre_Empleado.Trim() + "' AS ELABORO ");
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                        Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                        Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det);
                        Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Anio);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing);
                        Mi_SQL.Append(" = " + Negocio.P_No_Movimiento_Ing.Trim());
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Estatus);
                        Mi_SQL.Append(" IN ('GENERADO', 'ACEPTADO', 'PREAUTORIZADO')");
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Rubro_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_SubConcepto_Ing_ID + " = ");
                        Mi_SQL.Append("CASE WHEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NOT NULL ");
                        Mi_SQL.Append(" THEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" ELSE NULL END ");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        Mi_SQL.Append(" GROUP BY ");
                        Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", ");
                        Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);

                        Mi_SQL.Append(" UNION ");

                        Mi_SQL.Append("SELECT '' AS CONCEPTO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ",0)) AS APROBADO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Ampliacion + ",0)) AS INCREMENTO, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Reduccion + ",0)) AS DISMINUCION, ");
                        Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ",0)) AS MODIFICADO, ");
                        Mi_SQL.Append(" 'TOTAL' AS TIPO, '" + Cls_Sessiones.Nombre_Empleado.Trim() + "' AS ELABORO ");
                        Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det);
                        Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Anio);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing);
                        Mi_SQL.Append(" = " + Negocio.P_No_Movimiento_Ing.Trim());
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Estatus);
                        Mi_SQL.Append(" IN ('GENERADO', 'ACEPTADO', 'PREAUTORIZADO')");
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Rubro_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" AN " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_SubConcepto_Ing_ID + " = ");
                        Mi_SQL.Append("CASE WHEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NOT NULL ");
                        Mi_SQL.Append(" THEN ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                        Mi_SQL.Append(" ELSE NULL END ");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    Mi_SQL.Append(" ORDER BY CONCEPTO ASC");

                    Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds != null)
                    {
                        Dt = Ds.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros de las modificaciones. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Movimientos
            ///DESCRIPCIÓN          : Obtiene datos de los movimientos
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 05/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Movimientos(Cls_Rpt_Psp_Movimeitnos_Presupuesto_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT ");
                    Mi_SQL.Append("(CASE WHEN ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL ");
                    Mi_SQL.Append(" THEN ");
                    Mi_SQL.Append(" REPLACE(" + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", ' ', '') + ' ' + ");
                    Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                    Mi_SQL.Append(" ELSE ");
                    Mi_SQL.Append(" REPLACE(" + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + ", ' ', '') + ' ' + ");
                    Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion);
                    Mi_SQL.Append(" END) AS CONCEPTO, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Modificado + ",0)) AS MODIFICADO, ");
                    Mi_SQL.Append(" 'CONCEPTO' AS TIPO ");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                    Mi_SQL.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                    Mi_SQL.Append(" = " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                    Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                    Mi_SQL.Append(" GROUP BY ");
                    Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", ");
                    Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + ", ");
                    Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + ", ");
                    Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion);

                    Mi_SQL.Append(" UNION ");

                    Mi_SQL.Append("SELECT ");
                    Mi_SQL.Append(" REPLACE(" + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + ", ' ', '') + ' ' + ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + " AS CONCEPTO");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Modificado + ",0)) AS MODIFICADO, ");
                    Mi_SQL.Append(" 'TOTAL' AS TIPO ");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Rubro_ID);
                    Mi_SQL.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_Anio);
                    Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "." + Ope_Psp_Presupuesto_Ing_Esp.Campo_No_Modificacion);
                    Mi_SQL.Append(" = " + Negocio.P_No_Movimiento_Ing.Trim());
                    Mi_SQL.Append(" GROUP BY ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + ", ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion);

                    Mi_SQL.Append(" ORDER BY CONCEPTO ASC");

                    Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds != null)
                    {
                        Dt = Ds.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros de las modificaciones. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt;
            }
        #endregion
    }
}
