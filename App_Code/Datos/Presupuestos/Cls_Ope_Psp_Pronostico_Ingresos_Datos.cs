﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using System.Text;
using JAPAMI.Ope_Psp_Pronosticos_Ingresos.Negocio;
using JAPAMI.Cat_Psp_Rubros.Datos;
using JAPAMI.Ope_Con_Poliza_Ingresos.Datos;

namespace JAPAMI.Ope_Psp_Pronosticos_Ingresos.Datos
{
    public class Cls_Ope_Psp_Pronostico_Ingresos_Datos
    {
        #region Metodos
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Pronostico
            ///DESCRIPCIÓN          : consulta para obtener los datos del pronostico
            ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 28/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consulta_Pronostico(Cls_Ope_Psp_Pronostico_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                    Mi_Sql.Append("SELECT " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS CLAVE_FTE_FINANCIAMIENTO, ");
                    Mi_Sql.Append(Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Rubro_ID + ", ");
                    Mi_Sql.Append("(CASE WHEN " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL THEN ");
                    Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                    Mi_Sql.Append(" ELSE ");
                    Mi_Sql.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion);
                    Mi_Sql.Append(" END) AS CLAVE_NOMBRE_CONCEPTOS, ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre);
                    Mi_Sql.Append(" AS CLAVE_NOMBRE_PROGRAMAS, ");
                    Mi_Sql.Append(Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Tipo_ID + ", ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Clase_Ing_ID + ", '') AS CLASE_ING_ID, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Concepto_Ing_ID + ", '') AS CONCEPTO_ING_ID, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_SubConcepto_Ing_ID + ", '') AS SUBCONCEPTO_ING_ID, ");
                    Mi_Sql.Append(Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Justificacion + ", ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Enero + ", 0) AS ENERO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Febrero + ", 0) AS FEBRERO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Marzo + ", 0) AS MARZO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Abril + ", 0) AS ABRIL, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Mayo + ", 0) AS MAYO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Junio + ", 0) AS JUNIO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Julio + ", 0) AS JULIO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Agosto + ", 0) AS AGOSTO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Septiembre+ ", 0) AS SEPTIEMBRE, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Octubre + ", 0) AS OCTUBRE, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Noviembre + ", 0) AS NOVIEMBRE, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Diciembre + ", 0) AS DICIEMBRE, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Total + ", 0) AS IMPORTE_TOTAL, ");
                    Mi_Sql.Append(Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Pronostico_Ing_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Anio + ", '' AS ID, ");
                    Mi_Sql.Append(Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Proyecto_Programa_ID + " AS PROGRAMA_ID, ");
                    Mi_Sql.Append(Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Estatus + ", ");
                    Mi_Sql.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + " AS CLAVE_NOMBRE_RUBRO, ");
                    Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + " AS CLAVE_NOMBRE_TIPO, ");
                    Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing  + "." + Cat_Psp_Clase_Ing.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + " AS CLAVE_NOMBRE_CLASE, ");
                    Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + " AS CLAVE_NOMBRE_CONCEPTO_ING ");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_Sql.Append(" ON " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                    Mi_Sql.Append(" ON " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Concepto_Ing_ID);
                    Mi_Sql.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                    Mi_Sql.Append(" ON " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Rubro_ID);
                    Mi_Sql.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                    Mi_Sql.Append(" ON " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Tipo_ID);
                    Mi_Sql.Append(" = " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                    Mi_Sql.Append(" ON " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Clase_Ing_ID);
                    Mi_Sql.Append(" = " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                    Mi_Sql.Append(" LEFT OUTER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                    Mi_Sql.Append(" ON " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_SubConcepto_Ing_ID);
                    Mi_Sql.Append(" = " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                    Mi_Sql.Append(" LEFT OUTER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                    Mi_Sql.Append(" ON " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Proyecto_Programa_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);

                    if (!String.IsNullOrEmpty(Negocio.P_Pronostico_Ing_ID))
                    {
                        if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" AND " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Pronostico_Ing_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_Pronostico_Ing_ID  + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Pronostico_Ing_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_Pronostico_Ing_ID + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Anio))
                    {
                        if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" AND " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Anio);
                            Mi_Sql.Append(" = '" + Negocio.P_Anio + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Anio);
                            Mi_Sql.Append(" = '" + Negocio.P_Anio + "'");
                        }
                    }

                    Mi_Sql.Append(" ORDER BY CLAVE_FTE_FINANCIAMIENTO, CLAVE_NOMBRE_CONCEPTOS ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString().Trim()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las clases. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Guardar_Registros
            ///DESCRIPCIÓN          : Consulta para guardar los registros
            ///PARAMETROS           1 Negocio conexion con la capa de negocio 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 28/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static Boolean Guardar_Registros(Cls_Ope_Psp_Pronostico_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder();
                String Pronostico_Ing_ID = String.Empty;
                String Presupuesto_Ing_ID = String.Empty;
                Int32 Presupuesto_ID = 0;
                Int32 Pronostico_ID = 0;
                Boolean Operacion_Exitosa = false;
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                Double Importe_Pro = 0.00;
                DataTable Dt_Pro = new DataTable();
                String[] Datos_Poliza_PSP = null;
                String No_Poliza_PSP = String.Empty;
                String Tipo_Poliza_PSP = String.Empty;
                String Mes_Anio_PSP = String.Empty;

                try
                {
                    Pronostico_Ing_ID = Cls_Cat_Psp_Rubros_Datos.Consecutivo_ID(Ope_Psp_Pronostico_Ingresos.Campo_Pronostico_Ing_ID, Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos, "10");
                    Pronostico_ID = Convert.ToInt32(Pronostico_Ing_ID);

                    if (Negocio.P_Estatus.Equals("AUTORIZADO"))
                    {
                        Presupuesto_Ing_ID = Cls_Cat_Psp_Rubros_Datos.Consecutivo_ID(Ope_Psp_Presupuesto_Ingresos.Campo_Presupuesto_Ing_ID, Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos, "10");
                        Presupuesto_ID = Convert.ToInt32(Presupuesto_Ing_ID);

                        //obtenemos los datos de la poliza presupuestal
                        Datos_Poliza_PSP = Registro_Mov_Autorizado(Cmd, Negocio.P_Dt_Datos, Negocio.P_Anio);

                        if (Datos_Poliza_PSP != null)
                        {
                            if (Datos_Poliza_PSP.Length > 0)
                            {
                                No_Poliza_PSP = Datos_Poliza_PSP[0].Trim();
                                Tipo_Poliza_PSP = Datos_Poliza_PSP[1].Trim();
                                Mes_Anio_PSP = Datos_Poliza_PSP[2].Trim();
                            }
                        }
                    }

                    foreach (DataRow Renglon in Negocio.P_Dt_Datos.Rows)
                    {
                        Mi_SQL = new StringBuilder();

                        Mi_SQL.Append("INSERT INTO " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + " (");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Pronostico_Ing_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                        if (!String.IsNullOrEmpty(Renglon["PROGRAMA_ID"].ToString().Trim()))
                        {
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Proyecto_Programa_ID + ", ");
                        }
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Dependencia_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Rubro_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Tipo_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Clase_Ing_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Concepto_Ing_ID + ", ");
                        if (!String.IsNullOrEmpty(Renglon["SUBCONCEPTO_ING_ID"].ToString().Trim()))
                        {
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_SubConcepto_Ing_ID + ", ");
                        }
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Anio + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Justificacion + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Estatus + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Enero + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Febrero + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Marzo + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Abril + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Mayo + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Junio + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Julio + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Agosto + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Septiembre + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Octubre + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Noviembre + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Diciembre + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Total + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_No_Poliza_Presupuestal + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Tipo_Poliza_ID_Presupuestal + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Mes_Anio_Presupuestal + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Usuario_Creo + ", ");
                        Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Fecha_Creo + ") VALUES (");
                        Mi_SQL.Append("'" + String.Format("{0:0000000000}", Pronostico_ID) + "', ");
                        Mi_SQL.Append("'" + Renglon["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "', ");
                        if (!String.IsNullOrEmpty(Renglon["PROGRAMA_ID"].ToString().Trim()))
                        {
                            Mi_SQL.Append("'" + Renglon["PROGRAMA_ID"].ToString().Trim() + "', ");
                        }
                        Mi_SQL.Append("NULL, ");
                        Mi_SQL.Append("'" + Renglon["RUBRO_ID"].ToString().Trim() + "', ");
                        Mi_SQL.Append("'" + Renglon["TIPO_ID"].ToString().Trim() + "', ");
                        Mi_SQL.Append("'" + Renglon["CLASE_ING_ID"].ToString().Trim() + "', ");
                        Mi_SQL.Append("'" + Renglon["CONCEPTO_ING_ID"].ToString().Trim() + "', ");
                        if (!String.IsNullOrEmpty(Renglon["SUBCONCEPTO_ING_ID"].ToString().Trim()))
                        {
                            Mi_SQL.Append("'" + Renglon["SUBCONCEPTO_ING_ID"].ToString().Trim() + "', ");
                        }
                        Mi_SQL.Append("'" + Negocio.P_Anio + "',");
                        Mi_SQL.Append("'" + Renglon["JUSTIFICACION"].ToString().Trim() + "', ");
                        Mi_SQL.Append("'" + Negocio.P_Estatus + "',");
                        Mi_SQL.Append(Renglon["ENERO"].ToString().Trim().Replace(",", "") + ", ");
                        Mi_SQL.Append(Renglon["FEBRERO"].ToString().Trim().Replace(",", "") + ", ");
                        Mi_SQL.Append(Renglon["MARZO"].ToString().Trim().Replace(",", "") + ", ");
                        Mi_SQL.Append(Renglon["ABRIL"].ToString().Trim().Replace(",", "") + ", ");
                        Mi_SQL.Append(Renglon["MAYO"].ToString().Trim().Replace(",", "") + ", ");
                        Mi_SQL.Append(Renglon["JUNIO"].ToString().Trim().Replace(",", "") + ", ");
                        Mi_SQL.Append(Renglon["JULIO"].ToString().Trim().Replace(",", "") + ", ");
                        Mi_SQL.Append(Renglon["AGOSTO"].ToString().Trim().Replace(",", "") + ", ");
                        Mi_SQL.Append(Renglon["SEPTIEMBRE"].ToString().Trim().Replace(",", "") + ", ");
                        Mi_SQL.Append(Renglon["OCTUBRE"].ToString().Trim().Replace(",", "") + ", ");
                        Mi_SQL.Append(Renglon["NOVIEMBRE"].ToString().Trim().Replace(",", "") + ", ");
                        Mi_SQL.Append(Renglon["DICIEMBRE"].ToString().Trim().Replace(",", "") + ", ");
                        Mi_SQL.Append(Renglon["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + ", ");

                        if (Negocio.P_Estatus.Equals("AUTORIZADO"))
                        {
                            Mi_SQL.Append("'" + No_Poliza_PSP.Trim() + "', ");
                            Mi_SQL.Append("'" + Tipo_Poliza_PSP.Trim() + "', ");
                            Mi_SQL.Append("'" + Mes_Anio_PSP.Trim() + "', ");
                        }
                        else 
                        {
                            Mi_SQL.Append("NULL, ");
                            Mi_SQL.Append("NULL, ");
                            Mi_SQL.Append("NULL, ");
                        }

                        Mi_SQL.Append("'" + Negocio.P_Usuario_Creo + "',");
                        Mi_SQL.Append("GETDATE())");
                        Cmd.CommandText = Mi_SQL.ToString().Trim();
                        Cmd.ExecuteNonQuery();
                        Pronostico_ID++;

                        if (Negocio.P_Estatus.Equals("AUTORIZADO"))
                        {
                            Mi_SQL = new StringBuilder();

                            Mi_SQL.Append("INSERT INTO " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + " (");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Presupuesto_Ing_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Anio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Enero + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Febrero + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Marzo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Abril + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Mayo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Junio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Julio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Agosto + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Septiembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Octubre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Noviembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Diciembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Total + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Enero + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Febrero + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Marzo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Abril + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Mayo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Junio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Julio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Agosto + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Septiembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Octubre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Noviembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Diciembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Enero + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Febrero + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Marzo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Abril + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Mayo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Junio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Julio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Agosto + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Septiembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Octubre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Noviembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Diciembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Enero + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Febrero + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Marzo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Abril + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Mayo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Junio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Julio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Agosto + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Septiembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Octubre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Noviembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Diciembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Saldo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Actualizado + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Usuario_Creo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Fecha_Creo + ") VALUES (");
                            Mi_SQL.Append("'" + Presupuesto_ID.ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Renglon["FUENTE_FINANCIAMIENTO_ID"].ToString() + "', ");
                            if (!String.IsNullOrEmpty(Renglon["PROGRAMA_ID"].ToString().Trim()))
                            {
                                Mi_SQL.Append("'" + Renglon["PROGRAMA_ID"].ToString().Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }
                            Mi_SQL.Append("'" + Renglon["RUBRO_ID"].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Renglon["TIPO_ID"].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Renglon["CLASE_ING_ID"].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Renglon["CONCEPTO_ING_ID"].ToString().Trim() + "', ");
                            if (!String.IsNullOrEmpty(Renglon["SUBCONCEPTO_ING_ID"].ToString().Trim()))
                            {
                                Mi_SQL.Append("'" + Renglon["SUBCONCEPTO_ING_ID"].ToString().Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }
                            Mi_SQL.Append("'" + Negocio.P_Anio + "',");
                            Mi_SQL.Append(Renglon["ENERO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["FEBRERO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["MARZO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["ABRIL"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["MAYO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["JUNIO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["JULIO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["AGOSTO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["SEPTIEMBRE"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["OCTUBRE"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["NOVIEMBRE"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["DICIEMBRE"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append(Renglon["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append(Renglon["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["ENERO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["FEBRERO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["MARZO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["ABRIL"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["MAYO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["JUNIO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["JULIO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["AGOSTO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["SEPTIEMBRE"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["OCTUBRE"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["NOVIEMBRE"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["DICIEMBRE"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("0.00, ");
                            Mi_SQL.Append("'NO', ");
                            Mi_SQL.Append("'" + Negocio.P_Usuario_Creo + "',");
                            Mi_SQL.Append("GETDATE())");
                            Cmd.CommandText = Mi_SQL.ToString().Trim();
                            Cmd.ExecuteNonQuery();
                            Presupuesto_ID++;

                            if (!String.IsNullOrEmpty(Renglon["PROGRAMA_ID"].ToString().Trim()))
                            {
                                //obtenemos si el concepto tiene un importe del programa
                                Mi_SQL = new StringBuilder();
                                Mi_SQL.Append("SELECT " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Importe);
                                Mi_SQL.Append(" FROM " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa);
                                Mi_SQL.Append(" WHERE " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Proyecto_Programa_ID);
                                Mi_SQL.Append(" = '" + Renglon["PROGRAMA_ID"].ToString().Trim() + "'");
                                Mi_SQL.Append(" AND " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Concepto_Ing_ID);
                                Mi_SQL.Append(" = '" + Renglon["CONCEPTO_ING_ID"].ToString().Trim() + "'");
                                Mi_SQL.Append(" AND " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Fuente_Financiamiento_ID);
                                Mi_SQL.Append(" = '" + Renglon["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");

                                Dt_Pro = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString().Trim()).Tables[0];

                                if (Dt_Pro != null && Dt_Pro.Rows.Count > 0)
                                {
                                    Importe_Pro = Convert.ToDouble(String.IsNullOrEmpty(Dt_Pro.Rows[0][Cat_Sap_Det_Fte_Programa.Campo_Importe].ToString().Trim()) ? "0" : Dt_Pro.Rows[0][Cat_Sap_Det_Fte_Programa.Campo_Importe]);
                                    if (Importe_Pro <= 0)
                                    {
                                        //insertamos el valor del programa y su concepto
                                        Mi_SQL = new StringBuilder();
                                        Mi_SQL.Append("UPDATE " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa);
                                        Mi_SQL.Append(" SET " + Cat_Sap_Det_Fte_Programa.Campo_Importe + " = " + Renglon["IMPORTE_TOTAL"].ToString().Trim().Replace(",", ""));
                                        Mi_SQL.Append(" WHERE " + Cat_Sap_Det_Fte_Programa.Campo_Proyecto_Programa_ID);
                                        Mi_SQL.Append(" = '" + Renglon["PROGRAMA_ID"].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Cat_Sap_Det_Fte_Programa.Campo_Concepto_Ing_ID);
                                        Mi_SQL.Append(" = '" + Renglon["CONCEPTO_ING_ID"].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Cat_Sap_Det_Fte_Programa.Campo_Fuente_Financiamiento_ID);
                                        Mi_SQL.Append(" = '" + Renglon["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");

                                        Cmd.CommandText = Mi_SQL.ToString().Trim();
                                        Cmd.ExecuteNonQuery();

                                        //insertamos el valor del programa y su concepto
                                        Mi_SQL = new StringBuilder();
                                        Mi_SQL.Append("UPDATE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                                        Mi_SQL.Append(" SET " + Cat_Sap_Proyectos_Programas.Campo_Importe );
                                        Mi_SQL.Append(" + ISNULL(" + Cat_Sap_Proyectos_Programas.Campo_Importe + ", 0) = " + Renglon["IMPORTE_TOTAL"].ToString().Trim().Replace(",", ""));
                                        Mi_SQL.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                                        Mi_SQL.Append(" = '" + Renglon["PROGRAMA_ID"].ToString().Trim() + "'");

                                        Cmd.CommandText = Mi_SQL.ToString().Trim();
                                        Cmd.ExecuteNonQuery();
                                    }
                                }
                            }
                        }

                    }
                    Trans.Commit();
                    Operacion_Exitosa = true;
                }
                catch (Exception Ex)
                {
                    Operacion_Exitosa = false;
                    Trans.Rollback();
                    throw new Exception("Error al intentar insertar los registros. Error: [" + Ex.Message + "]");
                }
                return Operacion_Exitosa;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Modificar_Registros
            ///DESCRIPCIÓN          : Consulta para modificar los registros
            ///PARAMETROS           1 Negocio conexion con la capa de negocio 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 28/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static Boolean Modificar_Registros(Cls_Ope_Psp_Pronostico_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder();
                String Pronostico_Ing_ID = String.Empty;
                String Presupuesto_Ing_ID = String.Empty;
                Int32 Pronostico_ID = 0;
                Int32 Presupuesto_ID = 0;
                Boolean Operacion_Exitosa = false;
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                Double Importe_Pro = 0.00;
                DataTable Dt_Pro = new DataTable();
                String[] Datos_Poliza_PSP = null;
                String No_Poliza_PSP = String.Empty;
                String Tipo_Poliza_PSP = String.Empty;
                String Mes_Anio_PSP = String.Empty;

                try
                {
                    Pronostico_Ing_ID = Cls_Cat_Psp_Rubros_Datos.Consecutivo_ID(Ope_Psp_Pronostico_Ingresos.Campo_Pronostico_Ing_ID, Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos, "10");
                    Pronostico_ID = Convert.ToInt32(Pronostico_Ing_ID);

                    if (Negocio.P_Estatus.Equals("AUTORIZADO"))
                    {
                        Presupuesto_Ing_ID = Cls_Cat_Psp_Rubros_Datos.Consecutivo_ID(Ope_Psp_Presupuesto_Ingresos.Campo_Presupuesto_Ing_ID, Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos, "10");
                        Presupuesto_ID = Convert.ToInt32(Presupuesto_Ing_ID);

                        //obtenemos los datos de la poliza presupuestal
                        Datos_Poliza_PSP = Registro_Mov_Autorizado(Cmd, Negocio.P_Dt_Datos, Negocio.P_Anio);

                        if (Datos_Poliza_PSP != null)
                        {
                            if (Datos_Poliza_PSP.Length > 0)
                            {
                                No_Poliza_PSP = Datos_Poliza_PSP[0].Trim();
                                Tipo_Poliza_PSP = Datos_Poliza_PSP[1].Trim();
                                Mes_Anio_PSP = Datos_Poliza_PSP[2].Trim();
                            }
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("DELETE FROM " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos);
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Pronostico_Ingresos.Campo_Anio + " = '" + Negocio.P_Anio + "'");
                        Mi_SQL.Append(" AND " + Ope_Psp_Pronostico_Ingresos.Campo_Estatus + " NOT IN ('AUTORIZADO')");
                        Cmd.CommandText = Mi_SQL.ToString().Trim();
                        Cmd.ExecuteNonQuery();

                        foreach (DataRow Renglon in Negocio.P_Dt_Datos.Rows)
                        {
                            Mi_SQL = new StringBuilder();

                            Mi_SQL.Append("INSERT INTO " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + " (");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Pronostico_Ing_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                            if (!String.IsNullOrEmpty(Renglon["PROGRAMA_ID"].ToString().Trim()))
                            {
                                Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Proyecto_Programa_ID + ", ");
                            }
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Dependencia_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Rubro_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Tipo_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Clase_Ing_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Concepto_Ing_ID + ", ");
                            if (!String.IsNullOrEmpty(Renglon["SUBCONCEPTO_ING_ID"].ToString().Trim()))
                            {
                                Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_SubConcepto_Ing_ID + ", ");
                            }
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Anio + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Justificacion + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Estatus + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Enero + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Febrero + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Marzo + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Abril + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Mayo + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Junio + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Julio + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Agosto + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Septiembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Octubre + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Noviembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Diciembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Importe_Total + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_No_Poliza_Presupuestal + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Tipo_Poliza_ID_Presupuestal + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Mes_Anio_Presupuestal + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Usuario_Creo + ", ");
                            Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Campo_Fecha_Creo + ") VALUES (");
                            Mi_SQL.Append("'" + String.Format("{0:0000000000}", Pronostico_ID) + "', ");
                            Mi_SQL.Append("'" + Renglon["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "', ");
                            if (!String.IsNullOrEmpty(Renglon["PROGRAMA_ID"].ToString().Trim()))
                            {
                                Mi_SQL.Append("'" + Renglon["PROGRAMA_ID"].ToString().Trim() + "', ");
                            }
                            Mi_SQL.Append("NULL, ");
                            Mi_SQL.Append("'" + Renglon["RUBRO_ID"].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Renglon["TIPO_ID"].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Renglon["CLASE_ING_ID"].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Renglon["CONCEPTO_ING_ID"].ToString().Trim() + "', ");
                            if (!String.IsNullOrEmpty(Renglon["SUBCONCEPTO_ING_ID"].ToString().Trim()))
                            {
                                Mi_SQL.Append("'" + Renglon["SUBCONCEPTO_ING_ID"].ToString().Trim() + "', ");
                            }
                            Mi_SQL.Append("'" + Negocio.P_Anio + "',");
                            Mi_SQL.Append("'" + Renglon["JUSTIFICACION"].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Negocio.P_Estatus + "',");
                            Mi_SQL.Append(Renglon["ENERO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["FEBRERO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["MARZO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["ABRIL"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["MAYO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["JUNIO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["JULIO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["AGOSTO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["SEPTIEMBRE"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["OCTUBRE"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["NOVIEMBRE"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["DICIEMBRE"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Renglon["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + ", ");

                            if (Negocio.P_Estatus.Equals("AUTORIZADO"))
                            {
                                Mi_SQL.Append("'" + No_Poliza_PSP.Trim() + "', ");
                                Mi_SQL.Append("'" + Tipo_Poliza_PSP.Trim() + "', ");
                                Mi_SQL.Append("'" + Mes_Anio_PSP.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                                Mi_SQL.Append("NULL, ");
                                Mi_SQL.Append("NULL, ");
                            }

                            Mi_SQL.Append("'" + Negocio.P_Usuario_Creo + "',");
                            Mi_SQL.Append("GETDATE())");
                            Cmd.CommandText = Mi_SQL.ToString().Trim();
                            Cmd.ExecuteNonQuery();
                            Pronostico_ID++;

                            if (Negocio.P_Estatus.Equals("AUTORIZADO"))
                            {
                                Mi_SQL = new StringBuilder();

                                Mi_SQL.Append("INSERT INTO " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + " (");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Presupuesto_Ing_ID + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Anio + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Enero + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Febrero + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Marzo + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Abril + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Mayo + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Junio + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Julio + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Agosto + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Septiembre + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Octubre + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Noviembre + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Diciembre + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Total + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Enero + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Febrero + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Marzo + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Abril + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Mayo + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Junio + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Julio + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Agosto + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Septiembre + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Octubre + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Noviembre + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Diciembre + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Enero + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Febrero + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Marzo + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Abril + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Mayo + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Junio + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Julio + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Agosto + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Septiembre + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Octubre + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Noviembre + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Diciembre + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Enero + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Febrero + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Marzo + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Abril + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Mayo + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Junio + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Julio + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Agosto + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Septiembre + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Octubre + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Noviembre + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Diciembre + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Saldo + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Actualizado + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Usuario_Creo + ", ");
                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Fecha_Creo + ") VALUES (");
                                Mi_SQL.Append("'" + Presupuesto_ID.ToString().Trim() + "', ");
                                Mi_SQL.Append("'" + Renglon["FUENTE_FINANCIAMIENTO_ID"].ToString() + "', ");
                                if (!String.IsNullOrEmpty(Renglon["PROGRAMA_ID"].ToString().Trim()))
                                {
                                    Mi_SQL.Append("'" + Renglon["PROGRAMA_ID"].ToString().Trim() + "', ");
                                }
                                else
                                {
                                    Mi_SQL.Append("NULL, ");
                                }
                                Mi_SQL.Append("'" + Renglon["RUBRO_ID"].ToString().Trim() + "', ");
                                Mi_SQL.Append("'" + Renglon["TIPO_ID"].ToString().Trim() + "', ");
                                Mi_SQL.Append("'" + Renglon["CLASE_ING_ID"].ToString().Trim() + "', ");
                                Mi_SQL.Append("'" + Renglon["CONCEPTO_ING_ID"].ToString().Trim() + "', ");
                                if (!String.IsNullOrEmpty(Renglon["SUBCONCEPTO_ING_ID"].ToString().Trim()))
                                {
                                    Mi_SQL.Append("'" + Renglon["SUBCONCEPTO_ING_ID"].ToString().Trim() + "', ");
                                }
                                else
                                {
                                    Mi_SQL.Append("NULL, ");
                                }
                                Mi_SQL.Append("'" + Negocio.P_Anio + "',");
                                Mi_SQL.Append(Renglon["ENERO"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["FEBRERO"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["MARZO"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["ABRIL"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["MAYO"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["JUNIO"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["JULIO"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["AGOSTO"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["SEPTIEMBRE"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["OCTUBRE"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["NOVIEMBRE"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["DICIEMBRE"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append(Renglon["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append(Renglon["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["ENERO"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["FEBRERO"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["MARZO"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["ABRIL"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["MAYO"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["JUNIO"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["JULIO"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["AGOSTO"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["SEPTIEMBRE"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["OCTUBRE"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["NOVIEMBRE"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Renglon["DICIEMBRE"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("0.00, ");
                                Mi_SQL.Append("'NO', ");
                                Mi_SQL.Append("'" + Negocio.P_Usuario_Creo + "',");
                                Mi_SQL.Append("GETDATE())");
                                Cmd.CommandText = Mi_SQL.ToString().Trim();
                                Cmd.ExecuteNonQuery();
                                Presupuesto_ID++;

                                if (!String.IsNullOrEmpty(Renglon["PROGRAMA_ID"].ToString().Trim()))
                                {
                                    //obtenemos si el concepto tiene un importe del programa
                                    Mi_SQL = new StringBuilder();
                                    Mi_SQL.Append("SELECT " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Importe);
                                    Mi_SQL.Append(" FROM " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa);
                                    Mi_SQL.Append(" WHERE " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Proyecto_Programa_ID);
                                    Mi_SQL.Append(" = '" + Renglon["PROGRAMA_ID"].ToString().Trim() + "'");
                                    Mi_SQL.Append(" AND " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Concepto_Ing_ID);
                                    Mi_SQL.Append(" = '" + Renglon["CONCEPTO_ING_ID"].ToString().Trim() + "'");
                                    Mi_SQL.Append(" AND " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Fuente_Financiamiento_ID);
                                    Mi_SQL.Append(" = '" + Renglon["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");

                                    Dt_Pro = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString().Trim()).Tables[0];

                                    if (Dt_Pro != null && Dt_Pro.Rows.Count > 0)
                                    {
                                        Importe_Pro = Convert.ToDouble(String.IsNullOrEmpty(Dt_Pro.Rows[0][Cat_Sap_Det_Fte_Programa.Campo_Importe].ToString().Trim()) ? "0" : Dt_Pro.Rows[0][Cat_Sap_Det_Fte_Programa.Campo_Importe]);
                                        if (Importe_Pro <= 0)
                                        {
                                            //insertamos el valor del programa y su concepto
                                            Mi_SQL = new StringBuilder();
                                            Mi_SQL.Append("UPDATE " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa);
                                            Mi_SQL.Append(" SET " + Cat_Sap_Det_Fte_Programa.Campo_Importe + " = " + Renglon["IMPORTE_TOTAL"].ToString().Trim().Replace(",", ""));
                                            Mi_SQL.Append(" WHERE " + Cat_Sap_Det_Fte_Programa.Campo_Proyecto_Programa_ID);
                                            Mi_SQL.Append(" = '" + Renglon["PROGRAMA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Cat_Sap_Det_Fte_Programa.Campo_Concepto_Ing_ID);
                                            Mi_SQL.Append(" = '" + Renglon["CONCEPTO_ING_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Cat_Sap_Det_Fte_Programa.Campo_Fuente_Financiamiento_ID);
                                            Mi_SQL.Append(" = '" + Renglon["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");

                                            Cmd.CommandText = Mi_SQL.ToString();
                                            Cmd.ExecuteNonQuery();

                                            //insertamos el valor del programa y su concepto
                                            Mi_SQL = new StringBuilder();
                                            Mi_SQL.Append("UPDATE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                                            Mi_SQL.Append(" SET " + Cat_Sap_Proyectos_Programas.Campo_Importe);
                                            Mi_SQL.Append(" + ISNULL(" + Cat_Sap_Proyectos_Programas.Campo_Importe + ", 0) = " + Renglon["IMPORTE_TOTAL"].ToString().Replace(",", ""));
                                            Mi_SQL.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                                            Mi_SQL.Append(" = '" + Renglon["PROGRAMA_ID"].ToString().Trim() + "'");

                                            Cmd.CommandText = Mi_SQL.ToString();
                                            Cmd.ExecuteNonQuery();
                                        }
                                    }
                                }
                            }
                        }
                    }

                    
                    Trans.Commit();
                    Operacion_Exitosa = true;
                }
                catch (Exception Ex)
                {
                    Operacion_Exitosa = false;
                    Trans.Rollback();
                    throw new Exception("Error al intentar insertar los registros. Error: [" + Ex.Message + "]");
                }
                return Operacion_Exitosa;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Fte_Financiamiento
            ///DESCRIPCIÓN          : consulta para obtener los datos de las fuentes de financiamiento
            ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 13/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consulta_Fte_Financiamiento()
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                    Mi_Sql.Append("SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Campo_Descripcion  + " AS CLAVE_NOMBRE, ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Estatus + " = 'ACTIVO'");

                    Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las fuentes de finannciamiento. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Rubros
            ///DESCRIPCIÓN          : consulta para obtener los datos de los rubros
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 15/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Rubros(Cls_Ope_Psp_Pronostico_Ingresos_Negocio Rubros_Negocios)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                    Mi_Sql.Append("SELECT " + Cat_Psp_Rubro.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Psp_Rubro.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                    Mi_Sql.Append(Cat_Psp_Rubro.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_Psp_Rubro.Campo_Descripcion + ", ");
                    Mi_Sql.Append(Cat_Psp_Rubro.Campo_Estatus + ", ");
                    Mi_Sql.Append(Cat_Psp_Rubro.Campo_Rubro_ID);
                    Mi_Sql.Append(" FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);

                    if (!String.IsNullOrEmpty(Rubros_Negocios.P_Clave))
                    {
                        if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" AND " + Cat_Psp_Rubro.Campo_Clave + " = '" + Rubros_Negocios.P_Clave.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" WHERE " + Cat_Psp_Rubro.Campo_Clave + " = '" + Rubros_Negocios.P_Clave.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Rubros_Negocios.P_Descripcion))
                    {
                        if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" AND " + Cat_Psp_Rubro.Campo_Descripcion + " = '" + Rubros_Negocios.P_Descripcion.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" WHERE " + Cat_Psp_Rubro.Campo_Descripcion + " = '" + Rubros_Negocios.P_Descripcion.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Rubros_Negocios.P_Estatus))
                    {
                        if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" AND " + Cat_Psp_Rubro.Campo_Estatus + " = '" + Rubros_Negocios.P_Estatus.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" WHERE " + Cat_Psp_Rubro.Campo_Estatus + " = '" + Rubros_Negocios.P_Estatus.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Rubros_Negocios.P_Rubro_ID))
                    {
                        if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" AND " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Rubros_Negocios.P_Rubro_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " = '" + Rubros_Negocios.P_Rubro_ID.Trim() + "'");
                        }
                    }

                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Rubro.Campo_Rubro_ID + " IN(");
                        Mi_Sql.Append(" SELECT " + Cat_Psp_Tipo.Campo_Rubro_ID + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " IN(SELECT " + Cat_Psp_Clase_Ing.Campo_Tipo_ID);
                        Mi_Sql.Append(" FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                        Mi_Sql.Append(" IN(SELECT " + Cat_Psp_Concepto_Ing.Campo_Clase_Ing_ID + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + ")))");
                    }
                    else 
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Rubro.Campo_Rubro_ID + " IN(");
                        Mi_Sql.Append(" SELECT " + Cat_Psp_Tipo.Campo_Rubro_ID + " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " IN(SELECT " + Cat_Psp_Clase_Ing.Campo_Tipo_ID);
                        Mi_Sql.Append(" FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                        Mi_Sql.Append(" IN(SELECT " + Cat_Psp_Concepto_Ing.Campo_Clase_Ing_ID + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + ")))");
                    }

                    Mi_Sql.Append(" ORDER BY " + Cat_Psp_Rubro.Campo_Clave + " ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de los rubros. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_tipos
            ///DESCRIPCIÓN          : consulta para obtener los datos de los tipos
            ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 15/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Tipos(Cls_Ope_Psp_Pronostico_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                    Mi_Sql.Append("SELECT " + Cat_Psp_Tipo.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Psp_Tipo.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                    Mi_Sql.Append(Cat_Psp_Tipo.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_Psp_Tipo.Campo_Descripcion + ", ");
                    Mi_Sql.Append(Cat_Psp_Tipo.Campo_Estatus + ", ");
                    Mi_Sql.Append(Cat_Psp_Tipo.Campo_Rubro_ID + ", ");
                    Mi_Sql.Append(Cat_Psp_Tipo.Campo_Tipo_ID);
                    Mi_Sql.Append(" FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);

                    if (!String.IsNullOrEmpty(Negocio.P_Clave))
                    {
                        if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" AND " + Cat_Psp_Tipo.Campo_Clave + " = '" + Negocio.P_Clave.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" WHERE " + Cat_Psp_Tipo.Campo_Clave + " = '" + Negocio.P_Clave.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Descripcion))
                    {
                        if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" AND " + Cat_Psp_Tipo.Campo_Descripcion + " = '" + Negocio.P_Descripcion.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" WHERE " + Cat_Psp_Tipo.Campo_Descripcion + " = '" + Negocio.P_Descripcion.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Estatus))
                    {
                        if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" AND " + Cat_Psp_Tipo.Campo_Estatus + " = '" + Negocio.P_Estatus.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" WHERE " + Cat_Psp_Tipo.Campo_Estatus + " = '" + Negocio.P_Estatus.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID))
                    {
                        if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" AND " + Cat_Psp_Tipo.Campo_Rubro_ID + " = '" + Negocio.P_Rubro_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" WHERE " + Cat_Psp_Tipo.Campo_Rubro_ID + " = '" + Negocio.P_Rubro_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID))
                    {
                        if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" AND " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID.Trim() + "'");
                        }
                    }

                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Tipo.Campo_Tipo_ID + " IN(SELECT " + Cat_Psp_Clase_Ing.Campo_Tipo_ID);
                        Mi_Sql.Append(" FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                        Mi_Sql.Append(" IN(SELECT " + Cat_Psp_Concepto_Ing.Campo_Clase_Ing_ID + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "))");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " IN(SELECT " + Cat_Psp_Clase_Ing.Campo_Tipo_ID);
                        Mi_Sql.Append(" FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                        Mi_Sql.Append(" IN(SELECT " + Cat_Psp_Concepto_Ing.Campo_Clase_Ing_ID + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "))");
                    }

                    Mi_Sql.Append(" ORDER BY " + Cat_Psp_Tipo.Campo_Clave + " ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de los tipos. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Clase_Ing
            ///DESCRIPCIÓN          : consulta para obtener los datos de las clases
            ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 20/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Clase_Ing(Cls_Ope_Psp_Pronostico_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                    Mi_Sql.Append("SELECT " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                    Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + ", ");
                    Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Estatus + ", ");
                    Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + ", ");
                    Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Tipo_ID);
                    Mi_Sql.Append(" FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo
                        + " ON " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID
                        + " = " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Tipo_ID);
                    Mi_Sql.Append(" WHERE " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Estatus 
                        + " = 'ACTIVO'");


                    if (!String.IsNullOrEmpty(Negocio.P_Clase_Ing_ID))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID 
                            + " = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Tipo_ID 
                            + " = '" + Negocio.P_Tipo_ID.Trim() + "'");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Rubro_ID 
                            + " = '" + Negocio.P_Rubro_ID.Trim() + "'");
                    }

                    Mi_Sql.Append(" AND " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                    Mi_Sql.Append(" IN(SELECT " + Cat_Psp_Concepto_Ing.Campo_Clase_Ing_ID + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + ")");

                    Mi_Sql.Append(" ORDER BY " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + " ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las clases. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Fte_Financiamiento_Todas
            ///DESCRIPCIÓN          : consulta para obtener los datos de las fuentes de financiamiento
            ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 13/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consulta_Fte_Financiamiento_Todas()
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                    Mi_Sql.Append("SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);

                    Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las fuentes de finannciamiento. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_SubConcepto_Concepto_Ing
            ///DESCRIPCIÓN          : consulta para obtener los datos de los conceptos y subconceptos
            ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 07/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Concepto_Ing(Cls_Ope_Psp_Pronostico_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                    if (String.IsNullOrEmpty(Negocio.P_Programa_ID))
                    {
                        Mi_Sql.Append("SELECT " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + " + ' ' + ");
                        Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                        Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                        Mi_Sql.Append(" FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing
                            + " ON " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID 
                            + " = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clase_Ing_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo
                            + " ON " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Tipo_ID
                            + " = " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Estatus);
                        Mi_Sql.Append(" = 'ACTIVO'");

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_Sql.Append(" AND " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Anio);
                            Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_Ing_ID))
                        {
                            Mi_Sql.Append(" AND " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID
                                + " = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Clase_Ing_ID))
                        {
                            Mi_Sql.Append(" AND " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID
                                + " = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID))
                        {
                            Mi_Sql.Append(" AND " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Tipo_ID
                                + " = '" + Negocio.P_Tipo_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID))
                        {
                            Mi_Sql.Append(" AND " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Rubro_ID
                                + " = '" + Negocio.P_Rubro_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Descripcion.Trim()))
                        {
                            Mi_Sql.Append(" AND (" + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + " + ' ' + ");
                            Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_Descripcion.Trim() + "%'");
                            Mi_Sql.Append(" OR " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + " + ' ' + ");
                            Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_Descripcion.Trim().ToUpper() + "%'");
                            Mi_Sql.Append(" OR " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + " + ' ' + ");
                            Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_Descripcion.Trim().ToLower() + "%')");
                        }
                    }
                    else 
                    {
                        Mi_Sql.Append("SELECT " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + " + ' ' + ");
                        Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                        Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + ", ");
                        Mi_Sql.Append(Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Importe);
                        Mi_Sql.Append(" FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa);
                        Mi_Sql.Append(" ON " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Concepto_Ing_ID);
                        Mi_Sql.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Estatus);
                        Mi_Sql.Append(" = 'ACTIVO'");

                        if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                        {
                            Mi_Sql.Append(" AND " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Anio);
                            Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                        {
                            Mi_Sql.Append(" AND " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Proyecto_Programa_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_Ing_ID.Trim()))
                        {
                            Mi_Sql.Append(" AND " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Concepto_Ing_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Descripcion.Trim()))
                        {
                            Mi_Sql.Append(" AND (" + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + " + ' ' + ");
                            Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_Descripcion.Trim() + "%'");
                            Mi_Sql.Append(" OR " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + " + ' ' + ");
                            Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_Descripcion.Trim().ToUpper() + "%'");
                            Mi_Sql.Append(" OR " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + " + ' ' + ");
                            Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_Descripcion.Trim().ToLower() + "%')");
                        }
                    }

                    Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las conceptos  Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Registro_Mov_Autorizado
            ///DESCRIPCIÓN          : Metodo para insertar la poliza presupuestal de autorizado
            ///PARAMETROS           :
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 04/Abril/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static String[] Registro_Mov_Autorizado(SqlCommand Cmd, DataTable Dt_Registro, String Anio) 
            {
                String[] No_Poliza = null;
                DataTable Dt_Psp_Ing = new DataTable();
                DataRow Fila;

                try
                {
                    //creamos el datatable de los registros
                    Dt_Psp_Ing.Columns.Add("Fte_Financiamiento_ID", typeof(System.String));
                    Dt_Psp_Ing.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                    Dt_Psp_Ing.Columns.Add(Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID, typeof(System.String));
                    Dt_Psp_Ing.Columns.Add(Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID, typeof(System.String));
                    Dt_Psp_Ing.Columns.Add("Anio", typeof(System.String));
                    Dt_Psp_Ing.Columns.Add("Importe", typeof(System.String));

                    foreach (DataRow Dr in Dt_Registro.Rows)
                    {
                        Fila = Dt_Psp_Ing.NewRow();
                        Fila[0] = Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                        Fila[1] = Dr["PROGRAMA_ID"].ToString().Trim();
                        Fila[2] = Dr["CONCEPTO_ING_ID"].ToString().Trim();
                        Fila[3] = Dr["SUBCONCEPTO_ING_ID"].ToString().Trim();
                        Fila[4] = Anio.Trim();
                        Fila[5] = Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "");
                        Dt_Psp_Ing.Rows.Add(Fila);
                    }

                    No_Poliza = Cls_Ope_Con_Poliza_Ingresos_Datos.Alta_Movimientos_Presupuestales(Dt_Psp_Ing, Cmd, "Registro Autorización Pronostico Ingresos", "", "", "",
                    Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado, Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar);
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al guardar los datos del registro de movimiento. Error[" + Ex.Message + "]"); 
                }
                return No_Poliza;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_SubConcepto_Ing
            ///DESCRIPCIÓN          : consulta para obtener los datos de los subconceptos
            ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 07/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_SubConcepto_Ing(Cls_Ope_Psp_Pronostico_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                    Mi_Sql.Append("SELECT " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                    Mi_Sql.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                    Mi_Sql.Append(" FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                    Mi_Sql.Append(" ON " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Concepto_Ing_ID);
                    Mi_Sql.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing
                            + " ON " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID
                            + " = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clase_Ing_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo
                        + " ON " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Tipo_ID
                        + " = " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                    Mi_Sql.Append(" WHERE " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Estatus);
                    Mi_Sql.Append(" = 'ACTIVO'");

                    if (!String.IsNullOrEmpty(Negocio.P_Concepto_Ing_ID))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID
                            + " = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Clase_Ing_ID))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID
                            + " = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Tipo_ID
                            + " = '" + Negocio.P_Tipo_ID.Trim() + "'");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Rubro_ID
                            + " = '" + Negocio.P_Rubro_ID.Trim() + "'");
                    }


                    if (!String.IsNullOrEmpty(Negocio.P_SubConcepto_Ing_ID))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_SubConcepto_Ing_ID.Trim() + "'");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Anio))
                    {
                        
                        Mi_Sql.Append(" AND " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Anio);
                        Mi_Sql.Append(" = " + Negocio.P_Anio.Trim() + " ");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Descripcion.Trim()))
                    {
                        Mi_Sql.Append(" AND (" + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + " + ' ' + ");
                        Mi_Sql.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion);
                        Mi_Sql.Append(" LIKE '%" + Negocio.P_Descripcion.Trim() + "%'");
                        Mi_Sql.Append(" OR " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + " + ' ' + ");
                        Mi_Sql.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion);
                        Mi_Sql.Append(" LIKE '%" + Negocio.P_Descripcion.Trim().ToUpper() + "%')");
                    }

                    Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las conceptos y subconcepto. Error: [" + Ex.Message + "]");
                }
            }
        #endregion
    }
}