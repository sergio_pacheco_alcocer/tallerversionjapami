﻿using System;
using System.Data;
using System.Text;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Rpt_Psp_Economias.Negocio;

namespace JAPAMI.Rpt_Psp_Economias.Datos
{
    public class Cls_Rpt_Psp_Economias_Datos
    {
        #region(Metodos)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Anios
        ///DESCRIPCIÓN          : Obtiene datos de los años del estado analitico de ingresos
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 23/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Anios()
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
            DataSet Ds_Anios = new DataSet(); //Dataset donde obtendremos los datos de la consulta
            DataTable Dt_Anios = new DataTable();
            try
            {
                Mi_SQL.Append("SELECT DISTINCT " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                Mi_SQL.Append(" ORDER BY " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " DESC");


                Ds_Anios = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                if (Ds_Anios != null)
                {
                    Dt_Anios = Ds_Anios.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros de los años. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Anios;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_FF
        ///DESCRIPCIÓN          : Obtiene datos de  las fuentes de financiamiento 
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 23/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_FF(Cls_Rpt_Psp_Economias_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
            DataSet Ds_FF = new DataSet(); //Dataset donde obtendremos los datos de la consulta
            DataTable Dt_FF = new DataTable();
            try
            {
                Mi_SQL.Append("SELECT DISTINCT " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + ", ");
                Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS CLAVE_NOMBRE ");
                Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);

                if (!String.IsNullOrEmpty(Negocio.P_Anio))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio);
                    }
                    else 
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio);
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Dependencia_ID + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Dependencia_ID + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Programa_ID))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Programa_ID + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Programa_ID + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Partida_ID))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Partida_ID + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Partida_ID + "'");
                    }
                }

                Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");


                Ds_FF = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                if (Ds_FF != null)
                {
                    Dt_FF = Ds_FF.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros de las fuentes de fianciamiento. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_FF;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Programas
        ///DESCRIPCIÓN          : Obtiene datos de  los programas
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 23/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Programas(Cls_Rpt_Psp_Economias_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
            DataSet Ds_FF = new DataSet(); //Dataset donde obtendremos los datos de la consulta
            DataTable Dt_FF = new DataTable();
            try
            {
                Mi_SQL.Append("SELECT DISTINCT " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + ", ");
                Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS CLAVE_NOMBRE ");
                Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                Mi_SQL.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);

                if (!String.IsNullOrEmpty(Negocio.P_Anio))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio);
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio);
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Dependencia_ID + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Dependencia_ID + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_ID + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_ID + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Partida_ID))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Partida_ID + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Partida_ID + "'");
                    }
                }

                Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");


                Ds_FF = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                if (Ds_FF != null)
                {
                    Dt_FF = Ds_FF.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros de los programas. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_FF;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_FUR
        ///DESCRIPCIÓN          : Obtiene datos de  las unidades responsables
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 23/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_UR(Cls_Rpt_Psp_Economias_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
            DataSet Ds_FF = new DataSet(); //Dataset donde obtendremos los datos de la consulta
            DataTable Dt_FF = new DataTable();
            try
            {
                Mi_SQL.Append("SELECT DISTINCT " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID + ", ");
                Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " + ' ' + ");
                Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS CLAVE_NOMBRE ");
                Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                Mi_SQL.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                Mi_SQL.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);

                if (!String.IsNullOrEmpty(Negocio.P_Anio))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio);
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio);
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_ID + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_ID + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Partida_ID))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Partida_ID + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Partida_ID + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Programa_ID))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Programa_ID + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Programa_ID + "'");
                    }
                }

                Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");


                Ds_FF = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                if (Ds_FF != null)
                {
                    Dt_FF = Ds_FF.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros de las dependencias. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_FF;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Partida
        ///DESCRIPCIÓN          : Obtiene datos de  las partidas
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 23/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Partida(Cls_Rpt_Psp_Economias_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
            DataSet Ds_FF = new DataSet(); //Dataset donde obtendremos los datos de la consulta
            DataTable Dt_FF = new DataTable();
            try
            {
                Mi_SQL.Append("SELECT DISTINCT " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + ", ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " + ' ' + ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS CLAVE_NOMBRE ");
                Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);

                if (!String.IsNullOrEmpty(Negocio.P_Anio))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio);
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio);
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_ID + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_ID + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Programa_ID))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Programa_ID + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Programa_ID + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Dependencia_ID + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Dependencia_ID + "'");
                    }
                }

                Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");


                Ds_FF = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                if (Ds_FF != null)
                {
                    Dt_FF = Ds_FF.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros de las dependencias. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_FF;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Pronostico_Ingresos
        ///DESCRIPCIÓN          : Obtiene datos de  las unidades responsables del estado analitico de ingresos
        ///PARAMETROS           1 Negocio: conexion con la capa de negocios 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 23/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Economias(Cls_Rpt_Psp_Economias_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
            DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
            DataTable Dt = new DataTable();
            try
            {
                Mi_SQL.Append("SELECT " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS CLAVE_NOMBRE_FF, ");
                Mi_SQL.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID + ", ");
                Mi_SQL.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " + ' ' + ");
                Mi_SQL.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Descripcion + " AS CLAVE_NOMBRE_AF, ");
                Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS CLAVE_NOMBRE_PROGRAMA, ");
                Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " + ' ' + ");
                Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS CLAVE_NOMBRE_UR, ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " + ' ' + ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS CLAVE_NOMBRE_PARTIDA, ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + ", ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ", ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ", ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ", ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + " AS ENE, ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + " AS FEB, ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + " AS MAR, ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + " AS ABR, ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + " AS MAY, ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + " AS JUN, ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + " AS JUL, ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + " AS AGO, ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + " AS SEP, ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + " AS OCT, ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + " AS NOV, ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + " AS DIC, ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " AS TOT ");
                Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                Mi_SQL.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                Mi_SQL.Append(" ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                Mi_SQL.Append(" = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                Mi_SQL.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);

                if (!String.IsNullOrEmpty(Negocio.P_Anio))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Partida_ID))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Programa_ID))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                    }
                }

                Mi_SQL.Append(" ORDER BY ");
                Mi_SQL.Append("CLAVE_NOMBRE_FF, ");
                Mi_SQL.Append("CLAVE_NOMBRE_AF, ");
                Mi_SQL.Append("CLAVE_NOMBRE_PROGRAMA, ");
                Mi_SQL.Append("CLAVE_NOMBRE_UR, ");
                Mi_SQL.Append("CLAVE_NOMBRE_PARTIDA ASC");

                Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                if (Ds != null)
                {
                    Dt = Ds.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros de las economias. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt;
        }

        #endregion
    }
}
