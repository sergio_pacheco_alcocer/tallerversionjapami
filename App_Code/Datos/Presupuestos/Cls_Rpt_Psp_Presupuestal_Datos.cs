﻿using System;
using System.Data;
using System.Text;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Rpt_Psp_Presupuestal.Negocio;

namespace JAPAMI.Rpt_Psp_Presupuestal.Datos
{
    public class Cls_Rpt_Psp_Presupuestal_Datos
    {
        #region(Metodos)
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Anios
            ///DESCRIPCIÓN          : Obtiene datos de los años del estado analitico de ingresos
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 24/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Anios()
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds_Anios = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt_Anios = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT DISTINCT " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_SQL.Append(" ORDER BY " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " DESC");


                    Ds_Anios = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds_Anios != null)
                    {
                        Dt_Anios = Ds_Anios.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros de los años. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Anios;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Analitico_Ingresos
            ///DESCRIPCIÓN          : Obtiene datos del presupuesto de ingresos
            ///PARAMETROS           1 Negocio: conexion con la capa de negocios 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 24/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Analitico_Ingresos(Cls_Rpt_Psp_Presupuestal_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + " AS INDICE_CRI, ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + " AS CONCEPTO, ");
                    Mi_SQL.Append("SUM("+ Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ") AS LEY_DE_INGRESOS_ESTIMADO,");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ") AS AMPLIACION, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ") AS REDUCCION, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ") AS MODIFICADO, ");
                    Mi_SQL.Append("'0' AS CARGO, '0' AS ABONO, 'RUBRO' AS TIPO,");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ") AS SALDO_D, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ") AS RECAUDADO, ");
                    Mi_SQL.Append("'0' AS AVANCE_DE_RECAUDACION, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ") AS SALDO ");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                    Mi_SQL.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                    if (!String.IsNullOrEmpty(Negocio.P_Anio))
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                    }
                    Mi_SQL.Append(" GROUP BY " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + ", ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion);
                    Mi_SQL.Append(" UNION ");

                    Mi_SQL.Append("SELECT " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + " AS INDICE_CRI, ");
                    Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + " AS CONCEPTO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ") AS LEY_DE_INGRESOS_ESTIMADO,");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ") AS AMPLIACION, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ") AS REDUCCION, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ") AS MODIFICADO, ");
                    Mi_SQL.Append("'0' AS CARGO, '0' AS ABONO, 'TIPO' AS TIPO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ") AS SALDO_D, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ") AS RECAUDADO, ");
                    Mi_SQL.Append("'0' AS AVANCE_DE_RECAUDACION, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ") AS SALDO ");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo );
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                    Mi_SQL.Append(" = " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo   + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                    if (!String.IsNullOrEmpty(Negocio.P_Anio))
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                    }
                    Mi_SQL.Append(" GROUP BY " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + ", ");
                    Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion);
                    Mi_SQL.Append(" UNION ");

                    Mi_SQL.Append("SELECT '' AS INDICE_CRI, '' AS CONCEPTO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ") AS LEY_DE_INGRESOS_ESTIMADO,");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ") AS AMPLIACION, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ") AS REDUCCION, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ") AS MODIFICADO, ");
                    Mi_SQL.Append("'0' AS CARGO, '0' AS ABONO, 'TOTAL' AS TIPO,");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ") AS SALDO_D, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ") AS RECAUDADO, ");
                    Mi_SQL.Append("'0' AS AVANCE_DE_RECAUDACION, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ") AS SALDO ");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);

                    if (!String.IsNullOrEmpty(Negocio.P_Anio))
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    Mi_SQL.Append(" ORDER BY INDICE_CRI ASC");

                    Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds != null)
                    {
                        Dt = Ds.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros del estado analítico de ingresos. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Analitico_Ejercido_Psp_Egresos
            ///DESCRIPCIÓN          : Obtiene datos del presupuesto de ingresos
            ///PARAMETROS           1 Negocio: conexion con la capa de negocios 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 24/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Analitico_Ejercido_Psp_Egresos(Cls_Rpt_Psp_Presupuestal_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt = new DataTable();
                Double Abono = 0.00;
                Double Cargo = 0.00;
                Double Abono_D = 0.00;
                Double Cargo_D = 0.00;
                
                try
                {
                    Mi_SQL.Append("SELECT " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + " AS INDICE_COG, ");
                    Mi_SQL.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Descripcion + " AS CONCEPTO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ") AS APROBADO,");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ") AS AMPLIACION, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ") AS REDUCCION, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ") AS MODIFICADO, ");
                    Mi_SQL.Append("'0' AS CARGO_C, '0' AS ABONO_C,");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ") AS SALDO_C, ");
                    Mi_SQL.Append("'0' AS CARGO_D, '0' AS ABONO_D, 'CAPITULO' AS TIPO,");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ") AS SALDO_D, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + ") AS EJERCIDO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ") AS PAGADO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ") AS DISPONIBLE, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ") AS COMPROMETIDO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ") + ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ") AS SIN_DEVENGAR, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + ") + ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ") AS POR_PAGAR ");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID);
                    Mi_SQL.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                    if (!String.IsNullOrEmpty(Negocio.P_Anio))
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                    }
                    Mi_SQL.Append(" GROUP BY " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + ", ");
                    Mi_SQL.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Descripcion);
                    Mi_SQL.Append(" UNION ");

                    Mi_SQL.Append("SELECT " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave + " AS INDICE_COG, ");
                    Mi_SQL.Append(Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Descripcion + " AS CONCEPTO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ") AS APROBADO,");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ") AS AMPLIACION, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ") AS REDUCCION, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ") AS MODIFICADO, ");
                    Mi_SQL.Append("'0' AS CARGO_C, '0' AS ABONO_C,");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ") AS SALDO_C, ");
                    Mi_SQL.Append("'0' AS CARGO_D, '0' AS ABONO_D, 'CONCEPTO' AS TIPO,");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ") AS SALDO_D, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + ") AS EJERCIDO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ") AS PAGADO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ") AS DISPONIBLE, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ") AS COMPROMETIDO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ") + ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ") AS SIN_DEVENGAR, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + ") + ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ") AS POR_PAGAR ");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                    Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas);
                    Mi_SQL.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                    Mi_SQL.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                    Mi_SQL.Append(" ON " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID);
                    Mi_SQL.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                    if (!String.IsNullOrEmpty(Negocio.P_Anio))
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                    }
                    Mi_SQL.Append(" GROUP BY " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave + ", ");
                    Mi_SQL.Append(Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Descripcion);
                    Mi_SQL.Append(" UNION ");

                    Mi_SQL.Append("SELECT '' AS INDICE_COG, '' AS CONCEPTO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ") AS APROBADO,");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ") AS AMPLIACION, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ") AS REDUCCION, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ") AS MODIFICADO, ");
                    Mi_SQL.Append("'0' AS CARGO_C, '0' AS ABONO_C,");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ") AS SALDO_C, ");
                    Mi_SQL.Append("'0' AS CARGO_D, '0' AS ABONO_D, 'TOTAL' AS TIPO,");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ") AS SALDO_D, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + ") AS EJERCIDO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ") AS PAGADO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ") AS DISPONIBLE, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ") AS COMPROMETIDO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ") + ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ") AS SIN_DEVENGAR, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + ") + ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ") AS POR_PAGAR ");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    
                    if (!String.IsNullOrEmpty(Negocio.P_Anio))
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    Mi_SQL.Append(" ORDER BY INDICE_COG ASC");

                    Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds != null)
                    {
                        Dt = Ds.Tables[0];

                        if (Dt.Rows.Count > 0)
                        { 
                            foreach(DataRow Dr in Dt.Rows)
                            {
                                Cargo = 0.00;
                                Abono = 0.00;
                                Cargo_D = 0.00;
                                Abono_D = 0.00;
                                if (Dr["TIPO"].ToString().Trim().Equals("CAPITULO"))
                                {
                                    Mi_SQL = new StringBuilder();
                                    Mi_SQL.Append("SELECT ISNULL(SUM(" + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Importe + "),0)");
                                    Mi_SQL.Append(" FROM " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos);
                                    Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                                    Mi_SQL.Append(" ON " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_No_Reserva);
                                    Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                                    Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                                    Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                                    Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                                    Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID);
                                    Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica);
                                    Mi_SQL.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                                    Mi_SQL.Append(" = " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                                    Mi_SQL.Append(" ON " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Concepto_ID);
                                    Mi_SQL.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                                    Mi_SQL.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                                    Mi_SQL.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Abono + " = 'COMPROMETIDO'");
                                    Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio + " = " + Negocio.P_Anio.Trim());
                                    Mi_SQL.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave);
                                    Mi_SQL.Append(" = '" + Dr["INDICE_COG"].ToString().Trim() + "'");

                                    Abono = Convert.ToDouble(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()));

                                    Mi_SQL = new StringBuilder();
                                    Mi_SQL.Append("SELECT ISNULL(SUM(" + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Importe + "),0)");
                                    Mi_SQL.Append(" FROM " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos);
                                    Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                                    Mi_SQL.Append(" ON " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_No_Reserva);
                                    Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                                    Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                                    Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                                    Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                                    Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID);
                                    Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica);
                                    Mi_SQL.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                                    Mi_SQL.Append(" = " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                                    Mi_SQL.Append(" ON " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Concepto_ID);
                                    Mi_SQL.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                                    Mi_SQL.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                                    Mi_SQL.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Cargo + " = 'COMPROMETIDO'");
                                    Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio + " = " + Negocio.P_Anio.Trim());
                                    Mi_SQL.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave);
                                    Mi_SQL.Append(" = '" + Dr["INDICE_COG"].ToString().Trim() + "'");

                                    Cargo = Convert.ToDouble(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()));

                                    Mi_SQL = new StringBuilder();
                                    Mi_SQL.Append("SELECT ISNULL(SUM(" + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Importe + "),0)");
                                    Mi_SQL.Append(" FROM " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos);
                                    Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                                    Mi_SQL.Append(" ON " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_No_Reserva);
                                    Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                                    Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                                    Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                                    Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                                    Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID);
                                    Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica);
                                    Mi_SQL.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                                    Mi_SQL.Append(" = " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                                    Mi_SQL.Append(" ON " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Concepto_ID);
                                    Mi_SQL.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                                    Mi_SQL.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                                    Mi_SQL.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Abono + " = 'DEVENGADO'");
                                    Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio + " = " + Negocio.P_Anio.Trim());
                                    Mi_SQL.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave);
                                    Mi_SQL.Append(" = '" + Dr["INDICE_COG"].ToString().Trim() + "'");

                                    Abono_D = Convert.ToDouble(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()));

                                    Mi_SQL = new StringBuilder();
                                    Mi_SQL.Append("SELECT ISNULL(SUM(" + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Importe + "),0)");
                                    Mi_SQL.Append(" FROM " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos);
                                    Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                                    Mi_SQL.Append(" ON " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_No_Reserva);
                                    Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                                    Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                                    Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                                    Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                                    Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID);
                                    Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica);
                                    Mi_SQL.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                                    Mi_SQL.Append(" = " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                                    Mi_SQL.Append(" ON " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Concepto_ID);
                                    Mi_SQL.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                                    Mi_SQL.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                                    Mi_SQL.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Cargo + " = 'DEVENGADO'");
                                    Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio + " = " + Negocio.P_Anio.Trim());
                                    Mi_SQL.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave);
                                    Mi_SQL.Append(" = '" + Dr["INDICE_COG"].ToString().Trim() + "'");

                                    Cargo_D = Convert.ToDouble(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()));

                                    Dr["ABONO_C"] = Abono.ToString().Trim();
                                    Dr["CARGO_C"] = Cargo.ToString().Trim();
                                    Dr["SALDO_C"] = Cargo - Abono;
                                    Dr["ABONO_D"] = Abono_D.ToString().Trim();
                                    Dr["CARGO_D"] = Cargo_D.ToString().Trim();
                                    Dr["SALDO_D"] = Cargo_D - Abono_D;
                                }
                                else 
                                {
                                    Mi_SQL = new StringBuilder();
                                    Mi_SQL.Append("SELECT ISNULL(SUM(" + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Importe + "),0)");
                                    Mi_SQL.Append(" FROM " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos);
                                    Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                                    Mi_SQL.Append(" ON " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_No_Reserva);
                                    Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                                    Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                                    Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                                    Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                                    Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID);
                                    Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica);
                                    Mi_SQL.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                                    Mi_SQL.Append(" = " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                                    Mi_SQL.Append(" ON " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Concepto_ID);
                                    Mi_SQL.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Abono + " = 'COMPROMETIDO'");
                                    Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio + " = " + Negocio.P_Anio.Trim());
                                    Mi_SQL.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave);
                                    Mi_SQL.Append(" = '" + Dr["INDICE_COG"].ToString().Trim() + "'");

                                    Abono = Convert.ToDouble(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()));

                                    Mi_SQL = new StringBuilder();
                                    Mi_SQL.Append("SELECT ISNULL(SUM(" + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Importe + "),0)");
                                    Mi_SQL.Append(" FROM " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos);
                                    Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                                    Mi_SQL.Append(" ON " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_No_Reserva);
                                    Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                                    Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                                    Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                                    Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                                    Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID);
                                    Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica);
                                    Mi_SQL.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                                    Mi_SQL.Append(" = " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                                    Mi_SQL.Append(" ON " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Concepto_ID);
                                    Mi_SQL.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Cargo + " = 'COMPROMETIDO'");
                                    Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio + " = " + Negocio.P_Anio.Trim());
                                    Mi_SQL.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave);
                                    Mi_SQL.Append(" = '" + Dr["INDICE_COG"].ToString().Trim() + "'");

                                    Cargo = Convert.ToDouble(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()));

                                    Mi_SQL = new StringBuilder();
                                    Mi_SQL.Append("SELECT ISNULL(SUM(" + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Importe + "),0)");
                                    Mi_SQL.Append(" FROM " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos);
                                    Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                                    Mi_SQL.Append(" ON " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_No_Reserva);
                                    Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                                    Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                                    Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                                    Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                                    Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID);
                                    Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica);
                                    Mi_SQL.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                                    Mi_SQL.Append(" = " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                                    Mi_SQL.Append(" ON " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Concepto_ID);
                                    Mi_SQL.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Abono + " = 'DEVENGADO'");
                                    Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio + " = " + Negocio.P_Anio.Trim());
                                    Mi_SQL.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave);
                                    Mi_SQL.Append(" = '" + Dr["INDICE_COG"].ToString().Trim() + "'");

                                    Abono_D = Convert.ToDouble(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()));

                                    Mi_SQL = new StringBuilder();
                                    Mi_SQL.Append("SELECT ISNULL(SUM(" + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Importe + "),0)");
                                    Mi_SQL.Append(" FROM " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos);
                                    Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                                    Mi_SQL.Append(" ON " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_No_Reserva);
                                    Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                                    Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                                    Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                                    Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                                    Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID);
                                    Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica);
                                    Mi_SQL.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                                    Mi_SQL.Append(" = " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID);
                                    Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                                    Mi_SQL.Append(" ON " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Concepto_ID);
                                    Mi_SQL.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Cargo + " = 'DEVENGADO'");
                                    Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio + " = " + Negocio.P_Anio.Trim());
                                    Mi_SQL.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave);
                                    Mi_SQL.Append(" = '" + Dr["INDICE_COG"].ToString().Trim() + "'");

                                    Cargo_D = Convert.ToDouble(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()));

                                    Dr["ABONO_C"] = Abono.ToString().Trim();
                                    Dr["CARGO_C"] = Cargo.ToString().Trim();
                                    Dr["SALDO_C"] = Cargo - Abono;
                                    Dr["ABONO_D"] = Abono_D.ToString().Trim();
                                    Dr["CARGO_D"] = Cargo_D.ToString().Trim();
                                    Dr["SALDO_D"] = Cargo_D - Abono_D;
                                }
                            }
                        }
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros del estado analítico de ingresos. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Analitico_Ejercido_Psp_Egresos_COG
            ///DESCRIPCIÓN          : Obtiene datos del presupuesto de ingresos
            ///PARAMETROS           1 Negocio: conexion con la capa de negocios 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 24/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Analitico_Ejercido_Psp_Egresos_COG(Cls_Rpt_Psp_Presupuestal_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt = new DataTable();
                Double Cargo = 0.00;
                Double Abono = 0.00;
                Double Cargo_D = 0.00;
                Double Abono_D = 0.00;
                try
                {
                    Mi_SQL.Append("SELECT " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " AS PARTIDA, ");
                    Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS DES_PARTIDA, ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS FF, ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS DES_FF, ");
                    Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " AS UR, ");
                    Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS DES_UR, ");
                    Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " AS PP, ");
                    Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS DES_PP, ");
                    Mi_SQL.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " AS AF, ");
                    Mi_SQL.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Descripcion + " AS DES_AF, ");
                    Mi_SQL.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID+ ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + " AS APROBADO, ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + " AS AMPLIACION, ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + " AS REDUCCION, ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + " AS MODIFICADO, ");
                    Mi_SQL.Append("'0' AS CARGO_C, '0' AS ABONO_C,");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + " AS SALDO_C, ");
                    Mi_SQL.Append("'0' AS CARGO_D, '0' AS ABONO_D, 'PARTIDA' AS TIPO,");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + " AS SALDO_D, ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + " AS EJERCIDO, ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + " AS PAGADO, ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " AS DISPONIBLE, ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + " AS COMPROMETIDO, ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " + ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + " AS SIN_DEVENGAR, ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + " + ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + " AS POR_PAGAR ");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                    Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                    Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                    Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                    Mi_SQL.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                    Mi_SQL.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                    Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                    Mi_SQL.Append(" ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                    Mi_SQL.Append(" = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);

                    if (!String.IsNullOrEmpty(Negocio.P_Anio))
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                    }


                    Mi_SQL.Append(" ORDER BY AF, PP, UR, FF, PARTIDA ASC");

                    Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds != null)
                    {
                        Dt = Ds.Tables[0];

                        if (Dt.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt.Rows)
                            {
                                Cargo = 0.00;
                                Abono = 0.00;
                                Cargo_D = 0.00;
                                Abono_D = 0.00;
                               
                                Mi_SQL = new StringBuilder();
                                Mi_SQL.Append("SELECT ISNULL(SUM(" + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Importe + "),0)");
                                Mi_SQL.Append(" FROM " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos);
                                Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                                Mi_SQL.Append(" ON " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_No_Reserva);
                                Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                                Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                                Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                                Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                                Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                                Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID);
                                Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Abono + " = 'COMPROMETIDO'");
                                Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio + " = " + Negocio.P_Anio.Trim());
                                Mi_SQL.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave);
                                Mi_SQL.Append(" = '" + Dr["PARTIDA"].ToString().Trim() + "'");

                                Abono = Convert.ToDouble(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()));

                                Mi_SQL = new StringBuilder();
                                Mi_SQL.Append("SELECT ISNULL(SUM(" + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Importe + "),0)");
                                Mi_SQL.Append(" FROM " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos);
                                Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                                Mi_SQL.Append(" ON " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_No_Reserva);
                                Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                                Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                                Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                                Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                                Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                                Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID);
                                Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Cargo + " = 'COMPROMETIDO'");
                                Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio + " = " + Negocio.P_Anio.Trim());
                                Mi_SQL.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave);
                                Mi_SQL.Append(" = '" + Dr["PARTIDA"].ToString().Trim() + "'");

                                Cargo = Convert.ToDouble(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()));

                                Mi_SQL = new StringBuilder();
                                Mi_SQL.Append("SELECT ISNULL(SUM(" + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Importe + "),0)");
                                Mi_SQL.Append(" FROM " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos);
                                Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                                Mi_SQL.Append(" ON " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_No_Reserva);
                                Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                                Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                                Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                                Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                                Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                                Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID);
                                Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Abono + " = 'DEVENGADO'");
                                Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio + " = " + Negocio.P_Anio.Trim());
                                Mi_SQL.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave);
                                Mi_SQL.Append(" = '" + Dr["PARTIDA"].ToString().Trim() + "'");

                                Abono_D = Convert.ToDouble(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()));

                                Mi_SQL = new StringBuilder();
                                Mi_SQL.Append("SELECT ISNULL(SUM(" + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Importe + "),0)");
                                Mi_SQL.Append(" FROM " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos);
                                Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                                Mi_SQL.Append(" ON " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_No_Reserva);
                                Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                                Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                                Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                                Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                                Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                                Mi_SQL.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID);
                                Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "." + Ope_Psp_Registro_Movimientos.Campo_Cargo + " = 'DEVENGADO'");
                                Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio + " = " + Negocio.P_Anio.Trim());
                                Mi_SQL.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave);
                                Mi_SQL.Append(" = '" + Dr["PARTIDA"].ToString().Trim() + "'");

                                Cargo_D = Convert.ToDouble(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()));

                                Dr["ABONO_C"] = Abono.ToString().Trim();
                                Dr["CARGO_C"] = Cargo.ToString().Trim();
                                Dr["SALDO_C"] = Cargo - Abono;
                                Dr["ABONO_D"] = Abono_D.ToString().Trim();
                                Dr["CARGO_D"] = Cargo_D.ToString().Trim();
                                Dr["SALDO_D"] = Cargo_D - Abono_D;
                            }
                        }
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros del estado analítico de ingresos. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt;
            }

        #endregion
    }
}
