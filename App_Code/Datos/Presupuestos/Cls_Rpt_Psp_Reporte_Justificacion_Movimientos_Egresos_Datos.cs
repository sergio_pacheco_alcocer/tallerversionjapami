﻿using System;
using System.Data;
using System.Text;
using JAPAMI.Constantes;
using JAPAMI.Reporte_Justificacion_Movimientos_Egresos.Negocio;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Cls_Rpt_Psp_Reporte_Justificacion_Movimientos_Egresos_Datos
/// </summary>
namespace JAPAMI.Reporte_Justificacion_Movimientos_Egresos.Datos
{
    public class Cls_Rpt_Psp_Reporte_Justificacion_Movimientos_Egresos_Datos
    {
        ///****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Reporte_Mov_Egresos
        ///DESCRIPCIÓN          : Metodo para generar el reporte de movimientos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 31/Julio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*****************************************************************************************************************
        internal static DataTable Reporte_Mov_Egresos(Cls_Rpt_Psp_Reporte_Justificacion_Movimientos_Egresos_Negocio Negocio) 
        {
            DataTable Dt_Datos_Consulta = new DataTable();
            DataSet Ds_Datos_Consulta = new DataSet();
            StringBuilder Mi_Sql = new StringBuilder();

            try
            {
                Mi_Sql.Append(" SELECT LTRIM(RTRIM(FF." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ")) AS FUENTE, " );
                Mi_Sql.Append(" LTRIM(RTRIM(AF." + Cat_SAP_Area_Funcional.Campo_Clave + ")) AS AREA, ");
                Mi_Sql.Append(" LTRIM(RTRIM(PP." + Cat_Sap_Proyectos_Programas.Campo_Clave + ")) AS PROGRAMA, ");
                Mi_Sql.Append(" LTRIM(RTRIM(UR." + Cat_Dependencias.Campo_Clave + ")) AS DEPENDENCIA, ");
                Mi_Sql.Append(" LTRIM(RTRIM(P." + Cat_Sap_Partidas_Especificas.Campo_Clave + ")) AS PARTIDA, ");
                Mi_Sql.Append(" DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion + ", ");
                Mi_Sql.Append(" DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Justificacion + ", ");
                Mi_Sql.Append("(CASE WHEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion + " = 'TRASPASO' ");
                Mi_Sql.Append(" AND DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + " = 'Destino' THEN 'AUMENTO - TRASPASO' ");
                Mi_Sql.Append(" WHEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion + " = 'TRASPASO' ");
                Mi_Sql.Append(" AND DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + " = 'Origen' THEN 'DISMINUCION - TRASPASO' ");
                Mi_Sql.Append(" WHEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion + " = 'AMPLIACION' ");
                Mi_Sql.Append(" AND DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + " = 'Origen' THEN 'AUMENTO' ");
                Mi_Sql.Append(" WHEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion + " = 'REDUCCION' ");
                Mi_Sql.Append(" AND DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + " = 'Origen' THEN 'DISMINUCION' ");
                Mi_Sql.Append(" END) AS TIPO_MOVIMIENTO,");
                Mi_Sql.Append(" (CASE WHEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + " = 'Destino' ");
                Mi_Sql.Append(" THEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Enero);
                Mi_Sql.Append(" ELSE - DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Enero + " END) AS IMPORTE_ENERO, ");
                Mi_Sql.Append(" (CASE WHEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + " = 'Destino' ");
                Mi_Sql.Append(" THEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Febrero);
                Mi_Sql.Append(" ELSE - DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Febrero + " END) AS IMPORTE_FEBRERO, ");
                Mi_Sql.Append(" (CASE WHEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + " = 'Destino' ");
                Mi_Sql.Append(" THEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Marzo);
                Mi_Sql.Append(" ELSE - DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Marzo + " END) AS IMPORTE_MARZO, ");
                Mi_Sql.Append(" (CASE WHEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + " = 'Destino' ");
                Mi_Sql.Append(" THEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Abril);
                Mi_Sql.Append(" ELSE - DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Abril + " END) AS IMPORTE_ABRIL, ");
                Mi_Sql.Append(" (CASE WHEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + " = 'Destino' ");
                Mi_Sql.Append(" THEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Mayo);
                Mi_Sql.Append(" ELSE - DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Mayo + " END) AS IMPORTE_MAYO, ");
                Mi_Sql.Append(" (CASE WHEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + " = 'Destino' ");
                Mi_Sql.Append(" THEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Junio);
                Mi_Sql.Append(" ELSE - DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Junio + " END) AS IMPORTE_JUNIO, ");
                Mi_Sql.Append(" (CASE WHEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + " = 'Destino' ");
                Mi_Sql.Append(" THEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Julio);
                Mi_Sql.Append(" ELSE - DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Julio + " END) AS IMPORTE_JULIO, ");
                Mi_Sql.Append(" (CASE WHEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + " = 'Destino' ");
                Mi_Sql.Append(" THEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Agosto);
                Mi_Sql.Append(" ELSE - DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Agosto + " END) AS IMPORTE_AGOSTO, ");
                Mi_Sql.Append(" (CASE WHEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + " = 'Destino' ");
                Mi_Sql.Append(" THEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Septiembre);
                Mi_Sql.Append(" ELSE - DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Septiembre + " END) AS IMPORTE_SEPTIEMBRE, ");
                Mi_Sql.Append(" (CASE WHEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + " = 'Destino' ");
                Mi_Sql.Append(" THEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Octubre);
                Mi_Sql.Append(" ELSE - DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Octubre + " END) AS IMPORTE_OCTUBRE, ");
                Mi_Sql.Append(" (CASE WHEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + " = 'Destino' ");
                Mi_Sql.Append(" THEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Noviembre);
                Mi_Sql.Append(" ELSE - DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Noviembre + " END) AS IMPORTE_NOVIEMBRE, ");
                Mi_Sql.Append(" (CASE WHEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + " = 'Destino' ");
                Mi_Sql.Append(" THEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Diciembre);
                Mi_Sql.Append(" ELSE - DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Diciembre + " END) AS IMPORTE_DICIEMBRE, ");
                Mi_Sql.Append(" (CASE WHEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + " = 'Destino' ");
                Mi_Sql.Append(" THEN DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Total);
                Mi_Sql.Append(" ELSE - DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Total + " END) AS IMPORTE_TOTAL ");
                Mi_Sql.Append(" FROM " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + " DET ");
                Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " FF ");
                Mi_Sql.Append(" ON DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Fuente_Financiamiento_ID);
                Mi_Sql.Append(" = FF." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + " AF ");
                Mi_Sql.Append(" ON DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Area_Funcional_ID);
                Mi_Sql.Append(" = AF." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " PP ");
                Mi_Sql.Append(" ON DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Proyecto_Programa_ID);
                Mi_Sql.Append(" = PP." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " UR ");
                Mi_Sql.Append(" ON DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Dependencia_ID);
                Mi_Sql.Append(" = UR." + Cat_Dependencias.Campo_Dependencia_ID);
                Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " P ");
                Mi_Sql.Append(" ON DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Partida_ID);
                Mi_Sql.Append(" = P." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                Mi_Sql.Append(" WHERE DET." + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + " = " + Negocio.No_Movimiento.Trim());
                Mi_Sql.Append(" AND DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Anio + " = " + Negocio.Anio.Trim());
                Mi_Sql.Append(" AND DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + " IN ('ACEPTADO', 'AUTORIZADO', 'RECIBIDO')");
                Mi_Sql.Append(" ORDER BY DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " ASC, ");
                Mi_Sql.Append(" DET." + Ope_Psp_Movimiento_Egr_Det.Campo_Movimiento_ID + " ASC ");

                Ds_Datos_Consulta = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());

                if (Ds_Datos_Consulta != null)
                {
                    Dt_Datos_Consulta = Ds_Datos_Consulta.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar el reporte de justificación de movimientos de egresos Error.[" + Ex.Message + "]");
            }

            return Dt_Datos_Consulta;
        }

        ///****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Reporte_Mov_Ingresos
        ///DESCRIPCIÓN          : Metodo para generar el reporte de movimientos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 01/Agosto/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*****************************************************************************************************************
        internal static DataTable Reporte_Mov_Ingresos(Cls_Rpt_Psp_Reporte_Justificacion_Movimientos_Egresos_Negocio Negocio) 
        {
            DataTable Dt_Datos_Consulta = new DataTable();
            DataSet Ds_Datos_Consulta = new DataSet();
            StringBuilder Mi_Sql = new StringBuilder();

            try
            {
                Mi_Sql.Append("SELECT LTRIM(RTRIM(FF." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ")) AS FUENTE,");
                Mi_Sql.Append(" LTRIM(RTRIM(RUB." + Cat_Psp_Rubro.Campo_Clave + ")) AS RUBRO,");
                Mi_Sql.Append(" LTRIM(RTRIM(TIP." + Cat_Psp_Tipo.Campo_Clave + ")) AS TIPO, ");
                Mi_Sql.Append(" LTRIM(RTRIM(CLA." + Cat_Psp_Clase_Ing.Campo_Clave + ")) AS CLASE, ");
                Mi_Sql.Append(" LTRIM(RTRIM(CON." + Cat_Psp_Concepto_Ing.Campo_Clave + ")) + ' ' +  ");
                Mi_Sql.Append(" CON." + Cat_Psp_Concepto_Ing.Campo_Descripcion + " AS CONCEPTO, ");
                Mi_Sql.Append(" LTRIM(RTRIM(SUBCON." + Cat_Psp_SubConcepto_Ing.Campo_Clave + ")) + ' ' +  ");
                Mi_Sql.Append(" SUBCON." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion + " AS SUBCONCEPTO, ");
                Mi_Sql.Append("(CASE WHEN DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Movimiento + " = 'AMPLIACION' THEN");
                Mi_Sql.Append(" 'AUMENTO' ELSE 'DISMINUCION' END) AS TIPO_MOVIMIENTO, ");
                Mi_Sql.Append("(CASE WHEN DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Movimiento + " = 'AMPLIACION' THEN");
                Mi_Sql.Append(" DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Enero + " ELSE ");
                Mi_Sql.Append(" - " + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Enero + " END) AS IMPORTE_ENERO, ");
                Mi_Sql.Append("(CASE WHEN DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Movimiento + " = 'AMPLIACION' THEN");
                Mi_Sql.Append(" DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Febrero + " ELSE ");
                Mi_Sql.Append(" - " + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Febrero + " END) AS IMPORTE_FEBRERO, ");
                Mi_Sql.Append("(CASE WHEN DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Movimiento + " = 'AMPLIACION' THEN");
                Mi_Sql.Append(" DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Marzo + " ELSE ");
                Mi_Sql.Append(" - " + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Marzo + " END) AS IMPORTE_MARZO, ");
                Mi_Sql.Append("(CASE WHEN DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Movimiento + " = 'AMPLIACION' THEN");
                Mi_Sql.Append(" DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Abril + " ELSE ");
                Mi_Sql.Append(" - " + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Abril + " END) AS IMPORTE_ABRIL, ");
                Mi_Sql.Append("(CASE WHEN DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Movimiento + " = 'AMPLIACION' THEN");
                Mi_Sql.Append(" DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Mayo + " ELSE ");
                Mi_Sql.Append(" - " + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Mayo + " END) AS IMPORTE_MAYO, ");
                Mi_Sql.Append("(CASE WHEN DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Movimiento + " = 'AMPLIACION' THEN");
                Mi_Sql.Append(" DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Junio + " ELSE ");
                Mi_Sql.Append(" - " + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Junio + " END) AS IMPORTE_JUNIO, ");
                Mi_Sql.Append("(CASE WHEN DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Movimiento + " = 'AMPLIACION' THEN");
                Mi_Sql.Append(" DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Julio + " ELSE ");
                Mi_Sql.Append(" - " + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Julio + " END) AS IMPORTE_JULIO, ");
                Mi_Sql.Append("(CASE WHEN DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Movimiento + " = 'AMPLIACION' THEN");
                Mi_Sql.Append(" DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Agosto + " ELSE ");
                Mi_Sql.Append(" - " + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Agosto + " END) AS IMPORTE_AGOSTO, ");
                Mi_Sql.Append("(CASE WHEN DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Movimiento + " = 'AMPLIACION' THEN");
                Mi_Sql.Append(" DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Septiembre + " ELSE ");
                Mi_Sql.Append(" - " + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Septiembre + " END) AS IMPORTE_SEPTIEMBRE, ");
                Mi_Sql.Append("(CASE WHEN DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Movimiento + " = 'AMPLIACION' THEN");
                Mi_Sql.Append(" DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Octubre + " ELSE ");
                Mi_Sql.Append(" - " + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Octubre + " END) AS IMPORTE_OCTUBRE, ");
                Mi_Sql.Append("(CASE WHEN DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Movimiento + " = 'AMPLIACION' THEN");
                Mi_Sql.Append(" DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Noviembre + " ELSE ");
                Mi_Sql.Append(" - " + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Noviembre + " END) AS IMPORTE_NOVIEMBRE, ");
                Mi_Sql.Append("(CASE WHEN DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Movimiento + " = 'AMPLIACION' THEN");
                Mi_Sql.Append(" DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Diciembre + " ELSE ");
                Mi_Sql.Append(" - " + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Diciembre + " END) AS IMPORTE_DICIEMBRE, ");
                Mi_Sql.Append("(CASE WHEN DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Movimiento + " = 'AMPLIACION' THEN");
                Mi_Sql.Append(" DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Total + " ELSE ");
                Mi_Sql.Append(" - " + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Total + " END) AS IMPORTE_TOTAL, ");
                Mi_Sql.Append(" DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Justificacion);
                Mi_Sql.Append(" FROM " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + " DET ");
                Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " FF ");
                Mi_Sql.Append(" ON DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Fuente_Financiamiento_ID);
                Mi_Sql.Append(" = FF." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " RUB ");
                Mi_Sql.Append(" ON DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Rubro_ID);
                Mi_Sql.Append(" = RUB." + Cat_Psp_Rubro.Campo_Rubro_ID);
                Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " TIP ");
                Mi_Sql.Append(" ON DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_ID);
                Mi_Sql.Append(" = TIP." + Cat_Psp_Tipo.Campo_Tipo_ID);
                Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " CLA ");
                Mi_Sql.Append(" ON DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Clase_Ing_ID);
                Mi_Sql.Append(" = CLA." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing  + " CON ");
                Mi_Sql.Append(" ON DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Clase_Ing_ID);
                Mi_Sql.Append(" = CON." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                Mi_Sql.Append(" INNER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " SUBCON ");
                Mi_Sql.Append(" ON DET." + Ope_Psp_Movimiento_Ing_Det.Campo_SubConcepto_Ing_ID);
                Mi_Sql.Append(" = SUBCON." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                Mi_Sql.Append(" WHERE DET." + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing + " = " + Negocio.No_Movimiento);
                Mi_Sql.Append(" AND  DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Anio + " = " + Negocio.Anio);
                Mi_Sql.Append(" AND DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Estatus + " IN ('ACEPTADO', 'AUTORIZADO')");
                Mi_Sql.Append(" ORDER BY DET." + Ope_Psp_Movimiento_Ing_Det.Campo_Movimiento_Ing_ID + " ASC ");

                Ds_Datos_Consulta = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());

                if (Ds_Datos_Consulta != null)
                {
                    Dt_Datos_Consulta = Ds_Datos_Consulta.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar el reporte de justificación de movimientos de ingresos Error.[" + Ex.Message + "]");
            }
            return Dt_Datos_Consulta;
        }
    }
}
