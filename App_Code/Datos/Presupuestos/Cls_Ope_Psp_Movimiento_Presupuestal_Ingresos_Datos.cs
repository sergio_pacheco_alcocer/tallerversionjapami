﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Collections.Generic;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Movimiento_Presupuestal_Ingresos.Negocio;
using JAPAMI.Sessiones;
using JAPAMI.Manejo_Presupuesto.Datos;


namespace JAPAMI.Movimiento_Presupuestal_Ingresos.Datos
{
    public class Cls_Ope_Psp_Movimiento_Presupuestal_Ingresos_Datos
    {
        #region(Metodos)
        /// ********************************************************************************************************************
        /// NOMBRE: Alta_Movimiento
        /// 
        /// COMENTARIOS: Esta operación inserta un nuevo registro de un movimiento presupuestal en la tabla de OPE_COM_SOLICITUD_TRANSF
        /// 
        /// PARÁMETROS: Datos.- Valor de los campos a insertar en la tabla de OPE_COM_SOLICITUD_TRANSF 
        /// 
        /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ: 18/Octubre/2011 
        /// USUARIO MODIFICO: Leslie Gonzalez Vazquez
        /// FECHA MODIFICO: 22/marzo/2012
        /// CAUSA DE LA MODIFICACIÓN:se acomodo la insercion de los movimientos
        /// ********************************************************************************************************************
        public static Boolean Alta_Movimiento(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Datos)
        {

            StringBuilder MI_SQL = new StringBuilder();//Variable que almacenara la consulta.
            SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion;//Variable para la conexión para la base de datos   
            SqlCommand Comando;//Sirve para la ejecución de las operaciones a la base de datos
            Object No_Solicitud_Max;//Identificador el elemento de la busque (cual es el mayor id de solicitud)
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            Boolean Operacion_Completa = false;
            String Tipo_Presupuesto = String.Empty;
            DataTable Dt_Meses = new DataTable();

            Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Comando = new SqlCommand();
            Conexion.Open();
            Transaccion = Conexion.BeginTransaction();
            Comando.Transaction = Transaccion;
            Comando.Connection = Conexion;

            String Mi_SQL = "";
            Int32 NO_SOLICITUD_DETALLE;
            String Anio = String.Format("{0:yyyy}", DateTime.Now);

            try
            {
                Dt_Meses = Datos.P_Meses;

                //CHECAMOS SI ES UNA PARTIDA EXISTENTE O NUEVA Y SI ES NUEVA LA INSERTAMOS EN EL PRESUPUESTO APROBADO
                if(Datos.P_Tipo_Partida.Trim().Equals("Nueva"))
                {
                    Mi_SQL = "INSERT INTO " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado;
                    Mi_SQL += "(" + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Anio + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Saldo + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Creo + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Creo + ", ";
                    Mi_SQL += " ACTUALIZADO) VALUES( ";

                    if (Datos.P_Tipo_Operacion.Trim().Equals("TRASPASO"))
                    {
                        Mi_SQL += "'" + Datos.P_Destino_Dependencia_Id + "', ";
                        Mi_SQL += "'" + Datos.P_Destino_Fuente_Financiamiento_Id + "', ";
                        Mi_SQL += "'" + Datos.P_Destino_Programa_Id + "', ";
                        Mi_SQL += "'" + Datos.P_Capitulo_ID + "', ";
                        Mi_SQL += "'" + Datos.P_Destino_Partida_Id + "', ";
                    }
                    else
                    {
                        Mi_SQL += "'" + Datos.P_Origen_Dependencia_Id + "', ";
                        Mi_SQL += "'" + Datos.P_Origen_Fuente_Financiamiento_Id + "', ";
                        Mi_SQL += "'" + Datos.P_Origen_Programa_Id + "', ";
                        Mi_SQL += "'" + Datos.P_Capitulo_ID + "', ";
                        Mi_SQL += "'" + Datos.P_Origen_Partida_Id + "', ";
                    }
                    Mi_SQL += String.Format("{0:yyyy}", DateTime.Now) + ", ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "'" + Cls_Sessiones.Nombre_Empleado + "', ";
                    Mi_SQL += "GETDATE(), ";
                    Mi_SQL += "'NO') ";

                    //Ejecutar consulta
                    Comando.CommandText = Mi_SQL.Trim();
                    Comando.ExecuteNonQuery();
                }
                
                MI_SQL = new StringBuilder();
                MI_SQL.Append("SELECT ISNULL(MAX (" + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + "),0) ");
                MI_SQL.Append("FROM " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf);
                No_Solicitud_Max = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, MI_SQL.ToString());

                if (Convert.IsDBNull(No_Solicitud_Max) == false)
                {

                    Datos.P_No_Solicitud = String.Format("{0:00000}", Convert.ToInt32(No_Solicitud_Max) + 1);
                }
                else
                {
                    Datos.P_No_Solicitud = "00001";
                }
                MI_SQL = new StringBuilder();
                //Da de Alta los datos del Nuevo Parametro con los datos proporcionados por el usuario.
                MI_SQL.Append("INSERT INTO " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + " (");
                MI_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + ", ");
                MI_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Codigo1 + ", ");
                MI_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Codigo2 + ", ");
                MI_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Importe + ", ");
                MI_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Justificacion + ", ");
                MI_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Estatus + ", ");
                MI_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Anio + ", ");
                MI_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Usuario_Creo + ", ");
                MI_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Fecha_Creo + ", ");
                MI_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Tipo_Operacion + ", ");
                MI_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Area_Funcional_Id + ", ");
                MI_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Programa_Id + ", ");
                MI_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Partida_Id + ", ");
                MI_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Dependencia_Id + ", ");
                MI_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Area_Funcional_Id + ", ");
                MI_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Programa_Id + ", ");
                MI_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Partida_Id + ", ");
                MI_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Dependencia_Id + ", ");
                MI_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Fuente_Financiamiento_Id + ", ");
                MI_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Fuente_Financiamiento_Id + ") ");
                MI_SQL.Append(" VALUES (" + Datos.P_No_Solicitud + ", ");
                MI_SQL.Append("'" + Datos.P_Codigo_Programatico_De + "', ");
                MI_SQL.Append("'" + Datos.P_Codigo_Programatico_Al + "', ");
                MI_SQL.Append(Datos.P_Monto + ", ");
                MI_SQL.Append("'" + Datos.P_Justificacion + "', ");
                MI_SQL.Append("'" + Datos.P_Estatus + "',");
                MI_SQL.Append("'" + Anio + "',");
                MI_SQL.Append("'" + Datos.P_Usuario_Creo + "',");
                MI_SQL.Append("GETDATE(), ");
                MI_SQL.Append("'" + Datos.P_Tipo_Operacion + "',");
                MI_SQL.Append("'" + Datos.P_Origen_Area_Funcional_Id + "',");
                MI_SQL.Append("'" + Datos.P_Origen_Programa_Id + "',");
                MI_SQL.Append("'" + Datos.P_Origen_Partida_Id + "',");
                MI_SQL.Append("'" + Datos.P_Origen_Dependencia_Id + "',");
                MI_SQL.Append("'" + Datos.P_Destino_Area_Funcional_Id + "',");
                MI_SQL.Append("'" + Datos.P_Destino_Programa_Id + "',");
                MI_SQL.Append("'" + Datos.P_Destino_Partida_Id + "',");
                MI_SQL.Append("'" + Datos.P_Destino_Dependencia_Id + "', ");
                MI_SQL.Append("'" + Datos.P_Origen_Fuente_Financiamiento_Id + "',");
                MI_SQL.Append("'" + Datos.P_Destino_Fuente_Financiamiento_Id + "')");
                //Ejecutar consulta
                Comando.CommandText = MI_SQL.ToString();
                Comando.ExecuteNonQuery();

                //INSERTAMOS LOS DETALLES DE LA SOLICITUD CON LOS IMPORTES DE LOS MESES
                NO_SOLICITUD_DETALLE = Obtener_Consecutivo(Ope_Com_Solicitud_Transf_Det.Campo_No_Solicitud_Detalle, Ope_Com_Solicitud_Transf_Det.Tabla_Ope_Psp_Hist_Calendar_Presu);

                if (Dt_Meses != null)
                {
                    if (Dt_Meses.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Meses.Rows)
                        {
                            MI_SQL = new StringBuilder();
                            MI_SQL.Append("INSERT INTO " + Ope_Com_Solicitud_Transf_Det.Tabla_Ope_Psp_Hist_Calendar_Presu + "(");
                            MI_SQL.Append(Ope_Com_Solicitud_Transf_Det.Campo_No_Solicitud_Detalle + ", ");
                            MI_SQL.Append(Ope_Com_Solicitud_Transf_Det.Campo_No_Solicitud + ", ");
                            MI_SQL.Append(Ope_Com_Solicitud_Transf_Det.Campo_Importe_Enero + ", ");
                            MI_SQL.Append(Ope_Com_Solicitud_Transf_Det.Campo_Importe_Febrero + ", ");
                            MI_SQL.Append(Ope_Com_Solicitud_Transf_Det.Campo_Importe_Marzo + ", ");
                            MI_SQL.Append(Ope_Com_Solicitud_Transf_Det.Campo_Importe_Abril + ", ");
                            MI_SQL.Append(Ope_Com_Solicitud_Transf_Det.Campo_Importe_Mayo + ", ");
                            MI_SQL.Append(Ope_Com_Solicitud_Transf_Det.Campo_Importe_Junio + ", ");
                            MI_SQL.Append(Ope_Com_Solicitud_Transf_Det.Campo_Importe_Julio + ", ");
                            MI_SQL.Append(Ope_Com_Solicitud_Transf_Det.Campo_Importe_Agosto + ", ");
                            MI_SQL.Append(Ope_Com_Solicitud_Transf_Det.Campo_Importe_Septiembre + ", ");
                            MI_SQL.Append(Ope_Com_Solicitud_Transf_Det.Campo_Importe_Octubre + ", ");
                            MI_SQL.Append(Ope_Com_Solicitud_Transf_Det.Campo_Importe_Noviembre + ", ");
                            MI_SQL.Append(Ope_Com_Solicitud_Transf_Det.Campo_Importe_Diciembre + ", ");
                            MI_SQL.Append(Ope_Com_Solicitud_Transf_Det.Campo_Tipo + ") VALUES(");
                            MI_SQL.Append(NO_SOLICITUD_DETALLE + ", ");
                            MI_SQL.Append(Datos.P_No_Solicitud + ", ");
                            MI_SQL.Append(Dr["ENERO"].ToString().Trim() + ", ");
                            MI_SQL.Append(Dr["FEBRERO"].ToString().Trim() + ", ");
                            MI_SQL.Append(Dr["MARZO"].ToString().Trim() + ", ");
                            MI_SQL.Append(Dr["ABRIL"].ToString().Trim() + ", ");
                            MI_SQL.Append(Dr["MAYO"].ToString().Trim() + ", ");
                            MI_SQL.Append(Dr["JUNIO"].ToString().Trim() + ", ");
                            MI_SQL.Append(Dr["JULIO"].ToString().Trim() + ", ");
                            MI_SQL.Append(Dr["AGOSTO"].ToString().Trim() + ", ");
                            MI_SQL.Append(Dr["SEPTIEMBRE"].ToString().Trim() + ", ");
                            MI_SQL.Append(Dr["OCTUBRE"].ToString().Trim() + ", ");
                            MI_SQL.Append(Dr["NOVIEMBRE"].ToString().Trim() + ", ");
                            MI_SQL.Append(Dr["DICIEMBRE"].ToString().Trim() + ", ");
                            MI_SQL.Append("'" + Dr["TIPO"].ToString().Trim() + "')");

                            Comando.CommandText = MI_SQL.ToString();
                            Comando.ExecuteNonQuery();

                            NO_SOLICITUD_DETALLE ++;
                        }
                    }
                }

                Transaccion.Commit();
                Operacion_Completa = true;
            }
            catch (SqlException Ex)
            {
                if (Transaccion != null)
                {
                    Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Conexion.Close();
                Comando = null;
                Conexion = null;
                Transaccion = null;
            }
            return Operacion_Completa;
        }

        /// ********************************************************************************************************************
        /// NOMBRE: Alta_Comentario
        /// 
        /// COMENTARIOS: Esta operación inserta un nuevo registro de un comentario presupuestal en la tabla de Ope_Psp_Cierre_Presup
        /// 
        /// PARÁMETROS: Datos.- Valor de los campos a insertar en la tabla de 
        /// 
        /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ: 17/Noviembre/2011 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static Boolean Alta_Comentario(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Datos)
        {

            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion;//Variable para la conexión para la base de datos   
            SqlCommand Comando;//Sirve para la ejecución de las operaciones a la base de datos
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            Boolean Operacion_Completa = false;
            StringBuilder MI_SQL = new StringBuilder();
            string Tipo_Presupuesto = string.Empty;
            Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Comando = new SqlCommand();
            Conexion.Open();
            Transaccion = Conexion.BeginTransaction();
            Comando.Transaction = Transaccion;
            Comando.Connection = Conexion;

            try
            {

                Mi_SQL.Append("UPDATE " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + " SET ");
                Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Estatus + "='" + Datos.P_Estatus + "', ");
                Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Creo + "', ");
                Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Fecha_Modifico + "=GETDATE() ");
                Mi_SQL.Append("WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + " = " + Datos.P_No_Solicitud);

                Comando.CommandText = Mi_SQL.ToString();
                Comando.ExecuteNonQuery();

                if (Datos.P_Estatus.Trim().Equals("RECHAZADA"))
                {
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("INSERT INTO " + Ope_Psp_Comentarios_Mov.Tabla_Ope_Psp_Comentarios_Mov + " (");
                    Mi_SQL.Append(Ope_Psp_Comentarios_Mov.Campo_Numero_Solicitud + ", ");
                    Mi_SQL.Append(Ope_Psp_Comentarios_Mov.Campo_Comentario + ", ");
                    Mi_SQL.Append(Ope_Psp_Comentarios_Mov.Campo_Fecha + ", ");
                    Mi_SQL.Append(Ope_Psp_Comentarios_Mov.Campo_Usuario_Creo + ", ");
                    Mi_SQL.Append(Ope_Psp_Comentarios_Mov.Campo_Fecha_Creo + ") ");
                    Mi_SQL.Append("VALUES (" + Datos.P_No_Solicitud + ", ");
                    Mi_SQL.Append("'" + Datos.P_Comentario + "', ");
                    Mi_SQL.Append("GETDATE() , ");
                    Mi_SQL.Append("'" + Datos.P_Usuario_Creo + "', ");
                    Mi_SQL.Append("GETDATE() )");

                    Comando.CommandText = Mi_SQL.ToString();
                    Comando.ExecuteNonQuery();

                //    if (Datos.P_Tipo_Operacion.Trim().Equals("TRASPASO"))
                //    {
                //        //consultamos el tipo de presupuesto
                //        MI_SQL = new StringBuilder();

                //        MI_SQL.Append("SELECT " + Cat_Parametros_Ejercer_Psp.Campo_Tipo_De_Consulta);
                //        MI_SQL.Append(" FROM " + Cat_Parametros_Ejercer_Psp.Tabla_Cat_Parametros_Ejercer_Psp);
                //        MI_SQL.Append(" WHERE " + Cat_Parametros_Ejercer_Psp.Campo_Anio + " = '" + Datos.P_Anio + "'");

                //        Tipo_Presupuesto = (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, MI_SQL.ToString());

                //        if (Tipo_Presupuesto.Trim().Equals("ANUAL"))
                //        {
                //            //MODIFICAMOS LOS MOMENTOS PRESUPUESTALES DEL PRESUPUESTO APROBADO DE LA PARTIDA
                //            MI_SQL = new StringBuilder();

                //            MI_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                //            MI_SQL.Append(" SET COMPROMETIDO = COMPROMETIDO - " + Datos.P_Monto.Replace(",", "") + ", ");
                //            MI_SQL.Append(" DISPONIBLE = DISPONIBLE + " + Datos.P_Monto.Replace(",", "") + ", ");
                //            MI_SQL.Append(" IMPORTE_" + Datos.P_Mes_Origen.Trim() + " = IMPORTE_" + Datos.P_Mes_Origen.Trim() + " + " + Datos.P_Monto.Replace(",", ""));
                //            MI_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Datos.P_Origen_Dependencia_Id.Trim() + "'");
                //            MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Datos.P_Origen_Fuente_Financiamiento_Id.Trim() + "'");
                //            MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Datos.P_Origen_Programa_Id.Trim() + "'");
                //            MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Datos.P_Origen_Partida_Id.Trim() + "'");
                //            MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Datos.P_Anio.ToString());

                //            Comando.CommandText = MI_SQL.ToString();
                //            Comando.ExecuteNonQuery();
                //        }
                //        else
                //        {
                //            if (Datos.P_Mes_Actual.Trim().Equals(Datos.P_Mes_Origen.Trim()))
                //            {
                //                //MODIFICAMOS LOS MOMENTOS PRESUPUESTALES DEL PRESUPUESTO APROBADO DE LA PARTIDA
                //                MI_SQL = new StringBuilder();

                //                MI_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                //                MI_SQL.Append(" SET COMPROMETIDO = COMPROMETIDO - " + Datos.P_Monto.Replace(",", "") + ", ");
                //                MI_SQL.Append(" DISPONIBLE = DISPONIBLE + " + Datos.P_Monto.Replace(",", "") + ", ");
                //                MI_SQL.Append(" IMPORTE_" + Datos.P_Mes_Origen.Trim() + " = IMPORTE_" + Datos.P_Mes_Origen.Trim() + " + " + Datos.P_Monto.Replace(",", ""));
                //                MI_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Datos.P_Origen_Dependencia_Id.Trim() + "'");
                //                MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Datos.P_Origen_Fuente_Financiamiento_Id.Trim() + "'");
                //                MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Datos.P_Origen_Programa_Id.Trim() + "'");
                //                MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Datos.P_Origen_Partida_Id.Trim() + "'");
                //                MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Datos.P_Anio.ToString());

                //                Comando.CommandText = MI_SQL.ToString();
                //                Comando.ExecuteNonQuery();
                //            }
                //            else
                //            {
                //                //MODIFICAMOS LOS MOMENTOS PRESUPUESTALES DEL PRESUPUESTO APROBADO DE LA PARTIDA
                //                MI_SQL = new StringBuilder();

                //                MI_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                //                MI_SQL.Append(" SET COMPROMETIDO = COMPROMETIDO - " + Datos.P_Monto.Replace(",", "") + ", ");
                //                MI_SQL.Append(" IMPORTE_" + Datos.P_Mes_Origen.Trim() + " = IMPORTE_" + Datos.P_Mes_Origen.Trim() + " + " + Datos.P_Monto.Replace(",", ""));
                //                MI_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Datos.P_Origen_Dependencia_Id.Trim() + "'");
                //                MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Datos.P_Origen_Fuente_Financiamiento_Id.Trim() + "'");
                //                MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Datos.P_Origen_Programa_Id.Trim() + "'");
                //                MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Datos.P_Origen_Partida_Id.Trim() + "'");
                //                MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Datos.P_Anio.ToString());

                //                Comando.CommandText = MI_SQL.ToString();
                //                Comando.ExecuteNonQuery();
                //            }
                //        }
                //    }
               }

                

                Transaccion.Commit();
                Conexion.Close();
                Operacion_Completa = true;
            }
            catch (SqlException Ex)
            {
                if (Transaccion != null)
                {
                    Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Conexion.Close();
                Comando = null;
                Conexion = null;
                Transaccion = null;
            }
            return Operacion_Completa;
        }

        /// ********************************************************************************************************************
        /// NOMBRE: Modificar_Movimiento
        /// 
        /// COMENTARIOS: Esta operación actualiza un registro del movimiento en la tabla de OPE_COM_SOLICITUD_TRANSF
        /// 
        /// PARÁMETROS: Datos.- Valor de los campos a Modificar en la tabla de OPE_COM_SOLICITUD_TRANSF 
        /// 
        /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ:  18/Octubre/2011 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static Boolean Modificar_Movimiento(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion;//Variable para la conexión para la base de datos   
            SqlCommand Comando;//Sirve para la ejecución de las operaciones a la base de datos
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            Boolean Operacion_Completa = false;

            Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Comando = new SqlCommand();
            Conexion.Open();
            Transaccion = Conexion.BeginTransaction();
            Comando.Transaction = Transaccion;
            Comando.Connection = Conexion;
            try
            {
                Mi_SQL.Append("UPDATE " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + " SET ");
                Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Codigo1 + "='" + Datos.P_Codigo_Programatico_De + "',");
                Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Codigo2 + "='" + Datos.P_Codigo_Programatico_Al + "', ");
                Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Importe + "=" + Datos.P_Monto + ", ");
                Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Justificacion + "='" + Datos.P_Justificacion + "', ");
                Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Estatus + "='" + Datos.P_Estatus + "', ");
                Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Tipo_Operacion + "='" + Datos.P_Tipo_Operacion + "', ");
                Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Creo + "', ");
                Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Area_Funcional_Id + "='" + Datos.P_Origen_Area_Funcional_Id + "', ");
                Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Dependencia_Id + "='" + Datos.P_Origen_Dependencia_Id + "', ");
                Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Fuente_Financiamiento_Id + "='" + Datos.P_Origen_Fuente_Financiamiento_Id + "', ");
                Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Partida_Id + "='" + Datos.P_Origen_Partida_Id  + "', ");
                Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Programa_Id + "='" + Datos.P_Origen_Programa_Id + "', ");
                Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Area_Funcional_Id + "='" + Datos.P_Destino_Area_Funcional_Id + "', ");
                Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Dependencia_Id  + "='" + Datos.P_Destino_Dependencia_Id + "', ");
                Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Fuente_Financiamiento_Id + "='" + Datos.P_Destino_Fuente_Financiamiento_Id + "', ");
                Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Partida_Id + "='" + Datos.P_Destino_Partida_Id + "', ");
                Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Programa_Id + "='" + Datos.P_Destino_Programa_Id + "', ");
                Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Fecha_Modifico + "=GETDATE() ");
                Mi_SQL.Append("WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + "=" + Datos.P_No_Solicitud);

                //Ejecutar consulta
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                Conexion.Close();
                Operacion_Completa = true;
            }
            catch (SqlException Ex)
            {
                if (Transaccion != null)
                {
                    Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }
                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Conexion.Close();
                Comando = null;
                Conexion = null;
                Transaccion = null;
            }
            return Operacion_Completa;
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Consecutivo
        ///DESCRIPCIÓN: Obtiene el numero consecutivo para las tablas ocupadas en esta clase
        ///PARAMETROS: 1.-Campo del cual se obtendra el consecutivo
        ///            2.-Nombre de la tabla
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 10/Enero/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Obtener_Consecutivo(String Campo_ID, String Tabla)
        {
            int Consecutivo = 0;
            String Mi_Sql;
            Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos
            Mi_Sql = "SELECT ISNULL(MAX (" + Campo_ID + "),'00000') FROM " + Tabla;
            Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            Consecutivo = (Convert.ToInt32(Obj) + 1);
            return Consecutivo;
        }

        /// ********************************************************************************************************************
        /// NOMBRE: Eliminar_Movimiento
        /// 
        /// COMENTARIOS: Esta operación eliminara un registro del movimiento que se haya realizado en la tabla de OPE_COM_SOLICITUD_TRANSF
        /// 
        /// PARÁMETROS: Datos.- Valor de los campos a eliminar en la tabla de OPE_COM_SOLICITUD_TRANSF
        /// 
        /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ:  14/Octubre/2011 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static Boolean Eliminar_Movimiento(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion;//Variable para la conexión para la base de datos   
            SqlCommand Comando;//Sirve para la ejecución de las operaciones a la base de datos
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            Boolean Operacion_Completa = false;

            Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Comando = new SqlCommand();
            Conexion.Open();
            Transaccion = Conexion.BeginTransaction();
            Comando.Transaction = Transaccion;
            Comando.Connection = Conexion;
            
            try
            {
                

                Mi_SQL.Append("Delete From " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + " ");
                Mi_SQL.Append("where " + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + "=" + Datos.P_No_Solicitud);
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
            }
            catch (SqlException Ex)
            {
                if (Transaccion != null)
                {
                    Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Conexion.Close();
                Comando = null;
                Conexion = null;
                Transaccion = null;
            }
            return Operacion_Completa;
        }

        /// ********************************************************************************************************************
        /// NOMBRE: Consultar_Movimiento
        /// 
        /// COMENTARIOS: Consulta el movimiento presupuestal que se haya llevado en la tabla OPE_COM_SOLICITUD_TRANSF  
        /// 
        /// PARÁMETROS: Datos.- Valor de los campos a consultar en la tabla de OPE_COM_SOLICITUD_TRANSF
        /// 
        /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ:  14/Octubre/2011 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consultar_Movimiento(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            DataTable Dt_Movimiento = new DataTable();
            String Anio = String.Format("{0:yyyy}", DateTime.Now);

            try
            {
                Mi_SQL.Append("Select " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + ".* ");
                Mi_SQL.Append("From " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf);

                if (!string.IsNullOrEmpty(Datos.P_No_Solicitud))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        //no llevan comilla simple es entero el numero de solicitud
                        Mi_SQL.Append(" OR " + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + "=" + Datos.P_No_Solicitud + "");
                    }
                     else
                    {
                        Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + "=" + Datos.P_No_Solicitud + "");
                    }
                }
                if (!string.IsNullOrEmpty(Datos.P_Monto))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" OR " + Cat_Ope_Com_Solicitud_Transf.Campo_Importe + "=" + Datos.P_Monto + "");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_Importe + "=" + Datos.P_Monto + "");
                    }
                }
                if (!string.IsNullOrEmpty(Datos.P_Estatus))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" OR " + Cat_Ope_Com_Solicitud_Transf.Campo_Estatus + "='" + Datos.P_Estatus + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_Estatus + "='" + Datos.P_Estatus + "'");
                    }
                }
                else 
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Cat_Ope_Com_Solicitud_Transf.Campo_Estatus + " IN ('AUTORIZADO','RECHAZADO','GENERADO')");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_Estatus + " IN ('AUTORIZADO','RECHAZADO','GENERADO')");
                    }

                }

                if (Mi_SQL.ToString().Contains("WHERE"))
                {
                    Mi_SQL.Append(" AND " + Cat_Ope_Com_Solicitud_Transf.Campo_Anio+ "='" + Anio + "'");
                }
                else
                {
                    Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_Anio + "='" + Datos.P_Anio + "'");
                }

                
                Mi_SQL.Append(" Order by " + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + " asc");
                Dt_Movimiento = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
               
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
            return Dt_Movimiento;
        }

        /// ********************************************************************************************************************
        /// NOMBRE: Consulta_Movimiento_Fecha
        /// 
        /// COMENTARIOS: Consulta el movimiento presupuestal que se haya llevado en la tabla OPE_COM_SOLICITUD_TRANSF por fecha  
        /// 
        /// PARÁMETROS: Datos.- Valor de los campos a consultar en la tabla de OPE_COM_SOLICITUD_TRANSF
        /// 
        /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ:  18/Noviembre/2011 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consulta_Movimiento_Fecha(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            DataTable Dt_Movimiento = new DataTable();
            try
            {
                Mi_SQL.Append("Select " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + ".* ");
                Mi_SQL.Append("From " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf);


                if ((!string.IsNullOrEmpty(Datos.P_Fecha_Inicio)) && (string.IsNullOrEmpty(Datos.P_Fecha_Final)))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Cat_Ope_Com_Solicitud_Transf .Campo_Fecha_Creo);
                        Mi_SQL.Append(" BETWEEN TO_DATE ('" + Datos.P_Fecha_Inicio + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS')");
                        Mi_SQL.Append(" AND TO_DATE('" + Datos.P_Fecha_Inicio + " 23:59:00', 'DD/MM/YYYY HH24:MI:SS')");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_Fecha_Creo);
                        Mi_SQL.Append(" BETWEEN TO_DATE ('" + Datos.P_Fecha_Inicio + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS')");
                        Mi_SQL.Append(" AND TO_DATE('" + Datos.P_Fecha_Inicio + " 23:59:00', 'DD/MM/YYYY HH24:MI:SS')");
                    }
                }
                else
                {
                    if ((!string.IsNullOrEmpty(Datos.P_Fecha_Inicio)) && !string.IsNullOrEmpty(Datos.P_Fecha_Final))
                    {
                        if (Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" AND " + Cat_Ope_Com_Solicitud_Transf.Campo_Fecha_Creo);
                            Mi_SQL.Append(" BETWEEN TO_DATE ('" + Datos.P_Fecha_Inicio + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS')");
                            Mi_SQL.Append(" AND TO_DATE('" + Datos.P_Fecha_Final + " 23:59:00', 'DD/MM/YYYY HH24:MI:SS')");
                        }
                        else
                        {
                            Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_Fecha_Creo);
                            Mi_SQL.Append(" BETWEEN TO_DATE ('" + Datos.P_Fecha_Inicio + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS')");
                            Mi_SQL.Append(" AND TO_DATE('" + Datos.P_Fecha_Final + " 23:59:00', 'DD/MM/YYYY HH24:MI:SS')");
                        }
                    }
                }

                if (!string.IsNullOrEmpty(Datos.P_Responsable))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Dependencia_Id);
                        Mi_SQL.Append(" = '" + Datos.P_Responsable + "'");
                        Mi_SQL.Append(" OR " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Dependencia_Id);
                        Mi_SQL.Append(" = '" + Datos.P_Responsable + "'");

                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Dependencia_Id);
                        Mi_SQL.Append(" = '" + Datos.P_Responsable + "'");
                        Mi_SQL.Append(" OR " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Dependencia_Id);
                        Mi_SQL.Append(" = '" + Datos.P_Responsable + "'");

                    }
                }

                if (Mi_SQL.ToString().Contains("WHERE"))
                {
                    Mi_SQL.Append(" AND " + Cat_Ope_Com_Solicitud_Transf.Campo_Estatus + " IN ('AUTORIZADO','RECHAZADO','GENERADO')");
                }
                else
                {
                    Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_Estatus + " IN ('AUTORIZADO','RECHAZADO','GENERADO')");
                }

                Mi_SQL.Append(" Order by " + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + " asc");
                Dt_Movimiento = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
               
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
            return Dt_Movimiento;
        }
        /// ********************************************************************************************************************
        /// NOMBRE: Consulta_Movimiento_Btn_Busqueda
        /// 
        /// COMENTARIOS: Consulta el movimiento presupuestal que se haya llevado en la tabla OPE_COM_SOLICITUD_TRANSF por fecha  
        /// 
        /// PARÁMETROS: Datos.- Valor de los campos a consultar en la tabla de OPE_COM_SOLICITUD_TRANSF
        /// 
        /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ:  18/Noviembre/2011 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consulta_Movimiento_Btn_Busqueda(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            DataTable Dt_Movimiento = new DataTable();
            try
            {
                Mi_SQL.Append("Select " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + ".* ");
                Mi_SQL.Append("From " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf);


                if (!string.IsNullOrEmpty(Datos.P_No_Solicitud))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        //no llevan comilla simple es entero el numero de solicitud
                        Mi_SQL.Append(" AND " + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + "=" + Datos.P_No_Solicitud + " ");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + "=" + Datos.P_No_Solicitud + " ");
                    }
                }

                if (!string.IsNullOrEmpty(Datos.P_Responsable))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Dependencia_Id);
                        Mi_SQL.Append(" = '" + Datos.P_Responsable + "'");
                        Mi_SQL.Append(" OR " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Dependencia_Id);
                        Mi_SQL.Append(" = '" + Datos.P_Responsable + "'");

                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Dependencia_Id);
                        Mi_SQL.Append(" = '" + Datos.P_Responsable + "'");
                        Mi_SQL.Append(" OR " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Dependencia_Id);
                        Mi_SQL.Append(" = '" + Datos.P_Responsable + "'");

                    }
                }

                if (Mi_SQL.ToString().Contains("WHERE"))
                {
                    Mi_SQL.Append(" AND " + Cat_Ope_Com_Solicitud_Transf.Campo_Estatus + " IN ('AUTORIZADO','RECHAZADO','GENERADO')");
                }
                else
                {
                    Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_Estatus + " IN ('AUTORIZADO','RECHAZADO','GENERADO')");
                }

                Mi_SQL.Append(" Order by " + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + " asc");
                Dt_Movimiento = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
            return Dt_Movimiento;
        }
        /// ********************************************************************************************************************
        /// NOMBRE: Consultar_programa
        /// 
        /// COMENTARIOS: Consulta el programa al que pertenece de la tabla CAT_SAP_PROYECTOS_PROGRAMAS  
        /// 
        /// PARÁMETROS: Datos.- Valor de los campos a consultar en la tabla de CAT_SAP_PROYECTOS_PROGRAMAS 
        /// 
        /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ:  14/Octubre/2011 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consultar_Programa(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Datos)
        {
            DataTable Dt_Consulta_Programa = new DataTable();
            StringBuilder Mi_SQL;
            try
            {
                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("Select *");
                Mi_SQL.Append(" from " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);


                if (!string.IsNullOrEmpty(Datos.P_Programa))
                {
                    Mi_SQL.Append(" where " + Cat_Sap_Proyectos_Programas.Campo_Clave + "='" + Datos.P_Programa + "'");
                }
                Dt_Consulta_Programa = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception("Error: [" + ex.Message + "]");
            }
            return Dt_Consulta_Programa;
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Area_Funcional
        /// DESCRIPCION : Consulta la clave en la  CAT_SAP_AREA_FUNCIONAL  
        /// PARAMETROS  :  Datos.- Valor de los campos a insertar en la tabla de CAT_SAP_AREA_FUNCIONAL
        /// CREO        : Hugo Enrique Ramirez Aguilera 
        /// FECHA_CREO  : 14-Octubre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Area_Funcional(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Datos)
        {
            StringBuilder Mi_SQL;
            try
            {
                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("Select *");
                Mi_SQL.Append(" From " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                if (!string.IsNullOrEmpty(Datos.P_Area_Funcional))
                {
                    Mi_SQL.Append(" where " + Cat_SAP_Area_Funcional.Campo_Clave + "='" + Datos.P_Area_Funcional + "'");
                }
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Like_Movimiento
        /// DESCRIPCION : Consulta la clave que contenga algun valor paresido en el registro en la  CAT_SAP_AREA_FUNCIONAL  
        /// PARAMETROS  :  Datos.- Valor de los campos a insertar en la tabla de OPE_COM_SOLICITUD_TRANSF
        /// CREO        : Hugo Enrique Ramirez Aguilera 
        /// FECHA_CREO  : 29-Octubre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Like_Movimiento(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            try
            {
               
                Mi_SQL.Append("SELECT * ");
                Mi_SQL.Append("FROM " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf +" ");
                if(!String.IsNullOrEmpty(Datos.P_Responsable.Trim()))
                {
                    Mi_SQL.Append("WHERE " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Dependencia_Id);
                    Mi_SQL.Append(" = '" + Datos.P_Responsable + "'");
                    Mi_SQL.Append(" OR " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Dependencia_Id);
                    Mi_SQL.Append(" = '" + Datos.P_Responsable + "'");
                }

                if (Mi_SQL.ToString().Contains("WHERE"))
                {
                    Mi_SQL.Append(" AND " + Cat_Ope_Com_Solicitud_Transf.Campo_Estatus + " IN ('AUTORIZADO','RECHAZADO','GENERADO')");
                }
                else
                {
                    Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_Estatus + " IN ('AUTORIZADO','RECHAZADO','GENERADO')");
                }

                Mi_SQL.Append(" ORDER BY " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + " DESC");
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Dependencia_Ordenada
        /// DESCRIPCION : Consulta la clave que contenga algun valor paresido en el registro en la  CAT_SAP_AREA_FUNCIONAL  
        /// PARAMETROS  :  Datos.- Valor de los campos a insertar en la tabla de CAT_DEPENDENCIA    
        /// CREO        : Hugo Enrique Ramirez Aguilera 
        /// FECHA_CREO  : 29-Octubre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Dependencia_Ordenada(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Datos)
        {
            StringBuilder Mi_SQL= new StringBuilder();
            try
            {
                Mi_SQL.Append("Select distinct " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " ");
                Mi_SQL.Append("from " + Cat_Dependencias.Tabla_Cat_Dependencias + " ");
                Mi_SQL.Append("Order by " + Cat_Dependencias.Campo_Clave + " ASC");
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Datos_Partidas
        /// DESCRIPCION : Consulta la clave que contenga algun valor paresido en el registro en la  CAT_SAP_AREA_FUNCIONAL  
        /// PARAMETROS  :  Datos.- Valor de los campos a insertar en la tabla de CAT_DEPENDENCIA    
        /// CREO        : Hugo Enrique Ramirez Aguilera 
        /// FECHA_CREO  : 29-Octubre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Datos_Partidas(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Datos)
         {
            StringBuilder Mi_SQL = new StringBuilder(); //Variable para la consulta SQL

            try
            {
                Mi_SQL.Append("SELECT * ");
                Mi_SQL.Append(" FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas);

                if (!string.IsNullOrEmpty(Datos.P_Partida))
                {
                    Mi_SQL.Append(" where " + Cat_Com_Partidas.Campo_Clave + " like '" + Datos.P_Partida + "%'");
                }

                Mi_SQL.Append("Order by " + Cat_Com_Partidas.Campo_Clave + " ASC");
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error: [" + Ex.Message + "]");
            }
        }
         ///*******************************************************************************
         /// NOMBRE DE LA FUNCION: Consulta_Datos_Comentarios
         /// DESCRIPCION : Consulta la clave que contenga algun valor paresido en el registro en la 
         ///                tabla Ope_Psp_Comentarios_Mov
         /// PARAMETROS  :  Datos.- Valor de los campos a insertar en la tabla de CAT_DEPENDENCIA    
         /// CREO        : Hugo Enrique Ramirez Aguilera 
         /// FECHA_CREO  : 17-noviembre-2011
         /// MODIFICO          :
         /// FECHA_MODIFICO    :
         /// CAUSA_MODIFICACION:
         ///*******************************************************************************
         public static DataTable Consulta_Datos_Comentarios(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Datos)
         {
             StringBuilder Mi_SQL = new StringBuilder(); //Variable para la consulta SQL

             try
             {
                 Mi_SQL.Append("SELECT * ");
                 Mi_SQL.Append(" FROM " + Ope_Psp_Comentarios_Mov.Tabla_Ope_Psp_Comentarios_Mov);

                 if (!string.IsNullOrEmpty(Datos.P_No_Solicitud))
                 {
                     Mi_SQL.Append(" where " + Ope_Psp_Comentarios_Mov.Campo_Numero_Solicitud + "=" + Datos.P_No_Solicitud);
                 }
                  Mi_SQL.Append(" Order by " + Ope_Psp_Comentarios_Mov.Campo_Fecha +" DESC");
                 
                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

             }
             catch (Exception Ex)
             {
                 throw new Exception("Error: [" + Ex.Message + "]");
             }
         }

        #endregion

        #region Metodos_Aprobado
         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Fuente_Financiamiento
         ///DESCRIPCIÓN          : consulta para obtener los datos de las fuentes de financiamiento
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 30/Noviembre/2011
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         internal static DataTable Consultar_Fuente_Financiamiento(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocio)
         {
             StringBuilder Mi_Sql = new StringBuilder();
             try
             {
                 Mi_Sql.Append("SELECT DISTINCT " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " +' '+ ");
                 Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS nombre, ");
                 Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " AS id ");
                 Mi_Sql.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                 Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                 Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                 Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                 Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                 Mi_Sql.Append(" = '" + Negocio.P_Anio + "'");
                 

                 if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID_Busqueda.Trim()))
                 {
                     Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                     Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                     Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                     Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                     Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID_Busqueda + "'");
                 }

                 Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                
                 Mi_Sql.Append(" ORDER BY nombre ASC");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error al intentar consultar los registros de las fuentes de financiamiento. Error: [" + Ex.Message + "]");
             }
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Programas
         ///DESCRIPCIÓN          : consulta para obtener los datos de los programas
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 30/Noviembre/2011
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         internal static DataTable Consultar_Programas_Aprobados(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocio)
         {
             StringBuilder Mi_Sql = new StringBuilder();
             try
             {
                 Mi_Sql.Append("SELECT DISTINCT " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " +' '+ ");
                 Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS nombre, ");
                 Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " AS Programa_id ");
                 Mi_Sql.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                 Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                 Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                 Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                 Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                 Mi_Sql.Append(" = '" + Negocio.P_Anio + "'");
                 Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                 Mi_Sql.Append(" = '" + Negocio.P_Fuente_Financiamiento_ID + "'");
                 Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                 Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                 Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                 Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                 Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID + "'");
                 Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                 Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID_Busqueda + "'");

                 Mi_Sql.Append(" ORDER BY nombre ASC");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error al intentar consultar los registros de los programas. Error: [" + Ex.Message + "]");
             }
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Partida_Especifica
         ///DESCRIPCIÓN          : consulta para obtener los datos de las partidas especificas
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 30/Noviembre/2011
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         internal static DataTable Consultar_Partida_Especifica(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocio)
         {
             StringBuilder Mi_Sql = new StringBuilder();
             try
             {
                 Mi_Sql.Append("SELECT " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " +' '+ ");
                 Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Descripcion + " AS nombre, ");
                 Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " AS Partida_id, ");
                 Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + ", ");
                 Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " +' '+ ");
                 Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS PROGRAMA, ");
                 Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas  + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " AS PROGRAMA_ID, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ",0) AS APROBADO, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ",0) AS DISPONIBLE, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ",0) AS AMPLIACION, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ",0) AS REDUCCION, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ",0) AS MODIFICADO, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ", 0) - ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Enero + ", 0) AS IMPORTE_ENERO, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ", 0) - ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Febrero + ", 0) AS IMPORTE_FEBRERO, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ", 0) - ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Marzo + ", 0) AS IMPORTE_MARZO, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ", 0) - ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Abril + ", 0) AS IMPORTE_ABRIL, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ", 0) - ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Mayo + ", 0) AS IMPORTE_MAYO, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ", 0) - ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Junio + ", 0) AS IMPORTE_JUNIO, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ", 0) - ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Julio + ", 0) AS IMPORTE_JULIO, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ", 0) - ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Agosto + ", 0) AS IMPORTE_AGOSTO, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ", 0) - ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Septiembre + ", 0) AS IMPORTE_SEPTIEMBRE, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ", 0) - ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Octubre + ", 0) AS IMPORTE_OCTUBRE, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre+ ", 0) - ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Noviembre+ ", 0) AS IMPORTE_NOVIEMBRE, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ", 0) - ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Diciembre + ", 0) AS IMPORTE_DICIEMBRE, ");
                 Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " AS FF_ID, ");
                 Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID + " AS AF_ID, ");
                 Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " AS UR_ID ");
                 Mi_Sql.Append(" FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                 Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                 Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                 Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                 Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                 Mi_Sql.Append(" = '" + Negocio.P_Anio + "'");
                 Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                 Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID_Busqueda + "'");
                 Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                 Mi_Sql.Append(" = '" + Negocio.P_Fuente_Financiamiento_ID + "'");

                 if (!String.IsNullOrEmpty(Negocio.P_Partida_Especifica_ID))
                 {
                     Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                     Mi_Sql.Append(" = '" + Negocio.P_Partida_Especifica_ID + "'");
                 }
                
                 Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas );
                 Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                 Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                 Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                 Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                 Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                 Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                 Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID + "'");


                 Mi_Sql.Append(" ORDER BY nombre ASC");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error al intentar consultar los registros de las partidas especificas. Error: [" + Ex.Message + "]");
             }
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Capitulos
         ///DESCRIPCIÓN          : consulta para obtener los datos de los capitulos
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 05/Diciembre/2011
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         internal static DataTable Consultar_Capitulos(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocio)
         {
             StringBuilder Mi_Sql = new StringBuilder();
             try
             {
                 Mi_Sql.Append("SELECT DISTINCT " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + " +' '+ ");
                 Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Descripcion + " AS nombre, ");
                 Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID + " AS id ");
                 Mi_Sql.Append(" FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                 Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                 Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID);
                 Mi_Sql.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                 Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                 Mi_Sql.Append(" = '" + Negocio.P_Anio + "'");
                 Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                 Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                 Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                 Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                 Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID_Busqueda + "'");
                 Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                 Mi_Sql.Append(" = '"+Negocio.P_Programa_ID +"'");
                 Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                 Mi_Sql.Append(" = '" + Negocio.P_Fuente_Financiamiento_ID + "'");
                
                 Mi_Sql.Append(" ORDER BY nombre ASC");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error al intentar consultar los registros de los capitulos. Error: [" + Ex.Message + "]");
             }
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Detalles_Importes
         ///DESCRIPCIÓN          : consulta para obtener los datos de los detalles de los importes
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 09/Marzo/2012
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         internal static DataTable Consultar_Detalles_Importes(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocio)
         {
             StringBuilder Mi_Sql = new StringBuilder();
             try
             {
                 Mi_Sql.Append("SELECT * FROM " + Ope_Com_Solicitud_Transf_Det.Tabla_Ope_Psp_Hist_Calendar_Presu);
                 Mi_Sql.Append(" WHERE " + Ope_Com_Solicitud_Transf_Det.Campo_No_Solicitud + " = '"+Negocio.P_No_Solicitud +"'");
                 
                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error al intentar consultar los registros de los capitulos. Error: [" + Ex.Message + "]");
             }
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Obtener_Capitulos
         ///DESCRIPCIÓN          : consulta para obtener los datos de los capitulos
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 20/Marzo/2012
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         internal static DataTable Obtener_Capitulos()
         {
             StringBuilder Mi_Sql = new StringBuilder();
             try
             {
                 Mi_Sql.Append("SELECT DISTINCT " +  Cat_SAP_Capitulos.Campo_Clave + " +' '+ ");
                 Mi_Sql.Append(Cat_SAP_Capitulos.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                 Mi_Sql.Append(Cat_SAP_Capitulos.Campo_Capitulo_ID);
                 Mi_Sql.Append(" FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                 Mi_Sql.Append(" WHERE  " + Cat_SAP_Capitulos.Campo_Estatus + " = 'ACTIVO'");
                 Mi_Sql.Append(" AND " + Cat_SAP_Capitulos.Campo_Clave + " NOT IN('1000')");

                 Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error al intentar consultar los registros de los capitulos. Error: [" + Ex.Message + "]");
             }
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Rol
         ///DESCRIPCIÓN          : consulta para obtener el rol del empleado
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 8/Marzo/2012
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         internal static DataTable Consultar_URs(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocio)
         {
             StringBuilder Mi_Sql = new StringBuilder();
             DataTable Dt_Ur = new DataTable();

             try
             {
                 if (Negocio.P_Tipo_Usuario.Trim().Equals("Administrador"))
                 {
                     Mi_Sql = new StringBuilder();
                     Mi_Sql.Append("SELECT DISTINCT " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " + ' ' + ");
                     Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS Nombre, ");
                     Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                     Mi_Sql.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
                     Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                     Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                     Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                     Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                     Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                     Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                     Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                     Mi_Sql.Append(" ORDER BY Nombre ASC");
                 }
                 else 
                 {
                     Mi_Sql = new StringBuilder();
                     Mi_Sql.Append("SELECT " + Cat_Dependencias.Campo_Clave + " + ' ' + ");
                     Mi_Sql.Append(Cat_Dependencias.Campo_Nombre + " AS Nombre, ");
                     Mi_Sql.Append(Cat_Dependencias.Campo_Dependencia_ID);
                     Mi_Sql.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
                     Mi_Sql.Append(" WHERE " + Cat_Dependencias.Campo_Estatus + " = 'ACTIVO'");
                     Mi_Sql.Append(" ORDER BY Nombre ASC");
                 }

                 Dt_Ur = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];

                 return Dt_Ur;
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error al intentar consultar las unidades responsables. Error: [" + Ex.Message + "]");
             }
         }


         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Fuente_Financiamiento
         ///DESCRIPCIÓN          : Obtiene datos de las fuentes de financiamiento de la Base de Datos y los regresa en un DataTable.
         ///PARAMETROS           : 
         ///CREO                 : Leslie González Vázquez
         ///FECHA_CREO           : 11/Noviembre/2011
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         internal static DataTable Consultar_Fuente_Financiamiento_Ramo33()
         {
             StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
             DataSet Ds_Fuente = new DataSet(); //Dataset donde obtendremos los datos de la consulta
             DataTable Dt_Fuente = new DataTable();
             try
             {
                 Mi_SQL.Append("SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + ", ");
                 Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Campo_Clave + "+' " + " " + "'+ " + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS NOMBRE ");
                 Mi_SQL.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                 Mi_SQL.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Estatus + " = 'ACTIVO'");
                 Mi_SQL.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33+ " = 'SI'");
                 Mi_SQL.Append(" ORDER BY " + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " ASC");

                 Ds_Fuente = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                 if (Ds_Fuente != null)
                 {
                     Dt_Fuente = Ds_Fuente.Tables[0];
                 }
             }
             catch (Exception Ex)
             {
                 String Mensaje = "Error al intentar consultar los registros de las fuentes de financiamiento. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                 throw new Exception(Mensaje);
             }
             return Dt_Fuente;
         }
        #endregion
    }
}