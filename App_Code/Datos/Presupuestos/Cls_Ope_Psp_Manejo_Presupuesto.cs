﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using SharpContent.ApplicationBlocks.Data;
using System.Text;
using JAPAMI.Parametros_Presupuestales.Datos;
using JAPAMI.Polizas.Negocios;
using JAPAMI.Generar_Reservas.Negocio;

namespace JAPAMI.Manejo_Presupuesto.Datos
{
    public class Cls_Ope_Psp_Manejo_Presupuesto
    {
        #region VARIABLES

        public static String CONSULTA_ANUAL = "ANUAL";
        public static String CONSULTA_MENSUAL = "MENSUAL";
        public static String CONSULTA_ACUMULADO_MENSUAL = "ACUMULADO";

        public static String DISPONIBLE = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible;// "DISPONIBLE";
        public static String PRE_COMPROMETIDO = Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido;// "PRE_COMPROMETIDO";
        public static String COMPROMETIDO = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido; //"COMPROMETIDO";
        public static String DEVENGADO = Ope_Psp_Presupuesto_Aprobado.Campo_Devengado; //"DEVENGADO";
        public static String EJERCIDO = Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido;// "EJERCIDO";
        public static String PAGADO = Ope_Psp_Presupuesto_Aprobado.Campo_Pagado; //"PAGADO";

        public static String OPERACION_TRASPASO = "TRASPASO";
        public static String OPERACION_INCREMENTAR = "INCREMENTAR";
        public static String OPERACION_DECREMENTAR = "DECREMENTAR";

        #endregion

        #region METODOS

        public Cls_Ope_Psp_Manejo_Presupuesto()
        {            
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Crear_Reserva_Directa
        ///DESCRIPCIÓN          : Metodo que crea la reserva para un proximo gasto 
        ///PARAMETROS           : String Dependencia_ID,String Estatus, String Beneficiario, String Fte_Financiamiento_ID, String P_Programa_ID, String Partida_ID,String Concepto,String Anio, double Importe
        ///CREO                 : Sergio Manuel Gallardo
        ///FECHA_CREO           : 30/Nov/2011
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        public static int Crear_Reserva_Directa(String Dependencia_ID, String Estatus, String Beneficiario, String Fte_Financiamiento_ID, String Programa_ID, String Concepto, String Anio, double Importe, String Proveedor_ID, String Empleado_ID, String Tipo_Solicitud_ID, DataTable Dt_Detalles, String Recurso, SqlCommand P_Cmmd, String Reserva_Directa)
        {
            String Mi_SQL = "";
            Int32 No_Reserva_Detalle;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            if (P_Cmmd != null)
            {
                Cmmd = P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                int No_Reserva = Obtener_Consecutivo(Ope_Psp_Reservas.Campo_No_Reserva, Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas, Cmmd);
                Mi_SQL = "INSERT INTO " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
                Mi_SQL = Mi_SQL + "(" + Ope_Psp_Reservas.Campo_No_Reserva;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Estatus;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Beneficiario;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Concepto;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Fecha;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Anio;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Importe_Inicial;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Saldo;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Proveedor_ID;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Empleado_ID;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Tipo_Solicitud_Pago_ID;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Recurso;
                if (!String.IsNullOrEmpty(Reserva_Directa))
                {
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Reserva_Directa;
                }
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Usuario_Creo;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Fecha_Creo;
                Mi_SQL = Mi_SQL + ") VALUES(";
                Mi_SQL = Mi_SQL + No_Reserva;
                Mi_SQL = Mi_SQL + ",'" + Estatus + "'";
                Mi_SQL = Mi_SQL + ",'" + Beneficiario + "'";
                Mi_SQL = Mi_SQL + ",'" + Concepto + "'";
                Mi_SQL = Mi_SQL + ",GETDATE()";
                if (!String.IsNullOrEmpty(Dependencia_ID))
                {
                    Mi_SQL = Mi_SQL + ",'" + Dependencia_ID.Trim() + "'";
                }
                else
                {
                    Mi_SQL = Mi_SQL + ",NULL";
                }
                Mi_SQL = Mi_SQL + ", " + Anio;
                Mi_SQL = Mi_SQL + ", " + Importe;
                Mi_SQL = Mi_SQL + ", " + Importe;
                //Verificar si es nulo
                if (String.IsNullOrEmpty(Proveedor_ID) == false)
                {
                    Mi_SQL = Mi_SQL + ", '" + Proveedor_ID + "'";
                }
                else
                {
                    Mi_SQL = Mi_SQL + ", NULL";
                }
                //verificar si es nulo
                if (String.IsNullOrEmpty(Empleado_ID) == false)
                {
                    Mi_SQL = Mi_SQL + ", '" + Empleado_ID + "'";
                }
                else
                {
                    Mi_SQL = Mi_SQL + ", NULL";
                }
                Mi_SQL = Mi_SQL + ", '" + Tipo_Solicitud_ID + "'";
                Mi_SQL = Mi_SQL + ", '" + Recurso + "'";
                if (!String.IsNullOrEmpty(Reserva_Directa))
                {
                    Mi_SQL = Mi_SQL + ", '" + Reserva_Directa + "'";
                }
                Mi_SQL = Mi_SQL + ",'" + Cls_Sessiones.Nombre_Empleado + "'";
                Mi_SQL = Mi_SQL + ",GETDATE())";
                Cmmd.CommandText = Mi_SQL.ToString();
                Cmmd.ExecuteNonQuery();
                No_Reserva_Detalle = Obtener_Consecutivo(Ope_Psp_Reservas_Detalles.Campo_No_Reserva_Detalle, Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles, Cmmd);
                foreach (DataRow Dr in Dt_Detalles.Rows)
                {
                    StringBuilder Mi_Sql = new StringBuilder();
                    Mi_Sql.Append("INSERT INTO " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "(");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_No_Reserva_Detalle + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_No_Reserva + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Partida_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Saldo + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Usuario_Creo + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Fecha_Creo + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Capitulo_ID + ")");
                    Mi_Sql.Append(" VALUES(" + No_Reserva_Detalle + ", ");
                    Mi_Sql.Append(No_Reserva + ", ");
                    Mi_Sql.Append("'" + Dr["PARTIDA_ID"].ToString() + "', ");
                    Mi_Sql.Append(Dr["IMPORTE"].ToString().Replace(",", "") + ", ");
                    Mi_Sql.Append(Dr["IMPORTE"].ToString().Replace(",", "") + ", ");
                    Mi_Sql.Append("'" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString() + "', ");
                    Mi_Sql.Append("'" + Dr["PROGRAMA_ID"].ToString() + "', ");
                    if (!String.IsNullOrEmpty(Dr["DEPENDENCIA_ID"].ToString()))
                    {
                        Mi_Sql.Append("'" + Dr["DEPENDENCIA_ID"].ToString() + "', ");
                    }
                    else
                    {
                        Mi_Sql.Append(",NULL");
                    }
                    Mi_Sql.Append("'" + Cls_Sessiones.Nombre_Empleado + "', ");
                    Mi_Sql.Append("GETDATE(), ");
                    Mi_Sql.Append("'" + Dr["CAPITULO_ID"].ToString().Trim() + "')");
                    Cmmd.CommandText = Mi_Sql.ToString();
                    Cmmd.ExecuteNonQuery();

                    No_Reserva_Detalle++;
                }

                if (P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return No_Reserva;
            }
            catch (SqlException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                Cn.Close();
            }
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Reservas_De_Requisicion_Taller
        ///DESCRIPCIÓN: Consultar_Reservas_De_Requisicion_Taller
        ///PARAMETROS: No_Requisicion
        ///CREO: Francisco Gallardo
        ///FECHA_CREO: 15/Ago/2013
        ///MODIFICO: 
        ///FECHA_MODIFICO: 
        ///CAUSA_MODIFICACIÓN: 
        ///*******************************************************************************
        public static DataTable Consultar_Reservas_De_Requisicion_Taller(String No_Requisicion)
        {
            DataTable Dt_Reserva = null;
            String Mi_SQL = "SELECT " +
            Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + ".*" +
            " FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + ", " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
            " WHERE " +
            Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + " = " +
            Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + ".NUM_RESERVA AND " +
            Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + ".NO_REQUISICION = " + No_Requisicion;
            try
            {
                Dt_Reserva = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
            return Dt_Reserva;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Reservas_De_Requisicion_Taller
        ///DESCRIPCIÓN: Consultar_Reservas_De_Requisicion_Taller
        ///PARAMETROS: No_Requisicion
        ///CREO: Francisco Gallardo
        ///FECHA_CREO: 15/Ago/2013
        ///MODIFICO: 
        ///FECHA_MODIFICO: 
        ///CAUSA_MODIFICACIÓN: 
        ///*******************************************************************************
        public static DataTable Consultar_Reservas_De_Requisicion_Taller(String No_Requisicion, ref SqlCommand P_Cmd)
        {
            SqlDataAdapter Obj_Adaptador = new SqlDataAdapter();
            DataTable Dt_Reserva = null;
            String Mi_SQL = "SELECT " +
            Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + ".*" +
            " FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + ", " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
            " WHERE " +
            Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + " = " +
            Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + ".NUM_RESERVA AND " +
            Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + ".NO_REQUISICION = " + No_Requisicion;
            try
            {
                //Ejecutar Consulta
                P_Cmd.CommandText = Mi_SQL;
                Obj_Adaptador.SelectCommand = P_Cmd;
                Obj_Adaptador.Fill(Dt_Reserva);
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
            return Dt_Reserva;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Encontrar_Duplicado
        ///DESCRIPCIÓN: Busca en la tabla si un presupuesta ya existe
        ///PARAMETROS: Dependencia_ID, Fte_Financiamiento_ID, Programa_ID, Capitulo_ID, Partida_ID, Anio
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 15/Nov/2011
        ///MODIFICO: Jennyfer Ivonne Ceja Lemus
        ///FECHA_MODIFICO: 23/Agosto/2012
        ///CAUSA_MODIFICACIÓN: Se agrego el nivel presupuestal como parametro de busqueda
        ///*******************************************************************************
        public static bool Encontrar_Duplicado(String Dependencia_ID, String Fte_Financiamiento_ID, String Programa_ID, String Capitulo_ID, String Partida_ID, int Anio)
        {
            bool Duplicado = false;
            try
            {
                DataTable _DataTable = Consultar_Presupuesto_Aprobado(Dependencia_ID, Fte_Financiamiento_ID, Programa_ID, Capitulo_ID, Partida_ID, Anio);
                if (_DataTable != null && _DataTable.Rows.Count > 0)
                {
                    Duplicado = true;
                }
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString() + "[NO SE PUDO VERIFICAR PRESUPUESTO DUPLICADO]");
            }
            return Duplicado;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Presupuesto_Calendarizado
        ///DESCRIPCIÓN: Guarda el presupuesto aprobado
        ///PARAMETROS: DataTable con el presupuesto calendarizado
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 15/Nov/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        ///String Dependencia_ID, String Fte_Financiamiento_ID, String Programa_ID, String Capitulo_ID, String Partida_ID, int Anio, String Estatus)
        public static DataTable Consultar_Presupuesto_Calendarizado(int Anio, String Estatus)
        {
            String Mi_SQL = "";
            DataTable Dt_Presupuesto_Calendarizado = null;
            try
            {
                Mi_SQL = "SELECT * FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu +
                 " WHERE " + Ope_Psp_Calendarizacion_Presu.Campo_Anio + " = " + Anio;
                if (!String.IsNullOrEmpty(Estatus))
                {
                    Mi_SQL += " AND " + Ope_Psp_Calendarizacion_Presu.Campo_Estatus + " = '" + Estatus + "'"; 
                }
                Mi_SQL += " ORDER BY " + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID + " ASC"; 
                Dt_Presupuesto_Calendarizado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString() + "[NO SE PUDO CONSULTAR PRESUPUESTO CALENDARIZADO]");
            }
            return Dt_Presupuesto_Calendarizado;
        }



        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Guardar_Presupuesto_Aprobado
        ///DESCRIPCIÓN: Guarda el presupuesto aprobado
        ///PARAMETROS: DataTable con el presupuesto calendarizado
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 15/Nov/2011
        ///MODIFICO: JENNYFER CEJA
        ///FECHA_MODIFIC: 8-Nov-12
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Guardar_Presupuesto_Aprobado(DataTable Dt_Presupuesto)
        {
            int Registros_Afectados = 0;
            String Anio = string.Empty;
            String Mi_SQL = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            String My_Sql = "";
            SqlDataAdapter Da_Datos = new SqlDataAdapter();
            DataSet Ds_Datos = new DataSet();
            DataTable Dt_Datos = new DataTable();
            Boolean Duplicado = false;
            Double Importe = 0.00;
            String[] Datos_Poliza_PSP = null;
            String No_Poliza_PSP = String.Empty;
            String Tipo_Poliza_PSP = String.Empty;
            String Mes_Anio_PSP = String.Empty;

            try
            {
                //validamos que no este duplicado algun registro del datatable
                if (Dt_Presupuesto != null)
                {
                    if (Dt_Presupuesto.Rows.Count > 0)
                    {
                        foreach (DataRow Dr_Presupuesto in Dt_Presupuesto.Rows)
                        {
                            Duplicado = false;

                            Mi_SQL = "INSERT INTO " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado +
                            "(" +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Anio + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + ", " +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ", " +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ", " +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Actualizado + ", " +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ", " +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ", " +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", " +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ", " +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ", " +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ", " +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + ", " +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ", " +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Enero + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Febrero + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Marzo + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Abril + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Mayo + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Junio + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Julio + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Agosto + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Septiembre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Octubre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Noviembre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Diciembre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Enero + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Febrero + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Marzo + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Abril + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Mayo + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Junio + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Julio + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Agosto + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Septiembre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Octubre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Noviembre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Diciembre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Enero + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Febrero + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Marzo + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Abril + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Mayo + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Junio + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Julio + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Agosto + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Septiembre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Octubre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Noviembre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Diciembre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Enero + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Febrero + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Marzo + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Abril + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Mayo + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Junio + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Julio + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Agosto + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Septiembre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Octubre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Noviembre + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Diciembre + "," +

                            Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Creo + "," +
                            Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Creo + ") VALUES (" +
                            "'" + Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID].ToString() + "'," +
                            "'" + Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID].ToString() + "'," +
                            "'" + Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID].ToString() + "'," +
                            "'" + Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID].ToString() + "'," +
                            "'" + Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID].ToString() + "',";

                            Mi_SQL = Mi_SQL +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Anio].ToString() + ", " +
                            "'" + Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID].ToString() + "'," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total].ToString() + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total].ToString() + ", 'NO'," +
                            0.00 + "," + 0.00 + "," +
                            Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total].ToString() + "," +
                            0.00 + "," +
                            0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," +
                            0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," +
                            0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," +
                            0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," +
                            0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," +
                            0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," +
                            0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," +
                            0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," +
                            0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," +
                            0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," +
                            0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," + 0.00 + "," +
                            "'" + Cls_Sessiones.Nombre_Empleado + "'," +
                            "GETDATE())";

                            //validamos que el registro no este duplicado
                            My_Sql = "SELECT * FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado +
                                " WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + int.Parse(Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Anio].ToString());
                            if (!String.IsNullOrEmpty(Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID].ToString()))
                            {
                                My_Sql += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID].ToString() + "'";
                            }
                            if (!String.IsNullOrEmpty(Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID].ToString()))
                            {
                                My_Sql += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID].ToString() + "'";
                            }
                            if (!String.IsNullOrEmpty(Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID].ToString()))
                            {
                                My_Sql += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID].ToString() + "'";
                            }
                            if (!String.IsNullOrEmpty(Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID].ToString()))
                            {
                                My_Sql += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID].ToString() + "'";
                            }
                            My_Sql += " ORDER BY " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " ASC";

                            Cmd.CommandText = My_Sql.Trim();
                            Da_Datos = new SqlDataAdapter(Cmd);
                            Da_Datos.Fill(Ds_Datos);

                            if (Ds_Datos != null)
                            {
                                Dt_Datos = Ds_Datos.Tables[0];
                            }

                            if (Dt_Datos != null)
                            {
                                if (Dt_Datos.Rows.Count > 0)
                                {
                                    Duplicado = true;
                                }
                            }

                            if (!Duplicado)
                            {
                                Cmd.CommandText = Mi_SQL;
                                Registros_Afectados += Cmd.ExecuteNonQuery();
                            }

                            Anio = Dr_Presupuesto[Ope_Psp_Presupuesto_Aprobado.Campo_Anio].ToString();
                        }

                        //obtenemos el importe del presupuesto
                        Importe = Dt_Presupuesto.AsEnumerable().Sum(x => x.Field<Double>(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total));

                        //obtenemos el id de la poliza presupuestal
                        Datos_Poliza_PSP = Registro_Movimientos_Presupuestales("", Ope_Psp_Presupuesto_Aprobado.Campo_Disponible,
                            Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado, Importe, "", "", "", "", Cmd);

                        if (Datos_Poliza_PSP != null)
                        {
                            if (Datos_Poliza_PSP.Length > 0)
                            {
                                No_Poliza_PSP = Datos_Poliza_PSP[0].Trim();
                                Tipo_Poliza_PSP = Datos_Poliza_PSP[1].Trim();
                                Mes_Anio_PSP = Datos_Poliza_PSP[2].Trim();
                            }
                        }

                        //hacemos los cambios en el calendarizado de presupuesto
                        Mi_SQL = "UPDATE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu;
                        Mi_SQL += " SET " + Ope_Psp_Calendarizacion_Presu.Campo_Estatus + " = 'CARGADO' ";

                        if (!String.IsNullOrEmpty(No_Poliza_PSP))
                        {
                            Mi_SQL += ", " + Ope_Psp_Calendarizacion_Presu.Campo_No_Poliza_Presupuestal + " = '" + No_Poliza_PSP.Trim() + "', ";
                        }
                        if (!String.IsNullOrEmpty(Tipo_Poliza_PSP))
                        {
                            Mi_SQL += Ope_Psp_Calendarizacion_Presu.Campo_Tipo_Poliza_ID_Presupuestal + " = '" + Tipo_Poliza_PSP.Trim() + "', ";
                        }
                        if (!String.IsNullOrEmpty(Mes_Anio_PSP))
                        {
                            Mi_SQL += Ope_Psp_Calendarizacion_Presu.Campo_Mes_Anio_Presupuestal + " = '" + Mes_Anio_PSP.Trim() + "' ";
                        }

                        Mi_SQL += " WHERE " + Ope_Psp_Calendarizacion_Presu.Campo_Anio + " = '" + Anio + "'";
                        Mi_SQL += " AND " + Ope_Psp_Calendarizacion_Presu.Campo_Estatus + " = 'AUTORIZADO'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }
                }

                Trans.Commit();
            }
            catch (Exception Ex)
            {
                Trans.Rollback();
                throw new Exception(Ex.ToString());
            }
            finally
            {
                Cn.Close();
            }
            return Registros_Afectados;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Guardar_Presupuesto_Aprobado
        ///DESCRIPCIÓN: Guarda el presupuesto aprobado solo de una partida
        ///PARAMETROS: DataTable con el presupuesto calendarizado
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 15/Nov/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Guardar_Presupuesto_Aprobado(
            String Dependencia_ID, String Fte_Financiamiento_ID, String Programa_ID, String Capitulo_ID, String Partida_ID, int Anio,
            double Importe_Enero, double Importe_Febrero, double Importe_Marzo, double Importe_Abril,
            double Importe_Mayo, double Importe_Junio, double Importe_Julio, double Importe_Agosto,
            double Importe_Septiembre, double Importe_Octubre, double Importe_Noviembre, double Importe_Diciembre, double Importe_Total
            )
        {
            int Registros_Afectados = 0;
            int No_Ajuste = 0;
            String Mi_SQL = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                    Mi_SQL = "INSERT INTO " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado +
                    "(" +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Anio + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Creo + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Creo +
                    ") VALUES (" +
                    "'" + Dependencia_ID + "'," +
                    "'" + Fte_Financiamiento_ID + "'," +
                    "'" + Programa_ID + "'," +
                    "'" + Capitulo_ID + "'," +
                    "'" + Partida_ID + "'," +
                    Anio + "," +
                    Importe_Enero + "," +
                    Importe_Febrero + "," +
                    Importe_Marzo + "," +
                    Importe_Abril + "," +
                    Importe_Mayo + "," +
                    Importe_Junio + "," +
                    Importe_Julio + "," +
                    Importe_Agosto + "," +
                    Importe_Septiembre + "," +
                    Importe_Octubre + "," +
                    Importe_Noviembre + "," +
                    Importe_Diciembre + "," +
                    Importe_Total + "," +
                    "'" + Cls_Sessiones.Nombre_Empleado + "'," +
                    "GETDATE()";
                    if (!Encontrar_Duplicado(
                            Dependencia_ID,
                            Fte_Financiamiento_ID,
                            Programa_ID,
                            Capitulo_ID,
                            Partida_ID,
                            Anio)
                        )
                    {
                        Cmd.CommandText = Mi_SQL;
                        Registros_Afectados += Cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        throw new Exception("LA PARTIDA PRESUPUESTAL YA SE ENCUENTRA REGISTRADA ");
                    }
                
                Trans.Commit();
            }
            catch (Exception Ex)
            {
                Trans.Rollback();
                throw new Exception(Ex.ToString());
            }
            finally
            {
                Cn.Close();
            }
            return Registros_Afectados;
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Eliminar_Presupuesto_Aprobado
        ///DESCRIPCIÓN: Elimina el presupuesto de un año
        ///PARAMETROS: Año a eliminar
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 15/Nov/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Eliminar_Presupuesto_Aprobado(int Anio)
        {
            String Mi_SQL = "";
            int Registros_Afectados = 0;

            try
            {
                Mi_SQL = "DELETE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado +
                " WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio;
                Registros_Afectados = SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
            return Registros_Afectados;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Presupuesto
        ///DESCRIPCIÓN: Retorna DataTable con todos los campos de la tabla de una partida
        ///PARAMETROS: Dependencia_ID, Fte_Financiamiento_ID, Programa_ID, Capitulo_ID, Partida_ID, Anio
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 15/Nov/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Presupuesto_Aprobado(String Dependencia_ID, String Fte_Financiamiento_ID, String Programa_ID, String Capitulo_ID, String Partida_ID, int Anio)
        {
            String Mi_SQL = "";
            DataTable Dt_Presupuesto = null;
            try
            {
                Mi_SQL = "SELECT * FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado +
                " WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio;
                if (!String.IsNullOrEmpty(Dependencia_ID))
                {
                    Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dependencia_ID + "'";
                }
                if (!String.IsNullOrEmpty(Fte_Financiamiento_ID))
                {
                    Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Fte_Financiamiento_ID + "'";
                }
                if (!String.IsNullOrEmpty(Programa_ID))
                {
                    Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Programa_ID + "'";
                }
                if (!String.IsNullOrEmpty(Partida_ID))
                {
                    Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Partida_ID + "'";
                }
                Mi_SQL += " ORDER BY " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " ASC";
                Dt_Presupuesto = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
            return Dt_Presupuesto;
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Presupuesto
        ///DESCRIPCIÓN: Retorna DataTable con todos los campos de la tabla de una partida
        ///PARAMETROS: Dependencia_ID, Fte_Financiamiento_ID, Programa_ID, Capitulo_ID, Partida_ID, Anio
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 15/Nov/2011
        ///MODIFICO: Jennyfer Ivonne Ceja Lemus
        ///FECHA_MODIFICO: 23/Agosto/2012
        ///CAUSA_MODIFICACIÓN: Se agrego el subnivel presuouestal como parametro de busqueda
        ///*******************************************************************************
        public static DataTable Consultar_Presupuesto_Aprobado(String Dependencia_ID, String Fte_Financiamiento_ID, String Programa_ID, String Capitulo_ID, String Partida_ID, String Subnivel_Presupuestal_ID, int Anio)
        {
            String Mi_SQL = "";
            DataTable Dt_Presupuesto = null;
            try
            {
                Mi_SQL = "SELECT * FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado +
                "  WITH(NOLOCK) WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio;
                if (!String.IsNullOrEmpty(Dependencia_ID))
                {
                    Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dependencia_ID + "'";
                }
                if (!String.IsNullOrEmpty(Fte_Financiamiento_ID))
                {
                    Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Fte_Financiamiento_ID + "'";
                }
                if (!String.IsNullOrEmpty(Programa_ID))
                {
                    Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Programa_ID + "'";
                }
                if (!String.IsNullOrEmpty(Capitulo_ID))
                {
                    Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + " = '" + Capitulo_ID + "'";
                }
                if (!String.IsNullOrEmpty(Partida_ID))
                {
                    Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Partida_ID + "'";
                }
                if (!String.IsNullOrEmpty(Subnivel_Presupuestal_ID)) //cambio
                {
                    Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Subnivel_Presupuestal_ID + " = '" + Subnivel_Presupuestal_ID + "'";
                }
                Mi_SQL += " ORDER BY " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " ASC";               
                Dt_Presupuesto = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
            return Dt_Presupuesto;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Importes_Presupuesto_Aprobado
        ///DESCRIPCIÓN: Se pueden consultar APROBADO, REDUCCION, AMPLIACION, 
        ///PARAMETROS: Dependencia_ID, Fte_Financiamiento_ID, Programa_ID, Capitulo_ID, Partida_ID, Anio
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 15/Nov/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static double Consultar_Importes_Presupuesto_Aprobado(String Dependencia_ID, String Fte_Financiamiento_ID, String Programa_ID, String Capitulo_ID, String Partida_ID, int Anio, String Campo)
        {
            String Mi_SQL = "";
            double Importe = 0;
            if (Campo == DISPONIBLE || Campo == PRE_COMPROMETIDO || Campo == COMPROMETIDO || Campo == DEVENGADO || Campo == EJERCIDO || Campo == PAGADO)
            {
                try
                {
                    Mi_SQL = "SELECT " + Campo + " FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado +
                    " WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio;
                    if (!String.IsNullOrEmpty(Dependencia_ID))
                    {
                        Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dependencia_ID + "'";
                    }
                    if (!String.IsNullOrEmpty(Fte_Financiamiento_ID))
                    {
                        Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Fte_Financiamiento_ID + "'";
                    }
                    if (!String.IsNullOrEmpty(Programa_ID))
                    {
                        Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Programa_ID + "'";
                    }
                    if (!String.IsNullOrEmpty(Capitulo_ID))
                    {
                        Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + " = '" + Capitulo_ID + "'";
                    }
                    if (!String.IsNullOrEmpty(Partida_ID))
                    {
                        Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Partida_ID + "'";
                    }
                    Mi_SQL += " ORDER BY " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " ASC";
                    Importe = Convert.ToDouble( SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL));
                }
                catch (Exception Ex)
                {
                    throw new Exception(Ex.ToString() + "[NO SE PUDO OBTENER EL IMPORTE SOLICITADO, VERIFIQUE CON SU ADMINISTRADOR]");
                }
            }
            else
            {
                throw new Exception("EL VALOR DEL PARAMETRO CAMPO NO ES VALIDO");
            }
            return Importe;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Actualizar_Momentos_Presupuestales
        ///DESCRIPCIÓN: 
        ///PARAMETROS: Dependencia_ID, Fte_Financiamiento_ID, Programa_ID, Capitulo_ID, Partida_ID, Anio
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 15/Nov/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Actualizar_Momentos_Presupuestales
            (String Dependencia_ID, String Fte_Financiamiento_ID, String Programa_ID, String Partida_ID, int Anio,
            String Cargo, String Abono, double Importe)
        {
            int Registros_Afectados = 0;
            String Mi_SQL = "";
            try
            {
                //Realizar el movimiento presupuestal solicitado
                Mi_SQL = "UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " SET " +
                Cargo + " = " + Cargo + " + " + Importe + ", " +
                Abono + " = " + Abono + " - " + Importe +
                " WHERE " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dependencia_ID + "' AND " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Fte_Financiamiento_ID + "' AND " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Programa_ID + "' AND " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Partida_ID + "' AND " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio;
                Registros_Afectados = SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
            return Registros_Afectados;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Actualizar_Momentos_Presupuestales
        ///DESCRIPCIÓN: 
        ///PARAMETROS: No_Reserva
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 15/Nov/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Actualizar_Momentos_Presupuestales(String No_Reserva, String Cargo, String Abono, double Importe)
        {
            int Registros_Afectados = 0;
            DataTable Dt_Codigo_Programatico = null;
            String Mi_SQL = "";
            try
            {
                Dt_Codigo_Programatico = Consultar_Reserva(No_Reserva);

                if (Dt_Codigo_Programatico != null && Dt_Codigo_Programatico.Rows.Count > 0)
                {
                    //Realizar el movimiento presupuestal solicitado
                    Mi_SQL = "UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " SET " +
                    Cargo + " = " + Cargo + " + " + Importe + ", " +
                    Abono + " = " + Abono + " - " + Importe +
                    " WHERE " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dt_Codigo_Programatico.Rows[0]["DEPENDENCIA_ID"].ToString() + "' AND " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dt_Codigo_Programatico.Rows[0]["FTE_FINANCIAMIENTO_ID"].ToString() + "' AND " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dt_Codigo_Programatico.Rows[0]["PROYECTO_PROGRAMA_ID"].ToString() + "' AND " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dt_Codigo_Programatico.Rows[0]["PARTIDA_ID"].ToString() + "' AND " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Dt_Codigo_Programatico.Rows[0]["ANIO"].ToString() + "";

                    Registros_Afectados = SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
            return Registros_Afectados;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Actualizar_Momentos_Presupuestales
        ///DESCRIPCIÓN          : 
        ///PARAMETROS           : 
        ///CREO                 : Gustavo Angeles Cruz
        ///FECHA_CREO           : 15/Nov/2011
        ///MODIFICO             : Leslie Gonzalez Vazquez
        ///FECHA_MODIFICO       : 21/Enero/2012
        ///CAUSA_MODIFICACIÓN   : Porque cambio la tabla de reservas y para incluir los detalles de las reservas
        ///*******************************************************************************
        public static int Actualizar_Momentos_Presupuestales(String No_Reserva, String Cargo, String Abono, DataTable Dt_Detalles)
        {
            int Registros_Afectados = 0;
            DataTable Dt_Codigo_Programatico = null;
            StringBuilder Mi_Sql = new StringBuilder();
            Double Importe = 0.00;
            DateTime Fecha_Creo;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;

            try
            {
                Dt_Codigo_Programatico = Consultar_Reserva(No_Reserva);

                if (Dt_Codigo_Programatico != null && Dt_Codigo_Programatico.Rows.Count > 0)
                {
                    if (Dt_Detalles != null)
                    {
                        if (Dt_Detalles.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Detalles.Rows)
                            {
                                Importe = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE"].ToString()) ? "0" : Dr["IMPORTE"].ToString().Trim());

                                Mi_Sql = new StringBuilder();

                                Mi_Sql.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                Mi_Sql.Append(" SET " + Cargo + " = " + Cargo + "+" + Importe + ", ");
                                Mi_Sql.Append(Abono + " = " + Abono + " - " + Importe);
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr["DEPENDENCIA_ID"].ToString() + "'");
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString() + "'");
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROGRAMA_ID"].ToString() + "'");
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr["PARTIDA_ID"].ToString() + "'");
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Dt_Codigo_Programatico.Rows[0]["ANIO"].ToString());
                                Cmd.CommandText = Mi_Sql.ToString();
                                Registros_Afectados += Cmd.ExecuteNonQuery();
                                if (Abono == "DISPONIBLE")
                                {
                                    Mi_Sql = new StringBuilder();
                                    Mi_Sql.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                    Mi_Sql.Append(" SET AFECTADO_" + String.Format("{0:MMMM}", DateTime.Now) + " = AFECTADO_" + String.Format("{0:MMMM}", DateTime.Now) + "+" + Importe);
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr["DEPENDENCIA_ID"].ToString() + "'");
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString() + "'");
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROGRAMA_ID"].ToString() + "'");
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr["PARTIDA_ID"].ToString() + "'");
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Dt_Codigo_Programatico.Rows[0]["ANIO"].ToString());
                                    Cmd.CommandText = Mi_Sql.ToString();
                                    Registros_Afectados += Cmd.ExecuteNonQuery();
                                }
                                if (Cargo == "DISPONIBLE")
                                {
                                    Mi_Sql = new StringBuilder();
                                    Fecha_Creo = Convert.ToDateTime(Dt_Codigo_Programatico.Rows[0]["FECHA_CREO"].ToString());
                                    Mi_Sql.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                    Mi_Sql.Append(" SET AFECTADO_" + String.Format("{0:MMMM}", Fecha_Creo) + " = AFECTADO_" + String.Format("{0:MMMM}", DateTime.Now) + "-" + Importe);
                                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr["DEPENDENCIA_ID"].ToString() + "'");
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString() + "'");
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROGRAMA_ID"].ToString() + "'");
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr["PARTIDA_ID"].ToString() + "'");
                                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Dt_Codigo_Programatico.Rows[0]["ANIO"].ToString());
                                    Cmd.CommandText = Mi_Sql.ToString();
                                    Registros_Afectados += Cmd.ExecuteNonQuery();
                                }

                            }
                        }
                    }

                    Trans.Commit();
                }
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
            return Registros_Afectados;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Traspaso_Presupuestal
        ///DESCRIPCIÓN: Se realizan traspasos de presupuesto 
        ///PARAMETROS: codigo origen y destino + importe
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 15/Nov/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Traspaso_Presupuestal
            (String Origen_Dependencia_ID, String Origen_Fte_Financiamiento_ID, String Origen_Programa_ID, String Origen_Partida_ID, int Origen_Anio,
             String Destino_Dependencia_ID, String Destino_Fte_Financiamiento_ID, String Destino_Programa_ID, String Destino_Partida_ID, int Destino_Anio,
             double Importe)
        {
            int Registros_Afectados = 0;
            String Mi_SQL = "";
            try
            {
                //Realizar el movimiento presupuestal solicitado para la partida ORIGEN
                Mi_SQL = "UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " SET " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + " + " + Importe + ", " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + " = " + 
                    Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + " + " + 
                    Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion +  " - " + 
                    Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ", " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " - " + Importe +  
                " WHERE " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Origen_Dependencia_ID + "' AND " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Origen_Fte_Financiamiento_ID + "' AND " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Origen_Programa_ID + "' AND " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Origen_Partida_ID + "' AND " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Origen_Anio;
                Registros_Afectados += SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                //Realizar el movimiento presupuestal solicitado para la partida DESTINO
                Mi_SQL = "UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " SET " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + " + " + Importe + ", " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + " = " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + " + " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + " - " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ", " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " + " + Importe +
                " WHERE " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Destino_Dependencia_ID + "' AND " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Destino_Fte_Financiamiento_ID + "' AND " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Destino_Programa_ID + "' AND " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Destino_Partida_ID + "' AND " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Destino_Anio;
                Registros_Afectados += SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
            return Registros_Afectados;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Incremento_Presupuestal
        ///DESCRIPCIÓN: Se realizan traspasos de presupuesto 
        ///PARAMETROS: codigo origen y destino + importe
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 15/Nov/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Incremento_Presupuestal
            (String Dependencia_ID, String Fte_Financiamiento_ID, String Programa_ID, String Partida_ID, int Anio, double Importe)
        {
            int Registros_Afectados = 0;
            String Mi_SQL = "";
            try
            {
                //Realizar el movimiento presupuestal solicitado para la partida 
                Mi_SQL = "UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " SET " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + " + " + Importe + ", " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + " = " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + " + " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + " - " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ", " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " + " + Importe +
                " WHERE " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dependencia_ID + "' AND " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Fte_Financiamiento_ID + "' AND " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Programa_ID + "' AND " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Partida_ID + "' AND " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio;
                Registros_Afectados += SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
            return Registros_Afectados;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Reduccion_Presupuestal
        ///DESCRIPCIÓN: Se realizan traspasos de presupuesto 
        ///PARAMETROS: codigo origen y destino + importe
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 15/Nov/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Reduccion_Presupuestal
            (String Dependencia_ID, String Fte_Financiamiento_ID, String Programa_ID, String Partida_ID, int Anio, double Importe)
        {
            int Registros_Afectados = 0;
            String Mi_SQL = "";
            try
            {
                //Realizar el movimiento presupuestal solicitado para la partida 
                Mi_SQL = "UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " SET " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + " + " + Importe + ", " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + " = " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + " + " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + " - " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ", " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " - " + Importe +
                " WHERE " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dependencia_ID + "' AND " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Fte_Financiamiento_ID + "' AND " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Programa_ID + "' AND " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Partida_ID + "' AND " +
                Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio;
                Registros_Afectados += SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
            return Registros_Afectados;
        }

//##########################################################

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Inicializar_Presupuesto_Mensual
        ///DESCRIPCIÓN: Metodo que Inicializa el disponible segun sea 
        ///PARAMETROS: String No_Reserva
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 16/Nov/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static void Inicializar_Presupuesto_Mensual()
        {
            //Verificamos Si es Anual o Mensual
            String Mi_SQL = "";
            DataTable Dt_Tipo_Consulta = null;
            String Tipo_Consulta = "";
            //Consultamos el tipo de presupuesto que se aplicara en el año en curso, puede ser MENSUAL, ANUAL o ACUMULADO
            Mi_SQL = "SELECT * FROM " + Cat_Parametros_Ejercer_Psp.Tabla_Cat_Parametros_Ejercer_Psp;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Parametros_Ejercer_Psp.Campo_Anio;
            Mi_SQL = Mi_SQL + "=EXTRACT(YEAR FROM GETDATE())";
            Dt_Tipo_Consulta = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            if (Dt_Tipo_Consulta != null)
            {
                if (Dt_Tipo_Consulta.Rows.Count > 0)
                {
                    Tipo_Consulta = Dt_Tipo_Consulta.Rows[0][Cat_Parametros_Ejercer_Psp.Campo_Tipo_De_Consulta].ToString().Trim();
                }
            }
            Mi_SQL = "";
            //Obtenemos la Fecha Actual
            DateTime Fecha_Actual = DateTime.Now;
            //Obtenemos el mes Actual
            int Mes_Actual = int.Parse(Fecha_Actual.Month.ToString());
            switch (Tipo_Consulta)
            {
                case "MENSUAL":
                    //Actualizamos el presupuesto

                    //Realizamos la Actualizacion del Disponible de acuerdo al mes en el que se encuentre
                    switch (Mes_Actual)
                    {
                        case 1:
                            //MES ENERO
                            //Actualizar_Disponible_Mensual(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero);
                            break;
                        case 2:
                            //MES FEBRERO
                            Actualizar_Disponible_Mensual(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero);
                            break;
                        case 3:
                            //MES MARZO
                            Actualizar_Disponible_Mensual(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo);
                            break;
                        case 4:
                            //MES ABRIL
                            Actualizar_Disponible_Mensual(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril);
                            break;
                        case 5:
                            //MES MAYO
                            Actualizar_Disponible_Mensual(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo);
                            break;
                        case 6:
                            //MES JUNIO
                            Actualizar_Disponible_Mensual(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio);
                            break;
                        case 7:
                            //MES JULIO
                            Actualizar_Disponible_Mensual(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio);
                            break;
                        case 8:
                            //MES AGOSTO
                            Actualizar_Disponible_Mensual(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto);
                            break;
                        case 9:
                            //MES SEPTIEMBRE
                            Actualizar_Disponible_Mensual(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre);
                            break;
                        case 10:
                            //MES OCTUBRE
                            Actualizar_Disponible_Mensual(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre);
                            break;
                        case 11:
                            //MES NOVIEMBRE
                            Actualizar_Disponible_Mensual(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre);
                            break;
                        case 12:
                            //MES DICIEMBRE
                            Actualizar_Disponible_Mensual(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre);
                            break;
                    }

                    break;
                case "ACUMULADO MENSUAL":
                    //Realizamos la Actualizacion del Disponible de acuerdo al mes en el que se encuentre
                    switch (Mes_Actual)
                    {
                        case 1:
                            //MES ENERO
                            //Actualizar_Disponible_Acumulado(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero);
                            break;
                        case 2:
                            //MES FEBRERO
                            Actualizar_Disponible_Acumulado(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero);
                            break;
                        case 3:
                            //MES MARZO
                            Actualizar_Disponible_Acumulado(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo);
                            break;
                        case 4:
                            //MES ABRIL
                            Actualizar_Disponible_Acumulado(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril);
                            break;
                        case 5:
                            //MES MAYO
                            Actualizar_Disponible_Acumulado(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo);
                            break;
                        case 6:
                            //MES JUNIO
                            Actualizar_Disponible_Acumulado(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio);
                            break;
                        case 7:
                            //MES JULIO
                            Actualizar_Disponible_Acumulado(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio);
                            break;
                        case 8:
                            //MES AGOSTO
                            Actualizar_Disponible_Acumulado(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto);
                            break;
                        case 9:
                            //MES SEPTIEMBRE
                            Actualizar_Disponible_Acumulado(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre);
                            break;
                        case 10:
                            //MES OCTUBRE
                            Actualizar_Disponible_Acumulado(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre);
                            break;
                        case 11:
                            //MES NOVIEMBRE
                            Actualizar_Disponible_Acumulado(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre);
                            break;
                        case 12:
                            //MES DICIEMBRE
                            Actualizar_Disponible_Acumulado(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre);
                            break;
                    }
                    break;
           }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Actualizar_Disponible
        ///DESCRIPCIÓN: Metodo que Inicializa el disponible segun sea el mes actual 
        ///PARAMETROS: String Campo:Nombre del campo al que se le asignara el disponible.
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 16/Nov/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private static void Actualizar_Disponible_Mensual(String Campo)
        {
            String Mi_SQL = "";
            //aCTUALIZAMOS EL DISPONIBLE EN CASO DE QUE SE APLIQUE LA AFECTACION PRESUPUESTAL MENSUAL
            Mi_SQL = "UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado;
            Mi_SQL = Mi_SQL + " SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible;
            Mi_SQL = Mi_SQL + "=" + Campo;
            Mi_SQL = Mi_SQL + ", ACTUALIZADO ='SI'";
            Mi_SQL = Mi_SQL + " WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio;
            Mi_SQL = Mi_SQL + "=EXTRACT(YEAR FROM GETDATE())";
            Mi_SQL = Mi_SQL + " AND EXTRACT(DAY FROM GETDATE())=1";
            Mi_SQL = Mi_SQL + " AND ACTUALIZADO='NO'";
            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

            //ACTUALIZAMOS EL CAMPO DE ACTUALIZADO A NO SOLO CUANOD ES EL DIA 28 DE CADA MES PARA QUE NOS 
            //DEJE MODIFICAR AL SIGUIENTE MES EL DISPONIBLE QUE CORRESPONDE AL MES EN CURSO
            Mi_SQL = "UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado;
            Mi_SQL = Mi_SQL + " SET ACTUALIZADO ='NO'";
            Mi_SQL = Mi_SQL + " WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio;
            Mi_SQL = Mi_SQL + "=EXTRACT(YEAR FROM GETDATE())";
            Mi_SQL = Mi_SQL + " AND EXTRACT(DAY FROM GETDATE())=28";

            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Actualizar_Disponible
        ///DESCRIPCIÓN: Metodo que Inicializa el disponible segun sea el mes actual 
        ///PARAMETROS: String Campo:Nombre del campo al que se le asignara el disponible.
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 16/Nov/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private static void Actualizar_Disponible_Acumulado(String Campo)
        {
            String Mi_SQL = "";
            //aCTUALIZAMOS EL DISPONIBLE EN CASO DE QUE SE APLIQUE LA AFECTACION PRESUPUESTAL MENSUAL
            Mi_SQL = "UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado;
            Mi_SQL = Mi_SQL + " SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible;
            Mi_SQL = Mi_SQL + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible +" + " +  Campo;
            Mi_SQL = Mi_SQL + ", ACTUALIZADO ='SI'";
            Mi_SQL = Mi_SQL + " WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio;
            Mi_SQL = Mi_SQL + "=EXTRACT(YEAR FROM GETDATE())";
            Mi_SQL = Mi_SQL + " AND EXTRACT(DAY FROM GETDATE())=1";
            Mi_SQL = Mi_SQL + " AND ACTUALIZADO='NO'";
            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

            //ACTUALIZAMOS EL CAMPO DE ACTUALIZADO A NO SOLO CUANOD ES EL DIA 28 DE CADA MES PARA QUE NOS 
            //DEJE MODIFICAR AL SIGUIENTE MES EL DISPONIBLE QUE CORRESPONDE AL MES EN CURSO
            Mi_SQL = "UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado;
            Mi_SQL = Mi_SQL + " SET ACTUALIZADO ='NO'";
            Mi_SQL = Mi_SQL + " WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio;
            Mi_SQL = Mi_SQL + "=EXTRACT(YEAR FROM GETDATE())";
            Mi_SQL = Mi_SQL + " AND EXTRACT(DAY FROM GETDATE())=28";

            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Codigo_Programatico_De_Reserva
        ///DESCRIPCIÓN: Metodo que consulta todo el Codigo Programatico de la reserva
        ///PARAMETROS: String No_Reserva
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 15/Nov/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Reserva(String No_Reserva)
        {
            //COnsultar tabla de Reservas
            DataTable Dt_Codigo_Programatico = null;
            String Mi_SQL = "";
            try
            {
                Mi_SQL = "SELECT * FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva;
                Mi_SQL = Mi_SQL + "=" + No_Reserva.Trim();
                Dt_Codigo_Programatico = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {

                throw new Exception(Ex.ToString());
            }
            return Dt_Codigo_Programatico;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Registro_Movimiento_Presupuestal
        ///DESCRIPCIÓN: Metodo que da de algta el movimiento presupuestal
        ///PARAMETROS: String No_Reserva, String Cargo, String Abono, double Importe
        ///CREO: Susana Trigueros Armenta 
        ///FECHA_CREO: 15/Nov/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Registro_Movimiento_Presupuestal(String No_Reserva, String Cargo, String Abono, double Importe, String No_Poliza, String Tipo_Poliza_ID, String Mes_Ano, String Partida)
        {
            int Registros_Guardados = 0;
            String Mi_SQL = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            String[] Datos_Poliza_PSP = null;
            String No_Poliza_PSP = String.Empty;
            String Tipo_Poliza_PSP = String.Empty;
            String Mes_Anio_PSP = String.Empty;
            DataTable Dt_Parametros_PSP = new DataTable();
            Cn.ConnectionString = Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Trans.Connection;
            Cmmd.Transaction = Trans;

            try
            {
                if (Importe != 0 && !String.IsNullOrEmpty(Cargo.Trim()) && !String.IsNullOrEmpty(Abono.Trim()))
                {
                    Dt_Parametros_PSP = Cls_Cat_Psp_Parametros_Presupuestales_Datos.Consultar_Parametros_Presupuestales();
                    Datos_Poliza_PSP = Crear_Poliza_Presupuestal_Egresos(Cargo, Abono, Importe.ToString().Trim(), Cmmd, Dt_Parametros_PSP);
                    //obtenemos los id de la poliza presupuestal creada
                    if (Datos_Poliza_PSP != null)
                    {
                        if (Datos_Poliza_PSP.Length > 0)
                        {
                            No_Poliza_PSP = Datos_Poliza_PSP[0].Trim();
                            Tipo_Poliza_PSP = Datos_Poliza_PSP[1].Trim();
                            Mes_Anio_PSP = Datos_Poliza_PSP[2].Trim();
                        }
                    }

                    Mi_SQL = "INSERT INTO " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos;
                    Mi_SQL = Mi_SQL + " (" + Ope_Psp_Registro_Movimientos.Campo_No_Reserva;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Cargo;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Abono;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Importe;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Fecha;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Usuario;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Usuario_Creo;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Fecha_Creo;
                    if (No_Poliza.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_No_Poliza;
                    if (Tipo_Poliza_ID.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Tipo_Poliza_ID;
                    if (Mes_Ano.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Mes_Ano;
                    if (Partida.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Partida;
                    if (No_Poliza_PSP.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_No_Poliza_Presupuestal;
                    if (Tipo_Poliza_PSP.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Tipo_Poliza_ID_Presupuestal;
                    if (Mes_Anio_PSP.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Mes_Anio_Presupuestal;


                    Mi_SQL = Mi_SQL + ") VALUES(";
                    Mi_SQL = Mi_SQL + No_Reserva;
                    Mi_SQL = Mi_SQL + ",'" + Cargo + "'";
                    Mi_SQL = Mi_SQL + ",'" + Abono + "'";
                    Mi_SQL = Mi_SQL + ",'" + Importe.ToString() + "'";
                    Mi_SQL = Mi_SQL + ",GETDATE()";
                    Mi_SQL = Mi_SQL + ",'" + Cls_Sessiones.Nombre_Empleado + "'";
                    Mi_SQL = Mi_SQL + ",'" + Cls_Sessiones.Nombre_Empleado + "'";
                    Mi_SQL = Mi_SQL + ",GETDATE()";
                    if (No_Poliza.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", '" + No_Poliza.Trim() + "'";
                    if (Tipo_Poliza_ID.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", '" + Tipo_Poliza_ID.Trim() + "'";
                    if (Mes_Ano.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", '" + Mes_Ano.Trim() + "'";
                    if (Partida.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + "," + Partida.Trim();
                    if (No_Poliza_PSP.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", '" + No_Poliza_PSP.Trim() + "'";
                    if (Tipo_Poliza_PSP.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", '" + Tipo_Poliza_PSP.Trim() + "'";
                    if (Mes_Anio_PSP.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", '" + Mes_Anio_PSP.Trim() + "'";

                    Mi_SQL = Mi_SQL + ")";

                    Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                    Registros_Guardados = Cmmd.ExecuteNonQuery();

                    Trans.Commit();
                }
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Cmmd == null)
                {
                    Cn.Close();
                }

            }
            return Registros_Guardados;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Registro_Movimiento_Presupuestal
        ///DESCRIPCIÓN: Metodo que da de algta el movimiento presupuestal
        ///PARAMETROS: String No_Reserva, String Cargo, String Abono, double Importe
        ///CREO: Sergio Manuel Gallardo Andrade
        ///FECHA_CREO: 15/Nov/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Registro_Movimiento_Presupuestal(String No_Reserva, String Cargo, String Abono, double Importe, String No_Poliza, String Tipo_Poliza_ID, String Mes_Ano, String Partida, SqlCommand P_Cmmd)
        {
            int Registros_Guardados = 0;
            String Mi_SQL = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Sql = new SqlDataAdapter();
            DataSet Ds_Sql = new DataSet();
            SqlDataAdapter Dt_Sql_Consulta = new SqlDataAdapter();
            DataSet Ds_Sql_Consulta = new DataSet();
            String[] Datos_Poliza_PSP = null;
            String No_Poliza_PSP = String.Empty;
            String Tipo_Poliza_PSP = String.Empty;
            String Mes_Anio_PSP = String.Empty;
            DataTable Dt_Parametros_PSP = new DataTable();

            if (P_Cmmd != null)
            {
                Cmmd = P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {

                if (Importe != 0 && !String.IsNullOrEmpty(Cargo.Trim()) && !String.IsNullOrEmpty(Abono.Trim()))
                {
                    Dt_Parametros_PSP = Cls_Cat_Psp_Parametros_Presupuestales_Datos.Consultar_Parametros_Presupuestales();
                    Datos_Poliza_PSP = Crear_Poliza_Presupuestal_Egresos(Cargo, Abono, Importe.ToString().Trim(), Cmmd, Dt_Parametros_PSP);
                    //obtenemos los id de la poliza presupuestal creada
                    if (Datos_Poliza_PSP != null)
                    {
                        if (Datos_Poliza_PSP.Length > 0)
                        {
                            No_Poliza_PSP = Datos_Poliza_PSP[0].Trim();
                            Tipo_Poliza_PSP = Datos_Poliza_PSP[1].Trim();
                            Mes_Anio_PSP = Datos_Poliza_PSP[2].Trim();
                        }
                    }


                    Mi_SQL = "INSERT INTO " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos;
                    Mi_SQL = Mi_SQL + " (" + Ope_Psp_Registro_Movimientos.Campo_No_Reserva;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Cargo;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Abono;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Importe;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Fecha;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Usuario;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Usuario_Creo;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Fecha_Creo;
                    if (No_Poliza.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_No_Poliza;
                    if (Tipo_Poliza_ID.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Tipo_Poliza_ID;
                    if (Mes_Ano.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Mes_Ano;
                    if (Partida.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Partida;

                    if (No_Poliza_PSP.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_No_Poliza_Presupuestal;
                    if (Tipo_Poliza_PSP.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Tipo_Poliza_ID_Presupuestal;
                    if (Mes_Anio_PSP.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Mes_Anio_Presupuestal;

                    Mi_SQL = Mi_SQL + ") VALUES(";
                    Mi_SQL = Mi_SQL + No_Reserva;
                    Mi_SQL = Mi_SQL + ",'" + Cargo + "'";
                    Mi_SQL = Mi_SQL + ",'" + Abono + "'";
                    Mi_SQL = Mi_SQL + ",'" + Importe.ToString() + "'";
                    Mi_SQL = Mi_SQL + ",GETDATE()";
                    Mi_SQL = Mi_SQL + ",'" + Cls_Sessiones.Nombre_Empleado + "'";
                    Mi_SQL = Mi_SQL + ",'" + Cls_Sessiones.Nombre_Empleado + "'";
                    Mi_SQL = Mi_SQL + ",GETDATE()";
                    if (No_Poliza.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", '" + No_Poliza.Trim() + "'";
                    if (Tipo_Poliza_ID.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", '" + Tipo_Poliza_ID.Trim() + "'";
                    if (Mes_Ano.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", '" + Mes_Ano.Trim() + "'";
                    if (Partida.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + "," + Partida.Trim();

                    if (No_Poliza_PSP.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", '" + No_Poliza_PSP.Trim() + "'";
                    if (Tipo_Poliza_PSP.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", '" + Tipo_Poliza_PSP.Trim() + "'";
                    if (Mes_Anio_PSP.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", '" + Mes_Anio_PSP.Trim() + "'";

                    Mi_SQL = Mi_SQL + ")";



                    Mi_SQL = "INSERT INTO " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos;
                    Mi_SQL = Mi_SQL + " ( ";
                    Mi_SQL = Mi_SQL + " " + Ope_Psp_Registro_Movimientos.Campo_Cargo;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Abono;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Importe;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Fecha;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Usuario;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Usuario_Creo;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Fecha_Creo;
                    if (No_Poliza.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_No_Poliza;
                    if (Tipo_Poliza_ID.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Tipo_Poliza_ID;
                    if (Mes_Ano.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Mes_Ano;
                    if (Partida.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Partida;

                    if (No_Poliza_PSP.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_No_Poliza_Presupuestal;
                    if (Tipo_Poliza_PSP.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Tipo_Poliza_ID_Presupuestal;
                    if (Mes_Anio_PSP.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Mes_Anio_Presupuestal;

                    Mi_SQL = Mi_SQL + ") VALUES(";
                    Mi_SQL = Mi_SQL + "'" + Cargo + "'";
                    Mi_SQL = Mi_SQL + ",'" + Abono + "'";
                    Mi_SQL = Mi_SQL + ",'" + Importe.ToString() + "'";
                    Mi_SQL = Mi_SQL + ",GETDATE()";
                    Mi_SQL = Mi_SQL + ",'" + Cls_Sessiones.Nombre_Empleado + "'";
                    Mi_SQL = Mi_SQL + ",'" + Cls_Sessiones.Nombre_Empleado + "'";
                    Mi_SQL = Mi_SQL + ",GETDATE()";
                    if (No_Poliza.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", '" + No_Poliza.Trim() + "'";
                    if (Tipo_Poliza_ID.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", '" + Tipo_Poliza_ID.Trim() + "'";
                    if (Mes_Ano.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", '" + Mes_Ano.Trim() + "'";
                    if (Partida.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + "," + Partida.Trim();

                    if (No_Poliza_PSP.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", '" + No_Poliza_PSP.Trim() + "'";
                    if (Tipo_Poliza_PSP.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", '" + Tipo_Poliza_PSP.Trim() + "'";
                    if (Mes_Anio_PSP.Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", '" + Mes_Anio_PSP.Trim() + "'";

                    Mi_SQL = Mi_SQL + ")";



                    Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                    Registros_Guardados = Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                    // Registros_Guardados = SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (P_Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (SqlException Ex)
            {
                if (P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (P_Cmmd == null)
                {
                    Cn.Close();
                }

            }
            return Registros_Guardados;
        }
       
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Crear_Reserva
        ///DESCRIPCIÓN: Metodo que crea la reserva para un proximo gasto 
        ///PARAMETROS: String Dependencia_ID, String Fte_Financiamiento_ID, String Programa_ID, String Partida_ID,String Concepto,String Anio, double Importe
        ///CREO: Susana Trigueros Armenta 
        ///FECHA_CREO: 16/Nov/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Crear_Reserva(String Dependencia_ID, String Fte_Financiamiento_ID, String Programa_ID, String Partida_ID, String Concepto, String Anio, double Importe)
        {
            int No_Reserva = Obtener_Consecutivo(Ope_Psp_Reservas.Campo_No_Reserva, Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
            String Mi_SQL = "";

            Mi_SQL = "INSERT INTO " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
            Mi_SQL = Mi_SQL + "(" + Ope_Psp_Reservas.Campo_No_Reserva;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Concepto;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Fecha;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Dependencia_ID;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Fte_Financimiento_ID;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Proyecto_Programa_ID;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Partida_ID;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Capitulo_ID;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Anio;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Importe_Inicial;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Saldo;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Usuario_Creo;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Fecha_Creo;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Tipo_Reserva;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Recurso;
            //Mi_SQL = Mi_SQL + ", " + "BENEFICIARIO";
            Mi_SQL = Mi_SQL + ") VALUES(";
            Mi_SQL = Mi_SQL + No_Reserva;
            Mi_SQL = Mi_SQL + ",'" + Concepto + "'";
            Mi_SQL = Mi_SQL + ",GETDATE()";
            Mi_SQL = Mi_SQL + ",'" + Dependencia_ID.Trim() + "'";
            Mi_SQL = Mi_SQL + ",'" + Fte_Financiamiento_ID.Trim() + "'";
            Mi_SQL = Mi_SQL + ",'" + Programa_ID.Trim() + "'";
            Mi_SQL = Mi_SQL + ",'" + Partida_ID.Trim() + "'";
            Mi_SQL = Mi_SQL + ",(SELECT " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto;
            Mi_SQL = Mi_SQL + " JOIN " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas;
            Mi_SQL = Mi_SQL + " ON " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID;
            Mi_SQL = Mi_SQL + "=" + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID;
            Mi_SQL = Mi_SQL + " JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
            Mi_SQL = Mi_SQL + " ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID;
            Mi_SQL = Mi_SQL + "=" + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID;
            Mi_SQL = Mi_SQL + "='" + Partida_ID.Trim() + "')";
            Mi_SQL = Mi_SQL + ", " + Anio;
            Mi_SQL = Mi_SQL + ", " + Importe;
            Mi_SQL = Mi_SQL + ", " + Importe;
            Mi_SQL = Mi_SQL + ",'" + Cls_Sessiones.Nombre_Empleado + "'";
            Mi_SQL = Mi_SQL + ",GETDATE(),'UNICA','RECURSO ASIGNADO')";

            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

            return No_Reserva;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Crear_Reserva
        ///DESCRIPCIÓN: Metodo que crea la reserva para un proximo gasto 
        ///PARAMETROS: String Dependencia_ID,String Estatus, String Beneficiario, String Fte_Financiamiento_ID, String P_Programa_ID, String Partida_ID,String Concepto,String Anio, double Importe
        ///CREO: Susana Trigueros Armenta 
        ///FECHA_CREO: 30/Nov/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Crear_Reserva(String Dependencia_ID, String Estatus, String Beneficiario, String Fte_Financiamiento_ID, String Programa_ID, String Partida_ID, String Concepto, String Anio, double Importe)
        {
            int No_Reserva = Obtener_Consecutivo(Ope_Psp_Reservas.Campo_No_Reserva, Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
            String Mi_SQL = "";

            Mi_SQL = "INSERT INTO " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
            Mi_SQL = Mi_SQL + "(" + Ope_Psp_Reservas.Campo_No_Reserva;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Estatus;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Beneficiario;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Concepto;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Fecha;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Dependencia_ID;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Fte_Financimiento_ID;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Proyecto_Programa_ID;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Partida_ID;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Capitulo_ID;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Anio;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Importe_Inicial;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Saldo;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Usuario_Creo;
            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Fecha_Creo;
            Mi_SQL = Mi_SQL + ") VALUES(";
            Mi_SQL = Mi_SQL + No_Reserva;
            Mi_SQL = Mi_SQL + ",'" + Estatus + "'";
            Mi_SQL = Mi_SQL + ",'" + Beneficiario + "'";
            Mi_SQL = Mi_SQL + ",'" + Concepto + "'";
            Mi_SQL = Mi_SQL + ",GETDATE()";
            Mi_SQL = Mi_SQL + ",'" + Dependencia_ID.Trim() + "'";
            Mi_SQL = Mi_SQL + ",'" + Fte_Financiamiento_ID.Trim() + "'";
            Mi_SQL = Mi_SQL + ",'" + Programa_ID.Trim() + "'";
            Mi_SQL = Mi_SQL + ",'" + Partida_ID.Trim() + "'";
            Mi_SQL = Mi_SQL + ",(SELECT " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto;
            Mi_SQL = Mi_SQL + " JOIN " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas;
            Mi_SQL = Mi_SQL + " ON " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID;
            Mi_SQL = Mi_SQL + "=" + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID;
            Mi_SQL = Mi_SQL + " JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
            Mi_SQL = Mi_SQL + " ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID;
            Mi_SQL = Mi_SQL + "=" + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID;
            Mi_SQL = Mi_SQL + "='" + Partida_ID.Trim() + "')";
            Mi_SQL = Mi_SQL + ", " + Anio;
            Mi_SQL = Mi_SQL + ", " + Importe;
            Mi_SQL = Mi_SQL + ", " + Importe;
            Mi_SQL = Mi_SQL + ",'" + Cls_Sessiones.Nombre_Empleado + "'";
            Mi_SQL = Mi_SQL + ",GETDATE())";

            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

            return No_Reserva;

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Crear_Reserva
        ///DESCRIPCIÓN          : Metodo que crea la reserva para un proximo gasto 
        ///PARAMETROS           : String Dependencia_ID,String Estatus, String Beneficiario, String Fte_Financiamiento_ID, String P_Programa_ID, String Partida_ID,String Concepto,String Anio, double Importe
        ///CREO                 : Susana Trigueros Armenta 
        ///FECHA_CREO           : 30/Nov/2011
        ///MODIFICO             : Leslie Gonzalez Vazquez
        ///FECHA_MODIFICO       : 20/Enero/2012
        ///CAUSA_MODIFICACIÓN   : Se agregaron los detalles de las reservas
        ///*******************************************************************************
        public static int Crear_Reserva(String Dependencia_ID, String Estatus, String Beneficiario, String Fte_Financiamiento_ID, String Programa_ID, String Concepto, String Anio, double Importe, String Proveedor_ID, String Empleado_ID, String Tipo_Solicitud_ID, DataTable Dt_Detalles, String Recurso)
        {
            int No_Reserva = Obtener_Consecutivo(Ope_Psp_Reservas.Campo_No_Reserva, Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
            String Mi_SQL = "";
            Int32 No_Reserva_Detalle;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;

            try
            {
                Mi_SQL = "INSERT INTO " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
                Mi_SQL = Mi_SQL + "(" + Ope_Psp_Reservas.Campo_No_Reserva;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Estatus;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Beneficiario;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Concepto;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Fecha;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Dependencia_ID;
                //Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Fte_Financimiento_ID;
                //Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Proyecto_Programa_ID;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Anio;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Importe_Inicial;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Saldo;
                if (Proveedor_ID.ToString() != String.Empty)
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Proveedor_ID;
                if (Empleado_ID.ToString() != String.Empty)
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Empleado_ID;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Tipo_Solicitud_Pago_ID;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Recurso;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Usuario_Creo;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Fecha_Creo;
                Mi_SQL = Mi_SQL + ") VALUES(";
                Mi_SQL = Mi_SQL + No_Reserva;
                Mi_SQL = Mi_SQL + ",'" + Estatus + "'";
                Mi_SQL = Mi_SQL + ",'" + Beneficiario + "'";
                Mi_SQL = Mi_SQL + ",'" + Concepto + "'";
                Mi_SQL = Mi_SQL + ",GETDATE()";
                Mi_SQL = Mi_SQL + ",'" + Dependencia_ID.Trim() + "'";
                //Mi_SQL = Mi_SQL + ",'" + Fte_Financiamiento_ID.Trim() + "'";
                //Mi_SQL = Mi_SQL + ",'" + Programa_ID.Trim() + "'";
                Mi_SQL = Mi_SQL + ", " + Anio;
                Mi_SQL = Mi_SQL + ", " + Importe;
                Mi_SQL = Mi_SQL + ", " + Importe;
                if (Proveedor_ID.ToString() != String.Empty)
                Mi_SQL = Mi_SQL + ", '" + Proveedor_ID + "'";
                if (Empleado_ID.ToString() != String.Empty)
                Mi_SQL = Mi_SQL + ", '" + Empleado_ID + "'";
                Mi_SQL = Mi_SQL + ", '" + Tipo_Solicitud_ID + "'";
                Mi_SQL = Mi_SQL + ", '" + Recurso + "'";
                Mi_SQL = Mi_SQL + ",'" + Cls_Sessiones.Nombre_Empleado + "'";
                Mi_SQL = Mi_SQL + ",GETDATE())";

                Cmd.CommandText = Mi_SQL.ToString();
                Cmd.ExecuteNonQuery();

                No_Reserva_Detalle = Obtener_Consecutivo(Ope_Psp_Reservas_Detalles.Campo_No_Reserva_Detalle, Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);

                foreach(DataRow Dr in Dt_Detalles.Rows)
                {
                    StringBuilder Mi_Sql = new StringBuilder();

                    Mi_Sql.Append("INSERT INTO " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "(");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_No_Reserva_Detalle + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_No_Reserva + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Partida_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Saldo + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Usuario_Creo + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Fecha_Creo + ")");
                    Mi_Sql.Append(" VALUES(" + No_Reserva_Detalle + ", ");
                    Mi_Sql.Append(No_Reserva + ", ");
                    Mi_Sql.Append("'" + Dr["PARTIDA_ID"].ToString() + "', ");
                    Mi_Sql.Append(Dr["IMPORTE"].ToString().Replace(",","") + ", ");
                    Mi_Sql.Append(Dr["IMPORTE"].ToString().Replace(",", "") + ", ");
                    Mi_Sql.Append("'" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString() + "', ");
                    Mi_Sql.Append("'" + Dr["PROGRAMA_ID"].ToString() + "', ");
                    Mi_Sql.Append("'" + Cls_Sessiones.Nombre_Empleado + "', ");
                    Mi_Sql.Append("GETDATE())");
                    Cmd.CommandText = Mi_Sql.ToString();
                    Cmd.ExecuteNonQuery();

                    No_Reserva_Detalle++;
                }

                Trans.Commit();
            }
            catch (Exception Ex)
            {
                Trans.Rollback();
                throw new Exception("Error al quere dar de alta los datos de las reservas. Error[" + Ex.Message + "]");
            }
            return No_Reserva;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Crear_Reserva
        ///DESCRIPCIÓN          : Metodo que crea la reserva para un proximo gasto 
        ///PARAMETROS           : String Dependencia_ID,String Estatus, String Beneficiario, String Fte_Financiamiento_ID, String P_Programa_ID, String Partida_ID,String Concepto,String Anio, double Importe
        ///CREO                 : Sergio Manuel Gallardo
        ///FECHA_CREO           : 30/Nov/2011
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        public static int Crear_Reserva(String Dependencia_ID, String Estatus, String Beneficiario, String Fte_Financiamiento_ID, String Programa_ID, String Concepto, String Anio, double Importe, String Proveedor_ID, String Empleado_ID, String Tipo_Solicitud_ID, DataTable Dt_Detalles, String Recurso, SqlCommand P_Cmmd)
        {
            String Mi_SQL = "";
            Int32 No_Reserva_Detalle;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            if (P_Cmmd != null)
            {
                Cmmd = P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                int No_Reserva = Obtener_Consecutivo(Ope_Psp_Reservas.Campo_No_Reserva, Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas, Cmmd);
                Mi_SQL = "INSERT INTO " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
                Mi_SQL = Mi_SQL + "(" + Ope_Psp_Reservas.Campo_No_Reserva;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Estatus;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Beneficiario;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Concepto;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Fecha;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Anio;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Importe_Inicial;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Saldo;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Proveedor_ID;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Empleado_ID;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Tipo_Solicitud_Pago_ID;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Recurso;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Usuario_Creo;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Fecha_Creo;
                Mi_SQL = Mi_SQL + ") VALUES(";
                Mi_SQL = Mi_SQL + No_Reserva;
                Mi_SQL = Mi_SQL + ",'" + Estatus + "'";
                Mi_SQL = Mi_SQL + ",'" + Beneficiario + "'";
                Mi_SQL = Mi_SQL + ",'" + Concepto + "'";
                Mi_SQL = Mi_SQL + ",GETDATE()";
                if (!String.IsNullOrEmpty(Dependencia_ID))
                {
                    Mi_SQL = Mi_SQL + ",'" + Dependencia_ID.Trim() + "'";
                }
                else
                {
                    Mi_SQL = Mi_SQL + ",NULL";
                }
                Mi_SQL = Mi_SQL + ", " + Anio;
                Mi_SQL = Mi_SQL + ", " + Importe;
                Mi_SQL = Mi_SQL + ", " + Importe;
                //Verificar si es nulo
                if (String.IsNullOrEmpty(Proveedor_ID) == false)
                {
                    Mi_SQL = Mi_SQL + ", '" + Proveedor_ID + "'";
                }
                else
                {
                    Mi_SQL = Mi_SQL + ", NULL";
                }
                //verificar si es nulo
                if (String.IsNullOrEmpty(Empleado_ID) == false)
                {
                    Mi_SQL = Mi_SQL + ", '" + Empleado_ID + "'";
                }
                else
                {
                    Mi_SQL = Mi_SQL + ", NULL";
                }
                Mi_SQL = Mi_SQL + ", '" + Tipo_Solicitud_ID + "'";
                Mi_SQL = Mi_SQL + ", '" + Recurso + "'";
                Mi_SQL = Mi_SQL + ",'" + Cls_Sessiones.Nombre_Empleado + "'";
                Mi_SQL = Mi_SQL + ",GETDATE())";
                Cmmd.CommandText = Mi_SQL.ToString();
                Cmmd.ExecuteNonQuery();
                No_Reserva_Detalle = Obtener_Consecutivo(Ope_Psp_Reservas_Detalles.Campo_No_Reserva_Detalle, Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles, Cmmd);
                foreach (DataRow Dr in Dt_Detalles.Rows)
                {
                    StringBuilder Mi_Sql = new StringBuilder();
                    Mi_Sql.Append("INSERT INTO " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "(");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_No_Reserva_Detalle + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_No_Reserva + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Partida_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Saldo + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Usuario_Creo + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Fecha_Creo + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Capitulo_ID + ")");
                    Mi_Sql.Append(" VALUES(" + No_Reserva_Detalle + ", ");
                    Mi_Sql.Append(No_Reserva + ", ");
                    Mi_Sql.Append("'" + Dr["PARTIDA_ID"].ToString() + "', ");
                    Mi_Sql.Append(Dr["IMPORTE"].ToString().Replace(",", "") + ", ");
                    Mi_Sql.Append(Dr["IMPORTE"].ToString().Replace(",", "") + ", ");
                    Mi_Sql.Append("'" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString() + "', ");
                    Mi_Sql.Append("'" + Dr["PROGRAMA_ID"].ToString() + "', ");
                    if (!String.IsNullOrEmpty(Dr["DEPENDENCIA_ID"].ToString()))
                    {
                        Mi_Sql.Append("'" + Dr["DEPENDENCIA_ID"].ToString() + "', ");
                    }
                    else
                    {
                        Mi_Sql.Append(",NULL");
                    }
                    Mi_Sql.Append("'" + Cls_Sessiones.Nombre_Empleado + "', ");
                    Mi_Sql.Append("GETDATE(), ");
                    Mi_Sql.Append("'" + Dr["CAPITULO_ID"].ToString().Trim() + "')");
                    Cmmd.CommandText = Mi_Sql.ToString();
                    Cmmd.ExecuteNonQuery();

                    No_Reserva_Detalle++;
                }

                if (P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return No_Reserva;
            }
            catch (SqlException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Modificar_Reserva
        /// DESCRIPCION : Modifica la reserva con los datos proporcionados por el usuario
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 22/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static Boolean Modificar_Reserva(DataTable Dt_Partidas_Reserva, String Tipo_Modificacion, Double Importe_Total_Modificado, SqlCommand Command)
        {
            Cls_Ope_Con_Reservas_Negocio Rs_Reserva = new Cls_Ope_Con_Reservas_Negocio(); //Variable de conexion hacia la capa de negocios
            int Registro_Presupuestal;
            Boolean Modificado = true;
            SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
            SqlCommand Comando_SQL = new SqlCommand();                                    //Sirve para la ejecución de las operaciones a la base de datos
            SqlTransaction Transaccion_SQL = null;    //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            if (Command == null)
            {
                if (Conexion_Base.State != ConnectionState.Open)
                {
                    Conexion_Base.Open(); //Abre la conexión a la base de datos            
                }
                Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
                Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
                Comando_SQL.Transaction = Transaccion_SQL;                                       //Abre la transacción para la ejecución en la base de datos
            }
            else
            {
                Comando_SQL = Command;
            }
            try
            {
                if (Tipo_Modificacion == "AMPLIACION")
                {
                    Registro_Presupuestal = Actualizar_Momentos_Presupuestales_Mensual("PRE_COMPROMETIDO", "DISPONIBLE", Dt_Partidas_Reserva, Comando_SQL);
                }
                else
                {
                    Registro_Presupuestal = Actualizar_Momentos_Presupuestales_Mensual("DISPONIBLE", "PRE_COMPROMETIDO", Dt_Partidas_Reserva, Comando_SQL);
                }
                if (Registro_Presupuestal > 0)
                {
                    if (Tipo_Modificacion == "AMPLIACION")
                    {
                        Registro_Movimiento_Presupuestal(Dt_Partidas_Reserva.Rows[0]["NO_RESERVA"].ToString(), "PRE_COMPROMETIDO", "DISPONIBLE", Importe_Total_Modificado, "", "", "", "", Comando_SQL); //Agrega el historial del movimiento de la partida presupuestal
                    }
                    else
                    {
                        Registro_Movimiento_Presupuestal(Dt_Partidas_Reserva.Rows[0]["NO_RESERVA"].ToString(), "DISPONIBLE", "PRE_COMPROMETIDO", Importe_Total_Modificado, "", "", "", "", Comando_SQL); //Agrega el historial del movimiento de la partida presupuestal
                    }
                    //Afectar el saldo de la reserva
                    Rs_Reserva.P_No_Reserva = Dt_Partidas_Reserva.Rows[0]["NO_RESERVA"].ToString();
                    Rs_Reserva.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado;
                    Rs_Reserva.P_Dt_Detalles_Reserva = Dt_Partidas_Reserva;
                    Rs_Reserva.P_Tipo_Modificacion = Tipo_Modificacion;
                    Rs_Reserva.P_Importe = Convert.ToString(Importe_Total_Modificado);
                    Rs_Reserva.P_Cmmd = Comando_SQL;
                    Rs_Reserva.Modificar_Saldos_Reserva(); //Modifica el registro en base a los datos proporcionado
                    if (Command == null)
                    {
                        Transaccion_SQL.Commit();
                    }
                }
                else
                {
                    if (Command == null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    Modificado = false;
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "RESERVAS", "alert('La Modificación de la reserva no se pudo efectuar debido a  que no hay suficiencia presupuestal');", true);
                }
            }
            catch (SqlException Ex)
            {
                if (Command == null)
                {
                    Transaccion_SQL.Rollback();
                }
            }
            finally
            {
                if (Command == null)
                {
                    Conexion_Base.Close();
                }
            }
            return Modificado;
        }
   

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Consecutivo
        ///DESCRIPCIÓN: Obtiene el numero consecutivo para las tablas ocupadas en esta clase
        ///PARAMETROS: 1.-Campo del cual se obtendra el consecutivo
        ///            2.-Nombre de la tabla
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 10/Enero/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Obtener_Consecutivo(String Campo_ID, String Tabla)
        {
            int Consecutivo = 0;
            String Mi_Sql;
            Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos
            Mi_Sql = "SELECT ISNULL(MAX (" + Campo_ID + "),'00000') FROM " + Tabla;
            Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            Consecutivo = (Convert.ToInt32(Obj) + 1);
            return Consecutivo;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Consecutivo
        ///DESCRIPCIÓN: Obtiene el numero consecutivo para las tablas ocupadas en esta clase
        ///PARAMETROS: 1.-Campo del cual se obtendra el consecutivo
        ///            2.-Nombre de la tabla
        ///CREO: Sergio Manuel Gallardo Andrade
        ///FECHA_CREO: 10/Octubre/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Obtener_Consecutivo(String Campo_ID, String Tabla, SqlCommand Cmmd)
        {
            int Consecutivo = 0;
            String Mi_Sql;
            Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos
            try
            {
                Mi_Sql = "SELECT ISNULL(MAX (" + Campo_ID + "),'00000') FROM " + Tabla;
                Cmmd.CommandText = Mi_Sql; //Realiza la ejecuón de la obtención del ID del empleado
                Obj = Cmmd.ExecuteScalar();
                // Obj = OracleHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                Consecutivo = (Convert.ToInt32(Obj) + 1);
            }
            catch (SqlException Ex)
            {
                new Exception(Ex.Message);
            }
            return Consecutivo;
        }

        public static DataTable Consultar_Reservas(String Fecha_Inico, String Fecha_Fin, int No_Reserva)
        {
            DataTable Dt_Historial = null;
            String Mi_SQL = "SELECT * FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
            Mi_SQL += " WHERE TO_DATE(TO_CHAR(" + Ope_Psp_Reservas.Campo_Fecha + ",'DD-MM-YYYY'))" +
                        " >= '" + Fecha_Inico + "' AND " +
                "TO_DATE(TO_CHAR(" + Ope_Psp_Reservas.Campo_Fecha + ",'DD-MM-YYYY'))" +
                        " <= '" + Fecha_Fin + "'";
            if (No_Reserva > 0)
            {
                Mi_SQL = "SELECT * FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
                Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + No_Reserva;
            }
            Mi_SQL += "ORDER BY " + Ope_Psp_Reservas.Campo_No_Reserva + " DESC";
            try
            {
                Dt_Historial = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
            return Dt_Historial;
        }

        //modificado 24 04 2012 GAC
        public static DataTable Consultar_Reservas_De_Requisicion(String No_Requisicion)
        {
            DataTable Dt_Reserva = null;
            String Mi_SQL = "SELECT " +
            Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + ".*" +
            " FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + ", " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
            " WHERE " +
            Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + " = " +
            Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + ".NUM_RESERVA AND " +
            Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + ".NO_REQUISICION = " + No_Requisicion;
            try
            {
                Dt_Reserva = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
            return Dt_Reserva;
        }

        public static DataTable Consultar_Reservas_De_Compras(String Orden_Compra)
        {
            DataTable Dt_Reserva = null;
            String Mi_SQL = "SELECT " +
            Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + ".*" +
            " FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + ", " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra +
            " WHERE " +
            Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + " = " +
            Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Reserva + " AND " +
            Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + "= '" + Orden_Compra + "'";
            try
            {
                Dt_Reserva = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
            return Dt_Reserva;
        }


        public static DataTable Consultar_Reservas(String Fecha_Inico, String Fecha_Fin, int No_Reserva, String Empleado_ID, String Proveedor_ID)
        {
            DataTable Dt_Historial = null;
            String Mi_SQL = "SELECT * FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
            //////Mi_SQL += " WHERE REPLACE(CONVERT(VARCHAR(11)," + Ope_Psp_Reservas.Campo_Fecha + ",106),' ','/')" +
            //////            " >= '" + Fecha_Inico + "' AND " +
            //////    "REPLACE(CONVERT(VARCHAR(11)," + Ope_Psp_Reservas.Campo_Fecha + ",106),' ','/')" +
            //////            " <= '" + Fecha_Fin + "'";
            Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_Fecha + " >= '" + string.Format("{0:dd/MM/yyyy}", Fecha_Inico) + " 00:00:00' " +
                "AND " + Ope_Psp_Reservas.Campo_Fecha + " <= '" + string.Format("{0:dd/MM/yyyy}", Fecha_Fin) + " 23:59:59' ";

            if (No_Reserva > 0)
            {
                Mi_SQL = "SELECT * FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
                Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + No_Reserva;
            }
            if (Empleado_ID.Trim() != String.Empty)
            {
                Mi_SQL = Mi_SQL + " AND " + Ope_Psp_Reservas.Campo_Empleado_ID + "='" + Empleado_ID + "'";
            }

            if (Proveedor_ID.Trim() != String.Empty)
            {
                Mi_SQL = Mi_SQL + " AND " + Ope_Psp_Reservas.Campo_Proveedor_ID + "='" + Proveedor_ID + "'";
            }


            Mi_SQL += " ORDER BY " + Ope_Psp_Reservas.Campo_No_Reserva + " DESC";
            try
            {
                Dt_Historial = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
            return Dt_Historial;
        }
        public static DataTable Consultar_Historial_Reservas(int No_Reserva)
        {
            DataTable Dt_Historial = null;
            String Mi_SQL = "SELECT * FROM " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos;
            if (No_Reserva > 0)
            {
                Mi_SQL += " WHERE " + Ope_Psp_Registro_Movimientos.Campo_No_Reserva + " = " + No_Reserva;
            }
            try
            {
                Dt_Historial = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
            return Dt_Historial;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Actualizar_Reserva_De_Compra
        ///DESCRIPCIÓN: Se actualiza la reserva cuando se hace la orden de compra
        ///PARAMETROS:
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 24/Abr/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Actualizar_Reserva_De_Compra(int No_Reserva, String Proveedor_ID, String Beneficiario)
        {
            int Renglones_Actualizados = 0;
            String Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas +
            " SET " + Ope_Psp_Reservas.Campo_Proveedor_ID + " = '" + Proveedor_ID + "', " +
            Ope_Psp_Reservas.Campo_Beneficiario + " = '" + Beneficiario + "'" +
            " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + No_Reserva;
            try
            {
                Renglones_Actualizados = SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
            return Renglones_Actualizados;
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Codigo_Programatico_De_Reserva
        ///DESCRIPCIÓN: Metodo que consulta todo el Codigo Programatico de la reserva
        ///PARAMETROS: String No_Reserva
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 15/Nov/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Detalles_Reserva(String No_Reserva)
        {
            //COnsultar tabla de Reservas
            DataTable Dt_Codigo_Programatico = null;
            String Mi_SQL = "";
            try
            {
                Mi_SQL = "SELECT * FROM " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles +
                " WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva +
                " = " + No_Reserva + " AND " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva_Detalle + " = " +
                "(" +
                    "SELECT MAX(" + Ope_Psp_Reservas_Detalles.Campo_No_Reserva_Detalle + ") FROM " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles +
                    " WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = " + No_Reserva +
                ")";
                Dt_Codigo_Programatico = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
            return Dt_Codigo_Programatico;
        }

        #endregion

        #region TRASPASOS ALMACEN

        ///*****************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Traspaso_Consumo_Stock_Almacen
        ///DESCRIPCIÓN          : consulta para actualizar los datos del presupuesto de egresos mensual
        ///PARAMETROS           : 
        ///CREO                 : Susana Trigueros Armenta
        ///FECHA_CREO           : 07/Diciembre/20 12
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************
        public static string Traspaso_Consumo_Stock_Almacen(Double Total, String No_Requisicion, SqlCommand P_Comando)
        {
            //OBTENEMOS EL MES ACTUAL QUE AFECTAREMOS
            String Mes_Actual = String.Format("{0:MM}", DateTime.Now);
            String Mensaje="";
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans= null;          

            if (P_Comando != null)
            {
                Cmd = P_Comando;
            }
            else
            {
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Transaction = Trans;
                Cmd.Connection = Cn;
            }


            try
            {
            //obtenemos las columnas que afectaremos
            String Disponible_mes = Obtener_Columna(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible.Trim(), Mes_Actual);
            Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado );
            Mi_SQL.Append(" SET " + Disponible_mes + "= " + Disponible_mes +" + " + Total);
            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
            Mi_SQL.Append(" = (SELECT " + Cat_Alm_Parametros_Listado.Campo_Unidad_Responsable_ID);
            Mi_SQL.Append(" FROM " + Cat_Alm_Parametros_Listado.Tabla_Cat_Alm_Parametros_Listado + ")");
            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
            Mi_SQL.Append(" = (SELECT " + Cat_Alm_Parametros_Listado.Campo_Proyecto_Programa_ID);
            Mi_SQL.Append(" FROM " + Cat_Alm_Parametros_Listado.Tabla_Cat_Alm_Parametros_Listado + ")");
            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
            Mi_SQL.Append(" = " + DateTime.Now.Year);
            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
            Mi_SQL.Append(" = (SELECT DISTINCT " + Ope_Com_Req_Producto.Campo_Partida_ID + " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto);
            Mi_SQL.Append(" WHERE " + Ope_Com_Req_Producto.Campo_Requisicion_ID + "=" + No_Requisicion.Trim() + ")");
            
            Cmd.CommandText = Mi_SQL.ToString();
            int registro = Cmd.ExecuteNonQuery();

            if (registro == 1)
            {
                Mensaje = "EXITO";
            }
            else
            {
                new Exception();
            }
            }
            catch (Exception ex)
            {
                ex.ToString();
                Trans.Rollback();
                Mensaje = "No se pudo realizar el traspaso al presupuesto de STOCK";
            }
            finally
            {
                if (P_Comando == null)
                {
                    Cn.Close();
                    Cmd = null;
                    Cn = null;
                    Trans = null;
                }
            }
            return Mensaje;
        }
        #endregion

        #region METODOS PRESUPUESTO MENSUAL

        ///*****************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Actualizar_Momentos_Presupuestales_Mensual
        ///DESCRIPCIÓN          : consulta para actualizar los datos del presupuesto de egresos mensual
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 07/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************
        public static int Actualizar_Momentos_Presupuestales_Mensual(String Cargo, String Abono, DataTable Dt_Detalles, SqlCommand P_Comando)
        {
            int Registros_Afectados = 0;
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion = new SqlConnection();//Variable para la conexión para la base de datos   
            SqlCommand Comando = new SqlCommand();//Sirve para la ejecución de las operaciones a la base de datos
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            DataTable Dt_Psp = new DataTable();
            String Mes_Actual = String.Empty;
            String Columna_Actual = String.Empty;
            String Columna_Anterior = String.Empty;
            Double Importe = 0.00;
            String Columna_Afectado = String.Empty;

            if (P_Comando != null)
            {
                Comando = P_Comando;
            }
            else
            {
                Conexion.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Conexion.Open();
                Transaccion = Conexion.BeginTransaction();
                Comando.Transaction = Transaccion;
                Comando.Connection = Conexion;
            }


            try
            {
                //validamos si se realizara una afectación de cierre de mes
                if (Acumular_Saldos_Fin_Mes(Comando))
                {
                    Dt_Psp = Dt_Detalles; //obtenemos los detalles del presupuesto

                    //VALIDAMOS QUE LOS DATOS PRINCIPALES NO VENGAN NULOS
                    if (Dt_Psp != null && !String.IsNullOrEmpty(Cargo.Trim())
                        && !String.IsNullOrEmpty(Abono.Trim()))
                    {
                        if (Dt_Psp.Rows.Count > 0)
                        {
                            //OBTENEMOS EL MES ACTUAL QUE AFECTAREMOS
                            Mes_Actual = String.Format("{0:MM}", DateTime.Now);

                            //obtenemos las columnas que afectaremos
                            Columna_Actual = Obtener_Columna(Cargo.Trim(), Mes_Actual);
                            Columna_Anterior = Obtener_Columna(Abono.Trim(), Mes_Actual);

                            //VALIDAMOS QUE NINGUN IMPORTE SOBREPASE EL IMPORTE PERMITIDO
                            if (Validar_Dt_Registros(Dt_Psp, Columna_Anterior, Mes_Actual, P_Comando))
                            {
                                foreach (DataRow Dr in Dt_Psp.Rows)
                                {
                                    //LIMPIAMOS LAS VARIABLES
                                    Importe = 0.00;

                                    //VALIDAMOS QUE EXISTAN TODOS LOS CAMPOS PARA PODER HACER LA AFECTACION
                                    if (!String.IsNullOrEmpty(Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim())
                                        && !String.IsNullOrEmpty(Dr["PROGRAMA_ID"].ToString().Trim())
                                        && !String.IsNullOrEmpty(Dr["DEPENDENCIA_ID"].ToString().Trim())
                                        && !String.IsNullOrEmpty(Dr["PARTIDA_ID"].ToString().Trim())
                                        && !String.IsNullOrEmpty(Dr["ANIO"].ToString().Trim())
                                        && !String.IsNullOrEmpty(Dr["IMPORTE"].ToString().Trim()))
                                    {
                                        Importe = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE"].ToString().Trim()) ? "0" : Dr["IMPORTE"].ToString().Trim());

                                        //modificamoes el presupuesto
                                        Mi_SQL = new StringBuilder();
                                        Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " SET ");
                                        //MODIFICAMOS LA COLUMNA GLOBAL DEL MOVIMIENTO ACTUAL 
                                        Mi_SQL.Append(Cargo.Trim() + " = ");
                                        Mi_SQL.Append(" ISNULL(" + Cargo.Trim() + ", 0) + " + Importe + ", ");
                                        //MODIFICAMOS LA COLUMNA DEL MES CORRESPONDIENTE AL MOVIMIENTO ACTUAL
                                        Mi_SQL.Append(Columna_Actual.Trim() + " = ISNULL(" + Columna_Actual.Trim() + ", 0) + " + Importe + ", ");
                                        //MODIFICAMOS LA COLUMNA GLOBAL DEL MOVIMIENTO ANTERIOR
                                        Mi_SQL.Append(Abono.Trim() + " = ");
                                        Mi_SQL.Append(" ISNULL(" + Abono.Trim() + ", 0) - " + Importe + ", ");
                                        //MODIFICAMOS LA COLUMNA DEL MES CORRESPONDIENTE AL MES ANTERIOR
                                        Mi_SQL.Append(Columna_Anterior.Trim() + " = ISNULL(" + Columna_Anterior.Trim() + ", 0) - " + Importe + ", ");
                                        Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado.Trim() + "'");
                                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                        Mi_SQL.Append(" = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                        Mi_SQL.Append(" = '" + Dr["PROGRAMA_ID"].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                        Mi_SQL.Append(" = '" + Dr["DEPENDENCIA_ID"].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                                        Mi_SQL.Append(" = '" + Dr["PARTIDA_ID"].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                                        Mi_SQL.Append(" = " + Dr["ANIO"].ToString().Trim());

                                        Comando.CommandText = Mi_SQL.ToString();
                                        Registros_Afectados += Comando.ExecuteNonQuery();

                                    }//fin validacion de los campos del datatable
                                }//fin foreach
                            }

                            if (P_Comando == null)
                            {
                                Transaccion.Commit();//aceptamos los cambios
                            }
                        }
                    }
                }
            }
            catch (SqlException Ex)
            {
                Transaccion.Rollback();
                Mensaje = "Error:  [" + Ex.Message + "]";
                throw new Exception(Mensaje, Ex);
            }
            catch (DBConcurrencyException Ex)
            {
                Transaccion.Rollback();
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                Transaccion.Rollback();
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (P_Comando == null)
                {
                    Conexion.Close();
                    Comando = null;
                    Conexion = null;
                    Transaccion = null;
                }
            }
            return Registros_Afectados;
        }

        ///*****************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Validar_Dt_Registros
        ///DESCRIPCIÓN          : consulta para validar los datos del datatable antes de realizar alguna modificacion al presupuesto
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 07/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************
        public static Boolean Validar_Dt_Registros(DataTable Dt_Psp, String Columna_Anterior, String Mes_Actual, SqlCommand P_Comando)
        {
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            Boolean Datos_Validos = true;
            Double Importe = 0.00;
            Double Importe_Disponible = 0.00;

            try
            {
                foreach (DataRow Dr in Dt_Psp.Rows)
                {
                    Importe = 0.00;

                    //VALIDAMOS QUE EXISTAN TODOS LOS CAMPOS PARA PODER HACER LA AFECTACION
                    if (!String.IsNullOrEmpty(Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim())
                        && !String.IsNullOrEmpty(Dr["PROGRAMA_ID"].ToString().Trim())
                        && !String.IsNullOrEmpty(Dr["DEPENDENCIA_ID"].ToString().Trim())
                        && !String.IsNullOrEmpty(Dr["PARTIDA_ID"].ToString().Trim())
                        && !String.IsNullOrEmpty(Dr["ANIO"].ToString().Trim())
                        && !String.IsNullOrEmpty(Dr["IMPORTE"].ToString().Trim()))
                    {
                        Importe = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE"].ToString().Trim()) ? "0" : Dr["IMPORTE"].ToString().Trim());

                        //consultamos si hay suficiencia en la partida para realizar el cambio
                        Importe_Disponible = Consultar_Importe_Psp(Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim(),
                            Dr["PROGRAMA_ID"].ToString().Trim(), Dr["DEPENDENCIA_ID"].ToString().Trim(), Dr["PARTIDA_ID"].ToString().Trim(),
                            Dr["ANIO"].ToString().Trim(), Columna_Anterior, P_Comando);

                        if (Importe > Importe_Disponible)
                        {
                            Datos_Validos = false;
                        }
                        
                    }//fin validacion de los campos del datatable
                }//fin foreach
            }
            catch (SqlException Ex)
            {
                throw new Exception(Mensaje, Ex);
            }
            finally
            {
            }

            return Datos_Validos;
        }

        ///*****************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Importe_Psp
        ///DESCRIPCIÓN          : consulta para obtener el importe de una partida
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 07/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************
        public static Double Consultar_Importe_Psp(String Fte_Financiamiento_ID, String Programa_ID, String Depedencia_ID,
            String Partida_ID, String Anio, String Columna, SqlCommand P_Comando)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            Double Importe = 0.00;
            object Obj_Importe;
            String Campo_Importe = String.Empty;
            SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion = new SqlConnection();//Variable para la conexión para la base de datos   
            SqlCommand Comando = new SqlCommand();//Sirve para la ejecución de las operaciones a la base de datos

            if (P_Comando != null)
            {
                Comando = P_Comando;
            }
            else
            {
                Conexion.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Conexion.Open();
                Transaccion = Conexion.BeginTransaction();
                Comando.Transaction = Transaccion;
                Comando.Connection = Conexion;
            }

            try
            {
                //verificamos que los datos principales no sean vacios
                if (!String.IsNullOrEmpty(Fte_Financiamiento_ID.Trim())
                    && !String.IsNullOrEmpty(Programa_ID.Trim())
                    && !String.IsNullOrEmpty(Depedencia_ID.Trim())
                    && !String.IsNullOrEmpty(Partida_ID.Trim())
                    && !String.IsNullOrEmpty(Anio.Trim()))
                {

                    Mi_Sql.Append("SELECT ISNULL(" + Columna.Trim() + ", 0) AS IMPORTE");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                    Mi_Sql.Append(" = '" + Fte_Financiamiento_ID.Trim() + "'");
                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                    Mi_Sql.Append(" = '" + Programa_ID.Trim() + "'");
                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = '" + Depedencia_ID.Trim() + "'");
                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                    Mi_Sql.Append(" = '" + Partida_ID.Trim() + "'");
                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                    Mi_Sql.Append(" =" + Anio.Trim());

                    Comando.CommandText = Mi_Sql.ToString();
                    Obj_Importe = Comando.ExecuteScalar();

                    if (!Convert.IsDBNull(Obj_Importe))
                    {
                        Campo_Importe = Convert.ToString(Obj_Importe);
                        Importe = Convert.ToDouble(String.IsNullOrEmpty(Campo_Importe) ? "0" : Campo_Importe);
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar el importe de la partida. Error[" + Ex.Message + "]");
            }

            return Importe;
        }

        ///*****************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Presupuesto_Egresos
        ///DESCRIPCIÓN          : consulta para actualizar los datos del presupuesto de egresos
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 12/Noviembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************
        public static String Obtener_Columna(String Momento_Presupuestal, String Mes)
        {
            String Columna = String.Empty;

            try
            {
                if (Momento_Presupuestal.Trim().Equals(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido.Trim()))
                {
                    #region (Pre Comprometido)
                    switch (Mes)
                    {
                        case "01":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero;
                            break;
                        case "02":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero;
                            break;
                        case "03":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo;
                            break;
                        case "04":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril;
                            break;
                        case "05":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo;
                            break;
                        case "06":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio;
                            break;
                        case "07":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio;
                            break;
                        case "08":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto;
                            break;
                        case "09":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre;
                            break;
                        case "10":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre;
                            break;
                        case "11":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre;
                            break;
                        case "12":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre;
                            break;
                    }
                    #endregion
                }
                else if (Momento_Presupuestal.Trim().Equals(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido.Trim()))
                {
                    #region (Comprometido)
                    switch (Mes)
                    {
                        case "01":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Enero;
                            break;
                        case "02":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Febrero;
                            break;
                        case "03":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Marzo;
                            break;
                        case "04":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Abril;
                            break;
                        case "05":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Mayo;
                            break;
                        case "06":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Junio;
                            break;
                        case "07":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Julio;
                            break;
                        case "08":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Agosto;
                            break;
                        case "09":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Septiembre;
                            break;
                        case "10":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Octubre;
                            break;
                        case "11":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Noviembre;
                            break;
                        case "12":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Diciembre;
                            break;
                    }
                    #endregion
                }
                else if (Momento_Presupuestal.Trim().Equals(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado.Trim()))
                {
                    #region (Devengado)
                    switch (Mes)
                    {
                        case "01":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Enero;
                            break;
                        case "02":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Febrero;
                            break;
                        case "03":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Marzo;
                            break;
                        case "04":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Abril;
                            break;
                        case "05":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Mayo;
                            break;
                        case "06":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Junio;
                            break;
                        case "07":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Julio;
                            break;
                        case "08":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Agosto;
                            break;
                        case "09":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Septiembre;
                            break;
                        case "10":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Octubre;
                            break;
                        case "11":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Noviembre;
                            break;
                        case "12":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Diciembre;
                            break;
                    }
                    #endregion
                }
                else if (Momento_Presupuestal.Trim().Equals(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido.Trim()))
                {
                    #region (Ejercido)
                    switch (Mes)
                    {
                        case "01":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Enero;
                            break;
                        case "02":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Febrero;
                            break;
                        case "03":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Marzo;
                            break;
                        case "04":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Abril;
                            break;
                        case "05":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Mayo;
                            break;
                        case "06":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Junio;
                            break;
                        case "07":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Julio;
                            break;
                        case "08":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Agosto;
                            break;
                        case "09":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Septiembre;
                            break;
                        case "10":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Octubre;
                            break;
                        case "11":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Noviembre;
                            break;
                        case "12":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Diciembre;
                            break;
                    }
                    #endregion
                }
                else if (Momento_Presupuestal.Trim().Equals(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado.Trim()))
                {
                    #region (Pagado)
                    switch (Mes)
                    {
                        case "01":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Enero;
                            break;
                        case "02":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Febrero;
                            break;
                        case "03":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Marzo;
                            break;
                        case "04":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Abril;
                            break;
                        case "05":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Mayo;
                            break;
                        case "06":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Junio;
                            break;
                        case "07":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Julio;
                            break;
                        case "08":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Agosto;
                            break;
                        case "09":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Septiembre;
                            break;
                        case "10":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Octubre;
                            break;
                        case "11":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Noviembre;
                            break;
                        case "12":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Diciembre;
                            break;
                    }
                    #endregion
                }
                else if (Momento_Presupuestal.Trim().Equals(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible.Trim()))
                {
                    #region (Disponible)
                    switch (Mes)
                    {
                        case "01":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero;
                            break;
                        case "02":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero;
                            break;
                        case "03":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo;
                            break;
                        case "04":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril;
                            break;
                        case "05":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo;
                            break;
                        case "06":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio;
                            break;
                        case "07":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio;
                            break;
                        case "08":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto;
                            break;
                        case "09":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre;
                            break;
                        case "10":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre;
                            break;
                        case "11":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre;
                            break;
                        case "12":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre;
                            break;
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al obtener el nombre de la columna que se afectara. Error[" + ex.Message + "]");
            }
            return Columna;
        }

        ///*****************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Presupuesto_Egresos
        ///DESCRIPCIÓN          : consulta para actualizar los datos del presupuesto de egresos
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 06/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************
        public static Boolean Acumular_Saldos_Fin_Mes()
        {
            Boolean Operacion_Completa = true;
            DataTable Dt_Parametros = new DataTable();
            DataTable Dt_Psp_Actualizado = new DataTable();
            String Anio = String.Format("{0:yyyy}", DateTime.Now);
            String Dia = String.Format("{0:dd}", DateTime.Now);
            String Mes = String.Format("{0:MM}", DateTime.Now);
            Boolean Presupuesto_Actualizado = false;
            Boolean Acumular_Disponible = false;
            StringBuilder Mi_SQL = new StringBuilder();
            String Mes_Anterior = String.Format("{0:00}", (DateTime.Now.Month - 1));
            String Disponible_Actual = String.Empty;
            String Afectado_Actual = String.Empty;
            String PreComprometido_Actual = String.Empty;
            String Comprometido_Actual = String.Empty;
            String Devengado_Actual = String.Empty;
            String Ejercido_Actual = String.Empty;
            String Disponible_Anterior = String.Empty;
            String Afectado_Anterior = String.Empty;
            String PreComprometido_Anterior = String.Empty;
            String Comprometido_Anterior = String.Empty;
            String Devengado_Anterior = String.Empty;
            String Ejercido_Anterior = String.Empty;

            try
            {
                //CONSULTAMOS DIA PARA APLICAR EL ACUMULADO
                if (Convert.ToInt32(Dia.Trim()) < 20) //si es 1ro aplicamos el acumulado
                {
                    //validamos el mes
                    if (Convert.ToInt32(Mes.Trim()) > 1)
                    {
                        //CONSULTAMOS SI EL PRESUPUESTO YA FUE ACTUALIZADO
                        Presupuesto_Actualizado = Cls_Ope_Psp_Manejo_Presupuesto.Presupuesto_Actualizado();

                        if (!Presupuesto_Actualizado)
                        {
                            //CONSULTAMOS SI SE APLICARA EL ACUMULADO TAMBIEN PARA EL CAMPO DE DISPONIBLE DEL PRESUPUESTO
                            Acumular_Disponible = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Disponible();

                            //CONSULTAMOS LOS NOMBRES DE LAS COLUMNAS A MODIFICAR DE ACUERDO AL MES
                            Disponible_Actual = Obtener_Columna(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible, Mes);
                            PreComprometido_Actual = Obtener_Columna(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido, Mes);
                            Comprometido_Actual = Obtener_Columna(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido, Mes);
                            Devengado_Actual = Obtener_Columna(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado, Mes);
                            Ejercido_Actual = Obtener_Columna(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido, Mes);

                            Disponible_Anterior = Obtener_Columna(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible, Mes_Anterior);
                            PreComprometido_Anterior = Obtener_Columna(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido, Mes_Anterior);
                            Comprometido_Anterior = Obtener_Columna(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido, Mes_Anterior);
                            Devengado_Anterior = Obtener_Columna(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado, Mes_Anterior);
                            Ejercido_Anterior = Obtener_Columna(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido, Mes_Anterior);

                            //ACTUALIZAMOS EL PRESUPUESTO ACUMULANDO LOS SALDOS
                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                            Mi_SQL.Append(" SET ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Actualizado + " = 'SI', ");
                            if (Acumular_Disponible)
                            {
                                //MODIFICAMOS EL DISPONIBLE
                                Mi_SQL.Append(Disponible_Actual);
                                Mi_SQL.Append(" = ISNULL(" + Disponible_Actual + ",0) + ISNULL(" + Disponible_Anterior + ", 0), ");
                                Mi_SQL.Append(Disponible_Anterior + " = 0, ");
                            }
                            //MODIFICAMOS EL PRECOMPROMETIDO
                            Mi_SQL.Append(PreComprometido_Actual);
                            Mi_SQL.Append(" = ISNULL(" + PreComprometido_Actual + ",0) + ISNULL(" + PreComprometido_Anterior + ", 0), ");
                            //MODIFICAMOS EL COMPROMETIDO
                            Mi_SQL.Append(Comprometido_Actual);
                            Mi_SQL.Append(" = ISNULL(" + Comprometido_Actual + ",0) + ISNULL(" + Comprometido_Anterior + ", 0), ");
                            //MODIFICAMOS EL DEVENGADO
                            Mi_SQL.Append(Devengado_Actual);
                            Mi_SQL.Append(" = ISNULL(" + Devengado_Actual + ",0) + ISNULL(" + Devengado_Anterior + ", 0), ");
                            //MODIFICAMOS EL EJERCIDO
                            Mi_SQL.Append(Ejercido_Actual);
                            Mi_SQL.Append(" = ISNULL(" + Ejercido_Actual + ",0) + ISNULL(" + Ejercido_Anterior + ", 0) ");
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio.Trim());

                            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        }
                    }
                }
                else //else validación día 1ro
                {
                    //si es otro dia validamos si es 20 de cada mes para modificar el presupuesto a actualizado a NO,
                    //para cuando seal inicio de otro mes volver a actualizar el presupuesto
                    if (Convert.ToInt32(Dia) >= 20)
                    {
                        //modificamos el presupuesto de egresos en actualizado = NO
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                        Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Actualizado + " = 'NO'");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                        SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    }
                }

                Operacion_Completa = true;
            }
            catch (Exception)
            {
                Operacion_Completa = false;
                throw new Exception();
            }
            return Operacion_Completa;
        }

        public static Boolean Acumular_Saldos_Fin_Mes(SqlCommand P_Command)
        {
            Boolean Operacion_Completa = true;
            DataTable Dt_Parametros = new DataTable();
            DataTable Dt_Psp_Actualizado = new DataTable();
            String Anio = String.Format("{0:yyyy}", DateTime.Now);
            String Dia = String.Format("{0:dd}", DateTime.Now);
            String Mes = String.Format("{0:MM}", DateTime.Now);
            Boolean Presupuesto_Actualizado = false;
            Boolean Acumular_Disponible = false;
            StringBuilder Mi_SQL = new StringBuilder();
            String Mes_Anterior = String.Format("{0:00}", (DateTime.Now.Month - 1));
            String Disponible_Actual = String.Empty;
            String Afectado_Actual = String.Empty;
            String PreComprometido_Actual = String.Empty;
            String Comprometido_Actual = String.Empty;
            String Devengado_Actual = String.Empty;
            String Ejercido_Actual = String.Empty;
            String Disponible_Anterior = String.Empty;
            String Afectado_Anterior = String.Empty;
            String PreComprometido_Anterior = String.Empty;
            String Comprometido_Anterior = String.Empty;
            String Devengado_Anterior = String.Empty;
            String Ejercido_Anterior = String.Empty;
            SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion = new SqlConnection();//Variable para la conexión para la base de datos   
            SqlCommand Comando = new SqlCommand();//Sirve para la ejecución de las operaciones a la base de datos

            if (P_Command != null)
            {
                Comando = P_Command;
            }
            else
            {
                Conexion.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Conexion.Open();
                Transaccion = Conexion.BeginTransaction();
                Comando.Transaction = Transaccion;
                Comando.Connection = Conexion;
            }
            try
            {
                //CONSULTAMOS DIA PARA APLICAR EL ACUMULADO
                if (Convert.ToInt32(Dia.Trim()) < 20) //si es 1ro aplicamos el acumulado
                {
                    //validamos el mes
                    if (Convert.ToInt32(Mes.Trim()) > 1)
                    {
                        //CONSULTAMOS SI EL PRESUPUESTO YA FUE ACTUALIZADO
                        Presupuesto_Actualizado = Cls_Ope_Psp_Manejo_Presupuesto.Presupuesto_Actualizado(Comando);

                        if (!Presupuesto_Actualizado)
                        {
                            //CONSULTAMOS SI SE APLICARA EL ACUMULADO TAMBIEN PARA EL CAMPO DE DISPONIBLE DEL PRESUPUESTO
                            Acumular_Disponible = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Disponible(Comando);

                            //CONSULTAMOS LOS NOMBRES DE LAS COLUMNAS A MODIFICAR DE ACUERDO AL MES
                            Disponible_Actual = Obtener_Columna(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible, Mes);
                            PreComprometido_Actual = Obtener_Columna(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido, Mes);
                            Comprometido_Actual = Obtener_Columna(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido, Mes);
                            Devengado_Actual = Obtener_Columna(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado, Mes);
                            Ejercido_Actual = Obtener_Columna(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido, Mes);

                            Disponible_Anterior = Obtener_Columna(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible, Mes_Anterior);
                            PreComprometido_Anterior = Obtener_Columna(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido, Mes_Anterior);
                            Comprometido_Anterior = Obtener_Columna(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido, Mes_Anterior);
                            Devengado_Anterior = Obtener_Columna(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado, Mes_Anterior);
                            Ejercido_Anterior = Obtener_Columna(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido, Mes_Anterior);

                            //ACTUALIZAMOS EL PRESUPUESTO ACUMULANDO LOS SALDOS
                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                            Mi_SQL.Append(" SET ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Actualizado + " = 'SI', ");
                            if (Acumular_Disponible)
                            {
                                //MODIFICAMOS EL DISPONIBLE
                                Mi_SQL.Append(Disponible_Actual);
                                Mi_SQL.Append(" = ISNULL(" + Disponible_Actual + ",0) + ISNULL(" + Disponible_Anterior + ", 0), ");
                                Mi_SQL.Append(Disponible_Anterior + " = 0, ");
                            }
                            //MODIFICAMOS EL PRECOMPROMETIDO
                            Mi_SQL.Append(PreComprometido_Actual);
                            Mi_SQL.Append(" = ISNULL(" + PreComprometido_Actual + ",0) + ISNULL(" + PreComprometido_Anterior + ", 0), ");
                            //MODIFICAMOS EL COMPROMETIDO
                            Mi_SQL.Append(Comprometido_Actual);
                            Mi_SQL.Append(" = ISNULL(" + Comprometido_Actual + ",0) + ISNULL(" + Comprometido_Anterior + ", 0), ");
                            //MODIFICAMOS EL DEVENGADO
                            Mi_SQL.Append(Devengado_Actual);
                            Mi_SQL.Append(" = ISNULL(" + Devengado_Actual + ",0) + ISNULL(" + Devengado_Anterior + ", 0), ");
                            //MODIFICAMOS EL EJERCIDO
                            Mi_SQL.Append(Ejercido_Actual);
                            Mi_SQL.Append(" = ISNULL(" + Ejercido_Actual + ",0) + ISNULL(" + Ejercido_Anterior + ", 0) ");
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio.Trim());
                            Comando.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                            Comando.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                            //SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        }
                    }
                }
                else //else validación día 1ro
                {
                    //si es otro dia validamos si es 20 de cada mes para modificar el presupuesto a actualizado a NO,
                    //para cuando seal inicio de otro mes volver a actualizar el presupuesto
                    if (Convert.ToInt32(Dia) >= 20)
                    {
                        //modificamos el presupuesto de egresos en actualizado = NO
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                        Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Actualizado + " = 'NO'");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);
                        Comando.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                        Comando.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                        // SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    }
                }

                Operacion_Completa = true;
            }
            catch (Exception)
            {
                Operacion_Completa = false;
                throw new Exception();
            }
            return Operacion_Completa;
        }

        ///*****************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Actualizar_Disponible
        ///DESCRIPCIÓN          : consulta para actualizar los datos del presupuesto de egresos
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 06/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************
        public static Boolean Actualizar_Disponible()
        {
            DataTable Dt_Parametros = new DataTable();
            Boolean Aplicar_Acumulado = false;

            try
            {
                //CONSULTAMOS SI SE APLICARA EL ACUMULADO TAMBIEN PARA EL CAMPO DE DISPONIBLE DEL PRESUPUESTO
                Dt_Parametros = Cls_Ope_Psp_Manejo_Presupuesto.Consultar_Parametros_Presupuestales();
                if (Dt_Parametros != null)
                {
                    if (Dt_Parametros.Rows.Count > 0)
                    {
                        if (Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Aplicar_Disponible_Acumulado].ToString().Trim().Equals("SI"))
                        {
                            Aplicar_Acumulado = true;
                        }
                        else
                        {
                            Aplicar_Acumulado = false;
                        }
                    }
                }
            }
            catch (Exception)
            {
                Aplicar_Acumulado = false;
                throw new Exception();
            }
            return Aplicar_Acumulado;
        }
        public static Boolean Actualizar_Disponible(SqlCommand P_Comando)
        {
            DataTable Dt_Parametros = new DataTable();
            Boolean Aplicar_Acumulado = false;
            SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion = new SqlConnection();//Variable para la conexión para la base de datos   
            SqlCommand Comando = new SqlCommand();//Sirve para la ejecución de las operaciones a la base de datos
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            if (P_Comando != null)
            {
                Comando = P_Comando;
            }
            else
            {
                Conexion.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Conexion.Open();
                Transaccion = Conexion.BeginTransaction();
                Comando.Transaction = Transaccion;
                Comando.Connection = Conexion;
            }
            try
            {
                //CONSULTAMOS SI SE APLICARA EL ACUMULADO TAMBIEN PARA EL CAMPO DE DISPONIBLE DEL PRESUPUESTO
                Dt_Parametros = Cls_Ope_Psp_Manejo_Presupuesto.Consultar_Parametros_Presupuestales(Comando);
                if (Dt_Parametros != null)
                {
                    if (Dt_Parametros.Rows.Count > 0)
                    {
                        if (Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Aplicar_Disponible_Acumulado].ToString().Trim().Equals("SI"))
                        {
                            Aplicar_Acumulado = true;
                        }
                        else
                        {
                            Aplicar_Acumulado = false;
                        }
                    }
                }
            }
            catch (Exception)
            {
                Aplicar_Acumulado = false;
                throw new Exception();
            }
            return Aplicar_Acumulado;
        }

        ///*****************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Presupuesto_Actualizado
        ///DESCRIPCIÓN          : consulta para actualizar los datos del presupuesto de egresos
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 06/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************
        public static Boolean Presupuesto_Actualizado()
        {
            DataTable Dt_Psp_Actualizado = new DataTable();
            String Anio = String.Format("{0:yyyy}", DateTime.Now);
            Boolean Datos_Actualizados = false;
            StringBuilder Mi_SQL = new StringBuilder();

            try
            {
                //CONSULTAMOS SI EL PRESUPUESTO YA FUE ACTUALIZADO
                Mi_SQL.Append("SELECT DISTINCT ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Actualizado + ", 'NO') AS ACTUALIZADO ");
                Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio.Trim());

                Dt_Psp_Actualizado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                if (Dt_Psp_Actualizado != null)
                {
                    if (Dt_Psp_Actualizado.Rows.Count > 0)
                    {
                        if (Dt_Psp_Actualizado.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Actualizado].ToString().Trim().Equals("NO"))
                        {
                            Datos_Actualizados = false;
                        }
                        else
                        {
                            Datos_Actualizados = true;
                        }
                    }
                }
            }
            catch (Exception)
            {
                Datos_Actualizados = false;
                throw new Exception();
            }
            return Datos_Actualizados;
        }
        public static Boolean Presupuesto_Actualizado(SqlCommand P_Comando)
        {
            DataTable Dt_Psp_Actualizado = new DataTable();
            String Anio = String.Format("{0:yyyy}", DateTime.Now);
            Boolean Datos_Actualizados = false;
            StringBuilder Mi_SQL = new StringBuilder();
            SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion = new SqlConnection();//Variable para la conexión para la base de datos   
            SqlCommand Comando = new SqlCommand();//Sirve para la ejecución de las operaciones a la base de datos
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            if (P_Comando != null)
            {
                Comando = P_Comando;
            }
            else
            {
                Conexion.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Conexion.Open();
                Transaccion = Conexion.BeginTransaction();
                Comando.Transaction = Transaccion;
                Comando.Connection = Conexion;
            }
            try
            {
                //CONSULTAMOS SI EL PRESUPUESTO YA FUE ACTUALIZADO
                Mi_SQL.Append("SELECT DISTINCT ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Actualizado + ", 'NO') AS ACTUALIZADO ");
                Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio.Trim());
                Comando.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Dt_Oracle.SelectCommand = Comando;
                Dt_Oracle.Fill(Ds_Oracle);
                Dt_Psp_Actualizado = Ds_Oracle.Tables[0];
                //Dt_Psp_Actualizado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                if (Dt_Psp_Actualizado != null)
                {
                    if (Dt_Psp_Actualizado.Rows.Count > 0)
                    {
                        if (Dt_Psp_Actualizado.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Actualizado].ToString().Trim().Equals("NO"))
                        {
                            Datos_Actualizados = false;
                        }
                        else
                        {
                            Datos_Actualizados = true;
                        }
                    }
                }
            }
            catch (Exception)
            {
                Datos_Actualizados = false;
                throw new Exception();
            }
            return Datos_Actualizados;
        }

        ///*****************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Disponible_Partida
        ///DESCRIPCIÓN          : consulta para obtener el disponible de una partida
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 06/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************
        public static Double Consultar_Disponible_Partida(String Fte_Financiamiento_ID, String Programa_ID, String Depedencia_ID,
            String Partida_ID, String Anio, String Columna)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            Double Importe = 0.00;
            String Campo_Importe = String.Empty;
            String Mes = String.Empty;
            DataTable Dt_Datos = new DataTable();

            try
            {
                Mes = String.Format("{0:MM}", DateTime.Now);

                //verificamos que los datos principales no sean vacios
                if (!String.IsNullOrEmpty(Fte_Financiamiento_ID.Trim())
                    && !String.IsNullOrEmpty(Programa_ID.Trim())
                    && !String.IsNullOrEmpty(Depedencia_ID.Trim())
                    && !String.IsNullOrEmpty(Partida_ID.Trim())
                    && !String.IsNullOrEmpty(Anio.Trim())
                    && !String.IsNullOrEmpty(Mes.Trim()))
                {
                    Campo_Importe = Obtener_Columna(Columna, Mes.Trim());

                    Mi_Sql.Append("SELECT ISNULL(" + Campo_Importe.Trim() + ", 0) AS DISPONIBLE");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                    Mi_Sql.Append(" = '" + Fte_Financiamiento_ID.Trim() + "'");
                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                    Mi_Sql.Append(" = '" + Programa_ID.Trim() + "'");
                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = '" + Depedencia_ID.Trim() + "'");
                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                    Mi_Sql.Append(" = '" + Partida_ID.Trim() + "'");
                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                    Mi_Sql.Append(" = '" + Anio.Trim() + "'");

                    Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];

                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            Campo_Importe = Dt_Datos.Rows[0]["DISPONIBLE"].ToString().Trim();
                            Importe = Convert.ToDouble(String.IsNullOrEmpty(Campo_Importe) ? "0" : Campo_Importe);
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar el Importe de la partida. Error[" + Ex.Message + "]");
            }

            return Importe;
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Parametros_Presupuestales
        ///DESCRIPCIÓN          : consulta para obtener los datos de los parametros presupuestales
        ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 06/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Parametros_Presupuestales()
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                Mi_Sql.Append("SELECT ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Aplicar_Disponible_Acumulado + ", 'NO') AS APLICAR_DISPONIBLE_ACUMULADO ");
                Mi_Sql.Append(" FROM " + Cat_Psp_Parametros_Presupuestales.Tabla_Cat_Psp_Parametro_Presupuestal);

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de los datos de los patametros. Error: [" + Ex.Message + "]");
            }
        }

        public static DataTable Consultar_Parametros_Presupuestales(SqlCommand P_Comando)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion = new SqlConnection();//Variable para la conexión para la base de datos   
            SqlCommand Comando = new SqlCommand();//Sirve para la ejecución de las operaciones a la base de datos
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            if (P_Comando != null)
            {
                Comando = P_Comando;
            }
            else
            {
                Conexion.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Conexion.Open();
                Transaccion = Conexion.BeginTransaction();
                Comando.Transaction = Transaccion;
                Comando.Connection = Conexion;
            }
            try
            {
                //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                Mi_Sql.Append("SELECT ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Aplicar_Disponible_Acumulado + ", 'NO') AS APLICAR_DISPONIBLE_ACUMULADO ");
                Mi_Sql.Append(" FROM " + Cat_Psp_Parametros_Presupuestales.Tabla_Cat_Psp_Parametro_Presupuestal);
                Comando.CommandText = Mi_Sql.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Dt_Oracle.SelectCommand = Comando;
                Dt_Oracle.Fill(Ds_Oracle);
                return Ds_Oracle.Tables[0];
                //return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de los datos de los patametros. Error: [" + Ex.Message + "]");
            }
        }
        #endregion

        #region (POLIZA PRESUPUESTAL EGRESOS)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Registro_Movimientos_Presupuestales
        ///DESCRIPCIÓN          : Metodo que da de algta el movimiento presupuestal
        ///PARAMETROS           : String No_Reserva, String Cargo, String Abono, double Importe
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 15/Nov/2012
        ///MODIFICO             : Leslie González Vázquez
        ///FECHA_MODIFICO       : 06/Abril/2013 
        ///CAUSA_MODIFICACIÓN   : Se agrego que se genere la poliza presupuestal y regresa el numero de poliza
        ///*******************************************************************************
        public static String[] Registro_Movimientos_Presupuestales(String No_Reserva, String Cargo, String Abono, double Importe, String No_Poliza, String Tipo_Poliza_ID, String Mes_Ano, String Partida, SqlCommand P_Cmmd)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            String[] Datos_Poliza_PSP = null;
            String No_Poliza_PSP = String.Empty;
            String Tipo_Poliza_PSP = String.Empty;
            String Mes_Anio_PSP = String.Empty;
            String Importes = String.Empty;
            String[] Imp = null;
            Double Ampliacion = 0.00;
            Double Reduccion = 0.00;
            Double Traspaso_Reduccion = 0.00;
            DataTable Dt_Parametros_PSP = new DataTable();
            Int32 No_Registros = 0;
            String Mov_Inicial = Cargo.Trim();
            String Mov_Final = Abono.Trim();
            DataTable Dt_Seg_Mov = new DataTable();


            if (P_Cmmd != null)
            {
                Cmmd = P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }

            try
            {
                if (Cargo.Trim().Equals(Ope_Psp_Presupuesto_Aprobado.Campo_Modificado))
                {
                    if (!String.IsNullOrEmpty(No_Reserva.Trim()))
                    {
                        Importes = No_Reserva;

                        Imp = Importes.Split(';');
                        if (Imp.Length > 0)
                        {
                            Ampliacion = Convert.ToDouble(String.IsNullOrEmpty(Imp[0]) ? "0" : Imp[0]);
                            Reduccion = Convert.ToDouble(String.IsNullOrEmpty(Imp[1]) ? "0" : Imp[1]);
                            Traspaso_Reduccion = Convert.ToDouble(String.IsNullOrEmpty(Imp[2]) ? "0" : Imp[2]);
                            Dt_Seg_Mov = Crear_Dt_Movimimientos();
                        }

                        No_Reserva = String.Empty;
                    }
                }
                else
                {
                    Importes = Importe.ToString();
                }

                Dt_Parametros_PSP = Cls_Cat_Psp_Parametros_Presupuestales_Datos.Consultar_Parametros_Presupuestales();
                Datos_Poliza_PSP = Crear_Poliza_Presupuestal_Egresos(Cargo, Abono, Importes, Cmmd, Dt_Parametros_PSP);
                //obtenemos los id de la poliza presupuestal creada
                if (Datos_Poliza_PSP != null)
                {
                    if (Datos_Poliza_PSP.Length > 0)
                    {
                        No_Poliza_PSP = Datos_Poliza_PSP[0].Trim();
                        Tipo_Poliza_PSP = Datos_Poliza_PSP[1].Trim();
                        Mes_Anio_PSP = Datos_Poliza_PSP[2].Trim();
                    }
                }

                if (Mov_Inicial.Trim().Equals(Ope_Psp_Presupuesto_Aprobado.Campo_Modificado) &&
                    Mov_Final.Trim().Equals(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible))
                {
                    Cargo = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible;
                    Abono = Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion;

                    No_Registros = 3;
                    Importe = Ampliacion;
                }
                else
                {
                    No_Registros = 1;
                }

                while (No_Registros > 0)
                {
                    if (Importe != 0)
                    {
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("INSERT INTO " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "(");
                        Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Campo_No_Reserva + ", ");
                        Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Campo_Cargo + ", ");
                        Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Campo_Abono + ", ");
                        Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Campo_Importe + ", ");
                        Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Campo_Fecha + ", ");
                        Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Campo_Usuario + ", ");
                        Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Campo_Usuario_Creo + ", ");
                        Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Campo_Fecha_Creo + ", ");
                        Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Campo_No_Poliza + ", ");
                        Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Campo_Tipo_Poliza_ID + ", ");
                        Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Campo_Mes_Ano + ", ");
                        Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Campo_Partida + ", ");
                        Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Campo_No_Poliza_Presupuestal + ", ");
                        Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Campo_Tipo_Poliza_ID_Presupuestal + ", ");
                        Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Campo_Mes_Anio_Presupuestal + ") VALUES( ");

                        if (!String.IsNullOrEmpty(No_Reserva))
                        {
                            Mi_SQL.Append(No_Reserva.Trim() + ", ");
                        }
                        else
                        {
                            Mi_SQL.Append("NULL, ");
                        }

                        Mi_SQL.Append("'" + Cargo.Trim() + "', ");
                        Mi_SQL.Append("'" + Abono.Trim() + "', ");
                        Mi_SQL.Append(Importe + ", ");
                        Mi_SQL.Append("GETDATE(), ");
                        Mi_SQL.Append("'" + Cls_Sessiones.Nombre_Empleado + "', ");
                        Mi_SQL.Append("'" + Cls_Sessiones.Nombre_Empleado + "', ");
                        Mi_SQL.Append("GETDATE(), ");

                        if (!String.IsNullOrEmpty(No_Poliza))
                        {
                            Mi_SQL.Append("'" + No_Poliza.Trim() + "', ");
                        }
                        else
                        {
                            Mi_SQL.Append("NULL, ");
                        }

                        if (!String.IsNullOrEmpty(Tipo_Poliza_ID))
                        {
                            Mi_SQL.Append("'" + Tipo_Poliza_ID.Trim() + "', ");
                        }
                        else
                        {
                            Mi_SQL.Append("NULL, ");
                        }

                        if (!String.IsNullOrEmpty(Mes_Ano))
                        {
                            Mi_SQL.Append("'" + Mes_Ano.Trim() + "', ");
                        }
                        else
                        {
                            Mi_SQL.Append("NULL, ");
                        }

                        if (!String.IsNullOrEmpty(Partida))
                        {
                            Mi_SQL.Append("'" + Partida.Trim() + "', ");
                        }
                        else
                        {
                            Mi_SQL.Append("NULL, ");
                        }

                        if (!String.IsNullOrEmpty(No_Poliza_PSP))
                        {
                            Mi_SQL.Append("'" + No_Poliza_PSP.Trim() + "', ");
                        }
                        else
                        {
                            Mi_SQL.Append("NULL, ");
                        }

                        if (!String.IsNullOrEmpty(Tipo_Poliza_PSP))
                        {
                            Mi_SQL.Append("'" + Tipo_Poliza_PSP.Trim() + "', ");
                        }
                        else
                        {
                            Mi_SQL.Append("NULL, ");
                        }

                        if (!String.IsNullOrEmpty(Mes_Anio_PSP))
                        {
                            Mi_SQL.Append("'" + Mes_Anio_PSP.Trim() + "') ");
                        }
                        else
                        {
                            Mi_SQL.Append("NULL) ");
                        }

                        Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                        Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                    }

                    if (Mov_Inicial.Trim().Equals(Ope_Psp_Presupuesto_Aprobado.Campo_Modificado) &&
                        Mov_Final.Trim().Equals(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible))
                    {
                        String Ini = Cargo;
                        String Fin = Abono;
                        Cargo = Obtener_Movimiento(Dt_Seg_Mov, Ini, Fin, "CARGO");
                        Abono = Obtener_Movimiento(Dt_Seg_Mov, Ini, Fin, "ABONO");

                        if (Cargo.Trim().Equals(Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion))
                        {
                            Importe = Reduccion;
                        }
                        else
                        {
                            Cargo = Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion;
                            Abono = Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion;

                            Importe = Traspaso_Reduccion;
                        }
                    }

                    No_Registros--;
                }




                if (P_Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (SqlException Ex)
            {
                if (P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (P_Cmmd == null)
                {
                    Cn.Close();
                }

            }
            return Datos_Poliza_PSP;
        }

        ///*****************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Crear_Poliza_Presupuestal_Egresos
        ///DESCRIPCIÓN          : consulta para crear la poliza presupuestal de egresos
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 05/Abril/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************
        public static String[] Crear_Poliza_Presupuestal_Egresos(String Momento_Inicial, String Momento_Final,
            String Imp, SqlCommand Cmmd, DataTable Dt_Parametros_Psp)
        {
            Cls_Ope_Con_Polizas_Negocio Rs_Alta_Ope_Con_Polizas = new Cls_Ope_Con_Polizas_Negocio();
            String ID_Inicial = String.Empty;
            String ID_Final = String.Empty;
            DataTable Dt_Detalle = new DataTable();
            DataRow Fila;
            String[] No_Poliza = null;
            Double Importe = 0.00;
            Double Importe_Debe = 0.00;
            Double Importe_Haber = 0.00;
            Int32 No_Registros = 0;
            String Mov_Inicial = Momento_Inicial.Trim();
            String Mov_Final = Momento_Final.Trim();
            String Tipo_Poliza = String.Empty;
            Int32 Contador = 0;
            DataTable Dt_Modificacion = new DataTable();
            DataTable Dt_Seg_Mov = new DataTable();
            Double Ampliacion = 0.00;
            Double Reduccion = 0.00;
            Double Traspaso_Reduccion = 0.00;

            StringBuilder Mi_SQL = new StringBuilder();
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans = null;

            if (Cmmd != null)
            {
                Cmd = Cmmd;
            }
            else
            {
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Trans.Connection;
                Cmd.Transaction = Trans;
            }

            try
            {
                //obtenemos los movimientos
                Dt_Seg_Mov = Crear_Dt_Movimimientos();

                //creamos las columnas del datatable para el alta de la poliza
                Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                Dt_Detalle.Columns.Add(Cat_Con_Cuentas_Contables.Campo_Cuenta, typeof(System.String));
                Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
                Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID, typeof(System.String));
                Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida_ID, typeof(System.String));
                Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Dependencia_ID, typeof(System.String));
                Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                Dt_Detalle.Columns.Add("MOMENTO_INICIAL", typeof(System.String));
                Dt_Detalle.Columns.Add("MOMENTO_FINAL", typeof(System.String));

                //Obtenemos los datos del movimiento inicial y final
                if (Mov_Inicial.Trim().Equals(Ope_Psp_Presupuesto_Aprobado.Campo_Modificado) &&
                    Mov_Final.Trim().Equals(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible))
                {
                    Momento_Inicial = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible;
                    Momento_Final = Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion;

                    No_Registros = 3;

                    String[] Importes = Imp.Split(';');
                    if (Importes.Length > 0)
                    {
                        Ampliacion = Convert.ToDouble(String.IsNullOrEmpty(Importes[0]) ? "0" : Importes[0]);
                        Reduccion = Convert.ToDouble(String.IsNullOrEmpty(Importes[1]) ? "0" : Importes[1]);
                        Traspaso_Reduccion = Convert.ToDouble(String.IsNullOrEmpty(Importes[2]) ? "0" : Importes[2]);
                    }

                    Importe = Ampliacion;
                }
                else if (Mov_Inicial.Trim().Equals(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido) &&
                    Mov_Final.Trim().Equals(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido))
                {
                    //Obtenemos el importe de los conceptos
                    Importe = Convert.ToDouble(String.IsNullOrEmpty(Imp) ? "0" : Imp);

                    Momento_Inicial = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido;
                    Momento_Final = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible;
                    No_Registros = 1;
                }
                else
                {
                    //Obtenemos el importe de los conceptos
                    Importe = Convert.ToDouble(String.IsNullOrEmpty(Imp) ? "0" : Imp);
                    No_Registros = 1;
                }

                //obtenemos el id de la cuenda contable del movimiento
                Tipo_Poliza = Obtener_Id_Poliza_Presupuestal(Dt_Parametros_Psp, Cat_Psp_Parametros_Presupuestales.Campo_Tipo_Poliza_ID_Psp_Egr);

                //hacemos un ciclo while para manejar el numero de registros que insertaremos
                while (No_Registros > 0)
                {
                    ID_Inicial = Obtener_Id_Poliza_Presupuestal(Dt_Parametros_Psp, Momento_Inicial);
                    ID_Final = Obtener_Id_Poliza_Presupuestal(Dt_Parametros_Psp, Momento_Final);

                    if (!String.IsNullOrEmpty(ID_Inicial.Trim()) && !String.IsNullOrEmpty(ID_Final.Trim())
                        && (Importe != 0))
                    {
                        //cargamos los dato de CARGO - DEBE
                        Fila = Dt_Detalle.NewRow();
                        Fila[Ope_Con_Polizas_Detalles.Campo_Partida] = ++Contador;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = ID_Inicial.Trim();
                        Fila["CUENTA"] = String.Empty;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Concepto] = "Póliza Presupuestal";
                        Fila[Ope_Con_Polizas_Detalles.Campo_Debe] = Math.Abs(Importe);
                        Fila[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = String.Empty;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = String.Empty;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = String.Empty;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = String.Empty;
                        Fila["MOMENTO_INICIAL"] = String.Empty;
                        Fila["MOMENTO_FINAL"] = String.Empty;
                        Dt_Detalle.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla

                        //cargamos los dato de ABONO - HABER
                        Fila = Dt_Detalle.NewRow();
                        Fila[Ope_Con_Polizas_Detalles.Campo_Partida] = ++Contador;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = ID_Final.Trim();
                        Fila["CUENTA"] = String.Empty;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Concepto] = "Póliza Presupuestal";
                        Fila[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Haber] = Math.Abs(Importe);
                        Fila[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = String.Empty;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = String.Empty;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = String.Empty;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = String.Empty;
                        Fila["MOMENTO_INICIAL"] = String.Empty;
                        Fila["MOMENTO_FINAL"] = String.Empty;
                        Dt_Detalle.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                    }

                    if (Mov_Inicial.Trim().Equals(Ope_Psp_Presupuesto_Aprobado.Campo_Modificado) &&
                    Mov_Final.Trim().Equals(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible))
                    {
                        String Ini = Momento_Inicial;
                        String Fin = Momento_Final;
                        Momento_Inicial = Obtener_Movimiento(Dt_Seg_Mov, Ini, Fin, "CARGO");
                        Momento_Final = Obtener_Movimiento(Dt_Seg_Mov, Ini, Fin, "ABONO");

                        if (Momento_Inicial.Trim().Equals(Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion))
                        {
                            Importe = Reduccion;
                        }
                        else
                        {
                            Importe = Traspaso_Reduccion;
                        }
                    }

                    No_Registros--;
                }

                if (Dt_Detalle.Rows.Count > 0 && !String.IsNullOrEmpty(Tipo_Poliza))
                {
                    //Obtenemos el importe del debe y del haber para totalizar la poliza
                    Importe_Debe = Dt_Detalle.AsEnumerable().Sum(x => Convert.ToDouble(String.IsNullOrEmpty(x.Field<Double>(Ope_Con_Polizas_Detalles.Campo_Debe).ToString()) ? "0" : x.Field<Double>(Ope_Con_Polizas_Detalles.Campo_Debe).ToString()));
                    Importe_Haber = Dt_Detalle.AsEnumerable().Sum(x => Convert.ToDouble(String.IsNullOrEmpty(x.Field<Double>(Ope_Con_Polizas_Detalles.Campo_Haber).ToString()) ? "0" : x.Field<Double>(Ope_Con_Polizas_Detalles.Campo_Haber).ToString()));

                    Rs_Alta_Ope_Con_Polizas.P_Tipo_Poliza_ID = Tipo_Poliza;
                    Rs_Alta_Ope_Con_Polizas.P_Mes_Ano = String.Format("{0:MMyy}", DateTime.Now);
                    Rs_Alta_Ope_Con_Polizas.P_Fecha_Poliza = DateTime.Now;
                    Rs_Alta_Ope_Con_Polizas.P_Concepto = "Póliza Presupuestal";
                    Rs_Alta_Ope_Con_Polizas.P_Total_Debe = Math.Abs(Importe_Debe);
                    Rs_Alta_Ope_Con_Polizas.P_Total_Haber = Math.Abs(Importe_Haber);
                    Rs_Alta_Ope_Con_Polizas.P_No_Partida = Contador;
                    Rs_Alta_Ope_Con_Polizas.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado.Trim();
                    Rs_Alta_Ope_Con_Polizas.P_Dt_Detalles_Polizas = Dt_Detalle;
                    Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Creo = Cls_Sessiones.Empleado_ID.Trim();
                    Rs_Alta_Ope_Con_Polizas.P_Cmmd = Cmd;
                    No_Poliza = Rs_Alta_Ope_Con_Polizas.Alta_Poliza(); //Da de alta los datos de la Póliza proporcionados por el usuario en la BD
                }


                if (Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (Exception Ex)
            {
                Trans.Rollback();
                throw new Exception(" Error al crear la poliza presupuestal de ingresos. Error[" + Ex.Message + "]");
            }
            finally
            {
                if (Cmmd == null)
                {
                    Cn.Close();
                }
            }
            return No_Poliza;
        }

        ///*****************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Id_Poliza_Presupuestal
        ///DESCRIPCIÓN          : consulta para crear la poliza presupuestal de ingresos
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 05/Abril/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************
        public static String Obtener_Id_Poliza_Presupuestal(DataTable Dt_Parametros_Psp, String Columna)
        {
            String Id = String.Empty;

            try
            {
                if (Dt_Parametros_Psp != null)
                {
                    if (Dt_Parametros_Psp.Rows.Count > 0)
                    {
                        switch (Columna)
                        {
                            //tipos de polizas egresos
                            case Cat_Psp_Parametros_Presupuestales.Campo_Tipo_Poliza_ID_Psp_Egr:
                                Id = Dt_Parametros_Psp.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Tipo_Poliza_ID_Psp_Egr].ToString().Trim();
                                break;
                            case Cat_Psp_Parametros_Presupuestales.Campo_Aplicar_Disponible_Acumulado:
                                Id = Dt_Parametros_Psp.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Aplicar_Disponible_Acumulado].ToString().Trim();
                                break;

                            //Cuentas de Egresos
                            case Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado:
                                Id = Dt_Parametros_Psp.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Aprobado].ToString().Trim();
                                break;
                            case Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion:
                                Id = Dt_Parametros_Psp.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Ampliacion].ToString().Trim();
                                break;
                            case Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion:
                                Id = Dt_Parametros_Psp.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Reduccion].ToString().Trim();
                                break;
                            case Ope_Psp_Presupuesto_Aprobado.Campo_Modificado:
                                Id = Dt_Parametros_Psp.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Modificado].ToString().Trim();
                                break;
                            case Ope_Psp_Presupuesto_Aprobado.Campo_Devengado:
                                Id = Dt_Parametros_Psp.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Devengado].ToString().Trim();
                                break;
                            case "TRASPASO_AMPLIACION":
                                Id = Dt_Parametros_Psp.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Ampliacion_Interna].ToString().Trim();
                                break;
                            case "TRASPASO_REDUCCION":
                                Id = Dt_Parametros_Psp.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Reduccion_Interna].ToString().Trim();
                                break;
                            case Ope_Psp_Presupuesto_Aprobado.Campo_Disponible:
                                Id = Dt_Parametros_Psp.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Por_Ejercer].ToString().Trim();
                                break;
                            case Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido:
                                Id = Dt_Parametros_Psp.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Comprometido].ToString().Trim();
                                break;
                            case Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido:
                                Id = Dt_Parametros_Psp.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Ejercido].ToString().Trim();
                                break;
                            case Ope_Psp_Presupuesto_Aprobado.Campo_Pagado:
                                Id = Dt_Parametros_Psp.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Pagado].ToString().Trim();
                                break;
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception(" Error al obtener los datos poliza presupuestal de ingresos. Error[" + Ex.Message + "]");
            }

            return Id;
        }

        ///*****************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Movimimientos
        ///DESCRIPCIÓN          : consulta para crear la poliza presupuestal de ingresos
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 05/Abril/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************
        public static DataTable Crear_Dt_Movimimientos()
        {
            DataTable Dt_Mov = new DataTable();
            DataRow Fila;
            try
            {
                //Creamos las columnas del datatable
                Dt_Mov.Columns.Add("Mov_Ini", typeof(System.String));
                Dt_Mov.Columns.Add("Mov_Final", typeof(System.String));
                Dt_Mov.Columns.Add("Cargo", typeof(System.String));
                Dt_Mov.Columns.Add("Abono", typeof(System.String));


                //modificacion de presupuesto (ampliacion)
                Fila = Dt_Mov.NewRow();
                Fila["Mov_Ini"] = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible;
                Fila["Mov_Final"] = Ope_Psp_Presupuesto_Aprobado.Campo_Modificado;
                Fila["Cargo"] = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible;
                Fila["Abono"] = Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion;
                Dt_Mov.Rows.Add(Fila);

                //reduccion de presupuesto
                Fila = Dt_Mov.NewRow();
                Fila["Mov_Ini"] = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible;
                Fila["Mov_Final"] = Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion;
                Fila["Cargo"] = Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion;
                Fila["Abono"] = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible;
                Dt_Mov.Rows.Add(Fila);

                //traspaso de presupuesto
                Fila = Dt_Mov.NewRow();
                Fila["Mov_Ini"] = Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion;
                Fila["Mov_Final"] = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible;
                Fila["Cargo"] = "TRASPASO_REDUCCION";
                Fila["Abono"] = "TRASPASO_AMPLIACION";
                Dt_Mov.Rows.Add(Fila);

                //comprometido presupuesto
                Fila = Dt_Mov.NewRow();
                Fila["Mov_Ini"] = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido;
                Fila["Mov_Final"] = Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido;
                Fila["Cargo"] = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido;
                Fila["Abono"] = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible;
                Dt_Mov.Rows.Add(Fila);


                //autorizacion de presupuesto
                Fila = Dt_Mov.NewRow();
                Fila["Mov_Ini"] = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible;
                Fila["Mov_Final"] = Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado;
                Fila["Cargo"] = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible;
                Fila["Abono"] = Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado;
                Dt_Mov.Rows.Add(Fila);

                //devengamos presupuesto
                Fila = Dt_Mov.NewRow();
                Fila["Mov_Ini"] = Ope_Psp_Presupuesto_Aprobado.Campo_Devengado;
                Fila["Mov_Final"] = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido;
                Fila["Cargo"] = Ope_Psp_Presupuesto_Aprobado.Campo_Devengado;
                Fila["Abono"] = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido;
                Dt_Mov.Rows.Add(Fila);

                //ejercemos presupuesto
                Fila = Dt_Mov.NewRow();
                Fila["Mov_Ini"] = Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido;
                Fila["Mov_Final"] = Ope_Psp_Presupuesto_Aprobado.Campo_Devengado;
                Fila["Cargo"] = Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido;
                Fila["Abono"] = Ope_Psp_Presupuesto_Aprobado.Campo_Devengado;
                Dt_Mov.Rows.Add(Fila);

                //pagamos presupuesto
                Fila = Dt_Mov.NewRow();
                Fila["Mov_Ini"] = Ope_Psp_Presupuesto_Aprobado.Campo_Pagado;
                Fila["Mov_Final"] = Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido;
                Fila["Cargo"] = Ope_Psp_Presupuesto_Aprobado.Campo_Pagado;
                Fila["Abono"] = Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido;
                Dt_Mov.Rows.Add(Fila);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al crear la tabla de movimientos. Error[" + Ex.Message + "]");
            }
            return Dt_Mov;
        }

        ///*****************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Movimiento
        ///DESCRIPCIÓN          : consulta para obtener el tipo de movimiento siguiente
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 06/Abril/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************
        public static String Obtener_Movimiento(DataTable Dt_Mov, String Mov_Ini, String Mov_Fin, String Tipo)
        {
            String Sig_Mov = String.Empty;
            try
            {
                if (Dt_Mov != null)
                {
                    if (Dt_Mov.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Mov.Rows)
                        {
                            if (Dr["Mov_Ini"].ToString().Trim().Equals(Mov_Ini.Trim())
                                && Dr["Mov_Final"].ToString().Trim().Equals(Mov_Fin.Trim()))
                            {
                                if (Tipo.Trim().Equals("CARGO"))
                                {
                                    Sig_Mov = Dr["Cargo"].ToString().Trim();
                                }
                                else
                                {
                                    Sig_Mov = Dr["Abono"].ToString().Trim();
                                }
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al seleccionar los movimientos. Error[" + Ex.Message + "]");
            }
            return Sig_Mov;
        }
        #endregion

    }
}
