﻿using System;
using System.Data;
using System.Text;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Calendarizacion_Pronostico_Ingresos.Negocio;

namespace JAPAMI.Calendarizacion_Pronostico_Ingresos.Datos
{
    public class Cls_Rpt_Psp_Calendarizacion_Pronostico_Ingresos_Datos
    {
        #region Consulta
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Pronostico_Ingresos
        ///DESCRIPCIÓN          : Obtiene datos de los años del estado analitico de ingresos
        ///PARAMETROS           : 
        ///CREO                 : Ramón Baeza Yépez
        ///FECHA_CREO           : 11/Junio/2013
        ///*******************************************************************************
        internal static DataTable Pronostico_Ingresos(Cls_Rpt_Psp_Calendarizacion_Pronostico_Ingresos Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
            DataSet Ds_Datos = new DataSet(); //Dataset donde obtendremos los datos de la consulta
            DataTable Dt_Datos = new DataTable();
            String Mes;
            try
            {
                DateTime Fecha_Inicio = new DateTime(Datos.Fecha.Year,01,01);
                Mes = Datos.Fecha.ToString("MMMMM").ToUpper();
                Mi_SQL.Append("Select Cla." + Cat_Psp_Clase_Ing.Campo_Clave + " As CLAVE_CALSE,Cla." + Cat_Psp_Clase_Ing.Campo_Descripcion + " As CLASE,");
                Mi_SQL.Append("Con." + Cat_Psp_Concepto_Ing.Campo_Clave + " As CLAVE_CONCEPTO, Con." + Cat_Psp_Concepto_Ing.Campo_Descripcion + " As CONCEPTO,");
                Mi_SQL.Append("Sub." + Cat_Psp_SubConcepto_Ing.Campo_Clave + " As CLAVE_SUBCONCEPTO, Sub." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion + " As SUBCONCEPTO,");
                Mi_SQL.Append("Pro.IMPORTE_" + Mes + " As PRONOSTICADO, Pre.IMPORTE_" + Mes + " As REAL,");
                Mi_SQL.Append("( " );
                for (int Cont_Meses = 0; Cont_Meses < Datos.Fecha.Month; Cont_Meses++ )
                    Mi_SQL.Append(" Pro.IMPORTE_" + Fecha_Inicio.AddMonths(Cont_Meses).ToString("MMMMM").ToUpper() + " +");
                Mi_SQL.Remove(Mi_SQL.ToString().Length - 1, 1);
                Mi_SQL.Append(") As ACUMULADO_PRONOSTICO, ");
                Mi_SQL.Append("( ");
                for (int Cont_Meses = 0; Cont_Meses < Datos.Fecha.Month; Cont_Meses++)
                    Mi_SQL.Append(" Pre.IMPORTE_" + Fecha_Inicio.AddMonths(Cont_Meses).ToString("MMMMM").ToUpper() + " +");
                Mi_SQL.Remove(Mi_SQL.ToString().Length - 1, 1);
                Mi_SQL.Append(") As ACUMULADO_REAL ");
                Mi_SQL.Append(" from " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + " As Sub ");
                Mi_SQL.Append(" LEFT OUTER JOIN "+ Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " As Con ");
                Mi_SQL.Append(" on Sub." + Cat_Psp_SubConcepto_Ing.Campo_Concepto_Ing_ID + " = Con." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " As Cla ");
                Mi_SQL.Append(" on Con." + Cat_Psp_Concepto_Ing.Campo_Clase_Ing_ID + " = Cla." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + " As Pro ");
                Mi_SQL.Append(" on Sub." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = Pro." + Ope_Psp_Pronostico_Ingresos.Campo_SubConcepto_Ing_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + " As Pre ");
                Mi_SQL.Append(" on Sub." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = Pre." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                Mi_SQL.Append(" WHERE Cla." + Cat_Psp_Clase_Ing.Campo_Estatus + " = 'ACTIVO' AND Con." + Cat_Psp_Concepto_Ing.Campo_Estatus + " = 'ACTIVO'");
               // Mi_SQL.Append(" And Con." + Cat_Psp_Concepto_Ing.Campo_Anio + " = " + Datos.Fecha.Year);
                Mi_SQL.Append(" AND Sub." + Cat_Psp_SubConcepto_Ing.Campo_Estatus + " = 'ACTIVO' ");
               // Mi_SQL.Append(" And Sub." + Cat_Psp_SubConcepto_Ing.Campo_Anio + " = " + Datos.Fecha.Year);
                Mi_SQL.Append(" And Pro." + Ope_Psp_Pronostico_Ingresos.Campo_Anio + " = " + Datos.Fecha.Year + " AND ");
                Mi_SQL.Append(" Pre." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = " + Datos.Fecha.Year);
                Mi_SQL.Append("ORDER BY Sub." + Cat_Psp_SubConcepto_Ing.Campo_Clave + " asc");
                Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0].Copy();
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros de los años. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }
        #endregion
    }
}
