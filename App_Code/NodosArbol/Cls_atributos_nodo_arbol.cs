﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;



namespace JAPAMI.AtributosNodoArbol
{

    public class Cls_atributos_nodo_arbol
    {


        #region  propiedades
        private String valor1_;
        private String valor2_;
        private String valor3_;
        private String valor4_;
        private String valor5_;
        private String valor6_;
        private String valor7_;
        #endregion


        public string valor1 {
            get { return valor1_; }
            set { valor1_ = value; }
        }

        public string valor2 {
            get { return valor2_; }
            set { valor2_ = value; }
        }

        public string valor3
        {
            get { return valor3_; }
            set { valor3_ = value; }
        }

        public string valor4
        {
            get { return valor4_; }
            set { valor4_ = value; }
        }

        public string valor5
        {
            get { return valor5_; }
            set { valor5_ = value; }
        }

        public string valor6
        {
            get { return valor6_; }
            set { valor6_ = value; }
        }

        public string valor7
        {
            get { return valor7_; }
            set { valor7_ = value; }
        }


    }


}

