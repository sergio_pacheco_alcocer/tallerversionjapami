﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JAPAMI.AtributosNodoArbol;

/// <summary>
/// Summary description for Cls_nodo_arbol
/// </summary>
/// 

namespace JAPAMI.NodoArbol

{

    public class Cls_nodo_arbol
    {

        # region  propiedades

        private String texto_;
        private String id_;
        private String state_;
        private Cls_atributos_nodo_arbol attributes_;
        private Boolean checked1_;

        #endregion

        public Boolean checked1{
            get { return checked1_; }
            set { checked1_ = value; }
         
        }

        public Cls_atributos_nodo_arbol attributes
        {
            get { return attributes_; }
            set { attributes_ = value; }
        }

        public string state {
            get { return state_; }
            set { state_ = value; }
        }

        public String text {
            get { return texto_; }
            set { texto_=value;}
        }
        public string id {
            get { return id_; }
            set { id_ = value; }
        }


    }


}

