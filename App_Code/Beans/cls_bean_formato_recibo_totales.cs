﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;



namespace JAPAMI.Formato_Recibo_Totales {

    public class cls_bean_formato_recibo_totales
    {
        public cls_bean_formato_recibo_totales()
        {

        }

        public double total_iva { get; set; }
        public double total_pagar { get; set; }
        public double saldo { get; set; }
        public double Descuento { get; set; }        
        public string Descripcion { get; set; }
        public double total_pagar_Bonificacion { get; set; }


    }



}


