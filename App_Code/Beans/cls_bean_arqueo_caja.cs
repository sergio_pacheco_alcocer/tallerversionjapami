﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;



namespace JAPAMI.Bean_Arqueo_Caja {


    public class cls_bean_arqueo_caja
    {

        public string no_corte { get; set; }
        public string operacion_caja_id { get; set; }
        public string fecha { get; set; }
        public string hora_inicial { get; set; }
        public string hora_final { get; set; }
        public int cantidad_moneda_0_05 { get; set; }
        public int cantidad_moneda_0_10 { get; set; }
        public int cantidad_moneda_0_20 { get; set; }
        public int cantidad_moneda_0_50 { get; set; }
        public int cantidad_moneda_1 { get; set; }
        public int cantidad_moneda_2 { get; set; }
        public int cantidad_moneda_5 { get; set; }
        public int cantidad_moneda_10 { get; set; }
        public int cantidad_moneda_20 { get; set; }
        public int cantidad_moneda_100 { get; set; }
        public int cantidad_billete_20 { get; set; }
        public int cantidad_billete_50 { get; set; }
        public int cantidad_billete_100 { get; set; }
        public int cantidad_billete_200 { get; set; }
        public int cantidad_billete_500 { get; set; }
        public int cantidad_billete_1000 { get; set; }
        public double total_efectivo { get; set; }
        public double total_cheque { get; set; }
        public double total_tarjeta { get; set; }
        public double total { get; set; }
        public double total_retiros { get; set; }
        public int total_movimentos { get; set; }
        public double total_pagos { get; set; }



    }



}


