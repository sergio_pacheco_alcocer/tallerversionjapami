﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace JAPAMI.Bean.PagodiversoEncabezado {

    public class cls_bean_pago_diverso_encabezado
    {
        public cls_bean_pago_diverso_encabezado()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public String no_diverso { set; get; }
        public String codigo_barras { set; get; }
        public String tipo_diverso { set; get; }
        public String usuario_creo { set; get; }
        public String no_cuenta_agregar { set; get; }
        public String id_usuario { set; get; }
        public String nombre { set; get; }
        public String calle { set; get; }
        public String colonia { set; get; }
        public String numero { set; get; }
        public String rfc { set; get; }
        public String ciudad { set; get; }
        public String estado { set; get; }
        public double iva { set; get; }
        public double subtotal { set; get; }
        public double importe_iva { set; get; }
        public double total { set; get; }
        public String fecha_limite { set; get; }
        public String fecha_emision { set; get; }
        public String domicilio { set; get; }
        public String estatus { set; get; }
        public String convenio { set; get; }


    }



}


