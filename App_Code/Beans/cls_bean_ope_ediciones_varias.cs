﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace JAPAMI.EdicionesVarias
{

    public class cls_bean_ope_ediciones_varias
    {
        public cls_bean_ope_ediciones_varias()
        {            
        }

        public string valor_anterior { set; get; }
        public string valor_nuevo { set; get; }
        public string usuario { set; get; }
        public string nombre_columna { set; get; }
        public string tabla_editada { set; get; }
        public string descripcion_edicion { set; get; }
        public string no_cuenta { set; get; }
        

    }
}