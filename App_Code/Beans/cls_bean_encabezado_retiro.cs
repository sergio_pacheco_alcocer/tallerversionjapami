﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace JAPAMI.Bean_encabezado_retiro
{

    public class cls_bean_encabezado_retiro
    {

        public string no_retiro { get; set; }
        public string operacion_caja_id { get; set; }
        public string fecha { get; set; }
        public string hora_inicio { get; set; }
        public string hora_fina { get; set; }
        public double total { get; set; }


    }
}