﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace JAPAMI.Formato_Recibo_Encabezados {


    public class cls_bean_formato_recibo
    {
        public cls_bean_formato_recibo()
        {
        }

        public string no_factura { set; get; }
        public string codigo_barras { set; get; }
        public string fecha_inicio_periodo { set; get; }
        public string fecha_termino_periodo { set; get; }
        public string no_cuenta { set; get; }
        public string lectura_anterior { set; get; }
        public string lectura_actual { set; get; }
        public string consumo { set; get; }
        public string precio_cubico { set; get; }
        public string fecha_emision { set; get; }
        public string fecha_limite_pago { set; get; }
        public string tasa_iva { set; get; }
        public string periodo_facturacion { set; get; }
        public string numero_region { set; get; }
        public string numero_sector { set; get; }
        public string numero_reparto { set; get; }
        public string usuario { set; get; }
        public string domicilio { set; get; }
        public string colonia { set; get; }
        public string tarifa { set; get; }
        public string giro { set; get; }
        public string medidor { set; get; }
        public string total_iva { set; get; }
        public string rfc { set; get; }
        public string fecha_formato { set; get; }


    }



}
