﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace JAPAMI.Bean_Pago_Anticipado {

    public class cls_bean_pago_anticipado
    {
        public cls_bean_pago_anticipado()
        {

        }

        public string no_factura_recibo_Parcial { set; get; }
        public string no_factura_recibo { set; get; }
        public string no_cuenta { set; get; }
        public string codigo_barras { set; get; }
        public string codigo_barras_parcial { set; get; }
        public double total_pagar { set; get; }
        public double total_abono { set; get; }
        public string estado_recibo { set; get; }
        public int numero_meses { set; get; }
        public string usuario_creo { set; get; }
        public double total_iva { set; get; }
        public double importe_anticipo { set; get; }
        public double porcentaje_descuento { set; get; }
        public string fecha_formato { set; get; }
        public string fecha_emision { set; get; }
        public string comentarios { set; get; }
        public string usuario_registro_descuento_alto { set; get; }
        public double promedio_consumo { set; get; }
        public double descuento_anticipo_cantidad { set; get; }
        public double fecha_limite_anticipo { set; get; }

    }


}

