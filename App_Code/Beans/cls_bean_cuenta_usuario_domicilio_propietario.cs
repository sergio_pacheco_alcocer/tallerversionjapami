﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;



namespace JAPAMI.Bean_Cuenta_Domicilio_Propietario
{

    public class cls_bean_cuenta_usuario_domicilio_propietario
    {
        public cls_bean_cuenta_usuario_domicilio_propietario()
        {

        }

        public string no_cuenta { set; get; }
        public string razon_social { set; get; }
        public string usuario_id { set; get; }
        public string nombre { set; get; }
        public string apellido_paterno { set; get; }
        public string apellido_materno { set; get; }
        public string colonia_propietario { set; get; }
        public string colonia_id { set; get; }
        public string calle_id { set; get; }
        public string calle_propietario { set; get; }
        public string no_exterior_propietario { set; get; }
        public string no_interior_propietario { set; get; }
        public string telefono_casa_propietario { set; get; }
        public string rfc { set; get; }



    }

}


