﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace JAPAMI.Bean_Cuenta_Usuario_Datos_Generales {


    public class cls_bean_cuenta_usuario_datos_generales
    {
        public cls_bean_cuenta_usuario_datos_generales()
        {
        }


        public string predio_id { get; set; }
        public string no_cuenta { get; set; }
        public string colonia { get; set; }
        public string colonia_id { get; set; }
        public string calle { get; set; }
        public string calle_id { get; set; }
        public string calle_referencia1_id { get; set; }
        public string calle_refencia1 { get; set; }
        public string calle_referencia2_id { get; set; }
        public string calle_refencia2 { get; set; }
        public string numero_exterior { get; set; }
        public string numero_interior { get; set; }
        public string numero_zona { get; set; }
        public string zona_id { get; set; }
        public string region_id { get; set; }
        public string no_region { get; set; }
        public string sector_id { get; set; }
        public string no_sector { get; set; }
        public string manzana_id { get; set; }
        public string nombre_manzana { get; set; }
        public string no_reparto { get; set; }

        public string giro_id { get; set; }
        public string nombre_giro { get; set; }
        public string giro_actividad_id { get; set; }
        public string actividad { get; set; }
        public string tarifa_id { get; set; }
        public string nombre_tarifa { get; set; }
        public string vivienda_id { get; set; }
        public string vivienda { get; set; }

        public string servicio { get; set; }
        public string grupo_concepto_id { get; set; }
        
    }


}


