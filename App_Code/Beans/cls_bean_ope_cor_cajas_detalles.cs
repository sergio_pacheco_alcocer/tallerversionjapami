﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;



namespace JAPAMI.Bean_apertura_caja_detalles {

    public class cls_bean_ope_cor_cajas_detalles
    {
        public cls_bean_ope_cor_cajas_detalles()
        {
        }

        public String operacion_caja_id { get; set; }
        public int cantidad_moneda_0_05 { get; set; }
        public int cantidad_moneda_0_10 { get; set; }
        public int cantidad_moneda_0_20 { get; set; }
        public int cantidad_moneda_0_50 { get; set; }
        public int cantidad_moneda_1 { get; set; }
        public int cantidad_moneda_2 { get; set; }
        public int cantidad_moneda_5 { get; set; }
        public int cantidad_moneda_10 { get; set; }
        public int cantidad_moneda_20 { get; set; }
        public int cantidad_moneda_100 { get; set; }
        public int cantidad_billete_20 { get; set; }
        public int cantidad_billete_50 { get; set; }
        public int cantidad_billete_100 { get; set; }
        public int cantidad_billete_200 { get; set; }
        public int cantidad_billete_500 { get; set; }
        public int cantidad_billete_1000 { get; set; }
        public double total_efectivo { get; set; }
        public double total { get; set; }
        public double total_cheques { get; set; }
        public double total_tarjetas { get; set; }
        public double retiro_cierre { get; set; }
        public double total_movimiento_cierre { get; set; }
        public double total_pagos_cierres { get; set; }


    }



}

