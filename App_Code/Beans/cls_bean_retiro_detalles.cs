﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace JAPAMI.Bean_retiro_detalles
{

    public class cls_bean_retiro_detalles
    {
        public int cantidad_moneda_0_05 { set; get; }
        public int cantidad_moneda_0_10 { set; get; }
        public int cantidad_moneda_0_20 { set; get; }
        public int cantidad_moneda_0_50 { set; get; }
        public int cantidad_moneda_1 { set; get; }
        public int cantidad_moneda_2 { set; get; }
        public int cantidad_moneda_5 { set; get; }
        public int cantidad_moneda_10 { set; get; }
        public int cantidad_moneda_20 { set; get; }
        public int cantidad_moneda_100 { set; get; }
        public int cantidad_billete_20 { set; get; }
        public int cantidad_billete_50 { set; get; }
        public int cantidad_billete_100 { set; get; }
        public int cantidad_billete_200 { set; get; }
        public int cantidad_billete_500 { set; get; }
        public int cantidad_billete_1000 { set; get; }
        
    }

}
