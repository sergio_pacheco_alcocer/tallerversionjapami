﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;



namespace JAPAMI.Formato_Concepto_Cobro {

    public class cls_bean_formato_conceptos_cobro
    {

        public string concepto { set; get; }
        public string concepto_id { set; get; }
        public double importe { set; get; }
        public double impuesto { set; get; }
        public double importe_abonado { set; get;}
        public double impuesto_abonado { set; get; }
        public double importe_saldo { set; get; }
        public double impuesto_saldo { set; get; }
        public double tasa_iva { set; get; }
    }



}

