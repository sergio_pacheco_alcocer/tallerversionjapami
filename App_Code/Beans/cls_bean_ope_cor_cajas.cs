﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace JAPAMI.Bean_apertura_caja {

    public class cls_bean_ope_cor_cajas
    {
        public cls_bean_ope_cor_cajas()
        {
          
        }

       public String caja_id {get; set;}
       public String empleado_id { get; set; }
       public String sucursal_id { get; set; }
       public String operacion_caja_id {get; set; }
       public String estado { get; set; }
       public double monto_inicial {get; set;}
       public String usuario_creo { get; set; }


    }



}

