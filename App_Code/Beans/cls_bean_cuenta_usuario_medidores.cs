﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace JAPAMI.Bean_Cuenta_Medidores {


    public class cls_bean_cuenta_usuario_medidores
    {
        public cls_bean_cuenta_usuario_medidores()
        {
    
        }

        public string no_medidor { get; set; }
        public string diametro { get; set; }
        public string marca { get; set; }
        public string medidor_id { get; set; }
        public string marca_id { get; set; }
        public string fecha_instalacion { get; set; }
        public string no_cuenta { get; set; }
        public string limite_medidor { get; set; }


    }

}


