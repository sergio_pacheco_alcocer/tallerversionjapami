﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace JAPAMI.Bean_distribucion_moneda
{

    public class cls_bean_distrubicion_moneda
    {
        public cls_bean_distrubicion_moneda()
        {
           
        }

        public String tipo_moneda { set; get; }
        public int cantidad { set; get; }
        public String importe_efectivo { set; get; }


    }
}