﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;



namespace JAPAMI.Bean_Cuenta_Datos_Cajero {


    public class Cls_bean_datos_cajero
    {

        public String nombre_sucursal { get; set; }
        public String nombre { set; get; }
        public int no_caja { set; get; }
        public double monto_inicial { set; get; }
        public String estado { set; get; }
        public String fecha { set; get; }
        public String hora { set; get; }
        public string monto_final { set; get; }
        public string fecha_cierre { set; get; }
        public string hora_cierre { set; get; }


    }



}

