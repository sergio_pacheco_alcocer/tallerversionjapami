﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace JAPAMI.Bean_Pago_Bonificacion {

    public class cls_bean_pago_bonificacion
    {

        public cls_bean_pago_bonificacion()
        {

        }

        public string bonificacion_id { set; get; }
        public string motivo_bonificacion_id { set; get; }
        public string no_cuenta { set; get; }
        public string tipo { set; get; }
        public string fecha_formato { set; get; }
        public string fecha_vencimiento { set; get; }
        public double iva { set; get; }
        public double cantidad_iva { set; get; }
        public double total_pagar { set; get; }
        public double total_descuento { set; get; }
        public string usuario_creo { set; get; }
        public string descripcion_bonificacion { set; get; }
        public string no_factura_recibo { set; get; }
        public string codigo_barras { set; get; }
        public string nombre { set; get; }
        public string domicilio { set; get; }
        public string colonia { set; get; }
        public string estatus_bonificacion { set; get; }
        public string no_factura_recibo_parcial { set; get; }
        public double importe_bonificacion { set; get; }
        public double importe_total_pagar { set; get; }
        public double total_iva_pagar { set; get; }



    }




}

