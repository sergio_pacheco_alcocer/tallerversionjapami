﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace JAPAMI.Formato_Recibo_Cosumo {

    public class cls_bean_formato_recibo_cosumo
    {
        public cls_bean_formato_recibo_cosumo()
        {

        }

        public string periodo_facturacion { set; get; }
        public string fecha_emision { set; get; }
        public string total_pagar { set; get; }
        public string lectura_anterior { set; get; }
        public string lectura_actual { set; get; }
        public string consumo { set; get; }

    }


}


