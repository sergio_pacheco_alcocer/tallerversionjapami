﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
/// <summary>
/// Summary description for Cls_Bitacora
/// </summary>
/// 

namespace JAPAMI.Bitacora_Eventos
{

    public class Cls_Bitacora
    {
        string Xml_Controles_Inicial = "";
        string Xml_Controles_Final = "";
        public Cls_Bitacora()
        {
            //
            // TODO: Add constructor logic here
            //
        }


        #region Metodos 
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Revisar_Actualizaciones
        ///DESCRIPCIÓN: Realiza la comparacion de dos data set para 
        ///verificar las diferencias de cada uno de sus campos
        ///PARAMETROS: 1.-Ds_Anterior, es el primer data a comparar
        ///            2.-Ds_Actual, segundo data set a comparar
        ///CREO: Silvia Morales Portuhondo
        ///FECHA_CREO: 17/Septiembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************


        public static String Revisar_Actualizaciones(DataSet Ds_Anterior, DataSet Ds_Actual)
        {

            String Actualizaciones = "Se actualizaron los siguientes datos: \n";
            String Dato1 = "";
            String Dato2 = "";
            int Num_Campos_Actualizados = 0;
            for (int i = 0; i < Ds_Actual.Tables[0].Columns.Count; i++)
            {
                Dato1 = Ds_Anterior.Tables[0].Rows[0].ItemArray[i].ToString();
                Dato2 = Ds_Actual.Tables[0].Rows[0].ItemArray[i].ToString();
                if (Dato1 != Dato2)
                {
                    Actualizaciones = Actualizaciones + Ds_Actual.Tables[0].Columns[i].ToString() +
                        ": " + Dato1 + " cambio a " + Dato2 + "\n";
                    Num_Campos_Actualizados = Num_Campos_Actualizados + 1;
                }
            }

            if (Num_Campos_Actualizados == 0)
            {
                Actualizaciones = "No se modificaron datos";
            }
            return Actualizaciones;
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Bitacora
        ///DESCRIPCIÓN: Metodo que registra un evento en la tabla de Apl_Bitacora
        ///PARAMETROS:   1.- String Usuario: Usuario que se logueo
        ///              2.- String Accion: Accion que ejecuta el usuario (Alta, Baja, Consulta, Imprimir, Modificar, Acceso)
        ///              3.- String Recurso: Nombre del catalogo en el que se realiza la accion ejem. "Frm_Cat_Colonias"
        ///              4.- String Nombre_Recurso: Nombre del recurso del que se realizo una accion 
        ///              5.- String Descripcion: descripcion de la accion que se realizo 
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 01/Octubre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Alta_Bitacora(String Usuario, String Accion, String Pagina, String Nombre_Recurso, String Descripcion)
        {
            //Obtiene la cadena de inserción hacía la base de datos
            String Mi_SQL = "";         
            // Aplicamos un substring al parametro de Descripcion,para validar que no sobrepase los 4000 de longitud validos 
            if(Descripcion.Length >3999)
               Descripcion = Descripcion.Substring(0,3999);
            //se le asigna un caracter para diferenciar hasta donde termina la descripcion 
            Descripcion = Descripcion + "_";
            //Obtiene el ID con la cual se guardo los datos en la base de datos
            int Bitacora_ID = Obtener_Consecutivo(Apl_Bitacora.Campo_Bitacora_ID, Apl_Bitacora.Tabla_Apl_Bitacora);

            // Realizamos la sentencia para insertar en la tabla APL_BITACORA 
            Mi_SQL = "INSERT INTO " + Apl_Bitacora.Tabla_Apl_Bitacora + 
                " (" + Apl_Bitacora.Campo_Bitacora_ID + 
                ", " + Apl_Bitacora.Campo_Empleado_ID + 
                ", " + Apl_Bitacora.Campo_Fecha_Hora + 
                ", " + Apl_Bitacora.Campo_Accion +
                ", " + Apl_Bitacora.Campo_Recurso + 
                ", " + Apl_Bitacora.Campo_Recurso_ID + 
                ", " + Apl_Bitacora.Campo_Descripcion + ") VALUES " +
                "('" + Bitacora_ID.ToString() +
                "','" + Usuario + 
                "', GETDATE(),'" + Accion + 
                "','" + Pagina + 
                "','" + Nombre_Recurso + 
                "','" + Descripcion + "')";

            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
        
        }//fin de Alta_Bitacora

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Consecutivo
        ///DESCRIPCIÓN: Obtiene el numero consecutivo para las tablas ocupadas en esta clase
        ///PARAMETROS: 1.-Campo del cual se obtendra el consecutivo
        ///            2.-Nombre de la tabla
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 10/Enero/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Obtener_Consecutivo(String Campo_ID, String Tabla)
        {
            int Consecutivo = 0;
            String Mi_Sql;
            Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos
            Mi_Sql = "SELECT ISNULL(MAX (" + Campo_ID + "),'00000') FROM " + Tabla;
            Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            Consecutivo = (Convert.ToInt32(Obj) + 1);
            return Consecutivo;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Cambios_Alta
        ///DESCRIPCIÓN: Obtiene la información del formulario para obtener los datos que se agregaron
        ///PARAMETROS: frm: Formulario que se consulta
        ///CREO:        Yañez Rodriguez Diego N.
        ///FECHA_CREO:  17/Agosto/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static String Cambios_Alta(System.Web.UI.Page webcontrols)
        {
            ContentPlaceHolder mpContentPlaceHolder;
            mpContentPlaceHolder = (ContentPlaceHolder)(webcontrols.Form.NamingContainer.FindControl("Cph_Area_Trabajo1"));

            string Cadena_Cambios = "";

            foreach (Control web in mpContentPlaceHolder.Controls)
            {
                foreach (Control webchild in web.Controls)
                {

                    if (webchild.GetType() == typeof(TextBox))
                    {
                        //Genera la cadena de cambios
                        if (!((TextBox)webchild).Text.Equals(""))
                        {
                            Cadena_Cambios += "Campo:" + webchild.ID.ToString().Substring(4).ToString() + ", Valor: " + ((TextBox)webchild).Text + "\n";
                        }
                    }
                    if (webchild.GetType() == typeof(DropDownList))
                    {
                        //Genera la cadena de cambios
                        Cadena_Cambios += "Campo:" + webchild.ID.ToString().Substring(4).ToString() + ", Valor: " + ((DropDownList)webchild).SelectedItem.ToString()+ "\n";
                    }
                    if (webchild.GetType() == typeof(CheckBox))
                    {
                        //Genera la cadena de cambios
                        Cadena_Cambios += "Campo:" + webchild.ID.ToString().Substring(4).ToString() + ", Valor: " + ((CheckBox)webchild).Checked.ToString() + "\n";
                    }
                    if (webchild.GetType() == typeof(RadioButton))
                    {
                        //Genera la cadena de cambios
                        Cadena_Cambios += "Campo:" + webchild.ID.ToString().Substring(4).ToString() + ", Valor: " + ((RadioButton)webchild).Checked.ToString() + "\n";
                    }
                    foreach (Control webchildchild in webchild.Controls)
                    {
                        if (webchildchild.GetType() == typeof(TextBox))
                        {
                            //Genera la cadena de cambios
                            if (!((TextBox)webchildchild).Text.Equals(""))
                            {
                                Cadena_Cambios += "Campo:" + webchildchild.ID.ToString().Substring(4).ToString() + ", Valor: " + ((TextBox)webchildchild).Text + "\n";
                            }
                        }
                        if (webchildchild.GetType() == typeof(DropDownList))
                        {
                            //Genera la cadena de cambios
                            Cadena_Cambios += "Campo:" + webchildchild.ID.ToString().Substring(4).ToString() + ", Valor: " + ((DropDownList)webchildchild).SelectedItem.ToString() + "\n";
                        }
                        if (webchildchild.GetType() == typeof(CheckBox))
                        {
                            //Genera la cadena de cambios
                            Cadena_Cambios += "Campo:" + webchildchild.ID.ToString().Substring(4).ToString() + ", Valor: " + ((CheckBox)webchildchild).Checked.ToString() + "\n";
                        }
                        if (webchildchild.GetType() == typeof(RadioButton))
                        {
                            //Genera la cadena de cambios
                            Cadena_Cambios += "Campo:" + webchildchild.ID.ToString().Substring(4).ToString() + ", Valor: " + ((RadioButton)webchildchild).Checked.ToString() + "\n";
                        }
                    }
                }
            }
            return Cadena_Cambios;
        }
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Cambios_Modificar
        ///DESCRIPCIÓN: Realiza la comparación de la formulario inicial con la final y arroja una cadea con los cambios
        ///PARAMETROS: 
        ///CREO:        Yañez Rodriguez Diego N.
        ///FECHA_CREO:  17/Agosto/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static String Cambios_Modificar(System.Web.UI.Page webcontrols)
        {
            string Forma_Cambios_Final = Estatus_Final(webcontrols);
            string Cadena_Cambios = "";
            //Realiza la comparación de la información
            //Parte la cadena por |
            string[] Inicial = Cls_Sessiones.Forma_Cambios.Split('|');
            string[] Final = Forma_Cambios_Final.Split('|');

            if (!Cls_Sessiones.Forma_Cambios.Equals("") && !Forma_Cambios_Final.Equals(""))
            {
                for (int cont = 0; cont < Inicial.Length - 1; cont++)
                {
                    if (!Inicial[cont].Equals(Final[cont]))
                    {
                        string Campo = "";
                        string valor1 = "";
                        string valor2 = "";

                        Campo = Inicial[cont].Remove(Inicial[cont].IndexOf(',')).ToString();
                        valor1 = Inicial[cont].Substring(Inicial[cont].IndexOf(','));
                        valor1 = valor1.Substring(valor1.IndexOf(':') + 1);
                        valor2 = Final[cont].Substring(Final[cont].IndexOf(','));
                        valor2 = valor2.Substring(valor2.IndexOf(':') + 1);
                        Cadena_Cambios += Campo + ", Modifico: " + valor1 + " -> " + valor2;
                    }
                }
            }
            else
            {
                Cadena_Cambios = "No hubo ningun cambio";
            }
            return Cadena_Cambios;
        }
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Estatus_Inicial
        ///DESCRIPCIÓN: Obtiene la información inicial con la que el formulario se carga
        ///PARAMETROS: 
        ///CREO:        Yañez Rodriguez Diego N.
        ///FECHA_CREO:  17/Agosto/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Estatus_Inicial(System.Web.UI.Page webcontrols)
        {
            ContentPlaceHolder mpContentPlaceHolder;
            mpContentPlaceHolder = (ContentPlaceHolder)(webcontrols.Form.NamingContainer.FindControl("Cph_Area_Trabajo1"));

            string Cadena_Cambios = "";

            foreach (Control web in mpContentPlaceHolder.Controls)
            {
                foreach (Control webchild in web.Controls)
                {

                    if (webchild.GetType() == typeof(TextBox))
                    {
                        Cadena_Cambios += "Campo:" + webchild.ID.ToString().Substring(4).ToString() + ", Valor:" + ((TextBox)webchild).Text + "|";
                    }
                    if (webchild.GetType() == typeof(DropDownList))
                    {
                        //Genera la cadena de cambios
                        Cadena_Cambios += "Campo:" + webchild.ID.ToString().Substring(4).ToString() + ", Valor:" + ((DropDownList)webchild).SelectedItem.ToString() + "|";
                    }
                    if (webchild.GetType() == typeof(CheckBox))
                    {
                        //Genera la cadena de cambios
                        Cadena_Cambios += "Campo:" + webchild.ID.ToString().Substring(4).ToString() + ", Valor:" + ((CheckBox)webchild).Checked.ToString() + "|";
                    }
                    if (webchild.GetType() == typeof(RadioButton))
                    {
                        //Genera la cadena de cambios
                        Cadena_Cambios += "Campo:" + webchild.ID.ToString().Substring(4).ToString() + ", Valor:" + ((RadioButton)webchild).Checked.ToString() + "|";
                    }
                    foreach (Control webchildchild in webchild.Controls)
                    {
                        if (webchildchild.GetType() == typeof(TextBox))
                        {
                            //Genera la cadena de cambios
                            Cadena_Cambios += "Campo:" + webchildchild.ID.ToString().Substring(4).ToString() + ", Valor:" + ((TextBox)webchildchild).Text + "|";
                        }
                        if (webchildchild.GetType() == typeof(DropDownList))
                        {
                            //Genera la cadena de cambios
                            Cadena_Cambios += "Campo:" + webchildchild.ID.ToString().Substring(4).ToString() + ", Valor:" + ((DropDownList)webchildchild).SelectedItem.ToString() + "|";
                        }
                        if (webchildchild.GetType() == typeof(CheckBox))
                        {
                            //Genera la cadena de cambios
                            Cadena_Cambios += "Campo:" + webchildchild.ID.ToString().Substring(4).ToString() + ", Valor:" + ((CheckBox)webchildchild).Checked.ToString() + "|";
                        }
                        if (webchildchild.GetType() == typeof(RadioButton))
                        {
                            //Genera la cadena de cambios
                            Cadena_Cambios += "Campo:" + webchildchild.ID.ToString().Substring(4).ToString() + ", Valor:" + ((RadioButton)webchildchild).Checked.ToString() + "|";
                        }
                    }
                }
            }
            Cls_Sessiones.Forma_Cambios = Cadena_Cambios;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Estatus_Final
        ///DESCRIPCIÓN: Obtiene la información Final con la que el formulario se carga
        ///PARAMETROS: 
        ///CREO:        Yañez Rodriguez Diego N.
        ///FECHA_CREO:  17/Agosto/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static String Estatus_Final(System.Web.UI.Page webcontrols)
        {
            ContentPlaceHolder mpContentPlaceHolder;
            mpContentPlaceHolder = (ContentPlaceHolder)(webcontrols.Form.NamingContainer.FindControl("Cph_Area_Trabajo1"));

            string Cadena_Cambios = "";

            foreach (Control web in mpContentPlaceHolder.Controls)
            {
                foreach (Control webchild in web.Controls)
                {

                    if (webchild.GetType() == typeof(TextBox))
                    {
                        Cadena_Cambios += "Campo:" + webchild.ID.ToString().Substring(4).ToString() + ", Valor:" + ((TextBox)webchild).Text + "|";
                    }
                    if (webchild.GetType() == typeof(DropDownList))
                    {
                        //Genera la cadena de cambios
                        Cadena_Cambios += "Campo:" + webchild.ID.ToString().Substring(4).ToString() + ", Valor:" + ((DropDownList)webchild).SelectedItem.ToString() + "|";
                    }
                    if (webchild.GetType() == typeof(CheckBox))
                    {
                        //Genera la cadena de cambios
                        Cadena_Cambios += "Campo:" + webchild.ID.ToString().Substring(4).ToString() + ", Valor:" + ((CheckBox)webchild).Checked.ToString() + "|";
                    }
                    if (webchild.GetType() == typeof(RadioButton))
                    {
                        //Genera la cadena de cambios
                        Cadena_Cambios += "Campo:" + webchild.ID.ToString().Substring(4).ToString() + ", Valor:" + ((RadioButton)webchild).Checked.ToString() + "|";
                    }
                    foreach (Control webchildchild in webchild.Controls)
                    {
                        if (webchildchild.GetType() == typeof(TextBox))
                        {
                            Cadena_Cambios += "Campo:" + webchildchild.ID.ToString().Substring(4).ToString() + ", Valor:" + ((TextBox)webchildchild).Text + "|";
                        }
                        if (webchildchild.GetType() == typeof(DropDownList))
                        {
                            //Genera la cadena de cambios
                            Cadena_Cambios += "Campo:" + webchildchild.ID.ToString().Substring(4).ToString() + ", Valor:" + ((DropDownList)webchildchild).SelectedItem.ToString() + "|";
                        }
                        if (webchildchild.GetType() == typeof(CheckBox))
                        {
                            //Genera la cadena de cambios
                            Cadena_Cambios += "Campo:" + webchildchild.ID.ToString().Substring(4).ToString() + ", Valor:" + ((CheckBox)webchildchild).Checked.ToString() + "|";
                        }
                        if (webchildchild.GetType() == typeof(RadioButton))
                        {
                            //Genera la cadena de cambios
                            Cadena_Cambios += "Campo:" + webchildchild.ID.ToString().Substring(4).ToString() + ", Valor:" + ((RadioButton)webchildchild).Checked.ToString() + "|";
                        }
                    }
                }
            }
            return Cadena_Cambios;
        }
    }
    #endregion

}
