﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for Cls_Archivos_Temporales
/// </summary>
/// 
namespace JAPAMI.Archivos.Temporales {

    public class Cls_Archivos_Temporales
    {
        public Cls_Archivos_Temporales()
        {
        }

        public string nombre_archivo { set; get; }
        public string nombre_nuevo { set; get; }
        public string comentario { set; get; }
    }


}
