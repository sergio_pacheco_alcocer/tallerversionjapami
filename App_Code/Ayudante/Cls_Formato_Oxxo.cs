﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;



namespace JAPAMI.FormatoArchivoOxxo {

    public class Cls_Formato_Oxxo
    {

        public String tienda { set; get; }
        public String no_cuenta { set; get; }
        public String fecha_pago { set; get; }
        public double monto { set; get; }
        public double saldo { set; get; }
        public String codigo { set; get; }
        public String no_factura_recibo { set; get; }

        public Cls_Formato_Oxxo()
        {
           
        }
    }


}

