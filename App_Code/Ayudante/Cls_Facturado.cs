﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace JAPAMI.Facturado_usuario {

    public class Cls_Facturado
    {

        public string concepto { set; get; }
        public double actual { set; get; }
        public double pago_actual { set; get; }
        public double anterior1 { set; get; }
        public double pago_anterior1 { set; get; }
        public double anterior2 { set; get; }
        public double pago_anterior2 { set; get; }
        public  string id_concepto { set; get; }
        public int index { set; get; }
        public string estado_actual { set; get; }
        public string estado_anterior1 { set; get; }
        public string estado_anterior2 { set; get; }

        public Double total_importe_actual { set; get; }
        public Double total_iva_actual { set; get; }
        public Double total_pagar_actual { set; get; }

        public Double total_importe_anterior1 { set; get; }
        public Double total_iva_anterior1 { set; get; }
        public Double total_pagar_actual_anterior1 { set; get; }

        public Double total_importe_anterior2 { set; get; }
        public Double total_iva_anterior2 { set; get; }
        public Double total_pagar_actual_anterior2 { set; get; }


        public Double total_abonado_actual { set; get; }
        public Double total_abonado_anterior1 { set; get; }
        public Double total_abonado_anterior2 { set; get; }



        public Cls_Facturado()
        {

        }



    }



}

