﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net.Mail;
using System.Web.Util;
using System.Net;
using System.Net.Mime;
using JAPAMI.Registro_Peticion.Datos;

//using System.Web.Mail;

/// <summary>
/// Summary description for Mail
/// </summary>
public class Cls_Mail{
    //datos del mail
    private String Envia;
    private String Recibe;
    private String Subject;
    private String Texto;
    //datos de configuracion
    private String Servidor;
    private String Password;
    private String Adjunto;
    private String Nmb_Imagen_Compras;
    private String Nmb_Imagen_Gerente;

    public String P_Adjunto
    {
        get { return Adjunto; }
        set { Adjunto = value; }
    }
    public String P_Nmb_Imagen_Compras
    {
        get { return Nmb_Imagen_Compras; }
        set { Nmb_Imagen_Compras = value; }
    }
    public String P_Nmb_Imagen_Gerente
    {
        get { return Nmb_Imagen_Gerente; }
        set { Nmb_Imagen_Gerente = value; }
    }
    
	public Cls_Mail()
	{
        Servidor = Cls_Cat_Ate_Peticiones_Datos.Consulta_Parametros().Rows[0][JAPAMI.Constantes.Apl_Parametros.Campo_Servidor_Correo].ToString();
        Envia = Cls_Cat_Ate_Peticiones_Datos.Consulta_Parametros().Rows[0][JAPAMI.Constantes.Apl_Parametros.Campo_Correo_Saliente].ToString();
        Password = Cls_Cat_Ate_Peticiones_Datos.Consulta_Parametros().Rows[0][JAPAMI.Constantes.Apl_Parametros.Campo_Password_Correo].ToString();
        
	}
    public string P_Envia
    {
        get { return Envia; }
        set { Envia = value; }
    }
    public string P_Recibe {
        get { return Recibe; }
        set { Recibe = value; }
    }
    public string P_Subject {
        get { return Subject; }
        set { Subject = value; }
    }
    public string P_Texto {
        get { return Texto; }
        set { Texto = value; }
    }
    public string P_Servidor {
        get { return Servidor; }
        set { Servidor = value; }
    }
    public string P_Password
    {
        get { return Password; }
        set { Password = value; }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: enviar_correo
    ///DESCRIPCIÓN: Realiza los envios del mail
    ///PROPIEDADES: 
    ///METODOS    : 
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 27/Agosto/2009 
    ///MODIFICO:Jesus Toledo
    ///FECHA_MODIFICO: 03/Sep/09
    ///CAUSA_MODIFICACIÓN:Se utilizó System.net.mail
    ///*******************************************************************************
    public void Enviar_Correo() {
        MailMessage email = new MailMessage(new MailAddress(Envia), new MailAddress(Recibe));
        Attachment Archivo_Mail = null;
        
        email.Subject = Subject;
        email.Body = Texto;
        email.IsBodyHtml = true;
        if (Adjunto != null)
        {
            Archivo_Mail = new Attachment(Adjunto);
            email.Attachments.Add(Archivo_Mail);
        }
        //Validamos k contengan el nombre las variables
        if ((!String.IsNullOrEmpty(P_Nmb_Imagen_Compras)) || (!String.IsNullOrEmpty(P_Nmb_Imagen_Gerente)))
        {
            AlternateView plainView = AlternateView.CreateAlternateViewFromString(
                    Texto, null, "text/plain");

            // Nosotros creamos la parte de html para poner la imagen(es), el cid es el id de la imagen
            string htmlBody = "<b>" + Texto + "</b><DIV> </DIV>"
                            + "<img alt=\"\" hspace=0 src=\"cid:Firma_ID\" align=baseline border=0 >"
                            + "<DIV> </DIV>"
                            + "<img alt=\"\" hspace=0 src=\"cid:Firma_ID2\" align=baseline border=0 >";

            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(htmlBody, null, "text/html");
            
            String Ruta = HttpContext.Current.Server.MapPath("~\\Archivos") + "\\Img_Firmas";
            
            //Creamos el resource de la imagen para LinkedResource class..            
            LinkedResource imageResource = new LinkedResource(Ruta + "\\" + P_Nmb_Imagen_Compras, "image/jpeg");
            imageResource.ContentId = "Firma_ID";
            imageResource.TransferEncoding = TransferEncoding.Base64;

            LinkedResource imageResource2 = new LinkedResource(Ruta + "\\" + P_Nmb_Imagen_Gerente, "image/jpeg");
            imageResource2.ContentId = "Firma_ID2";
            imageResource2.TransferEncoding = TransferEncoding.Base64;

            // adding the imaged linked to htmlView...
            htmlView.LinkedResources.Add(imageResource);
            htmlView.LinkedResources.Add(imageResource2);

            // add the views
            email.AlternateViews.Add(plainView);
            email.AlternateViews.Add(htmlView);
        }

        ////////////////////////////////
        System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
        smtp.Host = Servidor;
        smtp.Port = 25;
        smtp.UseDefaultCredentials = false;
        smtp.Credentials = new NetworkCredential(Envia, Password);
        smtp.Send(email);        
        email = null;        
        
    }//fin de enviar correo

}
