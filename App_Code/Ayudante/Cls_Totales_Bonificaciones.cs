﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for Cls_Totales_Bonificaciones
/// </summary>
/// 

namespace JAPAMI.TotalesBonificaciones {
    
    public class Cls_Totales_Bonificaciones
    {
        public string concepto { set; get; }
        public float debe { set; get; }
        public float paga { set; get; }
        public float ajuste { set; get; }


    }


}
