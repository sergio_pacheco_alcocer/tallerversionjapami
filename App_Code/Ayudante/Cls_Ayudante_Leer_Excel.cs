﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.OleDb;
using System.Text;
using System.Collections.Generic;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Empleados.Negocios;
using JAPAMI.Proveedores.Negocios;
using JAPAMI.Cap_Masiva_Prov_Fijas.Negocio;
using JAPAMI.Cat_Nom_Percepciones_Deducciones_Opcional.Negocios;

namespace JAPAMI.Ayudante_Excel
{
    public class Cls_Ayudante_Leer_Excel
    {
        public static DataTable Leer_Tabla_Excel(String Path, String Tabla)
        {
            OleDbConnection Conexion = new OleDbConnection();
            OleDbCommand Comando = new OleDbCommand();
            OleDbDataAdapter Adaptador = new OleDbDataAdapter();
            DataSet Ds_Informacion = new DataSet();
            String Query = String.Empty;

            try
            {
                Conexion.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" + 
                    "Data Source=" + Path + ";" + 
                    "Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";

                Conexion.Open();
                Comando.CommandText = "Select * From " + Tabla;
                Comando.Connection = Conexion;
                Adaptador.SelectCommand = Comando;
                Adaptador.Fill(Ds_Informacion);
                Conexion.Close();
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al leer un archivo de excel. Error: " + Ex.Message + "]");
            }
            return Ds_Informacion.Tables[0];
        }

        public static DataTable Crear_Estructura_Carga_Masiva(DataTable Dt_Empleados_In)
        {
            Cls_Cat_Empleados_Negocios INF_EMPLEADO = null;
            DataTable Dt_Empleados_Out = new DataTable();
            String No_Empleado = String.Empty;
            String Empleado_ID =String.Empty;
            String Nombre = String.Empty;
            Double Cantidad  = 0.0;
            String Referencia = String.Empty;

            try
            {
                Dt_Empleados_Out.Columns.Add(Cat_Empleados.Campo_Empleado_ID, typeof(String));
                Dt_Empleados_Out.Columns.Add(Cat_Empleados.Campo_Nombre, typeof(String));
                Dt_Empleados_Out.Columns.Add(Cat_Nom_Emp_Perc_Dedu_Deta.Campo_Cantidad, typeof(Double));

                if (Dt_Empleados_In is DataTable) {
                    if (Dt_Empleados_In.Rows.Count > 0) {
                        foreach (DataRow EMPLEADO in Dt_Empleados_In.Rows) {
                            if (EMPLEADO is DataRow) {

                                if (!String.IsNullOrEmpty(EMPLEADO[Cat_Empleados.Campo_No_Empleado].ToString()))
                                {
                                    No_Empleado = EMPLEADO[Cat_Empleados.Campo_No_Empleado].ToString().Trim();
                                    INF_EMPLEADO = JAPAMI.Ayudante_Informacion.Cls_Ayudante_Nom_Informacion._Informacion_Empleado(String.Format("{0:000000}", Convert.ToInt32(No_Empleado)));

                                    if (!String.IsNullOrEmpty(INF_EMPLEADO.P_Empleado_ID))
                                    {
                                        Empleado_ID = INF_EMPLEADO.P_Empleado_ID;
                                        Nombre = "[" + String.Format("{0:000000}", Convert.ToInt32(No_Empleado)) + "] -- " + INF_EMPLEADO.P_Apellido_Paterno + " " + INF_EMPLEADO.P_Apelldo_Materno + " " + INF_EMPLEADO.P_Nombre;

                                        if (!String.IsNullOrEmpty(EMPLEADO[Cat_Nom_Emp_Perc_Dedu_Deta.Campo_Cantidad].ToString()))
                                            Cantidad = Convert.ToDouble(EMPLEADO[Cat_Nom_Emp_Perc_Dedu_Deta.Campo_Cantidad].ToString().Trim());

                                        DataRow RENGLON = Dt_Empleados_Out.NewRow();
                                        RENGLON[Cat_Empleados.Campo_Empleado_ID] = Empleado_ID;
                                        RENGLON[Cat_Empleados.Campo_Nombre] = Nombre;
                                        RENGLON[Cat_Nom_Emp_Perc_Dedu_Deta.Campo_Cantidad] = Cantidad;
                                        Dt_Empleados_Out.Rows.Add(RENGLON);
                                    }
                                    else {
                                        throw new Exception("Hay Empleados en el archivo seleccionado de los cuales su número de empleado no existe en el sistema, por lo tanto el archivo no será cargado!!");
                                    }
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al crear la estructura de la tabla para la carga masiva de informacion. Error: [" + Ex.Message + "]");
            }
            return Dt_Empleados_Out;
        }

        public static DataTable Crear_Estructura_Carga_Masiva_Proveedor(DataTable Dt_Empleados_In, String Proveedor_ID)
        {
            Cls_Cat_Empleados_Negocios INF_EMPLEADO = null;
            DataTable Dt_Empleados_Out = new DataTable();
            String No_Empleado = String.Empty;
            String Empleado_ID = String.Empty;
            String Nombre = String.Empty;
            Double Cantidad = 0.0;
            Double Importe = 0.0;
            String Referencia = String.Empty;
            String Deduccion_ID = String.Empty;
            String Nombre_Deduccion = String.Empty;

            try
            {
                Dt_Empleados_Out.Columns.Add(Cat_Empleados.Campo_Empleado_ID, typeof(String));
                Dt_Empleados_Out.Columns.Add(Cat_Empleados.Campo_Nombre, typeof(String));
                Dt_Empleados_Out.Columns.Add(Cat_Nom_Emp_Perc_Dedu_Deta.Campo_Cantidad, typeof(Double));
                Dt_Empleados_Out.Columns.Add(Cat_Nom_Emp_Perc_Dedu_Deta.Campo_Importe, typeof(Double));
                Dt_Empleados_Out.Columns.Add(Cat_Nom_Emp_Perc_Dedu_Deta.Campo_Saldo, typeof(Double));
                Dt_Empleados_Out.Columns.Add(Cat_Nom_Emp_Perc_Dedu_Deta.Campo_Cantidad_Retenida, typeof(Double));
                Dt_Empleados_Out.Columns.Add("DEDUCCION_ID", typeof(String));
                Dt_Empleados_Out.Columns.Add("NOMBRE_DEDUCCION", typeof(String));


                if (Dt_Empleados_In is DataTable)
                {
                    if (Dt_Empleados_In.Rows.Count > 0)
                    {
                        foreach (DataRow EMPLEADO in Dt_Empleados_In.Rows)
                        {
                            if (EMPLEADO is DataRow)
                            {

                                if (!String.IsNullOrEmpty(EMPLEADO[Cat_Empleados.Campo_No_Empleado].ToString()))
                                {
                                    No_Empleado = EMPLEADO[Cat_Empleados.Campo_No_Empleado].ToString().Trim();
                                    INF_EMPLEADO = JAPAMI.Ayudante_Informacion.Cls_Ayudante_Nom_Informacion._Informacion_Empleado(String.Format("{0:000000}", Convert.ToInt32(No_Empleado)));

                                    if (!String.IsNullOrEmpty(INF_EMPLEADO.P_Empleado_ID))
                                    {
                                        Deduccion_ID = Consultar_Deduccion_Consecutiva_Corresponde_Empleado(INF_EMPLEADO.P_Empleado_ID, Proveedor_ID);
                                        if (!String.IsNullOrEmpty(Deduccion_ID))
                                            Nombre_Deduccion = Consultar_Nombre_Deduccion(Deduccion_ID);
                                        else Nombre_Deduccion = String.Empty;

                                        Empleado_ID = INF_EMPLEADO.P_Empleado_ID;
                                        Nombre = "[" + String.Format("{0:000000}", Convert.ToInt32(No_Empleado)) + "] -- " + INF_EMPLEADO.P_Apellido_Paterno + " " + INF_EMPLEADO.P_Apelldo_Materno + " " + INF_EMPLEADO.P_Nombre;

                                        if (!String.IsNullOrEmpty(EMPLEADO[Cat_Nom_Emp_Perc_Dedu_Deta.Campo_Cantidad].ToString()))
                                            Cantidad = Convert.ToDouble(EMPLEADO[Cat_Nom_Emp_Perc_Dedu_Deta.Campo_Cantidad].ToString().Trim());

                                        if (!String.IsNullOrEmpty(EMPLEADO[Cat_Nom_Emp_Perc_Dedu_Deta.Campo_Importe].ToString()))
                                            Importe = Convert.ToDouble(EMPLEADO[Cat_Nom_Emp_Perc_Dedu_Deta.Campo_Importe].ToString().Trim());

                                        DataRow RENGLON = Dt_Empleados_Out.NewRow();
                                        RENGLON[Cat_Empleados.Campo_Empleado_ID] = Empleado_ID;
                                        RENGLON[Cat_Empleados.Campo_Nombre] = Nombre;
                                        RENGLON[Cat_Nom_Emp_Perc_Dedu_Deta.Campo_Cantidad] = Cantidad;
                                        RENGLON[Cat_Nom_Emp_Perc_Dedu_Deta.Campo_Importe] = Importe;
                                        RENGLON[Cat_Nom_Emp_Perc_Dedu_Deta.Campo_Saldo] = Importe;
                                        RENGLON[Cat_Nom_Emp_Perc_Dedu_Deta.Campo_Cantidad_Retenida] = 0;
                                        RENGLON["DEDUCCION_ID"] = Deduccion_ID;
                                        RENGLON["NOMBRE_DEDUCCION"] = Nombre_Deduccion;
                                        Dt_Empleados_Out.Rows.Add(RENGLON);
                                    }
                                    else
                                    {
                                        throw new Exception("Hay Empleados en el archivo seleccionado de los cuales su número de empleado no existe en el sistema, por lo tanto el archivo no será cargado!!");
                                    }
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al crear la estructura de la tabla para la carga masiva de informacion. Error: [" + Ex.Message + "]");
            }
            return Dt_Empleados_Out;
        }

        public static String Obtener_Referencia(DataTable Dt_Referencia)
        {
            String Referencia = String.Empty;

            try
            {
                if (Dt_Referencia is DataTable) {
                    if (Dt_Referencia.Rows.Count > 0) {
                        foreach (DataRow REFERENCIA in Dt_Referencia.Rows) {
                            if (REFERENCIA is DataRow) {
                                if (!String.IsNullOrEmpty(REFERENCIA[Ope_Nom_Deducciones_Var.Campo_Referencia].ToString())) {
                                    Referencia = REFERENCIA[Ope_Nom_Deducciones_Var.Campo_Referencia].ToString();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al obtener la referencia. Error: [" + Ex.Message + "]");
            }
            return Referencia;
        }

        /// *****************************************************************************************
        /// Nombre: Consultar_Deduccion_Consecutiva_Corresponde_Empleado
        /// 
        /// Descripción: Consulta el consecitivo de la deduccion que le corresponde al empleado
        ///              de acuerdo al proveedor seleccionado.
        /// 
        /// Parámetros: Empleado_ID .- Identificador del empleado a consultar.
        /// 
        /// Usuario Creó: Juan Alberto Hernández Negrete.
        /// Fecha Creó: 13/Julio/2011
        /// Usuario Modifico:
        /// Fecha Modifico:
        /// *****************************************************************************************
        private static String Consultar_Deduccion_Consecutiva_Corresponde_Empleado(String Empleado_ID, String Proveedor_ID)
        {
            Cls_Cat_Nom_Proveedores_Negocio obj_Proveedores_Deducciones = new Cls_Cat_Nom_Proveedores_Negocio();
            Cls_Ope_Nom_Cap_Masiva_Prov_Fijas_Negocio Obj_Cap_Masiva_Prov_Fijas = new Cls_Ope_Nom_Cap_Masiva_Prov_Fijas_Negocio();
            DataTable Dt_Proveedores_Deducciones = null;
            DataTable Dt_Perc_Dedu_Empl = null;
            String Percepcion_Deduccion_ID = String.Empty;
            String Deduccion_Consecutiva = String.Empty;

            try
            {
                obj_Proveedores_Deducciones.P_Proveedor_ID = Proveedor_ID;
                Dt_Proveedores_Deducciones = obj_Proveedores_Deducciones.Consultar_Deducciones_Proveedor();

                Obj_Cap_Masiva_Prov_Fijas.P_Empleado_ID = Empleado_ID;
                Dt_Perc_Dedu_Empl = Obj_Cap_Masiva_Prov_Fijas.Consultar_Perc_Dedu_Empleado();

                if (Dt_Proveedores_Deducciones is DataTable)
                {
                    if (Dt_Proveedores_Deducciones.Rows.Count > 0)
                    {
                        foreach (DataRow DEDUCCION in Dt_Proveedores_Deducciones.Rows)
                        {
                            if (DEDUCCION is DataRow)
                            {
                                if (!String.IsNullOrEmpty(DEDUCCION[Cat_Nom_Percepcion_Deduccion.Campo_Percepcion_Deduccion_ID].ToString().Trim()))
                                {
                                    Percepcion_Deduccion_ID = DEDUCCION[Cat_Nom_Percepcion_Deduccion.Campo_Percepcion_Deduccion_ID].ToString().Trim();

                                    DataRow[] Deduccion_Buscada = Dt_Perc_Dedu_Empl.Select(Cat_Nom_Percepcion_Deduccion.Campo_Percepcion_Deduccion_ID + "=" + Percepcion_Deduccion_ID);

                                    if (Deduccion_Buscada.Length > 0)
                                    {
                                        Deduccion_Consecutiva = Percepcion_Deduccion_ID;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar la deducción consecutiva que le corresponde al empleado" +
                    " según el proveedor seleccionado. Error: [" + Ex.Message + "]");
            }
            return Deduccion_Consecutiva;
        }
        /// *****************************************************************************************
        /// Nombre: Consultar_Nombre_Deduccion
        /// 
        /// Descripción: Consulta el nombre y la clave de la deducción por el identificador de la
        ///              la misma.
        /// 
        /// Parámetros: Deduccion_ID .- Identificador de la deducción a consultar.
        /// 
        /// Usuario Creó: Juan Alberto Hernández Negrete.
        /// Fecha Creó: 13/Julio/2011
        /// Usuario Modifico:
        /// Fecha Modifico:
        /// *****************************************************************************************
        private static String Consultar_Nombre_Deduccion(String Deduccion_ID)
        {
            Cls_Cat_Nom_Percepciones_Deducciones_Business Obj_Percepcion_Deduccion =
                new Cls_Cat_Nom_Percepciones_Deducciones_Business();//Variable de conexion con la capa de negocios.
            DataTable Dt_Percepcion_Deduccion = null;//Variable que gusrada un listado de las percepciones y deducciones que tiene asiganadas el empleado.
            String Nombre_Deduccion = String.Empty;//Variable que almacenara el identificador del concepto.

            try
            {
                Obj_Percepcion_Deduccion.P_PERCEPCION_DEDUCCION_ID = Deduccion_ID;
                Dt_Percepcion_Deduccion = Obj_Percepcion_Deduccion.Consultar_Percepciones_Deducciones_General();

                if (Dt_Percepcion_Deduccion is DataTable)
                {
                    if (Dt_Percepcion_Deduccion.Rows.Count > 0)
                    {
                        foreach (DataRow DEDUCCION in Dt_Percepcion_Deduccion.Rows)
                        {
                            if (DEDUCCION is DataRow)
                            {
                                Nombre_Deduccion = "[" + DEDUCCION[Cat_Nom_Percepcion_Deduccion.Campo_Clave] + "] -- " +
                                    DEDUCCION[Cat_Nom_Percepcion_Deduccion.Campo_Nombre];
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar el nombre de la deducción. Error: [" + Ex.Message + "]");
            }
            return Nombre_Deduccion;
        }
    }


}