﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JAPAMI.Parametros_Busquedas; 

/// <summary>
/// Summary description for Cls_Parametros_Busquedas
/// </summary>
/// 
namespace JAPAMI.Parametros_Busquedas { 



public class Cls_Parametros_Busquedas
{


    public String colonia { get; set; }
    public String sector { get; set; }
    public String folio { get; set; }
    public String calle { get; set; }
    public String no_cuenta { get; set; }
    public String estado { get; set; }
    public String inspector { get; set; }

}

}