﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace JAPAMI.columnas.reporte_detallado_bonificacion
{

    public class Cls_columnas_reporte_detallado_bonificacion
    {   
        public string movimiento { set; get;}
        public int cantidad { set; get; }
        public double ajuste { set; get; }
        public double bonificacion { set; get; }
    }


}

