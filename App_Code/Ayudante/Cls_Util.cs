﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using System.Collections.Generic;
using JAPAMI.Sessiones;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
/// <summary>
/// Summary description for Cls_Util
/// </summary>
/// 
    public class Cls_Util
    {
        public Cls_Util() { }

        #region HISTORIAL ORDEN COMPRA

        public static void Registrar_Historial_Orden_Compra(String No_Orden_Compra, String Estatus, String Empleado, String Proveedor)
        {
            try
            {
                //INSERT INTO OPE_COM_HISTORIAL_ORD_COMPRA (NO_REQUISICION, ESTATUS, EMPLEADO) VALUES (1,'B','C')
                String Mi_SQL = "INSERT INTO OPE_COM_HISTORIAL_ORD_COMPRA (NO_ORDEN_COMPRA, FECHA, ESTATUS, EMPLEADO, PROVEEDOR) VALUES (" +
                    No_Orden_Compra + ", GETDATE(),'" + Estatus + "','" + Empleado + "','" + Proveedor + "')";
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            catch (Exception Ex)
            {
                throw new Exception("No se guardo historial de Orden de Compra [" + Ex.ToString() + "]");
            }
        }

        public static void Registrar_Historial_Orden_Compra(String No_Orden_Compra, String Estatus, String Empleado)
        {
            try
            {
                String Mi_SQL = "INSERT INTO OPE_COM_HISTORIAL_ORD_COMPRA (NO_ORDEN_COMPRA, FECHA, ESTATUS, EMPLEADO, PROVEEDOR) VALUES (" +
                    No_Orden_Compra + ", GETDATE(),'" + Estatus + "','" + Empleado + "','" + "" + "')";
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            catch (Exception Ex)
            {
                throw new Exception("No se guardo historial de Orden de Compra [" + Ex.ToString() + "]");
            }
        }

        public static DataTable Consultar_Historial_Orden_Compra(String No_Orden_Compra)
        {
            try
            {
                String Mi_SQL = "SELECT HISTORIAL.*, RQ.DEPENDENCIA_ID FROM OPE_COM_HISTORIAL_ORD_COMPRA HISTORIAL " +
                    " JOIN " +
                    Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " RQ ON " +
                    " HISTORIAL.NO_ORDEN_COMPRA = RQ." + Ope_Com_Requisiciones.Campo_No_Orden_Compra +
                    " WHERE HISTORIAL.NO_ORDEN_COMPRA = " + No_Orden_Compra +
                    " ORDER BY HISTORIAL.FECHA ASC";
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Consultar Historial OC [" + Ex.ToString() + "]");
                return null;
            }
        }

        #endregion

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Cuenta_Contable_RQ
        ///DESCRIPCIÓN: Devuelve un DataTable con la informacin para dar de alta una poliza
        ///PARAMETROS:
        ///CREO:        Susana Trigueros
        ///FECHA_CREO: 24/Noviembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Cuenta_Contable_RQ(String No_Requisicion)
        {
            String Mi_SQL = "";
            Boolean Entro_Where = false;

            Mi_SQL = "SELECT * FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
            Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + No_Requisicion;
            DataTable Dt_Requisicion = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            if (Dt_Requisicion.Rows.Count > 0)
            {
                String Depedencia_ID = Dt_Requisicion.Rows[0][Ope_Com_Requisiciones.Campo_Dependencia_ID].ToString().Trim();
                String Partida_ID = Dt_Requisicion.Rows[0][Ope_Com_Requisiciones.Campo_Partida_ID].ToString().Trim();

                Mi_SQL = "SELECT PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID;
                Mi_SQL += ", FTE_FIN." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS CLAVE_FTE_FINANCIAMIENTO";
                Mi_SQL += ", FTE_FIN." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS FTE_FINANCIAMIENTO";
                Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID;
                Mi_SQL += ", AREA_FUN." + Cat_SAP_Area_Funcional.Campo_Clave + " AS CLAVE_AREA_FUNCIONAL";
                Mi_SQL += ", AREA_FUN." + Cat_SAP_Area_Funcional.Campo_Descripcion + " AS AREA_FUNCIONAL";
                Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID;
                Mi_SQL += ", PROY_PROG." + Cat_Sap_Proyectos_Programas.Campo_Clave + " AS CLAVE_PROYECTO_PROGRAMA";
                Mi_SQL += ", PROY_PROG." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS PROYECTO_PROGRAMA";
                Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID;
                Mi_SQL += ", DEP." + Cat_Dependencias.Campo_Clave + " AS CLAVE_DEPENDENCIA";
                Mi_SQL += ", DEP." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID;
                Mi_SQL += ", PARTIDAS." + Cat_Sap_Partidas_Especificas.Campo_Clave + " AS CLAVE_PARTIDA";
                Mi_SQL += ", PARTIDAS." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS PARTIDA";
                Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID;
                Mi_SQL += ", CUENTAS." + Cat_Con_Cuentas_Contables.Campo_Descripcion + " AS CUENTA";
                Mi_SQL += " FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " PRESUPUESTOS";

                Mi_SQL += " LEFT OUTER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " FTE_FIN";
                Mi_SQL += " ON FTE_FIN." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID;

                Mi_SQL += " LEFT OUTER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + " AREA_FUN";
                Mi_SQL += " ON AREA_FUN." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID;

                Mi_SQL += " LEFT OUTER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " PROY_PROG";
                Mi_SQL += " ON PROY_PROG." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID;

                Mi_SQL += " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEP";
                Mi_SQL += " ON DEP." + Cat_Dependencias.Campo_Dependencia_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID;

                Mi_SQL += " LEFT OUTER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PARTIDAS";
                Mi_SQL += " ON PARTIDAS." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID;

                Mi_SQL += " LEFT OUTER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " CUENTAS";
                Mi_SQL += " ON CUENTAS." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID;

                if (!String.IsNullOrEmpty(Depedencia_ID))
                {
                    Mi_SQL += Entro_Where ? " AND PRESUPUESTOS." : " WHERE PRESUPUESTOS."; Entro_Where = true;
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Depedencia_ID + "'";
                }
                if (!String.IsNullOrEmpty(Partida_ID))
                {
                    Mi_SQL += Entro_Where ? " AND PRESUPUESTOS." : " WHERE PRESUPUESTOS."; Entro_Where = true;
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Partida_ID + "'";
                }

                Mi_SQL += Entro_Where ? " AND PRESUPUESTOS." : " WHERE PRESUPUESTOS."; Entro_Where = true;
                Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = YEAR(GETDATE())";

                Dt_Requisicion = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Dt_Requisicion;
            }
            else
            {
                return new DataTable();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_UR_De_Empleado
        ///DESCRIPCIÓN: Busca a que grupo de Roles pertenece un Rol_ID
        ///Devuelve un DataTable con Rol_ID,Grupo_Rol_ID, Nombre
        ///PARAMETROS: 1.-Rol_ID
        ///CREO:        Susana Trigueros
        ///FECHA_CREO: 24/Noviembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Codigo_Programatico_RQ(String No_Requisicion)
        {
            String Mi_SQL = "";

            Mi_SQL = "SELECT REQ_DET." + Ope_Com_Req_Producto.Campo_Partida_ID +
                             ",REQ_DET." + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID +
                             ", (SELECT " + Ope_Com_Requisiciones.Campo_Total +
                             " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                             " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                             "= REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + ") AS TOTAL" +
                             ", (SELECT " + Ope_Com_Requisiciones.Campo_Total_Cotizado +
                             " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                             " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                             "= REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + ") AS TOTAL_COTIZADO" +
                             ", (SELECT " + Ope_Com_Requisiciones.Campo_Dependencia_ID +
                             " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                             " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                             "= REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + ") AS DEPENDENCIA_ID" +
                             ", REQ_DET." + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID +
                             ", (SELECT NUM_RESERVA" +
                             " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                             " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                             "= REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + ") AS NUM_RESERVA" +
                             " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_DET" +
                             " WHERE REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + "='" + No_Requisicion.Trim() + "'";
            DataTable Dt_Requisicion = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Requisicion;

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_UR_De_Empleado
        ///DESCRIPCIÓN: Busca a que grupo de Roles pertenece un Rol_ID
        ///Devuelve un DataTable con Rol_ID,Grupo_Rol_ID, Nombre
        ///PARAMETROS: 1.-Rol_ID
        ///CREO:        Susana Trigueros
        ///FECHA_CREO: 24/Noviembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************

        public static DataTable Consultar_URs_De_Empleado(String Empleado_ID)
        {
            String Mi_SQL = "";
            Mi_SQL = "SELECT " + Cat_Dependencias.Campo_Dependencia_ID + ", " +
            Cat_Dependencias.Campo_Clave + "+' '+" + Cat_Dependencias.Campo_Nombre + " AS CLAVE_NOMBRE" +
            " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias +
            " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " ='" + Cls_Sessiones.Dependencia_ID_Empleado + "'" +
            " UNION ALL " +
            "SELECT " + Cat_Dependencias.Campo_Dependencia_ID + ", " +
            Cat_Dependencias.Campo_Clave + "+' '+" + Cat_Dependencias.Campo_Nombre + " AS CLAVE_NOMBRE" +
            " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias +
            " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " IN (SELECT " +
            Cat_Det_Empleado_UR.Campo_Dependencia_ID + " FROM " + Cat_Det_Empleado_UR.Tabla_Cat_Det_Empleado_UR +
            " WHERE " + Cat_Det_Empleado_UR.Campo_Empleado_ID + " ='" + Empleado_ID + "')";
            DataSet _DataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            DataTable Tabla = null;
            if (_DataSet.Tables.Count != 0)
            {
                Tabla = _DataSet.Tables[0];
            }
            return Tabla;
        }

        ///****************************************************************************************
        //    NOMBRE DE LA FUNCION: Subir_Archivo
        //    DESCRIPCION         : Subir los archivos al directorio de Archivos
        //    PARAMETROS          : 1.-  Tipo - Si es Subir quiere decir que el archivo ya se va a guardar definitivamente
        //                                      Si es Temporal quiere decir que el archivo solo está temporalmente guardado
        //                          2.- Nombre_Archivo - Es el nombre del archivo que le designa el usuario
        //    CREO                : Josué Daniel Sámano García
        //    FECHA_CREO          : 02/Septiembre/2011
        //    MODIFICO            :
        //    FECHA_MODIFICO      :
        //    CAUSA_MODIFICACION  :
        //****************************************************************************************/
        public static String Crear_Ruta(String Tipo)
        {
            DirectoryInfo Ruta;
            if (Tipo == "Subir")
            {
                Ruta = new DirectoryInfo(HttpContext.Current.Server.MapPath("~/Archivos/" + System.DateTime.Now.ToString("yyyy") + "/" + System.DateTime.Now.ToString("MM").ToUpper() + "/"));
                if (!Ruta.Exists)
                    Ruta.Create();
                return Ruta.ToString();
            }
            else if (Tipo == "Temporal")
            {
                Ruta = new DirectoryInfo(HttpContext.Current.Server.MapPath("~/Temporal/"));
                if (!Ruta.Exists)
                    Ruta.Create();
                return Ruta.ToString();
            }
            return "";
        }


        ///****************************************************************************************
        //    NOMBRE DE LA FUNCION: Obtener_IVA
        //    DESCRIPCION         : Consulta el iva de la tabal parametros
        //    PARAMETROS          : 
        //    CREO                : Fernando Gonzalez B
        //    FECHA_CREO          : 06/Junio/2012
        //    MODIFICO            :
        //    FECHA_MODIFICO      :
        //    CAUSA_MODIFICACION  :
        //****************************************************************************************/
        public static Double Obtener_IVA()
        {
            String Mi_SQL="";
            DataTable Dt_Parametros;

            Mi_SQL="SELECT * FROM Cat_Cor_Parametros";
            Dt_Parametros = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Double.Parse(Dt_Parametros.Rows[0]["Porcentaje_IVA"].ToString());
        }

        public static String Consecutivo_Catalogo(String Campo_ID, String Tabla)
        {
            String Consecutivo = "",Mi_SQL; //Obtiene la cadena de inserción hacía la base de datos
            Object ID; //Obtiene el ID con la cual se guardo los datos en la base de datos


            Mi_SQL = "SELECT ISNULL(MAX (" + Campo_ID + "),'00000') " +
                     "FROM " + Tabla;

            ID = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

            if (Convert.IsDBNull(ID))
                Consecutivo = "00001";
            else
                Consecutivo = String.Format("{0:00000}", Convert.ToInt32(ID) + 1);

            return Consecutivo;
        }

        public static String Consecutivo_Operacion(String Campo_ID, String Tabla)
        {
            String Consecutivo = "", Mi_SQL; //Obtiene la cadena de inserción hacía la base de datos
            Object ID; //Obtiene el ID con la cual se guardo los datos en la base de datos

            Mi_SQL = "SELECT ISNULL(MAX (" + Campo_ID + "),'0000000000') " +
                     "FROM " + Tabla;

            ID = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

            if (Convert.IsDBNull(ID))
                Consecutivo = "0000000001";
            else
                Consecutivo = String.Format("{0:0000000000}", Convert.ToInt32(ID) + 1);

            return Consecutivo;
        }

        public static String Consecutivo(String Formato, String Campo_ID, String Tabla)
        {
            String Consecutivo = "", Mi_SQL; //Obtiene la cadena de inserción hacía la base de datos
            Object ID; //Obtiene el ID con la cual se guardo los datos en la base de datos

            Mi_SQL = "SELECT ISNULL(MAX (" + Campo_ID + "),'" + Formato + "') " +
                     "FROM " + Tabla;

            ID = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

            if (Convert.IsDBNull(ID))
                Consecutivo = String.Format("{0:" + Formato + "}", Convert.ToInt32(Formato) + 1);
            else
                Consecutivo = String.Format("{0:" + Formato + "}", Convert.ToInt32(ID) + 1);

            return Consecutivo;
        }


        ///*******************************************************************************
        ///NOMBRE DE LA METODO: Do_LLenar_Combo_Id
        ///        DESCRIPCIÓN: llena todos los combos
        ///         PARAMETROS: 1.- Obj_DropDownList, ID del control
        ///                     2.- Dt_Temporal, Datatable con los valores. 
        ///               CREO: Alberto Pantoja Hernández
        ///         FECHA_CREO: 24/8/2010
        ///           MODIFICO:
        ///     FECHA_MODIFICO:
        /// CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Do_Llenar_Combo_ID(DropDownList Obj_DropDownList, DataTable Dt_Temporal)
        {
            int Int_Contador;


            for (Int_Contador = 0; Int_Contador < Dt_Temporal.Rows.Count; Int_Contador++)
            {
                Obj_DropDownList.Items.Add(Convert.ToString(Dt_Temporal.Rows[Int_Contador][1]));
                Obj_DropDownList.Items[Int_Contador].Value = Convert.ToString(Dt_Temporal.Rows[Int_Contador][1]);
            }
            Obj_DropDownList.Items.Add("<<Seleccionar>>");
            Obj_DropDownList.Items[Obj_DropDownList.Items.Count - 1].Value = "0";
            Obj_DropDownList.Items[Obj_DropDownList.Items.Count - 1].Selected = true;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_BD
        ///DESCRIPCIÓN: Llena un combo con los registros de un campo de una tabla de la base de datos
        ///PARAMETROS:   combo: combo que se va a llenar, tabla:tabla a la cual pertenece el campo 
        ///que se requiere, campo: nombre del campo de la tabla al cual se le sacaran los 
        ///registros para ser colocados en el combo
        ///CREO: Silvia Morales Portuhondo
        ///FECHA_CREO: 30-Jlio-2010
        ///MODIFICO             : Julio Cruz
        ///FECHA_MODIFICO       : 05-31-2012
        ///CAUSA_MODIFICACIÓN   : se estaba generando un error en la pagina al llenar el combo con el siguiente caracter menor que
        ///*******************************************************************************
        public static void Llenar_Combo_BD(DropDownList combo, string tabla, string campo)
        {
            try
            {

                String sql = "SELECT " + campo + " FROM " + tabla;
                SqlConnection Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Conexion.Open();
                SqlCommand Comando = new SqlCommand();
                Comando.Connection = Conexion;
                Comando.CommandType = CommandType.Text;
                Comando.CommandText = sql;
                SqlDataReader Data_Reader = Comando.ExecuteReader();
                while (Data_Reader.Read())
                {
                    if (Data_Reader.FieldCount == 1)
                    {
                        combo.Items.Add(new ListItem(Data_Reader.GetSqlString(0).ToString()));
                    }
                    else
                    {
                        combo.Items.Add(new ListItem(Data_Reader.GetSqlString(1).ToString(), Data_Reader.GetSqlString(0).ToString()));
                    }
                }
                combo.Items.Insert(0, new ListItem("<<Seleccionar>>", "-1"));
                Conexion.Close();

            }
            catch (Exception Ex)
            {
                throw new Exception("Advertencia: " + Ex.Message);
            }

        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: To_DateOracle
        ///DESCRIPCIÓN: Combierte la fecha que se obtiene del sistema a fecha con formato de tipo de dato TIMESTAMP 
        ///             en oracle. 
        ///PARAMETROS:  
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 03-Septiembre-2010
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static String To_DateOracle(String Fecha_Sistema)
        {
            //Aplicamos un Split a la Fecha del sistema para obtener la solamente la fecha MM/DD/YY
            char caracter = ' ';
            String[] fecha_hora = Fecha_Sistema.Split(caracter);
            caracter = '/';
            //Aplicamos un split para obtener por separado los meses y dias y posteriormente realizar el cambio
            String[] only_date = fecha_hora[0].Split(caracter);

            //Cambiamos los meses de formato MM a formato MMM

            switch (only_date[0])
            {
                case "01":
                    only_date[0] = "ENE";
                    break;
                case "02":
                    only_date[0] = "FEB";
                    break;
                case "03":
                    only_date[0] = "MAR";
                    break;
                case "04":
                    only_date[0] = "ABR";
                    break;
                case "05":
                    only_date[0] = "MAY";
                    break;
                case "06":
                    only_date[0] = "JUN";
                    break;
                case "07":
                    only_date[0] = "JUL";
                    break;
                case "08":
                    only_date[0] = "AGO";
                    break;
                case "09":
                    only_date[0] = "SEP";
                    break;
                case "10":
                    only_date[0] = "OCT";
                    break;
                case "11":
                    only_date[0] = "NOV";
                    break;
                case "12":
                    only_date[0] = "DEC";
                    break;
            }//fin de switch
            //Cambiamos el formato de p.m y a.m

            String[] Hora = fecha_hora[2].Split('.');
            //Obtenemos el año con formato YY y no YYYY
            char[] anio = only_date[2].ToCharArray();
            only_date[2] = anio[2].ToString() + anio[3].ToString();
            // Pasamos la fecha del sistema al formato DD/MMM/YY 00:00:00 AM
            String Fecha_Valida = only_date[1] + "/" + only_date[0] + "/" + only_date[2] + " " + fecha_hora[1] + " " + Hora[0] + Hora[1];

            return Fecha_Valida;
        }//fin de To_DateOracle

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Registrar_Bitacora
        ///DESCRIPCIÓN: Inserta un registro en la tabla de de Apl_Bitacora para 
        ///             llevar los registros de las acciones dentro del sistema
        ///PARAMETROS:  1.- String Usuario: Es el usuario que en ese momento esta logeado en el Sistema
        ///             2.- String Accion: Es la accion que esta realizando el usuario en el sistema, 
        ///             solo puede aceptar las siguientes acciones: (Alta,Modificar,Eliminar,Consultar,Imprimir,Acceso, Reporte, Estadistica)
        ///             3.- String Recurso: Es el nombre del recurso al que se le realiza la accion (Titulo del Catalogo o Formulario de Operacion)
        ///             4.- String Nombre_Recurso: Es el nombre del recurso al que se le aplica la accion, Este solo en caso de Existir 
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 03-Septiembre-2010
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static void Registrar_Bitacora(String Usuario, String Accion, String Recurso, String Nombre_Recurso, String Descripcion)
        {
            // PRIMERO SE GENERA UN ID PARA EL NUEVO REGISTRO A CREAR
            String Bitacora_ID = Consecutivo_Catalogo(Apl_Bitacora.Campo_Bitacora_ID, Apl_Bitacora.Tabla_Apl_Bitacora);
            //REALIZAMOS LA SENTENCIA PARA INSERTAR EN LA TABLA APL_BITACORA 
            String Sentencia = "INSERT INTO " + Apl_Bitacora.Tabla_Apl_Bitacora + " (" + Apl_Bitacora.Campo_Bitacora_ID + ", " +
                Apl_Bitacora.Campo_Empleado_ID + ", " + Apl_Bitacora.Campo_Fecha_Hora + ", " + Apl_Bitacora.Campo_Accion +
                ", " + Apl_Bitacora.Campo_Recurso + ", " + Apl_Bitacora.Campo_Recurso_ID + ", " + Apl_Bitacora.Campo_Descripcion + ") VALUES " +
                "('" + Bitacora_ID + "','" + Usuario + "'," + String.Format("{0:dd/MM/yyyy}", DateTime.Now) + ",'" + Accion + "','" + Recurso + "','" + Nombre_Recurso + "','" + Descripcion + "')";
            //LLAMAMOS EL METODO ExecuteNonQuery PARA EJECUTAR LA SENTENCIA DE ORACLE
            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Sentencia);
        }//Fin de Registrar_Bitacora



        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Con_DataTable
        ///DESCRIPCIÓN: Llena un combo con un objeto DataTable
        ///verificar las diferencias de cada uno de sus campos
        ///PARAMETROS: 1.-DropDownList Obj_Combo - combo a llenar
        ///            2.-DataTable Dt_Temporal - Datos para llenar combo
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 1/Octubre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Llenar_Combo_Con_DataTable(DropDownList Obj_Combo, DataTable Dt_Temporal)
        {
            Obj_Combo.SelectedIndex = -1;
            Obj_Combo.DataSource = Dt_Temporal;
            Obj_Combo.DataTextField = Dt_Temporal.Columns[1].ToString();
            Obj_Combo.DataValueField = Dt_Temporal.Columns[0].ToString();
            Obj_Combo.DataBind();
            Obj_Combo.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("<SELECCIONAR>"), "0"));
            
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Con_DataTable
        ///DESCRIPCIÓN: Llena un combo con un objeto DataTable
        ///verificar las diferencias de cada uno de sus campos
        ///PARAMETROS: 1.-DropDownList Obj_Combo - combo a llenar
        ///            2.-DataTable Dt_Temporal - Datos para llenar combo
        ///            3.-Dysplay.- El numero de columna del DataTable que se desea mostrar
        ///            4.- Value .- El numero de columna del DataTable que se desea obtener como valor
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 1/Octubre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Llenar_Combo_Con_DataTable_Generico(DropDownList Obj_Combo, DataTable Dt_Temporal, int Display, int Value)
        {
            Obj_Combo.SelectedIndex = -1;
            Obj_Combo.DataSource = Dt_Temporal;
            Obj_Combo.DataTextField = Dt_Temporal.Columns[Display].ToString();
            Obj_Combo.DataValueField = Dt_Temporal.Columns[Value].ToString();
            Obj_Combo.DataBind();
            Obj_Combo.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("<SELECCIONAR>"), "0"));
            
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Con_DataTable
        ///DESCRIPCIÓN: Llena un combo con un objeto DataTable
        ///verificar las diferencias de cada uno de sus campos
        ///PARAMETROS: 1.-DropDownList Obj_Combo - combo a llenar
        ///            2.-DataTable Dt_Temporal - Datos para llenar combo
        ///            3.-Dysplay.- El nombre de columna del DataTable que se desea mostrar
        ///            4.- Value .- El nombre de columna del DataTable que se desea obtener como valor
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 1/Octubre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Llenar_Combo_Con_DataTable_Generico(DropDownList Obj_Combo, DataTable Dt_Temporal, String Display, String Value)
        {
            Obj_Combo.DataSource = Dt_Temporal;
            if (Dt_Temporal != null)
            {
                Obj_Combo.DataTextField = Dt_Temporal.Columns[Display].ToString();
                Obj_Combo.DataValueField = Dt_Temporal.Columns[Value].ToString();
                Obj_Combo.DataBind();
            }
            Obj_Combo.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("<SELECCIONAR>"), "0"));
            Obj_Combo.SelectedIndex = 0;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Con_DataTable_Generico
        ///DESCRIPCIÓN:          Llena un combo con un objeto DataTable
        ///                      verificar las diferencias de cada uno de sus campos
        ///PARAMETROS:           1.- DropDownList Obj_Combo - combo a llenar
        ///                      2.- String Tabla - El nombre de la columna para realizar la busqueda a esta tabla
        ///                      3.- Valor.- El nombre de columna del DataTable que se desea obtener como valor
        ///                      4.- Texto .- El nombre de columna del DataTable que se desea mostrar
        ///CREO:                 Josué Daniel Sámano García
        ///FECHA_CREO:           30/Agosto/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Llenar_Combo_Con_DataTable_Generico(DropDownList Combo, String Tabla, String Valor, String Texto)
        {
            DataTable Dt_Temporal = Consultar_Tabla_Generica(Tabla, Valor, Texto);

            Combo.DataSource = Dt_Temporal;
            Combo.Items.Clear();
            if (!Dt_Temporal.Equals(null))
            {
                if (Dt_Temporal.Rows.Count > 0)
                {
                    Combo.DataValueField = Dt_Temporal.Columns[0].ToString();
                    Combo.DataTextField = Dt_Temporal.Columns[1].ToString();
                    Combo.DataBind();
                }
            }
            Combo.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("<SELECCIONAR>"), "0"));
            Combo.SelectedIndex = 0;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Con_DataTable_Generico
        ///DESCRIPCIÓN:          Llena un combo con un objeto DataTable
        ///                      verificar las diferencias de cada uno de sus campos
        ///PARAMETROS:           1.- DropDownList Obj_Combo - combo a llenar
        ///                      2.- String Tabla - El nombre de la columna para realizar la busqueda a esta tabla
        ///                      3.- Valor.- El nombre de columna del DataTable que se desea obtener como valor
        ///                      4.- Texto .- El nombre de columna del DataTable que se desea mostrar
        ///                      5.- Condicion .- La condicion para la clausula WHERE
        ///CREO:                 Raúl Raya Ortega
        ///FECHA_CREO:           07/Septiembre/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Llenar_Combo_Con_DataTable_Generico(DropDownList Combo, String Tabla, String Valor, String Texto, String Condicion)
        {
            DataTable Dt_Temporal = Consultar_Tabla_Generica(Tabla, Valor, Texto, Condicion);
            Combo.Items.Clear();
            Combo.DataSource = Dt_Temporal;
            if (!Dt_Temporal.Equals(null))
            {
                if (Dt_Temporal.Rows.Count > 0)
                {
                    Combo.DataValueField = Dt_Temporal.Columns[0].ToString();
                    Combo.DataTextField = Dt_Temporal.Columns[1].ToString();
                    Combo.DataBind();
                }
            }
            Combo.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("<SELECCIONAR>"), "0"));
            Combo.SelectedIndex = 0;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Con_DataTable_Generico
        ///DESCRIPCIÓN:          Llena un combo con un objeto DataTable
        ///                      verificar las diferencias de cada uno de sus campos
        ///PARAMETROS:           1.- DropDownList Obj_Combo - combo a llenar
        ///                      2.- String Tabla - El nombre de la columna para realizar la busqueda a esta tabla
        ///                      3.- Valor.- El nombre de columna del DataTable que se desea obtener como valor
        ///                      4.- Texto .- El nombre de columna del DataTable que se desea mostrar
        ///                      5.- Condicion .- La condicion para la clausula WHERE
        ///CREO:                 Raúl Raya Ortega
        ///FECHA_CREO:           07/Septiembre/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Llenar_Combo_Con_DataTable_Generico(DropDownList Combo, String Tabla, String Valor, String Texto, Boolean Distinct)
        {
            DataTable Dt_Temporal = Consultar_Tabla_Generica(Tabla, Valor, Texto,Distinct);

            Combo.DataSource = Dt_Temporal;
            if (!Dt_Temporal.Equals(null))
            {
                Combo.DataValueField = Dt_Temporal.Columns[0].ToString();
                Combo.DataTextField = Dt_Temporal.Columns[1].ToString();
                Combo.DataBind();
            }
            Combo.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("<SELECCIONAR>"), "0"));
            Combo.SelectedIndex = 0;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Tabla_Generica
        ///DESCRIPCIÓN:          Regresa una consulta con el ID y un campo para que lo identifique como un nombre
        ///PARAMETROS:           1.- String Tabla - El nombre de la columna para realizar la busqueda a esta tabla
        ///                      2.- Valor.- El nombre de columna del DataTable que se desea obtener como valor
        ///                      3.- Texto .- El nombre de columna del DataTable que se desea mostrar
        ///CREO:                 Josué Daniel Sámano García
        ///FECHA_CREO:           30/Agosto/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Tabla_Generica(String Tabla, String Valor, String Texto) 
        {
            DataTable Dt_Temporal;
            String Mi_SQL;

            Mi_SQL = "SELECT " + Valor + "," + Texto + " " +
                     "FROM " + Tabla;

            Dt_Temporal = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            return Dt_Temporal;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Tabla_Generica
        ///DESCRIPCIÓN:          Regresa una consulta con el ID y un campo para que lo identifique como un nombre
        ///PARAMETROS:           1.- String Tabla - El nombre de la columna para realizar la busqueda a esta tabla
        ///                      2.- Valor.- El nombre de columna del DataTable que se desea obtener como valor
        ///                      3.- Texto .- El nombre de columna del DataTable que se desea mostrar
        ///                      4.- Condicion .- la condicion para el parametro WHERE
        ///CREO:                 Raúl Raya Ortega
        ///FECHA_CREO:           07/Septiembre/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Tabla_Generica(String Tabla, String Valor, String Texto, String Condicion)
        {
            DataTable Dt_Temporal;
            String Mi_SQL;

            Mi_SQL = "SELECT " + Valor + "," + Texto + " " +
                     "FROM " + Tabla+" "+
                     "WHERE " + Condicion;

            Dt_Temporal = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            return Dt_Temporal;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Tabla_Generica
        ///DESCRIPCIÓN:          Regresa una consulta con el ID y un campo para que lo identifique como un nombre
        ///PARAMETROS:           1.- String Tabla - El nombre de la columna para realizar la busqueda a esta tabla
        ///                      2.- Valor.- El nombre de columna del DataTable que se desea obtener como valor
        ///                      3.- Texto .- El nombre de columna del DataTable que se desea mostrar
        ///                      4.- Condicion .- la condicion para el parametro WHERE
        ///CREO:                 Raúl Raya Ortega
        ///FECHA_CREO:           07/Septiembre/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Tabla_Generica(String Tabla, String Valor, String Texto,Boolean Distinct)
        {
            DataTable Dt_Temporal;
            String Mi_SQL;
            if (Distinct)
            {
                Mi_SQL = "SELECT DISTINCT " + Valor + "," + Texto + " " +
                     "FROM " + Tabla + " ";
                     
            }
            else
            {
                Mi_SQL = "SELECT " + Valor + "," + Texto + " " +
                     "FROM " + Tabla + " ";
            }
            

            Dt_Temporal = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            return Dt_Temporal;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Grupo_Rol_ID
        ///DESCRIPCIÓN: Busca a que grupo de Roles pertenece un Rol_ID
        ///Devuelve un DataTable con Rol_ID,Grupo_Rol_ID, Nombre
        ///PARAMETROS: 1.-Rol_ID
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 24/Noviembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************

        public static DataTable Consultar_Grupo_Rol_ID(String Rol_ID)
        {
            String Mi_SQL = "SELECT " +
                Apl_Cat_Roles.Campo_Rol_ID + ", " +
                Apl_Cat_Roles.Campo_Grupo_Roles_ID + ", " +
                Apl_Cat_Roles.Campo_Nombre +
                " FROM " + Apl_Cat_Roles.Tabla_Apl_Cat_Roles +
                " WHERE " + Apl_Cat_Roles.Campo_Rol_ID + " ='" + Rol_ID + "'";
            DataSet _DataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            DataTable Tabla = null;
            if (_DataSet.Tables.Count != 0)
            {
                Tabla = _DataSet.Tables[0];
            }
            return Tabla;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Conceptos_De_Partidas
        ///DESCRIPCIÓN: Busca a que grupo de Roles pertenece un Rol_ID
        ///Devuelve un DataTable con Rol_ID,Grupo_Rol_ID, Nombre
        ///PARAMETROS: 1.-Rol_ID
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 19/Abril/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Conceptos_De_Partidas(String Lista_Partidas)
        {
            //SELECT DISTINCT (CAT_SAP_CONCEPTO.CONCEPTO_ID),CAT_SAP_CONCEPTO.CLAVE,CAT_SAP_CONCEPTO.DESCRIPCION AS NOMBRE, CAT_SAP_CONCEPTO.CLAVE 
            //+' '+CAT_SAP_CONCEPTO.DESCRIPCION AS CLAVE_NOMBRE FROM 
            //CAT_SAP_PARTIDAS_ESPECIFICAS JOIN CAT_SAP_PARTIDA_GENERICA
            //ON CAT_SAP_PARTIDAS_ESPECIFICAS.PARTIDA_GENERICA_ID = CAT_SAP_PARTIDA_GENERICA.PARTIDA_GENERICA_ID
            //JOIN CAT_SAP_CONCEPTO ON CAT_SAP_PARTIDA_GENERICA.CONCEPTO_ID = CAT_SAP_CONCEPTO.CONCEPTO_ID

            String Mi_Sql = "";
            Mi_Sql = "SELECT DISTINCT(" +
                Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID + "), " +
                Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave + ", " +
                Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Descripcion + ", " +
                Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Clave + " +' '+ " +
                Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Descripcion + " AS CLAVE_NOMBRE" +
                " FROM " +
                Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas +
                " JOIN " +
                Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas +
                " ON " +
                Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." +
                Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = " +
                Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." +
                Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID +
                " JOIN " +
                Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto +
                " ON " +
                Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." +
                Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + " = " +
                Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID +
                " WHERE " +
                Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." +
                Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " IN (" + Lista_Partidas + ")";

            DataSet _DataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            DataTable _DataTable = null;
            if (_DataSet != null && _DataSet.Tables.Count > 0 && _DataSet.Tables[0].Rows.Count > 0)
            {
                _DataTable = _DataSet.Tables[0];
            }
            return _DataTable;

        }
        public static void Configuracion_Acceso_Sistema_SIAS(List<ImageButton> Botones, DataRow Accesos)
        {
            String Operacion = String.Empty;

            try
            {
                foreach (ImageButton Btn_Aux in Botones)
                {
                    if (Btn_Aux is ImageButton)
                    {
                        switch (Btn_Aux.ToolTip.Trim().ToUpper())
                        {
                            case "NUEVO":
                                if (!String.IsNullOrEmpty(Accesos[Apl_Cat_Accesos.Campo_Alta].ToString()))
                                    Btn_Aux.Visible = (Accesos[Apl_Cat_Accesos.Campo_Alta].ToString().Equals("S")) ? true : false;
                                break;
                            case "MODIFICAR":
                                if (!String.IsNullOrEmpty(Accesos[Apl_Cat_Accesos.Campo_Cambio].ToString()))
                                    Btn_Aux.Visible = (Accesos[Apl_Cat_Accesos.Campo_Cambio].ToString().Equals("S")) ? true : false;
                                break;
                            case "ELIMINAR":
                                if (!String.IsNullOrEmpty(Accesos[Apl_Cat_Accesos.Campo_Eliminar].ToString()))
                                    Btn_Aux.Visible = (Accesos[Apl_Cat_Accesos.Campo_Eliminar].ToString().Equals("S")) ? true : false;
                                break;
                            case "CONSULTAR":
                                if (!String.IsNullOrEmpty(Accesos[Apl_Cat_Accesos.Campo_Consultar].ToString()))
                                    Btn_Aux.Visible = (Accesos[Apl_Cat_Accesos.Campo_Consultar].ToString().Equals("S")) ? true : false;
                                break;
                            case "AVANZADA":
                                if (!String.IsNullOrEmpty(Accesos[Apl_Cat_Accesos.Campo_Consultar].ToString()))
                                    Btn_Aux.Visible = (Accesos[Apl_Cat_Accesos.Campo_Consultar].ToString().Equals("S")) ? true : false;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al configurar el control de acceso a nivel de operacion de las páginas del sistema. Error: [" + Ex.Message + "]");
            }
        }

        public static void Configuracion_Acceso_Sistema_SIAS(List<LinkButton> Botones, DataRow Accesos)
        {
            String Operacion = String.Empty;

            try
            {
                foreach (LinkButton Btn_Aux in Botones)
                {
                    if (Btn_Aux is LinkButton)
                    {
                        switch (Btn_Aux.ToolTip.Trim().ToUpper())
                        {
                            case "NUEVO":
                                if (!String.IsNullOrEmpty(Accesos[Apl_Cat_Accesos.Campo_Alta].ToString()))
                                    Btn_Aux.Visible = (Accesos[Apl_Cat_Accesos.Campo_Alta].ToString().Equals("S")) ? true : false;
                                break;
                            case "MODIFICAR":
                                if (!String.IsNullOrEmpty(Accesos[Apl_Cat_Accesos.Campo_Cambio].ToString()))
                                    Btn_Aux.Visible = (Accesos[Apl_Cat_Accesos.Campo_Cambio].ToString().Equals("S")) ? true : false;
                                break;
                            case "ELIMINAR":
                                if (!String.IsNullOrEmpty(Accesos[Apl_Cat_Accesos.Campo_Eliminar].ToString()))
                                    Btn_Aux.Visible = (Accesos[Apl_Cat_Accesos.Campo_Eliminar].ToString().Equals("S")) ? true : false;
                                break;
                            case "CONSULTAR":
                                if (!String.IsNullOrEmpty(Accesos[Apl_Cat_Accesos.Campo_Consultar].ToString()))
                                    Btn_Aux.Visible = (Accesos[Apl_Cat_Accesos.Campo_Consultar].ToString().Equals("S")) ? true : false;
                                break;
                            case "AVANZADA":
                                if (!String.IsNullOrEmpty(Accesos[Apl_Cat_Accesos.Campo_Consultar].ToString()))
                                    Btn_Aux.Visible = (Accesos[Apl_Cat_Accesos.Campo_Consultar].ToString().Equals("S")) ? true : false;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al configurar el control de acceso a nivel de operacion de las páginas del sistema. Error: [" + Ex.Message + "]");
            }
        }

        public static void Configuracion_Acceso_Sistema_SIAS(List<Button> Botones, DataRow Accesos)
        {
            String Operacion = String.Empty;

            try
            {
                foreach (Button Btn_Aux in Botones)
                {
                    if (Btn_Aux is Button)
                    {
                        switch (Btn_Aux.ToolTip.Trim().ToUpper())
                        {
                            case "NUEVO":
                                if (!String.IsNullOrEmpty(Accesos[Apl_Cat_Accesos.Campo_Alta].ToString()))
                                    Btn_Aux.Visible = (Accesos[Apl_Cat_Accesos.Campo_Alta].ToString().Equals("S")) ? true : false;
                                break;
                            case "MODIFICAR":
                                if (!String.IsNullOrEmpty(Accesos[Apl_Cat_Accesos.Campo_Cambio].ToString()))
                                    Btn_Aux.Visible = (Accesos[Apl_Cat_Accesos.Campo_Cambio].ToString().Equals("S")) ? true : false;
                                break;
                            case "ELIMINAR":
                                if (!String.IsNullOrEmpty(Accesos[Apl_Cat_Accesos.Campo_Eliminar].ToString()))
                                    Btn_Aux.Visible = (Accesos[Apl_Cat_Accesos.Campo_Eliminar].ToString().Equals("S")) ? true : false;
                                break;
                            case "CONSULTAR":
                                if (!String.IsNullOrEmpty(Accesos[Apl_Cat_Accesos.Campo_Consultar].ToString()))
                                    Btn_Aux.Visible = (Accesos[Apl_Cat_Accesos.Campo_Consultar].ToString().Equals("S")) ? true : false;
                                break;
                            case "AVANZADA":
                                if (!String.IsNullOrEmpty(Accesos[Apl_Cat_Accesos.Campo_Consultar].ToString()))
                                    Btn_Aux.Visible = (Accesos[Apl_Cat_Accesos.Campo_Consultar].ToString().Equals("S")) ? true : false;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al configurar el control de acceso a nivel de operacion de las páginas del sistema. Error: [" + Ex.Message + "]");
            }
        }

        public static void Configuracion_Acceso_Sistema_SIAS_AlternateText(List<ImageButton> Botones, DataRow Accesos)
        {
            String Operacion = String.Empty;

            try
            {
                foreach (ImageButton Btn_Aux in Botones)
                {
                    if (Btn_Aux is ImageButton)
                    {
                        switch (Btn_Aux.AlternateText.Trim().ToUpper())
                        {
                            case "NUEVO":
                                if (!String.IsNullOrEmpty(Accesos[Apl_Cat_Accesos.Campo_Alta].ToString()))
                                    Btn_Aux.Visible = (Accesos[Apl_Cat_Accesos.Campo_Alta].ToString().Equals("S")) ? true : false;
                                break;
                            case "MODIFICAR":
                                if (!String.IsNullOrEmpty(Accesos[Apl_Cat_Accesos.Campo_Cambio].ToString()))
                                    Btn_Aux.Visible = (Accesos[Apl_Cat_Accesos.Campo_Cambio].ToString().Equals("S")) ? true : false;
                                break;
                            case "ELIMINAR":
                                if (!String.IsNullOrEmpty(Accesos[Apl_Cat_Accesos.Campo_Eliminar].ToString()))
                                    Btn_Aux.Visible = (Accesos[Apl_Cat_Accesos.Campo_Eliminar].ToString().Equals("S")) ? true : false;
                                break;
                            case "CONSULTAR":
                                if (!String.IsNullOrEmpty(Accesos[Apl_Cat_Accesos.Campo_Consultar].ToString()))
                                    Btn_Aux.Visible = (Accesos[Apl_Cat_Accesos.Campo_Consultar].ToString().Equals("S")) ? true : false;
                                break;
                            case "AVANZADA":
                                if (!String.IsNullOrEmpty(Accesos[Apl_Cat_Accesos.Campo_Consultar].ToString()))
                                    Btn_Aux.Visible = (Accesos[Apl_Cat_Accesos.Campo_Consultar].ToString().Equals("S")) ? true : false;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al configurar el control de acceso a nivel de operacion de las páginas del sistema. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Consecutivo
        ///DESCRIPCIÓN: Obtiene el numero consecutivo para las tablas ocupadas en esta clase
        ///PARAMETROS: 1.-Campo del cual se obtendra el consecutivo
        ///            2.-Nombre de la tabla
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 10/Enero/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Obtener_Consecutivo(String Campo_ID, String Tabla)
        {
            int Consecutivo = 0;
            String Mi_Sql;
            Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos
            Mi_Sql = "SELECT ISNULL(MAX (" + Campo_ID + "),0) FROM " + Tabla;
            Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            Consecutivo = (Convert.ToInt32(Obj) + 1);
            return Consecutivo;
        }

        public static bool Registrar_Historial(String Estatus, String No_Requisicion)
        {
            No_Requisicion = No_Requisicion.Replace("RQ-", "");
            bool Resultado = false;
            try
            {
                int Consecutivo =
                    Obtener_Consecutivo(Ope_Com_Historial_Req.Campo_No_Historial, Ope_Com_Historial_Req.Tabla_Ope_Com_Historial_Req);
                String Mi_SQL = "";
                Mi_SQL = "INSERT INTO " + Ope_Com_Historial_Req.Tabla_Ope_Com_Historial_Req + " (" +
                    Ope_Com_Historial_Req.Campo_No_Historial + "," +
                    Ope_Com_Historial_Req.Campo_No_Requisicion + "," +
                    Ope_Com_Historial_Req.Campo_Estatus + "," +
                    Ope_Com_Historial_Req.Campo_Fecha + "," +
                    Ope_Com_Historial_Req.Campo_Empleado + "," +
                    Ope_Com_Historial_Req.Campo_Usuario_Creo + "," +
                    Ope_Com_Historial_Req.Campo_Fecha_Creo + ") VALUES (" +
                    Consecutivo + ", " +
                    No_Requisicion + ", '" +
                    Estatus + "', " +
                    "GETDATE(), '" +
                    Cls_Sessiones.Nombre_Empleado + "', '" +
                    Cls_Sessiones.Nombre_Empleado + "', " +
                    " GETDATE())";
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                Resultado = true;
            }
            catch (Exception Ex)
            {
                String Str_Ex = Ex.ToString();
                Resultado = false;
            }
            return Resultado;
        }
        public static string Obtener_Tipo_Contenido(string extension)
        {
            switch (extension.ToLower())
            {

                case ".png": return "imagen/png";
                case ".pptx": return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                case ".docx": return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                case ".xlsx": return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";   
                case "": return "application/octet-stream";
                case ".323": return "text/h323";
                case ".acx": return "application/internet-property-stream";
                case ".ai": return "application/postscript";
                case ".aif": return "audio/x-aiff";
                case ".aifc": return "audio/x-aiff";
                case ".aiff": return "audio/x-aiff";
                case ".asf": return "video/x-ms-asf";
                case ".asr": return "video/x-ms-asf";
                case ".asx": return "video/x-ms-asf";
                case ".au": return "audio/basic";
                case ".avi": return "video/x-msvideo";
                case ".axs": return "application/olescript";
                case ".bas": return "text/plain";
                case ".bcpio": return "application/x-bcpio";
                case ".bin": return "application/octet-stream";
                case ".bmp": return "image/bmp";
                case ".c": return "text/plain";
                case ".cat": return "application/vnd.ms-pkiseccat";
                case ".cdf": return "application/x-cdf";
                case ".cer": return "application/x-x509-ca-cert";
                case ".class": return "application/octet-stream";
                case ".clp": return "application/x-msclip";
                case ".cmx": return "image/x-cmx";
                case ".cod": return "image/cis-cod";
                case ".cpio": return "application/x-cpio";
                case ".crd": return "application/x-mscardfile";
                case ".crl": return "application/pkix-crl";
                case ".crt": return "application/x-x509-ca-cert";
                case ".csh": return "application/x-csh";
                case ".css": return "text/css";
                case ".dcr": return "application/x-director";
                case ".der": return "application/x-x509-ca-cert";
                case ".dir": return "application/x-director";
                case ".dll": return "application/x-msdownload";
                case ".dms": return "application/octet-stream";
                case ".doc": return "application/msword";
                case ".dot": return "application/msword";
                case ".dvi": return "application/x-dvi";
                case ".dxr": return "application/x-director";
                case ".eps": return "application/postscript";
                case ".etx": return "text/x-setext";
                case ".evy": return "application/envoy";
                case ".exe": return "application/octet-stream";
                case ".fif": return "application/fractals";
                case ".flr": return "x-world/x-vrml";
                case ".gif": return "image/gif";
                case ".gtar": return "application/x-gtar";
                case ".gz": return "application/x-gzip";
                case ".h": return "text/plain";
                case ".hdf": return "application/x-hdf";
                case ".hlp": return "application/winhlp";
                case ".hqx": return "application/mac-binhex40";
                case ".hta": return "application/hta";
                case ".htc": return "text/x-component";
                case ".htm": return "text/html";
                case ".html": return "text/html";
                case ".htt": return "text/webviewhtml";
                case ".ico": return "image/x-icon";
                case ".ief": return "image/ief";
                case ".iii": return "application/x-iphone";
                case ".ins": return "application/x-internet-signup";
                case ".isp": return "application/x-internet-signup";
                case ".jfif": return "image/pipeg";
                case ".jpe": return "image/jpeg";
                case ".jpeg": return "image/jpeg";
                case ".jpg": return "image/jpeg";
                case ".js": return "application/x-javascript";
                case ".latex": return "application/x-latex";
                case ".lha": return "application/octet-stream";
                case ".lsf": return "video/x-la-asf";
                case ".lsx": return "video/x-la-asf";
                case ".lzh": return "application/octet-stream";
                case ".m13": return "application/x-msmediaview";
                case ".m14": return "application/x-msmediaview";
                case ".m3u": return "audio/x-mpegurl";
                case ".man": return "application/x-troff-man";
                case ".mdb": return "application/x-msaccess";
                case ".me": return "application/x-troff-me";
                case ".mht": return "message/rfc822";
                case ".mhtml": return "message/rfc822";
                case ".mid": return "audio/mid";
                case ".mny": return "application/x-msmoney";
                case ".mov": return "video/quicktime";
                case ".movie": return "video/x-sgi-movie";
                case ".mp2": return "video/mpeg";
                case ".mp3": return "audio/mpeg";
                case ".mpa": return "video/mpeg";
                case ".mpe": return "video/mpeg";
                case ".mpeg": return "video/mpeg";
                case ".mpg": return "video/mpeg";
                case ".mpp": return "application/vnd.ms-project";
                case ".mpv2": return "video/mpeg";
                case ".ms": return "application/x-troff-ms";
                case ".mvb": return "application/x-msmediaview";
                case ".nws": return "message/rfc822";
                case ".oda": return "application/oda";
                case ".p10": return "application/pkcs10";
                case ".p12": return "application/x-pkcs12";
                case ".p7b": return "application/x-pkcs7-certificates";
                case ".p7c": return "application/x-pkcs7-mime";
                case ".p7m": return "application/x-pkcs7-mime";
                case ".p7r": return "application/x-pkcs7-certreqresp";
                case ".p7s": return "application/x-pkcs7-signature";
                case ".pbm": return "image/x-portable-bitmap";
                case ".pdf": return "application/pdf";
                case ".pfx": return "application/x-pkcs12";
                case ".pgm": return "image/x-portable-graymap";
                case ".pko": return "application/ynd.ms-pkipko";
                case ".pma": return "application/x-perfmon";
                case ".pmc": return "application/x-perfmon";
                case ".pml": return "application/x-perfmon";
                case ".pmr": return "application/x-perfmon";
                case ".pmw": return "application/x-perfmon";
                case ".pnm": return "image/x-portable-anymap";
                case ".pot,": return "application/vnd.ms-powerpoint";
                case ".ppm": return "image/x-portable-pixmap";
                case ".pps": return "application/vnd.ms-powerpoint";
                case ".ppt": return "application/vnd.ms-powerpoint";
                case ".prf": return "application/pics-rules";
                case ".ps": return "application/postscript";
                case ".pub": return "application/x-mspublisher";
                case ".qt": return "video/quicktime";
                case ".ra": return "audio/x-pn-realaudio";
                case ".ram": return "audio/x-pn-realaudio";
                case ".ras": return "image/x-cmu-raster";
                case ".rgb": return "image/x-rgb";
                case ".rmi": return "audio/mid";
                case ".roff": return "application/x-troff";
                case ".rtf": return "application/rtf";
                case ".rtx": return "text/richtext";
                case ".scd": return "application/x-msschedule";
                case ".sct": return "text/scriptlet";
                case ".setpay": return "application/set-payment-initiation";
                case ".setreg": return "application/set-registration-initiation";
                case ".sh": return "application/x-sh";
                case ".shar": return "application/x-shar";
                case ".sit": return "application/x-stuffit";
                case ".snd": return "audio/basic";
                case ".spc": return "application/x-pkcs7-certificates";
                case ".spl": return "application/futuresplash";
                case ".src": return "application/x-wais-source";
                case ".sst": return "application/vnd.ms-pkicertstore";
                case ".stl": return "application/vnd.ms-pkistl";
                case ".stm": return "text/html";
                case ".svg": return "image/svg+xml";
                case ".sv4cpio": return "application/x-sv4cpio";
                case ".sv4crc": return "application/x-sv4crc";
                case ".swf": return "application/x-shockwave-flash";
                case ".t": return "application/x-troff";
                case ".tar": return "application/x-tar";
                case ".tcl": return "application/x-tcl";
                case ".tex": return "application/x-tex";
                case ".texi": return "application/x-texinfo";
                case ".texinfo": return "application/x-texinfo";
                case ".tgz": return "application/x-compressed";
                case ".tif": return "image/tiff";
                case ".tiff": return "image/tiff";
                case ".tr": return "application/x-troff";
                case ".trm": return "application/x-msterminal";
                case ".tsv": return "text/tab-separated-values";
                case ".txt": return "text/plain";
                case ".uls": return "text/iuls";
                case ".ustar": return "application/x-ustar";
                case ".vcf": return "text/x-vcard";
                case ".vrml": return "x-world/x-vrml";
                case ".wav": return "audio/x-wav";
                case ".wcm": return "application/vnd.ms-works";
                case ".wdb": return "application/vnd.ms-works";
                case ".wks": return "application/vnd.ms-works";
                case ".wmf": return "application/x-msmetafile";
                case ".wps": return "application/vnd.ms-works";
                case ".wri": return "application/x-mswrite";
                case ".wrl": return "x-world/x-vrml";
                case ".wrz": return "x-world/x-vrml";
                case ".xaf": return "x-world/x-vrml";
                case ".xbm": return "image/x-xbitmap";
                case ".xla": return "application/vnd.ms-excel";
                case ".xlc": return "application/vnd.ms-excel";
                case ".xlm": return "application/vnd.ms-excel";
                case ".xls": return "application/vnd.ms-excel";
                case ".xlt": return "application/vnd.ms-excel";
                case ".xlw": return "application/vnd.ms-excel";
                case ".xof": return "x-world/x-vrml";
                case ".xpm": return "image/x-xpixmap";
                case ".xwd": return "image/x-xwindowdump";
                case ".z": return "application/x-compress";
                case ".zip": return "application/zip";
            }
            return "application/octet-stream";
        }

        public static string Tipo_Archivo(string tipo)
        {
            switch (tipo.ToLower())
            {
                case "image": return "IMAGEN";
                case "audio": return "AUDIO";
                case "video": return "VIDEO";
            }

            return "OTROS";
        }

        public static string Ruta_Icono_Mime(string extension)
        {
            switch (extension)
            {
                case "": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".323": return "~\\paginas\\imagenes\\mime\\txt.ico";
                case ".acx": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".ai": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".aif": return "~\\paginas\\imagenes\\mime\\audio.ico";
                case ".aifc": return "~\\paginas\\imagenes\\mime\\audio.ico";
                case ".aiff": return "~\\paginas\\imagenes\\mime\\audio.ico";
                case ".asf": return "~\\paginas\\imagenes\\mime\\vid.ico";
                case ".asr": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".asx": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".au": return "~\\paginas\\imagenes\\mime\\audio.ico";
                case ".avi": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".axs": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".bas": return "~\\paginas\\imagenes\\mime\\txt.ico";
                case ".bcpio": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".bin": return "~\\paginas\\imagenes\\mime\\exe.ico";
                case ".bmp": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".c": return "~\\paginas\\imagenes\\mime\\txt.ico";
                case ".cat": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".cdf": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".cer": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".class": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".clp": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".cmx": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".cod": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".cpio": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".crd": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".crl": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".crt": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".csh": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".css": return "~\\paginas\\imagenes\\mime\\css.ico";
                case ".dcr": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".der": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".dir": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".dll": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".dms": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".doc": return "~\\paginas\\imagenes\\mime\\docx.ico";
                case ".docx": return "~\\paginas\\imagenes\\mime\\docx.ico";
                case ".dot": return "~\\paginas\\imagenes\\mime\\docx.ico";
                case ".dvi": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".dxr": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".eps": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".etx": return "~\\paginas\\imagenes\\mime\\txt.ico";
                case ".evy": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".exe": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".fif": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".flr": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".gif": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".gtar": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".gz": return "~\\paginas\\imagenes\\mime\\paq.ico";
                case ".h": return "~\\paginas\\imagenes\\mime\\txt.ico";
                case ".hdf": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".hlp": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".hqx": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".hta": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".htc": return "~\\paginas\\imagenes\\mime\\txt.ico";
                case ".htm": return "~\\paginas\\imagenes\\mime\\html.ico";
                case ".html": return "~\\paginas\\imagenes\\mime\\html.ico";
                case ".htt": return "~\\paginas\\imagenes\\mime\\html.ico";
                case ".ico": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".ief": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".iii": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".ins": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".isp": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".jfif": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".jpe": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".jpeg": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".jpg": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".js": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".latex": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".lha": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".lsf": return "~\\paginas\\imagenes\\mime\\vid.ico";
                case ".lsx": return "~\\paginas\\imagenes\\mime\\vid.ico";
                case ".lzh": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".m13": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".m14": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".m3u": return "~\\paginas\\imagenes\\mime\\audio.ico";
                case ".man": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".mdb": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".me": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".mht": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".mhtml": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".mid": return "~\\paginas\\imagenes\\mime\\audio.ico";
                case ".mny": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".mov": return "~\\paginas\\imagenes\\mime\\vid.ico";
                case ".movie": return "~\\paginas\\imagenes\\mime\\vid.ico";
                case ".mp2": return "~\\paginas\\imagenes\\mime\\vid.ico";
                case ".mp3": return "~\\paginas\\imagenes\\mime\\audio.ico";
                case ".mpa": return "~\\paginas\\imagenes\\mime\\vid.ico";
                case ".mpe": return "~\\paginas\\imagenes\\mime\\vid.ico";
                case ".mpeg": return "~\\paginas\\imagenes\\mime\\vid.ico";
                case ".mpg": return "~\\paginas\\imagenes\\mime\\vid.ico";
                case ".mpp": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".mpv2": return "~\\paginas\\imagenes\\mime\\vid.ico";
                case ".ms": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".mvb": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".nws": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".oda": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".p10": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".p12": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".p7b": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".p7c": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".p7m": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".p7r": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".p7s": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".pbm": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".pdf": return "~\\paginas\\imagenes\\mime\\pdf.ico";
                case ".pfx": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".pgm": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".pko": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".pma": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".pmc": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".pml": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".pmr": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".pmw": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".pnm": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".pot,": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".ppm": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".pps": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".ppt": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".prf": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".ps": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".pub": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".qt": return "~\\paginas\\imagenes\\mime\\vid.ico";
                case ".ra": return "~\\paginas\\imagenes\\mime\\audio.ico";
                case ".ram": return "~\\paginas\\imagenes\\mime\\audio.ico";
                case ".ras": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".rgb": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".rmi": return "~\\paginas\\imagenes\\mime\\audio.ico";
                case ".roff": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".rtf": return "~\\paginas\\imagenes\\mime\\txt.ico";
                case ".rtx": return "~\\paginas\\imagenes\\mime\\txt.ico";
                case ".scd": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".sct": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".setpay": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".setreg": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".sh": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".shar": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".sit": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".snd": return "~\\paginas\\imagenes\\mime\\audio.ico";
                case ".spc": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".spl": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".src": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".sst": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".stl": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".stm": return "~\\paginas\\imagenes\\mime\\html.ico";
                case ".svg": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".sv4cpio": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".sv4crc": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".swf": return "~\\paginas\\imagenes\\mime\\fla.ico";
                case ".t": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".tar": return "~\\paginas\\imagenes\\mime\\paq.ico";
                case ".tcl": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".tex": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".texi": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".texinfo": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".tgz": return "~\\paginas\\imagenes\\mime\\paq.ico";
                case ".tif": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".tiff": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".tr": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".trm": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".tsv": return "~\\paginas\\imagenes\\mime\\txt.ico";
                case ".txt": return "~\\paginas\\imagenes\\mime\\txt.ico";
                case ".uls": return "~\\paginas\\imagenes\\mime\\txt.ico";
                case ".ustar": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".vcf": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".vrml": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".wav": return "~\\paginas\\imagenes\\mime\\audio.ico";
                case ".wcm": return "~\\paginas\\imagenes\\mime\\txt.ico";
                case ".wdb": return "~\\paginas\\imagenes\\mime\\txt.ico";
                case ".wks": return "~\\paginas\\imagenes\\mime\\txt.ico";
                case ".wmf": return "~\\paginas\\imagenes\\mime\\txt.ico";
                case ".wps": return "~\\paginas\\imagenes\\mime\\txt.ico";
                case ".wri": return "~\\paginas\\imagenes\\mime\\txt.ico";
                case ".wrl": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".wrz": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".xaf": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".xbm": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".xla": return "~\\paginas\\imagenes\\mime\\xlsx.ico";
                case ".xlc": return "~\\paginas\\imagenes\\mime\\xlsx.ico";
                case ".xlm": return "~\\paginas\\imagenes\\mime\\xlsx.ico";
                case ".xls": return "~\\paginas\\imagenes\\mime\\xlsx.ico";
                case ".xlsx": return "~\\paginas\\imagenes\\mime\\xlsx.ico";
                case ".xlt": return "~\\paginas\\imagenes\\mime\\xlsx.ico";
                case ".xlw": return "~\\paginas\\imagenes\\mime\\xlsx.ico";
                case ".xof": return "~\\paginas\\imagenes\\mime\\file.ico";
                case ".xpm": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".xwd": return "~\\paginas\\imagenes\\mime\\img.ico";
                case ".z": return "~\\paginas\\imagenes\\mime\\paq.ico";
                case ".zip": return "~\\paginas\\imagenes\\mime\\paq.ico";
            }
            return "~\\paginas\\imagenes\\mime\\file.ico";
        }


        public static void Llenar_Combo_Con_DataTable_Generico()
        {
            throw new NotImplementedException();
        }

        public static Boolean EsNumerico(String Valor)
        {
            //Declaracion de variables
            Boolean Resultado = false; //variable para el resultado
            Regex Patron = new Regex("[^0-9]");
            try
            {
                //Verificar si no es nulo
                if (String.IsNullOrEmpty(Valor) == false)
                {
                    //verificar si concuerda
                    Resultado = !Patron.IsMatch(Valor);
                }

                //Entregar resultado
                return Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        public static int Entrega_Indice_Combo(DropDownList Combo, String Valor)
        {
            //Declaracion de variables
            int Cont_Elementos = 0; //variable para el contador
            int Indice = 0; //variable para el indice del combo

            try
            {
                //Ciclo para el barrido del combo
                for (Cont_Elementos = 0; Cont_Elementos < Combo.Items.Count; Cont_Elementos++)
                {
                    //verificar si el valor es el mismo
                    if (Combo.Items[Cont_Elementos].Value == Valor)
                    {
                        Indice = Cont_Elementos;
                        break;
                    }
                }

                //Entregar resultado
                return Indice;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_URS_Por_Grupo
        ///DESCRIPCIÓN: Busca a que grupo de Roles pertenece un Rol_ID
        ///Devuelve un DataTable con Rol_ID,Grupo_Rol_ID, Nombre
        ///PARAMETROS: 1.-Rol_ID
        ///CREO: Sergio Manuel Gallardo A.
        ///FECHA_CREO: 22/Agosto/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_URS_Por_Grupo(String Grupo)
        {
            String Mi_SQL = "";
            Mi_SQL = "SELECT " + Cat_Dependencias.Campo_Dependencia_ID + ", " + Cat_Dependencias.Campo_Clave + "+' '+" + Cat_Dependencias.Campo_Nombre + " AS CLAVE_NOMBRE";
            Mi_SQL += " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias;
            Mi_SQL += " WHERE " + Cat_Dependencias.Campo_Grupo_Dependencia_ID + " ='" + Grupo + "'" + " AND " + Cat_Dependencias.Campo_Estatus + "='ACTIVO' ORDER BY " + Cat_Dependencias.Campo_Clave;
            DataSet _DataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            DataTable Tabla = null;
            if (_DataSet.Tables.Count != 0)
            {
                Tabla = _DataSet.Tables[0];
            }
            return Tabla;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_URS_Por_Grupo
        ///DESCRIPCIÓN: Busca a que grupo de Roles pertenece un Rol_ID
        ///Devuelve un DataTable con Rol_ID,Grupo_Rol_ID, Nombre
        ///PARAMETROS: 1.-Rol_ID
        ///CREO: Sergio Manuel Gallardo A.
        ///FECHA_CREO: 22/Agosto/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Reservas_Por_Grupo(String Fecha_Inico, String Fecha_Fin, int RESERVA, String GRUPO)
        {
            String Mi_SQL = "";
            Mi_SQL = "SELECT " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + ".* FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
            Mi_SQL += " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " ON " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID + "=" + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
            Mi_SQL += " WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID + "='" + GRUPO + "'";
            Mi_SQL += " AND " + Ope_Psp_Reservas.Campo_Fecha + ">='" + Fecha_Inico + "' AND " + Ope_Psp_Reservas.Campo_Fecha + "<='" + Fecha_Fin + "'";
            if (RESERVA > 0)
            {
                Mi_SQL = "AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + " = " + RESERVA;
            }
            Mi_SQL += " ORDER BY " + Ope_Psp_Reservas.Campo_No_Reserva + " DESC";
            DataSet _DataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            DataTable Tabla = null;
            if (_DataSet.Tables.Count != 0)
            {
                Tabla = _DataSet.Tables[0];
            }
            return Tabla;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_URS_Por_Grupo
        ///DESCRIPCIÓN: Busca a que grupo de Roles pertenece un Rol_ID
        ///Devuelve un DataTable con Rol_ID,Grupo_Rol_ID, Nombre
        ///PARAMETROS: 1.-Rol_ID
        ///CREO: Sergio Manuel Gallardo A.
        ///FECHA_CREO: 22/Agosto/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Reservas_Por_Grupo(String GRUPO)
        {
            String Mi_SQL = "";
            Mi_SQL = "SELECT " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + ",";
            Mi_SQL += " (LTRIM(RTRIM(STR(" + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + ")))+'-'+" + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Concepto + ") AS Reserva";
            Mi_SQL += " FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
            Mi_SQL += " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " ON " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID + "=" + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
            Mi_SQL += " WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID + "='" + GRUPO + "'";
            Mi_SQL += " AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Saldo + " > 0";
            Mi_SQL += " AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Estatus + " ='GENERADA'";
            Mi_SQL += " ORDER BY " + Ope_Psp_Reservas.Campo_No_Reserva + " DESC";
            DataSet _DataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            DataTable Tabla = null;
            if (_DataSet.Tables.Count != 0)
            {
                Tabla = _DataSet.Tables[0];
            }
            return Tabla;
        }

    }
