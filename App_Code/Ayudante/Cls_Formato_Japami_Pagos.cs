﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace JAPAMI.Formato_Pago
{

    public class Cls_Formato_Japami_Pagos
    {    
         
         public string no_consecutivo  { get; set;}
         public string institucion { get; set; }
         public string sucursal { get; set; }
         public string fecha_pago { get; set; }
         public string no_cuenta { get; set; }
         public double monto { get; set; }
         public double saldo { set; get; }
         public String codigo { set; get; }
         public String no_factura_recibo { set; get; }


        public Cls_Formato_Japami_Pagos()
        {
        }
    }

}
    