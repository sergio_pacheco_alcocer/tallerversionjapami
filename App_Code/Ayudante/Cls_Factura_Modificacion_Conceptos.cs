﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;



namespace JAPAMI.ModificacionFactura {


    public class Cls_Factura_Modificacion_Conceptos
    {

        public string concepto { get; set; }
        public double importe { get; set; }
        public double impuesto { get; set; }
        public double total { get; set; }
        public double total_abonado { get; set; }
        public double total_saldo { get; set; }
        public string no_factura_recibo { get; set; }
        public string concepto_id { get; set; }


        public Cls_Factura_Modificacion_Conceptos()
        {

        }
    }


}


