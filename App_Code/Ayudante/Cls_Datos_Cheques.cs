﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace JAPAMI.Datos_Cheques {

    public class Cls_Datos_Cheques
    {

        public string no_cheque { set; get; }
        public double cantidad { set; get; }
        public string banco { set; get; }
        public string banco_id { set; get; }
        public string nombre { set; get; }
        public string paterno { set; get; }
        public string materno { set; get; }
        public string cantidad_letra { set; get; }


    }



}

