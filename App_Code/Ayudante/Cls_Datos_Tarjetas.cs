﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace JAPAMI.Datos_Tarjetas {

    public class Cls_Datos_Tarjetas
    {

        public double monto {set; get;}
        public string no_aprobacion { set; get; }
        public string no_tarjeta { set; get; }
        public string banco { set; get; }
        public string banco_id { set; get; }
        public string tarjeta_id { set; get; }
        public string operacion_caja_id { set; get; }
        public string no_recibo { set; get; }

    }
}


