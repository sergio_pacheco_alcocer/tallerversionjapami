﻿$(function() {

    ///INICIALIZACION DE ELEMENTOS
    Inicializar_Controles();

    ///METODOS

    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN: Inicializar_Controles
    //DESCRIPCIÓN: Inicializa los Controles del Formulario
    //PARÁMETROS:
    //CREO:Francisco Antonio Gallardo Castañeda.
    //FECHA_CREO: Viernes, 02 Agosto 2013
    //MODIFICÓ:
    //FECHA_MODIFICÓ:
    //CAUSA_MODIFICACIÓN:
    //*******************************************************************************************************
    function Inicializar_Controles() {
        //Habilitar/Inhabilitar Controles
        Habilitar_Controles("LISTADO");

        //Carga de Combos
        Cargar_Combo_Estatus();
        Cargar_Combos_Dependencias();
        Cargar_Combo_Tipo_Reparacion();

        //Limpieza de Elementos
        Limpiar_Filtros_Listado();

        //Carga de Estilos, Eventos y propiedades
        $("#Div_Contenedor_Resultados").tabs();
        $("#Div_Contenedor_Resultados").attr({
            style: 'width: 810px'
        });
        $('#Btn_Limpiar_Filtros_Listado').click(function(event) {
            Limpiar_Filtros_Listado();
        });
        $('#Btn_Actualizar_Listado').click(function(event) {
            Llenar_Grid_Solicitudes();
        });
        var Dp_Fecha_Recepcion_Inicial = $("[id$='Txt_Fecha_Recepcion_Inicial']");
        var Dp_Fecha_Recepcion_Final = $("[id$='Txt_Fecha_Recepcion_Final']");
        Asignar_Propiedades_DatePicker(Dp_Fecha_Recepcion_Inicial);
        Asignar_Propiedades_DatePicker(Dp_Fecha_Recepcion_Final);

        //Llenado de Grids de Pantalla Inicial
        Crear_Tabla_Revistas_Mecanicas();
        Crear_Tabla_Servicios_Correctivos();
        Crear_Tabla_Servicios_Preventivos();
        Crear_Tabla_Verificaciones();
        Llenar_Grid_Solicitudes();
    }

    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN: Limpiar_Filtros_Listado
    //DESCRIPCIÓN: Limpiar Filtros de Busqueda
    //PARÁMETROS:
    //CREO: Francisco Antonio Gallardo Castañeda.
    //FECHA_CREO: Viernes, 02 Agosto 2013
    //MODIFICÓ:
    //FECHA_MODIFICÓ:
    //CAUSA_MODIFICACIÓN:
    //*******************************************************************************************************
    function Limpiar_Filtros_Listado() {
        $("[id$='Txt_Folio_Solicitud_Busqueda']").val("");
        $("[id$='Cmb_Unidad_Responsable_Busqueda']").val("");
        $("[id$='Txt_Numero_Inventario_Busqueda']").val("");
        $("[id$='Txt_Numero_Economico_Busqueda']").val("");
        $("[id$='Txt_Fecha_Recepcion_Inicial']").val("");
        $("[id$='Txt_Fecha_Recepcion_Final']").val("");
        $("[id$='Cmb_Filtrado_Estatus']").val("");
        $("[id$='Cmb_Reparacion_Busqueda']").val("");
    }

    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN: Cargar_Combo_Estatus
    //DESCRIPCIÓN: Carga el Combo de Estatus
    //PARÁMETROS:
    //CREO: Francisco Antonio Gallardo Castañeda.
    //FECHA_CREO: Viernes, 02 Agosto 2013
    //MODIFICÓ:
    //FECHA_MODIFICÓ:
    //CAUSA_MODIFICACIÓN:
    //*******************************************************************************************************
    function Cargar_Combo_Estatus() {
        var select = $("[id$='Cmb_Filtrado_Estatus']");
        $('option', select).remove();
        var options = '<option value="">&lt; - - TODOS - - &gt;</option>';
        options += '<option value="PENDIENTE">PENDIENTE</option>';
        options += '<option value="RECHAZADA">RECHAZADA</option>';
        options += '<option value="CANCELADA">CANCELADA</option>';
        options += '<option value="AUTORIZADA">AUTORIZADA</option>';
        options += '<option value="EN_REPARACION">EN REPARACI&Oacute;N</option>';
        options += '<option value="ENTREGADO">ENTREGADO</option>';
        select.append(options);
    }

    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN: Cargar_Combo_Tipo_Reparacion
    //DESCRIPCIÓN: Carga el Combo de Tipos de Reparacion
    //PARÁMETROS:
    //CREO: Francisco Antonio Gallardo Castañeda.
    //FECHA_CREO: Viernes, 02 Agosto 2013
    //MODIFICÓ:
    //FECHA_MODIFICÓ:
    //CAUSA_MODIFICACIÓN:
    //*******************************************************************************************************
    function Cargar_Combo_Tipo_Reparacion() {
        var select = $("[id$='Cmb_Reparacion_Busqueda']");
        $('option', select).remove();
        var options = '<option value="">&lt; - - TODAS - - &gt;</option>';
        options += '<option value="INTERNA">INTERNA</option>';
        options += '<option value="EXTERNA">EXTERNA</option>';
        select.append(options);
    }

    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN: Cargar_Combos_Dependencias
    //DESCRIPCIÓN: Carga los Combos de Dependencias
    //PARÁMETROS:
    //CREO: Francisco Antonio Gallardo Castañeda.
    //FECHA_CREO: Viernes, 02 Agosto 2013
    //MODIFICÓ:
    //FECHA_MODIFICÓ:
    //CAUSA_MODIFICACIÓN:
    //*******************************************************************************************************
    function Cargar_Combos_Dependencias() {
        $.ajax({
            url: "Frm_Ope_Tal_Controlador_Bitacora_Servicios_Vehiculo.aspx?accion=consultar_listado_unidades_responsables",
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data) {
                if (data != null) {
                    $('option', $("[id$='Cmb_Unidad_Responsable_Busqueda']")).remove();
                    var options = '<option value="">&lt;- -TODAS- -&gt;</option>';
                    $.each(data, function(i, item) {
                        options += '<option value="' + item.dependencia_id + '">' + item.clave_nombre + '</option>';
                    });
                    $("[id$='Cmb_Unidad_Responsable_Busqueda']").append(options);
                }
            },
            error: function(result) {
                alert("ERROR " + result.status + " " + result.statusText);
            }
        });
    }

    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN: Asignar_Propiedades_DatePicker
    //DESCRIPCIÓN: Carga las propiedades al DatePicker
    //PARÁMETROS:
    //CREO: Francisco Antonio Gallardo Castañeda.
    //FECHA_CREO: Lunes, 05 Agosto 2013
    //MODIFICÓ:
    //FECHA_MODIFICÓ:
    //CAUSA_MODIFICACIÓN:
    //*******************************************************************************************************
    function Asignar_Propiedades_DatePicker(Dp_Elemento_Fecha) {
        $.datepicker.setDefaults($.datepicker.regional["es"]);
        Dp_Elemento_Fecha.datepicker({
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
            dateFormat: 'dd/mm/yy',
            selectOtherMonths: false
        });
    }

    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN: Habilitar_Controles
    //DESCRIPCIÓN: Habilita/Inhabilita los Controles
    //PARÁMETROS:Estatus. Se cargará o no los Componentes
    //CREO: Francisco Antonio Gallardo Castañeda.
    //FECHA_CREO: Lunes, 05 Agosto 2013
    //MODIFICÓ:
    //FECHA_MODIFICÓ:
    //CAUSA_MODIFICACIÓN:
    //*******************************************************************************************************
    function Habilitar_Controles(Estatus) {
        if (Estatus == 'LISTADO') {
            $('#Btn_Imprimir_Solicitud_Servicio').attr({
                style: 'display:none;'
            });
            $('#Btn_Imprimir_Bitacora_Seguimiento').attr({
                style: 'display:none;'
            });
            $('#Btn_Cancelar').attr({
                style: 'display:none;'
            });
            $('#Btn_Dianostico').attr({
                style: 'display:none;'
            });
            $('#Btn_Salir').attr({
                style: 'display:inline;'
            });
        } else {
            $('#Btn_Imprimir_Solicitud_Servicio').attr({
                style: 'display:inline;'
            });
            $('#Btn_Imprimir_Bitacora_Seguimiento').attr({
                style: 'display:inline;'
            });
            $('#Btn_Cancelar').attr({
                style: 'display:inline;'
            });
            $('#Btn_Dianostico').attr({
                style: 'display:inline;'
            });
            $('#Btn_Salir').attr({
                style: 'display:inline;'
            });
        }
    }

    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN: Crear_Tabla_Revistas_Mecanicas
    //DESCRIPCIÓN: Crea el Grid para cargar el Listado.
    //PARÁMETROS:
    //CREO: Francisco Antonio Gallardo Castañeda
    //FECHA_CREO: Lunes, 05/Agosto/2013
    //MODIFICÓ:
    //FECHA_MODIFICÓ:
    //CAUSA_MODIFICACIÓN:
    //*******************************************************************************************************
    function Crear_Tabla_Revistas_Mecanicas() {
        $('#Grid_Revistas_Mecanicas').datagrid({
            title: 'Listado Solicitudes de Revista Mecanica',
            width: 790,
            height: 500,
            columns: [[
                { field: 'NO_SOLICITUD', title: 'NO_SOLICITUD', width: 80, sortable: true, align: 'center' },
                { field: 'FOLIO', title: 'Folio', width: 80, sortable: true, align: 'center' },
                { field: 'FECHA_ELABORACION_TBL', title: 'Fecha', width: 80, sortable: true, align: 'center'},
                { field: 'FECHA_RECEPCION_REAL_TBL', title: 'Fecha Recepción', width: 80, sortable: true, align: 'center'},
                { field: 'TIPO_SERVICIO', title: 'TIPO_SERVICIO', width: 80, sortable: true, align: 'center' },
                { field: 'NO_INVENTARIO', title: 'No. Inventario', width: 80, sortable: true, align: 'center' },
                { field: 'ESTATUS', title: 'Estatus', width: 80, sortable: true, align: 'center' },
                { field: 'NO_ECONOMICO', title: 'No. Ecónomico', width: 80, sortable: true, align: 'center' }
            ]],
            pageSize: 50,
            pagination: true,
            rownumbers: true,
            remoteSort: false,
            fitColumns: true,
            singleSelect: true,
            showFooter: false,
            striped: true,
            fit: true,
            loadMsg: 'Cargando las Revistas Mecanicas...',
            nowrap: false
        });   // tabla final
        $('#Grid_Revistas_Mecanicas').datagrid('hideColumn', 'NO_SOLICITUD');
        $('#Grid_Revistas_Mecanicas').datagrid('hideColumn', 'TIPO_SERVICIO');
        $('#Grid_Revistas_Mecanicas').datagrid('hideColumn', 'FECHA_RECEPCION_REAL');
    }

    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN: Crear_Tabla_Servicios_Correctivos
    //DESCRIPCIÓN: Crea el Grid para cargar el Listado.
    //PARÁMETROS:
    //CREO: Francisco Antonio Gallardo Castañeda
    //FECHA_CREO: Lunes, 05/Agosto/2013
    //MODIFICÓ:
    //FECHA_MODIFICÓ:
    //CAUSA_MODIFICACIÓN:
    //*******************************************************************************************************
    function Crear_Tabla_Servicios_Correctivos() {
        $('#Grid_Servicios_Correctivos').datagrid({
            title: 'Listado Solicitudes de Servicio Correctivo',
            width: 790,
            height: 500,
            columns: [[
                { field: 'NO_SOLICITUD', title: 'NO_SOLICITUD', width: 80, sortable: true, align: 'center' },
                { field: 'FOLIO', title: 'Folio', width: 80, sortable: true, align: 'center' },
                { field: 'FECHA_ELABORACION', title: 'Fecha', width: 80, sortable: true, align: 'center' },
                { field: 'FECHA_RECEPCION_REAL', title: 'Fecha Recepción', width: 80, sortable: true, align: 'center' },
                { field: 'TIPO_SERVICIO', title: 'TIPO_SERVICIO', width: 80, sortable: true, align: 'center' },
                { field: 'NO_INVENTARIO', title: 'No. Inventario', width: 80, sortable: true, align: 'center' },
                { field: 'ESTATUS', title: 'Estatus', width: 80, sortable: true, align: 'center' },
                { field: 'NO_ECONOMICO', title: 'No. Ecónomico', width: 80, sortable: true, align: 'center' }
            ]],
            pageSize: 50,
            pagination: true,
            rownumbers: true,
            remoteSort: false,
            fitColumns: true,
            singleSelect: true,
            showFooter: false,
            striped: true,
            fit: true,
            loadMsg: 'Cargando los Servicios Correctivos...',
            nowrap: false
        });   // tabla final
        $('#Grid_Servicios_Correctivos').datagrid('hideColumn', 'NO_SOLICITUD');
        $('#Grid_Servicios_Correctivos').datagrid('hideColumn', 'TIPO_SERVICIO');
        $('#Grid_Servicios_Correctivos').datagrid('hideColumn', 'FECHA_RECEPCION_REAL');
    }

    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN: Crear_Tabla_Servicios_Preventivos
    //DESCRIPCIÓN: Crea el Grid para cargar el Listado.
    //PARÁMETROS:
    //CREO: Francisco Antonio Gallardo Castañeda
    //FECHA_CREO: Lunes, 05/Agosto/2013
    //MODIFICÓ:
    //FECHA_MODIFICÓ:
    //CAUSA_MODIFICACIÓN:
    //*******************************************************************************************************
    function Crear_Tabla_Servicios_Preventivos() {
        $('#Grid_Servicios_Preventivos').datagrid({
            title: 'Listado Solicitudes de Servicio Preventivo',
            width: 790,
            height: 500,
            columns: [[
                { field: 'NO_SOLICITUD', title: 'NO_SOLICITUD', width: 80, sortable: true, align: 'center' },
                { field: 'FOLIO', title: 'Folio', width: 80, sortable: true, align: 'center' },
                { field: 'FECHA_ELABORACION', title: 'Fecha', width: 80, sortable: true, align: 'center' },
                { field: 'FECHA_RECEPCION_REAL', title: 'Fecha Recepción', width: 80, sortable: true, align: 'center' },
                { field: 'TIPO_SERVICIO', title: 'TIPO_SERVICIO', width: 80, sortable: true, align: 'center' },
                { field: 'NO_INVENTARIO', title: 'No. Inventario', width: 80, sortable: true, align: 'center' },
                { field: 'ESTATUS', title: 'Estatus', width: 80, sortable: true, align: 'center' },
                { field: 'NO_ECONOMICO', title: 'No. Ecónomico', width: 80, sortable: true, align: 'center' }
            ]],
            pageSize: 50,
            pagination: true,
            rownumbers: true,
            remoteSort: false,
            fitColumns: true,
            singleSelect: true,
            showFooter: false,
            striped: true,
            fit: true,
            loadMsg: 'Cargando los Servicios Preventivos...',
            nowrap: false
        });   // tabla final
        $('#Grid_Servicios_Preventivos').datagrid('hideColumn', 'NO_SOLICITUD');
        $('#Grid_Servicios_Preventivos').datagrid('hideColumn', 'TIPO_SERVICIO');
        $('#Grid_Servicios_Preventivos').datagrid('hideColumn', 'FECHA_RECEPCION_REAL');
    }

    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN: Crear_Tabla_Verificaciones
    //DESCRIPCIÓN: Crea el Grid para cargar el Listado.
    //PARÁMETROS:
    //CREO: Francisco Antonio Gallardo Castañeda
    //FECHA_CREO: Lunes, 05/Agosto/2013
    //MODIFICÓ:
    //FECHA_MODIFICÓ:
    //CAUSA_MODIFICACIÓN:
    //*******************************************************************************************************
    function Crear_Tabla_Verificaciones() {
        $('#Grid_Verificaciones').datagrid({
            title: 'Listado Solicitudes de Verificación',
            width: 790,
            height: 500,
            columns: [[
                { field: 'NO_SOLICITUD', title: 'NO_SOLICITUD', width: 80, sortable: true, align: 'center' },
                { field: 'FOLIO', title: 'Folio', width: 80, sortable: true, align: 'center' },
                { field: 'FECHA_ELABORACION', title: 'Fecha', width: 80, sortable: true, align: 'center' },
                { field: 'FECHA_RECEPCION_REAL', title: 'Fecha Recepción', width: 80, sortable: true, align: 'center' },
                { field: 'TIPO_SERVICIO', title: 'TIPO_SERVICIO', width: 80, sortable: true, align: 'center' },
                { field: 'NO_INVENTARIO', title: 'No. Inventario', width: 80, sortable: true, align: 'center' },
                { field: 'ESTATUS', title: 'Estatus', width: 80, sortable: true, align: 'center' },
                { field: 'NO_ECONOMICO', title: 'No. Ecónomico', width: 80, sortable: true, align: 'center' }
            ]],
            pageSize: 50,
            pagination: true,
            rownumbers: true,
            remoteSort: false,
            fitColumns: true,
            singleSelect: true,
            showFooter: false,
            striped: true,
            fit: true,
            loadMsg: 'Cargando las Verificaciones...',
            nowrap: false
        });   // tabla final
        $('#Grid_Verificaciones').datagrid('hideColumn', 'NO_SOLICITUD');
        $('#Grid_Verificaciones').datagrid('hideColumn', 'TIPO_SERVICIO');
        $('#Grid_Verificaciones').datagrid('hideColumn', 'FECHA_RECEPCION_REAL');
    }

    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN: Llenar_Grid_Solicitudes
    //DESCRIPCIÓN: Llena los Grids del Listado Principal
    //PARÁMETROS:
    //CREO: Francisco Antonio Gallardo Castañeda
    //FECHA_CREO: Lunes, 05/Agosto/2013
    //MODIFICÓ:
    //FECHA_MODIFICÓ:
    //CAUSA_MODIFICACIÓN:
    //*******************************************************************************************************
    function Llenar_Grid_Solicitudes() {
        ///Carga de Filtros para la busqueda
        var Folio = $("[id$='Txt_Folio_Solicitud_Busqueda']").val();
        var Dependencia_ID = "";
        if ($("[id$='Cmb_Unidad_Responsable_Busqueda']").val() != null) Dependencia_ID = $("[id$='Cmb_Unidad_Responsable_Busqueda']").val();
        var No_Inventario;
        if ($("[id$='Txt_Numero_Inventario_Busqueda']").val().length > 0) { No_Inventario = $("[id$='Txt_Numero_Inventario_Busqueda']").val() } else { No_Inventario = "-1"; }
        var No_Economico = $("[id$='Txt_Numero_Economico_Busqueda']").val();
        var Fecha_Inicial = $("[id$='Txt_Fecha_Recepcion_Inicial']").val();
        var Fecha_Final = $("[id$='Txt_Fecha_Recepcion_Final']").val();
        var Estatus = $("[id$='Cmb_Filtrado_Estatus']").val();
        var Tipo_Reparacion = $("[id$='Cmb_Reparacion_Busqueda']").val();
        //Llenado de Grid de Revistas Mecanicas
        $('#Grid_Revistas_Mecanicas').datagrid('loadData', { total: 0, rows: [] });
        $('#Grid_Revistas_Mecanicas').datagrid({ url: 'Frm_Ope_Tal_Controlador_Bitacora_Servicios_Vehiculo.aspx', queryParams: {
            accion: 'Consultar_Solicitudes_Servicios',
            tipo_solicitud: 'REVISTA_MECANICA',
            folio: Folio,
            dependencia_id: Dependencia_ID,
            no_inventario: No_Inventario,
            no_economico: No_Economico,
            fecha_inicial: Fecha_Inicial,
            fecha_final: Fecha_Final,
            estatus: Estatus,
            tipo_reparacion: Tipo_Reparacion
        }, pageNumber: 1
        });
        //Llenado de Grid de Servicios Correctivos
        $('#Grid_Servicios_Correctivos').datagrid('loadData', { total: 0, rows: [] });
        $('#Grid_Servicios_Correctivos').datagrid({ url: 'Frm_Ope_Tal_Controlador_Bitacora_Servicios_Vehiculo.aspx', queryParams: {
            accion: 'Consultar_Solicitudes_Servicios',
            tipo_solicitud: 'SERVICIO_CORRECTIVO',
            folio: Folio,
            dependencia_id: Dependencia_ID,
            no_inventario: No_Inventario,
            no_economico: No_Economico,
            fecha_inicial: Fecha_Inicial,
            fecha_final: Fecha_Final,
            estatus: Estatus,
            tipo_reparacion: Tipo_Reparacion
        }, pageNumber: 1
        });
        //Llenado de Grid de Servicios Preventivos
        $('#Grid_Servicios_Preventivos').datagrid('loadData', { total: 0, rows: [] });
        $('#Grid_Servicios_Preventivos').datagrid({ url: 'Frm_Ope_Tal_Controlador_Bitacora_Servicios_Vehiculo.aspx', queryParams: {
            accion: 'Consultar_Solicitudes_Servicios',
            tipo_solicitud: 'SERVICIO_PREVENTIVO',
            folio: Folio,
            dependencia_id: Dependencia_ID,
            no_inventario: No_Inventario,
            no_economico: No_Economico,
            fecha_inicial: Fecha_Inicial,
            fecha_final: Fecha_Final,
            estatus: Estatus,
            tipo_reparacion: Tipo_Reparacion
        }, pageNumber: 1
        });
        //Llenado de Grid de Verificaciones
        $('#Grid_Verificaciones').datagrid('loadData', { total: 0, rows: [] });
        $('#Grid_Verificaciones').datagrid({ url: 'Frm_Ope_Tal_Controlador_Bitacora_Servicios_Vehiculo.aspx', queryParams: {
            accion: 'Consultar_Solicitudes_Servicios',
            tipo_solicitud: 'VERIFICACION',
            folio: Folio,
            dependencia_id: Dependencia_ID,
            no_inventario: No_Inventario,
            no_economico: No_Economico,
            fecha_inicial: Fecha_Inicial,
            fecha_final: Fecha_Final,
            estatus: Estatus,
            tipo_reparacion: Tipo_Reparacion
        }, pageNumber: 1
        });
    }

    ///EVENTOS

    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN: EVENTOS 'Txt_Folio_Solicitud_Busqueda'
    //DESCRIPCIÓN: blur: Llenar el folio completo de la Solitud
    //             focus: Eliminar Ceros a la izquierda del Folio para modifcación
    //PARÁMETROS:
    //CREO:Francisco Antonio Gallardo Castañeda.
    //FECHA_CREO: Viernes, 02 Agosto 2013
    //MODIFICÓ:
    //FECHA_MODIFICÓ:
    //CAUSA_MODIFICACIÓN:
    //*******************************************************************************************************
    $("[id$='Txt_Folio_Solicitud_Busqueda']").live({ 'blur': function() {
        var Folio_Solicitud = $("[id$='Txt_Folio_Solicitud_Busqueda']").val();
        if (Folio_Solicitud.length > 0) {
            var contador;
            var completado = "";
            var restante = 10 - Folio_Solicitud.length;
            for (contador = 0; contador < restante; contador++)
                completado += "0";
            $("[id$='Txt_Folio_Solicitud_Busqueda']").val(completado + Folio_Solicitud);
        }
    }, 'focus': function() {
        var Folio_Solicitud = $("[id$='Txt_Folio_Solicitud_Busqueda']").val();
        if (Folio_Solicitud.length > 0) {
            var contador = 0;
            do {
                contador++;
            } while (Folio_Solicitud[contador] == 0);
            $("[id$='Txt_Folio_Solicitud_Busqueda']").val(Folio_Solicitud.substring(contador, 10));
        }
    }
    });

    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN: EVENTOS 'Txt_Numero_Economico_Busqueda'
    //DESCRIPCIÓN: blur: Al salir carga el numero economico completo.
    //PARÁMETROS:
    //CREO: Jesus Salvador Toledo Rodriguez
    //FECHA_CREO: Viernes, 02 Agosto 2013
    //MODIFICÓ:
    //FECHA_MODIFICÓ:
    //CAUSA_MODIFICACIÓN:
    //*******************************************************************************************************
    $("[id$='Txt_Numero_Economico_Busqueda']").live({ 'keyup': function() {
        var Str_Economico = $("[id$='Txt_Numero_Economico_Busqueda']").val();
        var Longitud_Palabra = Str_Economico.length;
        if (Longitud_Palabra > 0) {
            if (!isNaN(Str_Economico[0]))
                Str_Economico = 'U' + Str_Economico;
            else
                Str_Economico = Str_Economico.toUpperCase();
        }
        $("[id$='Txt_Numero_Economico_Busqueda']").val(Str_Economico);

    }, 'blur': function() {
        var Str_Economico = $("[id$='Txt_Numero_Economico_Busqueda']").val();
        if (Str_Economico.length > 0) {
            var Str_Economico_Copia = new Array(6);
            Str_Economico_Copia[0] = Str_Economico[0];
            Str_Economico_Copia[1] = '0';
            Str_Economico_Copia[2] = '0';
            Str_Economico_Copia[3] = '0';
            Str_Economico_Copia[4] = '0';
            Str_Economico_Copia[5] = '0';
            Str_Economico_Copia[6] = '0';
            Longitud_Palabra = Str_Economico.length;
            if (Longitud_Palabra < 6) {
                for (var i = 1; i < Longitud_Palabra; i++) {
                    Str_Economico_Copia[6 - (Longitud_Palabra - i)] = Str_Economico[i];
                }
            }
            $("[id$='Txt_Numero_Economico_Busqueda']").val(Str_Economico_Copia[0] + Str_Economico_Copia[1] + Str_Economico_Copia[2] + Str_Economico_Copia[3] + Str_Economico_Copia[4] + Str_Economico_Copia[5]);
        }
    }
    });
});
