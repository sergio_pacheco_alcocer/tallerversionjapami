﻿
var fechainicio;
var fechafinal;
var controles;

//---------------------------------------------------------------------------------------------------
$(function(){
    
    
// inicializamos fechas
$("[id$='txt_fecha_incio']").datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'dd/mm/yy'
});


$("[id$='txt_fecha_final']").datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'dd/mm/yy'
});


crearTablas();


// registro de evenos
     
      $("[id$='Btn_Buscar']").click(function(event) {
        obtenerInformacion();
        event.preventDefault();
    });

     $("[id$='Btn_exportar_excel']").click(function(event) {
        exportarexcel();
        event.preventDefault();
    });


    $("[id$='Btn_Salir']").click(function(event) {
        event.preventDefault();
    });

});


//--------------------------------------------------------------------------------------------------

function crearTablas(){

  $('#tabla_bonificaciones').datagrid({  // tabla inicio
        title: 'Detallado de Ajustes y Bonificaiones',
        width: 850,
        height: 300,
        columns: [[
        { field: 'movimiento', title: 'Movimiento', width: 300 },
        { field: 'cantidad', title: 'Cantidad', width: 150 },
        { field: 'ajuste', title: 'Ajuste', width: 150 },
         { field: 'bonificacion', title: 'Bonificaciones', width: 150 }            
    ]],
        pageSize: 10,
        pageList: [30],
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        striped: true,
        showFooter:true,
        loadMsg: 'cargando...',
        nowrap: false
   

    });          // tabla final


}

//--------------------------------------------------------------------------------------------------

function obtenerInformacion(){
 
  $('#tabla_bonificaciones').datagrid('loadData', { total: 0, rows: [] });  
  $('#tabla_bonificaciones').datagrid('reloadFooter',{ rows: [] });
  
  
  if (validarFechas($("[id$='txt_fecha_incio']"), $("[id$='txt_fecha_final']"))) {
      return;
    }
    controles= toJSON($('#datos :input').serializeArray()); 
    $('#tabla_bonificaciones').datagrid({ url: '../../Controladores/frm_controlador_reportes.aspx', queryParams: {
         accion: 'reporte_obtener_bonificacion_ajuste_detallado',
         controles:controles
     }, pageNumber: 1
     });
   
}

//--------------------------------------------------------------------------------------------------
function exportarexcel(){

var renglones;

renglones=$('#tabla_bonificaciones').datagrid('getRows');
  if (renglones.length == 0) {
        $.messager.alert('Reportes', 'No hay Datos que mandar a excel');
        return;
    }
    
   window.location = "../../Reporte/Frm_controlador_excel.aspx?controles=" + controles + "&accion=reporte_obtener_bonificacion_ajuste_detallado" ;


}
