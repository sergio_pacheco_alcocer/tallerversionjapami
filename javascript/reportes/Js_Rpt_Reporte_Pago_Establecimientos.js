﻿
var controles;

$(function (){

// inicializamos fechas
$("[id$='txt_fecha_inicio']").datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'dd/mm/yy'
});


$("[id$='txt_fecha_final']").datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'dd/mm/yy'
 });

crearTabla();

// evento combo
$("[id$='cmb_institucion']").change(function (event) {
    var establecimiento_id;       
     establecimiento_id=$("[id$='cmb_institucion']").val();
     
     if(establecimiento_id != -1 ){
         obtenerSucursales(establecimiento_id);
     }else{
         $("[id$='cmb_sucursal']").get(0).selectedIndex =0;
     } 
}); // fin




});



//--------------------------------------------------------------------------------------------------

function crearTabla(){

    $('#tabla_pago').datagrid({  // tabla inicio
        title: 'Pagos Establecimientos',
        width: 800,
        height: 300,
        columns: [[
        { field:'institucion', title: 'Institucion', width: 200 },
        { field:'sucursal', title: 'Sucursal', width: 150 },
        { field:'monto', title: 'Monto', width: 150 },
        { field:'cuenta', title: 'Cuenta', width: 100 },
        { field:'fecha_pago', title: 'Fecha Pago', width: 150 },            
        { field:'fecha_creo', title: 'Fecha Creo', width: 150 },
        { field:'estado', title: 'Estado', width: 150 }                          
    ]],
        pageSize: 10,
        pageList: [30,60,90],
        pagination: true,
        rownumbers: true,
        remoteSort: false,
        fitColumns:false,
        singleSelect: true,
        striped: true,
        showFooter:true,
        loadMsg: 'cargando...',
        nowrap: false
   

    });          // tabla final

}



//--------------------------------------------------------------------------------------------------

function obtenerSucursales(establecimiento_id){

 $('#ventana_mensaje').window('open'); 

 $.ajax({ //inicio
             type: "POST",
             url: "../../Controladores/frm_controlador_reportes.aspx/obtenerSucursales",
             data: "{'establecimiento_id':'" + establecimiento_id + "'}",
             contentType: "application/json; charset=utf-8",
             dataType: "json",
             success: function(response) {
                 $('#ventana_mensaje').window('close'); 
                 var respuesta = eval("(" + response.d + ")");   
                 procesoExitosoSucursal(respuesta);              
             },
             error: function(result) {
                 $('#ventana_mensaje').window('close');
                 alert("ERROR " + result.status + ' ' + result.statusText);

             }


  }); // fin



}




function procesoExitosoSucursal( respuesta){

   //Borramos todas las opciones del combo 
 $("[id$='cmb_sucursal']").find('option').remove().end(); 
 var contador;
   
  contador=0; 
   
  var options = '<option value="-1"><-SELECCIONAR-></option>';
  if (respuesta.length>0) { 
          $.each(respuesta, function (i, item) {
            contador++;
            options += '<option value="' + item.sucursal + '">' + item.sucursal + '</option>';
           });       
        }else{
           $.messager.alert('Japami','Se encontro en información');
        }
        
 $("[id$='cmb_sucursal']").append(options);


}


//--------------------------------------------------------------------------------------------------

function agregarDatos(nombre_control,valor_asignar,arreglo_datos){

    if(arreglo_datos[nombre_control] !== undefined){
              if (!arreglo_datos[nombre_control].push){
                  arreglo_datos[nombre_control]=[arreglo_datos[nombre_control]];          
              }
              arreglo_datos[nombre_control].push(valor_asignar || '');
            }else {
                arreglo_datos[nombre_control]=valor_asignar || '';
            }
            

}


//--------------------------------------------------------------------------------------------------
function buscarPagosEstablecimiento(){

    var datos={};
     
     
     $('#tabla_pago').datagrid('loadData', { total: 0, rows: [] });
     $('#tabla_pago').datagrid('reloadFooter',{ rows: [] });
  
     
    if (validarFechas($("[id$='txt_fecha_inicio']"), $("[id$='txt_fecha_final']"))) {
      return;
    }
    
    if(validar_check_box_combos("datos",datos)){
     return ;
    }
    
    agregarDatos("txt_fecha_inicio",$("[id$='txt_fecha_inicio']").val(),datos);
    agregarDatos("txt_fecha_final",$("[id$='txt_fecha_final']").val(),datos);
    
    //controles= toJSON($('#datos :input').serializeArray());
     controles= toJSON(datos);
    
    $('#tabla_pago').datagrid({ url: '../../Controladores/frm_controlador_reportes.aspx', queryParams: {
         accion: 'reporte_pagos_establecimiento',
         controles:controles
     }, pageNumber: 1
     });
    
    
     

}

//----------------------------------------------------------------------------------------------

function exportarExcel(){

var renglones;

renglones=$('#tabla_pago').datagrid('getRows');

  if (renglones.length == 0) {
        $.messager.alert('Reportes', 'No hay Datos que mandar a excel');
        return;
    }
      
 window.location = "../../Reporte/Frm_controlador_excel.aspx?controles=" + controles + "&accion=reporte_pagos_establecimiento" ;


}
