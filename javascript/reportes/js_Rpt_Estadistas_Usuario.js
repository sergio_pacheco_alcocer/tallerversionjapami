﻿

$(function(){
   crearTablaZonas();
   crearTablagiros();
   crearTablaTarifa();
   crearTablaRegion();
   crearTablaSector();

   
   // obtener los datos
   
      $('#tabla_zonas').datagrid({ url: '../../Controladores/frm_controlador_reportes.aspx', queryParams: {
         accion: 'reporte_zona_estatisticas'
     }, pageNumber: 1
     });
   
     $('#tabla_tarifas').datagrid({ url: '../../Controladores/frm_controlador_reportes.aspx', queryParams: {
         accion: 'reporte_tarifa_estadistica'
     }, pageNumber: 1
     });
   
      $('#tabla_giros').datagrid({ url: '../../Controladores/frm_controlador_reportes.aspx', queryParams: {
         accion: 'reporte_giro_estadistica'
     }, pageNumber: 1
     });
     
      $('#tabla_region').datagrid({ url: '../../Controladores/frm_controlador_reportes.aspx', queryParams: {
         accion: 'reporte_regiones_estadistica'
     }, pageNumber: 1
     });
     
       $('#tabla_sector').datagrid({ url: '../../Controladores/frm_controlador_reportes.aspx', queryParams: {
         accion: 'reporte_sectores_estadistica'
     }, pageNumber: 1
     });
   

});

//-----------------------------------------------------------------------------------------------------

function crearTablaZonas(){
  
  $('#tabla_zonas').datagrid({  // tabla inicio
        title: 'Usuarios por Zona',
        width: 600,
        height: 150,
        columns: [[
        { field: 'concepto', title:'Zona', width: 250 },
        { field: 'usuario', title: 'Cantidad', width: 150 },
        { field: 'porcentaje', title: 'Porcentaje', width: 150 }           
    ]],
        pageSize: 10,
        pageList: [30],
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        striped: true,
        showFooter:true,
        loadMsg: 'cargando...',
        nowrap: false
   

    });          // tabla final
  
  
}

//-----------------------------------------------------------------------------------------------------

function crearTablagiros(){
  
  $('#tabla_giros').datagrid({  // tabla inicio
        title: 'Usuarios por Giro',
        width: 600,
        height: 150,
        columns: [[
        { field: 'concepto', title:'Giros', width: 250 },
        { field: 'usuario', title: 'Cantidad', width: 150 },
        { field: 'porcentaje', title: 'Porcentaje', width: 150 }           
    ]],
        pageSize: 10,
        pageList: [30],
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        striped: true,
        showFooter:true,
        loadMsg: 'cargando...',
        nowrap: false
   

    });          // tabla final
  
  
}

//-----------------------------------------------------------------------------------------------------

function crearTablaTarifa(){
  
  $('#tabla_tarifas').datagrid({  // tabla inicio
        title: 'Usuarios por Tarifa',
        width: 600,
        height: 150,
        columns: [[
        { field: 'concepto', title:'Tarifa', width: 250 },
        { field: 'usuario', title: 'Cantidad', width: 150 },
        { field: 'porcentaje', title: 'Porcentaje', width: 150 }           
    ]],
        pageSize: 10,
        pageList: [30],
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        striped: true,
        showFooter:true,
        loadMsg: 'cargando...',
        nowrap: false
   

    });          // tabla final
  
  
}

//------------------------------------------------------------------------------------------------

function crearTablaRegion(){
  
  $('#tabla_region').datagrid({  // tabla inicio
        title: 'Usuarios por Region',
        width: 600,
        height: 150,
        columns: [[
        { field: 'concepto', title:'Region', width: 250 },
        { field: 'usuario', title: 'Cantidad', width: 150 },
        { field: 'porcentaje', title: 'Porcentaje', width: 150 }           
    ]],
        pageSize: 10,
        pageList: [30],
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        striped: true,
        showFooter:true,
        loadMsg: 'cargando...',
        nowrap: false
   

    });          // tabla final
  
  
}

//----------------------------------------------------------------------------------------------


function crearTablaSector(){
  
  $('#tabla_sector').datagrid({  // tabla inicio
        title: 'Usuarios por Sector',
        width: 600,
        height: 150,
        columns: [[
        { field: 'concepto', title:'Sector', width: 250 },
        { field: 'usuario', title: 'Cantidad', width: 150 },
        { field: 'porcentaje', title: 'Porcentaje', width: 150 }           
    ]],
        pageSize: 10,
        pageList: [30],
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        striped: true,
        showFooter:true,
        loadMsg: 'cargando...',
        nowrap: false
   

    });          // tabla final
  
  
}


//----------------------------------------------------------------------------------------------
function enviarExcel(){
 window.location = "../../Reporte/Frm_controlador_excel.aspx?accion=estadisticas_usuario" ;


}