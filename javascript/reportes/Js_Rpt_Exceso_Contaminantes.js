﻿
var objEmpresa;
var controles;

$(function(){
    
    
    
// registro de evenos    
      $("[id$='Btn_Buscar']").click(function(event) {
        buscarInformacion();
        event.preventDefault();
    });

     $("[id$='Btn_exportar_pdf']").click(function(event) {
        exportarPDF();
        event.preventDefault();
    });


    $("[id$='Btn_Salir']").click(function(event) {
        event.preventDefault();
    });

    
    autocompletarUsuarios();   
    crearTablaContaminantes();
    
    
});

//--------------------------------------------------



function format(item) {
    return item.usuario ;
}


function autocompletarUsuarios(){

 //inciamos el auto completado
    $("[id$='txt_nombre']").autocomplete("../../Controladores/frm_controlador_reportes.aspx", {
        extraParams: { accion: 'autoCompletarEmpresasExcesoContaminantes' },
        dataType: "json",
        parse: function(data) {
            return $.map(data, function(row) {
                return {
                    data: row,
                    value: row.usuario_id,
                    result: row.usuario

                }
            });
        },
        formatItem: function(item) {
            return format(item);
        }
    }).result(function(e, item) {
          objEmpresa=item; 
      });


}

//-------------------------------------------------------------------------------

function agregarDatos(nombre_control,valor_asignar,arreglo_datos){

    if(arreglo_datos[nombre_control] !== undefined){
              if (!arreglo_datos[nombre_control].push){
                  arreglo_datos[nombre_control]=[arreglo_datos[nombre_control]];          
              }
              arreglo_datos[nombre_control].push(valor_asignar || '');
            }else {
                arreglo_datos[nombre_control]=valor_asignar || '';
            }
            
}

//---------------------------------------------------------------------



function crearTablaContaminantes(){

  $('#tabla_contaminantes').datagrid({  // tabla inicio
        title: 'Exceso de Contaminantes',
        width: 790,
        height: 400,
        columns: [[
        { field: 'no_muestra', title: 'No. muestra', width:100 },
        { field: 'periodo', title: 'Periodo', width: 100 },
        { field: 'no_cuenta', title: 'No.Cuenta', width: 100 },
        { field: 'usuario', title: 'Usuario', width: 250 },
        { field: 'estatus_usuario', title: 'Estado U.', width: 150 },
        { field: 'consumo1', title: 'Consumo 1', width: 150 },
        { field: 'consumo2', title: 'Consumo 2', width: 150 },
        { field: 'consumo3', title: 'Consumo 3', width: 150 },
        { field: 'ph', title: 'PH', width: 150 },
        { field: 'sst', title: 'SST', width: 150 }, 
        { field: 'dqo', title: 'DQO', width: 150 } ,
        { field: 'ga', title: 'GA', width: 150 } ,
        { field: 'ph_dinero', title: 'PH($)', width: 150 }, 
        { field: 'sst_dinero', title: 'SST($)', width: 150 }, 
        { field: 'dqo_dinero', title: 'DQO($)', width: 150 } ,
        { field: 'excesos', title: 'Exceso', width: 150 }, 
        { field: 'analisis_fq', title: 'Analisis', width: 150 },
        { field: 'laboratorio', title: 'Laboratorio', width: 150 },  
        { field: 'factura', title: 'Facturada', width: 150 } 
                          
    ]],
        pageSize: 30,
        pageList: [30,60,90],
        pagination: true,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        striped: true,
        showFooter:true,
        loadMsg: 'cargando...',
        nowrap: false
                
   }); 
   
}

//-----------------------------------------------------------------

function agregarDatos(nombre_control,valor_asignar,arreglo_datos){

    if(arreglo_datos[nombre_control] !== undefined){
              if (!arreglo_datos[nombre_control].push){
                  arreglo_datos[nombre_control]=[arreglo_datos[nombre_control]];          
              }
              arreglo_datos[nombre_control].push(valor_asignar || '');
            }else {
                arreglo_datos[nombre_control]=valor_asignar || '';
            }
            

}



//------------------------------------------------------------------

function buscarInformacion(){

var datos={};
var year_inicio;
var year_final;


$('#tabla_contaminantes').datagrid('loadData',{total:0, rows:[]});

if( valoresRequeridosGeneral("datos")==false){
    $.messager.alert('Japami','Se requieres algunos valores');
   return ;
}

if(validar_check_box_cajas_textos("datos",datos)){
     return ;
 }   
 
 year_inicio= parseInt($("[id$='txt_fecha_inicio']").val());
 year_final= parseInt($("[id$='txt_fecha_final']").val());
 
 if(year_inicio> year_final){
   $.messager.alert('Japami','error al seleccionar los años');
   return;
 }
  
 agregarDatos("txt_fecha_inicio",year_inicio,datos);
 agregarDatos("txt_fecha_final",year_final,datos);
    
 controles= toJSON(datos);
 
   $('#tabla_contaminantes').datagrid({ url: '../../Controladores/frm_controlador_reportes.aspx', queryParams: {
         accion: 'reporteExcesoContaminantes',
         controles:controles
     }, pageNumber: 1
     });
 

}

//------------------------------------------------------------------



function exportarPDF(){

var renglones;

renglones=$('#tabla_contaminantes').datagrid('getRows');

  if (renglones.length == 0) {
        $.messager.alert('Reportes', 'No hay Datos que mandar a pdf');
        return;
    }
      
 window.open("../../Reporte/frm_controlador_crystal.aspx?nombre_reporte=reporteExcesoContaminantes&controles=" + controles ,"Descargas","width=400px,height=400px,scrollbars=NO");

}

//-------------------------------------------------

