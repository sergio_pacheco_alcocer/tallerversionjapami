﻿
var controles;

$(function(){

  // inicializamos fechas
$("[id$='txt_fecha_inicio']").datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'dd/mm/yy'
});


$("[id$='txt_fecha_final']").datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'dd/mm/yy'
});



// registro de evenos
     
      $("[id$='Btn_Buscar']").click(function(event) {
        buscarInformacion();
        event.preventDefault();
    });

     $("[id$='Btn_exportar_excel']").click(function(event) {
        exportarExcel();
        event.preventDefault();
    });


    $("[id$='Btn_Salir']").click(function(event) {
        event.preventDefault();
    });

   //--------------cargas componentes

   crearTablas();

});


//--------------------------------------------------------------------------



function crearTablas(){

  $('#tabla_carga_trabajo').datagrid({  // tabla inicio
        title: 'Carga de Trabajo',
        width: 820,
        height: 300,
        columns: [[
        { field: 'clave_brigada', title: 'Cve. Brigada', width: 150 },
        { field: 'nombre_brigada', title: 'Nombre Brigada', width: 200 },
        { field: 'folio', title: 'Folio', width: 150 },
        { field: 'actividad', title: 'Actividad', width: 200 },
        { field: 'fecha_termino', title: 'F. Termino', width: 150 },
        { field: 'hora_termino', title: 'H. Termino', width: 150 },
        { field: 'estado', title: 'Estatus', width: 150 }              
    ]],
        pageSize: 10,
        pageList: [30],
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        striped: true,
        showFooter:true,
        loadMsg: 'cargando...',
        nowrap: false
   

    });          // tabla final


}



//--------------------------------------------------------------------------



function agregarDatos(nombre_control,valor_asignar,arreglo_datos){

    if(arreglo_datos[nombre_control] !== undefined){
              if (!arreglo_datos[nombre_control].push){
                  arreglo_datos[nombre_control]=[arreglo_datos[nombre_control]];          
              }
              arreglo_datos[nombre_control].push(valor_asignar || '');
            }else {
                arreglo_datos[nombre_control]=valor_asignar || '';
            }
            

}



//--------------------------------------------------------------------------

function buscarInformacion(){

var datos={};


$('#tabla_carga_trabajo').datagrid('loadData', { total: 0, rows: [] });
$('#tabla_carga_trabajo').datagrid('reloadFooter',{ rows: [] });

if (validarFechas($("[id$='txt_fecha_inicio']"), $("[id$='txt_fecha_final']"))) {
      return;
    }
    
 if(validar_check_box_combos("datos",datos)){
     return ;
    }    
    
    agregarDatos("txt_fecha_inicio",$("[id$='txt_fecha_inicio']").val(),datos);
    agregarDatos("txt_fecha_final",$("[id$='txt_fecha_final']").val(),datos);
    
      controles= toJSON(datos);
    
    $('#tabla_carga_trabajo').datagrid({ url: '../../Controladores/frm_controlador_reportes.aspx', queryParams: {
         accion: 'obtenerCargaTrabajo',
         controles:controles
     }, pageNumber: 1
     });

}

//-------------------------------------------------------------------------------------------------


function exportarExcel(){

var renglones;

renglones=$('#tabla_carga_trabajo').datagrid('getRows');

  if (renglones.length == 0) {
        $.messager.alert('Reportes', 'No hay Datos que mandar a excel');
        return;
    }
      
 window.location = "../../Reporte/Frm_controlador_excel.aspx?controles=" + controles + "&accion=reporte_carga_trabajo" ;


}


