﻿

var objEmpresa;

$(function(){


  // inicializamos fechas
$("[id$='txt_fecha_inicio']").datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'dd/mm/yy'
});


$("[id$='txt_fecha_final']").datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'dd/mm/yy'
});



// registro de evenos    
      $("[id$='Btn_Buscar']").click(function(event) {
        buscarInformacion();
        event.preventDefault();
    });

     $("[id$='Btn_exportar_excel']").click(function(event) {
        exportarExcel();
        event.preventDefault();
    });


    $("[id$='Btn_Salir']").click(function(event) {
        event.preventDefault();
    });

   //componentes
   auto_completado_empresas(); 
   crearTablasPagosPortatiles();
   crearTablaDescargaDetalles();

});

//-----------------------------------------------------------------------------------------

function format(item) {
    return item.nombre ;
}

function auto_completado_empresas() { 

//inciamos el auto completado
    $("[id$='txt_nombre']").autocomplete("../../Controladores/frm_controlador_reportes.aspx", {
        extraParams: { accion: 'autoCompletarEmpresas' },
        dataType: "json",
        parse: function(data) {
            return $.map(data, function(row) {
                return {
                    data: row,
                    value: 1,
                    result: row.nombre

                }
            });
        },
        formatItem: function(item) {
            return format(item);
        }
    }).result(function(e, item) {
          objEmpresa=item; 
      });

}

//---------------------------------------------------------------------------------

function crearTablasPagosPortatiles(){

  $('#tabla_pagos_encabezado').datagrid({  // tabla inicio
        title: 'Pagos Descargas Baños Portatiles',
        width: 790,
        height: 300,
        columns: [[
        { field: 'no_diverso', title: 'no_diverso', width: 0 },
        { field: 'empresa', title: 'Empresa', width: 200 },
        { field: 'metros_cubicos', title: 'M.cubicos', width: 150 },
        { field: 'total_pesos', title: 'Total', width: 150 },
        { field: 'saldo_metros_cubicos', title: 'Saldo m.cubicos', width: 150 },
        { field: 'saldo_pesos', title: 'Saldo total', width: 150 },
        { field: 'fecha_creacion', title: 'F.Creacion', width: 150 },
        { field: 'codigo_barras', title: 'Codigo', width: 150 }               
    ]],
        pageSize: 30,
        pageList: [30,60,90],
        pagination: true,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        striped: true,
        showFooter:true,
        loadMsg: 'cargando...',
        nowrap: false,
        onClickRow:function(index,datos){
             
           $('#tabla_pagos_descargas').datagrid({ url: '../../Controladores/frm_controlador_reportes.aspx', queryParams: {
              accion: 'obtenerPagosDescargasDetallado',
              no_diverso:datos.no_diverso
              }, pageNumber: 1
            });

        }
   

    });          // tabla final

    
    $('#tabla_pagos_encabezado').datagrid('hideColumn','no_diverso');
    
}

//-------------------------------------------------------------------------------

function crearTablaDescargaDetalles(){

  $('#tabla_pagos_descargas').datagrid({  // tabla inicio
        title: 'Pagos Descargas Baños Portatiles Detalles',
        width: 790,
        height: 300,
        columns: [[
        { field: 'fecha_descarga', title: 'Fecha Descarga', width:150 },
        { field: 'cantidad_descarga_metros', title: 'Metros Cubicos', width: 200 },
        { field: 'costo', title: 'Costo ', width: 150 },
        { field: 'precio_m', title: 'Precio m ', width: 150 },
        { field: 'tarifa', title: 'Tarifa ', width: 150 }          
    ]],
        pageSize: 30,
        pageList: [30,60,90],
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        striped: true,
        showFooter:true,
        loadMsg: 'cargando...',
        nowrap: false
   

    });          // tabla final


}

//-------------------------------------------------------------------------------

function agregarDatos(nombre_control,valor_asignar,arreglo_datos){

    if(arreglo_datos[nombre_control] !== undefined){
              if (!arreglo_datos[nombre_control].push){
                  arreglo_datos[nombre_control]=[arreglo_datos[nombre_control]];          
              }
              arreglo_datos[nombre_control].push(valor_asignar || '');
            }else {
                arreglo_datos[nombre_control]=valor_asignar || '';
            }
            

}


//-------------------------------------------------------------------------------
function buscarInformacion(){

var datos={};


$('#tabla_pagos_encabezado').datagrid('loadData', { total: 0, rows: [] });
$('#tabla_pagos_encabezado').datagrid('reloadFooter',{ rows: [] });

$('#tabla_pagos_descargas').datagrid('loadData',{total:0, rows:[]});
$('#tabla_pagos_descargas').datagrid('reloadFooter',{ rows: [] });

if (validarFechas($("[id$='txt_fecha_inicio']"), $("[id$='txt_fecha_final']"))) {
      return;
    }
    
 if(validar_check_box_cajas_textos("datos",datos)){
     return ;
 }   
    
   
    agregarDatos("txt_fecha_inicio",$("[id$='txt_fecha_inicio']").val(),datos);
    agregarDatos("txt_fecha_final",$("[id$='txt_fecha_final']").val(),datos);
    
    
    controles= toJSON(datos);
    
    $('#tabla_pagos_encabezado').datagrid({ url: '../../Controladores/frm_controlador_reportes.aspx', queryParams: {
         accion: 'obtenerPagosDescargasEncabezado',
         controles:controles
     }, pageNumber: 1
     });



}

//------------------------------------------------------------------------------------------------------------



function exportarExcel(){

var renglones;

renglones=$('#tabla_pagos_encabezado').datagrid('getRows');

  if (renglones.length == 0) {
        $.messager.alert('Reportes', 'No hay Datos que mandar a excel');
        return;
    }
      
 window.location = "../../Reporte/Frm_controlador_excel.aspx?controles=" + controles + "&accion=reporte_descargas_industriales" ;


}