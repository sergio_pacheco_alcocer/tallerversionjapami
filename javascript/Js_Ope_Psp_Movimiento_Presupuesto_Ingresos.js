﻿///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Sumar
///DESCRIPCIÓN          : Funcion para sumar los importes de los meses y obtener el total
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 18/Abril/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///***********************************************************************************************************************
function Sumar()
{
    var Total=parseFloat("0.00");
    if($('input[id$=Txt_Enero]').val() != "" && $('input[id$=Txt_Enero]').val() != "NaN"){
        Total = Total + parseFloat($('input[id$=Txt_Enero]').val().replace(/,/gi,''));
    }
    if($('input[id$=Txt_Febrero]').val() != "" && $('input[id$=Txt_Febrero]').val() != "NaN"){
        Total = Total + parseFloat($('input[id$=Txt_Febrero]').val().replace(/,/gi,''));
    }
     if($('input[id$=Txt_Marzo]').val() != "" && $('input[id$=Txt_Marzo]').val() != "NaN"){
         Total = Total + parseFloat($('input[id$=Txt_Marzo]').val().replace(/,/gi,''));
    }
     if($('input[id$=Txt_Abril]').val() != "" && $('input[id$=Txt_Abril]').val() != "NaN"){
        Total = Total + parseFloat($('input[id$=Txt_Abril]').val().replace(/,/gi,''));
    }
     if($('input[id$=Txt_Mayo]').val() != "" && $('input[id$=Txt_Mayo]').val() != "NaN"){
        Total = Total + parseFloat($('input[id$=Txt_Mayo]').val().replace(/,/gi,''));
    }
     if($('input[id$=Txt_Junio]').val() != "" && $('input[id$=Txt_Junio]').val() != "NaN"){
        Total = Total + parseFloat($('input[id$=Txt_Junio]').val().replace(/,/gi,''));
    }
     if($('input[id$=Txt_Julio]').val() != "" && $('input[id$=Txt_Julio]').val() != "NaN"){
        Total = Total + parseFloat($('input[id$=Txt_Julio]').val().replace(/,/gi,''));
    }   
     if($('input[id$=Txt_Agosto]').val() != "" && $('input[id$=Txt_Agosto]').val() != "NaN"){
        Total = Total + parseFloat($('input[id$=Txt_Agosto]').val().replace(/,/gi,''));
    }
     if($('input[id$=Txt_Septiembre]').val() != "" && $('input[id$=Txt_Septiembre]').val() != "NaN"){
        Total = Total + parseFloat($('input[id$=Txt_Septiembre]').val().replace(/,/gi,''));
    }
     if($('input[id$=Txt_Octubre]').val() != "" && $('input[id$=Txt_Octubre]').val() != "NaN"){
        Total = Total + parseFloat($('input[id$=Txt_Octubre]').val().replace(/,/gi,''));
    }
     if($('input[id$=Txt_Noviembre]').val() != "" && $('input[id$=Txt_Noviembre]').val() != "NaN"){
        Total = Total + parseFloat($('input[id$=Txt_Noviembre]').val().replace(/,/gi,''));
    }
     if($('input[id$=Txt_Diciembre]').val() != "" && $('input[id$=Txt_Diciembre]').val() != "NaN"){
        Total = Total + parseFloat($('input[id$=Txt_Diciembre]').val().replace(/,/gi,''));
    }
    
    $('input[id$=Txt_Total]').val(Total);
    $('input[id$=Txt_Total]').formatCurrency({colorize:true, region: 'es-MX'});
    
    $('input[id$=Txt_Importe]').val(Total);
    $('input[id$=Txt_Importe]').formatCurrency({colorize:true, region: 'es-MX'});
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Validar
///DESCRIPCIÓN          : Funcion para validar los importes de los meses con el disponible del mes
///PARAMETROS           1 Mes: numero del mes que validaremos 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 018/Abril/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///***********************************************************************************************************************
function Validar(Mes)
{
    var Error = "";
    var Importe = 0.00;
    var Disponible = 0.00;
    var Importe_Mes = 0.00;
    var Importe_Disp_Mes = 0.00;
    var Total;
    var Tipo_Operacion = $("select[id$=Cmb_Operacion] option:selected").val();
    var Tipo_Concepto = $('[id$=Hf_Tipo_Concepto]').val();
    
    $('[id$=Lbl_Validacion]').text("");
    
    if(Tipo_Operacion == "REDUCCION")
    {
        if($('[id$=Hf_Modificado]').val() != ""  && $('[id$=Hf_Modificado]').val() != "NaN"){
            Disponible = parseFloat($('[id$=Hf_Modificado]').val().replace(/,/gi,''));
            if(Importe > Disponible)
            {
                Error = "* El Importe no puede ser mayor al Modificado.   ";
                $('input[id$=Txt_Importe]').val("0.00");
            }
         }
    
        switch(Mes)
        {
            case 1:
                if($('input[id$=Txt_Enero]').val() != "" && $('input[id$=Txt_Enero]').val() != "NaN"){
                    Importe_Mes = parseFloat($('input[id$=Txt_Enero]').val().replace(/,/gi,''));
                }
                if($('[id$=Lbl_Disp_Ene]').text() != "" && $('[id$=Lbl_Disp_Ene]').text() != "NaN"){
                    Importe_Disp_Mes = parseFloat($('[id$=Lbl_Disp_Ene]').text().replace(/,/gi,''));
                }
                if(Importe_Mes > Importe_Disp_Mes){
                    Error += " * El Importe del mes de Enero no puede ser mayor al disponible.";
                    $('input[id$=Txt_Enero]').val("0.00");
                }
            break;
            case 2:
                if($('input[id$=Txt_Febrero]').val() != "" && $('input[id$=Txt_Febrero]').val() != "NaN"){
                    Importe_Mes = parseFloat($('input[id$=Txt_Febrero]').val().replace(/,/gi,''));
                }
                if($('[id$=Lbl_Disp_Feb]').text() != "" && $('[id$=Lbl_Disp_Feb]').text() != "NaN"){
                    Importe_Disp_Mes = parseFloat($('[id$=Lbl_Disp_Feb]').text().replace(/,/gi,''));
                }
                if(Importe_Mes > Importe_Disp_Mes){
                    Error += " * El Importe del mes de Febrero no puede ser mayor al disponible.";
                    $('input[id$=Txt_Febrero]').val("0.00");
                }
            break;
            case 3:
                if($('input[id$=Txt_Marzo]').val() != "" && $('input[id$=Txt_Marzo]').val() != "NaN"){
                    Importe_Mes = parseFloat($('input[id$=Txt_Marzo]').val().replace(/,/gi,''));
                }
                if($('[id$=Lbl_Disp_Mar]').text() != "" && $('[id$=Lbl_Disp_Mar]').text() != "NaN"){
                    Importe_Disp_Mes = parseFloat($('[id$=Lbl_Disp_Mar]').text().replace(/,/gi,''));
                }
                if(Importe_Mes > Importe_Disp_Mes){
                    Error += " * El Importe del mes de Marzo no puede ser mayor al disponible.";
                    $('input[id$=Txt_Marzo]').val("0.00");
                }
            break;
            case 4:
                if($('input[id$=Txt_Abril]').val() != "" && $('input[id$=Txt_Abril]').val() != "NaN"){
                    Importe_Mes = parseFloat($('input[id$=Txt_Abril]').val().replace(/,/gi,''));
                }
                if($('[id$=Lbl_Disp_Abr]').text() != "" && $('[id$=Lbl_Disp_Abr]').text() != "NaN"){
                    Importe_Disp_Mes = parseFloat($('[id$=Lbl_Disp_Abr]').text().replace(/,/gi,''));
                }
                if(Importe_Mes > Importe_Disp_Mes){
                    Error += " * El Importe del mes de Abril no puede ser mayor al disponible.";
                    $('input[id$=Txt_Abril]').val("0.00");
                }
            break;
            case 5:
                if($('input[id$=Txt_Mayo]').val() != "" && $('input[id$=Txt_Mayo]').val() != "NaN"){
                    Importe_Mes = parseFloat($('input[id$=Txt_Mayo]').val().replace(/,/gi,''));
                }
                if($('[id$=Lbl_Disp_May]').text() != "" && $('[id$=Lbl_Disp_May]').text() != "NaN"){
                    Importe_Disp_Mes = parseFloat($('[id$=Lbl_Disp_May]').text().replace(/,/gi,''));
                }
                if(Importe_Mes > Importe_Disp_Mes){
                    Error += " * El Importe del mes de Mayo no puede ser mayor al disponible.";
                    $('input[id$=Txt_Mayo]').val("0.00");
                }
            break;
            case 6:
                if($('input[id$=Txt_Junio]').val() != "" && $('input[id$=Txt_Junio]').val() != "NaN"){
                    Importe_Mes = parseFloat($('input[id$=Txt_Junio]').val().replace(/,/gi,''));
                }
                if($('[id$=Lbl_Disp_Jun]').text() != "" && $('[id$=Lbl_Disp_Jun]').text() != "NaN"){
                    Importe_Disp_Mes = parseFloat($('[id$=Lbl_Disp_Jun]').text().replace(/,/gi,''));
                }
                if(Importe_Mes > Importe_Disp_Mes){
                    Error += " * El Importe del mes de Junio no puede ser mayor al disponible.";
                    $('input[id$=Txt_Junio]').val("0.00");
                }
            break;
            case 7:
                if($('input[id$=Txt_Julio]').val() != "" && $('input[id$=Txt_Julio]').val() != "NaN"){
                    Importe_Mes = parseFloat($('input[id$=Txt_Julio]').val().replace(/,/gi,''));
                }
                if($('[id$=Lbl_Disp_Jul]').text() != "" && $('[id$=Lbl_Disp_Jul]').text() != "NaN"){
                    Importe_Disp_Mes = parseFloat($('[id$=Lbl_Disp_Jul]').text().replace(/,/gi,''));
                }
                if(Importe_Mes > Importe_Disp_Mes){
                    Error += " * El Importe del mes de Julio no puede ser mayor al disponible.";
                    $('input[id$=Txt_Julio]').val("0.00");
                }
            break;
            case 8:
                if($('input[id$=Txt_Agosto]').val() != "" && $('input[id$=Txt_Agosto]').val() != "NaN"){
                    Importe_Mes = parseFloat($('input[id$=Txt_Agosto]').val().replace(/,/gi,''));
                }
                if($('[id$=Lbl_Disp_Ago]').text() != "" && $('[id$=Lbl_Disp_Ago]').text() != "NaN"){
                    Importe_Disp_Mes = parseFloat($('[id$=Lbl_Disp_Ago]').text().replace(/,/gi,''));
                }
                if(Importe_Mes > Importe_Disp_Mes){
                    Error += " * El Importe del mes de Agosto no puede ser mayor al disponible.";
                    $('input[id$=Txt_Agosto]').val("0.00");
                }
            break;
            case 9:
                if($('input[id$=Txt_Septiembre]').val() != "" && $('input[id$=Txt_Septiembre]').val() != "NaN"){
                    Importe_Mes = parseFloat($('input[id$=Txt_Septiembre]').val().replace(/,/gi,''));
                }
                if($('[id$=Lbl_Disp_Sep]').text() != "" && $('[id$=Lbl_Disp_Sep]').text() != "NaN"){
                    Importe_Disp_Mes = parseFloat($('[id$=Lbl_Disp_Sep]').text().replace(/,/gi,''));
                }
                if(Importe_Mes > Importe_Disp_Mes){
                     Error += " * El Importe del mes de Septiembre no puede ser mayor al disponible.";
                    $('input[id$=Txt_Septiembre]').val("0.00");
                }
            break;
            case 10:
                if($('input[id$=Txt_Octubre]').val() != "" && $('input[id$=Txt_Octubre]').val() != "NaN"){
                    Importe_Mes = parseFloat($('input[id$=Txt_Octubre]').val().replace(/,/gi,''));
                }
                if($('[id$=Lbl_Disp_Oct]').text() != "" && $('[id$=Lbl_Disp_Oct]').text() != "NaN"){
                    Importe_Disp_Mes = parseFloat($('[id$=Lbl_Disp_Oct]').text().replace(/,/gi,''));
                }
                if(Importe_Mes > Importe_Disp_Mes){
                    Error += " * El Importe del mes de Octubre no puede ser mayor al disponible.";
                    $('input[id$=Txt_Octubre]').val("0.00");
                }
            break;
            case 11:
                if($('input[id$=Txt_Noviembre]').val() != "" && $('input[id$=Txt_Noviembre]').val() != "NaN"){
                    Importe_Mes = parseFloat($('input[id$=Txt_Noviembre]').val().replace(/,/gi,''));
                }
                if($('[id$=Lbl_Disp_Nov]').text() != "" && $('[id$=Lbl_Disp_Nov]').text() != "NaN"){
                    Importe_Disp_Mes = parseFloat($('[id$=Lbl_Disp_Nov]').text().replace(/,/gi,''));
                }
                if(Importe_Mes > Importe_Disp_Mes){
                    Error += " * El Importe del mes de Noviembre no puede ser mayor al disponible.";
                    $('input[id$=Txt_Noviembre]').val("0.00");
                }
            break;
            case 12:
                if($('input[id$=Txt_Diciembre]').val() != "" && $('input[id$=Txt_Diciembre]').val() != "NaN"){
                    Importe_Mes = parseFloat($('input[id$=Txt_Diciembre]').val().replace(/,/gi,''));
                }
                if($('[id$=Lbl_Disp_Dic]').text() != "" && $('[id$=Lbl_Disp_Dic]').text() != "NaN"){
                    Importe_Disp_Mes = parseFloat($('[id$=Lbl_Disp_Dic]').text().replace(/,/gi,''));
                }
                if(Importe_Mes > Importe_Disp_Mes){
                    Error += " * El Importe del mes de Diciembre no puede ser mayor al disponible.";
                    $('input[id$=Txt_Diciembre]').val("0.00");
                }
            break;
            default:
                if($('[id$=Hf_Modificado]').val() != ""  && $('[id$=Hf_Modificado]').val() != "NaN"){
                    if(Importe > Disponible)
                    {
                        $('[id$=Lbl_Validacion]').text("El Importe no puede ser mayor al disponible");
                        $('input[id$=Txt_Importe]').val("0.00");
                    }
                }
            break;
        }
        if($('input[id$=Txt_Enero]').val() != undefined)
        {
             Sumar();
            
            if($('input[id$=Txt_Importe]').val() != "" && $('input[id$=Txt_Importe]').val() != "NaN"){
                Importe = parseFloat($('input[id$=Txt_Importe]').val().replace(/,/gi,''));
             }
            
             if($('input[id$=Txt_Importe]').val() != "" && $('input[id$=Txt_Importe]').val() != "NaN"){
                if($('input[id$=Txt_Total]').val() != ""  && $('input[id$=Txt_Total]').val() != "NaN")
                {
                    Total = parseFloat($('input[id$=Txt_Total]').val().replace(/,/gi,''));
                    if(Total > Importe)
                    {
                        Error += " * A EXCEDIDO EL IMPORTE DE LA OPERACIÓN, REVISE LOS IMPORTES DE LOS MESES.";
                    }
                }
             }
         }
    }
    
    if(Tipo_Operacion != "REDUCCION")
    {
        if($('input[id$=Txt_Enero]').val() != undefined)
        {
            Sumar();
        
            if($('input[id$=Txt_Importe]').val() != "" && $('input[id$=Txt_Importe]').val() != "NaN"){
                Importe = parseFloat($('input[id$=Txt_Importe]').val().replace(/,/gi,''));
             }
        
             if($('input[id$=Txt_Importe]').val() != "" && $('input[id$=Txt_Importe]').val() != "NaN"){
                if($('input[id$=Txt_Total]').val() != ""  && $('input[id$=Txt_Total]').val() != "NaN")
                {
                    Total = parseFloat($('input[id$=Txt_Total]').val().replace(/,/gi,''));
                    if(Total > Importe)
                    {
                        Error += " * A EXCEDIDO EL IMPORTE DE LA OPERACIÓN, REVISE LOS IMPORTES DE LOS MESES.";
                    }
                }
             }
        }
    }
    
    $('[id$=Lbl_Validacion]').text(Error);
}
