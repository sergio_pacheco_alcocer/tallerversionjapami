﻿$(function () {
    AutocompletadoEmpleados();
});

function format(item) {
    return item.nombre;
}

function AutocompletadoEmpleados() {
    //inciamos el auto completado
    $("[id$='Txt_Empleado']").autocomplete("../../Controladores/Frm_Controlador_Cajas.aspx", {
        extraParams: { accion: 'autocompletar_empleados' },
        dataType: "json",
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.empleado_id,
                    result: row.nombre
                }
            });
        },
        formatItem: function (item) {
            return format(item);
        }
    }).result(function (e, item) {
        $("[id$='Txt_Empleado_ID']").val(item.empleado_id);
    });
}