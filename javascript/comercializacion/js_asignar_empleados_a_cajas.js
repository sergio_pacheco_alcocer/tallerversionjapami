﻿
var accion;
var sucursal_id;
var caja_id;
var numero_caja;
var nombre_sucursal;
var estado;
var blnSeleccionarEmpleado=false; 
var empleado_id;
var cajero_id;


$(function(){

     
     // inicializar popus
   $('#ventana_mensaje').window('close');
   $('#ventana_buscar_cajas').window('close');
     
    crear_arbol_cajero();
    crear_arbol();       
    bloquearControles();
   
    
   //eventos de los botones de la barra de herramientas inicio
    $("[id$='Btn_Nuevo']").click(function(event) {
        nuevoElemento();
        event.preventDefault();
    });

    $("[id$='Btn_Modificar']").click(function(event) {
        editarElemento();
        event.preventDefault();
    });

   
    $("[id$='Btn_Guardar']").click(function(event) {
        guardarRegistro();
        event.preventDefault();
    });

    $("[id$='Btn_Cancelar']").click(function(event) {
        cancelarRegistro();
        event.preventDefault();
    });

    $("[id$='Btn_Salir']").click(function(event) {
        event.preventDefault();
    });




});// fin

// ------------------------------------------------------------------------------------------------

function crear_arbol_cajero(){

    $('#arbol_cajero').tree({
        url: '../../Controladores/frm_controlador_asignacion_cajas_sucursal_usuario.aspx?accion=obtenerSucursalesquetienenasigandosempleados', 
        checkbox:false,
        onBeforeExpand: function(node) {
          llenarArbolEmpleados(node);   
        },
        onClick: function(node) {
              seleccionarEmpleado(node);
        },
        onCheck: function(node, checked) {
      
        }
    });


}


function llenarArbolEmpleados(node) {
 
     accion = node.attributes.valor1;
     sucursal_id=node.attributes.valor2;
     
     var caja_id =node.id;    
    
     
    if (accion == "cajas")
        $('#arbol_cajero').tree('options').url = "../../Controladores/frm_controlador_asignacion_cajas_sucursal_usuario.aspx?accion=obtenerCajaEmpleados&sucursal_id=" + sucursal_id;    
        
     if(accion=="empleados")    
        $('#arbol_cajero').tree('options').url = "../../Controladores/frm_controlador_asignacion_cajas_sucursal_usuario.aspx?accion=obtenerCajerosAsignados&caja_id=" + caja_id;    
}

function seleccionarEmpleado(node){


     if (node.attributes.valor1=="fin"){
          empleado_id=node.id;
          caja_id=node.attributes.valor2;
          estado=node.attributes.valor3;
          
          var nodoPapa= $('#arbol_sucursales').tree('getParent',node.target);
          numero_caja=nodoPapa.text;
          
          var nodoPapaPapa=$('#arbol_sucursales').tree('getParent',nodoPapa.target);
          nombre_sucursal=nodoPapaPapa.text;
          
          
        $("[id$='txt_sucursal']").val(nombre_sucursal);
        $("[id$='txt_caja']").val(numero_caja);
          
        $("[id$='cmb_empleado'] option[value=" + empleado_id + "]").attr("selected",true);
        $("[id$='cmb_estado']").findByOptionTextMG(estado); 
        
        cajero_id=node.attributes.valor4;
          
        blnSeleccionarEmpleado=true  
           
            
       
     }else{
        blnSeleccionarEmpleado=false;
        desSeleccionarCombos();
        caja_id="";
        cajero_id="";
        limpiarControles();
     }
     
     

}

//-------------------------------------------------------------------
function desSeleccionarCombos(){
   
   $("[id$='cmb_empleado'] option[value=-1]").attr("selected",true);
   $("[id$='cmb_estado'] option[value=-1]").attr("selected",true);
}



// ------------------------------------------------------------------------------------------------

function crear_arbol() {

    $('#arbol_sucursales').tree({
        url: '../../Controladores/frm_controlador_asignacion_cajas_sucursal_usuario.aspx?accion=obtener_sucursales', 
        checkbox:false,
        onBeforeExpand: function(node) {
            llenarArbol(node);
        },
        onClick: function(node) {
              seleccionarCaja(node);
        },
        onCheck: function(node, checked) {
      
        }
    });
    

}


function llenarArbol(node) {
 
     accion = node.attributes.valor1;
     sucursal_id=node.attributes.valor2;
     
    if (accion == "cajas")
        $('#arbol_sucursales').tree('options').url = "../../Controladores/frm_controlador_asignacion_cajas_sucursal_usuario.aspx?accion=obtenerCajas&sucursal_id=" + sucursal_id;    
}



function seleccionarCaja(node){
   if (node.attributes.valor1=="fin"){
         caja_id=node.id;
         numero_caja=node.text;
         var nodoPapa= $('#arbol_sucursales').tree('getParent',node.target);
         nombre_sucursal= nodoPapa.text;
         
         $("[id$='txt_sucursal']").val(nombre_sucursal);
         $("[id$='txt_caja']").val(numero_caja);
         
   }
   else{
      caja_id="";
      cajero_id="";
      $("[id$='txt_sucursal']").val('');
      $("[id$='txt_caja']").val('');
   }
   
}


//-------------------------------------------------------------------------------------------------
//  metodos auxiliares
    function bloquearControles() {
        $("#datos_registro :input").attr('disabled', true);
        $('#btn_buscar').linkbutton({disabled:true});   
    }
    
   function desbloquearControles() {
        $("#datos_registro :input").attr('disabled', false);
       $('#btn_buscar').linkbutton({disabled:false});
    }
    
    
    function limpiarControles() {  
        $('#datos_registro :text').val('');
     }
      
   
    function cerrarPanel(){
      $('#panel_explorador').panel('collapse');
    }

    function abrirPanel(){
      $('#panel_explorador').panel('expand');
    }   
      
      
  //funciones de los botones
//-------------------------------------------------------------------------------------------------
    function nuevoElemento() {
        ext_ocultarBotonesOperacion();
        ext_visualizarBotonesGuardarCancelar();
        desbloquearControles();
        desSeleccionarCombos();
        limpiarControles();
        cerrarPanel();
        operacion = "nuevo";
       
     
    }
    
    
  //---
      function editarElemento() {
            
            
        if (blnSeleccionarEmpleado==false){
           $.messager.alert('Japami','Tienes que seleccionar un empleado','info');
           return ;
        }       
            
        ext_ocultarBotonesOperacion();
        ext_visualizarBotonesGuardarCancelar();
        desbloquearControles();
        cerrarPanel();
        operacion = "editar";
        

    }  
    
    //--
    
    function cancelarRegistro() {
        ext_ocultarBotonesGuardarCancelar();
        ext_visualizarBotonesOperacion();
        bloquearControles();
        limpiarControles();
        desSeleccionarCombos();
        caja_id="";
        cajero_id="";
        operacion = "ninguno";
        blnSeleccionarEmpleado=false;
        abrirPanel();

    }
    
   //-----
   
    function guardarRegistro() {
    
      if (valoresRequeridosGeneral("datos_registro")==false ){
        $.messager.alert('Japami','Se requieren algunos valores','info');
        return;    
    }
    
    caja_id=$.trim(caja_id); 
    if(caja_id.length==0){
        $.messager.alert('Japami','Selecciona una caja','info');
        return; 
    }
  
    empleado_id= $("[id$='cmb_empleado']").val();
    estado =$("[id$='cmb_estado'] option:selected").text();
    
           $('#ventana_mensaje').window('open');
             
            $.ajax({
            type: "POST",
            url: "../../Controladores/frm_controlador_asignacion_cajas_sucursal_usuario.aspx/asignarCajero",
            data: "{'empleado_id':'" + empleado_id + "','caja_id':'" + caja_id + "','estado':'" + estado + "','cajero_id':'" + cajero_id + "','operacion':'" + operacion + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                
                var respuesta = eval("(" + response.d + ")");
                if (respuesta.mensaje == "bien") {
                  $('#ventana_mensaje').window('close');
                  procesoExitoso();
                  
                }
                else {
                    $('#ventana_mensaje').window('close');
                    $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                              

                }

            },
            error: function(result) {
                $('#ventana_mensaje').window('close');
                $.messager.alert('Japami', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');

            }

        });
        
        
    
    }
    
    function procesoExitoso(){
      
      $.messager.alert('Japami','Proceso Terminado','info',function(){
         cancelarRegistro();
        $('#arbol_cajero').tree({url:'../../Controladores/frm_controlador_asignacion_cajas_sucursal_usuario.aspx?accion=obtenerSucursalesquetienenasigandosempleados'}); 
     }); 
      
      
    
    }
    
    
  //---------------------
  
  function buscarCajas(){
  
          $('#ventana_buscar_cajas').window({
                      closed: false,
                      onOpen: function() {
                       
                       $("[id$='txt_sucursal']").css('border-style', 'groove');
                       $("[id$='txt_sucursal']").css('border-color', '');
                       
                       $("[id$='txt_caja']").css('border-style', 'groove');
                       $("[id$='txt_caja']").css('border-color', '');
                        
                      }                     
                  });
  
  
  }
   
 //----------------------
 
 function cerrarVentanas(){
    $('#ventana_buscar_cajas').window('close');
 
 }
 
 
 //-------------------------------------------
 
  //usage $("[id$='Cmb_Consultorio']").findByOptionTextMG(renglon_datos.nombre_consultorio);
 
jQuery.fn.findByOptionTextMG = function (data) {
     $(this).find("option").each(function(){
            if ($(this).text() == data)
            $(this).attr("selected","selected");
      });
};
 
jQuery.fn.findByOptionValueMG = function (data) {
     $(this).find("option").each(function(){
            if ($(this).val() == data)
            $(this).attr("selected","selected");
      });
};   
        