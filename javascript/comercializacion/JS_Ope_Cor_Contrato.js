﻿var var_colonia_id, var_calle_id, var_calle_referencia1_id, var_calle_referencia2_id;

$(function () {
    AutocompletadoColonias_Toma();
    AutocompletadoCiudades_Propietario();
    AutocompletadoColonias_Propietario();
    AutocompletadoColonias_Generales();
    AutocompletadoColonias_Filtros();
});

function format(item) 
{
    return item.nombre;
}

function AutocompletadoColonias_Toma() 
{
    //inciamos el auto completado
    $("[id$='Txt_Colonia_Toma']").autocomplete("../../Controladores/Frm_Controlador_Contrato.aspx", {
        extraParams: { accion: 'autocompletar_colonia_toma' },
        dataType: "json",
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.colonia_id,
                    result: row.nombre
                }
            });
        },
        formatItem: function (item) {
            return format(item);
        }
    }).result(function (e, item) {
        var_colonia_id = item.colonia_id;
        $("[id$='Txt_Colonia_ID_Toma']").val(var_colonia_id);
        AutocompletadoCalles_Toma(var_colonia_id);
    });
}

function AutocompletadoCalles_Toma(colonia_id) 
{
    //inciamos el auto completado
    $("[id$='Txt_Calle_Toma']").autocomplete("../../Controladores/Frm_Controlador_Contrato.aspx", {
        extraParams: { accion: 'autocompletar_calle_toma', colonia_id: colonia_id },
        dataType: "json",
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.calle_id,
                    result: row.nombre
                }
            });
        },
        formatItem: function (item) {
            return format(item);
        }
    }).result(function (e, item) {
        var_calle_id = item.calle_id;
        $("[id$='Txt_Calle_ID_Toma']").val(var_calle_id);
        AutocompletadoCallesReferencia1_Toma(var_colonia_id, var_calle_id);
    });
}

function AutocompletadoCallesReferencia1_Toma(colonia_id, calle_id) {
    //inciamos el auto completado
    $("[id$='Txt_Calle_Referencia1_Toma']").autocomplete("../../Controladores/Frm_Controlador_Contrato.aspx", {
        extraParams: { accion: 'autocompletar_calle_referencia1_toma', colonia_id: colonia_id, calle_id: calle_id },
        dataType: "json",
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.calle_id,
                    result: row.nombre
                }
            });
        },
        formatItem: function (item) {
            return format(item);
        }
    }).result(function (e, item) {
        var_calle_referencia1_id = item.calle_id;
        $("[id$='Txt_Calle_Referencia1_ID_Toma']").val(var_calle_referencia1_id);
        AutocompletadoCallesReferencia2_Toma(var_colonia_id, var_calle_id, var_calle_referencia1_id);
    });
}

function AutocompletadoCallesReferencia2_Toma(colonia_id, calle_id, calle_referencia1_id) {
    //inciamos el auto completado
    $("[id$='Txt_Calle_Referencia2_Toma']").autocomplete("../../Controladores/Frm_Controlador_Contrato.aspx", {
        extraParams: { accion: 'autocompletar_calle_referencia2_toma', colonia_id: colonia_id, calle_id: calle_id, calle_referencia1_id: calle_referencia1_id },
        dataType: "json",
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.calle_id,
                    result: row.nombre
                }
            });
        },
        formatItem: function (item) {
            return format(item);
        }
    }).result(function (e, item) {
        var_calle_referencia2_id = calle_id;
        $("[id$='Txt_Calle_Referencia2_ID_Toma']").val(var_calle_referencia2_id);
    });
}

function AutocompletadoCiudades_Propietario() {
    //inciamos el auto completado
    $("[id$='Txt_Ciudad_Propietario']").autocomplete("../../Controladores/Frm_Controlador_Contrato.aspx", {
        extraParams: { accion: 'autocompletar_ciudad_propietario' },
        dataType: "json",
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.ciudad_id,
                    result: row.nombre
                }
            });
        },
        formatItem: function (item) {
            return format(item);
        }
    }).result(function (e, item) {
    });
}

function AutocompletadoColonias_Propietario() {
    //inciamos el auto completado
    $("[id$='Txt_Colonia_Propietario']").autocomplete("../../Controladores/Frm_Controlador_Contrato.aspx", {
        extraParams: { accion: 'autocompletar_colonia_propietario' },
        dataType: "json",
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.colonia_id,
                    result: row.nombre
                }
            });
        },
        formatItem: function (item) {
            return format(item);
        }
    }).result(function (e, item) {
        var_colonia_id = item.colonia_id;
        AutocompletadoCalles_Propietario(var_colonia_id);
    });
}

function AutocompletadoCalles_Propietario(colonia_id) {
    //inciamos el auto completado
    $("[id$='Txt_Calle_Propietario']").autocomplete("../../Controladores/Frm_Controlador_Contrato.aspx", {
        extraParams: { accion: 'autocompletar_calle_propietario', colonia_id: colonia_id },
        dataType: "json",
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.calle_id,
                    result: row.nombre
                }
            });
        },
        formatItem: function (item) {
            return format(item);
        }
    }).result(function (e, item) {
    });
}

function AutocompletadoColonias_Generales() {
    //inciamos el auto completado
    $("[id$='Txt_Colonia_Generales']").autocomplete("../../Controladores/Frm_Controlador_Contrato.aspx", {
        extraParams: { accion: 'autocompletar_colonia_generales' },
        dataType: "json",
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.colonia_id,
                    result: row.nombre
                }
            });
        },
        formatItem: function (item) {
            return format(item);
        }
    }).result(function (e, item) {
        var_colonia_id = item.colonia_id;
        $("[id$='Txt_Colonia_ID_Generales']").val(var_colonia_id);
    });
}

function AutocompletadoColonias_Filtros() {
    //inciamos el auto completado
    $("[id$='Txt_Colonia_Filtro']").autocomplete("../../Controladores/Frm_Controlador_Contrato.aspx", {
        extraParams: { accion: 'autocompletar_colonia_filtro' },
        dataType: "json",
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.colonia_id,
                    result: row.nombre
                }
            });
        },
        formatItem: function (item) {
            return format(item);
        }
    }).result(function (e, item) {
        var_colonia_id = item.colonia_id;
        $("[id$='Txt_Colonia_ID_Filtro']").val(var_colonia_id);
        AutocompletadoCalles_Filtros(var_colonia_id);
    });
}

function AutocompletadoCalles_Filtros(colonia_id) {
    //inciamos el auto completado
    $("[id$='Txt_Calle_Filtro']").autocomplete("../../Controladores/Frm_Controlador_Contrato.aspx", {
        extraParams: { accion: 'autocompletar_calle_filtro', colonia_id: colonia_id },
        dataType: "json",
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.calle_id,
                    result: row.nombre
                }
            });
        },
        formatItem: function (item) {
            return format(item);
        }
    }).result(function (e, item) {
        var_calle_id = item.calle_id;
        $("[id$='Txt_Calle_ID_Filtro']").val(var_calle_id);
    });
}