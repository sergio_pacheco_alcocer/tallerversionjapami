﻿

var saldo_a_mostrar;
var saldo_nuevo;
var monto_cheques;
var monto_tarjetas;

$(function(){
   
    //cierra las ventans del popup
    $('#Div_Cheques').window('close');
    $('#Div_Tarjetas').window('close');
    $('#ventana_mensaje').window('close');
    
    crearTablaCheque();
    crearTablaTarjeta();

});


//------------------------------------------------------------------------------

function crearTablaCheque(){
  
    $('#tabla_cheque').datagrid({
        title: 'Cheques',
        width: 750,
        height: 200,
        columns: [[
	                     { field: 'no_cheque', title: 'No. Cheque', width: 100, sortable: true },
	                     { field: 'cantidad', title: 'Cantidad', width: 100, sortable: true },
	                     { field: 'banco', title: 'Banco', width: 150, sortable: true },
	                     { field: 'nombre', title: 'Nombre', width: 150, sortable: true },
	                     { field: 'paterno', title: 'A. Paterno', width: 150, sortable: true },
	                     { field: 'materno', title: 'A. Materno', width: 150, sortable: true },
	                     { field: 'cantidad_letra', title: 'Cant. Letra', width: 150, sortable: true },
	                     { field: 'banco_id', title: 'id_banco', width:0 , sortable: true }
	              ]],
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        showFooter: false,
        striped: true,
        loadMsg: 'Cargando Datos',
        nowrap: false,
        toolbar: [{
            id: 'btneliminar',
            text: 'Eliminar',
            iconCls: 'icon-cancel',
            handler: function() {
                eliminarCheque();
            }
        

}]


        });
        
        $('#tabla_cheque').datagrid('hideColumn','banco_id');

}


function crearTablaTarjeta(){

   $('#tabla_tarjetas').datagrid({
        title: 'Tarjetas',
        width: 750,
        height: 200,
        columns: [[
	                     { field: 'monto', title: 'Monto', width: 100, sortable: true },
	                     { field: 'no_aprobacion', title: 'No.Aprobacion', width: 100, sortable: true },
	                     { field: 'no_tarjeta', title: 'No.Tarjeta', width: 150, sortable: true },
	                     { field: 'banco', title: 'Banco', width: 150, sortable: true },
	                     { field: 'banco_id', title: 'id_banco', width:0 , sortable: true }
	              ]],
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        showFooter: false,
        striped: true,
        loadMsg: 'Cargando Datos',
        nowrap: false,
        toolbar: [{
            id: 'btneliminar',
            text: 'Eliminar',
            iconCls: 'icon-cancel',
            handler: function() {
                eliminarTarjeta();
            }
        

}]


        });
        
        $('#tabla_tarjetas').datagrid('hideColumn','banco_id');

}

//------------------------------------------------------------------------------
function abrirVentanaCheques(){
 
saldo_a_mostrar=obtenerNuevoSaldo();

if ( isNaN(saldo_a_mostrar) || saldo_a_mostrar==0 ){
   $.messager.alert('Japami','No hay saldo que liquidar','Error');
   return;
}

$('#txt_mostrar_saldo_en_pantalla_cheques').val(saldo_a_mostrar);
monto_cheques=obtenerSumaMontoCheques();
$('#txt_total_cantidad_cheques').val(monto_cheques);

$('#Div_Cheques').window('open');

}

function abrirVentanaTarjetas(){

saldo_a_mostrar=obtenerNuevoSaldo();

if ( isNaN(saldo_a_mostrar) || saldo_a_mostrar==0 ){
   $.messager.alert('Japami','No hay saldo que liquidar','Error');
   return;
}

$('#txt_mostrar_saldo_en_pantalla_tarjeta').val(saldo_a_mostrar);
monto_tarjetas=obtenerSumaMontoTarjetas();
$('#txt_total_cantidad_tarjeta').val(monto_tarjetas);


 $('#Div_Tarjetas').window('open');
}

//------------------------------------------------------------------------------

function cerrarVentanas(){
  $('#Div_Cheques').window('close');
  $('#Div_Tarjetas').window('close');
    
}

//------------------------------------------------------------------------------

function agregarTarjeta(){
  var monto_agregar;

 if(valoresRequeridosGeneral("Div_Tarjetas")==false){
    $.messager.alert('Japami','Se requieren algunos valores','error');
    return;  
    }
    
   if (buscar_en_grid("tabla_tarjetas",$('#txt_No_Tarjeta').val(),"no_tarjeta")){
      $.messager.alert('Japami','No. tarjeta  Repetido','error');
      return;
   }
   
   // validar_monto
 monto_agregar= parseFloat($('#txt_Monto_Tarjeta').val());

   if(monto_agregar>saldo_a_mostrar){
      $.messager.alert('Japami','La cantidad del cheque supera el saldo','error');
      return;
    }
    
 
    
    $('#tabla_tarjetas').datagrid('appendRow', {
           monto:$('#txt_Monto_Tarjeta').val(),
           no_aprobacion:$('#txt_No_Aprobacion_Tarjeta').val(),
           no_tarjeta:$('#txt_No_Tarjeta').val(),
           banco:$('#cmb_Banco_ID_Tarjeta option:selected').text(),
           banco_id:$('#cmb_Banco_ID_Tarjeta').val()            
        }); 
    
    $('#Div_Tarjetas :input').val('');
    
 monto_tarjetas=obtenerSumaMontoTarjetas();
$('#txt_total_cantidad_tarjeta').val(monto_tarjetas);
$('#Txt_Tarjetas').val(monto_tarjetas);
    
 saldo_a_mostrar=obtenerNuevoSaldo();
 $('#txt_mostrar_saldo_en_pantalla_tarjeta').val(saldo_a_mostrar);
 $('#Txt_Saldo').val(saldo_a_mostrar)

}

function eliminarTarjeta(){

 var renglon;
  var index;
 renglon=$('#tabla_tarjetas').datagrid('getSelected');
 
 if(renglon==null){
    $.messager.alert('Japami','Tienes que seleccionar un elemento');
    return;
 }
  
 index=$('#tabla_tarjetas').datagrid('getRowIndex',renglon);
 $('#tabla_tarjetas').datagrid('deleteRow', index);
   
 monto_tarjetas=obtenerSumaMontoTarjetas();
$('#txt_total_cantidad_tarjeta').val(monto_tarjetas);
$('#Txt_Tarjetas').val(monto_tarjetas);
    
 saldo_a_mostrar=obtenerNuevoSaldo();
 $('#txt_mostrar_saldo_en_pantalla_tarjeta').val(saldo_a_mostrar);
 $('#Txt_Saldo').val(saldo_a_mostrar)


}


//------------------------------------------------------------------------------

function agregarCheque(){
var monto_agregar;

  if(valoresRequeridosGeneral("Div_Cheques")==false){
    $.messager.alert('Japami','Se requieren algunos valores','error');
    return;  
    }
    
   if (buscar_en_grid("tabla_cheque",$('#txt_No_Cheque').val(),"no_cheque")){
      $.messager.alert('Japami','No. cheque Repetido','error');
      return;
   }
    
    // validar_monto
    monto_agregar= parseFloat($('#txt_Cantidad_Numero_Cheque').val());
    
    if(monto_agregar>saldo_a_mostrar){
      $.messager.alert('Japami','La cantidad del cheque supera el saldo','error');
      return;
    }
    
     $('#tabla_cheque').datagrid('appendRow', {
           no_cheque:$('#txt_No_Cheque').val(),
           banco:$('#cmb_Banco_Cheque option:selected').text(),
           cantidad:$('#txt_Cantidad_Numero_Cheque').val(),
           nombre: $('#txt_Nombre_Usuario_Cheque').val(),
           paterno:$('#txt_Apellido_Paterno_Usuario_Cheque').val(),
           materno:$('#txt_Apellido_Materno_Usuario_Cheque').val(),
           cantidad_letra:$('#txt_Cantidad_Letra_Cheque').val(),
           banco_id:$('#cmb_Banco_Cheque').val()            
        });
    
   
      $('#Div_Cheques :input').val('');
      
      monto_cheques=obtenerSumaMontoCheques();
      $('#txt_total_cantidad_cheques').val(monto_cheques);
      $('#Txt_Cheques').val(monto_cheques);
      
      saldo_a_mostrar=obtenerNuevoSaldo();
      $('#txt_mostrar_saldo_en_pantalla_cheques').val(saldo_a_mostrar);
      $('#Txt_Saldo').val(saldo_a_mostrar)
     

}


function eliminarCheque(){
  var renglon;
  var index;
 renglon=$('#tabla_cheque').datagrid('getSelected');
 
 if(renglon==null){
    $.messager.alert('Japami','Tienes que seleccionar un elemento');
    return;
 }
  
 index=$('#tabla_cheque').datagrid('getRowIndex',renglon);
 $('#tabla_cheque').datagrid('deleteRow', index);
  
 monto_cheques=obtenerSumaMontoCheques();
 $('#txt_total_cantidad_cheques').val(monto_cheques);
 $('#Txt_Cheques').val(monto_cheques);
 
 saldo_a_mostrar=obtenerNuevoSaldo();
 $('#txt_mostrar_saldo_en_pantalla_cheques').val(saldo_a_mostrar);
 $('#Txt_Saldo').val(saldo_a_mostrar);  

}


//---------------------------------------

function obtenerSumaMontoCheques(){
  var renglones;
  var renglon;
  var suma=0;
  
  renglones=$('#tabla_cheque').datagrid('getRows');
  
  for(var i=0 ;i<renglones.length; i++){
       renglon=renglones[i];
       suma=suma + parseFloat( renglon.cantidad);
  }
  
  return suma;
}

function obtenerSumaMontoTarjetas(){
var renglones;

 renglones=$('#tabla_tarjetas').datagrid('getRows');
 var renglones;
  var renglon;
  var suma=0;
  
  renglones=$('#tabla_tarjetas').datagrid('getRows');
  
  for(var i=0 ;i<renglones.length; i++){
       renglon=renglones[i];
       suma= suma + parseFloat(renglon.monto);
  }
  
  return suma;
}

function obtenerNuevoSaldo(){
 var total_pagar,total_efectivo,total_tarjeta,total_cheques,saldo;
 
 total_pagar= parseFloat( $('#Txt_Total').val());
 total_efectivo=parseFloat( $('#Txt_Efectivo').val());
 total_tarjeta=parseFloat( $('#Txt_Tarjetas').val());
 total_cheques=parseFloat( $('#Txt_Cheques').val());
 
 saldo=total_pagar-(total_efectivo+ total_tarjeta + total_cheques);
 
 return saldo;
}