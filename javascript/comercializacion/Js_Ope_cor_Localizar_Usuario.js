﻿
$(function(){

   crearTabla();
   auto_completado_calles();
   auto_completado_colonias();
   
});

//----------------------------------------------------------

function format_calle(item) {
    // como se ve en el div contendor
    return item.calle + ' '+ item.vialidad ;
}

function auto_completado_calles() {

    //inciamos el auto completado
    $("[id$='txt_calle']").autocomplete("../../Controladores/Frm_Controlador_Consulta_Usuario.aspx", {
    extraParams: { accion: 'autocompletar_calle' },
        dataType: "json",
        parse: function(data) {
            return $.map(data, function(row) {
                return {
                    data: row,
                    value: row.calle_id,
                    result: row.calle 

                }
            });
        },
        formatItem: function(item) {
            return format_calle(item);
        }
    }).result(function(e, item) {

    });

}

//--------------------------------------------------------------


//------------------------------------------------------------------------------------------------

function format(item) {
    return item.nombre;
}

function auto_completado_colonias() { 

//inciamos el auto completado
    $("[id$='txt_colonia']").autocomplete("../../Controladores/Frm_Controlador_Consulta_Usuario.aspx", {
        extraParams: { accion: 'autocompletar_colonia' },
        dataType: "json",
        parse: function(data) {
            return $.map(data, function(row) {
                return {
                    data: row,
                    value: row.colonia_id,
                    result: row.nombre

                }
            });
        },
        formatItem: function(item) {
            return format(item);
        }
    }).result(function(e, item) {
        
    });

}


//----------------------------------------------------------

function crearTabla(){

      $('#grid_usuarios').datagrid({  // tabla inicio
            title: 'Usuarios',
            width: 800,
            height: 300,
            columns: [[
        { field: 'no_cuenta', title: 'No Cuenta', width: 100, sortable: true },
        { field: 'usuario', title: 'Usuario', width: 250, sortable: true },
        { field: 'no_region', title: 'Reg.', width: 80, sortable: true },
        { field: 'no_sector', title: 'Sec.', width: 80, sortable: true },
        { field: 'no_reparto', title: 'Rep.', width: 80, sortable: true },
        { field: 'tipo', title: 'Tipo', width: 100, sortable: true },
        { field: 'calle', title: 'Vialidad', width: 200, sortable: true },
        { field: 'exterior', title: 'Ext.', width: 100, sortable: true },
        { field: 'interior', title: 'Int.', width: 100, sortable: true },
        { field: 'colonia', title: 'Colonia', width: 200, sortable: true },
        { field: 'no_medidor', title: 'Medidor', width: 200, sortable: true },
        { field: 'tarifa', title: 'Tarifa', width: 200, sortable: true },
        { field: 'no_zona', title: 'Zona', width: 200, sortable: true },
        { field: 'estatus', title: 'Estatus', width: 200, sortable: true }
    

    ]],     
             onClickRow: function(rowIndex, rowData) {
            
            }, 
            pageSize: 50,
            pagination: true,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            showFooter: false,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap: false

        });  // tabla final

}

//---------------------------------------------------------

function buscarUsuarios()
{
     
   var datos_busqueda;
   var datos={};  
   
     if (validar_check_box_cajas_textos("criterios_busqueda",datos)) {
         return false;
     }
     //datos_busqueda = JSON.stringify($("#bus_inspeccion :input").serializeObject());
     datos_busqueda=toJSON(datos); 

//  var no_cuenta;
//   var nombre_usuario;
//   
//   no_cuenta=$.trim( $("#txt_no_cuenta").val());
//   nombre_usuario=$.trim( $("#txt_usuario").val());
//   
//   if ($('#opt_no_cuenta').attr('checked')){
//      
//      if (no_cuenta.length==0){
//            $.messager.alert('Japami','Introduce un No. de cuenta');
//            return ; 
//      }
//   }else {no_cuenta='';}
//   
//   if($('#opt_usuario').attr('checked')){
//       if(nombre_usuario.length==0){
//           $.messager.alert('Japami','Introduce un Nombre de Usuario');
//            return ; 
//       }
//      
//   }else{nombre_usuario='';}
   
    $('#grid_usuarios').datagrid('loadData',{total:0, rows:[]});
    $('#grid_usuarios').datagrid({ url: '../../Controladores/Frm_Controlador_Consulta_Usuario.aspx', queryParams: { accion: 'obtener_usuario', datos: datos_busqueda}, pageNumber: 1 });
    

}
//---------------------------------------------------------

function consultarInformacion(){
var renglon;

renglon=$('#grid_usuarios').datagrid('getSelected');

if(renglon==null){
   $.messager.alert('Japami','Debes seleccionar un usuario');
   return ;
}

window.location = "Frm_Ope_Cor_Consulta_Usuario.aspx?nocuenta=" + renglon.no_cuenta;

}

//------------------------------------------------------------------------------

function editarUsuario(){

renglon=$('#grid_usuarios').datagrid('getSelected');

if(renglon==null){
   $.messager.alert('Japami','Debes seleccionar un usuario');
   return ;
}

window.location = "frm_cat_cor_editar_usuario.aspx?nocuenta=" + renglon.no_cuenta;

}

