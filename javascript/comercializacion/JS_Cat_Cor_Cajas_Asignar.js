﻿///*******************************************************************************
///NOMBRE DE LA FUNCIÓN : $(document).ready()
///DESCRIPCIÓN          : Funcion que indica cuando el documento ha sido cargado, para poder modificar los elementod DOM
///PARAMETROS           :   
///CREO                 : Julio Cruz
///FECHA_CREO           : 19/Enero/2012 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///*******************************************************************************
$(document).ready(function () {
    Configuracion_Inicial();
});

//COnfiguración Inicial de los controles --------------------------------------------------------------------------------
function Configuracion_Inicial() {
        //progress de espera
         progress();
        //Dar formato a la tabla
        formatoTabla();
        //Llenar la datagrid con todas las cajas existentes de cada sucursal
        datosTabla();
        //Llamada a funcion en la cual verifica cual operacion se va a realizar de acuerdo al evento cliqueado
        operacionesClick();
        //Autocompletado de sucursales
        autocompletarSucursales();
        autocompletarEmpleados();
        //Verificar cuando cambie el combo de cajas
        cambioComboCaja();        
 }

//funcion para las configuraciones iniciales
function configuracionInicial() {
    $('.controles').val('');
    $('.datos').val('');
    $('.limpiar').val('');
    $('#Txt_Buscar').val('');
    $('.datos').attr('readonly', 'readonly');
}

function mostrarDatos(Empleado_ID) {
    //Dar formato del id de la caja
    var longitudCaja = 10 - Empleado_ID.toString().length,
        formatoCaja = "";

    for (i = 0; i < longitudCaja; i++)
        formatoCaja += "0";

    formatoCaja += Empleado_ID;   

    var datos = $('.datos'),
        datos_serializados = datos.serialize();

    //Traer los datos para asignarlos a los controles
    $.ajax({
        url: '../../Controladores/Frm_Controlador_Cajas.aspx?accion=datos_asignar&Empleado_ID=' + formatoCaja + "&Buscar=''",        
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $.each(data, function (i, item) {
                    $('#Txt_Sucursal').val('');
                    $('#Txt_Sucursal_ID').val('');
                    $('#Txt_Caja_ID').val('');
                    $('#Cmb_Caja').val('');
                    $('#Txt_Empleado_ID').val('');
                    $('#Txt_Empleado').val('');
                    $('#Cmb_Estatus').val('');
                    $('#Txt_Sucursal').val(item.nombre_sucursal);
                    $('#Txt_Sucursal_ID').val(item.sucursal_id);
                    cargarCajasSucursales(item.sucursal_id);
                    $('#Txt_Caja_ID').val(item.caja_id);
                    $('#Cmb_Caja').val(item.caja_id);
                    $('#Txt_Empleado_ID').val(item.empleado_id);
                    $('#Txt_Empleado').val(item.nombre);
                    $('#Cmb_Estatus').val(item.estatus_empleado);
                });
            }
            else {
                alert('No se han encontrado cajas con estas especificaciones');
            }
        }
    });
}

//función para dar el formato a una tabla
function formatoTabla() {
    $('#Div_Tabla').datagrid({
        title: 'Cajas',
        width: 800,
        height: 370,
        columns: [[
                    { field: 'Empleado_ID', title: 'Sel.', width: 50,
                        formatter: function (value, rec, index) {
                            return '<img src="../imagenes/paginas/blue_button.png" onclick="mostrarDatos(' + rec.Empleado_ID + ');"' +
                                        'alt="Seleccionar" title="Seleccionar" style="cursor:pointer;" class="seleccionar"/>';
                        }
                    },
                    { field: 'Sucursal_ID', width: 1, hidden: true },
                    { field: 'Sucursal', title: 'Sucursal', width: 250, sortable: true },
                    { field: 'Caja', title: 'Caja', width: 250, sortable: true },
                    { field: 'Empleado_ID', width: 1, hidden: true },
                    { field: 'Empleado', title: 'Empleado', width: 250, sortable: true },
                    { field: 'Estatus', title: 'Estatus', width: 260, sortable: true }
            ]],
        pagination: true,
        rownumbers: true,
        idField: 'Empleado_ID',
        sortOrder: 'asc',
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        loadMsg: "Cargando, espere ...",
        pageNumber: 1,
        pageSize: 10,
        fitColumns: true,
        striped: true,
        nowrap: false,
        pageList: [10],
        showFooter: true
    });
}

//funcion para traer los datos a la data grid de acuerdo a los filtros realizados, si no hay filtros se traen todos los datos
function datosTabla() {
    $('#Div_Tabla').datagrid('loadData', { total: 0, rows: [] });

    var datos = $('.datos'),
        datos_serializados = datos.serialize();

    $.ajax({
        url: '../../Controladores/Frm_Controlador_Cajas.aspx?accion=datos_asignar&' + datos_serializados + '&Buscar=' + $('#Txt_Buscar').val(),
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $.each(data, function (i, item) {
                    $('#Div_Tabla').datagrid('appendRow', {
                        Caja_ID: item.caja_id,
                        Caja: item.numero_caja,
                        Sucursal_ID: item.sucursal_id,
                        Sucursal: item.nombre_sucursal,
                        Empleado_ID: item.empleado_id,
                        Empleado: item.nombre,
                        Estatus: item.estatus_empleado
                    });
                });
            }
            else {
                alert('No se han encontrado cajas con estas especificaciones');
            }
        }
    });
}

//Funcion la cual contiene todos los eventos correspondientes a los click en las imagenes
function operacionesClick() {
    $('#Btn_Nuevo').click(function () {
        var estatus = $('#Btn_Nuevo').attr('alt');

        if (estatus == "No_Nuevo") {
            alert('+Estás modificando un registro en este momento.\n+Para poder agregar un nuevo registro favor de cancelar la modificación');
        }
        else if (estatus == 'Nuevo') {
            $('.datos').removeAttr('readonly');
            $('.limpiar').val('');
            $('#Cmb_Estatus').attr('readonly', 'readonly');
            $('#Txt_Buscar').attr('readonly', 'readonly');
            $('#Txt_Buscar').val('');
            $("#Cmb_Caja").html(''); 
            //Falta bloquear la datagrid

            $('#Btn_Nuevo').attr({ 'src': '../imagenes/paginas/icono_guardar.png', 'alt': 'Guardar', 'title': 'Guardar' });
            $('#Btn_Modificar').attr({ 'src': '../imagenes/paginas/icono_modificar_deshabilitado.png', 'alt': 'No_Modificar', 'title': 'Modificar' });
            $('#Btn_Buscar').attr({ 'src': '../imagenes/paginas/busqueda.png', 'alt': 'No_Buscar', 'title': 'Buscar' });
            $('#Btn_Salir').attr({ 'src': '../imagenes/paginas/icono_cancelar.png', 'alt': 'Cancelar', 'title': 'Cancelar' });
        }
        else if (estatus == 'Guardar') {
            //peticion ajax para insertar los datos de la caja
            var datos = $('.datos'),
                datos_serializados = datos.serialize();

            $.ajax({
                url: '../../Controladores/Frm_Controlador_Cajas.aspx?accion=nuevo_asignacion&' + datos_serializados,
                type: 'POST',
                async: false,
                contentType: "application/json; charset=utf-8",
                dataType: "html",
                success: function (data) {
                    if (data != null)
                        alert(data);
                    else
                        alert('Ha ocurrido un error');
                }
            });

            $("#Cmb_Caja").html(''); 
            $('.limpiar').val('');
            $('.datos').attr('readonly', 'readonly');
            $('#Txt_Buscar').removeAttr('readonly');

            $('#Btn_Nuevo').attr({ 'src': '../imagenes/paginas/icono_nuevo.png', 'alt': 'Nuevo', 'title': 'Nuevo' });
            $('#Btn_Modificar').attr({ 'src': '../imagenes/paginas/icono_modificar.png', 'alt': 'Modificar', 'title': 'Modificar' });
            $('#Btn_Buscar').attr({ 'src': '../imagenes/paginas/busqueda.png', 'alt': 'Buscar', 'title': 'Buscar' });
            $('#Btn_Salir').attr({ 'src': '../imagenes/paginas/icono_salir.png', 'alt': 'Salir', 'title': 'Salir' });

            datosTabla();
        }
    });

    $('#Btn_Modificar').click(function () {
        //Verificar si habilitar Modificar o realizar el proceso para guardar los datos
        var estatus = $('#Btn_Modificar').attr('alt');

        if (estatus == 'Modificar') {
            //Variable para saber si se ha seleccionado un registro
            var caja_id = $('#Txt_Caja_ID').val();
            //Verificar si hay o no registros para realizar la operacion que le corresponde
            if (caja_id == null || caja_id == '')
                alert('No has seleccionado un registro a modificar.\nFavor de seleccionar uno.');
            else {
                $('#Cmb_Estatus').removeAttr('readonly');
                $('#Txt_Buscar').attr('readonly', 'readonly');
                $('#Txt_Buscar').val('');

                $('#Btn_Nuevo').attr({ 'src': '../imagenes/paginas/icono_nuevo_deshabilitado.png', 'alt': 'No_Nuevo', 'title': 'Nuevo' });
                $('#Btn_Modificar').attr({ 'src': '../imagenes/paginas/icono_guardar.png', 'alt': 'Guardar', 'title': 'Guardar' });
                $('#Btn_Buscar').attr({ 'src': '../imagenes/paginas/busqueda.png', 'alt': 'No_Buscar', 'title': 'Buscar' });
                $('#Btn_Salir').attr({ 'src': '../imagenes/paginas/icono_cancelar.png', 'alt': 'Cancelar', 'title': 'Cancelar' });
            }
        }
        else if (estatus == 'Guardar') {
            var datos = $('.datos'),
                datos_serializados = datos.serialize();

            //Proceso para modificar el estatus del o los registros 
            $.ajax({
                url: '../../Controladores/Frm_Controlador_Cajas.aspx?accion=modificar_asignacion&' + datos_serializados,
                type: 'POST',
                async: false,
                contentType: "application/json; charset=utf-8",
                dataType: "html",
                success: function (data) {
                    if (data != null) {
                        alert(data);
                        datosTabla();
                    }
                    else
                        alert('Ha ocurrido un error');
                }
            });

            $("#Cmb_Caja").html(''); 
            $('.limpiar').val('');
            $('.datos').attr('readonly', 'readonly');
            $('#Txt_Buscar').removeAttr('readonly');

            $('#Btn_Nuevo').attr({ 'src': '../imagenes/paginas/icono_nuevo.png', 'alt': 'Nuevo', 'title': 'Nuevo' });
            $('#Btn_Modificar').attr({ 'src': '../imagenes/paginas/icono_modificar.png', 'alt': 'Modificar', 'title': 'Modificar' });
            $('#Btn_Buscar').attr({ 'src': '../imagenes/paginas/busqueda.png', 'alt': 'Buscar', 'title': 'Buscar' });
            $('#Btn_Salir').attr({ 'src': '../imagenes/paginas/icono_salir.png', 'alt': 'Salir', 'title': 'Salir' });

            datosTabla();
        }
    });

    $('#Btn_Salir').click(function () {
        //Verificar que tipo de operacion se va a realizar
        var estatus = $('#Btn_Salir').attr('alt');

        if (estatus == 'Cancelar') {
            $('#Btn_Nuevo').attr({ 'src': '../imagenes/paginas/icono_nuevo.png', 'alt': 'Nuevo', 'title': 'Nuevo' });
            $('#Btn_Modificar').attr({ 'src': '../imagenes/paginas/icono_modificar.png', 'alt': 'Modificar', 'title': 'Modificar' });
            $('#Btn_Buscar').attr({ 'src': '../imagenes/paginas/busqueda.png', 'alt': 'Buscar', 'title': 'Buscar' });
            $('#Btn_Salir').attr({ 'src': '../imagenes/paginas/icono_salir.png', 'alt': 'Salir', 'title': 'Salir' });
            $('#Txt_Buscar').removeAttr('readonly');
            $("#Cmb_Caja").html(''); 
            configuracionInicial();
        }
        else if (estatus == 'Salir')
            setTimeout("location.href='../Paginas_Generales/Frm_Apl_Principal.aspx'", 200);
    });

    $('#Btn_Buscar').click(function () {
        var estatus = $('#Btn_Buscar').attr('alt');

        if (estatus == 'Buscar') {
            datosTabla();
            $('#Txt_Buscar').val('');
        }
        else if (estatus == 'No_Buscar')
            alert('En estos momentos está realizando una operación en la cual está deshabilitada la opción para buscar.');
    });
}
//Autocompletar las Sucursales
function autocompletarSucursales() {
    $('#Txt_Sucursal_ID').val('');
    //Autocompletado de las Sucursales
    $('#Txt_Sucursal').autocomplete("../../Controladores/Frm_Controlador_Cajas.aspx", {
        extraParams: { accion: 'autocompletar_sucursales' },
        dataType: "json",
        selectOnly: true,
        minChars: 1,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.sucursal_id,
                    result: row.sucursal
                }
            });
        },
        formatItem: function (item) {
            return item.sucursal;
        }
    }).result(function (e, item) {
        var sucursal_id = item.sucursal_id;
        $('#Txt_Sucursal_ID').val(sucursal_id);
        cargarCajasSucursales(sucursal_id);
    });
}

//Llenar el combo de cajas de sucursales
function cargarCajasSucursales(sucursal_id) {
    var select = $('#Cmb_Caja');
    $('option', select).remove();

    $.ajax({
        url: '../../Controladores/Frm_Controlador_Cajas.aspx?accion=combo_caja&sucursal_id=' + sucursal_id,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            var options = '<option value="">< - SELECCIONE - ></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.caja_id + '">' + item.caja + '</option>';
                });
                $('#Cmb_Caja').append(options);
            }
        }
    });
}

function cambioComboCaja() {
    $('#Cmb_Caja').change(function () {
        $('#Txt_Caja_ID').val($('#Cmb_Caja').val());
    });
}

//Autocompletar Empleados
function autocompletarEmpleados() {
    $('#Txt_Empleado_ID').val('');
    //Autocompletado de las Sucursales
    $('#Txt_Empleado').autocomplete("../../Controladores/Frm_Controlador_Cajas.aspx", {
        extraParams: { accion: 'autocompletar_empleados' },
        dataType: "json",
        selectOnly: true,
        minChars: 1,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.empleado_id,
                    result: row.nombre
                }
            });
        },
        formatItem: function (item) {
            return item.nombre;
        }
    }).result(function (e, item) {
        var empleado_id = item.empleado_id;
        $('#Txt_Empleado_ID').val(empleado_id);
    });
}

//Progress
function progress() {
    jQuery.ajaxSetup({
        async: false,
        cache: false,
        timeout: (2 * 1000),
        beforeSend: function () {
            MostrarProgress();
        },
        complete: function () {
            OcultarProgress();
        },
        error: function () {
            OcultarProgress();
        }
    });

}

function MostrarProgress() {
    $('[id$=Upgrade]').show();
}

function OcultarProgress() {
    $('[id$=Upgrade]').delay(2000).hide();
}