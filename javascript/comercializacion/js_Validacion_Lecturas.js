﻿$(document).ready(function() {   //Inicio (Como el page load de la pagina)
    try {
        //****************************ASIGNACION DE PROPIEDADES DE ELEMENTOS**************************
        //Colocar las opciones del modal
        $("#Div_Modal_1").dialog({
            buttons: [{
                id: 'Btn_Cancelar_1',
                text: 'Cancelar',
                iconCls: 'icon-cancel',
                handler: function() {
                    try {
                        //Cerrar la ventana
                        Mostrar_Ventanas_Modales(1, 'close');
                    } catch (ex) {
                        Mostrar_Mensaje_Error_Modal(1, true, 'Error: Div_Modal_1_Cancelar' + ex.Description);
                    }
                }
                }, {
                    id: 'Btn_Imprimir_1',
                    text: 'Exportar',
                    iconCls: 'icon-excel',
                    handler: function() { mandarImprimir("Grid_Lecturas_1", 1, "llenar_reporte_1_5", "Validación de Lecturas:Desperfectos"); }
                }, {
                    id: 'Btn_Pdf_1',
                    text: 'Imprimir',
                    iconCls: 'icon-print',
                    handler: function() {
                        mandarPDF("Grid_Lecturas_1", 1, "imprimir_reporte_1_5", "Validación de Lecturas:Desperfectos");
                }
            }]
        });

            //Colocar las opciones del modal
        $("#Div_Modal_2").dialog({
            buttons: [{
                id: 'Btn_Cancelar_2',
                text: 'Cancelar',
                iconCls: 'icon-cancel',
                handler: function() {
                    try {
                        //Cerrar la ventana
                        Mostrar_Ventanas_Modales(2, 'close');
                    } catch (ex) {
                        Mostrar_Mensaje_Error_Modal(2, true, 'Error: Div_Modal_2_Cancelar' + ex.Description);
                    }
                }
                }, {
                    id: 'Btn_Imprimir_20',
                    text: 'Exportar',
                    iconCls: 'icon-excel',
                    handler: function() {
                       mandarImprimir("Grid_Lecturas_2", 2, "llenar_reporte_1_5", "Validación de Lecturas:Sobre Giros");
                    }
                }, {
                    id: 'Btn_Pdf_2',
                    text: 'Imprimir',
                    iconCls: 'icon-print',
                    handler: function() {
                         mandarPDF("Grid_Lecturas_2", 2, "imprimir_reporte_1_5", "Validación de Lecturas:Sobre Giros");
                    }
            }]
        });

        //Colocar las opciones del modal
        $("#Div_Modal_3").dialog({
            buttons: [{
            id: 'Btn_Cancelar_3',
            text: 'Cancelar',
            iconCls: 'icon-cancel',
            handler: function() {
                try {
                    //Cerrar la ventana
                    Mostrar_Ventanas_Modales(3, 'close');
                } catch (ex) {
                    Mostrar_Mensaje_Error_Modal(3, true, 'Error: Div_Modal_3_Cancelar' + ex.Description);
                }
            }

            }, {
            id: 'Btn_Imprimir_3',
            text: 'Exportar',
            iconCls: 'icon-excel',
            handler: function() {
               mandarImprimir("Grid_Lecturas_3", 3, "llenar_reporte_1_5", "Validación de Lecturas:Sin Lectura");
            }
            }, {
            id: 'Btn_Pdf_3',
            text: 'Imprimir',
            iconCls: 'icon-print',
            handler: function() {
             mandarPDF("Grid_Lecturas_3", 3, "imprimir_reporte_1_5", "Validación de Lecturas:Sin Lectura");
            }
            }]
        });

        //Colocar las opciones del modal
        $("#Div_Modal_4").dialog({
            buttons: [{
                id: 'Btn_Cancelar_4',
                text: 'Cancelar',
                iconCls: 'icon-cancel',
                handler: function() {
                    try {
                        //Cerrar la ventana
                        Mostrar_Ventanas_Modales(4, 'close');
                    } catch (ex) {
                        Mostrar_Mensaje_Error_Modal(4, true, 'Error: Div_Modal_4_Cancelar' + ex.Description);
                    }
                }
            }, {
                id: 'Btn_Imprimir_4',
                text: 'Exportar',
                iconCls: 'icon-excel',
                handler: function() {
                    mandarImprimir("Grid_Lecturas_4", 4, "llenar_reporte_1_5", "Validación: Usuarios Variaciones");
                }
            }, {
                id: 'Btn_Pdf_4',
                text: 'Imprimir',
                iconCls: 'icon-print',
                handler: function() {
                    mandarPDF("Grid_Lecturas_4", 4, "imprimir_reporte_1_5", "Validación: Usuarios Variaciones");
                } 
            }]
        });

        //Colocar las opciones del modal
        $("#Div_Modal_5").dialog({
            buttons: [{
            id: 'Btn_Cancelar_5',
            text: 'Cancelar',
            iconCls: 'icon-cancel',
            handler: function() {
                try {
                    //Cerrar la ventana
                    Mostrar_Ventanas_Modales(5, 'close');
                } catch (ex) {
                    Mostrar_Mensaje_Error_Modal(5, true, 'Error: Div_Modal_5_Cancelar' + ex.Description);
                }
            }
            }, {
                id: 'Btn_Imprimir_5',
                text: 'Exportar',
                iconCls: 'icon-excel',
                handler: function() {
                    mandarImprimir("Grid_Lecturas_5", 5, "llenar_reporte_1_5", "Validación Anomalías");
                }
            }, {
                id: 'Btn_Pdf_5',
                text: 'Imprimir',
                iconCls: 'icon-print',
                handler: function() {
                mandarPDF("Grid_Lecturas_5", 5, "imprimir_reporte_1_5", "Validación Anomalías");
                } 
            }]
        });

        //Colocar las opciones del modal
        $("#Div_Modal_6").dialog({
            buttons: [{
                id: 'Btn_Cancelar_6',
                text: 'Cancelar',
                iconCls: 'icon-cancel',
                handler: function() {
                    try {
                        //Cerrar la ventana
                        Mostrar_Ventanas_Modales(6, 'close');
                    } catch (ex) {
                        Mostrar_Mensaje_Error_Modal(6, true, 'Error: Div_Modal_6_Cancelar' + ex.Description);
                    }
                }
            }, {
                id: 'Btn_Imprimir_6',
                text: 'Exportar',
                iconCls: 'icon-excel',
                handler: function() {
                    mandarImprimir("Grid_Lecturas_6", 6, "llenar_reporte_6", "Usuarios sin Lectura Aplicada");
                }
            }, {
                id: 'Btn_Pdf_6',
                text: 'Imprimir',
                iconCls: 'icon-print',
                handler: function() {
                   mandarPDF("Grid_Lecturas_6", 6, "imprimir_reporte_6", "Usuarios sin Lectura Aplicada");
                } 
            }]
        });

        //Colocar las opciones del modal
        $("#Div_Modal_7").dialog({
            buttons: [{
                id: 'Btn_Cancelar_7',
                text: 'Cancelar',
                iconCls: 'icon-cancel',
                handler: function() {
                    try {
                        //Cerrar la ventana
                        Mostrar_Ventanas_Modales(7, 'close');
                    } catch (ex) {
                        Mostrar_Mensaje_Error_Modal(7, true, 'Error: Div_Modal_7_Cancelar' + ex.Description);
                    }
                }

            }, {
                id: 'Btn_Imprimir_7',
                text: 'Exportar',
                iconCls: 'icon-excel',
                handler: function() {
                    mandarImprimir("Grid_Lecturas_7", 7, "llenar_reporte_7_8", "Usuarios Variaciones Consumo Mayores 90% Menores 50%");
                }
            }, {
                id: 'Btn_Pdf_7',
                text: 'Imprimir',
                iconCls: 'icon-print',
                handler: function() {
                    mandarPDF("Grid_Lecturas_7", 7, "imprimir_reporte_7_8", "Usuarios Variaciones Consumo Mayores 90% Menores 50%");
                } 
            }]
        });

        //Colocar las opciones del modal
        $("#Div_Modal_8").dialog({
            buttons: [{
                id: 'Btn_Cancelar_8',
                text: 'Cancelar',
                iconCls: 'icon-cancel',
                handler: function() {
                    try {
                        //Cerrar la ventana
                        Mostrar_Ventanas_Modales(8, 'close');
                    } catch (ex) {
                        Mostrar_Mensaje_Error_Modal(8, true, 'Error: Div_Modal_8_Cancelar' + ex.Description);
                    }
                }

            }, {
                id: 'Btn_Imprimir_8',
                text: 'Exportar',
                iconCls: 'icon-excel',
                handler: function() {
                    mandarImprimir("Grid_Lecturas_8", 8, "llenar_reporte_7_8", "Usuarios Lectura Actual o Anterior Cero");
                }
            }, {
                id: 'Btn_Pdf_8',
                text: 'Imprimir',
                iconCls: 'icon-print',
                handler: function() {
                    mandarPDF("Grid_Lecturas_8", 8, "imprimir_reporte_7_8", "Usuarios Lectura Actual o Anterior Cero");
                } 
               }]
            });

            //Colocar las opciones del modal
            $("#Div_Modal_9").dialog({
                buttons: [{
                    id: 'Btn_Cancelar_9',
                    text: 'Cancelar',
                    iconCls: 'icon-cancel',
                    handler: function() {
                        try {
                            //Cerrar la ventana
                            Mostrar_Ventanas_Modales(9, 'close');
                        } catch (ex) {
                            Mostrar_Mensaje_Error_Modal(9, true, 'Error: Div_Modal_9_Cancelar' + ex.Description);
                        }
                    }

                },{
                    id: 'Btn_Registrar',
                    text: 'Eventos',
                    iconCls: 'icon-ok',
                    handler: function() 
                    {
                    var reporte='9';
                     var nombreGrid = "#Grid_Lecturas_9";
                       var registro = JSON.stringify($(nombreGrid).datagrid('getRows'));
                        var tipo_Consulta='Registrar_Observaciones';                
                        $.ajax(
                        {
                            url: '../../Controladores/Frm_Controlador_Validacion_Lecturas.aspx?Tipo_Consulta=' + tipo_Consulta + '&Informacion=' + registro,
                            method: 'POST',
                            dataType: 'text',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) 
                            {
                                if (data != null) 
                                {
                                    //$.each(data, function (i, item) {
                                    var error = data[1].error;
                                    var alta = data[0].alta; ;
                                    if (error != "") 
                                    {
                                        $.messager.show({
                                            title:'Información Adicional',
                                            msg:error,
                                            timeout:5000,
                                            showType:'slide'
                                        });
                                    }
                                    else 
                                    {
                                        $.messager.show({
                                            title:'Información Adicional',
                                            msg:alta,
                                            timeout:5000,
                                            showType:'slide'
                                        });
                                    }
                                }
                            },
                            beforeSend: function () 
                            {
                                $('#img_progress_9').show();
                            },
                            complete: function () 
                            {
                                
                                $('#img_progress_9').hide().delay(1000);
                                var Region_ID="";
                                var Mes="";
                                var fecha=new Date();
                                var anio="";
                                Region_ID = $("#Cmb_Regiones").combobox('getValue');
                                Mes = fecha.getMonth();
                                anio = fecha.getFullYear();
                                Llena_Reportes_9("9",Region_ID, Mes, anio);
                            },
                            error: function (data) 
                            {
                                $('#img_progress_9').hide().delay(1000);
                                if (data != null) 
                                {
                                    var error = data[1].error;
                                    var alta = data[0].alta; ;
                                    if (error != "") 
                                    {
                                        $.messager.show({
                                            title:'Información Adicional',
                                            msg:error,
                                            timeout:5000,
                                            showType:'slide'
                                        });
                                    }
                                    else 
                                    {
                                        $.messager.show({
                                            title:'Información Adicional',
                                            msg:alta,
                                            timeout:5000,
                                            showType:'slide'
                                        });
                                    }
                                }
                            }
                        },100);
                    }
                },{
                    id: 'Btn_Imprimir_9',
                    text: 'Exportar',
                    iconCls: 'icon-excel',
                    handler: function() {
                        mandarImprimir("Grid_Lecturas_9", 9, "llenar_reporte_9", "Observaciones Rutas de Lecturas");
                    }
                }, {
                    id: 'Btn_Pdf_9',
                    text: 'Imprimir',
                    iconCls: 'icon-print',
                    handler: function() {
                        mandarPDF("Grid_Lecturas_9", 9, "imprimir_reporte_9", "Observaciones Rutas de Lecturas");
                    } 
                    }]
                });
                                            
                                            
        //Colocar las opciones del modal
        $("#Div_Modal_10").dialog({
            buttons: [{
                id: 'Btn_Cancelar_10',
                text: 'Cancelar',
                iconCls: 'icon-cancel',
                handler: function() {
                    try {
                        //Cerrar la ventana
                        Mostrar_Ventanas_Modales(10, 'close');
                    } catch (ex) {
                        Mostrar_Mensaje_Error_Modal(10, true, 'Error: Div_Modal_10_Cancelar' + ex.Description);
                    }
                }
            }, {
                id: 'Btn_Imprimir_10',
                text: 'Exportar',
                iconCls: 'icon-excel',
                handler: function() {
                    mandarImprimir("Grid_Lecturas_10", 10, "llenar_reporte_10", "Usuarios con Consumo Mayores a 100m3");
                }
            }, {
                id: 'Btn_Pdf_10',
                text: 'Imprimir',
                iconCls: 'icon-print',
                handler: function() {
                    mandarPDF("Grid_Lecturas_10", 10, "imprimir_reporte_10", "Usuarios con Consumo Mayores a 100m3");
                } 
            }]
        });
            
        //Colocar las opciones del modal
        $("#Div_Modal_11").dialog({
            buttons: [{
                id: 'Btn_Cancelar_11',
                text: 'Cancelar',
                iconCls: 'icon-cancel',
                handler: function() {
                    try {
                        //Cerrar la ventana
                        Mostrar_Ventanas_Modales(11, 'close');
                    } catch (ex) {
                        Mostrar_Mensaje_Error_Modal(11, true, 'Error: Div_Modal_11_Cancelar' + ex.Description);
                    }
                }
            }, {
                id: 'Btn_Imprimir_11',
                text: 'Exportar',
                iconCls: 'icon-excel',
                handler: function() {
                    mandarImprimir("Grid_Lecturas_11", 11, "llenar_reporte_10", "Usuarios sin Lectura del Presente Mes");
                }
            }, {
                id: 'Btn_Pdf_11',
                text: 'Imprimir',
                iconCls: 'icon-print',
                handler: function() {
                    mandarPDF("Grid_Lecturas_11", 11, "imprimir_reporte_10", "Usuarios sin Lectura del Presente Mes");
                } 
            }]
        });
                                            
//********************************************************************************************
//*******************************************EVENTOS******************************************
//                                            $("#Cmb_Regiones").combobox({
//                                                onSelect: function() {
//                                                    //Declaracion de variables
//                                                    var region; //variable para ver el elemento seleccionado

//                                                    try {
//                                                        //Verificar si se ha seleccionado un elemento
//                                                        region = $("#Cmb_Regiones").combobox('getValue');
//                                                        if (region != "" && region != null) {
//                                                            Llena_Combo_Sectores(region);
//                                                        } else {
//                                                            Llena_Combo_Sectores('00000');
//                                                        }
//                                                    } catch (ex) {
//                                                        $("[id$='Lbl_Mensaje_Error']").html('Error (Cmb_Regiones_onSelect): ' + ex.Description);
//                                                        Mostrar_Mensaje_Error(true);
//                                                    }
//                                                }
//                                            });

        $("#Img_Btn_Consultar").click(function() {
            var validacion = ""; //variable para la validacion
            try {
                //validar que esten los datos completos
                validacion = Valida_Datos_Consulta();
                if (validacion == "") {
                    $("[id$='Lbl_Mensaje_Error']").html(validacion);
                    Mostrar_Mensaje_Error(false);
                    Mostrar_Reportes();
                } else {
                    $("[id$='Lbl_Mensaje_Error']").html(validacion);
                    Mostrar_Mensaje_Error(true);
                }
            } catch (ex) {
                $("[id$='Lbl_Mensaje_Error']").html('Error (Img_Btn_Consultar_click): ' + ex.Description);
                Mostrar_Mensaje_Error(true);
            }
        });
            
            
        $("#Img_Btn_Print").click(function() {
            var validacion = ""; //variable para la validacion
            try {
                //validar que esten los datos completos
                validacion = Valida_Datos_Consulta();
                if (validacion == "") {
                    $("[id$='Lbl_Mensaje_Error']").html(validacion);
                    Mostrar_Mensaje_Error(false);
                    Mostrar_Reportes_PDF();
                } else {
                    $("[id$='Lbl_Mensaje_Error']").html(validacion);
                    Mostrar_Mensaje_Error(true);
                }
            } catch (ex) {
                $("[id$='Lbl_Mensaje_Error']").html('Error (Img_Btn_Consultar_click): ' + ex.Description);
                Mostrar_Mensaje_Error(true);
            }
        });
            
        $("#Img_Btn_Aplicar").click(function() 
        {
            var validacion = ""; 
            try 
            {
                $("[id$='Lbl_Mensaje_Error']").html(validacion);
                Mostrar_Mensaje_Error(false);
                $.messager.confirm('Validacion de Lecturas', '¿Esta Seguro(a) que Desea Aplicar Lecturas?', 
                function(r)
                {
                if (r)
                {
                    //validar que esten los datos completos
                    validacion = Valida_Aplica_Lecturas();
                    if (validacion == "") 
                        Aplicar_Lecturas();
                    else 
                    {
                        $("[id$='Lbl_Mensaje_Error']").html(validacion);
                        Mostrar_Mensaje_Error(true);
                    } 
                } 
                });
                
            } 
            catch (ex) 
            {
                $("[id$='Lbl_Mensaje_Error']").html('Error (Img_Btn_Consultar_click): ' + ex.Description);
                Mostrar_Mensaje_Error(true);
            }
        });

        $("#Img_Btn_Salir").click(function() {
            try {
                window.location.href = "../Paginas_Generales/Frm_Apl_Principal.aspx";
            } catch (ex) {
                $("[id$='Lbl_Mensaje_Error']").html('Error (Img_Btn_Salir_click): ' + ex.Description);
                Mostrar_Mensaje_Error(true);
            }
        });
        //********************************************************************************************
        Estado_Inicial();
        } catch (ex) {
            $("[id$='Lbl_Mensaje_Error']").html('Error (Load): ' + ex.Description);
            Mostrar_Mensaje_Error(true);
        }
    });


//*********************************************FUNCIONES*************************************
//*******************************************************************************
//NOMBRE DE LA FUNCION:    Estado_Inicial
//DESCRIPCION:             Asigna la configuracion inicial de la pantalla
//PARAMETROS:              
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              04/Octubre/2011 19:00 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Estado_Inicial() {
    try {
        //ocultar seccion del error
        $("[id$='Lbl_Mensaje_Error']").html('');
        //Ocultar Mensajes de Error
        Mostrar_Mensaje_Error(false);
        //Muestra en el combo las regiones
        Llena_Combo_Regiones();
        //Ocultar los modals popup
        Mostrar_Ventanas_Modales(0, 'close');
    } catch (ex) {
        $("[id$='Lbl_Mensaje_Error']").html('Error (Estado_Inicial): ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Aplicar_Lecturas
//DESCRIPCION:             Actualiza el estatus de las Lecturas a 'APLICADA' de 
//                         determinada region
//PARAMETROS:              
//CREO:                    Fernando Gonzalez B.
//FECHA_CREO:              04/Septiembre/2012 09:00 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Aplicar_Lecturas() {
    try {
        var fecha = new Date();
        var Region_ID = $("#Cmb_Regiones").combobox('getValue');
        var Mes = fecha.getMonth();
        var tipo_Consulta='Aplicar_Lecturas';
        $.ajax(
        {
            type: "POST",
            url: "../../Controladores/Frm_Controlador_Validacion_Lecturas.aspx?Tipo_Consulta=" + tipo_Consulta + "&Region_ID=" + Region_ID + "&Mes=" + Mes,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) 
            {
                if (response != null) 
                {
                    var error = response[1].error;
                    var alta = response[0].alta; 
                    if (error != "") 
                    {
                       $.messager.show({
                            title:'Información Adicional',
                            msg:error,
                            timeout:5000,
                            showType:'slide'
                        });
                    }
                    else 
                    {
                       $.messager.show({
                            title:'Información Adicional',
                            msg:alta,
                            timeout:5000,
                            showType:'slide'
                        });
                    }
                }
            },
            error: function (result) 
            {
                if (result!=null)
                {
                    var error = result[1].error;
                     
                    $.messager.show({
                        title:'Información Adicional',
                        msg:error,
                        timeout:5000,
                        showType:'slide'
                    });
                }
            }
        },500);
    } catch (ex) {

        $("[id$='Lbl_Mensaje_Error']").html('Error (Img_Btn_Aplicar_click): ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Mostrar_Mensaje_Error
//DESCRIPCION:             Mostrar u ocultar el bloque del mensaje de error
//PARAMETROS:              Mostrar: Booleano que indice si se va a mostrar o no el mensaje
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              05/Julio/2011 16:30 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Mostrar_Mensaje_Error(Mostrar) {
    //Verificar si se tiene que mostrar el mensaje de error
    if (Mostrar == true) {
        $("#Div_Error").show();
    } else {
        $("[id$='Lbl_Mensaje_Error']").html('');
        $("#Div_Error").hide();
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Llena_Combo_Regiones
//DESCRIPCION:             Llenar el combo de las regiones
//PARAMETROS:              
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              04/Octubre/2011 19:00 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Llena_Combo_Regiones() {
    try {
        $("#Cmb_Regiones").combobox({
            url: '../../Controladores/Frm_Controlador_Validacion_Lecturas.aspx?Tipo_Consulta=Regiones',
            valueField: 'Region_ID',
            textField: 'Numero_Region',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            editable: false
        }); 
    } catch (ex) {
        $("[id$='Lbl_Mensaje_Error']").html('Error (Estado_Inicial): ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Llena_Combo_Sectores
//DESCRIPCION:             Llenar el combo de las sectores de acuerdo a la region
//PARAMETROS:              
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              04/Octubre/2011 19:00 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Llena_Combo_Sectores(Region_ID) {
//    try {
//        $("#Cmb_Sectores").combobox({
//            url: '../../Controladores/Frm_Controlador_Validacion_Lecturas.aspx?Tipo_Consulta=Sectores&Region_ID=' + Region_ID,
//            valueField: 'Sector_ID',
//            textField: 'Numero_Sector',
//            contentType: "application/json; charset=utf-8",
//            dataType: "json",
//            editable: false
//        }); 
//    } catch (ex) {
//        $("[id$='Lbl_Mensaje_Error']").html('Error (Estado_Inicial): ' + ex.Description);
//        Mostrar_Mensaje_Error(true);
//    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Valida_Aplica_Lecturas
//DESCRIPCION:             valida que se seleccione la informacion necesaria para
//                         aplicar las lecturas
//PARAMETROS:              
//CREO:                    Fernando Gonzalez B.
//FECHA_CREO:              04/Septiembre/2012 17:00 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Valida_Aplica_Lecturas(){
    var Region_ID;
    var resultado="";
    try
    {
    Region_ID = $("#Cmb_Regiones").combobox('getValue');
        if (Region_ID == "" || Region_ID == null){
            resultado ="Favor de seleccionar:<br />";
            resultado += "<tr>";
            resultado += "<td>+Regi&oacute;n</td>";
            resultado += "</tr>";
        }
        return resultado;
    }
    catch(ex)
    {
        $("[id$='Lbl_Mensaje_Error']").html('Error (Estado_Inicial): ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Valida_Aplica_Lecturas
//DESCRIPCION:             valida que se seleccione la informacion necesaria para
//                         aplicar las lecturas
//PARAMETROS:              
//CREO:                    Fernando Gonzalez B.
//FECHA_CREO:              04/Septiembre/2012 17:00 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Valida_Datos_Consulta() {
    var resultado = "";
    var contElementos = 0; //variable para el contador
    var contCheck = 0; //variable que cuenta los check seleccionados
    var idElemento = "";
    var Region_ID; //variable para el id de la region
    var mes; //variable para el mes

    try {
        //ciclo para el barrido de los check
        for (contElementos = 0; contElementos <= 11; contElementos++) {
            //Construir el nombre del check
            idElemento = "#Chk_Tipo_Reporte_" + contElementos;

            //Verificar si el elemento esta seleccionado
            if ($(idElemento).is(':checked')) {
                contCheck++;
            }
        }
        //Obtener los valores del los combos
        Region_ID = $("#Cmb_Regiones").combobox('getValue');

        //Verificar si hay valores
        if (Region_ID == "" || Region_ID == null || contCheck == 0) {
            resultado ="Favor de seleccionar:<br />";
            resultado += "<table border='0'>";
            resultado += "<tr>";
            resultado += "<td>+Regi&oacute;n</td>";
            resultado += "<td>+Tipo de Reporte</td>";
            resultado += "</tr>";
            resultado += "</table>";
        }

        //Entregar resultado
        return resultado;
    } catch (ex) {
        $("[id$='Lbl_Mensaje_Error']").html('Error (Estado_Inicial): ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}



//********************************************************************************************
//**************************************FUNCIONES MODAL***************************************
//*******************************************************************************
//NOMBRE DE LA FUNCION:    Mostrar_Ventanas_Modales
//DESCRIPCION:             Mostrar u ocultar las ventanas modales de los reportes de lectura.
//PARAMETROS:              1. ventana: Entero que indica la ventana a mostrar
//                         2. mostrar: cadena de texto con los valores open, close que indican si la venana se abre o cierra
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              04/Octubre/2011 14:00 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Mostrar_Ventanas_Modales(ventana, mostrar) {
    var contElementos = 0; //variable para el contador
    var idElemento = ""; //Variable para la construccion del ID del elemento

    try {
        //verificar si la opcion es mayor de cero
        if (ventana > 0) {
            //construir el nombre
            idElemento = "#Div_Modal_" + ventana;

            $(idElemento).dialog(mostrar);
            $(idElemento + ' .panel window').removeAttr('style');
            $(idElemento + ' .panel window').attr('style', 'width: 788px; left: 275.5px; top: 0px; z-index: 9026;');
        } else {
            //Ciclo para ocultar las ventanas modales
            for (contElementos = 1; contElementos <= 11; contElementos++) {
                //Construir el nombre
                idElemento = "#Div_Modal_" + contElementos;

                //Ocultar el div
                $(idElemento).dialog('close');
            }
        }
    } catch (ex) {
        $("[id$='Lbl_Mensaje_Error']").html('Error (Mostrar_Ventanas_Modales): ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Mostrar_Mensaje_Error_Modal
//DESCRIPCION:             Mostrar u ocultar las ventanas modales de los reportes de lectura.
//PARAMETROS:              1. ventana: Entero que indica la ventana a mostrar
//                         2. mostrar: cadena de texto con los valores open, close que indican si la venana se abre o cierra
//                         3. mensaje: cadena que contiene el texto del error a mostar
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              04/Octubre/2011 14:00 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Mostrar_Mensaje_Error_Modal(ventana, mostrar, mensaje) {
    var idElemento = ""; //variable patra el ID del elemento
    var contElemento = 0; //variable para el contador

    try {
        //verificar si la ventana es mayo de cero
        if (ventana > 0) {
            //construir el nombre
            idElemento = "#Div_Error_Modal_" + ventana;

            //verificar si se tiene que mostrar
            if (mostrar == true) {
                //Mostrar el div
                $(idElemento).show();

                //Colocar el texto a la etiqueta
                idElemento = "[id$='Lbl_Mensaje_Error_Modal_" + ventana + "']";
                $(idElemento).html(mensaje);
            } else {
                //Ocultar el div
                $(idElemento).hide();
            }
        } else {
            //Ciclo para ocultar los mensajes de error
            for (contElementos = 1; contElementos <= 11; contElementos++) {
                //Construir el nombre
                idElemento = "#Div_Error_Modal_" + ventana;

                //Ocultar el div
                $(idElemento).hide();

                //Construir el nombre
                idElemento = "[id$='Lbl_Mensaje_Error_Modal_" + ventana + "']";

                //limpiar la etiqueta
                $(idElemento).html('');
            }
        }
    } catch (ex) {
        $("[id$='Lbl_Mensaje_Error']").html('Error (Mostrar_Ventanas_Modales): ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Mostrar_Reportes_PDF
//DESCRIPCION:             Mostrar las ventanas de los reportes seleccionados
//PARAMETROS:              
//CREO:                    Fernando Gonzalez B.
//FECHA_CREO:              04/Septiembre/2012 17:45 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Mostrar_Reportes_PDF() {
    var contElementos = 0; //Variable para el contador
    var idElemento = ""; //variable para el id del elemento
    var contSeleccionados = 0; //variable para el contador de los elementos seleccionados
    var nombreGrid="";
    try {

        //Ciclo para el barrido de los elementos
        for (contElementos = 1; contElementos <= 11; contElementos++) {
            //Construir el id del elemento
            idElemento = "#Chk_Tipo_Reporte_" + contElementos;
            
            //Verificar si el elemento esta seleccionado
            if ($(idElemento).is(':checked')) {
                //Mostrar la ventana emergente
                nombreGrid="Grid_Lecturas_" + contElementos;
                 switch (contElementos) {
                    case 1:
                        mandarPDF(nombreGrid, contElementos, "imprimir_reporte_1_5", "Validación Lecturas: Desperfectos");
                        break;
                    case 2:
                        mandarPDF(nombreGrid, contElementos, "imprimir_reporte_1_5", "Validación Lecturas: Sobregiros");
                        break;
                    case 3:
                        mandarPDF(nombreGrid, contElementos, "imprimir_reporte_1_5", "Validación Lecturas: Sin Lectura");
                        break;                  
                    case 4:
                        mandarPDF(nombreGrid, contElementos, "imprimir_reporte_1_5", "Validación Lecturas: Sin Variaciones");
                        break;
                    case 5:
                        mandarPDF(nombreGrid, contElementos, "imprimir_reporte_1_5", "Validación Lecturas: Anomalias");
                        break;
                    case 6:
                        mandarPDF(nombreGrid, contElementos, "imprimir_reporte_6", "Usuarios Sin Lectura Aplicada");
                        break;
                    case 7:
                        mandarPDF(nombreGrid, contElementos, "imprimir_reporte_6_8", "Usuarios Variaciones Consumo Mayores 90% Menores 50%");
                        break;
                    case 8:
                        mandarPDF(nombreGrid, contElementos, "imprimir_reporte_6_8", "Usuarios Lectura Anterior Cero");
                        break;
                    case 9:
                        mandarPDF(nombreGrid, contElementos, "imprimir_reporte_9", "Observaciones Rutas Lecturas");
                        break;
                    case 10:
                        mandarPDF(nombreGrid, contElementos, "imprimir_reporte_10", "Usuarios con Consumo Mayores a 100m3");
                        break;
                    case 11:
                        mandarPDF(nombreGrid, contElementos, "imprimir_reporte_10", "Usuarios sin Lectura del Presente Mes");
                        break;
                    default:
                        break;
                }
                
            }
        }
    } catch (ex) {
        $("[id$='Lbl_Mensaje_Error']").html('Error (Mostrar_Reportes): ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Mostrar_Reportes
//DESCRIPCION:             Mostrar las ventanas de los reportes seleccionados
//PARAMETROS:              
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              04/Octubre/2011 17:45 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Mostrar_Reportes() {
    var contElementos = 0; //Variable para el contador
    var idElemento = ""; //variable para el id del elemento
    var contSeleccionados = 0; //variable para el contador de los elementos seleccionados
    var Region_ID = ""; //variable para el ID de la region
    var mes = ""; //variable para el mes
    var fecha = new Date(); //variable para la fecha
    var anio = 0; //variable para el año

    try {
        //Obtener los elementos de los combos
        Region_ID = $("#Cmb_Regiones").combobox('getValue');
        mes = fecha.getMonth()-4;
        anio = fecha.getFullYear();

        //Ciclo para el barrido de los elementos
        for (contElementos = 1; contElementos <= 11; contElementos++) {
            //Construir el id del elemento
            idElemento = "#Chk_Tipo_Reporte_" + contElementos;

            //Verificar si el elemento esta seleccionado
            if ($(idElemento).is(':checked')) {
                //Mostrar la ventana emergente
                Mostrar_Ventanas_Modales(contElementos, 'open');
                Llenar_Reportes(contElementos, Region_ID, mes, anio);
            }
        }
    } catch (ex) {
        $("[id$='Lbl_Mensaje_Error']").html('Error (Mostrar_Reportes): ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

function Llenar_Reportes(reporte, Region_ID, mes, anio) {
    try {
        //Seleccionar el tipo de reporte
        switch (reporte) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                Llena_Reportes_1_5(reporte, Region_ID, mes, anio, 1, 10);
                break;
            case 6:
                Llena_Reportes_6(reporte, Region_ID, mes, anio);
                break;
            case 7:
            case 8:
                Llena_Reportes_7_8(reporte, Region_ID, mes, anio);
                break;
            case 9:
                Llena_Reportes_9("9",Region_ID, mes, anio);
                break;
            case 10:
                Llena_Reportes_10(reporte, Region_ID, mes, anio);
                break;
            case 11:
                Llena_Reportes_10(reporte, Region_ID, mes, anio);
                break;
            default:
                break;
        }
    } catch (ex) {
        $("[id$='Lbl_Mensaje_Error']").html('Error (Llenar_Reportes): ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

function Llena_Reportes_1_5(reporte, Region_ID, Mes, Anio, pagina, cantidadRenglones) {
    //Declaracion de variables
    var nombreGrid = ""; //variable para el nombre del grid

    try {
        //Colocar el nombre del reporte
        nombreGrid = "#Grid_Lecturas_" + reporte;

        $(nombreGrid).datagrid({
            title: '',
            iconCls: 'icon-blank',
            width: 1650,
            height: 400,
            url: '../../Controladores/Frm_Controlador_Validacion_Lecturas.aspx?Tipo_Consulta=Grid_Lecturas&Tipo_Reporte=' + reporte + '&Region_ID=' + Region_ID + '&Mes=' + Mes + '&Anio=' + Anio + '&Pagina=' + pagina + '&Cantidad_Renglones=' + cantidadRenglones,
            idField: 'No_Cuenta',
            frozenColumns: null,
            pagination: true,
            rownumbers: false,
            loadMsg: 'Cargando datos',
            pageSize: 10,
            pageList:[10,20,30,40,50],
            singleSelect: true,
            striped: true,
            nowrap: false,
            fitColumns: true,
            pageNumber: 1,
            columns: [[
                { field: 'no_cuenta', title: 'No Cuenta', align: 'right', width: 100 },
                { field: 'nombre_usuario', title: 'Usuario', align: 'left', width: 200 },
                { field: 'calle', title: 'Calle', align: 'left', width: 200 },
                { field: 'fecha', title: 'Fecha', align: 'center', width: 50, hidden: true },
                { field: 'sector', title: 'Sector', align: 'right', width: 50, hidden: true },
                { field: 'reparto', title: 'Reparto', align: 'right', width: 50, hidden: true },
                { field: 'usuario', title: 'Usuario', align: 'left', width: 150, hidden: true },
                { field: 'domicilio', title: 'Domicilio', align: 'left', width: 50, hidden:true },
                { field: 'numero_exterior', title: 'Ext', align: 'right', width: 50 },
                { field: 'numero_interior', title: 'Int', align: 'right', width: 50 },
                { field: 'colonia', title: 'Colonia', align: 'left', width: 100 },
                { field: 'lectura_anterior', title: 'L.Ant', align: 'right', width: 50 },
                { field: 'lectura_actual', title: 'L.Act', align: 'right', width: 50 },
                { field: 'consumo', title: 'Cons.', align: 'right', width: 50 },
                { field: 'lecturista', title: 'Lecturista', align: 'left', width: 50, hidden:true },
                { field: 'validacion', title: 'Nueva Lectura', align: 'right', width: 100, hidden: true,
                    editor: {
                        type: 'numberbox',
                        options: {
                            valueField: 'validacion',
                            precision: 0
                        }
                    }
                },
                { field: 'comentario_validacion', title: 'Comentarios', align: 'left', width: 100, editor: 'text',hidden: true },
                { field: 'action', title: 'Acción', width: 70, align: 'center', hidden: true,
                    formatter: function (value, row, index) {
                        if (row.editing) {
                            var a = '<img onclick="saverow(' + index + ',' + reporte + ',' + row.no_cuenta + ')" src="../imagenes/paginas/accept.png" class="Img_Button" alt="Aplicar Lectura" style="cursor:pointer;width:24px;height:24px;"/>';
                            var b = '<img onclick="cancelrow(' + index + ',' + reporte + ')" src="../imagenes/paginas/delete.png" class="Img_Button" alt="Aplicar Lectura" style="cursor:pointer;width:24px;height:24px;"/>';
                            return a + b;
                        } else {
                            var c = '<img onclick="editrow(' + index + ',' + reporte + ')" src="../imagenes/gridview/grid_update.png" class="Img_Button" alt="Aplicar Lectura" style="cursor:pointer;width:16px;height:16px;"/>';
                            return c;
                        }
                    }
                },
                { field: 'no_medidor', title: 'Medidor', align: 'center', width: 100 },
                { field: 'anomalia_desperfecto', title: 'Anomal&iacute;a/desperfecto', align: 'left', width: 200 },
                { field: 'medidor_detalle_id', hidden: true },
                { field: 'predio_id', hidden: true }

            ]],
            onLoadSuccess: function (data) {
            var nombreGrid;
            nombreGrid= "#Grid_Lecturas_" + reporte;
            var Result_Consult= $(nombreGrid).datagrid('getRows');
            if (Result_Consult.length ==0){
                $.messager.show({
	                title:'Información Adicional',
	                msg:'No se encontraron resultados para esta busqueda.',
	                timeout:5000,
	                showType:'slide'
                    });
                }
            },
            onBeforeEdit: function (index, row) {
                row.editing = true;
                updateActions(reporte);
            },
            onAfterEdit: function (index, row) {
                row.editing = false;
                updateActions(reporte); //editrow(index, reporte);
            },
            onCancelEdit: function (index, row) {
                row.editing = false;
                updateActions(reporte); //cancelrow(index, reporte);
            },
            onBeforeLoad: function (param) {
                return;
            }
        });
    } catch (ex) {
        Mostrar_Mensaje_Error_Modal(reporte, true, 'Error: (Llena_Reporte_1_5): ' + ex.Description);
    }
}

function Llena_Reportes_6(reporte, Region_ID, Mes, Anio) {
    //Declaracion de variables
    var nombreGrid = ""; //variable para el nombre del grid

    try {
        //Colocar el nombre del reporte
        nombreGrid = "#Grid_Lecturas_" + reporte;

        $(nombreGrid).datagrid({
            title: '',
            iconCls: 'icon-blank',
            width: 1700,
            height: 400,
            url: '../../Controladores/Frm_Controlador_Validacion_Lecturas.aspx?Tipo_Consulta=Grid_Lecturas&Tipo_Reporte=' + reporte + '&Region_ID=' + Region_ID + '&Mes=' + Mes + '&Anio=' + Anio,
            idField: 'No_Cuenta',
            frozenColumns: null,
            pagination: true,
            rownumbers: false,
            loadMsg: 'Cargando datos',
            pageSize: 10,
            singleSelect: true,
            striped: true,
            nowrap: false,
            fitColumns: true,
            columns: [[
                { field: 'no_cuenta', title: 'No Cuenta', align: 'right', width: 20 },
                { field: 'sector', title: 'Sector', align: 'right', width: 20 },
                { field: 'reparto', title: 'Reparto', align: 'right', width: 20 },
                { field: 'usuario', title: 'Usuario', align: 'left', width: 75 },
                { field: 'domicilio', title: 'Domicilio', align: 'left', width: 75 },             
                { field: 'colonia', title: 'Colonia', align: 'right', width: 50 },
                { field: 'no_medidor', title: 'Medidor', align: 'right', width: 35 },
                { field: 'nueva_lectura', title: 'Nueva Lectura', align: 'right', width: 35 }
                
//                { field: 'action', title: 'Acción', width: 70, align: 'center', hidden: true,
//                    formatter: function (value, row, index) {
//                        if (row.editing) {
//                            var a = '<img onclick="saverow(' + index + ',' + reporte + ',' + row.no_cuenta + ')" src="../imagenes/paginas/accept.png" class="Img_Button" alt="Aplicar Lectura" style="cursor:pointer;width:24px;height:24px;"/>';
//                            var b = '<img onclick="cancelrow(' + index + ',' + reporte + ')" src="../imagenes/paginas/delete.png" class="Img_Button" alt="Aplicar Lectura" style="cursor:pointer;width:24px;height:24px;"/>';
//                            return a + b;
//                        } else {
//                            var c = '<img onclick="editrow(' + index + ',' + reporte + ')" src="../imagenes/gridview/grid_update.png" class="Img_Button" alt="Aplicar Lectura" style="cursor:pointer;width:16px;height:16px;"/>';
//                            return c;
//                        }
//                    }
//                }
            ]],
            onLoadSuccess: function (data) {
                var nombreGrid;
                nombreGrid= "#Grid_Lecturas_" + reporte;
                var Result_Consult= $(nombreGrid).datagrid('getRows');
                if (Result_Consult.length ==0){
                    $.messager.show({
	                    title:'Información Adicional',
	                    msg:'No se encontraron resultados para esta busqueda.',
	                    timeout:5000,
	                    showType:'slide'
                        });
                    }
            },
            onBeforeEdit: function (index, row) {
                row.editing = true;
                updateActions(reporte);
            },
            onAfterEdit: function (index, row) {
                row.editing = false;
                updateActions(reporte); //editrow(index, reporte);
            },
            onCancelEdit: function (index, row) {
                row.editing = false;
                updateActions(reporte); //cancelrow(index, reporte);
            },
            onBeforeLoad: function (param) {
                return;
            }
        });
                
    } catch (ex) {
        Mostrar_Mensaje_Error_Modal(reporte, true, 'Error: (Llena_Reporte_6): ' + ex.Description);
    }
}

function Llena_Reportes_7_8(reporte, Region_ID, Mes, Anio) {
    //Declaracion de variables
    var nombreGrid = ""; //variable para el nombre del grid

    try {
        //Colocar el nombre del reporte
        nombreGrid = "#Grid_Lecturas_" + reporte;

        $(nombreGrid).datagrid({
            title: '',
            iconCls: 'icon-blank',
            width: 1700,
            height: 400,
            url: '../../Controladores/Frm_Controlador_Validacion_Lecturas.aspx?Tipo_Consulta=Grid_Lecturas&Tipo_Reporte=' + reporte + '&Region_ID=' + Region_ID + '&Mes=' + Mes + '&Anio=' + Anio,
            idField: 'No_Cuenta',
            frozenColumns: null,
            pagination: true,
            rownumbers: false,
            loadMsg: 'Cargando datos',
            pageSize: 10,
            singleSelect: true,
            striped: true,
            nowrap: false,
            fitColumns: true,
            columns: [[
                { field: 'no_cuenta', title: 'No Cuenta', align: 'right', width: 50 },
                { field: 'sector', title: 'Sector', align: 'right', width: 50 },
                { field: 'reparto', title: 'Reparto', align: 'right', width: 50 },
                { field: 'usuario', title: 'Usuario', align: 'left', width: 150 },
                { field: 'domicilio', title: 'Domicilio', align: 'left', width: 150 }, 
                                
                { field: 'lectura_actual_anterior', title: 'Lec.Ant Actual', align: 'right', width: 50 },
                { field: 'lectura_actual_actual', title: 'Lec.Act', align: 'right', width: 50 },
                { field: 'consumo_actual', title: 'Cons. Actual', align: 'right', width: 50 } ,
                
                { field: 'lectura_anterior_anterior', title: 'Lec. Ant ', align: 'right', width: 50 },
                { field: 'lectura_anterior_actual', title: 'Lec. Act. Ant.', align: 'right', width: 50 },
                { field: 'consumo_anterior', title: 'Cons. Anterior', align: 'right', width: 50 },  
                    
                { field: 'validacion', title: 'Nueva Lectura', align: 'right', width: 50, hidden: true,
                    editor: {
                        type: 'numberbox',
                        options: {
                            valueField: 'validacion',
                            precision: 0
                        }
                    }
                },
                { field: 'Comentario_Validacion', title: 'Comentarios', align: 'left', width: 50, editor: 'text', hidden: true },
                
                { field: 'action', title: 'Acción', width: 70, align: 'center', hidden: true,
                    formatter: function (value, row, index) {
                        if (row.editing) {
                            var a = '<img onclick="saverow(' + index + ',' + reporte + ',' + row.no_cuenta + ')" src="../imagenes/paginas/accept.png" class="Img_Button" alt="Aplicar Lectura" style="cursor:pointer;width:24px;height:24px;"/>';
                            var b = '<img onclick="cancelrow(' + index + ',' + reporte + ')" src="../imagenes/paginas/delete.png" class="Img_Button" alt="Aplicar Lectura" style="cursor:pointer;width:24px;height:24px;"/>';
                            return a + b;
                        } else {
                            var c = '<img onclick="editrow(' + index + ',' + reporte + ')" src="../imagenes/gridview/grid_update.png" class="Img_Button" alt="Aplicar Lectura" style="cursor:pointer;width:16px;height:16px;"/>';
                            return c;
                        }
                    }
                },
                { field: 'medidor_detalle_id', title:'me', width:0, hidden: true},
                { field: 'predio_id', title:'pre', width:0,hidden: true }
            ]],
            onLoadSuccess: function (data) {
            var nombreGrid;
            nombreGrid= "#Grid_Lecturas_" + reporte;
            var Result_Consult= $(nombreGrid).datagrid('getRows');
            if (Result_Consult.length ==0){
                $.messager.show({
	                title:'Información Adicional',
	                msg:'No se encontraron resultados para esta busqueda.',
	                timeout:5000,
	                showType:'slide'
                    });
                }
            },
            onBeforeEdit: function (index, row) {
                row.editing = true;
                updateActions(reporte);
            },
            onAfterEdit: function (index, row) {
                row.editing = false;
                updateActions(reporte); //editrow(index, reporte);
            },
            onCancelEdit: function (index, row) {
                row.editing = false;
                updateActions(reporte); //cancelrow(index, reporte);
            },
            onBeforeLoad: function (param) {
                return;
            }
        });
        
    } catch (ex) {
        Mostrar_Mensaje_Error_Modal(reporte, true, 'Error: (Llena_Reporte_7_8): ' + ex.Description);
    }
}

function Llena_Reportes_9(reporte,Region_ID, Mes, Anio) {
    try {
        $("#Grid_Lecturas_9").datagrid({
            title: '',
            iconCls: 'icon-blank',
            width: 1700,
            height: 400,
            url: '../../Controladores/Frm_Controlador_Validacion_Lecturas.aspx?Tipo_Consulta=Grid_Lecturas&Tipo_Reporte=9&Region_ID=' + Region_ID + '&Mes=' + Mes + '&Anio=' + Anio,
            idField: 'No_Cuenta',
            frozenColumns: null,
            pagination: true,
            rownumbers: false,
            loadMsg: 'Cargando datos',
            pageSize: 25,
            singleSelect: true,
            striped: true,
            nowrap: false,
            fitColumns: true,
            columns: [[
                { field: 'no_cuenta', title: 'No Cuenta', align: 'right', width: 50 },
                { field: 'fecha', title: 'Fecha', align: 'center', width: 50 },
                { field: 'domicilio', title: 'Domicilio', align: 'left', width: 50 },
                { field: 'numero_exterior', title: 'Ext', align: 'right', width: 50 },
                { field: 'numero_interior', title: 'Int', align: 'right', width: 50 },
                { field: 'colonia', title: 'Colonia', align: 'left', width: 75 },
                { field: 'consumo', title: 'Cons.', align: 'right', width: 50, hidden: true },
                { field: 'lecturista', title: 'Lecturista', align: 'left', width: 50 },
                { field: 'observaciones', title: 'Observaciones', align: 'left', width: 50 },
                { field: 'medidor_detalle_id', hidden: true },
                { field: 'predio_id', hidden: true }
            ]],
            onLoadSuccess: function (data) {
            var nombreGrid;
            nombreGrid= "#Grid_Lecturas_" + reporte;
            var Result_Consult= $(nombreGrid).datagrid('getRows');
            if (Result_Consult.length ==0){
                $.messager.show({
	                title:'Información Adicional',
	                msg:'No se encontraron resultados para esta busqueda.',
	                timeout:5000,
	                showType:'slide'
                    });
                }
            },
            onBeforeEdit: function (index, row) {
                row.editing = true;
                updateActions(reporte);
            },
            onAfterEdit: function (index, row) {
                row.editing = false;
                updateActions(reporte); //editrow(index, reporte);
            },
            onCancelEdit: function (index, row) {
                row.editing = false;
                updateActions(reporte); //cancelrow(index, reporte);
            },
            onBeforeLoad: function (param) {
                return;
            }
        });
    } catch (ex) {
        Mostrar_Mensaje_Error_Modal(9, true, 'Error: (Llena_Reportes_9): ' + ex.Description);
    }
}

function Llena_Reportes_10(reporte, Region_ID, Mes, Anio) {
    //Declaracion de variables
    var nombreGrid = ""; //variable para el nombre del grid
    var consumo;
    try {
        //Colocar el nombre del reporte
        nombreGrid = "#Grid_Lecturas_" + reporte;
//        $('#Txt_Consumo').value=105;
        var v = $("#Txt_Consumo").val();
        $(nombreGrid).datagrid({
            title: '',
            iconCls: 'icon-blank',
            width: 1700,
            height: 400,
            url: '../../Controladores/Frm_Controlador_Validacion_Lecturas.aspx?Tipo_Consulta=Grid_Lecturas&Tipo_Reporte=' + reporte + '&Region_ID=' + Region_ID + '&Mes=' + Mes + '&Anio=' + Anio + '&Consumo=' + v,
            idField: 'No_Cuenta',
            frozenColumns: null,
            pagination: true,
            rownumbers: false,
            loadMsg: 'Cargando datos',
            pageSize: 10,
            singleSelect: true,
            striped: true,
            nowrap: false,
            fitColumns: true,
            columns: [[
                { field: 'no_cuenta', title: 'No Cuenta', align: 'right', width: 25 },
                { field: 'nombre_usuario', title: 'Usuario', align: 'right', width: 100 },
                { field: 'calle', title: 'Calle', align: 'right', width: 100 },
                { field: 'numero_exterior', title: 'Numero Ext.', align: 'left', width: 25 },
                { field: 'numero_interior', title: 'Numero Int.', align: 'left', width: 25 },             
                { field: 'colonia', title: 'Colonia', align: 'right', width: 50 },
                { field: 'lectura_actual', title: 'Lectura Act.', align: 'right', width: 25 },
                { field: 'lectura_anterior', title: 'Lectura Ant.', align: 'right', width: 25 },
                { field: 'consumo', title: 'Consumo', align: 'right', width: 25 },
                { field: 'no_medidor', title: 'No. Medidor', align: 'right', width: 50 },
                { field: 'medidor_detalle_id', title: 'Medidor Detalle', align: 'right', width: 50, hidden: true },
                { field: 'predio_id', title: 'Predio ID', align: 'right', width: 50, hidden: true }
                
//                { field: 'action', title: 'Acción', width: 70, align: 'center', hidden: true,
//                    formatter: function (value, row, index) {
//                        if (row.editing) {
//                            var a = '<img onclick="saverow(' + index + ',' + reporte + ',' + row.no_cuenta + ')" src="../imagenes/paginas/accept.png" class="Img_Button" alt="Aplicar Lectura" style="cursor:pointer;width:24px;height:24px;"/>';
//                            var b = '<img onclick="cancelrow(' + index + ',' + reporte + ')" src="../imagenes/paginas/delete.png" class="Img_Button" alt="Aplicar Lectura" style="cursor:pointer;width:24px;height:24px;"/>';
//                            return a + b;
//                        } else {
//                            var c = '<img onclick="editrow(' + index + ',' + reporte + ')" src="../imagenes/gridview/grid_update.png" class="Img_Button" alt="Aplicar Lectura" style="cursor:pointer;width:16px;height:16px;"/>';
//                            return c;
//                        }
//                    }
//                }
            ]],
            onLoadSuccess: function (data) {
                var nombreGrid;
                nombreGrid= "#Grid_Lecturas_" + reporte;
                var Result_Consult= $(nombreGrid).datagrid('getRows');
                if (Result_Consult.length ==0){
                    $.messager.show({
	                    title:'Información Adicional',
	                    msg:'No se encontraron resultados para esta busqueda.',
	                    timeout:5000,
	                    showType:'slide'
                        });
                    }
            },
            onBeforeEdit: function (index, row) {
                row.editing = true;
                updateActions(reporte);
            },
            onAfterEdit: function (index, row) {
                row.editing = false;
                updateActions(reporte); //editrow(index, reporte);
            },
            onCancelEdit: function (index, row) {
                row.editing = false;
                updateActions(reporte); //cancelrow(index, reporte);
            },
            onBeforeLoad: function (param) {
                return;
            }
        });
        
    } catch (ex) {
         Mostrar_Mensaje_Error_Modal(reporte, true, 'Error: (Llena_Reporte_6): ' + ex.Description);
    }
}

function updateActions(reporte) {
    //Declaracion de variables
    var nombreGrid = "#Grid_Lecturas_" + reporte;
    var rowcount = 0; //variable para el total del los renglones

    try {
        //Obtener el total del los renglones
        rowcount = $(nombreGrid).datagrid('getRows').length;
        for (var i = 0; i < rowcount; i++) {
            $(nombreGrid).datagrid('updateRow', {
                index: i,
                row: { action: '' }
            });
        }
    } catch (ex) {
        Mostrar_Mensaje_Error_Modal(reporte, true, 'Error: (updateActions): ' + ex.Description);
    }
}

function editrow(index, reporte) {
    //Declaracion de variables
    var nombreGrid = "#Grid_Lecturas_" + reporte;
    try {
        $(nombreGrid).datagrid('beginEdit', index);
    } catch (ex) {
        Mostrar_Mensaje_Error_Modal(reporte, true, 'Error: (editrow): ' + ex.Description);
    }
}

function saverow(index, reporte, No_Cuenta) {
    //Declaracion de variables
    var nombreGrid = "#Grid_Lecturas_" + reporte;

    try {
        //Finaliza la edición de las cajas
        $(nombreGrid).datagrid('endEdit', index);
        //modifica el consumo lectura validado - inicial
        //Agrega dinamicamente un editor al consumo
        //Consumo
        var Nombre_Campo_Consumo='consumo';
        var Nombre_Campo_lectura='lectura_anterior';
        if (reporte==7 || reporte==8){
            Nombre_Campo_Consumo='consumo_actual';
            Nombre_Campo_lectura='lectura_actual_anterior';
        }
        var consumo = $(nombreGrid).datagrid('getColumnOption', Nombre_Campo_Consumo);
        consumo.editor = { type: 'numberbox', options: { valueField: Nombre_Campo_Consumo} };
        //Lectura Inicial
        var lect = $(nombreGrid).datagrid('getColumnOption', Nombre_Campo_lectura);
        lect.editor = { type: 'numberbox', options: { valueField: Nombre_Campo_lectura} };
        //var lect_cantidad = lect.target.val()
        //Obtiene los valores de los campos de edición
        $(nombreGrid).datagrid('beginEdit', index);
        var Nueva_Lectura = $(nombreGrid).datagrid('getEditor', { index: index, field:'validacion' });
        var Consumo_Actual = $(nombreGrid).datagrid('getEditor', { index: index, field: Nombre_Campo_Consumo });
        var Lectura_Anterior = $(nombreGrid).datagrid('getEditor', { index: index, field: Nombre_Campo_lectura });
        var consumo_nuevo
        setTimeout(function () {
        
        if (reporte==8){
//            Lectura_Anterior.terget.val(Nueva_Lectura);
            consumo_nuevo = Consumo_Actual.target.val() - Nueva_Lectura.target.val();
            Consumo_Actual.target.val(consumo_nuevo);
            $(nombreGrid).datagrid('endEdit', index);
        }
        else{
            consumo_nuevo = Nueva_Lectura.target.val() - Lectura_Anterior.target.val();
            Consumo_Actual.target.val(consumo_nuevo);
            $(nombreGrid).datagrid('endEdit', index);
            //quita el editor
        }
            //Consumo
            var consumo = $(nombreGrid).datagrid('getColumnOption', Nombre_Campo_Consumo);
            consumo.editor = {};
            //Lectura Inicial
            var lect = $(nombreGrid).datagrid('getColumnOption', Nombre_Campo_lectura);
            lect.editor = {};
            //Despues de editar la información la envia al servidor
            //recupera la información del grid para enviarlo y actualizarlo en el servidor
            var registro = JSON.stringify($(nombreGrid).datagrid('getRows'));
            var tipo_Consulta='Validacion_Lectura';                
            $.ajax({
                url: '../../Controladores/Frm_Controlador_Validacion_Lecturas.aspx?Tipo_Consulta=' + tipo_Consulta + '&No_Cuenta=' + No_Cuenta + '&Informacion=' + registro +  '&Tipo_Reporte=' + reporte,
                method: 'POST',
                dataType: 'text',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data != null) {
                        //$.each(data, function (i, item) {
                        var error = data[1].error;
                        var alta = data[0].alta; ;
                        if (error != "") {
                            $.messager.show({
	                            title:'Información Adicional',
	                            msg:error,
	                            timeout:5000,
	                            showType:'slide'
                            });
                        }
                        else {
                            $.messager.show({
	                            title:'Información Adicional',
	                            msg:alta,
	                            timeout:3000,
	                            showType:'slide'
                                });
                        }
                    }
                },
                beforeSend: function () {
                    $('#img_progress_' + reporte).show();
                },
                complete: function () {
                    $('#img_progress_' + reporte).hide().delay(1000);
                },
                error: function () {
                    $('#img_progress_' + reporte).hide().delay(1000);
                }
            });
        }, 500);
        
    } catch (ex) {
        Mostrar_Mensaje_Error_Modal(reporte, true, 'Error: (saverow): ' + ex.Description);
    }
}

function cancelrow(index, reporte) {
    //Declaracion de variables
    var nombreGrid = "#Grid_Lecturas_" + reporte;

    try {
        $(nombreGrid).datagrid('cancelEdit', index);
    } catch (ex) {
        Mostrar_Mensaje_Error_Modal(reporte, true, 'Error: (cancelrow): ' + ex.Description);
    }
}

//function validacionLecturas(reporte) {
//    //Declaracion de variables
//    var cadenaValidacion = ""; //Cadena para las validaciones

//    try {
//        cadenaValidacion = construyeDatos(reporte);

//        //Realizar la peticion de AJAX
//        $.ajax({
//            url: '../../Controladores/Frm_Controlador_Validacion_Lecturas.aspx?Tipo_Consulta=Validacion_Lecturas&Validacion_Lecturas=' + cadenaValidacion,
//            method: 'POST',
//            dataType: 'text',
//            contentType: "application/json; charset=utf-8",
//            dataType: "json",
//            success: function (response) {
//                try {
//                    //Mandar mensaje del archivo dado de alta
//                    $.messager.alert('Validaci&oacute;n Lecturas', "La validaci&oacute;n de las lecturas ha sido realizada de manera satisfactoria");

//                    //Cerrar el modal
//                    Mostrar_Ventanas_Modales(reporte, 'close');

//                    //Colocar la pagina en un estado inicial
//                    Estado_Inicial();
//                } catch (ex) {
//                    $("[id$='Lbl_Mensaje_Error']").html('Error (validacionLecturas): ' + ex.Description);
//                    Mostrar_Mensaje_Error(true);
//                }
//            },
//            error: function (result) {
//                try {
//                    $("[id$='Lbl_Mensaje_Error']").html('Error (validacionLecturas): ' + result.status + ' ' + result.statusText);
//                    Mostrar_Mensaje_Error(true);
//                    //alert('ERROR: ' + result.status + ' ' + result.statusText);
//                } catch (ex) {
//                    $("[id$='Lbl_Mensaje_Error']").html('Error (validacionLecturas): ' + ex.Description);
//                    Mostrar_Mensaje_Error(true);
//                }
//            }
//        });
//    } catch (ex) {
//        $("[id$='Lbl_Mensaje_Error']").html('Error (validacionLecturas): ' + ex.Description);
//        Mostrar_Mensaje_Error(true);
//    }
//}

//function construyeDatos(reporte) {
//    //Declaracion de variables
//    var nombreGrid = "#Grid_Lecturas_" + reporte;
//    var renglones; //variable para los renglones
//    var resultado = ""; //variable para el resultado
//    var contElementos = 0; //variable para el contador
//    var renglonesValidados; //variable para los renglones validados
//    var consumo = 0; //variable para el consumo

//    try {
//        //obtener los renglones
//        renglones = $(nombreGrid).datagrid('getRows');

//        //ciclo para finalizar la edicion
//        for (contElementos = 0; contElementos < renglones.length; contElementos++) {
//            $(nombreGrid).datagrid('endEdit', contElementos);
//        }

//        //obtener los renglones validados
//        renglonesValidados = $(nombreGrid).datagrid('getRows');

//        //ciclo para construir el resultado
//        for (contElementos = 0; contElementos < renglonesValidados.length; contElementos++) {
//            //verificar si se tiene que tomar en cuenta
//            if (renglonesValidados[contElementos].validacion != "" && renglonesValidados[contElementos].validacion != null && renglonesValidados[contElementos].comentario_validacion != "" && renglonesValidados[contElementos].comentario_validacion != null) {
//                //calcular el consumo
//                consumo = Number(renglonesValidados[contElementos].validacion) - Number(renglonesValidados[contElementos].lectura_anterior);
//                resultado += renglonesValidados[contElementos].medidor_detalle_id + "_" + renglonesValidados[contElementos].predio_id + "_" + renglonesValidados[contElementos].validacion + "_" + renglonesValidados[contElementos].comentario_validacion + "_" + renglonesValidados[contElementos].no_cuenta + "_" + consumo + "|";
//            }
//        }

//        //Entregar resultado
//        return resultado;
//    } catch (ex) {
//        $("[id$='Lbl_Mensaje_Error']").html('Error (validacionLecturas): ' + ex.Description);
//        Mostrar_Mensaje_Error(true);
//    }
//}
