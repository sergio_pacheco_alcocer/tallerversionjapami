﻿
var  controles;
var  no_cuenta;
var  usuario;
$(function(){


   // inicializamos fechas
$("[id$='f_inicio']").datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'dd/mm/yy'
});


$("[id$='f_final']").datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'dd/mm/yy'
});


crearTabla();

cargarAutomaticamenteDatos();

});

//-------------------------------------------------------------------------------------

function cargarAutomaticamenteDatos(){
     
   no_cuenta= $("[id$='txt_no_cuenta_informacion']").val(); 
   $('#tabla_facturas').datagrid('loadData', { total: 0, rows: [] });   
   controles= toJSON($('#datos :input').serializeArray()); 
    $('#tabla_facturas').datagrid({ url: '../../Controladores/Frm_Controlador_Consulta_Usuario.aspx', queryParams: {
         accion: 'historico_facturas',
         controles:controles,
         nocuenta:no_cuenta
     }, pageNumber: 1
     });

}


//----------------------------------------------------------------------------------------

function crearTabla() {

    //incio tabla
    $('#tabla_facturas').datagrid({
        title: 'Hist&oacute;rico de Facturas ',
        //view: detailview,
        detailFormatter: function(index, row) {
            return '<div style="padding:2px"><table id="ddv-' + index + '"></table></div>';
        },
        width: 950,
        height: 400,
        columns: [[
           { field: 'no_factura_recibo', title: 'idS', width: 50 },
	       { field: 'no_cuenta', title: 'No. Cuenta', width: 100},
	       { field: 'no_recibo', title: 'No Recibo', width: 100 },
	       { field: 'periodo_facturacion', title: 'P.Facturacion', width: 120 },
	       { field: 'facturado', title: 'Facturado', width: 120 },
	       { field:  'pagado',title:'Pagado', width:120 },
	       { field: 'saldo', title: 'Saldo', width: 120 },
	       { field: 'lectura_actual', title: 'Lec. Actual', width: 140 },
	       { field: 'lectura_anterior', title: 'Lec. Anterior', width: 140 },
	       { field: 'consumo', title: 'Consumo', width: 120 },
	       { field: 'agua', title: 'Agua', width: 120 },
	       { field: 'drenaje', title: 'Drenaje', width: 120 },
	       { field: 'saneamiento', title: 'Saneamiento', width: 120 },
	       { field: 'rezagos', title: 'Rezagos', width: 120 },
	       { field: 'recargos', title: 'Recargos', width: 120 },
	       { field: 'tasa_iva', title: 'Tasa IVA', width: 120 },
	       { field: 'total_iva', title: 'Total IVA', width: 120 },
           { field: 'estado_recibo', title: 'Estado', width: 120 },
           { field: 'fecha_creo', title: 'Fecha Emisi&oacute;n', width: 120 }
           
    ]],
        pageSize: 50,
        pagination: true,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        striped: true,
        nowrap: true,
        onExpandRow: function(index, row) {
            $('#ddv-'+index).datagrid({  
            url:'../../Controladores/Frm_Controlador_Consulta_Usuario.aspx?accion=historico_facturas_detalles&no_factura_recibo='+row.no_factura_recibo,  
            fitColumns:false,  
            singleSelect:true,
            nowrap: true,  
            rownumbers:true,  
            loadMsg:'',  
            height:150,
            width:700,  
            columns:[[  
                 { field: 'concepto', title: 'Concepto', width: 200, sortable: true },
	             { field: 'total', title: 'Total', width: 150, sortable: true },
	             { field: 'total_abonado', title: 'Abonado', width: 150, sortable: true },
	             { field: 'total_saldo', title: 'Saldo', width: 150, sortable: true }
            ]],  
            onResize:function(){  
                $('#tabla_facturas').datagrid('fixDetailRowHeight',index);  
            },  
            onLoadSuccess:function(){  
                setTimeout(function(){  
                    $('#tabla_facturas').datagrid('fixDetailRowHeight',index);  
                },0);  
            }  
        });
            
            $('#tabla_facturas').datagrid('fixDetailRowHeight', index);
        }

    });

     // ocultar columna
    $('#tabla_facturas').datagrid('hideColumn', 'no_factura_recibo');


}

//-------------------------------------------------------------------------------------------------------

function buscarInformacion(){

  $('#tabla_facturas').datagrid('loadData', { total: 0, rows: [] });  
  no_cuenta= $("[id$='txt_no_cuenta_informacion']").val();
  usuario=$("[id$='txt_usuario']").val();
  
 
   if (validarFechas($("#f_inicio"), $("#f_final"))) {
      return;
    }

   controles= toJSON($('#datos :input').serializeArray()); 
    $('#tabla_facturas').datagrid({ url: '../../Controladores/Frm_Controlador_Consulta_Usuario.aspx', queryParams: {
         accion: 'historico_facturas',
         controles:controles,
         nocuenta:no_cuenta
     }, pageNumber: 1
     });


}

//-------------------------------------------------------------------------------------------------------
function exportarExcel(){
var renglones ;

renglones=$('#tabla_facturas').datagrid('getRows');
 
if(renglones.length==0){
   $.messager.alert('Japami','No hay Elementos que Exportar');
   return ;
  }
   
   
 window.location = "../../Reporte/Frm_controlador_excel.aspx?accion=reporte_facturas&controles=" + controles +"&nocuenta="+no_cuenta + "&nombre=" + usuario;  

}
