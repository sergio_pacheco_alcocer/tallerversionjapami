﻿
$(function(){
   principal();

});

//----------------------------------------------------------------------------------

function principal(){
  crearBoton();
  crearEventoDiv();
  subirArchivo();
  eventoslink();  
}

//----------------------------------------------------------------------------------

function eventoslink(){

 $('.fotos').click(function(event){
     var no_detalle_foto;

     no_detalle_foto=$(this).attr('no_estudio_detalle');
     window.open("../../Controladores/frm_visor_archivos.aspx?accion=estudios_socioeconimicos&no_detalle_foto=" + no_detalle_foto , "VisorImagenes", "width=300px,height=100px,scrollbars=NO"); 
     
     event.preventDefault();
     
 });

 
}


//----------------------------------------------------------------------------------

function crearEventoDiv(){

// este evento es para crear el file upload
$("[id$='TabPanel_fotos']").click(function(){   
    subirArchivo();
});

}

//----------------------------------------------------------------------------
function crearBoton(){
  $('#btn_buscar_archivo').linkbutton();
}

//----------------------------------------------------------------------------
function eventoBotonBuscar(){

    $('#btn_buscar_archivo').click(function(){
        //subirArchivo();
    });

}


//----------------------------------------------------------------------------

function subirArchivo(){

var boton;

boton=$('#btn_buscar_archivo').attr('id');

if (boton !=undefined){

  new AjaxUpload('#btn_buscar_archivo', {
		action: '../../Controladores/frm_controlador_subir_archivo.aspx', 
		data : {accion:'subir_archivo'},
		onSubmit : function(file , ext){
		      
		    if (!(ext && /^(jpg|png)$/i.test(ext))) {
                $.messager.alert('Japami', 'Formato Incorrecto');
                return false;
            }  
		    
		   $("[id$='txt_nombre_archivo']").val('');
		   $("[id$='txt_archivo_foto_escondido']").val('');
		   
		   
			$('#ventana_mensaje').window('open');
	
		},
		onComplete : function(file){
		   $("[id$='txt_nombre_archivo']").val(file);
		   $("[id$='txt_archivo_foto_escondido']").val(file);
		   $('#ventana_mensaje').window('close');
		   
		     
		}		
	});
  
  }	

}

//----------------------------------------------------------------------------



