﻿

var objdatos_usuario;
var concepto_agua;
var concepto_drenaje;
var concepto_saniamiento;
var concepto_rezagos;
var concepto_recargos;
var objRenglones_modelo;
var pagoCliente;
var aplicarPagoRezago;
var aplicarPagoRecargo;
var t1,t2;
var index;
var total;
var strDetallesConceptos;
var strTotales;
var porcentaje_minimo;
var objConceptos_id;
var no_factura_recibo_parcial="";


$(function (){

crearTabla();
objConceptos_id=json_parse($("[id$='txt_conceptos_id_h']").val() ); 
$('#btnImprimirgridpagoparcial').linkbutton('disable');   

$("[id$='txt_cantidad_parcial']").blur(function(){
   pagoCliente= parseFloat( $(this).val());
   realizarCalculo();
   
});


});
//----------------------------------------------------------------------------

function crearTabla(){

  $('#tabla_factura').datagrid({
        title: 'Factura',
        width: 785,
        height: 300,
        columns: [[
	                     { field: 'no_factura_recibo', title: 'id', width: 1, sortable: true },
	                     { field: 'tasa_iva', title: 'tasa_iva', width: 1, sortable: true },
	                     { field: 'concepto_id', title: 'concepto_id', width: 1, sortable: true },
	                     { field: 'concepto', title: 'Concepto', width: 250, sortable: true },
	                     { field: 'total_saldo', title: 'Saldo debe', width: 200, sortable: true },
	                     { field: 'nuevo_saldo', title: 'Saldo Paga', width:200 , sortable: true }
	              ]],
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        showFooter: true,
        striped: true,
        loadMsg: 'Cargando Datos',
        nowrap: false,
        toolbar: [{
            id: 'btnImprimirgridpagoparcial',
            text: 'Imprimir',
            disabled:true,
            iconCls: 'icon-print',
            handler: function() {
               imprimirReciboParcial();  
            }
           

}]


        });
        
        $('#tabla_factura').datagrid('hideColumn','no_factura_recibo');
        $('#tabla_factura').datagrid('hideColumn','concepto_id');
        $('#tabla_factura').datagrid('hideColumn','tasa_iva');




}

//----------------------------------------------------

function cerrarVentanaParcial(){
$('#ventana_pago_parcial').window('close');
}

//----------------------------------------------------
function ponerEnCeroGrid(){
 var datos;
 
datos=$('#tabla_factura').datagrid('getData');

for(var i=0;i<datos.rows.length;i++){
   datos.rows[i].nuevo_saldo=0;
}

datos.footer[0].nuevo_saldo=0;
$('#tabla_factura').datagrid('loadData',datos);

}


//-----------------------------------------------------

function obtenerTotal(renglones){
  var renglones;
  var renglon;
  var suma=0;
    
  for(var i=0 ;i<renglones.length; i++){        
       renglon=renglones[i];
       suma= suma + parseFloat(renglon.nuevo_saldo);
          
  }
  
  return suma;

}


//-----------------------------------------------------

function obtenerSumaConcepto(codigo){
var renglones;


 var renglones;
  var renglon;
  var suma=0;
  
  renglones=$('#tabla_factura').datagrid('getRows');
  
  for(var i=0 ;i<renglones.length; i++){        
       renglon=renglones[i];
       if(renglon.concepto_id==codigo){
          suma= suma + parseFloat(renglon.total_saldo);
          }
  }
  
  return suma;
}

//-----------------------------------------------------

function obtenerIndex(datos,id_conceptos){
 var renglon;
 var index=0;
for(var i=0;i<datos.length;i++){
   renglon=datos[i];
     if(renglon.concepto_id==id_conceptos){
      index=i;
      return index;
    }
}

return index;

}

//-----------------------------------------------------
function realizarCalculo(){

  var renglones;
  var pies;
  var totales;

 renglones=$('#tabla_factura').datagrid('getRows');

 
  if(renglones.length==0){
      $.messager.alert('Japami', 'No se hay datos suficientes para esta operación', 'error');
      return ;
  }
  
 pies=$('#tabla_factura').datagrid('getFooterRows');
 totales=parseFloat(pies[0].total_saldo); 
  
  
   if( isNaN(pagoCliente) || pagoCliente==0 ){
      $.messager.alert('Japami', 'El Pago es incorrecto', 'error');
      return ; 
  }
  
   if(pagoCliente>=totales){
     $.messager.alert('Japami', 'El pago no debe ser mayo a la deuda', 'error');
      return ;
  }
 

  ponerEnCeroGrid();
  
  concepto_agua=obtenerSumaConcepto(objConceptos_id.concepto_agua_id);
  concepto_drenaje=obtenerSumaConcepto(objConceptos_id.concepto_drenaje_id);
  concepto_saniamiento=obtenerSumaConcepto(objConceptos_id.concepto_saneamiento_id);
  concepto_rezagos=obtenerSumaConcepto(objConceptos_id.concepto_rezago_id);
  concepto_recargos=obtenerSumaConcepto(objConceptos_id.concepto_recargo_id);

  objdatos_usuario= $('#tabla_factura').datagrid('getData');
     
    if(concepto_rezagos>0 && concepto_recargos>0){
         t1=concepto_rezagos + concepto_recargos;
         // si el pago es menor que la suma de los recargos y rezagos
         if (pagoCliente<t1) {
             t2=concepto_recargos/t1;   
             aplicarPagoRecargo= decimal( pagoCliente * t2,2);
             aplicarPagoRezago= decimal( pagoCliente-aplicarPagoRecargo,2);
             index=obtenerIndex(objdatos_usuario.rows,objConceptos_id.concepto_rezago_id);
             objdatos_usuario.rows[index].nuevo_saldo=aplicarPagoRezago;
             index=obtenerIndex(objdatos_usuario.rows,objConceptos_id.concepto_recargo_id);
             objdatos_usuario.rows[index].nuevo_saldo=aplicarPagoRecargo; 
             total=decimal( obtenerTotal(objdatos_usuario.rows),2);
             objdatos_usuario.footer[0].nuevo_saldo=total;             
             $('#tabla_factura').datagrid('loadData',objdatos_usuario);
         }else{
             index=obtenerIndex(objdatos_usuario.rows,objConceptos_id.concepto_rezago_id);
             objdatos_usuario.rows[index].nuevo_saldo=objdatos_usuario.rows[index].total_saldo;
             pagoCliente=decimal( pagoCliente-objdatos_usuario.rows[index].total_saldo,2);
             index=obtenerIndex(objdatos_usuario.rows,objConceptos_id.concepto_recargo_id);
             objdatos_usuario.rows[index].nuevo_saldo=objdatos_usuario.rows[index].total_saldo;
             pagoCliente=decimal( pagoCliente-objdatos_usuario.rows[index].total_saldo,2); 
             calcularSaldoSintomarEncuentaRezagosyRecargos(pagoCliente,objdatos_usuario);
        
         }//fin pago a cliente t1
     
  }// fin de los conceptos_rezagos and conceptos_recargos
  else{
        calcularSaldoSintomarEncuentaRezagosyRecargos(pagoCliente,objdatos_usuario);
    }

   
   
   
}

//-------------------------------------------------------

function calcularSaldoSintomarEncuentaRezagosyRecargos(pagoCliente,objdatos_usuario){
var saldo_concepto;

 if(pagoCliente>concepto_agua){
      index=obtenerIndex(objdatos_usuario.rows,objConceptos_id.concepto_agua_id);
      objdatos_usuario.rows[index].nuevo_saldo=objdatos_usuario.rows[index].total_saldo;
      pagoCliente=decimal( pagoCliente-objdatos_usuario.rows[index].total_saldo,2);
      
 }else
  {
     index=obtenerIndex(objdatos_usuario.rows,objConceptos_id.concepto_agua_id);
      objdatos_usuario.rows[index].nuevo_saldo=pagoCliente;
      pagoCliente=0;
      total= decimal( obtenerTotal(objdatos_usuario.rows),2);
      objdatos_usuario.footer[0].nuevo_saldo=total;
      $('#tabla_factura').datagrid('loadData',objdatos_usuario);
      return;
   }
     
 if(pagoCliente>concepto_drenaje){
      index=obtenerIndex(objdatos_usuario.rows,objConceptos_id.concepto_drenaje_id);
      objdatos_usuario.rows[index].nuevo_saldo=objdatos_usuario.rows[index].total_saldo;
      pagoCliente=decimal( pagoCliente-objdatos_usuario.rows[index].total_saldo,2);
      
 }else
  {
     index=obtenerIndex(objdatos_usuario.rows,objConceptos_id.concepto_drenaje_id);
      objdatos_usuario.rows[index].nuevo_saldo=pagoCliente;
      pagoCliente=0;
      total=decimal( obtenerTotal(objdatos_usuario.rows),2);
      objdatos_usuario.footer[0].nuevo_saldo=total;
      $('#tabla_factura').datagrid('loadData',objdatos_usuario);
      return;
   }
   
      
  if(pagoCliente>concepto_saniamiento){
      index=obtenerIndex(objdatos_usuario.rows,concepto_saneamiento_id);
      objdatos_usuario.rows[index].nuevo_saldo=objdatos_usuario.rows[index].total_saldo;
      pagoCliente=decimal( pagoCliente-objdatos_usuario.rows[index].total_saldo,2);
      
 }else
  {
     index=obtenerIndex(objdatos_usuario.rows,concepto_saneamiento_id);
      objdatos_usuario.rows[index].nuevo_saldo=pagoCliente;
      pagoCliente=0;
      total=decimal( obtenerTotal(objdatos_usuario.rows),2);
      objdatos_usuario.footer[0].nuevo_saldo=total;
      $('#tabla_factura').datagrid('loadData',objdatos_usuario);
      return;
   }
   

}// fin de la funcion
//----------------------------------------------------------------------------------

function guardarPagoParcial(){

var no_cuenta;
var t; 
var renglones;

 no_factura_recibo_parcial="";

renglones=$('#tabla_factura').datagrid('getRows');
if(renglones.length==0){
      $.messager.alert('Japami', 'No hay pagos asignados', 'error');
      return ;
}

strDetallesConceptos=toJSON( $('#tabla_factura').datagrid('getRows'));
strTotales=  $('#tabla_factura').datagrid('getFooterRows');
no_cuenta= $("[id$='txt_no_cuenta_parcial']").val();

t= parseFloat( strTotales[0].nuevo_saldo);

if( isNaN(t) || t==0){
   $.messager.alert('Japami', 'No hay pagos asignados', 'error');
      return ;
}

strTotales=toJSON($('#tabla_factura').datagrid('getFooterRows'));

$('#ventana_mensaje').window('open');  

 $.ajax({ //inicio
             type: "POST",
             url: "../../Controladores/frm_controlador_pagos_parciales.aspx/guadarFacturaParcial",
             data: "{'nocuenta':'" + no_cuenta + "','detalles_pagos':'" + strDetallesConceptos + "','totales':'" + strTotales + "'}",
             contentType: "application/json; charset=utf-8",
             dataType: "json",
             success: function(response) {
                 $('#ventana_mensaje').window('close'); 
                 var respuesta = eval("(" + response.d + ")");
                 if (respuesta.mensaje == "bien") {                   
                     procesoExitosoGuardar(respuesta)
                     
                 } else {
                    $('#ventana_mensaje').window('close');
                     $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                     
                 }

             },
             error: function(result) {
                 $('#ventana_mensaje').window('close');
                 alert("ERROR " + result.status + ' ' + result.statusText);

             }


         });  //fin peticion ajax

}

//----------------------------------------------------------------------------------


function procesoExitosoGuardar(respuesta){

 no_factura_recibo_parcial=respuesta.dato1;
 
$('#btnImprimirgridpagoparcial').linkbutton('enable');
$('#btn_guardar_dato_parcial').linkbutton({disabled:true});  

  $.messager.alert('Japami', 'Proceso Terminado', 'info',function(){   
      window.open("../../Reporte/frm_controlador_reporte_pdf.aspx?nombre_reporte=formato_recibo_parcial&no_factura_recibo_parcial=" + no_factura_recibo_parcial,"ReciboParcial","width=1000px,height=1000px,scrollbars=YES,resizable=YES");
           
  });

}

//----------------------------------------------------------------------------------

function imprimirReciboParcial(){
 
 no_factura_recibo_parcial= $.trim( no_factura_recibo_parcial);

 if (no_factura_recibo_parcial.length>0){
   window.open("../../Reporte/frm_controlador_reporte_pdf.aspx?nombre_reporte=formato_recibo_parcial&no_factura_recibo_parcial=" + no_factura_recibo_parcial,"ReciboParcial","width=1000px,height=1000px,scrollbars=YES,resizable=YES");
   }  
}


//----------------------------------------------------------------------------------

Number.prototype.decimal = function(num) {
    pot = Math.pow(10, num);
    return parseInt(this * pot) / pot;
}

function decimal(valor,no_decimales) {
    n = eval(valor);
    return n.decimal(no_decimales);
} 