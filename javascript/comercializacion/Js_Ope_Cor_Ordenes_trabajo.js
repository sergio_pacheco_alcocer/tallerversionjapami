﻿///*******************************************************************************
///NOMBRE DE LA FUNCIÓN : $(document).ready()
///DESCRIPCIÓN          : Funcion que indica cuando el documento ha sido cargado, para poder modificar los elementod DOM
///PARAMETROS           :   
///CREO                 : Julio Cruz
///FECHA_CREO           : 29/Octubre/2011 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///*******************************************************************************
function EjecutaFuncion() {
    $(document).ready(function() {
        jQuery.ajaxSetup({
            async: false,
            cache: false,
            timeout: (2 * 1000)//,
            //        beforeSend: function () {
            //            creaModal("Cargando... Espere por favor", "", true, 100);
            //        },
            //        complete: function () {
            //            destruyeModal(1000);
            //        },
            //        error: function (data) {
            //            alert(data);
            //            destruyeModal(1000);
            //        }
        });


        //Prepara la tabla
        $('#Grid_Predios').datagrid({
            title: 'Catalogo Predios',
            width: 700,
            height: 220,
            columns: [[
                    { field: 'boton', title: 'Sel', width: 20, align: 'center',
                        formatter: function(value, rec) {
                            return '<input type="button" id="btnRealizar_' + rec.Predio_ID + '" style="width:10px" AutoPostBack="true" onclick="Btn_Valor_ID(' + rec.Predio_ID + ',' + rec.No_Cuenta + ')" />';
                        }
                    },
                    { field: 'Predio_ID', title: 'Predio ID', width: 1, hidden: true },
                    { field: 'Nombre_Uuario', title: 'Nombre', width: 100, sortable: true },
                    { field: 'Domicilio', title: 'Domicilio', width: 300, sortable: true },
                    { field: 'No_Cuenta', title: 'No Cuenta', width: 80, sortable: true },
                    { field: 'Estatus', title: 'Estatus', width: 70, sortable: true }
            ]],
            pagination: true,
            rownumbers: true,
            idField: 'Predio_ID',
            sortOrder: 'asc',
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            loadMsg: "Cargando, espere ...",
            pageNumber: 1,
            pageSize: 10,
            fitColumns: true,
            striped: true,
            nowrap: false,
            pageList: [10, 20],
            remoteSort: false,
            showFooter: true
        });

        $('#Btn_Filtrar_Predios').click(function(event) {
            Filtrar_Predios();
            event.preventDefault();
        });

        AutocompletadoColonias();
        var colonia_id = $('#Txt_Busqueda_Colonia_Hidden').val();
        AutocompletadoCalles(colonia_id);
        AutocompletadoZonas();
        AutocompletadoGiros();
        AutocompletadoTarifas();
        AutocompletadoFraccionador();
        AutocompletadoViviendas();
        AutocompletadoNombre();


    });
}

///*******************************************************************************
///NOMBRE DE LA FUNCIÓN : Btn_Valor_ID
///DESCRIPCIÓN          : obtiene el predio y el numero de cuenta del usuario seleccionado
///PARAMETROS           :   
///CREO                 : Julio Cruz
///FECHA_CREO           : 6/Noviembre/2011 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///*******************************************************************************
function Btn_Valor_ID(Predio_ID, No_Cuenta) {
    //Se asigna la cuenta de Usuario    
    $("[id$='_Txt_Cuenta']").val(No_Cuenta);   
    //Se cierra el modalPopup
    ClosePopup();
}

function format(item) {
    return item.nombre;
}

function Filtrar_Predios() {
    var parametros = "";
    //Obtiene los filtros para mandarlos a consulta
    //Estatus
    parametros += "&Estatus=" + $('#Cmb_Busqueda_Estatus').val();
    //Zona
    parametros += "&Zona_ID=" + $('#Txt_Busqueda_Zona_Hidden').val();
    //Giro
    parametros += "&Giro_ID=" + $('#Txt_Busqueda_Giro_Hidden').val();
    //Colonia
    parametros += "&Colina_ID=" + $('#Txt_Busqueda_Colonia_Hidden').val();
    //Colonia nombre
    parametros += "&Colonia=" + $('#Txt_Busqueda_Colonia').val();
    //Calle id
    parametros += "&Calle_ID=" + $('#Txt_Busqueda_Calle_Hidden').val();
    //Calle Nombre
    parametros += "&Calle=" + $('#Txt_Busqueda_Calle').val();
    //Numeo exterior
    parametros += "&Numero_Exterior=" + $('#Txt_Busqueda_No_Exterior').val();
    //Tarifa
    parametros += "&Tarifa_ID=" + $('#Txt_Busqueda_Tarifa_Hidden').val();
    //Fraccionador
    parametros += "&Fraccionador_ID=" + $('#Txt_Busqueda_Fraccionador_Hidden').val();
    //Tipo Vivienda
    parametros += "&Tipo_Vivienda_ID=" + $('#Txt_Busqueda_Tipo_Vivienda_Hidden').val();
    //Nombre del dueño de la cuenta
    parametros += "&Nombre_Usuario=" + $('#Txt_Busqueda_Nombre').val();
    //Pagina Actual
    //parametros += getPager;
    //No registros por pagina
    $('#Grid_Predios').datagrid({
        url: '../../Controladores/Frm_Controlador_Predios.aspx?Accion=Filtrar_Predios' + parametros,
        pageNumber: 1
    });
}

function AutocompletadoColonias() {

    $('#Txt_Busqueda_Colonia_Hidden').val('');
    //Autocompletado de las Colonias
    $('#Txt_Busqueda_Colonia').autocomplete("../../Controladores/Frm_Controlador_Predios.aspx", {
        extraParams: { Accion: 'autocompletar_colonia' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.colonia_id,
                    result: row.colonia
                }
            });
        },
        formatItem: function (item) {
            return item.colonia;
        }
    }).result(function (e, item) {
        var colonia_id = item.colonia_id;
        $('#Txt_Busqueda_Colonia_Hidden').val(colonia_id);
    });
}

function AutocompletadoCalles(colonia_id) {
    $('#Txt_Busqueda_Calle_Hidden').val('');
    //Obtiene el valor de la colonia

    //Autocompletado de las calles por colonia seleccionada
    $('#Txt_Busqueda_Calle').autocomplete(
        "../../Controladores/Frm_Controlador_Predios.aspx", {
            selectOnly: true,
            minChars: 3,
            extraParams: {
                Accion: 'autocompletar_calle', colonia_id: colonia_id
            },
            dataType: "json",
            parse: function (data) {
                return $.map(data, function (row) {
                    return {
                        data: row,
                        value: row.calle_id,
                        result: row.calle
                    }
                });
            },
            formatItem: function (item) {
                return item.calle; // format(item);
            }
        }).result(function (e, item) {
            var calle_id = item.calle_id;
            $('#Txt_Busqueda_Calle_Hidden').val(calle_id);
            //alert("Calle_ID_" + $('#Txt_Busqueda_Calle_Hidden').val());
        });
}

function AutocompletadoZonas() {
    //Autocompletado de las Zonas
    $('#Txt_Busqueda_Zona').autocomplete("../../Controladores/Frm_Controlador_Predios.aspx", {
        extraParams: { Accion: 'autocompletar_zona' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.zona_id,
                    result: row.zona
                }
            });
        },
        formatItem: function (item) {
            return item.zona;
        }
    }).result(function (e, item) {
        var zona_id = item.zona_id;
        $('#Txt_Busqueda_Zona_Hidden').val(zona_id);
    });
}

function AutocompletadoGiros() {
    //Autocompletado de los giros
    $('#Txt_Busqueda_Giro').autocomplete("../../Controladores/Frm_Controlador_Predios.aspx", {
        extraParams: { Accion: 'autocompletar_giros' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.giro_id,
                    result: row.giro
                }
            });
        },
        formatItem: function (item) {
            return item.giro;
        }
    }).result(function (e, item) {
        var giro_id = item.giro_id;
        $('#Txt_Busqueda_Giro_Hidden').val(giro_id);
    });
}

function AutocompletadoTarifas() {
    //Autocompletado de las tarifas
    $('#Txt_Busqueda_Tarifa').autocomplete("../../Controladores/Frm_Controlador_Predios.aspx", {
        extraParams: { Accion: 'autocompletar_tarifa' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.tarifa_id,
                    result: row.tarifa
                }
            });
        },
        formatItem: function (item) {
            return item.tarifa;
        }
    }).result(function (e, item) {
        var tarifa_id = item.tarifa_id;
        $('#Txt_Busqueda_Tarifa_Hidden').val(tarifa_id);
    });
}
function AutocompletadoFraccionador() {
    //Autocompletado de los fraccionadores
    $('#Txt_Busqueda_Fraccionador').autocomplete("../../Controladores/Frm_Controlador_Predios.aspx", {
        extraParams: { Accion: 'autocompletar_fraccionador' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.constructora_id,
                    result: row.constructora
                }
            });
        },
        formatItem: function (item) {
            return item.constructora;
        }
    }).result(function (e, item) {
        var constructora_id = item.constructora_id;
        $('#Txt_Busqueda_Fraccionador_Hidden').val(constructora_id);
    });
}

function AutocompletadoViviendas() {
    //Autocompletado de los tipos de vivienda
    $('#Txt_Busqueda_Tipo_Vivienda').autocomplete("../../Controladores/Frm_Controlador_Predios.aspx", {
        extraParams: { Accion: 'autocompletar_tipo_vivienda' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.tipo_vivienda_id,
                    result: row.tipo
                }
            });
        },
        formatItem: function (item) {
            return item.tipo;
        }
    }).result(function (e, item) {
        var tipo_vivienda_id = item.tipo_vivienda_id;
        $('#Txt_Busqueda_Tipo_Vivienda_Hidden').val(tipo_vivienda_id);
    });
    }
    function AutocompletadoNombre() {

        $('#Txt_Busqueda_Nombre_Hidden').val('');
        //Autocompletado de las Colonias
        $('#Txt_Busqueda_Nombre').autocomplete("../../Controladores/Frm_Controlador_Predios.aspx", {
        extraParams: { Accion: 'autocompletar_nombre' },
            dataType: "json",
            selectOnly: true,
            minChars: 3,
            parse: function(data) {
                return $.map(data, function(row) {
                    return {
                        data: row,
                        value: row.usuario_id,
                        result: row.nombre
                    }
                });
            },
            formatItem: function(item) {
            return item.nombre;
            }
        }).result(function(e, item) {
        var usuario_id = item.usuario_id;
        $('#Txt_Busqueda_Nombre_Hidden').val(usuario_id);
        });
    }

