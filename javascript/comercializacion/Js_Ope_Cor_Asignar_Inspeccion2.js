﻿
var inspeccion_id;
var renglon_inspeccion;

$(function(){ //inicio
  
  
  //creamos el popup
  
  $('#ventana_registrar_inspeccion').window('close');
  $('#ventana_cancelar_inspecciones').window('close');
  
  
  
  //construimos los autocompletados
  auto_completado_colonias_asignadas();
  auto_completado_calles_asignadas();
  auto_completado_estatus();
  auto_completado_inspectores();
   
   
  //crear grid 
  CrearGridObservaciones();

}); // fin


//------------------------------------------------------------------------------------

function format_colonia_asignada(item) {
    return item.nombre;
}

function auto_completado_colonias_asignadas() { 

//inciamos el auto completado
    $("[id$='txt__colonia']").autocomplete("../../Controladores/frm_controlador_inspecciones.aspx", {
        extraParams: { accion: 'autocompletar_colonias_asignadas' },
        dataType: "json",
        parse: function(data) {
            return $.map(data, function(row) {
                return {
                    data: row,
                    value: row.colonia_id,
                    result: row.nombre

                }
            });
        },
        formatItem: function(item) {
            return format_colonia_asignada(item);
        }
    }).result(function(e, item) {
        
    });

}


//---------------------------
function format_calle_asignada(item) {
    // como se ve en el div contendor
    return item.calle ;
}

function auto_completado_calles_asignadas() {

    //inciamos el auto completado
    $("[id$='txt__calle']").autocomplete("../../Controladores/frm_controlador_inspecciones.aspx", {
    extraParams: { accion: 'autocompletar_calle_asingadas' },
        dataType: "json",
        parse: function(data) {
            return $.map(data, function(row) {
                return {
                    data: row,
                    value: row.calle_id,
                    result: row.calle 

                }
            });
        },
        formatItem: function(item) {
            return format_calle_asignada(item);
        }
    }).result(function(e, item) {

    });

}

//------------------------------------------------------------------------------------

function format_estatus_inspeccion(item) {
    // como se ve en el div contendor
    return item.estado; 
}

function auto_completado_estatus() {

    //inciamos el auto completado
    $("[id$='txt__estado']").autocomplete("../../Controladores/frm_controlador_inspecciones.aspx", {
    extraParams: { accion: 'autompletar_status_inspeccion' },
        dataType: "json",
        parse: function(data) {
            return $.map(data, function(row) {
                return {
                    data: row,
                    value:row.estado_id,
                    result: row.estado

                }
            });
        },
        formatItem: function(item) {
            return format_estatus_inspeccion(item);
        }
    }).result(function(e, item) {

    });

}

//------------------------------------------------------------------------------------


function format_inspeccion(item) {
    // como se ve en el div contendor
    return item.nombre_empleado;
}

function auto_completado_inspectores() {

    //inciamos el auto completado
    $("[id$='txt__inspector']").autocomplete("../../Controladores/frm_controlador_inspecciones.aspx", {
    extraParams: { accion: 'autocompletar_nombre_inspectores' },
        dataType: "json",
        parse: function(data) {
            return $.map(data, function(row) {
                return {
                    data: row,
                     value:row.inspector_id,
                    result: row.nombre_empleado

                }
            });
        },
        formatItem: function(item) {
            return format_inspeccion (item);
        }
    }).result(function(e, item) {

    });

}

//------------------------------------------------------------------------------------

function CrearGridObservaciones(){


 $('#grid_inspecciones_asignadas').datagrid({  // tabla inicio
        title: 'Inspecciones Asignadas',
        width: 820,
        height: 250,
        columns: [[
        { field: 'no_inspeccion', title: 'ID', width: 50, sortable: true },
        { field: 'folio_inspeccion', title: 'Folio', width: 100, sortable: true },
        { field: 'no_cuenta', title: 'No.Cuenta', width: 100, sortable: true },
        { field: 'sector', title: 'Sector', width: 100, sortable: true },
        { field: 'calle', title: 'Calle', width: 200, sortable: true },
        { field: 'colonia', title: 'Colonia', width: 200, sortable: true },
        { field: 'numero_interior', title: 'Num.Int.', width: 70, sortable: true },
        { field: 'numero_exterior', title: 'Num.Ext.', width: 70, sortable: true },
        { field: 'solicito', title: 'Solicito', width: 200, sortable: true },
        { field: 'inspector', title: 'Inspector', width: 200, sortable: true },
        { field: 'motivo', title: 'Motivo Insp.', width: 200, sortable: true },
        { field: 'fecha_elaboro', title: 'Fecha Elaboro', width: 150, sortable: true },
        { field: 'fecha_inspeccion', title: 'Fecha Inspeccion', width: 150, sortable: true,editor: 'text' },      
        { field: 'status', title: 'Estado', width: 150, sortable: true,editor: 'text' },
        { field: 'observaciones', title: 'Observaciones', width: 300, sortable: true ,editor: 'text'},
        { field: 'motivo_cancelacion', title: 'Motivo Cancelacion', width: 300, sortable: true,editor: 'text' },
        { field: 'condicion_revision', title: 'Condiciones Rev', width: 250, sortable: true },
        { field: 'no_solicitud', title: 'No.Solicitud', width: 150, sortable: true }
           
    ]],
        pageSize: 10,
        pageList: [30],
        pagination: true,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        striped: true,
        loadMsg: 'cargando...',
        nowrap: false,
        toolbar: [{
            id: 'btn_table_quitar',
            text: 'Quitar',
            iconCls: 'icon-back',
            handler: function() {
               desAsignarInspeccion();
            }
          },'-',{
					id:'btn_table_registrar',
					text:'Registrar',
					iconCls:'icon-save',
					handler:function(){
						resgistrarInpeccion();
					}
            },'-',{
					id:'btn_table_cancelar',
					text:'Cancelar',
					iconCls:'icon-cancel',
					handler:function(){
						cancelar_inspeccion();
					}
           
        }],
        onClickRow: function(rowIndex, rowData) {  
        },
        onBeforeLoad: function() {
          
        },
        onLoadSuccess: function() {

        }

    });          // tabla final

   $('#grid_inspecciones_asignadas').datagrid('hideColumn', 'no_inspeccion');


}

//---------------------------------------------


 function buscarInspeccionesAsignadas() {
     var datos_busqueda;
     var datos={};

     if (validar_check_box_cajas_textos("bus_inspecciones_asignadas",datos)) {
         return false;
     }
   
   
     datos_busqueda=toJSON(datos);
     $('#grid_inspecciones_asignadas').datagrid('loadData', { total: 0, rows: [] });
     $('#grid_inspecciones_asignadas').datagrid({ url: '../../Controladores/frm_controlador_inspecciones.aspx', queryParams: {
     accion: 'obtenerInspeccionesAsignadas',
     datos_busqueda:datos_busqueda,
     estado:'GENERADA'    
     }, pageNumber: 1
     });
    
    
 }
 
 
 //---------------------------------------------
 
 function desAsignarInspeccion(){
      
  var renglon_sel;
  
  renglon_sel=$('#grid_inspecciones_asignadas').datagrid('getSelected');
  
  if(renglon_sel==null){
     $.messager.alert('Japami','Tienes que seleccionar una inspecci&oacute;n');
     return;
   }
   
   if(renglon_sel.status!=''){
     $.messager.alert('Japami','la inspecci&oacute;n selecionada no se desasignar')
     return;
   }
   
   $('#ventana_mensaje').window('open');
    
        $.ajax({
            type: "POST",
            url: "../../Controladores/frm_controlador_inspecciones.aspx/cambioEstadoSolicitud",
            data: "{'no_inspeccion':'" + renglon_sel.no_inspeccion + "','inspector_id':'','inspector_area_id':'','estado':'GENERADA'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                var respuesta = eval("(" + response.d + ")");
                if (respuesta.mensaje == "bien") {
                    $('#ventana_mensaje').window('close');
                     var index=$('#grid_inspecciones_asignadas').datagrid('getRowIndex',renglon_sel);
                    $('#grid_inspecciones_asignadas').datagrid('deleteRow',index);
                    
                    $.messager.alert('Japami', 'Proceso terminado');
                }
                else {
                    $('#ventana_mensaje').window('close');
                    $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                }

            },
            error: function(result) {
                $('#ventana_mensaje').window('close');
                $.messager.alert('Japami', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');

            }

        });
   
 
  
  
 
 }
 
 
 //--------------------------------------------------------------------------
 
 function resgistrarInpeccion(){
 
 renglon_inspeccion=$('#grid_inspecciones_asignadas').datagrid('getSelected');
  
  if(renglon_inspeccion==null){
     $.messager.alert('Japami','Tienes que seleccionar una inspecci&oacute;n');
     return;
   }
   
   if(renglon_inspeccion.status!='ASIGNADA'){
      $.messager.alert('Japami','la inspecci&oacute;n selecionada no se puede registar');
     return; 
   }
 
 
 $('#grid_subir_archivos').datagrid('loadData',{ total: 0, rows: [] });
 $('#txt_observaciones_ins').val('');
 $('#ventana_registrar_inspeccion').window('open');
 mostrar_informacion_inspeccion(renglon_inspeccion);
 $('#grid_subir_archivos').datagrid('resize');
 }
 
 
  //--------------------------------------------------------------------------
    
    function mostrar_informacion_inspeccion(renglon_datos){
    
     $('#txt_folio_ins').val(renglon_datos.folio_inspeccion);
     $('#txt_no_cuenta_ins').val(renglon_datos.no_cuenta);
     $('#txt_solicito_ins').val(renglon_datos.solicito);
     $('#txt_direccion_ins').val(renglon_datos.calle + ' ' + renglon_datos.colonia + ' No. int ' + renglon_datos.numero_interior + '  No.Ext' +  renglon_datos.numero_exterior);
     $('#txt_asunto_ins').val(renglon_datos.condicion_revision);
     $('#txt_motivo_ins').val(renglon_datos.motivo);
     
     inspeccion_id=renglon_datos.no_inspeccion;
     
    
    }
    
  //--------------------------------------------------------------------------
  
  
 function guardar_registro_inspeccion(){
 var fecha,observaciones,archivos;
 
 observaciones= $.trim($('#txt_observaciones_ins').val());
 fecha=$.trim($('#txt_fecha_registro_folio').val());
  
  if(fecha.length==0){
     $.messager.alert('Japami','Te Falta una Fecha');
     return ;
  }
  
  if(observaciones.length==0){
     $.messager.alert('Japami','Te Falta una Obervacion');
     return ;
  }
  $('#grid_subir_archivos').datagrid('acceptChanges');
  archivos= toJSON($('#grid_subir_archivos').datagrid('getRows'));
  
   $('#ventana_mensaje').window('open');
        $.ajax({
            type: "POST",
            url: "../../Controladores/frm_controlador_inspecciones.aspx/registrarInspeccion",
            data: "{'observaciones_inspecciones':'" + observaciones + "','fecha_inspeccion':'" + fecha + "','fotos':'" + archivos + "','inspeccion_id':'" + inspeccion_id + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                var respuesta = eval("(" + response.d + ")");
                if (respuesta.mensaje == "bien") {
                    $('#ventana_mensaje').window('close');
                     actualizarGridRegistroInspeccion(observaciones,fecha,renglon_inspeccion);
                    
                    $.messager.alert('Japami', 'Proceso terminado','',function(){ $('#ventana_registrar_inspeccion').window('close');});
                }
                else {
                    $('#ventana_mensaje').window('close');
                    $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                }

            },
            error: function(result) {
                $('#ventana_mensaje').window('close');
                $.messager.alert('Japami', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');

            }

        });
   
 
  

}


//-----------------------------------------------------------------------------------------------

function actualizarGridRegistroInspeccion(observaciones,fecha,renglon_sel)
{     
      var editores;
      var index=$('#grid_inspecciones_asignadas').datagrid('getRowIndex',renglon_sel);
      
      $('#grid_inspecciones_asignadas').datagrid('beginEdit',index);
       editores = $('#grid_inspecciones_asignadas').datagrid('getEditors', index);
       // fecha inspeccion
       editores[0].target.val(fecha);
       //status
       editores[1].target.val('REALIZADA');
       //observaciones
       editores[2].target.val(observaciones);
       //motivo_cancelacion
       //editores[3].target.val();
       
      $('#grid_inspecciones_asignadas').datagrid('endEdit',index); 
      $('#grid_inspecciones_asignadas').datagrid('unselectAll'); 
      
               
}



//------------------------------------------------------------------------------------------------

function cancelar_inspeccion()
{
  renglon_inspeccion=$('#grid_inspecciones_asignadas').datagrid('getSelected');
  
  if(renglon_inspeccion==null){
     $.messager.alert('Japami','Tienes que seleccionar una inspecci&oacute;n');
     return;
   }
   
   if(renglon_inspeccion.status!='ASIGNADA'){
      $.messager.alert('Japami','la inspecci&oacute;n selecionada no se puede cancelar');
     return; 
   }
 
 inspeccion_id=renglon_inspeccion.no_inspeccion;
 $('#txt_motivo_cancelacion').val('');
 $('#ventana_cancelar_inspecciones').window('open');


}

//------------------------------------------------------------------------------------------------

function guardar_cancelar_inspeccion(){
var motivo_cancelacion;
 
 motivo_cancelacion= $.trim($('#txt_motivo_cancelacion').val());
  
  
  if(motivo_cancelacion.length==0){
     $.messager.alert('Japami','Te Falta Escribir la cancelacion');
     return ;
  }   

 $('#ventana_mensaje').window('open');
        $.ajax({
            type: "POST",
            url: "../../Controladores/frm_controlador_inspecciones.aspx/cancelarInspeccion",
            data: "{'p_motivo_cancelacion':'" + motivo_cancelacion + "','inspeccion_id':'" + inspeccion_id + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                var respuesta = eval("(" + response.d + ")");
                if (respuesta.mensaje == "bien") {
                    $('#ventana_mensaje').window('close');
                    
                    actualizarGridCancelarInspeccion(motivo_cancelacion,renglon_inspeccion);
                    $.messager.alert('Japami', 'Proceso terminado','',function(){ $('#ventana_cancelar_inspecciones').window('close');});
                }
                else {
                    $('#ventana_mensaje').window('close');
                    $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                }

            },
            error: function(result) {
                $('#ventana_mensaje').window('close');
                $.messager.alert('Japami', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');

            }

        });
  
  

}
//---------------------------------------------------------

function actualizarGridCancelarInspeccion(motivo,renglon_sel)
{     
      var editores;
      var index=$('#grid_inspecciones_asignadas').datagrid('getRowIndex',renglon_sel);
      
      $('#grid_inspecciones_asignadas').datagrid('beginEdit',index);
       editores = $('#grid_inspecciones_asignadas').datagrid('getEditors', index);
       // fecha inspeccion
      // editores[0].target.val(fecha);
       //status
       editores[1].target.val('CANCELADA');
       //observaciones
       //editores[2].target.val(observaciones);
       //motivo_cancelacion
       editores[3].target.val(motivo);
       
      $('#grid_inspecciones_asignadas').datagrid('endEdit',index); 
      $('#grid_inspecciones_asignadas').datagrid('unselectAll'); 
      
               
}