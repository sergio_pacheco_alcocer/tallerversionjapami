﻿///*******************************************************************************
///NOMBRE DE LA FUNCIÓN : $(document).ready()
///DESCRIPCIÓN          : Funcion que indica cuando el documento ha sido cargado, para poder modificar los elementod DOM
///PARAMETROS           :   
///CREO                 : Yañez Rodriguez Diego
///FECHA_CREO           : 00/Marzo/2011 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///*******************************************************************************
$(document).ready(function () {
    Configuracion_Inicial();
});
//COnfiguración Inicial de los controles --------------------------------------------------------------------------------
function Configuracion_Inicial() {
    jQuery.ajaxSetup({
        async: false,
        cache: false,
        timeout: (2 * 1000),
        beforeSend: function () {
            MostrarProgress();
        },
        complete: function () {
            OcultarProgress();
        },
        error: function () {
            OcultarProgress();
        }
    });

    //Configuracion de los controles de Usuarios----------------------------------------------------------------------------------------------------
    //Configuracion del grid de Usuarios
    $('#Grid_Usuarios').datagrid({
        title: 'Catalogo Usuarios',
        width: 850,
        height: 280,
        columns: [[
                    { field: 'boton', title: 'Sel', width: 20, align: 'center',
                        formatter: function (value, rec) {
                            return '<input type="button" id="' + rec.USUARIO_ID + '" style="width:10px"/>';
                        }
                    },
                    { field: 'Usuario_ID', title: 'Usuario ID', width: 1, hidden: true },
                    { field: 'RFC', title: 'RFC', width: 80, sortable: true },
                    { field: 'Razon_Social', title: 'Razon Social', width: 100, sortable: true },
                    { field: 'Nombre_Completo', title: 'Nombre', width: 100, sortable: true },
                    { field: 'Calle', title: 'Direccion', width: 100, sortable: true, hidden: true }               
            ]],
       pagination: true,
       rownumbers: true,
       idField: 'USUARIO_ID',
       sortOrder: 'asc',
       remoteSort: false,
       fitColumns: false,
       singleSelect: true,
       loadMsg: "Cargando, espere ...",
       pageNumber: 1,
       pageSize: 10,
       fitColumns: true,
       striped: true,
       nowrap: false,
       pageList: [10, 20],
       remoteSort: false,
       showFooter: true,
       onClickRow: function(rowIndex, rowData) {
            $("#Lbl_Mensaje_Error_Usuario").text('');
            $("#Lbl_Mensaje_Error_Usuario").hide();
            $("#Img_Error_Usuario").hide();
            $('#Txt_Usuario_ID').val(rowData.Usuario_ID);
            Cargar_Usuario(rowData.Usuario_ID);
            $('#Filtros_Usuarios').hide();
            $('#Alta_Usuarios').show();
            $('#Btn_Salir').attr('alt', 'Cancelar');
        }
    });

    //autocompleatodos filtros usuarios
    AutocompletadoNombreUsuariosFiltros();
    AutocompletadoApellidoPaternoUsuariosFiltros();
    AutocompletadoCalleUsuariosFiltros();
    AutocompletadoColoniaUsuariosFiltros();
    AutocompletadoCiudadUsuariosFiltros();
    //autocompletado alta usuarios
    //Carga de Informacion
    AutocompletadoNombreUsuarios();
    AutocompletadoApellidoPaternoUsuarios();
    AutocompletadoApellidoMaternoUsuarios();
    AutocompletadoColoniaUsuarios();
    AutocompletadoCalleUsuarios();
    Cargar_Estados();
    //Eventos de Controles
    $('#Txt_Nombre_Usuario_Alta').keyup(function () {
        var longitud = $('#Txt_Nombre_Usuario_Alta').val().length;
        if (longitud == 0) {
            $('#Txt_Nombre_Usuario_Alta').removeAttr('style');
            $('#Txt_Nombre_Usuario_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
        }
        else {
            $('#Txt_Nombre_Usuario_Alta').removeAttr('style');
            $('#Txt_Nombre_Usuario_Alta').attr('style', 'width:98%;');
        }
    });
    $('#Txt_RFC_Usuario_Alta').keyup(function () {
        var longitud = $('#Txt_RFC_Usuario_Alta').val().length;
        if (longitud == 0) {
            $('#Txt_RFC_Usuario_Alta').removeAttr('style');
            $('#Txt_RFC_Usuario_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
        }
        else {
            $('#Txt_RFC_Usuario_Alta').removeAttr('style');
            $('#Txt_RFC_Usuario_Alta').attr('style', 'width:98%;');
        }
    });
    $('#Txt_RFC_Usuario_Alta').blur(function() {
        var longitud = $('#Txt_RFC_Usuario_Alta').val().length;
        if (longitud == 0) {
            $('#Txt_RFC_Usuario_Alta').val('XAXX010101000');
        }
    });
    $('#Txt_Apellido_Paterno_Usuario_Alta').keyup(function () {
        var longitud = $('#Txt_Apellido_Paterno_Usuario_Alta').val().length;
        if (longitud == 0) {
            $('#Txt_Apellido_Paterno_Usuario_Alta').removeAttr('style');
            $('#Txt_Apellido_Paterno_Usuario_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
        }
        else {
            $('#Txt_Apellido_Paterno_Usuario_Alta').removeAttr('style');
            $('#Txt_Apellido_Paterno_Usuario_Alta').attr('style', 'width:98%;');
        }
    });
    $('#Txt_Razon_Social_Usuario').keyup(function () {
        var longitud = $('#Txt_Razon_Social_Usuario').val().length;
        if (longitud == 0) {
            $('#Txt_Razon_Social_Usuario').removeAttr('style');
            $('#Txt_Razon_Social_Usuario').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
        }
        else {
            $('#Txt_Razon_Social_Usuario').removeAttr('style');
            $('#Txt_Razon_Social_Usuario').attr('style', 'width:98%;');
        }
    });
    $('#Cmb_Estado_Usuario').change(function (event) {
        var estado_id = $("#Cmb_Estado_Usuario option:selected").val();
        if (estado_id != 'Seleccione') {
            $('#Cmb_Estado_Usuario').removeAttr('style');
            $('#Cmb_Estado_Usuario').attr('style', 'width:98%;');
            Cargar_Ciudades();
            $('#Cmb_Ciudad_Usuario').removeAttr('disabled');
        }
        else {
            $('#Cmb_Estado_Usuario').removeAttr('style');
            $('#Cmb_Estado_Usuario').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
            $('#Cmb_Ciudad_Usuario').attr('disabled', 'disabled');
        }
    });
    $('#Cmb_Ciudad_Usuario').change(function (event) {
        var ciudad_id = $("#Cmb_Ciudad_Usuario option:selected").val();
        if (ciudad_id != 'Seleccione') {
            $('#Cmb_Ciudad_Usuario').removeAttr('style');
            $('#Cmb_Ciudad_Usuario').attr('style', 'width:98%;');
        }
        else {
            $('#Cmb_Ciudad_Usuario').removeAttr('style');
            $('#Cmb_Ciudad_Usuario').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
        }
    });
    $('#Txt_Colonia_Usuario').keyup(function () {
        var longitud = $('#Txt_Colonia_Usuario').val().length;
        if (longitud == 0) {
            $('#Txt_Colonia_Usuario_Hidden').val('');
            $('#Txt_Colonia_Usuario').removeAttr('style');
            $('#Txt_Colonia_Usuario').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
        }
    });
    $('#Txt_Calle_Usuario').keyup(function () {
        var longitud = $('#Txt_Calle_Usuario').val().length;
        if (longitud == 0) {
            $('#Txt_Calle_Usuario_Hidden').val('');
            $('#Txt_Calle_Usuario').removeAttr('style');
            $('#Txt_Calle_Usuario').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
        }
    });
    $('#Txt_No_Exterior_Usuario').keyup(function () {
        var longitud = $('#Txt_No_Exterior_Usuario').val().length;
        if (longitud == 0) {
            $('#Txt_No_Exterior_Usuario').removeAttr('style');
            $('#Txt_No_Exterior_Usuario').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
        }
        else {
            $('#Txt_No_Exterior_Usuario').removeAttr('style');
            $('#Txt_No_Exterior_Usuario').attr('style', 'width:98%;');
        }
    });
    //ConfiguracionInicial
    Configurar_Botones_Alta_Usuario("Inicial");
    //Activa el modal de los usuarios
    $('#Btn_Filtrar_Usuarios').click(function (event) {
        event.preventDefault();
        Filtrar_Usuarios();
    });
    $('#Btn_Nuevo').click(function (event) {
        event.preventDefault();
        Btn_Nuevo_Click();
    });
    $('#Btn_Modificar').click(function (event) {
        event.preventDefault();
        Btn_Modificar_Click();
    });
    $('#Btn_Eliminar').click(function (event) {
        event.preventDefault();
        Btn_Eliminar_Click();
    });
    $('#Btn_Salir').click(function (event) {
        event.preventDefault();
        Btn_Salir_Click();
    });
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}
//Modal progress, uso de animación de preogresso, indicador de actividad -------------------------------------------------
function MostrarProgress() {
    $('[id$=Upgrade]').show();
}
function OcultarProgress() {
    $('[id$=Upgrade]').delay(2000).hide();
}

//Inicio Catalogo de Usuarios -------------------------------------------------------------------------------------------------------------------
//Consulta de Información-------------------------------------------------------------------------------
function Obtnener_Parametros_Filtro_Usuarios() {
    var parametros = "";
    //Obtiene los filtros para mandarlos a consulta
    //RFC
    parametros += "&RFC=" + $('#Txt_Busqueda_Rfc').val();
    //Razon Social
    parametros += "&Razon_Social=" + $('#Txt_Busqueda_Razon_Social').val();
    //Nombre
    parametros += "&Nombre=" + $('#Txt_Busqueda_Nombre').val();
    //Apellido Paterno
    parametros += "&Apellido_Paterno=" + $('#Txt_Busqueda_Apellido_Paterno').val();
    //Calle
    parametros += "&Calle=" + $('#Txt_Busqueda_Calle').val();
    //Numero Exterior
    parametros += "&Numero_Exterior=" + $('#Txt_Busqueda_No_Exterior').val();
    //Colonia
    parametros += "&Colonia=" + $('#Txt_Busqueda_Colonia').val();
    //Ciudad
    parametros += "&Ciudad_ID=" + $('#Txt_Busqueda_Ciudad_Hidden').val();
    return parametros;
}
function Filtrar_Usuarios() {
    //No registros por pagina
    $('#Grid_Usuarios').datagrid({
        url: '../../Controladores/Frm_Controlador_Usuarios.aspx?Accion=Filtrar_Usuarios' + Obtnener_Parametros_Filtro_Usuarios(),
        pageNumber: 1
    });
}
//Configuración de elementos en eventso---------------------------------------------------------------------------------
function Configurar_Botones_Alta_Usuario(modo) {
    try {
        switch (modo) {
            case "Inicial":
                $('#Btn_Nuevo').removeAttr('disabled');
                $('#Btn_Modificar').removeAttr('disabled');
                $('#Btn_Nuevo').attr('src', '../imagenes/paginas/icono_nuevo.png');
                $('#Btn_Modificar').attr('src', '../imagenes/paginas/icono_modificar.png');
                $('#Btn_Eliminar').attr('src', '../imagenes/paginas/icono_eliminar.png');
                $('#Btn_Salir').attr('src', '../imagenes/paginas/icono_salir.png');
                $('#Btn_Nuevo').attr('alt', 'Nuevo');
                $('#Btn_Salir').attr('alt', 'Salir');
                $('#Btn_Modificar').attr('alt', 'Modificar');
                $('#Filtros_Usuarios').show();
                $('#Alta_Usuarios').hide();
                $('#Lbl_Mensaje_Error_Usuario').text('');
                $('#Img_Error_Usuario').hide();
                $('.altaUsuario').attr('disabled', 'disabled');
                //Configuracion_Acceso("Frm_Cat_Cor_Medidores.aspx");
                break;

            case "Nuevo":
                $('#Btn_Nuevo').removeAttr('disabled');
                $('#Btn_Nuevo').attr('src', '../imagenes/paginas/icono_guardar.png');
                $('#Btn_Nuevo').attr('alt', 'Guardar');
                $('#Btn_Modificar').removeAttr('disabled');
                $('#Btn_Modificar').attr('disabled', 'disabled');
                $('#Btn_Eliminar').removeAttr('disabled');
                $('#Btn_Eliminar').attr('disabled', 'disabled');
                $('#Btn_Modificar').attr('src', '../imagenes/paginas/icono_modificar_deshabilitado.png');
                $('#Btn_Modificar').attr('alt', 'Modificar');
                $('#Btn_Eliminar').attr('src', '../imagenes/paginas/icono_eliminar_deshabilitado.png');
                $('#Btn_Salir').attr('src', '../imagenes/paginas/icono_cancelar.png');
                $('#Btn_Salir').attr('alt', 'Cancelar');
                $('.altaUsuario').removeAttr('disabled');
                $('#Filtros_Usuarios').hide();
                $('#Alta_Usuarios').show();
                $('#Cmb_Estatus_Usuario').val('ACTIVO');
                $('#Cmb_Estatus_Usuario').attr('disabled', 'disabled');
                break;

            case "Modificar":
                $('#Btn_Modificar').removeAttr('disabled');
                $('#Btn_Modificar').attr('src', '../imagenes/paginas/icono_guardar.png');
                $('#Btn_Modificar').attr('alt', 'Guardar');
                $('#Btn_Nuevo').removeAttr('disabled');
                $('#Btn_Nuevo').attr('src', '../imagenes/paginas/icono_nuevo_deshabilitado.png');
                $('#Btn_Nuevo').attr('alt', 'Nuevo');
                $('#Btn_Nuevo').attr('disabled', 'disabled');
                $('#Btn_Eliminar').removeAttr('disabled');
                $('#Btn_Eliminar').attr('disabled', 'disabled');
                $('#Btn_Eliminar').attr('src', '../imagenes/paginas/icono_eliminar_deshabilitado.png');
                $('#Btn_Salir').attr('src', '../imagenes/paginas/icono_cancelar.png');
                $('#Btn_Salir').attr('alt', 'Cancelar');
                $('#Filtros_Usuarios').hide();
                $('#Alta_Usuarios').show();
                //habilita los controles para edicion
                $('.altaUsuario').removeAttr('disabled');

                $('#Cmb_Estatus_Usuario').removeAttr('disabled','false');
                break;
            default: break;
        }

    }
    catch (ex) {
        //Error
        $("#Lbl_Mensaje_Error_Usuario").removeAttr('style');
        $("#Lbl_Mensaje_Error_Usuario").attr('style', 'color: rgb(153, 0, 0); display: inline;');
        $("#Lbl_Mensaje_Error_Usuario").text(ex);
        $("#Img_Error_Usuario").show();
    }
    finally {
    }
}
function Limpia_Campos_Usuarios() {
    $('.altaUsuario').val('Seleccione');
    $('.altaUsuario').val('');
    $('.validacion').val('');
    //$("#Cmb_Estado_Usuario option:selected").val('Seleccione');
    $('#Cmb_Estado_Usuario').val('Seleccione');
    $('#Txt_RFC_Usuario_Alta').val('XAXX010101000');
    $('.filtros').val('');
    $('.requeridoUsuario').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
}
function AutocompletadoNombreUsuariosFiltros() {
    //Autocompletado de los Nombres de usuarios
    $('#Txt_Busqueda_Nombre').autocomplete("../../Controladores/Frm_Controlador_Usuarios.aspx", {
        extraParams: { Accion: 'autocompletar_nombre_usuarios' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.usuario_id,
                    result: row.nombre
                }
            });
        },
        formatItem: function (item) {
            return item.nombre;
        }
    }).result(function (e, item) {
        var nombre = item.nombre;
    });
}
function AutocompletadoApellidoPaternoUsuariosFiltros() {
    //Autocompletado de los Nombres de usuarios
    $('#Txt_Busqueda_Apellido_Paterno').autocomplete("../../Controladores/Frm_Controlador_Usuarios.aspx", {
        extraParams: { Accion: 'autocompletar_appaterno_usuarios' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.usuario_id,
                    result: row.apellido_paterno
                }
            });
        },
        formatItem: function (item) {
            return item.apellido_paterno;
        }
    }).result(function (e, item) {
        var nombre = item.apellido_paterno;
    });
}
function AutocompletadoCalleUsuariosFiltros() {
    //Autocompletado de los Nombres de usuarios
    $('#Txt_Busqueda_Calle').autocomplete("../../Controladores/Frm_Controlador_Usuarios.aspx", {
        extraParams: { Accion: 'autocompletar_calle_usuarios' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.usuario_id,
                    result: row.calle
                }
            });
        },
        formatItem: function (item) {
            return item.calle;
        }
    }).result(function (e, item) {
        var nombre = item.calle;
    });
}
function AutocompletadoColoniaUsuariosFiltros() {
    //Autocompletado de los Nombres de usuarios
    $('#Txt_Busqueda_Colonia').autocomplete("../../Controladores/Frm_Controlador_Usuarios.aspx", {
        extraParams: { Accion: 'autocompletar_colonia_usuarios' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.usuario_id,
                    result: row.colonia
                }
            });
        },
        formatItem: function (item) {
            return item.colonia;
        }
    }).result(function (e, item) {
        var nombre = item.colonia;
    });
}
function AutocompletadoCiudadUsuariosFiltros() {
    $('#Txt_Busqueda_Ciudad_Hidden').val('');
    //Autocompletado de los Nombres de usuarios
    $('#Txt_Busqueda_Ciudad').autocomplete("../../Controladores/Frm_Controlador_Usuarios.aspx", {
        extraParams: { Accion: 'autocompletar_ciudad_usuarios' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.ciudad_id,
                    result: row.ciudad
                }
            });
        },
        formatItem: function (item) {
            return item.ciudad_id;
        }
    }).result(function (e, item) {
        var ciudad_id = item.ciudad_id;
        $('#Txt_Busqueda_Ciudad_Hidden').val(ciudad_id);
    });
}
//Autocompletado alta usuarios
function AutocompletadoNombreUsuarios() {
    //Autocompletado de los Nombres de usuarios
    $('#Txt_Nombre_Usuario_Alta').autocomplete("../../Controladores/Frm_Controlador_Usuarios.aspx", {
        extraParams: { Accion: 'autocompletar_nombre_usuarios' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.usuario_id,
                    result: row.nombre
                }
            });
        },
        formatItem: function (item) {
            return item.nombre;
        }
    }).result(function (e, item) {
        var nombre = item.nombre;
    });
}
function AutocompletadoApellidoPaternoUsuarios() {
    //Autocompletado de los Nombres de usuarios
    $('#Txt_Apellido_Paterno_Usuario_Alta').autocomplete("../../Controladores/Frm_Controlador_Usuarios.aspx", {
        extraParams: { Accion: 'autocompletar_appaterno_usuarios' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.usuario_id,
                    result: row.apellido_paterno
                }
            });
        },
        formatItem: function (item) {
            return item.apellido_paterno;
        }
    }).result(function (e, item) {
        var nombre = item.apellido_paterno;
    });
}
function AutocompletadoApellidoMaternoUsuarios() {
    //Autocompletado de los Nombres de usuarios
    $('#Txt_Apellido_Materno_Usuario').autocomplete("../../Controladores/Frm_Controlador_Usuarios.aspx", {
        extraParams: { Accion: 'autocompletar_appaterno_usuarios' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.usuario_id,
                    result: row.apellido_paterno
                }
            });
        },
        formatItem: function (item) {
            return item.apellido_paterno;
        }
    }).result(function (e, item) {
        var nombre = item.apellido_paterno;
    });
}
function AutocompletadoColoniaUsuarios() {
    //Autocompletado de los Nombres de usuarios
    $('#Txt_Colonia_Usuario').autocomplete("../../Controladores/Frm_Controlador_Usuarios.aspx", {
        extraParams: { Accion: 'autocompletar_colonia_usuarios1' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.colonia_id,
                    result: row.colonia
                }
            });
        },
        formatItem: function (item) {
            return item.colonia;
        }
    }).result(function (e, item) {
        var colonia_id = item.colonia_id;
        $('#Txt_Colonia_Usuario_Hidden').val(colonia_id);
        $('#Txt_Colonia_Usuario').removeAttr('style');
        $('#Txt_Colonia_Usuario').attr('style', 'width:98%;');
    });
}
function AutocompletadoCalleUsuarios() {
    //Autocompletado de los Nombres de usuarios
    var colonia_id = $('#Txt_Colonia_Usuario_Hidden').val();
    $('#Txt_Calle_Usuario').autocomplete("../../Controladores/Frm_Controlador_Usuarios.aspx", {
        extraParams: { Accion: 'autocompletar_calle_usuarios1', colonia_id: colonia_id },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.calle_id,
                    result: row.calle
                }
            });
        },
        formatItem: function (item) {
            return item.calle;
        }
    }).result(function (e, item) {
        var calle_id = item.calle_id;
        $('#Txt_Calle_Usuario_Hidden').val(calle_id);
        $('#Txt_Calle_Usuario').removeAttr('style');
        $('#Txt_Calle_Usuario').attr('style', 'width:98%;');
    });
}
function Cargar_Estados() {
    var select = $('#Cmb_Estado_Usuario');
    $('option', select).remove();

    $.ajax({
        url: "../../Controladores/Frm_Controlador_Usuarios.aspx?Accion=consulta_estados",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.estado_id + '">' + item.estado + '</option>';
                });
                $('#Cmb_Estado_Usuario').append(options);
            }
        }
    });
}
function Cargar_Ciudades() {
    var select = $('#Cmb_Ciudad_Usuario');
    $('option', select).remove();
    var Estado_ID = $('#Cmb_Estado_Usuario').val();
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Usuarios.aspx?Accion=consulta_ciudades&Estado_Id=" + Estado_ID,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.ciudad_id + '">' + item.ciudad + '</option>';
                });
                $('#Cmb_Ciudad_Usuario').append(options);
            }
        }
    });
}

//eventos de botones
function Btn_Nuevo_Click() {
    $("#Lbl_Mensaje_Error_Usuario").text('');
    $("#Lbl_Mensaje_Error_Usuario").hide();
    $("#Img_Error_Usuario").hide();

    var Btn_Nuevo = $("#Btn_Nuevo");
    var Btn_Modificar = $("#Btn_Modificar");
    //Verificar el tooltip del boton
    if (Btn_Nuevo.attr('alt') == "Nuevo") {
        if (Btn_Modificar.attr('alt') == "Guardar") {
            return;
        }
        //Habilitar y limpiar
        Configurar_Botones_Alta_Usuario("Nuevo");
        Limpia_Campos_Usuarios();
        //agrega los valores iniciales de pais y estatus
        $('#Txt_Pais_Usuario').val('Mexico');
        $('#Txt_Pais_Usuario').removeAttr('style');
        $('#Txt_Pais_Usuario').attr('style', 'width: 98%;');
        $('#Cmb_Estatus_Usuario').removeAttr('style');
        $('#Cmb_Estatus_Usuario').attr('style', 'width: 98%;');
    }
    else {
        Alta_Usuarios();
    }
}
function Alta_Usuarios() {
    //recupera la información de que se enviará al servidor para validar el alta
    
    var requeridos = Valida_Datos_Requeridos_Usuario();
    if (requeridos == "") {
        //Recupera la información a enviar
        var registro = $('.altaUsuario');

        var datos = registro.serialize();
        //registro += $('.validacion');
        var valor_combo;

        valor_combo = $('#Cmb_Estatus_Usuario').val();
        
        
        //envia la información al servidor
        $.ajax({
        url: "../../Controladores/Frm_Controlador_Usuarios.aspx?Accion=alta_usuario&" + registro.serialize() + '&Estatus_Usuario=' + valor_combo,
            type: 'POST',
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    //$.each(data, function (i, item) {
                    var error = data[2].error;
                    var usuario_id = data[0].usuario_id;
                    var alta = data[1].alta; ;
                    if (error != "") {
                        $("#Lbl_Mensaje_Error_Usuario").removeAttr('style');
                        $("#Lbl_Mensaje_Error_Usuario").attr('style', 'color: rgb(153, 0, 0); display: inline;');
                        $("#Lbl_Mensaje_Error_Usuario").text(error);
                        $("#Lbl_Mensaje_Error_Usuario").show();
                        $("#Img_Error_Usuario").show();
                    }
                    else {
                        Limpia_Campos_Usuarios();
                        //regresa a la pantalla principal
                        Configurar_Botones_Alta_Usuario("Inicial");
                        $("#Lbl_Mensaje_Error_Usuario").removeAttr('style');
                        $("#Lbl_Mensaje_Error_Usuario").attr('style', 'color: rgb(153, 0, 0); display: inline;');
                        $("#Lbl_Mensaje_Error_Usuario").text(alta);
                        $("#Lbl_Mensaje_Error_Usuario").show();
                        $("#Img_Error_Usuario").show();
                    } 
                }
            }
        });
    }
    else {
        $("#Lbl_Mensaje_Error_Usuario").removeAttr('style');
        $("#Lbl_Mensaje_Error_Usuario").attr('style', 'color: rgb(153, 0, 0); display: inline;');
        $("#Lbl_Mensaje_Error_Usuario").text(requeridos);
        $("#Lbl_Mensaje_Error_Usuario").show();
        $("#Img_Error_Usuario").show();
    }
}
function Valida_Datos_Requeridos_Usuario() {
    var opciones = $('.requeridoUsuario');
    var mensaje = "";
    opciones.each(function (i, item) {
        if (item.type == "text") {
            if ($('#' + item.id + '').val() == "") {
                mensaje += item.name + ", ";
            }
        }
        if (item.type == "select-one") {
            var cmboption = $("#" + item.id + " option:selected").val();
            if (cmboption == "Seleccione") {
                mensaje += item.name + ", ";
            }
        }
    });
    if (mensaje != "") {
        mensaje = "Falta la siguiente información por llenar: " + mensaje;
    }
    return mensaje;
}
function Btn_Modificar_Click() {
    $("#Lbl_Mensaje_Error_Usuario").text('');
    $("#Lbl_Mensaje_Error_Usuario").hide();
    $("#Img_Error_Usuario").hide();

    var Btn_Nuevo = $("#Btn_Nuevo");
    var Btn_Modificar = $("#Btn_Modificar");
    //Verificar el tooltip del boton
    if (Btn_Modificar.attr('alt') == "Modificar") {
        if (Btn_Nuevo.attr('alt') == "Guardar") {
            return;
        }
        //Verifica que se ha seleccionado un usuario
        var usuario_id = $('#Txt_Usuario_ID').val();
        if (usuario_id == "") {
            $("#Lbl_Mensaje_Error_Usuario").text('Seleccione un usuario para poder modificar');
            $("#Lbl_Mensaje_Error_Usuario").show();
            $("#Img_Error_Usuario").show();
            return;
        }
        //Habilitar y limpiar
        Configurar_Botones_Alta_Usuario("Modificar");
    }
    else {
        Modifica_Usuarios();
    }
}
function Btn_Salir_Click() {
    var estatus = $("#Btn_Salir").attr('alt')

    Limpia_Campos_Usuarios();
    if (estatus == "Salir") {
        //Valida que la información no se este mostrando

        window.location = '../Paginas_Generales/Frm_Apl_Principal.aspx';
    }
    else {
        Configurar_Botones_Alta_Usuario("Inicial");
    }
}
function Btn_Eliminar_Click(){
    var Btn_Nuevo = $("#Btn_Nuevo");
    var Btn_Modificar = $("#Btn_Modificar");
    if (Btn_Nuevo.attr('alt') == "Guardar") {
        return;
    }
    if (Btn_Modificar.attr('alt') == "Guardar") {
        return;
    }
    //Verifica que se ha seleccionado un usuario
    var usuario_id = $('#Txt_Usuario_ID').val();
    if (usuario_id == "") {
        $("#Lbl_Mensaje_Error_Usuario").text('Seleccione un usuario para poder eliminar');
        $("#Lbl_Mensaje_Error_Usuario").show();
        $("#Img_Error_Usuario").show();
        return;
    }
    else {
        Elimina_Usuarios();
    }
}
function Cargar_Usuario(Usuario_ID) {
    //Llena la información del usuario en las cajas de texto
    Limpia_Campos_Usuarios();
    $("#Lbl_Mensaje_Error").text('');
    $("#Lbl_Mensaje_Error").hide();
    $("#Img_Error").hide();
    //Realiza la consulta de la información del predio para colocarla en las cajas correspondientes
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Usuarios.aspx?Accion=Filtrar_Usuarios&Usuario_ID=" + Usuario_ID,
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $.each(data.rows, function (i, item) {
                    //asinga los valores a las cajas de texto y combos
                    //generales
                    $('#Txt_Usuario_ID').val(Usuario_ID);
                    $('#Cmb_Estatus_Usuario').val(item.Estatus);
                    $('#Txt_Nombre_Usuario_Alta').val(item.Nombre);
                    $('#Txt_RFC_Usuario_Alta').val(item.RFC);
                    $('#Txt_Apellido_Paterno_Usuario_Alta').val(item.Apellido_Paterno);
                    $('#Txt_Apellido_Materno_Usuario').val(item.Apellido_Materno);
                    $('#Txt_Razon_Social_Usuario').val(item.Razon_Social);
                    $('#Txt_Correo_Electronico_Usuario').val(item.Correo_Electronico);
                    $('#Txt_Pais_Usuario').val(item.Pais);
                    $('#Cmb_Estado_Usuario').val(item.Estado_ID);
                    Cargar_Ciudades();
                    $('#Cmb_Ciudad_Usuario').val(item.Ciudad_ID);
                    $('#Txt_Colonia_Usuario').val(item.Colonia);
                    $('#Txt_Colonia_Usuario_Hidden').val(item.Colonia_ID);
                    $('#Txt_Calle_Usuario').val(item.Calle);
                    $('#Txt_Calle_Usuario_Hidden').val(item.Calle_ID);
                    $('#Txt_No_Exterior_Usuario').val(item.No_Exterior);
                    $('#Txt_No_Interior_Usuario').val(item.No_Interior);
                    $('#Txt_Telefono_1_Usuario').val(item.Telefono_Oficina);
                    $('#Txt_Telefono_2_Usuario').val(item.Telefono_Casa);
                    $('#Txt_Telefono_Celular_Usuario').val(item.Telefono_Celular);
                    $('#Txt_Nextel_Usuario').val(item.Telefono_Nextel);
                    $('.requeridoUsuario').removeAttr('style');
                    $('.requeridoUsuario').attr('style','width:98%;');
                });
            }
        }
    });
}
function Modifica_Usuarios() {
    //recupera la información de que se enviará al servidor para validar el alta

    var requeridos = Valida_Datos_Requeridos_Usuario();
    if (requeridos == "") {
        //Recupera la información a enviar
        var registro = $('.altaUsuario');

        //envia la información al servidor
        $.ajax({
            url: "../../Controladores/Frm_Controlador_Usuarios.aspx?Accion=modifica_usuario&" + registro.serialize(),
            type: 'POST',
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    //$.each(data, function (i, item) {
                    var error = data[2].error;
                    var usuario_id = data[0].usuario_id;
                    var alta = data[1].alta; ;
                    if (error != "") {
                        $("#Lbl_Mensaje_Error_Usuario").removeAttr('style');
                        $("#Lbl_Mensaje_Error_Usuario").attr('style', 'color: rgb(153, 0, 0); display: inline;');
                        $("#Lbl_Mensaje_Error_Usuario").text(error);
                        $("#Lbl_Mensaje_Error_Usuario").show();
                        $("#Img_Error_Usuario").show();
                    }
                    else {
                        Limpia_Campos_Usuarios();
                        //regresa a la pantalla principal
                        Configurar_Botones_Alta_Usuario("Inicial");
                        $("#Lbl_Mensaje_Error_Usuario").removeAttr('style');
                        $("#Lbl_Mensaje_Error_Usuario").attr('style', 'color: rgb(153, 0, 0); display: inline;');
                        $("#Lbl_Mensaje_Error_Usuario").text(alta);
                        $("#Lbl_Mensaje_Error_Usuario").show();
                        $("#Img_Error_Usuario").show();
                    }
                }
            }
        });
    }
    else {
        $("#Lbl_Mensaje_Error_Usuario").removeAttr('style');
        $("#Lbl_Mensaje_Error_Usuario").attr('style', 'color: rgb(153, 0, 0); display: inline;');
        $("#Lbl_Mensaje_Error_Usuario").text(requeridos);
        $("#Lbl_Mensaje_Error_Usuario").show();
        $("#Img_Error_Usuario").show();
    }
}

function Elimina_Usuarios() {
    //recupera la información de que se enviará al servidor para validar el alta
    var usuario_id = $('#Txt_Usuario_ID').val();
    //envia la información al servidor
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Usuarios.aspx?Accion=elimina_usuario&Usuario_ID=" + usuario_id,
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                //$.each(data, function (i, item) {
                var error = data[2].error;
                var usuario_id = data[0].usuario_id;
                var alta = data[1].alta; ;
                if (error != "") {
                    $("#Lbl_Mensaje_Error_Usuario").removeAttr('style');
                    $("#Lbl_Mensaje_Error_Usuario").attr('style', 'color: rgb(153, 0, 0); display: inline;');
                    $("#Lbl_Mensaje_Error_Usuario").text(error);
                    $("#Lbl_Mensaje_Error_Usuario").show();
                    $("#Img_Error_Usuario").show();
                }
                else {
                    Limpia_Campos_Usuarios();
                    //Limpia el grid
                    //Limpia los grids
                    var usuarios = $('#Grid_Usuarios').datagrid('getRows');
                    $.each(usuarios, function (i, item) {
                        var indice = $('#Grid_Usuarios').datagrid('getRows').length - 1;
                        $('#Grid_Usuarios').datagrid('deleteRow', indice);
                    });

                    //regresa a la pantalla principal
                    Configurar_Botones_Alta_Usuario("Inicial");
                    $("#Lbl_Mensaje_Error_Usuario").removeAttr('style');
                    $("#Lbl_Mensaje_Error_Usuario").attr('style', 'color: rgb(153, 0, 0); display: inline;');
                    $("#Lbl_Mensaje_Error_Usuario").text(alta);
                    $("#Lbl_Mensaje_Error_Usuario").show();
                    $("#Img_Error_Usuario").show();
                }
            }
        }
    });
}
