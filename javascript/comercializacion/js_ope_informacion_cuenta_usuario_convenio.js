﻿
var objfooterRows;
var objtableRows;
var crezagos;
var crecargos;
var csaldo;

var campos_abono;
var campo_total_descuento;
var no_cuenta_convenio;
var no_factura;
var nota_cargo_id;

$(function(){
 
 no_cuenta_convenio=$("[id$='txt_no_cuenta']").val();
 no_factura= $("[id$='txt_no_factura_convenio']").val();
 campos_abono=$('#txt_abonos_c');
 campo_total_descuento=$('#txt_total_descuentos_c');

$('#ventana_convenios').window('close');

// inicializar fechas

$("[id$='txt_fecha_inicio_c']").datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'dd/mm/yy'
});

  bloquearControlesDinero();
   
 //eventos_plazos
 $('#txt_plazo_c').blur(function(){
     calcularAbonos();
 });  
 
 // eventos descuentos rezagos
 $('#txt_descuentos_rezago_c').blur(function(){
   calcularAbonos();
 }); 
 
 // eventos descuentos recargos
  $('#txt_descuentos_recargos_c').blur(function(){
   calcularAbonos();
 }); 
 
 
  // eventos descuentos rezagos
  $('#txt_descuentos_rezago_dinero_c').blur(function(){
   calcularAbonos();
 }); 
 
 
  // eventos descuentos recargos
  $('#txt_descuentos_recargos_dinero_c').blur(function(){
   calcularAbonos();
});



// eventos Pago inicial

$('#Txt_Pago_Inicial').blur(function () {
    calcularAbonos();
}); 
 
  $('#optPorcentaje').click(function(){
      
      bloquearControlesDinero();
      desbloquearControlesDescuentos();
      calcularAbonos(); 
  });
   
  $('#optDinero').click(function(){
     bloquearControlesDescuentos();
     desbloquearControlesDinero();
     calcularAbonos(); 
     
  });
  


});

//-------------------------------------------------------------------------------

function bloquearControlesDescuentos(){

 $('#txt_descuentos_rezago_c').attr('disabled',true);
 $('#txt_descuentos_recargos_c').attr('disabled',true);

}

function bloquearControlesDinero(){
  $('#txt_descuentos_rezago_dinero_c').attr('disabled',true);
  $('#txt_descuentos_recargos_dinero_c').attr('disabled',true);   
}


function desbloquearControlesDescuentos(){
  $('#txt_descuentos_rezago_c').attr('disabled',false);
  $('#txt_descuentos_recargos_c').attr('disabled',false);
}

function desbloquearControlesDinero(){

  $('#txt_descuentos_rezago_dinero_c').attr('disabled',false);
  $('#txt_descuentos_recargos_dinero_c').attr('disabled',false);  
  
}


//-------------------------------------------------------------------------------

function calcularAbonos(){
 var periodo;
 var descuentos_rezagos;
 var descuentos_recargos;
 
 var descuentos_rezagos_dinero;
 var descuentos_recargos_dinero;
 
 
 var total_descuentos;
 var total_abono;
 var total_apagar;
 
 periodo=parseInt( $('#txt_plazo_c').val());
 descuentos_rezagos= parseFloat($('#txt_descuentos_rezago_c').val());
 descuentos_recargos= parseFloat($('#txt_descuentos_recargos_c').val());
 
 
 descuentos_rezagos_dinero=parseFloat($('#txt_descuentos_rezago_dinero_c').val());
 descuentos_recargos_dinero=parseFloat($('#txt_descuentos_recargos_dinero_c').val());
 
 
 
 if(isNaN(periodo) || periodo=='undefined'){
   periodo=0; 
 }
 
 if(isNaN(descuentos_rezagos)|| descuentos_rezagos=='undefined'){
     descuentos_rezagos=0;
 }
 
 if(isNaN(descuentos_recargos)|| descuentos_recargos=='undefined'){
     descuentos_recargos=0;
 }
 
  if(isNaN(descuentos_rezagos_dinero)|| descuentos_rezagos_dinero=='undefined'){
     descuentos_rezagos_dinero=0;
 }
 
  if(isNaN(descuentos_recargos_dinero)|| descuentos_recargos_dinero=='undefined'){
     descuentos_recargos_dinero=0;
 }
 
 
 
 if ($('#optPorcentaje').attr('checked')){
         
         
         if(crezagos==0){
           $('#txt_descuentos_rezago_c').val(0);
          
         }
         
         if(crecargos==0){
           $('#txt_descuentos_recargos_c').val(0);
         }
         
         
         descuentos_rezagos_dinero= decimal((crezagos * descuentos_rezagos)/100,2);
         descuentos_recargos_dinero= decimal((crecargos*descuentos_recargos)/100,2);
        
         //total_descuentos= ((crezagos * descuentos_rezagos)/100)+ ((crecargos*descuentos_recargos)/100);
         
         if(isNaN(descuentos_rezagos_dinero) || descuentos_rezagos_dinero== 'undefined'){
           descuentos_rezagos_dinero=0
         }
         
          if(isNaN(descuentos_recargos_dinero) || descuentos_recargos_dinero== 'undefined'){
           descuentos_recargos_dinero=0
         }
         
         $('#txt_descuentos_rezago_dinero_c').val(descuentos_rezagos_dinero);
         $('#txt_descuentos_recargos_dinero_c').val(descuentos_recargos_dinero);
         
         
         total_descuentos=descuentos_rezagos_dinero+descuentos_recargos_dinero;
         
         
         if(isNaN(total_descuentos) || total_descuentos== 'undefined'){
           total_descuentos=0
         }
 
         total_descuentos=decimal(total_descuentos,2)
         campo_total_descuento.val(total_descuentos);
 
         if(periodo!=0)
          total_abono=(csaldo-total_descuentos)/ periodo;
         else
          total_abono=0; 
   
         if(isNaN(total_abono) || total_abono== 'undefined'){
            total_abono=0;
         }
 
        total_abono=decimal(total_abono,2);
        campos_abono.val(total_abono);
        
        total_apagar= decimal(  csaldo-total_descuentos,2);
        $('#txt_total_a_pagar_c').val(total_apagar);

        //siexisteunpagoinicialcalculatotalpagaryabonos
        if ($('#Txt_Pago_Inicial').val() > 0)
        {

            $('#txt_total_a_pagar_c').val(total_apagar - $('#Txt_Pago_Inicial').val());            
            if (periodo != 0) 
            {
                total_abono = ($('#txt_total_a_pagar_c').val()) / periodo;
            }
            else 
            {
                total_abono = 0;
            }
            total_abono = decimal(total_abono, 2);
            campos_abono.val(total_abono);
                       
        }
  }
  
  
  
   if ($('#optDinero').attr('checked')){
         
        
        if (descuentos_rezagos_dinero>crezagos){
                descuentos_rezagos_dinero=crezagos;
                $('#txt_descuentos_rezago_dinero_c').val(descuentos_rezagos_dinero);
        }
        if(descuentos_recargos_dinero>crecargos){
                descuentos_recargos_dinero=crecargos;
                $('#txt_descuentos_recargos_dinero_c').val(descuentos_recargos_dinero)
        }
          
         if(crezagos !=0) 
            descuentos_rezagos= decimal( (descuentos_rezagos_dinero * 100)/ crezagos ,2);
          else
            descuentos_rezagos=0
            
          if(crecargos !=0)  
             descuentos_recargos= decimal((descuentos_recargos_dinero *100)/crecargos,2);
            else
            descuentos_recargos=0  
        
        
         //total_descuentos= ((crezagos * descuentos_rezagos)/100)+ ((crecargos*descuentos_recargos)/100);
         
         if(isNaN(descuentos_rezagos) || descuentos_rezagos== 'undefined'){
           descuentos_rezagos=0
         }
         
          if(isNaN(descuentos_recargos) || descuentos_recargos== 'undefined'){
           descuentos_recargos=0
         }
         
         $('#txt_descuentos_rezago_c').val(descuentos_rezagos);
         $('#txt_descuentos_recargos_c').val(descuentos_recargos);
         
         
         total_descuentos=descuentos_rezagos_dinero+descuentos_recargos_dinero;
         
         
         if(isNaN(total_descuentos) || total_descuentos== 'undefined'){
           total_descuentos=0
         }
 
         total_descuentos=decimal(total_descuentos,2)
         campo_total_descuento.val(total_descuentos);
 
         if(periodo!=0)
          total_abono=(csaldo-total_descuentos)/ periodo;
         else
          total_abono=0; 
   
         if(isNaN(total_abono) || total_abono== 'undefined'){
            total_abono=0;
         }
 
        total_abono=decimal(total_abono,2);
        campos_abono.val(total_abono);
 
        total_apagar= decimal(  csaldo-total_descuentos,2);
        $('#txt_total_a_pagar_c').val(total_apagar);

        //siexisteunpagoinicialcalculatotalpagaryabonos
        if ($('#Txt_Pago_Inicial').val() > 0) {

            $('#txt_total_a_pagar_c').val(total_apagar - $('#Txt_Pago_Inicial').val());
            if (periodo != 0) {
                total_abono = ($('#txt_total_a_pagar_c').val()) / periodo;
            }
            else {
                total_abono = 0;
            }
            total_abono = decimal(total_abono, 2);
            campos_abono.val(total_abono);

        }
 
  }
  
  
  
  
 
 
}

//-------------------------------------------------------------------------------

function cargarValores(){
    csaldo=0;
    crezagos=0;
    crecargos=0;

    
    objfooterRows=$('#grid_faturado').datagrid('getFooterRows');
    objtableRows=$('#grid_faturado').datagrid('getRows'); 
    csaldo=parseFloat( objfooterRows[4].actual);
    crezagos=obtenerValor(objtableRows,"Rezago");
    crecargos=obtenerValor(objtableRows,"Recargo");
    
    $('#txt_s_convenio').val(csaldo);
    $('#txt_rz_convenio').val(crezagos);
    $('#txt_rc_convenio').val(crecargos);
    
}

function obtenerValor(obj,columna){
var row;
var valor=0;

   for(var i=0;i<obj.length;i++){
          row=obj[i];
         if(row.concepto==columna){
             valor= parseFloat( row.actual);
             return valor;
         }     
    }
  
  return valor;
}


//---------------------------------------------------------------------------------

function  guardarConvenio(){
var datos;
var detalle_factura;
var abonitos;
var total_apagar;

  if(valoresRequeridosGeneral("ventana_convenios")==false){
    $.messager.alert('Japami','Se requieren algunos valores','error');
    return;  
   }
    
   
  abonitos= parseFloat( $('#txt_abonos_c').val()); 
  total_apagar= parseFloat($('#txt_total_a_pagar_c').val());
  
  if(isNaN(abonitos) || abonitos<=0){
   $.messager.alert('Japami','No se han hecho abonos','error');
    return; 
  }
  
   if(isNaN(total_apagar) || total_apagar<=0){
   $.messager.alert('Japami','La canitadad a pagar es cero','error');
    return; 
  }
  
   
   desbloquearControlesDescuentos();
   desbloquearControlesDinero(); 
   
   datos= toJSON( $("#ventana_convenios :input").serializeArray());
   detalle_factura= toJSON(objtableRows);
  
   if ($('#optPorcentaje').attr('checked')){
       bloquearControlesDinero();
      desbloquearControlesDescuentos();
   
   }

  if ($('#optDinero').attr('checked')){
     bloquearControlesDescuentos();
     desbloquearControlesDinero();
  }
  
    
   $('#ventana_mensaje').window('open');     
       
     $.ajax({ //inicio
             type: "POST",
             url: "../../Controladores/Frm_Controlador_Consulta_Usuario.aspx/guardarConvenios",
             data: "{'datos':'" + datos + "','no_cuenta':'" + no_cuenta_convenio + "','no_factura':'" + no_factura + "','detalle_factura':'"+ detalle_factura +"'}",
             contentType: "application/json; charset=utf-8",
             dataType: "json",
             success: function (response) {
                 var respuesta = eval("(" + response.d + ")");
                 respuesta.mensaje = respuesta.mensaje.slice(0, respuesta.mensaje.indexOf('}') + 1);                 
                 $('#ventana_mensaje').window('close');
                 var Obj = jQuery.parseJSON(respuesta.mensaje);
                 
                 if (Obj.Mensaje == "bien") {      
                     nota_cargo_id=respuesta.dato1;
                     procesoExitoso_convenio(Obj.No_Pago);
                     
                 } else {
                    $('#ventana_mensaje').window('close');
                    $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.Mensaje, 'error');
                     
                 }

             },
             error: function(result) {
                 $('#ventana_mensaje').window('close');
                 alert("ERROR " + result.status + ' ' + result.statusText);

             }


         });  //fin peticion ajax

   

}



//---------------------------------------------------------------------------------

function procesoExitoso_convenio( No_Pago ){

    $.messager.alert('Japami', 'Proceso Exitoso', 'info', function () {
        $("#ventana_convenios :input").val('');
        $("#ventana_convenios").window('close');
        

        window.open("../../Reporte/frm_controlador_crystal.aspx?nombre_reporte=formato_nota_cargo&nota_cargo_id=" + nota_cargo_id, "NotaCargo", "width=400px,height=400px,scrollbars=NO");

        window.location = "Frm_Ope_Cor_Consulta_Usuario.aspx?nocuenta=" + no_cuenta_convenio;

        window.open("../../Reporte/frm_controlador_reporte_pdf.aspx?nombre_reporte=Formato_Convenio&noPagoConvenio=" + No_Pago, "CertificarRecibo", "width=1000px,height=1000px,scrollbars=YES,resizable=YES");



    });

}


//---------------------------------------------------------------------------------

Number.prototype.decimal = function(num) {
    pot = Math.pow(10, num);
    return parseInt(this * pot) / pot;
}

function decimal(valor,no_decimales) {
    n = eval(valor);
    return n.decimal(no_decimales);
} 
