﻿
$(function(){

    crearGridSubirArchivo();
    subirArchivo();
    // inicializamos fechas
$("[id$='txt_fecha_registro_folio']").datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'dd/mm/yy'
});
    
   
});


//---------------------------------------------
function crearGridSubirArchivo(){
var lastIndex;

$('#grid_subir_archivos').datagrid({  // tabla inicio
        title: 'Subir Archivos',
        width: 750,
        height: 200,
        columns: [[
        { field: 'nombre_archivo', title: 'Archivo', width: 200 },
        { field: 'nombre_nuevo', title: 'Nombre Nuevo', width: 200,editor:'text' },
        { field: 'comentario', title: 'Comentario', width: 300,editor: 'text' }      
    ]],
        pageSize: 10,
        pageList: [30],
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        striped: true,
        loadMsg: 'cargando...',
        nowrap: false,
        toolbar: [{
            id: 'btn_table_subir_archivo',
            text: 'Subir',
            iconCls: 'icon-search',
            handler: function() {
              
            }
          },'-',{
					id:'btn_table_aceptar_cambios',
					text:'Aceptar',
					iconCls:'icon-save',
					handler:function(){
						$('#grid_subir_archivos').datagrid('acceptChanges');
						lastIndex=-1;

					}
             },'-',{
					id:'btn_table_eliminar_archivo',
					text:'Eliminar',
					iconCls:'icon-remove',
					handler:function(){
						eliminarArchivo();
					}
           
        }],
        onClickRow: function(rowIndex, rowData) {
          if (lastIndex != rowIndex){
						$('#grid_subir_archivos').datagrid('endEdit', lastIndex);
						$('#grid_subir_archivos').datagrid('beginEdit', rowIndex);
						validarCajas(rowIndex);
					}
					lastIndex = rowIndex;
					

        },
        onBeforeLoad: function() {
             	$('#grid_subir_archivos').datagrid('acceptChanges');
        },
        onLoadSuccess: function() {

        }

    });          // tabla final


}


//---------------------------------------------

function validarCajas(rowIndex){

var editors = $('#grid_subir_archivos').datagrid('getEditors', rowIndex);  
var editor_nombre_nuevo=editors[0];
var editor_comentario_archivo=editors[1];

editor_nombre_nuevo.target.bind('change', function(){  
        calculate();  
    }); 

editor_comentario_archivo.target.bind('change', function(){  
        calculate();  
    }); 


 function calculate(){  
        setTimeout(function(){
            var datos;
            datos=obtenerCadenaPermitida( editor_nombre_nuevo.target.val(),""); 
            datos=datos.substring(0,50);         
            editor_nombre_nuevo.target.val($.trim(datos));  
            
            datos=obtenerCadenaPermitida( editor_comentario_archivo.target.val(),"");
            datos=datos.substring(0,80);
            editor_comentario_archivo.target.val($.trim(datos));
            
        }, 100);  
    } 

}


//---------------------------------------------

function subirArchivo(){

    new AjaxUpload('#btn_table_subir_archivo', {
        action: '../../Controladores/frm_controlador_subir_archivo.aspx',
        data: { accion: 'subir_archivo' },
        onSubmit: function(file, ext) {
            if (!(ext && /^(jpg|png)$/i.test(ext))) {
                $.messager.alert('Japami', 'Formato Incorrecto');
                return false;
            }
            $('#ventana_mensaje').window('open');

        },
        onComplete: function(file) {
            $('#ventana_mensaje').window('close');
            if (buscar_en_grid("grid_subir_archivos", file, "nombre_archivo") == false)
                $('#grid_subir_archivos').datagrid('appendRow', { nombre_archivo: file });
        }
    });

}

//---------------------------------------------

function eliminarArchivo(){
 var renglon;
 
 renglon=$("#grid_subir_archivos").datagrid('getSelected');
 
 if(renglon==null){
    $.messager.alert('Japami','Selecciona una registro');
    return false;
 }
 
 
 $('#ventana_mensaje').window('open');
 
     $.post("../../Controladores/frm_controlador_subir_archivo.aspx", { accion: 'eliminar_archivo', nombre_archivo: renglon.nombre_archivo },
    function(response) {
       $('#ventana_mensaje').window('close');
       if (response.mensaje == "bien") {
           var index=$('#grid_subir_archivos').datagrid('getRowIndex',renglon);
            $('#grid_subir_archivos').datagrid('deleteRow',index);                 
            $.messager.alert('Japami', 'Proceso terminado');
         
     }
    else { 
          $('#ventana_mensaje').window('close');
          $.messager.alert('Japami', response.mensaje); }

}, "json");
 

}

//------------------------------------------------

