﻿
var accion;
var REGION_ID;
var SECTOR_ID;
var MANZANA_ID;
var COLONIA_ID;
var Ruta_Reparto_ID;
var nodo_ruta;
var nodo_sector;
var id_combinado;
$(function() { //inicio

// ventana utilizada como modal que aparece cuando se hace una petición
$('#ventana_mensaje').window('close');

// inicializamos fechas
$("[id$='txt_fecha_visita']").datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'dd/mm/yy'
});

    
    crear_arbol_region();
    crear_arbol_rutas();
    crearGridRutas();
});    // fin


//---------------------------------------------------------

function crear_arbol_region() {

    $('#arbol_programacion').tree({
        checkbox:false,
        url: '../../Controladores/frm_controlador_programacion_lectura.aspx?accion=region',
        onBeforeExpand: function(node) {
            llenarArbol(node);
        },
        onClick: function(node) {

        },
        onCheck: function(node, checked) {
      
        }
    });
    

}

//---------------------------------------------------------
function llenarArbol(node) {
    accion = node.attributes.valor1;
    REGION_ID = node.attributes.valor2;
    SECTOR_ID = node.attributes.valor3;
    MANZANA_ID = node.attributes.valor4;
    COLONIA_ID = node.attributes.valor5;
    if (accion == "sectores")
        $('#arbol_programacion').tree('options').url = "../../Controladores/frm_controlador_programacion_lectura.aspx?accion=" + accion + "&REGION_ID=" + REGION_ID;
    if  (accion == "manzanas")
        $('#arbol_programacion').tree('options').url = "../../Controladores/frm_controlador_programacion_lectura.aspx?accion=" + accion + "&REGION_ID=" + REGION_ID + "&SECTOR_ID=" + SECTOR_ID;
    if (accion == "colonias")
        $('#arbol_programacion').tree('options').url = "../../Controladores/frm_controlador_programacion_lectura.aspx?accion=" + accion + "&REGION_ID=" + REGION_ID + "&SECTOR_ID=" + SECTOR_ID + "&MANZANA_ID=" + MANZANA_ID;
    if (accion == "calles")
        $('#arbol_programacion').tree('options').url = "../../Controladores/frm_controlador_programacion_lectura.aspx?accion=" + accion + "&REGION_ID=" + REGION_ID + "&SECTOR_ID=" + SECTOR_ID + "&MANZANA_ID=" + MANZANA_ID + "&COLONIA_ID="+COLONIA_ID;
           
            
}

//---------------------------------------------------------

function crear_arbol_rutas() {

    $('#arbol_rutas').tree({
        checkbox: false,
        url: '../../Controladores/frm_controlador_programacion_lectura.aspx?accion=rutas',
        onBeforeExpand: function(node) {
            llenarArbolRuta(node);
        },
        onClick: function(node) {

        }
    });
   
}


//---------------------------------------------------------

function llenarArbolRuta(node) {
    accion = node.attributes.valor1;
    Ruta_Reparto_ID = node.attributes.valor2;
    
    if (accion == "lecturistas")
        $('#arbol_rutas').tree('options').url = "../../Controladores/frm_controlador_programacion_lectura.aspx?accion=" + accion + "&Ruta_Reparto_ID=" + Ruta_Reparto_ID;

}

//---------------------------------------------------------

function crearGridRutas() {

    $('#tabla_ruta').treegrid({
        title: 'Ruta',
        width: 810,
        height: 150,
        rownumbers: true,
        animate: true,
        idField: 'clave',
        treeField: 'clave',
        animate: true,
        frozenColumns: [[
	  { title: 'clave', field: 'clave', width: 37 },
	  { title: 'Ruta', field: 'ruta', width: 300 },
	  { title: 'lecturista', field: 'lecturista', width: 300 }
	 ]],
        columns: [[
	  { field: 'region', title: 'Region', width: 100 },
	  { field: 'id_region', title: 'id_region', width: 10 },
	  { field: 'id_lecturista', title: 'id_lecturista', width: 10 },
	  { field: 'id_sector', title: 'id_sector', width: 10 }

     ]],
        toolbar: [{
            id: 'btn_treegrid_agregar',
            text: 'Agregar',
            iconCls: 'icon-agregar',
            handler: function() {
                agregaRuta();
            }
        }, '-', {
            id: 'btn_treegrid_eliminar',
            text: 'Eliminar',
            iconCls: 'icon-eliminar',
            handler: function() {
                eliminarRuta();
            }
}]

        });

//ocultar_columnas
$('#tabla_ruta').treegrid('hideColumn', 'id_region');
$('#tabla_ruta').treegrid('hideColumn', 'id_lecturista');
$('#tabla_ruta').treegrid('hideColumn', 'id_sector');    
    
}


//---------------------------------------------------------

function agregaRuta() {
    var id_lecturista;
    var nodo_ruta;
    nodo_ruta_hijo = $('#arbol_rutas').tree('getSelected');
    nodo_sector = $('#arbol_programacion').tree('getSelected');


    if (nodo_ruta_hijo == null) {
        $.messager.alert('Programaci&oacute;n de Lecturas', 'Selecciona un lecturista');
        return;
    }

    if (nodo_ruta_hijo.attributes.valor7 != "lecturistas") {
        $.messager.alert('Programaci&oacute;n de Lecturas', 'Selecciona un Lecturista');
        return;
    }

    if (nodo_sector == null) {
        $.messager.alert('Programaci&oacute;n de Lecturas', 'Selecciona un Sector');
        return;
    }

    if (nodo_sector.attributes.valor7 != "sector") {
        $.messager.alert('Programaci&oacute;n de Lecturas', 'Selecciona un Sector');
        return;
    }


    // recuparamos el id_lecturista
    var nodo_ruta = $('#arbol_rutas').tree('getParent', nodo_ruta_hijo.target);
    id_lecturista = nodo_ruta_hijo.attributes.valor3;
     
    //id_combinado = nodo_ruta.id + '-' + nodo_sector.id;
    id_combinado = 's' + nodo_sector.id;
    var node1 = $('#tabla_ruta').treegrid('find', id_combinado);
    if (node1 != null) {
       $.messager.alert('Programaci&oacute;n de Lecturas', 'El Sector Seleccionado ya esta agregado');
        return;
    }
    
    // hacer el papa
    var node = $('#tabla_ruta').treegrid('find', nodo_ruta.id);
    if (node == null) {
        var data = [{
            clave: nodo_ruta.id,
            ruta: nodo_ruta.text,
            lecturista:nodo_ruta_hijo.text
            }];
            $('#tabla_ruta').treegrid('append', {
                parent: null,
                data: data
            });
        }
     
        // hacer la hijo
            var node_papa = $('#arbol_programacion').tree('getParent', nodo_sector.target);
                var data1 = [{
                    clave: id_combinado,
                    ruta: nodo_sector.text,
                    id_sector:nodo_sector.id,
                    region:node_papa.text,
                  id_region:node_papa.id,
                  id_lecturista:id_lecturista
                }];

                $('#tabla_ruta').treegrid('append', {
                    parent: nodo_ruta.id,
                    data: data1
                });

       $.messager.alert('Programaci&oacute;n de Lecturas', 'Elemento agregado');
    


}

//---------------------------------------------------------
function eliminarRuta() {
    var nodo;
    var id_nodo;
    nodo = $('#tabla_ruta').treegrid('getSelected');
    
    if (nodo == null) {
        $.messager.alert('Programaci&oacute;n de Lecturas', 'Tienes que seleccionar un elemento');
        return;
    }

    id_nodo = nodo.clave;

    if (id_nodo.indexOf("s") != -1) {
        var nodo_papa = $('#tabla_ruta').treegrid('getParent', nodo.clave);
        var nodo_hijos = $('#tabla_ruta').treegrid('getChildren', nodo_papa.clave);
        if (nodo_hijos.length == 1) {
            $('#tabla_ruta').treegrid('remove', nodo.clave);
            $('#tabla_ruta').treegrid('remove', nodo_papa.clave);
        } else {
            $('#tabla_ruta').treegrid('remove', nodo.clave);
        }

    }
    else {
        $('#tabla_ruta').treegrid('remove', nodo.clave);
    }
     
    $('#tabla_ruta').treegrid('refresh');





}


//---------------------------------------------------------

function guardarProgramacion() {
    var str_fecha;
    var programacion;
    
    str_fecha = $.trim($("[id$='txt_fecha_visita']").val());
    programacion = $('#tabla_ruta').treegrid('getData');

    if (programacion.length == 0) {
        $.messager.alert('', 'Tienes que hacer una programaci&oacute;n');
        return;
    }

    if (str_fecha.length == 0) {
        $.messager.alert('Programaci&oacute;n de Lecturas', 'Tienes que poner una fecha de Lectura');
        return;
    }

    $('#ventana_mensaje').window('open');
    $.ajax({
        type: "POST",
        url: "../../Controladores/frm_controlador_programacion_lectura.aspx/guardarProgramacion",
        data: "{'programacion':'" + JSON.stringify(programacion) + "','fecha':'" + str_fecha + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(response) {
            var respuesta = eval("(" + response.d + ")");
            if (respuesta.mensaje == "bien") {
                $('#ventana_mensaje').window('close');
                $.messager.alert('Programaci&oacute;n de Lecturas', 'Proceso terminado');
            }
            else {
                $('#ventana_mensaje').window('close');
                $.messager.alert('Programaci&oacute;n de Lecturas', 'Ocurrio un error:' + respuesta.mensaje, 'error');
            }

        },
        error: function(result) {
            $('#ventana_mensaje').window('close');
            $.messager.alert('Programaci&oacute;n de Lecturas', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');

        }

    });

    
  

}