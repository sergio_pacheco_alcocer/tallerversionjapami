﻿var lastIndex;
var blnBanderaEdito=false;

$(function(){
   
 crearTabla(); 
 
});


//------------------------------------------------------------------------------------------------

function crearTabla(){


      $('#tabla_conceptos').datagrid({  // tabla inicio
            title: 'Conceptos a Pagar',
            width: 820,
            height: 300,
            columns: [[
            { field: 'concepto', title:'Concepto', width:150 },
            { field: 'importe', title: 'Importe', width: 120 },
            { field: 'impuesto', title: 'Impuesto', width: 120 },
            { field: 'total', title: 'Total', width: 120 },  
            { field: 'total_abonado', title: 'T.Abonado', width: 120},
            { field: 'total_saldo', title: 'T.Saldo', width: 120, editor:{type:'numberbox',options:{precision:2, min:0, max:999999}}},
            { field: 'cantidad_iva', title: '%Iva', width: 80 },
            { field: 'lleva_iva', title: 'Lleva Iva', width: 80 },
            { field: 'no_factura_recibo', title: 'id', width: 0 },
            { field: 'concepto_id', title: 'id', width: 0 }
           
                       
        ]],
           onClickRow: function(rowIndex, rowData) {
               
             if (lastIndex != rowIndex){
			      $('#tabla_conceptos').datagrid('endEdit', lastIndex);
			      $('#tabla_conceptos').datagrid('beginEdit', rowIndex);
		          calcularValoreNuevos(rowIndex,rowData);  	  								
			    }
		    lastIndex = rowIndex;
          
            },
        
            pageSize: 10,
            pageList: [30],
            pagination: false,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            striped: true,
            showFooter:true,
            loadMsg: 'cargando...',
            nowrap: false
       

        });          // tabla final
        
    $('#tabla_conceptos').datagrid('hideColumn','no_factura_recibo'); 
    $('#tabla_conceptos').datagrid('hideColumn','concepto_id'); 

}

function calcularValoreNuevos(rowIndex,renglon_datos){
    var editores = $('#tabla_conceptos').datagrid('getEditors', rowIndex);
    var editor_saldo = editores[0];
    
    
     editor_saldo.target.bind('change', function(){  
        calcular();  
    });
    
  
    function calcular(){  
        setTimeout(function(){        
              calcularTotales(renglon_datos);
        }, 100);
        
    
     }

}

//-------------------------------------------------------------------------------------------------

function calcularTotales(renglon_datos){
   var objDatos;
   var renglon;
   var sumatoria_total=0;
   var sumatoria_impuesto=0;
   var sumatoria_importe=0;
   var sumatoria_total_abonanado=0;
   var sumatoria_total_saldo=0;
   
   
   $('#tabla_conceptos').datagrid('acceptChanges');  
   lastIndex=-1;
   objDatos= $('#tabla_conceptos').datagrid('getData');
  
   if(objDatos.total>0){
        for(var i=0;i <objDatos.rows.length;i++){  
        
           // encontramos el registro que editamos con la nueva cantidad
           if(objDatos.rows[i].concepto==renglon_datos.concepto){
              var importe;
              var impuesto;
              var total;
              var total_abonado; 
              var total_saldo;
              var cantidad_iva;
              
              total_abonado=parseFloat(objDatos.rows[i].total_abonado);
              total_saldo=parseFloat(objDatos.rows[i].total_saldo);
              cantidad_iva=parseFloat(objDatos.rows[i].cantidad_iva);
              
              if(isNaN(total_saldo)){ total_saldo=0;}
              if(isNaN(cantidad_iva)){cantidad_iva=0;}
              if(isNaN(total_abonado)){total_abonado=0;}
              
              //calculamos el nuevo importe
              importe= ((total_saldo * 100) + (total_abonado *100))/(100 + cantidad_iva);
              //calculamos el nuevo impuesto
              impuesto=(importe * cantidad_iva)/100;
              //calculamos el nuevo impuesto
              total=importe+ impuesto;
              
              objDatos.rows[i].total_saldo= decimal( total_saldo,2);
              objDatos.rows[i].importe= decimal( importe,2);
              objDatos.rows[i].impuesto= decimal( impuesto,2);
              objDatos.rows[i].total= decimal( total,2);
              blnBanderaEdito=true; 
           }
           
           sumatoria_total = sumatoria_total + parseFloat( objDatos.rows[i].total);
           sumatoria_importe= sumatoria_importe + parseFloat( objDatos.rows[i].importe);
           sumatoria_impuesto= sumatoria_impuesto + parseFloat( objDatos.rows[i].impuesto);
           sumatoria_total_abonanado= sumatoria_total_abonanado + parseFloat(objDatos.rows[i].total_abonado);
           sumatoria_total_saldo= sumatoria_total_saldo + parseFloat(objDatos.rows[i].total_saldo);                
             
                     
        }
        
         objDatos.footer[0].importe= decimal( sumatoria_importe,2);
         objDatos.footer[0].impuesto=decimal( sumatoria_impuesto,2);
         objDatos.footer[0].total= decimal( sumatoria_total,2);
         objDatos.footer[0].total_abonado= decimal(sumatoria_total_abonanado,2);
         objDatos.footer[0].total_saldo= decimal(sumatoria_total_saldo,2);
          
        
        $('#tabla_conceptos').datagrid('loadData',objDatos);
   }
   
   
}


//-------------------------------------------------------------------------------------------------

function buscarConceptosPagos(){
 var no_cuenta;
 
 no_cuenta=$.trim( $("[id$='txt_no_cuenta']").val());

 if(no_cuenta ==""){
    $.messager.alert('Japami','Introduce un n&uacute;mero de cuenta');
    return ;
 } 
    
$('#tabla_conceptos').datagrid('loadData',{total:0 , rows:[]});
$('#tabla_conceptos').datagrid('reloadFooter', { total: 0, rows: [] });
$('#tabla_conceptos').datagrid({ url: '../../Controladores/Frm_Controlador_Consulta_Usuario.aspx', queryParams: { accion: 'obtenerfacturacionconceptos', nocuenta:no_cuenta}, pageNumber: 1 }); 


}


//--------------------------------------------------------------------------------------------------

function  guardarDatos(){
   var datos;
   var totales;
 
   if(blnBanderaEdito==false){
      $.messager.alert('Japami','Aun no has editado alg&uacute;n concepto');
      return;
   }
        
   datos= toJSON( $('#tabla_conceptos').datagrid('getRows'));
   totales= toJSON( $('#tabla_conceptos').datagrid('getFooterRows'));
   
   $('#ventana_mensaje').window('open');    
    
        $.ajax({ //inicio
             type: "POST",
             url: "../../Controladores/Frm_Controlador_Consulta_Usuario.aspx/guardarCambiosFacturacion",
             data: "{'datos':'" + datos + "','totales':'" + totales + "'}",
             contentType: "application/json; charset=utf-8",
             dataType: "json",
             success: function(response) {
                 $('#ventana_mensaje').window('close'); 
                 var respuesta = eval("(" + response.d + ")");
                 if (respuesta.mensaje == "bien") {                   
                     procesoExitoso_GuardarCambios();
                     
                 } else {
                    $('#ventana_mensaje').window('close');
                     $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                     
                 }

             },
             error: function(result) {
                 $('#ventana_mensaje').window('close');
                 alert("ERROR " + result.status + ' ' + result.statusText);

             }


         });  //fin peticion ajax
   
   
   
}

//--------------------------------------------------------------------------------------------------
function procesoExitoso_GuardarCambios(){
 blnBanderaEdito=false   
 $.messager.alert('Japami','Proceso Teminado','info',function(){
    $('#tabla_conceptos').datagrid('loadData',{total:0 , rows:[]});
    $('#tabla_conceptos').datagrid('reloadFooter', { total: 0, rows: [] });
   
 });

}

//--------------------------------------------------------------------------------------------------
Number.prototype.decimal = function(num) {
    pot = Math.pow(10, num);
    return parseInt(this * pot) / pot;
}

function decimal(valor,no_decimales) {
    n = eval(valor);
    return n.decimal(no_decimales);
} 
