﻿

$(function(){
  $('#ventana_mensaje').window('close');  
  subirArchivo();
  
  // evento de pestallas
  $('#Cmb_Hojas').change(function (event) {
       validarArchivo();
  
  });
  

});


//---------------------------------------------------------------------------------------------------------------


function subirArchivo(){

  new AjaxUpload('#btn_buscar_archivo', {
		action: '../../Controladores/frm_controlador_subir_archivo.aspx', 
		data : {accion:'subir_archivo'},
		onSubmit : function(file , ext){
		      
		    if (!(ext && /^(xls|xlsx)$/i.test(ext))) {
                $.messager.alert('Japami', 'Formato Incorrecto');
                return false;
            }  
		    
		    $('#txt_nombre_archivo').val('');
		    //Borramos todas las opciones del combo 
            $('#Cmb_Hojas').find('option').remove().end();  
            limpiarGrid();   
			$('#ventana_mensaje').window('open');
	
		},
		onComplete : function(file){
		   $('#ventana_mensaje').window('close');
		   $('#txt_nombre_archivo').val(file);
		     
		}		
	});

}



//---------------------------------------------------------------------------------------------------------------

function  validarArchivo(){
  
 var nombre_archivo;
 var nombre_tienda;
 var id_establecimiento;
 var nombre_pestaña; 

nombre_archivo=$.trim($('#txt_nombre_archivo').val());
id_establecimiento=$('#Cmb_Establecimientos option:selected').val();
nombre_tienda= $.trim($('#Cmb_Establecimientos option:selected').text());
nombre_pestaña= $.trim($('#Cmb_Hojas option:selected').text());

limpiarGrid();

if (nombre_tienda=="" ){
  $.messager.alert('Japami','Tiene que seleccionar un establecimiento');
   return ;
}

if (nombre_tienda.indexOf("SELECCIONE")!= -1 ){
  $.messager.alert('Japami','Tiene que seleccionar un establecimiento');
   return ;
}

if (nombre_pestaña.indexOf("SELECCIONE")!= -1){
    $.messager.alert('Japami','Tienes que seleccionar un nombre de pestaña');
    return ;
}

if (nombre_archivo==""){
   $.messager.alert('Japami','Tiene que haber subido un archivo');
   return ;
}

leerArchivoExcel(nombre_archivo,id_establecimiento,nombre_pestaña);

}

//---------------------------------------------------------------------------------------------------------------

function crearTablaOxxo(nombre){
 
 
 
   $('#' + nombre).datagrid({  // tabla inicio
            title: 'Pagos ',
            width: 750,
            height: 350,
            columns: [[
        {field:'ck',checkbox:true},    
        { field: 'no_consecutivo', title: 'No.', width: 40, sortable: true },
        { field: 'institucion', title: 'Institucion', width: 150, sortable: true },
        { field: 'sucursal', title: 'Sucursal', width: 150, sortable: true },
        { field: 'fecha_pago', title: 'F.Pago', width: 150, sortable: true },
        { field: 'no_cuenta', title: 'Cuenta', width: 150, sortable: true },
        { field: 'monto', title: 'Monto', width: 150, sortable: true },
        { field: 'saldo', title: 'Saldo', width: 150, sortable: true },
        { field: 'codigo', title: 'Codigo', width: 150, sortable: true },
        { field: 'no_factura_recibo', title: 'no_factura', width: 150, sortable: true }

    ]],     
             onClickRow: function(rowIndex, rowData) {
            
            }, 
            pageSize: 50,
            pagination: false,
            rownumbers: false,
            remoteSort: false,
            fitColumns: false,
            singleSelect: false,
            showFooter: true,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap: false

        });  // tabla final
        
        $('#' + nombre).datagrid('hideColumn','no_factura_recibo');
        
        
 
    
}


//------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------------

function obtenerNombrePestañas(nombre_archivo){

 $('#ventana_mensaje').window('open'); 

 $.ajax({ //inicio
             type: "POST",
             url: "../../Controladores/Frm_Controlador_Cajas_Pagos_Establecimientos.aspx/obtenerPestañasArchivoExcel",
             data: "{'nombre_archivo':'" + nombre_archivo + "'}",
             contentType: "application/json; charset=utf-8",
             dataType: "json",
             success: function(response) {
                 $('#ventana_mensaje').window('close'); 
                 var respuesta = eval("(" + response.d + ")");
                 procesoExitosoPestañas(respuesta);
                 
                 
             },
             error: function(result) {
                 $('#ventana_mensaje').window('close');
                 alert("ERROR " + result.status + ' ' + result.statusText);

             }


  }); // fin




}


function procesoExitosoPestañas(respuesta){
  
  
 //Borramos todas las opciones del combo 
 $('#Cmb_Hojas').find('option').remove().end();    
  
  var options = '<option value="Seleccione"><-SELECCIONE-></option>';
  if (respuesta.length>0) { 
          $.each(respuesta, function (i, item) {
            options += '<option value="' + item.value + '">' + item.name + '</option>';
           });       
        }else{
           $.messager.alert('Japami','Se encontro en información');
        }
        
 $('#Cmb_Hojas').append(options);

}


//---------------------------------------------------------------------------------------------------------------

function leerArchivoExcel(nombre_archivo,id_establecimiento,nombre_pestaña){

 $('#ventana_mensaje').window('open'); 

 $.ajax({ //inicio
             type: "POST",
             url: "../../Controladores/Frm_Controlador_Cajas_Pagos_Establecimientos.aspx/leerArchivo",
             data: "{'nombre_archivo':'" + nombre_archivo + "','id_establecimiento':'" + id_establecimiento + "','nombre_pestaña':'" + nombre_pestaña + "'}",
             contentType: "application/json; charset=utf-8",
             dataType: "json",
             success: function(response) {
                 //$('#ventana_mensaje').window('close'); 
                 var respuesta = eval("(" + response.d + ")");
                 if (respuesta.mensaje == "bien") {                   
                      procesoExitosoLectura(respuesta);
                             
                 } else {
                     $('#ventana_mensaje').window('close');
                     $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                     
                 }

             },
             error: function(result) {
                 $('#ventana_mensaje').window('close');
                 alert("ERROR " + result.status + ' ' + result.statusText);

             }


  });

}

function procesoExitosoLectura(respuesta){
  
  var objDatosAplicados;
  var objDatosNoAplicados;
  var objDatosErrores;
  
 
  // creamos las tablas 
  crearTablaOxxo("tabla_pagos_realizar"); 
  objDatosNoAplicados=json_parse(respuesta.dato2);
  $('#tabla_pagos_realizar').datagrid('loadData',objDatosNoAplicados);
  $('#tabla_pagos_realizar').datagrid({title:'Pagos por Realizar Total Elementos:' + objDatosNoAplicados.total});
  
  crearTablaOxxo("tabla_pagos_aplicados"); 
  objDatosAplicados=json_parse(respuesta.dato1);
  $('#tabla_pagos_aplicados').datagrid('loadData',objDatosAplicados);
  $('#tabla_pagos_aplicados').datagrid({title:'Pagos Aplicados Total Elementos:' + objDatosAplicados.total});
  
  
  crearTablaOxxo("tabla_pagos_errores"); 
  objDatosErrores=json_parse(respuesta.dato3);
  $('#tabla_pagos_errores').datagrid('loadData',objDatosErrores);
  $('#tabla_pagos_errores').datagrid({title:'Errores de Lectura:' + objDatosErrores.total});
  
  
  $('#ventana_mensaje').window('close'); 

}


//---------------------------------------------------------------------------------------------------------------


function aplicarCobros(id_establecimiento,nombre_tienda){
var datos;
var caja_id;

 $('#ventana_mensaje').window('open'); 
 
 datos= toJSON( $('#tabla_pagos_realizar').datagrid('getSelections'));
 caja_id=$("[id$='Txt_Operacion_Caja_ID']").val();

 $.ajax({ //inicio
             type: "POST",
             url: "../../Controladores/Frm_Controlador_Cajas_Pagos_Establecimientos.aspx/cobrarEstablecimiento",
             data: "{'datos':'" + datos + "','caja_id':'" + caja_id + "','establecimiento_id': '" + id_establecimiento + "','establecimiento': '" + nombre_tienda + "'}",
             contentType: "application/json; charset=utf-8",
             dataType: "json",
             success: function(response) {
                 //$('#ventana_mensaje').window('close'); 
                 var respuesta = eval("(" + response.d + ")");
                 if (respuesta.mensaje == "bien") {                   
                      procesoExistosoCobro(respuesta);
                             
                 } else {
                    $('#ventana_mensaje').window('close');
                     $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                     
                 }

             },
             error: function(result) {
                 $('#ventana_mensaje').window('close');
                 alert("ERROR " + result.status + ' ' + result.statusText);

             }


  });


}


function procesoExistosoCobro(respuesta){
    $('#ventana_mensaje').window('close'); 
    
   $.messager.alert('Japami', 'Proceso Terminado', 'info', function(){
        limpiarGrid();    
   });

}

//---------------------------------------------------------------------------------------------------------------
function limpiarGrid(){
   $('#Div_Pagos_Realizar').html("<table id='tabla_pagos_realizar'> </table>");
   $('#Div_Pagos_Aplicados').html("<table id='tabla_pagos_aplicados'> </table>");
   $('#Div_Pagos_Error').html("<table id='tabla_pagos_errores'> </table>");

}