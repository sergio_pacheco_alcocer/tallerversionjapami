﻿

$(function(){

crearTablavisualizarConvenios();
crearTablaDetallesConvenios();

 // obtenemos datos de convenios
   $('#grid_convenios').datagrid({ url: '../../Controladores/Frm_Controlador_Consulta_Usuario.aspx', queryParams: { accion: 'obtenerDatosConvenios', nocuenta:no_cuenta}, pageNumber: 1 });
   

});

//-----------------------------------------------------------------

function crearTablaDetallesConvenios(){

       $('#grid_convenios_detalles').datagrid({  // tabla inicio
            title: 'Detalles Pagos a Convenios',
            width: 790,
            height: 250,
            columns: [[
        { field: 'no_convenio', title: 'No. Convenio', width: 120, sortable: true },
        { field: 'no_pago', title: 'No. Pago', width: 120, sortable: true },
        { field: 'monto', title: 'Monto', width: 150, sortable: true },
        { field: 'fecha_pago', title: 'F. Pago', width: 150, sortable: true },
        { field: 'abono', title: 'Abono', width: 100, sortable: true },
        { field: 'saldo', title: 'Saldo', width: 200, sortable: true }
      
    ]],     
             onClickRow: function(rowIndex, rowData) {
            
               
                
               
            }, 
            pageSize: 50,
            pagination: true,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            showFooter: false,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap: false

        });


}


//------------------------------------------------------------------

function crearTablavisualizarConvenios(){

       $('#grid_convenios').datagrid({  // tabla inicio
            title: 'convenios',
            width: 790,
            height: 250,
            columns: [[
        { field: 'no_convenio', title: 'No. Convenio', width: 120, sortable: true },
        { field: 'no_cuenta', title: 'No. Cuenta', width: 120, sortable: true },
        { field: 'adeudo', title: 'Adeudo', width: 150, sortable: true },
        { field: 'saldo', title: 'Saldo', width: 150, sortable: true },
        { field: 'estatus', title: 'Estatus', width: 100, sortable: true },
        { field: 'numero_meses', title: 'No. Meses', width: 200, sortable: true },
        { field: 'fecha_inicio', title: 'F. Inicio', width: 200, sortable: true },
        { field: 'total_descuento', title: 'T. Descuento', width: 200, sortable: true },
        { field: 'total_descuento_rezago', title: 'D. Rezagos', width: 150, sortable: true },
        { field: 'total_descuento_recargo', title: 'D. Recargo', width: 150, sortable: true },
        { field: 'tasa_iva', title: 'Tasa IVA', width: 150, sortable: true }

    ]],     
             onClickRow: function(rowIndex, rowData) {
            
            $('#grid_convenios_detalles').datagrid('loadData',{total:0,rows:[]});
            $('#grid_convenios_detalles').datagrid({ url: '../../Controladores/Frm_Controlador_Consulta_Usuario.aspx', queryParams: { accion: 'obtenerDatosConveniosDetalles', noconvenio:rowData.no_convenio}, pageNumber: 1 });
   
            
            }, 
            pageSize: 50,
            pagination: true,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            showFooter: false,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap: false

        });  // tabla final



}

