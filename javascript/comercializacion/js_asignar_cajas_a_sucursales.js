﻿var operacion = "ninguno";
var accion;
var sucursal_id;
var caja_id;
var numero_caja;
var caja_id_catalogo;
var estado;
var blnSeleccionarCaja=false; 

$(function(){
  
  
   crear_arbol();
   bloquearControles();
  
   //eventos de los botones de la barra de herramientas inicio
    $("[id$='Btn_Nuevo']").click(function(event) {
        nuevoElemento();
        event.preventDefault();
    });

    $("[id$='Btn_Modificar']").click(function(event) {
        editarElemento();
        event.preventDefault();
    });

   
    $("[id$='Btn_Guardar']").click(function(event) {
        guardarRegistro();
        event.preventDefault();
    });

    $("[id$='Btn_Cancelar']").click(function(event) {
        cancelarRegistro();
        event.preventDefault();
    });

    $("[id$='Btn_Salir']").click(function(event) {
        event.preventDefault();
    });


}); // fin 

//-------------------------------------------------------------------------------------------------


function crear_arbol() {

    $('#arbol_sucursales').tree({
        checkbox:false,
        url: '../../Controladores/frm_controlador_asignacion_cajas_sucursal_usuario.aspx?accion=obtener_sucursales',
        onBeforeExpand: function(node) {
            llenarArbol(node);
        },
        onClick: function(node) {
              seleccionarCaja(node);
        },
        onCheck: function(node, checked) {
      
        }
    });
    

}

//--------------------------------------------------------------------------------------------------

function llenarArbol(node) {
 
     accion = node.attributes.valor1;
     sucursal_id=node.attributes.valor2;
     
    if (accion == "cajas")
        $('#arbol_sucursales').tree('options').url = "../../Controladores/frm_controlador_asignacion_cajas_sucursal_usuario.aspx?accion=obtenerCajas&sucursal_id=" + sucursal_id;    
}


function seleccionarCaja(node){

   if (node.attributes.valor1=="fin"){
         caja_id=node.id;
         numero_caja=node.text;
         caja_id_catalogo=node.attributes.valor2;
         estado=node.attributes.valor3;  
         var nodoPapa= $('#arbol_sucursales').tree('getParent',node.target);
         sucursal_id= nodoPapa.id;
         
          $("[id$='cmb_sucursal'] option[value=" + sucursal_id+ "]").attr("selected",true);
          $("[id$='cmb_caja'] option[value=" + caja_id_catalogo + "]").attr("selected",true);
          $("[id$='cmb_estado']").findByOptionTextMG(estado);  
           
         blnSeleccionarCaja=true;
   }
   else{
       blnSeleccionarCaja=false;
       desSeleccionarCombos();    
   }

}

//------------------------------------------------------------------------------------------------


function desSeleccionarCombos(){
   $("[id$='cmb_sucursal'] option[value=-1]").attr("selected",true);
   $("[id$='cmb_caja'] option[value=-1]").attr("selected",true);
   $("[id$='cmb_estado'] option[value=-1]").attr("selected",true);
}


//-------------------------------------------------------------------------------------------------
//  metodos auxiliares
    function bloquearControles() {
        $("#datos_registro :input").attr('disabled', true);   
    }
    
   function desbloquearControles() {
        $("#datos_registro :input").attr('disabled', false);
       
    }
    
    
    function limpiarControles() {
      
    }
    
    function cerrarPanel(){
      $('#panel_explorador').panel('collapse');
    }

    function abrirPanel(){
      $('#panel_explorador').panel('expand');
    }



//funciones de los botones
//-------------------------------------------------------------------------------------------------
    function nuevoElemento() {
        ext_ocultarBotonesOperacion();
        ext_visualizarBotonesGuardarCancelar();
        desSeleccionarCombos();
        desbloquearControles();
        limpiarControles();
        cerrarPanel();
        operacion = "nuevo";
       
     
    }
    
    
  //---
      function editarElemento() {
            
        if (blnSeleccionarCaja==false){
           $.messager.alert('Japami','Tienes que seleccionar una caja','info');
           return ;
        }    
            
        ext_ocultarBotonesOperacion();
        ext_visualizarBotonesGuardarCancelar();
        desbloquearControles();
        cerrarPanel();
        operacion = "editar";
        

    }  
    
    //--
    
    function cancelarRegistro() {
        ext_ocultarBotonesGuardarCancelar();
        ext_visualizarBotonesOperacion();
        bloquearControles();
        limpiarControles();
        desSeleccionarCombos();
        operacion = "ninguno";
        blnSeleccionarCaja=false;
        abrirPanel();

    }
    
   //-----
   
 function guardarRegistro() {
    
       
    if (valoresRequeridosGeneral("datos_registro")==false ){
        $.messager.alert('Japami','Se requieren algunos valores','info');
        return;    
    }
    
    sucursal_id= $("[id$='cmb_sucursal']").val();
    caja_id_catalogo=$("[id$='cmb_caja']").val();
    numero_caja=$("[id$='cmb_caja'] option:selected").text();
    estado=$("[id$='cmb_estado'] option:selected").text();
    
    
       $('#ventana_mensaje').window('open');
             
            $.ajax({
            type: "POST",
            url: "../../Controladores/frm_controlador_asignacion_cajas_sucursal_usuario.aspx/guardar",
            data: "{'id_sucursal':'" + sucursal_id + "','id_caja_catalago':'" + caja_id_catalogo + "','numero_caja':'" + numero_caja + "','estado':'" + estado + "','caja_id': '" + caja_id + "','operacion':'" + operacion + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                var respuesta = eval("(" + response.d + ")");
                if (respuesta.mensaje == "bien") {
                    $('#ventana_mensaje').window('close'); 
                    procesoExitoso();
                      
                }
                else {
                    $('#ventana_mensaje').window('close');
                    $.messager.alert('Manazana', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                }

            },
            error: function(result) {
                $('#ventana_mensaje').window('close');
                $.messager.alert('Japami', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');

            }

        });
        
        
    
    }
    
    
    function procesoExitoso(){
      
     $.messager.alert('Japami','Proceso Terminado','info',function(){
         cancelarRegistro();
        $('#arbol_sucursales').tree({url:'../../Controladores/frm_controlador_asignacion_cajas_sucursal_usuario.aspx?accion=obtener_sucursales'}); 
     }); 
      
    
    }
    
    
 //---------------------------------------------------------------------------------   
 //usage $("[id$='Cmb_Consultorio']").findByOptionTextMG(renglon_datos.nombre_consultorio);
 
jQuery.fn.findByOptionTextMG = function (data) {
     $(this).find("option").each(function(){
            if ($(this).text() == data)
            $(this).attr("selected","selected");
      });
};
 
jQuery.fn.findByOptionValueMG = function (data) {
     $(this).find("option").each(function(){
            if ($(this).val() == data)
            $(this).attr("selected","selected");
      });
};
 
 
 