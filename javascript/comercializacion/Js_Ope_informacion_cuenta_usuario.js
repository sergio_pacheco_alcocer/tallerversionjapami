﻿
var no_cuenta='';
var usuario;
var str_facturado;
var obj_facturado;
var bandera=0;
var dtmFechaActual;
var strFechaActual;



$(function(){
   
   //obtenerFecha Actual
   
   dtmFechaActual=new Date();
   var mes;
   
   mes= String(dtmFechaActual.getMonth() +1);
  
   if(mes.length==1){
      mes= "0" + mes
   }
   
   strFechaActual=dtmFechaActual.getDate() + "/" + mes + "/" + dtmFechaActual.getFullYear();
   
   

   //cierra las ventans del popup
   $('#ventana_imprimir_pagos').window('close');
   $('#ventana_mensaje').window('close');
   
   // inicializamos fechas
$("[id$='f_inicio']").datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'dd/mm/yy'
});


$("[id$='f_final']").datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'dd/mm/yy'
});


$("[id$='txt_fecha_evento']").datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'dd/mm/yy'
});
   

   crearTab();
   no_cuenta=$("[id$='txt_no_cuenta']").val();
   usuario=$("[id$='txt_usuario']").val();
   str_facturado=$("[id$='txt_facturado']").val();
   obj_facturado=json_parse(str_facturado);
   
   crearTablaFacturado();
   crearTablaPagos();
   crearTablaLecturas();
   
   // obtenemos lo facturado
   $('#grid_faturado').datagrid('loadData',obj_facturado);
   
   // obtenemos las lecturas
   $('#grid_lecturas').datagrid({ url: '../../Controladores/Frm_Controlador_Consulta_Usuario.aspx', queryParams: { accion: 'obtener_lecturas', nocuenta:no_cuenta}, pageNumber: 1 });
   
   // obtenemos los pagos
   $('#grid_pagos').datagrid({ url: '../../Controladores/Frm_Controlador_Consulta_Usuario.aspx', queryParams: { accion: 'obtener_pagos', nocuenta:no_cuenta}, pageNumber: 1 });
   
 
  


 
   
});

//-------------------------------------------------------------------------------------------------------

function obtenerLecturasyConsumos(){
 var renglon;
 var renglones;
 
 var dato;
 
 dato=$("[id$='txt_lectura_actual']").val();
 
 if (dato.length>0){return;}
 
 
 renglones=$('#grid_lecturas').datagrid('getRows');
    
 for(var i=0;i<renglones.length;i++){
     renglon=renglones[i];    
     if(i<4){      
         switch(i){
             case 0:
                    $("[id$='txt_lectura_actual']").val(renglon.lectura);
                    $("[id$='txt_consumo_actual']").val(renglon.consumo);
                break;
             case 1:
                   $("[id$='txt_lectura_anterior1']").val(renglon.lectura);
                   $("[id$='txt_consumo_anterior1']").val(renglon.consumo);
               break;
             case 2:
                  $("[id$='txt_lectura_anterior2']").val(renglon.lectura);
                  $("[id$='txt_consumo_anterior2']").val(renglon.consumo);
               break;
             case 3:
                 $("[id$='txt_lectura_anterio3']").val(renglon.lectura);
                 $("[id$='txt_consumo_anterior3']").val(renglon.consumo);
              break;       
         }
         
     } 
     else{return;}
     
 }   


}

//-------------------------------------------------------------------------------------------------------

function crearTab() {
        $('#tab').tabs({
            onSelect: function(title) {
               
//               if(title=="Info. General"){                  
//                   obtenerLecturasyConsumos();
//               }
               
     
            }
     });
     
 } 
 
//--------------------------------------------------------------------------------------------------------------

function crearTablaFacturado(){

 $('#grid_faturado').datagrid({  // tabla inicio
            title: 'Facturado',
            width: 790,
            height: 400,
            columns: [[
        { field: 'concepto', title: 'Concepto', width: 100, sortable: true },
        { field: 'actual', title: 'Actual', width: 200, sortable: true },
        { field: 'anterior1', title: 'Anterior 1', width: 200, sortable: true },
        { field: 'anterior2', title: 'Anterior 2', width: 150, sortable: true }

    ]],     
             onClickRow: function(rowIndex, rowData) {
            
            }, 
            pageSize: 50,
            pagination: false,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            showFooter: true,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap: false

        });  // tabla final

}    

//------------------------------------------------------------------------------

function crearTablaPagos(){

      $('#grid_pagos').datagrid({  // tabla inicio
            title: 'Hist&oacute;rico de Pagos',
            width: 790,
            height: 250,
            columns: [[
        { field: 'fecha_pago', title: 'Fecha pago', width: 100, sortable: true },
        { field: 'lugar_pago', title: 'Lugar pago', width: 200, sortable: true },
        { field: 'concepto', title: 'Concepto', width: 200, sortable: true },
        { field: 'importe', title: 'Importe.', width: 150, sortable: true },
        { field: 'p_facturacion', title: 'Fecha Fac.', width: 100, sortable: true },
        { field: 'no_recibo', title: 'No. recibo', width: 200, sortable: true }
    

    ]],     
             onClickRow: function(rowIndex, rowData) {
            
            }, 
            pageSize: 50,
            pagination: true,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            showFooter: false,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap: false

        });  // tabla final


}

//--------------------------------------------------------------------------------

function crearTablaLecturas(){

       $('#grid_lecturas').datagrid({  // tabla inicio
            title: 'Hist&oacute;rico de Lecturas',
            width: 790,
            height: 250,
            columns: [[
        { field: 'fecha_lectura', title: 'Fecha', width: 100, sortable: true },
        { field: 'lectura',   title: 'Lectura', width: 120, sortable: true },
        { field: 'consumo', title: 'Consumo', width: 120, sortable: true },
        { field: 'anomalia', title: 'Impedimiento', width: 200, sortable: true },
        { field: 'clave_lec',    title: 'Lecturista', width: 100, sortable: true }
    

    ]],     
             onClickRow: function(rowIndex, rowData) {
            
            },
            onLoadSuccess:function(data){
             if (bandera==0){
               bandera++;
               obtenerLecturasyConsumos();
             
             }
            
            }, 
            pageSize: 50,
            pagination: true,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            showFooter: false,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap: false

        });  // tabla final


}
//-----------------------------------------------------------------------------------


function consultarEventosUsuarios(){

  //window.location = "Frm_Ope_Cor_Eventos.aspx?PAGINA=417";
  //window.open("Frm_Ope_Cor_Eventos.aspx?PAGINA=417", "Eventos", "width=1200px,height=700px,scrollbars=YES");
  
   $('#ventana_eventos_usuario').window({
                      closed: false,
                      onOpen: function() {
                      
                        $("#txt_nombre_eventos").val(usuario);
                        $("[id$='txt_no_cuenta_evento']").val(no_cuenta);
                        $("[id$='txt_fecha_evento']").val(strFechaActual);
                        
                         // obtenemos los eventos
                        $('#tabla_eventos').datagrid({total:0 , rows:[]}); 
                        $('#tabla_eventos').datagrid({ url: '../../Controladores/Frm_Controlador_Consulta_Usuario.aspx', queryParams: { accion: 'obtenerEventosUsuarios', nocuenta:no_cuenta}, pageNumber: 1 });
                        
                      }                     
         });

}


//----------------------------------------------------------------------------------


function consultarInspecciones(){
  //window.location = "Frm_Ope_Cor_Inspecciones.aspx?PAGINA=355";
 // window.open("Frm_Ope_Cor_Inspecciones.aspx?PAGINA=355", "Inspecciones", "width=1200px,height=700px,scrollbars=YES");
 
  $('#ventana_inspecciones').window({
                      closed: false,
                      onOpen: function() {
                        $("[id$='txt_numero_cuenta_i']").val(no_cuenta);
                        $("#txt_nombre_i").val(usuario);
                        $("[id$='txt_solicitado_por_i']").val(usuario);
                        
                           $('#tabla_inspecciones').datagrid({total:0 , rows:[]});
                        
                         // obtenemos los pagos
                         $('#tabla_inspecciones').datagrid({ url: '../../Controladores/Frm_Controlador_Consulta_Usuario.aspx', queryParams: { accion: 'obtenerInspeccionesUsuario', nocuenta:no_cuenta}, pageNumber: 1 });
                        
                      }                     
                      });

 
 
}

//----------------------------------------------------------------------------------

function consultarQuejas(){
   //window.location = "Frm_Ope_Cor_Ordenes_Trabajo.aspx?PAGINA=401";
   //window.open("Frm_Ope_Cor_Ordenes_Trabajo.aspx?PAGINA=401", "OrdenesTrabajo", "width=1200px,height=700px,,scrollbars=YES");
   
     $('#ventana_quejas').window({
                      closed: false,
                      onOpen: function() {
                      
                        $("#txt_nombre_quejas").val(usuario);
                         // obtenemos las quejas
                        $('#tablas_quejas').datagrid({ url: '../../Controladores/Frm_Controlador_Consulta_Usuario.aspx', queryParams: { accion: 'obtenerQuejasUsuario', nocuenta:no_cuenta}, pageNumber: 1 });
                        
                      }                     
         });
   
    
}


//----------------------------------------------------------------------------------

function consultarCortes(){
   //window.location = "Frm_Ope_Cor_Cortes.aspx?PAGINA=409";
   //window.open("Frm_Ope_Cor_Cortes.aspx?PAGINA=409", "Cortes", "width=1200px,height=700px,scrollbars=YES");
   
   
   $('#ventana_corte').window({
                      closed: false,
                      onOpen: function() {
                        $("#txt_nombre_cortes").val(usuario);
                         // obtenemos los pagos
                         $('#tablas_cortes').datagrid({ url: '../../Controladores/Frm_Controlador_Consulta_Usuario.aspx', queryParams: { accion: 'obtenerCortesUsuario', nocuenta:no_cuenta}, pageNumber: 1 });
                        
                      }                     
                      });
   
   
   
}

//----------------------------------------------------------------------------------

function consultarConvenios(){
  //window.location = "Frm_Ope_Cor_Convenios.aspx?PAGINA=412";
  // window.open("Frm_Ope_Cor_Convenios.aspx?PAGINA=412", "Convenios", "width=1200px,height=700px,scrollbars=YES");
  
  var objfooter;
  var saldo;
  var saldo_en_convenio;
  
  saldo_en_convenio= parseFloat( $("[id$='txt_saldo_convenio']").val());
  
  
  if(saldo_en_convenio!=0){
    $.messager.alert('Japami','El usuario ya tiene un convenio','error');
    return ;
  }
  
 
  objfooter=$('#grid_faturado').datagrid('getFooterRows');
  
  if(objfooter.length==0){
    $.messager.alert('Japami','Se requiere tener un saldo deudor','error');
    return ;
  }
  
  if(objfooter.length<6){
     $.messager.alert('Japami','Se requiere tener un saldo deudor','error');
    return;
  }
  
  
  if(objfooter[5].actual !='PENDIENTE' && objfooter[5].actual!='PAGO PARCIAL' ){
    $.messager.alert('Japami','Se requiere tener un saldo deudor','error');
    return ;
  }
  
  saldo= parseFloat( objfooter[4].actual);
  if (isNaN(saldo)){
    $.messagera.alert('Japami','Se requiere tener un saldo deudor','error');
    return ;
  }
  
  if (saldo=="undefined"){
     $.messager.alert('Japami','Se requiere tener un saldo deudor','error');
     return ;
  }
  
  if(saldo<=0 ){
     $.messager.alert('Japami','Se requiere tener un saldo deudor','error');
     return ;
  }
  
  
  
  
  $('#ventana_convenios').window({
                      closed: false,
                      onOpen: function() {
                            cargarValores();
                        
                        $("#txt_nombre_convenio").val(usuario);
                         // obtenemos las quejas
                        //$('#tablas_quejas').datagrid({ url: '../../Controladores/Frm_Controlador_Consulta_Usuario.aspx', queryParams: { accion: 'obtenerQuejasUsuario', nocuenta:no_cuenta}, pageNumber: 1 });
                        
                      }                     
         });
  
}

//----------------------------------------------------------------------------------

function consultaEstudioSociecomico(){
 
 
 $('#ventana_estudios_socioeconmicos').window({
                      closed: false,
                      onOpen: function() {
                        $("[id$='txt_numero_cuenta_s']").val(no_cuenta);
                        $("#txt_nombre_s").val(usuario);
                        $("[id$='txt_solicitado_por_s']").val(usuario);
                        
                        $('#tabla_solicitudes').datagrid({ url: '../../Controladores/Frm_Controlador_Consulta_Usuario.aspx', queryParams: { accion: 'obtenerSolicitudesSocioeconomicas', nocuenta:no_cuenta}, pageNumber: 1 });
                        
                      }                     
                      });

   //window.location = "Frm_Ope_Cor_Solicitudes_Estudio_Socioeconomico.aspx?PAGINA=332";
   //window.open("Frm_Ope_Cor_Solicitudes_Estudio_Socioeconomico.aspx?PAGINA=332", "EstudiosSocieconomicos", "width=1200px,height=700px,scrollbars=YES");
   
}

//--------------------------------------------------------------------------------------------------


function realizarPagoParcial(){
 
 
 $('#ventana_pago_parcial').window({
                      closed: false,
                      onOpen: function() {
                          
                        $('#btnImprimirgridpagoparcial').linkbutton('disable');                                     
                        $("[id$='txt_no_cuenta_parcial']").val(no_cuenta);
                        $("[id$='txt_nombre_parcial']").val(usuario);                       
                                                
                        $('#tabla_factura').datagrid('reloadFooter',{ rows: [] });                        
                        $('#tabla_factura').datagrid('loadData', { total: 0, rows: [] }); 
                        $('#tabla_factura').datagrid({ url: '../../Controladores/frm_controlador_pagos_parciales.aspx', queryParams: {
                           accion: 'obtenerFactura',
                             nocuenta:no_cuenta
                           }, pageNumber: 1
                         });
                        
                       }                     
                      });

   //window.location = "Frm_Ope_Cor_Solicitudes_Estudio_Socioeconomico.aspx?PAGINA=332";
   //window.open("Frm_Ope_Cor_Solicitudes_Estudio_Socioeconomico.aspx?PAGINA=332", "EstudiosSocieconomicos", "width=1200px,height=700px,scrollbars=YES");
   
}


//----------------------------------------------------------------------------------

function abrirPantallaModificacionConceptosFactura(){

$('#ventana_modificacion_factura').window({
                      closed: false,
                      onOpen: function() {
                        $("[id$='txt_no_cuenta_fac']").val(no_cuenta);
                        $("[id$='txt_usuario_fac']").val(usuario); 
                        
                        
                         $('#tabla_conceptos_modificados').datagrid('loadData',{total:0 , rows:[]});   
                         $('#tabla_conceptos_actual').datagrid('loadData',{total:0 , rows:[]});
                        
                        $('#tabla_principal').datagrid('loadData',{total:0 , rows:[]})                                                                
                        $('#tabla_principal').datagrid({ url: '../../Controladores/Frm_Controlador_Consulta_Usuario.aspx', queryParams: { accion: 'obtenerConceptosModificadosFactura', nocuenta:no_cuenta}, pageNumber: 1 });
                        
                      }                     
              });


}

//-----------------------------------------------------------------------------------

function consultaReimprimirRecibo(){
   //window.location = "Frm_Ope_Cor_Recibos_Parciales.aspx?PAGINA=420";
   //window.open("Frm_Ope_Cor_Recibos_Parciales.aspx?PAGINA=420", "ReimprimirRecibo", "width=1200px,height=700px,scrollbars=YES");
   
   var no_factura_recibo;
   
   no_factura_recibo= $.trim($("[id$='txt_no_factura_convenio']").val());
   
   if (no_factura_recibo.length==0){
      $.messager.alert('Japami','No hay Recibos que imprimir');
      return
   }
   
   window.open("../../Reporte/frm_controlador_reporte_pdf.aspx?nombre_reporte=formato_recibo_normal&no_factura_recibo=" + no_factura_recibo,"Recibo","width=1000px,height=1000px,scrollbars=YES,resizable=YES");
   //window.open("Frm_Ope_Cor_Facturas_Recibos_Impresion.aspx?&Region_ID=&No_Factura_Recibo=" + no_factura_recibo + "&No_Cuenta=&Folio_Inicial=&Folio_Final=" ,"Impesion","width=100,height=100,left=0,top=0");

}


//----------------------------------------------------------------------------------

function abrirPopup(){
 $('#ventana_imprimir_pagos').window('open');
}

//----------------------------------------------------------------------------------

function imprimirReportePagosExecel(){
 
 if (validarFechas($("#f_inicio"), $("#f_final"))) {
      return;
    }
    
    cerrarVentana();
    window.location = "../../Reporte/Frm_controlador_excel.aspx?accion=reporte_pagos&fecha_inicio=" +$("#f_inicio").val() + "&fecha_final="+$("#f_final").val() +"&nocuenta="+no_cuenta + "&nombre="+ usuario ;
 
}

//----------------------------------------------------------------------------------

function imprimirReporteEdicionesCuenta(){

  window.location = "../../Reporte/Frm_controlador_excel.aspx?nocuenta="+no_cuenta + "&descripcion_edicion=Editar Cuenta Usuario&accion=reporte_edicion_cuenta&nombre=" + $("[id$='txt_usuario']").val();

}



//----------------------------------------------------------------------------------


function cerrarVentana(){
  $('#ventana_imprimir_pagos').window('close');
}


//----------------------------------------------------------------------------------


function consultaHistoricoFacturas(){
 window.open("frm_ope_cor_encabezado_factura.aspx?nocuenta=" + no_cuenta + "&nombre=" + $("[id$='txt_usuario']").val(), "Facturas", "width=1000px,height=610px,scrollbars=NO");

}

//-----------------------------------------------------------------------------------


function cerrarVentanas(){
    $("#ventana_estudios_socioeconmicos").window('close');
    $('#ventana_eventos_usuario').window('close');
    $('#ventana_quejas').window('close');
    $('#ventana_corte').window('close');
    $('#ventana_inspecciones').window('close');
    $('#ventana_convenios').window('close');
    $('#ventana_modificacion_factura').window('close');
}