﻿///*******************************************************************************
///NOMBRE DE LA FUNCIÓN : $(document).ready()
///DESCRIPCIÓN          : Funcion que indica cuando el documento ha sido cargado, para poder modificar los elementod DOM
///PARAMETROS           :   
///CREO                 : Yañez Rodriguez Diego
///FECHA_CREO           : 00/Marzo/2011 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///*******************************************************************************
$(document).ready(function () {
    Configuracion_Inicial();
});
//COnfiguración Inicial de los controles --------------------------------------------------------------------------------
function Configuracion_Inicial() {
    jQuery.ajaxSetup({
        async: false,
        cache: false,
        timeout: (2 * 1000),
        beforeSend: function () {
            MostrarProgress();
        },
        complete: function () {
            OcultarProgress();
        },
        error: function () {
            OcultarProgress();
        }
    });
    //Limpiar campos de filtros
    $('#Txt_Busqueda_Zona').keyup(function () {
        var longitud = $('#Txt_Busqueda_Zona').val().length;
        if (longitud == 0) {
            $('#Txt_Busqueda_Zona_Hidden').val('');
        }
    });
    $('#Txt_Busqueda_Giro').keyup(function () {
        var longitud = $('#Txt_Busqueda_Giro').val().length;
        if (longitud == 0) {
            $('#Txt_Busqueda_Giro_Hidden').val('');
        }
    });
    $('#Txt_Busqueda_Colonia').keyup(function () {
        var longitud = $('#Txt_Busqueda_Colonia').val().length;
        if (longitud == 0) {
            $('#Txt_Busqueda_Colonia_Hidden').val('');
        }
    });
    $('#Txt_Busqueda_Fraccionador').keyup(function () {
        var longitud = $('#Txt_Busqueda_Fraccionador').val().length;
        if (longitud == 0) {
            $('#Txt_Busqueda_Fraccionador_Hidden').val('');
        }
    });
    $('#Txt_Busqueda_Calle').keyup(function () {
        var longitud = $('#Txt_Busqueda_Calle').val().length;
        if (longitud == 0) {
            $('#Txt_Busqueda_Calle_Hidden').val('');
        }
    });
    //Campos Numericos
    $('#Txt_Superficie_M2').numberbox({
        min: 1,
        max: 999999999,
        precision: 2
    });
    $('#Txt_Superficie_Construida').numberbox({
        min: 1,
        max: 999999999,
        precision: 2
    });
    $('#Txt_Superficie_Jardin').numberbox({
        min: 1,
        max: 999999999,
        precision: 2
    });
    $('#Txt_Niveles').numberbox({
        min: 1,
        max: 99,
        precision: 2
    });
    $('#Txt_Muebles_Sanitarios').numberbox({
        min: 1,
        max: 99,
        precision: 2
    });
    $('#Txt_Subpredios').numberbox({
        min: 1,
        max: 99,
        precision: 2
    });
    $('#Txt_Latitud_GMS_Grados').numberbox({
        min: 1,
        max: 90,
        precision: 2
    });
    $('#Txt_Latitud_GMS_Minutos').numberbox({
        min: 1,
        max: 59.9999,
        precision: 2
    });
    $('#Txt_Latitud_GMS_Segundos').numberbox({
        min: 1,
        max: 9999999,
        precision: 2
    });
    $('#Txt_Latitud_Decimal').numberbox({
        min: 1,
        max: 90,
        precision: 6
    });
    $('#Txt_Longitud_GMS_Grados').numberbox({
        min: 1,
        max: 90,
        precision: 2
    });
    $('#Txt_Longitud_GMS_Minutos').numberbox({
        min: 1,
        max: 59.9999,
        precision: 2
    });
    $('#Txt_Longitud_GMS_Segundos').numberbox({
        min: 1,
        max: 9999999,
        precision: 2
    });
    $('#Txt_Longitud_Decimal').numberbox({
        min: 1,
        max: 90,
        precision: 6
    });
    //grid de resultados de predios
    $('#Grid_Predios').datagrid({
        title: 'Catalogo Predios',
        width: 850,
        height: 250,
        columns: [[
                    { field: 'boton', title: 'Sel', width: 20, align: 'center',
                        formatter: function (value, rec) {
                            return '<input type="button" id="' + rec.Predio_ID + '" style="width:10px" />';
                        }
                    },
                    { field: 'Predio_ID', title: 'Predio ID', width: 1, hidden: true },
                    { field: 'Domicilio', title: 'Domicilio', width: 300, sortable: true },
                    { field: 'No_Cuenta', title: 'No Cuenta', width: 80, sortable: true },
                    { field: 'Estatus', title: 'Estatus', width: 80, sortable: true },
                    { field: 'Colonia', title: 'Colonia', width: 0, sortable: true, hidden: true },
                    { field: 'Calle', title: 'Calle', width: 0, sortable: true, hidden: true },
                    { field: 'No_Exterior', title: 'No Exterior', width: 0, sortable: true, hidden: true },
                    { field: 'No_Interior', title: 'No Interior', width: 0, sortable: true, hidden: true },
                    { field: 'Giro_ID', title: 'Giro_ID', width: 0, sortable: true, hidden: true }

            ]],
        pagination: true,
        rownumbers: true,
        idField: 'Predio_ID',
        sortOrder: 'asc',
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        loadMsg: "Cargando, espere ...",
        pageNumber: 1,
        pageSize: 10,
        fitColumns: true,
        striped: true,
        nowrap: false,
        pageList: [10, 20],
        remoteSort: false,
        showFooter: true,
        onSelect: function (rowIndex, rowData) {
            Seleccionar_Predio(rowData.Predio_ID);
        }
    });
    //Se activa la función del boton buscar
    $('#Btn_Filtrar_Predios').click(function (event) {
        event.preventDefault();
        Filtrar_Predios();
    });
    //Para filtros
    AutocompletadoColoniasFiltro();
    var colonia_id = $('#Txt_Busqueda_Colonia_Hidden').val();
    AutocompletadoCallesFiltro(colonia_id);
    AutocompletadoZonasFiltro();
    AutocompletadoGirosFiltro();
    AutocompletadoFraccionadorFiltro();
    //activa la busqueda modal de predios
    $('#Modal_Predios').jqm();
    $('#Btn_Buscar_Predio').click(function (event) {
        event.preventDefault();
        Limpia_Campos_Predios();
        Configurar_Botones_Predio("Inicial");
        $('#Modal_Predios').jqmShow();
    });

    //LLenado de información para el alta
    AutocompletadoColonias();
    //    AutocompletadoZonas();
    AutocompletadoGiros();
    AutocompletadoTarifas();
    AutocompletadoFraccionador();
    //    AutocompletadoViviendas();
    Cargar_Zonas();
    Cargar_Giros();
    Cargar_Tipos_Vivienda();
    Cargar_Tarifas();
    Cargar_Condicion();
    Cargar_Materiales();
    Cargar_Region();
    //Configuración de enventos de controles
    $('#Cmb_Calle').change(function (event) {
        var calle_id = $("#Cmb_Calle option:selected").val();
        if (calle_id != 'Seleccione') {
            $('#Cmb_Calle').removeAttr('style');
            $('#Cmb_Calle').attr('style', 'width:98%;');
            Cargar_Calles_Referencia();
            $('#Cmb_Calle_Referencia1').removeAttr('disabled');
            $('#Cmb_Calle_Referencia1').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
            $('#Cmb_Calle_Referencia2').removeAttr('disabled');
            $('#Cmb_Calle_Referencia2').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
            $('#Cmb_Manzana').removeAttr('disabled');
            $('#Cmb_Manzana').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
            //Asigna la misma calle a la toma
            $('#Cmb_Calle_Toma').val(calle_id);
            Cargar_Calles_Referencia_Toma();

        }
        else {
            $('#Cmb_Calle').removeAttr('style');
            $('#Cmb_Calle').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
            $('#Cmb_Calle_Referencia1').removeAttr('style');
            $('#Cmb_Calle_Referencia1').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
            $('#Cmb_Calle_Referencia1').attr('disabled', 'disabled');
            $('#Cmb_Calle_Referencia1').val('Seleccione');
            $('#Cmb_Calle_Referencia2').removeAttr('style');
            $('#Cmb_Calle_Referencia2').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
            $('#Cmb_Calle_Referencia2').attr('disabled', 'disabled');
            $('#Cmb_Calle_Referencia2').val('Seleccione');
            $('#Cmb_Manzana').removeAttr('style');
            $('#Cmb_Manzana').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
            $('#Cmb_Manzana').attr('disabled', 'disabled');
            $('#Cmb_Manzana').val('Seleccione');
        }
    });
    $('#Cmb_Calle_Referencia1').change(function (event) {
        var calle_id = $("#Cmb_Calle_Referencia1 option:selected").val();
        if (calle_id != 'Seleccione') {
            $('#Cmb_Calle_Referencia1').removeAttr('style');
            $('#Cmb_Calle_Referencia1').attr('style', 'width:98%;');
            //Carga las manzanas que coinciden con la busqueda
            Cargar_Manzanas();
            //Asigna la misma calle a la ubicacion de la toma
            $('#Cmb_Calle_Referencia_1_Toma').val(calle_id);
        }
        else {
            $('#Cmb_Calle_Referencia1').removeAttr('style');
            $('#Cmb_Calle_Referencia1').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
            $('#Cmb_Calle_Referencia1').val('Seleccione');
        }
    });
    $('#Cmb_Calle_Referencia2').change(function (event) {
        var calle_id = $("#Cmb_Calle_Referencia2 option:selected").val();
        if (calle_id != 'Seleccione') {
            $('#Cmb_Calle_Referencia2').removeAttr('style');
            $('#Cmb_Calle_Referencia2').attr('style', 'width:98%;');
            //Carga las manzanas que coinciden con la busqueda
            Cargar_Manzanas();
            $('#Cmb_Calle_Referencia_2_Toma').val(calle_id);
        }
        else {
            $('#Cmb_Calle_Referencia2').removeAttr('style');
            $('#Cmb_Calle_Referencia2').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
            $('#Cmb_Calle_Referencia2').val('Seleccione');
        }
    });
    //Para la toma
    $('#Cmb_Calle_Toma').change(function (event) {
        var calle_id = $("#Cmb_Calle_Toma option:selected").val();
        if (calle_id != 'Seleccione') {
            $('#Cmb_Calle_Toma').removeAttr('style');
            $('#Cmb_Calle_Toma').attr('style', 'width:98%;');
            Cargar_Calles_Referencia_Toma();
            $('#Cmb_Calle_Referencia_1_Toma').removeAttr('disabled');
            $('#Cmb_Calle_Referencia_2_Toma').removeAttr('disabled');
        }
        else {
            $('#Cmb_Calle_Toma').removeAttr('style');
            $('#Cmb_Calle_Toma').attr('style', 'width:98%;');
            $('#Cmb_Calle_Referencia_1_Toma').removeAttr('style');
            $('#Cmb_Calle_Referencia_1_Toma').attr('style', 'width:98%;');
            $('#Cmb_Calle_Referencia_1_Toma').attr('disabled', 'disabled');
            $('#Cmb_Calle_Referencia_1_Toma').val('Seleccione');
            $('#Cmb_Calle_Referencia_2_Toma').removeAttr('style');
            $('#Cmb_Calle_Referencia_2_Toma').attr('style', 'width:98%;');
            $('#Cmb_Calle_Referencia_2_Toma').attr('disabled', 'disabled');
            $('#Cmb_Calle_Referencia_2_Toma').val('Seleccione');
        }
    });
    $('#Cmb_Calle_Referencia_1_Toma').change(function (event) {
        var calle_id = $("#Cmb_Calle_Referencia_1_Toma option:selected").val();
        if (calle_id != 'Seleccione') {
            $('#Cmb_Calle_Referencia_1_Toma').removeAttr('style');
            $('#Cmb_Calle_Referencia_1_Toma').attr('style', 'width:98%;');
        }
        else {
            $('#Cmb_Calle_Referencia_1_Toma').removeAttr('style');
            $('#Cmb_Calle_Referencia_1_Toma').attr('style', 'width:98%;');
            $('#Cmb_Calle_Referencia_1_Toma').val('Seleccione');
        }
    });
    $('#Cmb_Calle_Referencia_2_Toma').change(function (event) {
        var calle_id = $("#Cmb_Calle_Referencia_2_Toma option:selected").val();
        if (calle_id != 'Seleccione') {
            $('#Cmb_Calle_Referencia_2_Toma').removeAttr('style');
            $('#Cmb_Calle_Referencia_2_Toma').attr('style', 'width:98%;');
        }
        else {
            $('#Cmb_Calle_Referencia_2_Toma').removeAttr('style');
            $('#Cmb_Calle_Referencia_2_Toma').attr('style', 'width:98%;');
            $('#Cmb_Calle_Referencia_2_Toma').val('Seleccione');
        }
    });
    $('#Cmb_Manzana').change(function (event) {
        var calle_id = $("#Cmb_Manzana option:selected").val();
        if (calle_id != 'Seleccione') {
            $('#Cmb_Manzana').removeAttr('style');
            $('#Cmb_Manzana').attr('style', 'width:98%;');
        }
        else {
            $('#Cmb_Manzana').removeAttr('style');
            $('#Cmb_Manzana').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
            $('#Cmb_Manzana').val('Seleccione');
        }
    });
    $('#Cmb_Region').change(function (event) {
        var region_id = $("#Cmb_Region option:selected").val();
        if (region_id != 'Seleccione') {
            Cargar_Sector();
            $('#Cmb_Sector').removeAttr('disabled');
        }
        else {
            $('#Cmb_Sector').attr('disabled', 'disabled');
            $('#Cmb_Sector').val('Seleccione');
        }
    });
    //Predios Generales
    $('#Cmb_Giros').change(function (event) {
        var giro_id = $("#Cmb_Giros option:selected").val();
        if (giro_id != 'Seleccione') {
            Cargar_Giros_Actividad(giro_id);
            Asigna_Tarifa();
            $('#Cmb_Giros').removeAttr('style');
            $('#Cmb_Giros').attr('style', 'width:98%;');
        }
        else {
            $('#Cmb_Giros').removeAttr('style');
            $('#Cmb_Giros').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
        }
    });
    
    
    $('#Cmb_Actividad').change(function (event) {
        var giro_actividad_id = $("#Cmb_Actividad option:selected").val();
        if (giro_actividad_id != 'Seleccione') {
            $('#Cmb_Actividad').removeAttr('style');
            $('#Cmb_Actividad').attr('style', 'width:98%;');
        }
        else {
            $('#Cmb_Actividad').removeAttr('style');
            $('#Cmb_Actividad').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
        }
    });
    $('#Cmb_Tarifa').change(function (event) {
        var tarifa_id = $("#Cmb_Tarifa option:selected").val();
        if (tarifa_id != 'Seleccione') {
            $('#Cmb_Tarifa').removeAttr('style');
            $('#Cmb_Tarifa').attr('style', 'width:98%;');
        }
        else {
            $('#Cmb_Tarifa').removeAttr('style');
            $('#Cmb_Tarifa').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
        }
    });
    $('#Cmb_Aplica_Notificacion').change(function(event) {
        var Aplica = $("#Cmb_Aplica_Notificacion option:selected").val();
        if (Aplica != 'SI') {
            $('#Txt_Motivo_No_Notificacion').removeClass('disabled');
            $('#Txt_Motivo_No_Notificacion').addClass('requerido');
            $('#Txt_Motivo_No_Notificacion').removeAttr('disabled');
            $('#Txt_Motivo_No_Notificacion').attr('style', 'width:98%;');
        }
        else {
            $('#Txt_Motivo_No_Notificacion').addClass('disabled');
            $('#Txt_Motivo_No_Notificacion').removeClass('requerido');
            $('#Txt_Motivo_No_Notificacion').attr('disabled', 'disabled');
        }
    });
    $('#Cmb_Tipo_Vivienda').change(function (event) {
        var tarifa_id = $("#Cmb_Tipo_Vivienda option:selected").val();
        if (tarifa_id != 'Seleccione') {
            $('#Cmb_Tipo_Vivienda').removeAttr('style');
            $('#Cmb_Tipo_Vivienda').attr('style', 'width:98%;');
        }
        else {
            $('#Cmb_Tipo_Vivienda').removeAttr('style');
            $('#Cmb_Tipo_Vivienda').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
        }
    });
    $('#Txt_Numero_Exterior').keyup(function () {
        var longitud = $('#Txt_Numero_Exterior').val().length;
        if (longitud == 0) {
            $('#Txt_Numero_Exterior').removeAttr('style');
            $('#Txt_Numero_Exterior').attr('style', 'width:30%; border-style:dotted; border-color:Red;');
        }
        else {
            $('#Txt_Numero_Exterior').removeAttr('style');
            $('#Txt_Numero_Exterior').attr('style', 'width:30%;');
        }
    });
    //Conversión de ubicaciones de los predios DMS a Decimal y viceversa
    $('#Txt_Latitud_Decimal').blur(function () {
        grados_a_dms_latidud();
    });
    $('#Txt_Longitud_Decimal').blur(function () {
        grados_a_dms_longitud();
    });
    $('#Txt_Latitud_GMS_Grados').blur(function () {
        dms_a_grados_latidud();
    });
    $('#Txt_Latitud_GMS_Minutos').blur(function () {
        dms_a_grados_latidud();
    });
    $('#Txt_Latitud_GMS_Segundos').blur(function () {
        dms_a_grados_latidud();
    });
    $('#Txt_Longitud_GMS_Grados').blur(function () {
        dms_a_grados_longitud();
    });
    $('#Txt_Longitud_GMS_Minutos').blur(function () {
        dms_a_grados_longitud();
    });
    $('#Txt_Longitud_GMS_Segundos').blur(function () {
        dms_a_grados_longitud();
    });
    //Para el predio
    //Tabs de Alta de Predios
    $('#Tab_Predios').tabs();
    // Alta de predio
    Configurar_Botones_Predio("Inicial");
    $('#Btn_Nuevo').click(function (event) {
        event.preventDefault();
        Btn_Nuevo_Click();
    });
    $('#Btn_Modificar').click(function (event) {
        event.preventDefault();
        Btn_Modificar_Click();
    });
    $('#Btn_Salir').click(function (event) {
        event.preventDefault();
        Btn_Salir_Click();
    });

}
//Consulta de Información-------------------------------------------------------------------------------
function Obtnener_Parametros_Filtro() {
    var parametros = "";
    //Obtiene los filtros para mandarlos a consulta
    //Predio_ID
    parametros += "&Predio_ID=" + $('#Txt_Busqueda_Predio_ID').val();
    //Estatus
    parametros += "&Estatus=" + $('#Cmb_Busqueda_Estatus').val();
    //Zona
    parametros += "&Zona_ID=" + $('#Txt_Busqueda_Zona_Hidden').val();
    //Giro
    parametros += "&Giro_ID=" + $('#Txt_Busqueda_Giro_Hidden').val();
    //Colonia
    parametros += "&Colonia_ID=" + $('#Txt_Busqueda_Colonia_Hidden').val();
    //Calle
    parametros += "&Calle_ID=" + $('#Txt_Busqueda_Calle_Hidden').val();
    //Tarifa
    parametros += "&Tarifa_ID=" + $('#Txt_Busqueda_Tarifa_Hidden').val();
    //Fraccionador
    parametros += "&Fraccionador_ID=" + $('#Txt_Busqueda_Fraccionador_Hidden').val();
    //Tipo Vivienda
    parametros += "&Tipo_Vivienda_ID=" + $('#Txt_Busqueda_Tipo_Vivienda_Hidden').val();
    //Numero Exterior
    parametros += "&Numero_Exterior=" + $('#Txt_Busqueda_Numero_Exterior').val();
    //Numero Interior
    parametros += "&Numero_Interior=" + $('#Txt_Busqueda_Numero_Interior').val();
    return parametros;
}
function Filtrar_Predios() {
    //No registros por pagina
    $('#Grid_Predios').datagrid({
        url: '../../Controladores/Frm_Controlador_Predios.aspx?Accion=Filtrar_Predios' + Obtnener_Parametros_Filtro(),
        pageNumber: 1
    });
}

//Configuración de elementos en eventso---------------------------------------------------------------------------------
function Configurar_Botones_Predio(modo) {
    try {
        switch (modo) {
            case "Inicial":
                $('#Btn_Nuevo').removeAttr('disabled');
                $('#Btn_Nuevo').attr('src', '../imagenes/paginas/icono_nuevo.png');
                $('#Btn_Modificar').removeAttr('disabled');
                $('#Btn_Modificar').attr('src', '../imagenes/paginas/icono_modificar.png');
                $('#Btn_Salir').attr('src', '../imagenes/paginas/icono_salir.png');
                $('#Btn_Nuevo').attr('alt', 'Nuevo');
                $('#Btn_Modificar').attr('alt', 'Modificar');
                $('#Btn_Salir').attr('alt', 'Salir');
                $('#Filtros_Predios').show();
                $('#Alta_Predios').hide();
                $('#Alta_Predios').attr('disabled', 'disabled');
                //Da formato a los tabs
                $('.tabs-header').removeAttr('style');
                $('.tabs-header').attr('style', 'width:100%;');
                $('.tabs-panels').removeAttr('style');
                $('.tabs-panels').attr('style', 'width:100%; height:400px;');
                $('.tabs-container').removeAttr('style');
                $('.tabs-container').attr('style', 'width:100%; height:400px;');
                $('.tabs-wrap').removeAttr('style');
                $('.tabs-wrap').attr('style', 'margin-left:0px; left:0px; width:100%;');
                $('#Lbl_Mensaje_Error').text('');
                $('#Img_Error').hide();
                $('#Tab_Predios').tabs('select', 'Condiciones Predio');
                $('#Tab_Predios').tabs('select', 'Generales');
                //Configuracion_Acceso("Frm_Cat_Cor_Predios.aspx");
                break;

            case "Nuevo":
                $('#Btn_Nuevo').removeAttr('disabled');
                $('#Btn_Nuevo').attr('src', '../imagenes/paginas/icono_guardar.png');
                $('#Btn_Nuevo').attr('alt', 'Guardar');
                $('#Btn_Modificar').removeAttr('disabled');
                $('#Btn_Modificar').attr('disabled', 'disabled');
                $('#Btn_Modificar').attr('src', '../imagenes/paginas/icono_modificar_deshabilitado.png');
                $('#Btn_Modificar').attr('alt', 'Modificar');
                $('#Filtros_Predios').hide();
                $('#Alta_Predios').show();
                $('#Alta_Predios').removeAttr('disabled');
                $('#Btn_Salir').attr('src', '../imagenes/paginas/icono_cancelar.png');
                $('#Btn_Salir').attr('alt', 'Cancelar');
                $('#Tab_Predios').tabs('select', 'Generales');
                $('.altaPredio').removeAttr('disabled');
                $('.disabled').attr('disabled', 'disabled');
                

                break;
            case "Modificar":
                $('#Btn_Modificar').removeAttr('disabled');
                $('#Btn_Modificar').attr('src', '../imagenes/paginas/icono_guardar.png');
                $('#Btn_Modificar').attr('alt', 'Guardar');
                $('#Btn_Nuevo').attr('disabled', 'disabled');
                $('#Btn_Nuevo').attr('src', '../imagenes/paginas/icono_nuevo_deshabilitado.png');
                $('#Btn_Nuevo').attr('alt', 'Nuevo');
                //                    $('#Filtros_Predios').hide();
                //                    $('#Alta_Predios').show();
                $('#Alta_Predios').removeAttr('disabled');
                $('#Btn_Salir').attr('src', '../imagenes/paginas/icono_cancelar.png');
                $('#Btn_Salir').attr('alt', 'Cancelar');
                //$('.requerido').attr('style', 'width:98%;');
                //$('#Txt_Numero_Exterior').attr('style', 'width:30%;');
                //QUita de los combos las opciones
                $('.disabled').removeAttr('disabled');
                $('.altaPredio').removeAttr('disabled');
                $('#Txt_Colonia').attr('disabled', 'disabled');
                $('#Cmb_Calle').attr('disabled', 'disabled');
                $('#Txt_Numero_Exterior').attr('disabled', 'disabled');
                $('#Txt_Numero_Interior').attr('disabled', 'disabled');
                break;
            default: break;
        }

    }
    catch (ex) {
        //Error
        $("#Lbl_Mensaje_Error").removeAttr('style');
        $("#Lbl_Mensaje_Error").attr('style', 'color: rgb(153, 0, 0); display: inline;');
        $("#Lbl_Mensaje_Error").val(ex.Description);
        $("#Img_Error").show();
    }
    finally {
    }
}
function Limpia_Campos_Predios() {
    $('.altaPredio').val('Seleccione');
    $('.altaPredio').val('');
    $('.filtros').val('');
    $('.requerido').val('');
    $('.requerido').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
    $('#Txt_Numero_Exterior').attr('style', 'width:30%; border-style:dotted; border-color:Red;');
    //QUita de los combos las opciones
    $('.disabled').attr('disabled', 'disabled');
}
function reestablece_controles_ubicacion() {
    //Predio
    $('#Cmb_Calle_Referencia1').removeAttr('style');
    $('#Cmb_Calle_Referencia1').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
    $('#Cmb_Calle_Referencia1').attr('disabled', 'disabled');
    $('#Cmb_Calle_Referencia1').val('Seleccione');
    $('#Cmb_Calle_Referencia2').removeAttr('style');
    $('#Cmb_Calle_Referencia2').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
    $('#Cmb_Calle_Referencia2').attr('disabled', 'disabled');
    $('#Cmb_Calle_Referencia2').val('Seleccione');
    $('#Cmb_Calle').removeAttr('style');
    $('#Cmb_Calle').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
    $('#Cmb_Calle').attr('disabled', 'disabled');
    $('#Cmb_Calle').val('Seleccione');
    $('#Cmb_Manzana').removeAttr('style');
    $('#Cmb_Manzana').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
    $('#Cmb_Manzana').attr('disabled', 'disabled');
    $('#Cmb_Manzana').val('Seleccione');
//    $('#Cmb_Aplica_Saniamiento').removeAttr('style');
//    $('#Cmb_Aplica_Saniamiento').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
//    $('#Cmb_Aplica_Saniamiento').attr('disabled', 'disabled');
//    $('#Cmb_Aplica_Saniamiento').val('Seleccione');
    //Toma
    $('#Cmb_Calle_Referencia_1_Toma').removeAttr('style');
    $('#Cmb_Calle_Referencia_1_Toma').attr('style', 'width:98%;');
    $('#Cmb_Calle_Referencia_1_Toma').attr('disabled', 'disabled');
    $('#Cmb_Calle_Referencia_1_Toma').val('Seleccione');
    $('#Cmb_Calle_Referencia_2_Toma').removeAttr('style');
    $('#Cmb_Calle_Referencia_2_Toma').attr('style', 'width:98%;');
    $('#Cmb_Calle_Referencia_2_Toma').attr('disabled', 'disabled');
    $('#Cmb_Calle_Referencia_2_Toma').val('Seleccione');
    $('#Cmb_Calle_Toma').removeAttr('style');
    $('#Cmb_Calle_Toma').attr('style', 'width:98%;');
    $('#Cmb_Calle_Toma').attr('disabled', 'disabled');
    $('#Cmb_Calle_Toma').val('Seleccione');
}
//autocompletado de controles para filtro-------------------------------------------------------------------------------
function AutocompletadoColoniasFiltro() {
    $('#Txt_Busqueda_Colonia_Hidden').val('');
    //Autocompletado de las Colonias
    $('#Txt_Busqueda_Colonia').autocomplete("../../Controladores/Frm_Controlador_Predios.aspx", {
        extraParams: { Accion: 'autocompletar_colonia' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.colonia_id,
                    result: row.colonia
                }
            });
        },
        formatItem: function (item) {
            return item.colonia;
        }
    }).result(function (e, item) {
        var colonia_id = item.colonia_id;
        $('#Txt_Busqueda_Colonia_Hidden').val(colonia_id);
    });
}

function AutocompletadoCallesFiltro(colonia_id) {
    $('#Txt_Busqueda_Calle_Hidden').val('');
    //Obtiene el valor de la colonia

    //Autocompletado de las calles por colonia seleccionada
    $('#Txt_Busqueda_Calle').autocomplete(
        "../../Controladores/Frm_Controlador_Predios.aspx", {
            selectOnly: true,
            minChars: 3,
            extraParams: {
                Accion: 'autocompletar_calle', colonia_id: colonia_id
            },
            dataType: "json",
            parse: function (data) {
                return $.map(data, function (row) {
                    return {
                        data: row,
                        value: row.calle_id,
                        result: row.calle
                    }
                });
            },
            formatItem: function (item) {
                return item.calle; // format(item);
            }
        }).result(function (e, item) {
            var calle_id = item.calle_id;
            $('#Txt_Busqueda_Calle_Hidden').val(calle_id);
            //alert("Calle_ID_" + $('#Txt_Busqueda_Calle_Hidden').val());
        });
}

function AutocompletadoZonasFiltro() {
    $('#Txt_Busqueda_Zona_Hidden').val('');
    //Autocompletado de las Zonas
    $('#Txt_Busqueda_Zona').autocomplete("../../Controladores/Frm_Controlador_Predios.aspx", {
        extraParams: { Accion: 'autocompletar_zona' },
        dataType: "json",
        selectOnly: true,
        minChars: 1,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.zona_id,
                    result: row.zona
                }
            });
        },
        formatItem: function (item) {
            return item.zona;
        }
    }).result(function (e, item) {
        var zona_id = item.zona_id;
        $('#Txt_Busqueda_Zona_Hidden').val(zona_id);
    });
}

function AutocompletadoGirosFiltro() {
    $('#Txt_Busqueda_Giro_Hidden').val('');
    //Autocompletado de los giros
    $('#Txt_Busqueda_Giro').autocomplete("../../Controladores/Frm_Controlador_Predios.aspx", {
        extraParams: { Accion: 'autocompletar_giros' },
        dataType: "json",
        selectOnly: true,
        minChars: 2,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.giro_id,
                    result: row.giro
                }
            });
        },
        formatItem: function (item) {
            return item.giro;
        }
    }).result(function (e, item) {
        var giro_id = item.giro_id;
        $('#Txt_Busqueda_Giro_Hidden').val(giro_id);
    });
}

function AutocompletadoFraccionadorFiltro() {
    $('#Txt_Busqueda_Fraccionador_Hidden').val('');
    //Autocompletado de los fraccionadores
    $('#Txt_Busqueda_Fraccionador').autocomplete("../../Controladores/Frm_Controlador_Predios.aspx", {
        extraParams: { Accion: 'autocompletar_fraccionador' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.constructora_id,
                    result: row.constructora
                }
            });
        },
        formatItem: function (item) {
            return item.constructora;
        }
    }).result(function (e, item) {
        var constructora_id = item.constructora_id;
        $('#Txt_Busqueda_Fraccionador_Hidden').val(constructora_id);
    });
}
//Formulario de Predios, configuraciones de autocompletado y llenado de información -----------------------------------
function AutocompletadoColonias() {
    //Coloca los valores default a las calles  y datos de la toma
    $('#Txt_Colonia_Hidden').val('');
    $('#Txt_Colonia').attr('style', 'width:98%; border-style:dotted; border-color:Red;');

    //Autocompletado de las Colonias
    $('#Txt_Colonia').autocomplete("../../Controladores/Frm_Controlador_Predios.aspx", {
        extraParams: { Accion: 'autocompletar_colonia' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.colonia_id,
                    result: row.colonia
                }
            });
        },
        formatItem: function (item) {
            return item.colonia;
        }
    }).result(function (e, item) {
        reestablece_controles_ubicacion();
        var colonia_id = item.colonia_id;
        var colonia = item.colonia;
        $('#Txt_Colonia_Hidden').val(colonia_id);
        //Quita el formato de requerido
        $('#Txt_Colonia').removeAttr('style');
        $('#Txt_Colonia').attr('style', 'width:98%;');
        //Habilita las calles
        $('#Cmb_Calle').removeAttr('disabled');
        Cargar_Calles(colonia_id);
        //Habilita y coloca la misma colonia para la toma
        AutocompletadoColoniasToma();
        $('#Txt_Colonia_Toma').val(colonia);
        $('#Txt_Colonia_Toma_Hidden').val(colonia_id);
        //Habilita las calles para la toma y llena el combo
        $('#Cmb_Calle_Toma').removeAttr('disabled');
        Cargar_Calles_Toma(colonia_id);
        Obtener_Zona_Colonia(colonia_id);
    });
}

function AutocompletadoColoniasToma() {
    $('#Txt_Colonia_Toma_Hidden').val('');
    //Autocompletado de las Colonias
    $('#Txt_Colonia_Toma').autocomplete("../../Controladores/Frm_Controlador_Predios.aspx", {
        extraParams: { Accion: 'autocompletar_colonia' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.colonia_id,
                    result: row.colonia
                }
            });
        },
        formatItem: function (item) {
            return item.colonia;
        }
    }).result(function (e, item) {
        var colonia_id = item.colonia_id;
        var colonia = item.colonia;
        $('#Txt_Colonia_Toma_Hidden').val(colonia_id);
        Cargar_Calles_Toma(colonia_id);
    });
}
function Cargar_Calles(colonia_id) {
    var select = $('#Cmb_Calle');
    $('option', select).remove();

    $.ajax({
        url: "../../Controladores/Frm_Controlador_Predios.aspx?Accion=autocompletar_calle&colonia_id=" + colonia_id,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.calle_id + '">' + item.calle + '</option>';
                });
                $('#Cmb_Calle').append(options);
            }
        }
    });
}
function Cargar_Calles_Toma(colonia_id) {
    var select = $('#Cmb_Calle_Toma');
    $('option', select).remove();

    $.ajax({
        url: "../../Controladores/Frm_Controlador_Predios.aspx?Accion=autocompletar_calle&colonia_id=" + colonia_id,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.calle_id + '">' + item.calle + '</option>';
                });
                $('#Cmb_Calle_Toma').append(options);
            }
        }
    });
}

function Cargar_Calles_Referencia() {
    var select1 = $('#Cmb_Calle_Referencia1');
    var select2 = $('#Cmb_Calle_Referencia2');
    $('option', select1).remove();
    $('option', select2).remove();
    var colonia_id = $('#Txt_Colonia_Hidden').val();
    var calle_id = $("#Cmb_Calle option:selected").val();
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Predios.aspx?Accion=autocompletar_calle_referencia&calle_id=" + calle_id + "&colonia_id=" + colonia_id,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.calle_id + '">' + item.calle + '</option>';
                });
                $('#Cmb_Calle_Referencia1').append(options);
                $('#Cmb_Calle_Referencia2').append(options);
            }
        }
    });
}
function Cargar_Manzanas() {
    var colonia_id = $('#Txt_Colonia_Hidden').val();
    var calle_id = $('#Cmb_Calle option:selected').val();
    var calleref1 = $('#Cmb_Calle_Referencia1 option:selected').val();
    var calleref2 = $('#Cmb_Calle_Referencia2 option:selected').val();

    var select = $('#Cmb_Manzana');
    $('option', select).remove();


    $.ajax({
        url: "../../Controladores/Frm_Controlador_Predios.aspx?Accion=consulta_manzanas" +
            "&calle_id=" + calle_id +
            "&colonia_id=" + colonia_id +
            "&calleref_id1=" + calleref1 +
            "&calleref_id2=" + calleref2,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.manzana_id + '">' + item.manzana + '</option>';
                });
                $('#Cmb_Manzana').append(options);
            }
        }
    });
}
function Cargar_Calles_Referencia_Toma() {
    var select1 = $('#Cmb_Calle_Referencia_1_Toma');
    var select2 = $('#Cmb_Calle_Referencia_2_Toma');
    $('option', select1).remove();
    $('option', select2).remove();
    var colonia_id = $('#Txt_Colonia_Toma_Hidden').val();
    var calle_id = $("#Cmb_Calle_Toma option:selected").val();
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Predios.aspx?Accion=autocompletar_calle_referencia&calle_id=" + calle_id + "&colonia_id=" + colonia_id,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.calle_id + '">' + item.calle + '</option>';
                });
                $('#Cmb_Calle_Referencia_1_Toma').append(options);
                $('#Cmb_Calle_Referencia_2_Toma').append(options);
                $('#Cmb_Calle_Referencia_1_Toma').removeAttr('disabled');
                $('#Cmb_Calle_Referencia_2_Toma').removeAttr('disabled');
            }
        }
    });
}
function Cargar_Region() {
    var select = $('#Cmb_Region');
    $('option', select).remove();
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Predios.aspx?Accion=consulta_region",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.region_id + '">' + item.region + '</option>';
                });
                $('#Cmb_Region').append(options);
            }
        }
    });
}
function Cargar_Sector() {
    var select = $('#Cmb_Sector');
    $('option', select).remove();
    var region_id = $('#Cmb_Region option:selected').val();
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Predios.aspx?Accion=consulta_sector&region_id=" + region_id,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.sector_id + '">' + item.sector + '</option>';
                });
                $('#Cmb_Sector').append(options);
            }
        }
    });
}
//function AutocompletadoZonas() {
//    $('#Txt_Zona_Hidden').val('');
//    //Autocompletado de las Zonas
//    $('#Txt_Zona').autocomplete("../../Controladores/Frm_Controlador_Predios.aspx", {
//        extraParams: { Accion: 'autocompletar_zona' },
//        dataType: "json",
//        selectOnly: true,
//        minChars: 1,
//        parse: function (data) {
//            return $.map(data, function (row) {
//                return {
//                    data: row,
//                    value: row.zona_id,
//                    result: row.zona
//                }
//            });
//        },
//        formatItem: function (item) {
//            return item.zona;
//        }
//    }).result(function (e, item) {
//        var zona_id = item.zona_id;
//        $('#Txt_Zona').removeAttr('style');
//        $('#Txt_Zona').attr('style', 'width:98%;');
//        $('#Txt_Zona_Hidden').val(zona_id);
//    });
//}
function AutocompletadoGiros() {
    $('#Txt_Giro_Hidden').val('');
    //Autocompletado de los giros
    $('#Txt_Giro').autocomplete("../../Controladores/Frm_Controlador_Predios.aspx", {
        extraParams: { Accion: 'autocompletar_giros' },
        dataType: "json",
        selectOnly: true,
        minChars: 1,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.giro_id,
                    result: row.giro
                }
            });
        },
        formatItem: function (item) {
            return item.giro;
        }
    }).result(function (e, item) {
        var giro_id = item.giro_id;
        $('#Txt_Giro').removeAttr('style');
        $('#Txt_Giro').attr('style', 'width:98%;');
        $('#Txt_Giro_Hidden').val(giro_id);
    });
}

function AutocompletadoTarifas() {
    $('#Txt_Tarifa_Hidden').val('');
    //Autocompletado de las tarifas
    $('#Txt_Tarifa').autocomplete("../../Controladores/Frm_Controlador_Predios.aspx", {
        extraParams: { Accion: 'autocompletar_tarifa' },
        dataType: "json",
        selectOnly: true,
        minChars: 1,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.tarifa_id,
                    result: row.tarifa
                }
            });
        },
        formatItem: function (item) {
            return item.tarifa;
        }
    }).result(function (e, item) {
        var tarifa_id = item.tarifa_id;
        $('#Txt_Tarifa').removeAttr('style');
        $('#Txt_Tarifa').attr('style', 'width:98%;');
        $('#Txt_Tarifa_Hidden').val(tarifa_id);
    });
}
function AutocompletadoFraccionador() {
    $('#Txt_Fraccionador_Hidden').val('');
    //Autocompletado de los fraccionadores
    $('#Txt_Fraccionador').autocomplete("../../Controladores/Frm_Controlador_Predios.aspx", {
        extraParams: { Accion: 'autocompletar_fraccionador' },
        dataType: "json",
        selectOnly: true,
        minChars: 1,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.constructora_id,
                    result: row.constructora
                }
            });
        },
        formatItem: function (item) {
            return item.constructora;
        }
    }).result(function (e, item) {
        var constructora_id = item.constructora_id;
        $('#Txt_Fraccionador').removeAttr('style');
        $('#Txt_Fraccionador').attr('style', 'width:98%;');
        $('#Txt_Fraccionador_Hidden').val(constructora_id);
    });
}

//function AutocompletadoViviendas() {
//    $('#Txt_Tipo_Vivienda_Hidden').val('');
//    //Autocompletado de los tipos de vivienda
//    $('#Txt_Tipo_Vivienda').autocomplete("../../Controladores/Frm_Controlador_Predios.aspx", {
//        extraParams: { Accion: 'autocompletar_tipo_vivienda' },
//        dataType: "json",
//        selectOnly: true,
//        minChars: 1,
//        parse: function (data) {
//            return $.map(data, function (row) {
//                return {
//                    data: row,
//                    value: row.tipo_vivienda_id,
//                    result: row.tipo
//                }
//            });
//        },
//        formatItem: function (item) {
//            return item.tipo;
//        }
//    }).result(function (e, item) {
//        var tipo_vivienda_id = item.tipo_vivienda_id;
//        $('#Txt_Tipo_Vivienda').removeAttr('style');
//        $('#Txt_Tipo_Vivienda').attr('style', 'width:98%;');
//        $('#Txt_Tipo_Vivienda_Hidden').val(tipo_vivienda_id);
//    });
//}
function Cargar_Zonas() {
    var select = $('#Cmb_Zona');
    $('option', select).remove();

    $.ajax({
        url: "../../Controladores/Frm_Controlador_Predios.aspx?Accion=autocompletar_zona",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.zona_id + '">' + item.zona + '</option>';
                });
                $('#Cmb_Zona').append(options);
            }
        }
    });
}
function Obtener_Zona_Colonia(colonia_id) {
    //Realiza la peticion de la zona por la colinia proporcionada
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Predios.aspx?Accion=consulta_colonia_zona&Colonia_ID=" + colonia_id,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $('#Cmb_Zona').val(data[0].zona_id);
                $('#Cmb_Zona').attr('readonly', 'readonly');
                $('#Cmb_Zona').removeAttr('style');
                $('#Cmb_Zona').attr('style', 'width:98%;');
                Asigna_Tarifa();
            }
        }
    });
}

function Cargar_Giros() {
    var select = $('#Cmb_Giros');
    $('option', select).remove();

    $.ajax({
        url: "../../Controladores/Frm_Controlador_Predios.aspx?Accion=autocompletar_giros",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.giro_id + '">' + item.giro + '</option>';
                });
                $('#Cmb_Giros').append(options);
            }
        }
    });
}
function Cargar_Giros_Actividad(giro_id) {
    var select = $('#Cmb_Actividad');
    $('option', select).remove();

    $.ajax({
        url: "../../Controladores/Frm_Controlador_Predios.aspx?Accion=consulta_actividades&giro_id=" + giro_id,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.actividad_id + '">' + item.actividad + '</option>';
                });
                $('#Cmb_Actividad').append(options);
                $('#Cmb_Actividad').removeAttr('disabled');
            }
        }
    });
}
function Cargar_Tipos_Vivienda() {
    var select = $('#Cmb_Tipo_Vivienda');
    $('option', select).remove();

    $.ajax({
        url: "../../Controladores/Frm_Controlador_Predios.aspx?Accion=consulta_tipo_vivienda",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.tipo_vivienda_id + '">' + item.tipo + '</option>';
                });
                $('#Cmb_Tipo_Vivienda').append(options);
            }
        }
    });
}
function Cargar_Tarifas() {
    var select = $('#Cmb_Tarifa');
    $('option', select).remove();

    $.ajax({
        url: "../../Controladores/Frm_Controlador_Predios.aspx?Accion=consulta_tarifas",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.tarifa_id + '">' + item.tarifa + '</option>';
                });
                $('#Cmb_Tarifa').append(options);
            }
        }
    });
}
function Cargar_Condicion() {
    var select = $('#Cmb_Condicion');
    $('option', select).remove();

    $.ajax({
        url: "../../Controladores/Frm_Controlador_Predios.aspx?Accion=consulta_condiciones",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.condicion_id + '">' + item.condicion + '</option>';
                });
                $('#Cmb_Condicion').append(options);
            }
        }
    });
}
function Cargar_Materiales() {
    var select = $('#Cmb_Material_Banqueta');
    $('option', select).remove();

    $.ajax({
        url: "../../Controladores/Frm_Controlador_Predios.aspx?Accion=consulta_materiales",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.material_id + '">' + item.material + '</option>';
                });
                $('#Cmb_Material_Banqueta').append(options);
            }
        }
    });
}

//Asigna la tarifa de acuerdo a la zona y el giro
function Asigna_Tarifa() {
    //obtiene la zona
    var zona_id = $('#Txt_Zona_Hidden').val();
    var giro_id = $("#Cmb_Giros option:selected").val();
    //Consulta la tarifa que le corresponderia
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Predios.aspx?Accion=consulta_tarifasxzonagiro&zona_id=" + zona_id + "&giro_id=" + giro_id,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null && data.length > 0) {
                $("#Cmb_Tarifa").val(data[0].tarifa_id);
                $('#Cmb_Tarifa').removeAttr('style');
                $('#Cmb_Tarifa').attr('style', 'width:98%;');
            }
        }
    });
}
//Alta del Predio--------------------------------------------------------------------------------------------------------
function Btn_Nuevo_Click() {
    $("#Lbl_Mensaje_Error").text('');
    $("#Lbl_Mensaje_Error").hide();
    $("#Img_Error").hide();

    var Btn_Nuevo = $("#Btn_Nuevo");
    var Btn_Modificar = $("#Btn_Modificar");
    //Verificar el tooltip del boton
    if (Btn_Nuevo.attr('alt') == "Nuevo") {
        if (Btn_Modificar.attr('alt') == "Guardar") {
            return;
        }
        //Habilitar y limpiar
        Configurar_Botones_Predio("Nuevo");
        Limpia_Campos_Predios();
    }
    else {
        Alta_Predios();
    }

}
function Alta_Predios() {
    //recupera la información de que se enviará al servidor para validar el alta
    var requeridos = Valida_Datos_Requeridos_Predio();
    if (requeridos == "") {
        //Recupera la información a enviar
        var registro = $('.altaPredio');

        //envia la información al servidor
        $.ajax({
            url: "../../Controladores/Frm_Controlador_Predios.aspx?Accion=alta_predio&" + registro.serialize(),
            type: 'POST',
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    //$.each(data, function (i, item) {
                    var error = data[2].error;
                    var predio_id = data[0].predio_id;
                    var alta = data[1].alta; ;
                    if (error != "") {
                        //Error al generar el alta
                        $("#Lbl_Mensaje_Error").removeAttr('style');
                        $("#Lbl_Mensaje_Error").attr('style', 'color: rgb(153, 0, 0); display: inline;');
                        $("#Lbl_Mensaje_Error").text(error);
                        $("#Lbl_Mensaje_Error").show();
                        $("#Img_Error").show();
                    }
                    else {
                        //Alta correcta
                        Configurar_Botones_Predio("Inicial");
                        $("#Lbl_Mensaje_Error").removeAttr('style');
                        $("#Lbl_Mensaje_Error").attr('style', 'color: rgb(153, 0, 0); display: inline;');
                        $("#Lbl_Mensaje_Error").text(alta);
                        $("#Lbl_Mensaje_Error").show();
                        $("#Img_Error").show();
                    }
                }
            }
        });

    }
    else {
        $("#Lbl_Mensaje_Error").removeAttr('style');
        $("#Lbl_Mensaje_Error").attr('style', 'color: rgb(153, 0, 0); display: inline;');
        $("#Lbl_Mensaje_Error").text(requeridos);
        $("#Lbl_Mensaje_Error").show();
        $("#Img_Error").show();
    }
}
function Valida_Datos_Requeridos_Predio() {
    var opciones = $('.requerido');
    var mensaje = "";
    opciones.each(function (i, item) {
        if (item.type == "text") {
            if ($('#' + item.id + '').val() == "") {
                mensaje += item.name + ",\n";
            }
        }
        if (item.type == "select-one") {
            var cmboption = $("#" + item.id + " option:selected").val();
            if (cmboption == "Seleccione") {
                mensaje += item.name + ", \n";
            }
        }
    });
    if (mensaje != "") {
        mensaje = "Falta la siguiente información por llenar: " + mensaje;
    }
    return mensaje;
}
//Modificar Predios ----------------------------------------------------------------------------------------------------------
function Btn_Modificar_Click() {
    $("#Lbl_Mensaje_Error").text('');
    $("#Lbl_Mensaje_Error").hide();
    $("#Img_Error").hide();

    var Btn_Modificar = $("#Btn_Modificar");
    var Btn_Nuevo = $("#Btn_Nuevo");
    //Verificar el tooltip del boton
    if (Btn_Modificar.attr('alt') == "Modificar") {
        if (Btn_Nuevo.attr('alt') == "Guardar") {
            return;
        }
        //verifica que un predio este seleccionado
        var Predio_ID = $('#Txt_Predio_ID').val();
        if (Predio_ID != "") {
            //Habilitar y limpiar
            Configurar_Botones_Predio("Modificar");
        }
        else {
            $("#Lbl_Mensaje_Error").text('Debe seleccionar un predio para poder modificarlo');
            $("#Lbl_Mensaje_Error").show();
            $("#Img_Error").show();
        }
    }
    else {
        Modificar_Predios();
    }
}
function Modificar_Predios() {
    //recupera la información de que se enviará al servidor para validar el alta
    var requeridos = Valida_Datos_Requeridos_Predio();
    if (requeridos == "") {
        //Recupera la información a enviar
        var registro = $('.altaPredio');

        //envia la información al servidor
        $.ajax({
            url: "../../Controladores/Frm_Controlador_Predios.aspx?Accion=modificar_predio&" + registro.serialize(),
            type: 'POST',
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    //$.each(data, function (i, item) {
                    var error = data[2].error;
                    var predio_id = data[0].predio_id;
                    var alta = data[1].alta; ;
                    if (error != "") {
                        //Error al generar el alta
                        $("#Lbl_Mensaje_Error").removeAttr('style');
                        $("#Lbl_Mensaje_Error").attr('style', 'color: rgb(153, 0, 0); display: inline;');
                        $("#Lbl_Mensaje_Error").text(error);
                        $("#Lbl_Mensaje_Error").show();
                        $("#Img_Error").show();
                    }
                    else {
                        //Edición correcta
                        Configurar_Botones_Predio("Inicial");
                        $("#Lbl_Mensaje_Error").removeAttr('style');
                        $("#Lbl_Mensaje_Error").attr('style', 'color: rgb(153, 0, 0); display: inline;');
                        $("#Lbl_Mensaje_Error").text(alta);
                        $("#Lbl_Mensaje_Error").show();
                        $("#Img_Error").show();
                        //Limpia_Campos_Predios();

                    }
                }
            }
        });

    }
    else {
        $("#Lbl_Mensaje_Error").removeAttr('style');
        $("#Lbl_Mensaje_Error").attr('style', 'color: rgb(153, 0, 0); display: inline;');
        $("#Lbl_Mensaje_Error").text(requeridos);
        $("#Lbl_Mensaje_Error").show();
        $("#Img_Error").show();
    }
}
function Seleccionar_Predio(Predio_ID) {
    Limpia_Campos_Predios();
    $("#Lbl_Mensaje_Error").text('');
    $("#Lbl_Mensaje_Error").hide();
    $("#Img_Error").hide();
    //Realiza la consulta de la información del predio para colocarla en las cajas correspondientes
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Predios.aspx?Accion=Filtrar_Predios&Predio_ID=" + Predio_ID,
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $.each(data.rows, function (i, item) {
                    //asinga los valores a las cajas de texto y combos
                    //generales
                    $('#Txt_Predio_ID').val(item.Predio_ID);
                    $('#Cmb_Estatus').val(item.Estatus);
                    $('#Txt_Colonia').val(item.Colonia);
                    $('#Txt_Colonia_Hidden').val(item.Colonia_ID);
                    //Carga las calles de acuerdo a la colonia
                    Cargar_Calles(item.Colonia_ID);
                    Cargar_Calles_Referencia();
                    $('#Cmb_Calle').val(item.Calle_ID);
                    $('#Txt_Numero_Exterior').val(item.No_Exterior);
                    $('#Txt_Numero_Interior').val(item.No_Interior);
                    $('#Cmb_Calle_Referencia1').val(item.Calle_Referencia1_ID);
                    $('#Cmb_Calle_Referencia2').val(item.Calle_Referencia2_ID);
                    Cargar_Manzanas();
                    Cargar_Region();
                    $('#Cmb_Region').val(item.Region_ID);
                    Cargar_Sector();
                    $('#Cmb_Sector').val(item.Sector_ID);
                    $('#Cmb_Manzana').val(item.Manzana_ID);
                    $('#Cmb_Zona').val(item.Zona_ID);
                    //$('#Txt_Comentarios').text(item.Comentarios);
                    $('#Txt_Comentarios').val(item.Comentarios);
                    //Condición del predio
                    $('#Cmb_Giros').val(item.Giro_ID);
                    Cargar_Giros_Actividad(item.Giro_ID);
                    $('#Cmb_Actividad').val(item.Giro_Actividad_ID);
                    $('#Cmb_Tarifa').val(item.Tarifa_ID);
                    $('#Cmb_Tipo_Vivienda').val(item.Tipo_Vivienda_ID);
                    $('#Txt_Constructora_ID_Hidden').val(item.Constructora_ID);  
                    //Pendiente de consultar la constructora
                    $('#Txt_Constructora_ID').val(item.Constructora);
                    $('#Cmb_Condicion').val(item.Condicion_ID);
                    //$('#Txt_Descripcion_Condicion').text(item.Descripcion_Condicion);
                    $('#Txt_Descripcion_Condicion').val(item.Descripcion_Condicion);
                    $('#Cmb_Material_Banqueta').val(item.Material_Banqueta_ID);
                    $('#Txt_Superfice_M2').val(item.Superficie_M2);
                    $('#Txt_Superfice_Construida').val(item.Superficie_Construida_M2);
                    $('#Txt_Superficie_Jardin').val(item.Superficie_Jardin_M2);
                    $('#Txt_Niveles').val(item.Niveles);
                    $('#Txt_Muebles_Sanitarios').val(item.Muebles_Sanitarios);
                    $('#Txt_Subpredios').val(item.Subpredios);
                    $('#Txt_Latitud_Decimal').val(item.Latitud);
                    $('#Txt_Longitud_Decimal').val(item.Longitud);
                    $('#Cmb_Aplica_Saniamiento').val(item.Aplica_Saneamiento);
                    $('#Cmb_Aplica_Notificacion').val(item.Aplica_Notificacion);
                    $('#Txt_Motivo_No_Notificacion').val(item.Motivo_No_Notificacion);
                    grados_a_dms_latidud();
                    grados_a_dms_longitud();
                    //Toma
                    $('#Txt_Colonia_Toma').val(item.Colonia);
                    $('#Txt_Colonia_Toma_Hidden').val(item.Colonia_Ubicacion_Toma_ID);
                    Cargar_Calles_Toma(item.Colonia_Ubicacion_Toma_ID);
                    $('#Cmb_Calle_Toma').val(item.Calle_Ubicacion_Toma_ID);
                    Cargar_Calles_Referencia_Toma();
                    $('#Cmb_Calle_Referencia_1_Toma').val(item.Calle_Ubicacion_Toma_Referencia1);
                    $('#Cmb_Calle_Referencia_2_Toma').val(item.Calle_Ubicacion_Toma_Referencia2);
                    $('#Cmb_Tipo_Conexion_Toma').val(item.Tipo_Conexion);
                    //Consulta la información del medido que tiene
                    $('#Txt_No_Medidor').val('');
                    $('#Txt_Fecha_Instalacion').val('');
                    $.ajax({
                        url: "../../Controladores/Frm_Controlador_Predios.aspx?Accion=consulta_medidor&Predio_ID=" + Predio_ID,
                        type: 'POST',
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data1) {
                            if (data1 != null) {
                                $.each(data1, function (k, item2) {
                                    $('#Txt_No_Medidor').val(item2.No_Medidor);
                                    $('#Txt_Fecha_Instalacion').val(item2.Fecha_Instalacion);
                                });
                            }
                        }
                    });
                    //Otros
                    $('#Txt_Cuenta_Predial').val(item.No_Cuenta_Predial);
                    $('#Txt_No_Cuenta').val(item.No_Cuenta);
                    $('#Txt_No_Contrato').val(item.No_Contrato);
                    //Consulta los datos de la domicialiacion
                    //Domiciliacion
                    $('#Txt_Cliente').val('');
                    $('#Txt_Cliente_Servicio').val('');
                    $('#Txt_Domicilio').val('');
                    $('#Txt_Telefono_Casa').val('');
                    $('#Txt_Telefono_Oficina').val('');
                    $('#Txt_Tipo_Tarjeta').val('');
                    $('#Txt_Banco').val('');
                    $('#Txt_Fecha_Domiciliacion').val('');
                    $.ajax({
                        url: "../../Controladores/Frm_Controlador_Predios.aspx?Accion=consulta_domiciliacion&Predio_ID=" + Predio_ID,
                        type: 'POST',
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data3) {
                            if (data3 != null) {
                                $.each(data3, function (j, item1) {
                                    $('#Txt_Cliente').val(item1.Nombre_Titular);
                                    $('#Txt_Cliente_Servicio').val(item1.Nombre_Cliente_Titular);
                                    $('#Txt_Domicilio').val(item1.Domicilio);
                                    $('#Txt_Telefono_Casa').val(item1.Telefono_Casa);
                                    $('#Txt_Telefono_Oficina').val(item1.Telefono_Oficina);
                                    $('#Txt_Tipo_Tarjeta').val(item1.Tipo_Tarjeta);
                                    $('#Txt_Banco').val(item1.Banco);
                                    $('#Txt_Fecha_Domiciliacion').val(item1.Fecha);
                                });
                            }
                        }
                    });
                    $('#Filtros_Predios').hide();
                    $('#Alta_Predios').show();
                    $('.requerido').attr('style', 'width:98%;');
                    $('#Txt_Numero_Exterior').attr('style', 'width:30%;');
                    //QUita de los combos las opciones
                    $('.disabled').removeAttr('disabled');
                    $('.altaPredio').attr('disabled', 'disabled');
                    $('#Btn_Salir').attr('alt', 'Cancelar');
                    $('#Tab_Predios').tabs('select', 'Condiciones Predio');
                    $('#Tab_Predios').tabs('select', 'Generales');
                });
            }
        }
    });
}
//Declaracion de variables
//            String Validacion = String.Empty; //Variable que contiene el resultado de la validacion

//            try
//            {
//                //Verificar el tooltip del boton
//                if (Btn_Modificar.ToolTip == "Modificar")
//                    //Verificar si se ha seleccionado un elemento
//                    if (Grid_Calles.SelectedIndex > -1)
//                        Habilita_Controles("Modificar");
//                    else
//                    {
//                        Lbl_Mensaje_Error.Visible = true;
//                        Img_Error.Visible = true;
//                        Lbl_Mensaje_Error.Text = "Favor de seleccionar La Calle a modificar";
//                    }
//                else
//                {
//                    //Verificar si se ha seleccionado un elementos
//                    if (Grid_Calles.SelectedIndex > -1)
//                    {
//                        //Verificar si la validacion es correcta
//                        Validacion = Valida_Datos();
//                        if (Validacion == "" || Validacion == String.Empty)
//                            Cambio_Calles();
//                        else
//                        {
//                            Lbl_Mensaje_Error.Visible = true;
//                            Img_Error.Visible = true;
//                            Lbl_Mensaje_Error.Text = Validacion;
//                        }
//                    }
//                    else
//                    {
//                        Lbl_Mensaje_Error.Visible = true;
//                        Img_Error.Visible = true;
//                        Lbl_Mensaje_Error.Text = "Favor de seleccionar la Calle a modificar";
//                    }
//                }
//            }
//            catch (Exception Ex)
//            {
//                Lbl_Mensaje_Error.Text = "Error: (Btn_Modificar_Click)" + Ex.ToString();
//                Mostrar_Informacion(1);
//            }
//}
//Salir y Cancelar Alta/Modificacion -----------------------------------------------------------------------------------------------------------------------------------------------------------------
function Btn_Salir_Click() {
    var estatus = $("#Btn_Salir").attr('alt')

    Limpia_Campos_Predios();
    if (estatus == "Salir") {
        window.location = '../Paginas_Generales/Frm_Apl_Principal.aspx';
    }
    else {
        Configurar_Botones_Predio("Inicial");

    }
}


//Funciones adicionales para validaciones---------------------------------------------------------------------------------
function grados_a_dms_latidud() {
    var grados = $('#Txt_Latitud_Decimal').val();
    var D;
    var M;
    var S;
    if (grados < 0) {
        return;
    }
    //Obtiene los grados, la parte entera
    D = parseInt(grados);
    //De la parte decimal obtiene los minutos
    M = (grados - D) * 60;
    S = M;
    M = parseInt(M);
    //De la parte decimal restante + 60 para obtener los segundos
    S = (S - M) * 60;
    S = Math.round(S);
    //asigna los valores a la caja de texto respectiva
    $('#Txt_Latitud_GMS_Grados').val(D);
    $('#Txt_Latitud_GMS_Minutos').val(M);
    $('#Txt_Latitud_GMS_Segundos').val(S);
}
function grados_a_dms_longitud() {
    var grados = $('#Txt_Longitud_Decimal').val();
    var D;
    var M;
    var S;
    if (grados < 0) {
        return;
    }
    //Obtiene los grados, la parte entera
    D = parseInt(grados);
    //De la parte decimal obtiene los minutos
    M = (grados - D) * 60;
    S = M;
    M = parseInt(M);
    //De la parte decimal restante + 60 para obtener los segundos
    S = (S - M) * 60;
    S = Math.round(S);
    //asigna los valores a la caja de texto respectiva
    $('#Txt_Longitud_GMS_Grados').val(D);
    $('#Txt_Longitud_GMS_Minutos').val(M);
    $('#Txt_Longitud_GMS_Segundos').val(S);
}
function dms_a_grados_latidud() {
    var D = $('#Txt_Latitud_GMS_Grados').val();
    var M = $('#Txt_Latitud_GMS_Minutos').val();
    var S = $('#Txt_Latitud_GMS_Segundos').val();
    var grados;
    var segundos;
    if (D < 0) {
        return;
    }
    // Se forman los grados
    grados = parseInt(D);
    //Obtiene los segundos
    //Valida que los minutos tengan informacion
    if (isNumber(M)) {
        M = parseFloat(M);
    }
    else {
        M = 0;
    }
    if (isNumber(S)) {
        S = parseFloat(S);
    }
    else {
        S = 0;
    }

    segundos = (M * 60) + S;
    //Obtenemos la parte fraccionaria
    grados += (segundos / 3600)

    //asigna los valores a la caja de texto respectiva
    $('#Txt_Latitud_Decimal').val(grados);
}
function dms_a_grados_longitud() {
    var D = $('#Txt_Longitud_GMS_Grados').val();
    var M = $('#Txt_Longitud_GMS_Minutos').val();
    var S = $('#Txt_Longitud_GMS_Segundos').val();
    var grados;
    var segundos;
    if (D < 0) {
        return;
    }
    // Se forman los grados
    grados = parseInt(D);
    if (isNumber(M)) {
        M = parseFloat(M);
    }
    else {
        M = 0;
    }
    if (isNumber(S)) {
        S = parseFloat(S);
    }
    else {
        S = 0;
    }
    segundos = (M * 60) + S;
    //Obtenemos la parte fraccionaria
    grados += (segundos / 3600)

    //asigna los valores a la caja de texto respectiva
    $('#Txt_Longitud_Decimal').val(grados);
}
function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

//Modal progress, uso de animación de preogresso, indicador de actividad -------------------------------------------------
function MostrarProgress() {
    $('[id$=Upgrade]').show();
}
function OcultarProgress() {
    $('[id$=Upgrade]').delay(2000).hide();
}