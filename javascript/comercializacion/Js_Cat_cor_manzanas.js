﻿

var COLONIA_ID;
var operacion = "ninguno";
var renglon_editar;


$(function() { // inicio

    // ventana utilizada como modal que aparece cuando se hace una petición
    $('#ventana_mensaje').window('close');


    crearArbolColonias();
    crearPaneles();
    crearTab();
    crearGridColonias();
    crearGridManzanas();
    crearGridManzanaDetalles();
    bloquearControles();


    //eventos de los botones de la barra de herramientas inicio
    $("[id$='Btn_Nuevo']").click(function(event) {
        NuevaManzana();
        event.preventDefault();
    });

    $("[id$='Btn_Modificar']").click(function(event) {
        editarManzana();
        event.preventDefault();
    });

    $("[id$='Btn_Eliminar']").click(function(event) {
        eliminarRegistro();
        event.preventDefault();
    });

    $("[id$='Btn_Guardar']").click(function(event) {
        guardarRegistro();
        event.preventDefault();
    });

    $("[id$='Btn_Cancelar']").click(function(event) {
        cancelarRegistro();
        event.preventDefault();
    });

    $("[id$='Btn_Salir']").click(function(event) {
        event.preventDefault();
    });


    // fin de los eventos  de los botones de la barra de herramientas





});           // fin

//-----------------------------------------------------------------------------------

function crearGridColonias() {

    $('#grid_colonias').datagrid({
        title: 'Calles Colonias',
        width: 728,
        height: 160,
        columns: [[
        { field: 'P_Calle_Colonia_ID', title: 'calle_colonia_id', width: 100, sortable: true }, 
        { field: 'P_Nombre_Calle', title: 'Calle', width: 200, sortable: true },
        { field: 'P_Nombre_Colonia', title: 'Colonia', width: 200, sortable: true }     
    ]],
        pageSize: 20,
        remoteSort: false,
        pagination: false,
        rownumbers: true,
        fitColumns: true,
        singleSelect: true,
        loadMsg: 'cargando...',
        nowrap: false,
        toolbar: [{
            id: 'btn_grid_buscar',
            text: 'Buscar',
            iconCls: 'icon-search',
            handler: function() {
                //                var x, y;
                //                x = ($(window).width() - $('#ventana_colonias').window().width()) / 2;
                //                y = ($(window).height() - $('#ventana_colonias').window().height()) / 2;
                //                $('#ventana_colonias').window('move', { left: x, top: y });
                $('#ventana_colonias').window({
                    closed: false,
                    onOpen: function() {
                        $('#grid_colonias_buscar').datagrid('resize');
                        $('#grid_calles_buscar').datagrid('resize');
                        $('#grid_colonias_buscar').datagrid('loadData', { total: 0, rows: [] });
                        $('#grid_calles_buscar').datagrid('loadData', { total: 0, rows: [] });
                        $("[id$='txt_colonia_buscar']").val('');
                        $("[id$='txt_colonia_buscar']").focus();


                    }
                });

            }
        }, '-', {
            id: 'btn_grid_eliminar',
            text: 'Eliminar',
            iconCls: 'icon-eliminar',
            handler: function() {
                elminar_colonia_de_manzana();
            }
}]
            
        });

// ocultar una columna
$('#grid_colonias').datagrid('hideColumn', "P_Calle_Colonia_ID");
        
    }
    
 //-----------------------------------------------------------------------------------

    function crearTab() {
        $('#tab').tabs({
            onSelect: function(title) {

     
            }
        });


    }

    //-----------------------------------------------------------------------------------

    function crearGridManzanas() {

        $('#grid_manzanas').datagrid({
            title: 'Manzanas',
            width: 750,
            height: 150,
            columns: [[
        { field: 'manzana_id', title: 'manzana_id', width: 1, sortable: true },
        { field: 'sector_id', title: 'sector_id', width: 1, sortable: true },
        { field: 'sector', title: 'Sector', width: 100, sortable: true },
        { field: 'numero_manzana', title: 'Numero Manzana', width: 100, sortable: true },
        { field: 'comentario', title: 'Comentario', width: 200, sortable: true }
    ]],
            pageSize: 50,
            remoteSort: false,
            pagination: true,
            rownumbers: true,
            fitColumns: true,
            singleSelect: true,
            loadMsg: 'cargando...',
            nowrap: false,
            onClickRow: function(rowIndex, rowData) {
            //$('#arbol_colonias').tree('loadData', []);
            $('#grid_manzana_detalles').datagrid('loadData', {total:0, rows:[] } );
                
            obtener_colonias_de_una_manzana_registrada(rowData);
           
            }
        });

        // ocultar una columna
        $('#grid_manzanas').datagrid('hideColumn', "manzana_id");
        $('#grid_manzanas').datagrid('hideColumn', "sector_id");
        
    
    }

    //-----------------------------------------------------------------------------------
    
    function crearGridManzanaDetalles()
    {
      $('#grid_manzana_detalles').datagrid({
            title: 'Calles - Colonias',
            width: 750,
            height: 150,
            columns: [[   
        { field: 'p_calle_colonia_id', title: 'id_colonia', width: 0, sortable: true },    
        { field: 'p_nombre_calle', title: 'Calle', width: 150, sortable: true },
        { field: 'p_nombre_colonia', title: 'Colonia', width:150 ,sortable: true}
    ]],
            pageSize: 50,
            remoteSort: false,
            pagination: false,
            rownumbers: true,
            fitColumns: true,
            singleSelect: true,
            loadMsg: 'cargando...',
            nowrap: false,
            onClickRow: function(rowIndex, rowData) {
           
           
            }
        });
        
        $('#grid_manzana_detalles').datagrid('hideColumn','p_calle_colonia_id');
        
        
    }
    
    
    //-----------------------------------------------------------------------------------

    function crearPaneles() {

        $('#panel_colonias').panel({
            title: 'Colonias',
            closed: false  
        });

    }

    //-----------------------------------------------------------------------------------

    function crearArbolColonias() {
        $('#arbol_colonias').tree({
            checkbox: false,
            onBeforeExpand: function(node) {
                llenar_arbol_colonias(node);
            },
            onClick: function(node) {

            },
            onCheck: function(node, checked) {

            },
            onLoadSuccess: function(node, data) {

               
            }

        });
    
    }

    //-----------------------------------------------------------------------------------

    function busqueda_manzana() {
        var tipo;
        var nombre;

        nombre = $("[id$='txt_manzana_buscar']"); 
        
        if ($("[id$='opt_todos_manzana']").attr('checked')) {
            tipo = "todos";
        }

        if ($("[id$='opt_nombre_manzana']").attr('checked')) {

            if ($.trim(nombre.val()).length == 0) {
                $.messager.alert('Manzanas', 'Debes escribir un Nombre de manzana', '', function() { nombre.focus(); });
                return;
            }
            
             tipo="por nombre de manzana" 
        }


        $('#grid_manzanas').datagrid('loadData', { total: 0, rows: [] });
        $('#grid_manzana_detalles').datagrid('loadData', { total: 0, rows: [] });

        $('#grid_manzanas').datagrid({ url: '../../Controladores/frm_controlador_programacion_lectura.aspx', queryParams: {
            accion: 'obtener_manzanas_registradas',
            nombre_manzana: $.trim(nombre.val()),
            tipo:tipo
        }, pageNumber: 1
        });
        
        
     
    }


    //-----------------------------------------------------------------------------------

    function obtener_colonias_de_una_manzana_registrada(renglon) {
        $('#grid_manzana_detalles').datagrid({url:'../../Controladores/frm_controlador_programacion_lectura.aspx?accion=obtener_colonias_de_manzanas_registradas&MANZANA_ID=' + renglon.manzana_id });
       // $('#arbol_colonias').tree({ url: '../../Controladores/frm_controlador_programacion_lectura.aspx?accion=obtener_colonias_de_manzanas_registradas&MANZANA_ID=' + renglon.manzana_id });
    }

    function llenar_arbol_colonias(nodo) {
        COLONIA_ID = nodo.id;
        $('#arbol_colonias').tree('options').url = "../../Controladores/frm_controlador_programacion_lectura.aspx?accion=obtener_calles_apartir_de_id_colonia_version_arbol&COLONIA_ID=" + COLONIA_ID;
    }
    

    //-----------------------------------------------------------------------------------
    
    function elminar_colonia_de_manzana() {
        var renglon_colonias;

        renglon_colonias = $('#grid_colonias').datagrid('getSelected');

        if (renglon_colonias == null) {
            $.messager.alert('Manzanas', 'Debes Seleccionar una Colonia');
            return;
        }

        var index = $('#grid_colonias').datagrid('getRowIndex', renglon_colonias);
        $('#grid_colonias').datagrid('deleteRow', index);
        
     
    }



//-------------------------------------------------------------------------------------------------------------------

    //-----------------------------------------------------------------------------------

    function bloquearControles() {
        $("#datos_registro :input").attr('disabled', true);
        $('#btn_grid_buscar').linkbutton({ disabled: true });
        $('#btn_grid_eliminar').linkbutton({ disabled: true });
      
    }

    //-----------------------------------------------------------------------------------

    function desbloquearControles() {
        $("#datos_registro :input").attr('disabled', false);
        $('#btn_grid_buscar').linkbutton({ disabled: false });
        $('#btn_grid_eliminar').linkbutton({ disabled: false });
    
    }

    //-----------------------------------------------------------------------------------

    function limpiarControles() {
        $('#datos_registro :text').val('');
        $('#grid_colonias').datagrid('loadData', { total: 0, rows: [] });
    }

    //-------------------------------------------------------------------------------------------------------------------

    function NuevaManzana() {
        ext_ocultarBotonesOperacion();
        ext_visualizarBotonesGuardarCancelar();
        desbloquearControles();
        limpiarControles();
        operacion = "nuevo";
        $('#tab').tabs('select', 'Registro Manzana');     
        $('.tabs li a.tabs-inner').bind('click', disableLink);
               
        
    }

    //-------------------------------------------------------------------------------------------------------------------

    function editarManzana() {

        renglon_editar = $('#grid_manzanas').datagrid('getSelected');
        limpiarControles();

       if (renglon_editar == null) {
           $.messager.alert('Manzana', 'Tienes que seleccionar una manzana');
           return;
       }

       asignarInformacion(renglon_editar);
            
        ext_ocultarBotonesOperacion();
        ext_visualizarBotonesGuardarCancelar();
        desbloquearControles();
        
        operacion = "editar";
        $('#tab').tabs('select', 'Registro Manzana');
        $('.tabs li a.tabs-inner').bind('click', disableLink);

    }

    //-------------------------------------------------------------------------------------------------------------------

    function guardarRegistro() {
        var controles;
        var colonias;
    
        if (valoresRequeridosGeneral("datos_registro") == false) {
            $.messager.alert('Manzanas', 'Se requieren Algunos Valores');
            return;
        }

        colonias = toJSON($('#grid_colonias').datagrid('getRows'));     
        controles = toJSON($("#datos_registro :input").serializeObject());
        
        var colonias_calles=$('#grid_colonias').datagrid('getRows');
        
       if(operacion=="editar"  && colonias_calles.length==0 ){
           $.messager.alert('Manzana','Se tienes que dejar al menos una calle en la manzana a editar');
           return;
       } 
           

        $('#ventana_mensaje').window('open');

        $.ajax({
            type: "POST",
            url: "../../Controladores/frm_controlador_programacion_lectura.aspx/guardarManzana",
            data: "{'controles':'" + controles + "','colonias':'" + colonias + "','operacion':'" + operacion + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                var respuesta = eval("(" + response.d + ")");
                if (respuesta.mensaje == "bien") {
                    $('#ventana_mensaje').window('close');
                    cancelarRegistro();
                    $('#grid_manzanas').datagrid('loadData', { total: 0, rows: [] });
                    $('#grid_manzana_detalles').datagrid('loadData',{ total: 0, rows: [] });        
                    $.messager.alert('Manazana', 'Proceso terminado');
                }
                else {
                    $('#ventana_mensaje').window('close');
                    $.messager.alert('Manazana', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                    if (operacion == "eliminar") {
                        cancelarRegistro();
                    }

                }

            },
            error: function(result) {
                $('#ventana_mensaje').window('close');
                $.messager.alert('Manazana', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');

            }

        });
                

    
    
    
    }
   
    //-------------------------------------------------------------------------------------------------------------------

    function eliminarRegistro() {
        renglon_editar = $('#grid_manzanas').datagrid('getSelected');
        limpiarControles();

        if (renglon_editar == null) {
            $.messager.alert('Manzana', 'Tienes que seleccionar una manzana');
            return;
        }

        $.messager.confirm('Manzana', 'Realmente Deseas Eliminar la Manzana Seleccionada', function(r) {
            if (r) {
                //alert('confirmed:' + r);
                asignarInformacion(renglon_editar);
                operacion = "eliminar";
                $('.tabs li a.tabs-inner').bind('click', disableLink);
                desbloquearControles();
                guardarRegistro();

            }
        });


    
    }
   

    //-------------------------------------------------------------------------------------------------------------------
    
    function cancelarRegistro() {
        ext_ocultarBotonesGuardarCancelar();
        ext_visualizarBotonesOperacion();
        bloquearControles();
        limpiarControles();
        operacion = "ninguno";
        $('.tabs li a.tabs-inner').unbind('click', disableLink);
        $("#[id$='cmb_P_Sector_ID'] > option[value=-1]").attr('selected', 'selected');

    }

    //-------------------------------------------------------------------------------------------------------------------

    function disableLink(e) {
        // cancels the event 
        e.preventDefault();
        return false;
    }

    //-------------------------------------------------------------------------------------------------------------------

    function asignarInformacion(renglon) {

       
        $('#grid_colonias').datagrid('loadData', { total: 0, rows: [] });
           
        $("[id$='txt_P_Manzana_ID']").val(renglon.manzana_id);
        $("[id$='txt_P_Numero_Manzana']").val(renglon.numero_manzana);
        $("[id$='txt_P_Comentarios']").val(renglon.comentario);
        $("#[id$='cmb_P_Sector_ID'] > option[value=" + renglon.sector_id + "]").attr('selected', 'selected');


        var rows = $('#grid_manzana_detalles').datagrid('getRows');
        
        for(var i=0;i<rows.length;i++){
        
             $('#grid_colonias').datagrid('appendRow', {
                    P_Nombre_Calle: rows[i].p_nombre_calle,
                    P_Nombre_Colonia: rows[i].p_nombre_colonia,
                    P_Calle_Colonia_ID: rows[i].p_calle_colonia_id
                });
           
        }
        
    
    }
    
 //--------------------------------------------------------------------------------------   