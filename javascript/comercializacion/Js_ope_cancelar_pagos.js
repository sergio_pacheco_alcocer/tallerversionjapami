﻿var estado; 
var tipo_pago;
var accion;
var bonificacion_id;
var no_diverso;


$(function(){

 $('#ventana_mensaje').window('close');

  $("[id$='txt_fecha_inicio']").datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'dd/mm/yy'
});


$("[id$='txt_fecha_final']").datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'dd/mm/yy'
});

crearTablaPagos();
crearTablasTarjeta();
cretaTablaCheque();

//eventos combos

$('.combito').change(function(){
  limpiarDatosTablas();
});



});

//-----------------------------------------------------------------------------

function cretaTablaCheque(){

      $('#tablas_cheques').datagrid({  // tabla inicio
            title: 'Pagos Cheques',
            width: 810,
            height: 250,
            columns: [[
        { field: 'no_cheque', title: 'No. Cheque', width:150, sortable: true },
        { field: 'cantidad', title: 'Cantidad', width: 150, sortable: true },
        { field: 'banco', title: 'Banco', width: 180, sortable: true },
        { field: 'nombre_cheque', title: 'Nombre', width: 250, sortable: true },
        { field: 'estado_cheque', title: 'Estado', width: 150, sortable: true }
                
        
    ]],     
             onClickRow: function(rowIndex, rowData) {
            
            }, 
            pageSize: 50,
            pagination: false,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            showFooter: false,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap: false

        });  // tabla final

   

}



//-----------------------------------------------------------------------------

function crearTablasTarjeta(){

   
   $('#tablas_tarjeta').datagrid({  // tabla inicio
            title: 'Pagos Tarjetas ',
            width: 810,
            height: 250,
            columns: [[ 
        { field: 'no_aprobacion', title: 'No.Aprobacion', width: 150, sortable: true },
        { field: 'cantidad', title: 'Cantidad', width: 150, sortable: true },
        { field: 'no_tarjeta', title: 'No. Tarjeta', width: 150, sortable: true },
        { field: 'banco', title: 'Banco', width: 200, sortable: true },
        { field: 'estado_tarjeta', title: 'Estado', width: 180, sortable: true }
                
        
    ]],     
             onClickRow: function(rowIndex, rowData) {
            
            }, 
            pageSize: 50,
            pagination: false,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            showFooter: false,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap: false

        });  // tabla final


}


//-----------------------------------------------------------------------------

function crearTablaPagos(){
   
   $('#tablas_pagos').datagrid({  // tabla inicio
            title: 'Pagos Realizados ',
            width: 810,
            height: 250,
            columns: [[
        { field: 'no_recibo', title: 'No. recibo', width: 0, sortable: true },
        { field: 'no_cuenta', title: 'No. Cuenta', width: 80, sortable: true },
        { field: 'codigo_barras', title: 'Codigos Barras', width: 150, sortable: true },
        { field: 'total_pago', title: 'Total Pago', width: 180, sortable: true },
        { field: 'pago_efectivo', title: 'Pago Efectivo', width: 180, sortable: true },
        { field: 'pago_cheque', title: 'Pago Cheque', width: 180, sortable: true },
        { field: 'pago_tarjeta', title: 'Pago Tarjeta', width: 180, sortable: true },
        { field: 'fecha_elaboro', title: 'F. Pago', width: 80, sortable: true },
        { field: 'estado_recibo', title: 'Estado Recibo', width: 150, sortable: true },
        { field: 'numero_caja', title: 'No. Caja', width: 100, sortable: true },
        { field: 'nombre_sucursal', title: 'Sucursal', width: 150, sortable: true },
        { field: 'establecimiento', title: 'Establecimiento', width: 150, sortable: true },
        { field: 'bonificacion_id', title: 'bonificacion_id', width: 0, sortable: true }, 
        { field: 'no_diverso', title: 'no_diverso', width: 0, sortable: true },
        { field: 'no_factura_recibo', title: 'no_factura_recibo', width: 0, sortable: true },
        { field: 'no_factura_recibo_parcial', title: 'no_factura_recibo_parcial', width: 0, sortable: true },
        { field: 'no_movimiento', title: 'no_movimiento', width: 0, sortable: true }
        
        
        
    ]],     
             onClickRow: function(rowIndex, rowData) {
              obtenerPagosChequesyTarjetas(rowData.no_recibo);
            }, 
            pageSize: 50,
            pagination: true,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            showFooter: false,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap: false

        });  // tabla final
        
       $('#tablas_pagos').datagrid('hideColumn','no_recibo');
       $('#tablas_pagos').datagrid('hideColumn','bonificacion_id');
       $('#tablas_pagos').datagrid('hideColumn','no_diverso');
       $('#tablas_pagos').datagrid('hideColumn','no_factura_recibo');
       $('#tablas_pagos').datagrid('hideColumn','no_factura_recibo_parcial');
       $('#tablas_pagos').datagrid('hideColumn','no_movimiento');
       
}


//-----------------------------------------------------------------------------

function obtenerPagosChequesyTarjetas(no_recibo){


 $('#tablas_tarjeta').datagrid('loadData', { total: 0, rows: [] }); 
 $('#tablas_tarjeta').datagrid({ url: '../../Controladores/frm_controlador_cancelacion_recibos.aspx', queryParams: {
         accion: 'obtenerPagosTarjetas',
         no_recibo:no_recibo
     }, pageNumber: 1
     });
     
   $('#tablas_cheques').datagrid('loadData', { total: 0, rows: [] });   
      $('#tablas_cheques').datagrid({ url: '../../Controladores/frm_controlador_cancelacion_recibos.aspx', queryParams: {
         accion: 'obtenerPagosCheques',
        no_recibo:no_recibo
     }, pageNumber: 1
     });



}


//-----------------------------------------------------------------------------

function obtenerAccionaRealizar(letra){
var respuesta="";

   if(letra=="F")
      respuesta="obtener_pagos_Factura_Normal";
   if(letra=="P")
      respuesta="obtener_pagos_Parciales";
   if(letra=="A")
      respuesta="obtener_pagos_Anticipado";
   if(letra=="D")
      respuesta="obtener_pagos_Diferidos";  
   if(letra=="B")
      respuesta="obtener_pagos_Bonficaciones";    
   if(letra=="T")
      respuesta="obtener_pagos_establecimiento";   
      
 
 return respuesta;  
}

//-----------------------------------------------------------------------------

function limpiarDatosTablas(){
   $('#tablas_pagos').datagrid('loadData', { total: 0, rows: [] }); 
   $('#tablas_tarjeta').datagrid('loadData', { total: 0, rows: [] });   
   $('#tablas_cheques').datagrid('loadData', { total: 0, rows: [] });   
}

//-----------------------------------------------------------------------------

function obtenerRecibosPagos(){

 
limpiarDatosTablas(); 
 
if (validarFechas($("[id$='txt_fecha_inicio']"), $("[id$='txt_fecha_final']"))) {
      return;
    }

estado=$("[id$='cmb_estado']").val(); 

if (estado.indexOf("Seleccione") != -1){
   $.messager.alert('Japami','Selecciona un estado','error');
   return;
}   

tipo_pago=$("[id$='cmb_tipo_pago']").val();

if (tipo_pago.indexOf("Seleccione") != -1){
   $.messager.alert('Japami','Selecciona un tipo de pago','error');
   return;
} 

accion=obtenerAccionaRealizar(tipo_pago);
 
 
 $('#tablas_pagos').datagrid({ url: '../../Controladores/frm_controlador_cancelacion_recibos.aspx', queryParams: {
         accion: accion,
         f_inicio: $("[id$='txt_fecha_inicio']").val(),
         f_final:$("[id$='txt_fecha_final']").val(),
         estado:estado,
         No_Cuenta: $("[id$='Txt_No_Cuenta']").val()
     }, pageNumber: 1
     });


}

//------------------------------------------------------------------------------

function cancelarRecibo(){
var renglon;

renglon=$('#tablas_pagos').datagrid('getSelected');

if(renglon==null){
  $.messager.alert('Japami','Tienes que seleccionar un pago','error');
  return;  
}

if(renglon.estado_recibo=="CANCELADO"){
  $.messager.alert('Japami','El elemento seleccionado ya esta cancelado','error');
  return;
}

 
 if (tipo_pago=="F"){
    cancelarFacturaNormal(renglon);     
 }
 if (tipo_pago=="B"){
    cancelarBonificacion(renglon);
 }
 if(tipo_pago=="D"){
    cancelarDiversos(renglon);
 }
 
 if(tipo_pago=="A"){
    cancelarAnticipados(renglon);
 }
 
 if(tipo_pago=="P"){
   cancelarPagosParciales(renglon);
 }
 
  if(tipo_pago=="T"){
   cancelarPagosEstablecimiento(renglon);
 }


}


//-----------------------------------------------------------------------------

function cancelarPagosEstablecimiento(renglon){

 $('#ventana_mensaje').window('open');  

 $.ajax({ //inicio
             type: "POST",
             url: "../../Controladores/frm_controlador_cancelacion_recibos.aspx/cancelarPagosEstablecimiento",
             data: "{'no_recibo':'" + renglon.no_recibo + "','codigo_barras':'" + renglon.codigo_barras + "','fecha_pago':'" + renglon.fecha_elaboro + "','no_movimiento':'" + renglon.no_movimiento + "','no_factura_recibo':'" + renglon.no_factura_recibo + "'}",
             contentType: "application/json; charset=utf-8",
             dataType: "json",
             success: function(response) {
                 $('#ventana_mensaje').window('close'); 
                 var respuesta = eval("(" + response.d + ")");
                 if (respuesta.mensaje == "bien") {                   
                    procesoExitosoEstablecimiento(respuesta,renglon);
                     
                 } else {
                    $('#ventana_mensaje').window('close');
                     $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                     
                 }

             },
             error: function(result) {
                 $('#ventana_mensaje').window('close');
                 alert("ERROR " + result.status + ' ' + result.statusText);

             }


         });  //fin peticion ajax


}


function  procesoExitosoEstablecimiento(respuesta,renglon){

    var index;
    $.messager.alert('Japami', 'Proceso Exitoso', 'info',function(){
        
        $('#tablas_tarjeta').datagrid('loadData', { total: 0, rows: [] });   
        $('#tablas_cheques').datagrid('loadData', { total: 0, rows: [] });   
       index=  $('#tablas_pagos').datagrid('getRowIndex',renglon);
       $('#tablas_pagos').datagrid('deleteRow',index);
       
    });

}



//-----------------------------------------------------------------------------


function cancelarPagosParciales(renglon){

 $('#ventana_mensaje').window('open');  

 $.ajax({ //inicio
             type: "POST",
             url: "../../Controladores/frm_controlador_cancelacion_recibos.aspx/cancelarPagosParciales",
             data: "{'no_recibo':'" + renglon.no_recibo + "','codigo_barras':'" + renglon.codigo_barras + "','fecha_pago':'" + renglon.fecha_elaboro + "','no_factura_recibo_parcial':'" + renglon.no_factura_recibo_parcial + "'}",
             contentType: "application/json; charset=utf-8",
             dataType: "json",
             success: function(response) {
                 $('#ventana_mensaje').window('close'); 
                 var respuesta = eval("(" + response.d + ")");
                 if (respuesta.mensaje == "bien") {                   
                    procesoExitosoCancelacionAnticipado(respuesta,renglon);
                     
                 } else {
                    $('#ventana_mensaje').window('close');
                     $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                     
                 }

             },
             error: function(result) {
                 $('#ventana_mensaje').window('close');
                 alert("ERROR " + result.status + ' ' + result.statusText);

             }


         });  //fin peticion ajax

}

function procesoExitosoCancelacionParciales(respuesta,renglon){
    var index;
     
    
        
    $.messager.alert('Japami', 'Proceso Exitoso', 'info',function(){
        
        $('#tablas_tarjeta').datagrid('loadData', { total: 0, rows: [] });   
        $('#tablas_cheques').datagrid('loadData', { total: 0, rows: [] });   
       index=  $('#tablas_pagos').datagrid('getRowIndex',renglon);
       $('#tablas_pagos').datagrid('deleteRow',index);
       
    });
}

//------------------------------------------------------------------------------

function cancelarAnticipados(renglon){

 $('#ventana_mensaje').window('open');  

 $.ajax({ //inicio
             type: "POST",
             url: "../../Controladores/frm_controlador_cancelacion_recibos.aspx/cancelarAnticipado",
             data: "{'no_recibo':'" + renglon.no_recibo + "','codigo_barras':'" + renglon.codigo_barras + "','fecha_pago':'" + renglon.fecha_elaboro + "','no_factura_recibo_parcial':'" + renglon.no_factura_recibo_parcial + "'}",
             contentType: "application/json; charset=utf-8",
             dataType: "json",
             success: function(response) {
                 $('#ventana_mensaje').window('close'); 
                 var respuesta = eval("(" + response.d + ")");
                 if (respuesta.mensaje == "bien") {                   
                    procesoExitosoCancelacionAnticipado(respuesta,renglon);
                     
                 } else {
                    $('#ventana_mensaje').window('close');
                     $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                     
                 }

             },
             error: function(result) {
                 $('#ventana_mensaje').window('close');
                 alert("ERROR " + result.status + ' ' + result.statusText);

             }


         });  //fin peticion ajax

}

function procesoExitosoCancelacionAnticipado(respuesta,renglon){
    var index;
     
    
        
    $.messager.alert('Japami', 'Proceso Exitoso', 'info',function(){
        
        $('#tablas_tarjeta').datagrid('loadData', { total: 0, rows: [] });   
        $('#tablas_cheques').datagrid('loadData', { total: 0, rows: [] });   
       index=  $('#tablas_pagos').datagrid('getRowIndex',renglon);
       $('#tablas_pagos').datagrid('deleteRow',index);
       
    });
}


//------------------------------------------------------------------------------

function cancelarDiversos(renglon){

 $('#ventana_mensaje').window('open');  

 $.ajax({ //inicio
             type: "POST",
             url: "../../Controladores/frm_controlador_cancelacion_recibos.aspx/cancelarDiversos",
             data: "{'no_recibo':'" + renglon.no_recibo + "','codigo_barras':'" + renglon.codigo_barras + "','fecha_pago':'" + renglon.fecha_elaboro + "','no_diverso':'" + renglon.no_diverso + "'}",
             contentType: "application/json; charset=utf-8",
             dataType: "json",
             success: function(response) {
                 $('#ventana_mensaje').window('close'); 
                 var respuesta = eval("(" + response.d + ")");
                 if (respuesta.mensaje == "bien") {                   
                    procesoExitosoCancelacionDiversos(respuesta,renglon);
                     
                 } else {
                    $('#ventana_mensaje').window('close');
                     $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                     
                 }

             },
             error: function(result) {
                 $('#ventana_mensaje').window('close');
                 alert("ERROR " + result.status + ' ' + result.statusText);

             }


         });  //fin peticion ajax



}

function procesoExitosoCancelacionDiversos(respuesta,renglon){
    var index;
     
    
        
    $.messager.alert('Japami', 'Proceso Exitoso', 'info',function(){
        
        $('#tablas_tarjeta').datagrid('loadData', { total: 0, rows: [] });   
        $('#tablas_cheques').datagrid('loadData', { total: 0, rows: [] });   
       index=  $('#tablas_pagos').datagrid('getRowIndex',renglon);
       $('#tablas_pagos').datagrid('deleteRow',index);
       
    });

}



//------------------------------------------------------------------------------

function cancelarBonificacion(renglon){

 $('#ventana_mensaje').window('open');  

 $.ajax({ //inicio
             type: "POST",
             url: "../../Controladores/frm_controlador_cancelacion_recibos.aspx/cancelarBonificaciones",
             data: "{'no_recibo':'" + renglon.no_recibo + "','codigo_barras':'" + renglon.codigo_barras + "','fecha_pago':'" + renglon.fecha_elaboro + "','bonificacion_id':'" + renglon.bonificacion_id + "'}",
             contentType: "application/json; charset=utf-8",
             dataType: "json",
             success: function(response) {
                 $('#ventana_mensaje').window('close'); 
                 var respuesta = eval("(" + response.d + ")");
                 if (respuesta.mensaje == "bien") {                   
                    procesoExitosoCancelacionBonificacion(respuesta,renglon);
                     
                 } else {
                    $('#ventana_mensaje').window('close');
                     $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                     
                 }

             },
             error: function(result) {
                 $('#ventana_mensaje').window('close');
                 alert("ERROR " + result.status + ' ' + result.statusText);

             }


         });  //fin peticion ajax

}


function procesoExitosoCancelacionBonificacion(respuesta,renglon){
    var index;
     
    $.messager.alert('Japami', 'Proceso Exitoso', 'info',function(){
        $('#tablas_tarjeta').datagrid('loadData', { total: 0, rows: [] });   
        $('#tablas_cheques').datagrid('loadData', { total: 0, rows: [] });   
        index=  $('#tablas_pagos').datagrid('getRowIndex',renglon);
        $('#tablas_pagos').datagrid('deleteRow',index);
       
    });

}


//------------------------------------------------------------------------------

function cancelarFacturaNormal(renglon)
{

  $('#ventana_mensaje').window('open');

  $.ajax({ //inicio
      type: "POST",
      url: "../../Controladores/frm_controlador_cancelacion_recibos.aspx/cancelarFacturaNormal",
      data: "{'no_recibo':'" + renglon.no_recibo + "','codigo_barras':'" + renglon.codigo_barras + "','fecha_pago':'" + renglon.fecha_elaboro + "','no_factura_recibo':'" + renglon.no_factura_recibo + "'}",
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function (response) {
          $('#ventana_mensaje').window('close');
          var respuesta = eval("(" + response.d + ")");
          if (respuesta.mensaje == "bien") {
              procesoExitosoCancelacionFacturaNormal(respuesta, renglon);
          }
          else {
              if (respuesta.mensaje == "NoPagado") {
                  $.messager.alert('Japami', respuesta.dato1, 'error');
              } else {
                  $('#ventana_mensaje').window('close');
                  $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');
              }

          }

      },
      error: function (result) {
          $('#ventana_mensaje').window('close');
          alert("ERROR " + result.status + ' ' + result.statusText);

      }


  });      //fin peticion ajax

}


function procesoExitosoCancelacionFacturaNormal(respuesta,renglon)
{  
   var index;
     
    $.messager.alert('Japami', 'Proceso Exitoso', 'info',function(){
         
        $('#tablas_tarjeta').datagrid('loadData', { total: 0, rows: [] });   
        $('#tablas_cheques').datagrid('loadData', { total: 0, rows: [] });    
        index=  $('#tablas_pagos').datagrid('getRowIndex',renglon);
        $('#tablas_pagos').datagrid('deleteRow',index);
       
    });
    
}

//-----------------------------------------------------------