﻿

$(function() {

    crearTablas();

});

//--------------------------------------------------------------------------------------

function crearTablas() {

    //incio tabla
    $('#tabla_listado_nc').datagrid({
        title: 'Notas de Cargo ',
        width: 830,
        height: 350,
        toolbar: [{
            text: 'Imprimir',
            iconCls: 'icon-print',
            id: 'btn_grid_imprimir',
            handler: function() {
                imprimir();
            }
}],
            columns: [[
           { field: 'no_cuenta', title: 'No_Cuenta', width: 100 },
           { field: 'usuario', title: 'Usuario', width: 200 },
	       { field: 'total', title: 'Total', width: 100 },
	       { field: 'fecha_creo', title: 'F.Creo', width: 100 },
	       { field: 'usuario_creo', title: 'U.Creo', width: 200 },
	       { field: 'no_convenio', title: 'No.Convenio', width: 150 },
	       { field: 'nota_cargo_id', title: '', width: 0 }
    ]],
            pageSize: 50,
            pagination: true,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            striped: true,
            nowrap: true,
            showFooter: true
        });

        $('#tabla_listado_nc').datagrid('hideColumn', 'nota_cargo_id');

    }

    //--------------------------------------------------------------------------------------

    function buscarNotasCargos() {
        var no_cuenta_buscar;
        var fecha_inicio;
        var fecha_fin;

        no_cuenta_buscar = $.trim($('#txt_buscar_no_cuenta').val());
        fecha_inicio = $.trim($('#txt_fecha_inicio').val());
        fecha_fin = $.trim($('#txt_fecha_fin').val());

        $('#tabla_listado_nc').datagrid('loadData', { total: 0, rows: [] });

        if ($('#opt_no_cuenta').attr('checked')) {
            if (no_cuenta_buscar.length == 0) {
                $.messager.alert('Japami', 'Tiene que escribir un número de cuenta');
                return;
            }
            fecha_inicio = '';
            fecha_fin = '';
        }
        if ($('#opt_fecha').attr('checked')) {
            if ((fecha_inicio.length == 0) || (fecha_fin.length == 0)) {
                $.messager.alert('Japami', 'Tiene que poner una fecha inicio y final');
                return;
            }
            else {
                if (compare_dates(fecha_inicio, fecha_fin)) {
                    $.messager.alert('Japami', 'La Feha Inicio debe ser menor a la Fecha Fin');
                    return;
                }
            }
            no_cuenta_buscar = '';
        }
        if ($('#opt_todos').attr('checked')) {
            no_cuenta_buscar = '';
            fecha_inicio = '';
            fecha_fin = '';
        }

        $('#tabla_listado_nc').datagrid({ url: '../../Controladores/frm_controlador_nota_cargo.aspx', queryParams: {
            accion: 'listado_nota_cargo',
            no_cuenta: no_cuenta_buscar,
            fecha_inicio: fecha_inicio,
            fecha_fin: fecha_fin

        }, pageNumber: 1
        });



    }

    //-----------------------------------------------------------------------------------------
    function imprimir() {
        var renglon;
        var nota_cargo_id;

        renglon = $('#tabla_listado_nc').datagrid('getSelected');

        if (renglon == null) {
            $.messager.alert('Japami', 'Tiene que seleccionar un elemento');
            return;
        }

        nota_cargo_id = renglon.nota_cargo_id;

        window.open("../../Reporte/frm_controlador_crystal.aspx?nombre_reporte=formato_nota_cargo&nota_cargo_id=" + nota_cargo_id, "NotaCargo", "width=400px,height=400px,scrollbars=NO");
    }

    function compare_dates(fecha, fecha2) {
        var xMonth = fecha.substring(3, 5);
        var xDay = fecha.substring(0, 2);
        var xYear = fecha.substring(6, 10);
        var yMonth = fecha2.substring(3, 5);
        var yDay = fecha2.substring(0, 2);
        var yYear = fecha2.substring(6, 10);

        if (xYear > yYear) {
            return (true)
        }
        else {
            if (xYear == yYear) {
                if (xMonth > yMonth) {
                    return (true)
                }
                else {
                    if (xMonth == yMonth) {
                        if (xDay > yDay)
                            return (true);
                        else
                            return (false);
                    }
                    else
                        return (false);
                }
            }
            else
                return (false);
        }
    }

