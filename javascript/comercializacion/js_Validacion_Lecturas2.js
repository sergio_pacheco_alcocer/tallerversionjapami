﻿$(function(){


});

//----------------------------------------------------------------------------------
//*******************************************************************************
//NOMBRE DE LA FUNCION:    mandarImprimir
//DESCRIPCION:             Funcion que exporta los datos a Excel
//PARAMETROS:              nombre_control: Contiene el nombre del grid en que se va mostar los datos
//                         no_reporte: Numero que indica la consulta que debe realizarse
//                         tipo_consulta: Indica el tipo de reporte a mostrar
//                         titulo: Contiene el texto del titulo del reporte
//CREO:                    Fernando Gonzalez B.
//FECHA_CREO:              04/Septiembre/2012 17:45 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function mandarImprimir(nombre_control,no_reporte,tipo_consulta,titulo){
var renglones;
var Region_ID;
var mes;
var anio;
var fecha = new Date();
var consumo;

renglones=$('#'+nombre_control).datagrid('getRows');
   if(renglones.length==0){
      $.messager.alert('Japami','No hay Elementos que exportar','info');
      return;
    }
        Region_ID = $("#Cmb_Regiones").combobox('getValue');
        mes = fecha.getMonth()-4;
        anio = fecha.getFullYear();
        consumo = $("#Txt_Consumo").numberbox('getValue');
   //url: '../../Controladores/Frm_Controlador_Validacion_Lecturas.aspx?Tipo_Consulta=Grid_Lecturas&Tipo_Reporte=' + reporte + '&Region_ID=' + Region_ID + '&Sector_ID=' + Sector_ID + '&Mes=' + Mes + '&Anio=' + Anio + '&Pagina=' + pagina + '&Cantidad_Renglones=' + cantidadRenglones
   window.location = "../../Reporte/frm_controlador_reportes_lecturas.aspx?Tipo_Consulta=" + tipo_consulta  + "&Tipo_Reporte=" + no_reporte  + "&Region_ID=" + Region_ID + "&Mes=" + mes + "&Anio=" + anio + "&titulo=" + titulo + "&Consumo=" + consumo;

}


//*******************************************************************************
//NOMBRE DE LA FUNCION:    mandarPDF
//DESCRIPCION:             Funcion que exporta los datos a PDF
//PARAMETROS:              nombre_control: Contiene el nombre del grid en que se va mostar los datos
//                         no_reporte: Numero que indica la consulta que debe realizarse
//                         tipo_consulta: Indica el tipo de reporte a mostrar
//                         titulo: Contiene el texto del titulo del reporte
//CREO:                    Fernando Gonzalez B.
//FECHA_CREO:              04/Septiembre/2012 17:45 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function mandarPDF(nombre_control, no_reporte, tipo_consulta, titulo) {
    var renglones;
    var Region_ID;
    var mes;
    var anio;
    var fecha = new Date();
    var consumo;

    Region_ID = $("#Cmb_Regiones").combobox('getValue');
    mes = fecha.getMonth()-4;
    anio = fecha.getFullYear();
    consumo = $("#Txt_Consumo").numberbox('getValue');
    //window.location = "../../Reporte/frm_controlador_reportes_lecturas.aspx?Tipo_Consulta=" + tipo_consulta + "&Tipo_Reporte=" + no_reporte + "&Region_ID=" + Region_ID + "&Sector_ID=" + Sector_ID + "&Mes=" + mes + "&Anio=" + anio + "&titulo=" + titulo;
    window.open("../../Reporte/frm_controlador_reportes_lecturas.aspx?Tipo_Consulta=" + tipo_consulta + "&Tipo_Reporte=" + no_reporte + "&Region_ID=" + Region_ID + "&Mes=" + mes + "&Anio=" + anio + "&titulo=" + titulo + "&Consumo=" + consumo,"ValidacionLecturas","toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600");
}
