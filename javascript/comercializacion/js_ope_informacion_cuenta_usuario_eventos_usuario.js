﻿
$(function(){

  $("#ventana_eventos_usuario").window('close');
  crearTablaUsuariosEventos(); 
   
});
//--------------------------------------------------------------

function crearTablaUsuariosEventos(){


  $('#tabla_eventos').datagrid({  // tabla inicio
            title: 'Eventos Usuarios ',
            width: 615,
            height: 250,
            columns: [[
        { field: 'fecha_evento', title: 'F.Evento', width: 100, sortable: true },
        { field: 'descripcion', title: 'Descripcion', width: 300, sortable: true },
        { field: 'elaboro', title: 'Elaboro', width: 150, sortable: true }
      
    

    ]],     
             onClickRow: function(rowIndex, rowData) {
            
            }, 
            pageSize: 50,
            pagination: true,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            showFooter: false,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap: true

        });  // tabla final


}

//--------------------------------------------------------------------------

function agregarEvento(){
var datos;

  if(valoresRequeridosGeneral("ventana_eventos_usuario")==false){
    $.messager.alert('Japami','Se requieren algunos valores','error');
    return;  
   }
   
   
   datos= toJSON( $("#ventana_eventos_usuario :input").serializeArray());
   $('#ventana_mensaje').window('open');     
       
     $.ajax({ //inicio
             type: "POST",
             url: "../../Controladores/Frm_Controlador_Consulta_Usuario.aspx/guardarEvento",
             data: "{'datos':'" + datos + "'}",
             contentType: "application/json; charset=utf-8",
             dataType: "json",
             success: function(response) {
                 $('#ventana_mensaje').window('close'); 
                 var respuesta = eval("(" + response.d + ")");
                 if (respuesta.mensaje == "bien") {                   
                     procesoExitoso_eventos();
                     
                 } else {
                    $('#ventana_mensaje').window('close');
                     $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                     
                 }

             },
             error: function(result) {
                 $('#ventana_mensaje').window('close');
                 alert("ERROR " + result.status + ' ' + result.statusText);

             }


         });  //fin peticion ajax
   



}


function procesoExitoso_eventos(){
    $.messager.alert('Japami', 'Proceso Exitoso', 'info',function(){
      
       $('#tabla_eventos').datagrid('loadData',{total:0, rows:[]});
       $('#tabla_eventos').datagrid({ url: '../../Controladores/Frm_Controlador_Consulta_Usuario.aspx', queryParams: { accion: 'obtenerEventosUsuarios', nocuenta:$("[id$='txt_no_cuenta_evento']").val()}, pageNumber: 1 });
        
       //$("[id$='txt_fecha_evento']").val('');
       $("[id$='txt_evento']").val('');
       
       
    });
}
