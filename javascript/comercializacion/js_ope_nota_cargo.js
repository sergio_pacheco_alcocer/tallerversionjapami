﻿
var objFooter;
var no_cuenta;
var fecha_inicio;
var mes_actual;
var mes_seleccionado;
var mi_fecha = new Date();
var nota_cargo_id;
var rezagos = 0;
var ultimoRenglon = 0;
var num = 0;
var sum_conceptos;
var iva;
var tasa_iva;
$(function() {
  
    $("[id$='Btn_Salir']").click(function(event) {
        event.preventDefault();
    });


    // disable boton de imprimir nota de cargo
    $('#btn_imprimir_nota_cargo').linkbutton({ disabled: true });


    // ventana mensajes
    $('#ventana_mensaje').window('close');

    // inicializamos fechas
    $("[id$='txt_fecha']").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd/mm/yy'
    });

    $("[id$='txt_fecha_inicio']").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd/mm/yy'
    });
    $("[id$='txt_fecha_fin']").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd/mm/yy'
    });
        
    // creamos tab
    crearTab();

    // creamos tabla de saldo actual
    crearTablaFactura();

    //creamos un evento para el evento blur de la caja de texto pierde el foco

    $('#txt_saldo_confirmado').blur(function() {
        calcularSaldoManual();
    });

    crearTablaCargosAdicionales();

});     // fin principal

//----------------------------------------------------------------------------------

function calcularSaldoManual(){
 var saldo_actual;
 var saldo_nuevo;
 var saldo_confirmado;

 objFooter= $('#tabla_facturas').datagrid('getFooterRows');
 if( objFooter==undefined){
   $('#txt_saldo_nuevo').val('');
   return; 
 }
 
 if(objFooter.total==0){
   $('#txt_saldo_nuevo').val('');
   return; 
 }
 
 saldo_actual= parseFloat( objFooter[3].importe);  
 saldo_confirmado=parseFloat($('#txt_saldo_confirmado').val()); 
 
 
 
 if((saldo_confirmado==0) || isNaN(saldo_confirmado)){
    $('#txt_saldo_nuevo').val('');
    return;
 }

 saldo_nuevo= decimal( saldo_actual+saldo_confirmado,2); 
 
 if(saldo_nuevo==0 || isNaN( saldo_nuevo)){
   $('#txt_saldo_nuevo').val('');
   return;
 }
 
 $('#txt_saldo_nuevo').val(saldo_nuevo); 
  
}


//------------------------------------------------------------------------------------
function crearTab() {
        $('#tab').tabs({
            onSelect: function(title) {
                               
            }
     });
     
 } 
 
 //------------------------------------------------------------------------------------
 
 function crearTablaFactura(){

  //incio tabla
     $('#tabla_facturas').datagrid({
         title: 'Saldo Actual ',
         toolbar: [{
             text: 'Buscar',
             iconCls: 'icon-search',
             id: 'btn_grid_buscar',
             handler: function() {
                 obtenerSaldo();
             }
         }, '-', {
             text: 'Calcular',
             iconCls: 'icon-ok',
             id: 'btn_grid_calcular',
             handler: function() {
                 calcularSaldoExtra();
             }
         }, '-', {
             text: 'Nuevo',
             iconCls: 'icon-reload',
             id: 'btn_grid_nuevo',
             handler: function() {
                 limpiarControles();
                 desbloquearControles();                 
             }

             //              },'-',{
             //                      text:'Quitar Saldo',
             //                      iconCls:'icon-remove',
             //                      id:'bnt_grid_quitar_saldo',
             //                      handler:function(){
             //                       quitarSaldo();
             //                      }                
         }, '-', {
             text: 'Agregar Cargos',
             iconCls: 'icon-add',
             id: 'btn_Agregar_Costo',
             handler: function() {
                 abrirPopup();
             }

}],
             width: 400,
             height: 450,
             columns: [[
           { field: 'no_factura_recibo', title: 'No. Recibo', width: 100 },
	       { field: 'concepto_id', title: 'concepto_id', width: 150 },
	       { field: 'concepto', title: 'Concepto', width: 150 },
	       { field: 'importe', title: 'Importe', width: 100 },
	       { field: 'no_cuenta', title: 'No.cuenta', width: 80 },
	       { field: 'predio_id', title: 'Id.predio', width: 80 },
	       { field: 'nombre', title: 'Nombre', width: 80 },
	       { field: 'periodo_facturacion', title: 'P.Facturacion', width: 100 },
	       { field: 'estado_recibo', title: 'EstadoRecibo', width: 100 },
	       { field: 'pagos_hechos', title: 'Pagos Hechos', width: 100 },
	       { field: 'tasa_iva', title: 'tasa iva', width: 100 },
	       { field: 'total_iva', title: 'total iva', width: 100 },
	       { field: 'total', title: 'total', width: 100 }


    ]],

             pageSize: 50,
             pagination: false,
             rownumbers: true,
             idField: 'concepto',
             remoteSort: false,
             fitColumns: false,
             singleSelect: true,
             striped: true,
             nowrap: true,
             showFooter: true,
             onLoadSuccess: function(data) {
                 num = num + 1;
                 ultimoRenglon = 0;
                 if (data.total > 0) {
                     $('#txt_fecha_ultima').val(data.rows[0].periodo_facturacion);
                     ultimoRenglon = data.rows.length;
                     $('#txt_usuario').val(data.rows[0].nombre);
                     $('#txt_total').val(data.rows[0].total);
                     num = 0;
                     showdiv(false)
                     sum_conceptos = 0;
                     iva = 0;
                     tasa_iva = 0;
                 } else {
                     if (num > 1) {
                         num = 0;
                         $.messager.alert('Japami', 'No exite la cuenta.');
                         return;
                     }
                 }
             }
         });

     // ocultar columna
    //$('#tabla_facturas').datagrid('hideColumn', 'no_factura_recibo');
    $('#tabla_facturas').datagrid('hideColumn', 'concepto_id');
    $('#tabla_facturas').datagrid('hideColumn', 'periodo_facturacion');
    $('#tabla_facturas').datagrid('hideColumn', 'estado_recibo');
    $('#tabla_facturas').datagrid('hideColumn', 'pagos_hechos');
    $('#tabla_facturas').datagrid('hideColumn', 'tasa_iva');
    $('#tabla_facturas').datagrid('hideColumn', 'total_iva');
    $('#tabla_facturas').datagrid('hideColumn', 'total');
    $('#tabla_facturas').datagrid('hideColumn', 'nombre');
    $('#tabla_facturas').datagrid('hideColumn', 'predio_id');
    $('#tabla_facturas').datagrid('hideColumn', 'no_cuenta');
   

}

//--------------------------------------------------------------------------------------------------

function obtenerSaldo() {
    var renglones;
    
   limpiarControles();
   no_cuenta=$('#txt_no_cuenta').val();
   
   $('#tabla_facturas').datagrid('loadData', { total: 0, rows: [] });   
   $('#tabla_facturas').datagrid('reloadFooter', {  rows: [] }); 
   
   if(no_cuenta.length==0){
     $.messager.alert('Japami','Introduce un numero de cuenta');
     return ;
 } 
  $('#tabla_facturas').datagrid({ url: '../../Controladores/frm_controlador_nota_cargo.aspx', queryParams: {
         accion: 'obtener_saldo_actual',
         no_cuenta: no_cuenta
     }, pageNumber: 1
 });
}

//--------------------------------------------------------------------------------------------------

function limpiarControles(){
  $('#txt_fecha_ultima').val('');
  $('#txt_saldo_confirmado').val('');
  $('#txt_saldo_nuevo').val('');  
  $('#txt_saldo_calculado').val('');
  $('#txt_tipo_cuota').val('');
  $('#txt_usuario').val('');
  $('#txt_mensualidades').val('');
  
   $('#tabla_facturas').datagrid('reloadFooter', { total: 0, rows: [] });
   $('#tabla_facturas').datagrid('loadData', { rows: [] });

   sum_conceptos = 0;
   iva = 0;
   tasa_iva = 0;
   num = 0; 
   showdiv(false)
}

//--------------------------------------------------------------------------------------------------

function calcularSaldoExtra(){
var renglones;

 fecha_inicio=$('#txt_fecha').val(); 
 
 
 if (fecha_inicio.length==0){
   $.messager.alert('Japami','Selecciona una fecha para calular el saldo');
   return ;
 }
 
 renglones=$('#tabla_facturas').datagrid('getRows');
 
 if (renglones.length==0){
   $.messager.alert('Japami','Debe haber un Saldo Para esta operación');
   return;
 }
 
 mes_actual=mi_fecha.getMonth()+1;
 mes_seleccionado=parseInt( fecha_inicio.substring(3,5));

if(mes_actual==mes_seleccionado){
   $.messager.alert('Japami','La selección de la fecha para calcular el saldo es incorrecta');
   return;
}

 no_cuenta=renglones[0].no_cuenta;

  $('#ventana_mensaje').window('open'); 

 $.ajax({
       type: "POST",
       url: "../../Controladores/frm_controlador_nota_cargo.aspx/calcularNuevoSaldo",
       data: "{'no_cuenta':'" + no_cuenta + "','fecha':'" + fecha_inicio + "'}",
       contentType: "application/json; charset=utf-8",
       dataType: "json",
       success: function (response) {
           $('#ventana_mensaje').window('close'); 
           var respuesta = eval("(" + response.d + ")");
      
           if( respuesta.mensaje=="bien"){  
              proceso_exitoso(respuesta);
            }else{ proceso_no_existoso(respuesta) };
          
       },
       error: function (result,t) {
           $('#ventana_mensaje').window('close'); 
           $.messager.alert('ERROR ' + result.status + ' ' + result.statusText +' ' + t);
       }
   });  


}
function habilita_mensualidad() {

    if ($('#opt_si').attr('checked')) {
        $('#txt_mensualidades').removeAttr('disabled');
    }
    if ($('#opt_no').attr('checked')) {
        $('#txt_mensualidades').val('');
        $('#txt_mensualidades').attr('disabled', 'disabled');
    }
}

function showdiv(valor) {
    div = document.getElementById('otros_cargos');
    if (valor == true) {div.style.display = "block"}
    if (valor == false) { div.style.display = "none" }
    
}
//--------------------------------------------------------------------------------------------------
function proceso_exitoso(respuesta){
 var saldo_actual;
 var saldo_nuevo;
 var saldo_calculado;
 
 
 
 
 if(respuesta.dato1.length==0){
 $('#txt_saldo_calculado').val(decimal(respuesta.datos_auxiliares,2));
 $('#txt_saldo_confirmado').val(decimal(respuesta.datos_auxiliares,2));
 $('#txt_tipo_cuota').val(respuesta.dato2);  
 objFooter= $('#tabla_facturas').datagrid('getFooterRows');
 saldo_actual= parseFloat( objFooter[3].importe);
 saldo_calculado=parseFloat(respuesta.datos_auxiliares);
 saldo_nuevo= decimal( saldo_calculado+ saldo_actual,2); 
 $('#txt_saldo_nuevo').val(saldo_nuevo);
  }else{
      $.messager.alert('Japami',respuesta.dato1);
  }
  
  
 
}

//--------------------------------------------------------------------------------------------------


function proceso_no_existoso(respuesta){
  $.messager.alert('Japami',respuesta.mensaje);
}


//--------------------------------------------------------------------------------------------------

function guardarNotaCargo(){

var renglones;
var saldo_nuevo;
var saldo_confirmado;
var predio_id;
var no_factura_recibo;
var concepto_rezago;
var sum_importes;
var total_importtes;
var conceptos = new Array();
var cadeConceptos;
var mensualidades;
var periodo_facturacion;

renglones = $('#tabla_facturas').datagrid('getRows');
for (var i = ultimoRenglon; i < renglones.length; i++) {
    renglon = renglones[i];
    conceptos.push({
        no_factura_recibo: renglon.no_factura_recibo,
        concepto_id: renglon.concepto_id,
        concepto: renglon.concepto,
        importe: renglon.importe,
        no_cuenta: renglon.no_cuenta,
        predio_id: renglon.predio_id,
        nombre: renglon.nombre,
        periodo_facturacion: renglon.periodo_facturacion,
        estado_recibo: renglon.estado_recibo,
        pagos_hechos: renglon.pagos_hechos,
        tasa_iva: renglon.tasa_iva,
        total_iva: renglon.total_iva,
        total: renglon.total
    });
}
 
 if (renglones.length==0){
   $.messager.alert('Japami','Debe haber un Saldo Para esta operación');
   return;
 }
 if (div.style.display == "block") {
     if (($('#opt_si').attr('checked')) || ($('#opt_no').attr('checked'))) {
         if ($('#opt_si').attr('checked')) {
             var meses = parseInt($('#txt_mensualidades').val());
            if ((meses < 1) || (isNaN(meses))) {
                $.messager.alert('Japami', 'Debe haber mensualidades');
                return;
            }
        }
     }
     else {
         $.messager.alert('Japami', 'Debe seleccionar una opcion de facturación');
         return;
     }
 }


saldo_nuevo= parseFloat( $('#txt_saldo_nuevo').val());
saldo_confirmado=parseFloat($('#txt_saldo_confirmado').val());


if (saldo_confirmado == 0 || isNaN(saldo_confirmado)) {
    saldo_confirmado = 0;
//  $.messager.alert('Japami','Debe haber un Saldo Para esta operación');
//   return;
}

if (saldo_nuevo == 0 || isNaN(saldo_nuevo)) {
    saldo_nuevo = 0;
//  $.messager.alert('Japami','Debe haber un Saldo Para esta operación');
//   return;
}

no_cuenta = renglones[0].no_cuenta;
predio_id = renglones[0].predio_id;
periodo_facturacion = renglones[0].periodo_facturacion;
no_factura_recibo = renglones[0].no_factura_recibo;
concepto_rezago = tieneElsaldoElconceptodeResagos();
cadeConceptos = toJSON(conceptos);
sum_importes = parseFloat($('#txt_imp_agregados').val());
total_importes = parseFloat($('#txt_total').val());
mensualidades = $('#txt_mensualidades').val();

$('#ventana_mensaje').window('open'); 
 $.ajax({
       type: "POST",
       url: "../../Controladores/frm_controlador_nota_cargo.aspx/guardarNotaCargaAumentoSaldo",
       data: "{'no_cuenta':'" + no_cuenta + "','saldo_nuevo':'" + saldo_nuevo + "','saldo_confirmado':'" + saldo_confirmado + "','total_importes':'" + total_importes + "','suma_imp_agregados':'" + sum_importes + "','no_factura_recibo':'" + no_factura_recibo + "','concepto_rezago':'" + concepto_rezago + "','predio_id':'" + predio_id + "','periodo_facturacion':'" + periodo_facturacion + "','mensualidades':'" + mensualidades + "','iva':'" + iva + "','tasa_iva':'" + tasa_iva + "','otroscargos':'" + cadeConceptos + "'}",
       contentType: "application/json; charset=utf-8",
       dataType: "json",
       success: function (response) {
           $('#ventana_mensaje').window('close'); 
           var respuesta = eval("(" + response.d + ")");
      
           if( respuesta.mensaje=="bien"){  
              proceso_exitoso_nuevo_saldo(respuesta);
            }else{ proceso_no_existoso_nuevo_saldo(respuesta) };
          
       },
       error: function (result,t) {
           $('#ventana_mensaje').window('close'); 
           $.messager.alert('ERROR ' + result.status + ' ' + result.statusText +' ' + t);
       }
   });



}

//-------------------------------------------------------------------------------------------------

function proceso_exitoso_nuevo_saldo(respuesta){
    $.messager.alert('Japami', 'proceso terminado', '', function() {

        $('#tabla_facturas').datagrid({ url: '../../Controladores/frm_controlador_nota_cargo.aspx', queryParams: {
            accion: 'obtener_saldo_actual',
            no_cuenta: no_cuenta
        }, pageNumber: 1
        });
        bloquerControles();
        nota_cargo_id = respuesta.datos_auxiliares;
        mandar_imprimir_diverso(respuesta.dato1);
    });

}
//-------------------------------------------------------------------------------------------------

function proceso_no_existoso_nuevo_saldo(respuesta){
    $.messager.alert('Japami',respuesta.mensaje);
}

//--------------------------------------------------------------------------------------------------

function tieneElsaldoElconceptodeResagos() {
 var respuesta ="NO";
 var renglones; 
 var renglon;
 
 renglones=$('#tabla_facturas').datagrid('getRows');
 
 for(var i=0;i<renglones.length;i++){
    renglon=renglones[i];
    
    if(renglon.concepto=='Rezagos'){
        respuesta="SI";
        rezagos=renglon.importe;
        return respuesta;
    }
 }
  
 return respuesta;
}

//--------------------------------------------------------------------------------------------------
function bloquerControles(){

$('#btn_grid_buscar').linkbutton({disabled:true});
$('#bnt_grid_quitar_saldo').linkbutton({disabled:true});
$('#btn_guardar_aumentar_saldo').linkbutton({disabled:true});
$('#btn_grid_calcular').linkbutton({disabled:true});
$('#btn_imprimir_nota_cargo').linkbutton({disabled:false});
$('#txt_saldo_confirmado').attr('disabled', true);
$('#btn_Agregar_Costo').linkbutton({ disabled: true });
  
}

function desbloquearControles(){
$('#btn_grid_buscar').linkbutton({disabled:false});
$('#btn_guardar_aumentar_saldo').linkbutton({disabled:false});
$('#btn_grid_calcular').linkbutton({disabled:false});
$('#bnt_grid_quitar_saldo').linkbutton({disabled:false});
$('#btn_imprimir_nota_cargo').linkbutton({disabled:true});
$('#txt_saldo_confirmado').attr('disabled', false);
$('#btn_Agregar_Costo').linkbutton({ disabled: false });

}
//--------------------------------------------------------------------------------------------------

function mandar_imprimir_nota_credito(){
  
  window.open("../../Reporte/frm_controlador_crystal.aspx?nombre_reporte=formato_nota_cargo&nota_cargo_id=" + nota_cargo_id,"NotaCargo","width=400px,height=400px,scrollbars=NO");
}
//--------------------------------------------------------------------------------------------------

function mandar_imprimir_diverso(diverso_id) {
    var diverso = diverso_id;
    if (diverso.length>0)
        window.open("../../paginas/Comercializacion/Frm_Ope_Cor_Diversos_Impresion.aspx?&Region_ID=&No_Diverso=" + diverso, "Impresion", "width=100,height=100,left=0,top=0");
}

//--------------------------------------------------------------------------------------------------

function quitarSaldo(){
var renglones;

rezagos=0;
renglones=$('#tabla_facturas').datagrid('getRows');

if (renglones.length==0){
   $.messager.alert('Japami','No hay saldos a quitar');
   return ;
}

if(tieneElsaldoElconceptodeResagos()=='NO'){
   $.messager.alert('Japami','No puede quitar saldo ya que no tiene rezagos');
   return ; 
}

if (rezagos==0){
   $.messager.alert('Japami','No puede quitar saldo ya que no tiene rezagos');
   return ; 
}


$.messager.confirm('Japami', '¿Estas Seguro que deseas Quitar el Saldo?', function(r){
				if (r){
					
					no_cuenta=renglones[0].no_cuenta;
no_factura_recibo=renglones[0].no_factura_recibo;

$('#ventana_mensaje').window('open'); 
 $.ajax({
       type: "POST",
       url: "../../Controladores/frm_controlador_nota_cargo.aspx/quitarSaldo",
       data: "{'no_cuenta':'" + no_cuenta + "','saldo_aquitar':'" + rezagos + "','no_factura_recibo':'" + no_factura_recibo + "'}",
       contentType: "application/json; charset=utf-8",
       dataType: "json",
       success: function (response) {
           $('#ventana_mensaje').window('close'); 
           var respuesta = eval("(" + response.d + ")");
      
           if( respuesta.mensaje=="bien"){  
              proceso_exitoso_quitar_saldo(respuesta);
            }else{ proceso_no_existoso_quitar_saldo(respuesta) };
          
       },
       error: function (result,t) {
           $('#ventana_mensaje').window('close'); 
           $.messager.alert('ERROR ' + result.status + ' ' + result.statusText +' ' + t);
       }
   });  


					
					
				}
			});




}

//--------------------------------------------------------------------------------------------------

function  proceso_exitoso_quitar_saldo(respuesta){
    $.messager.alert('Japami','proceso terminado','',function(){
    
    limpiarControles();
    $('#tabla_facturas').datagrid({ url: '../../Controladores/frm_controlador_nota_cargo.aspx', queryParams: {
         accion: 'obtener_saldo_actual',
         no_cuenta:no_cuenta
     }, pageNumber: 1
     });
   bloquerControles();
   nota_cargo_id=respuesta.datos_auxiliares;
  
  });
}

function proceso_no_existoso_quitar_saldo(respuesta){
  $.messager.alert('Japami',respuesta.mensaje);
}

//--------------------------------------------------------------------------------------------------
Number.prototype.decimal = function(num) {
    pot = Math.pow(10, num);
    return parseInt(this * pot) / pot;
}

function decimal(valor,no_decimales) {
    n = eval(valor);
    return n.decimal(no_decimales);
}


function cerrarVentanas() {
    $("#venta_cargos_adicionales").window('close');
}

function abrirPopup() {
    var renglones;
    renglones = $('#tabla_facturas').datagrid('getRows');

    if (renglones.length == 0) {
        $.messager.alert('Japami', 'No Existe ninguna cuenta para agregar cargos');
        return;
    }
    $('#venta_cargos_adicionales').window('open');
    $('#txt_nombre_concepto').val('');
    $('#txt_cargo').val('');

    $('#tabla_cargos_adicionales').datagrid({ url: '../../Controladores/frm_controlador_nota_cargo.aspx', queryParams: {
    accion: 'Consulta_Cargos'
    }, pageNumber: 1
    });
}

function crearTablaCargosAdicionales() {
    $('#tabla_cargos_adicionales').datagrid({  // tabla inicio
        title: 'Cargos Adicionales ',
        width: 615,
        height: 250,
        columns: [[
        { field: 'concepto', title: 'Concepto', width: 100, sortable: true },
        { field: 'nombre', title: 'Nombre', width: 250, sortable: true },
        { field: 'iva', title: 'IVA', width: 40, sortable: true },
        { field: 'impuesto', title: 'Impuesto', width: 100, sortable: true },
        { field: 'porcentaje_impuesto', title: 'Porcentaje Impuesto', width: 150, sortable: true }



    ]],
        onClickRow: function(rowIndex, rowData) {
            ValorRegonglon();
        },
        pageSize: 50,
        pagination: true,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        showFooter: false,
        striped: true,
        fit: false,
        loadMsg: 'cargando...',
        nowrap: false

    });   // tabla final

        $('#tabla_cargos_adicionales').datagrid('hideColumn', 'concepto');
    }
    function ValorRegonglon() {
        var Renglon;
        Renglon = $('#tabla_cargos_adicionales').datagrid('getSelected');
        $('#txt_nombre_concepto').val(Renglon.nombre);
         
    }

    function AgregarConcepto() {
        var renglones;
        var renglonSeleccionado;        
        renglones = $('#tabla_facturas').datagrid('getRows');
        renglones[0].no_factura_recibo;

        renglonSeleccionado = $('#tabla_cargos_adicionales').datagrid('getSelected');

        var index = $('#tabla_facturas').datagrid('getRowIndex', renglonSeleccionado.nombre);
        if (index >= 0) {
            $.messager.alert('Alerta', 'El concepto ya se ha agregado');
            return;

        }
        
        if ($("#txt_cargo").val() != '' && $('#txt_nombre_concepto').val()!='') {
            $('#tabla_facturas').datagrid('appendRow', {
            
            no_factura_recibo:renglones[0].no_factura_recibo,
            concepto_id: renglonSeleccionado.concepto,
            concepto: renglonSeleccionado.nombre,
            importe: $("#txt_cargo").val(),
            no_cuenta: renglones[0].no_cuenta,
            predio_id: renglones[0].predio_id,
            nombre: renglones[0].nombre,
            periodo_facturacion: renglones[0].periodo_facturacion,
            estado_recibo: renglones[0].estado_recibo,
            pagos_hechos: renglones[0].pagos_hechos,
            tasa_iva: renglonSeleccionado.porcentaje_impuesto,
            total_iva: (renglonSeleccionado.porcentaje_impuesto>0)?(parseFloat($("#txt_cargo").val())*renglonSeleccionado.porcentaje_impuesto)/100:0,
            total: renglones[0].total
        });
        SumaTotal();

        //sum_conceptos = parseFloat($("#txt_imp_agregados").val());
        sum_conceptos = sum_conceptos + parseFloat($("#txt_cargo").val());
        $("#txt_imp_agregados").val(decimal(sum_conceptos, 2));

        iva = iva + (renglonSeleccionado.porcentaje_impuesto > 0) ? (parseFloat($("#txt_cargo").val()) * renglonSeleccionado.porcentaje_impuesto) / 100 : 0;
        tasa_iva = renglonSeleccionado.porcentaje_impuesto
        showdiv(true)
        $('#opt_si').attr('checked', 'checked');
        $('#txt_mensualidades').val('1');
        $("#venta_cargos_adicionales").window('close');
        }
    }

    function SumaTotal() {
        var sumaImporte = 0;
        var sumaIva = 0;
        var sumaPagos = 0;
        var renglones;
        var renglon;
        renglones = $('#tabla_facturas').datagrid('getRows');
        for (var i = 0; i < renglones.length; i++) {
            renglon = renglones[i];
            sumaImporte += parseFloat(renglon.importe);
            sumaIva += parseFloat(renglon.total_iva);
            sumaPagos += parseFloat(renglon.pagos_hechos);
            $('#txt_total').val(decimal(sumaImporte,2));
        }

        $('#tabla_facturas').datagrid('reloadFooter', [
	        { "concepto": "Iva", "importe": sumaIva },
            { "concepto": "Total", "importe": decimal( sumaImporte,2) },
            { "concepto": "Pagos", "importe": sumaPagos },
            { "concepto": "Saldo", "importe": decimal(sumaImporte - sumaPagos,2) }
            ])
        }
    