﻿$(document).ready(function () {
    //Configuracion Inicial
    $('.filtro').val('');
    $('#Txt_Operacion_Caja_ID_Valor').val($("[id$='Txt_Operacion_Caja_ID']").val());
    $('#Txt_Empleado_Valor').val($("[id$='Txt_Empleado']").val());

    //Dar formato a la tabla
    formatoTabla();

    //Llamada a funcion en la cual verifica cual operacion se va a realizar de acuerdo al evento cliqueado
    operacionesClick();

    //Cargar los datos de los bancos registrados en el combo de bancos
    cargarBanco();

    //Autocompletado de sucursales y de sus cajas
    autocompletarSucursales();

    //progress de espera
    progress();

    $('#Txt_Fecha').datepicker({
        showOn: 'button',
        buttonImageOnly: true,
        buttonImage: '../imagenes/paginas/SmallCalendar.gif',
        changeMonth: false,
        changeYear: false,
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
        dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Me', 'Jue', 'Vie', 'Sab'],
        defaultDate: null,
        nextText: 'Sig',
        prevText: 'Ant',
        firstDay: 1,
        altFormat: 'dd/MM/yyyy'
    });
});

//Configuración del modal
function modal() {
    $('.tabs-header').removeAttr('style');
    $('.tabs-header').attr('style', 'width:100%;');
    $('.tabs-panels').removeAttr('style');
    $('.tabs-panels').attr('style', 'width:100%; height:400px;');
    $('.tabs-container').removeAttr('style');
    $('.tabs-container').attr('style', 'width:100%; height:400px;');
    $('.tabs-wrap').removeAttr('style');
    $('.tabs-wrap').attr('style', 'margin-left:0px; left:0px; width:100%;');
    $('.jqmWindow').removeAttr('style');
    $('.jqmWindow').attr('style', 'left:50%; top:50%; width:800px; margin-top:-157.5px; margin-left:-400px;');
}

//Mostrar la informacion del cheque seleccionado
function infoCheque(cheque_id) {

    //Verificar que el id esté correctamente con el formato de 10 digitos
    var longitudCheque = 10 - cheque_id.toString().length,
        formatoCheque = "";

    for (i = 0; i < longitudCheque; i++)
        formatoCheque += "0";

    formatoCheque += cheque_id;

    $('#Txt_Cheque_ID').val(formatoCheque);

    //Serializar los controles de los filtros
    var filtros = $('.filtro'),
        filtros_serializados = filtros.serialize();

    //Realizar la consulta para traer los datos informativos del cheque seleccionado
    $.ajax({
        url: '../../Controladores/Frm_Controlador_Cheques.aspx?accion=filtro&' + filtros_serializados,
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                var info = '';
                $.each(data, function (i, item) {
                    info += '<div id="Div_Informacion"> ' +
                                '<table border="1" cellspacing="0" class="estilo_fuente" width="100%" style="border-bottom-width:thin;"> ' +
                                    '<tr> ' +
                                        '<td class="label_titulo" style="width:100%;" colspan="6">Información No. Cheque ' + item.no_cheque + '</td> ' +
                                    '</tr> ' +
                                    '<tr class="barra_busqueda" style="width:100%;"> ' +
                                        '<td align="right" valign="middle" colspan="6"> ' +
                                            '<img alt="Cerrar" src="../imagenes/paginas/icono_cancelar.png" ' +
                                                 'style="cursor:pointer;" class="Img_Button" title="Cerrar" onclick="cerrarInfoCheque();"/> ' +
                                        '</td> ' +
                                    '</tr> ' +
                                    '<tr>' +
                                        '<td><label><b>Usuario</b></label></td>' +
                                        '<td><label>' + item.nombre + '</label></td>' +
                                    '</tr>' +
                                    '<tr>' +
                                        '<td><label><b>No. Cuenta</b></label></td>' +
                                        '<td><label>' + item.no_cuenta + '</label></td>' +
                                    '</tr>' +
                                    '<tr>' +
                                        '<td><label><b>Sucursal</b></label></td>' +
                                        '<td><label>' + item.sucursal + '</label></td>' +
                                    '</tr>' +
                                    '<tr>' +
                                        '<td><label><b>Caja</b></label></td>' +
                                        '<td><label>' + item.caja + '</label></td>' +
                                    '</tr>' +
                                    '<tr>' +
                                        '<td><label><b>Banco</b></label></td>' +
                                        '<td><label>' + item.banco + '</label></td>' +
                                    '</tr>' +
                                    '<tr>' +
                                        '<td><label><b>Cantidad No.</b></label></td>' +
                                        '<td><label>' + item.monto + '</label></td>' +
                                    '</tr>' +
                                    '<tr>' +
                                        '<td><label><b>Cantidad Letra</b></label></td>' +
                                        '<td><label>' + item.cantidad_letra + '</label></td>' +
                                    '</tr>' +
                                    '<tr>' +
                                        '<td><label><b>Fecha Pago</b></label></td>' +
                                        '<td><label>' + item.fecha_pago + '</label></td>' +
                                    '</tr>' +
                                    '<tr>' +
                                        '<td><label><b>Estatus</b></label></td>' +
                                        '<td><label>' + item.estatus + '</label></td>' +
                                    '</tr>' +
                                '</table>' +
                             '</div>';
                });
                $('#Div_Modal').empty();
                $('#Div_Modal').append(info);
                $('#Div_Modal').jqm();
                modal();
                $('#Div_Modal').jqmShow();
            }
            else {
                alert('No se han encontrado información sobre este cheque');
            }
        }
    });
}

function cerrarInfoCheque() {
    $('#Div_Modal').jqmHide();
}

//Variables utilizadas para la serializacion
var arregloCheques = new Array(),
        arregloEstatus = new Array(),
        contCheque = 0, contEstatus = 0;

//Identificar que Select ha sido cambiado de estatus, añadiendo una clase para su posterior serializacion
function cambioEstatus(combo) {
    arregloCheques[contCheque++] = combo.id;
    arregloEstatus[contEstatus++] = combo.value;
} 

//función para dar el formato a una tabla
function formatoTabla() {
    $('#Div_Tabla').datagrid({
        title: 'Cheques',
        width: 800,
        height: 370,
        columns: [[
                    { field: 'Cheque_ID', title: 'Ver', width: 5,
                        formatter: function (value, rec, index) {
                            return '<img src="../imagenes/paginas/busqueda.png" onclick="infoCheque(' + rec.Cheque_ID + ');" ' +
                                        'alt="Ver" title="Ver" style="cursor:pointer;"/>';
                        }
                    },
                    { field: 'No_Cuenta', title: 'No. Cuenta', width: 250, sortable: true },
                    { field: 'Monto', title: 'Monto', width: 250, sortable: true },
                    { field: 'Banco', title: 'Banco', width: 250, sortable: true },
                    { field: 'Fecha_Pago', title: 'Fecha Pago', width: 250, sortable: true },
                    { field: 'Estatus', title: 'Estatus', width: 260, sortable: true,
                        formatter: function (value, rec, index) {
                            if (rec.Estatus == 'PENDIENTE')
                                return '<select id="' + rec.Cheque_ID + '" name="Estatus" class="sel" disabled="disabled" onchange="cambioEstatus(this);">' +
                                            '<option value="PENDIENTE" selected="selected">PENDIENTE</option>' +
                                            '<option value="COBRADO">COBRADO</option>' +
                                            '<option value="CANCELADO">CANCELADO</option>' +
                                        '</select>';
                            else if (rec.Estatus == 'COBRADO')
                                return '<select id="' + rec.Cheque_ID + '" name="Estatus" disabled="disabled" onchange="cambioEstatus(this);">' +
                                            '<option value="PENDIENTE">PENDIENTE</option>' +
                                            '<option value="COBRADO" selected="selected">COBRADO</option>' +
                                            '<option value="CANCELADO">CANCELADO</option>' +
                                        '</select>';
                            else if (rec.Estatus == 'CANCELADO')
                                return '<select id="' + rec.Cheque_ID + '" name="Estatus" disabled="disabled" onchange="cambioEstatus(this);">' +
                                            '<option value="PENDIENTE">PENDIENTE</option>' +
                                            '<option value="COBRADO">COBRADO</option>' +
                                            '<option value="CANCELADO" selected="selected">CANCELADO</option>' +
                                        '</select>';
                        }
                    }
            ]],
        pagination: true,
        rownumbers: true,
        idField: 'Cheque_ID',
        sortOrder: 'asc',
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        loadMsg: "Cargando, espere ...",
        pageNumber: 1,
        pageSize: 10,
        fitColumns: true,
        striped: true,
        nowrap: false,
        pageList: [10],
        showFooter: true
    });
}

//funcion para traer los datos a la data grid de acuerdo a los filtros realizados, si no hay filtros se traen todos los datos
function datosTabla() {
    var filtros = $('.filtro'),
        filtros_serializados = filtros.serialize();

    $.ajax({ 
        url: '../../Controladores/Frm_Controlador_Cheques.aspx?accion=filtro&' + filtros_serializados,
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $.each(data, function (i, item) {
                    $('#Div_Tabla').datagrid('appendRow', {
                        Cheque_ID: item.cheque_id,
                        No_Cuenta: item.no_cuenta,
                        Monto: item.monto,
                        Banco: item.banco,
                        Fecha_Pago: item.fecha_pago,
                        Estatus: item.estatus
                    });
                });
            }
            else {
                alert('No se han encontrado cheques con estas especificaciones');
            }
        }
    });
}

//Funcion la cual contiene todos los eventos correspondientes a los click en las imagenes
function operacionesClick() {
    $('#Btn_Modificar').click(function () {
        //Verificar que haya registros para poder habilitar esta opcion
        if ($('#Div_Tabla').val() != null) {

            //Verificar si habilitar Modificar o realizar el proceso para guardar los datos
            var estatus = $('#Btn_Modificar').attr('alt');

            if (estatus == 'Modificar') {
                //Variable para saber la cantidad de registros que tiene la datagrid
                var cantFilas = $('#Div_Tabla').datagrid('getRows').length;
                
                //Verificar si hay o no registros para realizar la operacion que le corresponde
                if (cantFilas < 1)
                    alert('No tienes registros que modificar.\nRealiza una búsqueda para poder modificar los registros.');
                else {
                    $('.sel').removeAttr('disabled');
                    $('.sel').attr('enabled', 'enabled');

                    $('#Btn_Modificar').attr({ 'src': '../imagenes/paginas/icono_guardar.png', 'alt': 'Guardar', 'title': 'Guardar' });
                    $('#Btn_Salir').attr({ 'src': '../imagenes/paginas/icono_cancelar.png', 'alt': 'Cancelar', 'title': 'Cancelar' });
                }
            }
            else if (estatus == 'Guardar') {
                //Proceso para modificar el estatus del o los registros 
                $.ajax({
                    url: '../../Controladores/Frm_Controlador_Cheques.aspx?accion=cambios&Cheques=' + arregloCheques + '&Estatus=' + arregloEstatus + '&Usuario=' + $('#Txt_Empleado_Valor').val(),
                    type: 'POST',
                    async: false,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (data != null)
                            alert(data);
                        else
                            alert('Ha ocurrido un error');
                    }
                });
                alert('Se ha guardado la información de los cheques seleccionados');
                $('#Div_Tabla').datagrid('loadData', { total: 0, rows: [] });
                datosTabla();
                $('.filtro').val('');
                $('#Btn_Modificar').attr({ 'src': '../imagenes/paginas/icono_modificar.png', 'alt': 'Modificar', 'title': 'Modificar' });
                $('#Btn_Salir').attr({ 'src': '../imagenes/paginas/icono_salir.png', 'alt': 'Salir', 'title': 'Salir' });
            }
        }
    });

    $('#Btn_Salir').click(function () {
        //Verificar que tipo de operacion se va a realizar
        var estatus = $('#Btn_Salir').attr('alt');

        if (estatus == 'Cancelar') {
            $('#Div_Tabla').datagrid('loadData', { total: 0, rows: [] });

            $('#Btn_Modificar').attr({ 'src': '../imagenes/paginas/icono_modificar.png', 'alt': 'Modificar', 'title': 'Modificar' });
            $('#Btn_Salir').attr({ 'src': '../imagenes/paginas/icono_salir.png', 'alt': 'Salir', 'title': 'Salir' });

            arregloCheques = null;
            arregloEstatus = null;
        }
        else if (estatus == 'Salir') {
            arregloCheques = null;
            arregloEstatus = null;
            setTimeout("location.href='../Paginas_Generales/Frm_Apl_Principal.aspx'", 200);
        }
    });

    $('#Btn_Buscar').click(function () {
        $('#Div_Tabla').datagrid('loadData', { total: 0, rows: [] });
        datosTabla();
        $('.filtro').val('');
    });

    $('#Btn_Borrar').click(function () {
        $('.filtro').val('');
    });
}

//Llenar combo de los bancos
function cargarBanco() {
    var select = $('#Cmb_Banco');
    $('option', select).remove();

    $.ajax({
        url: '../../Controladores/Frm_Controlador_Cheques.aspx?accion=banco',
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            var options = '<option value="" selected="selected">< - SELECCIONE - ></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.banco_id + '">' + item.nombre + '</option>';
                });
                $('#Cmb_Banco').append(options);
            }
        }
    });
}

//Autocompletar las Sucursales
function autocompletarSucursales() {
    $('#Txt_Sucursal_ID').val('');
    //Autocompletado de las Sucursales
    $('#Txt_Sucursal').autocomplete("../../Controladores/Frm_Controlador_Cheques.aspx", {
        extraParams: { accion: 'autocompletar_sucursales' },
        dataType: "json",
        selectOnly: true,
        minChars: 1,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.sucursal_id,
                    result: row.sucursal
                }
            });
        },
        formatItem: function (item) {
            return item.sucursal;
        }
    }).result(function (e, item) {
        var sucursal_id = item.sucursal_id;
        $('#Txt_Sucursal_ID').val(sucursal_id);
        autocompletarCajas(sucursal_id);
    });
}

//Autocompletar las Cajas de una Sucursal
function autocompletarCajas(sucursal_id) {
    $('#Txt_Caja_ID').val('');
    //Autocompletado de las Cajas
    $('#Txt_Caja').autocomplete("../../Controladores/Frm_Controlador_Cheques.aspx", {
        extraParams: { accion: 'autocompletar_cajas', sucursal_id : sucursal_id},
        dataType: "json",
        selectOnly: true,
        minChars: 1,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.caja_id,
                    result: row.caja
                }
            });
        },
        formatItem: function (item) {
            return item.caja;
        }
    }).result(function (e, item) {
        var caja_id = item.caja_id;
        $('#Txt_Caja_ID').val(caja_id);
    });
}

//Progress
function progress() {
    jQuery.ajaxSetup({
        async: false,
        cache: false,
        timeout: (2 * 1000),
        beforeSend: function () {
            MostrarProgress();
        },
        complete: function () {
            OcultarProgress();
        },
        error: function () {
            OcultarProgress();
        }
    });

}

function MostrarProgress() {
    $('[id$=Upgrade]').show();
}

function OcultarProgress() {
    $('[id$=Upgrade]').delay(2000).hide();
}