﻿

$(function(){


crearTablaPrincipal();
crearTablaDetallesModificado();
crearTablaDetallesActual();

});

//---------------------------------------------------------------------------------------------------

function crearTablaPrincipal(){


  $('#tabla_principal').datagrid({  // tabla inicio
            title: 'Historial Modificaciones',
            width: 800,
            height: 200,
            columns: [[
        { field: 'codigo', title: 'Codigo', width: 170, sortable: true },
        { field: 'total_pagar_actual', title: 'Pago Actual', width:150, sortable: true },
        { field: 'total_pagar_anterior', title: 'Pago Anterior', width: 150, sortable: true },
        { field: 'fecha_modificacion', title: 'F.Modificaci&oacute;n', width: 150, sortable: true },
        { field: 'usuario_modifico', title: 'U.Modifico', width: 200, sortable: true },
        { field: 'saldo', title: 'S.Actual', width: 150, sortable: true },
        { field: 'id_factura_recibo_historial', title: 'id', width: 10, sortable: true },
        { field: 'no_factura_recibo', title: 'no', width: 10, sortable: true }
        

    ]],     
             onClickRow: function(rowIndex, rowData) {
            
                   $('#tabla_conceptos_modificados').datagrid('loadData',{total:0 , rows:[]});                                                                
                   $('#tabla_conceptos_modificados').datagrid({ url: '../../Controladores/Frm_Controlador_Consulta_Usuario.aspx', queryParams: { accion: 'obtenerDetalleconceptosFacturacionHistorial', id_factura_recibo_historial:rowData.id_factura_recibo_historial}, pageNumber: 1 });
                   
                   $('#tabla_conceptos_actual').datagrid('loadData',{total:0 , rows:[]});
                   $('#tabla_conceptos_actual').datagrid({ url: '../../Controladores/Frm_Controlador_Consulta_Usuario.aspx', queryParams: { accion: 'obtenerDetallesConceptosFacturacionActual', no_factura_recibo:rowData.no_factura_recibo}, pageNumber: 1 })
                        
            
            }, 
            pageSize: 50,
            pagination: true,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            showFooter: false,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap:true

        });  // tabla final


    $('#tabla_principal').datagrid('hideColumn','id_factura_recibo_historial');
    $('#tabla_principal').datagrid('hideColumn','no_factura_recibo');
     

}


//------------------------------------------

function crearTablaDetallesModificado(){ 

 $('#tabla_conceptos_modificados').datagrid({  // tabla inicio
        title: 'Conceptos de Facturaci&oacute;n Modificados',
        width: 800,
        height: 150,
        columns: [[
        { field: 'concepto', title:'Concepto', width:150 },
        { field: 'importe', title: 'Importe', width: 120 },
        { field: 'impuesto', title: 'Impuesto', width: 120 },
        { field: 'total', title: 'Total', width: 120 },  
        { field: 'total_abonado', title: 'T.Abonado', width: 120},
        { field: 'total_saldo', title: 'T.Saldo', width: 120},
        { field: 'cantidad_iva', title: '%Iva', width: 80 },
        { field: 'lleva_iva', title: 'Lleva Iva', width: 80 }
       
                   
    ]],
       onClickRow: function(rowIndex, rowData) {
           
        
      
        },
    
        pageSize: 10,
        pageList: [30],
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        striped: true,
        showFooter:true,
        loadMsg: 'cargando...',
        nowrap: false
   

    });          // tabla final
    
    
  }  
  //--------------------------------------------------------
  
  
  function crearTablaDetallesActual(){
      $('#tabla_conceptos_actual').datagrid({  // tabla inicio
        title: 'Conceptos de Facturaci&oacute;n Actual',
        width: 800,
        height: 150,
        columns: [[
        { field: 'concepto', title:'Concepto', width:150 },
        { field: 'importe', title: 'Importe', width: 120 },
        { field: 'impuesto', title: 'Impuesto', width: 120 },
        { field: 'total', title: 'Total', width: 120 },  
        { field: 'total_abonado', title: 'T.Abonado', width: 120},
        { field: 'total_saldo', title: 'T.Saldo', width: 120},
        { field: 'cantidad_iva', title: '%Iva', width: 80 },
        { field: 'lleva_iva', title: 'Lleva Iva', width: 80 }
       
                   
    ]],
       onClickRow: function(rowIndex, rowData) {
           
        
      
        },
    
        pageSize: 10,
        pageList: [30],
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        striped: true,
        showFooter:true,
        loadMsg: 'cargando...',
        nowrap: false
   

    });          // tabla final
  
  
  }
  
  
 //--------------------------------------------------------