﻿
$(function() { // inicio


  //cierra las ventans del popup
$('#ventana_colonias').window('close');
   crearGridColoniasBuscar();
   crearGridCallesBuscar();

});   // fin


//--------------------------------------

function crearGridColoniasBuscar() {

    $('#grid_colonias_buscar').datagrid({
        title: 'Colonias',
        width: 720,
        height: 160,
        columns: [[
        { field: 'colonia_id', title: 'colonia_id', width: 100, sortable: true },
        { field: 'nombre', title: 'Colonias', width: 200, sortable: true }
    ]],
        pageSize: 50,
        remoteSort: false,
        pagination: true,
        rownumbers: true,
        fitColumns: true,
        singleSelect: true,
        loadMsg: 'cargando...',
        nowrap: false,
        onClickRow: function(rowIndex, rowData) {

        $('#grid_calles_buscar').datagrid({ url: '../../Controladores/frm_controlador_programacion_lectura.aspx', queryParams: {
            accion: 'buscar_colonia_calle',
            COLONIA_ID: rowData.colonia_id
        }, pageNumber: 1
        });
          
        }
    });

        // ocultar una columna
        $('#grid_colonias_buscar').datagrid('hideColumn', "colonia_id");

}

//--------------------------------------
function crearGridCallesBuscar() {
    $('#grid_calles_buscar').datagrid({
        title: 'Calles',
        width: 720,
        height: 160,
        columns: [[
        { field:'ck',checkbox:true},
        { field: 'calle_colonia_id', title: 'calle_colonia_id', width: 1, sortable: true },
        { field: 'nombre_calle', title: 'Calle', width: 200, sortable: true },
        { field: 'nombre_colonia', title: 'Colonia', width: 200, sortable: true }
    ]],
        pageSize: 50,
        pagination: false,
        remoteSort: false,
        rownumbers: true,
        fitColumns: true,
        singleSelect: false,
        loadMsg: 'cargando...',
        nowrap: false
    });

    // ocultar una columna
     $('#grid_calles_buscar').datagrid('hideColumn', "nombre_colonia");
     $('#grid_calles_buscar').datagrid('hideColumn', "calle_colonia_id");

}

//--------------------------------------
function cerraVentana() { $('#ventana_colonias').window('close'); }
//--------------------------------------

function buscarColonia() {

    var nombre = $("[id$='txt_colonia_buscar']");
   
    if ($.trim(nombre.val()).length == 0) {
        $.messager.alert('Manzanas', 'Debes escribir un nombre de una colonia', '', function() { $(nombre).focus(); });
        return;
    }
    

   $('#grid_colonias_buscar').datagrid('loadData', { total: 0, rows: [] });
   $('#grid_calles_buscar').datagrid('loadData', { total: 0, rows: [] });

   $('#grid_colonias_buscar').datagrid({ url: '../../Controladores/frm_controlador_programacion_lectura.aspx', queryParams: {
        accion: 'buscar_colonia_para_manzana',
        nombre: $.trim(nombre.val())
    }, pageNumber: 1
    });

}

//--------------------------------------
function agregar_colonia_manzana() {

    var renglon_colonias;

    renglon_colonias = $('#grid_colonias_buscar').datagrid('getSelected');

    if (renglon_colonias == null) {
        $.messager.alert('Manzanas', 'Debes Seleccionar una Colonia');
        return;
    }
              
    var rows = $('#grid_calles_buscar').datagrid('getSelections');
     
    if (rows.length==0){
       $.messager.alert('Manzana','Debes Seleccionar al menos una calle'); 
       return ;
    } 
   
   
   for(var i=0;i<rows.length;i++){
   
      if (estaRepetidaLaColonia(rows[i].calle_colonia_id) == false) {

        $('#grid_colonias').datagrid('appendRow', {
           P_Nombre_Colonia: rows[i].nombre_colonia,
           P_Calle_Colonia_ID:rows[i].calle_colonia_id,
           P_Nombre_Calle: rows[i].nombre_calle
        });   
    } 
   
   
   }
   
    $('#grid_calles_buscar').datagrid('clearSelections');    
    $.messager.alert('Manzanas', 'Elemento Agregado');
   

}

//--------------------------------------
function estaRepetidaLaColonia(calle_colonia_id) {
    var renglones;
    var row;
    var respuesta;
    respuesta = false;

    calle_colonia_id = $.trim(calle_colonia_id);
    renglones = $('#grid_colonias').datagrid('getRows');
    if (renglones != null) {
        for (var i = 0; i < renglones.length; i++) {
            row = renglones[i];
            if (calle_colonia_id == $.trim(row.P_Calle_Colonia_ID)) {
                return true;
            }
        }

    }

    return respuesta;

}

//--------------------------------------


