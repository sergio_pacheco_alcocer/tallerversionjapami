﻿///*******************************************************************************
///NOMBRE DE LA FUNCIÓN : $(document).ready()
///DESCRIPCIÓN          : Funcion que indica cuando el documento ha sido cargado, para poder modificar los elementod DOM
///PARAMETROS           :   
///CREO                 : Yañez Rodriguez Diego
///FECHA_CREO           : 00/Marzo/2011 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///*******************************************************************************
$(document).ready(function () {
    //Configuracion_Inicial();
    
    // obtenemos los establecimientos
    Cargar_Establecimientos(0); 
    
    
    // evento click del combo
      $('#Cmb_Establecimientos').change(function (event) {
        $('#Lbl_Formato_Columnas').text('');
        
        var establecimiento = $("#Cmb_Establecimientos option:selected").val();
        
        if (establecimiento != 'Seleccione') {
            //$('#Cmb_Establecimientos').removeAttr('style');
            //$('#Cmb_Establecimientos').attr('style', 'width:300px;');
            var detalles = $('#Cmb_Establecimientos option:selected').attr('detalles');
            $('#Lbl_Formato_Columnas').text(detalles);
        }
        else {
            //$('#Cmb_Establecimientos').removeAttr('style');
            //$('#Cmb_Establecimientos').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
        }
    });
    
    
});
//Prepara la interfaz de usuario con los eventos de botones y comportamiento de la pagina
function Configuracion_Inicial() {
    jQuery.ajaxSetup({
        async: false,
        cache: false,
        timeout: (2 * 1000),
        beforeSend: function () {
            MostrarProgress();
        },
        complete: function () {
            OcultarProgress();
        },
        error: function () {
            OcultarProgress();
        }
    });
    $('#Cmb_Establecimientos').change(function (event) {
        $('#Lbl_Formato_Columnas').text('');
        var establecimiento = $("#Cmb_Establecimientos option:selected").val();
        if (establecimiento != 'Seleccione') {
            $('#Cmb_Establecimientos').removeAttr('style');
            $('#Cmb_Establecimientos').attr('style', 'width:98%;');
            var detalles = $('#Cmb_Establecimientos option:selected').attr('detalles');
            $('#Lbl_Formato_Columnas').text(detalles);
        }
        else {
            $('#Cmb_Establecimientos').removeAttr('style');
            $('#Cmb_Establecimientos').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
        }
    });
    $('#Cmb_Hojas').change(function (event) {
        var hoja = $("#Cmb_Hojas option:selected").val();
        if (hoja != 'Seleccione') {
            $('#Cmb_Hojas').removeAttr('style');
            $('#Cmb_Hojas').attr('style', 'width:98%;');
        }
        else {
            $('#Cmb_Hojas').removeAttr('style');
            $('#Cmb_Hojas').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
        }
    });
    $('#Div_Pestanias').tabs();
    Cargar_Establecimientos();
    Configurar_Botones_Cajas_Pagos_Establecimientos("Inicial");
    $('#Btn_Nuevo').click(function (event) {
        event.preventDefault();
        Btn_Nuevo_Click();
    });
    $('#Btn_Salir').click(function (event) {
        event.preventDefault();
        Btn_Salir_Click();
    });
    $('#Btn_Importar').click(function (event) {
        event.preventDefault();
        Btn_Importar_Click();
    });
}

//Inicio Eventos Solicitudes -----------------------------------------------------------------------------------------------------------------
//inicializa los controles para ABC
function Btn_Nuevo_Click() {
    $("#Lbl_Mensaje_Error").text('');
    $("#Lbl_Mensaje_Error").hide();
    $("#Img_Error").hide();

    var Btn_Nuevo = $("#Btn_Nuevo");
    //Verificar el tooltip del boton
    if (Btn_Nuevo.attr('alt') == "Nuevo") {
        //Habilitar y limpiar
        Configurar_Botones_Cajas_Pagos_Establecimientos("Nuevo");
        Limpiar_Controles_Pagos();
    }
    else {
        Alta_Pagos();
    }
}

function Alta_Pagos() 
{
    //Asignación de los valores reelevantes de la caja Operacion_Caja_ID y Empleado_ID
    $('#Txt_Operacion_Caja_ID_Valor').val($("[id$='Txt_Operacion_Caja_ID']").val());
    $('#Txt_Empleado_Valor').val($("[id$='Txt_Empleado']").val());

    //Variables del archivo seleccionado
    var nombreArchivo = $('#Txt_Archivo_ID').val();
    var establecimiento = $('#Cmb_Establecimientos option:selected').val();
    var hoja = $('#Cmb_Hojas option:selected').val();
    //recorremos el serializarray para formar la cadena de los controles encontrados
    var enviar = "Operacion_Caja_ID=" + $('#Txt_Operacion_Caja_ID_Valor').val() +
                 "&Usuario=" + $('#Txt_Empleado_Valor').val() +
                 "&Recibos_Cobrar=" + cantidadRecibos + "&Total_Recibos=" + totalRecibos + 
                 "&Archivo=" + nombreArchivo + "&Establecimiento_ID=" + establecimiento +
                 "&Hoja_Archivo=" + hoja + "&Columnas=" + arregloNombreColumnas;

    //redireccionamos a la pagina y pasamos como parametro la cadena, para controlar la informacion
    $.ajax({ url: "../../Controladores/Frm_Controlador_Cajas_Pagos_Establecimientos.aspx?Accion=CobroEstablecimiento&" + enviar,
        type: "POST",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        success: function (data) {
            Configurar_Botones_Cajas_Pagos_Establecimientos('Inicial');
            alert(data);
        }
    });
}

function Btn_Importar_Click() {
    $("#Lbl_Mensaje_Error").text('');
    $("#Lbl_Mensaje_Error").show();
    $("#Img_Error").hide();
    var mensaje = Validar_Informacion_Importacion();
    if (mensaje != "") {
        $('#Img_Error').show();
        $('#Lbl_Mensaje_Error').text(mensaje);
        return;
    }
    var nombreArchivo = $('#Txt_Archivo_ID').val();
    var establecimiento = $('#Cmb_Establecimientos option:selected').val();
    var hoja = $('#Cmb_Hojas option:selected').val();
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Cajas_Pagos_Establecimientos.aspx?Accion=ejecutar_importacion" +
            "&Archivo=" + nombreArchivo +
            "&Establecimiento_ID=" + establecimiento +
            "&Hoja_Archivo=" + hoja,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            //revisa la accion a realizar
            var accion = data.Resultado[1].accion;
            var error = data.Resultado[2].error;
            switch (accion) {
                case "Hojas": //El archivo tiene varias hojas y las despliega para seleccion
                    $('#Lbl_Mensaje_Error').text(error);
                    $("#Img_Error").show();
                    var select = $('#Cmb_Hojas');
                    $('option', select).remove();
                    var options = '<option value="Seleccione"><-SELECCIONE-></option>';
                    $.each(data.Hojas, function (i, item) {
                        options += '<option value="' + item.Hoja + '">' + item.Hoja + '</option>';
                    });
                    $('#Cmb_Hojas').append(options);
                    $('#Cmb_Hojas').show();
                    break;
                case "Formato":
                    $('#Lbl_Mensaje_Error').text(error);
                    $("#Img_Error").show();
                    break;
                case "Pagos":
                    $('#Lbl_Mensaje_Error').text(error);
                    //Genera las tablas de los pagos por aplicar
                    Genera_Tabla_Pagos(data.Encabezado, data.Pagos);
                    Genera_Tabla_Pagos_Aplicados(data.Encabezado, data.Pagos_Aplicados);
                    Genera_Tabla_Pagos_Error(data.Encabezado, data.Pagos_Error);
                    break;
                default: break;
            }
        }
    });
    
}
function Btn_Salir_Click() {
    var estatus = $("#Btn_Salir").attr('alt')
    Limpiar_Controles_Pagos();
    if (estatus == "Salir") {
        window.location = '../Paginas_Generales/Frm_Apl_Principal.aspx';
    }
    else {
        Configurar_Botones_Cajas_Pagos_Establecimientos("Inicial");
    }
}
function Configurar_Botones_Cajas_Pagos_Establecimientos(modo) {
    try {
        switch (modo) {
            case "Inicial":
                $('#Btn_Nuevo').removeAttr('disabled');
                $('#Btn_Nuevo').attr('src', '../imagenes/paginas/icono_nuevo.png');
                $('#Btn_Modificar').attr('src', '../imagenes/paginas/icono_modificar.png');
                $('#Btn_Salir').attr('src', '../imagenes/paginas/icono_salir.png');
                $('#Btn_Nuevo').attr('alt', 'Nuevo');
                $('#Btn_Modificar').attr('alt', 'Modificar');
                $('#Btn_Salir').attr('alt', 'Salir');
                $('#Lbl_Mensaje_Error').text('');
                $('#Img_Error').hide();
                //bloquea los controles
                $('.altaPagos').attr('disabled', 'disabled');
                $('#ctl00_Cph_Area_Trabajo1_AFU_Archivo_Establecimientos_ctl02').attr('disabled', 'disabled');
                $('#Lbl_Hojas').text('');
                $('#Cmb_Hojas').hide();
                Limpiar_Controles_Pagos();
                $('#Div_Pagos_Cabecera').empty();
                $('#Div_Pagos_Realizar').empty();
                $('#Div_Pagos_Totales').empty();
                $('#Div_Pagos_Aplicados_Cabecera').empty();
                $('#Div_Pagos_Aplicados').empty();
                $('#Div_Pagos_Aplicados_Totales').empty();
                $('#Div_Pagos_Error_Cabecera').empty();
                $('#Div_Pagos_Error').empty();
                $('#Div_Pagos_Error_Totales').empty();
                
                //Configuracion_Acceso("Frm_Cat_Cor_Medidores.aspx");
                break;

            case "Nuevo":
                $('#Btn_Nuevo').removeAttr('disabled');
                $('#Btn_Nuevo').attr('src', '../imagenes/paginas/icono_guardar.png');
                $('#Btn_Nuevo').attr('alt', 'Guardar');
                $('#Btn_Modificar').attr('src', '../imagenes/paginas/icono_modificar_deshabilitado.png');
                $('#Btn_Salir').attr('src', '../imagenes/paginas/icono_cancelar.png');
                $('#Btn_Salir').attr('alt', 'Cancelar');
                $('.altaPagos').removeAttr('disabled');
                $("[id$='Pnl_Pagos]']").removeAttr('disabled');
                $('#ctl00_Cph_Area_Trabajo1_AFU_Archivo_Establecimientos_ctl02').removeAttr('disabled');

                break;

            default: break;
        }

    }
    catch (ex) {
        //Error
        $("#Lbl_Mensaje_Error").removeAttr('style');
        $("#Lbl_Mensaje_Error").attr('style', 'color: rgb(153, 0, 0); display: inline;');
        $("#Lbl_Mensaje_Error").val(ex.Description);
        $("#Img_Error").show();
    }
    finally {
    }
}
function Limpiar_Controles_Pagos() {
    $('#ctl00_Cph_Area_Trabajo1_AFU_Archivo_Establecimientos_ctl02').val('');
    $('#Cmb_Establecimientos').val('Seleccione');
    $('#LblMensajeCarga').text('');
    $('#Txt_Archivo_ID').val('');
}
//Se valida que un archivo se haya subido y se ha seleccionado el establecimiento
function Validar_Informacion_Importacion() {
    var establecimiento = $('#Cmb_Establecimientos option:selected').val();
    var documento = $('#Txt_Archivo_ID').val();
    var hoja = $('#Cmb_Hojas option:selected').val();
    var mensaje = "";

    if (establecimiento == "Seleccione") {
        mensaje += "Seleccione el establecimiento,";
    }
    if (documento == "") {
        mensaje += "Seleccione el archivo que contiene los pagos";
    }
    if ($('#Cmb_Hojas').is(':visible')) {
        if (hoja == "Seleccione") {
            mensaje += "Seleccione la hoja del archivo que contiene los pagos a registrar";
        }
    }
    return mensaje;
}
//Carga los establecimientos en el combo correspondiente
function Cargar_Establecimientos(colonia_id) {
    var select = $('#Cmb_Establecimientos');
    $('option', select).remove();

    $.ajax({
        url: "../../Controladores/Frm_Controlador_Cajas_Pagos_Establecimientos.aspx?Accion=consulta_establecimientos",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.establecimiento_id + '" detalles="' + item.Cadena_Detalles + '">' + item.establecimiento + '</option>';
                });
                $('#Cmb_Establecimientos').append(options);
            }
        }
    });
}

//VARIABLES USADAS PARA EL PAGO MASIVO DE LOS RECIBOS
var totalRecibos = 0, cantidadRecibos = 0, arregloNombreColumnas = new Array(), cont = 0;

// Carga los registros de los pagos
function Genera_Tabla_Pagos(Encabezado, Pagos) {
    //Limpia las tablas PAgos
    $('#Div_Pagos_Cabecera').empty();
    $('#Div_Pagos_Realizar').empty();
    $('#Div_Pagos_Totales').empty();

    //Genera la tabla con los registros del archivo
    var Registro_Pago = "";
    var Encabezado_Tabla = "";
    var Pie_Tabla = "";
    var Total_Registros = 0;
    var Total_Cobro = 0;
    //Encabezado
    Encabezado_Tabla = '<table id="Grid_Recibos_Cabecera" border="0" cellspacing="0" class="GridView_1"' +
                     ' style="width:100%;border-collapse:collapse;white-space:normal">' +
                     '<thead> ' +
                     '  <tr class="GridHeader">';
    $.each(Encabezado, function (i, item) {
//        if (item.Cabecera.toUpperCase() == "NO CUENTA" || item.Cabecera.toUpperCase() == "FECHA PAGO" || item.Cabecera.toUpperCase() == "MONTO" || item.Cabecera.toUpperCase() == "CODIGO" || item.Cabecera.toUpperCase() == "SALDO") {
//            Encabezado_Tabla += '<th align="right" style="width:20%;">' + item.Cabecera + '</th>';
//        }
//        else {
//            Encabezado_Tabla += '<th align="center" style="width:20%;">' + item.Cabecera + '</th>';
//        }
        Encabezado_Tabla += '<th align="left" style="width:15%;">' + item.Cabecera + '</th>';  
        arregloNombreColumnas[cont] = item.Cabecera;
        cont++;
    });
    Encabezado_Tabla += '     </tr>'+
                        '</thead>';
    Pie_Tabla = '</table>';
    $('#Div_Pagos_Cabecera').append(Encabezado_Tabla + Pie_Tabla);
    //
    //Muestra los registros a pagar
    var registro = 0;
    Registro_Pago = '<table id="Grid_Recibos" border="0" cellspacing="0" class="GridView_1"' +
                     ' style="width:100%;border-collapse:collapse;white-space:normal">';
    Registro_Pago += '<tbody style="height:250px; overflow:scroll;">';
    $.each(Pagos, function (i, item) {
        registro++;
        if ((registro % 2) != 0) {
            clase = "GridItem";
        }
        else {
            clase = "GridAltItem";
        }
        Registro_Pago += '<tr class="' + clase + '">';
        //Recorre el arreglo del registro de pago para formar la fila
        $.each(item, function (key, item2) {
            var valor = item2;
            var clase = "GridItem";
            //define la alineacion
            var alineacion = "";

            if (key.toUpperCase() == "NO CUENTA" || key.toUpperCase() == "FECHA PAGO" || key.toUpperCase() == "MONTO" || key.toUpperCase() == "CODIGO_BARRAS" || key.toUpperCase() == "SALDO") {
                alineacion = "right";
                if (key.toUpperCase() == "MONTO") {
                    valor = Number(valor).toFixed(2);
                    Total_Cobro += Number(valor);
                }
            }
            else {
                alineacion = "left";
                
            }

            //Asignación de los valores de cada registro que se utilizarán para serializarse
            if (key.toUpperCase() == "MONTO") { totalRecibos += Number(valor); cantidadRecibos++; }

            //Registro_Pago += '<td align="' + alineacion + '" style="width:20%;"><label class="">' + valor + '</label></td>';
            Registro_Pago += '<td align="left" style="width:20%;"><label class="">' + valor + '</label></td>';
        });
    });
    Registro_Pago += '</tbody>';
    //Pie de la tabla de pagos a realizar
    $('#Div_Pagos_Realizar').append(Registro_Pago + Pie_Tabla);
    var Totales = '<table Width="100%">' +
                    '<tr class="GridHeader">' +
                        '<td align="Right">' +
                            'Total Registros: <label style:"text-align:right;width:30px">' + registro + '</label>' +
                            '&nbsp;&nbsp;&nbsp;&nbsp;' +
                            'Total Pagos: <label style:"text-align:right;width:30px">' + Total_Cobro.toFixed(2) + '</label>' +
                        '</td>' +
                    '</tr>' +
                  '</table>';
    $('#Div_Pagos_Totales').append(Totales);
    
    return;
}

//Carga los registros que ya han sido aplicados con el archivo seleccionado
function Genera_Tabla_Pagos_Aplicados(Encabezado, Pagos_Aplicados) {
    //Limpia las Tabla de Pagos Aplicados
    $('#Div_Pagos_Aplicados_Cabecera').empty();
    $('#Div_Pagos_Aplicados').empty();
    $('#Div_Pagos_Aplicados_Totales').empty();
    //Genera la tabla con los registros del archivo
    var Registro_Pago_Aplicados = "";
    var Encabezado_Tabla = "";
    var Pie_Tabla = "";
    var Total_Registros = 0;
    var Total_Cobro = 0;
    //Encabezado
    Encabezado_Tabla = '<table id="Grid_Recibos_Pagados_Cabecera" border="0" cellspacing="0" class="GridView_1"' +
                     ' style="width:100%;border-collapse:collapse;white-space:normal">' +
                     '<thead> ' +
                     '  <tr class="GridHeader">';
    $.each(Encabezado, function (i, item) {
//        if (item.Cabecera.toUpperCase() == "NO CUENTA" || item.Cabecera.toUpperCase() == "FECHA PAGO" || item.Cabecera.toUpperCase() == "MONTO") {
//            Encabezado_Tabla += '<th align="right" style="width:20%;">' + item.Cabecera + '</th>';
//        }
//        else {
//            Encabezado_Tabla += '<th align="center" style="width:20%;">' + item.Cabecera + '</th>';
//        }
       Encabezado_Tabla += '<th align="left" style="width:15%;">' + item.Cabecera + '</th>'; 
    });
    Encabezado_Tabla += '     </tr>' +
                        '</thead>';
    Pie_Tabla = '</table>';
    $('#Div_Pagos_Aplicados_Cabecera').append(Encabezado_Tabla + Pie_Tabla);
    //Contenido de Pagos
    //Muestra los registros a pagar
    var registro = 0;
    Registro_Pago_Aplicados = '<table id="Grid_Recibos_Pagados" border="0" cellspacing="0" class="GridView_1"' +
                     ' style="width:100%;border-collapse:collapse;white-space:normal">';
    Registro_Pago_Aplicados += '<tbody style="height:250px; overflow:scroll;">';
    $.each(Pagos_Aplicados, function (i, item) {
        registro++;
        if ((registro % 2) != 0) {
            clase = "GridItem";
        }
        else {
            clase = "GridAltItem";
        }
        Registro_Pago_Aplicados += '<tr class="' + clase + '">';
        //Recorre el arreglo del registro de pago para formar la fila
        $.each(item, function (key, item2) {
            var valor = item2;
            var clase = "GridItem";
            //define la alineacion
            var alineacion = "";

            if (key.toUpperCase() == "NO CUENTA" || key.toUpperCase() == "FECHA PAGO" || key.toUpperCase() == "MONTO") {
                alineacion = "right";
                if (key.toUpperCase() == "MONTO") {
                    valor = Number(valor).toFixed(2);
                    Total_Cobro += Number(valor);
                }
            }
            else {
                alineacion = "left";
            }

            //Registro_Pago_Aplicados += '<td align="' + alineacion + '" style="width:20%;"><label class="">' + valor + '</label></td>';
            Registro_Pago_Aplicados += '<td align="left" style="width:15%;"><label class="">' + valor + '</label></td>';
        });
    });
    Registro_Pago_Aplicados += '</tbody>';
    //Pie de la tabla de pagos a realizar
    $('#Div_Pagos_Aplicados').append(Registro_Pago_Aplicados + Pie_Tabla);
    //Pie de la tabla de pagos a realizar
    var Totales_Aplicados = '<table Width="100%">' +
                    '<tr class="GridHeader">' +
                        '<td align="Right">' +
                            'Total Registros: <label style:"text-align:right;width:30px">' + registro + '</label>' +
                            '&nbsp;&nbsp;&nbsp;&nbsp;' +
                            'Total Pagos Aplicados: <label style:"text-align:right;width:30px">' + Total_Cobro.toFixed(2) + '</label>' +
                        '</td>' +
                    '</tr>' +
                  '</table>';
    $('#Div_Pagos_Aplicados_Totales').append(Totales_Aplicados);
    return;
}
//Carga los registros que no se pudo leer la informacion de la cuenta o no existe el no de cuenta
function Genera_Tabla_Pagos_Error(Encabezado, Pagos_Error) {
    //Limpia las Tabla de Pagos Aplicados
    $('#Div_Pagos_Error_Cabecera').empty();
    $('#Div_Pagos_Error').empty();
    $('#Div_Pagos_Error_Totales').empty();
    //Genera la tabla con los registros del archivo
    var Registro_Pago_Error = "";
    var Encabezado_Tabla = "";
    var Pie_Tabla = "";
    var Total_Registros = 0;
    var Total_Cobro = 0;
    //Encabezado
    Encabezado_Tabla = '<table id="Grid_Recibos_Error_Cabecera" border="0" cellspacing="0" class="GridView_1"' +
                     ' style="width:100%;border-collapse:collapse;white-space:normal">' +
                     '<thead> ' +
                     '  <tr class="GridHeader">';
    $.each(Encabezado, function (i, item) {
//        if (item.Cabecera.toUpperCase() == "NO CUENTA" || item.Cabecera.toUpperCase() == "FECHA PAGO" || item.Cabecera.toUpperCase() == "MONTO") {
//            Encabezado_Tabla += '<th align="right" style="width:20%;">' + item.Cabecera + '</th>';
//        }
//        else {
//            Encabezado_Tabla += '<th align="center" style="width:20%;">' + item.Cabecera + '</th>';
//        }
      Encabezado_Tabla += '<th align="left" style="width:15%;">' + item.Cabecera + '</th>'; 
    });
    Encabezado_Tabla += '     </tr>' +
                        '</thead>';
    Pie_Tabla = '</table>';
    $('#Div_Pagos_Error_Cabecera').append(Encabezado_Tabla + Pie_Tabla);
    //Contenido de Pagos
    //Muestra los registros a pagar
    var registro = 0;
    Registro_Pago_Error = '<table id="Grid_Recibos_Error" border="0" cellspacing="0" class="GridView_1"' +
                     ' style="width:100%;border-collapse:collapse;white-space:normal">';
    Registro_Pago_Error += '<tbody style="height:250px; overflow:scroll;">';
    $.each(Pagos_Error, function (i, item) {
        registro++;
        if ((registro % 2) != 0) {
            clase = "GridItem";
        }
        else {
            clase = "GridAltItem";
        }
        Registro_Pago_Error += '<tr class="' + clase + '">';
        //Recorre el arreglo del registro de pago para formar la fila
        $.each(item, function (key, item2) {
            var valor = item2;
            var clase = "GridItem";
            //define la alineacion
            var alineacion = "";

            if (key.toUpperCase() == "NO CUENTA" || key.toUpperCase() == "FECHA PAGO" || key.toUpperCase() == "MONTO") {
                alineacion = "right";
                if (key.toUpperCase() == "MONTO") {
                    valor = Number(valor).toFixed(2);
                    Total_Cobro += Number(valor);
                }
            }
            else {
                alineacion = "left";
            }

            //Registro_Pago_Error += '<td align="' + alineacion + '" style="width:20%;"><label class="">' + valor + '</label></td>';
            Registro_Pago_Error += '<td align="left" style="width:15%;"><label class="">' + valor + '</label></td>';
        });
    });
    Registro_Pago_Error += '</tbody>';
    //Pie de la tabla de pagos a realizar
    $('#Div_Pagos_Error').append(Registro_Pago_Error + Pie_Tabla);
    //Pie de la tabla de pagos a realizar
    var Totales_Error = '<table Width="100%">' +
                    '<tr class="GridHeader">' +
                        '<td align="Right">' +
                            'Total Registros: <label style:"text-align:right;width:30px">' + registro + '</label>' +
                            '&nbsp;&nbsp;&nbsp;&nbsp;' +
                            'Total Error: <label style:"text-align:right;width:30px">' + Total_Cobro.toFixed(2) + '</label>' +
                        '</td>' +
                    '</tr>' +
                  '</table>';
    $('#Div_Pagos_Error_Totales').append(Totales_Error);
    return;
}

//Control de archivos
//Funcion que indica cuando el archivo se ha subido correctamente, informa sobre el archivo
function AFU_Archivo_Establecimientos_Complete(sender, args) {
    $('#LblMensajeCarga').text('');
    //informacion del tamaño de archivo
    var AsyncFileUpload = $get(sender._element.id);
    var txts = AsyncFileUpload.getElementsByTagName("input");
//    var nombreArchivo = txts[1].files[0].name;
//    var tamanoArchivo = txts[1].files[0].size / 1024;
//    var mimeArchivo = txts[1].files[0].type;
    var tamanoArchivo = args.get_length();
    var nombreArchivo = args.get_fileName();
    //guarda el nombre del archivo en el campo seleccionado
    $('#Txt_Archivo_ID').val(nombreArchivo);
    $('#LblMensajeCarga').text("Archivo:" + nombreArchivo + "; tamaño:" + tamanoArchivo.toFixed(0) + "KB, Puede continuar con el proceso");
    clearContents(sender._element.id);
}
//Funcion que indica el inicio para subir un archivo, valida el tipo de archivo permitido, en caso de no ser valido
// genera un error
function AFU_Archivo_Establecimientos_Started(sender, args) {
    $('#Txt_Archivo_ID').val('');
    $('#LblMensajeCarga').text('');
    var select = $('#Cmb_Hojas');
    $('option', select).remove();
    var options = '<option value="Seleccione"><-SELECCIONE-></option>';
    $('#Cmb_Hojas').append(options);
    $('#Cmb_Hojas').hide();
    var archivoValido = false;
    //informacion del tamaño de archivo
    var AsyncFileUpload = $get(sender._element.id);
    var txts = AsyncFileUpload.getElementsByTagName("input");
//    var nombreArchivo = txts[1].files[0].name;
//    var tamanoArchivo = txts[1].files[0].size;
    //    var mimeArchivo = txts[1].files[0].type;
    var tamanoArchivo = args.get_length();
    var nombreArchivo = args.get_fileName();
    //Valida el tipo de archivo
    if (Validar_Archivo_Pagos(nombreArchivo)) {
        $('#LblMensajeCarga').text('El archivo se esta cargando, por favor espere...');
        return;
    }
    else {
        var err = new Error();
        err.name = 'SIISR3';
        err.message = 'Formato incorrecto (debe ser: xls, xlsx)';
        throw (err);
        return;
    }
}
//Funcion que muestra los mensajes de error al subir archivos
function AFU_Archivo_Establecimientos_Error(sender, args) {
    $('#LblMensajeCarga').text(args.get_errorMessage());
}
//Funcion para validar el tipo de archivos a subir
function Validar_Archivo_Pagos(nombreArchivo) {
    var extension = "";
    var extensionesValidas = new Array("xls", "xlsx", "cvs");
    var extensionValida = false;
    //Revisa la extensión del archivo para ver si es excel
    extension = nombreArchivo.substr(nombreArchivo.length - 3, 3);
    for (var index = 0; index < extensionesValidas.length; index++) {
        if (extension.toLowerCase() == extensionesValidas[index].toString().toLowerCase()) {
            extensionValida = true;

        }
    }
    return extensionValida;
}
//Funcion para limpiar el contenido de AsyncFileUpload
function clearContents(id) {
    var AsyncFileUpload = $get(id);
    var txts = AsyncFileUpload.getElementsByTagName("input");
    var input = ('#ctl00_Cph_Area_Trabajo1_AFU_Archivo_Establecimientos_ctl02');
    $(input).val('');
    $(input).attr('style', 'backgroundColor:white;');

//    for (var i = 0; i < txts.length; i++) {
//        if (txts[i].type == "text") {
//            $(txts[i]).val('');
//            $(txts[i]).attr('style', 'backgroundColor:white;');
//        }
//    }
}
//Funciones adicionales -----------------------------------------------------------------------------------------
function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}
//Modal progress, uso de animación de preogresso, indicador de actividad -------------------------------------------------
function MostrarProgress() {
    $('[id$=Upgrade]').show();
}
function OcultarProgress() {
    $('[id$=Upgrade]').delay(2000).hide();
}


//--------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------

function leerArchivo(){ // inicio
var nombre_archivo;
var nombre_tienda;
var id_establecimiento;

nombre_archivo=$.trim($('#txt_nombre_archivo').val());
id_establecimiento=$('#Cmb_Establecimientos option:selected').val();
nombre_tienda= $.trim($('#Cmb_Establecimientos option:selected').text());



if (nombre_tienda=="" ){
  $.messager.alert('Japami','Tiene que seleccionar un establecimiento');
   return ;
}

if (nombre_tienda.indexOf("SELECCIONE")!= -1 ){
  $.messager.alert('Japami','Tiene que seleccionar un establecimiento');
   return ;
}

if (nombre_archivo==""){
   $.messager.alert('Japami','Tiene que haber subido un archivo');
   return ;
}


//if (nombre_tienda="OXXO" ){
//       
//    leerArchivoOxxo(nombre_archivo,id_establecimiento);
//    
//   return ;
//}


obtenerNombrePestañas(nombre_archivo);



}// fin


//---------------------------------------------------------------------------------------


function guardarLecturas(){

var nombre_archivo;
var nombre_tienda;
var id_establecimiento;

var objRegistroSeleccionado;

nombre_archivo=$.trim($('#txt_nombre_archivo').val());
id_establecimiento=$('#Cmb_Establecimientos option:selected').val();
nombre_tienda= $.trim($('#Cmb_Establecimientos option:selected').text());

try{
   objRegistroSeleccionado=$('#tabla_pagos_realizar').datagrid('getSelections');
}
catch (err) {
    $.messager.alert('Japami','No hay informaci&oacute;n de lecturas');
    return ;
}

 if(objRegistroSeleccionado.length==0){
    $.messager.alert('Japami','No hay elementos selccionados');
    return ;
 }


aplicarCobros(id_establecimiento,nombre_tienda);

 
// 
// if(nombre_tienda=="OXXO"){
//    aplicarCobrosOxxo(id_establecimiento,nombre_tienda);
//    return;
// }



}




//---------------------------------------------------------------------------------------

function limpiarTablas(){
   $('#Div_Pagos_Realizar').html("<table id='tabla_pagos_realizar'> </table>");
   $('#Div_Pagos_Aplicados').html("<table id='tabla_pagos_aplicados'> </table>");
   $('#Div_Pagos_Error').html("<table id='tabla_pagos_errores'> </table>");
   
   $('#txt_nombre_archivo').val('');
   
}

//---------------------------------------------------------------------------------------

