﻿///*******************************************************************************
///NOMBRE DE LA FUNCIÓN : $(document).ready()
///DESCRIPCIÓN          : Funcion que indica cuando el documento ha sido cargado, para poder modificar los elementod DOM
///PARAMETROS           :   
///CREO                 : Yañez Rodriguez Diego
///FECHA_CREO           : 00/Marzo/2011 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///*******************************************************************************
$(document).ready(function () {
    Configuracion_Inicial();    
});
//COnfiguración Inicial de los controles --------------------------------------------------------------------------------
function Configuracion_Inicial() {
    jQuery.ajaxSetup({
        async: false,
        cache: false,
        timeout: (2 * 1000),
        beforeSend: function () {
            MostrarProgress();
        },
        complete: function () {
            OcultarProgress();
        },
        error: function () {
            OcultarProgress();
        }
        
    });
    //Agregar el DatePiker a la caja de texto de la fecha
    $('#Txt_Fecha_Solicito').datepicker({
        showOn: 'button',
        buttonImageOnly: true,
        buttonImage: '../imagenes/paginas/SmallCalendar.gif',
        changeMonth: true,
        changeYear: true,
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
        dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Me', 'Jue', 'Vie', 'Sab'],
        defaultDate: null,
        nextText: 'Sig',
        prevText: 'Ant',
        firstDay: 1,
        altFormat: 'dd-MMM-yyyy'
    });
    //Subir archivo----------------------------------------------------------------------------------------------------
      crearBoton();
      crearBoton_Evidencias();
      crearEventoDiv();
      crearEventoDiv2();
      subirArchivo();
      subirImagen();
      eventoslink(); 
    //Informacion Alta Solicitudes--------------------------------------------------------------------------------------
    $('#Div_Pestanias').tabs();
    //Configuracion de los grids
    $('#Grid_Documentos').datagrid({
        title: 'Documentos',
        width: 800,
        height: 200,
        columns: [[

                    { field: 'Documento_ID', title: 'Documento_ID', width: 1, hidden: true },
                    { field: 'Nombre_Documento', title: 'Documento', width: 80, sortable: true },
                    { field: 'Obligatorio', title: 'Obligatorio', width: 20, sortable: true, hidden: true },
                    { field: 'Media_ID', title: 'Media_ID', width: 80, sortable: true, hidden: true },
                    { field: 'Nombre_Clave', title: 'Nombre_Clave', width: 1, sortable: true, hidden: true },
                    { field: 'Nombre_Media', title: 'Nombre ', width: 1, resortable: true, hidden: true },
                    { field: 'Ruta', title: 'Ruta', width: 1, sortable: true, hidden: true },
                    { field: 'Extension', title: 'Extension', width: 80, sortable: true, hidden: true },
                    { field: 'Comentarios', title: 'Comentarios', width: 100, sortable: true },
                    { field: 'boton', title: '', width: 20, align: 'center',
                        formatter: function (value, rec, index) {
                            return '<img onclick="Btn_Eliminar_Documento_Click(' + rec.Documento_ID + ')" src="../imagenes/paginas/delete.png" class="Img_Button" alt="quitar" style="cursor:pointer;width:24px;height:24px;"/>';
                        }
                    }
            ]],
        pagination: true,
        rownumbers: true,
        idField: 'Documento_ID',
        sortOrder: 'asc',
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        loadMsg: "Cargando, espere ...",
        pageNumber: 1,
        pageSize: 10,
        fitColumns: true,
        striped: true,
        nowrap: false,
        pageList: [10],
        showFooter: true
    });

    $('#Grid_Evidencias').datagrid({
        title: 'Evidencias',
        width: 800,
        height: 200,
        columns: [[
                    
                    { field: 'No_Solicitud', title: 'No_Solicitud', width: 1, hidden: true },
                    { field: 'No_Inspeccion', title: 'No_Inspeccion', width: 1, sortable: true, hidden: true },
                    { field: 'Media_ID', title: 'Media_ID', width: 1, sortable: true, hidden: true },
                    { field: 'Nombre_Clave', title: 'Nombre_Clave', width: 1, sortable: true, hidden:true },
                    { field: 'Nombre_Media', title: 'Nombre', width: 80, resortable: true},
                    { field: 'Ruta', tilitle: 'Ruta', width: 1, sortable: true, hidden:true },
                    { field: 'Extension', title: 'Extension', width: 80, sortable: true, hidden:true },
                    { field: 'Comentarios', title: 'Comentarios', width: 100, sortable: true},
                    { field: 'boton', title: '', width: 20, align: 'center',
                        formatter: function (value, rec, index) {
                            return '<img onclick="Btn_Eliminar_Evidencia_Click(' + rec.No_Solicitud + ')" src="../imagenes/paginas/delete.png" class="Img_Button" alt="quitar" style="cursor:pointer;width:24px;height:24px;"/>';
                        }
                    }
            ]],
        pagination: true,
        rownumbers: true,
        idField: 'No_Solicitud',
        sortOrder: 'asc',
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        loadMsg: "Cargando, espere ...",
        pageNumber: 1,
        pageSize: 10,
        fitColumns: true,
        striped: true,
        nowrap: false,
        pageList: [10],
        showFooter: true
    });

    //Carga la informacion requerida
    Cargar_Giros_Solicitud();
    Cargar_Servicios_Solicitud();
    Cargar_Motivos_Inspeccion_Solicitud();
    Cargar_Grupos_Conceptos_Solicitud();
    Cargar_Documentos_Solicitud();
    Cargar_Grupos_Conceptos();
    //Eventos de controles
    $('#Cmb_Estatus').change(function (event) {
        var estatus = $("#Cmb_Estatus option:selected").val();
        $('.altaSolicitudContrato').attr('disabled','disabled');
        switch (estatus) {
            case 'APROBADO':
                $('.altaSolicitudContrato').removeAttr('disabled');
                break;
            case 'SELECCIONE':
                $('#Cmb_Estatus').removeAttr('style');
                $('#Cmb_Estatus').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
                break;
            default:
                $('#Cmb_Estatus').removeAttr('style');
                $('#Cmb_Estatus').attr('style', 'width:98%;');
                break;
        }
    });
    $('#Cmb_Duenio').change(function (event) {
        var duenio = $("#Cmb_Duenio option:selected").val();
        if (duenio != 'SELECCIONE') {
            $('#Cmb_Duenio').removeAttr('style');
            $('#Cmb_Duenio').attr('style', 'width:98%;');
        }
        else {
            $('#Cmb_Duenio').removeAttr('style');
            $('#Cmb_Duenio').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
        }
    });
    $('#Cmb_Giro').change(function (event) {
        var giro = $("#Cmb_Giro option:selected").val();
        if (giro != 'SELECCIONE') {
            $('#Cmb_Giro').removeAttr('style');
            $('#Cmb_Giro').attr('style', 'width:98%;');
        }
        else {
            $('#Cmb_Giro').removeAttr('style');
            $('#Cmb_Giro').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
        }
    });
    $('#Cmb_Tipo_Servicio').change(function (event) {
        var servicio = $("#Cmb_Tipo_Servicio option:selected").val();
        if (servicio != 'SELECCIONE') {
            $('#Cmb_Tipo_Servicio').removeAttr('style');
            $('#Cmb_Tipo_Servicio').attr('style', 'width:98%;');
        }
        else {
            $('#Cmb_Tipo_Servicio').removeAttr('style');
            $('#Cmb_Tipo_Servicio').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
        }
    });
    $('#Cmb_Condicion').change(function (event) {
        var condicion = $("#Cmb_Condicion option:selected").val();
        if (condicion != 'SELECCIONE') {
            $('#Cmb_Condicion').removeAttr('style');
            $('#Cmb_Condicion').attr('style', 'width:98%;');
        }
        else {
            $('#Cmb_Condicion').removeAttr('style');
            $('#Cmb_Condicion').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
        }
    });
    $('#Cmb_Grupos_Conceptos').change(function (event) {
        var grupo = $("#Cmb_Grupos_Conceptos option:selected").val();
        if (grupo != 'SELECCIONE') {
            var detalles = $('#Cmb_Grupos_Conceptos option:selected').attr('detalles');
            $('#Txt_Detalles_Grupos').val(detalles);
        }
        else {
            $('#Txt_Detalles_Grupos').val('');
        }
    });
    
    $('#Txt_Nombre_Solicitante').keyup(function () {
        var longitud = $('#Txt_Nombre_Solicitante').val().length;
        if (longitud == 0) {
            $('#Txt_Nombre_Solicitante').removeAttr('style');
            $('#Txt_Nombre_Solicitante').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
        }
        else {
            $('#Txt_Nombre_Solicitante').removeAttr('style');
            $('#Txt_Nombre_Solicitante').attr('style', 'width:98%;');
        }
    }); 
    $('#Txt_Tiempo_Habilitado_Deshabilitado').keyup(function () {
        var longitud = $('#Txt_Tiempo_Habilitado_Deshabilitado').val().length;
        if (longitud == 0) {
            $('#Txt_Tiempo_Habilitado_Deshabilitado').removeAttr('style');
            $('#Txt_Tiempo_Habilitado_Deshabilitado').attr('style', 'width:30%;border-style:dotted; border-color:Red;');
        }
        else {
            $('#Txt_Tiempo_Habilitado_Deshabilitado').removeAttr('style');
            $('#Txt_Tiempo_Habilitado_Deshabilitado').attr('style', 'width:30%;');
        }
    });
    $('#Txt_Tiempo_Habilitado_Deshabilitado').numberbox({
        min: 1,
        max: 99,
        precision: 0
    });
    $('#Cmb_Periodo_Tiempo_Habilitado_Deshabilitado').change(function (event) {
        var grupo = $("#Cmb_Periodo_Tiempo_Habilitado_Deshabilitado option:selected").val();
        if (grupo != 'SELECCIONE') {
            $('#Cmb_Periodo_Tiempo_Habilitado_Deshabilitado').removeAttr('style');
            $('#Cmb_Periodo_Tiempo_Habilitado_Deshabilitado').attr('style', 'width:50%;');
        }
        else {
            $('#Cmb_Periodo_Tiempo_Habilitado_Deshabilitado').removeAttr('style');
            $('#Cmb_Periodo_Tiempo_Habilitado_Deshabilitado').attr('style', 'width:50%;border-style:dotted; border-color:Red;');
        }
    });
    $('#Txt_No_Personas').keyup(function () {
        var longitud = $('#Txt_No_Personas').val().length;
        if (longitud == 0) {
            $('#Txt_No_Personas').removeAttr('style');
            $('#Txt_No_Personas').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
        }
        else {
            $('#Txt_No_Personas').removeAttr('style');
            $('#Txt_No_Personas').attr('style', 'width:98%;');
        }
    });
    $('#Txt_No_Personas').numberbox({
        min: 1,
        max: 99,
        precision: 0
    });
    $('#Cmb_Medidor').change(function(event) {
    var grupo = $("#Cmb_Medidor option:selected").val();
        if (grupo != 'SELECCIONE') {
            $('#Cmb_Medidor').removeAttr('style');
            $('#Cmb_Medidor').attr('style', 'width:98%;');            
        }
        else {
            $('#Cmb_Medidor').removeAttr('style');
            $('#Cmb_Medidor').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
        }
    });
    $('#Txt_No_Medidor').keyup(function () {
        var longitud = $('#Txt_No_Medidor').val().length;
        if (longitud == 0) {
            $('#Txt_No_Medidor').removeAttr('style');
            $('#Txt_No_Medidor').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
        }
        else {
            $('#Txt_No_Medidor').removeAttr('style');
            $('#Txt_No_Medidor').attr('style', 'width:98%;');
        }
    });
    $('#Txt_Lectura_Medidor').keyup(function () {
        var longitud = $('#Txt_Lectura_Medidor').val().length;
        if (longitud == 0) {
            $('#Txt_Lectura_Medidor').removeAttr('style');
            $('#Txt_Lectura_Medidor').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
        }
        else {
            $('#Txt_Lectura_Medidor').removeAttr('style');
            $('#Txt_Lectura_Medidor').attr('style', 'width:98%;');
        }
    });
    $('#Txt_Lectura_Medidor').numberbox({
        min: 1,
        max: 999999999,
        precision: 0
    });
    $('#Txt_Medida_Largo').keyup(function () {
        var longitud = $('#Txt_Medida_Largo').val().length;
        if (longitud == 0) {
            $('#Txt_Medida_Largo').removeAttr('style');
            $('#Txt_Medida_Largo').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
        }
        else {
            $('#Txt_Medida_Largo').removeAttr('style');
            $('#Txt_Medida_Largo').attr('style', 'width:98%;');
        }
    });
    $('#Txt_Medida_Largo').numberbox({
        min: 1,
        max: 999999999,
        precision: 0
    });
    $('#Txt_Medida_Ancho').keyup(function () {
        var longitud = $('#Txt_Medida_Ancho').val().length;
        if (longitud == 0) {
            $('#Txt_Medida_Ancho').removeAttr('style');
            $('#Txt_Medida_Ancho').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
        }
        else {
            $('#Txt_Medida_Ancho').removeAttr('style');
            $('#Txt_Medida_Ancho').attr('style', 'width:98%;');
        }
    });
    $('#Txt_Medida_Ancho').numberbox({
        min: 1,
        max: 999999999,
        precision: 0
    });
    $('#Txt_Diametro_Toma').keyup(function () {
        var longitud = $('#Txt_Diametro_Toma').val().length;
        if (longitud == 0) {
            $('#Txt_Diametro_Toma').removeAttr('style');
            $('#Txt_Diametro_Toma').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
        }
        else {
            $('#Txt_Diametro_Toma').removeAttr('style');
            $('#Txt_Diametro_Toma').attr('style', 'width:98%;');
        }
    });
    // Alta de Solicitud
    Configurar_Botones_Alta_Solicitud("Inicial");
    $('#Btn_Nuevo').click(function (event) {
        event.preventDefault();
        Btn_Nuevo_Click();
    });
    $('#Btn_Modificar').click(function (event) {
        event.preventDefault();
        Btn_Modificar_Click();
    });
    $('#Btn_Contrato').click(function (event) {
        event.preventDefault();
        Btn_Contrato_Click();
    });
    $('#Btn_Imprimir_Solicitud').click(function (event) {
        event.preventDefault();
        Btn_Imprimir_Solicitud_Click();
    });
    $('#Btn_Salir').click(function (event) {
        event.preventDefault();
        Btn_Salir_Click();
    });
    $('#Btn_Agregar_Documento').click(function (event) {
        event.preventDefault();
        Btn_Agregar_Documento_Click();
    });
    $('#Btn_Agregar_Evidencia').click(function (event) {
        event.preventDefault();
        Btn_Agregar_Evidencia_Click();
    });
    $('#Chk_Inspeccion').change(function (event) {
        Chk_Inspeccion_Change();
    });
    //Informacion para las solicitudes----------------------------------------------------------------------------------
    //grid de resultados de predios
    $('#Grid_Solicitudes').datagrid({
        title: 'Solicitudes de Contrato',
        width: 850,
        height: 300,
        columns: [[
                    { field: 'boton', title: 'Sel', width: 20, align: 'center',
                        formatter: function (value, rec) {
                            return '<input type="button" id="' + rec.Predio_ID + '" style="width:10px" />';
                        }
                    },
                    { field: 'No_Solicitud', title: 'No_Solicitud', width: 1, hidden: true },
                    { field: 'Folio', title: 'Folio', width: 80, sortable: true },
                    { field: 'Domicilio', title: 'Domicilio', width: 250, sortable: true },
                    { field: 'Fecha', title: 'Fecha', width: 80, sortable: true },
                    { field: 'Solicito', title: 'Solicito', width: 250, sortable: true },
                    { field: 'Estatus', title: 'Estatus', width: 0, sortable: true },
                    { field: 'Predio_ID', title: 'Predio_ID', width: 1, hidden: true }
            ]],
        pagination: true,
        rownumbers: true,
        idField: 'Predio_ID',
        sortOrder: 'asc',
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        loadMsg: "Cargando, espere ...",
        pageNumber: 1,
        pageSize: 10,
        fitColumns: true,
        striped: true,
        nowrap: false,
        pageList: [10, 20],
        remoteSort: false,
        showFooter: true,
        onSelect: function (rowIndex, rowData) {
            Cargar_Informacion_Solicitud(rowData.No_Solicitud, rowData.Predio_ID);
        }
    });
    //Eventos Controles
    $('#Txt_Busqueda_Solicitud_Zona').keyup(function () {
        var longitud = $('#Txt_Busqueda_Solicitud_Zona').val().length;
        if (longitud == 0) {
            $('#Txt_Busqueda_Zona_Hidden').val('');
        }
    });
    $('#Txt_Busqueda_Solicitud_Colonia').keyup(function () {
        var longitud = $('#Txt_Busqueda_Solicitud_Colonia').val().length;
        if (longitud == 0) {
            $('#Txt_Busqueda_Solicitud_Colonia_Hidden').val('');
        }
    });
    $('#Txt_Busqueda_Solicitud_Calle').keyup(function () {
        var longitud = $('#Txt_Busqueda_Solicitud_Calle').val().length;
        if (longitud == 0) {
            $('#Txt_Busqueda_Solicitud_Calle_Hidden').val('');
        }
    });
    $('#Txt_Busqueda_Solicitud_Giro').keyup(function () {
        var longitud = $('#Txt_Busqueda_Solicitud_Giro').val().length;
        if (longitud == 0) {
            $('#Txt_Busqueda_Solicitud_Giro_Hidden').val('');
        }
    });
    $('#Btn_Buscar_Solicitudes').click(function (event) {
        event.preventDefault();
        Filtrar_Solicitudes();
    });
    //autocompletados
    AutocompletadoSolicitudesZonasFiltro();
    AutocompletadoSolicitudesGirosFiltro();
    AutocompletadoSolicitudesColoniaFiltro();
    AutocompletadoSolicitudesCalleFiltro();

    //Información para los predios -------------------------------------------------------------------------------------
    //Limpiar campos de filtros
    $('#Txt_Busqueda_Zona_Alta').keyup(function() {
        var longitud =  $('#Txt_Busqueda_Zona_Alta').val().length;
        if(longitud==0)
        {
            $('#Txt_Busqueda_Zona_Alta_Hidden').val('');
        }  
    });
    $('#Txt_Busqueda_Giro_Alta').keyup(function() {
        var longitud = $('#Txt_Busqueda_Giro_Alta').val().length;
        if(longitud==0)
        {
            $('#Txt_Busqueda_Giro_Alta_Hidden').val('');
        }  
    });
    $('#Txt_Busqueda_Colonia_Alta').keyup(function() {
        var longitud = $('#Txt_Busqueda_Colonia_Alta').val().length;
        if(longitud==0)
        {
            $('#Txt_Busqueda_Colonia_Alta_Hidden').val('');
        }  
    });
    $('#Txt_Busqueda_Fraccionador_Alta').keyup(function() {
        var longitud = $('#Txt_Busqueda_Fraccionador_Alta').val().length;
        if(longitud==0)
        {
            $('#Txt_Busqueda_Fraccionador_Alta_Hidden').val('');
        }  
    });
    $('#Txt_Busqueda_Calle_Alta').keyup(function() {
        var longitud = $('#Txt_Busqueda_Calle_Alta').val().length;
        if(longitud==0)
        {
            $('#Txt_Busqueda_Calle_Alta_Hidden').val('');
        }  
    });
    //Campos Numericos
    $('#Txt_Superficie_M2_Alta').numberbox({
        min: 1,
        max: 999999999,
        precision: 2
    });
    $('#Txt_Superficie_Construida_Alta').numberbox({
        min: 1,
        max: 999999999,
        precision: 2
    });
    $('#Txt_Superficie_Jardin_Alta').numberbox({
        min: 1,
        max: 999999999,
        precision: 2
    });
    $('#Txt_Niveles_Alta').numberbox({
        min: 1,
        max: 99,
        precision: 2
    });
    $('#Txt_Muebles_Sanitarios_Alta').numberbox({
        min: 1,
        max: 99,
        precision: 2
    });
    $('#Txt_Subpredios_Alta').numberbox({
        min: 1,
        max: 99,
        precision: 2
    });
    $('#Txt_Latitud_GMS_Grados_Alta').numberbox({
        min: 1,
        max: 90,
        precision: 2
    });
    $('#Txt_Latitud_GMS_Minutos_Alta').numberbox({
        min: 1,
        max: 59.9999,
        precision: 2
    });
    $('#Txt_Latitud_GMS_Segundos_Alta').numberbox({
        min: 1,
        max: 9999999,
        precision: 2
    });
    $('#Txt_Latitud_Decimal_Alta').numberbox({
        min: 1,
        max: 90,
        precision: 6
    });
    $('#Txt_Longitud_GMS_Grados_Alta').numberbox({
        min: 1,
        max: 90,
        precision: 2
    });
    $('#Txt_Longitud_GMS_Minutos_Alta').numberbox({
        min: 1,
        max: 59.9999,
        precision: 2
    });
    $('#Txt_Longitud_GMS_Segundos_Alta').numberbox({
        min: 1,
        max: 9999999,
        precision: 2
    });
    $('#Txt_Longitud_Decimal_Alta').numberbox({
        min: 1,
        max: 90,
        precision: 6
    });
    //grid de resultados de predios
    $('#Grid_Predios').datagrid({
        title: 'Catalogo Predios',
        width: 850,
        height: 250,
        columns: [[
                    { field: 'boton', title: 'Sel', width: 20, align: 'center',
                        formatter: function (value, rec) {
                            return '<input type="button" id="' + rec.Predio_ID + '" style="width:10px" />';
                        }
                    },
                    { field: 'Predio_ID', title: 'Predio ID', width: 1, hidden: true },
                    { field: 'Domicilio', title: 'Domicilio', width: 300, sortable: true },
                    { field: 'No_Cuenta', title: 'No Cuenta', width: 80, sortable: true },
                    { field: 'Estatus', title: 'Estatus', width: 80, sortable: true },
                    { field: 'Colonia', title: 'Colonia', width: 0, sortable: true, hidden: true },
                    { field: 'Calle', title: 'Calle', width: 0, sortable: true, hidden: true },
                    { field: 'No_Exterior', title: 'No Exterior', width: 0, sortable: true, hidden: true },
                    { field: 'No_Interior', title: 'No Interior', width: 0, sortable: true, hidden: true },
                    { field: 'Giro_ID', title: 'Giro_ID', width: 0, sortable: true, hidden: true }

            ]],
        pagination: true,
        rownumbers: true,
        idField: 'Predio_ID',
        sortOrder: 'asc',
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        loadMsg: "Cargando, espere ...",
        pageNumber: 1,
        pageSize: 10,
        fitColumns: true,
        striped: true,
        nowrap: false,
        pageList: [10, 20],
        remoteSort: false,
        showFooter: true,
        onSelect: function (rowIndex, rowData) {
            $('#Txt_Predio_ID').val(rowData.Predio_ID);
            $('#Txt_Colonia').val(rowData.Colonia);
            $('#Txt_Calle').val(rowData.Calle);
            $('#Txt_No_Exterior').val(rowData.No_Exterior);
            $('#Txt_No_Interior').val(rowData.No_Interior);
            $('#Cmb_Giro').val(rowData.Giro_ID);
            Limpia_Campos_Predios();
            $('#Cmb_Giro').removeAttr('style');
            $('#Cmb_Giro').attr('style', 'width:98%');
            //regresa a la pantalla principal
            $('#Modal_Predios').delay(2000).jqmHide();
        }
    });
    //Se activa la función del boton buscar
    $('#Btn_Filtrar_Predios').click(function (event) {
        event.preventDefault();
        Filtrar_Predios();
    });
    //Para filtros
    AutocompletadoColoniasFiltro();
    var colonia_id = $('#Txt_Busqueda_Colonia_Hidden').val();
    AutocompletadoCallesFiltro(colonia_id);
    AutocompletadoZonasFiltro();
    AutocompletadoGirosFiltro();
    AutocompletadoFraccionadorFiltro();
    //activa la busqueda modal de predios
    $('#Modal_Predios').jqm();
    $('#Btn_Buscar_Predio').click(function (event) {
        event.preventDefault();
        Limpia_Campos_Predios();
        Configurar_Botones_Alta_Predio("Inicial");
        $('#Modal_Predios').jqmShow();
    });
    
    //LLenado de información para el alta
    AutocompletadoColonias();
    AutocompletadoZonas();
    AutocompletadoGiros();
    AutocompletadoTarifas();
    AutocompletadoFraccionador();
    AutocompletadoViviendas();
    Cargar_Giros();
    Cargar_Tipos_Vivienda();
    Cargar_Tarifas();
    Cargar_Condicion();
    Cargar_Materiales();
    Cargar_Region();
    //Configuración de enventos de controles
    $('#Cmb_Calle_Alta').change(function (event) {
        var calle_id = $("#Cmb_Calle_Alta option:selected").val();
        if (calle_id != 'Seleccione') {
            $('#Cmb_Calle_Alta').removeAttr('style');
            $('#Cmb_Calle_Alta').attr('style', 'width:98%;');
            Cargar_Calles_Referencia();
            $('#Cmb_Calle_Referencia1_Alta').removeAttr('disabled');
            $('#Cmb_Calle_Referencia1_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
            $('#Cmb_Calle_Referencia2_Alta').removeAttr('disabled');
            $('#Cmb_Calle_Referencia2_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
            $('#Cmb_Manzana_Alta').removeAttr('disabled');
            $('#Cmb_Manzana_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
            //Asigna la misma calle a la toma
            $('#Cmb_Calle_Toma_Alta').val(calle_id);
            Cargar_Calles_Referencia_Toma();

        }
        else {
            $('#Cmb_Calle_Alta').removeAttr('style');
            $('#Cmb_Calle_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
            $('#Cmb_Calle_Referencia1_Alta').removeAttr('style');
            $('#Cmb_Calle_Referencia1_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
            $('#Cmb_Calle_Referencia1_Alta').attr('disabled', 'disabled');
            $('#Cmb_Calle_Referencia1_Alta').val('Seleccione');
            $('#Cmb_Calle_Referencia2_Alta').removeAttr('style');
            $('#Cmb_Calle_Referencia2_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
            $('#Cmb_Calle_Referencia2_Alta').attr('disabled', 'disabled');
            $('#Cmb_Calle_Referencia2_Alta').val('Seleccione');
            $('#Cmb_Manzana_Alta').removeAttr('style');
            $('#Cmb_Manzana_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
            $('#Cmb_Manzana_Alta').attr('disabled', 'disabled');
            $('#Cmb_Manzana_Alta').val('Seleccione');
        }
    });
    $('#Cmb_Calle_Referencia1_Alta').change(function (event) {
        var calle_id = $("#Cmb_Calle_Referencia1_Alta option:selected").val();
        if (calle_id != 'Seleccione') {
            $('#Cmb_Calle_Referencia1_Alta').removeAttr('style');
            $('#Cmb_Calle_Referencia1_Alta').attr('style', 'width:98%;');
            //Carga las manzanas que coinciden con la busqueda
            Cargar_Manzanas();
            //Asigna la misma calle a la ubicacion de la toma
            $('#Cmb_Calle_Referencia_1_Toma_Alta').val(calle_id);
        }
        else {
            $('#Cmb_Calle_Referencia1_Alta').removeAttr('style');
            $('#Cmb_Calle_Referencia1_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
            $('#Cmb_Calle_Referencia1_Alta').val('Seleccione');
        }
    });
    $('#Cmb_Calle_Referencia2_Alta').change(function (event) {
        var calle_id = $("#Cmb_Calle_Referencia2_Alta option:selected").val();
        if (calle_id != 'Seleccione') {
            $('#Cmb_Calle_Referencia2_Alta').removeAttr('style');
            $('#Cmb_Calle_Referencia2_Alta').attr('style', 'width:98%;');
            //Carga las manzanas que coinciden con la busqueda
            Cargar_Manzanas();
            $('#Cmb_Calle_Referencia_2_Toma_Alta').val(calle_id);
        }
        else {
            $('#Cmb_Calle_Referencia2_Alta').removeAttr('style');
            $('#Cmb_Calle_Referencia2_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
            $('#Cmb_Calle_Referencia2_Alta').val('Seleccione');
        }
    });
    //Para la toma
    $('#Cmb_Calle_Toma_Alta').change(function (event) {
        var calle_id = $("#Cmb_Calle_Toma_Alta option:selected").val();
        if (calle_id != 'Seleccione') {
            $('#Cmb_Calle_Toma_Alta').removeAttr('style');
            $('#Cmb_Calle_Toma_Alta').attr('style', 'width:98%;');
            Cargar_Calles_Referencia_Toma();
            $('#Cmb_Calle_Referencia_1_Toma_Alta').removeAttr('disabled');
            $('#Cmb_Calle_Referencia_2_Toma_Alta').removeAttr('disabled');
        }
        else {
            $('#Cmb_Calle_Toma_Alta').removeAttr('style');
            $('#Cmb_Calle_Toma_Alta').attr('style', 'width:98%;');
            $('#Cmb_Calle_Referencia_1_Toma_Alta').removeAttr('style');
            $('#Cmb_Calle_Referencia_1_Toma_Alta').attr('style', 'width:98%;');
            $('#Cmb_Calle_Referencia_1_Toma_Alta').attr('disabled', 'disabled');
            $('#Cmb_Calle_Referencia_1_Toma_Alta').val('Seleccione');
            $('#Cmb_Calle_Referencia_2_Toma_Alta').removeAttr('style');
            $('#Cmb_Calle_Referencia_2_Toma_Alta').attr('style', 'width:98%;');
            $('#Cmb_Calle_Referencia_2_Toma_Alta').attr('disabled', 'disabled');
            $('#Cmb_Calle_Referencia_2_Toma_Alta').val('Seleccione');
        }
    });
    $('#Cmb_Calle_Referencia_1_Toma').change(function (event) {
        var calle_id = $("#Cmb_Calle_Referencia_1_Toma_Alta option:selected").val();
        if (calle_id != 'Seleccione') {
            $('#Cmb_Calle_Referencia_1_Toma_Alta').removeAttr('style');
            $('#Cmb_Calle_Referencia_1_Toma_Alta').attr('style', 'width:98%;');
        }
        else {
            $('#Cmb_Calle_Referencia_1_Toma_Alta').removeAttr('style');
            $('#Cmb_Calle_Referencia_1_Toma_Alta').attr('style', 'width:98%;');
            $('#Cmb_Calle_Referencia_1_Toma_Alta').val('Seleccione');
        }
    });
    $('#Cmb_Calle_Referencia_2_Toma_Alta').change(function (event) {
        var calle_id = $("#Cmb_Calle_Referencia_2_Toma_Alta option:selected").val();
        if (calle_id != 'Seleccione') {
            $('#Cmb_Calle_Referencia_2_Toma_Alta').removeAttr('style');
            $('#Cmb_Calle_Referencia_2_Toma_Alta').attr('style', 'width:98%;');
        }
        else {
            $('#Cmb_Calle_Referencia_2_Toma_Alta').removeAttr('style');
            $('#Cmb_Calle_Referencia_2_Toma_Alta').attr('style', 'width:98%;');
            $('#Cmb_Calle_Referencia_2_Toma_Alta').val('Seleccione');
        }
    });
    $('#Cmb_Manzana_Alta').change(function (event) {
        var calle_id = $("#Cmb_Manzana_Alta option:selected").val();
        if (calle_id != 'Seleccione') {
            $('#Cmb_Manzana_Alta').removeAttr('style');
            $('#Cmb_Manzana_Alta').attr('style', 'width:98%;');
        }
        else {
            $('#Cmb_Manzana_Alta').removeAttr('style');
            $('#Cmb_Manzana_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
            $('#Cmb_Manzana_Alta').val('Seleccione');
        }
    });
    $('#Cmb_Region_Alta').change(function (event) {
        var region_id = $("#Cmb_Region_Alta option:selected").val();
        if (region_id != 'Seleccione') {
            Cargar_Sector();
            $('#Cmb_Sector_Alta').removeAttr('disabled');
        }
        else {
            $('#Cmb_Sector_Alta').attr('disabled', 'disabled');
            $('#Cmb_Sector_Alta').val('Seleccione');
        }
    });
    //Predios Generales
    $('#Cmb_Giros_Alta').change(function (event) {
        var giro_id = $("#Cmb_Giros_Alta option:selected").val();
        if (giro_id != 'Seleccione') {
            Cargar_Giros_Actividad(giro_id);
            Asigna_Tarifa();
            $('#Cmb_Giros_Alta').removeAttr('style');
            $('#Cmb_Giros_Alta').attr('style', 'width:98%;');
        }
        else {
            $('#Cmb_Giros_Alta').removeAttr('style');
            $('#Cmb_Giros_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
        }
    });
    $('#Cmb_Actividad_Alta').change(function (event) {
        var giro_actividad_id = $("#Cmb_Actividad_Alta option:selected").val();
        if (giro_actividad_id != 'Seleccione') {
            $('#Cmb_Actividad_Alta').removeAttr('style');
            $('#Cmb_Actividad_Alta').attr('style', 'width:98%;');
        }
        else {
            $('#Cmb_Actividad_Alta').removeAttr('style');
            $('#Cmb_Actividad_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
        }
    });
    $('#Cmb_Tarifa_Alta').change(function (event) {
        var tarifa_id = $("#Cmb_Tarifa_Alta option:selected").val();
        if (tarifa_id != 'Seleccione') {
            $('#Cmb_Tarifa_Alta').removeAttr('style');
            $('#Cmb_Tarifa_Alta').attr('style', 'width:98%;');
        }
        else {
            $('#Cmb_Tarifa_Alta').removeAttr('style');
            $('#Cmb_Tarifa_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
        }
    });
    $('#Cmb_Tipo_Vivienda_Alta').change(function (event) {
        var tarifa_id = $("#Cmb_Tipo_Vivienda_Alta option:selected").val();
        if (tarifa_id != 'Seleccione') {
            $('#Cmb_Tipo_Vivienda_Alta').removeAttr('style');
            $('#Cmb_Tipo_Vivienda_Alta').attr('style', 'width:98%;');
        }
        else {
            $('#Cmb_Tipo_Vivienda_Alta').removeAttr('style');
            $('#Cmb_Tipo_Vivienda_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
        }
    });
    $('#Txt_Numero_Exterior_Alta').keyup(function () {
        var longitud =  $('#Txt_Numero_Exterior_Alta').val().length;
        if(longitud==0)
        {
            $('#Txt_Numero_Exterior_Alta').removeAttr('style');
            $('#Txt_Numero_Exterior_Alta').attr('style', 'width:30%; border-style:dotted; border-color:Red;');
        }
        else{
            $('#Txt_Numero_Exterior_Alta').removeAttr('style');
            $('#Txt_Numero_Exterior_Alta').attr('style', 'width:30%;');
        }
    });
    //Conversión de ubicaciones de los predios DMS a Decimal y viceversa
    $('#Txt_Latitud_Decimal_Alta').blur(function () {
        grados_a_dms_latidud();
    });
    $('#Txt_Longitud_Decimal_Alta').blur(function () {
        grados_a_dms_longitud();
    });
    $('#Txt_Latitud_GMS_Grados_Alta').blur(function () {
        dms_a_grados_latidud();
    });
    $('#Txt_Latitud_GMS_Minutos_Alta').blur(function () {
        dms_a_grados_latidud();
    });
    $('#Txt_Latitud_GMS_Segundos_Alta').blur(function () {
        dms_a_grados_latidud();
    });
    $('#Txt_Longitud_GMS_Grados_Alta').blur(function () {
        dms_a_grados_longitud();
    });
    $('#Txt_Longitud_GMS_Minutos_Alta').blur(function () {
        dms_a_grados_longitud();
    });
    $('#Txt_Longitud_GMS_Segundos_Alta').blur(function () {
        dms_a_grados_longitud();
    });
    //Para el predio
    // Alta de predio
    Configurar_Botones_Alta_Predio("Inicial");
    $('#Btn_Guardar_Predio').click(function (event) {
        event.preventDefault();
        Btn_Guardar_Predio_Click();
    });
    $('#Btn_Cancelar_Predio').click(function (event) {
        event.preventDefault();
        Btn_Cancelar_Predio_Click();
    });
    //Tabs de Alta de Predios
    $('#Tab_Predios').tabs();
//Configuracion de los controles de Usuarios----------------------------------------------------------------------------------------------------
    //Configuracion del grid de Usuarios
       $('#Grid_Usuarios').datagrid({
        title: 'Catalogo  Usuarios',
        width: 850,
        height: 250,
        columns: [[
                    { field: 'boton', title: 'Sel', width: 20, align: 'center',
                        formatter: function (value, rec) {
                            return '<input type="button" id="' + rec.Usuario_ID + '" style="width:10px" />';
                        }
                    },
                    { field: 'Usuario_ID', title: 'Usuario ID', width: 1, hidden: true },
                    { field: 'RFC', title: 'RFC', width: 80, sortable: true },
                    { field: 'Razon_Social', title: 'Razon Social', width: 100, sortable: true },
                    { field: 'Nombre', title: 'Nombre', width: 100, sortable: true },
                    { field: 'Direccion', title: 'Direccion', width: 100,hidden: true }
            ]],
        pagination: true,
        rownumbers: true,
        idField: 'Usuario_ID',
        sortOrder: 'asc',
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        loadMsg: "Cargando, espere ...",
        pageNumber: 1,
        pageSize: 10,
        fitColumns: true,
        striped: true,
        nowrap: false,
        pageList: [10, 20],
        remoteSort: false,
        showFooter: true,
        onSelect: function (rowIndex, rowData) {
            $('#Txt_Usuario_ID').val(rowData.Usuario_ID);
            $('#Txt_Nombre_Usuario').val(rowData.Nombre);
            $('#Txt_Nombre_Solicitante').val(rowData.Nombre);
            Limpia_Campos_Usuarios();
            //regresa a la pantalla principal
            $('#Modal_Usuarios').delay(2000).jqmHide();
            //Quita el valor de requerido del usuario
            $('#Txt_Nombre_Usuario').removeAttr('style');
            $('#Txt_Nombre_Usuario').attr('style', 'width:80%');
            $('#Txt_Nombre_Solicitante').removeAttr('style');
            $('#Txt_Nombre_Solicitante').attr('style', 'width:98%');
        }
    });
  
    //autocompleatodos filtros usuarios
    AutocompletadoNombreUsuariosFiltros();
    AutocompletadoApellidoPaternoUsuariosFiltros();
    AutocompletadoCalleUsuariosFiltros();
    AutocompletadoColoniaUsuariosFiltros();
    AutocompletadoCiudadUsuariosFiltros();
    //autocompletado alta usuarios
    //Carga de Informacion
    AutocompletadoNombreUsuarios();
    AutocompletadoApellidoPaternoUsuarios();
    AutocompletadoApellidoMaternoUsuarios();
    AutocompletadoColoniaUsuarios();
    AutocompletadoCalleUsuarios();
    Cargar_Estados();
    //Eventos de Controles
    $('#Txt_Nombre_Usuario_Alta').keyup(function () {
        var longitud = $('#Txt_Nombre_Usuario_Alta').val().length;
        if (longitud == 0) {
            $('#Txt_Nombre_Usuario_Alta').removeAttr('style');
            $('#Txt_Nombre_Usuario_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
        }
        else {
            $('#Txt_Nombre_Usuario_Alta').removeAttr('style');
            $('#Txt_Nombre_Usuario_Alta').attr('style', 'width:98%;');
        }
    });
    $('#Txt_RFC_Usuario_Alta').keyup(function () {
        var longitud = $('#Txt_RFC_Usuario_Alta').val().length;
        if (longitud == 0) {
            $('#Txt_RFC_Usuario_Alta').removeAttr('style');
            $('#Txt_RFC_Usuario_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
        }
        else {
            $('#Txt_RFC_Usuario_Alta').removeAttr('style');
            $('#Txt_RFC_Usuario_Alta').attr('style', 'width:98%;');
        }
    });
    $('#Txt_Apellido_Paterno_Usuario_Alta').keyup(function () {
        var longitud = $('#Txt_Apellido_Paterno_Usuario_Alta').val().length;
        if (longitud == 0) {
            $('#Txt_Apellido_Paterno_Usuario_Alta').removeAttr('style');
            $('#Txt_Apellido_Paterno_Usuario_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
        }
        else {
            $('#Txt_Apellido_Paterno_Usuario_Alta').removeAttr('style');
            $('#Txt_Apellido_Paterno_Usuario_Alta').attr('style', 'width:98%;');
        }
    });
    $('#Txt_Razon_Social_Usuario').keyup(function () {
        var longitud = $('#Txt_Razon_Social_Usuario').val().length;
        if (longitud == 0) {
            $('#Txt_Razon_Social_Usuario').removeAttr('style');
            $('#Txt_Razon_Social_Usuario').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
        }
        else {
            $('#Txt_Razon_Social_Usuario').removeAttr('style');
            $('#Txt_Razon_Social_Usuario').attr('style', 'width:98%;');
        }
    });
    $('#Cmb_Estado_Usuario').change(function (event) {
        var estado_id = $("#Cmb_Estado_Usuario option:selected").val();
        if (estado_id != 'Seleccione') {
            $('#Cmb_Estado_Usuario').removeAttr('style');
            $('#Cmb_Estado_Usuario').attr('style', 'width:98%;');
            Cargar_Ciudades();
            $('#Cmb_Ciudad_Usuario').removeAttr('disabled');
        }
        else {
            $('#Cmb_Estado_Usuario').removeAttr('style');
            $('#Cmb_Estado_Usuario').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
            $('#Cmb_Ciudad_Usuario').attr('disabled', 'disabled');
        }
    });
    $('#Cmb_Ciudad_Usuario').change(function (event) {
        var ciudad_id = $("#Cmb_Ciudad_Usuario option:selected").val();
        if (ciudad_id != 'Seleccione') {
            $('#Cmb_Ciudad_Usuario').removeAttr('style');
            $('#Cmb_Ciudad_Usuario').attr('style', 'width:98%;');
        }
        else {
            $('#Cmb_Ciudad_Usuario').removeAttr('style');
            $('#Cmb_Ciudad_Usuario').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
        }
    });
    $('#Txt_Colonia_Usuario').keyup(function () {
        var longitud = $('#Txt_Colonia_Usuario').val().length;
        if (longitud == 0) {
            $('#Txt_Colonia_Usuario_Hidden').val('');
            $('#Txt_Colonia_Usuario').removeAttr('style');
            $('#Txt_Colonia_Usuario').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
        }
    });
    $('#Txt_Calle_Usuario').keyup(function () {
        var longitud = $('#Txt_Calle_Usuario').val().length;
        if (longitud == 0) {
            $('#Txt_Calle_Usuario_Hidden').val('');
            $('#Txt_Calle_Usuario').removeAttr('style');
            $('#Txt_Calle_Usuario').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
        }
    });
    $('#Txt_No_Exterior_Usuario').keyup(function () {
        var longitud = $('#Txt_No_Exterior_Usuario').val().length;
        if (longitud == 0) {
            $('#Txt_No_Exterior_Usuario').removeAttr('style');
            $('#Txt_No_Exterior_Usuario').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
        }
        else {
            $('#Txt_No_Exterior_Usuario').removeAttr('style');
            $('#Txt_No_Exterior_Usuario').attr('style', 'width:98%;');
        }
    });
    //ConfiguracionInicial
    Configurar_Botones_Alta_Usuario("Inicial");
    //Activa el modal de los usuarios
    $('#Btn_Filtrar_Usuarios').click(function (event) {
        event.preventDefault();
        Filtrar_Usuarios();
    });
    $('#Modal_Usuarios').jqm();
    $('#Btn_Buscar_Usuario').click(function (event) {
        event.preventDefault();
        $('#Modal_Usuarios').jqmShow();
    });
    $('#Btn_Guardar_Usuario').click(function (event) {
        event.preventDefault();
        Btn_Guardar_Usuario_Click();
    });
    $('#Btn_Cancelar_Usuario').click(function (event) {
        event.preventDefault();
        Btn_Cancelar_Usuario_Click();
    });
}
//Inicio Solicitudes de Contratos -------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------
function AutocompletadoSolicitudesZonasFiltro() {
    $('#Txt_Busqueda_Zona_Hidden').val('');
    //Autocompletado de las Zonas
    $('#Txt_Busqueda_Solicitud_Zona').autocomplete("../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
        extraParams: { Accion: 'autocompletar_zona' },
        dataType: "json",
        selectOnly: true,
        minChars: 1,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.zona_id,
                    result: row.zona
                }
            });
        },
        formatItem: function (item) {
            return item.zona;
        }
    }).result(function (e, item) {
        var zona_id = item.zona_id;
        $('#Txt_Busqueda_Zona_Hidden').val(zona_id);
    });
}
function AutocompletadoSolicitudesGirosFiltro() {
    $('#Txt_Busqueda_Solicitud_Giro_Hidden').val('');
    //Autocompletado de los giros
    $('#Txt_Busqueda_Solicitud_Giro').autocomplete("../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
        extraParams: { Accion: 'autocompletar_giros' },
        dataType: "json",
        selectOnly: true,
        minChars: 2,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.giro_id,
                    result: row.giro
                }
            });
        },
        formatItem: function (item) {
            return item.giro;
        }
    }).result(function (e, item) {
        var giro_id = item.giro_id;
        $('#Txt_Busqueda_Solicitud_Giro_Hidden').val(giro_id);
    });
}
function AutocompletadoSolicitudesColoniaFiltro() {
    $('#Txt_Busqueda_Solicitud_Colonia_Hidden').val('');
    //Autocompletado de las Colonias
    $('#Txt_Busqueda_Solicitud_Colonia').autocomplete("../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
        extraParams: { Accion: 'autocompletar_colonia' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.colonia_id,
                    result: row.colonia
                }
            });
        },
        formatItem: function (item) {
            return item.colonia;
        }
    }).result(function (e, item) {
        var colonia_id = item.colonia_id;
        $('#Txt_Busqueda_Solicitud_Colonia_Hidden').val(colonia_id);
    });
}
function AutocompletadoSolicitudesCalleFiltro(colonia_id) {
    $('#Txt_Busqueda_Solicitud_Calle_Hidden').val('');
    //Autocompletado de las calles por colonia seleccionada
    $('#Txt_Busqueda_Solicitud_Calle').autocomplete(
        "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
            selectOnly: true,
            minChars: 3,
            extraParams: {
                Accion: 'autocompletar_calle', colonia_id: colonia_id
            },
            dataType: "json",
            parse: function (data) {
                return $.map(data, function (row) {
                    return {
                        data: row,
                        value: row.calle_id,
                        result: row.calle
                    }
                });
            },
            formatItem: function (item) {
                return item.calle; // format(item);
            }
        }).result(function (e, item) {
            var calle_id = item.calle_id;
            $('#Txt_Busqueda_Solicitud_Calle_Hidden').val(calle_id);
        });
}
function Obtnener_Parametros_Filtro_Solicitud() {
    var parametros = "";
    //Obtiene los filtros para mandarlos a consulta
    //No Solicitud
    parametros += "&Folio=" + $('#Txt_Busqueda_Solicitud_Folio').val();
    //Estatus
    parametros += "&Estatus=" + $('#Cmb_Busqueda_Solicitud_Estatus').val(); ;
    //Giro
    parametros += "&Giro_ID=" + $('#Txt_Busqueda_Solicitud_Giro_Hidden').val();
    //Colonia
    parametros += "&Colonia_ID=" + $('#Txt_Busqueda_Solicitud_Colonia_Hidden').val();
    //Calle
    parametros += "&Calle_ID=" + $('#Txt_Busqueda_Solicitud_Calle_Hidden').val();
    //Numero Exterior
    parametros += "&Numero_Exterior=" + $('#Txt_Busqueda_Solicitud_Numero_Exterior').val();
    //Numero Interior
    parametros += "&Numero_Interior=" + $('#Txt_Busqueda_Solicitud_Numero_Interior').val();
    //Solicito
    parametros += "&Solicito=" + $('#Txt_Busqueda_Solicitud_Solicito').val();
    //Fecha_Inicio
    parametros += "&Fecha_Inicio=" + $('[id$=Txt_Busqueda_Solicitud_Fecha_Inicio]').val();
    //Fecha_Termino
    parametros += "&Fecha_Termino=" + $('[id$=Txt_Busqueda_Solicitud_Fecha_Termino]').val();


    return parametros;
}
function Filtrar_Solicitudes() {
    //No registros por pagina
    $('#Grid_Solicitudes').datagrid({
        url: '../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=Filtrar_Solicitudes' + Obtnener_Parametros_Filtro_Solicitud(),
        pageNumber: 1
    });
}

//Termino Solicitudes de Contratos -------------------------------------------------------------------------------------------------------------

//Inicio Catalogo de Predios -------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------

//Consulta de Información-------------------------------------------------------------------------------
function Obtnener_Parametros_Filtro() {
    var parametros = "";
    //Obtiene los filtros para mandarlos a consulta
    //Predio_ID
    parametros += "&Predio_ID=";
    //Estatus
    parametros += "&Estatus=CONTRATO";
    //Zona
    parametros += "&Zona_ID=" + $('#Txt_Busqueda_Zona_Alta_Hidden').val();
    //Giro
    parametros += "&Giro_ID=" + $('#Txt_Busqueda_Giro_Alta_Hidden').val();
    //Colonia
    parametros += "&Colonia_ID=" + $('#Txt_Busqueda_Colonia_Alta_Hidden').val();
    //Calle
    parametros += "&Calle_ID=" + $('#Txt_Busqueda_Calle_Alta_Hidden').val();
    //Fraccionador
    parametros += "&Fraccionador_ID=" + $('#Txt_Busqueda_Fraccionador_Alta_Hidden').val();
    //Numero Exterior
    parametros += "&Numero_Exterior=" + $('#Txt_Busqueda_Numero_Exterior_Alta').val();
    //Numero Interior
    parametros += "&Numero_Interior=" + $('#Txt_Busqueda_Numero_Interior_Alta').val();

    return parametros;
}
function Filtrar_Predios() {
    //No registros por pagina
    $('#Grid_Predios').datagrid({
        url: '../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=Filtrar_Predios' + Obtnener_Parametros_Filtro(),
        pageNumber: 1
    });
}
//Configuración de elementos en eventso---------------------------------------------------------------------------------
function Configurar_Botones_Alta_Predio(modo) {
    try {
        switch (modo) {
            case "Inicial":
                $('#Btn_Guardar_Predio').removeAttr('disabled');
                $('#Btn_Guardar_Predio').attr('src', '../imagenes/paginas/icono_nuevo.png');
                $('#Btn_Cancelar_Predio').attr('src', '../imagenes/paginas/icono_salir.png');
                $('#Btn_Guardar_Predio').attr('alt', 'Nuevo');
                $('#Btn_Cancelar_Predio').attr('alt', 'Salir');
                $('#Filtros_Predios').show();
                $('#Alta_Predios').hide();
                //Da formato a los tabs
                $('.tabs-header').removeAttr('style');
                $('.tabs-header').attr('style', 'width:100%;');
                $('.tabs-panels').removeAttr('style');
                $('.tabs-panels').attr('style', 'width:100%; height:400px;');
                $('.tabs-container').removeAttr('style');
                $('.tabs-container').attr('style', 'width:100%; height:400px;');
                $('.tabs-wrap').removeAttr('style');
                $('.tabs-wrap').attr('style', 'margin-left:0px; left:0px; width:100%;');
                $('.panel-body').removeAttr('style');
                $('.panel-body').attr('style', 'padding: 10px; width: 98%; height: auto;');
                $('.panel-header').removeAttr('style');
                $('.panel-header').attr('style', 'width: 838px;');
                $('#Modal_Predios').removeAttr('style');
                $('#Modal_Predios').attr('style', 'left:50%; top:15%; width:850px; margin-left:-425px;');
                $('.datagrid-view').removeAttr('style');
                $('.datagrid-view').attr('style', 'width: 848px; height: 191px;');
                $('.datagrid-view1').removeAttr('style');
                $('.datagrid-view1').attr('style', 'width: 26px');
                $('.datagrid-view2').removeAttr('style');
                $('.datagrid-view2').attr('style', 'width: 822px; left: 26px;');
                $('#Lbl_Mensaje_Error_Predio').text('');
                $('#Img_Error_Predio').hide();
                //Configuracion_Acceso("Frm_Cat_Cor_Medidores.aspx");
                break;

            case "Nuevo":
                $('#Btn_Guardar_Predio').removeAttr('disabled');
                $('#Btn_Guardar_Predio').attr('src', '../imagenes/paginas/icono_guardar.png');
                $('#Btn_Guardar_Predio').attr('alt', 'Guardar');
                $('#Btn_Cancelar_Predio').attr('src', '../imagenes/paginas/icono_cancelar.png');
                $('#Btn_Cancelar_Predio').attr('alt', 'Cancelar');
                $('#Filtros_Predios').hide();
                $('#Alta_Predios').show();
                

                break;

            default: break;
        }

    }
    catch (ex) {
        //Error
        $("#Lbl_Mensaje_Error_Predio").removeAttr('style');
        $("#Lbl_Mensaje_Error_Predio").attr('style', 'color: rgb(153, 0, 0); display: inline;');
        $("#Lbl_Mensaje_Error_Predio").val(ex.Description);
        $("#Img_Error_Predio").show();
    }
    finally {
    }
}
function Limpia_Campos_Predios() {
    $('.altaPredio').val('Seleccione');
    $('.altaPredio').val('');
    $('.filtros').val('');
    $('.requerido').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
    $('#Txt_Numero_Exterior_Alta').attr('style', 'width:30%; border-style:dotted; border-color:Red;');
    //QUita de los combos las opciones
    $('.disabled').attr('disabled', 'disabled');
    $('.reset').find('option')
        .remove()
        .end()
        .append('<option value=""></option>')
        .val('');
    $('.Primer_Elemento').val($('.Primer_Elemento option:first').val());
    var select = $('#Cmb_Manzana_Alta');
    $('option', select).remove();
    //$("#Generales:input").val('disabled', false);
}
function reestablece_controles_ubicacion() {
    //Predio
    $('#Cmb_Calle_Referencia1_Alta').removeAttr('style');
    $('#Cmb_Calle_Referencia1_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
    $('#Cmb_Calle_Referencia1_Alta').attr('disabled', 'disabled');
    $('#Cmb_Calle_Referencia1_Alta').val('Seleccione');
    $('#Cmb_Calle_Referencia2_Alta').removeAttr('style');
    $('#Cmb_Calle_Referencia2_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
    $('#Cmb_Calle_Referencia2_Alta').attr('disabled', 'disabled');
    $('#Cmb_Calle_Referencia2_Alta').val('Seleccione');
    $('#Cmb_Calle_Alta').removeAttr('style');
    $('#Cmb_Calle_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
    $('#Cmb_Calle_Alta').attr('disabled', 'disabled');
    $('#Cmb_Calle_Alta').val('Seleccione');
    $('#Cmb_Manzana_Alta').removeAttr('style');
    $('#Cmb_Manzana_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
    $('#Cmb_Manzana_Alta').attr('disabled', 'disabled');
    $('#Cmb_Manzana_Alta').val('Seleccione');
    //Toma
    $('#Cmb_Calle_Referencia_1_Toma_Alta').removeAttr('style');
    $('#Cmb_Calle_Referencia_1_Toma_Alta').attr('style', 'width:98%;');
    $('#Cmb_Calle_Referencia_1_Toma_Alta').attr('disabled', 'disabled');
    $('#Cmb_Calle_Referencia_1_Toma_Alta').val('Seleccione');
    $('#Cmb_Calle_Referencia_2_Toma_Alta').removeAttr('style');
    $('#Cmb_Calle_Referencia_2_Toma_Alta').attr('style', 'width:98%;');
    $('#Cmb_Calle_Referencia_2_Toma_Alta').attr('disabled', 'disabled');
    $('#Cmb_Calle_Referencia_2_Toma_Alta').val('Seleccione');
    $('#Cmb_Calle_Toma_Alta').removeAttr('style');
    $('#Cmb_Calle_Toma_Alta').attr('style', 'width:98%;');
    $('#Cmb_Calle_Toma_Alta').attr('disabled', 'disabled');
    $('#Cmb_Calle_Toma_Alta').val('Seleccione');
}
//autocompletado de controles para filtro-------------------------------------------------------------------------------
function AutocompletadoColoniasFiltro() {
    $('#Txt_Busqueda_Colonia_Alta_Hidden').val('');
    //Autocompletado de las Colonias
    $('#Txt_Busqueda_Colonia_Alta').autocomplete("../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
        extraParams: { Accion: 'autocompletar_colonia' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.colonia_id,
                    result: row.colonia
                }
            });
        },
        formatItem: function (item) {
            return item.colonia;
        }
    }).result(function (e, item) {
        var colonia_id = item.colonia_id;
        $('#Txt_Busqueda_Colonia_Alta_Hidden').val(colonia_id);
    });
}
function AutocompletadoCallesFiltro(colonia_id) {
    $('#Txt_Busqueda_Calle_Alta_Hidden').val('');
    //Autocompletado de las calles por colonia seleccionada
    $('#Txt_Busqueda_Calle_Alta').autocomplete(
        "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
            selectOnly: true,
            minChars: 3,
            extraParams: {
                Accion: 'autocompletar_calle', colonia_id: colonia_id
            },
            dataType: "json",
            parse: function (data) {
                return $.map(data, function (row) {
                    return {
                        data: row,
                        value: row.calle_id,
                        result: row.calle
                    }
                });
            },
            formatItem: function (item) {
                return item.calle; // format(item);
            }
        }).result(function (e, item) {
            var calle_id = item.calle_id;
            $('#Txt_Busqueda_Calle_Alta_Hidden').val(calle_id);
            //alert("Calle_ID_" + $('#Txt_Busqueda_Calle_Hidden').val());
        });
}
function AutocompletadoZonasFiltro() {
    $('#Txt_Busqueda_Zona_Alta_Hidden').val('');
    //Autocompletado de las Zonas
    $('#Txt_Busqueda_Zona_Alta').autocomplete("../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
        extraParams: { Accion: 'autocompletar_zona' },
        dataType: "json",
        selectOnly: true,
        minChars: 1,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.zona_id,
                    result: row.zona
                }
            });
        },
        formatItem: function (item) {
            return item.zona;
        }
    }).result(function (e, item) {
        var zona_id = item.zona_id;
        $('#Txt_Busqueda_Zona_Alta_Hidden').val(zona_id);
    });
}
function AutocompletadoGirosFiltro() {
    $('#Txt_Busqueda_Giro_Alta_Hidden').val('');
    //Autocompletado de los giros
    $('#Txt_Busqueda_Giro_Alta').autocomplete("../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
        extraParams: { Accion: 'autocompletar_giros' },
        dataType: "json",
        selectOnly: true,
        minChars: 2,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.giro_id,
                    result: row.giro
                }
            });
        },
        formatItem: function (item) {
            return item.giro;
        }
    }).result(function (e, item) {
        var giro_id = item.giro_id;
        $('#Txt_Busqueda_Giro_Alta_Hidden').val(giro_id);
    });
}
function AutocompletadoFraccionadorFiltro() {
    $('#Txt_Busqueda_Fraccionador_Alta_Hidden').val('');
    //Autocompletado de los fraccionadores
    $('#Txt_Busqueda_Fraccionador_Alta').autocomplete("../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
        extraParams: { Accion: 'autocompletar_fraccionador' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.constructora_id,
                    result: row.constructora
                }
            });
        },
        formatItem: function (item) {
            return item.constructora;
        }
    }).result(function (e, item) {
        var constructora_id = item.constructora_id;
        $('#Txt_Busqueda_Fraccionador_Alta_Hidden').val(constructora_id);
    });
}
//Formulario de Predios, configuraciones de autocompletado y llenado de información -----------------------------------
function AutocompletadoColonias() {
    //Coloca los valores default a las calles  y datos de la toma
    $('#Txt_Colonia_Alta_Hidden').val('');
    $('#Txt_Colonia_Alta').attr('style', 'width:98%; border-style:dotted; border-color:Red;');

    //Autocompletado de las Colonias
    $('#Txt_Colonia_Alta').autocomplete("../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
        extraParams: { Accion: 'autocompletar_colonia' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.colonia_id,
                    result: row.colonia
                }
            });
        },
        formatItem: function (item) {
            return item.colonia;
        }
    }).result(function (e, item) {
        reestablece_controles_ubicacion();
        var colonia_id = item.colonia_id;
        var colonia = item.colonia;
        $('#Txt_Colonia_Alta_Hidden').val(colonia_id);
        //Quita el formato de requerido
        $('#Txt_Colonia_Alta').removeAttr('style');
        $('#Txt_Colonia_Alta').attr('style', 'width:98%;');
        //Habilita las calles
        $('#Cmb_Calle_Alta').removeAttr('disabled');
        Cargar_Calles(colonia_id);
        //Habilita y coloca la misma colonia para la toma
        AutocompletadoColoniasToma();
        $('#Txt_Colonia_Toma_Alta').val(colonia);
        $('#Txt_Colonia_Toma_Alta_Hidden').val(colonia_id);
        //Habilita las calles para la toma y llena el combo
        $('#Cmb_Calle_Toma_Alta').removeAttr('disabled');
        Cargar_Calles_Toma(colonia_id);
        Obtener_Zona_Colonia(colonia_id);
    });
}
function AutocompletadoColoniasToma() {
    $('#Txt_Colonia_Toma_Alta_Hidden').val('');
    //Autocompletado de las Colonias
    $('#Txt_Colonia_Toma_Alta').autocomplete("../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
        extraParams: { Accion: 'autocompletar_colonia' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.colonia_id,
                    result: row.colonia
                }
            });
        },
        formatItem: function (item) {
            return item.colonia;
        }
    }).result(function (e, item) {
        var colonia_id = item.colonia_id;
        var colonia = item.colonia;
        $('#Txt_Colonia_Toma_Alta_Hidden').val(colonia_id);
        Cargar_Calles_Toma(colonia_id);
    });
}
function Cargar_Grupos_Conceptos() {
    var select = $('#Cmb_Grupos_Conceptos');
    $('option', select).remove();
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=consulta_grupos_conceptos",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.grupo_concepto_id + '">' + item.grupo_concepto + '</option>';
                });
                $('#Cmb_Grupos_Conceptos').append(options);
            }
        }
    });
}
function Cargar_Calles(colonia_id) {
    var select = $('#Cmb_Calle_Alta');
    $('option', select).remove();

    $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=autocompletar_calle&colonia_id=" + colonia_id,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.calle_id + '">' + item.calle + '</option>';
                });
                $('#Cmb_Calle_Alta').append(options);
            }
        }
    });
}
function Cargar_Calles_Toma(colonia_id) {
    var select = $('#Cmb_Calle_Toma_Alta');
    $('option', select).remove();

    $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=autocompletar_calle&colonia_id=" + colonia_id,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.calle_id + '">' + item.calle + '</option>';
                });
                $('#Cmb_Calle_Toma_Alta').append(options);
            }
        }
    });
}
function Cargar_Calles_Referencia() {
    var select1 = $('#Cmb_Calle_Referencia1_Alta');
    var select2 = $('#Cmb_Calle_Referencia2_Alta');
    $('option', select1).remove();
    $('option', select2).remove();
    var colonia_id = $('#Txt_Colonia_Alta_Hidden').val();
    var calle_id = $("#Cmb_Calle_Alta option:selected").val();
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=autocompletar_calle_referencia&calle_id=" + calle_id + "&colonia_id=" + colonia_id,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.calle_id + '">' + item.calle + '</option>';
                });
                $('#Cmb_Calle_Referencia1_Alta').append(options);
                $('#Cmb_Calle_Referencia2_Alta').append(options);
            }
        }
    });
}
function Cargar_Manzanas() {
    var colonia_id = $('#Txt_Colonia_Alta_Hidden').val();
    var calle_id = $('#Cmb_Calle_Alta option:selected').val();
    var calleref1 = $('#Cmb_Calle_Referencia1_Alta option:selected').val();
    var calleref2 = $('#Cmb_Calle_Referencia2_Alta option:selected').val();

    var select = $('#Cmb_Manzana_Alta');
    $('option', select).remove();


    $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=consulta_manzanas" +
            "&calle_id=" + calle_id +
            "&colonia_id=" + colonia_id +
            "&calleref_id1=" + calleref1 +
            "&calleref_id2=" + calleref2,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.manzana_id + '">' + item.manzana + '</option>';
                });
                $('#Cmb_Manzana_Alta').append(options);
            }
        }
    });
}
function Cargar_Calles_Referencia_Toma() {
    var select1 = $('#Cmb_Calle_Referencia_1_Toma_Alta');
    var select2 = $('#Cmb_Calle_Referencia_2_Toma_Alta');
    $('option', select1).remove();
    $('option', select2).remove();
    var colonia_id = $('#Txt_Colonia_Toma_Alta_Hidden').val();
    var calle_id = $("#Cmb_Calle_Toma_Alta option:selected").val();
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=autocompletar_calle_referencia&calle_id=" + calle_id + "&colonia_id=" + colonia_id,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.calle_id + '">' + item.calle + '</option>';
                });
                $('#Cmb_Calle_Referencia_1_Toma_Alta').append(options);
                $('#Cmb_Calle_Referencia_2_Toma_Alta').append(options);
                $('#Cmb_Calle_Referencia_1_Toma_Alta').removeAttr('disabled');
                $('#Cmb_Calle_Referencia_2_Toma_Alta').removeAttr('disabled');
            }
        }
    });
}
function Cargar_Region() {
    var select = $('#Cmb_Region_Alta');
    $('option', select).remove();
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=consulta_region",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            //var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            var options = '';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.region_id + '">' + item.region + '</option>';
                });
                $('#Cmb_Region_Alta').append(options);
            }
        }
    });
    //Selecciona el item de SIN REGION
    $('#Cmb_Region_Alta option:eq(0)').attr("selected","selected");
    Cargar_Sector();
    $('#Cmb_Sector_Alta').removeAttr('disabled');
}
function Cargar_Sector() {
    var select = $('#Cmb_Sector_Alta');
    $('option', select).remove();
    var region_id = $('#Cmb_Region_Alta option:selected').val();
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=consulta_sector&region_id=" + region_id,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            //var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            var options = '';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.sector_id + '">' + item.sector + '</option>';
                });
                $('#Cmb_Sector_Alta').append(options);
            }
        }
    });
    //Selecciona el item de SIN REGION
    $('#Cmb_Sector_Alta option:eq(0)').attr("selected","selected");
}
function AutocompletadoZonas() {
    $('#Txt_Zona_Alta_Hidden').val('');
    //Autocompletado de las Zonas
    $('#Txt_Zona_Alta').autocomplete("../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
        extraParams: { Accion: 'autocompletar_zona' },
        dataType: "json",
        selectOnly: true,
        minChars: 1,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.zona_id,
                    result: row.zona
                }
            });
        },
        formatItem: function (item) {
            return item.zona;
        }
    }).result(function (e, item) {
        var zona_id = item.zona_id;
        $('#Txt_Zona_Alta').removeAttr('style');
        $('#Txt_Zona_Alta').attr('style', 'width:98%;');
        $('#Txt_Zona_Alta_Hidden').val(zona_id);
    });
}
function AutocompletadoGiros() {
    $('#Txt_Giro_Alta_Hidden').val('');
    //Autocompletado de los giros
    $('#Txt_Giro_Alta').autocomplete("../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
        extraParams: { Accion: 'autocompletar_giros' },
        dataType: "json",
        selectOnly: true,
        minChars: 1,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.giro_id,
                    result: row.giro
                }
            });
        },
        formatItem: function (item) {
            return item.giro;
        }
    }).result(function (e, item) {
        var giro_id = item.giro_id;
        $('#Txt_Giro_Alta').removeAttr('style');
        $('#Txt_Giro_Alta').attr('style', 'width:98%;');
        $('#Txt_Giro_Alta_Hidden').val(giro_id);
    });
}
function AutocompletadoTarifas() {
    $('#Txt_Tarifa_Alta_Hidden').val('');
    //Autocompletado de las tarifas
    $('#Txt_Tarifa').autocomplete("../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
        extraParams: { Accion: 'autocompletar_tarifa' },
        dataType: "json",
        selectOnly: true,
        minChars: 1,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.tarifa_id,
                    result: row.tarifa
                }
            });
        },
        formatItem: function (item) {
            return item.tarifa;
        }
    }).result(function (e, item) {
        var tarifa_id = item.tarifa_id;
        $('#Txt_Tarifa_Alta').removeAttr('style');
        $('#Txt_Tarifa_Alta').attr('style', 'width:98%;');
        $('#Txt_Tarifa_Alta_Hidden').val(tarifa_id);
    });
}
function AutocompletadoFraccionador() {
    $('#Txt_Fraccionador_Alta_Hidden').val('');
    //Autocompletado de los fraccionadores
    $('#Txt_Fraccionador_Alta').autocomplete("../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
        extraParams: { Accion: 'autocompletar_fraccionador' },
        dataType: "json",
        selectOnly: true,
        minChars: 1,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.constructora_id,
                    result: row.constructora
                }
            });
        },
        formatItem: function (item) {
            return item.constructora;
        }
    }).result(function (e, item) {
        var constructora_id = item.constructora_id;
        $('#Txt_Fraccionador_Alta').removeAttr('style');
        $('#Txt_Fraccionador_Alta').attr('style', 'width:98%;');
        $('#Txt_Fraccionador_Alta_Hidden').val(constructora_id);
    });
}
function AutocompletadoViviendas() {
    $('#Txt_Tipo_Vivienda_Alta_Hidden').val('');
    //Autocompletado de los tipos de vivienda
    $('#Txt_Tipo_Vivienda_Alta').autocomplete("../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
        extraParams: { Accion: 'autocompletar_tipo_vivienda' },
        dataType: "json",
        selectOnly: true,
        minChars: 1,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.tipo_vivienda_id,
                    result: row.tipo
                }
            });
        },
        formatItem: function (item) {
            return item.tipo;
        }
    }).result(function (e, item) {
        var tipo_vivienda_id = item.tipo_vivienda_id;
        $('#Txt_Tipo_Vivienda_Alta').removeAttr('style');
        $('#Txt_Tipo_Vivienda_Alta').attr('style', 'width:98%;');
        $('#Txt_Tipo_Vivienda_Alta_Hidden').val(tipo_vivienda_id);
    });
}
function Obtener_Zona_Colonia(colonia_id) {
    //Realiza la peticion de la zona por la colinia proporcionada
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=consulta_colonia_zona&Colonia_ID=" + colonia_id,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $('#Txt_Zona_Alta').val(data[0].zona);
                $('#Txt_Zona_Alta_Hidden').val(data[0].zona_id);
                $('#Txt_Zona_Alta').attr('readonly', 'readonly');
                $('#Txt_Zona_Alta').removeAttr('style');
                $('#Txt_Zona_Alta').attr('style', 'width:98%;');
                Asigna_Tarifa();
            }
        }
    });
}
function Cargar_Giros() {
    var select = $('#Cmb_Giros_Alta');
    $('option', select).remove();

    $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=autocompletar_giros",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.giro_id + '">' + item.giro + '</option>';
                });
                $('#Cmb_Giros_Alta').append(options);
            }
        }
    });
}
function Cargar_Giros_Actividad(giro_id) {
    var select = $('#Cmb_Actividad_Alta');
    $('option', select).remove();

    $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=consulta_actividades&giro_id=" + giro_id,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.actividad_id + '">' + item.actividad + '</option>';
                });
                $('#Cmb_Actividad_Alta').append(options);
                $('#Cmb_Actividad_Alta').removeAttr('disabled');
            }
        }
    });
}
function Cargar_Tipos_Vivienda() {
    var select = $('#Cmb_Tipo_Vivienda_Alta');
    $('option', select).remove();

    $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=consulta_tipo_vivienda",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.tipo_vivienda_id + '">' + item.tipo + '</option>';
                });
                $('#Cmb_Tipo_Vivienda_Alta').append(options);
            }
        }
    });
}
function Cargar_Tarifas() {
    var select = $('#Cmb_Tarifa_Alta');
    $('option', select).remove();

    $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=consulta_tarifas",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.tarifa_id + '">' + item.tarifa + '</option>';
                });
                $('#Cmb_Tarifa_Alta').append(options);
            }
        }
    });
}
function Cargar_Condicion() {
    var select = $('#Cmb_Condicion_Alta');
    $('option', select).remove();

    $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=consulta_condiciones",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.condicion_id + '">' + item.condicion + '</option>';
                });
                $('#Cmb_Condicion_Alta').append(options);
            }
        }
    });
}
function Cargar_Materiales() {
    var select = $('#Cmb_Material_Banqueta_Alta');
    $('option', select).remove();

    $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=consulta_materiales",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.material_id + '">' + item.material + '</option>';
                });
                $('#Cmb_Material_Banqueta_Alta').append(options);
            }
        }
    });
}
//Asigna la tarifa de acuerdo a la zona y el giro
function Asigna_Tarifa() {
    //obtiene la zona
    var zona_id = $('#Txt_Zona_Alta_Hidden').val();
    var giro_id = $("#Cmb_Giros_Alta option:selected").val();
    //Consulta la tarifa que le corresponderia
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=consulta_tarifasxzonagiro&zona_id=" + zona_id + "&giro_id=" + giro_id,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null && data.length > 0) {
                $("#Cmb_Tarifa_Alta").val(data[0].tarifa_id);
                $('#Cmb_Tarifa_Alta').removeAttr('style');
                $('#Cmb_Tarifa_Alta').attr('style', 'width:98%;');
            }
        }
    });
}
//Alta del Predio------------------------------------------------------------------------------------------------------------------------------------------------------------
function Btn_Guardar_Predio_Click() {
    $("#Lbl_Mensaje_Error_Predio").text('');
    $("#Lbl_Mensaje_Error_Predio").hide();
    $("#Img_Error_Predio").hide();

    var Btn_Nuevo = $("#Btn_Guardar_Predio");
    //Verificar el tooltip del boton
    if (Btn_Nuevo.attr('alt') == "Nuevo") {
        //Habilitar y limpiar
        Configurar_Botones_Alta_Predio("Nuevo");
        Limpia_Campos_Predios();
        $('#Txt_No_Reparto_Alta').val('99999');
    }
    else {
        Alta_Predios();
    }
}
function Alta_Predios() {
    //recupera la información de que se enviará al servidor para validar el alta

    var requeridos = Valida_Datos_Requeridos_Predio();
    if (requeridos == "") {
        //Recupera la información a enviar
        var registro = $('.altaPredio');

        //envia la información al servidor
        $.ajax({
            url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=alta_predio&" + registro.serialize(),
            type: 'POST',
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    //$.each(data, function (i, item) {
                    var error = data[2].error;
                    var predio_id = data[0].predio_id;
                    var alta = data[1].alta; ;
                    if (error != "") {
                        $("#Lbl_Mensaje_Error_Predio").removeAttr('style');
                        $("#Lbl_Mensaje_Error_Predio").attr('style', 'color: rgb(153, 0, 0); display: inline;');
                        $("#Lbl_Mensaje_Error_Predio").text(error);
                        $("#Lbl_Mensaje_Error_Predio").show();
                        $("#Img_Error_Predio").show();
                    }
                    else {
                        $("#Lbl_Mensaje_Error_Predio").removeAttr('style');
                        $("#Lbl_Mensaje_Error_Predio").attr('style', 'color: rgb(153, 0, 0); display: inline;');
                        $("#Lbl_Mensaje_Error_Predio").text(alta);
                        $("#Lbl_Mensaje_Error_Predio").show();
                        $("#Img_Error_Predio").show();
                        //Se ingresan los datos del predio en la solicitud.
                        $('[id$=Txt_Predio_ID]').val(predio_id);
                        $('[id$=Txt_Colonia]').val($('#Txt_Colonia_Alta').val());
                        $('[id$=Txt_Calle]').val($('#Cmb_Calle_Alta option:selected').text());
                        $('[id$=Txt_No_Exterior]').val($('#Txt_Numero_Exterior_Alta').val());
                        $('[id$=Txt_No_Interior]').val($('#Txt_Numero_Interior_Alta').val());
                        $('[id$=Cmb_Giro]').val($('#Cmb_Giros_Alta option:selected').val());
                        Limpia_Campos_Predios();
                        //regresa a la pantalla principal
                        $('#Modal_Predios').delay(2000).jqmHide();
                    }
                }
            }
        });
    }
    else {
        $("#Lbl_Mensaje_Error_Predio").removeAttr('style');
        $("#Lbl_Mensaje_Error_Predio").attr('style', 'color: rgb(153, 0, 0); display: inline;');
        $("#Lbl_Mensaje_Error_Predio").text(requeridos);
        $("#Lbl_Mensaje_Error_Predio").show();
        $("#Img_Error_Predio").show();
    }
}
function Valida_Datos_Requeridos_Predio() {
    var opciones = $('.requerido');
    var mensaje = "";
    opciones.each(function (i, item) {
        if (item.type == "text") {
            if ($('#' + item.id + '').val() == "") {
                mensaje += item.name + ", ";
            }
        }
        if (item.type == "select-one") {
            var cmboption = $("#" + item.id + " option:selected").val();
            if (cmboption == "Seleccione") {
                mensaje += item.name + ", ";
            }
        }
    });
    if (mensaje != "") {
        mensaje = "Falta la siguiente información por llenar: " + mensaje;
    }
    return mensaje;
}

//Salir y Cancelar Alta -----------------------------------------------------------------------------------------------------------------------------------------------------------------
function Btn_Cancelar_Predio_Click() {
    var estatus = $("#Btn_Cancelar_Predio").attr('alt')

    Limpia_Campos_Predios();
    if (estatus == "Salir") {
        $('#Modal_Predios').jqmHide();
    }
    else {
        Configurar_Botones_Alta_Predio("Inicial");
        $('#Modal_Predios').removeAttr('style');
        $('#Modal_Predios').attr('style', 'left:50%; top:15%; width:850px; margin-left:-425px; display:block; z-index:3000;');
        $('#Modal_Predios').jqmShow();
    }
}

//Funciones adicionales para validaciones---------------------------------------------------------------------------------
function grados_a_dms_latidud() {
    var grados = $('#Txt_Latitud_Decimal_Alta').val();
    var D;
    var M;
    var S;
    if (grados < 0) {
        return;
    }
    //Obtiene los grados, la parte entera
    D = parseInt(grados);
    //De la parte decimal obtiene los minutos
    M = (grados - D) * 60;
    S = M;
    M = parseInt(M);
    //De la parte decimal restante + 60 para obtener los segundos
    S = (S - M) * 60;
    S = Math.round(S);
    //asigna los valores a la caja de texto respectiva
    $('#Txt_Latitud_GMS_Grados_Alta').val(D);
    $('#Txt_Latitud_GMS_Minutos_Alta').val(M);
    $('#Txt_Latitud_GMS_Segundos_Alta').val(S);
}
function grados_a_dms_longitud() {
    var grados = $('#Txt_Longitud_Decimal_Alta').val();
    var D;
    var M;
    var S;
    if (grados < 0) {
        return;
    }
    //Obtiene los grados, la parte entera
    D = parseInt(grados);
    //De la parte decimal obtiene los minutos
    M = (grados - D) * 60;
    S = M;
    M = parseInt(M);
    //De la parte decimal restante + 60 para obtener los segundos
    S = (S - M) * 60;
    S = Math.round(S);
    //asigna los valores a la caja de texto respectiva
    $('#Txt_Longitud_GMS_Grados_Alta').val(D);
    $('#Txt_Longitud_GMS_Minutos_Alta').val(M);
    $('#Txt_Longitud_GMS_Segundos_Alta').val(S);
}
function dms_a_grados_latidud() {
    var D = $('#Txt_Latitud_GMS_Grados_Alta').val();
    var M = $('#Txt_Latitud_GMS_Minutos_Alta').val();
    var S = $('#Txt_Latitud_GMS_Segundos_Alta').val();
    var grados;
    var segundos;
    if (D < 0) {
        return;
    }
    // Se forman los grados
    grados = parseInt(D);
    //Obtiene los segundos
    //Valida que los minutos tengan informacion
    if (isNumber(M)) {
        M = parseFloat(M);
    }
    else {
        M = 0;
    }
    if (isNumber(S)) {
        S = parseFloat(S);
    }
    else {
        S = 0;
    }
    
    segundos = (M * 60) + S;
    //Obtenemos la parte fraccionaria
    grados += (segundos / 3600)

    //asigna los valores a la caja de texto respectiva
    $('#Txt_Latitud_Decimal_Alta').val(grados);
}
function dms_a_grados_longitud() {
    var D = $('#Txt_Longitud_GMS_Grados_Alta').val();
    var M = $('#Txt_Longitud_GMS_Minutos_Alta').val();
    var S = $('#Txt_Longitud_GMS_Segundos_Alta').val();
    var grados;
    var segundos;
    if (D < 0) {
        return;
    }
    // Se forman los grados
    grados = parseInt(D);
    if (isNumber(M)) {
        M = parseFloat(M);
    }
    else {
        M = 0;
    }
    if (isNumber(S)) {
        S = parseFloat(S);
    }
    else {
        S = 0;
    }
    segundos = (M * 60) + S;
    //Obtenemos la parte fraccionaria
    grados += (segundos / 3600)

    //asigna los valores a la caja de texto respectiva
    $('#Txt_Longitud_Decimal_Alta').val(grados);
}
//Fin Catalogo de Predios -------------------------------------------------------------------------------------------------------------------
//Inicio Catalogo de Usuarios -------------------------------------------------------------------------------------------------------------------
//Consulta de Información-------------------------------------------------------------------------------
function Obtnener_Parametros_Filtro_Usuarios() {
    var parametros = "";
    //Obtiene los filtros para mandarlos a consulta
    //RFC
    parametros += "&RFC=" + $('#Txt_Busqueda_Rfc').val();
   //Razon Social
    parametros += "&Razon_Social=" + $('#Txt_Busqueda_Razon_Social').val();
    //Nombre
    parametros += "&Nombre=" + $('#Txt_Busqueda_Nombre').val();
    //Apellido Paterno
    parametros += "&Apellido_Paterno=" + $('#Txt_Busqueda_Apellido_Paterno').val();
    //Calle
    parametros += "&Calle=" + $('#Txt_Busqueda_Calle').val();
    //Numero Exterior
    parametros += "&Numero_Exterior=" + $('#Txt_Busqueda_No_Exterior').val();
    //Colonia
    parametros += "&Colonia=" + $('#Txt_Busqueda_Colonia').val();
    //Ciudad
    parametros += "&Ciudad_ID=" + $('#Txt_Busqueda_Ciudad_Hidden').val();
    return parametros;
}
function Filtrar_Usuarios() {
    //No registros por pagina
    $('#Grid_Usuarios').datagrid({
        url: '../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=Filtrar_Usuarios' + Obtnener_Parametros_Filtro_Usuarios(),
        pageNumber: 1
    });
}
//Configuración de elementos en eventso---------------------------------------------------------------------------------
function Configurar_Botones_Alta_Usuario(modo) {
    try {
        switch (modo) {
            case "Inicial":
                $('#Btn_Guardar_Usuario').removeAttr('disabled');
                $('#Btn_Guardar_Usuario').attr('src', '../imagenes/paginas/icono_nuevo.png');
                $('#Btn_Cancelar_Usuario').attr('src', '../imagenes/paginas/icono_salir.png');
                $('#Btn_Guardar_Usuario').attr('alt', 'Nuevo');
                $('#Btn_Cancelar_Usuario').attr('alt', 'Salir');
                $('#Filtros_Usuarios').show();
                $('#Alta_Usuarios').hide();
                //Da formato a los tabs
                $('#Modal_Usuarios').removeAttr('style');
                $('#Modal_Usuarios').attr('style', 'left:50%; top:15%; width:850px; margin-left:-425px;');
//                $('.datagrid-view').removeAttr('style');
//                $('.datagrid-view').attr('style', 'width: 848px; height: 191px;');
//                $('.datagrid-view1').removeAttr('style');
//                $('.datagrid-view1').attr('style', 'width: 26px');
//                $('.datagrid-view2').removeAttr('style');
//                $('.datagrid-view2').attr('style', 'width: 822px; left: 26px;');
                $('#Lbl_Mensaje_Error_Usuario').text('');
                $('#Img_Error_Usuario').hide();
                //Configuracion_Acceso("Frm_Cat_Cor_Medidores.aspx");
                break;

            case "Nuevo":
                $('#Btn_Guardar_Usuario').removeAttr('disabled');
                $('#Btn_Guardar_Usuario').attr('src', '../imagenes/paginas/icono_guardar.png');
                $('#Btn_Guardar_Usuario').attr('alt', 'Guardar');
                $('#Btn_Cancelar_Usuario').attr('src', '../imagenes/paginas/icono_cancelar.png');
                $('#Btn_Cancelar_Usuario').attr('alt', 'Cancelar');
                $('#Filtros_Usuarios').hide();
                $('#Alta_Usuarios').show();
                

                break;

            default: break;
        }

    }
    catch (ex) {
        //Error
        $("#Lbl_Mensaje_Error_Usuario").removeAttr('style');
        $("#Lbl_Mensaje_Error_Usuario").attr('style', 'color: rgb(153, 0, 0); display: inline;');
        $("#Lbl_Mensaje_Error_Usuario").val(ex.Description);
        $("#Img_Error_Usuario").show();
    }
    finally {
    }
}
function Limpia_Campos_Usuarios() {
    $('.altaUsuario').val('Seleccione');
    $('.altaUsuario').val('');
    $('.filtros').val('');
    $('.requeridoUsuario').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
}
function AutocompletadoNombreUsuariosFiltros() {
    //Autocompletado de los Nombres de usuarios
    $('#Txt_Busqueda_Nombre').autocomplete("../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
        extraParams: { Accion: 'autocompletar_nombre_usuarios' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.usuario_id,
                    result: row.nombre
                }
            });
        },
        formatItem: function (item) {
            return item.nombre;
        }
    }).result(function (e, item) {
        var nombre = item.nombre;
    });
}
function AutocompletadoApellidoPaternoUsuariosFiltros() {
    //Autocompletado de los Nombres de usuarios
    $('#Txt_Busqueda_Apellido_Paterno').autocomplete("../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
        extraParams: { Accion: 'autocompletar_appaterno_usuarios' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.usuario_id,
                    result: row.apellido_paterno
                }
            });
        },
        formatItem: function (item) {
            return item.apellido_paterno;
        }
    }).result(function (e, item) {
        var nombre = item.apellido_paterno;
    });
}
function AutocompletadoCalleUsuariosFiltros() {
    //Autocompletado de los Nombres de usuarios
    $('#Txt_Busqueda_Calle').autocomplete("../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
        extraParams: { Accion: 'autocompletar_calle_usuarios' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.usuario_id,
                    result: row.calle
                }
            });
        },
        formatItem: function (item) {
            return item.calle;
        }
    }).result(function (e, item) {
        var nombre = item.calle;
    });
}
function AutocompletadoColoniaUsuariosFiltros() {
    //Autocompletado de los Nombres de usuarios
    $('#Txt_Busqueda_Colonia').autocomplete("../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
        extraParams: { Accion: 'autocompletar_colonia_usuarios' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.usuario_id,
                    result: row.colonia
                }
            });
        },
        formatItem: function (item) {
            return item.colonia;
        }
    }).result(function (e, item) {
        var nombre = item.colonia;
    });
}
function AutocompletadoCiudadUsuariosFiltros() {
    $('#Txt_Busqueda_Ciudad_Hidden').val('');
    //Autocompletado de los Nombres de usuarios
    $('#Txt_Busqueda_Ciudad').autocomplete("../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
        extraParams: { Accion: 'autocompletar_ciudad_usuarios' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.ciudad_id,
                    result: row.ciudad
                }
            });
        },
        formatItem: function (item) {
            return item.ciudad_id;
        }
    }).result(function (e, item) {
        var ciudad_id = item.ciudad_id;
        $('#Txt_Busqueda_Ciudad_Hidden').val(ciudad_id);
    });
}
//Autocompletado alta usuarios
function AutocompletadoNombreUsuarios() {
    //Autocompletado de los Nombres de usuarios
    $('#Txt_Nombre_Usuario_Alta').autocomplete("../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
        extraParams: { Accion: 'autocompletar_nombre_usuarios' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.usuario_id,
                    result: row.nombre
                }
            });
        },
        formatItem: function (item) {
            return item.nombre;
        }
    }).result(function (e, item) {
        var nombre = item.nombre;
    });
}
function AutocompletadoApellidoPaternoUsuarios() {
    //Autocompletado de los Nombres de usuarios
    $('#Txt_Apellido_Paterno_Usuario_Alta').autocomplete("../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
        extraParams: { Accion: 'autocompletar_appaterno_usuarios' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.usuario_id,
                    result: row.apellido_paterno
                }
            });
        },
        formatItem: function (item) {
            return item.apellido_paterno;
        }
    }).result(function (e, item) {
        var nombre = item.apellido_paterno;
    });
}
function AutocompletadoApellidoMaternoUsuarios() {
    //Autocompletado de los Nombres de usuarios
    $('#Txt_Apellido_Materno_Usuario').autocomplete("../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
        extraParams: { Accion: 'autocompletar_appaterno_usuarios' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.usuario_id,
                    result: row.apellido_paterno
                }
            });
        },
        formatItem: function (item) {
            return item.apellido_paterno;
        }
    }).result(function (e, item) {
        var nombre = item.apellido_paterno;
    });
}
function AutocompletadoColoniaUsuarios() {
    //Autocompletado de los Nombres de usuarios
    $('#Txt_Colonia_Usuario').autocomplete("../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
        extraParams: { Accion: 'autocompletar_colonia_usuarios1' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.colonia_id,
                    result: row.colonia
                }
            });
        },
        formatItem: function (item) {
            return item.colonia;
        }
    }).result(function (e, item) {
        var colonia_id = item.colonia_id;
        $('#Txt_Colonia_Usuario_Hidden').val(colonia_id);
        $('#Txt_Colonia_Usuario').removeAttr('style');
        $('#Txt_Colonia_Usuario').attr('style', 'width:98%;');
    });
}
function AutocompletadoCalleUsuarios() {
    //Autocompletado de los Nombres de usuarios
    var colonia_id = $('#Txt_Colonia_Usuario_Hidden').val();
    $('#Txt_Calle_Usuario').autocomplete("../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx", {
        extraParams: { Accion: 'autocompletar_calle_usuarios1', colonia_id: colonia_id },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function (data) {
            return $.map(data, function (row) {
                return {
                    data: row,
                    value: row.calle_id,
                    result: row.calle
                }
            });
        },
        formatItem: function (item) {
            return item.calle;
        }
    }).result(function (e, item) {
        var calle_id = item.calle_id;
        $('#Txt_Calle_Usuario_Hidden').val(calle_id);
        $('#Txt_Calle_Usuario').removeAttr('style');
        $('#Txt_Calle_Usuario').attr('style', 'width:98%;');
    });
}
function Cargar_Estados() {
    var select = $('#Cmb_Estado_Usuario');
    $('option', select).remove();

    $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=consulta_estados",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.estado_id + '">' + item.estado + '</option>';
                });
                $('#Cmb_Estado_Usuario').append(options);
            }
        }
    });
}
function Cargar_Ciudades() {
    var select = $('#Cmb_Ciudad_Usuario');
    $('option', select).remove();
    var Estado_ID = $('#Cmb_Estado_Usuario').val();
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=consulta_ciudades&Estado_Id=" + Estado_ID,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="Seleccione"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.ciudad_id + '">' + item.ciudad + '</option>';
                });
                $('#Cmb_Ciudad_Usuario').append(options);
            }
        }
    });
}

//eventos de botones
function Btn_Guardar_Usuario_Click() {
    $("#Lbl_Mensaje_Error_Usuario").text('');
    $("#Lbl_Mensaje_Error_Usuario").hide();
    $("#Img_Error_Usuario").hide();

    var Btn_Nuevo = $("#Btn_Guardar_Usuario");
    //Verificar el tooltip del boton
    if (Btn_Nuevo.attr('alt') == "Nuevo") {
        //Habilitar y limpiar
        Configurar_Botones_Alta_Usuario("Nuevo");
        Limpia_Campos_Usuarios();
        //agrega los valores iniciales de pais y estatus
        $('#Txt_Pais_Usuario').val('Mexico');
        $('#Txt_Pais_Usuario').removeAttr('style');
        $('#Txt_Pais_Usuario').attr('style', 'width: 98%;');
        $('#Cmb_Estatus_Usuario').removeAttr('style');
        $('#Cmb_Estatus_Usuario').attr('style', 'width: 98%;');
    }
    else {
        Alta_Usuarios();
    }
}
function Alta_Usuarios() {
    //recupera la información de que se enviará al servidor para validar el alta

    var requeridos = Valida_Datos_Requeridos_Usuario();
    if (requeridos == "") {
        //Recupera la información a enviar
        var registro = $('.altaUsuario');

        //envia la información al servidor
        $.ajax({
            url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=alta_usuario&" + registro.serialize(),
            type: 'POST',
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    //$.each(data, function (i, item) {
                    var error = data[2].error;
                    var usuario_id = data[0].usuario_id;
                    var alta = data[1].alta; ;
                    if (error != "") {
                        $("#Lbl_Mensaje_Error_Usuario").removeAttr('style');
                        $("#Lbl_Mensaje_Error_Usuario").attr('style', 'color: rgb(153, 0, 0); display: inline;');
                        $("#Lbl_Mensaje_Error_Usuario").text(error);
                        $("#Lbl_Mensaje_Error_Usuario").show();
                        $("#Img_Error_Usuario").show();
                    }
                    else {
                        $("#Lbl_Mensaje_Error_Usuario").removeAttr('style');
                        $("#Lbl_Mensaje_Error_Usuario").attr('style', 'color: rgb(153, 0, 0); display: inline;');
                        $("#Lbl_Mensaje_Error_Usuario").text(alta);
                        $("#Lbl_Mensaje_Error_Usuario").show();
                        $("#Img_Error_Usuario").show();
                        //Se ingresan los datos del usuario en la solicitud.
                        var nombre = $("#Txt_Nombre_Usuario_Alta").val() + ' ' + $("#Txt_Apellido_Paterno_Usuario_Alta").val() + ' ' + $("#Txt_Apellido_Materno_Usuario").val();
                        $('#Txt_Usuario_ID').val(usuario_id);
                        $('#Txt_Nombre_Usuario').val(nombre);
                        $('#Txt_Nombre_Solicitante').val(nombre);
                        Limpia_Campos_Usuarios();
                        //regresa a la pantalla principal
                        setTimeout("$('#Modal_Usuarios').jqmHide()", 4000);
                        //Quita el valor de requerido del usuario
                        $('#Txt_Nombre_Usuario').removeAttr('style');
                        $('#Txt_Nombre_Usuario').attr('style', 'width:80%');
                        $('#Txt_Nombre_Solicitante').removeAttr('style');
                        $('#Txt_Nombre_Solicitante').attr('style', 'width:98%');
                    }
                }
            }
        });
    }
    else {
        $("#Lbl_Mensaje_Error_Usuario").removeAttr('style');
        $("#Lbl_Mensaje_Error_Usuario").attr('style', 'color: rgb(153, 0, 0); display: inline;');
        $("#Lbl_Mensaje_Error_Usuario").text(requeridos);
        $("#Lbl_Mensaje_Error_Usuario").show();
        $("#Img_Error_Usuario").show();
    }
}
function Valida_Datos_Requeridos_Usuario() {
    var opciones = $('.requeridoUsuario');
    var mensaje = "";
    opciones.each(function (i, item) {
        if (item.type == "text") {
            if ($('#' + item.id + '').val() == "") {
                mensaje += item.name + ", ";
            }
        }
        if (item.type == "select-one") {
            var cmboption = $("#" + item.id + " option:selected").val();
            if (cmboption == "Seleccione") {
                mensaje += item.name + ", ";
            }
        }
    });
    if (mensaje != "") {
        mensaje = "Falta la siguiente información por llenar: " + mensaje;
    }
    return mensaje;
}
function Btn_Cancelar_Usuario_Click() {
    var estatus = $("#Btn_Cancelar_Usuario").attr('alt')

    Limpia_Campos_Usuarios();
    if (estatus == "Salir") {
        $('#Modal_Usuarios').jqmHide();
    }
    else {
        Configurar_Botones_Alta_Usuario("Inicial");
        $('#Modal_Usuarios').removeAttr('style');
        $('#Modal_Usuarios').attr('style', 'left:50%; top:15%; width:850px; margin-left:-425px; display:block; z-index:3000;');
        $('#Modal_Usuarios').jqmShow();
    }
}
//Fin Catalogo de Usuarios -------------------------------------------------------------------------------------------------------------------
//Configuración de elementos Solicitud-------------------------------------------------------------------------------------------------------
function Cargar_Giros_Solicitud() {
    var select = $('#Cmb_Giro');
    $('option', select).remove();
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=consulta_giros_solicitud",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="SELECCIONE"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.giro_id + '">' + item.giro + '</option>';
                });
                $('#Cmb_Giro').append(options);
            }
        }
    });
}
function Cargar_Servicios_Solicitud() {
    var select = $('#Cmb_Servicio');
    $('option', select).remove();
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=consulta_servicios_predios",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="SELECCIONE"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.servicio_id + '">' + item.servicio + '</option>';
                });
                $('#Cmb_Servicio').append(options);
            }
        }
    });
}
function Cargar_Motivos_Inspeccion_Solicitud() {
    var select = $('#Cmb_Motivo_Inspeccion');
    $('option', select).remove();
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=consulta_motivos_inspeccion",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="SELECCIONE"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.motivo_id + '">' + item.motivo + '</option>';
                });
                $('#Cmb_Motivo_Inspeccion').append(options);
            }
        }
    });
}
function Cargar_Grupos_Conceptos_Solicitud() {
    var select = $('#Cmb_Grupos_Conceptos');
    $('option', select).remove();
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=consulta_grupos_conceptos",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="SELECCIONE"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.grupo_concepto_id + '" detalles="' + item.detalles + '">' + item.grupo_concepto + '</option>';
                });
                $('#Cmb_Grupos_Conceptos').append(options);
            }
        }
    });
}
function Cargar_Documentos_Solicitud() {
    var select = $('#Cmb_Documento');
    $('option', select).remove();
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=consulta_documentos_solicitud",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var options = '<option value="SELECCIONE"><-SELECCIONE-></option>';
            if (data != null) {
                $.each(data, function (i, item) {
                    options += '<option value="' + item.documento_id + '" obligatorio="' + item.obligatorio + '" descripcion="' + item.descripcion + '">' + item.documento + '</option>';
                });
                $('#Cmb_Documento').append(options);
            }
        }
    });
}

//Inicio Eventos Solicitudes -----------------------------------------------------------------------------------------------------------------
function Configurar_Botones_Alta_Solicitud(modo) {
    try {
        switch (modo) {
            case "Inicial":
                $('#Btn_Nuevo').removeAttr('disabled');
                $('#Btn_Nuevo').attr('src', '../imagenes/paginas/icono_nuevo.png');
                $('#Btn_Modificar').attr('src', '../imagenes/paginas/icono_modificar.png');
                $('#Btn_Contrato').attr('src', '../imagenes/gridview/grid_docto.png');
                $('#Btn_Contrato').attr('style','cursor:pointer;width:24px;height:24px;display:none;');
                $('#Btn_Salir').attr('src', '../imagenes/paginas/icono_salir.png');
                $('#Btn_Nuevo').attr('alt', 'Nuevo');
                $('#Btn_Modificar').attr('alt', 'Modificar');
                $('#Btn_Contrato').attr('alt', 'Contrato');
                $('#Btn_Salir').attr('alt', 'Regresar');
                //$('#Div_Filtros_Solicitudes').show();
                //$('#Div_Solicitud').hide();
                $('#Lbl_Mensaje_Error').text('');
                $('#Img_Error').hide();
                //Limpia los grids
                var doc = $('#Grid_Documentos').datagrid('getRows');
                $.each(doc, function(i,item){
                    var indicedoc = $('#Grid_Documentos').datagrid('getRows').length-1;
                    $('#Grid_Documentos').datagrid('deleteRow', indicedoc); 
                });
                var evidencias = $('#Grid_Evidencias').datagrid('getRows');
                $.each(evidencias, function(i,item){
                    var indiceevidencias = $('#Grid_Evidencias').datagrid('getRows').length-1;
                    $('#Grid_Evidencias').datagrid('deleteRow', indiceevidencias); 
                });
                //Configuracion_Acceso("Frm_Cat_Cor_Medidores.aspx");
                //$('#Cmb_Giro').removeAttr('disabled');                
                break;

            case "Nuevo":
                $('#Btn_Nuevo').removeAttr('disabled');
                $('#Btn_Nuevo').attr('src', '../imagenes/paginas/icono_guardar.png');
                $('#Btn_Nuevo').attr('alt', 'Guardar');
                $('#Btn_Modificar').attr('src', '../imagenes/paginas/icono_modificar_deshabilitado.png');
                //$('#Btn_Contrato').attr('src', '../imagenes/gridview/grid_docto.png');
                //$('#Btn_Contrato').attr('style', 'cursor:pointer;width:24px;height:24px;');
                $('#Btn_Salir').attr('src', '../imagenes/paginas/icono_cancelar.png');
                $('#Btn_Salir').attr('alt', 'Cancelar');
                Limpia_Campos_Solicitud();
                $('#Div_Filtros_Solicitudes').hide();
                $('#Div_Solicitud').show();
                $('#Cmb_Estatus').removeAttr('style');
                $('#Cmb_Estatus').attr('style', 'width:98%;');
                $('#Cmb_Estatus').val('GENERADO');
                $('#Div_Pestanias').tabs('select','Grupos Conceptos');
                $('#Div_Pestanias').tabs('select','Datos Solicitud');
                $('#Cmb_Giro').attr('disabled', 'disabled');
                var fecha = new Date();
                $('#Txt_Fecha_Solicito').val(fecha.format("dd-mm-yyyy"));
                $('#Txt_Folio').removeAttr('disabled');
                break;

            case "Modificar":
                $('#Btn_Nuevo').attr('src', '../imagenes/paginas/icono_nuevo_deshabilitado.png');
                $('#Btn_Modificar').attr('src', '../imagenes/paginas/icono_guardar.png');
                $('#Btn_Modificar').attr('alt', 'Guardar');
                //$('#Btn_Contrato').attr('src', '../imagenes/gridview/grid_docto.png');
                //$('#Btn_Contrato').attr('style', 'cursor:pointer;width:24px;height:24px;');
                $('#Btn_Salir').attr('src', '../imagenes/paginas/icono_cancelar.png');
                $('#Btn_Salir').attr('alt', 'Cancelar');
                $('#Div_Filtros_Solicitudes').hide();
                $('#Div_Solicitud').show();
                $('#Cmb_Estatus').removeAttr('style');
                $('#Cmb_Estatus').attr('style', 'width:98%;');
                $('#Div_Pestanias').tabs('select', 'Grupos Conceptos');
                $('#Div_Pestanias').tabs('select', 'Datos Solicitud');
                $('#Cmb_Giro').attr('disabled', 'disabled');
                Hab_Des_RevisionPredio();
                $('#Txt_Folio').attr('disabled', 'disabled');
                break;

            default: break;
        }

    }
    catch (ex) {
        //Error
        $("#Lbl_Mensaje_Error").removeAttr('style');
        $("#Lbl_Mensaje_Error").attr('style', 'color: rgb(153, 0, 0); display: inline;');
        $("#Lbl_Mensaje_Error").val(ex.Description);
        $("#Img_Error").show();
    }
    finally {
    }
}
function Limpia_Campos_Solicitud() {
    $('#Txt_Folio').val('');
    $('.altaSolicitud').val('SELECCIONE');
    $('.altaSolicitud').val('');
    $('.requeridoSolicitud').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
    $('.requeridoSolicitudContrato').attr('style', 'width:98%; border-style:dotted; border-color:Red;');
    
    $('.altaSolicitudContrato').val('SELECCIONE');
    $('.altaSolicitudContrato').val('');
    $('.altaSolicitudContrato').attr('disabled','disabled');
    $('.disabled').attr('disabled', 'disabled');
    //Formato de tamño para determinados controles
    $('#Txt_Nombre_Usuario').attr('style', 'width:80%; border-style:dotted; border-color:Red;');
    $('#Txt_Tiempo_Habilitado_Deshabilitado').attr('style', 'width:30%; border-style:dotted; border-color:Red;');
    $('#Cmb_Periodo_Tiempo_Habilitado_Deshabilitado').attr('style', 'width:50%; border-style:dotted; border-color:Red;');
}

function Btn_Nuevo_Click() {
    $("#Lbl_Mensaje_Error").text('');
    $("#Lbl_Mensaje_Error").hide();
    $("#Img_Error").hide();

    var Btn_Nuevo = $("#Btn_Nuevo");
    var Btn_Modificar = $("#Btn_Modificar");
    //Verificar el tooltip del boton
    if (Btn_Nuevo.attr('alt') == "Nuevo") {
        if (Btn_Modificar.attr('alt') == "Guardar") {
            return;
        }
        //Habilitar y limpiar
        Configurar_Botones_Alta_Solicitud("Nuevo");
        Habilita_Controles_Solicitud(true);
    }
    else {
        Alta_Solicitud();
    }
}

function Habilita_Controles_Solicitud(habilitar){
    //Habilita los controles
    if (habilitar==true){
        $('#Txt_Folio').removeAttr('disabled');
        $('#Txt_Nombre_Solicitante').removeAttr('disabled');
        $('#Cmb_Duenio').removeAttr('disabled');
        $('#Txt_Fecha_Solicito').removeAttr('disabled');
        $('#Chk_Factibilidad').removeAttr('disabled');
        $('#Chk_Inspeccion').removeAttr('disabled');
        $('#Chk_Estudio_Socioeconomico').removeAttr('disabled');
        $('#Cmb_Tipo_Servicio').removeAttr('disabled');
        $('#Cmb_Condicion').removeAttr('disabled');
        $('#Cmb_Pozo_Propio').removeAttr('disabled');
        $('#Cmb_Tipo_Laboratorio').removeAttr('disabled');
        $('#Cmb_Planta_Tratamiento').removeAttr('disabled');
        $('#Cmb_Servicio').removeAttr('disabled');
        $('#Btn_Buscar_Usuario').removeAttr('style');
        $('#Btn_Buscar_Usuario').attr('style','cursor:pointer;display:block;');
        $('#Btn_Buscar_Predio').removeAttr('style');
        $('#Btn_Buscar_Predio').attr('style','cursor:pointer;display:block;');
        $('#Btn_Agregar_Servicio').removeAttr('style');
        $('#Btn_Agregar_Servicio').attr('style','cursor:pointer;display:block;width:24px;height:24px;');
        $('#Btn_Agregar_Grupo_Concepto').removeAttr('style');
        $('#Btn_Agregar_Grupo_Concepto').attr('style','cursor:pointer;display:block;width:24px;height:24px;');
        $('#Cmb_Grupos_Conceptos').removeAttr('disabled');
        $('#Txt_Detalles_Grupos').removeAttr('disabled');
        $('#Btn_Agregar_Documento').removeAttr('style');
        $('#Btn_Agregar_Documento').attr('style','cursor:pointer;display:block;width:24px;height:24px;');
        $('#Cmb_Documento').removeAttr('disabled');
        $('#btn_buscar_archivo').removeAttr('style');
        $('#btn_buscar_archivo').attr('style','cursor:pointer;display:block;width:24px;height:24px;');
        $('#btn_buscar_img').removeAttr('style');
        $('#btn_buscar_img').attr('style','cursor:pointer;display:block;width:24px;height:24px;');
        $('#Txt_Comentarios_Documentos').removeAttr('disabled');
        /////////////////////////////////////////////////////////////////////////
        $('#Txt_Tiempo_Habilitado_Deshabilitado').removeAttr('disabled');
        $('#Cmb_Periodo_Tiempo_Habilitado_Deshabilitado').removeAttr('disabled');
        $('#Txt_No_Personas').removeAttr('disabled');
        $('#Cmb_Medidor').removeAttr('disabled');
        $('#Txt_Observaciones_Solicitud').removeAttr('disabled');           
        
    }else{//Deshabilita los controles
        $('#Txt_Folio').attr('disabled', 'disabled');
        $('#Cmb_Motivo_Inspeccion').attr('disabled','disabled');
        $('#Txt_Nombre_Solicitante').attr('disabled','disabled');
        $('#Cmb_Duenio').attr('disabled','disabled');
        $('#Txt_Fecha_Solicito').attr('disabled','disabled');
        $('#Chk_Factibilidad').attr('disabled','disabled');
        $('#Chk_Inspeccion').attr('disabled','disabled');
        $('#Chk_Estudio_Socioeconomico').attr('disabled','disabled');
        $('#Cmb_Tipo_Servicio').attr('disabled','disabled');
        $('#Cmb_Condicion').attr('disabled','disabled');
        $('#Cmb_Pozo_Propio').attr('disabled','disabled');
        $('#Cmb_Tipo_Laboratorio').attr('disabled','disabled');
        $('#Cmb_Planta_Tratamiento').attr('disabled','disabled');
        $('#Cmb_Servicio').attr('disabled','disabled');
        $('#Cmb_Grupos_Conceptos').attr('disabled','disabled');
        $('#Txt_Detalles_Grupos').attr('disabled','disabled');
        $('#Cmb_Documento').attr('disabled','disabled');
        $('#Txt_Comentarios_Documentos').attr('disabled','disabled');
        $('#Btn_Buscar_Usuario').removeAttr('style');
        $('#Btn_Buscar_Usuario').attr('style','cursor:pointer;display:none;');
        $('#Btn_Buscar_Predio').removeAttr('style');
        $('#Btn_Buscar_Predio').attr('style','cursor:pointer;display:none;');
        $('#Btn_Agregar_Servicio').removeAttr('style');
        $('#Btn_Agregar_Servicio').attr('style','cursor:pointer;display:none;width:24px;height:24px;');
        $('#Btn_Agregar_Grupo_Concepto').removeAttr('style');
        $('#Btn_Agregar_Grupo_Concepto').attr('style','cursor:pointer;display:none;width:24px;height:24px;');
        $('#Btn_Agregar_Documento').removeAttr('style');
        $('#Btn_Agregar_Documento').attr('style','cursor:pointer;display:none;width:24px;height:24px;');
        $('#btn_buscar_archivo').removeAttr('style');
        $('#btn_buscar_archivo').attr('style','cursor:pointer;display:none;width:24px;height:24px;');
        /////////////////////////////////////////////////////////////////////////
        $('#Txt_Tiempo_Habilitado_Deshabilitado').attr('disabled','disabled');
        $('#Cmb_Periodo_Tiempo_Habilitado_Deshabilitado').attr('disabled', 'disabled');
        $('#Txt_No_Personas').attr('disabled', 'disabled'); ;
        $('#Cmb_Medidor').attr('disabled', 'disabled'); ;
        $('#Txt_Observaciones_Solicitud').attr('disabled', 'disabled'); ;              
    }
}

function Alta_Solicitud() {
    //recupera la información de que se enviará al servidor para validar el alta
    //Valida los datos requeridos para la solicitud y en su caso contrato
    var estatus = $('#Cmb_Estatus option:selected').val();
    var requeridos = Valida_Datos_Requeridos_Solicitud();
    if (estatus=='APROBADO'){
        requeridos += Valida_Datos_Requeridos_Solicitud_Contratos();
    }
    if (requeridos == "") {
        //Recupera la información a enviar
        var solicitud = $('.altaSolicitud');
        var solicitudcontrato = $('.altaSolicitudContrato');
        var folio = $('#Txt_Folio').val();
        var solicitud_giro = $('#Cmb_Giro option:selected').val();
        var solicitud_estatus = estatus;
        //informacion de servicios
        var servicios_datos="&Total_Servicios=1";
        var servicios = $('#Cmb_Servicio option:selected').val();
        //Conceptos Cobros
        var conceptos_datos="&Total_Conceptos=1";
        var conceptos = $('#Cmb_Grupos_Conceptos option:selected').val();
        //Documentos
        var documentos_datos="&Total_Documentos=" + $('#Grid_Documentos').datagrid('getRows').length;
        var documentos = JSON.stringify($('#Grid_Documentos').datagrid('getRows'));
        //Evidencias
        var evidencias_datos="&Total_Evidencias=" + $('#Grid_Evidencias').datagrid('getRows').length;
        var evidencias = JSON.stringify($('#Grid_Evidencias').datagrid('getRows'));
        //Informacion de los checkbox
        var checks ="";
        var solicitud_motivo="";
        if ($("#Chk_Inspeccion").is(':checked')){
            checks += "&Solicitud_Inspeccion=SI";
            solicitud_motivo=$('#Cmb_Motivo_Inspeccion option:selected').val();
        }
        else{
            checks += "&Solicitud_Inspeccion=NO";
        }
        if ($("#Chk_Factibilidad").is(':checked')){
            checks += "&Solicitud_Factibilidad=SI";
        }
        else{
            checks += "&Solicitud_Factibilidad=NO";
        }
        if ($("#Chk_Estudio_Socioeconomico").is(':checked')){
            checks += "&Solicitud_Estudio_Socioeconomico=SI";
        }
        else{
            checks += "&Solicitud_Estudio_Socioeconomico=NO";
        }
        
        //envia la información al servidor
        $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=alta_solicitud&" + solicitud.serialize()  + "&" + 
                solicitudcontrato.serialize() + checks + servicios_datos + "&Servicios=" + servicios + conceptos_datos + 
                "&Conceptos_Cobros=" + conceptos + "&Documentos=" + documentos + documentos_datos + "&Evidencias=" + evidencias + evidencias_datos +
                "&solicitud_estatus=" + solicitud_estatus + "&Solicitud_Giro_ID=" + solicitud_giro + "&Folio=" + folio,
            type: 'POST',
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    //$.each(data, function (i, item) {
                    var error = data[2].error;
                    var no_solicitud = data[0].no_solicitud;
                    var alta = data[1].alta; ;
                    if (error != "") {
                        $("#Lbl_Mensaje_Error").removeAttr('style');
                        $("#Lbl_Mensaje_Error").attr('style', 'color: rgb(153, 0, 0); display: inline;');
                        $("#Lbl_Mensaje_Error").text(error);
                        $("#Lbl_Mensaje_Error").show();
                        $("#Img_Error").show();
                    }
                    else {
                        Limpia_Campos_Solicitud();
                        Habilita_Controles_Solicitud(false);
                        Configurar_Botones_Alta_Solicitud("Inicial");
                        $("#Lbl_Mensaje_Error").removeAttr('style');
                        $("#Lbl_Mensaje_Error").attr('style', 'color: rgb(153, 0, 0); display: inline;');
                        $("#Lbl_Mensaje_Error").text(alta);
                        $("#Lbl_Mensaje_Error").show();
                        $("#Img_Error").show();
                        window.open("../../Reporte/Solicitud_Contrato_" + no_solicitud + ".pdf");
                    }
                }
            }
        });
    }
    else {
        $("#Lbl_Mensaje_Error").removeAttr('style');
        $("#Lbl_Mensaje_Error").attr('style', 'color: rgb(153, 0, 0); display: inline;');
        $("#Lbl_Mensaje_Error").text(requeridos);
        $("#Lbl_Mensaje_Error").show();
        $("#Img_Error").show();
    }
}

function Valida_Datos_Requeridos_Solicitud() {
    var opciones = $('.requeridoSolicitud');
    var mensaje = "";
    opciones.each(function (i, item) {
        if (item.type == "text") {
            if ($('#' + item.id + '').val() == "") {
                mensaje += item.name + ", ";
            }
        }
        if (item.type == "select-one") {
            var cmboption = $("#" + item.id + " option:selected").val();
            if (cmboption == "SELECCIONE") {
                mensaje += item.name + ", ";
            }
        }
    });
    //Valida que haya seleccionado
    var rowcount1 = $('#Grid_Documentos').datagrid('getRows').length;
    if (rowcount1 < 1) {
        mensaje += "Debe seleccionar un documento, "
    }
    
    if (mensaje != "") {
        mensaje = "Falta la siguiente información por llenar: " + mensaje;
    }
    return mensaje;
}
function Valida_Datos_Requeridos_Solicitud_Contratos() {
    var opciones = $('.requeridoSolicitudContrato');
    var mensaje = "";
    opciones.each(function (i, item) {
        if (item.type == "text") {
            if ($('#' + item.id + '').val() == "") {
                mensaje += item.name + ", ";
            }
        }
        if (item.type == "select-one") {
            var cmboption = $("#" + item.id + " option:selected").val();
            if (cmboption == "Seleccione") {
                mensaje += item.name + ", ";
            }
        }
    });
    if (mensaje != "") {
        mensaje = "Falta la siguiente información por llenar: " + mensaje;
    }
    return mensaje;
}

function Btn_Modificar_Click() {
    $("#Lbl_Mensaje_Error").text('');
    $("#Lbl_Mensaje_Error").hide();
    $("#Img_Error").hide();

    var Btn_Nuevo = $("#Btn_Nuevo");
    var Btn_Modificar = $("#Btn_Modificar");
    //Verificar el tooltip del boton
    if (Btn_Modificar.attr('alt') == "Modificar") {
        if ($("#Txt_No_Solicitud").val() != ""){
            if (Btn_Nuevo.attr('alt') == "Guardar") {
                return;
            }
            //Habilitar y limpiar
            Habilita_Controles_Solicitud(true);
            Configurar_Botones_Alta_Solicitud("Modificar");
        }else{
            alert('Seleccione la solicitud a modificar.');
        }
    }
    else {
        Modifica_Solicitud();
        Habilita_Controles_Solicitud(false);
    }
}

function Modifica_Solicitud() {
    //recupera la información de que se enviará al servidor para validar el alta
    //Valida los datos requeridos para la solicitud y en su caso contrato
    var estatus = $('#Cmb_Estatus option:selected').val();
    var requeridos = Valida_Datos_Requeridos_Solicitud();
    if (estatus=='APROBADO'){
        requeridos += Valida_Datos_Requeridos_Solicitud_Contratos();
    }
    if (requeridos == "") {
        //Recupera la información a enviar
        var solicitud = $('.altaSolicitud');
        var solicitudcontrato = $('.altaSolicitudContrato');
        var solicitud_estatus = estatus;
        //informacion de servicios
        var servicios_datos = "&Total_Servicios=1";
        var servicios = $('#Cmb_Servicio option:selected').val();
        //Conceptos Cobros
        var conceptos_datos = "&Total_Conceptos=1";
        var conceptos = $('#Cmb_Grupos_Conceptos option:selected').val();
        //Documentos
        var documentos_datos="&Total_Documentos=" + $('#Grid_Documentos').datagrid('getRows').length;
        var documentos = JSON.stringify($('#Grid_Documentos').datagrid('getRows'));
        //Evidencias
        var evidencias_datos = "&Total_Evidencias=" + $('#Grid_Evidencias').datagrid('getRows').length;
        var evidencias = JSON.stringify($('#Grid_Evidencias').datagrid('getRows'));
        //Informacion de los checkbox
        var checks ="";
        if ($("#Chk_Inspeccion").is(':checked')){
            checks += "&Solicitud_Inspeccion=SI";
        }
        else{
            checks += "&Solicitud_Inspeccion=NO";
        }
        if ($("#Chk_Factibilidad").is(':checked')){
            checks += "&Solicitud_Factibilidad=SI";
        }
        else{
            checks += "&Solicitud_Factibilidad=NO";
        }
        if ($("#Chk_Estudio_Socioeconomico").is(':checked')){
            checks += "&Solicitud_Estudio_Socioeconomico=SI";
        }
        else{
            checks += "&Solicitud_Estudio_Socioeconomico=NO";
        }

         //envia la información al servidor
        $.ajax({
            url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=modifica_solicitud&" + solicitud.serialize() + "&" +
                solicitudcontrato.serialize() + checks +  servicios_datos + "&Servicios=" + servicios + conceptos_datos + "&Conceptos_Cobros=" + conceptos +
                "&Documentos=" + documentos + documentos_datos + "&Evidencias=" + evidencias + evidencias_datos + "&solicitud_estatus=" + solicitud_estatus ,
            type: 'POST',
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    //$.each(data, function (i, item) {
                    var error = data[2].error;
                    var no_solicitud = data[0].no_solicitud;
                    var alta = data[1].alta; ;
                    if (error != "") {
                        $("#Lbl_Mensaje_Error").removeAttr('style');
                        $("#Lbl_Mensaje_Error").attr('style', 'color: rgb(153, 0, 0); display: inline;');
                        $("#Lbl_Mensaje_Error").text(error);
                        $("#Lbl_Mensaje_Error").show();
                        $("#Img_Error").show();
                    }
                    else {
                        Limpia_Campos_Solicitud();
                        Configurar_Botones_Alta_Solicitud("Inicial");
                        $("#Lbl_Mensaje_Error").removeAttr('style');
                        $("#Lbl_Mensaje_Error").attr('style', 'color: rgb(153, 0, 0); display: inline;');
                        $("#Lbl_Mensaje_Error").text(alta);
                        $("#Lbl_Mensaje_Error").show();
                        $("#Img_Error").show();
                    }
                }
            }
        });
    }
    else {
        $("#Lbl_Mensaje_Error").removeAttr('style');
        $("#Lbl_Mensaje_Error").attr('style', 'color: rgb(153, 0, 0); display: inline;');
        $("#Lbl_Mensaje_Error").text(requeridos);
        $("#Lbl_Mensaje_Error").show();
        $("#Img_Error").show();
    }
}

function Cargar_Informacion_Solicitud(No_Solicitud, Predio_ID){
    //Consulta los datos de la solicitud para llenar los campos
    Limpia_Campos_Solicitud();
    $("#Lbl_Mensaje_Error").text('');
    $("#Lbl_Mensaje_Error").hide();
    $("#Img_Error").hide();
    //Realiza la consulta de la información del predio para colocarla en las cajas correspondientes
    $.ajax({
        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=Filtrar_Solicitudes&No_Solicitud=" + No_Solicitud,
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            if (data != null) {
                $.each(data.rows, function(i, item) {
                    //asinga los valores a las cajas de texto y combos

                    $('#Txt_No_Solicitud').val(item.No_Solicitud);
                    $('#Txt_Elaboro_ID').val(item.Elaboro_ID);
                    $('#Txt_Folio').val(item.Folio);
                    $('#Cmb_Estatus').val(item.Estatus);

                    $('#Txt_Predio_ID').val(item.Predio_ID);
                    $('#Txt_Colonia').val(item.Colonia);
                    $('#Txt_Calle').val(item.Calle);
                    $('#Txt_No_Exterior').val(item.No_Exterior);
                    $('#Txt_No_Interior').val(item.No_Interior);

                    $('#Txt_Usuario_ID').val(item.Usuario_ID);
                    $('#Txt_Nombre_Usuario').val(item.Solicito);
                    $('#Txt_Nombre_Solicitante').val(item.Solicito);
                    $("#Txt_Razon_Social_Usuario").val(item.Solicito);

                    $('#Cmb_Duenio').val(item.Duenio);
                    $('#Cmb_Giro').val(item.Giro);
                    $('#Txt_Fecha_Solicito').val(item.Fecha);

                    $('#Txt_Servicio_ID').val(item.Servicio_ID);
                    $('#Cmb_Tipo_Servicio').val(item.Tipo_Servicio);
                    $('#Cmb_Giro').val(item.Giro_ID);
                    $('#Cmb_Servicio').val(item.Servicio_ID);
                    $('#Cmb_Grupos_Conceptos').val(item.Grupo_Concepto_ID);
                    $('#Cmb_Condicion').val(item.Condicion);
                    $('#Cmb_Pozo_Propio').val(item.Pozo_Propio);
                    $('#Cmb_Tipo_Laboratorio').val(item.Tipo_Laboratorio);
                    $('#Cmb_Planta_Tratamiento').val(item.Planta_Tratamiento);

                    if (item.Inspeccion == "SI") {
                        $('#Chk_Inspeccion').attr('checked', 'true');
                        $('#Cmb_Motivo_Inspeccion').val(item.Motivo_Inspeccion_ID);
                    } else {
                        $('#Chk_Inspeccion').removeAttr('checked');
                    }
                    if (item.Estudio_Factibilidad == "SI") {
                        $('#Chk_Factibilidad').attr('checked', 'true');
                    } else {
                        $('#Chk_Factibilidad').removeAttr('checked');
                    }
                    if (item.Estudio_Socioeconomico == "SI") {
                        $('#Chk_Estudio_Socioeconomico').attr('checked', 'true');
                    } else {
                        $('#Chk_Estudio_Socioeconomico').removeAttr('checked');
                    }
                    //Coloca los datos de Revision Predios
                    $('#Txt_Tiempo_Habilitado_Deshabilitado').val(item.Tiempo_Habilitado_Deshabilitado);
                    $('#Cmb_Periodo_Tiempo_Habilitado_Deshabilitado').val(item.Periodo_Tiempo_Habilitado_Deshabilitado);
                    $('#Txt_No_Personas').val(item.No_Personas);
                    $('#Cmb_Medidor').val(item.Medidor);
                    $('#Txt_No_Medidor').val(item.No_Medidor);
                    $('#Txt_Lectura_Medidor').val(item.Lectura_Medidor);
                    $('#Txt_Medida_Largo').val(item.Medida_Largo);
                    $('#Txt_Medida_Ancho').val(item.Medida_Ancho);
                    $('#Txt_Diametro_Toma').val(item.Diametro_Toma);
                    $('#Txt_Observaciones_Solicitud').val(item.Observaciones);
                    
                    //Coloca los Documentos en el Grid
                    var documentos = $('#Grid_Documentos').datagrid('getRows');
                    $.each(documentos, function(i, item) {
                        var indice = $('#Grid_Documentos').datagrid('getRows').length - 1;
                        $('#Grid_Documentos').datagrid('deleteRow', indice);
                    });

                    $.ajax({
                    url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=consulta_documentos_detalles_solicitud&No_Solicitud=" + No_Solicitud,
                        type: 'POST',
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function(data1) {
                            if (data1 != null) {
                                $.each(data1, function(k, item2) {
                                    //Agrega el servicio
                                    var rowcount = $('#Grid_Documentos').datagrid('getRows').length - 1;
                                    $('#Grid_Documentos').datagrid('appendRow', {
                                        Documento_ID: item2.Documento_ID,
                                        Nombre_Documento: item2.Nombre_Documento,
                                        Nombre_Media: item2.Nombre_Media,
                                        Comentarios: item2.Comentarios
                                    });
                                });
                            }
                        }
                    }); 
                    
                    //Coloca las evidencias en el Grid
                    var evidencias = $('#Grid_Evidencias').datagrid('getRows');
                    $.each(evidencias, function(i, item) {
                        var indice = $('#Grid_Evidencias').datagrid('getRows').length - 1;
                        $('#Grid_Evidencias').datagrid('deleteRow', indice);
                    });

                    $.ajax({
                        url: "../../Controladores/Frm_Controlador_Solicitudes_Contrato_Servicio.aspx?Accion=consulta_evidencias_solicitud&No_Solicitud=" + No_Solicitud,
                        type: 'POST',
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function(data1) {
                            if (data1 != null) {
                                $.each(data1, function(k, item2) {
                                    //Agrega el servicio
                                    var rowcount = $('#Grid_Evidencias').datagrid('getRows').length - 1;
                                    $('#Grid_Evidencias').datagrid('appendRow', {
                                        No_Solicitud: item2.No_Solicitud,
                                        Nombre_Media: item2.Nombre_Media,
                                        Comentarios: item2.Comentarios
                                    });
                                });
                            }
                        }
                    });

                    //Coloca los datos de la inspección
                    $.ajax({
                        url: "../../Controladores/frm_controlador_inspecciones.aspx?Accion=obtenerInspecciones&Predio_ID=" + Predio_ID,
                        type: 'POST',
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function(data1) {
                            if (data1 != null) {
                                $.each(data1, function(k, item2) {
                                    $('#Txt_No_Medidor').val(item2.No_Medidor);
                                    $('#Txt_Fecha_Instalacion').val(item2.Fecha_Instalacion);
                                });
                            }
                        }
                    });
                    
                    //Coloca los datos del medidor                    
                    $.ajax({
                        url: "../../Controladores/Frm_Controlador_Predios.aspx?Accion=consulta_medidor&Predio_ID=" + Predio_ID,
                        type: 'POST',
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function(data1) {
                            if (data1 != null) {
                                $.each(data1, function(k, item2) {
                                    $('#Txt_No_Medidor').val(item2.No_Medidor);
                                    $('#Txt_Fecha_Instalacion').val(item2.Fecha_Instalacion);
                                });
                            }
                        }
                    });
                });
            }
        }
    });
    $('#Div_Filtros_Solicitudes').hide();
    $('#Btn_Salir').attr('alt', 'Regresar');
    $('#Div_Solicitud').show();
    $('#Div_Pestanias').tabs('select','Grupos Conceptos');
    $('#Div_Pestanias').tabs('select','Datos Solicitud');
}
function Btn_Contrato_Click() {

}
function Btn_Imprimir_Solicitud_Click(){
    //Envia informacion para la generacion del reporte de solicitud *****************************************************
    if ($("#Txt_No_Solicitud").attr("value") != "") {
        window.open("../../Reporte/Solicitud_Contrato_" + $("#Txt_No_Solicitud").attr("value") + ".pdf");
    }else{
        $("#Lbl_Mensaje_Error").removeAttr('style');
        $("#Lbl_Mensaje_Error").attr('style', 'color: rgb(153, 0, 0); display: inline;');
        $("#Lbl_Mensaje_Error").text('Seleccione la solicitud a imprimir.');
        $("#Img_Error").show();        
    }
}
function Btn_Salir_Click() {
    var estatus = $("#Btn_Salir").attr('alt')

    if (estatus == "Salir") {
        window.location = '../Paginas_Generales/Frm_Apl_Principal.aspx';
    }
    else {
        Limpia_Campos_Solicitud();
        if (estatus=="Cancelar"){
            Habilita_Controles_Solicitud(false);
            Configurar_Botones_Alta_Solicitud("Inicial");
            $("#Btn_Salir").attr('alt','Regresar')
        }else{
            if (estatus=="Regresar"){
                $('#Div_Filtros_Solicitudes').show();
                $('#Btn_Salir').attr('alt', 'Salir');
                $('#Div_Solicitud').hide();
            }
        }
    }
}

function Btn_Agregar_Servicio_Click() {
    $("#Lbl_Mensaje_Error").text('');
    $("#Lbl_Mensaje_Error").hide();
    $("#Img_Error").hide();
    //Inserta el registro del servicio en el grid
    //valida que se ha seleccionado un elemento de la lista
    var servicio = $("#Cmb_Servicio option:selected").text();
    var servicio_id = $("#Cmb_Servicio option:selected").val();
    if (servicio_id == 'SELECCIONE') {
        $("#Lbl_Mensaje_Error").removeAttr('style');
        $("#Lbl_Mensaje_Error").attr('style', 'color: rgb(153, 0, 0); display: inline;');
        $("#Lbl_Mensaje_Error").text('Seleccione un servicio para agregar');
        $("#Img_Error").show();
        return;
    }
    
    //Valida que no se haya agregado el servicio
    var index = $('#Grid_Servicios').datagrid('getRowIndex',servicio_id);
    if (index>=0){
        $.messager.alert('Alerta','El servicio ya se ha agregado');
        return;
    }

    //Agrega el servicio
    var rowcount = $('#Grid_Servicios').datagrid('getRows').length-1;
    $('#Grid_Servicios').datagrid('appendRow', {
        Servicio_ID : servicio_id,
        Servicio : servicio
    });
}
function Btn_Eliminar_Servicio_Click(servicio_id){
    $.messager.confirm('Confirmar','Desea eliminar el servicio?',function(r){  
        if (r){
            //localiza por el id el servicio
            var index = $('#Grid_Servicios').datagrid('getRowIndex',servicio_id);
            $('#Grid_Servicios').datagrid('deleteRow', index);  
        }  
    });  
}

function Btn_Agregar_Grupo_Click() {
    $("#Lbl_Mensaje_Error").text('');
    $("#Lbl_Mensaje_Error").hide();
    $("#Img_Error").hide();
    //Inserta el registro del servicio en el grid
    //valida que se ha seleccionado un elemento de la lista
    var grupo = $("#Cmb_Grupos_Conceptos option:selected").text();
    var grupo_id = $("#Cmb_Grupos_Conceptos option:selected").val();
    if (grupo_id == 'Seleccione') {
        $("#Lbl_Mensaje_Error").removeAttr('style');
        $("#Lbl_Mensaje_Error").attr('style', 'color: rgb(153, 0, 0); display: inline;');
        $("#Lbl_Mensaje_Error").text('Seleccione un grupo para agregar');
        $("#Img_Error").show();
        return;
    }
    
    //Valida que no se haya agregado el servicio
    var index = $('#Grid_Grupos_Conceptos').datagrid('getRowIndex',grupo_id);
    if (index>=0){
        $.messager.alert('Alerta','El grupo ya se ha agregado');
        return;
    }

    //Agrega el servicio
    var rowcount = $('#Grid_Grupos_Conceptos').datagrid('getRows').length-1;  
    $('#Grid_Grupos_Conceptos').datagrid('appendRow', {
        Grupo_Concepto_ID : grupo_id,
        Nombre : grupo
    });
}
function Btn_Eliminar_Grupo_Click(grupo_id){
    $.messager.confirm('Confirmar','Desea eliminar el grupo?',function(r){  
        if (r){
            //localiza por el id el servicio
            var index = $('#Grid_Grupos_Conceptos').datagrid('getRowIndex',grupo_id);
            $('#Grid_Grupos_Conceptos').datagrid('deleteRow', index);  
        }  
    });  
}
function Btn_Agregar_Documento_Click(){
    $("#Lbl_Mensaje_Error").text('');
    $("#Lbl_Mensaje_Error").hide();
    $("#Img_Error").hide();
    //Inserta el registro del servicio en el grid
    //valida que se ha seleccionado un elemento de la lista
    var documento = $("#Cmb_Documento option:selected").text();
    var documento_id = $("#Cmb_Documento option:selected").val();
    if (documento_id == 'SELECCIONE') {
        $("#Lbl_Mensaje_Error").removeAttr('style');
        $("#Lbl_Mensaje_Error").attr('style', 'color: rgb(153, 0, 0); display: inline;');
        $("#Lbl_Mensaje_Error").text('Seleccione un documento para agregar');
        $("#Img_Error").show();
        return;
    }
    
    //Valida que no se haya agregado el servicio
    var index = $('#Grid_Documentos').datagrid('getRowIndex',documento_id);
    if (index>=0){
        $.messager.alert('Alerta','El documento ya se ha agregado');
        return;
    }

    //Valida que la carga del archivo se realizo con exito
    if($("[id$='txt_nombre_archivo']").val()=='')
    {
        $.messager.alert('Alerta','El documento no se ha cargado, favor de esperar a que se haya cargado o seleccione otro');
        return;
    }
    //Agrega el servicio
    var rowcount = $('#Grid_Documentos').datagrid('getRows').length-1;  
    $('#Grid_Documentos').datagrid('appendRow', {
        Documento_ID : documento_id,
        Nombre_Documento : documento,
        Obligatorio :'',
        Media_ID :'',
        Nombre_Clave :'',
        Nombre_Media :$("[id$='txt_nombre_archivo']").val(),
        Ruta :'',
        Extension :'',
        Comentarios :$('#Txt_Comentarios_Documentos').val()
    });
    //limpia los controles
    $("[id$='txt_nombre_archivo']").val('');
    
    $('#Txt_Comentarios_Documentos').text('');
}
function Btn_Eliminar_Documento_Click(documento_id){
    $.messager.confirm('Confirmar','Desea eliminar el documento?',function(r){  
        if (r){
            //localiza por el id el servicio
            var index = $('#Grid_Documentos').datagrid('getRowIndex',documento_id);
            $('#Grid_Documentos').datagrid('deleteRow', index);  
        }  
    });  
}
//----------------------------------------------------------------------------------

function eventoslink(){

 $('.fotos').click(function(event){
     var no_detalle_foto;

     no_detalle_foto=$(this).attr('no_estudio_detalle');
     window.open("../../Controladores/frm_visor_archivos.aspx?accion=estudios_socioeconimicos&no_detalle_foto=" + no_detalle_foto , "VisorImagenes", "width=300px,height=100px,scrollbars=NO"); 
     
     event.preventDefault();
     
 });

 
}
//----------------------------------------------------------------------------------

function crearEventoDiv(){

// este evento es para crear el file upload
$("[id$='Div_Pestania_Documentos']").click(function(){   
    subirArchivo();
});
}

function crearEventoDiv2(){
$("[id$='Div_Pestania_Evidencias']").click(function(){
    subirImagen();
});

}

//----------------------------------------------------------------------------
function crearBoton(){
  $('#btn_buscar_archivo').linkbutton();
}

function crearBoton_Evidencias(){
  $('#btn_buscar_img').linkbutton();
}

//----------------------------------------------------------------------------
function eventoBotonBuscar(){

    $('#btn_buscar_archivo').click(function(){
        //subirArchivo();
    });

}

function eventoBotonBuscar_img(){

    $('#btn_buscar_img').click(function(){
        //subirArchivo();
    });

}

//----------------------------------------------------------------------------
function subirArchivo(){

var boton;

boton=$('#btn_buscar_archivo').attr('id');

if (boton !=undefined){

  new AjaxUpload('#btn_buscar_archivo', {
		action: '../../Controladores/frm_controlador_subir_archivo.aspx', 
		data : {accion:'subir_archivo'},
		onSubmit : function(file , ext){
		      
		    if (!(ext && /^(pdf)$/i.test(ext))) {
                $.messager.alert('Japami', 'Formato Incorrecto');
                return false;
            }  
		    
		   $("[id$='txt_nombre_archivo']").val('');
		   $("[id$='txt_archivo_foto_escondido']").val('');
		   
		   
			$('#ventana_mensaje').window('open');
	
		},
		onComplete : function(file){
		   $("[id$='txt_nombre_archivo']").val(file);
		   $("[id$='txt_archivo_foto_escondido']").val(file);
		   $('#ventana_mensaje').window('close');		   
		     
		}		
	});
  }	
}

function subirImagen(){

var boton;

boton=$('#btn_buscar_img').attr('id');

if (boton !=undefined){

  new AjaxUpload('#btn_buscar_img', {
		action: '../../Controladores/frm_controlador_subir_archivo.aspx', 
		data : {accion:'subir_archivo'},
		onSubmit : function(file , ext){
		      
		    if (!(ext && /^(jpg||png)$/i.test(ext))) {
                $.messager.alert('Japami', 'Formato Incorrecto');
                return false;
            }  
		    
		   $("[id$='txt_nombre_img']").val('');
		   $("[id$='txt_img_escondido']").val('');
		   
		   
			$('#ventana_mensaje').window('open');
	
		},
		onComplete : function(file){
		   $("[id$='txt_nombre_img']").val(file);
		   $("[id$='txt_img_escondido']").val(file);
		   $('#ventana_mensaje').window('close');		   
		     
		}		
	});
  }	
}

function clearContents(id) {
    var AsyncFileUpload = $get(id);
    var txts = AsyncFileUpload.getElementsByTagName("input");
    for (var i = 0; i < txts.length; i++) {
        if (txts[i].type == "text") {
            txts[i].value = "";
            txts[i].style.backgroundColor =  "white";
        }
    }
}

function Chk_Inspeccion_Change(){
    if ($("#Chk_Inspeccion").is(':checked')){
        $('#Cmb_Motivo_Inspeccion').removeAttr('disabled');
    }
    else{
        $('#Cmb_Motivo_Inspeccion').attr('disabled','disabled');
    }
}

function Btn_Agregar_Evidencia_Click(){
    $("#Lbl_Mensaje_Error").text('');
    $("#Lbl_Mensaje_Error").hide();
    $("#Img_Error").hide();
    //Inserta el registro del servicio en el grid
    //valida que se ha seleccionado un elemento de la lista
    var evidencia = $("#Txt_Nombre_Evidencia").val();
    if (evidencia == '') {
        $("#Lbl_Mensaje_Error").removeAttr('style');
        $("#Lbl_Mensaje_Error").attr('style', 'color: rgb(153, 0, 0); display: inline;');
        $("#Lbl_Mensaje_Error").text('Ingrese nombre a la evidencia.');
        $("#Img_Error").show();
        return;
    }

    //Valida que la carga del archivo se realizo con exito
    if ($("[id$='txt_nombre_img']").val() == '') {
        $.messager.alert('Alerta', 'La imagen no se ha cargado, favor de esperar a que se haya cargado o seleccione otro');
        return;
    }
    //Agrega el servicio
    var rowcount = $('#Grid_Evidencias').datagrid('getRows').length-1;  
    $('#Grid_Evidencias').datagrid('appendRow', {
        No_Solicitud :(rowcount + 2),
        No_Inspeccion :'',
        Media_ID :'',
        Nombre_Clave :'',
        Nombre_Media :$("[id$='txt_nombre_img']").val(),
        Ruta :'',
        Extension :'',
        Comentarios :$('#Txt_Comentarios_Evidencias').val()
    });
    //limpia los controles
    $('#Txt_Nombre_Evidencia').val('');
    $("[id$='txt_nombre_img']").val('');
    $('#Txt_Comentarios_Evidencias').text('');
}
function Btn_Eliminar_Evidencia_Click(evidencia){
    $.messager.confirm('Confirmar','Desea eliminar la imagen?',function(r){  
        if (r){
            //localiza por el id el servicio
            var index = $('#Grid_Evidencias').datagrid('getRowIndex',evidencia);
            $('#Grid_Evidencias').datagrid('deleteRow', index);  
        }  
    });
}
//Termino Eventos Solicitudes -----------------------------------------------------------------------------------------------------------------


function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}
//Modal progress, uso de animación de preogresso, indicador de actividad -------------------------------------------------
function MostrarProgress() {
    $('[id$=Upgrade]').show();
}
function OcultarProgress() {
    $('[id$=Upgrade]').delay(2000).hide();
}

function Hab_Des_RevisionPredio() {
    var grupo = $("#Cmb_Medidor option:selected").val();
    
    if (grupo == "SI") {
        $('#Txt_No_Medidor').removeAttr('disabled');
        $('#Txt_Lectura_Medidor').removeAttr('disabled');
        $('#Txt_Medida_Largo').removeAttr('disabled');
        $('#Txt_Medida_Ancho').removeAttr('disabled');
        $('#Txt_Diametro_Toma').removeAttr('disabled');       
    }
    else {        
        $('#Txt_No_Medidor').removeAttr('style');
        $('#Txt_No_Medidor').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
        $('#Txt_No_Medidor').attr('disabled', 'disabled');
        $('#Txt_No_Medidor').val("0");
                
        $('#Txt_Lectura_Medidor').removeAttr('style');
        $('#Txt_Lectura_Medidor').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
        $('#Txt_Lectura_Medidor').attr('disabled', 'disabled');
        $('#Txt_Lectura_Medidor').val("0");
        
        $('#Txt_Medida_Largo').removeAttr('style');
        $('#Txt_Medida_Largo').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
        $('#Txt_Medida_Largo').attr('disabled', 'disabled');
        $('#Txt_Medida_Largo').val("0");
                
        $('#Txt_Medida_Ancho').removeAttr('style');
        $('#Txt_Medida_Ancho').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
        $('#Txt_Medida_Ancho').attr('disabled', 'disabled');
        $('#Txt_Medida_Ancho').val("0");
        
        $('#Txt_Diametro_Toma').removeAttr('style');
        $('#Txt_Diametro_Toma').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
        $('#Txt_Diametro_Toma').attr('disabled', 'disabled');
        $('#Txt_Diametro_Toma').val("0");
    }
}
