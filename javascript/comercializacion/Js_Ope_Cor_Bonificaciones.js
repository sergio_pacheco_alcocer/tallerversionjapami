﻿
var lastIndex;
var iva;
var fecha_emision;
var panel_faturado;
var panel_usuario;
var renglon;
var id_bonificacion;
var no_factura_recibo;

var objPagosBonificaciones={
 motivo_bonificacion_id:'',no_cuenta:'',
 tipo:'',descripcion_bonificacion:'',no_factura_recibo:'',
 nombre:'',domicilio:'',colonia:'',iva:0 
};


$(function(){ //inicio
  // ocultamos la ventana de mensajes 
 $('#ventana_mensaje').window('close');
 crearGridUsuarios();
 crearGridFacturas();

 limpiarControles();
 
 


// bloquamos botones de impresion
bloquerBotoneImpresion();

});//fin

//---------------------------------------------------------------------------------------------------

function abrirVentanaBusqueda(){

   $('#ventana_busqueda').window('open');
}

function cerrarVentanBusqueda(){
  $('#ventana_busqueda').window('close');
}


function bloquearControles(){
     $('#btn_buscar_usuario').linkbutton({disabled:true});
     $('#btn_guardar').linkbutton({disabled:true});
     $('#btn_abrir_ventana_busqueda').linkbutton({disabled:true});        
}

function desbloquearControles(){
   $('#btn_buscar_usuario').linkbutton({disabled:false});
   $('#btn_guardar').linkbutton({disabled:false});    
   $('#btn_abrir_ventana_busqueda').linkbutton({disabled:false});
}

function bloquerBotoneImpresion(){
   $('#btn_imprimir_recibo').linkbutton({disabled:true});
   $('#btn_imprimir_formato').linkbutton({disabled:true});

}

function desbloquearBotonesImpresion(){
   $('#btn_imprimir_recibo').linkbutton({disabled:false});
   $('#btn_imprimir_formato').linkbutton({disabled:false});
}

function limpiarGrids(){
  
  $('#grid_facturado').datagrid('loadData',{ total:0, rows: [] });
  $('#grid_facturado').datagrid('reloadFooter',{ rows: [] });
}


//-------------------------------------------------------------------------------------------------

function nuevoProceso(){

    
    $('#txt_descripcion_bonificacion').val('');    
    $('#txt_cantidad_bonficacion').val('');    
    limpiarControles(); 
    limpiarGrids();
    bloquerBotoneImpresion();
    desbloquearControles();

}


//---------------------------------------------------------------------------------------------------

function crearGridFacturas(){
 $('#grid_facturado').datagrid({  // tabla inicio
            title: 'Saldo',
            width: 815,
            height: 300,
            columns: [[
        { field: 'total_importe', title: 'total_importe', width: 0, sortable: true },
        { field: 'total_pagar', title: 'total_pagar', width: 0, sortable: true },    
        { field: 'no_cuenta', title: 'No Cuenta', width: 0, sortable: true },
        { field: 'no_factura_recibo', title: 'no_factura', width: 0, sortable: true },
        { field: 'impuesto_saldo', title: 'impuesto_saldo', width: 0, sortable: true },
        { field: 'total_iva', title: 'total_iva', width: 0, sortable: true },
        { field: 'fecha_emicion', title: 'Fecha', width: 0, sortable: true },
        { field: 'iva', title: 'iva', width: 0, sortable: true },
        { field: 'saldo', title: 'saldo', width: 0, sortable: true },
        { field: 'concepto_id', title: 'clave', width: 0, sortable: true },
        { field: 'concepto', title: 'Concepto', width: 150 },
        { field: 'debe', title: 'Debe', width: 150 },
        { field: 'paga', title: 'Paga', width: 150, editor:{type:'numberbox',options:{precision:2, min:0, max:999999}} },
        { field: 'ajuste', title: 'Bon/Ajust', width:150}


    ]],  onClickRow: function(rowIndex, rowData) {
            
    },
            pageSize: 50,
            pagination: false,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            showFooter: true,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap: false

        });  // tabla final
        
        $('#grid_facturado').datagrid('hideColumn','total_importe');
        $('#grid_facturado').datagrid('hideColumn','total_pagar');       
        $('#grid_facturado').datagrid('hideColumn','impuesto_saldo');
        $('#grid_facturado').datagrid('hideColumn','no_factura_recibo');
        $('#grid_facturado').datagrid('hideColumn','no_cuenta');
        $('#grid_facturado').datagrid('hideColumn','total_iva'); 
        $('#grid_facturado').datagrid('hideColumn','fecha_emicion');
        $('#grid_facturado').datagrid('hideColumn','iva');
        $('#grid_facturado').datagrid('hideColumn','saldo');   
        $('#grid_facturado').datagrid('hideColumn','concepto_id'); 

}


//---------------------------------------------------------------------------------------------------

function crearGridUsuarios(){

      $('#grid_usuarios').datagrid({  // tabla inicio
            title: 'Usuarios',
            width: 790,
            height: 250,
            columns: [[
        { field: 'no_cuenta', title: 'No Cuenta', width: 150, sortable: true },
        { field: 'usuario', title: 'Usuario', width: 250, sortable: true },
        { field: 'giro', title: 'Giro', width: 200, sortable: true },
        { field: 'iva', title: 'IVA', width: 80, sortable: true },
        { field: 'tarifa', title: 'Tarifa', width: 300, sortable: true },
        { field: 'calle', title: 'Calle', width: 250, sortable: true },
        { field: 'colonia', title: 'Colonia', width: 250, sortable: true },
        { field: 'exterior', title: 'No. Exterior', width: 100, sortable: true },
        { field: 'interior', title: 'No. Interior', width: 100, sortable: true }

    ]],     
             onClickRow: function(rowIndex, rowData) {
             
                    
            }, 
            pageSize: 50,
            pagination: true,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            showFooter: false,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap: false

        });  // tabla final

}

//---------------------------------------------------------------------------------------------------

function seleccionarElemento(){


 renglon=$('#grid_usuarios').datagrid('getSelected');
  
if(renglon==null){
  $.messager.alert('Japami','Tienes que seleccionar un elemento','info');
  return;
}    

$('#grid_facturado').datagrid('loadData',{total:0, rows:[]});
$('#grid_facturado').datagrid('reloadFooter',{ rows: [] });
$('#grid_facturado').datagrid({url:'../../Controladores/frm_controlador_bonificaciones.aspx',queryParams:{accion:'obtener_facturado',nocuenta:renglon.no_cuenta},pageNumber: 1});

$('#txt_no_cuenta_sel').val(renglon.no_cuenta);
$('#txt_nombre_sel').val(renglon.usuario);

cerrarVentanBusqueda();  
  

}

//--------------------------------------------------------------------------------------------------

function buscarUsuario()
{
   var no_cuenta;
   var nombre_usuario;
   var renglones;
   
   no_cuenta=$.trim( $("#txt_no_cuenta").val());
   nombre_usuario=$.trim( $("#txt_usuario").val());
   
   if ($('#opt_no_cuenta').attr('checked')){
      
      if (no_cuenta.length==0){
            $.messager.alert('Japami','Introduce un No. de cuenta');
            return ; 
      }
   }else {no_cuenta='';}
   
   if($('#opt_usuario').attr('checked')){
       if(nombre_usuario.length==0){
           $.messager.alert('Japami','Introduce un No. de cuenta');
            return ; 
       }
      
   }else{nombre_usuario='';}
   
   
   limpiarControles(); 
   $('#grid_usuarios').datagrid('loadData',{total:0, rows:[]});
   $('#grid_facturado').datagrid('loadData',{total:0, rows:[]});
   $('#grid_usuarios').datagrid({ url: '../../Controladores/frm_controlador_bonificaciones.aspx', queryParams: { accion: 'obtener_usuario', nocuenta: no_cuenta, nombre: nombre_usuario }, pageNumber: 1 });


   
}

//----------------------------------------------------------------------
function asignarDescuento(){
var renglon_l;
var cantidad_asignar_l;
var valor_asignar_l;
var index_l;
var datos_grid_l;   
var renglones_l;


   
renglones_l=$('#grid_facturado').datagrid('getRows');
if(renglones_l.lengt==0){
   $.messager.alert('Japami','No hay datos que seleccionar','info');
   return;  
}
   
   
renglon_l=$('#grid_facturado').datagrid('getSelected');
index_l=$('#grid_facturado').datagrid('getRowIndex',renglon_l);
datos_grid_l=$('#grid_facturado').datagrid('getData');

if(renglon_l==null){
   $.messager.alert('Japami','Tienes que seleccionar el concepto al cual deseas hacerle un descuento','info');
   return;  
}
concepto_debe_b=parseFloat(renglon_l.debe);


if(isNaN(concepto_debe_b) || concepto_debe_b<=0 || concepto_debe_b=='undefined'){
   $.messager.alert('Japami','La cantidad a ajustar es cero','info');
   return;
      
}

cantidad_asignar_l= parseFloat( $("[id$='txt_cantidad_bonficacion']").val());

if(isNaN(cantidad_asignar_l) || cantidad_asignar_l<0 || cantidad_asignar_l=='undefined'){
   $.messager.alert('Japami','Cantidad Asignar Incorrecta','info');
   return;
      
}


if($('#opt_porcentaje_b').attr('checked')){
   
   if(cantidad_asignar_l> 100){
      $.messager.alert('Japami','Porcentaje Incorrecto','info');
     return;
   }
   
   valor_asignar_l=  decimal( (cantidad_asignar_l * concepto_debe_b) / 100,2);
   
}


if($('#opt_cantidad_b').attr('checked')){
    
    if(cantidad_asignar_l>concepto_debe_b){
      $.messager.alert('Japami','Cantidad Incorrecta','info');
      return;
    }
   valor_asignar_l=cantidad_asignar_l;
}



var aux_l= decimal( concepto_debe_b- valor_asignar_l,2);

if (aux_l==0){
      datos_grid_l.rows[index_l].paga= 0;
      datos_grid_l.rows[index_l].ajuste=valor_asignar_l;
   }else{
    datos_grid_l.rows[index_l].paga=valor_asignar_l    
    datos_grid_l.rows[index_l].ajuste=aux_l; 
  }



$('#grid_facturado').datagrid('loadData',datos_grid_l);
var importe_l;
var total_iva_l;
var total_l
var tasa_iva_l= parseInt( renglon_l.iva);


importe_l=  decimal( obtenerSumaGrid("paga"),2);
total_iva_l= decimal( (importe_l * tasa_iva_l)/100,2);
total_l=  decimal( importe_l + total_iva_l,2);


 // update footer row values and then refresh
      var rows = $('#grid_facturado').datagrid('getFooterRows');
      rows[0]['paga'] = importe_l;
      rows[1]['paga'] = total_iva_l
      rows[2]['paga'] = total_l;
     $('#grid_facturado').datagrid('reloadFooter');

importe_l= decimal( obtenerSumaGrid("ajuste"),2);
total_iva_l= decimal( (importe_l * tasa_iva_l)/100,2);
total_l= decimal( importe_l + total_iva_l,2);


rows = $('#grid_facturado').datagrid('getFooterRows');
      rows[0]['ajuste'] = importe_l;
      rows[1]['ajuste'] = total_iva_l
      rows[2]['ajuste'] = total_l;
     $('#grid_facturado').datagrid('reloadFooter');


$("[id$='txt_cantidad_bonficacion']").val('');

}



//-----------------------------------------------------------------------------


function obtenerSumaGrid(columna){

var renglon_l;
var renglones_l;
var suma_l=0;
var aux;

renglones_l=$('#grid_facturado').datagrid('getRows');

for(var i=0; i<renglones_l.length;i++){
  renglon_l=renglones_l[i];
  
   aux=parseFloat(renglon_l[columna]);
   if( isNaN(aux) || aux=='undefined') 
       aux=0; 
    suma_l=suma_l + aux;
     
} 

return suma_l;

}



//-----------------------------------------------------------------------------

function limpiarControles()
{
  
 $("[id$='txt_no_cuenta_sel']").val('');
 $("[id$='txt_nombre_sel']").val('');

}

//-----------------------------------------------------------------------------

function GuardarBonificacion(){
var renglones_datos;
var renglones_totales;
var paga;
var p_totales;
var p_totales_detalles;
var p_motivo;
var p_tipo_ajustes;
var p_descripcion;
var p_Nombre;
var p_Domicilio;
var p_Colonia;
var p_Tasa_IVA;
var p_Cantidad_IVA;


no_factura_recibo="";
id_bonificacion="";
 
renglones_datos=$('#grid_facturado').datagrid('getRows');
if(renglones_datos.length==0){
   $.messager.alert('Japami','No hay datos que guardar','info');
   return;  
}

renglones_totales=$('#grid_facturado').datagrid('getFooterRows');
 
 
 paga= parseFloat(renglones_totales[2].ajuste); 
     
  if(paga<=0  || isNaN(paga) || paga=='undefined' ){
     $.messager.alert('Japami','No se ha hecho ninguna bonificacion o ajuste');
     return;
  }
  
  
  
  p_motivo= $("[id$='cmb_motivo_modificacion']").attr('value');

  if (p_motivo == "0" || p_motivo == "Seleccione") {
    $.messager.alert('Japami','Tienes que seleccionar un motivo de la bonificaci&oacute;n o ajuste');
     return;
 }
  
 p_descripcion=$.trim( $('#txt_descripcion_bonificacion').val());
 
 if (p_descripcion.length==0){
      $.messager.alert('Japami','Tienes que Escribir una descripcion');
      return ;
 }
    
  if($('#opt_tipo_bonificacion').attr('checked'))
       p_tipo_ajustes='BONIFICACION';
    else
      p_tipo_ajustes='AJUSTE';   
  
 
  
var strBonificacionesDetalles= toJSON(renglones_datos);
var strBonificacionesTotales= toJSON(renglones_totales);



no_factura_recibo=renglones_datos[0].no_factura_recibo;
objPagosBonificaciones.no_factura_recibo= renglones_datos[0].no_factura_recibo;
objPagosBonificaciones.no_cuenta=$("[id$='txt_no_cuenta_sel']").val();
objPagosBonificaciones.nombre=$("[id$='txt_nombre_sel']").val();
objPagosBonificaciones.domicilio= renglon.calle + 'No. Ext. ' + renglon.exterior;
objPagosBonificaciones.colonia=renglon.colonia;
objPagosBonificaciones.motivo_bonificacion_id=p_motivo;
objPagosBonificaciones.descripcion_bonificacion=p_descripcion;
objPagosBonificaciones.tipo=p_tipo_ajustes;
objPagosBonificaciones.iva=renglones_datos[0].iva;

var strPagosBonificacion= toJSON(objPagosBonificaciones);
 
$('#ventana_mensaje').window('open');

 $.ajax({
     type: "POST",
     url: "../../Controladores/frm_controlador_bonificaciones.aspx/guardarBonificacion",
     data: "{'strBonificacion':'" + strPagosBonificacion + "','strBonificacion_detalle':'" + strBonificacionesDetalles + "','strBonificacionTotales':'" + strBonificacionesTotales + "'}",
     contentType: "application/json; charset=utf-8",
     dataType: "json",
     success: function (response) {
         $('#ventana_mensaje').window('close');
         var respuesta = eval("(" + response.d + ")");        
         if (respuesta.mensaje == "bien") {
           procesoExitoso(respuesta);
            
         }
         else {
             $('#ventana_mensaje').window('close');
             $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');
         }


     },

     error: function (result) {
         $('#ventana_mensaje').window('close');
         $.messager.alert('ERROR ' + result.status + ' ' + result.statusText);
     }
 });
 

}


//--------------------------------------------------------------------------------
function   procesoExitoso(respuesta){
 
  
  $.messager.alert('Japami','Proceso Terminado','info',function(){
    id_bonificacion=respuesta.dato1;
    bloquearControles();
    desbloquearBotonesImpresion(); 
     ImprimirBonificacion();
  
  });
  
}

//-------------------------------------------------------------------------------------
function generarReporteFormatoBonificaciones(){  
  if ( id_bonificacion.length == 0) {        
            $.messager.alert('Japami', 'No hay datos que Imprimir');
            return;      
    } else {
            window.open("../../imprimir/frm_imprimir_bonificacion.aspx?&bonificaciones_id=" + id_bonificacion + "", "Impresion", "width=100,height=100,left=0,top=0");            
     }  
  
}

//----------------------------------------------------------------------------------------------------------------------------------


function mandar_imprimir_bonificacion(){
  
  if (id_bonificacion.length==0){
       $.messager.alert('Japami', 'No hay datos que Imprimir');
       return
  }else{
    window.open("../../Reporte/frm_controlador_crystal.aspx?nombre_reporte=formato_bonificacion&bonificacion_id=" + id_bonificacion ,"Bonificacion","width=400px,height=400px,scrollbars=NO");
  }
  
  
}


//-----------------------------------------------------------------------------------------------------
Number.prototype.decimal = function(num) {
    pot = Math.pow(10, num);
    return parseInt(this * pot) / pot;
}

function decimal(valor,no_decimales) {
    n = eval(valor);
    return n.decimal(no_decimales);
}
function Rdb_opt_tipo_bonificacion_Click() {
if ($('#opt_tipo_bonificacion').attr('checked')) {
            alert('if');
            var criterio = $('#opt_tipo_bonificacion').val();
            cargarCombo(criterio);
        }
 }
onclick="Rdb_opt_tipo_bonificacion_Click()"

$(document).ready(function() {

    //Evento del radio buton  opt_tipo_bonificacion
    $('#opt_tipo_bonificacion').change(function(event) {
        if ($('#opt_tipo_bonificacion').attr('checked')) {
            
            var criterio = $('#opt_tipo_bonificacion').val();
            cargarCombo(criterio);
        }
    });

    //Evento del radiobuton opt_tipo_ajuste
    $('#opt_tipo_ajuste').change(function(event) {
    if ($('#opt_tipo_ajuste').attr('checked')) {
       
            var criterio = $('#opt_tipo_ajuste').val();
            cargarCombo(criterio);
        }
    });

});

function cargarCombo(criterio) {
    $('#ventana').window('open');
    $.ajax({
        url: "../../Controladores/frm_controlador_bonificaciones.aspx?Accion=datos_Combo" +
            "&Bonificacion_o_Ajuste=" + criterio,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            $('#ventana').window('close');
            if (data != null) {
                var select = $("[id$='cmb_motivo_modificacion']");
                $('option', select).remove();
                var options = '<option value="Seleccione"><-SELECCIONE-></option>';
                $.each(data, function(i, item) {
                    options += '<option value="' + item.motivo_bonificacion_id + '">' + item.descripcion + '</option>';
                });
                $("[id$='cmb_motivo_modificacion']").append(options);

                //$('#cmb_motivo_modificacion').show();
            }
        },
        error: function(result) {
        $('#ventana').window('close');
            alert("ERROR " + result.status + " " + result.statusText);
        }
    });
}

function ImprimirBonificacion() {
    var no_factura_recibo = '';
    var bonificacion ='';
    
    no_Bonificacion = id_bonificacion;
    window.open("../../Reporte/frm_controlador_reporte_pdf.aspx?nombre_reporte=formato_Bonificaciones&no_Bonificacion=" + no_Bonificacion +"", "Bonificacion", "width=1000px,height=1000px,scrollbars=YES,resizable=YES");       
    

}
