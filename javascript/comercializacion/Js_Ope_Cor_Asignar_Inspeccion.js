﻿
$(function() {

    crearTab();
    crearGridInspeccionesPendientes();
    crearGridEmpleadoInspector();
    obtenerInspectores();
    auto_completado_colonias();
    auto_completado_calles();
    // ventana    modal;
    $('#ventana_mensaje').window('close');


});


//------------------------------------------------------------------------------------------------

function format(item) {
    return item.nombre;
}

function auto_completado_colonias() { 

//inciamos el auto completado
    $("[id$='txt_colonia']").autocomplete("../../Controladores/frm_controlador_inspecciones.aspx", {
        extraParams: { accion: 'autocompletar_colonia' },
        dataType: "json",
        parse: function(data) {
            return $.map(data, function(row) {
                return {
                    data: row,
                    value: row.colonia_id,
                    result: row.nombre

                }
            });
        },
        formatItem: function(item) {
            return format(item);
        }
    }).result(function(e, item) {
        
    });

}


//------------------------------------------------------------------------------------------------



function format_calle(item) {
    // como se ve en el div contendor
    return item.calle;
}

function auto_completado_calles() {

    //inciamos el auto completado
    $("[id$='txt_calle']").autocomplete("../../Controladores/frm_controlador_inspecciones.aspx", {
    extraParams: { accion: 'autocompletar_calle' },
        dataType: "json",
        parse: function(data) {
            return $.map(data, function(row) {
                return {
                    data: row,
                    value: row.calle_id,
                    result: row.calle 

                }
            });
        },
        formatItem: function(item) {
            return format_calle(item);
        }
    }).result(function(e, item) {

    });

}


//------------------------------------------------------------------------------------------------

function crearTab() {
    $('#tab').tabs({
        onSelect: function(title) {


        }
    });
}

//------------------------------------------------------------------------------------------------

function crearGridInspeccionesPendientes() {

    $('#grid_inspecciones_pendientes').datagrid({  // tabla inicio
        title: 'Inspecciones Pendientes',
        width: 820,
        height: 250,
        columns: [[
        { field: 'no_inspeccion', title: 'ID', width: 50, sortable: true },
        { field: 'folio_inspeccion', title: 'Folio', width: 100, sortable: true },
        { field: 'no_cuenta', title: 'No.Cuenta', width: 100, sortable: true },
        { field: 'sector', title: 'Sector', width: 100, sortable: true },
        { field: 'calle', title: 'Calle', width: 200, sortable: true },
        { field: 'colonia', title: 'Colonia', width: 200, sortable: true },
        { field: 'numero_interior', title: 'Num.Int.', width: 70, sortable: true },
        { field: 'numero_exterior', title: 'Num.Ext.', width: 70, sortable: true },
        { field: 'condicion_revision', title: 'Condiciones rev.', width: 200, sortable: true },
        { field: 'solicito', title: 'Solicito', width: 200, sortable: true },
        { field: 'motivo', title: 'Motivo', width: 200, sortable: true },
        { field: 'fecha_elaboro', title: 'Fecha Elaboro', width: 150, sortable: true },      
        { field: 'status', title: 'Estado', width: 150, sortable: true },
        { field: 'no_solicitud', title: 'No.Solicitud', width: 150, sortable: true }
           
    ]],
        pageSize: 10,
        pageList: [30],
        pagination: true,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        striped: true,
        loadMsg: 'cargando...',
        nowrap: false,
        toolbar: [{
            id: 'btnasignar_inspeccion',
            text: 'Asignar',
            iconCls: 'icon-ok',
            handler: function() {
               asignarInspeccion();
            }
        }],
        onClickRow: function(rowIndex, rowData) {  
        },
        onBeforeLoad: function() {
          
        },
        onLoadSuccess: function() {

        }

    });          // tabla final

   $('#grid_inspecciones_pendientes').datagrid('hideColumn', 'no_inspeccion');

}

//------------------------------------------------------------------------------------------------

function crearGridEmpleadoInspector() {

    $('#grid_inspectores').datagrid({  // tabla inicio
        title: 'Inspectores',
        width: 820,
        height: 160,
        columns: [[
        { field: 'empleado_id', title: 'ID', width: 50 },
        { field: 'area_id', title: 'ID', width: 50 },
        { field: 'no_empleado', title: 'No.Empleado', width: 100 },
        { field: 'nombre', title: 'Nombre', width: 100 },
        { field: 'a.paterno', title: 'Apell. Parteno', width: 100},
        { field: 'a.materno', title: 'Apell. Materno', width: 100 },
        { field: 'area', title: 'Area', width: 100, sortable: true }       
    ]],
        pageSize: 10,
        pageList: [30],
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        striped: true,
        loadMsg: 'cargando...',
        nowrap: false,
        onClickRow: function(rowIndex, rowData) {
        },
        onBeforeLoad: function() {

        },
        onLoadSuccess: function() {

        }

    });          // tabla final

    $('#grid_inspectores').datagrid('hideColumn', 'empleado_id');
    $('#grid_inspectores').datagrid('hideColumn', 'area_id');


 }

 //------------------------------------------------------------------------------------------------

 function obtenerInspectores() {

     $('#grid_inspectores').datagrid({ url: '../../Controladores/frm_controlador_inspecciones.aspx', queryParams: {
         accion: 'obtener_inspectores'
     }, pageNumber: 1
     });
    

 }


 //------------------------------------------------------------------------------------------------
 //------------------------------------------------------------------------------------------------
 //------------------------------------------------------------------------------------------------

 function buscarInspecciones() {
     var datos_busqueda;
     var datos={};

     if (validar_check_box_cajas_textos("bus_inspeccion",datos)) {
         return false;
     }
     //datos_busqueda = JSON.stringify($("#bus_inspeccion :input").serializeObject());
     datos_busqueda=toJSON(datos);
    
     $('#grid_inspecciones_pendientes').datagrid('loadData', { total: 0, rows: [] });
     $('#grid_inspecciones_pendientes').datagrid({ url: '../../Controladores/frm_controlador_inspecciones.aspx', queryParams: {
     accion: 'obtener_inspecciones_no_asignadas',
     datos_busqueda:datos_busqueda,
     estado:'GENERADA'    
     }, pageNumber: 1
     });
    
    
 }
 
 //------------------------------------------------------------------------------------------------
 //------------------------------------------------------------------------------------------------
 //------------------------------------------------------------------------------------------------

 function asignarInspeccion(){
   var renglon_inspecciones;
   var renglon_inspector;
   
   renglon_inspecciones=$('#grid_inspecciones_pendientes').datagrid('getSelected');
   renglon_inspector=$('#grid_inspectores').datagrid('getSelected');
   
   
   if(renglon_inspecciones==null){
      $.messager.alert('Japami','Tienes que Seleccionar una Inspecci&oacute;n pendiente');
      return;
   }
   
  if(renglon_inspector==null){
      $.messager.alert('Japami','Tines que Seleccionar un Inspector');
      return ;
  }
   
   
    $('#ventana_mensaje').window('open');
    
        $.ajax({
            type: "POST",
            url: "../../Controladores/frm_controlador_inspecciones.aspx/cambioEstadoSolicitud",
            data: "{'no_inspeccion':'" + renglon_inspecciones.no_inspeccion + "','inspector_id':'" + renglon_inspector.empleado_id + "','inspector_area_id':'" + renglon_inspector.area_id + "','estado':'ASIGNADA'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                var respuesta = eval("(" + response.d + ")");
                if (respuesta.mensaje == "bien") {
                    $('#ventana_mensaje').window('close');
                     var index=$('#grid_inspecciones_pendientes').datagrid('getRowIndex',renglon_inspecciones);
                    $('#grid_inspecciones_pendientes').datagrid('deleteRow',index);
                    
                    $.messager.alert('Japami', 'Proceso terminado');
                }
                else {
                    $('#ventana_mensaje').window('close');
                    $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                }

            },
            error: function(result) {
                $('#ventana_mensaje').window('close');
                $.messager.alert('Japami', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');

            }

        });
   
 
 }
 
 
 
 