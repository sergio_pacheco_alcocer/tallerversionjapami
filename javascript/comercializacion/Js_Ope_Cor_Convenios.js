﻿//*******************************************************************************
///NOMBRE DE LA FUNCIÓN : $(document).ready()
///DESCRIPCIÓN          : Funcion que indica cuando el documento ha sido cargado, para poder modificar los elementod DOM
///PARAMETROS           :   
///CREO                 : Julio Cruz
///FECHA_CREO           : 29/Octubre/2011 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///*******************************************************************************
function EjecutaFuncion() {
    $(document).ready(function() {
        jQuery.ajaxSetup({
            async: false,
            cache: false,
            timeout: (2 * 1000)//,
        });

        // evento click de la nota de cargo

        $("[id$='img_imprimir_nota_cargo']").click(function(event) {
            var nota_cargo_id;
            nota_cargo_id = $("[id$='txt_nota_cargo_id_hidden']").val();
            window.open("../../Reporte/frm_controlador_crystal.aspx?nombre_reporte=formato_nota_cargo&nota_cargo_id=" + nota_cargo_id, "NotaCargo", "width=400px,height=400px,scrollbars=NO");
            event.preventDefault();
        });

        //Prepara la tabla
        $('#Grid_Predios').datagrid({
            title: 'Catalogo Predios',
            width: 700,
            height: 220,
            columns: [[
                    { field: 'boton', title: 'Sel', width: 20, align: 'center',
                        formatter: function(value, rec) {
                            return '<input type="button" id="btnRealizar_' + rec.Predio_ID + '" style="width:10px" AutoPostBack="true" onclick="Btn_Valor_ID(' + rec.Predio_ID + ',' + rec.No_Cuenta + ')" />';
                        }
                    },
                    { field: 'Predio_ID', title: 'Predio ID', width: 1, hidden: true },
                    { field: 'Domicilio', title: 'Domicilio', width: 300, sortable: true },
                    { field: 'No_Cuenta', title: 'No Cuenta', width: 80, sortable: true },
                    { field: 'Estatus', title: 'Estatus', width: 80, sortable: true }
            ]],
            pagination: true,
            rownumbers: true,
            idField: 'Predio_ID',
            sortOrder: 'asc',
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            loadMsg: "Cargando, espere ...",
            pageNumber: 1,
            pageSize: 10,
            fitColumns: true,
            striped: true,
            nowrap: false,
            pageList: [10, 20],
            remoteSort: false,
            showFooter: true
        });

        //Prepara la tabla Recibos
        $('#Grid_Facturacion_Recibos').datagrid({
            title: 'Facturas',
            width: 758,
            height: 200,
            columns: [[
                    { field: 'boton', title: 'Sel', width: 20, align: 'center',
                        formatter: function(value, rec) {
                            return '<input type="button" id="btnRealizar_' + rec.No_Factura_Recibo + '" style="width:10px"/>';
                        }
                    },
                    { field: 'No_Factura_Recibo', title: 'No. Recibo', width: 300, sortable: true },
                    { field: 'Concepto', title: 'Concepto', width: 80, sortable: true },
                    { field: 'Total_Saldo', title: 'Saldo', width: 80, sortable: true },
                    { field: 'Concepto_Id', title: 'concepto_id', width: 80, sortable: true }

            ]],
            pagination: true,
            rownumbers: true,
            idField: 'No_Factura_Recibo',
            sortOrder: 'asc',
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            loadMsg: "Cargando, espere ...",
            pageNumber: 1,
            pageSize: 10,
            fitColumns: true,
            striped: true,
            nowrap: false,
            pageList: [10, 20],
            remoteSort: false,
            showFooter: true,
            onLoadSuccess: function(data) {
                var datos;
                var rezagos = 0;
                var recargos = 0;

                if (data.total > 0) {
                    datos = toJSON($('#Grid_Facturacion_Recibos').datagrid('getRows'));
                    $("[id$='txt_facturado_hidden']").val(datos);
                    for (var i = 0; i < data.rows.length; i++) {
                        if (data.rows[i].Concepto == "Rezago") {
                            rezagos = parseFloat(data.rows[i].Total_Saldo);
                            $("[id$='txt_Rezagos_descuento_hidden']").val(rezagos);
                        }
                        if (data.rows[i].Concepto == "Recargo") {
                            recargos = parseFloat(data.rows[i].Total_Saldo);
                            $("[id$='txt_Recargos_descuento_hidden']").val(recargos);
                        }
                    }


                } else { $("[id$='txt_facturado_hidden']").val(0); }



            }
        });

        $('Grid_Facturacion_Recibos').datagrid('hideColumn', 'Conepto_Id');

        $('#Btn_Filtrar_Predios').click(function(event) {
            Filtrar_Predios();
            event.preventDefault();
        });

        $('#Img_Consultar_Facturas').click(function(event) {
            var no_cuenta_b;

            no_cuenta_b = $.trim($('#Txt_Cuenta_Usuario').val());

            if (no_cuenta_b.length == 0) {
                $.messager.alert('Japami', 'Tienes que escribir un numero de cuenta');
                return;
            }

            limpiarControles();
            Consulta_Datos_Usuario();
            Validar_Convenios_Pendientes();
            event.preventDefault();
        });

        AutocompletadoColonias();
        var colonia_id = $("[id$='Txt_Busqueda_Colonia_Hidden']").val();
        AutocompletadoCalles(colonia_id);
        AutocompletadoZonas();
        AutocompletadoGiros();
        AutocompletadoTarifas();
        AutocompletadoFraccionador();
        AutocompletadoViviendas();


    });
}

///*******************************************************************************
///NOMBRE DE LA FUNCIÓN : Btn_Valor_ID
///DESCRIPCIÓN          : obtiene el predio y el numero de cuenta del usuario seleccionado
///PARAMETROS           :   
///CREO                 : Julio Cruz
///FECHA_CREO           : 6/Noviembre/2011 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///*******************************************************************************
function Btn_Valor_ID(Predio_ID, No_Cuenta) {
    //Se asigna la cuenta de Usuario
    $('#Txt_Cuenta_Usuario').val(No_Cuenta);
    $("[id$='_Txt_Cuenta_Usuario_Hidden']").val(No_Cuenta);
    $("[id$='_Txt_Predio_ID_Hidden']").val(Predio_ID);
    //Se cierra el modalPopup
    ClosePopup();
}

///*******************************************************************************
///NOMBRE DE LA FUNCIÓN : Consulta_Facturas_Detalles
///DESCRIPCIÓN          : Consulta los detalles de las facturas
///PARAMETROS           :   
///CREO                 : Julio Cruz
///FECHA_CREO           : 6/Noviembre/2011 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///*******************************************************************************
function Consulta_Facturas_Detalles() {
    var parametros = "";
    parametros += "&No_Cuenta=" + $('#Txt_Cuenta_Usuario').val();
    //No registros por pagina
    $('#Grid_Facturacion_Recibos').datagrid({
        url: '../../Controladores/Frm_Controlador_Convenios.aspx?Accion=Llena_Grid_Facturas' + parametros,
        pageNumber: 1
    });
}

///*******************************************************************************
///NOMBRE DE LA FUNCIÓN : Obtiene_Saldo
///DESCRIPCIÓN          : Consulta el saldo del usuario
///PARAMETROS           :   
///CREO                 : Julio Cruz
///FECHA_CREO           : 4/Noviembre/2011 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///*******************************************************************************
function Obtiene_Saldo() {
    var parametros = "";
    $('#Txt_Saldo').val('');
    $('#Txt_Saldo_Datos_Convenio').val('');
    $("[id$='_Txt_Saldo_Hidden']").val('');
    parametros += "&No_Cuenta=" + $('#Txt_Cuenta_Usuario').val();
    $.ajax({
        type: "POST",
        url: "../../Controladores/Frm_Controlador_Convenios.aspx?Accion=Saldo" + parametros,
        contentType: "TEXT",
        async: false,
        cache: false,
        success: function (cadena) {
            cadena = cadena.slice(0, cadena.indexOf('}') + 1);
            var obj = jQuery.parseJSON(cadena);
            $('#Txt_Saldo').val(obj.Saldo);
            $("[id$='_Txt_Saldo_Hidden']").val(obj.Saldo);
            $('#Txt_Saldo_Datos_Convenio').val(obj.Saldo);
            //Impuesto
            $("[id$='_Hdd_Impuesto']").val(obj.Impuesto);
        },
        error: function(result) {
            alert('ERROR ' + result.status + ' ' + result.statusText);
        }
    });
}



function limpiarControles() {
    $("[id$='_Txt_Predio_ID_Hidden']").val('');

    $('#Txt_Usuario').val('');
    $('#Txt_Nombre_Solicitante_Convenio').val('');
    $("[id$='_Txt_Nombre_Solicitante_Convenio_Hidden']").val('');
    $('#Txt_Apellido_Paterno').val('');
    $('#Txt_Apellido_Materno').val('');
    $('#Txt_Giro').val('');
    $('#txt_periodos_Adeudos').val('');
    $('#Grid_Facturacion_Recibos').datagrid('loadData', { total: 0, rows: [] });
    $('#Txt_Saldo').val('');
    $("[id$='_Txt_Saldo_Hidden']").val('');
    $('#Txt_Saldo_Datos_Convenio').val('');
    $("[id$='_Hdd_Impuesto']").val('');

}

///*******************************************************************************
///NOMBRE DE LA FUNCIÓN : Consulta_Datos_Usuario
///DESCRIPCIÓN          : Consulta los datos del usuario, recibe un string que convierte a datos JSON
//                        Ejemplo: var obj = jQuery.parseJSON('{"name":"John","Apellido_Paterno":"p","Apellido_Materno":"m"}');
//                        alert(obj.name);            
///PARAMETROS           :   
///CREO                 : Julio Cruz
///FECHA_CREO           : 4/Noviembre/2011 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///*******************************************************************************
function Consulta_Datos_Usuario() {
    var parametros = "";
    parametros += "&No_Cuenta=" + $('#Txt_Cuenta_Usuario').val();
    $.ajax({
        type: "POST",
        url: '../../Controladores/Frm_Controlador_Convenios.aspx?Accion=Datos_Usuario' + parametros,
        success: function(response) {
            response = response.slice(0, response.indexOf('}') + 1);
            var obj = jQuery.parseJSON(response);
            $("[id$='_Txt_Predio_ID_Hidden']").val(obj.PREDIO_ID);
            $('#Txt_Cuenta_Usuario').val(obj.NO_CUENTA);
            $('#Txt_Usuario').val(obj.USUARIO);
            //$('#Txt_Nombre_Solicitante_Convenio').val(obj.NOMBRE);
            $("[id$='_Txt_Nombre_Solicitante_Convenio_Hidden']").val(obj.NOMBRE + ' ' + obj.APELLIDO_PATERNO + ' ' + obj.APELLIDO_MATERNO);
            //$('#Txt_Apellido_Paterno').val(obj.APELLIDO_PATERNO);
            //$('#Txt_Apellido_Materno').val(obj.APELLIDO_MATERNO);
            $('#Txt_Giro').val(obj.NOMBRE_GIRO);
            $('#txt_periodos_Adeudos').val(obj.PERIODOS_ADEUDO);
            $("#Cmb_Periodo").val('MENSUAL');
            $("[id$='_Cmb_Periodo_Hidden']").val('MENSUAL');
            $("#Cmb_Estatus_Convenio_Alta").val('PENDIENTE');
            $("[id$='_Cmb_Estatus_Hidden']").val('PENDIENTE');
            $("[id$='_Txt_Cuenta_Usuario_Hidden']").val($('#Txt_Cuenta_Usuario').val());

            $("[id$='_Txt_Nombre_Solicitante_Convenio']").val(obj.NOMBRE);
            $("[id$='_Txt_Apellido_Paterno']").val(obj.APELLIDO_PATERNO);
            $("[id$='_Txt_Apellido_Materno']").val(obj.APELLIDO_MATERNO);
            $('#Txt_No_Cuenta_2').val(obj.NO_CUENTA);
        },
        error: function(result) {
            alert('ERROR ' + result.status + ' ' + result.statusText);
        }
    });

}

///*******************************************************************************
///NOMBRE DE LA FUNCIÓN : Calcula_Abonos
///DESCRIPCIÓN          : Calcula los abonos segun el saldo dividido entre el plazo
///PARAMETROS           :   
///CREO                 : Julio Cruz
///FECHA_CREO           : 6/Noviembre/2011 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///*******************************************************************************
function Calcula_Abonos() {
    var Abonos = 0;
    var plazo = 0;
    plazo = ($("#Txt_Plazo").val() != '') ? $("#Txt_Plazo").val() : 0;
    if (plazo > 0) {
        Abonos = $("#Txt_Saldo_Datos_Convenio").val() / plazo;
        $("#Txt_Abonos").val(Abonos.toFixed(2));
    }
    $("[id$='_Txt_Plazo_Hidden']").val($("#Txt_Plazo").val());
    $("[id$='_Txt_Abonos_Hidden']").val(Abonos.toFixed(2));
}

///*******************************************************************************
///NOMBRE DE LA FUNCIÓN : Asigna_Comentarios_Hidden
///DESCRIPCIÓN          : Asigna lo de la caja de texto observaciones a un caja de texto hidden
///PARAMETROS           :   
///CREO                 : Julio Cruz
///FECHA_CREO           : 7/Noviembre/2011 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///*******************************************************************************
function Asigna_Comentarios_Hidden() {
    $("[id$='_Txt_Observaciones_Hidden']").val('');
    $("[id$='_Txt_Observaciones_Hidden']").val($("#Txt_Observacioness").val());
}

///*******************************************************************************
///NOMBRE DE LA FUNCIÓN : Asigna_Credencial_Hidden
///DESCRIPCIÓN          : Asigna la caja de texto credencial a una caja de texto hidden
///PARAMETROS           :   
///CREO                 : Julio Cruz
///FECHA_CREO           : 7/Noviembre/2011 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///*******************************************************************************
function Asigna_Credencial_Hidden() {
    $("[id$='_Txt_Credencial_Hidden']").val('');
    $("[id$='_Txt_Credencial_Hidden']").val($("#Txt_Credencial").val());
}

///*******************************************************************************
///NOMBRE DE LA FUNCIÓN : Calcula_Descuento_Total
///DESCRIPCIÓN          : Suma la Cantidad de descuento de los rezagos y recargos
///PARAMETROS           :   
///CREO                 : Armando Zavala Moreno
///FECHA_CREO           : 30/Mayo/2012 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///*******************************************************************************
function Calcula_Descuento_Total() {
    var Saldo_Final = 0;
    var rezago = 0;
    var recargos = 0;
    var Total_Descuento = 0;
    var Pago_Inicial = 0;
    var Por_Rezago = 0;
    var Por_Recargo = 0;
    var Rezago_Hidden = parseFloat($("[id$='txt_Rezagos_descuento_hidden']").val());
    var Recargo_Hidden = parseFloat($("[id$='txt_Recargos_descuento_hidden']").val());

    if ($('input:radio[name=Tipo_Descuento]')[0].checked == true) {
        $('#Txt_Total_Descuento_Rez').attr('disabled', true);
        $('#Txt_Total_Descuento_Rec').attr('disabled', true);
        $('#Txt_descuento_Rez').attr('disabled', false);
        $('#Txt_descuento_Rec').attr('disabled', false);

        rezago = ($('#Txt_descuento_Rez').val() != '') ? parseFloat($('#Txt_descuento_Rez').val()) : 0;
        recargos = ($('#Txt_descuento_Rec').val() != '') ? parseFloat($('#Txt_descuento_Rec').val()) : 0;        
        rezago = Rezago_Hidden * $('#Txt_descuento_Rez').val() / 100;
        recargos = Recargo_Hidden * ($('#Txt_descuento_Rec').val() / 100);
        rezago = Math.round(rezago * 100) / 100;
        recargos = Math.round(recargos * 100) / 100;
        if ($('#Txt_descuento_Rez').val() < 101 && $('#Txt_descuento_Rec').val() < 101) {
            Total_Descuento = rezago + recargos;
            $('#Txt_Total_Descuento_Rez').val(rezago);
            $('#Txt_Total_Descuento_Rec').val(recargos);
        }
        else {
            if ($('#Txt_descuento_Rez').val() > 100) {
                $('#Txt_descuento_Rez').val('');
                $('#Txt_Total_Descuento_Rez').val('');
                Total_Descuento = recargos;
            }
            if ($('#Txt_descuento_Rec').val() > 100) {
                $('#Txt_descuento_Rec').val('');
                $('#Txt_Total_Descuento_Rec').val('');
                Total_Descuento = rezago;
            }
        }
    }
    if ($('input:radio[name=Tipo_Descuento]')[1].checked == true) {
        $('#Txt_Total_Descuento_Rez').attr('disabled', false);
        $('#Txt_Total_Descuento_Rec').attr('disabled', false);
        $('#Txt_descuento_Rez').attr('disabled', true);
        $('#Txt_descuento_Rec').attr('disabled', true);
        //
        rezago = ($('#Txt_Total_Descuento_Rez').val() != '') ? parseFloat($('#Txt_Total_Descuento_Rez').val()) : 0;
        recargos = ($('#Txt_Total_Descuento_Rec').val() != '') ? parseFloat($('#Txt_Total_Descuento_Rec').val()) : 0;
        if ($('#Txt_Total_Descuento_Rez').val() <= Rezago_Hidden) {
            Por_Rezago = parseFloat(rezago * 100 / Rezago_Hidden);
            Por_Rezago = Math.round(Por_Rezago * 100) / 100;
            $('#Txt_descuento_Rez').val(Por_Rezago);
        }        
        else {
                $('#Txt_Total_Descuento_Rez').val('');
                $('#Txt_descuento_Rez').val('');
                Total_Descuento = recargos;
        }
        if ($('#Txt_Total_Descuento_Rec').val() <= Recargo_Hidden) {
            Por_Recargo = parseFloat(recargos * 100 / Recargo_Hidden);
            Por_Recargo = Math.round(Por_Recargo * 100) / 100;
            $('#Txt_descuento_Rec').val(Por_Recargo);
        }
        else {
                $('#Txt_Total_Descuento_Rec').val('');
                $('#Txt_descuento_Rec').val('');
                Total_Descuento = rezago;
            }
        Total_Descuento = rezago + recargos;
        if (Total_Descuento > parseFloat($("[id$='_Txt_Saldo_Hidden']").val())) {
            Total_Descuento = 0;
            $('#Txt_Total_Descuento_Rez').val('');
            $('#Txt_Total_Descuento_Rec').val('');
            $('#Txt_descuento_Rez').val('');
            $('#Txt_descuento_Rec').val('');
        }        
    }


    Total_Descuento = Math.round(Total_Descuento * 100) / 100;
    $('#Tx_Descuento_Total').val(Total_Descuento);
    $("[id$='_Txt_Descuento_Total_Hidden']").val(Total_Descuento);
    Saldo_Final = parseFloat($("[id$='_Txt_Saldo_Hidden']").val()) - Total_Descuento;
    if ($('#Txt_Pago_Inicial').val() != '') {
        Pago_Inicial = parseFloat($('#Txt_Pago_Inicial').val());
        if (Pago_Inicial < Saldo_Final) {
            Saldo_Final -= Pago_Inicial;
            $("[id$='Txt_Pago_Inicial_Hidden']").val(Pago_Inicial);
        }
        else {
            $('#Txt_Pago_Inicial').val('');
        }
    }
    Saldo_Final = Math.round(Saldo_Final * 100) / 100;
    $("[id$='_Txt_descuento_Rez_Hidden']").val($('#Txt_descuento_Rez').val());
    $("[id$='_Txt_descuento_Rec_Hidden']").val($('#Txt_descuento_Rec').val());
    $("[id$='Txt_Total_Descuento_Rez_Hidden']").val($('#Txt_Total_Descuento_Rez').val());
    $("[id$='Txt_Total_Descuento_Rec_Hidden']").val($('#Txt_Total_Descuento_Rec').val());
    $('#Txt_Saldo_Datos_Convenio').val(Saldo_Final);
    $("[id$='txt_Saldo_Final_hidden']").val(Saldo_Final);

    if ($('#Txt_Plazo').val() != '') {
        Calcula_Abonos();
    }
    if (isNaN($('#Txt_descuento_Rez').val())) {
        $("[id$='_Txt_descuento_Rez_Hidden']").val('0');
        $('#Txt_descuento_Rez').val('0')
    }
    if (isNaN($('#Txt_descuento_Rec').val())) {
        $("[id$='_Txt_descuento_Rec_Hidden']").val('0');
        $('#Txt_descuento_Rec').val('0')
    }
}
///*******************************************************************************
///NOMBRE DE LA FUNCIÓN : Calcula_Descuento_Rezagos
///DESCRIPCIÓN          : calcula el descuento sobre los rezagos
///PARAMETROS           :   
///CREO                 : Julio Cruz
///FECHA_CREO           : 7/Noviembre/2011 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///*******************************************************************************
function Calcula_Descuento_Rezagos() {
    var parametros = "";
    var Descuento = 0;
    var Total_Descuento = 0;
    $("[id$='_Txt_descuento_Rez_Hidden']").val('');
    $("[id$='_Txt_descuento_Rez_Hidden']").val($('#Txt_descuento_Rez').val());
    parametros += "&No_Cuenta=" + $('#Txt_Cuenta_Usuario').val();
    $.ajax({
        type: "POST",
        url: "../../Controladores/Frm_Controlador_Convenios.aspx?Accion=Rezago" + parametros,
        contentType: "TEXT",
        async: false,
        cache: false,
        success: function(cadena) {
            Descuento = cadena * ($('#Txt_descuento_Rez').val() / 100);
        },
        error: function(result) {
            alert('ERROR ' + result.status + ' ' + result.statusText);
        }
    });
    return Descuento;
}

///*******************************************************************************
///NOMBRE DE LA FUNCIÓN : Calcula_Descuentos_Recargos
///DESCRIPCIÓN          : calcula el descuento sobre los recargos
///PARAMETROS           :   
///CREO                 : Julio Cruz
///FECHA_CREO           : 7/Noviembre/2011 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///*******************************************************************************
function Calcula_Descuentos_Recargos() {
    var parametros = "";
    var Descuento = 0;
    var Total_Descuento = 0;
    $("[id$='_Txt_descuento_Rec_Hidden']").val('');
    $("[id$='_Txt_descuento_Rec_Hidden']").val($('#Txt_descuento_Rec').val());
    parametros += "&No_Cuenta=" + $('#Txt_Cuenta_Usuario').val();
    $.ajax({
        type: "POST",
        url: "../../Controladores/Frm_Controlador_Convenios.aspx?Accion=Recargo" + parametros,
        contentType: "TEXT",
        async: false,
        cache: false,
        success: function(cadena) {
            Descuento = cadena * ($('#Txt_descuento_Rec').val() / 100);
        },
        error: function(result) {
            alert('ERROR ' + result.status + ' ' + result.statusText);
        }
    });
    return Descuento;
}

///*******************************************************************************
///NOMBRE DE LA FUNCIÓN : Validar_Convenios_Pendientes
///DESCRIPCIÓN          : Validar_Convenios_Pendientes
///PARAMETROS           :   
///CREO                 : Julio Cruz
///FECHA_CREO           : 8/Noviembre/2011 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///*******************************************************************************
function Validar_Convenios_Pendientes() {
    var parametros = "";
    parametros += "&Predio_ID=" + $("[id$='_Txt_Predio_ID_Hidden']").val();
    $.ajax({
        type: "POST",
        url: "../../Controladores/Frm_Controlador_Convenios.aspx?Accion=Consulta_Convenios_Pendientes" + parametros,
        contentType: "TEXT",
        async: false,
        cache: false,
        success: function(cadena) {
            if (cadena == 'PENDIENTE') {
                $('#Txt_Saldo').val('');
                $('#Txt_Saldo_Datos_Convenio').val('');
                $("[id$='_Txt_Saldo_Hidden']").val('');
                //Limpia las cajas de texto hidden
                $("[id$='_Txt_Abonos_Hidden']").val('');
                $("[id$='_Txt_Credencial_Hidden']").val('');
                $("[id$='_Txt_Cuenta_Usuario_Hidden']").val('');
                $("[id$='_Txt_descuento_Rec_Hidden']").val('');
                $("[id$='_Txt_descuento_Rez_Hidden']").val('');
                $("[id$='_Txt_Descuento_Total_Hidden']").val('');
                $("[id$='_Txt_Fecha_Inicio_Convenio_Hidden']").val('');
                $("[id$='_Txt_Id_Convenio_Hidden']").val('');
                $("[id$='_Txt_Nombre_Solicitante_Convenio_Hidden']").val('');
                $("[id$='_Txt_Observaciones_Hidden']").val('');
                $("[id$='_Txt_Plazo_Hidden']").val('');
                $("[id$='_Txt_Predio_ID_Hidden']").val('');
                $("[id$='_Txt_Saldo_Hidden']").val('');
                $("[id$='_Cmb_Estatus_Hidden']").val('');
                $("[id$='_Cmb_Periodo_Hidden']").val('');
                //Limpia las cajas de texto                
                $('#Txt_Cuenta_Usuario').val('');
                $('#Txt_Usuario').val('');
                $('#Txt_Nombre_Solicitante_Convenio').val('');
                $('#Txt_Apellido_Paterno').val('');
                $('#Txt_Apellido_Materno').val('');
                $('#Txt_Giro').val('');
                $("#Cmb_Periodo").val('');
                $("#Cmb_Estatus_Convenio_Alta").val('');
                $('#Tx_Descuento_Total').val('')
                $("#Txt_Credencial").val('');
                $("#Txt_Observacioness").val('');
                $("#Txt_Txt_Abonos").val('');
                $("#Txt_Txt_descuento_Rec").val('');
                $("#Txt_descuento_Rez").val('');
                $("[id$='txt_nota_cargo_id_hidden']").val('');
                $("[id$='txt_Rezagos_descuento_hidden']").val('');
                $("[id$='txt_Recargos_descuento_hidden']").val('');
                $("[id$='txt_Saldo_Final_hidden']").val('');
                $("[id$='txt_facturado_hidden']").val('');
                alert('Existe un convenio pendiente');
            }
            else {
                Consultar_Regiones_Pasos_Facturacion();
            }
        },
        error: function(result) {
            alert('ERROR ' + result.status + ' ' + result.statusText);
        }
    });
}
///*******************************************************************************
///NOMBRE DE LA FUNCIÓN : Consultar_Regiones_Pasos_Facturacion
///DESCRIPCIÓN          : Consultar_Regiones_Pasos_Facturacion
///PARAMETROS           :   
///CREO                 : Aramando Zavala Moreno
///FECHA_CREO           : 13/Noviembre/2011 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///*******************************************************************************
function Consultar_Regiones_Pasos_Facturacion() {
    var parametros = "";
    parametros += "&No_Cuenta=" + $("#Txt_Cuenta_Usuario").val();
    $.ajax({
        type: "POST",
        url: "../../Controladores/Frm_Controlador_Convenios.aspx?Accion=Consultar_Regiones_Pasos_Facturacion" + parametros,
        contentType: "TEXT",
        async: false,
        cache: false,
        success: function(cadena) {
        if (cadena == 'SI') {
                $('#Txt_Saldo').val('');
                $('#Txt_Saldo_Datos_Convenio').val('');
                $("[id$='_Txt_Saldo_Hidden']").val('');
                //Limpia las cajas de texto hidden
                $("[id$='_Txt_Abonos_Hidden']").val('');
                $("[id$='_Txt_Credencial_Hidden']").val('');
                $("[id$='_Txt_Cuenta_Usuario_Hidden']").val('');
                $("[id$='_Txt_descuento_Rec_Hidden']").val('');
                $("[id$='_Txt_descuento_Rez_Hidden']").val('');
                $("[id$='_Txt_Descuento_Total_Hidden']").val('');
                $("[id$='_Txt_Fecha_Inicio_Convenio_Hidden']").val('');
                $("[id$='_Txt_Id_Convenio_Hidden']").val('');
                $("[id$='_Txt_Nombre_Solicitante_Convenio_Hidden']").val('');
                $("[id$='_Txt_Observaciones_Hidden']").val('');
                $("[id$='_Txt_Plazo_Hidden']").val('');
                $("[id$='_Txt_Predio_ID_Hidden']").val('');
                $("[id$='_Txt_Saldo_Hidden']").val('');
                $("[id$='_Cmb_Estatus_Hidden']").val('');
                $("[id$='_Cmb_Periodo_Hidden']").val('');
                //Limpia las cajas de texto                
                $('#Txt_Cuenta_Usuario').val('');
                $('#Txt_Usuario').val('');
                $('#Txt_Nombre_Solicitante_Convenio').val('');
                $('#Txt_Apellido_Paterno').val('');
                $('#Txt_Apellido_Materno').val('');
                $('#Txt_Giro').val('');
                $("#Cmb_Periodo").val('');
                $("#Cmb_Estatus_Convenio_Alta").val('');
                $('#Tx_Descuento_Total').val('')
                $("#Txt_Credencial").val('');
                $("#Txt_Observacioness").val('');
                $("#Txt_Txt_Abonos").val('');
                $("#Txt_Txt_descuento_Rec").val('');
                $("#Txt_descuento_Rez").val('');
                $("[id$='txt_nota_cargo_id_hidden']").val('');
                $("[id$='txt_Rezagos_descuento_hidden']").val('');
                $("[id$='txt_Recargos_descuento_hidden']").val('');
                $("[id$='txt_Saldo_Final_hidden']").val('');
                $("[id$='txt_facturado_hidden']").val('');
                alert('La region de esta cuenta se encuentra en facturacion');
            }
            else {
                Consulta_Facturas_Detalles();
                Obtiene_Saldo();
            }
        },
        error: function(result) {
            alert('ERROR ' + result.status + ' ' + result.statusText);
        }
    });
}

function format(item) {
    return item.nombre;
}

function Filtrar_Predios() {
    var parametros = "";
    //Obtiene los filtros para mandarlos a consulta
    //Estatus
    parametros += "&Estatus=" + $('#Cmb_Busqueda_Estatus').val();
    //Zona
    parametros += "&Zona_ID=" + $("[id$='Txt_Busqueda_Zona_Hidden']").val();
    //Giro
    parametros += "&Giro_ID=" + $("[id$='Txt_Busqueda_Giro_Hidden']").val();
    //Colonia
    parametros += "&Colina_ID=" + $("[id$='Txt_Busqueda_Colonia_Hidden']").val();
    //Calle
    parametros += "&Calle_ID=" + $("[id$='Txt_Busqueda_Calle_Hidden']").val();
    //Tarifa
    parametros += "&Tarifa_ID=" + $("[id$='Txt_Busqueda_Tarifa_Hidden']").val();
    //Fraccionador
    parametros += "&Fraccionador_ID=" + $("[id$='Txt_Busqueda_Fraccionador_Hidden']").val();
    //Tipo Vivienda
    parametros += "&Tipo_Vivienda_ID=" + $("[id$='Txt_Busqueda_Tipo_Vivienda_Hidden']").val();
    //Pagina Actual
    //parametros += getPager;
    //No registros por pagina
    $('#Grid_Predios').datagrid({
        url: '../../Controladores/Frm_Controlador_Predios.aspx?Accion=Filtrar_Predios' + parametros,
        pageNumber: 1
    });
}

function AutocompletadoColonias() {

    $("[id$='Txt_Busqueda_Colonia_Hidden']").val('');
    //Autocompletado de las Colonias
    $('#Txt_Busqueda_Colonia').autocomplete("../../Controladores/Frm_Controlador_Predios.aspx", {
        extraParams: { Accion: 'autocompletar_colonia' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function(data) {
            return $.map(data, function(row) {
                return {
                    data: row,
                    value: row.colonia_id,
                    result: row.colonia
                }
            });
        },
        formatItem: function(item) {
            return item.colonia;
        }
    }).result(function(e, item) {
        var colonia_id = item.colonia_id;
        $("[id$='Txt_Busqueda_Colonia_Hidden']").val(colonia_id);
    });
}

function AutocompletadoCalles(colonia_id) {
    $("[id$='Txt_Busqueda_Calle_Hidden']").val('');
    //Obtiene el valor de la colonia

    //Autocompletado de las calles por colonia seleccionada
    $('#Txt_Busqueda_Calle').autocomplete(
        "../../Controladores/Frm_Controlador_Predios.aspx", {
            selectOnly: true,
            minChars: 3,
            extraParams: {
                Accion: 'autocompletar_calle', colonia_id: colonia_id
            },
            dataType: "json",
            parse: function(data) {
                return $.map(data, function(row) {
                    return {
                        data: row,
                        value: row.calle_id,
                        result: row.calle
                    }
                });
            },
            formatItem: function(item) {
                return item.calle; // format(item);
            }
        }).result(function(e, item) {
            var calle_id = item.calle_id;
            $("[id$='Txt_Busqueda_Calle_Hidden']").val(calle_id);
            //alert("Calle_ID_" + $('#Txt_Busqueda_Calle_Hidden').val());
        });
}

function AutocompletadoZonas() {
    //Autocompletado de las Zonas
    $('#Txt_Busqueda_Zona').autocomplete("../../Controladores/Frm_Controlador_Predios.aspx", {
        extraParams: { Accion: 'autocompletar_zona' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function(data) {
            return $.map(data, function(row) {
                return {
                    data: row,
                    value: row.zona_id,
                    result: row.zona
                }
            });
        },
        formatItem: function(item) {
            return item.zona;
        }
    }).result(function(e, item) {
        var zona_id = item.zona_id;
        $("[id$='Txt_Busqueda_Zona_Hidden']").val(zona_id);
    });
}

function AutocompletadoGiros() {
    //Autocompletado de los giros
    $('#Txt_Busqueda_Giro').autocomplete("../../Controladores/Frm_Controlador_Predios.aspx", {
        extraParams: { Accion: 'autocompletar_giros' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function(data) {
            return $.map(data, function(row) {
                return {
                    data: row,
                    value: row.giro_id,
                    result: row.giro
                }
            });
        },
        formatItem: function(item) {
            return item.giro;
        }
    }).result(function(e, item) {
        var giro_id = item.giro_id;
        $("[id$='Txt_Busqueda_Giro_Hidden']").val(giro_id);
    });
}

function AutocompletadoTarifas() {
    //Autocompletado de las tarifas
    $('#Txt_Busqueda_Tarifa').autocomplete("../../Controladores/Frm_Controlador_Predios.aspx", {
        extraParams: { Accion: 'autocompletar_tarifa' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function(data) {
            return $.map(data, function(row) {
                return {
                    data: row,
                    value: row.tarifa_id,
                    result: row.tarifa
                }
            });
        },
        formatItem: function(item) {
            return item.tarifa;
        }
    }).result(function(e, item) {
        var tarifa_id = item.tarifa_id;
        $("[id$='Txt_Busqueda_Tarifa_Hidden']").val(tarifa_id);
    });
}
function AutocompletadoFraccionador() {
    //Autocompletado de los fraccionadores
    $('#Txt_Busqueda_Fraccionador').autocomplete("../../Controladores/Frm_Controlador_Predios.aspx", {
        extraParams: { Accion: 'autocompletar_fraccionador' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function(data) {
            return $.map(data, function(row) {
                return {
                    data: row,
                    value: row.constructora_id,
                    result: row.constructora
                }
            });
        },
        formatItem: function(item) {
            return item.constructora;
        }
    }).result(function(e, item) {
        var constructora_id = item.constructora_id;
        $("[id$='Txt_Busqueda_Fraccionador_Hidden']").val(constructora_id);
    });
}

function AutocompletadoViviendas() {
    //Autocompletado de los tipos de vivienda
    $('#Txt_Busqueda_Tipo_Vivienda').autocomplete("../../Controladores/Frm_Controlador_Predios.aspx", {
        extraParams: { Accion: 'autocompletar_tipo_vivienda' },
        dataType: "json",
        selectOnly: true,
        minChars: 3,
        parse: function(data) {
            return $.map(data, function(row) {
                return {
                    data: row,
                    value: row.tipo_vivienda_id,
                    result: row.tipo
                }
            });
        },
        formatItem: function(item) {
            return item.tipo;
        }
    }).result(function(e, item) {
        var tipo_vivienda_id = item.tipo_vivienda_id;
        $("[id$='Txt_Busqueda_Tipo_Vivienda_Hidden']").val(tipo_vivienda_id);
    });


}

function CheckBox() {
    if ($('#Chk_Porcentaje').attr('checked') == true) {
        $('#Chk_Moneda').attr('checked', false);
        
    }
    if ($('#Chk_Moneda').attr('checked') == true) {
        $('#Chk_Porcentaje').attr('checked', false);
    }
}




