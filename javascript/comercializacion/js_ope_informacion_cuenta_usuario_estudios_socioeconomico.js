﻿
$(function(){

  $("#ventana_estudios_socioeconmicos").window('close');
  crearTablaSolicitudesSocioeconomicos();
});


//--------------------------------------------------------------------------------------------------------





//------------------------------------------------------------------------------------------------------


function crearTablaSolicitudesSocioeconomicos(){


  $('#tabla_solicitudes').datagrid({  // tabla inicio
            title: 'Solicitudes Socio Econ&oacute;micas ',
            width: 615,
            height: 250,
            columns: [[
        { field: 'folio', title: 'Folio', width: 100, sortable: true },
        { field: 'motivo', title: 'Motivo', width: 150, sortable: true },
        { field: 'solicitada_por', title: 'Solicitada Por', width: 150, sortable: true },
        { field: 'fecha_solicitud', title: 'F. Solicitud', width: 100, sortable: true },
        { field: 'elaboro', title: 'Elaboro', width: 150, sortable: true },
        { field: 'observaciones', title: 'Observaciones', width: 200, sortable: true }
    

    ]],     
             onClickRow: function(rowIndex, rowData) {
            
            }, 
            pageSize: 50,
            pagination: true,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            showFooter: false,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap: true

        });  // tabla final


}


//------------------------------------------------------------------------------------------------------
function agregarEstudios(){
var datos;

if(valoresRequeridosGeneral("ventana_estudios_socioeconmicos")==false){
    $.messager.alert('Japami','Se requieren algunos valores','error');
    return;  
   }
   

   
   
   datos= toJSON( $("#ventana_estudios_socioeconmicos :input").serializeArray());
   $('#ventana_mensaje').window('open');     
       
     $.ajax({ //inicio
             type: "POST",
             url: "../../Controladores/Frm_Controlador_Consulta_Usuario.aspx/guardarSolicitudesSocioeconomicas",
             data: "{'datos':'" + datos + "'}",
             contentType: "application/json; charset=utf-8",
             dataType: "json",
             success: function(response) {
                 $('#ventana_mensaje').window('close'); 
                 var respuesta = eval("(" + response.d + ")");
                 if (respuesta.mensaje == "bien") {                   
                     procesoExitoso();
                     
                 } else {
                    $('#ventana_mensaje').window('close');
                     $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                     
                 }

             },
             error: function(result) {
                 $('#ventana_mensaje').window('close');
                 alert("ERROR " + result.status + ' ' + result.statusText);

             }


         });  //fin peticion ajax



}


//------------------------------------------------------------------------------------------------------

function procesoExitoso(){
    $.messager.alert('Japami', 'Proceso Exitoso', 'info',function(){
       
       
        $("[id$='txt_solicitado_por_s']").val('');
        $("[id$='txt_observaciones_s']").val('');
          
       $('#tabla_solicitudes').datagrid('loadData',{ total:0, rows:[]});
       $('#tabla_solicitudes').datagrid({ url: '../../Controladores/Frm_Controlador_Consulta_Usuario.aspx', queryParams: { accion: 'obtenerSolicitudesSocioeconomicas', nocuenta:$("[id$='txt_numero_cuenta_s']").val()}, pageNumber: 1 });
                        
       
       
    });
}

//-------------------------------------------------------------------------------------------------------