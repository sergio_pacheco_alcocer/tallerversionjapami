﻿

$(function(){
   $('#ventana_inspecciones').window('close');
   crearTablaInspecciones();   
});

//------------------------------------------------------------------------------------------------------

function crearTablaInspecciones(){


  $('#tabla_inspecciones').datagrid({  // tabla inicio
            title: 'Inspecciones',
            width: 760,
            height: 250,
            columns: [[
        { field: 'folio', title: 'Folio', width: 100, sortable: true },
        { field: 'motivo', title: 'Motivo', width: 150, sortable: true },
        { field: 'solicitud', title: 'Solicitada Por', width: 150, sortable: true },
        { field: 'fecha_solicitud', title: 'F. Solicitud', width: 100, sortable: true },
        { field: 'usuario_creo', title: 'Elaboro', width: 150, sortable: true },
        { field: 'estatus', title: 'Estatus', width: 150, sortable: true },
        { field: 'observacion_inicial', title: 'Obser. Inicial', width: 200, sortable: true },
        { field: 'observacion_final', title: 'Obser. Final', width: 200, sortable: true },
        { field: 'motivo_cancelacion', title: 'M.Cancelacion', width: 200, sortable: true },
        { field: 'no_inspeccion', title: 'no_inspeccion', width: 0, sortable: true }
    

    ]],     
             onClickRow: function(rowIndex, rowData) {
            
            }, 
            pageSize: 50,
            pagination: true,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            showFooter: false,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap: false,
            toolbar:[{ 
            text:'Imprimir',
		    iconCls:'icon-print',
		    id:'btn_grid_imprimir',
		    handler:function(){
		              imprimirFormato();
					}   
				 }, '-', {
             text: 'Imprimir Soluci&oacute;n',
             iconCls: 'icon-print',
             id: 'btn_grid_imprimir_resultado',
             handler: function() {
                 imprimirFormatoComplento();
             }	
					
					
				}]
            

        });  // tabla final

   $('#tabla_inspecciones').datagrid('hideColumn','no_inspeccion');
}

//------------------------------------------------------------------------------------------------------
function agregarInspeccion(){
var datos;

  if(valoresRequeridosGeneral("ventana_inspecciones")==false){
    $.messager.alert('Japami','Se requieren algunos valores','error');
    return;  
   }
   
      
   datos= toJSON( $("#ventana_inspecciones :input").serializeArray());
   $('#ventana_mensaje').window('open');     
       
     $.ajax({ //inicio
             type: "POST",
             url: "../../Controladores/Frm_Controlador_Consulta_Usuario.aspx/guardarInspecciones",
             data: "{'datos':'" + datos + "'}",
             contentType: "application/json; charset=utf-8",
             dataType: "json",
             success: function(response) {
                 $('#ventana_mensaje').window('close'); 
                 var respuesta = eval("(" + response.d + ")");
                 if (respuesta.mensaje == "bien") {                   
                     procesoExitoso_inspeciones();
                     
                 } else {
                    $('#ventana_mensaje').window('close');
                     $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                     
                 }

             },
             error: function(result) {
                 $('#ventana_mensaje').window('close');
                 alert("ERROR " + result.status + ' ' + result.statusText);

             }


         });  //fin peticion ajax

}

//------------------------------------------------------------------------------------------------------

function procesoExitoso_inspeciones(){
    $.messager.alert('Japami', 'Proceso Exitoso', 'info',function(){
      
      //$("#ventana_inspecciones").window('close');
       $('#tabla_inspecciones').datagrid('loadData',{total:0, rows:[]});
       $('#tabla_inspecciones').datagrid({ url: '../../Controladores/Frm_Controlador_Consulta_Usuario.aspx', queryParams: { accion: 'obtenerInspeccionesUsuario', nocuenta:$("[id$='txt_numero_cuenta_i']").val()}, pageNumber: 1 });
       $("[id$='txt_folio_i']").val('');
       $("[id$='txt_solicitado_por_i']").val('');
       $("[id$='txt_observaciones_i']").val('');
        
       
    });
}


//------------------------------------------------------------------------------------------------------

function exportar_excel(){
var renglones;

renglones=$('#tabla_inspecciones').datagrid('getRows');
  if (renglones.length == 0) {
        $.messager.alert('Reportes', 'No hay Datos que mandar a excel');
        return;
    }
    
   window.location = "../../Reporte/Frm_controlador_excel.aspx?nombre=" + $("[id$='txt_nombre_i']").val() + "&nocuenta=" + $("[id$='txt_numero_cuenta_i']").val() + "&accion=reporte_inspecciones" ;



}


//------------------------------------------------------------------------------------------------------

function imprimirFormato(){

var renglon;

 renglon=  $('#tabla_inspecciones').datagrid('getSelected');

if(renglon==null){
  $.messager.alert('Japami','Tienes que selecciona un elemento');
  return;
}

 window.open("../../Reporte/frm_controlador_reporte_pdf.aspx?nombre_reporte=formato_inspeccion&no_folio=" + renglon.folio,"Inspeccion","width=1000px,height=1000px,scrollbars=YES,resizable=YES"); 

}

//-------------------------------------------
function imprimirFormatoComplento(){

var renglon;

 renglon=  $('#tabla_inspecciones').datagrid('getSelected');

if(renglon==null){
  $.messager.alert('Japami','Tienes que seleccionar un elemento');
  return;
}


if(renglon.estatus=='GENERADA'){
   $.messager.alert('Japami','El elemento seleccionado tiene un estatus de generado aun no tiene soluci&oacute;n');
   return;
}

 window.open("../../Reporte/frm_controlador_reporte_pdf.aspx?nombre_reporte=reporte_inspeccion_completo&no_folio=" + renglon.folio,"Inspeccion","width=1000px,height=1000px,scrollbars=YES,resizable=YES"); 

}