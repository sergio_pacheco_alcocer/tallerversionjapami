﻿$(function (){

  $('#ventana_corte').window('close');
 
  crearTablaUsuarioCortes();

});

//----------------------------------------------------------------------------------------

function crearTablaUsuarioCortes(){


  $('#tablas_cortes').datagrid({  // tabla inicio
            title: 'Cortes a Usuario ',
            width: 615,
            height: 250,
            columns: [[
        { field: 'folio', title: 'Folio', width: 100, sortable: true },
        { field: 'fecha_corte', title: 'F.Corte', width: 100, sortable: true },
        { field: 'adeudo', title: 'Adeudo', width: 150, sortable: true },
        { field: 'empleado_realizo', title: 'E. Realizo', width: 200, sortable: true },
        { field: 'estatus', title: 'Estatus', width: 150, sortable: true },
        { field: 'observaciones', title: 'Observaciones', width: 200, sortable: true },
        { field: 'aviso', title: 'Aviso', width: 150, sortable: true }
        
      
    

    ]],     
             onClickRow: function(rowIndex, rowData) {
            
            }, 
            pageSize: 50,
            pagination: true,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            showFooter: false,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap: false

        });  // tabla final


}