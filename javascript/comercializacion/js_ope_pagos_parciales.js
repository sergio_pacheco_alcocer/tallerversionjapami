﻿var objdatos_usuario;
var concepto_agua;
var concepto_drenaje;
var concepto_saniamiento;
var concepto_rezagos;
var concepto_recargos;
var concepto_otros_cargos;
var objRenglones_modelo;
var pagoCliente;
var aplicarPagoRezago;
var aplicarPagoRecargo;
var t1,t2;
var index;
var total;
var strDetallesConceptos;
var strTotales;
var porcentaje_minimo;
var objConceptos_id;
var no_factura_recibo_parcial="";


$(function(){

 crearTablas();   
 $('#ventana_mensaje').window('close');  
 //$('#btn_imprimir_recibo_parcial').
 
 porcentaje_minimo= parseFloat( $("[id$='txt_porcentaje_pago_parcial']").val());
 objConceptos_id=json_parse($("[id$='txt_conceptos_id_h']").val() ); 
 
  
  //eventos de los botones
 $("[id$='Btn_Buscar']").click(function(event) {
        obtenerInformacionUsuario();
        event.preventDefault();
    });
  
  
  $("[id$='Btn_Imprimir']").click(function(event) {
        imprimiFacturas();
        event.preventDefault();
    });
  
  
  //evento de la parcialidad que se va pagar
   $("[id$='Txt_Total_Pago']").blur(function(){
        pagoCliente= parseFloat( $(this).val());
        calcularPagoParcial();
   });
   

});

//-----------------------------------------------------------------------------------------

function imprimiFacturas(){
  var renglones;
  
  renglones=$('#tabla_factura').datagrid('getRows');
  
  if(renglones.length==0){
     $.messager.alert('Japamo','No hay recibo que mandar a imprimir','info');
    return;
  }



 window.open("../../Reporte/frm_controlador_reporte_pdf.aspx?nombre_reporte=formato_recibo_normal&no_factura_recibo="+renglones[0].no_factura_recibo,"Recibo","width=1000px,height=1000px,scrollbars=YES,resizable=YES");
 //window.open("Frm_Ope_Cor_Facturas_Recibos_Impresion.aspx?&Region_ID=&No_Factura_Recibo=" + renglones[0].no_factura_recibo + "&No_Cuenta=&Folio_Inicial=&Folio_Final=" ,"Impesion","width=100,height=100,left=0,top=0");


}

//-----------------------------------------------------------------------------------------

function crearTablas(){

  $('#tabla_factura').datagrid({
        title: 'Factura',
        width: 785,
        height: 300,
        columns: [[
	                     { field: 'no_factura_recibo', title: 'id', width: 1, sortable: true },
	                     { field: 'tasa_iva', title: 'tasa_iva', width: 1, sortable: true },
	                     { field: 'concepto_id', title: 'concepto_id', width: 1, sortable: true },
	                     { field: 'concepto', title: 'Concepto', width: 250, sortable: true },
	                     { field: 'total_saldo', title: 'Saldo debe', width: 200, sortable: true },
	                     { field: 'nuevo_saldo', title: 'Saldo Paga', width:200 , sortable: true }
	              ]],
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        showFooter: true,
        striped: true,
        loadMsg: 'Cargando Datos',
        nowrap: false,
        toolbar: [{
            id: 'btn_imprimir_recibo_parcial',
            disabled:true,
            text: 'Imprimir',
            iconCls: 'icon-print',
            handler: function() {
                imprimirReciboParcial();  
            }
           },'-',{
            id: 'btnguardar',
            text: 'Guardar',
            iconCls: 'icon-save',
            handler: function() {
              guadarDatos();
            }
        

}]


        });
        
        $('#tabla_factura').datagrid('hideColumn','no_factura_recibo');
        $('#tabla_factura').datagrid('hideColumn','concepto_id');
        $('#tabla_factura').datagrid('hideColumn','tasa_iva');


}


//----------------------------------------------------------------------------------------

function limpiarCajas(){
  
  limpiarCajasTexto();
  $('#tabla_factura').datagrid('loadData', { total: 0, rows: [] }); 
  $('#tabla_factura').datagrid('reloadFooter',{ rows: [] });
  
}

//----------------------------------------------------------------------------------------

function obtenerInformacionUsuario(){
var no_cuenta;
 
 limpiarCajas();
 no_cuenta= $("[id$='Txt_Busqueda']").val();
 
 if(no_cuenta=="<Ingrese Numero de Cuenta>"){
    $.messager.alert('Japami','Introduce un número de cuenta','error');
    return;
 }
 
 
 $('#ventana_mensaje').window('open');  

 $.ajax({ //inicio
             type: "POST",
             url: "../../Controladores/frm_controlador_pagos_parciales.aspx/obtener_datos_cuenta",
             data: "{'nocuenta':'" + no_cuenta + "'}",
             contentType: "application/json; charset=utf-8",
             dataType: "json",
             success: function(response) {
                 $('#ventana_mensaje').window('close'); 
                 var respuesta = eval("(" + response.d + ")");
                 if (respuesta.mensaje == "bien") {                   
                     procesoExistosoObtenerDatos(respuesta);
                     
                 } else {
                    $('#ventana_mensaje').window('close');
                     $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                     
                 }

             },
             error: function(result) {
                 $('#ventana_mensaje').window('close');
                 alert("ERROR " + result.status + ' ' + result.statusText);

             }


         });  //fin peticion ajax
 

}


function procesoExistosoObtenerDatos(datos){

 objdatos_usuario=json_parse(datos.dato1);
 $("[id$=' Txt_Convenio.Text']").val(datos.dato2);
 
 if(objdatos_usuario.length>0){
  
 $("[id$='Txt_Predio_ID']").val(objdatos_usuario[0].predio_id);
 $("[id$='Txt_No_Cuenta']").val(objdatos_usuario[0].no_cuenta);
 $("[id$='Txt_Tarifa']").val(objdatos_usuario[0].tarifa);
 $("[id$='Txt_Nombre_Usuario']").val(objdatos_usuario[0].nombre_usuario);
 $("[id$='Txt_Direccion']").val(objdatos_usuario[0].direccion);   
 $("[id$='Txt_Colonia']").val(objdatos_usuario[0].colonia); 
 buscarFacturistas(objdatos_usuario[0].no_cuenta);
 
 }else{
    $.messager.alert('Japami', 'No se encontro informaci&oacute;n de la cuenta', 'info');
 }
 
}

//-----------------------------------------------------------------------------------------
function buscarFacturistas(no_cuenta){

 $('#tabla_factura').datagrid('loadData', { total: 0, rows: [] }); 
 $('#tabla_factura').datagrid({ url: '../../Controladores/frm_controlador_pagos_parciales.aspx', queryParams: {
         accion: 'obtenerFactura',
         nocuenta:no_cuenta
     }, pageNumber: 1
     });

}

//--------------------------------------------------------------------------------------------
function obtenerSumaConcepto(codigo){
var renglones;


 var renglones;
  var renglon;
  var suma=0;
  
  renglones=$('#tabla_factura').datagrid('getRows');
  
  for(var i=0 ;i<renglones.length; i++){        
       renglon=renglones[i];
       if(renglon.concepto_id==codigo){
          suma= suma + parseFloat(renglon.total_saldo);
          }
  }
  
  return suma;
}

//-------------------------------------------------------------------------------------------

function obtenerIndex(datos,id_conceptos){
 var renglon;
 var index=0;
for(var i=0;i<datos.length;i++){
   renglon=datos[i];
     if(renglon.concepto_id==id_conceptos){
      index=i;
      return index;
    }
}

return index;

}


function pagoSuperaPorcentajeMinimo(total,pagoUsuario){
var calculo;


calculo= decimal( (total* porcentaje_minimo) /100,2);

  if(pagoUsuario<calculo){
        return false;
     }
  
  return true;
}

//-------------------------------------------------------------------------------------------

function calcularPagoParcial(){
  var renglones;
  var pies;
  var totales;
  
  renglones=$('#tabla_factura').datagrid('getRows');
 
    
  if (porcentaje_minimo==0){
     $.messager.alert('Japami', 'El parámetro de porcentaje minimo a cobrar esta en cero', 'error');
     return;
  }
  
  
  if(renglones.length==0){
      $.messager.alert('Japami', 'No se hay datos suficientes para esta operación', 'error');
      return ;
  }
  
  pies=$('#tabla_factura').datagrid('getFooterRows'); 
  totales=parseFloat(pies[0].total_saldo);
  
 
  
  if( isNaN(pagoCliente) || pagoCliente==0 ){
      $.messager.alert('Japami', 'El Pago es incorrecto', 'error');
      return ; 
  }
  
  if(pagoCliente>=totales){
     $.messager.alert('Japami', 'El pago no debe ser mayo a la deuda', 'error');
      return ;
  }
  
  
  if(pagoSuperaPorcentajeMinimo(totales,pagoCliente)==false){  
    $.messager.alert('Japami', 'El pago debe superar el porcentaje minimo del: ' + porcentaje_minimo + '%  del total de la deuda', 'error');
      return ;
  }
  
  ponerEnCeroGrid();
  
  concepto_agua=obtenerSumaConcepto(objConceptos_id.concepto_agua_id);
  concepto_drenaje=obtenerSumaConcepto(objConceptos_id.concepto_drenaje_id);
  concepto_saniamiento=obtenerSumaConcepto(objConceptos_id.concepto_saneamiento_id);
  concepto_rezagos=obtenerSumaConcepto(objConceptos_id.concepto_rezago_id);
  concepto_recargos=obtenerSumaConcepto(objConceptos_id.concepto_recargo_id);  
  concepto_otros_cargos=obtenerSumaConcepto(objConceptos_id.concepto_otro_cargo_id);  
  
  
 objdatos_usuario= $('#tabla_factura').datagrid('getData');
  
  if(concepto_rezagos>0 && concepto_recargos>0){
         t1=concepto_rezagos + concepto_recargos;
         // si el pago es menor que la suma de los recargos y rezagos
         if (pagoCliente<t1) {
             t2=concepto_recargos/t1;   
             aplicarPagoRecargo= decimal( pagoCliente * t2,2);
             aplicarPagoRezago= decimal( pagoCliente-aplicarPagoRecargo,2);
             index=obtenerIndex(objdatos_usuario.rows,objConceptos_id.concepto_rezago_id);
             objdatos_usuario.rows[index].nuevo_saldo=aplicarPagoRezago;
             index=obtenerIndex(objdatos_usuario.rows,objConceptos_id.concepto_recargo_id);
             objdatos_usuario.rows[index].nuevo_saldo=aplicarPagoRecargo; 
             total=decimal( obtenerTotal(objdatos_usuario.rows),2);
             objdatos_usuario.footer[0].nuevo_saldo=total;             
             $('#tabla_factura').datagrid('loadData',objdatos_usuario);
         }else{
             index=obtenerIndex(objdatos_usuario.rows,objConceptos_id.concepto_rezago_id);
             objdatos_usuario.rows[index].nuevo_saldo=objdatos_usuario.rows[index].total_saldo;
             pagoCliente=decimal( pagoCliente-objdatos_usuario.rows[index].total_saldo,2);
             index=obtenerIndex(objdatos_usuario.rows,objConceptos_id.concepto_recargo_id);
             objdatos_usuario.rows[index].nuevo_saldo=objdatos_usuario.rows[index].total_saldo;
             pagoCliente=decimal( pagoCliente-objdatos_usuario.rows[index].total_saldo,2); 
             calcularSaldoSintomarEncuentaRezagosyRecargos(pagoCliente,objdatos_usuario);
        
         }//fin pago a cliente t1
     
  }// fin de los conceptos_rezagos and conceptos_recargos
  else{
        calcularSaldoSintomarEncuentaRezagosyRecargos(pagoCliente,objdatos_usuario);
    }

}// fin de la funcion

//--------------------------------------------------------------------------------------------

function calcularSaldoSintomarEncuentaRezagosyRecargos(pagoCliente,objdatos_usuario){
var saldo_concepto;

 if(pagoCliente>concepto_agua){
      index=obtenerIndex(objdatos_usuario.rows,objConceptos_id.concepto_agua_id);
      objdatos_usuario.rows[index].nuevo_saldo=objdatos_usuario.rows[index].total_saldo;
      pagoCliente=decimal( pagoCliente-objdatos_usuario.rows[index].total_saldo,2);
      
 }else
  {
     index=obtenerIndex(objdatos_usuario.rows,objConceptos_id.concepto_agua_id);
      objdatos_usuario.rows[index].nuevo_saldo=pagoCliente;
      pagoCliente=0;
      total= decimal( obtenerTotal(objdatos_usuario.rows),2);
      objdatos_usuario.footer[0].nuevo_saldo=total;
      $('#tabla_factura').datagrid('loadData',objdatos_usuario);
      return;
   }
   
   
  if(pagoCliente>concepto_drenaje){
      index=obtenerIndex(objdatos_usuario.rows,objConceptos_id.concepto_drenaje_id);
      objdatos_usuario.rows[index].nuevo_saldo=objdatos_usuario.rows[index].total_saldo;
      pagoCliente=decimal( pagoCliente-objdatos_usuario.rows[index].total_saldo,2);
      
 }else
  {
     index=obtenerIndex(objdatos_usuario.rows,objConceptos_id.concepto_drenaje_id);
      objdatos_usuario.rows[index].nuevo_saldo=pagoCliente;
      pagoCliente=0;
      total=decimal( obtenerTotal(objdatos_usuario.rows),2);
      objdatos_usuario.footer[0].nuevo_saldo=total;
      $('#tabla_factura').datagrid('loadData',objdatos_usuario);
      return;
   }
   
   
   
   
  if(pagoCliente>concepto_saniamiento){
      index=obtenerIndex(objdatos_usuario.rows,objConceptos_id.concepto_saneamiento_id);
      objdatos_usuario.rows[index].nuevo_saldo=objdatos_usuario.rows[index].total_saldo;
      pagoCliente=decimal( pagoCliente-objdatos_usuario.rows[index].total_saldo,2);
      
 }else
  {
     index=obtenerIndex(objdatos_usuario.rows,objConceptos_id.concepto_saneamiento_id);
      objdatos_usuario.rows[index].nuevo_saldo=pagoCliente;
      pagoCliente=0;
      total=decimal( obtenerTotal(objdatos_usuario.rows),2);
      objdatos_usuario.footer[0].nuevo_saldo=total;
      $('#tabla_factura').datagrid('loadData',objdatos_usuario);
      return;
   }
   
   
 if(pagoCliente>concepto_otros_cargos){
      index=obtenerIndex(objdatos_usuario.rows,objConceptos_id.concepto_otro_cargo_id);
      objdatos_usuario.rows[index].nuevo_saldo=objdatos_usuario.rows[index].total_saldo;
      pagoCliente=decimal( pagoCliente-objdatos_usuario.rows[index].total_saldo,2);
      
 }else
  {
     index=obtenerIndex(objdatos_usuario.rows,objConceptos_id.concepto_otro_cargo_id);
      objdatos_usuario.rows[index].nuevo_saldo=pagoCliente;
      pagoCliente=0;
      total=decimal( obtenerTotal(objdatos_usuario.rows),2);
      objdatos_usuario.footer[0].nuevo_saldo=total;
      $('#tabla_factura').datagrid('loadData',objdatos_usuario);
      return;
   }
   
   

}// fin de la funcion

//--------------------------------------------------------------------------------------------

function guadarDatos(){
var no_cuenta;
var t; 
var renglones;


renglones=$('#tabla_factura').datagrid('getRows');
if(renglones.length==0){
      $.messager.alert('Japami', 'No hay pagos asignados', 'error');
      return ;
}

strDetallesConceptos=toJSON( $('#tabla_factura').datagrid('getRows'));
strTotales=  $('#tabla_factura').datagrid('getFooterRows');
no_cuenta= $("[id$='Txt_No_Cuenta']").val();

t= parseFloat( strTotales[0].nuevo_saldo);

if( isNaN(t) || t==0){
   $.messager.alert('Japami', 'No hay pagos asignados', 'error');
      return ;
}

no_factura_recibo_parcial="";
strTotales=toJSON($('#tabla_factura').datagrid('getFooterRows'));

$('#ventana_mensaje').window('open');  

 $.ajax({ //inicio
             type: "POST",
             url: "../../Controladores/frm_controlador_pagos_parciales.aspx/guadarFacturaParcial",
             data: "{'nocuenta':'" + no_cuenta + "','detalles_pagos':'" + strDetallesConceptos + "','totales':'" + strTotales + "'}",
             contentType: "application/json; charset=utf-8",
             dataType: "json",
             success: function(response) {
                 $('#ventana_mensaje').window('close'); 
                 var respuesta = eval("(" + response.d + ")");
                 if (respuesta.mensaje == "bien") {                   
                     procesoExitosoGuardar(respuesta)
                     
                 } else {
                    $('#ventana_mensaje').window('close');
                     $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                     
                 }

             },
             error: function(result) {
                 $('#ventana_mensaje').window('close');
                 alert("ERROR " + result.status + ' ' + result.statusText);

             }


         });  //fin peticion ajax
 

}


function procesoExitosoGuardar(respuesta){
  no_factura_recibo_parcial=respuesta.dato1;

  $.messager.alert('Japami', 'Proceso Terminado', 'info',function(){   
      window.open("../../Reporte/frm_controlador_reporte_pdf.aspx?nombre_reporte=formato_recibo_parcial&no_factura_recibo_parcial=" + no_factura_recibo_parcial,"ReciboParcial","width=1000px,height=1000px,scrollbars=YES,resizable=YES");
      limpiarCajas(); 
      $("[id$='Txt_Total_Pago']").val('');
      $('#btn_imprimir_recibo_parcial').linkbutton('enable');
      
  });

}



//--------------------------------------------------------------------------------------------
function obtenerTotal(renglones){
  var renglones;
  var renglon;
  var suma=0;
    
  for(var i=0 ;i<renglones.length; i++){        
       renglon=renglones[i];
       suma= suma + parseFloat(renglon.nuevo_saldo);
          
  }
  
  return suma;

}

//--------------------------------------------------------------------------------------------
Number.prototype.decimal = function(num) {
    pot = Math.pow(10, num);
    return parseInt(this * pot) / pot;
}

function decimal(valor,no_decimales) {
    n = eval(valor);
    return n.decimal(no_decimales);
} 
//-----------------------------------------------------------------------------------------

function limpiarCajasTexto(){

 $("[id$='Txt_Predio_ID']").val('');
 $("[id$='Txt_No_Cuenta']").val('');
 $("[id$='Txt_Tarifa']").val('');
 $("[id$='Txt_Nombre_Usuario']").val('');
 $("[id$='Txt_Direccion']").val('');   
 $("[id$='Txt_Colonia']").val(''); 

}

//-------------------------------------------------------------------------------------------

function ponerEnCeroGrid(){
 var datos;
 
datos=$('#tabla_factura').datagrid('getData');

for(var i=0;i<datos.rows.length;i++){
   datos.rows[i].nuevo_saldo=0;
}

datos.footer[0].nuevo_saldo=0;
$('#tabla_factura').datagrid('loadData',datos);

}

//-------------------------------------------------------------------------------------------

function imprimirReciboParcial(){
 
 no_factura_recibo_parcial= $.trim( no_factura_recibo_parcial);

 if (no_factura_recibo_parcial.length>0){
   window.open("../../Reporte/frm_controlador_reporte_pdf.aspx?nombre_reporte=formato_recibo_parcial&no_factura_recibo_parcial=" + no_factura_recibo_parcial,"ReciboParcial","width=1000px,height=1000px,scrollbars=YES,resizable=YES");
   }  
}