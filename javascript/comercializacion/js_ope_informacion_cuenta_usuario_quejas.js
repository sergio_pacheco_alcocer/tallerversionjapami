﻿$(function(){
    
    //---------------------------------------
    
    $("#ventana_quejas").window('close');
    
    //creamos tabla de quejas de usuario
    crearTablaUsuarioQuejas();

});

//---------------------------------------------------------------------------------------------------------

function crearTablaUsuarioQuejas(){


  $('#tablas_quejas').datagrid({  // tabla inicio
            title: 'Quejas Usuarios ',
            width: 760,
            height: 250,
            columns: [[
        { field: 'folio', title: 'F.Evento', width: 100, sortable: true },
        { field: 'Fallo', title: 'Descripcion', width: 200, sortable: true },
        { field: 'brigada', title: 'Brigada', width: 150, sortable: true },
        { field: 'distrito', title: 'Distrito', width: 150, sortable: true },
        { field: 'reporto', title: 'Reporto', width: 150, sortable: true },
        { field: 'comentario', title: 'Comentario', width: 200, sortable: true },
        { field: 'observaciones_trabajo_realizado', title: 'Observ.Trabajo', width: 200, sortable: true },
        { field: 'estado', title: 'Estado', width: 150, sortable: true },
        { field: 'fecha_creacion', title: 'F. Elaboro', width: 150, sortable: true }
      
    

    ]],     
             onClickRow: function(rowIndex, rowData) {
            
            }, 
            pageSize: 50,
            pagination: true,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            showFooter: false,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap: false

        });  // tabla final


}