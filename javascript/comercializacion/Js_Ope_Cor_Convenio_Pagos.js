﻿
//---------------------------------------------------------------------------------------------------------
$(function () {
    crearTabla();
});

//--------------------Crea Tabla de Convenios--------------------------------------------------------------
function crearTabla() {

    $('#Grid_Convenios').datagrid({  // tabla inicio
        title: 'Convenios',
        width: 826,
        height: 300,
        columns: [[
        { field: 'Usuario', title: 'Usuario', width: 1, hidden: true },
        { field: 'No_Cuenta', title: 'No.Cuenta', width: 100, sortable: true },
        { field: 'No_Convenio', title: 'No.Convenio', width: 250, sortable: true },
        { field: 'Adeudo', title: 'Adeudo', width: 80, sortable: true },
        { field: 'Saldo', title: 'Saldo', width: 200, sortable: true },
        { field: 'Mensualidades', title: 'Mensualidades', width: 1, hidden: true },
        { field: 'Tasa_IVA', title: 'Tasa_IVA', width: 1, hidden: true },        
        { field: 'Pago_Inicial', title: 'Pago_Inicial', width: 1, hidden: true } 
    ]],
        onClickRow: function (rowIndex, rowData) {

        },
        pageSize: 50,
        pagination: true,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        showFooter: false,
        striped: true,
        fit: false,
        loadMsg: 'cargando...',
        nowrap: false

    });  // tabla final

}

//---------------------------Consulta Convenios------------------------------------------------------------------
function BuscarConvenio() {
    var datos_busqueda
    var datos = {};

    if (validarCheckBoxCajasTextos("criterios_busqueda", datos)) {
        return false;
    }
    datos_busqueda = JSON.stringify(datos, null, 2);    

    $('#Grid_Convenios').datagrid('loadData', { total: 0, rows: [] });
    $('#Grid_Convenios').datagrid({ url: '../../Controladores/Frm_Controlador_Convenio_Pagos.aspx', queryParams: { accion: 'Obtener_Convenio', datos: datos_busqueda }, pageNumber: 1 });
}

//--------------------------Valida Filtros de Busqueda de convenios--------------------------------------------------
// validarCheckBoxCajasTextos con caja de texo
function validarCheckBoxCajasTextos(panel, arreglo_datos) {

    var controles_check, control_check, i;
    var nombre_control_aux, nombre_control_aux1, nombre_control, aviso, respuesta, valor_campo;
    var valor_asignar;
    controles_check = $('#' + panel + ' :checkbox');


    respuesta = false;
    arrDatos = {};
    for (i = 0; i < controles_check.length; i++) {//inicio

        control_check = controles_check[i];

        nombre_control_aux = $(control_check).attr('id');
        pos = nombre_control_aux.indexOf("chk");
        nombre_control_aux1 = nombre_control_aux.substring(pos, nombre_control_aux.length);

        // nombre y propiedades de los combos
        pos = nombre_control_aux1.indexOf("_");
        nombre_control = "txt_" + nombre_control_aux1.substring(pos + 1, nombre_control_aux1.length);

        texto_control = $("[id$='" + nombre_control + "']");
        aviso = texto_control.attr('aviso');
        valor_campo = $.trim(texto_control.val());

        if ($(control_check).attr('checked')) {
            valor_asignar = valor_campo
        } else
        { valor_asignar = ''; }

        if (arreglo_datos[nombre_control] !== undefined) {
            if (!arreglo_datos[nombre_control].push) {
                arreglo_datos[nombre_control] = [arreglo_datos[nombre_control]];
            }
            arreglo_datos[nombre_control].push(valor_asignar || '');
        } else {
            arreglo_datos[nombre_control] = valor_asignar || '';
        }


        if ($(control_check).attr('checked')) {
            if (valor_campo.length == 0) {
                $.messager.alert('Japami', aviso);
                return true;
            }
        }


    } //fin

    return respuesta;

}

//------------------------------------------------------------------------------------

function abrirPopupPagos() {
    var renglon;

    $('#txt_nombre_convenio').val('');
    $('#txt_Saldo').val('');
    $('#txt_abonos').val('');
    $('#txt_observaciones_convenio').val('');
    $("[id$='Hdd_No_Convenio']").val('');
    $("[id$='Hdd_Tasa_IVA']").val('');
    $("[id$='Hdd_No_Cuenta']").val(''); 

    renglon = $('#Grid_Convenios').datagrid('getSelected');


    if (renglon == null) {
        $.messager.alert('Japami', 'Debes seleccionar un Convenio');
        return;
    }

    $('#Ventana_Pagos').window('open');

    //Asigna los valores
    $('#txt_nombre_convenio').val(renglon.Usuario);
    $('#txt_Saldo').val(renglon.Saldo);
    var numero = new oNumero((renglon.Adeudo - renglon.Pago_Inicial ) / renglon.Mensualidades)
    $('#txt_abonos').val(numero.formato(4, true));
    $("[id$='Hdd_No_Convenio']").val(renglon.No_Convenio);
    $("[id$='Hdd_Tasa_IVA']").val(renglon.Tasa_IVA);
    $("[id$='Hdd_No_Cuenta']").val(renglon.No_Cuenta);  
}
//------------------------------------------------------------------------------------
function cerrarVentanaPagos() {
    $('#Ventana_Pagos').window('close');
}
//------------------------------------------------------------------------------------
function AltaPago() {

   var datos_busqueda
   var datos ;

    datos = "{\"Comentarios\":\"" + $('#txt_observaciones_convenio').val() + "\"," +
    "\"abonos\":\"" + $('#txt_abonos').val() + "\"," +
    "\"Tasa_IVA\":\"" + $("[id$='Hdd_Tasa_IVA']").val() + "\"," +
    "\"no_Cuenta\":\"" + $("[id$='Hdd_No_Cuenta']").val() + "\"," +
    "\"no_Convenio\":\"" + $("[id$='Hdd_No_Convenio']").val() + "\"}";    
    datos_busqueda = datos
    //envia la información al servidor
    $.ajax({
        type: 'POST',
        url: "../../Controladores/Frm_Controlador_Convenio_Pagos.aspx/AltaPago?datos=" + datos_busqueda,
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (cadena) {
            var respuesta = eval("(" + cadena.d + ")");
            if (respuesta.mensaje == "Bien") {
                $.messager.alert('Japami', 'Proceso terminado');
                $('#Ventana_Pagos').window('close');
                //Genera el recibo                                                           
                window.open("../../Reporte/frm_controlador_reporte_pdf.aspx?nombre_reporte=Formato_Convenio&noPagoConvenio=" + respuesta.dato1 , "CertificarRecibo", "width=1000px,height=1000px,scrollbars=YES,resizable=YES");
            }
            else {
                $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                $('#Ventana_Pagos').window('close');
            }
        },
        error: function (result) {
            alert('ERROR ' + result.status + ' ' + result.statusText);
        }

    });
 }
//-------------------------------------------------------------------------------------

//Objeto oNumero
function oNumero(numero) {
    //Propiedades
    this.valor = numero || 0
    this.dec = -1;
    //Métodos
    this.formato = numFormat;
    this.ponValor = ponValor;
    //Definición de los métodos
    function ponValor(cad) {
        if (cad == '-' || cad == '+') return
        if (cad.length == 0) return
        if (cad.indexOf('.') >= 0)
            this.valor = parseFloat(cad);
        else
            this.valor = parseInt(cad);
    }
    function numFormat(dec, miles) {
        var num = this.valor, signo = 3, expr;
        var cad = "" + this.valor;
        var ceros = "", pos, pdec, i;
        for (i = 0; i < dec; i++)
            ceros += '0';
        pos = cad.indexOf('.')
        if (pos < 0)
            cad = cad + "." + ceros;
        else {
            pdec = cad.length - pos - 1;
            if (pdec <= dec) {
                for (i = 0; i < (dec - pdec); i++)
                    cad += '0';
            }
            else {
                num = num * Math.pow(10, dec);
                num = Math.round(num);
                num = num / Math.pow(10, dec);
                cad = new String(num);
            }
        }
        pos = cad.indexOf('.')
        if (pos < 0) pos = cad.lentgh
        if (cad.substr(0, 1) == '-' || cad.substr(0, 1) == '+')
            signo = 4;
        if (miles && pos > signo)
            do {
                expr = /([+-]?\d)(\d{3}[\.\,]\d*)/
                cad.match(expr)
                cad = cad.replace(expr, RegExp.$1 + ',' + RegExp.$2)
            }
            while (cad.indexOf(',') > signo)
            if (dec < 0) cad = cad.replace(/\./, '')
            return cad;
        }
    } //Fin del objeto oNumero:


    function validarSiNumero(numero) {
        if (!/^([0-9]).*$/.test(numero)) {
            alert("El valor " + numero + " no es un número");
            $('#txt_abonos').val('');
        } else {
            var numero = new oNumero($('#txt_abonos').val())
            $('#txt_abonos').val(numero.formato(4, true));
        }        
    }