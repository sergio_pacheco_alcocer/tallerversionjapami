﻿/*################################################################################################################*/
/*Carga Script*/
$(document).ready(function () {
    $('#Txt_Operacion_Caja_ID_Valor').val($("[id$='Txt_Operacion_Caja_ID']").val());
    $('#Txt_Empleado_Valor').val($("[id$='Txt_Empleado']").val());
    
    //ocultamos estos descuentos 
    ocultarDescuentos();

    //EStado Página
    estadoPagina();

    //Codigo Barras
    operacionCodigoBarras();

    //Evento Campo Efectivo,Tarjetas y Cheques
    eventoEfectivo();
    eventoTarjeta();
    eventoCheque();

    //Validar Vacios

    //Formato Campos
    formatoCampo();

    //Modal
    //modalRetiro();
    modalDomiciliacion();
    //modalCheque();
    //modalTarjetas();

    //Operaciones
    operacionesDomiciliacion();
    //operacionesCheque();
    //operacionesTarjetas();

    //Progreso Peticiones
    //progress();
});

/*################################################################################################################*/
/*################################################################################################################*/
/*Funciones Generales*/

function modal() 
{
    $('.tabs-header').removeAttr('style');
    $('.tabs-header').attr('style', 'width:100%;');
    $('.tabs-panels').removeAttr('style');
    $('.tabs-panels').attr('style', 'width:100%; height:400px;');
    $('.tabs-container').removeAttr('style');
    $('.tabs-container').attr('style', 'width:100%; height:400px;');
    $('.tabs-wrap').removeAttr('style');
    $('.tabs-wrap').attr('style', 'margin-left:0px; left:0px; width:100%;');
    $('.jqmWindow').removeAttr('style');
    $('.jqmWindow').attr('style', 'left:50%; top:50%; width:800px; margin-top:-157.5px; margin-left:-400px;');
}

function formatoCampo() 
{
    //Formatos de los campos
    $('.nombre').format({ type: "alphabet", autofix: true });
    $('.numero').format({ precision: 0, autofix: true });
    $('.decimal').format({ precision: 2, autofix: true });
    $('.telefono').format({ type: "phone-number", autofix: true });
    $('.correo').format({ type: "email" });
    $('.comentario').format({ type: "alphabet-coment", autofix: true });
}

function estadoPagina()
{
    $('.campo_cobro').val('');
    $('.campo_domiciliacion').val('');
    $('.campo_cheque').val('');
    $('.campo_tarjeta').val('');
    //$('.disabled').removeAttr('disabled');
    $('.disabled').attr('style', 'text-align:right; border-style:groove ; background-color:#DDDDDD; width:90%;');
    $('.enabled').attr('style', 'width:90%;text-align:right;border-style:inset;border-color:#0B6DDB;');

    operacionesPagina();
}

function operacionesPagina() 
{
    $('#Btn_Nuevo').click(function () {
        var estatus = $('#Btn_Nuevo').attr('alt');

        if (estatus == "EsperaGuardar") {
            alert('No has insertado un recibo que pagar. Por favor inserte uno.');
        }
        else
            if (estatus == "Guardar") {

                if ($('#Txt_Saldo').val() == 0) {
                    //Operaciones para guardar
                    var campos = $('.campo_cobro');
                    var campos_cobro = campos.serialize();
                    var Recibos_Cobrar = $('.cod');
                    var Codigo_Barras = arreglo_Codigos;
                    var Ajuste = arreglo_Ajuste;
                    var No_Cuentas = arreglo_Cuenta;
                    var campos_tarjeta = $('.campo_tarjeta');
                    var campos_tarjeta_serializados = campos_tarjeta.serialize();
                    var campos_cheque = $('.campo_cheque');
                    var campos_cheque_serializados = campos_cheque.serialize();
                    var fecha = new Date();
                    var tabla_campos_tarjeta;
                    var tabla_campos_cheques;
                    var codigo_barras_bonificacion = $.trim($('.codigo_bonificacion').text());


                    if (codigo_barras_bonificacion == "undefined") {
                        codigo_barras_bonificacion = "";
                    }

                    var codigo_barras_parciales = $.trim($('.codigo_parciales').text());
                    if (codigo_barras_parciales == "undefined") {
                        codigo_barras_parciales = "";
                    }

                    // obtenemos los datos de las tablas de tarjeta y cheques
                    tabla_campos_tarjeta = toJSON($('#tabla_tarjetas').datagrid('getRows'));
                    tabla_campos_cheques = toJSON($('#tabla_cheque').datagrid('getRows'));

                    fecha = (fecha.getDate() < 10 ? '0' + fecha.getDate() : fecha.getDate()) + '/' +
                    ((fecha.getMonth() + 1) < 10 ? '0' + (fecha.getMonth() + 1) : (fecha.getMonth() + 1)) + '/' +
                    fecha.getFullYear() + ' ' +
                    (fecha.getHours() < 10 ? '0' + fecha.getHours() : fecha.getHours()) + ':' +
                    (fecha.getMinutes() < 10 ? '0' + fecha.getMinutes() : fecha.getMinutes()) + ':' +
                    (fecha.getSeconds() < 10 ? '0' + fecha.getSeconds() : fecha.getSeconds());

                    //recorremos el serializarray para formar la cadena de los controles encontrados
                    var enviar = "Operacion_Caja_ID=" + $('#Txt_Operacion_Caja_ID_Valor').val() + "&" +
                        campos_cobro + "&" + campos_tarjeta_serializados + "&" + campos_cheque_serializados +
                        "&Usuario=" + $('#Txt_Empleado_Valor').val() +
                        "&Recibos_Cobrar=" + Recibos_Cobrar.length + "&Total_Recibos=" + $('#Txt_Total').val() +
                        "&Codigo_Barras=" + Codigo_Barras + "&Ajuste=" + Ajuste + "&No_Cuentas=" + No_Cuentas + "&Fecha=" + fecha + "&codigo_barras_bonificacion=" + codigo_barras_bonificacion + "&codigo_barras_parciales=" + codigo_barras_parciales;


                    $('#ventana_mensaje').window('open');
                    //redireccionamos a la pagina y pasamos como parametro la cadena, para controlar la informacion 
                    $.ajax({ url: "../../Controladores/Frm_Controlador_Cobro_Recibos.aspx?accion=Cobro&" + enviar + "&tarjetas_tabla=" + tabla_campos_tarjeta + "&cheques_tabla=" + tabla_campos_cheques,
                        type: 'POST',
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        dataType: "html",
                        success: function (data) {
                            $('#ventana_mensaje').window('close');
                            $('#Lbl_Mensaje').text($('#Lbl_Mensaje').html(data));
                            $('#Lbl_Mensaje').css({ 'font-family': '"Verdana", Times, serif', 'font-size': '1em' });
                            //arreglo_Codigos = null;
                            //arreglo_Ajuste = null;
                            //arreglo_Cuenta = null; 
                            arreglo_Codigos = new Array();
                            arreglo_Cuenta = new Array();
                            arreglo_Ajuste = new Array();
                            contCodigo = 0;
                            contCuenta = 0;
                            contAjuste = 0;
                            $('#Txt_Efectivo').val("0");
                            $('#Txt_Tarjetas').val("0")
                            $('#Txt_Cheques').val("0");
                            $('#Txt_Total').val("0");
                            $('#Txt_Saldo').val("0");
                            $('#txt_descuentos').val("0")
                            limpiarTablasTarjetasCheques();
                            window.open("../../Reporte/frm_controlador_reporte_pdf.aspx?nombre_reporte=reporteCertificarRecibo&operacion_caja_id=" + $('#Txt_Operacion_Caja_ID_Valor').val(), "CertificarRecibo", "width=1000px,height=1000px,scrollbars=YES,resizable=YES");

                        },
                        error: function (result) {
                            $('#ventana_mensaje').window('close');
                            alert("ERROR " + result.status + ' ' + result.statusText);

                        }

                    });

                    $('#Btn_Nuevo').attr({ 'src': '../imagenes/paginas/icono_guardar.png', 'alt': 'EsperaGuardar', 'title': 'Pagar' });
                    $('#Btn_Salir').attr({ 'src': '../imagenes/paginas/icono_cancelar.png', 'alt': 'Cancelar', 'title': 'Cancelar' });
                    $('.cod').remove(); //Remover todos los recibos de la tabla de recibos
                    // $('.campo_cobro').val('');
                    //$('.campo_tarjeta').val('');
                    //$('.campo_cheque').val('');
                    $('#Txt_Efectivo').val("0");
                    $('#Txt_Tarjetas').val("0")
                    $('#Txt_Cheques').val("0");
                    $('#Txt_Total').val("0");
                    $('#Txt_Saldo').val("0");
                    $('#txt_descuentos').val("0");
                    ocultarDescuentos();
                } else { alert('El saldo del cobro debe ser cero.') }
            }
    });

    $('#Btn_Salir').click(function () {
        var estatus = $('#Btn_Salir').attr('alt');

        if (estatus == "Cancelar") {
            $('#Btn_Nuevo').attr({ 'src': '../imagenes/paginas/icono_guardar.png', 'alt': 'EsperaGuardar', 'title': 'Pagar' });
            $('#Btn_Salir').attr({ 'src': '../imagenes/paginas/icono_cancelar.png', 'alt': 'Cancelar', 'title': 'Cancelar' });
            //$('.campo_cobro').val('');
            //$('.campo_tarjeta').val('');
            //$('.campo_cheque').val('');
            $('#Txt_Efectivo').val("0");
            $('#Txt_Tarjetas').val("0")
            $('#Txt_Cheques').val("0"); 
            $('#Txt_Total').val("0");
            $('#Txt_Saldo').val("0");
            $('#txt_descuentos').val('');
            ocultarDescuentos();
            contCodigo=0;
            contCuenta=0;
            contAjuste=0;
            $('.cod').remove(); //Remover todos los recibos de la tabla de recibos
            $('#Lbl_Mensaje').text('Bienvenido!');
            $('#Lbl_bonificaciones').text('');
                       
            
             if (arreglo_Codigos==null){
                return;
             }
            
            arreglo_Codigos.splice(0, arreglo_Codigos.length);
            arreglo_Ajuste.splice(0, arreglo_Ajuste.length);
            arreglo_Cuenta.splice(0, arreglo_Cuenta.length);
        }

    });

    $('#Btn_Imprimir_Recibo').click(function () {
        location.href = "../Comercializacion/Frm_Ope_Cor_Recibos_Parciales.aspx?PAGINA=420";        
    });
}

function progress() 
{
    jQuery.ajaxSetup({
        async: false,
        cache: false,
        timeout: (2 * 1000),
        beforeSend: function () {
            MostrarProgress();
        },
        complete: function () {
            OcultarProgress();
        },
        error: function () {
            OcultarProgress();
        }
    });

}

function MostrarProgress() 
{
    $('[id$=Upgrade]').show();
}

function OcultarProgress() 
{
    $('[id$=Upgrade]').delay(2000).hide();
}

/*################################################################################################################*/
/*################################################################################################################*/
/*Funciones Retiro*/

function modalRetiro() 
{
    $('#Div_Retiro').jqm();

    $('#Btn_Retiros').click(function (event) {
        event.preventDefault();
        //Atributos del Modal
        modal();

        //Configuaracion
        $('.campo_retiro').attr('disabled', 'disabled');

        $('#Div_Retiro').jqmShow();
    });

    $('#Btn_Salir_Domiciliacion').click(function () {
        var estatus = $("#Btn_Salir_Retiro").attr('alt')

        if (estatus == "Salir") {
            $('#Div_Retiro').jqmHide();
        }
        else if (estatus == "Cancelar") {
            $('.campo_retiro').attr('disabled', 'disabled');
            $('.campo_retiro').val('');
            $('#Btn_Nuevo_Retiro').attr('src', '../imagenes/paginas/icono_nuevo.png');
            $('#Btn_Nuevo_Retiro').attr('alt', 'Nuevo');
            $('#Btn_Salir_Retiro').attr('src', '../imagenes/paginas/icono_salir.png');
            $('#Btn_Salir_Retiro').attr('alt', 'Salir');
        }
    });
}

/*################################################################################################################*/
/*################################################################################################################*/
/*Funciones Domiciliacion*/

function modalDomiciliacion() 
{
    $('#Div_Domiciliacion').jqm();

    $('#Btn_Datos_Tarjeta').click(function (event) {
        event.preventDefault();
        //Atributos del Modal
        modal();

        //Configuaracion
        $('.campo_domiciliacion').attr('disabled', 'disabled');

        $('#Div_Domiciliacion').jqmShow();
    });

    $('#Btn_Salir_Domiciliacion').click(function () {
        var estatus = $("#Btn_Salir_Domiciliacion").attr('alt')

        if (estatus == "Salir") {
            $('#Div_Domiciliacion').jqmHide();
        }
        else if (estatus == "Cancelar") {
            $('.campo_domiciliacion').attr('disabled', 'disabled');
            $('.campo_domiciliacion').val('');
            $('#Btn_Nueva_Domiciliacion').attr('src', '../imagenes/paginas/icono_nuevo.png');
            $('#Btn_Nueva_Domiciliacion').attr('alt', 'Nuevo');
            $('#Btn_Salir_Domiciliacion').attr('src', '../imagenes/paginas/icono_salir.png');
            $('#Btn_Salir_Domiciliacion').attr('alt', 'Salir');
        }
    });
}

function operacionesDomiciliacion() 
{
    $('#Btn_Nueva_Domiciliacion').click(function () {
        var estatus = $('#Btn_Nueva_Domiciliacion').attr('alt');

        if (estatus == "Nuevo") {
            $('.campo_domiciliacion').removeAttr('disabled');
            $('#Btn_Nueva_Domiciliacion').attr('src', '../imagenes/paginas/icono_guardar.png');
            $('#Btn_Nueva_Domiciliacion').attr('alt', 'Guardar');
            $('#Btn_Salir_Domiciliacion').attr('src', '../imagenes/paginas/icono_cancelar.png');
            $('#Btn_Salir_Domiciliacion').attr('alt', 'Cancelar');
            $('#Cmb_Tipo_Movimiento_Domiciliacion').attr({ 'disabled': 'disabled' });
            $('#Txt_Motivo_Baja_Domiciliacion').attr({ 'disabled': 'disabled' });
        }
        else if (estatus == "Guardar") {

            var campos = $('.campo_domiciliacion');
            var campo_domiciliacion = campos.serialize();

            //recorremos el serializarray para formar la cadena de los controles encontrados
            var enviar = campo_domiciliacion +
                        "&Usuario=" + $('#Txt_Empleado_Valor').val();

            //redireccionamos a la pagina y pasamos como parametro la cadena, para controlar la informacion 
            $.ajax({ url: "../../Controladores/Frm_Controlador_Cobro_Recibos.aspx?accion=Domiciliacion&" + enviar,
                type: 'POST',
                async: false,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('.campo_domiciliacion').attr('disabled', 'disabled');
                    $('#Div_Domiciliacion').jqmHide();
                }
            });
        }
    });
}

$('#Cmb_Tipo_Tarjeta_Domiciliacion').change(function (event) {
    var banco = $("#Cmb_Tipo_Tarjeta_Domiciliacion option:selected").val();
        $('#Cmb_Banco_Domiciliacion').removeAttr('style');
        $('#Cmb_Banco_Domiciliacion').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
});

$('#Cmb_Banco_Domiciliacion').change(function (event) {
    var banco = $("#Cmb_Banco_Cheque option:selected").val();
    if (banco != 'SELECCIONE') {
        $('#Cmb_Banco_Domiciliacion').removeAttr('style');
        $('#Cmb_Banco_Domiciliacion').attr('style', 'width:98%;');
    }
    else {
        $('#Cmb_Banco_Domiciliacion').removeAttr('style');
        $('#Cmb_Banco_Domiciliacion').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
    }
});

/*################################################################################################################*/
/*################################################################################################################*/
/*Funciones Cheque*/

function modalCheque() {
    $('#Div_Cheques').jqm();

    $('#Btn_Datos_Cheque').click(function (event) {
        event.preventDefault();
        //Atributos del Modal
        modal();

        //Configuracion
        $('.campo_cheque').attr('disabled', 'disabled');

        $('#Div_Cheques').jqmShow();
    });

    $('#Btn_Salir_Cheque').click(function () {
        var estatus = $("#Btn_Salir_Cheque").attr('alt')

        if (estatus == "Salir") {
            $('#Div_Cheques').jqmHide();
        }
        else if (estatus == "Cancelar") {
            $('.campo_cheque').attr('disabled', 'disabled');
            $('.campo_cheque').val('');
            $('#Btn_Nuevo_Cheque').attr('src', '../imagenes/paginas/icono_nuevo.png');
            $('#Btn_Nuevo_Cheque').attr('alt', 'Nuevo');
            $('#Btn_Salir_Cheque').attr('src', '../imagenes/paginas/icono_salir.png');
            $('#Btn_Salir_Cheque').attr('alt', 'Salir');
            $('#Txt_Cheques').val('0');
        }
    });
}

function operacionesCheque() {
    $('#Btn_Nuevo_Cheque').click(function () {
        var estatus = $('#Btn_Nuevo_Cheque').attr('alt');

        if (estatus == "Nuevo") {
            $('.campo_cheque').removeAttr('disabled');
            $('#Btn_Nuevo_Cheque').attr('src', '../imagenes/paginas/icono_guardar.png');
            $('#Btn_Nuevo_Cheque').attr('alt', 'Guardar');
            $('#Btn_Salir_Cheque').attr('src', '../imagenes/paginas/icono_cancelar.png');
            $('#Btn_Salir_Cheque').attr('alt', 'Cancelar');
        }
        else if (estatus == "Guardar") {
            //$('.campo_cheque').attr('disabled', 'disabled');
            $('#Btn_Nuevo_Cheque').attr('src', '../imagenes/paginas/icono_nuevo.png');
            $('#Btn_Nuevo_Cheque').attr('alt', 'Nuevo');
            $('#Btn_Salir_Cheque').attr('src', '../imagenes/paginas/icono_salir.png');
            $('#Btn_Salir_Cheque').attr('alt', 'Salir');
            $('#Txt_Cheques').val($('#Txt_Cantidad_Numero_Cheque').val());
            $('#Div_Cheques').jqmHide();
        }
    });
}

$('#Cmb_Banco_Cheque').change(function (event) {
    var banco = $("#Cmb_Banco_Cheque option:selected").val();
    if (banco != 'SELECCIONE') {
        $('#Cmb_Banco_Cheque').removeAttr('style');
        $('#Cmb_Banco_Cheque').attr('style', 'width:98%;');
    }
    else {
        $('#Cmb_Banco_Cheque').removeAttr('style');
        $('#Cmb_Banco_Cheque').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
    }
});

/*################################################################################################################*/
/*################################################################################################################*/
/*Funciones Tarjetas*/

function modalTarjetas() 
{
    $('#Div_Tarjetas').jqm();

    $('#Btn_Datos_Tarjetas').click(function (event) {
        event.preventDefault();
        //Atributos del Modal
        modal();

        //Configuaracion
        $('.campo_tarjeta').attr('disabled', 'disabled');

        $('#Div_Tarjetas').jqmShow();
    });

    $('#Bnt_Salir_Tarjeta').click(function () {
        var estatus = $("#Bnt_Salir_Tarjeta").attr('alt')

        if (estatus == "Salir") {
            $('#Div_Tarjetas').jqmHide();
        }
        else if (estatus == "Cancelar") {
            $('.campo_tarjeta').attr('disabled', 'disabled');
            $('.campo_tarjeta').val('');
            $('#Btn_Nuevo_Tarjeta').attr('src', '../imagenes/paginas/icono_nuevo.png');
            $('#Btn_Nuevo_Tarjeta').attr('alt', 'Nuevo');
            $('#Bnt_Salir_Tarjeta').attr('src', '../imagenes/paginas/icono_salir.png');
            $('#Bnt_Salir_Tarjeta').attr('alt', 'Salir');
            $('#Txt_Tarjetas').val('0');
        }
    });
}

function operacionesTarjetas() 
{
    $('#Btn_Nuevo_Tarjeta').click(function () {
        var estatus = $('#Btn_Nuevo_Tarjeta').attr('alt');

        if (estatus == "Nuevo") {
            $('.campo_tarjeta').removeAttr('disabled');
            $('#Btn_Nuevo_Tarjeta').attr('src', '../imagenes/paginas/icono_guardar.png');
            $('#Btn_Nuevo_Tarjeta').attr('alt', 'Guardar');
            $('#Bnt_Salir_Tarjeta').attr('src', '../imagenes/paginas/icono_cancelar.png');
            $('#Bnt_Salir_Tarjeta').attr('alt', 'Cancelar');
        }
        else if (estatus == "Guardar") {
            $('.campo_cheque').attr('disabled', 'disabled');
            $('#Btn_Nuevo_Tarjeta').attr('src', '../imagenes/paginas/icono_nuevo.png');
            $('#Btn_Nuevo_Tarjeta').attr('alt', 'Nuevo');
            $('#Bnt_Salir_Tarjeta').attr('src', '../imagenes/paginas/icono_salir.png');
            $('#Bnt_Salir_Tarjeta').attr('alt', 'Salir');
            $('#Txt_Tarjetas').val($('#Txt_Monto_Tarjeta').val());
            $('#Div_Tarjetas').jqmHide();
        }
    });
}

$('#Cmb_Banco_ID_Tarjeta').change(function (event) {
    var banco = $("#Cmb_Banco_ID_Tarjeta option:selected").val();
    $('#Cmb_Banco_ID_Tarjeta').removeAttr('style');
    $('#Cmb_Banco_ID_Tarjeta').attr('style', 'width:98%;border-style:dotted; border-color:Red;');
});

/*################################################################################################################*/
/*################################################################################################################*/
/*Funciones Codigo Barras*/
var arreglo_Codigos = new Array(), contCodigo = 0;
var arreglo_Cuenta = new Array(), contCuenta = 0;
var arreglo_Ajuste = new Array(), contAjuste = 0;

function operacionCodigoBarras() 
{
    $("[id$='Txt_Codigo_Barras']").keypress(function (event) {
        if (event.keyCode == 13) {
            
            //if ($("[id$='Txt_Codigo_Barras']").val().length > 15) {
            limpiarTablasTarjetasCheques();
            var Codigo_Barras = $("[id$='Txt_Codigo_Barras']").val(),
                cantTR = $('.cod'),
                clase = 'GridItem',
                texto = '',
                codigo = $('.codigo'),
                options = '',
                encontrado = false;
                
             if (Codigo_Barras.length==0){
                 $.messager.alert('Japami','Escribe un n&uacute;mero de cuenta o c&oacute;digo de barras','Error');
                 return ;
             }   
              
              if(cantTR.length>0){
                 $.messager.alert('Japami','Solo un Recibo A la Vez','Error');
                 return ;
              }
              
              
            var accion = "";
            if (Codigo_Barras.indexOf("F") > -1 || Codigo_Barras.indexOf("f") > -1) accion = "Facturacion";
            else if (Codigo_Barras.indexOf("P") > -1 || Codigo_Barras.indexOf("p") > -1) accion = "Parcial";
            else if (Codigo_Barras.indexOf("A") > -1 || Codigo_Barras.indexOf("a") > -1) accion = "Anticipado";
            else if (Codigo_Barras.indexOf("D") > -1 || Codigo_Barras.indexOf("d") > -1) accion = "Diversos";
            else if (Codigo_Barras.indexOf("B") > -1 || Codigo_Barras.indexOf("b") > -1) accion = "Bonificaciones";
            else accion = "Cuenta"
             
             $('#ventana_mensaje').window('open');
            $.ajax({
                url: "../../Controladores/Frm_Controlador_Cobro_Recibos.aspx?accion=" + accion + "&codigo_barras=" + Codigo_Barras,
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('#ventana_mensaje').window('close');
                    if (data != null) {
                        $.each(data, function (i, item) {
                            
                            $('#Lbl_bonificaciones').text('');

                            if (item.estatus_region == "INICIADA" || item.estatus_region == "CALCULADA") {
                                $('#Lbl_Mensaje').text($('#Lbl_Mensaje').html('Lo sentimos la Factura de tú Región ' + item.no_region +
                                                                              ' se encuentra <b>' + item.estatus_region + '</b>.<br/>' +
                                                                              'Por favor regresa el siguiente día hábil.'));
                            }
                            else if (item.fecha_creo != item.fecha_actual) {
                                $('#Lbl_Mensaje').text($('#Lbl_Mensaje').html('Lo sentimos tu Recibo <b>' + item.codigo_barras + '</b> ha <b>CADUCADO</b>'));
                            }
                            else {
                                if (item.estatus == "PAGADO") {
                                    $('#Lbl_Mensaje').text($('#Lbl_Mensaje').html('Tu recibo <b>' + item.codigo_barras + '</b> ha sido <b>PAGADO</b>'));
                                }
                                else if (item.estatus == "CANCELADO") {
                                    $('#Lbl_Mensaje').text($('#Lbl_Mensaje').html('Tu recibo <b>' + item.codigo_barras + '</b> ha sido <b>CANCELADO</b>'));
                                }
                                else if (item.estatus == "PAGO PARCIAL") {
                                    $('#Lbl_Mensaje').text($('#Lbl_Mensaje').html('Tu recibo <b>' + item.codigo_barras + '</b> ya no es válido.<br/>' +
                                                                                  'Se encuentra en <b>PAGO PARCIAL</b>, solicite su nuevo recibo.<br/>' +
                                                                                  'Gracias por su atención.'));
                                }
                                else {
                                    //Turnar la clase del estilo del tr
                                    if (cantTR.length > 0) {
                                        clase = (cantTR.length % 2 == 0 ? 'GridItem' : 'GridAltItem');
                                        codigo.each(function (j, item2) {
                                            if (item2.id == Codigo_Barras.toUpperCase()) { encontrado = true; return; }
                                        });
                                    }

                                    if (encontrado == false) {
                                        options += '<tr class="' + clase + ' cod ' + item.codigo_barras + '">' +
                                                '<td align="left" style="width:10%;"><label id="' + item.codigo_barras + '" class="codigo" onclick="javascript:Mensaje(this.id);" ' +
                                                                                    'style="width:100%;"><img alt="Seleccionar" src="../imagenes/gridview/blue_button.png" style="cursor:pointer;background-color:Transparent;border:0px;"/>&nbsp;' + item.codigo_barras + '</label>' +
                                                '</td>' +
                                                '<td align="center" style="width:10%;"><label class="no_cuenta">' + item.no_cuenta + '</label></td>' +
                                                '<td align="center" style="width:10%;"><label class="importe">' + item.monto + '</label></td>' +
                                                '<td align="center" style="width:10%;"><label class="ajuste">' + item.ajuste + '</label></td>' +
                                                '<td align="center" style="width:10%;"><label class="total">' + item.total + '</label></td>' +
                                                '<td align="center" style="width:10%;"><label class="comentarios">' + item.comentarios + '</label></td>' +
                                                '<td align="center" style="width:0%;display:none;"><label class="mensajes">' + item.mensaje + '</label></td>' +
                                                '<td align="center" style="width:0%;display:none;" class="codigo_bonificacion" >' + item.codigo_barras_bonificacion + ' </td> ' +
                                                '<td align="center" style="width:0%;display:none;" class="codigo_parciales" >' + item.codigo_barras_parcial + ' </td> ' +
                                                '<td align="center" style="width:10%;" class="' + item.codigo_barras + '"><button type="button" name="' + item.codigo_barras + '" ' +
                                                                                            'onclick="javascript:quitarCodigo(this);" ' +
                                                                                            'style="cursor:pointer;background-color:Transparent;border:0px;">' +
                                                                                        '<img alt="Quitar" src="../imagenes/paginas/delete.png"/>' +
                                                                                    '</button>' +
                                                '</td>' +
                                            '</tr>';

                                        arreglo_Codigos[contCodigo] = item.codigo_barras;
                                        contCodigo++;
                                        arreglo_Ajuste[contAjuste] = item.ajuste;
                                        contAjuste++;
                                        arreglo_Cuenta[contCuenta] = item.no_cuenta;
                                        contCuenta++;

                                        $('#Lbl_Mensaje').text($('#Lbl_Mensaje').html('<b>Recibo&nbsp;' + item.codigo_barras + '</b><br/>' + item.mensaje));
                                                                                                      
                                        $('#Txt_Total').val(Number($('#Txt_Total').val()) + Number(item.total));
                                        $('#Txt_Efectivo').val($('#Txt_Total').val());
                                        $('#Txt_Saldo').val('0');
                                        $('#Txt_Cambio').val('0');
                                        $('#Btn_Nuevo').attr({ 'alt': 'Guardar' });
                                        
                                           if(accion=="Bonificaciones"){
                                             var flodescuentos;
                                             var intdescuentos;
                                             var descuento_ajuste;
                                             var total_descuento;
                                             
                                             flodescuentos=parseFloat(item.total_descuento);
                                             intdescuentos=parseInt(item.total_descuento);
                                             
                                             descuento_ajuste=(1-(flodescuentos-intdescuentos));                        
                                             if(descuento_ajuste<= 0.99)
                                                  total_descuento=flodescuentos+ descuento_ajuste;
                                                else
                                                  total_descuento=intdescuentos; 
                                             
                                             $('#txt_descuentos').val(total_descuento);
                                             $('#Txt_Total').val(Number($('#Txt_Total').val()) - Number($('#txt_descuentos').val()));
                                             $('#Txt_Efectivo').val($('#Txt_Total').val());
                                             
                                             visualizarDescuentos();
                                               var lbl_mensaje;
                                               
                                              
                                              lbl_mensaje= '<b>Recibo bonificaci&oacute;n:' + item.codigo_barras_bonificacion + '</b></br>'  + item.conceptos_bonificados + '</br>'; 
                                              $('#Lbl_bonificaciones').text($('#Lbl_bonificaciones').html(lbl_mensaje));
                                        }
                                        
                                        if(accion=="Parcial"){
                                            lbl_mensaje= '<b>Recibo Parcial:' + item.codigo_barras_parcial + '</b></br>'  + item.conceptos_factura_parcial + '</br>'; 
                                              $('#Lbl_bonificaciones').text($('#Lbl_bonificaciones').html(lbl_mensaje));
                                              $('#Txt_Efectivo').val(parseFloat(item.total_pagar));
                                              $('#Txt_Total').val(parseFloat(item.total_pagar));
                                              
                                        }
                                        
                                        
                                        
                                        
                                    }
                                    else if (encontrado == true) alert('Este Código de Barras ya ha sido insertado.');
                                }
                            }
                        });
                        $('#Grid_Recibos').append(options);
                    }
                    else {
                        $('#Lbl_Mensaje').text($('#Lbl_Mensaje').html('<p style="color:red;">Recibo <b>' + $("[id$='Txt_Codigo_Barras']").val() +
                                                                          '</b> no existe o no se ha encontrado o ya fue cobrado.<br/><br/>Verifique que se haya insertado correctamente</p>'));
                    }
                },
                 error: function(result) {
                 $('#ventana_mensaje').window('close');
                 alert("ERROR " + result.status + ' ' + result.statusText);

             }
            });
              $("[id$='Txt_Codigo_Barras']").val('');
            //}
        }
    });
}

function quitarCodigo(quitar) 
{
    $('#Txt_Total').val(Number($('#Txt_Total').val()) - Number($('.' + quitar.name + '').contents().find('label.total').text()));
    $('#Txt_Efectivo').val($('#Txt_Total').val());
    Operaciones("Quitar");
    $('tr.' + quitar.name + '').remove();
      
      $('#Txt_Efectivo').val("0");
      $('#Txt_Tarjetas').val("0")
      $('#Txt_Cheques').val("0"); 
      $('#Txt_Total').val("0");
      $('#Txt_Saldo').val("0");
      $('#txt_descuentos').val('');
      ocultarDescuentos();
      
    //Actualizar también el arreglo removiendo el codigo
    for (var i = 0; i < arreglo_Codigos.length; i++)
        if (arreglo_Codigos[i] == quitar.name)
            arreglo_Codigos.splice(i, 1);

    for (var i = 0; i < arreglo_Ajuste.length; i++)
        if (arreglo_Ajuste[i] == quitar.name)
            arreglo_Ajuste.splice(i, 1);

    for (var i = 0; i < arreglo_Cuenta.length; i++)
        if (arreglo_Cuenta[i] == quitar.name)
            arreglo_Cuenta.splice(i, 1);

    //var cantTR = $('.cod');
    //if (cantTR.length < 1) 
    //{    
        $('#Btn_Nuevo').attr({ 'src': '../imagenes/paginas/icono_guardar.png', 'alt': 'EsperaGuardar', 'title': 'Guardar' }); 
        //$('#Btn_Nuevo').attr({ 'src': '../imagenes/paginas/icono_guardar.png', 'alt': 'EsperaGuardar', 'disabled': 'disabled', 'title': 'Guardar' });
        $('#Btn_Salir').attr({ 'src': '../imagenes/paginas/icono_cancelar.png', 'alt': 'Cancelar', 'title': 'Cancelar' });
    //}
    arreglo_Codigos = new Array();
    arreglo_Cuenta = new Array();
    arreglo_Ajuste = new Array();
    contCodigo=0;
    contCuenta=0;
    contAjuste=0;
    $('#Lbl_Mensaje').text('Bienvenido!');
    $('#Lbl_bonificaciones').text('');
}

function Mensaje(Codigo_Barras) 
{
    $('#Lbl_Mensaje').text($('#Lbl_Mensaje').html('<b>Recibo&nbsp;' + $('.' + Codigo_Barras + '').contents().find('label.codigo').text() + '</b><br/><br/>' +
                                                                      $('.' + Codigo_Barras + '').contents().find('label.mensajes').text()));
}

function Operaciones(Tipo) 
{
    if (Tipo == "Agregar") {
        $('#Txt_Saldo').val(Number($('#Txt_Total').val()) - Number($('#Txt_Efectivo').val()) - Number($('#Txt_Tarjetas').val()) - Number($('#Txt_Cheques').val()));
        $('#Txt_Cambio').val(-Number($('#Txt_Total').val()) + (Number($('#Txt_Efectivo').val()) + Number($('#Txt_Tarjetas').val()) + Number($('#Txt_Cheques').val())));
        if (Number($('#Txt_Saldo').val()) < 0) $('#Txt_Saldo').val('0');
        if (Number($('#Txt_Cambio').val()) < 0) $('#Txt_Cambio').val('0');
    }
    else if (Tipo == "Quitar") {
        $('#Txt_Saldo').val(Number($('#Txt_Efectivo').val()) + Number($('#Txt_Tarjetas').val()) + Number($('#Txt_Cheques').val()) - Number($('#Txt_Total').val()));
        $('#Txt_Cambio').val(Number($('#Txt_Total').val()) - (Number($('#Txt_Efectivo').val()) + Number($('#Txt_Tarjetas').val()) + Number($('#Txt_Cheques').val())));
        if (Number($('#Txt_Saldo').val()) < 0) $('#Txt_Saldo').val('0');
        if (Number($('#Txt_Cambio').val()) < 0) $('#Txt_Cambio').val('0');
    }
}

/*################################################################################################################*/
/*################################################################################################################*/
/*Eventos Efectivo*/
function eventoEfectivo()
{
    $('#Txt_Efectivo').keypress(function (event) {
        if (event.keyCode == 13) Operaciones('Agregar');
        if ($('#Txt_Efectivo').val() == "") $('#Txt_Efectivo').val('0');
    });
    $('#Txt_Efectivo').blur(function (event) {
        Operaciones('Agregar');
        if ($('#Txt_Efectivo').val() == "") $('#Txt_Efectivo').val('0');
    });
    $('#Txt_Efectivo').change(function (event) {
        Operaciones('Agregar');
        if ($('#Txt_Efectivo').val() == "") $('#Txt_Efectivo').val('0');
    });
    $('#Txt_Tarjetas').val('0');
    $('#Txt_Cheques').val('0');
}

/*################################################################################################################*/
/*################################################################################################################*/
/*Eventos Tarjeta*/
function eventoTarjeta() {
    $('#Txt_Tarjetas').keypress(function (event) {
        if (event.keyCode == 13) Operaciones('Agregar');
        if ($('#Txt_Tarjetas').val() == "") $('#Txt_Tarjetas').val('0');
    });
    $('#Txt_Tarjetas').blur(function (event) {
        Operaciones('Agregar');
        if ($('#Txt_Tarjetas').val() == "") $('#Txt_Tarjetas').val('0');
    });
    $('#Txt_Tarjetas').change(function (event) {
        Operaciones('Agregar');
        if ($('#Txt_Tarjetas').val() == "") $('#Txt_Tarjetas').val('0');
    });
    $('#Txt_Efectivo').val('0');
    $('#Txt_Cheques').val('0');
}

/*################################################################################################################*/
/*################################################################################################################*/
/*Eventos Cheque*/
function eventoCheque() {
    $('#Txt_Cheques').keypress(function (event) {
        if (event.keyCode == 13) Operaciones('Agregar');
        if ($('#Txt_Cheques').val() == "") $('#Txt_Cheques').val('0');
    });
    $('#Txt_Cheques').blur(function (event) {
        Operaciones('Agregar');
        if ($('#Txt_Cheques').val() == "") $('#Txt_Cheques').val('0');
    });
    $('#Txt_Cheques').change(function (event) {
        Operaciones('Agregar');
        if ($('#Txt_Cheques').val() == "") $('#Txt_Cheques').val('0');
    });
    $('#Txt_Tarjetas').val('0');
    $('#Txt_Efectivo').val('0');
}
//---------------------------------------------------------------------------------------------------
function limpiarTablasTarjetasCheques(){
     $('#tabla_tarjetas').datagrid('loadData',{total:0 , rows:[]});
     $('#tabla_cheque').datagrid('loadData',{total:0, rows:[]});

}

//---------------------------------------------------------------------------------------------------

function ocultarDescuentos(){
   $('.descuentos').css('display','none');

}
//---------------------------------------------------------------------------------------------------

function visualizarDescuentos(){
  $('.descuentos').css('display','');
}

//---------------------------------------------------------------------------------------------------