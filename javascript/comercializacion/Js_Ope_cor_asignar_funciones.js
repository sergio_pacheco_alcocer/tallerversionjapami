﻿
var estado_seleccionar = 'bloqueado'
var estado_seleccionar_empleado = 'desbloqueado'
var total_registros;
var panel_tabla_empleado;
var paginador_tabla_empleado;
var opciones_paginador_tabla_empleado;
var funciones_empleado = {};
var renglon_editar;
var index_empleado=-1;

$(function() { //inicio

    // ventana utilizada como modal que aparece cuando se hace una petición
    $('#ventana_mensaje').window('close');

    crearGridEmpleados();
    CrearGridFuncionesEmpleados();
    obtenerDatosInciales();

    //eventos de los botones de la barra de herramientas inicio
    $("[id$='Btn_Buscar']").click(function(event) {
        buscar_empleado();
        event.preventDefault();
    });

    $("[id$='Btn_Modificar']").click(function(event) {
        editar_funciones_empleado();
        event.preventDefault();
    });

    $("[id$='Btn_Guardar']").click(function(event) {
        guardarRegistro();
        event.preventDefault();
    });

    $("[id$='Btn_Cancelar']").click(function(event) {
        cancelarRegistro();
        event.preventDefault();
    });



    $("[id$='Btn_Salir']").click(function(event) {
        event.preventDefault();
    });

 

});     // fin


//--------------------------------------------------------------------------------------------------------

function crearGridEmpleados() {

    $('#grid_empleados').datagrid({  // tabla inicio
        title: 'Empleados',
        width: 790,
        height: 160,
        columns: [[
        { field: 'empleado_id', title: 'ID', width: 50, sortable: true },
        { field: 'no_empleado', title: 'No Empleado', width: 100, sortable: true },
        { field: 'nombre_empelado', title: 'Nombre', width: 220, sortable: true },
        { field: 'apellido_paterno', title: 'Apellido Paterno', width: 210, sortable: true },
        { field: 'apellido_materno', title: 'Apellido Materno', width: 210, sortable: true },
        { field: 'Puesto', title: 'Estatus', width: 200, sortable: true }

    ]],
        pageSize: 10,
        pageList: [30],
        pagination: true,
        rownumbers: true,
        remoteSort: false,
        fitColumns: true,
        singleSelect: true,
        striped: true,
        loadMsg: 'cargando...',
        nowrap: false,
        onClickRow: function(rowIndex, rowData) {
            if (estado_seleccionar_empleado == "bloqueado")
                $('#grid_empleados').datagrid('selectRow', index_empleado);
            else
                obtenerFuncionesEmpledadoSeleccionado(rowData.empleado_id);
        },
        onBeforeLoad: function() {
            funciones_empleado = {};
            $('#grid_funciones').datagrid('unselectAll');
            paginador_tabla_empleado = $('#grid_empleados').datagrid('getPager');
            $(paginador_tabla_empleado).pagination({ showRefresh: false });
            
            
        },
        onLoadSuccess: function() {

        }

    });          // tabla final

    $('#grid_empleados').datagrid('hideColumn', 'empleado_id');
  
 }


 //--------------------------------------------------------------------------------------------------------

 function CrearGridFuncionesEmpleados() {

     $('#grid_funciones').datagrid({  // tabla inicio
         title: 'Funciones Asignadas',
         width: 790,
         height: 160,
         columns: [[
        { field: 'ck', checkbox: true },
        { field: 'funcion_id', title: 'ID', width: 50, sortable: true },
        { field: 'nombre_funcion', title: 'Funcion Empleado', width: 450, sortable: false }

    ]],
         pageSize: 10,
         pagination: false,
         rownumbers: true,
         remoteSort: false,
         fitColumns: true,
         singleSelect: false,
         striped: true,
         loadMsg: 'cargando...',
         nowrap: false,
         onClickRow: function(rowIndex, rowData) {

             if (estado_seleccionar == "bloqueado") {
                 visualizarLasFuncionesDeEmpleadosEngrid(funciones_empleado);

             }
         },
         onLoadSuccess: function(data) {
             $(':checkbox').attr('disabled', true);

         }

     });       // tabla final

     $('#grid_funciones').datagrid('hideColumn', 'funcion_id');
 
 }
 //--------------------------------------------------------------------------------------------------------

 function obtenerDatosInciales() {

     $('#grid_empleados').datagrid({ url: '../../Controladores/frm_controlador_asignacion_puestos.aspx', queryParams: {
     accion: 'obtenerEmpleados',
         busqueda: ''
     }, pageNumber: 1
 });

 $('#grid_funciones').datagrid({ url: '../../Controladores/frm_controlador_asignacion_puestos.aspx', queryParams: {
     accion: 'obtenerFunciones'
 }, pageNumber: 1
});



 }
 //--------------------------------------------------------------------------------------------------------

 function disableLink(e) {
     // cancels the event 
     e.preventDefault();
     return false;
 }

 //--------------------------------------------------------------------------------------------------------
 function obtenerFuncionesEmpledadoSeleccionado(empleado_id) {

     $('#ventana_mensaje').window('open');

     $.ajax({
         type: "POST",
         url: "../../Controladores/frm_controlador_asignacion_puestos.aspx/obtenerFuncionesdeUnempleado",
         data: "{'empleado_id':'" + empleado_id + "'}",
         contentType: "application/json; charset=utf-8",
         dataType: "json",
         success: function(response) {
             var respuesta = eval("(" + response.d + ")");
             if (respuesta.mensaje == "bien") {
                 $('#ventana_mensaje').window('close');
                 funciones_empleado = JSON.parse(respuesta.datos_auxiliares);
                 visualizarLasFuncionesDeEmpleadosEngrid(funciones_empleado);
//                 if(funciones_empleado.length>0)
//                    //$.messager.alert('Funciones Empleado', 'Proceso Terminado');        
//                 else
//                    //$.messager.alert('Funciones Empleado', 'El Empleado no tiene asignado ninguna funcion');
             }
             else {
                 $('#ventana_mensaje').window('close');
                 $.messager.alert('Funciones Empleado', 'Ocurrio un error:' + respuesta.mensaje, 'error');

             }

         },
         error: function(result) {
             $('#ventana_mensaje').window('close');
             $.messager.alert('Funciones Empleado', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');

         }

     });
   
 }

 //-------------------------------------------------------------------------------------------------------

 function visualizarLasFuncionesDeEmpleadosEngrid(funciones) {
     var renglones;
     var row;
     var index;
     $('#grid_funciones').datagrid('unselectAll');
     if (funciones.length > 0) {
         renglones = $('#grid_funciones').datagrid('getRows');
         for (var i = 0; i < renglones.length; i++) {
             row = renglones[i];
             for (var c = 0; c < funciones.length; c++) {
                 if (row.funcion_id == funciones[c].funcion_id) {
                     index = $('#grid_funciones').datagrid('getRowIndex',row);
                     $('#grid_funciones').datagrid('selectRow', index);
                          
                 }
             }
         }
     }
     
 }

 //-------------------------------------------------------------------------------------------------------

 function buscar_empleado() {
     var busqueda_txt;
     busqueda_txt = $("[id$='Txt_Busqueda']");
     funciones_empleado = {};
     $('#grid_funciones').datagrid('unselectAll');
     $('#grid_empleados').datagrid('loadData', { total: 0, rows: [] });
     $('#grid_empleados').datagrid({ url: '../../Controladores/frm_controlador_asignacion_puestos.aspx', queryParams: {
         accion: 'obtenerEmpleados',
         busqueda: $.trim(busqueda_txt.val())
     }, pageNumber: 1
     });
    
      
 }

//--------------------------------------------------------------------------------------------------------------
 function editar_funciones_empleado() {

     renglon_editar = $('#grid_empleados').datagrid('getSelected');
     if (renglon_editar == null) {
         $.messager.alert('Funciones de Empleado', 'Tienes que seleccionar un elemento');
         return;
     }
     index_empleado=$('#grid_empleados').datagrid('getRowIndex',renglon_editar);
     ext_ocultarBotonesOperacion();
     ext_visualizarBotonesGuardarCancelar();
     desbloquearControles();
     limpiarControles();
     estado_seleccionar = "desbloqueado";
     estado_seleccionar_empleado = "bloqueado";
     $(paginador_tabla_empleado).css('display', 'none');             
 }

 //-----------------------------------------------------------------------------------

 function bloquearControles() {

 }

 function desbloquearControles() {
 }
 
 function  limpiarControles() {
     $('#controles_operacion :text').val('');

 }

 function cancelarRegistro() {
     ext_ocultarBotonesGuardarCancelar();
     ext_visualizarBotonesOperacion();
     bloquearControles();
     limpiarControles();
     estado_seleccionar = "bloqueado";
     estado_seleccionar_empleado = "desbloqueado"
     $(paginador_tabla_empleado).css('display', 'block');  
     visualizarLasFuncionesDeEmpleadosEngrid(funciones_empleado);  
 }


 function guardarRegistro() {

     var rows;
     var str_claves;
    
     rows = $('#grid_funciones').datagrid('getSelections');
     str_claves = JSON.stringify(rows);
     
    
     $('#ventana_mensaje').window('open');

     $.ajax({
         type: "POST",
         url: "../../Controladores/frm_controlador_asignacion_puestos.aspx/actualizarFuncionesdeUnempleado",
         data: "{'claves':'" + str_claves + "','empleado_id':'" + renglon_editar.empleado_id + "'}",
         contentType: "application/json; charset=utf-8",
         dataType: "json",
         success: function(response) {
             var respuesta = eval("(" + response.d + ")");
             if (respuesta.mensaje == "bien") {
                 $('#ventana_mensaje').window('close');
                 funciones_empleado = JSON.parse(respuesta.datos_auxiliares);
                 cancelarRegistro();
                 $.messager.alert('Funciones Empleado', 'Proceso Terminado');
             }
             else {
                 $('#ventana_mensaje').window('close');
                 $.messager.alert('Funciones Empleado', 'Ocurrio un error:' + respuesta.mensaje, 'error');

             }

         },
         error: function(result) {
             $('#ventana_mensaje').window('close');
             $.messager.alert('Funciones Empleado', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');

         }

     });

   
 }

 //---------------------------------------------------------------------------------------