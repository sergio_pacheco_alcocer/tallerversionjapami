﻿$(document).ready(Inicializar_Controles_GRG);

function Inicializar_Controles_GRG() {
    Eventos_Cambios_Seleccion();
}

function Eventos_Cambios_Seleccion() {
    $('select[id$=Cmb_Dependencia_Panel]').change(
        function(e) {
            $('select[id$=Cmb_Dependencia]').val($('select[id$=Cmb_Dependencia_Panel]').val());
        }
    );
    $('select[id$=Cmb_Estatus]').change(
        function(e) {
        if ($('select[id$=Cmb_Estatus]').val() == 'TERMINADA') {
                $('select[id$=Cmb_Resultado]').removeAttr("disabled");
                $('textarea[id$=Txt_Diagnostico]').removeAttr("disabled");
            } else {
                $('select[id$=Cmb_Resultado]').attr("disabled", "true");
                $('textarea[id$=Txt_Diagnostico]').attr("disabled", "true");
            }
        }
    );
}