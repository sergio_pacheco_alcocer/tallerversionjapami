﻿
var lastIndex;
var concepto_debe_b;

$(function(){
 crearTablaBonificacion();

//----eventos

    //Evento del radio buton  opt_tipo_bonificacion
    $('#opt_tipo_bonificacion').change(function(event) {
        if ($('#opt_tipo_bonificacion').attr('checked')) {
            
            var criterio = $('#opt_tipo_bonificacion').val();
                criterio="BONIFICACION";
            cargarCombo(criterio);
        }
    });

    //Evento del radiobuton opt_tipo_ajuste
    $('#opt_tipo_ajuste').change(function(event) {
    if ($('#opt_tipo_ajuste').attr('checked')) {
       
            var criterio = $('#opt_tipo_ajuste').val();
                criterio="AJUSTE";  
            cargarCombo(criterio);
        }
     });


});

//------------------------------------------------------------------------------
function crearTablaBonificacion(){



      $('#grid_facturado').datagrid({  // tabla inicio
            title: 'Saldo',
            width: 650,
            height: 300,
            columns: [[
        { field: 'total_importe', title: 'total_importe', width: 0, sortable: true },
        { field: 'total_pagar', title: 'total_pagar', width: 0, sortable: true },    
        { field: 'no_cuenta', title: 'No Cuenta', width: 0, sortable: true },
        { field: 'no_factura_recibo', title: 'no_factura', width: 0, sortable: true },
        { field: 'impuesto_saldo', title: 'impuesto_saldo', width: 0, sortable: true },
        { field: 'total_iva', title: 'total_iva', width: 0, sortable: true },
        { field: 'fecha_emicion', title: 'Fecha', width: 0, sortable: true },
        { field: 'iva', title: 'iva', width: 0, sortable: true },
        { field: 'saldo', title: 'saldo', width: 0, sortable: true },
        { field: 'concepto_id', title: 'clave', width: 0, sortable: true },
        { field: 'concepto', title: 'Concepto', width: 150 },
        { field: 'debe', title: 'Debe', width: 150 },
        { field: 'paga', title: 'Paga', width: 150, editor:{type:'numberbox',options:{precision:2, min:0, max:999999}} },
        { field: 'ajuste', title: 'Bon/Ajust', width:150}


    ]],  onClickRow: function(rowIndex, rowData) {
            
    },
            pageSize: 50,
            pagination: false,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            showFooter: true,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap: false

        });  // tabla final
        
        $('#grid_facturado').datagrid('hideColumn','total_importe');
        $('#grid_facturado').datagrid('hideColumn','total_pagar');       
        $('#grid_facturado').datagrid('hideColumn','impuesto_saldo');
        $('#grid_facturado').datagrid('hideColumn','no_factura_recibo');
        $('#grid_facturado').datagrid('hideColumn','no_cuenta');
        $('#grid_facturado').datagrid('hideColumn','total_iva'); 
        $('#grid_facturado').datagrid('hideColumn','fecha_emicion');
        $('#grid_facturado').datagrid('hideColumn','iva');
        $('#grid_facturado').datagrid('hideColumn','saldo');   
        $('#grid_facturado').datagrid('hideColumn','concepto_id'); 




}


//-------------------------------------------------------------------------------

function abrirBonificaciones(){
   
   
     $('#ventana_bonificaciones').window({
                      closed: false,
                      onOpen: function() {                                                
                       
                        
                          
                      }                     
        });
   

}

//-----------------------------------------------------------------------

function quitarBonificaciones(){
  
  $('#ventana_bonificaciones :input').val('');
  $("[id$='txt_total_bonificaciones']").val('');
  
  var no_cuenta_l;

  no_cuenta_l=$("[id$='txt_no_cuenta']").val();
  $('#grid_facturado').datagrid('loadData',{total:0 , rows:[]});
  $('#grid_facturado').datagrid({ url: '../../Controladores/frm_controlador_pagos.aspx', queryParams: { accion: 'ultimaFacturacion', nocuenta: no_cuenta_l }, pageNumber: 1 });

  
}

//------------------------------------------------------------------------

function cargarCombo(criterio) {
    $('#ventana').window('open');
    $.ajax({
        url: "../../Controladores/frm_controlador_bonificaciones.aspx?Accion=datos_Combo" +
            "&Bonificacion_o_Ajuste=" + criterio,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            $('#ventana').window('close');
            if (data != null) {
                var select = $("[id$='cmb_motivo_modificacion']");
                $('option', select).remove();
                var options = '<option value="-1"><-SELECCIONE-></option>';
                $.each(data, function(i, item) {
                    options += '<option value="' + item.motivo_bonificacion_id + '">' + item.descripcion + '</option>';
                });
                $("[id$='cmb_motivo_modificacion']").append(options);

                //$('#cmb_motivo_modificacion').show();
            }
        },
        error: function(result) {
        $('#ventana').window('close');
            alert("ERROR " + result.status + " " + result.statusText);
        }
    });
}

//------------------------------------------------------------------------------------

function asignarDescuento(){
var renglon_l;
var cantidad_asignar_l;
var valor_asignar_l;
var index_l;
var datos_grid_l;   
var renglones_l;


   
renglones_l=$('#grid_facturado').datagrid('getRows');

if(renglones_l.lengt==0){
   $.messager.alert('Japami','No hay datos que seleccionar','info');
   return;
  
}
   
   
renglon_l=$('#grid_facturado').datagrid('getSelected');
index_l=$('#grid_facturado').datagrid('getRowIndex',renglon_l);
datos_grid_l=$('#grid_facturado').datagrid('getData');

if(renglon_l==null){
   $.messager.alert('Japami','Tienes que seleccionar el concepto al cual deseas hacerle un descuento','info');
   return;  
}
concepto_debe_b=parseFloat(renglon_l.debe);


if(isNaN(concepto_debe_b) || concepto_debe_b<=0 || concepto_debe_b=='undefined'){
   $.messager.alert('Japami','La cantidad a ajustar es cero','info');
   return;
      
}


cantidad_asignar_l= parseFloat( $("[id$='txt_cantidad_bonficacion']").val());

if(isNaN(cantidad_asignar_l) || cantidad_asignar_l<0 || cantidad_asignar_l=='undefined'){
   $.messager.alert('Japami','Cantidad Asignar Incorrecta','info');
   return;
      
}


if($('#opt_porcentaje_b').attr('checked')){
   
   if(cantidad_asignar_l> 100){
      $.messager.alert('Japami','Porcentaje Incorrecto','info');
     return;
   }
   
   valor_asignar_l=  decimal( (cantidad_asignar_l * concepto_debe_b) / 100,2);
   
}


if($('#opt_cantidad_b').attr('checked')){
    
    if(cantidad_asignar_l>concepto_debe_b){
      $.messager.alert('Japami','Cantidad Incorrecta','info');
      return;
    }
   valor_asignar_l=cantidad_asignar_l;
}



var aux_l= decimal( concepto_debe_b- valor_asignar_l,2);

if (aux_l==0){
      datos_grid_l.rows[index_l].paga= 0;
      datos_grid_l.rows[index_l].ajuste=valor_asignar_l;
   }else{
    datos_grid_l.rows[index_l].paga=valor_asignar_l    
    datos_grid_l.rows[index_l].ajuste=aux_l; 
  }



$('#grid_facturado').datagrid('loadData',datos_grid_l);

var importe_l;
var total_iva_l;
var total_l
var tasa_iva_l= parseInt( renglon_l.iva);


importe_l=  decimal( obtenerSumaGrid("paga"),2);
total_iva_l= decimal( (importe_l * tasa_iva_l)/100,2);
total_l=  decimal( importe_l + total_iva_l,2);


 // update footer row values and then refresh
      var rows = $('#grid_facturado').datagrid('getFooterRows');
      rows[0]['paga'] = importe_l;
      rows[1]['paga'] = total_iva_l
      rows[2]['paga'] = total_l;
     $('#grid_facturado').datagrid('reloadFooter');

importe_l= decimal( obtenerSumaGrid("ajuste"),2);
total_iva_l= decimal( (importe_l * tasa_iva_l)/100,2);
total_l= decimal( importe_l + total_iva_l,2);


rows = $('#grid_facturado').datagrid('getFooterRows');
      rows[0]['ajuste'] = importe_l;
      rows[1]['ajuste'] = total_iva_l
      rows[2]['ajuste'] = total_l;
     $('#grid_facturado').datagrid('reloadFooter');


$("[id$='txt_cantidad_bonficacion']").val('');

}

//-------------------------------------------------------------------------------


function obtenerSumaGrid(columna){

var renglon_l;
var renglones_l;
var suma_l=0;
var aux;

renglones_l=$('#grid_facturado').datagrid('getRows');

for(var i=0; i<renglones_l.length;i++){
  renglon_l=renglones_l[i];
  
   aux=parseFloat(renglon_l[columna]);
   if( isNaN(aux) || aux=='undefined') 
       aux=0; 
    suma_l=suma_l + aux;
     
} 

return suma_l;

}

// ---------------------------------

function obtenerCantidadBonificacion(){

var footer_bonificacion_l;
var total_bonificacion_l
var comentario_l;
var id_motivo_l;
var renglones_l;

renglones_l=$('#grid_facturado').datagrid('getRows');

if(renglones_l.length==0){ 
 $.messager.alert('Japami','No hay datos que bonificar','info');
  return ;
}



footer_bonificacion_l=$('#grid_facturado').datagrid('getFooterRows');
total_bonificacion_l=footer_bonificacion_l[2]['ajuste'];
comentario_l= $.trim($("[id$='txt_comentario_bonificacion']").val());
id_motivo_l=$("[id$='cmb_motivo_modificacion']").val();



if(id_motivo_l==-1){
   $.messager.alert('Japami','Tienes que seleccionar un motivo','info');
   return ; 
}


if(isNaN(total_bonificacion_l) || total_bonificacion_l<=0 || total_bonificacion_l=='undefined'){
  $.messager.alert('Japami','Tiene que haber cantidad de ajuste','info');
  return ; 
}

if(comentario_l.length==0){
   $.messager.alert('Japami','Se requiere un Comentario','info');
   return ; 
}


$("[id$='txt_total_bonificaciones']").val(total_bonificacion_l);


      $.messager.show({
				title:'Japami',
				msg:'Bonificaci&oacute;n agregada',
				timeout:20,
				showType:'slide'
			});

}



