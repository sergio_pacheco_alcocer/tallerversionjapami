﻿
var bandera_calle_a_buscar=null;
var bandera_colonia_a_buscar=null;

var objetoColonia=null;
var objetoCalle=null;
var objetoSector=null;
var objetoManzana=null;
var objetoActividadesGiro=null;




$(function(){

auto_completado_colonias();


// evento de cambio de region
$("[id$='cmb_region']").change(function(){
     $("[id$='txt_sector']").val('');
     $("[id$='txt_no_manzana']").val('');

});

$("[id$='cmb_giros']").change(function(){
     $("[id$='txt_actividad']").val('');
     

});



});

//-----------------------------------------------------------------------------


function format(item) {
    return item.nombre ;
}

function auto_completado_colonias() { 

//inciamos el auto completado
    $("[id$='txt_colonia_buscar']").autocomplete("../../Controladores/Frm_Controlador_Consulta_Usuario.aspx", {
        extraParams: { accion: 'autocompletar_colonia' },
        dataType: "json",
        parse: function(data) {
            return $.map(data, function(row) {
                return {
                    data: row,
                    value: row.colonia_id,
                    result: row.nombre

                }
            });
        },
        formatItem: function(item) {
            return format(item);
        }
    }).result(function(e, item) {
          objetoColonia=item; 
      });

}

function aceptarVentanaColonia(){
     
     if (objetoColonia==null){
       $.messager.alert('Japami','Tienes que seleccionar un elemento');
       return;
     }
     
     if (bandera_colonia_a_buscar=="datos_generales"){
     
         $("[id$='txt_colonia']").val(objetoColonia.nombre);
         $("[id$='txt_colonia']").attr('valor',objetoColonia.colonia_id);
     
         $("[id$='txt_calle']").val('');
         $("[id$='txt_calle_referencia1']").val(''); 
         $("[id$='txt_calle_referencia2']").val(''); 
         $("[id$='txt_no_manzana']").val(''); 
      
      }
      
      if(bandera_colonia_a_buscar=="datos_domicilio"){
            $("[id$='txt_colonia_propietario']").val(objetoColonia.nombre);
            $("[id$='txt_colonia_propietario']").attr('valor',objetoColonia.colonia_id);
            $("[id$='txt_calle_propietario']").val('');
         
      }
      
      
      objetoColonia=null; 
      
       $.messager.show({
				title:'Japami',
				msg:'Elemento Agregado',
				timeout:5,
				showType:'slide'
			});
       
}

//-----------------------------------------------------------------------------------------


function formatCalle(item) {
    return item.calle + ' ' + item.vialidad;
}

function autoCompletarCalles(col_id){

//inciamos el auto completado
    $("[id$='txt_calle_buscar']").autocomplete("../../Controladores/frm_controlador_editar_cuenta_usuario.aspx", {
        extraParams: { accion: 'autocompletar_calle',colonia_id:col_id },
        dataType: "json",
        parse: function(data) {
            return $.map(data, function(row) {
                return {
                    data: row,
                    value: row.calle_id,
                    result: row.calle

                }
            });
        },
        formatItem: function(item) {
            return formatCalle(item);
        }
    }).result(function(e, item) {
          objetoCalle=item; 
      });

}



function aceptarVentanaCalle(){


if(objetoCalle==null){
   $.messager.alert('Japami','Tienes que seleccionar un elemento');
       return;
}

 if( bandera_calle_a_buscar=="calle"){
    $("[id$='txt_calle']").val(objetoCalle.calle);
    $("[id$='txt_calle']").attr('valor',objetoCalle.calle_id);
   
 }
 
  if( bandera_calle_a_buscar=="calle_referencia1"){
    $("[id$='txt_calle_referencia1']").val(objetoCalle.calle);
    $("[id$='txt_calle_referencia1']").attr('valor',objetoCalle.calle_id);
   
 }
 
  if( bandera_calle_a_buscar=="calle_referencia2"){    
    $("[id$='txt_calle_referencia2']").val(objetoCalle.calle);
    $("[id$='txt_calle_referencia2']").attr('valor',objetoCalle.calle_id);
   
 }
 
  if( bandera_calle_a_buscar=="calle_propietario"){    
    $("[id$='txt_calle_propietario']").val(objetoCalle.calle);
    $("[id$='txt_calle_propietario']").attr('valor',objetoCalle.calle_id);
   
 }
 
 
 $.messager.show({
				title:'Japami',
				msg:'Elemento Agregado',
				timeout:5,
				showType:'slide'
			});
 
 bandera_calle_a_buscar="";
 objetoCalle=null;
 
}


//---------------------------------------------------------------------------------

function formatSector(item) {
    return item.numero_sector;
}

function autoCompletarSectores(region_id){

//inciamos el auto completado
    $("[id$='txt_sectores_buscar']").autocomplete("../../Controladores/frm_controlador_editar_cuenta_usuario.aspx", {
        extraParams: { accion: 'autocompletar_sectores',region_id:region_id },
        dataType: "json",
        parse: function(data) {
            return $.map(data, function(row) {
                return {
                    data: row,
                    value: row.sector_id,
                    result: row.numero_sector

                }
            });
        },
        formatItem: function(item) {
            return formatSector (item);
        }
    }).result(function(e, item) {
          objetoSector=item; 
      });

}


function aceptarVentanaSector(){

  if(objetoSector==null){
     $.messager.alert('Japami','Tienes que seleccionar un elmento');
     return;
  
  }
  
   $("[id$='txt_sector']").val(objetoSector.numero_sector);
   $("[id$='txt_sector']").attr('valor',objetoSector.sector_id);
   
   objetoSector=null;
   
    $.messager.show({
				title:'Japami',
				msg:'Elemento Agregado',
				timeout:5,
				showType:'slide'
			});


}

//------------------------------------------------------------------------------

function formatoManzana(item) {
    return item.numero_manzana;
}

function autoCompletarManzana(sector_id){

//inciamos el auto completado
    $("[id$='txt_buscar_manzana']").autocomplete("../../Controladores/frm_controlador_editar_cuenta_usuario.aspx", {
        extraParams: { accion: 'autocompletar_manzana',sector_id:sector_id },
        dataType: "json",
        parse: function(data) {
            return $.map(data, function(row) {
                return {
                    data: row,
                    value: row.manzana_id,
                    result: row.numero_manzana

                }
            });
        },
        formatItem: function(item) {
            return formatoManzana(item);
        }
    }).result(function(e, item) {
          objetoManzana=item; 
      });

}



function aceptarVentanaManzana(){

 
 if(objetoManzana==null){
     $.messager.alert('Japami','Tienes que seleccionar un elmento');
     return; 
 }
 
 
   $("[id$='txt_no_manzana']").val(objetoManzana.numero_manzana);
   $("[id$='txt_no_manzana']").attr('valor',objetoManzana.manzana_id);
   
   objetoManzana=null;
 
 
   $.messager.show({
				title:'Japami',
				msg:'Elemento Agregado',
				timeout:5,
				showType:'slide'
			});
 
 
}


//------------------------------------------------------------------------------

function formatoActividad(item) {
    return item.nombre_actividad;
}

function autoCompletarActividad(giro_id){

//inciamos el auto completado
    $("[id$='txt_actividad_buscar']").autocomplete("../../Controladores/frm_controlador_editar_cuenta_usuario.aspx", {
        extraParams: { accion: 'autocompletar_actividades_giros',giro_id:giro_id },
        dataType: "json",
        parse: function(data) {
            return $.map(data, function(row) {
                return {
                    data: row,
                    value: row.actividad_giro_id,
                    result: row.nombre_actividad

                }
            });
        },
        formatItem: function(item) {
            return formatoActividad(item);
        }
    }).result(function(e, item) {
          objetoActividadesGiro=item; 
      });

}


function aceptarVentanaActividad(){

 
 if(objetoActividadesGiro==null){
     $.messager.alert('Japami','Tienes que seleccionar un elmento');
     return; 
 }
 
 
   $("[id$='txt_actividad']").val(objetoActividadesGiro.nombre_actividad);
   $("[id$='txt_actividad']").attr('valor',objetoActividadesGiro.actividad_giro_id);
   
   objetoActividadesGiro=null;
 
 
   $.messager.show({
				title:'Japami',
				msg:'Elemento Agregado',
				timeout:5,
				showType:'slide'
			});
 
 
}