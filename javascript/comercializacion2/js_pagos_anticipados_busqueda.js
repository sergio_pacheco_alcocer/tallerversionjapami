﻿
var listaObjFacturacion;
var objFacturacion;


$(function(){

  crearTablaBusqueda();
  

});

//--------------------------------------------

function crearTablaBusqueda(){

         $('#grid_usuarios').datagrid({  // tabla inicio
            title: 'Usuarios',
            width: 820,
            height: 250,
            columns: [[
        { field: 'no_cuenta', title: 'No. cuenta', width:80, sortable: true },
        { field: 'usuario', title: 'Usuario', width: 200, sortable: true },
        { field: 'giro', title: 'Giro', width: 150, sortable: true },
        { field: 'tarifa', title: 'Tarifa', width: 150, sortable: true },
        { field: 'clave', title: 'Cve. Tarifa', width: 80, sortable: true },
        { field: 'tipo_cuota', title: 'T.cuota', width: 120, sortable: true },
        { field: 'aplica_iva', title: 'Aplica IVA', width: 100, sortable: true },
        { field: 'calle', title: 'calle', width: 150, sortable: true },
        { field: 'numero_exterior', title: 'No. Ext', width: 100, sortable: true },
        { field: 'numero_interior', title: 'No. Int', width: 100, sortable: true },
        { field: 'colonia', title: 'Colonia', width: 150, sortable: true },
        { field: 'colonia', title: 'Colonia', width: 150, sortable: true },
        { field: 'descuento_especial', title: 'Des. Especial', width: 100, sortable: true },
        { field: 'servicio', title: 'Servicio', width: 100, sortable: true },
        { field: 'descuento_personalizado', title: 'Des. Personalizado', width: 100, sortable: true },        
        { field: 'predio_id', title: '', width: 0, sortable: true },
        { field: 'grupo_concepto_id', title: '', width: 0, sortable: true },     
        { field: 'tarifa_id', title: '', width: 0, sortable: true }
        

    ]],     
             onClickRow: function(rowIndex, rowData) {
                  
            }, 
            pageSize: 50,
            pagination: true,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            showFooter: false,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap: true

        });  // tabla final


  
   $('#grid_usuarios').datagrid('hideColumn','predio_id');
   $('#grid_usuarios').datagrid('hideColumn','grupo_concepto_id');
   $('#grid_usuarios').datagrid('hideColumn','tarifa_id');
   
   
}


//------------------------------

function cerrarVentana(){
$('#ventana_busqueda').window('close');
$('#ventana_calculo_servicio_medido').window('close');
$('#ventana_calculo_cuota_fija').window('close');
$('#ventana_bonificaciones').window('close')

}

//--------------------------------------

function abrirVentana(){

 
   $('#ventana_busqueda').window({
                      closed: false,
                      onOpen: function() {
                          
                           if($("#id_aplica_descuentos").attr('checked')){
                              $("#id_aplica_descuentos").attr('checked',false);
                            }
                                                       
                         $('#calculos :input').val(''); 
                         $('#cuenta_usuario_general :input').val('');
                         $('#cuenta_usuario_general :hidden').val(''); 
                         $('#datos_opciones_descuentos :input').val('');   
                         bloquearOpcionesDescuentos();
                         bandera_de_meses_permitidos=0;
                         usuario_registro_descuento="";
                         $('#btn_ventana_autentificacion').linkbutton({disabled:true});
                         $('#btn_ventana_autentificacion2').linkbutton({disabled:true});
         
                                                                      
                         $('#grid_usuarios').datagrid('loadData',{total:0 , rows:[]});                         
                         $('#grid_facturado').datagrid('loadData',{ total:0, rows: [] });
                         $('#grid_facturado').datagrid('reloadFooter',{ rows: [] });
                         
                         $('#tabla_historico_lecturas').datagrid('loadData',{total:0 , rows:[]});                         
                         $('#ventana_bonificaciones :input').val('');
                         $("#opt_no_presenta_doc").attr('checked',true);
                         
                         $('#tabla_cuenta').datagrid('loadData',{total:0, rows:[]});
                         $('#tabla_cuenta').datagrid('reloadFooter',{ rows: [] });
                          
                      }                     
        });
 
}



//----------------------------------------

function buscarUsuario(){

 var no_cuenta;
   var nombre_usuario;
   var renglones;
   
   no_cuenta=$.trim( $("#txt_cuenta_b").val());
   nombre_usuario=$.trim( $("#txt_usuario_b").val());
   
   if ($('#opt_no_cuenta').attr('checked')){
      
      if (no_cuenta.length==0){
            $.messager.alert('Japami','Introduce un No. de cuenta');
            return ; 
      }
   }else {no_cuenta='';}
   
   if($('#opt_usuario').attr('checked')){
       if(nombre_usuario.length==0){
           $.messager.alert('Japami','Introduce un Nombre de usuario');
            return ; 
       }
      
   }else{nombre_usuario='';}
   

   $('#grid_usuarios').datagrid('loadData',{total:0, rows:[]});
   $('#grid_usuarios').datagrid({ url: '../../Controladores/frm_controlador_pagos.aspx', queryParams: { accion: 'obtenerUsuariosAnticipados', nocuenta: no_cuenta, nombre: nombre_usuario }, pageNumber: 1 });




}

//---------------------------------------
function aceptarUsuario(){

 var renglon;
 
 renglon=$('#grid_usuarios').datagrid('getSelected');
 
 if(renglon==null){
   $.messager.alert('Japami', 'Tienes que seleccionar un elemento');
   return; 
 }
 
buscarDatosFacturacion(renglon.no_cuenta,renglon);  


}


function buscarDatosFacturacion(no_cuenta,renglon){

 $('#ventana_mensaje').window('open');
   
           $.ajax({
            type: "POST",
            url: "../../Controladores/frm_controlador_pagos.aspx/obtenerDatosFacturacion",
            data: "{'no_cuenta':'" + no_cuenta + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                $('#ventana_mensaje').window('close'); 
                var respuesta = eval("(" + response.d + ")");
                if (respuesta.mensaje == "bien") {
                   procesoExitoso(respuesta,renglon);
                                              
                }
                else { 
                    $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');

                }

            },
            error: function(result) {
                $('#ventana_mensaje').window('close');
                $.messager.alert('Japami', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');

            }

        });
   



}
//-------------------------------------------------------------------------------
function procesoExitoso(respuesta,renglon){
  listaObjFacturacion= json_parse( respuesta.dato1);
   
  if(listaObjFacturacion.length==0 || listaObjFacturacion==null){
      $.messager.alert('Japami','No se encontro información de facturacion','info');
      return;
  }
  
  objFacturacion=listaObjFacturacion[0]; 
  
  if(objFacturacion=='undefined' || objFacturacion==null ){
     $.messager.alert('Japami','No se encontro información de facturacion','info');
      return;
  }
  
  llenarCajas("datos_domicilio",renglon);
  llenarCajas("datos_cuenta",renglon);
  llenarCajas("datos_facturacion",objFacturacion);
  llenarCajas("datos_descuentos",renglon);
  llenarCajas("datos_servicio",renglon);
  
  
  $("[id$='txt_porcentaje_iva']").val(objFacturacion.tasa_iva);
  
  obtenerLecturas(renglon);
  asignarid(objFacturacion,renglon);
  calcularPeriodoMaximo();
  obtenerUltimoFacturado(renglon);
  obtenerUltimoFacturadoImportes(renglon);
  $("[id$='txt_convenios']").val(respuesta.dato2);
  $("[id$='txt_otros_cargos']").val(respuesta.dato3);
  desbloquearOpcionesCalculo();
  
  //$("[id$='txt_porcentaje_iva']").val( porcentaje_iva); 
   
      $.messager.show({
				title:'Japami',
				msg:'Elemento Seleccionado',
				timeout:30,
				showType:'slide'
			});


	
  
  
}

//-------------------------------------------------------------------------------

function llenarCajas(panel,objetoDatos){
 var listaCajas;
 var caja;
 var atributo_nombre;
 var valor;
 listaCajas=$('#' + panel + ' :input');
 
 
  for(var i=0;i<listaCajas.length;i++){
     caja=listaCajas[i];
     atributo_nombre="";
     atributo_nombre=$(caja).attr('nombre');
     if(atributo_nombre!="undefined" ){
        valor= objetoDatos[atributo_nombre];     
        $(caja).val(valor); 
     }
   
  }

}

//--------------------------------------------------------------------------------------
function asignarid(objetoDatos,renglon){

 $("[id$='txt_predio_id']").val(renglon.predio_id);
 $("[id$='txt_no_factura_recibo']").val(objetoDatos.no_factura_recibo);
 $("[id$='txt_tarifa_id']").val(renglon.tarifa_id);
 
 $("[id$='txt_mes_periodo']").val(objetoDatos.mes_numero_periodo);
 $("[id$='txt_mes_actual']").val(objetoDatos.mes_numero_actual);
 $("[id$='txt_anos_periodo']").val(objetoDatos.anos_periodo);
 $("[id$='txt_anos_actual']").val(objetoDatos.anos_actual);


}
//-----------------------------------------------------------------------------------------------------
function calcularPeriodoMaximo(){
var saldo_actual_l;
var mes_actual_l;
var mes_periodo_l;
var estado_factura_l;
var no_pagos_permitidos_l;

saldo_actual_l=parseFloat($("[id$='txt_saldo']").val());
mes_actual_l=parseInt($("[id$='txt_mes_actual']").val());
mes_periodo_l=parseInt($("[id$='txt_mes_periodo']").val());
estado_factura_l=$("[id$='estado_recibo']").val();

no_pagos_permitidos_l= (12-mes_periodo_l);

if(no_pagos_permitidos_l==0){
   no_pagos_permitidos_l=12;
}

$("[id$='txt_no_pagos_permitidos']").val(no_pagos_permitidos_l);
$("[id$='txt_no_pagos']").val(no_pagos_permitidos_l);

}


//------------------------------------------------------------------------------

function obtenerUltimoFacturado(renglon){
 $('#grid_facturado').datagrid({ url: '../../Controladores/frm_controlador_pagos.aspx', queryParams: { accion: 'ultimaFacturacion', nocuenta: renglon.no_cuenta }, pageNumber: 1 });
}

//--------------------------------------------------------------------------------

function obtenerUltimoFacturadoImportes(renglon){

$('#tabla_cuenta').datagrid({ url: '../../Controladores/frm_controlador_pagos.aspx', queryParams: { accion: 'ultimaFacturacionImporte', nocuenta: renglon.no_cuenta }, pageNumber: 1 });

}

//--------------------------------------------------------------------------------

function obtenerLecturas(renglon){
   $('#tabla_historico_lecturas').datagrid('loadData',{total:0 , rows:[]}); 
   $('#tabla_historico_lecturas').datagrid({ url: '../../Controladores/frm_controlador_pagos.aspx', queryParams: { accion: 'obtenerLecturas', nocuenta: renglon.no_cuenta }, pageNumber: 1 });
}