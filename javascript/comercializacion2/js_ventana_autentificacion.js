﻿

var bandera_tipo_autentificacion;

$(function(){


});


//-----------------------------------------------------------------------------

function abrirVentanaAutentificacion(tipo){
 
 $('#txt_no_usuario_p').val('');
 $('#txt_password_p').val(''); 

 $('#ventana_verificacion_password').window('open');
 bandera_tipo_autentificacion=tipo;   
   
}

//----------------------------------------------------------------------------

function cerrarVentanaAutentificacion(){

 $('#ventana_verificacion_password').window('close');
  
}

//-----------------------------------------------------------------------------

function aceptarAutentificacion(){
   var  no_usuario_p;
   var no_password_p;
   
   no_usuario_p= $.trim($('#txt_no_usuario_p').val());
   no_password_p= $.trim($('#txt_password_p').val());
   
   
   if(no_usuario_p.length==0){
      $.messager.alert('Japami','Tienes que poner un número de usuario');
      return;
   
   }
      
   if(no_password_p.length==0){
      $.messager.alert('Japami','Tienes que poner un password');
      return;
   }
   
   
   
    $('#ventana_mensaje').window('open');
   
           $.ajax({
            type: "POST",
            url: "../../Controladores/frm_controlador_pagos.aspx/verificarAutentificacion",
            data: "{'no_usuario':'" + no_usuario_p + "','password':'" + no_password_p + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                $('#ventana_mensaje').window('close'); 
                var respuesta = eval("(" + response.d + ")");
                if (respuesta.mensaje == "bien") {
                   procesoExitoVerificacion(respuesta);
                                              
                }
                else { 
                    $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');

                }

            },
            error: function(result) {
                $('#ventana_mensaje').window('close');
                $.messager.alert('Japami', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');

            }

        });
   
}


//--------------------------------------------------------------------------------------

function procesoExitoVerificacion(respuesta){

  if(respuesta.dato1=="no_registrado"){
    $.messager.alert('Japami','El usuario no esta registrado');
    usuario_registro_descuento="";
    return;
  }
   
   
   
    

  if(bandera_tipo_autentificacion=="fuera_tiempo"){
     desbloquearOpcionesDescuentos();
     usuario_registro_descuento=respuesta.dato2;
     $('#btn_ventana_autentificacion').linkbutton({disabled:true});
  }
   
  if(bandera_tipo_autentificacion=="en_tiempo"){
     $('#opt_presenta_doc').attr('disabled',false);
     usuario_registro_descuento=respuesta.dato2;
     $('#btn_ventana_autentificacion2').linkbutton({disabled:true}); 
  } 
   
  $.messager.alert('Japami','Usuario Autentificado correctamente','info',function(){
    cerrarVentanaAutentificacion();
  });
  
   
  
   

}

