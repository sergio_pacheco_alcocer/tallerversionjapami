﻿var lastIndex;
var empleado_id;
var operacion_caja_id;
$(function(){

$('#btn_imprimir').linkbutton({disabled:true}); 
  
 // obtenemos el id_empleado
 
 empleado_id=$("[id$='txt_no_empleado']").val();  
 crearTablaCajerosAsigandos();
 crearTablaCajaAbierta();
 
 crearTablaDistribucionMonedaConsulta();
 
 crearTablaDistribucionMonedas();
// asignar monedas al grid
llenarTabladeDistribucionMonedas();


//llenamos el grid que nos permite ver las cajas asignadas
obtenerCajerosAsigandos();

// obtenemos los datos de que si el usuario tiene una caja abierta
obtenerCajasAbiertas();




});

//------------------------------------------------------------------------------

function crearTablaCajaAbierta(){

   //incio tabla
    $('#tabla_caja_abierta').datagrid({
        title: 'Caja Abierta ',
        width: 790,
        height: 150,
        toolbar:[{ 
            text:'Imprimir',
		    iconCls:'icon-print',
		    id:'btn_grid_imprimir',
		    handler:function(){
		              imprimirReporte();
					}
				}],
        columns: [[
           { field: 'operacion_caja_id', title: '', width: 0 },           
	       { field: 'nombre_sucursal', title: 'Sucursal', width:150},
	       { field: 'no_caja', title: 'No. Caja', width:80 },
	       { field: 'nombre', title: 'Cajero', width: 150 },
	       { field: 'monto_inicial', title: 'Monto Inicial', width: 120 },
	       { field: 'estado', title: 'Estado', width: 80 },
	       { field: 'fecha', title: 'Fecha', width: 80 },
	       { field: 'hora', title: 'Hora', width: 80 }
	            
           
    ]],onClickRow: function(rowIndex, rowData) {
         $('#tabla_distribucion_moneda').datagrid('loadData',{total:0, rows:[]});
         $('#tabla_distribucion_moneda').datagrid({url:'../../Controladores/frm_controlador_operacion_cajas.aspx',queryParams: {
         accion: 'obtenerDistribucionMonedas',
         operacion_caja_id:rowData.operacion_caja_id
     }, pageNumber: 1});
               
                          
    },
        pageSize: 50,
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        striped: true,
        nowrap: true,
        showFooter:true
    });
    
    $('#tabla_caja_abierta').datagrid('hideColumn','operacion_caja_id');


}



//------------------------------------------------------------------------------

function crearTablaCajerosAsigandos(){


 //incio tabla
    $('#tabla_cajeros_asignados').datagrid({
        title: 'Cajas Asignadas ',
        width: 790,
        height: 150,
        columns: [[
           { field: 'sucursal_id', title: '', width: 0 },           
	       { field: 'caja_id', title: '', width:0},
	       { field: 'empleado_id', title: '', width:0 },
	       { field: 'nombre_sucursal', title: 'Sucursal', width: 200 },
	       { field: 'numero_caja', title: 'Caja', width: 80 },
	       { field: 'nombre', title: 'Cajero', width: 200 }     
           
    ]],
        pageSize: 50,
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        striped: true,
        nowrap: true,
        showFooter:true
    });

   $('#tabla_cajeros_asignados').datagrid('hideColumn','sucursal_id');
   $('#tabla_cajeros_asignados').datagrid('hideColumn','caja_id');
   $('#tabla_cajeros_asignados').datagrid('hideColumn','empleado_id');

}

//-----------------------------------------------------------------------------------

function crearTablaDistribucionMonedas(){

//incio tabla
    $('#tabla_monedas').datagrid({
        title: 'Distribuci&oacute;n de Monedas',
        width: 790,
        height: 150,
        columns: [[
           { field: 'tipo_moneda', title: 'Tipo Moneda', width: 150 },           
	       { field: 'cantidad', title: 'Cantidad', width:150, editor:{type:'numberbox',options:{precision:0, min:0, max:999999}} },
	       { field: 'importe_efectivo', title: 'Importe Efectivo', width:150 }
	       
           
    ]],
         onClickRow: function(rowIndex, rowData) {
           
             if (lastIndex != rowIndex){
			      $('#tabla_monedas').datagrid('endEdit', lastIndex);
			      $('#tabla_monedas').datagrid('beginEdit', rowIndex);
		          calcularMontoInicial(rowIndex);	  								
			    }
		    lastIndex = rowIndex;
         
         },
        pageSize: 50,
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        striped: true,
        nowrap: true,
        showFooter:false
    });



}

function calcularMontoInicial(rowIndex){
  
  var editores = $('#tabla_monedas').datagrid('getEditors', rowIndex);
  var editor_cantidad = editores[0];

  
   editor_cantidad.target.bind('change', function(){  
        calcular();  
    });

       function calcular(){  
        setTimeout(function(){        
          $('#tabla_monedas').datagrid('acceptChanges');  
          lastIndex=-1;
          var  objDatos= $('#tabla_monedas').datagrid('getRows');     
          var monto_inicial=0;   
          var producto;
          for(var contador=0;contador<objDatos.length;contador++){
              producto= objDatos[contador].cantidad  *  objDatos[contador].importe_efectivo ;
              monto_inicial = monto_inicial + producto;      
          }
          
          $('#txt_monto_inicial').val(monto_inicial);
             
        }, 100);

     }



}


//-----------------------------------------------------------------------------------

function crearTablaDistribucionMonedaConsulta(){

   
   //incio tabla
    $('#tabla_distribucion_moneda').datagrid({
        title: 'Distribuci&oacute;n de Monedas Consulta',
        width: 790,
        height: 150,
        columns: [[
           { field: 'tipo_moneda', title: 'Tipo Moneda', width: 150 },           
	       { field: 'valor', title: 'Cantidad', width:150  }       
    ]], 
       
        pageSize: 50,
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        striped: true,
        nowrap: true,
        showFooter:false
    });

   
  
  
}



//-----------------------------------------------------------------------------------

function obtenerCajerosAsigandos(){

    $('#tabla_cajeros_asignados').datagrid('loadData',{total:0 , rows:[] });  
    $('#tabla_cajeros_asignados').datagrid({url:'../../Controladores/frm_controlador_operacion_cajas.aspx',queryParams: {
         accion: 'obtenerCajerosParaEmpleados',
         empleado_id:empleado_id
     }, pageNumber: 1});
     

}

//--------------------------------------------------------------------------------

function obtenerCajasAbiertas(){

  $('#tabla_caja_abierta').datagrid('loadData',{total:0 , rows:[] });  
  $('#tabla_caja_abierta').datagrid({url:'../../Controladores/frm_controlador_operacion_cajas.aspx',queryParams: {
         accion: 'obtenerCajasAbierta',
         empleado_id:empleado_id
     }, pageNumber: 1});

}

//------------------------------------------------------------------------------------

function llenarTabladeDistribucionMonedas(){
 var lista_moneda; 
 var renglon;
 var valores_moneda;
 var contador=0;
 lista_moneda= new Array();
 
// var moneda = {tipo_moneda:'0.05',cantidas:0,importe_efectivo:0.05}
 
  
 valores_moneda= new clase_moneda(' moneda 0.05',0,0.05);
 lista_moneda[contador]=valores_moneda;
  
  contador ++;
  valores_moneda= new clase_moneda('moneda 0.10',0,0.10);
  lista_moneda[contador]=valores_moneda;
 
  contador ++;
  valores_moneda= new clase_moneda('moneda 0.20',0,0.20);
  lista_moneda[contador]=valores_moneda;
 
 
  contador ++;
  valores_moneda= new clase_moneda('moneda 0.50',0,0.50);
  lista_moneda[contador]=valores_moneda;
 
  contador ++;
  valores_moneda= new clase_moneda('moneda 1',0,1);
  lista_moneda[contador]=valores_moneda;
 
  contador ++;
  valores_moneda= new clase_moneda('moneda 2',0,2);
  lista_moneda[contador]=valores_moneda;
 
  contador ++;
  valores_moneda= new clase_moneda('moneda 5',0,5);
  lista_moneda[contador]=valores_moneda;
 
  contador ++;
  valores_moneda= new clase_moneda('moneda 10',0,10);
  lista_moneda[contador]=valores_moneda;
 
  contador ++;
  valores_moneda= new clase_moneda('moneda 20',0,20);
  lista_moneda[contador]=valores_moneda;
 
  contador ++;
  valores_moneda= new clase_moneda('moneda 100',0,100);
  lista_moneda[contador]=valores_moneda;
 
  contador ++;
  valores_moneda= new clase_moneda('billete 20',0,20);
  lista_moneda[contador]=valores_moneda;
 
  contador ++;
  valores_moneda= new clase_moneda('billete 50',0,50);
  lista_moneda[contador]=valores_moneda;
 
  contador ++;
  valores_moneda= new clase_moneda('billete 100',0,100);
  lista_moneda[contador]=valores_moneda;
 
  contador ++;
  valores_moneda= new clase_moneda('billete 200',0,200);
  lista_moneda[contador]=valores_moneda;
 
  contador ++;
  valores_moneda= new clase_moneda('billete 500',0,500);
  lista_moneda[contador]=valores_moneda;
 
  contador ++;
  valores_moneda= new clase_moneda('billete 1000',0,1000);
  lista_moneda[contador]=valores_moneda;
 
 
 for(contador=0;contador<lista_moneda.length;contador++){
     renglon=lista_moneda[contador];
      $('#tabla_monedas').datagrid('appendRow',{
         tipo_moneda: renglon.tipo_moneda,
         cantidad: renglon.cantidad,
         importe_efectivo:renglon.importe_efectivo
      });
      
 }
 
  

}

//--------------------------------------------
function  clase_moneda(tipo_moneda,cantidad,importe_efectivo){
  this.tipo_moneda=tipo_moneda;
  this.cantidad=cantidad;
  this.importe_efectivo=importe_efectivo;
}


//--------------------------------------------

function agregarDatos(nombre_control,valor_asignar,arreglo_datos){

    if(arreglo_datos[nombre_control] !== undefined){
              if (!arreglo_datos[nombre_control].push){
                  arreglo_datos[nombre_control]=[arreglo_datos[nombre_control]];          
              }
              arreglo_datos[nombre_control].push(valor_asignar || '');
            }else {
                arreglo_datos[nombre_control]=valor_asignar || '';
            }
            

}


//-------------------------------------------

function guadarInformacion(){
   var renglon;
   var monto_inicial;
   var datos={};
   var str_valores;  
   var str_valores_detalles;
   
   
   $('#tabla_monedas').datagrid('acceptChanges');  
   renglon=$('#tabla_cajeros_asignados').datagrid('getSelected');
   
   if(renglon==null){
     $.messager.alert('Japami','Selecciona una caja');
     return ;
   }
   
   monto_inicial= parseFloat($('#txt_monto_inicial').val());
   
   if( isNaN(monto_inicial)){
     $.messager.alert('Japami','Introduce un monto inicial');
     return ;
        
   }
   
   if(monto_inicial==0){
     $.messager.alert('Japami','Introduce un monto inicial');
     return ;   
   }
   
   
   
   agregarDatos("caja_id",renglon.caja_id,datos);
   agregarDatos("empleado_id",renglon.empleado_id,datos);
   agregarDatos("sucursal_id",renglon.sucursal_id,datos);
   agregarDatos("monto_inicial",monto_inicial,datos);
   agregarDatos("estado","ABIERTA",datos);
   agregarDatos("operacion_caja_id","",datos);
   
   
  
   str_valores=toJSON(datos);
   
   str_valores_detalles=toJSON( $('#tabla_monedas').datagrid('getRows'));
   
   $('#ventana_mensaje').window('open');
   
           $.ajax({
            type: "POST",
            url: "../../Controladores/frm_controlador_operacion_cajas.aspx/guardarInformacion",
            data: "{'oper_cor_cajas':'" + str_valores + "','oper_cor_cajas_detalles':'" + str_valores_detalles + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                $('#ventana_mensaje').window('close'); 
                var respuesta = eval("(" + response.d + ")");
                if (respuesta.mensaje == "bien") {
                   procesoExitoso(respuesta);
                                              
                }
                else { 
                    $.messager.alert('Manazana', 'Ocurrio un error:' + respuesta.mensaje, 'error');

                }

            },
            error: function(result) {
                $('#ventana_mensaje').window('close');
                $.messager.alert('Japami', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');

            }

        });
        
   
   
   
   
}

function procesoExitoso(respuesta){
     
   $.messager.alert('Japmai','Se abrio la Caja Exitosamente','info',function(){
   
     $('#txt_monto_inicial').val('');
         $('#tabla_monedas').datagrid('loadData',{total:0 , rows:[]});
        operacion_caja_id=respuesta.dato1; 
        obtenerCajasAbiertas();
     
       $('#btn_guardar').linkbutton({disabled:true}); 
       $('#btn_imprimir').linkbutton({disabled:false}); 
   
   });    

}

//-----------------------------------------------------------------------------------------------


function imprimirReporteDirecto(){

 window.open("../../Reporte/frm_controlador_reporte_pdf.aspx?nombre_reporte=reporteAperturaCaja&operacion_caja_id=" + operacion_caja_id,"AperturaCaja","width=1000px,height=1000px,scrollbars=YES,resizable=YES"); 

}

//-----------------------------------------------------------------------------------------------

function imprimirReporte(){
var renglon ;

renglon=$('#tabla_caja_abierta').datagrid('getSelected');

if(renglon==null){
   $.messager.alert('Japami','Selecciona un elemento','info');
   return;
}


 window.open("../../Reporte/frm_controlador_reporte_pdf.aspx?nombre_reporte=reporteAperturaCaja&operacion_caja_id=" + renglon.operacion_caja_id,"ApeturaCaja","width=1000px,height=1000px,scrollbars=YES,resizable=YES"); 

}
