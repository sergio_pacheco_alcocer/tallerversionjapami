﻿var no_factura_recibo;
var tipo_cuota;
var tarifa_id;
var predio_id;
var mes_actual;
var ano_actual;
var mes_periodo;
var listaObjServicioMedido;
var objServicioMedido;
var objCuotaFija;
var no_pagos;
var maximo_pagos;
var porcentaje_iva;
var porcentaje_descuento;
var aplica_iva;
var bandera_de_meses_permitidos=0;
var usuario_registro_descuento;
var promedio_consumo_final;
var objConceptos_id;


$(function(){
    
    // recuperamos los id_ de los conceptos cobros
     objConceptos_id=json_parse($("[id$='txt_conceptos_id_h']").val() );
     
     if(objConceptos_id.length>0){
     
     }
    
    crearTableCuentas();
    
    bloquearOpcionesDescuentos();
    bloquearOpcionesCalculo();
    $('#btn_imprimir_recibo').linkbutton({disabled:true});
    $('#btn_ventana_autentificacion').linkbutton({disabled:true});
    $('#btn_ventana_autentificacion2').linkbutton({disabled:true});  
    
   
   
   // evento check que aplica los descuentos 
   $("[id$='id_aplica_descuentos']").click(function(){
   
          if($(this).attr('checked')){
            realizarDescuento();
            calcularPagoTotal();
          }else{
            eliminarDescuento();
            calcularPagoTotal();
          }
      
      
   });
   
   
   // evento cuando se selecciona si presento o no documentos
   $('#opt_no_presenta_doc').click(function(){
   
      if($("[id$='id_aplica_descuentos']").attr('checked')){
        realizarDescuento();
        calcularPagoTotal();
        }
   
   });
   
   //evento cuando se selecciona si presento o no documentos
  $('#opt_presenta_doc').click(function(){
       if($("[id$='id_aplica_descuentos']").attr('checked')){  
          realizarDescuento();
          calcularPagoTotal();
      }
  });
    
   
});


//-----------------------------------------------------------
function calcularPagos(){
var saldo_cotizacion_l;
var saldo_otros_cargos_l;


saldo_cotizacion_l= parseFloat( $("[id$='txt_convenios']").val());

if ( isNaN(saldo_cotizacion_l)  || saldo_cotizacion_l =='undefined' ){
    saldo_cotizacion_l=0;
}

saldo_otros_cargos_l= parseFloat( $("[id$='txt_otros_cargos']").val());

if ( isNaN(saldo_otros_cargos_l)  || saldo_otros_cargos_l =='undefined' ){
    saldo_otros_cargos_l=0;
}


if(saldo_cotizacion_l>0){
    $.messager.alert('Japami','El usuario seleccionado no puede  realizar un pago anticipado por tiene un saldo en convenio','info');
    return;
}

if(saldo_otros_cargos_l>0){
   $.messager.alert('Japami','El usuario seleccionado no puede  realizar un pago anticipado por tiene un saldo en otros cargos','info');
    return;
   
}


no_factura_recibo=$.trim( $("[id$='txt_no_factura_recibo']").val());

if (no_factura_recibo.length==0){
    $.messager.alert('Japami','Tienes que seleccionar una cuenta','info');
    return;
}

no_pagos=parseInt( $("[id$='txt_no_pagos']").val());
maximo_pagos= parseInt($("[id$='txt_no_pagos_permitidos']").val());

if ( isNaN(no_pagos) || no_pagos==0 || no_pagos=='undefined' ){
    $.messager.alert('Japami','Tienes que indicar un n&uacute;mero de pagos','info');
    return;
}


if ( isNaN(maximo_pagos) || maximo_pagos==0 || maximo_pagos=='undefined' ){
    $.messager.alert('Japami','Se requiere un m&aacute;ximo de pago','info');
    return;
}


if(no_pagos>maximo_pagos){
  $.messager.alert('Japami','el n&uacute;mero de pagos no sebe ser mayor al m&aacute;ximo de pagos','info');
    return;
}


aplica_iva= $.trim($("[id$='txt_aplica_iva']").val());
tipo_cuota=$.trim( $("[id$='txt_tipo_cuota']").val());
tarifa_id=$.trim( $("[id$='txt_tarifa_id']").val());
predio_id=$.trim($("[id$='txt_predio_id']").val());

mes_actual=parseInt($("[id$='txt_mes_actual']").val());
ano_actual=parseInt($("[id$='txt_anos_actual']").val());
mes_periodo=parseInt($("[id$='txt_mes_periodo']").val());



 $('#ventana_mensaje').window('open');
   
           $.ajax({
            type: "POST",
            url: "../../Controladores/frm_controlador_pagos.aspx/calculoPagoAnticipado",
            data: "{'tipo_cuota':'" + tipo_cuota + "','tarifa_id':'" + tarifa_id + "','predio_id':'" + predio_id + "','mes_actual':'" + mes_actual + "','anos_actual':'" + ano_actual + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                $('#ventana_mensaje').window('close'); 
                var respuesta = eval("(" + response.d + ")");
                if (respuesta.mensaje == "bien") {
                   procesoExitosoobtenerParametrosCalculo(respuesta);
                                              
                }
                else { 
                    $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');

                }

            },
            error: function(result) {
                $('#ventana_mensaje').window('close');
                $.messager.alert('Japami', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');

            }

        });




}

//--------------------------------------------------------------------------------
function procesoExitosoobtenerParametrosCalculo(respuesta){
 
 
 if(tipo_cuota=="Servicio Medido"){
    objServicioMedido= json_parse( respuesta.dato1);
    llenarCajas("ventana_calculo_servicio_medido",objServicioMedido);  
    $('#ventana_calculo_servicio_medido').window('open');  
  }
  
   if(tipo_cuota=="Cuota Fija"){
      objCuotaFija= json_parse( respuesta.dato1);
      llenarCajas("ventana_calculo_cuota_fija",objCuotaFija);
      $('#ventana_calculo_cuota_fija').window('open'); 
         
   }
  
  
  
}

//-----------------------------------------------------------------------------------

function aceptarCosto(){
   
   if(tipo_cuota=="Servicio Medido"){
     if  (aceptarCostoSM()==false){
        return;
     } 
   }
   
   if(tipo_cuota=="Cuota Fija"){
     if( aceptarCostoCF()==false){
        return;
     }
   }
   
   cerrarVentana();
      
   if(mes_actual==1 || mes_actual==2){
      desbloquearOpcionesDescuentos();
      bandera_de_meses_permitidos=1;
      $('#opt_presenta_doc').attr('disabled',true);
      $('#btn_ventana_autentificacion').linkbutton({disabled:true});
      $('#btn_ventana_autentificacion2').linkbutton({disabled:false});
              
   }else{
      $('#btn_ventana_autentificacion').linkbutton({disabled:false});
      $('#btn_ventana_autentificacion2').linkbutton({disabled:true});
   }
     
   bloquearOpcionesCalculo();
}


//-----------------------------------------------------------------------------------

function aceptarCostoCF(){

var valor_calculado_l;
var valor_manual_l;
var valor_seleccionado_l;


valor_calculado_l= decimal( parseFloat( $("[id$='txt_total_f']").val()),2);
valor_manual_l= decimal( parseFloat($("[id$='txt_cantidad_cuota_fija_f']").val()),2);


 if ($("[id$='opt_cuota_fija_calculado']").attr('checked')){
 
      if( isNaN(valor_calculado_l) || valor_calculado_l==0 || valor_calculado_l=='undefined'){
         $.messager.alert('Japami','Se requiere un costos inicial','info');
         return false;
      }
     valor_seleccionado_l=valor_calculado_l;
    
 }

 if ($("[id$='opt_cuota_fija_manual']").attr('checked')){
       if( isNaN(valor_manual_l) || valor_manual_l==0 || valor_manual_l=='undefined'){
         $.messager.alert('Japami','Se requiere un costos inicial','info');
         return false;
      }
   valor_seleccionado_l=valor_manual_l;
 }
 
 
 $("[id$='txt_importe_agua']").val(valor_seleccionado_l);
 $("[id$='txt_importe_anticipado']").val( decimal( no_pagos * valor_seleccionado_l,2)); 
 
 calcularPagoTotal();



}


//-----------------------------------------------------------------------------------
function aceptarCostoSM(){
var valor_calculado_l;
var valor_manual_l;
var valor_seleccionado_l;
var valor_catalogo_l;


valor_calculado_l=  decimal( parseFloat( $("[id$='txt_total_calculo_s']").val()),2);
valor_manual_l=  decimal( parseFloat($("[id$='txt_cantidad_servicio_medido_s']").val()),2);
valor_catalogo_l=decimal(parseFloat($("[id$='txt_cantidad_servicio_medido_catalogo_s']").val()),2);

 
 if ($("[id$='opt_servicio_medido_calculado']").attr('checked')){
 
      if( isNaN(valor_calculado_l) || valor_calculado_l==0 || valor_calculado_l=='undefined'){
         $.messager.alert('Japami','Se requiere un costo inicial','info');
         return false;
      }
     valor_seleccionado_l=valor_calculado_l;
     promedio_consumo_final=decimal( parseFloat( $("[id$='txt_promedio_consumo_s']").val()),2);
 }

 if ($("[id$='opt_servicio_medido_manual']").attr('checked')){
       if( isNaN(valor_manual_l) || valor_manual_l==0 || valor_manual_l=='undefined'){
         $.messager.alert('Japami','Se requiere un costo inicial','info');
         return false;
      }
   valor_seleccionado_l=valor_manual_l;
   promedio_consumo_final=decimal( parseFloat( $("[id$='txt_promedio_personalizado_s']").val()),2);
 }
 
  if ($("[id$='opt_servicio_medido_catalogo']").attr('checked')){
       if( isNaN(valor_catalogo_l) || valor_catalogo_l==0 || valor_catalogo_l=='undefined'){
         $.messager.alert('Japami','Se requiere un costo inicial','info');
         return false;
      }
   valor_seleccionado_l=valor_catalogo_l;
   promedio_consumo_final=decimal( parseFloat( $("[id$='txt_promedio_catalogo_s']").val()),2);
 }
 

  if( isNaN(promedio_consumo_final)  || promedio_consumo_final=='undefined'){
        promedio_consumo_final=0;   
     }


 $("[id$='txt_importe_agua']").val(valor_seleccionado_l);
 $("[id$='txt_importe_anticipado']").val( decimal( no_pagos * valor_seleccionado_l,2)); 
 calcularPagoTotal();

  return true;

}

//-------------------------------------------------------------------------

function eliminarDescuento(){
$("[id$='txt_importe_descuento']").val(0);    
$("[id$='txt_descuento_aplicado']").val(0);
}


//-------------------------------------------------------------------------

function realizarDescuento(){

var aux_l;
var anticipo_l;

if($('#opt_no_presenta_doc').attr('checked')){
   porcentaje_descuento= parseFloat( $("[id$='txt_descuento_general']").val());
}


if($('#opt_presenta_doc').attr('checked')){
  porcentaje_descuento= parseFloat( $("[id$='txt_descuento_personalizado']").val());
  
  if( isNaN(porcentaje_descuento) || porcentaje_descuento==0 || porcentaje_descuento=='undefined'){
      porcentaje_descuento= parseFloat( $("[id$='txt_descuento_especial']").val());
  }
   
}

 if( isNaN(porcentaje_descuento) || porcentaje_descuento==0 || porcentaje_descuento=='undefined'){
      porcentaje_descuento=0;
 }
 

  if( isNaN(porcentaje_descuento) || porcentaje_descuento==0 || porcentaje_descuento=='undefined'){
      porcentaje_descuento= parseFloat( $("[id$='txt_descuento_especial']").val());
  }
 
 
 $("[id$='txt_descuento_aplicado']").val(porcentaje_descuento);
 
 anticipo_l=  parseFloat( $("[id$='txt_importe_anticipado']").val());
 if(isNaN(anticipo_l) || anticipo_l=='undefined'){
   anticipo_l=0;
 }
  

if(mes_actual==2 && mes_periodo==1){
   var importe_enero_l;
   importe_enero_l=obtenerCostoMesEnero();
   aux_l= decimal( ((anticipo_l + importe_enero_l) * porcentaje_descuento)/100,2); 
   $("[id$='txt_importe_descuento']").val(aux_l);  

}else{
   aux_l= decimal( (anticipo_l * porcentaje_descuento)/100,2); 
   $("[id$='txt_importe_descuento']").val(aux_l);   
}  
 
 

}


//---------------------------------------------------------------------------

function calcularPagoTotal(){
 var anticipo_l;
 var descuento_l;
 var aux_l;
 var total_pagar_l;
 
 anticipo_l= parseFloat( $("[id$='txt_importe_anticipado']").val());
 descuento_l=parseFloat( $("[id$='txt_importe_descuento']").val()); 
 
 
  if( isNaN(anticipo_l)  || anticipo_l=='undefined'){
      anticipo_l=0;
  }
 
   if( isNaN(descuento_l)  || descuento_l=='undefined'){
      descuento_l=0;
  }

// if(aplica_iva=="SI"){
//     aux_l= decimal( ((anticipo_l-descuento_l) * porcentaje_iva)/100,2);      
//  }else{
//     aux_l=0;
//     
//  }
  
 porcentaje_iva=$("[id$='txt_porcentaje_iva']").val(); 
 
 if(isNaN(porcentaje_iva) ||  porcentaje_descuento=='undefined' ){
  porcentaje_iva=0;
 }
  
 aux_l= decimal( ((anticipo_l-descuento_l) * porcentaje_iva)/100,2);    
$("[id$='txt_iva_pagar']").val(aux_l);

total_pagar_l= decimal( (anticipo_l -descuento_l) + aux_l,2);
$("[id$='txt_total_pagar']").val(total_pagar_l);



}

//---------------------------------------------------------------------------


function bloquearOpcionesDescuentos(){
  $('#datos_opciones_descuentos :input').attr('disabled',true);
}

function desbloquearOpcionesDescuentos(){
  $('#datos_opciones_descuentos :input').attr('disabled',false);
}

function bloquearOpcionesCalculo(){
    $('#datos_opciones_calculo :input').attr('disabled',true);
    $('#btn_calcular').linkbutton({disabled:true})
}

function desbloquearOpcionesCalculo(){
    $('#datos_opciones_calculo :input').attr('disabled',false);
    $('#btn_calcular').linkbutton({disabled:false})
}



//---------------------------------------------------------------------------

function nuevo_calculo(){
var aux_l;

  if($("#id_aplica_descuentos").attr('checked')){
         $("#id_aplica_descuentos").attr('checked',false);
    } 
  
  
 aux_l=$("[id$='txt_no_pagos_permitidos']").val(); 
 $('#calculos :input').val('');
 $("[id$='txt_no_pagos_permitidos']").val(aux_l);
 
 bloquearOpcionesDescuentos();
 desbloquearOpcionesCalculo();
 obtenerBonificacion();
 $('#btn_imprimir_recibo').linkbutton({disabled:true});
 $("[id$='txt_cantidad_bonficacion']").val('');
 $("[id$='txt_comentario_bonificacion']").val('');

 usuario_registro_descuento="";
 $("#opt_no_presenta_doc").attr('checked',true);
 

 
}


function obtenerBonificacion(){
var no_cuenta_l;

  no_cuenta_l=$("[id$='txt_no_cuenta']").val();
  $('#grid_facturado').datagrid({ url: '../../Controladores/frm_controlador_pagos.aspx', queryParams: { accion: 'ultimaFacturacion', nocuenta: no_cuenta_l }, pageNumber: 1 });

}


//------------------------------------------------------------------------------

function nuevoPago(){

   $('#btn_imprimir_recibo').linkbutton({disabled:true});
   $('#btn_guardar_informacion').linkbutton({disabled:false});
   $('#btn_buscar').linkbutton({disabled:false});
   $('#btn_quitarBonificacion').linkbutton({disabled:false});
   $('#btn_abrirBonificacion').linkbutton({disabled:false}) 
  
    
     if($("#id_aplica_descuentos").attr('checked')){
        $("#id_aplica_descuentos").attr('checked',false);
     }  
                              
     $('#calculos :input').val(''); 
     $('#cuenta_usuario_general :input').val('');
     $('#cuenta_usuario_general :hidden').val('');
     $('#datos_opciones_descuentos :input').val('');                                              
     $('#grid_usuarios').datagrid('loadData',{total:0 , rows:[]});
     $('#grid_facturado').datagrid('loadData',{total:0, rows:[]});
     $('#grid_facturado').datagrid('reloadFooter',{ rows: [] });
     $('#ventana_bonificaciones :input').val('');
     $('#tabla_historico_lecturas').datagrid('loadData',{total:0, rows:[]});
     
     usuario_registro_descuento="";
     
     bloquearOpcionesDescuentos();
     bandera_de_meses_permitidos=0;
     
     $("#opt_no_presenta_doc").attr('checked',true);
     
     $('#tabla_cuenta').datagrid('loadData',{total:0, rows:[]});
     $('#tabla_cuenta').datagrid('reloadFooter',{ rows: [] });

}


//------------------------------------------------------------------------------


function crearTableCuentas(){


         $('#tabla_cuenta').datagrid({  // tabla inicio
            title: 'Cuenta Usuario',
            width: 770,
            height: 300,
            columns: [[
        { field: 'concepto_id', title: 'concepto_id', width:80, sortable: true },
        { field: 'concepto', title: 'Concepto', width: 200, sortable: true },
        { field: 'importe', title: 'Importe', width: 150, sortable: true }
        

    ]],     
             onClickRow: function(rowIndex, rowData) {
                  
            }, 
            pageSize: 50,
            pagination: false,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            showFooter: true,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap: true

        });  // tabla final


        $('#tabla_cuenta').datagrid('hideColumn','concepto_id');
  
   


}
//------------------------------------------------------------------------------

function obtenerCostoMesEnero(){
var renglones_1;
var respuesta_l=0;
var agua_l; 
var drenaje_l;
var saneamiento_;


 if (objConceptos_id=='undefined' || objFacturacion==null  ){
   return 0;
 }


agua_l=obtenerSumaConcepto(objConceptos_id.concepto_agua_id);
drenaje_l=obtenerSumaConcepto(objConceptos_id.concepto_drenaje_id);
saneamiento_=obtenerSumaConcepto(objConceptos_id.concepto_saneamiento_id); 

return agua_l + drenaje_l + saneamiento_;


}


//-------------------------------------------------------------------
function obtenerSumaConcepto(codigo){

  var renglones_ls;
  var renglon_ls;
  var suma_ls=0;
  
  
  
  if (codigo=='undefined'){
    return 0;
  }
   
  renglones_ls=$('#tabla_cuenta').datagrid('getRows');
  
  for(var i=0 ;i<renglones_ls.length; i++){        
       renglon_ls=renglones_ls[i];
       if(renglon_ls.concepto_id==codigo){
          suma_ls= suma_ls + parseFloat(renglon_ls.importe);
          }
  }
  
  return suma_ls;
}


//------------------------------------------------------------------------------

Number.prototype.decimal = function(num) {
    pot = Math.pow(10, num);
    return parseInt(this * pot) / pot;
}

function decimal(valor,no_decimales) {
    n = eval(valor);
    return n.decimal(no_decimales);
} 



