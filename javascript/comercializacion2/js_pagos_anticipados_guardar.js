﻿
var objPagosAnticipado= {no_factura_recibo:'',
 no_cuenta:0,codigo_barras:'',total_pagar:0,
 numero_meses:0,total_iva:0,importe_anticipo:0,porcentaje_descuento:0,comentarios:'',usuario_registro_descuento_alto:'',promedio_consumo:0,descuento_anticipo_cantidad:0};


var objPagosBonificaciones={
 motivo_bonificacion_id:'',no_cuenta:'',
 tipo:'',descripcion_bonificacion:'',no_factura_recibo:'',
 nombre:'',domicilio:'',colonia:'',iva:0
 
 
};

var id_bonificacion="";
var id_anticipo="";

$(function(){


})
//--------------------------------------------------------------


function obtenerDatosBonificacion(){

var motivo_bonificacion_id=$("[id$='cmb_motivo_modificacion']").val();
var comentario_bonificacion=$("[id$='txt_comentario_bonificacion']").val();
var tipo_bonificacion;
var iva_l;


if($("[id$='opt_tipo_bonificacion']").attr('checked')){
  tipo_bonificacion='BONIFICACION';
}

if($("[id$='opt_tipo_ajuste']").attr('checked')){
  tipo_bonificacion='AJUSTE';
}

iva_l= parseFloat($("[id$='txt_porcentaje_iva']").val());



objPagosBonificaciones.no_factura_recibo=no_factura_recibo;
objPagosBonificaciones.no_cuenta=$("[id$='txt_no_cuenta']").val();
objPagosBonificaciones.nombre=$("[id$='txt_nombre']").val();
objPagosBonificaciones.domicilio=$("[id$='txt_calle']").val() + ' No.' + $("[id$='txt_numero_exterior']").val();
objPagosBonificaciones.colonia=$("[id$='txt_colonia']").val();
objPagosBonificaciones.motivo_bonificacion_id=motivo_bonificacion_id;
objPagosBonificaciones.descripcion_bonificacion=comentario_bonificacion;
objPagosBonificaciones.tipo=tipo_bonificacion;
objPagosBonificaciones.iva=iva_l;

}


//--------------------------------------------------------------
function guardarDatos(){
var total_pagar_l;
var total_iva_l;
var importe_anticipo_l;
var anticipo_l;
var descuento_l;
var porcentaje_descuento_l;

var total_bonificacion_l;

var comentarios_l;



comentarios_l=$.trim( $("[id$='txt_comentario_descuentos']").val());


total_pagar_l= parseFloat( $("[id$='txt_total_pagar']").val());

if(isNaN(total_pagar_l) || total_pagar_l<=0 || total_pagar_l=='undefined'){
  $.messager.alert('Japami','No hay informaci&oacute;n que almacenar','info');
  return;
}

total_iva_l= parseFloat( $("[id$='txt_iva_pagar']").val());

if(isNaN(total_iva_l) || total_iva_l=='undefined' ){
   total_iva_l=0;
}

anticipo_l=parseFloat($("[id$='txt_importe_anticipado']").val());


if(isNaN(anticipo_l) || anticipo_l=='undefined' ){
   anticipo_l=0;
}

descuento_l= parseFloat($("[id$='txt_importe_descuento']").val());

if(isNaN(descuento_l) || descuento_l=='undefined' ){
   descuento_l=0;
}

if (descuento_l>0){
   
   if(comentarios_l.length==0){
      $.messager.alert('Japami','Debe haber un comentario por el descuento','info');
      return;
   }

}


importe_anticipo_l=anticipo_l;

porcentaje_descuento_l=parseFloat($("[id$='txt_descuento_aplicado']").val());

if(isNaN(porcentaje_descuento_l) || porcentaje_descuento_l=='undefined' ){
   porcentaje_descuento_l=0;
}


if(isNaN(promedio_consumo_final) || promedio_consumo_final=='undefined' ){
   promedio_consumo_final=0;
}



objPagosAnticipado.no_factura_recibo=no_factura_recibo;
objPagosAnticipado.no_cuenta=$("[id$='txt_no_cuenta']").val();
objPagosAnticipado.codigo_barras=$("[id$='codigo_barras']").val();
objPagosAnticipado.total_pagar=total_pagar_l;
objPagosAnticipado.numero_meses=no_pagos;
objPagosAnticipado.total_iva=total_iva_l
objPagosAnticipado.importe_anticipo=importe_anticipo_l
objPagosAnticipado.porcentaje_descuento=porcentaje_descuento_l;
objPagosAnticipado.comentarios=comentarios_l;
objPagosAnticipado.usuario_registro_descuento_alto=usuario_registro_descuento; 
objPagosAnticipado.descuento_anticipo_cantidad=descuento_l;
objPagosAnticipado.promedio_consumo=promedio_consumo_final;

var objBonificacionDetalles=$('#grid_facturado').datagrid('getRows');
var objBonificacionesTotales=$('#grid_facturado').datagrid('getFooterRows');

var strBonificacionesDetalles= toJSON($('#grid_facturado').datagrid('getRows'));
var strBonificacionesTotales= toJSON($('#grid_facturado').datagrid('getFooterRows'));

obtenerDatosBonificacion();


var bandera_lleva_bonificacion;
total_bonificacion_l=$("[id$='txt_total_bonificaciones']").val();

if(isNaN(total_bonificacion_l) || total_bonificacion_l<=0 || total_bonificacion_l=='undefined' )
     bandera_lleva_bonificacion="NO"
 else
    bandera_lleva_bonificacion="SI"    
    
var strPagosAnticipados= toJSON(objPagosAnticipado);
var strPagosBonificacion= toJSON(objPagosBonificaciones);    


 $('#ventana_mensaje').window('open');
   
           $.ajax({
            type: "POST",
            url: "../../Controladores/frm_controlador_pagos.aspx/guardarPagoAnticipado",
            data: "{'strPagoAnticipado':'" + strPagosAnticipados + "','strBonificacion':'" + strPagosBonificacion + "','strBonificacion_detalle':'" + strBonificacionesDetalles + "','strBonificacionTotales':'" + strBonificacionesTotales + "','bandera_lleva_bonificacion' : '" + bandera_lleva_bonificacion + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                $('#ventana_mensaje').window('close'); 
                var respuesta = eval("(" + response.d + ")");
                if (respuesta.mensaje == "bien") {
                   procesoExitoGuardar(respuesta);
                                              
                }
                else { 
                    $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');

                }

            },
            error: function(result) {
                $('#ventana_mensaje').window('close');
                $.messager.alert('Japami', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');

            }

        });




}

//---------------------------------------------------

function procesoExitoGuardar(respuesta){
 


 $.messager.alert('Japami','Proceso Terminado','info',function(){
 
   $('#btn_imprimir_recibo').linkbutton({disabled:false});
   $('#btn_guardar_informacion').linkbutton({disabled:true});
   $('#btn_buscar').linkbutton({disabled:true});
   
   $('#btn_quitarBonificacion').linkbutton({disabled:true});
   $('#btn_abrirBonificacion').linkbutton({disabled:true})
   
   $('#calculos :input').attr('disabled',true);
   $('#btn_nuevo_calculo').linkbutton({disabled:true});
   
   $('#btn_ventana_autentificacion').linkbutton({disabled:true});
   $('#btn_ventana_autentificacion2').linkbutton({disabled:true});
   
   
   window.open("../../Reporte/frm_controlador_reporte_pdf.aspx?nombre_reporte=formato_anticipo&no_factura_recibo_parcial=" + respuesta.dato1, "Anticipado", "width=1000px,height=1000px,scrollbars=YES,resizable=YES"); 
   id_bonificacion= $.trim(respuesta.dato2);
   id_anticipo=$.trim(respuesta.dato1);
   
   if(id_bonificacion.length>0){
      window.open("../../Reporte/frm_controlador_crystal.aspx?nombre_reporte=formato_bonificacion&bonificacion_id=" + id_bonificacion ,"Bonificacion","width=400px,height=400px,scrollbars=NO");
   }
   
   
 });
  

}

//----------------------------------------------------

function imprimirRecibo(){

 window.open("../../Reporte/frm_controlador_reporte_pdf.aspx?nombre_reporte=formato_anticipo&no_factura_recibo_parcial=" + id_anticipo, "Anticipado", "width=1000px,height=1000px,scrollbars=YES,resizable=YES"); 

 if(id_bonificacion.length>0){
      window.open("../../Reporte/frm_controlador_crystal.aspx?nombre_reporte=formato_bonificacion&bonificacion_id=" + id_bonificacion ,"Bonificacion","width=400px,height=400px,scrollbars=NO");
   }

}


