﻿
var operacion = "ninguno";
var panel_tabla;
var id_catalogo_caja="";
var numero_caja;
var estado;

$(function (){

  crearTabla();
  panel_tabla=$('#tabla_cajas').datagrid('getPanel');
  
  bloquearControles();

 //eventos de los botones de la barra de herramientas inicio
    $("[id$='Btn_Nuevo']").click(function(event) {
        nuevoElemento();
        event.preventDefault();
    });

    $("[id$='Btn_Modificar']").click(function(event) {
        editarElemento();
        event.preventDefault();
    });

   
    $("[id$='Btn_Guardar']").click(function(event) {
        guardarRegistro();
        event.preventDefault();
    });

    $("[id$='Btn_Cancelar']").click(function(event) {
        cancelarRegistro();
        event.preventDefault();
    });

    $("[id$='Btn_Salir']").click(function(event) {
        event.preventDefault();
    });

// llena el grid con informacion
obtenerCajas();


});

//------------------------------------------------------------------------------------

function crearTabla(){
  
   //incio tabla
    $('#tabla_cajas').datagrid({
        title: 'Cajas ',
        width: 810,
        height: 300,
        columns: [[
           { field: 'id_catalogo_caja', title: 'id', width: 0 },           
	       { field: 'no_caja', title: 'No. Caja', width:100},
	       { field: 'estado', title: 'Estado', width:150 }
	         
           
    ]],onClickRow: function(rowIndex, rowData) {
                  seleccionarElemento(rowData);      
    },
        pageSize: 50,
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        striped: true,
        nowrap: true,
        showFooter:true
    });

   $('#tabla_cajas').datagrid('hideColumn','id_catalogo_caja');
 
 

}


function seleccionarElemento(rowData){
   id_catalogo_caja=rowData.id_catalogo_caja;
   $("[id$='txt_no_caja']").val(rowData.no_caja);
   $("[id$='cmb_estado']").findByOptionTextMG(rowData.estado); 
   

}


//-------------------------------------------------------------------------------------------------

function obtenerCajas(){

    $('#tabla_cajas').datagrid('loadData',{total:0 , rows:[] });  
    $('#tabla_cajas').datagrid({url:'../../Controladores/frm_controlador_operacion_cajas.aspx',queryParams: {
         accion: 'obtenerCajas'
     }, pageNumber: 1});
     


}


//-------------------------------------------------------------------------------------------------

//  metodos auxiliares
    function bloquearControles() {
        $("#datos_registro :input").attr('disabled', true);   
    }
    
   function desbloquearControles() {
        $("#datos_registro :input").attr('disabled', false);
       
    }
    
    
    function limpiarControles() {
      $('#datos_registro :text').val('');
      
    }

    
    function cerrarPanel(){
      $(panel_tabla).panel('collapse');
    }

    function abrirPanel(){
      $(panel_tabla).panel('expand');
    }   
    
    
    function desSeleccionarCombos(){   
      $("[id$='cmb_estado'] option[value=-1]").attr("selected",true);  
  }

      
    



//funciones de los botones
//-------------------------------------------------------------------------------------------------
    function nuevoElemento() {
        ext_ocultarBotonesOperacion();
        ext_visualizarBotonesGuardarCancelar();
        desbloquearControles();
        limpiarControles();
        cerrarPanel();
        desSeleccionarCombos();
        operacion = "nuevo";
       
     
    }
    
    
  //---
      function editarElemento() {
       var renglon;
        
        renglon=$('#tabla_cajas').datagrid('getSelected');
        if(renglon==null){
           $.messager.alert('Japami','Tienes que seleccionar un elemento','info');
           return;
        }
            
        ext_ocultarBotonesOperacion();
        ext_visualizarBotonesGuardarCancelar();
        desbloquearControles();
        cerrarPanel();
        operacion = "editar";
        

    }  
    
    //--
    
    function cancelarRegistro() {
        ext_ocultarBotonesGuardarCancelar();
        ext_visualizarBotonesOperacion();
        desSeleccionarCombos();
        bloquearControles();
        limpiarControles();
        abrirPanel();
        operacion = "ninguno";
        id_catalogo_caja=""; 

    }
    
   //-----
   
    function guardarRegistro() {
    
     if (valoresRequeridosGeneral("datos_registro")==false ){
          $.messager.alert('Japami','Se requieren algunos valores','info');
          return;    
      }
        
      numero_caja=$("[id$='txt_no_caja']").val();
      
      if(numero_caja==0){
         $.messager.alert('Japami','El numero de caja no puede ser cero','info');
         return;
      }   
      estado =$("[id$='cmb_estado'] option:selected").text();
    
    
 $('#ventana_mensaje').window('open');
             
            $.ajax({
            type: "POST",
            url: "../../Controladores/frm_controlador_operacion_cajas.aspx/guardarCaja",
            data: "{'id_catalogo_caja':'" + id_catalogo_caja + "','numero_caja':'" + numero_caja + "','estado':'" + estado + "','operacion':'" + operacion + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                 $('#ventana_mensaje').window('close');
                var respuesta = eval("(" + response.d + ")");
                if (respuesta.mensaje == "bien") {            
                    procesoExitoso(respuesta);       
                }   
                else {
                  $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');   
                }

            },
            error: function(result) {
                $('#ventana_mensaje').window('close');
                $.messager.alert('Japami', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');

            }

        });
        
        
    
    }
    
    
    function procesoExitoso(respuesta){
    
       $.messager.alert('Japami','Proceso Terminado','info',function(){
          cancelarRegistro();
          obtenerCajas();
          
       });      
    
    }
    
    
    
    
    //-------------------------------------------------------------------------------------------------
    
    
      //usage $("[id$='Cmb_Consultorio']").findByOptionTextMG(renglon_datos.nombre_consultorio);
 
jQuery.fn.findByOptionTextMG = function (data) {
     $(this).find("option").each(function(){
            if ($(this).text() == data)
            $(this).attr("selected","selected");
      });
};
 
jQuery.fn.findByOptionValueMG = function (data) {
     $(this).find("option").each(function(){
            if ($(this).val() == data)
            $(this).attr("selected","selected");
      });
};   
    
    


