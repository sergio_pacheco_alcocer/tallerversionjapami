﻿
var datos={};
 var strDatosAnterior;
 var strDatosNuevos;

$(function(){

  
  
  
});

//------------------------------------------------------------

function agregarDatos(nombre_control,valor_asignar,arreglo_datos){

    if(arreglo_datos[nombre_control] !== undefined){
              if (!arreglo_datos[nombre_control].push){
                  arreglo_datos[nombre_control]=[arreglo_datos[nombre_control]];          
              }
              arreglo_datos[nombre_control].push(valor_asignar || '');
            }else {
                arreglo_datos[nombre_control]=valor_asignar || '';
            }
            

}



function empaquetarDatos(panel,datos_empaquetados){
    var atributo_nombre,atributo_valor,atributo_clave;
    var valor_campo;
    var listaCampos,campo;
    
    var tag_control;
    var tipo_control;
    
    listaCampos=$('#' + panel + ' :input');
 
    for (var i = 0; i < listaCampos.length; i++) { //inicio for 
   
         campo = listaCampos[i];
    
         atributo_nombre=$.trim($(campo).attr('nombre'));
         atributo_valor=$.trim($(campo).attr('valor'));
         atributo_clave=$.trim($(campo).attr('clave'));
         valor_campo=$.trim($(campo).val());  
          
         tag_control= $(campo).get(0).tagName;
         tipo_control=$(campo).attr('type');
        
          if(tag_control=='INPUT'){
             if(tipo_control=='text'){
                
                 // si este campo contiene datos 
                  if(atributo_clave.length>0){
                    agregarDatos(atributo_clave,atributo_valor,datos_empaquetados);
                    agregarDatos(atributo_nombre,valor_campo,datos_empaquetados);      
                   }
                   else{
                       agregarDatos(atributo_nombre,valor_campo,datos_empaquetados);      
                      }
                        
             }
          }
          
          if(tag_control=='SELECT'){
              agregarDatos(atributo_clave,valor_campo,datos_empaquetados);
              agregarDatos(atributo_nombre,$.trim($(campo).find("option:selected").text()),datos_empaquetados);
          }
          
                     
        
         

   }


}



//-----------------------------------------------------------------------------------------------

function guardarDatos(panel){
 var strPanel; 
 
 strPanel=panel;
  
     if (valoresRequeridosGeneral(panel)==false ){
          $.messager.alert('Japami','Se requieren algunos valores','info');
          return;    
      }
          
      datos={};
      empaquetarDatos(panel,datos);
      strDatosNuevos=toJSON(datos);
      
      if(strPanel=="datos_generales"){      
       strDatosAnterior=toJSON(objDatosGenerales[0]);
      }
      
      if(strPanel=="datos_medidor_agua"){
        strDatosAnterior=toJSON(objDatosMedidores[0]);
      }
      
      if(strPanel=="datos_domicilio_propietario"){
        strDatosAnterior=toJSON(objDatosDomicilio[0]);
      }
      
     
       $('#ventana_mensaje').window('open');
   
           $.ajax({
            type: "POST",
            url: "../../Controladores/frm_controlador_editar_cuenta_usuario.aspx/guardarDatos",
            data: "{'strDatosNuevos':'" + strDatosNuevos + "','strDatosViejos':'" + strDatosAnterior + "','panel':'" + strPanel + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                $('#ventana_mensaje').window('close'); 
                var respuesta = eval("(" + response.d + ")");
                if (respuesta.mensaje == "bien") {
                   procesoExitoso(respuesta,panel);
                                              
                }
                else { 
                    $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');

                }

            },
            error: function(result) {
                $('#ventana_mensaje').window('close');
                $.messager.alert('Japami', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');

            }

        });
        
   
   
   
      
      
      
     

}


function procesoExitoso(respuesta,panel){
  
  $.messager.alert('Japami','Proceso Terminado','info',function(){
      
      
     
      
      if(panel=="datos_generales"){      
       objDatosGenerales= json_parse( "[" + respuesta.dato1  + "]");
       cargarValoresDatosGenerales();
      }
      
      if(panel=="datos_medidor_agua"){
         objDatosMedidores= json_parse( "[" + respuesta.dato1 + "]" );
         cargarValoresDatosMedidores();
      }
      
       if(panel=="datos_domicilio_propietario"){
        objDatosDomicilio= json_parse( "[" + respuesta.dato1 + "]" );
        cargarValoresDomicilio();
      }
      
      
  });
  
  
}


//-------------------------------------------------------------------------------------------------
