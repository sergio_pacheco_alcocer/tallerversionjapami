﻿
var objDatosGenerales;
var objDatosMedidores;
var objDatosDomicilio;


$(function(){


 // inicializamos fechas
$("[id$='txt_fecha_instalacion']").datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'dd/mm/yy'
});

objDatosGenerales= json_parse( $("[id$='txt_datos_generales']").val() );
objDatosMedidores= json_parse( $("[id$='txt_datos_medidores']").val() );
objDatosDomicilio= json_parse( $("[id$='txt_datos_domicilio_propietario']").val() );
cargarValoresDatosGenerales();
cargarValoresDatosMedidores();
cargarValoresDomicilio();

});


//-------------------------------------------------------------



function cargarValoresDomicilio(){

  if(objDatosDomicilio.length>0){
     
       $("[id$='txt_razon_social']").val(objDatosDomicilio[0].razon_social);
       $("[id$='txt_razon_social']").attr('clave','');
       $("[id$='txt_razon_social']").attr('nombre','razon_social');
       $("[id$='txt_razon_social']").attr('valor','');
       
       
       $("[id$='txt_nombre_propietario']").val(objDatosDomicilio[0].nombre);
       $("[id$='txt_nombre_propietario']").attr('clave','');
       $("[id$='txt_nombre_propietario']").attr('nombre','nombre');
       $("[id$='txt_nombre_propietario']").attr('valor','');
       
       $("[id$='txt_apellido_paterno_propietario']").val(objDatosDomicilio[0].apellido_paterno);
       $("[id$='txt_apellido_paterno_propietario']").attr('clave','');
       $("[id$='txt_apellido_paterno_propietario']").attr('nombre','apellido_paterno');
       $("[id$='txt_apellido_paterno_propietario']").attr('valor','');
       
       
       
       $("[id$='txt_apellido_materno_propietario']").val(objDatosDomicilio[0].apellido_materno);
       $("[id$='txt_apellido_materno_propietario']").attr('clave','');
       $("[id$='txt_apellido_materno_propietario']").attr('nombre','apellido_materno');
       $("[id$='txt_apellido_materno_propietario']").attr('valor','');
       
       
       $("[id$='txt_colonia_propietario']").val(objDatosDomicilio[0].colonia_propietario);
       $("[id$='txt_colonia_propietario']").attr('clave','colonia_id');
       $("[id$='txt_colonia_propietario']").attr('nombre','colonia_propietario');
       $("[id$='txt_colonia_propietario']").attr('valor',objDatosDomicilio[0].colonia_id);
       
       
       $("[id$='txt_calle_propietario']").val(objDatosDomicilio[0].calle_propietario);
       $("[id$='txt_calle_propietario']").attr('clave','calle_id');
       $("[id$='txt_calle_propietario']").attr('nombre','calle_propietario');
       $("[id$='txt_calle_propietario']").attr('valor',objDatosDomicilio[0].calle_id);
       
       
       $("[id$='txt_numero_exterior_propietario']").val(objDatosDomicilio[0].no_exterior_propietario);
       $("[id$='txt_numero_exterior_propietario']").attr('clave','');
       $("[id$='txt_numero_exterior_propietario']").attr('nombre','no_exterior_propietario');
       $("[id$='txt_numero_exterior_propietario']").attr('valor','');
       
       
       $("[id$='txt_numero_interior_propietario']").val(objDatosDomicilio[0].no_interior_propietario);
       $("[id$='txt_numero_interior_propietario']").attr('clave','');
       $("[id$='txt_numero_interior_propietario']").attr('nombre','no_interior_propietario');
       $("[id$='txt_numero_interior_propietario']").attr('valor','');
       
       
       $("[id$='txt_numero_telefono']").val(objDatosDomicilio[0].telefono_casa_propietario);
       $("[id$='txt_numero_telefono']").attr('clave','');
       $("[id$='txt_numero_telefono']").attr('nombre','telefono_casa_propietario');
       $("[id$='txt_numero_telefono']").attr('valor','');
       
       $("[id$='txt_rfc_propietario']").val(objDatosDomicilio[0].rfc);
       $("[id$='txt_rfc_propietario']").attr('clave','');
       $("[id$='txt_rfc_propietario']").attr('nombre','rfc');
       $("[id$='txt_rfc_propietario']").attr('valor','');
       
       
      
  }else{
    $('#btn_guardar_datos_domicilio').linkbutton({disabled:true});
  }
  
}


//------------------------------------------------------------

function cargarValoresDatosMedidores(){
   
   if(objDatosMedidores.length>0){
      
       $("[id$='txt_fecha_instalacion']").val(objDatosMedidores[0].fecha_instalacion);
       $("[id$='txt_fecha_instalacion']").attr('clave','');
       $("[id$='txt_fecha_instalacion']").attr('nombre','fecha_instalacion');
       $("[id$='txt_fecha_instalacion']").attr('valor','');
       
       $("[id$='txt_no_medidor']").val(objDatosMedidores[0].no_medidor);
       $("[id$='txt_no_medidor']").attr('clave','');
       $("[id$='txt_no_medidor']").attr('nombre','no_medidor');
       $("[id$='txt_no_medidor']").attr('valor','');
       
       $("[id$='txt_limite_medidor']").val(objDatosMedidores[0].limite_medidor);
       $("[id$='txt_limite_medidor']").attr('clave','');
       $("[id$='txt_limite_medidor']").attr('nombre','limite_medidor');
       $("[id$='txt_limite_medidor']").attr('valor','');
       
  
       $("[id$='txt_diametro']").val(objDatosMedidores[0].diametro);
       $("[id$='txt_diametro']").attr('clave','');
       $("[id$='txt_diametro']").attr('nombre','diametro');
       $("[id$='txt_diametro']").attr('valor','');
       
            
       $("[id$='cmb_marca']").attr('clave','marca_id');
       $("[id$='cmb_marca']").attr('nombre','marca');
       $("[id$='cmb_marca']").attr('valor',objDatosMedidores[0].marca_id);
       
       $("[id$='cmb_marca']  option[value=" + objDatosMedidores[0].marca_id + "] ").attr("selected",true);  
   
   }
   else{
        $('#btn_guardar_datos_medidor').linkbutton({disabled:true});
   }
  
}


//------------------------------------------------------------

function cargarValoresDatosGenerales(){
  
  
  if (objDatosGenerales.length>0){
  
  $("[id$='txt_colonia']").val(objDatosGenerales[0].colonia);
  $("[id$='txt_colonia']").attr('clave','colonia_id');
  $("[id$='txt_colonia']").attr('nombre','colonia');
  $("[id$='txt_colonia']").attr('valor',objDatosGenerales[0].colonia_id);
  
  
  $("[id$='txt_calle']").val(objDatosGenerales[0].calle);
  $("[id$='txt_calle']").attr('clave','calle_id');
  $("[id$='txt_calle']").attr('nombre','calle');
  $("[id$='txt_calle']").attr('valor',objDatosGenerales[0].calle_id);
  
  $("[id$='txt_no_exterior']").val(objDatosGenerales[0].numero_exterior);
  $("[id$='txt_no_exterior']").attr('clave','');
  $("[id$='txt_no_exterior']").attr('nombre','numero_exterior');
  $("[id$='txt_no_exterior']").attr('valor','');
  
  $("[id$='txt_no_interior']").val(objDatosGenerales[0].numero_interior);
  $("[id$='txt_no_interior']").attr('clave','');
  $("[id$='txt_no_interior']").attr('nombre','numero_interior');
  $("[id$='txt_no_interior']").attr('valor','');
  
  
  $("[id$='txt_calle_referencia1']").val(objDatosGenerales[0].calle_refencia1);
  $("[id$='txt_calle_referencia1']").attr('clave','calle_referencia1_id');
  $("[id$='txt_calle_referencia1']").attr('nombre','calle_refencia1');
  $("[id$='txt_calle_referencia1']").attr('valor',objDatosGenerales[0].calle_referencia1_id); 
   
  $("[id$='txt_calle_referencia2']").val(objDatosGenerales[0].calle_refencia2);
  $("[id$='txt_calle_referencia2']").attr('clave','calle_referencia2_id');
  $("[id$='txt_calle_referencia2']").attr('nombre','calle_refencia2');
  $("[id$='txt_calle_referencia2']").attr('valor',objDatosGenerales[0].calle_referencia2_id);

  
  //$("[id$='cmb_region']").val(objDatosGenerales[0].no_region);
  $("[id$='cmb_region']").attr('clave','region_id');
  $("[id$='cmb_region']").attr('nombre','no_region');
  $("[id$='cmb_region']").attr('valor',objDatosGenerales[0].region_id);
  
  $("[id$='cmb_region']  option[value=" + objDatosGenerales[0].region_id + "] ").attr("selected",true);
  
  $("[id$='txt_sector']").val(objDatosGenerales[0].no_sector);
  $("[id$='txt_sector']").attr('clave','sector_id');
  $("[id$='txt_sector']").attr('nombre','no_sector');
  $("[id$='txt_sector']").attr('valor',objDatosGenerales[0].sector_id);
  
  
  $("[id$='txt_no_reparto']").val(objDatosGenerales[0].no_reparto);
  $("[id$='txt_no_reparto']").attr('clave','');
  $("[id$='txt_no_reparto']").attr('nombre','no_reparto');
  $("[id$='txt_no_reparto']").attr('valor','');
 
 
  $("[id$='txt_no_manzana']").val(objDatosGenerales[0].nombre_manzana);
  $("[id$='txt_no_manzana']").attr('clave','manzana_id');
  $("[id$='txt_no_manzana']").attr('nombre','nombre_manzana');
  $("[id$='txt_no_manzana']").attr('valor',objDatosGenerales[0].manzana_id);
  
  
  //$("[id$='cmb_zona_economica']").val(objDatosGenerales[0].numero_zona);
  $("[id$='cmb_zona_economica']").attr('clave','zona_id');
  $("[id$='cmb_zona_economica']").attr('nombre','numero_zona');
  $("[id$='cmb_zona_economica']").attr('valor',objDatosGenerales[0].zona_id);

 $("[id$='cmb_zona_economica']  option[value=" + objDatosGenerales[0].zona_id + "] ").attr("selected",true);
 
 
 
  $("[id$='cmb_giros']").attr('clave','giro_id');
  $("[id$='cmb_giros']").attr('nombre','nombre_giro');
  $("[id$='cmb_giros']").attr('valor',objDatosGenerales[0].giro_id);

  $("[id$='cmb_giros']  option[value=" + objDatosGenerales[0].giro_id + "] ").attr("selected",true);
 
 
 
  $("[id$='txt_actividad']").val(objDatosGenerales[0].actividad);
  $("[id$='txt_actividad']").attr('clave','giro_actividad_id');
  $("[id$='txt_actividad']").attr('nombre','actividad');
  $("[id$='txt_actividad']").attr('valor',objDatosGenerales[0].giro_actividad_id);
  
 
 
  $("[id$='cmb_tarifa']").attr('clave','tarifa_id');
  $("[id$='cmb_tarifa']").attr('nombre','nombre_tarifa');
  $("[id$='cmb_tarifa']").attr('valor',objDatosGenerales[0].tarifa_id);

 $("[id$='cmb_tarifa']  option[value=" + objDatosGenerales[0].tarifa_id + "] ").attr("selected",true);
 
 
 
  $("[id$='cmb_vivienda']").attr('clave','vivienda_id');
  $("[id$='cmb_vivienda']").attr('nombre','vivienda');
  $("[id$='cmb_vivienda']").attr('valor',objDatosGenerales[0].vivienda_id);

 $("[id$='cmb_vivienda']  option[value=" + objDatosGenerales[0].vivienda_id+ "] ").attr("selected",true);
 
 
  $("[id$='cmb_servicio']").attr('clave','grupo_concepto_id');
  $("[id$='cmb_servicio']").attr('nombre','servicio');
  $("[id$='cmb_servicio']").attr('valor',objDatosGenerales[0].grupo_concepto_id);

 $("[id$='cmb_servicio']  option[value=" + objDatosGenerales[0].grupo_concepto_id + "] ").attr("selected",true);
 
 

  }
  else{
      $('#btn_guardar_datos_generales').linkbutton({disabled:true});
  }

}

//-----------------------------------------------------------------

function ventanaColonias(colonia_a_buscar){

   bandera_colonia_a_buscar=colonia_a_buscar;
   $("[id$='txt_colonia_buscar']").val('');
   $('#ventana_busqueda_colonias').window('open');
 
}

//----------------------------------------------------------------
function cerrarVentanas(){
   $('#ventana_busqueda_colonias').window('close');
   $('#ventana_calles').window('close');
   $('#ventana_sectores').window('close');
   $('#ventana_manzana').window('close');
   $('#ventana_actividades').window('close');
  
}


function ventanaCalles(calle_a_escribir,vista_tab){
 var  col_id;  
 var  nombreColonia;
    
    bandera_calle_a_buscar=calle_a_escribir;  
    
    
    if(vista_tab=="datos_generales_calle"){
        col_id=$("[id$='txt_colonia']").attr('valor');
        nombreColonia=$.trim( $("[id$='txt_colonia']").val());    
    }
    
    if(vista_tab=="datos_domicilios_calle"){
        col_id=$("[id$='txt_colonia_propietario']").attr('valor');
        nombreColonia=$.trim( $("[id$='txt_colonia_propietario']").val());
    }
    
    
      if(nombreColonia.length==0){
             $.messager.alert('Japami','Tienes que seleccionar una colonia Primero');
           return;
       }   
          
      if(col_id.length==0){
           $.messager.alert('Japami','Tienes que seleccionar una colonia Primero');
           return;
        }
    
    
    $("[id$='txt_calle_buscar']").val('');
     autoCompletarCalles(col_id); 
     $('#ventana_calles').window('open');
     

}

//----------------------------------------------------------------

function ventanaSectores(){
var id_region;

 id_region=$("[id$='cmb_region']").val();
 
 if(id_region==-1){
    $.messager.alert('Japmai','Selecciona una región primero','info');
    return; 
 }
 
 $("[id$='txt_sectores_buscar']").val('');
 autoCompletarSectores(id_region);
 $('#ventana_sectores').window('open');  

}

//----------------------------------------------------------------

function ventanaMananza(){
 var sector_id;
 var numero_sector;
 
 sector_id= $.trim( $("[id$='txt_sector']").attr('valor'));
 numero_sector=$.trim( $("[id$='txt_sector']").val());
 
 if (numero_sector.length==0){
    $.messager.alert('Japmai','Selecciona una región primero','info');
    return; 
 }
 
 $("[id$='txt_buscar_manzana']").val('');
 autoCompletarManzana(sector_id);
 $('#ventana_manzana').window('open');  

}

//----------------------------------------------------------------

function ventanaActividades()
{
  var giro_id;
  var nombre_giro;
  
 giro_id= $("[id$='cmb_giros']").val();

 if(giro_id==-1){
    $.messager.alert('Japmai','Selecciona un Giro Primero','info');
    return; 
 }
  $("[id$='txt_actividad_buscar']").val('');
 autoCompletarActividad(giro_id);
 $('#ventana_actividades').window('open');

}

