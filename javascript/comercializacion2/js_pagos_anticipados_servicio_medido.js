﻿
var sm_cantidad_seleccionado;
var sm_total_promedio;
var sm_clave_servicio;

$(function(){
   
    crearTablaLecturas();
    crearTablaConsumosVivienda();
    $('#tabla_promedio_catalogo').datagrid({ url: '../../Controladores/frm_controlador_pagos.aspx', queryParams: { accion: 'obtenerConsumoVivienda' }, pageNumber: 1 });

});

//-------------------------------------------------------------

function crearTablaConsumosVivienda(){

  $('#tabla_promedio_catalogo').datagrid({  // tabla inicio
            title: 'Lecturas',
            width: 435,
            height: 250,
            columns: [[         
        { field: 'numero_persona', title: 'No.Personas', width: 140, sortable: true },
        { field: 'consumo_promedio', title: 'Consumo', width: 140, sortable: true }
            
    ]],     
             onClickRow: function(rowIndex, rowData) {              
              obtenerPromedio();
              
            },
            onLoadSuccess: function(data) {
            
            }, 
            pageSize: [50,100],
            pagination: false,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect:false,
            showFooter: false,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap: false

        });  // tabla final

}

//-------------------------------------------------------------

function crearTablaLecturas(){

  $('#tabla_historico_lecturas').datagrid({  // tabla inicio
            title: 'Lecturas',
            width: 435,
            height: 250,
            columns: [[         
        { field: 'fecha_lectura', title: 'F. lectura', width: 140, sortable: true },
        { field: 'consumo_facturacion', title: 'Consumo', width: 140, sortable: true }
            
    ]],     
             onClickRow: function(rowIndex, rowData) {              
              obtenerPromedio();
              
            },
            onLoadSuccess: function(data) {
            
            }, 
            pageSize: [50,100],
            pagination: false,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect:false,
            showFooter: false,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap: false

        });  // tabla final

}

//-------------------------------------------------------------------
function cerrarVentanaSM(){
$('#ventana_promedios').window('close');
$('#ventana_promedios_catalogo').window('close');
}

//--------------------------------------------------------------------

function abrirVentanaPromedio(){
   
   $('#ventana_promedios').window('open');
}

//--------------------------------------------------------------------

function abrirVentanaPromedioCatalogo(){
   $('#ventana_promedios_catalogo').window('open');

}

//--------------------------------------------------------------------

function obtenerPromedio(){
 var sm_objLecturas;
 var sm_promedio=0; 
 

  sm_objLecturas=$('#tabla_historico_lecturas').datagrid('getSelections');
  sm_cantidad_seleccionado=sm_objLecturas.length; 
  $("[id$='txt_elementos_sel_p']").val(sm_cantidad_seleccionado); 
  
  for(var i=0;i<sm_objLecturas.length;i++){
     sm_promedio= sm_promedio + parseFloat(sm_objLecturas[i].consumo_facturacion);
  }
  
  if (sm_cantidad_seleccionado>0 ){
     sm_total_promedio=  decimal( sm_promedio / sm_cantidad_seleccionado,2); 
     $("[id$='txt_promedio_p']").val(sm_total_promedio); 
   }
   else{
     $("[id$='txt_promedio_p']").val(sm_total_promedio);
   }
}



//-------------------------------------------------------------------------------
function aceptarPromedioPersonalizado(){
 
 sm_cantidad_seleccionado= parseInt($("[id$='txt_elementos_sel_p']").val());
 sm_total_promedio=parseFloat($("[id$='txt_promedio_p']").val());
 
 
 
 if(isNaN(sm_cantidad_seleccionado) ||  sm_cantidad_seleccionado<3 || sm_cantidad_seleccionado=='undefined' ){
    $.messager.alert('Japami','Tienes que seleccionar varios consumos','info');
    return;
 }
 
 if(isNaN(sm_total_promedio) || sm_total_promedio==0 || sm_total_promedio=='undefined'){
   $.messager.alert('Japami','Tienes que haber un promedio','info');
    return;
 }
 
 sm_clave_servicio= parseInt( $("[id$='txt_clave_servicio']").val()); 
 sm_clave_servicio=validarCantidad(sm_clave_servicio);
 
 
 if(sm_clave_servicio==0){
   $.messager.alert('Japami','El usuario no tiene asignado un tipo de servicio','info');
    return;
 }
 
  
  obtenerCostoConsumoParaCalculo("rango_fechas");
  

}


function aceptarPromedioCatalogo(){
 var sm_renglon;
 
 sm_renglon=$('#tabla_promedio_catalogo').datagrid('getSelected');
 
 if(sm_renglon==null){
      $.messager.alert('Japami','Selecciona un elemento','info');
      return;
 }
 
 sm_total_promedio=parseFloat(sm_renglon.consumo_promedio);
 sm_cantidad_seleccionado= validarCantidad(sm_cantidad_seleccionado);
 
 sm_clave_servicio= parseInt( $("[id$='txt_clave_servicio']").val()); 
 sm_clave_servicio=validarCantidad(sm_clave_servicio);
 
 
 if(sm_clave_servicio==0){
   $.messager.alert('Japami','El usuario no tiene asignado un tipo de servicio','info');
    return;
 }
 
  
  obtenerCostoConsumoParaCalculo("catalogo");
 
 

}

//----------------------------------------------------------------

function validarCantidad(cantidad){
   
   if(isNaN(cantidad) || cantidad=='undefined'){
     cantidad=0;
   } 
  return cantidad;
  
}

//----------------------------------------------------------------

function calcularImporteServicioMedido(consumo_promedio,clave_servicio,costo_cosumo,ban){
  var sm_cuota_base;
  var sm_rango_final;
  var sm_costo_cosumo;
  var sm_precio_metro_cubico;
  var sm_porcentaje_saneamiento;
  var sm_procentaje_drenaje;
  
  var sm_importe_l=0;
  var sm_total_importe_l=0;
  
  sm_cuota_base=parseFloat($("[id$='txt_cuota_base_s']").val());
  sm_cuota_base=validarCantidad(sm_cuota_base);
  sm_rango_final=parseFloat($("[id$='txt_rango_final_s']").val());
  sm_rango_final=validarCantidad(sm_rango_final);
  sm_costo_cosumo=costo_cosumo
  sm_costo_cosumo=validarCantidad(sm_costo_cosumo);
  sm_precio_metro_cubico=parseFloat($("[id$='txt_precio_cubico_s']").val());
  sm_precio_metro_cubico=validarCantidad(sm_precio_metro_cubico);
  sm_porcentaje_saneamiento=parseFloat($("[id$='txt_porcentaje_saneamiento']").val());
  sm_porcentaje_saneamiento=validarCantidad(sm_porcentaje_saneamiento);
  sm_procentaje_drenaje=parseFloat($("[id$='txt_porcentaje_drenaje']").val());
  sm_procentaje_drenaje=validarCantidad(sm_procentaje_drenaje);
  
  
  
  if(consumo_promedio >=0 && consumo_promedio<= sm_rango_final ){
       sm_importe_l= decimal( sm_cuota_base + sm_costo_cosumo,2);
    }
      
   if(consumo_promedio> sm_rango_final){
         sm_importe_l= decimal( sm_cuota_base + (sm_precio_metro_cubico * consumo_promedio),2);
     }
  
  
  // habla de servicio de agua y drenaje
  if (clave_servicio==1){
    sm_total_importe_l=  decimal( sm_importe_l + ((sm_importe_l * sm_procentaje_drenaje)/100) + ((sm_importe_l * sm_porcentaje_saneamiento) /100),2);        
    
  }
  
  // habla de solo agua
  if (clave_servicio==2){
    sm_total_importe_l=sm_importe_l;
  }
  
  
  if(ban=="rango_fechas"){
     $("[id$='txt_costo_consumo_sm_rango_fechas']").val(costo_cosumo);  
     $("[id$='txt_cantidad_servicio_medido_s']").val(sm_total_importe_l);
     $("[id$='txt_promedio_personalizado_s']").val(consumo_promedio);
  }
  
  if(ban=="catalogo"){
     $("[id$='txt_costo_consumo_sm_catalogo_s']").val(costo_cosumo);  
     $("[id$='txt_cantidad_servicio_medido_catalogo_s']").val(sm_total_importe_l);
     $("[id$='txt_promedio_catalogo_s']").val(consumo_promedio);
  }
  
  
  
  
   $.messager.show({
				title:'Japami',
				msg:'Importe Calculado',
				timeout:30,
				showType:'slide'
			});
  
}

//----------------------------------------------------------------


function obtenerCostoConsumoParaCalculo(tipo_bandera){



$('#ventana_mensaje').window('open');
   
           $.ajax({
            type: "POST",
            url: "../../Controladores/frm_controlador_pagos.aspx/obtenerCostoConsumo",
            data: "{'tarifa_id':'" + tarifa_id + "','promedio_consumo':'" + sm_total_promedio + "','mes_actual':'" + mes_actual + "','anos_actual':'" + ano_actual + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                $('#ventana_mensaje').window('close'); 
                var respuesta = eval("(" + response.d + ")");
                if (respuesta.mensaje == "bien") {
                    var sm_costo_consumo= parseFloat( respuesta.dato1);
                   calcularImporteServicioMedido(sm_total_promedio,sm_clave_servicio,sm_costo_consumo,tipo_bandera); 
                                              
                }
                else { 
                    $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');

                }

            },
            error: function(result) {
                $('#ventana_mensaje').window('close');
                $.messager.alert('Japami', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');

            }

        });

}