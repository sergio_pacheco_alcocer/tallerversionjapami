﻿$(function(){

  $('#ventana_busqueda').window('close'); 
  crearTablaUsuarios();

});

//---------------------------------

function crearTablaUsuarios(){


      $('#grid_usuarios').datagrid({  // tabla inicio
            title: 'Usuarios',
            width: 820,
            height: 250,
            columns: [[
        { field: 'usuario_id', title: 'usuario_id', width: 0, sortable: true },
        { field: 'no_cuenta', title: 'No. Cuenta', width: 80, sortable: true },
        { field: 'usuario', title: 'Usuario', width: 200, sortable: true },
        { field: 'calle', title: 'Calle', width: 150, sortable: true },
        { field: 'colonia', title: 'Colonia', width: 150, sortable: true },
        { field: 'numero_exterior', title: 'No. Ext', width: 80, sortable: true },
        { field: 'numero_interior', title: 'No. Int', width: 80, sortable: true },
        { field: 'rfc', title: 'RFC', width: 80, sortable: true },
        { field: 'estado', title: 'Estado', width: 100, sortable: true },
        { field: 'ciudad', title: 'Municipio', width: 100, sortable: true }
        

    ]],     
             onClickRow: function(rowIndex, rowData) {
                  
            }, 
            pageSize: 50,
            pagination: true,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            showFooter: false,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap: false

        });  // tabla final


   $('#grid_usuarios').datagrid('hideColumn','usuario_id');

}

//------------------------------

function cerrarVentana(){
$('#ventana_busqueda').window('close');
}

//--------------------------------------

function abrirVentana(){
 $('#ventana_busqueda').window('open');
}


//----------------------------------------

function buscarUsuario(){

 var no_cuenta;
   var nombre_usuario;
   var renglones;
   
   no_cuenta=$.trim( $("#txt_no_cuenta").val());
   nombre_usuario=$.trim( $("#txt_usuario").val());
   
   if ($('#opt_no_cuenta').attr('checked')){
      
      if (no_cuenta.length==0){
            $.messager.alert('Japami','Introduce un No. de cuenta');
            return ; 
      }
   }else {no_cuenta='';}
   
   if($('#opt_usuario').attr('checked')){
       if(nombre_usuario.length==0){
           $.messager.alert('Japami','Introduce un Nombre de usuario');
            return ; 
       }
      
   }else{nombre_usuario='';}
   

   $('#grid_usuarios').datagrid('loadData',{total:0, rows:[]});
   $('#grid_usuarios').datagrid({ url: '../../Controladores/frm_controlador_pagos.aspx', queryParams: { accion: 'obtenerUsuarios', nocuenta: no_cuenta, nombre: nombre_usuario }, pageNumber: 1 });




}

//---------------------------------------
function aceptarUsuario(){

 var renglon;
 
 renglon=$('#grid_usuarios').datagrid('getSelected');
 
 if(renglon==null){
   $.messager.alert('Japami', 'Tienes que seleccionar un elemento');
   return; 
 }
 asignarValores(renglon);
  
     $.messager.show({
				title:'Japami',
				msg:'Elemento Agregado',
				timeout:5,
				showType:'slide'
			});

}



