﻿
 var objConcepto;
 var no_diverso;

$(function(){


  $('#ventana_mensaje').window('close');    
  bloquearControles();
  
   $('#btn_imprimir').linkbutton({disabled:true});
   auto_completado_conceptos();
  
 
  // evento de los controles de radio  
  $('#pago_usuario_registrado').click(function(){
       $('#btn_abrir_busqueda_usuario').linkbutton({disabled:false})
       limpiarControles();
       bloquearControles();
  });

   $('#pago_usuario_no_registrado').click(function(){
     $('#btn_abrir_busqueda_usuario').linkbutton({disabled:true})
     desbloquearControles();
     limpiarControles();
     
     $("[id$='txt_no_cuenta_agregar']").attr('disabled',true);
     $("[id$='txt_no_cuenta_agregar']").val('0');
  }); 
   
   
   crearTablaConceptos();
   
   
   $("[id$='txt_concepto']").keydown(function(e){
              
        if(e.which == 8 || e.which==46) {
            $("[id$='txt_importe']").val('');
            objConcepto==null;
        }
   });
   
   
   // update footer rows with new data inicializa valores en el grid
$('#conceptos_diversos').datagrid('reloadFooter',[
	{concepto: 'Subtotal', importe: 0},
	{concepto: 'IVA', importe: 0},
	{concepto: 'Total', importe: 0}
]);


// update footer row values and then refresh
//var rows = $('#conceptos_diversos').datagrid('getFooterRows');
//rows[0]['concepto'] = 'new name';
//rows[0]['importe'] = 60000;
//$('#conceptos_diversos').datagrid('reloadFooter');

   
   
});

//------------------------------------------------------------------------

function crearTablaConceptos(){

        $('#conceptos_diversos').datagrid({  // tabla inicio
            title: 'Conceptos',
            width: 800,
            height: 250,
            columns: [[
        { field: 'concepto_id', title: 'usuario_id', width: 0, sortable: true },
        { field: 'concepto', title: 'Concepto', width: 550, sortable: true },
        { field: 'importe', title: 'Importe', width: 150, sortable: true }

    ]],     
             onClickRow: function(rowIndex, rowData) {
                  
            }, 
            pageSize: 50,
            pagination: false,
            rownumbers: true,
            remoteSort: false,
            fitColumns: false,
            singleSelect: true,
            showFooter: true,
            striped: true,
            fit: false,
            loadMsg: 'cargando...',
            nowrap: false,
            toolbar: [{
            id: 'btneliminar',
            text: 'Eliminar',
            iconCls: 'icon-cancel',
            handler: function() {
                eliminarConcepto();
            }
        

}]

        });  // tabla final


 
  $('#conceptos_diversos').datagrid('hideColumn','concepto_id');

  
  
}

//--------------------------------------------------------------------------



function format(item) {
    return item.concepto + ' ['+ item.importe + ']' ;
}

function auto_completado_conceptos() { 

//inciamos el auto completado
    $("[id$='txt_concepto']").autocomplete("../../Controladores/frm_controlador_pagos.aspx", {
        extraParams: { accion: 'autoCompletarConceptos' },
        dataType: "json",
        parse: function(data) {
            return $.map(data, function(row) {
                return {
                    data: row,
                    value: row.concepto_id,
                    result: row.concepto 

                }
            });
        },
        formatItem: function(item) {
            return format(item);
        }
    }).result(function(e, item) {
          objConcepto=item; 
          $("[id$='txt_importe']").val(objConcepto.importe);
      });

}






//-------------------------------------------------------------------------
function limpiarControles(){
   $('#datos :input').val('');
   $('#conceptos_diversos').datagrid('loadData',{ total:0, rows:[]});
   
 // update footer rows with new data inicializa valores en el grid
$('#conceptos_diversos').datagrid('reloadFooter',[
	{concepto: 'Subtotal', importe: 0},
	{concepto: 'IVA', importe: 0},
	{concepto: 'Total', importe: 0}
]);
   
}

function bloquearControles(){
  $("#datos :input").attr('disabled', true);
}

function desbloquearControles(){
 $('#datos :input').attr('disabled', false);
}

//-----------------------------------------------------------------------

function asignarValores(renglon){
      
  
   $("[id$='txt_id_usuario']").val(renglon.usuario_id); 
   $("[id$='txt_no_cuenta_agregar']").val(renglon.no_cuenta);
   $("[id$='txt_nombre']").val(renglon.usuario);
   $("[id$='txt_calle']").val(renglon.calle);
   $("[id$='txt_colonia']").val(renglon.colonia);
   $("[id$='txt_numero']").val(renglon.numero_exterior);
   $("[id$='txt_rfc']").val(renglon.rfc);
   $("[id$='txt_ciudad']").val(renglon.ciudad);
   $("[id$='txt_estado']").val(renglon.estado);
  
}

//------------------------------------------------------

function agregarConcepto(){
 var importe;   
     
    if(objConcepto==null){
      $.messager.alert('Japmai','Tienes que seleccionar un elemento','info');
      return;
    }
    
    
    importe= parseFloat( $("[id$='txt_importe']").val()); 
    if(importe==0 ||  isNaN(importe) ){
         $.messager.alert('Japami','El importe es incorrecto');
         return;
    }
    
    
   if (buscar_en_grid("conceptos_diversos",objConcepto.tramite_servicio_id,"concepto_id")){
      $.messager.alert('Japami','El Concepto Esta Repetido','error');
      return;
   }  
      
      
     $('#conceptos_diversos').datagrid('appendRow',
     {
         concepto_id: objConcepto.concepto_id,
         concepto:objConcepto.concepto,
         importe:importe
     });
    
      
      var subtotal=  decimal( obtenerSubtotal(),2);
      var importe_iva= decimal( obtenerImporteIVA(subtotal),2);
      var total= subtotal +   importe_iva;
      
      // update footer row values and then refresh
      var rows = $('#conceptos_diversos').datagrid('getFooterRows');
      rows[0]['importe'] = subtotal;
      rows[1]['importe'] = importe_iva;
      rows[2]['importe'] = total;
     $('#conceptos_diversos').datagrid('reloadFooter');
      
      objConcepto=null;
      $("[id$='txt_importe']").val('');
      $("[id$='txt_concepto']").val('');
      
      
}

//------------------------------------------

function obtenerSubtotal(){
  var renglones;
  var renglon;
  var suma=0;
  
  renglones=$('#conceptos_diversos').datagrid('getRows');
  
  for(var i=0 ;i<renglones.length; i++){
       renglon=renglones[i];
       suma=suma + parseFloat( renglon.importe);
  }
  
  return suma;
}

//----------------------------------------------------

function obtenerImporteIVA(subtotal){
var iva;
var resultado;

iva= parseFloat($("[id$='txt_iva']").val());
if( isNaN(iva)){
  iva=0;
}

  if($("[id$='chk_lleva_iva']").attr('checked')){
      resultado=  parseFloat( (subtotal * iva) /100);
   }else{
      resultado=parseFloat( 0); 
   }
  
  return resultado;

}

//-----------------------------------------------------------------

function eliminarConcepto(){

  var renglon;
  var index;
  
  renglon=$('#conceptos_diversos').datagrid('getSelected');
 
 if(renglon==null){
    $.messager.alert('Japami','Tienes que seleccionar un elemento');
    return;
 }
 
 index=$('#conceptos_diversos').datagrid('getRowIndex',renglon);
 $('#conceptos_diversos').datagrid('deleteRow', index);

      var subtotal=  decimal( obtenerSubtotal(),2);
      var importe_iva= decimal( obtenerImporteIVA(subtotal),2);
      var total= subtotal +   importe_iva;
       
      // update footer row values and then refresh
      var rows = $('#conceptos_diversos').datagrid('getFooterRows');
      rows[0]['importe'] = subtotal;
      rows[1]['importe'] = importe_iva;
      rows[2]['importe'] = total;
     $('#conceptos_diversos').datagrid('reloadFooter');
      


}
//-------------------------------------------------------------------

function agregarDatos(nombre_control,valor_asignar,arreglo_datos){

    if(arreglo_datos[nombre_control] !== undefined){
              if (!arreglo_datos[nombre_control].push){
                  arreglo_datos[nombre_control]=[arreglo_datos[nombre_control]];          
              }
              arreglo_datos[nombre_control].push(valor_asignar || '');
            }else {
                arreglo_datos[nombre_control]=valor_asignar || '';
            }            

}


//-----------------------------------------------------------------

function guardarPago(){
 var datos_conceptos;
 var valor_no_cuenta;
 var dato_a_enviar;
 var dato_importe;
 
 
 datos_conceptos=$('#conceptos_diversos').datagrid('getRows');
 dato_importe=$('#conceptos_diversos').datagrid('getFooterRows');
 
 
 if (datos_conceptos.length==0){
    $.messager.alert('JAPAMI','No hay datos que pagar','info');
    return;
 }

 if($('#pago_usuario_no_registrado').attr('checked')){
     
      if(valoresRequeridosGeneral("datos")==false){
        $.messager.alert('Japami','Se requieren algunos valores','error');
        return;  
    }
    
 
 }else{
     
    valor_no_cuenta= $.trim( $("[id$='txt_no_cuenta_agregar']").val());
    if (valor_no_cuenta.length==0){
         $.messager.alert('Japami','Se requiere seleccionar una cuenta');
         return;
    }
    
 
 }
 
 desbloquearControles();
 dato_a_enviar= $('#datos :input').serializeObject();
 
 agregarDatos("iva",$("[id$='txt_iva']").val(),dato_a_enviar);
 agregarDatos("subtotal",dato_importe[0]['importe'],dato_a_enviar);
 
 if(parseFloat( dato_importe[1]['importe'])==0){
    agregarDatos("importe_iva",0,dato_a_enviar); 
    dato_a_enviar.importe_iva=0;
 }else{
    agregarDatos("importe_iva",dato_importe[1]['importe'],dato_a_enviar);
 }
 

 agregarDatos("total",dato_importe[2]['importe'],dato_a_enviar);
 
 var datos_usuario= toJSON(dato_a_enviar);
 var detalles_conceptos= toJSON(datos_conceptos);
 
 
        $('#ventana_mensaje').window('open');
   
           $.ajax({
            type: "POST",
            url: "../../Controladores/frm_controlador_pagos.aspx/elaborarPagoDiverso",
            data: "{'datos_usuario':'" + datos_usuario + "','detalles_conceptos':'" + detalles_conceptos + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                $('#ventana_mensaje').window('close'); 
                var respuesta = eval("(" + response.d + ")");
                if (respuesta.mensaje == "bien") {
                   procesoExitoso(respuesta);
                                              
                }
                else { 
                    $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');

                }

            },
            error: function(result) {
                $('#ventana_mensaje').window('close');
                $.messager.alert('Japami', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');

            }

        });
   
 
 


}


//-------------------------------------------------------------------------------------------------------

function procesoExitoso(respuesta){
   
    $.messager.alert('Japami','Proceso Terminado','info',function(){
        bloquearControles();
       $('#btn_abrir_busqueda_usuario').linkbutton({disabled:true});
       $('#btn_guardarDiverso').linkbutton({disabled:true});
       $('#btn_agregar_concepto').linkbutton({disabled:true});
       $('#btneliminar').linkbutton({disabled:true});
       $('#pago_usuario_no_registrado').attr('disabled',true); 
       $("[id$=['txt_concepto']").attr('disabled',true);
       $('#pago_usuario_registrado').attr('disabled',true);
       
       
       $('#btn_imprimir').linkbutton({disabled:false});  
       no_diverso=respuesta.dato1;
       window.open("../../Reporte/frm_controlador_reporte_pdf.aspx?nombre_reporte=formato_recibo_diverso&no_diverso="+no_diverso,"ReciboDiverso","width=1000px,height=1000px,scrollbars=YES,resizable=YES");
    });
   
}


//-----------------------------------------------------------------------------------------------------

function imprimirRecibo(){
window.open("../../Reporte/frm_controlador_reporte_pdf.aspx?nombre_reporte=formato_recibo_diverso&no_diverso="+no_diverso,"ReciboDiverso","width=1000px,height=1000px,scrollbars=YES,resizable=YES");
}


//-----------------------------------------------------------------------------------------------------

function nuevoRecibo(){
 limpiarControles();
 bloquearControles();
 
  $('#btn_abrir_busqueda_usuario').linkbutton({disabled:false});
  $('#btn_guardarDiverso').linkbutton({disabled:false});
  $('#btn_agregar_concepto').linkbutton({disabled:false});
  $('#btneliminar').linkbutton({disabled:false});
  $('#btn_imprimir').linkbutton({disabled:true});
  
  $('#pago_usuario_no_registrado').attr('disabled',false); 
  $("[id$=['txt_concepto']").attr('disabled',false);
  $('#pago_usuario_registrado').attr('disabled',false);

  $('#pago_usuario_registrado').click();    

  no_diverso=""; 

}

//------------------------------------------------------------------------------------------------------
Number.prototype.decimal = function(num) {
    pot = Math.pow(10, num);
    return parseInt(this * pot) / pot;
}

function decimal(valor,no_decimales) {
    n = eval(valor);
    return n.decimal(no_decimales);
} 


