﻿

var arrDatos;


$(function() {


//valida las caja de textos multiline
$('textarea').keypress(function(event) {
    var no = $.trim($(this).val()).length;
    var propiedad = parseInt($(this).attr('max'));

    if (no >= propiedad) { event.preventDefault(); }


});

$('.validacion').blur(function() { // inicio evento blur
    var campo = $(this);
    var tipo = $.trim(campo.attr('tipo'));
    var valores = $.trim(campo.attr('valores'));
   
   //validacion de fecha
   if (tipo == "fecha") {  //inicio
      if (esFecha(campo.val()) == false) {
        campo.val('');
    }
   } //fin
    
   //validacion de caracteres 
   if (tipo == "caracteres") { //inicio
       var respuesta;
       respuesta = obtenerCadenaPermitida($.trim(campo.val()), valores);
       campo.val(respuesta);

   } // fin
   
    //validacion numeros
    if (tipo == "numeros") {
            var respuesta;
            respuesta = obtenerCadenaNumeros($.trim(campo.val()), valores);
            campo.val(respuesta);
        }
   


   //eventos de foco
   $('.validacion').focus(function() { // inicio 
       var campo = $(this);
       $(campo).css('border-style', 'groove');
       $(campo).css('border-color', '');
   }); // fin del evento focus


   $('.easyui-numberbox').focus(function() { //inicio
       var campo = $(this);
       $(campo).css('border-style', 'groove');
       $(campo).css('border-color', '');

   }); // fin


   

}); // fin




});


// metodos para validar--------------------------------------------------------------------------------

function esFecha(cadena) {

    var fecha = new String(cadena);
    var ano = new String(fecha.substring(fecha.lastIndexOf("/") + 1, fecha.length));
    var mes = new String(fecha.substring(fecha.indexOf("/") + 1, fecha.lastIndexOf("/")));
    var dia = new String(fecha.substring(0, fecha.indexOf("/")));

    if (mes.length == 0 || dia.length == 0) {
        return false;
    }

    // valida si el año
    if (isNaN(ano) || ano.length < 4 || parseFloat(ano) < 1900) {
        return false;
    }



    if (isNaN(mes) || parseFloat(mes) < 1 || parseFloat(mes) > 12) {
        return false;
    }

    // valida el dia


    if (isNaN(dia) || parseInt(dia, 10) < 1 || parseInt(dia, 10) > 31) {
        return false;
    }

    if (parseInt(mes) == 4 || parseInt(mes) == 6 || parseInt(mes) == 9 || parseInt(mes) == 11) {
        if (parseInt(dia) > 30) {
            return false;
        }
    }

    // para el año biciesto
    if (parseInt(ano) % 4 == 0) {
        // si es biciesto
        if (parseInt(dia) > 29 && parseInt(mes) == 2) {
            return false;
        }
    }
    else {
        // nos biciesto
        if (parseInt(dia) > 28 && parseInt(mes) == 2) {
            return false;
        }
    }

    return true;

}

// metodos para validar--------------------------------------------------------------------------------


function obtenerCadenaPermitida(cadena, cadendaExtra) {

    var respuesta = "";
    var letra = "";
    var enter = "\n";

    var caracteres, i;
    caracteres = "abcdefghijklmnopqrstuvwxyz1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZ" + enter + cadendaExtra;
    for (i = 0; i < cadena.length; i++) {
        letra = cadena.substring(i, i + 1);
        if (caracteres.indexOf(letra) != -1) {
            respuesta = respuesta + letra
        }
    }
    return respuesta;

}

// metodos para validar numeros

function obtenerCadenaNumeros(cadena, cadendaExtra) {

    var respuesta = "";
    var letra = "";
    var enter = "\n";

    var caracteres, i;
    caracteres = "1234567890" + enter + cadendaExtra;
    for (i = 0; i < cadena.length; i++) {
        letra = cadena.substring(i, i + 1);
        if (caracteres.indexOf(letra) != -1) {
            respuesta = respuesta + letra
        }
    }
    return respuesta;
    

}

//-------------------------------------------------------------------------------------


function validarFechas(objFechaInicio,objFechaFinal) { //inicio

    fecha_inicio = objFechaInicio;
    fecha_final = objFechaFinal;

    fecha_inicio_valor = $.trim(fecha_inicio.val());
    fecha_final_valor = $.trim(fecha_final.val());


    if (fecha_inicio_valor.length == 0) {
        $.messager.alert('Búquedas y Reportes', 'Escribe una fecha de Inicio', '', function() { $(fecha_inicio).focus(); });
        return true;
    }

    if (fecha_final_valor.length == 0) {
        $.messager.alert('Búsqueda y reportes', 'Escribe una Fecha final', '', function() { $(fecha_final).focus(); });
        return true;
    }

    if ( mayor(fecha_inicio_valor,fecha_final_valor)) {
        $.messager.alert('Búsqueda ', 'la fecha Inicial es mayor que la fecha final', '', function() { $(fecha_final).focus(); });
        return true;
    }


} // final




//-------------------------------------------------------------------------------------
//  http://micodigobeta.com.ar/?p=189
function mayor(fecha, fecha2) { //incio
    var xMes = fecha.substring(3, 5);
    var xDia = fecha.substring(0, 2);
    var xAnio = fecha.substring(6, 10);
    var yMes = fecha2.substring(3, 5);
    var yDia = fecha2.substring(0, 2);
    var yAnio = fecha2.substring(6, 10);
    if (xAnio > yAnio) {
        return (true);
    } else {
        if (xAnio == yAnio) {
            if (xMes > yMes) {
                return (true);
            }
            if (xMes == yMes) {
                if (xDia > yDia) {
                    return (true);
                } else {
                    return (false);
                }
            } else {
                return (false);
            }
        } else {
            return (false);
        }

    }

} // final


// metodos para desplegar controles--------------------------------------------------------------------------------

function ext_ocultarBotonesOperacion() {$('#controles_operacion').hide(); }
function ext_visualizarBotonesOperacion() { $('#controles_operacion').show(); }
function ext_ocultarBotonesGuardarCancelar() { $('#controles_aceptar_cancelar').hide(); }
function ext_visualizarBotonesGuardarCancelar() { $('#controles_aceptar_cancelar').show(); }

// funcion que obtenie los valores requeridos

function valoresRequeridosGeneral(panel) { //inicio

    var campos, campo, valor_campo, nombre_campo, nombre_campo_mostrar, respuesta;
    campos = $('#' + panel + ' :input[id][requerido$="true"]');
    respuesta = true;

    for (var i = 0; i < campos.length; i++) { //inicio for

        campo = campos[i];
        valor_campo = $.trim($(campo).val());
        nombre_campo = $(campo).attr('name');

        //validacion para las cajas de texto

        if (nombre_campo.indexOf("txt") != -1) { //inicio
            if (valor_campo.length == 0) {
                respuesta = false;
                $(campo).css('border-style', 'dashed');
                $(campo).css('border-color', 'red');
            }
            else {
                $(campo).css('border-style', 'groove');
                $(campo).css('border-color', '');

            }

        } //fin


        //validacion para el combo

        // combos normalitos
        if (nombre_campo.indexOf("cmb") != -1) {//inicio validacion de combo
            var posicion = nombre_campo.indexOf("cmb");
            var nombre = nombre_campo.substring(posicion, nombre_campo.length);
            //var texto = $("[id$='cmb_metodo_planificacion_79'] option:selected").text();
            var texto = $("[id$='" + nombre + "'] option:selected").text();
            var id = $(campo).attr('value');
            if (texto.indexOf("<Seleccione>") != -1) {
                respuesta = false;
                $(campo).css('border-style', 'dashed');
                $(campo).css('border-color', 'red');
            }
            else {
                $(campo).css('border-style', 'groove');
                $(campo).css('border-color', '');
            }


        } //fin validacion combo


    } //fin del for

    return respuesta;

} // fin de la funcion

//---------------------------------------------------------------------------------------

// para serializar un formulario
$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.nombre]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

//---------------------------------------------------------------------------------------
// validad_check_box con combos

function validar_check_box_combos(panel, arreglo_datos) {

    var controles_check, control_check,i;
    var nombre_control_aux, nombre_control_aux1, nombre_control,aviso,respuesta,valor_campo;
    var valor_asignar;
    controles_check = $('#' + panel + ' :checkbox');


    respuesta = false;
    arrDatos = {};
    for (i = 0; i < controles_check.length; i++) {//inicio
      
        control_check = controles_check[i];
       
            nombre_control_aux = $(control_check).attr('id');
            pos = nombre_control_aux.indexOf("chk");
            nombre_control_aux1 = nombre_control_aux.substring(pos, nombre_control_aux.length);

            // nombre y propiedades de los combos
            pos = nombre_control_aux1.indexOf("_");
            nombre_control = "cmb_" + nombre_control_aux1.substring(pos + 1, nombre_control_aux1.length);
             
            texto_control = $("[id$='" + nombre_control + "']");
            aviso = texto_control.attr('aviso');
            valor_campo = $.trim(texto_control.val());
            
             if ($(control_check).attr('checked')) { 
               valor_asignar=valor_campo 
             }else
             { valor_asignar='';} 
            
            if(arreglo_datos[nombre_control] !== undefined){
              if (!arreglo_datos[nombre_control].push){
                  arreglo_datos[nombre_control]=[arreglo_datos[nombre_control]];          
              }
              arreglo_datos[nombre_control].push(valor_asignar || '');
            }else {
                arreglo_datos[nombre_control]=valor_asignar || '';
            }
            
            
           if ($(control_check).attr('checked')) {   
               if ( valor_campo == "-1") {
                   $.messager.alert('Japami', aviso);
                  return true;
                }
           }
       
       
    }//fin
    
    return respuesta;

}

// validad_check_box con caja de texo

function validar_check_box_cajas_textos(panel, arreglo_datos) {

    var controles_check, control_check,i;
    var nombre_control_aux, nombre_control_aux1, nombre_control,aviso,respuesta,valor_campo;
    var valor_asignar;
    controles_check = $('#' + panel + ' :checkbox');


    respuesta = false;
    arrDatos = {};
    for (i = 0; i < controles_check.length; i++) {//inicio
      
        control_check = controles_check[i];
       
            nombre_control_aux = $(control_check).attr('id');
            pos = nombre_control_aux.indexOf("chk");
            nombre_control_aux1 = nombre_control_aux.substring(pos, nombre_control_aux.length);

            // nombre y propiedades de los combos
            pos = nombre_control_aux1.indexOf("_");
            nombre_control = "txt_" + nombre_control_aux1.substring(pos + 1, nombre_control_aux1.length);
             
            texto_control = $("[id$='" + nombre_control + "']");
            aviso = texto_control.attr('aviso');
            valor_campo = $.trim(texto_control.val());
            
             if ($(control_check).attr('checked')) { 
               valor_asignar=valor_campo 
             }else
             { valor_asignar='';} 
            
            if(arreglo_datos[nombre_control] !== undefined){
              if (!arreglo_datos[nombre_control].push){
                  arreglo_datos[nombre_control]=[arreglo_datos[nombre_control]];          
              }
              arreglo_datos[nombre_control].push(valor_asignar || '');
            }else {
                arreglo_datos[nombre_control]=valor_asignar || '';
            }
            
            
           if ($(control_check).attr('checked')) {   
               if ( valor_campo.length == 0) {
                   $.messager.alert('Japami', aviso);
                  return true;
                }
           }
       
       
    }//fin
    
    return respuesta;

}

// validaciones buscar archivo en un grid
function buscar_en_grid(nombre_grid,dato_buscar,columna){
 var renglones,renglon;
 var respuesta;
 
 renglones=$('#'+ nombre_grid).datagrid('getRows');
 
 for(var i=0;i<renglones.length;i++){
     renglon=renglones[i];
     
     if(renglon[columna]==dato_buscar){
        return true
     }
     
 }
  return false;

}

