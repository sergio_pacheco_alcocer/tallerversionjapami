﻿var Pagina;
Pagina=$(document);
Pagina.ready(inicializarEventosVacaciones);

function inicializarEventosVacaciones()
{
    Cancelar_Vacaciones();
    Contar_Caracteres();
}

function Cancelar_Vacaciones(){
   $('input[id$=Btn_Cancelar_Vacaciones]').click(function(e){
        if(!confirm('¿Estas seguro de cancelar las vacaciones del empleado [' + $('select[id$=Cmb_Empleado] option:selected').text() + ']?')){
            e.preventDefault();
        }   
   });
}

function Contar_Caracteres(){
    $('textarea[id$=Txt_Comentarios]').keyup(function() {
        var Caracteres =  $(this).val().length;
        
        if (Caracteres > 250) {
            this.value = this.value.substring(0, 250);
            $(this).css("background-color", "Yellow");
            $(this).css("color", "Red");
        }else{
            $(this).css("background-color", "White");
            $(this).css("color", "Black");
        }
        
        $('#Mostrar').text(250 - Caracteres);
    });
}