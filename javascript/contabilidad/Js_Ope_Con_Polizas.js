﻿$(document).ready(function () { //Inicio (Como el Load de la pagina)
    try {
        //***************************************************************************************************************************************
        //                                                  INICIALIZACION DE CONTROLES
        //***************************************************************************************************************************************
        $("#Dtp_Fecha_Poliza").datepicker({
            changeMonth: true,
            changeYear: true,
            showOn: "button",
            buttonImage: "../../paginas/imagenes/paginas/SmallCalendar.gif",
            buttonImageOnly: true,
            dateFormat: 'dd/M/yy',
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
            constrainInput: true,
            onSelect: function (dateText, inst) {
                //Declaracion de variables
                var fechaPoliza; //variable para obtener la fecha de la poliza
                var tipoPoliza; //variable para obtener el tipo de la poliza

                try {

                    //Ocultar mensaje de error
                    $("#Lbl_Error").html('');
                    Mostrar_Mensaje_Error(false);

                    //Obtener la fecha y el tipo de la poliza
                    fechaPoliza = $("#Dtp_Fecha_Poliza").datepicker('getDate');
                    tipoPoliza = $("#Cmb_Tipo_Poliza").combobox('getValue');

                    //Limpiar la caja de texto del prefijo
                    $("#Txt_Prefijo").val('');

                    //Verificar si no son nulos o cero los elementos
                    if (fechaPoliza == null || tipoPoliza == null || tipoPoliza == "0") {
                        $("#Lbl_Error").html('Favor de proporcionar la fecha y el tipo de p&oacute;liza.');
                        Mostrar_Mensaje_Error(true);
                    } else {
                        Construye_Prefijo(tipoPoliza, fechaPoliza);
                    }
                } catch (ex) {
                    $("#Lbl_Error").html('Error: (Dtp_Fecha_Poliza_onSelect) ' + ex.Description);
                    Mostrar_Mensaje_Error(true);
                }
            }
        });

        //Colocar el calendario en español
        $.datepicker.setDefaults($.datepicker.regional[""]);
        $("#Dtp_Fecha_Poliza").datepicker($.datepicker.regional["es"]);

        //Cajas de texto del debe y el haber
        $("input.caja_texto_monto").numberbox({
            min: 0,
            precision: 2
        });


        //***************************************************************************************************************************************
        //                                                           EVENTOS
        //***************************************************************************************************************************************
        $("#Cmb_Tipo_Poliza").combobox({
            onSelect: function (record) {
                //Declaracion de variables
                var fechaPoliza; //variable para obtener la fecha de la poliza
                var tipoPoliza; //variable para obtener el tipo de la poliza

                try {
                    //Ocultar mensaje de error
                    $("#Lbl_Error").html('');
                    Mostrar_Mensaje_Error(false);

                    //Obtener la fecha y el tipo de la poliza
                    fechaPoliza = $("#Dtp_Fecha_Poliza").datepicker('getDate');
                    tipoPoliza = $("#Cmb_Tipo_Poliza").combobox('getValue');

                    //Limpiar la caja de texto del prefijo
                    $("#Txt_Prefijo").val('');

                    //Verificar si no son nulos o cero los elementos
                    if (fechaPoliza == null || tipoPoliza == null || tipoPoliza == "0") {
                        $("#Lbl_Error").html('Favor de proporcionar la fecha y el tipo de p&oacute;liza.');
                        Mostrar_Mensaje_Error(true);
                    } else {
                        Construye_Prefijo(tipoPoliza, fechaPoliza);
                    }
                } catch (ex) {
                    $("#Lbl_Error").html('Error: (Cmb_Tipo_Poliza_onSelect) ' + ex.Description);
                    Mostrar_Mensaje_Error(true);
                }
            }
        });

        $("#Img_Btn_Agregar_Partida").click(function () {
            //Declaracion de variables
            var validacion; //variable para la validacion de los datos
            var debe = 0; //variable para el debe
            var haber = 0; //variable para el haber
            var aux; //variable auxiliar

            try {
                //Ocultar mensaje de error
                $("#Lbl_Error").html('');
                Mostrar_Mensaje_Error(false);

                //valiadr si estan todos los datos
                validacion = Validacion_Partida();
                if (validacion == null || validacion == "") {
                    //Obtener el debe y el haber
                    aux = $("#Txt_Debe_Partida").val();
                    if (aux == null || aux == "") {
                        debe = 0;
                    } else {
                        debe = aux;
                    }

                    aux = $("#Txt_Haber_Partida").val();
                    if (aux == null || aux == "") {
                        haber = 0;
                    } else {
                        haber = aux;
                    }

                    Agregar_Partida($("#Txt_Cuenta_Contable").val(), $("#Txt_Caja_Busqueda").val().substring(19, $("#Txt_Caja_Busqueda").val().length - 1),
                        $("#Txt_Concepto_Partida").val(), debe, haber, '', '',
                        $("#Txt_Cuenta_Contable_ID").val(), $("#Cmb_Fuentes_Financiamiento").combobox('getValue'), '', '', '');

                    //limpiar los controles de la partida
                    Limpiar_Controles_Partida();

                    //Colocar el foco en la caja de texto de la busqueda
                    $("#Txt_Caja_Busqueda").focus();
                } else {
                    $("#Lbl_Error").html(validacion);
                    Mostrar_Mensaje_Error(true);
                }
            } catch (ex) {
                $("#Lbl_Error").html('Error: (Img_Btn_Agregar_Partida_click) ' + ex.Description);
                Mostrar_Mensaje_Error(true);
            }
        });

        $("#Img_Btn_Guardar").click(function () {
            //Declaracion de variables
            var validacion = ""; //variable para la validacion de los datos

            try {
                //Ocultar mensaje de error
                $("#Lbl_Error").html('');
                Mostrar_Mensaje_Error(false);

                //validar si se cumple con las condiciones
                validacion = Valida_Datos_Poliza();
                if (validacion == "" || validacion == null) {
                    Alta_Poliza();
                } else {
                    $("#Lbl_Error").html(validacion);
                    Mostrar_Mensaje_Error(true);
                }
            } catch (ex) {
                $("#Lbl_Error").html('Error: (Img_Btn_Guardar_click) ' + ex.Description);
                Mostrar_Mensaje_Error(true);
            }
        });

        $("#Img_Btn_Salir").click(function () {
            try {
                //Salir
                window.location = "../../paginas/Paginas_Generales/Frm_Apl_Principal.aspx";

            } catch (ex) {
                $("#Lbl_Error").html('Error: (Img_Salir_click) ' + ex.Description);
                Mostrar_Mensaje_Error(true);
            }
        });

        //Cajas del debe y el haber
        $("input.caja_texto_monto").keyup(function () {
            //Declaracion de variables
            var aux = ""; //variable auxiliar

            try {
                //Obtener el valor
                aux = $(this).val();

                //verificar si no es cero
                if (aux != "0") {
                    //Ejecutar la asimetria
                    Asimetria_Debe_Haber($(this).attr('tipo'));
                }
            } catch (ex) {
                $("#Lbl_Error").html('Error: (input.caja_texto_monto_keyup) ' + ex.Description);
                Mostrar_Mensaje_Error(true);
            }
        });

        $("input.caja_texto_monto").keypress(function (event, sender) {
            //Declaracion de variables
            var codigoCaracter; //variable para el codigo del caracter proporcionado
            var validacion = ""; //variable para la validacion
            var debe = 0; //variable para el debe
            var haber = 0; //variable para el haber
            var aux; //variable auxiliar

            try {
                //Obtener el caracter
                if (event && event.which) { //NN4 (supongo mozilla)
                    event = event;
                    codigoCaracter = event.which;
                } else {
                    event = event; //IE (explorer)
                    codigoCaracter = event.keyCode;
                }

                //verificar si fue un enter
                if (codigoCaracter == 13) {
                    //validar si estan todos los datos
                    validacion = Validacion_Partida();
                    if (validacion == "" || validacion == null) {
                        //Obtener el debe y el haber
                        aux = $("#Txt_Debe_Partida").val();
                        if (aux == null || aux == "") {
                            debe = 0;
                        } else {
                            debe = aux;
                        }

                        aux = $("#Txt_Haber_Partida").val();
                        if (aux == null || aux == "") {
                            haber = 0;
                        } else {
                            haber = aux;
                        }

                        Agregar_Partida($("#Txt_Cuenta_Contable").val(), $("#Txt_Caja_Busqueda").val().substring(19, $("#Txt_Caja_Busqueda").val().length - 1),
                        $("#Txt_Concepto_Partida").val(), debe, haber, '', '',
                        $("#Txt_Cuenta_Contable_ID").val(), $("#Cmb_Fuentes_Financiamiento").combobox('getValue'), '', '', '');

                        //limpiar los controles de la partida
                        Limpiar_Controles_Partida();

                        //Colocar el foco en la caja de texto de la busqueda
                        $("#Txt_Caja_Busqueda").focus();
                    } else {
                        $("#Lbl_Error").html(validacion);
                        Mostrar_Mensaje_Error(true);
                    }
                }
            } catch (ex) {
                $("#Lbl_Error").html('Error: (input.caja_texto_monto_keypress) ' + ex.Description);
                Mostrar_Mensaje_Error(true);
            }
        });




        Estado_Inicial(true);
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Load) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
});





//***************************************************************************************************************************************
//                                                           FUNCIONES PAGINA
//***************************************************************************************************************************************

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Mostrar_Mensaje_Error
//DESCRIPCION:             Mostrar u ocultar el bloque del mensaje de error
//PARAMETROS:              Mostrar: Booleano que indice si se va a mostrar o no el mensaje
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              05/Julio/2011 16:30 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Mostrar_Mensaje_Error(Mostrar) {
    //Verificar si se tiene que mostrar el mensaje de error
    if (Mostrar == true) {
        $("#Div_Error").show();
    } else {
        $("#Lbl_Error").html('');
        $("#Div_Error").hide();
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Estado_Inicial
//DESCRIPCION:             Colocar la pagina en un estado inicial para su navegacion
//PARAMETROS:              llenarCombos: Booleano que indica si se tienen que llenar los combos
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              08/Octubre/2012 12:30 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Estado_Inicial(llenarCombos) {
    //Declaracion de variables
    var fecha = new Date(); //variable para la fecha

    try {
        //Ocultar mensaje de error
        $("#Lbl_Error").html('');
        Mostrar_Mensaje_Error(false);

        //verificar si se tiene que llenar los combos
        if (llenarCombos == true) {
            //Llenar los combos
            Llena_Combo_Tipos_Polizas();
            Llena_Combo_Fuentes_Financiamiento();
        }

        //Autocompletado de la cuentas contables
        Autocompletado_Cuentas_Contables();

        //Colocar los datos del usuario
        Colocar_Datos_Usuario();

        //Colocar la fecha en el control
        //$("#Dtp_Fecha_Poliza").datepicker('setDate', Formato_Fecha(fecha));

        //Limpiar el Grid

    } catch (ex) {
        $("#Lbl_Error").html('Error: (Estado_Inicial) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Limpiar_Controles_Partida
//DESCRIPCION:             Limpiar los controles de las partidas
//PARAMETROS:              
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              11/Octubre/2012 18:30 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Limpiar_Controles_Partida() {
    try {
        //Cajas de texto
        $("#Txt_Debe_Partida").val('');
        $("#Txt_Haber_Partida").val('');
        $("#Txt_Caja_Busqueda").val('');
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Estado_Inicial) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}



//***************************************************************************************************************************************
//                                                                 GRID
//***************************************************************************************************************************************
//*******************************************************************************
//NOMBRE DE LA FUNCION:    Agregar_Partida
//DESCRIPCION:             Agregar la partida al Grid de las polizas
//PARAMETROS:              1.  cuenta: Cadena de texto con el numero de la cuenta
//                         2.  nombreCuenta: Cadena de texto con el nombre de la cuenta
//                         3.  concepto: Cadena de texto con el concepto de la partida 
//                         4.  debe: Flotante con el monto del debe
//                         5.  haber: Flotante con el monto del haber 
//                         6.  momentoInicial: Cadena de texto con el momento presupuestal inicial 
//                         7.  momentoFinal: Cadena de texto con el momento presupuestal final 
//                         8.  cuentaContableID: Cadena de texto con el ID de la cuenta contable 
//                         9.  fuenteFinanciamientoID: Cadena de texto con el ID de la fuente de financiamiento 
//                         10. proyectoProgramaID: Cadena de texto con el ID del proyecto-programa 
//                         11. partidaID: Cadena de texto con el ID de la partida 
//                         12. dependenciaID: Cadena de texto con el ID de la unidad responsable
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              11/Octubre/2012 18:15 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Agregar_Partida(cuenta, nombreCuenta, concepto, debe, haber, momentoInicial, momentoFinal, cuentaContableID, fuenteFinanciamientoID, proyectoProgramaID, partidaID, dependenciaID) {
    //Declaracion de variables
    var partidasJSON; //variable para las partidas de JSON
    var auxJSON; //variable auxiliar para la construccion de la ultima cadena de JSON
    var noPartidas = 0; //variable para el numero de las partidas
    var aux; //variable auxiliar

    try {
        //Obtener el numero de las partidas
        aux = $("#Txt_No_Partidas").val();
        if (aux != null || aux != "") {
            noPartidas = Number(aux);
        }

        //Incrementar el numero de la partida
        noPartidas++;

        //Construir la cadena JSON
        auxJSON = '{ "CUENTA":"' + cuenta + '", "NOMBRE_CUENTA":"' + nombreCuenta + '", "CONCEPTO":"' + concepto + '", "DEBE":"' + debe + '", "HABER":"' + haber + '", '
            + '"MOMENTO_INICIAL":"' + momentoInicial + '", "MOMENTO_FINAL":"' + momentoFinal + '", "CUENTA_CONTABLE_ID":"' + cuentaContableID + '", '
            + '"FUENTE_FINANCIAMIENTO_ID":"' + fuenteFinanciamientoID + '", "PROYECTO_PROGRAMA_ID":"' + proyectoProgramaID + '", '
            + '"PARTIDA_ID":"' + partidaID + '", "DEPENDENCIA_ID":"' + dependenciaID + '", "NO_PARTIDA": "' + noPartidas + '", "edicion": "0"}';

        //verificar si ya hay partidas
        partidasJSON = $("#Txt_Partidas").val();
        if (partidasJSON == null || partidasJSON == "") {
            partidasJSON = '[' + auxJSON + ']';
        } else {
            //quitar el ultimo ]
            partidasJSON = partidasJSON.substring(0, partidasJSON.length - 1);

            //colocar la nueva cadena
            partidasJSON += ", " + auxJSON + ']';
        }

        //Colocar la nueva cadena JSON en la caja de texto
        $("#Txt_Partidas").val(partidasJSON);

        //Incrementar el numero de las partidas
        Calcula_No_Partidas(1);

        //Calcular los totales
        Calcula_Totales();

        //Llenar el grid
        Llena_Grid_Partidas();
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Agregar_Partida) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Llena_Grid_Partidas
//DESCRIPCION:             Llenar el Grid de las partidas con la cadena JSON en la caja de texto
//PARAMETROS:              
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              26/Octubre/2012 16:00 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Llena_Grid_Partidas() {
    try {
        //Instanciar el datagrid
        $("#Grid_Partidas").datagrid({
            title: 'Partidas de la P&oacute;liza',
            iconCls: 'icon-reimprimir',
            width: 800,
            height: 200,
            idField: 'NO_PARTIDA',
            frozenColumns: null,
            pagination: false,
            rownumbers: false,
            loadMsg: 'Cargando Datos',
            pageSize: 10,
            singleSelect: true,
            striped: true,
            nowrap: false,
            fitColumns: true,
            columns: [[
                { field: 'NO_PARTIDA', title: 'Partida', width: 75, align: 'left' },
                { field: 'CUENTA', title: 'Cuenta', width: 175, align: 'left' },
                { field: 'NOMBRE_CUENTA', title: 'Nombre Cuenta', width: 125, align: 'left' },
                { field: 'CONCEPTO', title: 'Concepto', width: 125, align: 'left', editor: 'text' },
                { field: 'DEBE', title: 'Debe', width: 100, editor: 'text' },
                { field: 'HABER', title: 'Haber', width: 100, editor: 'text' },
                { field: 'MOMENTO_INICIAL', hidden: true, width: 0 },
                { field: 'MOMENTO_FINAL', hidden: true, width: 0 },
                { field: 'CUENTA_CONTABLE_ID', hidden: true, width: 0 },
                { field: 'FUENTE_FINANCIAMIENTO_ID', hidden: true, width: 0 },
                { field: 'PROYECTO_PROGRAMA_ID', hidden: true, width: 0 },
                { field: 'PARTIDA_ID', hidden: true, width: 0 },
                { field: 'DEPENDENCIA_ID', hidden: true, width: 0 },
                { field: 'action', title: '', width: 100,
                    formatter: function (value, row, index) {
                        try {
                            if (row.edicion == 1) {
                                var modificar = '<a href="#" onclick="Guardar_Cambios_Renglon(' + index + ')"><img src="../../paginas/imagenes/gridview/filesave.png" title="Modificar Partida"></a>';
                                var cancelar = '<a href="#" onclick="Cancelar_Cambios_Renglon(' + index + ')"><img src="../../paginas/imagenes/gridview/no.png" title="Cancelar"></a>';

                                //entregar resultado
                                return modificar + cancelar;
                            } else {
                                var editar = '<a href="#" onclick="Editar_Renglon(' + index + ')"><img src="../../paginas/imagenes/gridview/icono_modificar_det.png" title="Editar Partida"></a>';
                                var eliminar = '<a href="#" onclick="Eliminar_Renglon(' + index + ')"><img src="../../paginas/imagenes/gridview/grid_garbage.png" title="Eliminar Partida"></a>';

                                //entregar resultado
                                return editar + eliminar;
                            }
                        } catch (ex) {
                            $("#Lbl_Error").html('Error: (formatter_Agregar_Partida) ' + ex.Description);
                            Mostrar_Mensaje_Error(true);
                        }
                    }
                }
            ]]
        });

        //Llenar el grid
        $("#Grid_Partidas").datagrid('loadData', json_parse($("#Txt_Partidas").val()));
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Llena_Grid_Partidas) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

function Actualizar_Acciones_Grid(indice) {
    try {
        //Actualizar las acciones del grid
        $("#Grid_Partidas").datagrid('updateRow', {
            index: indice,
            row: {}
        });
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Actualizar_Acciones_Grid) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

function Obtener_Indice_Renglon(objetivo) {
    //Declaracion de variables
    var objetivoGrid; //variabler para el objetivo del grid
    var resultado = 0; //variable para el resultado

    try {
        //Instanciar el objetivo del grid
        objetivoGrid = $(objetivo).closest('objetivoGrid.datagrid-row');

        //Obtener el indice
        resultado = parseInt(objetivoGrid.attr('datagrid-row-index'));

        //Entregar resultado
        return resultado;
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Obtener_Indice_Renglon) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Editar_Renglon
//DESCRIPCION:             Habilitar el modo de edicion de un renglon
//PARAMETROS:              
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              26/Octubre/2012 16:00 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Editar_Renglon(indice) {
    //Declaracion de variables
    var jsonPartidas; //variable para las partidas de JSON

    try {
        //Obtener la cadena de las partidas
        jsonPartidas = json_parse($("#Txt_Partidas").val());

        //Verificar si el indice es menor o igual al total de elementos
        if (indice <= jsonPartidas.length) {
            jsonPartidas[indice].edicion = 1;
        }

        //Convertir el objeto en cadena JSON nuevamente
        $("#Txt_Partidas").val(toJSON(jsonPartidas));

        //llenar el grid nuevamente
        Llena_Grid_Partidas();

        //Colocar el modo de edicion del renglon
        $("#Grid_Partidas").datagrid('beginEdit', indice);
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Editar_Renglon) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Eliminar_Renglon
//DESCRIPCION:             Eliminar el renglon del Grid de acuerdo al indice
//PARAMETROS:              indice: Entero que contiene el indice del renglon a eliminar del Grid
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              25/Octubre/2012 13:30 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Eliminar_Renglon(indice) {
    try {
        //Eliminar partida
        $("#Grid_Partidas").datagrid('deleteRow', indice);

        //Colocar los datos actuales en la caja de texto
        $("#Txt_Partidas").val(toJSON($("#Grid_Partidas").datagrid('getRows')));

        //Calcular las partidas y los totales
        Calcula_No_Partidas(-1);
        Calcula_Totales();
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Eliminar_Renglon) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}


function Guardar_Cambios_Renglon(indice) {
    //Declaracion de variables
    var jsonPartidas; //variable para las partidas de JSON de la caja de texto
    var jsonPartidasGrid; //variable para las partidas JSON del Grid

    try {
        //Obtener la cadena de las partidas de la caja de texto
        jsonPartidas = json_parse($("#Txt_Partidas").val());

        //Obtener la cadena de las partidas del Grid
        jsonPartidasGrid = $("#Grid_Partidas").datagrid('getRows');

        //Verificar si el indice es menor o igual al total de elementos
        if (indice <= jsonPartidas.length && indice <= jsonPartidasGrid.length) {
            jsonPartidas[indice].edicion = 0;

        }

        //Convertir el objeto en cadena JSON nuevamente
        $("#Txt_Partidas").val(toJSON(jsonPartidas));

        //llenar el grid nuevamente
        Llena_Grid_Partidas();

        //Finalizar la edicion
        $("#Grid_Partidas").datagrid('endEdit', indice);
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Guardar_Cambios_Renglon) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Cancelar_Cambios_Renglon
//DESCRIPCION:             Cancelar las modificaciones realizadas en el renglon
//PARAMETROS:              indice: Entero que contiene el indice del renglon del cual se van a cancelar los cambios
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              26/Octubre/2012 15:10 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Cancelar_Cambios_Renglon(indice) {
    //Declaracion de variables
    var jsonPartidas; //variable para las partidas de JSON

    try {
        //Obtener la cadena de las partidas
        jsonPartidas = json_parse($("#Txt_Partidas").val());

        //Verificar si el indice es menor o igual al total de elementos
        if (indice <= jsonPartidas.length) {
            jsonPartidas[indice].edicion = 0;
        }

        //Convertir el objeto en cadena JSON nuevamente
        $("#Txt_Partidas").val(toJSON(jsonPartidas));

        //llenar el grid nuevamente
        Llena_Grid_Partidas();

        //Cancelar la edicion
        $("#Grid_Partidas").datagrid('cancelEdit', indice);
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Cancelar_Cambios_Renglon) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}





//***************************************************************************************************************************************
//                                                           FUNCIONES CONSULTA
//***************************************************************************************************************************************
//*******************************************************************************
//NOMBRE DE LA FUNCION:    Llena_Combo_Tipos_Polizas
//DESCRIPCION:             Llenar el combo de los tipos de las polizas
//PARAMETROS:              
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              08/Octubre/2012 12:45 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Llena_Combo_Tipos_Polizas() {
    try {
        //Llenar el combo
        $("#Cmb_Tipo_Poliza").combobox({
            url: 'Frm_Controlador_Polizas.aspx?Tipo_Consulta=Consulta_Tipos_Polizas',
            valueField: 'TIPO_POLIZA_ID',
            textField: 'DESCRIPCION',
            dataType: 'jsonp'
        });
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Llena_Combo_Tipos_Polizas) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Llena_Combo_Fuentes_Financiamiento
//DESCRIPCION:             Llenar el combo de los tipos de las fuentes de financiamiento
//PARAMETROS:              
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              08/Octubre/2012 17:00 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Llena_Combo_Fuentes_Financiamiento() {
    try {
        //Llenar el combo
        $("#Cmb_Fuentes_Financiamiento").combobox({
            url: 'Frm_Controlador_Polizas.aspx?Tipo_Consulta=Consulta_Fuentes_Financiamento',
            valueField: 'FUENTE_FINANCIAMIENTO_ID',
            textField: 'Nombre',
            dataType: 'jsonp'
        });
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Llena_Combo_Fuentes_Financiamiento) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Llena_Combo_Cuentas_Contables
//DESCRIPCION:             Llenar el combo de las cuentas contables
//PARAMETROS:              
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              08/Octubre/2012 19:00 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
//function Llena_Combo_Cuentas_Contables() {
//    try {
//        //Llenar el combo
//        $("#Cmb_Descripcion").combobox({
//            url: '../../Controladores/Ctr_Ope_Con_Polizas.aspx?Tipo_Consulta=Consulta_Cuentas_Contables',
//            valueField: 'CUENTA_CONTABLE_ID',
//            textField: 'DESCRIPCION',
//            dataType: 'jsonp'
//        });
//    } catch (ex) {
//        $("#Lbl_Error").html('Error: (Llena_Combo_Cuentas_Contables) ' + ex.Description);
//        Mostrar_Mensaje_Error(true);
//    }
//}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Construye_Prefijo
//DESCRIPCION:             Colocar el prefijo en la caja de texto de acuerdo al tipo de poliza y la fecha
//PARAMETROS:              1. tipoPoliza: Cadena de texto con el ID del tipo de la poliza
//                         2. fechaPoliza: Cadena de texto que contiene la fecha del DatePicker
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              10/Octubre/2012 19:00 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Construye_Prefijo(tipoPoliza, fechaPoliza) {
    try {
        //Usar una peticion de AJAX para el envio de los datos
        $.ajax({
            url: 'Frm_Controlador_Polizas.aspx?Tipo_Consulta=Consulta_Prefijo&Tipo_Poliza_ID=' + tipoPoliza + '&Fecha=' + fechaPoliza,
            method: 'POST',
            dataType: 'text',
            success: function (response) {
                //Colocar el prefijo en la caja de texto
                $("#Txt_Prefijo").val(response);
            },
            error: function (result) {
                $("#Lbl_Error").html("Error: (Construye_Prefijo) " + result);
                Mostrar_Mensaje_Error(true);
            }
        });
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Construye_Prefijo) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}


//*******************************************************************************
//NOMBRE DE LA FUNCION:    Autocompletado_Cuentas_Contables
//DESCRIPCION:             Llenar la caja de texto con el resultado de una consulta de acuerdo a lo que van escribiendo
//PARAMETROS:              
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              12/Octubre/2012 18:00 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Autocompletado_Cuentas_Contables() {
    try {
        $("#Txt_Caja_Busqueda").autocomplete("Frm_Controlador_Polizas.aspx", {
            extraParams: { Tipo_Consulta: 'Autocompletar_Cuentas_Contables' },
            multiple: true,
            dataType: "json",
            parse: function (data) {
                try {
                    return $.map(data, function (row) {
                        return {
                            data: row,
                            value: row.cuenta_contable_id,
                            result: row.cuenta_descripcion
                        }
                    });
                } catch (ex) {
                    $("#Lbl_Error").html('Error: (Autocompletado_Cuentas_Contables) ' + ex.Description);
                    Mostrar_Mensaje_Error(true);
                }
            },
            formatItem: function (item) {
                try {
                    return Format_Cuenta_Contable(item);
                } catch (ex) {
                    $("#Lbl_Error").html('Error: (Autocompletado_Cuentas_Contables) ' + ex.Description);
                    Mostrar_Mensaje_Error(true);
                }
            }
        }).result(function (e, item) {
            //Declaracion de variables
            var conceptoPoliza; //variable  para el concepto de la poliza

            try {
                //Colocar el ID de la cuenta contable en la caja de texto oculta
                $("#Txt_Cuenta_Contable_ID").val(item.cuenta_contable_id);

                //Colocar la cuenta contable
                $("#Txt_Cuenta_Contable").val(item.cuenta);

                //Verificar si hay concepto general de la poliza
                conceptoPoliza = $("#Txt_Concepto_Poliza").val();
                if (conceptoPoliza == null || conceptoPoliza == "") {
                    //Colocar el texto de la cuenta contable
                    $("#Txt_Concepto_Partida").val($("#Txt_Caja_Busqueda").val().substring(19, $("#Txt_Caja_Busqueda").val().length - 1));
                } else {
                    //Colocar el concepto general de la poliza
                    $("#Txt_Concepto_Partida").val(conceptoPoliza);
                }

                //Colocar el foco en la caja de texto del debe
                $("#Txt_Debe_Partida").focus();
            } catch (ex) {
                $("#Lbl_Error").html('Error: (Autocompletado_Cuentas_Contables) ' + ex.Description);
                Mostrar_Mensaje_Error(true);
            }
        });
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Autocompletado_Cuentas_Contables) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Format_Cuenta_Contable
//DESCRIPCION:             Entregar lo que va a mostrar de resultado el autocompletado
//PARAMETROS:              item: Objeto que contiene el valor seleccionado, y sus propiedades son los campos del dato
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              12/Octubre/2012 18:00 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Format_Cuenta_Contable(item) {
    try {
        // como se ve en el div contendor
        return item.cuenta_descripcion;
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Format_Cuenta_Contable) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Alta_Poliza
//DESCRIPCION:             Dar de alta una poliza
//PARAMETROS:              
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              04/Abril/2013 14:00 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Alta_Poliza() {
    //Declaracion de variables
    var Datos; //variable para los datos de la poliza
    var aux; //Variable auxiliar

    try {
        //Construir la cadena del resultado
        Datos = "0|" + $("#Cmb_Tipo_Poliza").combobox('getValue') + "|" + $("#Dtp_Fecha_Poliza").datepicker('getDate') + "|" + $("#Txt_Concepto_Poliza").val() + "|" +
            $("#Txt_Total_Debe").val() + "|" + $("#Txt_Total_Haber").val() + "|" + $("#Txt_No_Partidas").val() + "|";

        //Construir los detalles
        aux = Construye_Detalles_Poliza();
        Datos += aux;

        //Dar de alta la poliza con una epeticion de AJAX
        $.ajax({
            url: 'Frm_Controlador_Polizas.aspx?Tipo_Consulta=Alta_Poliza&Datos_Poliza=' + Datos,
            method: 'POST',
            dataType: 'text',
            success: function (response) {
                try {
                    //verificar la respuesta
                    if (isFinite(response) == true) {
                        $.messager.alert('Alta P&oacute;liza', 'La p&oacute;liza ' + response + ' ha sido dada de alta');

                        //Colocar la pagina en un estado inicial
                        Estado_Inicial(false);
                    } else {
                        $("#Lbl_Error").html('Error: (Alta_Poliza) ' + response);
                        Mostrar_Mensaje_Error(true);
                    }
                } catch (ex) {
                    $("#Lbl_Error").html('Error: (Alta_Poliza) ' + ex.Description);
                    Mostrar_Mensaje_Error(true);
                }
            },
            error: function (result) {
                try {
                    $("#Lbl_Error").html('Error: (Alta_Poliza) ' + result.status + ' ' + result.statusText);
                    Mostrar_Mensaje_Error(true);
                } catch (ex) {
                    $("#Lbl_Error").html('Error: (Alta_Poliza) ' + ex.Description);
                    Mostrar_Mensaje_Error(true);
                }
            }
        });
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Alta_Poliza) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Colocar_Datos_Usuario
//DESCRIPCION:             Colocar los datos del usuario
//PARAMETROS:              
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              30/Marzo/2012 12:37 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Colocar_Datos_Usuario() {
    //Declaracion de variables

    try {
        //Peticion de AJAX
        $.ajax({
            url: 'Frm_Controlador_Polizas.aspx?Tipo_Consulta=Datos_Usuario',
            method: 'POST',
            dataType: 'text',
            success: function (response) {
                //Declaracion de variables
                var datosUsuario; //variable para el resultado

                try {
                    //Construir el objeto
                    datosUsuario = JSON.parse(response);

                    //Colocar los datos del usuario en los controles
                    $("#Txt_Empleado_Creo").val(datosUsuario.Nombre_Usuario);
                    $("#Txt_Empleado_Autorizo").val(datosUsuario.No_Empleado);
                    $("#Cmb_Nombre_Empleado").combobox('setValue', datosUsuario.Nombre_Usuario);
                } catch (ex) {
                    $("#Lbl_Error").html('Error: (Colocar_Datos_Usuario) ' + ex.Description);
                    Mostrar_Mensaje_Error(true);
                }
            },
            error: function (result) {
                try {
                    $("#Lbl_Error").html('Error: (Colocar_Datos_Usuario) ' + result.status + ' ' + result.statusText);
                    Mostrar_Mensaje_Error(true);
                } catch (ex) {
                    $("#Lbl_Error").html('Error: (Colocar_Datos_Usuario) ' + ex.Description);
                    Mostrar_Mensaje_Error(true);
                }
            }
        });
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Colocar_Datos_Usuario) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

function Imprimir_Poliza(noPoliza, tipoPolizaID, mesAnio) {
    try {
        //Descargar el reporte de la poliza
        window.location = 'Frm_Controlador_Polizas.aspx?Tipo_Consulta=Datos_Usuario',
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Colocar_Datos_Usuario) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}






//***************************************************************************************************************************************
//                                                           FUNCIONES VALIDACION
//***************************************************************************************************************************************
//*******************************************************************************
//NOMBRE DE LA FUNCION:    Validacion_Partida
//DESCRIPCION:             Validar si esta toda la informacion para la partida de una poliza
//PARAMETROS:              
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              11/Octubre/2012 15:00 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Validacion_Partida() {
    //Declaracion de variables
    var resultado = ""; //variable para el resultado
    var auxMensajeError = ""; //variable para el mensaje del error
    var mostrarMensaje = false; //variable que indica si se muestra el mensaje
    var aux; //variable auxiliar
    var aux2; //variable auxiliar;

    try {
        //verificar si estan todos los elementos para agregar una partida
        aux = $("#Txt_Cuenta_Contable_ID").val();
        if (aux == null || aux == '0') {
            mostrarMensaje = true;
        }

        aux = $("#Txt_Concepto_Partida").val();
        if (aux == null || aux == '') {
            mostrarMensaje = true;
        }

        aux = $("#Cmb_Fuentes_Financiamiento").combobox('getValue');
        if (aux == null || aux == '0') {
            mostrarMensaje = true;
        }

        aux = $("#Txt_Debe_Partida").val();
        aux2 = $("#Txt_Haber_Partida").val();
        if ((aux == null && aux2 == null) || (aux == '0' && aux2 == '0')) {
            mostrarMensaje = true;
        }

        //Verificar si se tiene que mostrar el mensaje de error
        if (mostrarMensaje == true) {
            //Llenar el texto del mensaje
            resultado = 'Es necesario proporcionar:<br />'
                + '<table border="0" class="estilo_fuente_mensaje_error">'
                + '<tr>'
                + '<td align="left">+Cuenta Contable</td>'
                + '<td align="left">+Fuente Financiamiento</td>'
                + "</tr>"
                + '<tr>'
                + '<td align="left">+Concepto de la P&oacute;liza</td>'
                + '<td align="left">+Debe &oacute; Haber</td>'
                + '</tr>'
                + '</table>';
        }

        //Entregar resultado
        return resultado;
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Validacion_Partida) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Valida_Totales
//DESCRIPCION:             Validar los totales del debe y el haber
//PARAMETROS:              
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              15/Octubre/2012 19:00 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Valida_Totales() {
    //Declaracion de variables
    var totalDebe = 0; //variable para el total del debe
    var totalHaber = 0; //variable para el total del haber
    var aux; //Variable auxiliar
    var resultado = ""; //variable para el resultado
    var mostrarMensaje = false; //variable que indica si se tiene que mostrar el mensaje de error

    try {
        //Verificar si ambos tienen valores
        aux = $("#Txt_Total_Debe").val();
        if (aux != "" && aux != null) {
            totalDebe = Number(aux);
        } else {
            mostrarMensaje = true;
        }

        aux = $("#Txt_Total_Haber").val();
        if (aux != "" && aux != null) {
            totalHaber = Number(aux);
        } else {
            mostrarMensaje = true;
        }

        //Verificar si las cantidades son iguales
        if (totalDebe == 0 || totalHaber == 0) {// && totalDebe != totalDebe != totalHaber) {
            mostrarMensaje = true;
        } else {
            if (totalDebe != totalHaber) {
                mostrarMensaje = true;
            }
        }

        //Veriricar si se tiene que mostrar el mensaje de error
        if (mostrarMensaje == true) {
            //Construir el mensaje del error
            resultado = "El monto del Debe y Haber deben de ser iguales y diferentes de cero.";
        }

        //Entregar resultado
        return resultado;
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Valida_Totales) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Valida_Datos_Poliza
//DESCRIPCION:             Validar los datos generales de la poliza
//PARAMETROS:              
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              15/Octubre/2012 19:00 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Valida_Datos_Poliza() {
    //Declaracion de variables
    var resultado = ""; //variable para el resultado
    var aux; //variable auxiliar
    var mostrarMensaje = false; //Variable que indica si se tiene que mostrar el mensaje

    try {
        //verificar si estan los datos generales
        aux = $("#Dtp_Fecha_Poliza").datepicker('getDate');
        if (aux == "" || aux == null) {
            mostrarMensaje = true;
        }

        aux = $("#Cmb_Tipo_Poliza").combobox('getValue');
        if (aux == "" || aux == null) {
            mostrarMensaje = true;
        }

        aux = $("#Txt_Concepto_Poliza").val();
        if (aux == "" || aux == null) {
            mostrarMensaje = true;
        }

        //Verificar si se tiene que mostrar el mensaje
        if (mostrarMensaje == true) {
            resultado = "Es necesario proporcionar:<br />"
                + "<table border='0' class='estilo_fuente_mensaje_error'>"
                + "<tr>"
                + "<td align='left'>+Tipo P&oacute;liza</td>"
                + "<td align='left'>+Concepto de P&oacute;liza</td>"
                + "</tr>"
                + "<tr>"
                + "<td align='left'>+Fecha P&oacute;liza</td>"
                + "<td align='left'>+Montos de Debe y Haber</td>"
                + "</tr>"
                + "</table>";
        } else {
            resultado = Valida_Totales();
        }

        //Entregar resultado
        return resultado;
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Valida_Totales) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Asimetria_Debe_Haber
//DESCRIPCION:             Colocar el debe o el haber en cero dependiendo de si se ingreso el contrario
//PARAMETROS:              tipo: Cadena de texto que contiene el tipo (debe o haber) que ha sido ingresado para hacer cero al contrario
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              25/Octubre/2012 19:00 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Asimetria_Debe_Haber(tipo) {
    try {
        //Verificar el tipo
        if (tipo == "debe") {
            //Hacer cero la caja del haber
            $("#Txt_Haber_Partida").val("0");
        }

        if (tipo == "haber") {
            //Hacer cero la caja del debe
            $("#Txt_Debe_Partida").val("0");
        }
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Asimetria_Debe_Haber) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Calcula_No_Partidas
//DESCRIPCION:             Colocar el numero de las partidas en la caja de texto
//PARAMETROS:              partida: Entero que contiene el incremento/decremento para el total de las partidas
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              15/Octubre/2012 10:45 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Calcula_No_Partidas(partida) {
    //Declaracion de partidas
    var noPartidas = 0; //variable para el numero de las partidas
    var aux; //variable auxiliar

    try {
        //Obtener el contenido de la caja de texto de las partidad
        aux = $("#Txt_No_Partidas").val();

        //verificar si es nulo
        if (aux != null && aux != "") {
            noPartidas = Number(aux);
        }

        //Incrementar el numero de las partidas
        noPartidas += Number(partida);

        //Colocar el numero de las partidas
        $("#Txt_No_Partidas").val(noPartidas);
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Calcula_No_Partidas) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Calcula_Totales
//DESCRIPCION:             Colocar los totales del debe y el haber en las cajas de texto
//PARAMETROS:              
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              15/Octubre/2012 11:00 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Calcula_Totales() {
    //Declaracion de variables
    var totalDebe = 0; //variable para el total del debe
    var totalHaber = 0; //variable para el total del haber
    var aux; //variable auxiliar
    var JSONPartidas; //variable para el manejo de los objetos JSON
    var contElementos = 0; //variable para el contador

    try {
        //instanciar el objeto JSON
        JSONPartidas = json_parse($("#Txt_Partidas").val());

        //verificar si el objeto tiene elementos
        if (JSONPartidas.length > 0) {
            //Ciclo para el barrido del arreglo
            for (contElementos = 0; contElementos < JSONPartidas.length; contElementos++) {
                //Calcular los totales
                totalDebe += Number(JSONPartidas[contElementos].DEBE);
                totalHaber += Number(JSONPartidas[contElementos].HABER);
            }
        }

        //Colocar los totales
        $("#Txt_Total_Debe").val(totalDebe);
        $("#Txt_Total_Haber").val(totalHaber);
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Calcula_Totales) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    Construye_Detalles_Poliza
//DESCRIPCION:             Construir una cadena con los detalles de la poliza
//PARAMETROS:              
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              03/Abril/2013 14:00 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Construye_Detalles_Poliza() {
    //Declaracion de variables
    var resultado = ""; //variable para el resultado
    var datosGrid; //variable para los renglones del grid
    var contElementos = 0; //variable para el contador

    try {
        //Obtener los renglones del grid
        datosGrid = $("#Grid_Partidas").datagrid('getRows');

        //Ciclo para el barrido de los elementos
        for (contElementos = 0; contElementos < datosGrid.length; contElementos++) {
            resultado += datosGrid[contElementos].NO_PARTIDA + "¬";
            resultado += datosGrid[contElementos].CUENTA_CONTABLE_ID + "¬";

            //Verificar si el texto es el mismo que el del concepto de la poliza
            if (datosGrid[contElementos].CONCEPTO == $("#Txt_Concepto_Poliza").val()) {
                resultado += "CP¬";
            } else {
                resultado += datosGrid[contElementos].CONCEPTO + "¬";
            }

            resultado += datosGrid[contElementos].FUENTE_FINANCIAMIENTO_ID + "¬";
            resultado += datosGrid[contElementos].DEBE + "¬";
            resultado += datosGrid[contElementos].HABER + "°";
        }

        //Quitar el ultimo separador
        resultado = resultado.substr(0, resultado.length - 1);

        //Entregar resultado
        return resultado;
    } catch (ex) {
        $("#Lbl_Error").html('Error: (Construye_Detalles_Poliza) ' + ex.Description);
        Mostrar_Mensaje_Error(true);
    }
}
