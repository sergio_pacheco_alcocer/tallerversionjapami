﻿
var region_id;
var texto_combo;

$(function(){
   // ventana mensajes
  $('#ventana_mensaje').window('close');
  
  // crear tablas
  
  crearTablaFactura();
   
  $("[id$='cmb_regiones']").change(function(){
             escuchadorEventoCombo();  
  });
  
   $("[id$='Btn_Salir']").click(function(event) {
        event.preventDefault();
    });

}); // fin funcion principal


//------------------------------------------------------------------------

function escuchadorEventoCombo(){
region_id=$("[id$='cmb_regiones']").val();
texto_combo=$("[id$='cmb_regiones'] option:selected").text();
$('#tabla_facturas').datagrid('loadData', { total: 0, rows: [] }); 


  if(texto_combo.indexOf("SELECCIONAR")!=-1){
      $.messager.alert('Japami',"Selecciona una regi&oacute;n");
      return;
    }


 $('#tabla_facturas').datagrid({ url: '../../Controladores/frm_controlador_facturacion_anual.aspx', queryParams: {
         accion: 'obtener_faturacion_anual_region',
         region_id:region_id
     }, pageNumber: 1
     });

  
}


//------------------------------------------------------------------

function generarFactura(){

   
    texto_combo=$("[id$='cmb_regiones'] option:selected").text();
    
    if(texto_combo.indexOf("SELECCIONAR")!=-1){
      $.messager.alert('Japami',"Selecciona una regi&oacute;n");
      return;
    }
    
   region_id=$("[id$='cmb_regiones']").val();
   
  $('#ventana_mensaje').window('open'); 
  $.ajax({
       type: "POST",
       url: "../../Controladores/frm_controlador_facturacion_anual.aspx/elaborarFacturaAnual",
       data: "{'region_id':'" + region_id + "'}",
       contentType: "application/json; charset=utf-8",
       dataType: "json",
       timeout:4200000,
       success: function (response) {
           $('#ventana_mensaje').window('close'); 
           var respuesta = eval("(" + response.d + ")");
      
           if( respuesta.mensaje=="bien"){  
              proceso_exitoso();
            }else{ proceso_no_existoso(respuesta) };
          
       },
       error: function (result,t) {
           //$('#ventana_mensaje').window('close'); 
           //$.messager.alert('ERROR ' + result.status + ' ' + result.statusText +' ' + t);
       }
   });  

}

//------------------------------------------------------------------
function proceso_exitoso(){
  $.messager.alert('Japami','Proceso Terminado','',function(){
  
   $('#tabla_facturas').datagrid('loadData', { total: 0, rows: [] }); 
  $('#tabla_facturas').datagrid({ url: '../../Controladores/frm_controlador_facturacion_anual.aspx', queryParams: {
         accion: 'obtener_faturacion_anual_region',
         region_id:region_id
     }, pageNumber: 1
     });
  
  });
  
}

//---------------------------------------------------------------------

function proceso_no_existoso(respuesta){
  $.messager.alert('Japami',respuesta.mensaje);
}
//--------------------------------------------------cre

function crearTablaFactura(){

  //incio tabla
    $('#tabla_facturas').datagrid({
        title: 'Faturacion Anual ',
        view: detailview,
        detailFormatter: function(index, row) {
            return '<div style="padding:2px"><table id="ddv-' + index + '"></table></div>';
        },
        width: 850,
        height: 400,
        columns: [[
           { field: 'no_factura_recibo', title: 'idS', width: 50 },
	       { field: 'no_cuenta', title: 'No. Cuenta', width: 100},
	       { field: 'no_recibo', title: 'No Recibo', width: 100 },
	       { field: 'periodo_facturacion', title: 'P.Facturacion', width: 120 },
	       { field: 'facturado', title: 'Facturado', width: 120 },
	       { field: 'pagado',title:'Pagado', width:120 },
	       { field: 'saldo', title: 'Saldo', width: 120 },
	       { field: 'no_region', title: 'No.Region', width: 140 },
	       { field: 'lectura_actual', title: 'Lec. Actual', width: 140 },
	       { field: 'lectura_anterior', title: 'Lec. Anterior', width: 140 },
	       { field: 'consumo', title: 'Consumo', width: 120 },
	       { field: 'tasa_iva', title: 'Tasa IVA', width: 120 },
	       { field: 'total_iva', title: 'Total IVA', width: 120 },
           { field: 'estado_recibo', title: 'Estado', width: 120 },
           { field: 'fecha_creo', title: 'Fecha Creo', width: 120 }
           
    ]],
        pageSize: 50,
        pagination: true,
        rownumbers: true,
        remoteSort: false,
        fitColumns: false,
        singleSelect: true,
        striped: true,
        nowrap: true,
        onExpandRow: function(index, row) {
            $('#ddv-'+index).datagrid({  
            url:'../../Controladores/frm_controlador_facturacion_anual.aspx?accion=facturas_anual_detalles&no_factura_recibo='+row.no_factura_recibo,  
            fitColumns:false,  
            singleSelect:true,
            nowrap: true,  
            rownumbers:true,  
            loadMsg:'',  
            height:150,
            width:700,  
            columns:[[  
                 { field: 'concepto', title: 'Concepto', width: 200, sortable: true },
	             { field: 'total', title: 'Total', width: 150, sortable: true },
	             { field: 'total_abonado', title: 'Abonado', width: 150, sortable: true },
	             { field: 'total_saldo', title: 'Saldo', width: 150, sortable: true }
            ]],  
            onResize:function(){  
                $('#tabla_facturas').datagrid('fixDetailRowHeight',index);  
            },  
            onLoadSuccess:function(){  
                setTimeout(function(){  
                    $('#tabla_facturas').datagrid('fixDetailRowHeight',index);  
                },0);  
            }  
        });
            
            $('#tabla_facturas').datagrid('fixDetailRowHeight', index);
        }

    });

     // ocultar columna
    $('#tabla_facturas').datagrid('hideColumn', 'no_factura_recibo');
    $('#tabla_facturas').datagrid('hideColumn', 'lectura_actual');
    $('#tabla_facturas').datagrid('hideColumn', 'lectura_anterior');
    $('#tabla_facturas').datagrid('hideColumn', 'consumo');

}