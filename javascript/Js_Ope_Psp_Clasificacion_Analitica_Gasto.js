﻿//*********************************************************************************************************************************************************
//************************************************************** VARIABLES ********************************************************************************
//*********************************************************************************************************************************************************
var nivel;
var Fte_Financiamiento;
var Area;
var Programa;
var Dependencia;
var Partida;
var Tipo_Descripcion;

//*********************************************************************************************************************************************************
//************************************************************** INICIO ***********************************************************************************
//*********************************************************************************************************************************************************

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN :$(document).ready(function
///DESCRIPCIÓN          : Funcion para iniciar con los datos del formulario
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 30/Noviembre/2011 
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///***********************************************************************************************************************
 $(document).ready(function() {
     try
     {
        $('#Div_Detalles').window('close');
        Llenar_Combo_Anios();
        Llenar_Combo_UR();
        $("#Tbl_Opciones").hide();
        
        //Evento del boton de salir
        $("input[id$=Btn_Salir]").click(function (e) {
            e.preventDefault();
            location.href='../Paginas_Generales/Frm_Apl_Principal.aspx';
        });
        
        //Evento del boton generar clasificacion
        $("input[id$=Btn_Generar]").click(function (e) {
            e.preventDefault();
            if($("select[id$=Cmb_Anios]").get(0).selectedIndex > 0){
                $("#Tbl_Opciones").show();
                Generar_TreeGrid();
            }else{
                $.messager.alert('Mensaje',"Favor de seleccionar un año");
                $("#Tbl_Opciones").hide();
            }
        });

    }catch(Ex){
        $.messager.alert('Mensaje','Error al ejecutar los eventos de la página. Error: [' + Ex + ']');
    }
 });
 
//*********************************************************************************************************************************************************
//************************************************************** METODOS **********************************************************************************
//*********************************************************************************************************************************************************
 
///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Anios
///DESCRIPCIÓN          : Funcion para llenar el combo de los anios
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 30/Noviembre/2011 
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Llenar_Combo_Anios()
{
    try{
        var Cmb =  $("select[id$=Cmb_Anios]").get(0);
        
        
        $.ajax({
            url: "Frm_Ope_Psp_Controlador.aspx?Accion=Anios",
            type:'POST',
            async: false,
            cache: false,
            dataType:'json',
            success: function(Datos) {
                 if (Datos != null) {
                     Cmb.options[0] = new Option('<- Seleccione ->', 'Seleccione'); 
                     $.each(Datos.anios, function (Contador, Elemento) {
                        Cmb.options[++Contador] = new Option(Elemento.ANIO, Elemento.ANIO);
                     });
                 }
                 else {
                      $.messager.alert('Mensaje',"No hay Años Presupuestados");
                 }
            }
        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar el combo de años presupuestados. Error: [' + Ex + ']');
    }
}
 
///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Generar_TreeGrid
///DESCRIPCIÓN          : Funcion para llenar el treegrid
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 30/Noviembre/2011 
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///***********************************************************************************************************************
 function Generar_TreeGrid()
 {
    try
    {
        Tipo_Descripcion = "";
        var Anio = $("select[id$=Cmb_Anios] option:selected").val();
        var UR = $("select[id$=Cmb_UR] option:selected").val();
        
        $('#Grid_CAG').treegrid({
		    title:'Clasificación Analítica del Gasto',
		    width:858,
		    height:530,
		    nowrap: false,
		    rownumbers: false,
		    animate:true, //Define si para mostrar el efecto de animación al nodo expandir o contraer. 	
		    collapsible:true,
		    url:'Frm_Ope_Psp_Controlador.aspx?Accion=Nivel_Fte_Financiamiento&Anio=' + Anio + '&UR=' + UR,
		    idField:'id',
		    treeField:'texto',
		    nowrap:true,
		    resizable:true,
		    frozenColumns:[[
                {title:'Nombre',field:'texto',width:358,
                    formatter:function(value){
                	    return '<span style="color:Navy; font-size:9px">'+value+'</span>';
                    }
                }
		    ]],
		    columns:[[
		        {field:'descripcion6',title:'Aprobado',width:100, align: 'right',
			             formatter:function(value){
                	        return '<span style="font-size:xx-small">'+value+'</span>';
                        }
			    },
			    {field:'descripcion7',title:'Modificado',width:100, align: 'right',
			             formatter:function(value){
                	        return '<span style="font-size:xx-small">'+value+'</span>';
                        }
			    },
		        {field:'descripcion5',title:'Disponible',width:100, align: 'right',
			             formatter:function(value){
                	        return '<span style="font-size:xx-small">'+value+'</span>';
                        }
			    },
			    {field:'descripcion4',title:'Comprometido',width:115, align: 'right',
			         formatter:function(value){
                	    return '<span><a onclick="Descripcion(4);" style="font-size:xx-small">'+value+'</a></span>';
                    }
			    },
			    {field:'descripcion1',title:'Devengado',width:100, align: 'right', 
			        formatter:function(value){
                	    return '<span><a onclick="Descripcion(1);" style="font-size:xx-small">'+value+'</a></span>';
                    }
			    },
			    {field:'descripcion2',title:'Ejercido',width:95, align: 'right',
			         formatter:function(value){
                	    return '<span><a onclick="Descripcion(2);" style="font-size:xx-small">'+value+'</a></span>';
                    }
			    },
			    {field:'descripcion3',title:'Pagado',width:95, align: 'right',
			         formatter:function(value){
                	    return '<span><a onclick="Descripcion(3);" style="font-size:xx-small">'+value+'</a></span>';
                    }
                }
		    ]],
		    onBeforeExpand:function(node){
		        Limpiar_Variables();
		        nivel = node.attributes.valor1;
		        Fte_Financiamiento = node.attributes.valor2;
		        Area = node.attributes.valor3;
		        Programa = node.attributes.valor4;
		        Dependencia = node.attributes.valor5;
		        Partida = node.attributes.valor6;
    		    
			    if (nivel == "nivel_FteFinanciamiento") {
                    $('#Grid_CAG').treegrid('options').url = 'Frm_Ope_Psp_Controlador.aspx?Accion=Nivel_Area_Funcional&Anio=' + Anio+'&Fte_Financiamiento=' + Fte_Financiamiento+ '&UR=' + UR;
                }
                if (nivel == "nivel_Areas") {
                    $('#Grid_CAG').treegrid('options').url = 'Frm_Ope_Psp_Controlador.aspx?Accion=Nivel_Programa&Anio=' + Anio+'&Area=' + Area+'&Fte_Financiamiento=' + Fte_Financiamiento+ '&UR=' + UR;
                }
                if (nivel == "nivel_Programas") {
                    $('#Grid_CAG').treegrid('options').url = 'Frm_Ope_Psp_Controlador.aspx?Accion=Nivel_Dependencias&Anio=' + Anio+'&Fte_Financiamiento=' + Fte_Financiamiento + "&Programa=" + Programa+'&Area=' + Area+ '&UR=' + UR;
                }
                if (nivel == "nivel_Dependencia") {
                    $('#Grid_CAG').treegrid('options').url = 'Frm_Ope_Psp_Controlador.aspx?Accion=Nivel_Partida_Especifica&Anio=' + Anio+'&Fte_Financiamiento=' + Fte_Financiamiento + "&Programa=" + Programa+'&Area=' + Area + "&Dependencia=" + Dependencia+ '&UR=' + UR;
                }
           },
           onClickRow: function(node){
                Limpiar_Variables();
                if(node.attributes != null)
                {
                    nivel = node.attributes.valor1;
		            Fte_Financiamiento = node.attributes.valor2;
		            Area = node.attributes.valor3;
		            Programa = node.attributes.valor4;
		            Dependencia = node.attributes.valor5;
		            Partida = node.attributes.valor6;
        		    
    		        if(Tipo_Descripcion != "")
    		        {
    		            Mostrar_Detalle();
    		        }
    		        Tipo_Descripcion = "";
    		    }
           }
	    });
	}catch(Ex){
        $.messager.alert('Mensaje','Error al llenar generar la tabla. Error: [' + Ex + ']');
    }
 }
 
///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Descripcion
///DESCRIPCIÓN          : Funcion para obtener el tipo de descripcion que mostraremos
///PARAMETROS           1 Numero: para saber que tipo de descripcion haremos 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 02/Diciembre/2011 
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///***********************************************************************************************************************
 function Descripcion(Numero){
     Tipo_Descripcion = "";
     if(Numero == 1){
        Tipo_Descripcion = "DEVENGADO"
     } 
     else if(Numero == 2){
        Tipo_Descripcion = "EJERCIDO"
     }
     else if(Numero == 3){
        Tipo_Descripcion = "PAGADO"
     }
     else if(Numero == 4){
        Tipo_Descripcion = "COMPROMETIDO"
     }
     else if(Numero == 5){
        Tipo_Descripcion = "DISPONIBLE"
     }
 }
 
///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Limpiar_Variables
///DESCRIPCIÓN          : Funcion para limpiar las variables
///PARAMETROS           :
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 02/Diciembre/2011 
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///***********************************************************************************************************************
 function Limpiar_Variables(){
    nivel = "";
    Fte_Financiamiento = "";
    Area = "";
    Programa = "";
    Dependencia = "";
    Partida = "";
 }
 
///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Mostrar_Detalle
///DESCRIPCIÓN          : Funcion para mostrar los detalles del tipo de descripcion
///PARAMETROS           :
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 02/Diciembre/2011 
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///***********************************************************************************************************************
function Mostrar_Detalle()
{
	try
	{
	    var Anio = $("select[id$=Cmb_Anios] option:selected").val();
    	
	    var Parametros = "&Anio=" + Anio + "&Fte_Financiamiento=" + Fte_Financiamiento + "&Area=" + Area + "&Programa=" + Programa;
	    Parametros += "&Dependencia=" + Dependencia + "&Partida="+ Partida + "&Tipo_Descripcion=" + Tipo_Descripcion;
	
        $.ajax({
            url: "Frm_Ope_Psp_Controlador.aspx?Accion=Detalles" + Parametros,
            type:'POST',
            async: false,
            cache: false,
            dataType:'json',
            success: function(Datos) {
                 if (Datos != null) {
                     //abrimos la ventana donde se mostraran los detalles
                    $('#Div_Detalles').window({
		                title: 'Detalles',
		                width: 875,
		                modal: true,
		                shadow: false,
		                closed: false,
		                height: 380, 
		                top: 100,
		                left:200
	                });
	                
	                $('#Grid_Detalles').datagrid({
                        title: '',
                        url:'Frm_Ope_Psp_Controlador.aspx?Accion=Detalles' + Parametros,
                        width: 830,
                        height: 340,
                        columns: [[
                            { field: 'NO_RESERVA', title: 'No Reserva', width: 80, sortable: true, align: 'right', editor: 'text' },
                            { field: 'FECHA', title: 'Fecha', width: 170, align: 'left', sortable: true, editor: 'text' },
                            { field: 'SALDO_FORMATEADO', title: 'Saldo', width: 130, sortable: true, align: 'right', editor: 'text' },
                            { field: 'CARGO', title: 'Cargo', width: 160, align: 'left', sortable: true, editor: 'text' },
                            { field: 'ABONO', title: 'Abono', width: 160, sortable: true, align: 'left', editor: 'text' },
                            { field: 'IMPORTE_FORMATEADO', title: 'Importe', width: 130, align: 'right', sortable: true, editor: 'text' }
                          ]],
                        pagination: false,
                        rownumbers: false,
                        remoteSort: false,
                        fitColumns: true,
                        singleSelect: true,
                        striped: true,
                        nowrap: false,
                        loadMsg: 'Cargando Datos'
                   });
                   
//                    $('#Grid_Detalles').datagrid(Datos);
                 }
                 else {
                      $.messager.alert('Mensaje',"No hay Detalles");
                 }
            }
        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al mostrar los detalles. Error: [' + Ex + ']');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Llenar_Combo_UR
///DESCRIPCIÓN          : Funcion para llenar el combo de las unidades responsables
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 01/Marzo/2012 
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Llenar_Combo_UR()
{
    try{
        var Cmb =  $("select[id$=Cmb_UR]").get(0);

        $.ajax({
            url: "Frm_Ope_Psp_Controlador.aspx?Accion=Unidades_Responsables",
            type:'POST',
            async: false,
            cache: false,
            dataType:'json',
            success: function(Datos) {
                 if (Datos != null) {
                     Cmb.options[0] = new Option('<- Seleccione ->', 'Seleccione'); 
                     $.each(Datos.ur, function (Contador, Elemento) {
                        Cmb.options[++Contador] = new Option(Elemento.Nombre, Elemento.DEPENDENCIA_ID);
                     });
                 }
                 else {
                      $.messager.alert('Mensaje',"No hay Unidades Responsables");
                 }
            }
        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar el combo de unidades responsables. Error: [' + Ex + ']');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Llenar_Combo_UR
///DESCRIPCIÓN          : Funcion para llenar el combo de las unidades responsables
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 01/Marzo/2012 
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function collapseAll(){
	$('#Grid_CAG').treegrid('collapseAll');
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : expandAll
///DESCRIPCIÓN          : Funcion para abrir todos los nodos del treegrid
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 07/Mayo/2012 
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function expandAll(){
	$('#Grid_CAG').treegrid('expandAll');
}
