﻿//*********************************************************************************************************************************************************
//************************************************************** INICIO ***********************************************************************************
//*********************************************************************************************************************************************************
//************************************************************** VARIABLES ********************************************************************************
    var Chk_FF_Ing = 'NO';
    var Chk_PP_Ing = 'NO';
    var Chk_R = 'NO';
    var Chk_T = 'NO';
    var Chk_Cl = 'NO';
    var Chk_Con_Ing = 'NO';
    var Chk_SubCon = 'NO';
    var Nivel_Ing = '';
    var Estado = '';
    var Chk_FF = 'NO';
    var Chk_AF = 'NO';
    var Chk_PP = 'NO';
    var Chk_Ur = 'NO';
    var Chk_Cap = 'NO';
    var Chk_Con = 'NO';
    var Chk_PG = 'NO';
    var Chk_PE = 'NO';
    var Nivel_Egr = '';
    var Estado_Egr = '';
//variables de los detalles de ingresos
    var Det_Niv = '';
    var Det_Rub = '';
    var Det_Tip = '';
    var Det_Cla = '';
    var Det_CoI = '';
    var Det_Sub = '';
    var Det_PPI = '';
    var Det_FFI = '';
//variables de los detalles de egresos
    var Det_Cap = '';
    var Det_Con = '';
    var Det_PG = '';
    var Det_PE = '';
    var Det_FF = '';
    var Det_PP = '';
    var Det_AF = '';
    var Det_UR = '';

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN :$(document).ready(function
///DESCRIPCIÓN          : Funcion para iniciar con los datos del formulario
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 26/Abril/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///***********************************************************************************************************************
 $(document).ready(function() {
     try
     {
     
        $('#Div_Psp').window('close');
        $('#Div_Mostrar_PDF').window('close');
        
        $('#Cmb_Tipo_Psp').combobox({
            panelHeight:"auto",
            onHidePanel:function()
            {
                Limpiar_Variables();
                if($('#Cmb_Tipo_Psp').combobox('getValue') == "ING")
                {
                    $("#Div_Filtros_Egresos").css("display","none");
                    $("#Div_Filtros_Ingresos").css("display","block");
                }else if($('#Cmb_Tipo_Psp').combobox('getValue') == "EGR")
                {
                    $("#Div_Filtros_Egresos").css("display","block");
                    $("#Div_Filtros_Ingresos").css("display","none");
                }else
                {
                    $("#Div_Filtros_Egresos").css("display","block");
                    $("#Div_Filtros_Ingresos").css("display","block");
                }
                Llenar_Combo_Anios();
            }
        });
        
        Obtener_Tipo_Usuario(); //obtenemos el tipo de usuario para mostrar los filtros a los que tendra derecho
        
        $('#Cmb_Meses').combobox({
            panelHeight:"auto"
        });
        
        Llenar_Combo_Anios();
         
        //Evento del boton de salir
        $("input[id$=Btn_Salir]").click(function (e) {
            e.preventDefault();
            location.href='../Paginas_Generales/Frm_Apl_Principal.aspx';
        });
        
        //Evento del boton generar clasificacion
        $("input[id$=Btn_Generar]").click(function (e) {
            e.preventDefault();
            Obtener_Presupuesto();
        });
        
        //boton para generar el reporte de excel
        $("input[id$=Btn_Exportar_Excel]").click(function (e) {
            e.preventDefault();
            Generar_Reporte('Excel');
        });
        
        //boton para generar el reporte de word
        $("input[id$=Btn_Exportar_Word]").click(function (e) {
            e.preventDefault();
            Generar_Reporte('Word');
        });
        
        //boton para generar el reporte de pdf
        $("input[id$=Btn_Exportar_Pdf]").click(function (e) {
            e.preventDefault();
            Generar_Reporte('PDF');
        });
            
        //boton para generar el reporte de actualizar los registros  del grid
        $("input[id$=Btn_Actualizar]").click(function (e) {
            e.preventDefault();
            Obtener_Presupuesto();
        });
        
        //Evento del boton de expandir
        $("input[id$=Btn_Expandir]").click(function (e) {
            e.preventDefault();
            if($('#Cmb_Tipo_Psp').combobox('getValue') == "ING"){
                $('#Grid_Psp_Ing').treegrid('expandAll');
            }
            else if($('#Cmb_Tipo_Psp').combobox('getValue') == "EGR"){
                $('#Grid_Psp_Egr').treegrid('expandAll');
            }else{
                $('#Grid_Psp_Ing').treegrid('expandAll');
                $('#Grid_Psp_Egr').treegrid('expandAll');
            }
        });
        
        //Evento del boton de contraer
        $("input[id$=Btn_Contraer]").click(function (e) {
            e.preventDefault();
            if($('#Cmb_Tipo_Psp').combobox('getValue') == "ING"){
                $('#Grid_Psp_Ing').treegrid('collapseAll');
            }
            else if($('#Cmb_Tipo_Psp').combobox('getValue') == "EGR"){
                $('#Grid_Psp_Egr').treegrid('collapseAll');
            }else{
                $('#Grid_Psp_Ing').treegrid('collapseAll');
                $('#Grid_Psp_Egr').treegrid('collapseAll');
            }
        });
        
        $('#Txt_Fecha_Ini').datebox({
            editable:false //indicamos que no se pueda escribir en el txt de la fecha
        });
        
         $('#Txt_Fecha_Fin').datebox({
            editable:false //indicamos que no se pueda escribir en el txt de la fecha
        });
    }catch(Ex){
        $.messager.alert('Mensaje','Error al ejecutar los eventos de la página. Error: [' + Ex + ']','error');
    }
 });
 
//*********************************************************************************************************************************************************
//************************************************************** METODOS **********************************************************************************
//*********************************************************************************************************************************************************

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Btn_Obtener_Presupuesto
///DESCRIPCIÓN          : Funcion para obtener los datos del presupuesto
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 17/Agosto/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Obtener_Presupuesto()
{
    try
    {
        if($('#Cmb_Anio').combobox('getValue') != ""){
             $('#Div_Psp').window({
	                title: 'Presupuesto',//texto del título de la ventana
	                width: 1200, // largo de la ventana
	                modal: true, // define si la ventana es una ventana modal
	                shadow: false, //para mostrar una sombra al abrir la ventana
	                closed: false, // definido para cerrar la ventana 
	                closable:true, // que muestre el boton de cerrado
	                collapsible:false, //define si mostrar el control de plegable( es decir que ya no se muestre el contenido pero si los botones)
	                maximizable:false, // para mostrar el control de maximizar
	                minimizable:false, //para mostrar el control de minimizar
	                draggable:true,// define si la ventana se puede arrastrar
	                top: 5, //indica la altura de la posicion donde se abrira la ventana
	                left:5
                });
                
                //limpiamos las variables
                Limpiar_Variables();
                
                if($('#Cmb_Tipo_Psp').combobox('getValue') == "ING")
                {
                    //validamos el nivel que se mostrara en el grid
                    Validar_Chk_Ing();
                    Generar_TreeGrid_Ing(Nivel_Ing, Estado, 480);
                    $('#Grid_Psp_Ing').treegrid('reload'); //recargamos el grid de ingresos para actualizar los datos
                    $("#Td_Grid_Psp_Ing").css("display","block"); // habilidatos el grid de ingresos
                    $("#Td_Grid_Psp_Egr").css("display","none"); // habilitamos el grid de egresos
                    $("#Td_Botones_Reportes").css("display","block"); //habilidatos los botones de los reportes
                }else if($('#Cmb_Tipo_Psp').combobox('getValue') == "EGR")
                {
                    //validamos el nivel que se mostrara en el grid
                    Validar_Chk_Egr();
                    Generar_TreeGrid_Egr(Nivel_Egr, Estado_Egr, 480);
                    $('#Grid_Psp_Egr').treegrid('reload');
                    $("#Td_Grid_Psp_Ing").css("display","none");
                    $("#Td_Grid_Psp_Egr").css("display","block");
                    $("#Td_Botones_Reportes").css("display","block"); //habilidatos los botones de los reportes
                }
                else
                {
                    //validamos el nivel que se mostrara en el grid
                    Validar_Chk_Ing();
                    Validar_Chk_Egr();
                    Generar_TreeGrid_Ing(Nivel_Ing, Estado, 320);
                    Generar_TreeGrid_Egr(Nivel_Egr, Estado_Egr, 320);
                    $('#Grid_Psp_Egr').treegrid('reload');
                    $('#Grid_Psp_Ing').treegrid('reload');
                    $("#Td_Grid_Psp_Ing").css("display","block");
                    $("#Td_Grid_Psp_Egr").css("display","block");
                    $("#Td_Botones_Reportes").css("display","none"); //deshabilidatos los botones de los reportes
                }
                $("#Td_Iconos").css("display","block");
                $("#Td_Grid_Det_Mov_Ing").css("display","none");
                $("#Td_Grid_Det_Rec_Ing").css("display","none");
                $("#Td_Grid_Det_Mov_Egr").css("display","none");
                $("#Td_Grid_Det_Egr").css("display","none");
                $("#Td_Mostrar_PFD").css("display","none");
        }else{
            $.messager.alert('Mensaje',"Favor de seleccionar un año",'warning');
        }
    }catch(Ex){
        $.messager.alert('Mensaje','Error al obtener los datos del presupuesto. Error: [' + Ex + ']','error');
    }
}


///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Obtener_Tipo_Usuario
///DESCRIPCIÓN          : Funcion para obtener el tipo de usuario
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 17/Agosto/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Obtener_Tipo_Usuario(){
    var parametros = "";
    var Usuario = "";
    var UR_ID = "";
    $("[id$=Hf_Tipo_Usuario]").val("");
    $("[id$=Hf_Dep_ID]").val("");
    
    try{
        $.ajax({
            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=Usuarios",
            type:'POST',
            async: false,
            cache: false,
            success: function(Datos) {
                 if (Datos != null) {
                    parametros = Datos.split('/');  
                    if(parametros.length > 0){
                        Usuario = parametros[0];
                        UR_ID = parametros[1];
                        $("[id$=Hf_Tipo_Usuario]").val(Usuario);
                        $("[id$=Hf_Dep_ID]").val(UR_ID);
                        Habilitar_Ocultar_Controles();
                    }
                 }
                 else {
                      $.messager.alert('Mensaje',"No se obtubo datos del usuario",'info');
                 }
            }
        });
    }catch(Ex){
        $.messager.alert('Mensaje','Error al obtener los datos del usuario. Error: [' + Ex + ']','error');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Habilitar_Ocultar_Controles
///DESCRIPCIÓN          : Funcion para mostrar los campos de acuerdo al usuario
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 17/Agosto/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Habilitar_Ocultar_Controles(){
    var Tipo_Usuario = $("[id$=Hf_Tipo_Usuario]").val();
    var UR = $("[id$=Hf_Dep_ID]").val();
    try{
        if (Tipo_Usuario == "Usuario")
        {
            $("#Div_Filtros_Egresos").css("display","block");
            $("#Div_Filtros_Ingresos").css("display","none");
            $('#Cmb_Tipo_Psp').combobox('setValue', 'EGR');
            $('#Cmb_Tipo_Psp').combobox('disable');
            $('#Cmb_UR').combobox('disable');
            $('#Cmb_UR_F').combobox('disable');
        }
        else if (Tipo_Usuario == "Coordinador" || Tipo_Usuario =="Inversiones" || Tipo_Usuario =="Nomina")
        {
            $("#Div_Filtros_Egresos").css("display","block");
            $("#Div_Filtros_Ingresos").css("display","none");
            $('#Cmb_Tipo_Psp').combobox('setValue', 'EGR');
            $('#Cmb_Tipo_Psp').combobox('disable');
            $('#Cmb_UR').combobox('enable');
            $('#Cmb_UR_F').combobox('enable');
        }
        else if (Tipo_Usuario == "Administrador")
        {
            $("#Div_Filtros_Egresos").css("display","block");
            $("#Div_Filtros_Ingresos").css("display","block");
            $('#Cmb_Tipo_Psp').combobox('enable');
            $('#Cmb_UR').combobox('enable');
            $('#Cmb_UR_F').combobox('enable');
//             $('#Cmb_Tipo_Psp').combobox('setValue', 'ING');
//             $("#Div_Filtros_Egresos").css("display","none");
        }
    }catch(Ex){
        $.messager.alert('Mensaje','Error al obtener los datos del usuario. Error: [' + Ex + ']','error');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Progress
///DESCRIPCIÓN          : Funcion para mostrar el progress
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 26/Abril/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Activar_Progress(Accion, Intervalo){
    var win;
    
    if(Accion == "Abrir")
    {
        win = $.messager.progress({
			title:'Espere Por Favor',
			msg:'Cargando...',
			interval: Intervalo
		});
    }
    else
    {
        setTimeout(function(){
		    $.messager.progress('close');
	    },5000)
    }
}


///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Anios
///DESCRIPCIÓN          : Funcion para llenar el combo de los anios
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 26/Abril/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Llenar_Combo_Anios()
{
    try{
        var Parametros = Obtener_Parametros();
    
        $('#Cmb_Anio').combobox('clear');
        $('#Cmb_Anio').combobox({
            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=Anios" + Parametros,
            valueField: 'ANIO_ID',
            textField: 'ANIO',
            mode:'remote',
            method: 'post',
            panelHeight:"auto",
            onHidePanel:function()
            {
                if($('#Cmb_Tipo_Psp').combobox('getValue') == "ING")
                {
                    Llenar_Combo_FF_Ing();
                    Llenar_Combo_Programas_Ing();
                    Llenar_Combo_Rubro();
                    Llenar_Combo_Tipo();
                    Llenar_Combo_Clase();
                    Llenar_Combo_Conceptos_Ing();
                    Llenar_Combo_SubConceptos();
                }else if($('#Cmb_Tipo_Psp').combobox('getValue') == "EGR")
                {
                    Llenar_Combo_FF();
                    Llenar_Combo_AF();
                    Llenar_Combo_UR()
                    Llenar_Combo_Programas();
                    Llenar_Combo_Capitulo();
                    Llenar_Combo_Concepto();
                    Llenar_Combo_Partida_Generica();
                    Llenar_Combo_Partida();
                }else
                {
                    Llenar_Combo_FF();
                    Llenar_Combo_AF();
                    Llenar_Combo_UR()
                    Llenar_Combo_Programas();
                    Llenar_Combo_Capitulo();
                    Llenar_Combo_Concepto();
                    Llenar_Combo_Partida_Generica();
                    Llenar_Combo_Partida();
                    Llenar_Combo_FF_Ing();
                    Llenar_Combo_Programas_Ing();
                    Llenar_Combo_Rubro();
                    Llenar_Combo_Tipo();
                    Llenar_Combo_Clase();
                    Llenar_Combo_Conceptos_Ing();
                    Llenar_Combo_SubConceptos();
                }
            }
        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar el combo de años presupuestados. Error: [' + Ex + ']','error');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Llenar_Combo_FF
///DESCRIPCIÓN          : Funcion para llenar el combo de las Fuentes de Financiamiento
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 26/Abril/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Llenar_Combo_FF()
{
    try{
        var Parametros = Obtener_Parametros();
        $('#Cmb_FF').combobox('clear');
        $('#Cmb_FF').combobox({
            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=FF" + Parametros,
            valueField: 'FUENTE_FINANCIAMIENTO_ID',
            textField: 'CLAVE_NOMBRE',
            mode:'remote',
            method: 'post',
            panelHeight:"auto",
            onHidePanel:function()
            {
                Llenar_Combo_AF();
                Llenar_Combo_Programas();
                Llenar_Combo_Capitulo();
                Llenar_Combo_Concepto();
                Llenar_Combo_Partida_Generica();
                Llenar_Combo_Partida();
            }
        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar el combo de Fuentes de Financiamiento. Error: [' + Ex + ']','error');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Llenar_Combo_AF
///DESCRIPCIÓN          : Funcion para llenar el combo de las areas funcionales
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 26/Abril/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Llenar_Combo_AF()
{
    try{
        var Parametros = Obtener_Parametros();
        
        $('#Cmb_AF').combobox('clear');
        $('#Cmb_AF').combobox({
            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=AF" + Parametros,
            valueField: 'AREA_FUNCIONAL_ID',
            textField: 'CLAVE_NOMBRE',
            mode:'remote',
            method: 'post',
            onHidePanel:function()
            {
                Llenar_Combo_Programas();
                Llenar_Combo_UR()
                Llenar_Combo_Capitulo();
                Llenar_Combo_Concepto();
                Llenar_Combo_Partida_Generica();
                Llenar_Combo_Partida();
            }
        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar el combo de las areas funcionales. Error: [' + Ex + ']','error');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Llenar_Combo_UR
///DESCRIPCIÓN          : Funcion para llenar el combo de las unidades responsables
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 23/Abril/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Llenar_Combo_UR()
{
    try{
       var Parametros = Obtener_Parametros();
        
        $('#Cmb_UR').combobox('clear');
        $('#Cmb_UR').combobox({
            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=UR" + Parametros,
            valueField: 'DEPENDENCIA_ID',
            textField: 'CLAVE_NOMBRE',
            mode:'remote',
            method: 'post',
            onHidePanel:function()
            {
                Llenar_Combo_Capitulo();
                Llenar_Combo_Concepto();
                Llenar_Combo_Partida_Generica();
                Llenar_Combo_Partida();
            }
        });
//        $('#Cmb_UR_F').combobox('clear');
//        $('#Cmb_UR_F').combobox({
//            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=UR" + Parametros,
//            valueField: 'DEPENDENCIA_ID',
//            textField: 'CLAVE_NOMBRE',
//            mode:'remote',
//            method: 'post'
//        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar el combo de unidades responsables. Error: [' + Ex + ']','error');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Programas
///DESCRIPCIÓN          : Funcion para llenar el combo de los proyectos y programas
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 23/Abril/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Llenar_Combo_Programas()
{
    try{
       var Parametros = Obtener_Parametros();
        
        $('#Cmb_PP').combobox('clear');
        $('#Cmb_PP').combobox({
            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=PP" + Parametros,
            valueField: 'PROYECTO_PROGRAMA_ID',
            textField: 'CLAVE_NOMBRE',
            mode:'remote',
            method: 'post',
            onHidePanel:function()
            {
                Llenar_Combo_UR();
                Llenar_Combo_Capitulo();
                Llenar_Combo_Concepto();
                Llenar_Combo_Partida_Generica();
                Llenar_Combo_Partida();
            }
        });
        
//        $('#Cmb_PP_F').combobox('clear');
//        $('#Cmb_PP_F').combobox({
//            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=PP" + Parametros,
//            valueField: 'PROYECTO_PROGRAMA_ID',
//            textField: 'CLAVE_NOMBRE',
//            mode:'remote',
//            method: 'post'
//        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar el combo de proyectos programas. Error: [' + Ex + ']','error');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Capitulo
///DESCRIPCIÓN          : Funcion para llenar el combo de los capitulos
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 23/Abril/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Llenar_Combo_Capitulo()
{
    try{
       var Parametros = Obtener_Parametros();
        
        $('#Cmb_Cap').combobox('clear');
        $('#Cmb_Cap').combobox({
            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=CAP" + Parametros,
            valueField: 'CAPITULO_ID',
            textField: 'CLAVE_NOMBRE',
            mode:'remote',
            method: 'post',
            panelHeight:"auto",
            onHidePanel:function()
            {
                Llenar_Combo_Concepto();
                Llenar_Combo_Partida_Generica();
                Llenar_Combo_Partida();
            }
        });
        
//        $('#Cmb_Cap_F').combobox('clear');
//        $('#Cmb_Cap_F').combobox({
//            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=CAP" + Parametros,
//            valueField: 'CAPITULO_ID',
//            textField: 'CLAVE_NOMBRE',
//            mode:'remote',
//            method: 'post',
//            panelHeight:"auto"
//        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar el combo de los capitulos. Error: [' + Ex + ']','error');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Concepto
///DESCRIPCIÓN          : Funcion para llenar el combo de los conceptos
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 23/Abril/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Llenar_Combo_Concepto()
{
    try{
       var Parametros = Obtener_Parametros();
        
        $('#Cmb_Con').combobox('clear');
        $('#Cmb_Con').combobox({
            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=CON" + Parametros,
            valueField: 'CONCEPTO_ID',
            textField: 'CLAVE_NOMBRE',
            mode:'remote',
            method: 'post',
            onHidePanel:function()
            {
                Llenar_Combo_Partida_Generica();
                Llenar_Combo_Partida();
            }
        });
//        $('#Cmb_Con_F').combobox('clear');
//        $('#Cmb_Con_F').combobox({
//            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=CON" + Parametros,
//            valueField: 'CONCEPTO_ID',
//            textField: 'CLAVE_NOMBRE',
//            mode:'remote',
//            method: 'post'
//        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar el combo de los conceptos. Error: [' + Ex + ']','error');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Partida_Generica
///DESCRIPCIÓN          : Funcion para llenar el combo de las partidas genericas
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 23/Abril/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Llenar_Combo_Partida_Generica()
{
    try{
       var Parametros = Obtener_Parametros();
        
        $('#Cmb_PG').combobox('clear');
        $('#Cmb_PG').combobox({
            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=PG" + Parametros,
            valueField: 'PARTIDA_GENERICA_ID',
            textField: 'CLAVE_NOMBRE',
            mode:'remote',
            method: 'post',
            onHidePanel:function()
            {
                Llenar_Combo_Partida();
            }
        });
//        $('#Cmb_PG_F').combobox('clear');
//        $('#Cmb_PG_F').combobox({
//            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=PG" + Parametros,
//            valueField: 'PARTIDA_GENERICA_ID',
//            textField: 'CLAVE_NOMBRE',
//            mode:'remote',
//            method: 'post'
//        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar el combo de los partidas genericas. Error: [' + Ex + ']','error');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Partida
///DESCRIPCIÓN          : Funcion para llenar el combo de las partidas
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 23/Abril/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Llenar_Combo_Partida()
{
    try{
       var Parametros = Obtener_Parametros();
        
        $('#Cmb_P').combobox('clear');
        $('#Cmb_P').combobox({
            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=P" + Parametros,
            valueField: 'PARTIDA_ID',
            textField: 'CLAVE_NOMBRE',
            mode:'remote',
            method: 'post'
        });
//        $('#Cmb_P_F').combobox('clear');
//        $('#Cmb_P_F').combobox({
//            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=P" + Parametros,
//            valueField: 'PARTIDA_ID',
//            textField: 'CLAVE_NOMBRE',
//            mode:'remote',

//            method: 'post'
//        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar el combo de las partidas. Error: [' + Ex + ']','error');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Obtener_Parametros
///DESCRIPCIÓN          : Funcion para obtener los parametros de los controles
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 23/Abril/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Obtener_Parametros()
{
    var Tipo_Psp =  $('#Cmb_Tipo_Psp').combobox('getValue');
    var Mes =  $('#Cmb_Meses').combobox('getValue');
    var Fecha_Ini =  $('#Txt_Fecha_Ini').datebox('getValue');
    var Fecha_Fin =  $('#Txt_Fecha_Fin').datebox('getValue');
    
    if($('#Cmb_Anio').length > 0){
        var Anio =  $('#Cmb_Anio').combobox('getValue');
    }else{
        var Anio = "";
    }
    
    if($('#Cmb_FF').length > 0){
        var FF =  $('#Cmb_FF').combobox('getValue');
    }else{
        var FF = "";
    }
    
    if($('#Cmb_FF_Ing').length > 0){
        var FF_Ing =  $('#Cmb_FF_Ing').combobox('getValue');
    }else{
        var FF_Ing =  "";
    }

    if($('#Cmb_PP_Ing').length > 0){
        var PP_Ing =  $('#Cmb_PP_Ing').combobox('getValue');
    }else{
        var PP_Ing = "";
    }
    
    if($('#Cmb_PP_Ing_F').length > 0){
        var PP_Ing_F =  $('#Cmb_PP_Ing_F').combobox('getValue');
    }else{
        var PP_Ing_F = "";
    }
    
    if($('#Cmb_AF').length > 0){
        var AF =  $('#Cmb_AF').combobox('getValue');
    }else{
        var AF = "";
    }

    if($('#Cmb_UR').length > 0){
        var UR =  $('#Cmb_UR').combobox('getValue');
    }else{
        var UR = "";
    }
    
     if($('#Cmb_UR_F').length > 0){
        var UR_F =  $('#Cmb_UR_F').combobox('getValue');
    }else{
        var UR_F = "";
    }
    
    if($('#Cmb_PP').length > 0){
        var PP =  $('#Cmb_PP').combobox('getValue');
    }else{
        var PP = "";
    }
    
    if($('#Cmb_PP_F').length > 0){
        var PP_F =  $('#Cmb_PP_F').combobox('getValue');
    }else{
        var PP_F = "";
    }

    if($('#Cmb_Cap').length > 0){
        var Cap =  $('#Cmb_Cap').combobox('getValue');
    }else{
        var Cap = "";
    }
    
    if($('#Cmb_Cap_F').length > 0){
        var Cap_F =  $('#Cmb_Cap_F').combobox('getValue');
    }else{
        var Cap_F = "";
    }

    if($('#Cmb_Con').length > 0){
        var Con =  $('#Cmb_Con').combobox('getValue');
    }else{
        var Con = "";
    }
    
    if($('#Cmb_Con_F').length > 0){
        var Con_F =  $('#Cmb_Con_F').combobox('getValue');
    }else{
        var Con_F = "";
    }

    if($('#Cmb_PG').length > 0){
        var PG =  $('#Cmb_PG').combobox('getValue');
    }else{
        var PG = "";
    }

    if($('#Cmb_PG_F').length > 0){
        var PG_F =  $('#Cmb_PG_F').combobox('getValue');
    }else{
        var PG_F = "";
    }

    if($('#Cmb_P').length > 0){
        var P =  $('#Cmb_P').combobox('getValue');
    }else{
        var P = "";
    }

    if($('#Cmb_P_F').length > 0){
        var P_F =  $('#Cmb_P_F').combobox('getValue');
    }else{
        var P_F = "";
    }

    if($('#Cmb_R').length > 0){
        var R =  $('#Cmb_R').combobox('getValue');
    }else{
        var R = "";
    }
    
    if($('#Cmb_R_F').length > 0){
        var R_F =  $('#Cmb_R_F').combobox('getValue');
    }else{
        var R_F = "";
    }
    
    if($('#Cmb_T').length > 0){
        var T =  $('#Cmb_T').combobox('getValue');
    }else{
        var T = "";
    }
    
    if($('#Cmb_T_F').length > 0){
        var T_F =  $('#Cmb_T_F').combobox('getValue');
    }else{
        var T_F = "";
    }
    
    if($('#Cmb_Cl').length > 0){
        var Cl =  $('#Cmb_Cl').combobox('getValue');
    }else{
        var Cl = "";
    }
   
    if($('#Cmb_Cl_F').length > 0){
        var Cl_F =  $('#Cmb_Cl_F').combobox('getValue');
    }else{
        var Cl_F = "";
    }
   
    if($('#Cmb_Con_Ing').length > 0){
        var Con_Ing =  $('#Cmb_Con_Ing').combobox('getValue');
    }else{
        var Con_Ing = "";
    }
   
    if($('#Cmb_Con_Ing_F').length > 0){
        var Con_Ing_F =  $('#Cmb_Con_Ing_F').combobox('getValue');
    }else{
        var Con_Ing_F = "";
    }
    
    if($('#Cmb_SubCon').length > 0){
        var SubCon =  $('#Cmb_SubCon').combobox('getValue');
    }else{
        var SubCon = "";
    }

    if($('#Cmb_SubCon_F').length > 0){
        var SubCon_F =  $('#Cmb_SubCon_F').combobox('getValue');
    }else{
        var SubCon_F = "";
    }

    //validamos los detalles de ingresos
    if(Det_FFI.length > 0){
        FF_Ing = Det_FFI;
    }
    if(Det_PPI.length > 0){
        PP_Ing = Det_PPI;
    }
    if(Det_Rub.length > 0){
        R = Det_Rub;
    }
    if(Det_Tip.length > 0){
        T = Det_Tip;
    }
    if(Det_Cla.length > 0){
        Cl = Det_Cla;
    }
    if(Det_CoI.length > 0){
        Con_Ing = Det_CoI;
    }
    if(Det_Sub.length > 0){
        SubCon = Det_Sub;
    }
    
    //validamos los detalles de egresos
    if(Det_FF.length > 0){
        FF = Det_FF;
    }
    if(Det_PP.length > 0){
        PP = Det_PP;
    }
    if(Det_Cap.length > 0){
        Cap = Det_Cap;
    }
    if(Det_Con.length > 0){
        Con = Det_Con;
    }
    if(Det_PG.length > 0){
        PG = Det_PG;
    }
    if(Det_PE.length > 0){
        P = Det_PE;
    }
    if(Det_UR.length > 0){
        UR = Det_UR;
    }
    if(Det_AF.length > 0){
        AF = Det_AF;
    }
    

    var Parametros = "&Anio=" + Anio + "&Tipo_Psp=" + Tipo_Psp + "&Mes=" + Mes + "&Fecha_Ini=" + Fecha_Ini + "&Fecha_Fin=" + Fecha_Fin;
    Parametros += "&FF=" + FF + "&AF=" + AF + "&FF_Ing=" + FF_Ing + "&UR=" + UR + "&PP=" + PP + "&PP_Ing=" + PP_Ing + "&Cap=" + Cap + "&Con=" + Con + "&PG=" + PG + "&P=" + P;
    Parametros += "&UR_F=" + UR_F + "&PP_F=" + PP_F + "&PP_Ing_F=" + PP_Ing_F+  "&Cap_F=" + Cap_F + "&Con_F=" + Con_F + "&PG_F=" + PG_F + "&P_F=" + P_F;
    Parametros += "&R=" + R + "&T=" + T + "&Cl=" + Cl + "&Con_Ing=" + Con_Ing + "&SubCon=" + SubCon;
    Parametros += "&R_F=" + R_F + "&T_F=" + T_F + "&Cl_F=" + Cl_F + "&Con_Ing_F=" + Con_Ing_F + "&SubCon_F=" + SubCon_F;
    
    return Parametros;
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Llenar_Combo_FF_Ing
///DESCRIPCIÓN          : Funcion para llenar el combo de las Fuentes de Financiamiento
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 26/Abril/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Llenar_Combo_FF_Ing()
{
    try{
        var Parametros = Obtener_Parametros();
        
        $('#Cmb_FF_Ing').combobox('clear');
        $('#Cmb_FF_Ing').combobox({
            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=FF_Ing" + Parametros,
            valueField: 'FUENTE_FINANCIAMIENTO_ID',
            textField: 'CLAVE_NOMBRE',
            mode:'remote',
            method: 'post',
            panelHeight:"auto",
            onHidePanel:function()
            {
                Llenar_Combo_Programas_Ing();
                Llenar_Combo_Rubro();
                Llenar_Combo_Tipo();
                Llenar_Combo_Clase();
                Llenar_Combo_Conceptos_Ing();
                Llenar_Combo_SubConceptos();
            }
        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar el combo de Fuentes de Financiamiento. Error: [' + Ex + ']','error');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Programas
///DESCRIPCIÓN          : Funcion para llenar el combo de los proyectos y programas
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 23/Abril/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Llenar_Combo_Programas_Ing()
{
    try{
       var Parametros = Obtener_Parametros();
        
        $('#Cmb_PP_Ing').combobox('clear');
        $('#Cmb_PP_Ing').combobox({
            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=PP_Ing" + Parametros,
            valueField: 'PROYECTO_PROGRAMA_ID',
            textField: 'CLAVE_NOMBRE',
            mode:'remote',
            method: 'post',
            onHidePanel:function()
            {
                Llenar_Combo_Rubro();
                Llenar_Combo_Tipo();
                Llenar_Combo_Clase();
                Llenar_Combo_Conceptos_Ing();
                Llenar_Combo_SubConceptos();
            }
        });
        
//        $('#Cmb_PP_Ing_F').combobox('clear');
//        $('#Cmb_PP_Ing_F').combobox({
//            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=PP_Ing" + Parametros,
//            valueField: 'PROYECTO_PROGRAMA_ID',
//            textField: 'CLAVE_NOMBRE',
//            mode:'remote',
//            method: 'post'
//        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar el combo de proyectos programas. Error: [' + Ex + ']','error');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Rubro
///DESCRIPCIÓN          : Funcion para llenar el combo de los rubros
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 23/Mayo/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Llenar_Combo_Rubro()
{
    try{
        var Parametros = Obtener_Parametros();
        
        $('#Cmb_R').combobox('clear');
        $('#Cmb_R').combobox({
            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=R" + Parametros,
            valueField: 'RUBRO_ID',
            textField: 'CLAVE_NOMBRE',
            mode:'remote',
            method: 'post',
            panelHeight:"auto",
            onHidePanel:function()
            {
                Llenar_Combo_Tipo();
                Llenar_Combo_Clase();
                Llenar_Combo_Conceptos_Ing();
                Llenar_Combo_SubConceptos();
            }
        });
//        $('#Cmb_R_F').combobox('clear');
//        $('#Cmb_R_F').combobox({
//            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=R" + Parametros,
//            valueField: 'RUBRO_ID',
//            textField: 'CLAVE_NOMBRE',
//            mode:'remote',
//            method: 'post',
//            panelHeight:"auto"
//        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar el combo de los rubros. Error: [' + Ex + ']','error');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Tipo
///DESCRIPCIÓN          : Funcion para llenar el combo de los tipos
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 23/Mayo/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Llenar_Combo_Tipo()
{
    try{
        var Parametros = Obtener_Parametros();
        
        $('#Cmb_T').combobox('clear');
        $('#Cmb_T').combobox({
            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=T" + Parametros,
            valueField: 'TIPO_ID',
            textField: 'CLAVE_NOMBRE',
            mode:'remote',
            method: 'post',
            onHidePanel:function()
            {
                Llenar_Combo_Clase();
                Llenar_Combo_Conceptos_Ing();
                Llenar_Combo_SubConceptos();
            }
        });
//        $('#Cmb_T_F').combobox('clear');
//        $('#Cmb_T_F').combobox({
//            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=T" + Parametros,
//            valueField: 'TIPO_ID',
//            textField: 'CLAVE_NOMBRE',
//            mode:'remote',
//            method: 'post'
//        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar el combo de tipos. Error: [' + Ex + ']','error');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Clase
///DESCRIPCIÓN          : Funcion para llenar el combo de los clase
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 23/Mayo/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Llenar_Combo_Clase()
{
    try{
        var Parametros = Obtener_Parametros();
        
        $('#Cmb_Cl').combobox('clear');
        $('#Cmb_Cl').combobox({
            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=CL" + Parametros,
            valueField: 'CLASE_ING_ID',
            textField: 'CLAVE_NOMBRE',
            mode:'remote',
            method: 'post',
            onHidePanel:function()
            {
                Llenar_Combo_Conceptos_Ing();
                Llenar_Combo_SubConceptos();
            }
        });
//        $('#Cmb_Cl_F').combobox('clear');
//        $('#Cmb_Cl_F').combobox({
//            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=CL" + Parametros,
//            valueField: 'CLASE_ING_ID',
//            textField: 'CLAVE_NOMBRE',
//            mode:'remote',
//            method: 'post'
//        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar el combo de clases. Error: [' + Ex + ']','error');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Conceptos_Ing
///DESCRIPCIÓN          : Funcion para llenar el combo de los conceptos
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 23/Mayo/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Llenar_Combo_Conceptos_Ing()
{
    try{
        var Parametros = Obtener_Parametros();
        
        $('#Cmb_Con_Ing').combobox('clear');
        $('#Cmb_Con_Ing').combobox({
            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=CON_ING" + Parametros,
            valueField: 'CONCEPTO_ING_ID',
            textField: 'CLAVE_NOMBRE',
            mode:'remote',
            method: 'post',
            onHidePanel:function()
            {
                Llenar_Combo_SubConceptos();
            }
        });
//        $('#Cmb_Con_Ing_F').combobox('clear');
//        $('#Cmb_Con_Ing_F').combobox({
//            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=CON_ING" + Parametros,
//            valueField: 'CONCEPTO_ING_ID',
//            textField: 'CLAVE_NOMBRE',
//            mode:'remote',
//            method: 'post'
//        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar el combo de conceptos. Error: [' + Ex + ']','error');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Llenar_Combo_SubConceptos
///DESCRIPCIÓN          : Funcion para llenar el combo de los subconceptos
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 23/Mayo/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Llenar_Combo_SubConceptos()
{
    try{
        var Parametros = Obtener_Parametros();
        
        $('#Cmb_SubCon').combobox('clear');
        $('#Cmb_SubCon').combobox({
            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=SUBCON" + Parametros,
            valueField: 'SUBCONCEPTO_ING_ID',
            textField: 'CLAVE_NOMBRE',
            mode:'remote',
            method: 'post'
        });
//        $('#Cmb_SubCon_F').combobox('clear');
//        $('#Cmb_SubCon_F').combobox({
//            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=SUBCON" + Parametros,
//            valueField: 'SUBCONCEPTO_ING_ID',
//            textField: 'CLAVE_NOMBRE',
//            mode:'remote',
//            method: 'post'
//        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar el combo de Subconceptos. Error: [' + Ex + ']','error');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Concepto
///DESCRIPCIÓN          : Funcion para llenar el combo de los conceptos
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 26/Mayo/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
 function Generar_TreeGrid_Ing(Nivel_Ingresos, Estado_Ing, Largo)
 {
    try
    {
        var Parametros = Obtener_Parametros();
        
        $('#Grid_Psp_Ing').treegrid({
		    title:'CRI',
		    width:1150,
		    height:Largo,
		    rownumbers: false,
		    animate:true, //Define si para mostrar el efecto de animación al nodo expandir o contraer. 	
		    collapsible:false,
		    url:'Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=' + Nivel_Ingresos + '&Estado=' + Estado_Ing  + "&Tot='SI'" + Parametros,
		    idField:'id',
		    treeField:'texto',
		    resizable:true,
		    nowrap:true,
		    showFooter:true,
		    frozenColumns:[[
                {title:'Nombre',field:'texto',width:360,
                    formatter:function(value){
                	    return '<span style="color:Navy; font-size:8pt">'+value+'</span>';
                    }
                }
		    ]],
		    columns:[[
		        {field:'descripcion1',title:'Estimado',width:108, align: 'right',
			             formatter:function(value){
                	        return '<span style="font-size:8pt">'+value+'</span>';
                        }
			    },
			    {field:'descripcion2',title:'Ampliación',width:108, align: 'right',
		             formatter:function(value,row){
		                if(parseFloat(value.replace(/,/gi,'')) > 0)
		                {
		                    var niv = row.attributes.valor1;
		                    var rub = parseInt(row.attributes.valor2.match(/([0-9])$/) ? row.attributes.valor2 : '0',10);
		                    var tip = parseInt(row.attributes.valor3.match(/([0-9])$/) ? row.attributes.valor3 : '0',10);
		                    var cla = parseInt(row.attributes.valor4.match(/([0-9])$/) ? row.attributes.valor4 : '0',10);
		                    var con = parseInt(row.attributes.valor5.match(/([0-9])$/) ? row.attributes.valor5 : '0',10);
		                    var sub = parseInt(row.attributes.valor6.match(/([0-9])$/) ? row.attributes.valor6 : '0',10);
		                    var fue = parseInt(row.attributes.valor7.match(/([0-9])$/) ? row.attributes.valor7 : '0',10);
		                    var pro = parseInt(row.attributes.valor8.match(/([0-9])$/) ? row.attributes.valor8 : '0',10);
			                
		                    if(niv == "Nivel_FF_Ing"){niv = 1;}
		                    else if(niv == "Nivel_PP_Ing"){niv = 2;}
		                    else if(niv == "Nivel_Rubro"){niv = 3;}
		                    else if(niv == "Nivel_Tipo"){niv = 4;}
		                    else if(niv == "Nivel_Clase"){niv = 5;}
		                    else if(niv == "Nivel_Concepto_Ing"){niv = 6;}
		                    else if(niv == "Nivel_SubConcepto"){niv = 7;}
                            else {niv = 0;}
                            
                            if(niv > 0){return '<span><a onclick="Detalles_Mov_Ing(1,'+niv+','+rub+','+tip+','+cla+','+con+','+sub+','+fue+','+sub+');" style="font-size:8pt; cursor:pointer;">'+value+'</a></span>';}
                            else{return '<span style="font-size:8pt">'+value+'</span>';}
		                }else
		                {
		                    return '<span style="font-size:8pt">'+value+'</span>';
		                }
                    }
			    },
		        {field:'descripcion3',title:'Reducción',width:108, align: 'right',
			            formatter:function(value,row){
		                if(parseFloat(value.replace(/,/gi,'')) > 0)
		                {
		                    var niv = row.attributes.valor1;
		                    var rub = parseInt(row.attributes.valor2.match(/([0-9])$/) ? row.attributes.valor2 : '0',10);
		                    var tip = parseInt(row.attributes.valor3.match(/([0-9])$/) ? row.attributes.valor3 : '0',10);
		                    var cla = parseInt(row.attributes.valor4.match(/([0-9])$/) ? row.attributes.valor4 : '0',10);
		                    var con = parseInt(row.attributes.valor5.match(/([0-9])$/) ? row.attributes.valor5 : '0',10);
		                    var sub = parseInt(row.attributes.valor6.match(/([0-9])$/) ? row.attributes.valor6 : '0',10);
		                    var fue = parseInt(row.attributes.valor7.match(/([0-9])$/) ? row.attributes.valor7 : '0',10);
		                    var pro = parseInt(row.attributes.valor8.match(/([0-9])$/) ? row.attributes.valor8 : '0',10);
			                
		                    if(niv == "Nivel_FF_Ing"){niv = 1;}
		                    else if(niv == "Nivel_PP_Ing"){niv = 2;}
		                    else if(niv == "Nivel_Rubro"){niv = 3;}
		                    else if(niv == "Nivel_Tipo"){niv = 4;}
		                    else if(niv == "Nivel_Clase"){niv = 5;}
		                    else if(niv == "Nivel_Concepto_Ing"){niv = 6;}
		                    else if(niv == "Nivel_SubConcepto"){niv = 7;}
                            else {niv = 0;}
                            
                            if(niv > 0){return '<span><a onclick="Detalles_Mov_Ing(2,'+niv+','+rub+','+tip+','+cla+','+con+','+sub+','+fue+','+sub+');" style="font-size:8pt; cursor:pointer;">'+value+'</a></span>';}
                            else{return '<span style="font-size:8pt">'+value+'</span>';}
		                }else
		                {
		                    return '<span style="font-size:8pt">'+value+'</span>';
		                }
                    }
			    },
			    {field:'descripcion4',title:'Modificado',width:108, align: 'right',
			         formatter:function(value,row){
		                if(parseFloat(value.replace(/,/gi,'')) > 0)
		                {
		                    var niv = row.attributes.valor1;
		                    var rub = parseInt(row.attributes.valor2.match(/([0-9])$/) ? row.attributes.valor2 : '0',10);
		                    var tip = parseInt(row.attributes.valor3.match(/([0-9])$/) ? row.attributes.valor3 : '0',10);
		                    var cla = parseInt(row.attributes.valor4.match(/([0-9])$/) ? row.attributes.valor4 : '0',10);
		                    var con = parseInt(row.attributes.valor5.match(/([0-9])$/) ? row.attributes.valor5 : '0',10);
		                    var sub = parseInt(row.attributes.valor6.match(/([0-9])$/) ? row.attributes.valor6 : '0',10);
		                    var fue = parseInt(row.attributes.valor7.match(/([0-9])$/) ? row.attributes.valor7 : '0',10);
		                    var pro = parseInt(row.attributes.valor8.match(/([0-9])$/) ? row.attributes.valor8 : '0',10);
			                
		                    if(niv == "Nivel_FF_Ing"){niv = 1;}
		                    else if(niv == "Nivel_PP_Ing"){niv = 2;}
		                    else if(niv == "Nivel_Rubro"){niv = 3;}
		                    else if(niv == "Nivel_Tipo"){niv = 4;}
		                    else if(niv == "Nivel_Clase"){niv = 5;}
		                    else if(niv == "Nivel_Concepto_Ing"){niv = 6;}
		                    else if(niv == "Nivel_SubConcepto"){niv = 7;}
                            else {niv = 0;}
                            
                            if(niv > 0){return '<span><a onclick="Detalles_Mov_Ing(3,'+niv+','+rub+','+tip+','+cla+','+con+','+sub+','+fue+','+sub+');" style="font-size:8pt; cursor:pointer;">'+value+'</a></span>';}
                            else{return '<span style="font-size:8pt">'+value+'</span>';}
		                }else
		                {
		                    return '<span style="font-size:8pt">'+value+'</span>';
		                }
                    }
			    },
			    {field:'descripcion9',title:'Por Recaudar',width:108, align: 'right',
			         formatter:function(value){
			            if(parseFloat(value) < 0)
			            {
			                return '<span  style="font-size:8pt; color:red;">'+value+'</span>';
			            }
			            else
			            {
			                return '<span  style="font-size:8pt">'+value+'</span>';
			            }
                    }
			    },
			    {field:'descripcion5',title:'Devengado',width:108, align: 'right', 
			        formatter:function(value,row){
		                if(parseFloat(value.replace(/,/gi,'')) > 0)
		                {
		                    var niv = row.attributes.valor1;
		                    var rub = parseInt(row.attributes.valor2.match(/([0-9])$/) ? row.attributes.valor2 : '0',10);
		                    var tip = parseInt(row.attributes.valor3.match(/([0-9])$/) ? row.attributes.valor3 : '0',10);
		                    var cla = parseInt(row.attributes.valor4.match(/([0-9])$/) ? row.attributes.valor4 : '0',10);
		                    var con = parseInt(row.attributes.valor5.match(/([0-9])$/) ? row.attributes.valor5 : '0',10);
		                    var sub = parseInt(row.attributes.valor6.match(/([0-9])$/) ? row.attributes.valor6 : '0',10);
		                    var fue = parseInt(row.attributes.valor7.match(/([0-9])$/) ? row.attributes.valor7 : '0',10);
		                    var pro = parseInt(row.attributes.valor8.match(/([0-9])$/) ? row.attributes.valor8 : '0',10);
			                
		                    if(niv == "Nivel_FF_Ing"){niv = 1;}
		                    else if(niv == "Nivel_PP_Ing"){niv = 2;}
		                    else if(niv == "Nivel_Rubro"){niv = 3;}
		                    else if(niv == "Nivel_Tipo"){niv = 4;}
		                    else if(niv == "Nivel_Clase"){niv = 5;}
		                    else if(niv == "Nivel_Concepto_Ing"){niv = 6;}
		                    else if(niv == "Nivel_SubConcepto"){niv = 7;}
                            else {niv = 0;}
                            
                            if(niv > 0){return '<span><a onclick="Detalles_Recaudado_Ing(2,'+niv+','+rub+','+tip+','+cla+','+con+','+sub+','+fue+','+sub+');" style="font-size:8pt; cursor:pointer;">'+value+'</a></span>';}
                            else{return '<span style="font-size:8pt">'+value+'</span>';}
		                }else
		                {
		                    if(parseFloat(value.replace(/,/gi,'')) < 0){
		                         return '<span  style="font-size:8pt; color:red;">'+value+'</span>';
		                    }
		                    else{
		                        return '<span style="font-size:8pt">'+value+'</span>';
		                    }
		                }
                    }
			    },
			    {field:'descripcion6',title:'Recaudado',width:108, align: 'right',
			         formatter:function(value,row){
		                if(parseFloat(value.replace(/,/gi,'')) > 0)
		                {
		                    var niv = row.attributes.valor1;
		                    var rub = parseInt(row.attributes.valor2.match(/([0-9])$/) ? row.attributes.valor2 : '0',10);
		                    var tip = parseInt(row.attributes.valor3.match(/([0-9])$/) ? row.attributes.valor3 : '0',10);
		                    var cla = parseInt(row.attributes.valor4.match(/([0-9])$/) ? row.attributes.valor4 : '0',10);
		                    var con = parseInt(row.attributes.valor5.match(/([0-9])$/) ? row.attributes.valor5 : '0',10);
		                    var sub = parseInt(row.attributes.valor6.match(/([0-9])$/) ? row.attributes.valor6 : '0',10);
		                    var fue = parseInt(row.attributes.valor7.match(/([0-9])$/) ? row.attributes.valor7 : '0',10);
		                    var pro = parseInt(row.attributes.valor8.match(/([0-9])$/) ? row.attributes.valor8 : '0',10);
			                
		                    if(niv == "Nivel_FF_Ing"){niv = 1;}
		                    else if(niv == "Nivel_PP_Ing"){niv = 2;}
		                    else if(niv == "Nivel_Rubro"){niv = 3;}
		                    else if(niv == "Nivel_Tipo"){niv = 4;}
		                    else if(niv == "Nivel_Clase"){niv = 5;}
		                    else if(niv == "Nivel_Concepto_Ing"){niv = 6;}
		                    else if(niv == "Nivel_SubConcepto"){niv = 7;}
                            else {niv = 0;}
                            
                            if(niv > 0){return '<span><a onclick="Detalles_Recaudado_Ing(1,'+niv+','+rub+','+tip+','+cla+','+con+','+sub+','+fue+','+sub+');" style="font-size:8pt; cursor:pointer;">'+value+'</a></span>';}
                            else{return '<span style="font-size:8pt">'+value+'</span>';}
		                }else
		                {
		                    if(parseFloat(value.replace(/,/gi,'')) < 0){
		                         return '<span  style="font-size:8pt; color:red;">'+value+'</span>';
		                    }
		                    else{
		                        return '<span style="font-size:8pt">'+value+'</span>';
		                    }
		                }
                    }
			    },
			    {field:'descripcion7',title:'Dev+Rec', align: 'right',hidden:'true',
			         formatter:function(value){
                	    return '<span  style="font-size:8pt">'+value+'</span>';
                    }
			    },
			    {field:'descripcion8',title:'Comprometido', align: 'right',hidden:'true',
			         formatter:function(value){
                	    return '<span  style="font-size:8pt">'+value+'</span>';
                    }
			    },
			    {field:'descripcion10',title:'Fte Financiamiento', align: 'left',hidden:'true',
			         formatter:function(value){
                	    return '<span  style="font-size:8pt">'+value+'</span>';
                    }
			    }
	    ]],
		    onBeforeExpand:function(node){
		        Limpiar_Variables();
		        if(node.attributes != null)
                {
		            Nivel = node.attributes.valor1;
		            Nivel_Ing = node.attributes.valor1;
		            Validar_Chk_Ing();
		            Param_Rubro = node.attributes.valor2;
		            Param_Tipo = node.attributes.valor3;
		            Param_Clase = node.attributes.valor4;
		            Param_Concepto_Ing = node.attributes.valor5;
		            Param_SubConcepto = node.attributes.valor6;
       	            Param_FF_Ing = node.attributes.valor7;
       	            Param_PP_Ing = node.attributes.valor8;
       	            
			        if (Nivel == "Nivel_FF_Ing") {
                       $('#Grid_Psp_Ing').treegrid('options').url = 'Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion='+ Nivel_Ing + '&Estado=' + Estado +'&Param_FF_Ing=' + Param_FF_Ing + Parametros;
                    }
                    if (Nivel == "Nivel_PP_Ing") {
                       $('#Grid_Psp_Ing').treegrid('options').url = 'Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion='+ Nivel_Ing + '&Estado=' + Estado + '&Param_FF_Ing=' + Param_FF_Ing +'&Param_PP_Ing=' + Param_PP_Ing + Parametros;
                    }
                    if (Nivel == "Nivel_Rubro") {
                        $('#Grid_Psp_Ing').treegrid('options').url = 'Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion='+ Nivel_Ing + '&Estado=' + Estado + '&Param_FF_Ing=' + Param_FF_Ing +'&Param_PP_Ing=' + Param_PP_Ing +'&Param_Rubro=' + Param_Rubro + Parametros;
                    }
                    if (Nivel == "Nivel_Tipo") {
                        $('#Grid_Psp_Ing').treegrid('options').url = 'Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion='+ Nivel_Ing + '&Estado=' + Estado + '&Param_FF_Ing=' + Param_FF_Ing +'&Param_PP_Ing=' + Param_PP_Ing +'&Param_Rubro=' + Param_Rubro + "&Param_Tipo=" + Param_Tipo + Parametros;
                    }
                    if (Nivel == "Nivel_Clase") {
                        $('#Grid_Psp_Ing').treegrid('options').url = 'Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion='+ Nivel_Ing + '&Estado=' + Estado + '&Param_FF_Ing=' + Param_FF_Ing +'&Param_PP_Ing=' + Param_PP_Ing +'&Param_Rubro=' + Param_Rubro + "&Param_Tipo=" + Param_Tipo +'&Param_Clase=' + Param_Clase + Parametros;
                    }
                    if (Nivel == "Nivel_Concepto_Ing") {
                        $('#Grid_Psp_Ing').treegrid('options').url = 'Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=Nivel_SubConcepto&Param_Rubro=' + Param_Rubro + '&Param_FF_Ing=' + Param_FF_Ing +'&Param_PP_Ing=' + Param_PP_Ing + "&Param_Tipo=" + Param_Tipo+'&Param_Clase=' + Param_Clase + '&Param_Concepto_Ing=' + Param_Concepto_Ing + Parametros;
                    }
                }
           }

	    });
	}catch(Ex){
        $.messager.alert('Mensaje','Error al llenar generar la tabla. Error: [' + Ex + ']','error');
    }
 }
 
 ///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Limpiar_Variables
///DESCRIPCIÓN          : Funcion para limpiar variables
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 26/Abril/2012
///*********************************************************************************************************************** 
 function Limpiar_Variables(){
    Nivel = "";
    Param_FF_Ing = "";
    Param_PP_Ing = "";
    Param_Rubro = "";
    Param_Tipo = "";
    Param_Clase = "";
    Param_Concepto_Ing = "";
    Param_SubConcepto = "";
    Param_FF = "";
    Param_AF = "";
    Param_UR = "";
    Param_Progrm = "";
    Param_Cap = "";
    Param_Con = "";
    Param_Par_Gen = "";
    Param_Par = "";
    
    Chk_FF_Ing = 'NO';
    Chk_PP_Ing = 'NO';
    Chk_R = 'NO';
    Chk_T = 'NO';
    Chk_Cl = 'NO';
    Chk_Con_Ing = 'NO';
    Chk_SubCon = 'NO';
    Nivel_Ing = '';
    Estado = '';
    
    Chk_FF = 'NO';
    Chk_AF = 'NO';
    Chk_PP = 'NO';
    Chk_Ur = 'NO';
    Chk_Cap = 'NO';
    Chk_Con = 'NO';
    Chk_PG = 'NO';
    Chk_PE = 'NO';
    Nivel_Egr = '';
    Estado_Egr = '';
    
    Det_Niv = '';
    Det_Rub = '';
    Det_Tip = '';
    Det_Cla = '';
    Det_CoI = '';
    Det_Sub = '';
    Det_PPI = '';
    Det_FFI = '';
    
    Det_Cap = '';
    Det_Con = '';
    Det_PG = '';
    Det_PE = '';
    Det_FF = '';
    Det_PP = '';
    Det_AF = '';
    Det_UR = '';
 }
 
 ///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Generar_TreeGrid_Egr
///DESCRIPCIÓN          : Funcion para llenar el treegrid
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 25/Mayo/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///***********************************************************************************************************************
 function Generar_TreeGrid_Egr(Nivel_Egresos, Estado_Egresos, Largo)
 {
    try
    {
        var Parametros = Obtener_Parametros();
        
        $('#Grid_Psp_Egr').treegrid({
		    title:'COG', //titulo de la tabla
		    width:1150, // ancho de la tabla
		    height:Largo, //alto de la tabla
		    rownumbers: false, //sirve para mostrar u ocultar los numeros de lineas
		    animate:true, //Define si para mostrar el efecto de animación al nodo expandir o contraer. 	
		    collapsible:false,
		    //conexion con la clase controladora para obtener el primer nivel del arbol
		    url:'Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=' + Nivel_Egresos + "&Estado=" + Estado_Egresos + "&Tot='SI'" + Parametros ,
		    idField:'id', //indicamos el campo llave para identificar un nodo de arbol
		    treeField:'texto', //define el campo del nodo de árbol
		    resizable:true, //permitir que las columnas puedan cambiar de tamaño
		    nowrap:true, //para mostrar los datos en un solo renglon
		    showFooter:true, //mostrar el pie de la tabla
		    fitColumns:false, //para expandir o contraer el tamaño de las columnas
		    frozenColumns:[[ //columnas que se quedaran estaticas a la izquierda
                {title:'Nombre',field:'texto',width:360,
                    formatter:function(value){
                	    return '<span style="color:Navy; font-size:8pt">'+value+'</span>';
                    }
                }
		    ]],
		    columns:[[ //columnas del grid
		        {field:'descripcion1',title:'Aprobado',width:105, align: 'right',
			         formatter:function(value){
                	        return '<span style="font-size:8pt">'+value+'</span>';
                        }
			    },
			    {field:'descripcion2',title:'Ampliación',width:105, align: 'right',
                    formatter:function(value,row){
		                if(parseFloat(value.replace(/,/gi,'')) > 0)
		                {
		                    var niv = row.attributes.valor1;
		                    var cap = parseInt(row.attributes.valor2.match(/([0-9])$/) ? row.attributes.valor2 : '0',10);
		                    var con = parseInt(row.attributes.valor3.match(/([0-9])$/) ? row.attributes.valor3 : '0',10);
		                    var pag = parseInt(row.attributes.valor4.match(/([0-9])$/) ? row.attributes.valor4 : '0',10);
		                    var par = parseInt(row.attributes.valor5.match(/([0-9])$/) ? row.attributes.valor5 : '0',10);
		                    var fue = parseInt(row.attributes.valor6.match(/([0-9])$/) ? row.attributes.valor6 : '0',10);
		                    var are = parseInt(row.attributes.valor7.match(/([0-9])$/) ? row.attributes.valor7 : '0',10);
		                    var pro = parseInt(row.attributes.valor8.match(/([0-9])$/) ? row.attributes.valor8 : '0',10);
		                    var dep = parseInt(row.attributes.valor9.match(/([0-9])$/) ? row.attributes.valor9 : '0',10);
			                
		                    if(niv == 'Nivel_FF'){niv = 1;}
                            else if(niv == 'Nivel_AF'){niv =2 ;}
                            else if(niv == 'Nivel_PP'){niv = 3;}
                            else if(niv == 'Nivel_UR'){niv = 4;}
                            else if(niv == 'Nivel_Capitulos'){niv = 5;}
                            else if(niv == 'Nivel_Concepto'){niv = 6;}
                            else if(niv == 'Nivel_Partida_Gen'){niv = 7;}
                            else if(niv == 'Nivel_Partida'){niv = 8;}
                            else{niv == 0}
                            
                            if(niv > 0){return '<span><a onclick="Detalles_Mov_Egr(1,'+niv+','+cap+','+con+','+pag+','+par+','+fue+','+are+','+pro+','+ dep + ');" style="font-size:8pt; cursor:pointer;">'+value+'</a></span>';}
                            else{return '<span style="font-size:8pt">'+value+'</span>';}
		                }else
		                {
		                    return '<span style="font-size:8pt">'+value+'</span>';
		                }
                    }
			    },
		        {field:'descripcion3',title:'Reducción',width:105, align: 'right',
			        formatter:function(value,row){
		                if(parseFloat(value.replace(/,/gi,'')) > 0)
		                {
		                    var niv = row.attributes.valor1;
		                    var cap = parseInt(row.attributes.valor2.match(/([0-9])$/) ? row.attributes.valor2 : '0',10);
		                    var con = parseInt(row.attributes.valor3.match(/([0-9])$/) ? row.attributes.valor3 : '0',10);
		                    var pag = parseInt(row.attributes.valor4.match(/([0-9])$/) ? row.attributes.valor4 : '0',10);
		                    var par = parseInt(row.attributes.valor5.match(/([0-9])$/) ? row.attributes.valor5 : '0',10);
		                    var fue = parseInt(row.attributes.valor6.match(/([0-9])$/) ? row.attributes.valor6 : '0',10);
		                    var are = parseInt(row.attributes.valor7.match(/([0-9])$/) ? row.attributes.valor7 : '0',10);
		                    var pro = parseInt(row.attributes.valor8.match(/([0-9])$/) ? row.attributes.valor8 : '0',10);
		                    var dep = parseInt(row.attributes.valor9.match(/([0-9])$/) ? row.attributes.valor9 : '0',10);
			                
		                    if(niv == 'Nivel_FF'){niv = 1;}
                            else if(niv == 'Nivel_AF'){niv =2 ;}
                            else if(niv == 'Nivel_PP'){niv = 3;}
                            else if(niv == 'Nivel_UR'){niv = 4;}
                            else if(niv == 'Nivel_Capitulos'){niv = 5;}
                            else if(niv == 'Nivel_Concepto'){niv = 6;}
                            else if(niv == 'Nivel_Partida_Gen'){niv = 7;}
                            else if(niv == 'Nivel_Partida'){niv = 8;}
                            else{niv == 0}
                            
                            if(niv > 0){return '<span><a onclick="Detalles_Mov_Egr(2,'+niv+','+cap+','+con+','+pag+','+par+','+fue+','+are+','+pro+','+ dep + ');" style="font-size:8pt; cursor:pointer;">'+value+'</a></span>';}
                            else{return '<span style="font-size:8pt">'+value+'</span>';}
		                }else
		                {
		                    return '<span style="font-size:8pt">'+value+'</span>';
		                }
                    }
			    },
			    {field:'descripcion4',title:'Modificado',width:105, align: 'right',
			        formatter:function(value,row){
		                if(parseFloat(value.replace(/,/gi,'')) > 0)
		                {
		                    var niv = row.attributes.valor1;
		                    var cap = parseInt(row.attributes.valor2.match(/([0-9])$/) ? row.attributes.valor2 : '0',10);
		                    var con = parseInt(row.attributes.valor3.match(/([0-9])$/) ? row.attributes.valor3 : '0',10);
		                    var pag = parseInt(row.attributes.valor4.match(/([0-9])$/) ? row.attributes.valor4 : '0',10);
		                    var par = parseInt(row.attributes.valor5.match(/([0-9])$/) ? row.attributes.valor5 : '0',10);
		                    var fue = parseInt(row.attributes.valor6.match(/([0-9])$/) ? row.attributes.valor6 : '0',10);
		                    var are = parseInt(row.attributes.valor7.match(/([0-9])$/) ? row.attributes.valor7 : '0',10);
		                    var pro = parseInt(row.attributes.valor8.match(/([0-9])$/) ? row.attributes.valor8 : '0',10);
		                    var dep = parseInt(row.attributes.valor9.match(/([0-9])$/) ? row.attributes.valor9 : '0',10);
			                
		                    if(niv == 'Nivel_FF'){niv = 1;}
                            else if(niv == 'Nivel_AF'){niv =2 ;}
                            else if(niv == 'Nivel_PP'){niv = 3;}
                            else if(niv == 'Nivel_UR'){niv = 4;}
                            else if(niv == 'Nivel_Capitulos'){niv = 5;}
                            else if(niv == 'Nivel_Concepto'){niv = 6;}
                            else if(niv == 'Nivel_Partida_Gen'){niv = 7;}
                            else if(niv == 'Nivel_Partida'){niv = 8;}
                            else{niv == 0}
                            
                            if(niv > 0){return '<span><a onclick="Detalles_Mov_Egr(3,'+niv+','+cap+','+con+','+pag+','+par+','+fue+','+are+','+pro+','+ dep + ');" style="font-size:8pt; cursor:pointer;">'+value+'</a></span>';}
                            else{return '<span style="font-size:8pt">'+value+'</span>';}
		                }else
		                {
		                    return '<span style="font-size:8pt">'+value+'</span>';
		                }
                    }
			    },
			    {field:'descripcion5',title:'Por Ejercer',width:105, align: 'right',
			         formatter:function(value){
                	    if(parseFloat(value) < 0)
			            {
			                return '<span  style="font-size:8pt; color:red;">'+value+'</span>';
			            }
			            else
			            {
			                return '<span  style="font-size:8pt">'+value+'</span>';
			            }
                    }
                },
                {field:'descripcion6',title:'PreComprometido',width:110, align: 'right',
			         formatter:function(value,row){
		                if(parseFloat(value.replace(/,/gi,'')) > 0)
		                {
		                    var niv = row.attributes.valor1;
		                    var cap = parseInt(row.attributes.valor2.match(/([0-9])$/) ? row.attributes.valor2 : '0',10);
		                    var con = parseInt(row.attributes.valor3.match(/([0-9])$/) ? row.attributes.valor3 : '0',10);
		                    var pag = parseInt(row.attributes.valor4.match(/([0-9])$/) ? row.attributes.valor4 : '0',10);
		                    var par = parseInt(row.attributes.valor5.match(/([0-9])$/) ? row.attributes.valor5 : '0',10);
		                    var fue = parseInt(row.attributes.valor6.match(/([0-9])$/) ? row.attributes.valor6 : '0',10);
		                    var are = parseInt(row.attributes.valor7.match(/([0-9])$/) ? row.attributes.valor7 : '0',10);
		                    var pro = parseInt(row.attributes.valor8.match(/([0-9])$/) ? row.attributes.valor8 : '0',10);
		                    var dep = parseInt(row.attributes.valor9.match(/([0-9])$/) ? row.attributes.valor9 : '0',10);
			                
		                    if(niv == 'Nivel_FF'){niv = 1;}
                            else if(niv == 'Nivel_AF'){niv =2 ;}
                            else if(niv == 'Nivel_PP'){niv = 3;}
                            else if(niv == 'Nivel_UR'){niv = 4;}
                            else if(niv == 'Nivel_Capitulos'){niv = 5;}
                            else if(niv == 'Nivel_Concepto'){niv = 6;}
                            else if(niv == 'Nivel_Partida_Gen'){niv = 7;}
                            else if(niv == 'Nivel_Partida'){niv = 8;}
                            else{niv == 0}
                            
                            if(niv > 0){return '<span><a onclick="Detalles_Mov_Contable_Egr(4,'+niv+','+cap+','+con+','+pag+','+par+','+fue+','+are+','+pro+','+ dep + ');" style="font-size:8pt; cursor:pointer;">'+value+'</a></span>';}
                            else{return '<span style="font-size:8pt">'+value+'</span>';}
		                }else
		                {
		                    if(parseFloat(value.replace(/,/gi,'')) < 0){
		                         return '<span  style="font-size:8pt; color:red;">'+value+'</span>';
		                    }
		                    else{
		                        return '<span style="font-size:8pt">'+value+'</span>';
		                    }
		                }
                    }
                },
                {field:'descripcion7',title:'Comprometido',width:105, align: 'right',
			         formatter:function(value,row){
		                if(parseFloat(value.replace(/,/gi,'')) > 0)
		                {
		                    var niv = row.attributes.valor1;
		                    var cap = parseInt(row.attributes.valor2.match(/([0-9])$/) ? row.attributes.valor2 : '0',10);
		                    var con = parseInt(row.attributes.valor3.match(/([0-9])$/) ? row.attributes.valor3 : '0',10);
		                    var pag = parseInt(row.attributes.valor4.match(/([0-9])$/) ? row.attributes.valor4 : '0',10);
		                    var par = parseInt(row.attributes.valor5.match(/([0-9])$/) ? row.attributes.valor5 : '0',10);
		                    var fue = parseInt(row.attributes.valor6.match(/([0-9])$/) ? row.attributes.valor6 : '0',10);
		                    var are = parseInt(row.attributes.valor7.match(/([0-9])$/) ? row.attributes.valor7 : '0',10);
		                    var pro = parseInt(row.attributes.valor8.match(/([0-9])$/) ? row.attributes.valor8 : '0',10);
		                    var dep = parseInt(row.attributes.valor9.match(/([0-9])$/) ? row.attributes.valor9 : '0',10);
			                
		                    if(niv == 'Nivel_FF'){niv = 1;}
                            else if(niv == 'Nivel_AF'){niv =2 ;}
                            else if(niv == 'Nivel_PP'){niv = 3;}
                            else if(niv == 'Nivel_UR'){niv = 4;}
                            else if(niv == 'Nivel_Capitulos'){niv = 5;}
                            else if(niv == 'Nivel_Concepto'){niv = 6;}
                            else if(niv == 'Nivel_Partida_Gen'){niv = 7;}
                            else if(niv == 'Nivel_Partida'){niv = 8;}
                            else{niv == 0}
                            
                            if(niv > 0){return '<span><a onclick="Detalles_Mov_Contable_Egr(2,'+niv+','+cap+','+con+','+pag+','+par+','+fue+','+are+','+pro+','+ dep + ');" style="font-size:8pt; cursor:pointer;">'+value+'</a></span>';}
                            else{return '<span style="font-size:8pt">'+value+'</span>';}
		                }else
		                {
		                    if(parseFloat(value.replace(/,/gi,'')) < 0){
		                         return '<span  style="font-size:8pt; color:red;">'+value+'</span>';
		                    }
		                    else{
		                        return '<span style="font-size:8pt">'+value+'</span>';
		                    }
		                }
                    }
                },
			    {field:'descripcion8',title:'Devengado',width:105, align: 'right', 
			        formatter:function(value,row){
		                if(parseFloat(value.replace(/,/gi,'')) > 0)
		                {
		                    var niv = row.attributes.valor1;
		                    var cap = parseInt(row.attributes.valor2.match(/([0-9])$/) ? row.attributes.valor2 : '0',10);
		                    var con = parseInt(row.attributes.valor3.match(/([0-9])$/) ? row.attributes.valor3 : '0',10);
		                    var pag = parseInt(row.attributes.valor4.match(/([0-9])$/) ? row.attributes.valor4 : '0',10);
		                    var par = parseInt(row.attributes.valor5.match(/([0-9])$/) ? row.attributes.valor5 : '0',10);
		                    var fue = parseInt(row.attributes.valor6.match(/([0-9])$/) ? row.attributes.valor6 : '0',10);
		                    var are = parseInt(row.attributes.valor7.match(/([0-9])$/) ? row.attributes.valor7 : '0',10);
		                    var pro = parseInt(row.attributes.valor8.match(/([0-9])$/) ? row.attributes.valor8 : '0',10);
		                    var dep = parseInt(row.attributes.valor9.match(/([0-9])$/) ? row.attributes.valor9 : '0',10);
			                
		                    if(niv == 'Nivel_FF'){niv = 1;}
                            else if(niv == 'Nivel_AF'){niv =2 ;}
                            else if(niv == 'Nivel_PP'){niv = 3;}
                            else if(niv == 'Nivel_UR'){niv = 4;}
                            else if(niv == 'Nivel_Capitulos'){niv = 5;}
                            else if(niv == 'Nivel_Concepto'){niv = 6;}
                            else if(niv == 'Nivel_Partida_Gen'){niv = 7;}
                            else if(niv == 'Nivel_Partida'){niv = 8;}
                            else{niv == 0}
                            
                            if(niv > 0){return '<span><a onclick="Detalles_Mov_Contable_Egr(1,'+niv+','+cap+','+con+','+pag+','+par+','+fue+','+are+','+pro+','+ dep + ');" style="font-size:8pt; cursor:pointer;">'+value+'</a></span>';}
                            else{return '<span style="font-size:8pt">'+value+'</span>';}
		                }else
		                {
		                    if(parseFloat(value.replace(/,/gi,'')) < 0){
		                         return '<span  style="font-size:8pt; color:red;">'+value+'</span>';
		                    }
		                    else{
		                        return '<span style="font-size:8pt">'+value+'</span>';
		                    }
		                }
                    }
			    },
			    {field:'descripcion9',title:'Ejercido',width:100, align: 'right',
			         formatter:function(value,row){
		                if(parseFloat(value.replace(/,/gi,'')) > 0)
		                {
		                    var niv = row.attributes.valor1;
		                    var cap = parseInt(row.attributes.valor2.match(/([0-9])$/) ? row.attributes.valor2 : '0',10);
		                    var con = parseInt(row.attributes.valor3.match(/([0-9])$/) ? row.attributes.valor3 : '0',10);
		                    var pag = parseInt(row.attributes.valor4.match(/([0-9])$/) ? row.attributes.valor4 : '0',10);
		                    var par = parseInt(row.attributes.valor5.match(/([0-9])$/) ? row.attributes.valor5 : '0',10);
		                    var fue = parseInt(row.attributes.valor6.match(/([0-9])$/) ? row.attributes.valor6 : '0',10);
		                    var are = parseInt(row.attributes.valor7.match(/([0-9])$/) ? row.attributes.valor7 : '0',10);
		                    var pro = parseInt(row.attributes.valor8.match(/([0-9])$/) ? row.attributes.valor8 : '0',10);
		                    var dep = parseInt(row.attributes.valor9.match(/([0-9])$/) ? row.attributes.valor9 : '0',10);
			                
		                    if(niv == 'Nivel_FF'){niv = 1;}
                            else if(niv == 'Nivel_AF'){niv =2 ;}
                            else if(niv == 'Nivel_PP'){niv = 3;}
                            else if(niv == 'Nivel_UR'){niv = 4;}
                            else if(niv == 'Nivel_Capitulos'){niv = 5;}
                            else if(niv == 'Nivel_Concepto'){niv = 6;}
                            else if(niv == 'Nivel_Partida_Gen'){niv = 7;}
                            else if(niv == 'Nivel_Partida'){niv = 8;}
                            else{niv == 0}
                            
                            if(niv > 0){return '<span><a onclick="Detalles_Mov_Contable_Egr(5,'+niv+','+cap+','+con+','+pag+','+par+','+fue+','+are+','+pro+','+ dep + ');" style="font-size:8pt; cursor:pointer;">'+value+'</a></span>';}
                            else{return '<span style="font-size:8pt">'+value+'</span>';}
		                }else
		                {
		                    if(parseFloat(value.replace(/,/gi,'')) < 0){
		                         return '<span  style="font-size:8pt; color:red;">'+value+'</span>';
		                    }
		                    else{
		                        return '<span style="font-size:8pt">'+value+'</span>';
		                    }
		                }
                    }
			    },
			    {field:'descripcion10',title:'Pagado',width:105, align: 'right',
			         formatter:function(value,row){
		                if(parseFloat(value.replace(/,/gi,'')) > 0)
		                {
		                    var niv = row.attributes.valor1;
		                    var cap = parseInt(row.attributes.valor2.match(/([0-9])$/) ? row.attributes.valor2 : '0',10);
		                    var con = parseInt(row.attributes.valor3.match(/([0-9])$/) ? row.attributes.valor3 : '0',10);
		                    var pag = parseInt(row.attributes.valor4.match(/([0-9])$/) ? row.attributes.valor4 : '0',10);
		                    var par = parseInt(row.attributes.valor5.match(/([0-9])$/) ? row.attributes.valor5 : '0',10);
		                    var fue = parseInt(row.attributes.valor6.match(/([0-9])$/) ? row.attributes.valor6 : '0',10);
		                    var are = parseInt(row.attributes.valor7.match(/([0-9])$/) ? row.attributes.valor7 : '0',10);
		                    var pro = parseInt(row.attributes.valor8.match(/([0-9])$/) ? row.attributes.valor8 : '0',10);
		                    var dep = parseInt(row.attributes.valor9.match(/([0-9])$/) ? row.attributes.valor9 : '0',10);
			                
		                    if(niv == 'Nivel_FF'){niv = 1;}
                            else if(niv == 'Nivel_AF'){niv =2 ;}
                            else if(niv == 'Nivel_PP'){niv = 3;}
                            else if(niv == 'Nivel_UR'){niv = 4;}
                            else if(niv == 'Nivel_Capitulos'){niv = 5;}
                            else if(niv == 'Nivel_Concepto'){niv = 6;}
                            else if(niv == 'Nivel_Partida_Gen'){niv = 7;}
                            else if(niv == 'Nivel_Partida'){niv = 8;}
                            else{niv == 0}
                            
                            if(niv > 0){return '<span><a onclick="Detalles_Mov_Contable_Egr(3,'+niv+','+cap+','+con+','+pag+','+par+','+fue+','+are+','+pro+','+ dep + ');" style="font-size:8pt; cursor:pointer;">'+value+'</a></span>';}
                            else{return '<span style="font-size:8pt">'+value+'</span>';}
		                }else
		                {
		                    if(parseFloat(value.replace(/,/gi,'')) < 0){
		                         return '<span  style="font-size:8pt; color:red;">'+value+'</span>';
		                    }
		                    else{
		                        return '<span style="font-size:8pt">'+value+'</span>';
		                    }
		                }
                    }
			    },
                {field:'descripcion11',title:'Fte Financiamiento', align: 'left', hidden:'true',
			         formatter:function(value){
                	    return '<span  style="font-size:8pt">'+value+'</span>';
                    }
                }
		    ]], //evento que se ejecutara al momento de expandir un nodo del arbol
		    onBeforeExpand:function(node){
		        Limpiar_Variables();
		        if(node.attributes != null) //verificamos que los atributos del nodo existan
		        {   //obtenemos los atributos del nodo
		            Nivel = node.attributes.valor1;
		            Nivel_Egr = node.attributes.valor1;
		            Validar_Chk_Egr();
		            Param_Cap = node.attributes.valor2;
		            Param_Con = node.attributes.valor3;
		            Param_Par_Gen = node.attributes.valor4;
		            Param_Par = node.attributes.valor5;
        		    Param_FF = node.attributes.valor6;
                    Param_AF = node.attributes.valor7;
                    Param_Progrm = node.attributes.valor8;
                    Param_UR = node.attributes.valor9;
                    
                    
			        if (Nivel == "Nivel_FF") {
                        $('#Grid_Psp_Egr').treegrid('options').url = 'Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=' + Nivel_Egr + '&Estado=' + Estado_Egr +  '&Param_FF=' + Param_FF + Parametros;
                    }
                    if (Nivel == "Nivel_AF") {
                        $('#Grid_Psp_Egr').treegrid('options').url = 'Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=' + Nivel_Egr + '&Estado=' + Estado_Egr + '&Param_FF=' + Param_FF + '&Param_AF=' + Param_AF + Parametros;
                    }
                    if (Nivel == "Nivel_PP") {
                        $('#Grid_Psp_Egr').treegrid('options').url = 'Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=' + Nivel_Egr + '&Estado=' + Estado_Egr + '&Param_FF=' + Param_FF + '&Param_AF=' + Param_AF + '&Param_Progrm=' + Param_Progrm + Parametros;
                    }
                    if (Nivel == "Nivel_UR") {
                        $('#Grid_Psp_Egr').treegrid('options').url = 'Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=' + Nivel_Egr + '&Estado=' + Estado_Egr + '&Param_FF=' + Param_FF + '&Param_AF=' + Param_AF + '&Param_Progrm=' + Param_Progrm + '&Param_UR=' + Param_UR + Parametros;
                    }
			        if (Nivel == "Nivel_Capitulos") {
                        $('#Grid_Psp_Egr').treegrid('options').url = 'Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=' + Nivel_Egr + '&Estado=' + Estado_Egr + '&Param_FF=' + Param_FF + '&Param_AF=' + Param_AF + '&Param_Progrm=' + Param_Progrm + '&Param_UR=' + Param_UR + '&Param_Cap=' + Param_Cap + Parametros;
                    }
                    if (Nivel == "Nivel_Concepto") {
                        $('#Grid_Psp_Egr').treegrid('options').url = 'Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=' + Nivel_Egr + '&Estado=' + Estado_Egr + '&Param_FF=' + Param_FF + '&Param_AF=' + Param_AF + '&Param_Progrm=' + Param_Progrm + '&Param_UR=' + Param_UR + '&Param_Cap=' + Param_Cap+'&Param_Con=' + Param_Con + Parametros;
                    }
                    if (Nivel == "Nivel_Partida_Gen") {
                        $('#Grid_Psp_Egr').treegrid('options').url = 'Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=Nivel_Partida&Param_Cap=' + Param_Cap +'&Param_Con=' + Param_Con + '&Param_Par_Gen=' + Param_Par_Gen + '&Param_FF=' + Param_FF + '&Param_AF=' + Param_AF + '&Param_Progrm=' + Param_Progrm + '&Param_UR=' + Param_UR + Parametros;
                    }
                }
           }
	    });
	}catch(Ex){
        $.messager.alert('Mensaje','Error al llenar generar la tabla. Error: [' + Ex + ']','error');
    }
 }
 
///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Validar_Chk_Ing
///DESCRIPCIÓN          : Funcion para validar los checks seleccionados del presupuesto
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 22/Agosto/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///***********************************************************************************************************************
 function Validar_Chk_Ing()
 {
    try{
        if($("input:checkbox[id$=Chk_FF_Ing]").attr('checked')){
            if(Nivel_Ing == "Nivel_FF_Ing"){
                Chk_FF_Ing = "NO";
            }else{
                Chk_FF_Ing = "SI";
            }
        }
        if($("input:checkbox[id$=Chk_PP_Ing]").attr('checked')){
            if(Nivel_Ing == "Nivel_PP_Ing"){
                Chk_PP_Ing = "NO";
                Chk_FF_Ing = "NO";
            }else{
                Chk_PP_Ing = "SI";
            }
        }
        if($("input:checkbox[id$=Chk_R]").attr('checked')){
            if(Nivel_Ing == "Nivel_Rubro"){
                Chk_R = "NO";
                Chk_PP_Ing = "NO";
                Chk_FF_Ing = "NO";
            }else{
                Chk_R = "SI";
            }
        }
        if($("input:checkbox[id$=Chk_T]").attr('checked')){
            if(Nivel_Ing == "Nivel_Tipo"){
                Chk_T = "NO";
                Chk_R = "NO";
                Chk_PP_Ing = "NO";
                Chk_FF_Ing = "NO";
            }else{
                Chk_T = "SI";
            }
        }
        if($("input:checkbox[id$=Chk_Cl]").attr('checked')){
            if(Nivel_Ing == "Nivel_Clase"){
                Chk_Cl = "NO";
                Chk_T = "NO";
                Chk_R = "NO";
                Chk_PP_Ing = "NO";
                Chk_FF_Ing = "NO";
            }else{
                Chk_Cl = "SI";
            }
        }
        if($("input:checkbox[id$=Chk_Con_Ing]").attr('checked')){
            if(Nivel_Ing == "Nivel_Concepto_Ing"){
                Chk_Con_Ing = "NO";
                Chk_Cl = "NO";
                Chk_T = "NO";
                Chk_R = "NO";
                Chk_PP_Ing = "NO";
                Chk_FF_Ing = "NO";
            }else{
                Chk_Con_Ing = "SI";
            }
        }
        if($("input:checkbox[id$=Chk_SubCon]").attr('checked')){
            Chk_SubCon = "SI"
        }
        
        /*** validar estado del nodo ***/
        if(Chk_FF_Ing == 'SI'){
            Nivel_Ing = "Nivel_FF_Ing";
            if(Chk_PP_Ing == 'SI' || Chk_R == 'SI' || Chk_T == 'SI' || Chk_Cl == 'SI' || Chk_Con_Ing == 'SI' || Chk_SubCon == 'SI'){
                Estado = "closed";
            }else{
                Estado = "opened";
            }
        }else{
            if(Chk_PP_Ing == 'SI'){
                Nivel_Ing = "Nivel_PP_Ing";
                if(Chk_R == 'SI' || Chk_T == 'SI' || Chk_Cl == 'SI' || Chk_Con_Ing == 'SI' || Chk_SubCon == 'SI'){
                    Estado = "closed";
                }else{
                    Estado = "opened";
                }
            }else{
                if(Chk_R == 'SI'){
                    Nivel_Ing = "Nivel_Rubro";
                    if(Chk_T == 'SI' || Chk_Cl == 'SI' || Chk_Con_Ing == 'SI' || Chk_SubCon == 'SI'){
                        Estado = "closed";
                    }else{
                        Estado = "opened";
                    }
                }else{
                    if(Chk_T == 'SI'){
                        Nivel_Ing = "Nivel_Tipo";
                        if(Chk_Cl == 'SI' || Chk_Con_Ing == 'SI' || Chk_SubCon == 'SI'){
                            Estado = "closed";
                        }else{
                            Estado = "opened";
                        }
                    }else{
                        if(Chk_Cl == 'SI'){
                            Nivel_Ing = "Nivel_Clase";
                            if(Chk_Con_Ing == 'SI' || Chk_SubCon == 'SI'){
                                Estado = "closed";
                            }else{
                                Estado = "opened";
                            }
                        }else{
                            if(Chk_Con_Ing == 'SI'){
                                Nivel_Ing = "Nivel_Concepto_Ing";
                                if(Chk_SubCon == 'SI'){
                                    Estado = "closed";
                                }else{
                                    Estado = "opened";
                                }
                            }else{
//                                Nivel_Ing = "Nivel_SubConcepto";
//                                Estado = "opened";
                                Estado = "opened";
                                Nivel_Ing = "Nivel_Concepto_Ing";
                            }
                        }
                    }
                }
            }
        }
        
    }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar validar los checks seleccionados. Error: [' + Ex + ']','error');
    }
 }
 
///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Validar_Chk_Egr
///DESCRIPCIÓN          : Funcion para validar los checks seleccionados del presupuesto
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 23/Agosto/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///***********************************************************************************************************************
 function Validar_Chk_Egr()
 {
    try{
        if($("input:checkbox[id$=Chk_FF]").attr('checked')){
            if(Nivel_Egr == "Nivel_FF"){
                Chk_FF = "NO";
            }else{
                Chk_FF = "SI";
            }
        }
        if($("input:checkbox[id$=Chk_AF]").attr('checked')){
            if(Nivel_Egr == "Nivel_AF"){
                Chk_AF = "NO";
                Chk_FF = "NO";
            }else{
                Chk_AF = "SI";
            }
        }
        if($("input:checkbox[id$=Chk_PP]").attr('checked')){
            if(Nivel_Egr == "Nivel_PP"){
                Chk_PP = "NO";
                Chk_AF = "NO";
                Chk_FF = "NO";
            }else{
                Chk_PP = "SI";
            }
        }
        if($("input:checkbox[id$=Chk_Ur]").attr('checked')){
            if(Nivel_Egr == "Nivel_UR"){
                Chk_Ur = "NO";
                Chk_PP = "NO";
                Chk_AF = "NO";
                Chk_FF = "NO";
            }else{
                Chk_Ur = "SI";
            }
        }
        if($("input:checkbox[id$=Chk_Cap]").attr('checked')){
            if(Nivel_Egr == "Nivel_Capitulos"){
                Chk_Cap = "NO";
                Chk_Ur = "NO";
                Chk_PP = "NO";
                Chk_AF = "NO";
                Chk_FF = "NO";
            }else{
                Chk_Cap = "SI";
            }
        }
        if($("input:checkbox[id$=Chk_Con]").attr('checked')){
            if(Nivel_Egr == "Nivel_Concepto"){
                Chk_Con = "NO";
                Chk_Cap = "NO";
                Chk_Ur = "NO";
                Chk_PP = "NO";
                Chk_AF = "NO";
                Chk_FF = "NO";
            }else{
                Chk_Con = "SI";
            }
        }
        if($("input:checkbox[id$=Chk_PG]").attr('checked')){
            if(Nivel_Egr == "Nivel_Partida_Gen"){
                Chk_PG = "NO";
                Chk_Con = "NO";
                Chk_Cap = "NO";
                Chk_Ur = "NO";
                Chk_PP = "NO";
                Chk_AF = "NO";
                Chk_FF = "NO";
            }else{
                Chk_PG = "SI";
            }
        }
        if($("input:checkbox[id$=Chk_PE]").attr('checked')){
            Chk_PE = "SI"
        }
        
        /*** validar estado del nodo ***/
        if(Chk_FF == 'SI'){
            Nivel_Egr = "Nivel_FF";
            if(Chk_AF == 'SI' || Chk_PP == 'SI' || Chk_Ur == 'SI' || Chk_Cap == 'SI' || Chk_Con == 'SI' || Chk_PG == 'SI' || Chk_PE == 'SI'){
                Estado_Egr = "closed";
            }else{
                Estado_Egr = "opened";
            }
        }else{
            if(Chk_AF == 'SI'){
                Nivel_Egr = "Nivel_AF";
                if(Chk_PP == 'SI' || Chk_Ur == 'SI' || Chk_Cap == 'SI' || Chk_Con == 'SI' || Chk_PG == 'SI' || Chk_PE == 'SI'){
                    Estado_Egr = "closed";
                }else{
                    Estado_Egr = "opened";
                }
            }else{
                if(Chk_PP == 'SI'){
                    Nivel_Egr = "Nivel_PP";
                    if(Chk_Ur == 'SI' || Chk_Cap == 'SI' || Chk_Con == 'SI' || Chk_PG == 'SI' || Chk_PE == 'SI'){
                        Estado_Egr = "closed";
                    }else{
                        Estado_Egr = "opened";
                    }
                }else{
                    if(Chk_Ur == 'SI'){
                        Nivel_Egr = "Nivel_UR";
                        if(Chk_Cap == 'SI' || Chk_Con == 'SI' || Chk_PG == 'SI' || Chk_PE == 'SI'){
                            Estado_Egr = "closed";
                        }else{
                            Estado_Egr = "opened";
                        }
                    }else{
                        if(Chk_Cap == 'SI'){
                            Nivel_Egr = "Nivel_Capitulos";
                            if(Chk_Con == 'SI' || Chk_PG == 'SI' || Chk_PE == 'SI'){
                                Estado_Egr = "closed";
                            }else{
                                Estado_Egr = "opened";
                            }
                        }else{
                            if(Chk_Con == 'SI'){
                                Nivel_Egr = "Nivel_Concepto";
                                if(Chk_PG == 'SI' || Chk_PE == 'SI'){
                                    Estado_Egr = "closed";
                                }else{
                                    Estado_Egr = "opened";
                                }
                            }else{
                                if(Chk_PG == 'SI'){
                                    Nivel_Egr = "Nivel_Partida_Gen";
                                    if(Chk_PE == 'SI'){
                                        Estado_Egr = "closed";
                                    }else{
                                        Estado_Egr = "opened";
                                    }
                                }else{
                                     Estado_Egr = "opened";
                                     Nivel_Egr = "Nivel_Partida";
                                }
                            }
                        }
                    }
                }
            }
        }
    }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar validar los checks seleccionados. Error: [' + Ex + ']','error');
    }
 }
 
///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Detalles_Mov_Ing
///DESCRIPCIÓN          : Funcion para llenar el grid de los detalles de los movimientos de ingresos
///PARAMETROS           1 Tipo: ampliacion o reduccion
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 24/Agosto/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///***********************************************************************************************************************
 function Detalles_Mov_Ing(Tipo, P_Nivel, P_Rubro, P_Tipo, P_Clase, P_Concepto, P_SubConcepto, P_FF, P_PP)
 {
    var Det = '';
    var Complemento = '';
    Det_Niv = '';
    Det_Rub = '';
    Det_Tip = '';
    Det_Cla = '';
    Det_CoI = '';
    Det_Sub = '';
    Det_PPI = '';
    Det_FFI = '';
    
    if(Tipo == 1)
    {
        Det = 'AMPLIACION';
    }else if(Tipo == 2)
    {
        Det = 'REDUCCION';
    }
    
    if(P_Nivel == 1){Det_Niv = 'Nivel_FF_Ing';}
    else if(P_Nivel == 2){Det_Niv = 'Nivel_PP_Ing';}
    else if(P_Nivel == 3){Det_Niv = 'Nivel_Rubro';}
    else if(P_Nivel == 4){Det_Niv = 'Nivel_Tipo';}
    else if(P_Nivel == 5){Det_Niv = 'Nivel_Clase';}
    else if(P_Nivel == 6){Det_Niv = 'Nivel_Concepto_Ing';}
    else if(P_Nivel == 7){Det_Niv = 'Nivel_SubConcepto';}
    
    if(P_Rubro > 0){
        for(i=0; i<=4-P_Rubro.toString().length; i++){Complemento = Complemento + "0";}
        Det_Rub = Complemento + P_Rubro;
    }
    Complemento = '';
    if(P_Tipo > 0){
        for(i=0; i<=4-P_Tipo.toString().length; i++){Complemento = Complemento + "0";}
        Det_Tip = Complemento + P_Tipo;
    }
    Complemento = '';
    if(P_Clase > 0){
        for(i=0; i<=9-P_Clase.toString().length; i++){Complemento = Complemento + "0";}
        Det_Cla = Complemento + P_Clase;
    }
    Complemento = '';
    if(P_Concepto > 0){
        for(i=0; i<=9-P_Concepto.toString().length; i++){Complemento = Complemento + "0";}
        Det_CoI = Complemento + P_Concepto;
    }
    Complemento = '';
    if(P_SubConcepto > 0){
        for(i=0; i<=9-P_SubConcepto.toString().length; i++){Complemento = Complemento + "0";}
        Det_Sub = Complemento + P_SubConcepto;
    }
    Complemento = '';
    if(P_PP > 0){
        for(i=0; i<=9-P_PP.toString().length; i++){Complemento = Complemento + "0";}
        Det_PPI = Complemento + P_PP;
    }
    Complemento = '';
    if(P_FF > 0){
        for(i=0; i<=4-P_FF.toString().length; i++){Complemento = Complemento + "0";}
        Det_FFI = Complemento + P_FF;
    }
    
    var Parametros = Obtener_Parametros();
    
    try{
        $.ajax({
            url:'Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=Det_Mov_Ingresos&Tipo_Det=' + Det + Parametros ,
            type:'POST',
            async: false,
            cache: false,
            dataType:'json',
            success: function(Datos) {
                 if (Datos != null) {
	                $('#Grid_Det_Mov_Ing').datagrid({
                        title: '', //texto del titulo de la tabla
                        //llamada al controlador para obtener los datos de la tabla
                        url:'Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=Det_Mov_Ingresos&Tipo_Det=' + Det + Parametros ,
                        width: 1150, //largo de la tabla
                        height:320, // ancho de la tabla
                        nowrap: false, //mostrar los datos en un solo renglon
                        striped: true, //mostrar las divisiones de las columnas de la tabla
                        collapsible:false,
                        pagination: false, //que muestre la paginacion de la tabla
                        rownumbers: false, //mostrar el numero de registro
                        fitColumns: true, //para expadir o cotraer el tamaño de las columnas
                        showFooter: true, //mostrar el pie de la tabla
                        remoteSort: true,
                        loadMsg: 'Cargando Datos', //mensaje a mostrar cuando se esten cargando los datos
                        toolbar:[{ //barra de herramientas para mostrar botones
					            text:'Detalles Movimientos Ingresos', //titulo del boton
					            iconCls:'icon-reimprimir' //icono que se usara 
				            },
				            { //barra de herramientas para mostrar botones
					            text:'Generar Reporte', //titulo del boton
					            iconCls:'icon-excel', //icono que se usara 
					            handler:function(){ //funcion que se efectuara al hacer click sobre el boton
						            Obtener_Reporte_Detalles(Parametros, 'Det_Mov_Ingresos', Det);
						        }
					        },
				            { //barra de herramientas para mostrar botones
					            text:'Regresar', //titulo del boton
					            iconCls:'icon-undo', //icono que se usara 
					            handler:function(){ //funcion que se efectuara al hacer click sobre el boton
						            $("#Td_Grid_Det_Mov_Ing").css("display","none");
						            $("#Td_Iconos").css("display","block");
						            Obtener_Presupuesto();
					            }
				            }
				        ],
                        columns: [[ //columnas de la tabla
                            { field: 'NO_SOLICITUD_ING', title:'No Solicitud', width: 70, sortable:true,
                                formatter:function(value, row){
                	                var No_Modificacion = parseInt(row.NO_MODIFICACION.match(/([0-9])$/) ? row.NO_MODIFICACION : '0',10);
                                    var No_Solicitud = parseInt(row.NO_MOVIMIENTO.match(/([0-9])$/) ? row.NO_MOVIMIENTO : '0',10);
                                    var Anio = parseInt(row.ANIO.match(/([0-9])$/) ? row.ANIO : '0',10);
                                    
                                    if(No_Modificacion != "")
                                    {return '<span><a onclick="Generar_Solicitud_Movimientos_Ingresos('+ No_Modificacion + ','+No_Solicitud+','+Anio+');" style="font-size:8pt; cursor:pointer;">'+value+'</a></span>';}
                                    else{return '<span style="font-size:xx-small">'+value+'</span>';}
                                }
                            },
                            { title: 'Concepto', field: 'CONCEPTO', width: 250, align: 'left',
                                formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { title:'Importe', field: 'IMPORTE', width: 80, align: 'right', 
                                formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { title: 'Fecha', field: 'FECHA_INICIO',  width: 50, align: 'left', sortable:true,
                                formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { title: 'Creo', field: 'USUARIO_CREO',  width: 150, align: 'left',
                                 formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { title: 'Justificación', field: 'JUSTIFICACION',  width: 250, align: 'left',
                                formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { title: 'Tipo', field: 'TIPO_MOVIMIENTO',  width: 70, align: 'left',
                                formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { title: 'Autorizo', field: 'USUARIO_MODIFICO', hidden: true, align: 'left' },
                            { title: 'Fecha Autorizo', field: 'FECHA_AUTORIZO', align: 'left', hidden: true},
                            { title: 'No Modificacion', field: 'NO_MODIFICACION', align: 'left', hidden: true},
                            { title: 'No Movimiento', field: 'NO_MOVIMIENTO', align: 'left', hidden: true},
                            { title: 'Anio', field: 'ANIO', align: 'left', hidden: true}
                          ]]
                   });
                   
                    $("#Td_Iconos").css("display","none");
                    $("#Td_Grid_Psp_Egr").css("display","none");
                    $("#Td_Grid_Psp_Ing").css("display","none");
                    $("#Td_Grid_Det_Mov_Ing").css("display","block");
                    $("#Td_Grid_Det_Rec_Ing").css("display","none");
                    $("#Td_Grid_Det_Mov_Egr").css("display","none");
                    $("#Td_Grid_Det_Egr").css("display","none");
                    $("#Td_Mostrar_PFD").css("display","none");
                 }
                 else {
                      $.messager.alert('Mensaje',"No hay Detalles",'info');
                 }
            }
        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar el grid de los detalles de ' + Det + '. Error: [' + Ex + ']','error');
    }
 }
 
///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Detalles_Mov_Egr
///DESCRIPCIÓN          : Funcion para llenar el grid de los detalles de los movimientos de egresos
///PARAMETROS           1 Tipo: ampliacion o reduccion
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 24/Agosto/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///***********************************************************************************************************************
 function Detalles_Mov_Egr(Tipo, P_Nivel, P_Capitulo, P_Concepto, P_Partida_Gen, P_Partida, P_FF, P_AF, P_PP, P_UR)
 {
    var Det = '';
    var Complemento = '';
    Det_Niv = '';
    Det_Cap = '';
    Det_Con = '';
    Det_PG = '';
    Det_PE = '';
    Det_FF = '';
    Det_PP = '';
    Det_AF = '';
    Det_UR = '';
    
    if(Tipo == 1)
    {
        Det = 'AMPLIACION';
    }else if(Tipo == 2)
    {
        Det = 'REDUCCION';
    }
    
    if(P_Nivel == 1){Det_Niv = 'Nivel_FF';}
    else if(P_Nivel == 2){Det_Niv = 'Nivel_AF';}
    else if(P_Nivel == 3){Det_Niv = 'Nivel_PP';}
    else if(P_Nivel == 4){Det_Niv = 'Nivel_UR';}
    else if(P_Nivel == 5){Det_Niv = 'Nivel_Capitulos';}
    else if(P_Nivel == 6){Det_Niv = 'Nivel_Concepto';}
    else if(P_Nivel == 7){Det_Niv = 'Nivel_Partida_Gen';}
    else if(P_Nivel == 8){Det_Niv = 'Nivel_Partida';}
    
    if(P_Capitulo > 0){
        for(i=0; i<=4-P_Capitulo.toString().length; i++){Complemento = Complemento + "0";}
        Det_Cap = Complemento + P_Capitulo;
    }
    Complemento = '';
    if(P_Concepto > 0){
        for(i=0; i<=4-P_Concepto.toString().length; i++){Complemento = Complemento + "0";}
        Det_Con = Complemento + P_Concepto;
    }
    Complemento = '';
    if(P_Partida_Gen > 0){
        for(i=0; i<=4-P_Partida_Gen.toString().length; i++){Complemento = Complemento + "0";}
        Det_PG = Complemento + P_Partida_Gen;
    }
    Complemento = '';
    if(P_Partida > 0){
        for(i=0; i<=9-P_Partida.toString().length; i++){Complemento = Complemento + "0";}
        Det_PE = Complemento + P_Partida;
    }
    Complemento = '';
    if(P_FF > 0){
        for(i=0; i<=4-P_FF.toString().length; i++){Complemento = Complemento + "0";}
        Det_FF = Complemento + P_FF;
    }
    Complemento = '';
    if(P_PP > 0){
        for(i=0; i<=9-P_PP.toString().length; i++){Complemento = Complemento + "0";}
        Det_PP = Complemento + P_PP;
    }
    Complemento = '';
    if(P_AF > 0){
        for(i=0; i<=4-P_AF.toString().length; i++){Complemento = Complemento + "0";}
        Det_AF = Complemento + P_AF;
    }
    Complemento = '';
    if(P_UR > 0){
        for(i=0; i<=4-P_UR.toString().length; i++){Complemento = Complemento + "0";}
        Det_UR = Complemento + P_UR;
    }
    
    var Parametros = Obtener_Parametros();
    
    try{
        $.ajax({
            url:'Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=Det_Mov_Egresos&Tipo_Det=' + Det + Parametros ,
            type:'POST',
            async: false,
            cache: false,
            dataType:'json',
            success: function(Datos) {
                 if (Datos != null) {
	                $('#Grid_Det_Mov_Egr').datagrid({
                        title: '',
                        url:'Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=Det_Mov_Egresos&Tipo_Det=' + Det + Parametros ,
                        width: 1150,
                        height:320,
                        nowrap: false,
                        striped: true,
                        collapsible:false,
                        pagination: false,
                        rownumbers: false,
                        fitColumns: true,
                        showFooter: true,
                        remoteSort: true,
                        loadMsg: 'Cargando Datos',
                        toolbar:[
                            {
					            text:'Detalles Movimientos Egresos',
					            iconCls:'icon-reimprimir'
				            },
				            { //barra de herramientas para mostrar botones
				                text:'Generar Reporte', //titulo del boton
				                iconCls:'icon-excel', //icono que se usara 
				                handler:function(){ //funcion que se efectuara al hacer click sobre el boton
					                Obtener_Reporte_Detalles(Parametros, 'Det_Mov_Egresos', Det);
					            }
				            },
				            { //barra de herramientas para mostrar botones
					            text:'Regresar', //titulo del boton
					            iconCls:'icon-undo', //icono que se usara 
					            handler:function(){ //funcion que se efectuara al hacer click sobre el boton
						            $("#Td_Grid_Det_Mov_Egr").css("display","none");
						            $("#Td_Iconos").css("display","block");
						            Obtener_Presupuesto();
					            }
				            }
				        ],
                        columns: [[
                            { field: 'NO_SOLICITUD_EGR', title:'No Solicitud', width: 105, sortable:true,
                                formatter:function(value, row){
                                    var No_Modificacion = parseInt(row.NO_MODIFICACION.match(/([0-9])$/) ? row.NO_MODIFICACION : '0',10);
                                    var No_Solicitud = parseInt(row.NO_MOVIMIENTO.match(/([0-9])$/) ? row.NO_MOVIMIENTO : '0',10);
                                    var Anio = parseInt(row.ANIO.match(/([0-9])$/) ? row.ANIO : '0',10);
                                    var Tipo_Operacion = row.TIPO_OPERACION;
                                    var Tipo = '';
                                    
                                    if(Tipo_Operacion == "TRASPASO"){
                                        Tipo = 1;
                                    }else if(Tipo_Operacion == "REDUCCION"){
                                        Tipo = 2;
                                    }else if(Tipo_Operacion == "AMPLIACION"){
                                        Tipo = 3;
                                    }else if(Tipo_Operacion == "ADECUACION"){
                                        Tipo = 4;
                                    }
                                    
                                    if(No_Solicitud != "")
                                    {return '<span><a onclick="Generar_Solicitud_Movimientos_Egresos('+ No_Modificacion + ','+No_Solicitud+','+Anio+','+Tipo+');" style="font-size:8pt; cursor:pointer;">'+value+'</a></span>';}
                                    else{return '<span style="font-size:xx-small">'+value+'</span>';}
                                }
                            },
                            { title: 'Partida', field: 'PARTIDA', width: 250, align: 'left',
                                formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { title:'Importe', field: 'IMPORTE', width: 70, align: 'right', 
                                formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { title: 'Fecha', field: 'FECHA_INICIO',  width: 50, align: 'left', sortable:true,
                                formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { title: 'Creo', field: 'USUARIO_CREO',  width: 140, align: 'left',
                                 formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { title: 'Justificación', field: 'JUSTIFICACION',  width: 250, align: 'left',
                                formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { title: 'Tipo', field: 'TIPO_OPERACION',  width: 55, align: 'left',
                                formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { title: 'Autorizo', field: 'USUARIO_MODIFICO',  width: 30, hidden: true, align: 'left' },
                            { title: 'No Modificacion', field: 'NO_MODIFICACION', align: 'left', hidden: true},
                            { title: 'No Movimiento', field: 'NO_MOVIMIENTO', align: 'left', hidden: true},
                            { title: 'Anio', field: 'ANIO', align: 'left', hidden: true},
                            { title: 'Fecha Autorizo', field: 'FECHA_AUTORIZO', align: 'left', hidden: true}
                          ]]
                   });
                   
                    $("#Td_Iconos").css("display","none");
                    $("#Td_Grid_Psp_Egr").css("display","none");
                    $("#Td_Grid_Psp_Ing").css("display","none");
                    $("#Td_Grid_Det_Mov_Ing").css("display","none");
                    $("#Td_Grid_Det_Rec_Ing").css("display","none");
                    $("#Td_Grid_Det_Mov_Egr").css("display","block");
                    $("#Td_Grid_Det_Egr").css("display","none");
                    $("#Td_Mostrar_PFD").css("display","none");
                 }
                 else {
                      $.messager.alert('Mensaje',"No hay Detalles",'info');
                 }
            }
        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar el grid de los detalles de ' + Det + '. Error: [' + Ex + ']','error');
    }
 }
 
 ///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Detalles_Recaudado_Ing
///DESCRIPCIÓN          : Funcion para llenar el grid de los detalles de l recaudado de ingresos
///PARAMETROS           1 Tipo: ampliacion o reduccion
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 29/Agosto/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///***********************************************************************************************************************
 function Detalles_Recaudado_Ing(Tipo_Det, P_Nivel, P_Rubro, P_Tipo, P_Clase, P_Concepto, P_SubConcepto, P_FF, P_PP)
 {
    var Complemento = '';
    Det_Niv = '';
    Det_Rub = '';
    Det_Tip = '';
    Det_Cla = '';
    Det_CoI = '';
    Det_Sub = '';
    Det_PPI = '';
    Det_FFI = '';
    P_Tipo_Det  = '';
    
    if(Tipo_Det == 1)
    {
        P_Tipo_Det = 'RECAUDADO';
    }else if(Tipo_Det == 2)
    {
        P_Tipo_Det = 'DEVENGADO';
    }
    
    if(P_Nivel == 1){Det_Niv = 'Nivel_FF_Ing';}
    else if(P_Nivel == 2){Det_Niv = 'Nivel_PP_Ing';}
    else if(P_Nivel == 3){Det_Niv = 'Nivel_Rubro';}
    else if(P_Nivel == 4){Det_Niv = 'Nivel_Tipo';}
    else if(P_Nivel == 5){Det_Niv = 'Nivel_Clase';}
    else if(P_Nivel == 6){Det_Niv = 'Nivel_Concepto_Ing';}
    else if(P_Nivel == 7){Det_Niv = 'Nivel_SubConcepto';}
    
    if(P_Rubro > 0){
        for(i=0; i<=4-P_Rubro.toString().length; i++){Complemento = Complemento + "0";}
        Det_Rub = Complemento + P_Rubro;
    }
    Complemento = '';
    if(P_Tipo > 0){
        for(i=0; i<=4-P_Tipo.toString().length; i++){Complemento = Complemento + "0";}
        Det_Tip = Complemento + P_Tipo;
    }
    Complemento = '';
    if(P_Clase > 0){
        for(i=0; i<=9-P_Clase.toString().length; i++){Complemento = Complemento + "0";}
        Det_Cla = Complemento + P_Clase;
    }
    Complemento = '';
    if(P_Concepto > 0){
        for(i=0; i<=9-P_Concepto.toString().length; i++){Complemento = Complemento + "0";}
        Det_CoI = Complemento + P_Concepto;
    }
    Complemento = '';
    if(P_SubConcepto > 0){
        for(i=0; i<=9-P_SubConcepto.toString().length; i++){Complemento = Complemento + "0";}
        Det_Sub = Complemento + P_SubConcepto;
    }
    Complemento = '';
    if(P_PP > 0){
        for(i=0; i<=9-P_PP.toString().length; i++){Complemento = Complemento + "0";}
        Det_PPI = Complemento + P_PP;
    }
    Complemento = '';
    if(P_FF > 0){
        for(i=0; i<=4-P_FF.toString().length; i++){Complemento = Complemento + "0";}
        Det_FFI = Complemento + P_FF;
    }
    
    var Parametros = Obtener_Parametros();
    
    try{
        $.ajax({
            url:'Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=Det_Recaudado_Ing&Tipo_Det=' + P_Tipo_Det + Parametros ,
            type:'POST',
            async: false,
            cache: false,
            dataType:'json',
            success: function(Datos) {
                 if (Datos != null) {
	                $('#Grid_Det_Rec_Ing').datagrid({
                        title: '',
                        url:'Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=Det_Recaudado_Ing&Tipo_Det=' + P_Tipo_Det + Parametros ,
                        width: 1150,
                        height:320,
                        nowrap: false,
                        striped: true,
                        collapsible:false,
                        pagination: false,
                        rownumbers: false,
                        fitColumns: true,
                        showFooter: true,
                        remoteSort: true,
                        loadMsg: 'Cargando Datos',
                        toolbar:[
                            {
					            text:'Detalles Recaudado Ingresos',
					            iconCls:'icon-reimprimir'
				            },
				            { //barra de herramientas para mostrar botones
					            text:'Generar Reporte', //titulo del boton
					            iconCls:'icon-excel', //icono que se usara 
					            handler:function(){ //funcion que se efectuara al hacer click sobre el boton
						            Obtener_Reporte_Detalles(Parametros, 'Det_Recaudado_Ing', P_Tipo_Det);
						        }
					        },
				            { //barra de herramientas para mostrar botones
					            text:'Regresar', //titulo del boton
					            iconCls:'icon-undo', //icono que se usara 
					            handler:function(){ //funcion que se efectuara al hacer click sobre el boton
						            $("#Td_Grid_Det_Rec_Ing").css("display","none");
						            $("#Td_Iconos").css("display","block");
						            Obtener_Presupuesto();
					            }
				            }
				        ],
                        columns: [[
                            { field: 'NO_MOVIMIENTO', title:'No', width: 30, sortable:true,
                                formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { title: 'No. Poliza', field: 'NO_POLIZA', width: 80, align: 'left', sortable:true,
                                formatter:function(value, row){
                                    var No_Poliza = parseInt(row.NO_POLIZA.match(/([0-9])$/) ? row.NO_POLIZA : '0',10);
                                    var Tipo_Poliza = parseInt(row.TIPO_POLIZA_ID.match(/([0-9])$/) ? row.TIPO_POLIZA_ID : '0',10);
                                    var Mes_Anio = parseInt(row.MES_ANO.match(/([0-9])$/) ? row.MES_ANO : '0',10);
                                    
                                    if(No_Poliza != "")
                                    {return '<span><a onclick="Generar_Poliza('+ No_Poliza + ','+Tipo_Poliza+','+Mes_Anio+');" style="font-size:8pt; cursor:pointer;">'+value+'</a></span>';}
                                    else{return '<span style="font-size:xx-small">'+value+'</span>';}
                                }
                            },
                            { title: 'Concepto', field: 'CONCEPTO', width: 250, align: 'left',
                                formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { title:'Importe', field: 'IMPORTE_TOTAL', width: 90, align: 'right', 
                                formatter:function(value){
                                    if(parseFloat(value.replace(/,/gi,'')) < 0){
		                                return '<span style="font-size:xx-small; color:red;">'+value+'</span>';
		                            }else{
		                                return '<span style="font-size:xx-small">'+value+'</span>';
		                            }
                                }
                            },
                            { title: 'Fecha', field: 'FECHA_INICIO',  width: 60, align: 'left', sortable:true,
                                formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { title: 'Creo', field: 'USUARIO_CREO',  width: 150, align: 'left',
                                 formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { title: 'Descripción', field: 'DESCRIPCION',  width: 250, align: 'left',
                                formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { title: 'Referencia', field: 'REFERENCIA',  width: 250, align: 'left',
                                formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { title: 'Tipo Poliza', field: 'TIPO_POLIZA_ID',  width: 30, hidden: true, align: 'left' },
                            { title: 'Mes Anio', field: 'MES_ANO',  width: 30, align: 'left', hidden: true}
                          ]]
                   });
                   
                    $("#Td_Iconos").css("display","none");
                    $("#Td_Grid_Psp_Egr").css("display","none");
                    $("#Td_Grid_Psp_Ing").css("display","none");
                    $("#Td_Grid_Det_Mov_Ing").css("display","none");
                    $("#Td_Grid_Det_Rec_Ing").css("display","block");
                    $("#Td_Grid_Det_Mov_Egr").css("display","none");
                    $("#Td_Grid_Det_Egr").css("display","none");
                    $("#Td_Mostrar_PFD").css("display","none");
                 }
                 else {
                      $.messager.alert('Mensaje',"No hay Detalles",'info');
                 }
            }
        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar el grid de los detalles del recaudado. Error: [' + Ex + ']','error');
    }
 }
 
///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Detalles_Mov_Contable_Egr
///DESCRIPCIÓN          : Funcion para llenar el grid de los detalles de los movimientos contables de egresos
///PARAMETROS           1 Tipo: devengado, pagado y comprometido
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 29/Agosto/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///***********************************************************************************************************************
 function Detalles_Mov_Contable_Egr(Tipo, P_Nivel, P_Capitulo, P_Concepto, P_Partida_Gen, P_Partida, P_FF, P_AF, P_PP, P_UR)
 {
    var Det = '';
    var Complemento = '';
    Det_Niv = '';
    Det_Cap = '';
    Det_Con = '';
    Det_PG = '';
    Det_PE = '';
    Det_FF = '';
    Det_PP = '';
    Det_AF = '';
    Det_UR = '';
    
    if(Tipo == 1)
    {
        Det = 'DEVENGADO';
    }else if(Tipo == 2)
    {
        Det = 'COMPROMETIDO';
    }
    else if(Tipo == 3)
    {
        Det = 'PAGADO';
    }
    else if(Tipo == 4)
    {
        Det = 'PRE_COMPROMETIDO';
    }
    else if(Tipo == 5)
    {
        Det = 'EJERCIDO';
    }
    
    if(P_Nivel == 1){Det_Niv = 'Nivel_FF';}
    else if(P_Nivel == 2){Det_Niv = 'Nivel_AF';}
    else if(P_Nivel == 3){Det_Niv = 'Nivel_PP';}
    else if(P_Nivel == 4){Det_Niv = 'Nivel_UR';}
    else if(P_Nivel == 5){Det_Niv = 'Nivel_Capitulos';}
    else if(P_Nivel == 6){Det_Niv = 'Nivel_Concepto';}
    else if(P_Nivel == 7){Det_Niv = 'Nivel_Partida_Gen';}
    else if(P_Nivel == 8){Det_Niv = 'Nivel_Partida';}
    
    if(P_Capitulo > 0){
        for(i=0; i<=4-P_Capitulo.toString().length; i++){Complemento = Complemento + "0";}
        Det_Cap = Complemento + P_Capitulo;
    }
    Complemento = '';
    if(P_Concepto > 0){
        for(i=0; i<=4-P_Concepto.toString().length; i++){Complemento = Complemento + "0";}
        Det_Con = Complemento + P_Concepto;
    }
    Complemento = '';
    if(P_Partida_Gen > 0){
        for(i=0; i<=4-P_Partida_Gen.toString().length; i++){Complemento = Complemento + "0";}
        Det_PG = Complemento + P_Partida_Gen;
    }
    Complemento = '';
    if(P_Partida > 0){
        for(i=0; i<=9-P_Partida.toString().length; i++){Complemento = Complemento + "0";}
        Det_PE = Complemento + P_Partida;
    }
    Complemento = '';
    if(P_FF > 0){
        for(i=0; i<=4-P_FF.toString().length; i++){Complemento = Complemento + "0";}
        Det_FF = Complemento + P_FF;
    }
    Complemento = '';
    if(P_PP > 0){
        for(i=0; i<=9-P_PP.toString().length; i++){Complemento = Complemento + "0";}
        Det_PP = Complemento + P_PP;
    }
    Complemento = '';
    if(P_AF > 0){
        for(i=0; i<=4-P_AF.toString().length; i++){Complemento = Complemento + "0";}
        Det_AF = Complemento + P_AF;
    }
    Complemento = '';
    if(P_UR > 0){
        for(i=0; i<=4-P_UR.toString().length; i++){Complemento = Complemento + "0";}
        Det_UR = Complemento + P_UR;
    }
    
    var Parametros = Obtener_Parametros();
    
    try{
        $.ajax({
            url:'Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=Det_Mov_Con_Egr&Tipo_Det=' + Det + Parametros ,
            type:'POST',
            async: false,
            cache: false,
            dataType:'json',
            success: function(Datos) {
                 if (Datos != null) {
	                $('#Grid_Det_Egr').datagrid({
                        title: '',
                        url:'Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=Det_Mov_Con_Egr&Tipo_Det=' + Det + Parametros ,
                        width: 1150,
                        height:320,
                        nowrap: false,
                        striped: true,
                        collapsible:false,
                        pagination: false,
                        rownumbers: false,
                        showFooter: true,
                        remoteSort: true,
                        loadMsg: 'Cargando Datos',
                        toolbar:[
                            {
					            text:'Detalles Movimientos Egresos',
					            iconCls:'icon-reimprimir'
				            },
				            { //barra de herramientas para mostrar botones
					            text:'Generar Reporte', //titulo del boton
					            iconCls:'icon-excel', //icono que se usara 
					            handler:function(){ //funcion que se efectuara al hacer click sobre el boton
						            Obtener_Reporte_Detalles(Parametros, 'Det_Mov_Con_Egr', Det);
						        }
					        },
				            { //barra de herramientas para mostrar botones
					            text:'Regresar', //titulo del boton
					            iconCls:'icon-undo', //icono que se usara 
					            handler:function(){ //funcion que se efectuara al hacer click sobre el boton
						            $("#Td_Grid_Det_Egr").css("display","none");
						            $("#Td_Iconos").css("display","block");
						            Obtener_Presupuesto();
					            }
				            }
				        ],
				        frozenColumns:[[
				            { field: 'NO_RESERVA', title:'No Reserva', width: 85, sortable:true, sortable:true,
                                formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { field: 'NO_POLIZA', title:'No Poliza', width: 75, sortable:true,
                                formatter:function(value, row){
                	                if(Det != "COMPROMETIDO" && Det != "PRE_COMPROMETIDO")
                	                {
                	                    var No_Poliza = parseInt(row.NO_POLIZA.match(/([0-9])$/) ? row.NO_POLIZA : '0',10);
                                        var Tipo_Poliza = parseInt(row.TIPO_POLIZA_ID.match(/([0-9])$/) ? row.TIPO_POLIZA_ID : '0',10);
                                        var Mes_Anio = parseInt(row.MES_ANO.match(/([0-9])$/) ? row.MES_ANO : '0',10);
                                        
                                        if(No_Poliza != "")
                                        {return '<span><a onclick="Generar_Poliza('+ No_Poliza + ','+Tipo_Poliza+','+Mes_Anio+');" style="font-size:8pt; cursor:pointer;">'+value+'</a></span>';}
                                        else{return '<span style="font-size:xx-small">'+value+'</span>';}
                	                }
                	                else
                	                {
                	                    return '<span style="font-size:xx-small">'+value+'</span>';
                	                }
                                }
                            },
                            { title: 'Partida', field: 'PARTIDA', width: 250, align: 'left',
                                formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { title:'Importe', field: 'IMPORTE', width: 80, align: 'right', 
                                formatter:function(value){
                                    if(parseFloat(value.replace(/,/gi,'')) < 0){
		                                return '<span style="font-size:xx-small; color:red;">'+value+'</span>';
		                            }else{
		                                return '<span style="font-size:xx-small">'+value+'</span>';
		                            }
                                }
                            }
				        ]],
                        columns: [[
                            { title: 'Fecha', field: 'FECHA',  width: 65, align: 'left', sortable:true, sortable:true,
                                formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { title: 'Creo', field: 'USUARIO_MOV',  width: 150, align: 'left',
                                 formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { title: 'Concepto', field: 'CONCEPTO',  width: 250, align: 'left',
                                formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { title: 'Beneficiario', field: 'BENEFICIARIO',  width: 250, align: 'left',
                                formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { title: 'Tipo Solicitud', field: 'TIPO_SOLICITUD',  width: 120, align: 'left',
                                formatter:function(value){
                	                return '<span style="font-size:xx-small">'+value+'</span>';
                                }
                            },
                            { title: 'Dependencia', field: 'DEPENDENCIA',  width: 30, hidden: true, align: 'left' },
                            { title: 'Programa', field: 'PROGRAMA',  width: 30, hidden: true, align: 'left' },
                            { title: 'Fuente Financiamiento', field: 'FUENTE',  width: 30, hidden: true, align: 'left' },
                            { title: 'Tipo Poliza', field: 'TIPO_POLIZA_ID',  width: 30, hidden: true, align: 'left' },
                            { title: 'Mes Anio', field: 'MES_ANO',  width: 30, hidden: true, align: 'left' },
                            { title: 'Anio', field: 'ANIO',  width: 30, align: 'left', hidden: true}
                          ]]
                   });
                   
                    $("#Td_Iconos").css("display","none");
                    $("#Td_Grid_Psp_Egr").css("display","none");
                    $("#Td_Grid_Psp_Ing").css("display","none");
                    $("#Td_Grid_Det_Mov_Ing").css("display","none");
                    $("#Td_Grid_Det_Rec_Ing").css("display","none");
                    $("#Td_Grid_Det_Mov_Egr").css("display","none");
                    $("#Td_Grid_Det_Egr").css("display","block");
                    $("#Td_Mostrar_PFD").css("display","none");
                    
                    if(Tipo == 1 || Tipo == 3 || Tipo == 5)
                    {
                        $('#Grid_Det_Egr').datagrid('showColumn', 'NO_POLIZA');
                    }
                    else if(Tipo == 2 || Tipo == 4)
                    {
                        $('#Grid_Det_Egr').datagrid('hideColumn', 'NO_POLIZA');
                    }
                 }
                 else {
                      $.messager.alert('Mensaje',"No hay Detalles",'info');
                 }
            }
        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar el grid de los detalles de ' + Det.toLowerCase() + '. Error: [' + Ex + ']','error');
    }
 }

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Mostrar_Archivo_PDF
///DESCRIPCIÓN          : Funcion para mostrar la pantalla de pdf con la solicitud de movimiento de egresos, las polizas 
///                     : o la solicitud de pagos
///PARAMETROS           1 No: numero de poliza, solicitud o reserva
///                     2 Tipo: si es una solicitud de pago, una poliza o una solicitud de movimiento de egresos
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 30/Agosto/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///***********************************************************************************************************************
 function Mostrar_Archivo_PDF(Ruta)
 {
    try{
        $('#Div_Mostrar_PDF').window({ //abrimos la ventana donde mostraremos el pdf
            title: 'Presupuesto',
            width: 1170,
            height:500,
            modal: false,
            shadow: false,
            closed: false,
            closable:true,
            collapsible:false,
            maximizable:false,
            minimizable:false,
            closable:true,
            draggable:true,
            top: 5,
            left:5
        });
    
        var Div_Mostrar_PDF = $('#Div_Mostrar_PDF'); //a la variable le asignamos el nombre del div
        var Cadena_PDF  = ''; //declaramos la variable donde almacenaremos el control <a> q crearemos para el link
        
        $("#Td_Mostrar_PFD").css("display","block"); //mostramos el td donde se encuetra  el pdf
        $('#Div_Mostrar_PDF').empty(); //limpiamos el contenido del div
        
        Cadena_PDF = '<a class="media" href="' + Ruta + '"></a>'; //creamos el control del link
        
        $(Cadena_PDF).appendTo(Div_Mostrar_PDF); //al div le agregamos el control q acabamos de crear
        $('a.media').media({width:1150, height:450}); //al plugin de media le agregamos las dimensiones para mostrar el pdf
        
        setTimeout("Eliminar_Archivo('" + Ruta + "')", 30000);
    }catch(Ex){
        $.messager.alert('Mensaje','Error al mostrar el pdf. Error: [' + Ex + ']','error');
    }
 }
 
 ///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Generar_Reporte
///DESCRIPCIÓN          : Funcion para obtener el reporte de excel, word o pdf
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 31/Agosto/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Generar_Reporte(Tipo_Reporte){
    var Parametros = Obtener_Parametros();
    var Niv_FF_Ing = 'N';
    var Niv_PP_Ing = 'N';
    var Niv_Rub = 'N';
    var Niv_Tip = 'N';
    var Niv_Cla = 'N';
    var Niv_Con_Ing = 'N';
    var Niv_SubCon = 'N';
    
    var Niv_FF = 'N';
    var Niv_PP = 'N';
    var Niv_AF = 'N';
    var Niv_UR = 'N';
    var Niv_Cap = 'N';
    var Niv_Con = 'N';
    var Niv_Par_Gen = 'N';
    var Niv_Par = 'N';
    
    //obtenemos los niveles del presupuesto
    if($("input:checkbox[id$=Chk_FF_Ing]").attr('checked')){
        Niv_FF_Ing = 'S';
    }
    if($("input:checkbox[id$=Chk_PP_Ing]").attr('checked')){
        Niv_PP_Ing = 'S';
    }
    if($("input:checkbox[id$=Chk_R]").attr('checked')){
        Niv_Rub = 'S';
    }
    if($("input:checkbox[id$=Chk_T]").attr('checked')){
       Niv_Tip = 'S';
    }
    if($("input:checkbox[id$=Chk_Cl]").attr('checked')){
        Niv_Cla = 'S';
    }
    if($("input:checkbox[id$=Chk_Con_Ing]").attr('checked')){
        Niv_Con_Ing = 'S';
    }
    if($("input:checkbox[id$=Chk_SubCon]").attr('checked')){
        Niv_SubCon = 'S';
    }
    
    if($("input:checkbox[id$=Chk_FF]").attr('checked')){
        Niv_FF = 'S';
    }
    if($("input:checkbox[id$=Chk_AF]").attr('checked')){
        Niv_AF = 'S';
    }
    if($("input:checkbox[id$=Chk_PP]").attr('checked')){
        Niv_PP = 'S';
    }
    if($("input:checkbox[id$=Chk_Ur]").attr('checked')){
        Niv_UR = 'S';
    }
    if($("input:checkbox[id$=Chk_Cap]").attr('checked')){
        Niv_Cap = 'S';
    }
    if($("input:checkbox[id$=Chk_Con]").attr('checked')){
        Niv_Con = 'S';
    }
    if($("input:checkbox[id$=Chk_PG]").attr('checked')){
        Niv_Par_Gen = 'S';
    }
    if($("input:checkbox[id$=Chk_PE]").attr('checked')){
        Niv_Par = 'S';
    }
    
    //obtenemos los parametros
    var Param_Niv = "&Niv_FF_Ing=" + Niv_FF_Ing + "&Niv_PP_Ing=" + Niv_PP_Ing + "&Niv_Rub=" + Niv_Rub +
        "&Niv_Tip=" + Niv_Tip + "&Niv_Cla=" + Niv_Cla + "&Niv_Con_Ing=" + Niv_Con_Ing + "&Niv_SubCon=" + Niv_SubCon + 
        "&Niv_FF=" + Niv_FF + "&Niv_AF=" + Niv_AF + "&Niv_PP=" + Niv_PP + "&Niv_UR=" + Niv_UR +
        "&Niv_Cap=" + Niv_Cap + "&Niv_Con=" + Niv_Con + "&Niv_Par_Gen=" + Niv_Par_Gen + "&Niv_Par=" + Niv_Par;
    
    try{
        $.ajax({
            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=Reporte&Reporte=" + Tipo_Reporte + Param_Niv + Parametros,
            type:'POST',
            async: false,
            cache: false,
            success: function(Datos) {
                 if (Datos != null) {
                   if(Datos != '')
                   {
                       if (Tipo_Reporte == 'PDF')
                       {
                            Mostrar_Archivo_PDF(Datos); //llamamos el metodo para mostrar el pdf
                       }else
                       {
                           document.location = decodeURI(Datos); //mostramos el reporte de excel y word en una ventada dando opcion a descargar, guardar o cancelar
                           setTimeout("Eliminar_Archivo('" + Datos + "')", 30000);
                       }
                   }
                 }
                 else {
                      $.messager.alert('Mensaje',"No se obtubo datos del reporte",'info');
                 }
            }
        });
    }catch(Ex){
        $.messager.alert('Mensaje','Error al obtener los datos del usuario. Error: [' + Ex + ']','error');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Progress
///DESCRIPCIÓN          : Funcion para generar el progress de la pagina(Prueba) porque solo funciona en e9, chrome, mozilla
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 12/Octubre/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Progress(Accion)
{
    try
    {
        if(Accion == "Crear")
        {
            // Create
            options = {
                img1: '../imagenes/paginas/Img_Psp_Progress1.png',
                img2: '../imagenes/paginas/Img_Psp_Progress2.png',
                speed: 100, // speed (timeout)
                PIStep : 0.05, // every step foreground area is bigger about this val
                limit: 100, // end value
                loop : false, //if true, no matter if limit is set, progressbar will be running
                showPercent : true, //show hide percent
                onInit: function(){console.log('init');},
                onProgress: function(p){/*console.log('progress',p);*/},
                onComplete: function(p){console.log('complete',p);}
            }; 

            myplugin = $('#p1').cprogress(options);//creamos el progress
        }
        else
        {
            myplugin=myplugin.destroy();//destruir el progress
            //activamos el progress
            if(myplugin){
                myplugin.start();
            } 
            
            //paramos el progress
            if(myplugin){
                myplugin.stop();
            } 
            
            //reiniciamos el progress
            if(myplugin){
                myplugin.reset();
            } 
        }
    }
    catch(Ex){
        $.messager.alert('Mensaje','Error al activar el progress. Error: [' + Ex + ']','error');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Generar_Poliza
///DESCRIPCIÓN          : Funcion para generar la poliza y mostrarla en pdf
///PARAMETROS           1 No_Poliza: numero de poliza
///                     2 Tipo_Poliza: tipo de poliza
///                     3 MesAnio: mes y año de poliza
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 04/Diciembre/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Generar_Poliza(No_Poliza, Tipo_Poliza, MesAnio)
{
    var Complemento = '';
    var Det_No_Poliza = '';
    var Det_Tipo_Poliza = '';
    var Parametros = '';
    var Det_Mes_Anio = '';
    
    try{
        if(No_Poliza > 0){
            for(i=0; i<=9-No_Poliza.toString().length; i++){Complemento = Complemento + "0";}
            Det_No_Poliza = Complemento + No_Poliza;
        }
        
        Complemento = '';
        if(Tipo_Poliza > 0){
            for(i=0; i<=4-Tipo_Poliza.toString().length; i++){Complemento = Complemento + "0";}
            Det_Tipo_Poliza = Complemento + Tipo_Poliza;
        }
        
         Complemento = '';
        if(MesAnio > 0){
            for(i=0; i<=3-MesAnio.toString().length; i++){Complemento = Complemento + "0";}
            Det_Mes_Anio = Complemento + MesAnio;
        }
        
        Parametros = "?Accion=Generar_Poliza&No_Poliza=" + Det_No_Poliza + "&Tipo_Poliza=" + Det_Tipo_Poliza;
        Parametros += "&Mes_Anio=" + Det_Mes_Anio; 
        
         $.ajax({
            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx" + Parametros,
            type:'POST',
            async: false,
            cache: false,
            success: function(Datos) {
                 if (Datos != null) {
                   if(Datos != '')
                   {
                        Mostrar_Archivo_PDF(Datos); //llamamos el metodo para mostrar el pdf
                   }
                 }
                 else {
                      $.messager.alert('Mensaje',"No se obtubo datos del reporte",'info');
                 }
            }
        });
    }catch(Ex){
        $.messager.alert('Mensaje','Error al generar la poliza. Error: [' + Ex + ']','error');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Eliminar_Archivo
///DESCRIPCIÓN          : Funcion para eliminar archivos
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 04/Diciembre/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Eliminar_Archivo(Ruta)
{
    try{
        $.ajax({
            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion=Eliminar_Archivo&Ruta=" + Ruta,
            type:'POST',
            async: false,
            cache: false
        });
    }catch(e){
        
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Generar_Solicitud_Movimientos_Ingresos
///DESCRIPCIÓN          : Funcion para generar la solicitud de modificaciones de ingresos y mostrarla en pdf
///PARAMETROS           1 No_Modificacion: número de modificación
///                     2 No_Solicitud: número de solicitud
///                     3 Anio: año de la solicitud
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 05/Diciembre/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Generar_Solicitud_Movimientos_Ingresos(No_Modificacion, No_Solicitud, Anio)
{
    var Parametros = '';
    
    try{

        Parametros = "?Accion=Generar_Solicitud_Ing&No_Modificacion=" + No_Modificacion + "&No_Solicitud=" + No_Solicitud;
        Parametros += "&Anio=" + Anio; 
        
         $.ajax({
            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx" + Parametros,
            type:'POST',
            async: false,
            cache: false,
            success: function(Datos) {
                 if (Datos != null) {
                   if(Datos != '')
                   {
                        Mostrar_Archivo_PDF(Datos); //llamamos el metodo para mostrar el pdf
                   }
                 }
                 else {
                      $.messager.alert('Mensaje',"No se obtubo datos del reporte",'info');
                 }
            }
        });
    }catch(Ex){
        $.messager.alert('Mensaje','Error al generar la solicitud de movimientos de Ingresos. Error: [' + Ex + ']','error');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Generar_Solicitud_Movimientos_Egresos
///DESCRIPCIÓN          : Funcion para generar la solicitud de modificaciones de Egresos y mostrarla en pdf
///PARAMETROS           1 No_Modificacion: número de modificación
///                     2 No_Solicitud: número de solicitud
///                     3 Anio: año de la solicitud
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 05/Diciembre/2012
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Generar_Solicitud_Movimientos_Egresos(No_Modificacion, No_Solicitud, Anio, Tipo)
{
    var Parametros = '';
    var Tipo_Det = '';
    
    if(Tipo == 1)
    {
        Tipo_Det = "TRASPASO";
    }
    else if(Tipo == 2)
    {
        Tipo_Det = "REDUCCION";
    }
    else if(Tipo == 3)
    {
        Tipo_Det = "AMPLIACION";
    }
    else if(Tipo == 4)
    {
        Tipo_Det = "ADECUACION";
    }
    
    try{

        Parametros = "?Accion=Generar_Solicitud_Egr&No_Modificacion=" + No_Modificacion + "&No_Solicitud=" + No_Solicitud;
        Parametros += "&Anio=" + Anio + "&Tipo_Det=" + Tipo_Det; 
        
         $.ajax({
            url: "Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx" + Parametros,
            type:'POST',
            async: false,
            cache: false,
            success: function(Datos) {
                 if (Datos != null) {
                   if(Datos != '')
                   {
                        Mostrar_Archivo_PDF(Datos); //llamamos el metodo para mostrar el pdf
                   }
                 }
                 else {
                      $.messager.alert('Mensaje',"No se obtubo datos del reporte",'info');
                 }
            }
        });
    }catch(Ex){
        $.messager.alert('Mensaje','Error al generar la solicitud de movimientos de Ingresos. Error: [' + Ex + ']','error');
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Obtener_Reporte_Detalles
///DESCRIPCIÓN          : Funcion obtener los detalles del presupuesto y generar el reporte en excel
///PARAMETROS           :
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 03/Septiembre/2013
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Obtener_Reporte_Detalles(Parametros, Tipo_Reporte, Detalle)
{
    try{
        $.ajax({
            url: 'Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos.aspx?Accion='+ Tipo_Reporte +'_Reporte&Tipo_Det=' + Detalle + Parametros ,
            type:'POST',
            async: false,
            cache: false,
            success: function(Datos) {
                 if (Datos != null) {
                   if(Datos != '')
                   {
                       document.location = decodeURI(Datos); //mostramos el reporte de excel en una ventana dando opcion a descargar, guardar o cancelar
//                       setTimeout("Eliminar_Archivo('" + Datos + "')", 90000);
                   }
                 }
                 else {
                      $.messager.alert('Mensaje',"No se obtubo datos del reporte",'info');
                 }
            }
        });
    }catch(Ex){
        $.messager.alert('Mensaje','Error al generar el reporte de los detalles. Error: [' + Ex + ']','error');
    }
}