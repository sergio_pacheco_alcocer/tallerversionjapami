﻿
var lastIndex;
var concepto_debe_b;
//Page Load
$(function() {
    Inicializar_Controles();
    //Se definen los eventos de los componentes
    $('#Txt_Busqueda').keydown(function(event) {
        if (event.which == 13) {
            //setTimeout(function(){Buscar_Usuarios()},20);
            Llenar_Tabla_Requisiciones();
        }
    });
    $('#Txt_Os').keydown(function(event) {
        if (event.which == 13) {
            //setTimeout(function(){Buscar_Usuarios()},20);
            Llenar_Tabla_Requisiciones();
        }
    });
    $('#Txt_Empleado_Recepcion').keydown(function(event) {
        if (event.which == 13) {
            //setTimeout(function(){Buscar_Usuarios()},20);
            Consultar_Empleado($('#Txt_Empleado_Recepcion').val(), $('#Txt_Nombre_Recepcion'));
        }
    });
    $('#Txt_Empleado_Envio').keydown(function(event) {
        if (event.which == 13) {
            //setTimeout(function(){Buscar_Usuarios()},20);
            Consultar_Empleado($('#Txt_Empleado_Envio').val(), $('#Txt_Nombre_Envio'));
        }
    });
    $('#Txt_Empleado_Entrada').keydown(function(event) {
        if (event.which == 13) {
            //setTimeout(function(){Buscar_Usuarios()},20);
            Consultar_Empleado($('#Txt_Empleado_Entrada').val(), $('#Txt_Nombre_Entrada'));
        }
    });
    $('#Txt_Empleado_Entrega').keydown(function(event) {
        if (event.which == 13) {
            //setTimeout(function(){Buscar_Usuarios()},20);
            Consultar_Empleado($('#Txt_Empleado_Entrega').val(), $('#Txt_Nombre_Entrega'));
        }
    });
    $('#Img_Buscar').click(function(event) {
        Btn_Buscar_Click();
    });
    $('#Img_Pdf').click(function(event) {
        Btn_Pdf_Click();
    });
    $('#Img_Listar_Requisiciones').click(function(event) {
        Habilitar_Controles("MODO_LISTADO");
    });
    $('#Img_Modificar').click(function(event) {
        var Estado = $('#Img_Modificar').attr('title');
        if (Estado == "Modificar") {
            Habilitar_Controles("MODO_MODIFICAR");
        }
        else {
            Guardar_Fechas();
        }
        return false;
    });
    $('#Img_Salir').click(function(event) {
        var Estado = $('#Img_Salir').attr('title');
        if (Estado == "Salir") {
            window.location = '../../Paginas/Paginas_Generales/Frm_Apl_Principal.aspx';
        }
        else {
            Habilitar_Controles("MODO_INICIAL");
            $('#Img_Warning').attr({
                style: 'display:none'
            });
            $('#Lbl_Informacion').html('<h1></h1>');
        }

    });
    $('#Img_Imprimir_Historial_Cambios').click(function(event) {
        Btn_Imprimir_Cambios_Click();
    });
    return false;
});
//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Inicializar_Controles()
//DESCRIPCIÓN: Inicializa los valores de los controles
//PARÁMETROS: 
//CREO: Jesus Toledo Rdz
//FECHA_CREO: 18/Jun/2013
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Inicializar_Controles() {
    var Combo_Dependencia_Panel = $("[id$='Cmb_Dependencia_Panel']");
    var Combo_Dependencia = $("[id$='Cmb_Dependencia']");
    var Dtp_Fecha_Inicial = $("#Dtp_Fecha_Inicial");
    var Dtp_Fecha_Final = $("#Dtp_Fecha_Final");

    var Dtp_Fecha_Envio = $("#Dtp_Fecha_Envio");
    var Dtp_Fecha_Recepcion = $("#Dtp_Fecha_Recepcion");
    var Dtp_Fecha_Entrada = $("#Dtp_Fecha_Entrada");
    var Dtp_Fecha_Entrega = $("#Dtp_Fecha_Entrega");

    var Fecha = new Date(); //variable para la fecha
    var Fecha_Inicial = new Date(); //variable para la fecha
    Cargar_Combo_Depencia(Combo_Dependencia_Panel);
    Cargar_Combo_Depencia(Combo_Dependencia);
    Cargar_Combos();
    Fecha_Inicial.setDate(1);
    Cargar_Data_Picker(Dtp_Fecha_Inicial, Fecha_Inicial);
    Cargar_Data_Picker(Dtp_Fecha_Final);
    Cargar_Data_Picker(Dtp_Fecha_Envio);
    Cargar_Data_Picker(Dtp_Fecha_Entrega);
    Cargar_Data_Picker(Dtp_Fecha_Recepcion);
    Cargar_Data_Picker(Dtp_Fecha_Entrada);
    //    $("#Dtp_Fecha_Entrega").datetimepicker();

    Dtp_Fecha_Envio.val("");
    Dtp_Fecha_Recepcion.val("");
    Dtp_Fecha_Entrada.val("");
    Dtp_Fecha_Entrega.val("");
    $('#Txt_Nombre_Entrega').val("");
    $('#Txt_Nombre_Entrada').val("");
    $('#Txt_Nombre_Envio').val("");
    $('#Txt_Nombre_Recepcion').val("");

    $('#Txt_Empleado_Entrega').val("");
    $('#Txt_Empleado_Entrada').val("");
    $('#Txt_Empleado_Envio').val("");
    $('#Txt_Empleado_Recepcion').val("");

    Habilitar_Controles("MODO_LISTADO");
    Crear_Tabla_Requisiciones();
    Crear_Tabla_Historial();
    Crear_Tabla_Observaciones();
    Crear_Tabla_Contrarecibos();
    Llenar_Tabla_Requisiciones();

}
function Limpiar_Controles() {
    var Dtp_Fecha_Envio = $("#Dtp_Fecha_Envio");
    var Dtp_Fecha_Recepcion = $("#Dtp_Fecha_Recepcion");
    var Dtp_Fecha_Entrada = $("#Dtp_Fecha_Entrada");
    var Dtp_Fecha_Entrega = $("#Dtp_Fecha_Entrega");
    Dtp_Fecha_Envio.val("");
    Dtp_Fecha_Recepcion.val("");
    Dtp_Fecha_Entrada.val("");
    Dtp_Fecha_Entrega.val("");
    $('#Txt_Nombre_Entrega').val("");
    $('#Txt_Nombre_Entrada').val("");
    $('#Txt_Nombre_Envio').val("");
    $('#Txt_Nombre_Recepcion').val("");

    $('#Txt_Empleado_Entrega').val("");
    $('#Txt_Empleado_Entrada').val("");
    $('#Txt_Empleado_Envio').val("");
    $('#Txt_Empleado_Recepcion').val("");

    $('#Img_Warning').attr({
        style: 'display:none'
    });
    $('#Lbl_Informacion').html('<h1></h1>');
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Cargar_Data_Picker()
//DESCRIPCIÓN: Inicializa un Input tipo Text como DataPicker
//PARÁMETROS: P_Selector: El control al que se le pone la extension DataPicker
//CREO: Jesus Toledo Rdz
//FECHA_CREO: 18/Jun/2013
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Cargar_Data_Picker(P_Selector, P_Fecha_Inicial) {
    var Selector = P_Selector; //Variable para obtener el control
    var Fecha = new Date(); //variable para la fecha
    if (P_Fecha_Inicial != null)
        Fecha = P_Fecha_Inicial;
    Selector.datepicker({
        changeMonth: true,
        changeYear: true,
        showOn: "button",
        buttonImage: "../../paginas/imagenes/paginas/SmallCalendar.gif",
        buttonImageOnly: true,
        dateFormat: 'dd/M/yy',
        altFormat: 'dd/M/yy',
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        constrainInput: true,
        defaultDate: Fecha
    });

    //Colocar el calendario en español
    $.datepicker.setDefaults($.datepicker.regional[""]);
    Selector.datepicker($.datepicker.regional["es"]);
    Selector.val(Formato_Fecha(Fecha));
}
//------------------------------------------------------------------------
//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Cargar_Combo()
//DESCRIPCIÓN: trae información del servidor para asignarlos a un combo
//PARÁMETROS: tipo de informacion atraer
//CREO: Sergio Pacheco Alcocer
//FECHA_CREO: 05/04/2012
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Cargar_Combo_Depencia(Combo) {
    $.ajax({
        url: "Frm_Controlador_Seguimiento_Rq_Servicios.aspx?Accion=Cargar_Combo_Dependencia",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            if (data != null) {
                var select = Combo;
                $('option', select).remove();
                var options = '<option value="-1"><-SELECCIONE-></option>';
                $.each(data, function(i, item) {
                    options += '<option value="' + item.dependencia_id + '">' + item.clave_nombre + '</option>';
                });
                Combo.append(options);
            }
        },
        error: function(result) {
            alert("ERROR " + result.status + " " + result.statusText);
        }
    });
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Cargar_Combos()
//DESCRIPCIÓN: trae información del servidor para asignarlos a un combo
//PARÁMETROS: tipo de informacion atraer
//CREO: Jesus Toledo Rodriguez
//FECHA_CREO: 17/Junio/2013
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Cargar_Combos() {
    var select = $("[id$='Cmb_Tipo_Busqueda']");
    $('option', select).remove();
    var options = '<option value="0"><-TODOS-></option>';
    options += '<option value="TRANSITORIA">TRANSITORIA</option>';
    options += '<option value="STOCK">STOCK</option>';
    select.append(options);

    select = $('#Cmb_Estatus');
    options = '<option value="0"><-TODOS-></option>';
    options += '<option value="RECIBIDO">RECIBIDO</option>';
    options += '<option value="ENVIADO">ENVIADO</option>';
    options += '<option value="ENTRADA">ENTRADA</option>';
    options += '<option value="ENTREGADO">ENTREGADO</option>';
    select.append(options);



}
//*******************************************************************************
//NOMBRE DE LA FUNCION:    Formato_Fecha
//DESCRIPCION:             Dar el formato dd/MMM/yyyy a una fecha
//PARAMETROS:              Fecha: Variable Date que contiene la fecha a convertir
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              13/Agosto/2011 11:40 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Formato_Fecha(Fecha) {
    //Declaracion de variables
    var Resultado = "";
    var Fecha_Date = new Date(Fecha);

    try {
        if (Fecha.toString().length > 0) {
            //Construccion de la fecha
            //Verificar si el dia es de un digito
            if (Fecha_Date.getDate().toString().length == 1) {
                Resultado = "0" + Fecha_Date.getDate().toString() + "/";
            } else {
                Resultado = Fecha_Date.getDate().toString() + "/";
            }

            //Seleccionar el mes
            switch (Fecha_Date.getMonth()) {
                case 0:
                    Resultado = Resultado + "Ene/";
                    break;

                case 1:
                    Resultado = Resultado + "Feb/";
                    break;

                case 2:
                    Resultado = Resultado + "Mar/";
                    break;

                case 3:
                    Resultado = Resultado + "Abr/";
                    break;

                case 4:
                    Resultado = Resultado + "May/";
                    break;

                case 5:
                    Resultado = Resultado + "Jun/";
                    break;

                case 6:
                    Resultado = Resultado + "Jul/";
                    break;

                case 7:
                    Resultado = Resultado + "Ago/";
                    break;

                case 8:
                    Resultado = Resultado + "Sep/";
                    break;

                case 9:
                    Resultado = Resultado + "Oct/";
                    break;

                case 10:
                    Resultado = Resultado + "Nov/";
                    break;

                case 11:
                    Resultado = Resultado + "Dic/";
                    break;

                default:
                    break;
            }

            //Colocar el año
            Resultado = Resultado + Fecha_Date.getFullYear().toString();
        }
        //Entregar resultado
        return Resultado;
    } catch (ex) {
        return ex.Message.toString();
    }
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Habilitar_Controles()
//DESCRIPCIÓN: Habilita la configuracion de acuerdo a la operacion
//PARÁMETROS: tipo de informacion atraer
//CREO: Jesus Toledo Rodriguez
//FECHA_CREO: 17/Junio/2013
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Habilitar_Controles(Modo) {
    switch (Modo) {
        case 'MODO_LISTADO':
            $('#Img_Nuevo').attr({
                style: 'display:none;cursor:pointer',
                title: 'Nuevo',
                AlternateText: 'Nuevo',
                src: '../../paginas/imagenes/paginas/icono_nuevo.png'
            });
            $('#Img_Modificar').attr({
                style: 'display:none;cursor:pointer',
                title: 'Modificar',
                AlternateText: 'Modificar',
                src: '../../paginas/imagenes/paginas/icono_modificar.png'
            });
            $('#Img_Eliminar').attr({
                style: 'display:none;cursor:pointer',
                title: 'Eliminar',
                AlternateText: 'Eliminar',
                src: '../../paginas/imagenes/paginas/icono_eliminar.png'
            });
            $('#Img_Listar_Requisiciones').attr({
                style: 'display:none;cursor:pointer',
                title: 'Listar Requisiciones',
                AlternateText: 'Listar_Requisiciones',
                src: '../../paginas/imagenes/paginas/icono_salir.png'
            });
            $('#Img_Salir').attr({
                style: 'display:inline;cursor:pointer',
                title: 'Salir',
                AlternateText: 'Salir',
                src: '../../paginas/imagenes/paginas/icono_salir.png'
            });
            $('.Cajas').attr({
                disabled: 'disabled',
                title: function() { return Formato_Fecha(Date()); }
            });
            $('#Dtp_Fecha_Inicial').attr({
                disabled: 'disabled'
            });
            $('#Dtp_Fecha_Final').attr({
                disabled: 'disabled'
            });
            $('.Detalles').attr({
                disabled: 'disabled'
            });
            $('.Combos').attr({
                disabled: 'disabled'
            });
            $("[id$='Div_Contenido']").attr({
                style: 'display:none;'
            });
            $("[id$='Div_Listado_Requisiciones']").attr({
                style: 'display:inline;'
            });
            $("[id$='Div_Encabezado']").attr({
                style: 'display:inline;'
            });
            $('#Img_Buscar').attr({
                src: '../../paginas/imagenes/paginas/busqueda.png',
                title: 'Consultar',
                style: 'display:inline;cursor:pointer',
                AlternateText: 'Consultar'
            });
            $('#Img_Imprimir_Historial_Cambios').attr({
                src: '../../paginas/imagenes/paginas/icono_rep_pdf.png',
                title: 'Imprimir Listado Cambios',
                style: 'display:none;cursor:pointer',
                AlternateText: 'Imprimir Listado Cambios'
            });
            break;
        case 'MODO_INICIAL':
            $("[id$='Div_Listado_Requisiciones']").attr({
                style: 'display:none;'
            });
            $("[id$='Div_Contenido']").attr({
                style: 'display:inline;'
            });
            $('.Cajas').attr({
                disabled: 'disabled'
            });
            $('.Combos').attr({
                disabled: 'disabled'
            });
            $('#Dtp_Fecha_Envio').datepicker('disable');
            $('#Dtp_Fecha_Entrega').datepicker('disable');
            $('#Dtp_Fecha_Entrada').datepicker('disable');
            $('#Dtp_Fecha_Recepcion').datepicker('disable');

            $('#Img_Listar_Requisiciones').attr({
                style: 'display:inline;cursor:pointer',
                title: 'Listar Requisiciones',
                AlternateText: 'Listar_Requisiciones',
                src: '../../paginas/imagenes/paginas/icono_salir.png'
            });
            $('#Img_Salir').attr({
                style: 'display:none;cursor:pointer',
                title: 'Salir',
                AlternateText: 'Salir',
                src: '../../paginas/imagenes/paginas/icono_salir.png'
            });
            $('#Img_Modificar').attr({
                style: 'display:inline;cursor:pointer',
                title: 'Modificar',
                AlternateText: 'Modificar',
                src: '../../paginas/imagenes/paginas/icono_modificar.png'
            });
            $('#Img_Imprimir_Historial_Cambios').attr({
                src: '../../paginas/imagenes/paginas/icono_rep_pdf.png',
                title: 'Imprimir Listado Cambios',
                style: 'display:inline;cursor:pointer',
                AlternateText: 'Imprimir Listado Cambios'
            });
            break;
        case 'MODO_MODIFICAR':
            $("[id$='Div_Listado_Requisiciones']").attr({
                style: 'display:none;'
            });
            $("[id$='Div_Contenido']").attr({
                style: 'display:inline;'
            });
            $('#Dtp_Fecha_Envio').datepicker('enable');
            $('#Dtp_Fecha_Entrega').datepicker('enable');
            $('#Dtp_Fecha_Entrada').datepicker('enable');
            $('#Dtp_Fecha_Recepcion').datepicker('enable');
            $('#Img_Listar_Requisiciones').attr({
                style: 'display:none;cursor:pointer',
                title: 'Listar Requisiciones',
                AlternateText: 'Listar_Requisiciones',
                src: '../../paginas/imagenes/paginas/icono_salir.png'
            });
            $('#Img_Salir').attr({
                style: 'display:none;cursor:pointer',
                title: 'Salir',
                AlternateText: 'Salir',
                src: '../../paginas/imagenes/paginas/icono_salir.png'
            });
            $('#Img_Modificar').attr({
                style: 'display:inline;cursor:pointer',
                title: 'Guardar',
                AlternateText: 'Guardar',
                src: '../../paginas/imagenes/paginas/icono_guardar.png'
            });
            $('.Cajas').removeAttr('disabled');
            //            $('.Combos').removeAttr('disabled');
            $('#Img_Salir').attr({
                style: 'display:inline;cursor:pointer',
                title: 'Cancelar',
                AlternateText: 'Cancelar',
                src: '../../paginas/imagenes/paginas/icono_cancelar.png'
            });
            $('#Img_Imprimir_Historial_Cambios').attr({
                src: '../../paginas/imagenes/paginas/icono_rep_pdf.png',
                title: 'Imprimir Listado Cambios',
                style: 'display:none;cursor:pointer',
                AlternateText: 'Imprimir Listado Cambios'
            });
            break;
        default: break;
    }
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Crear_Tabla_Requisiciones
//DESCRIPCIÓN: crea un grid de easy ui
//PARÁMETROS:
//CREO: Jesus Toledo Rodriguez
//FECHA_CREO: 17/Jun/2013
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Crear_Tabla_Requisiciones() {
    $('#Tbl_Requisiciones').datagrid({  // tabla inicio
        title: 'Listado Requisiciones',
        width: 790,
        height: 500,
        columns: [[
        { field: 'FOLIO', title: 'Folio', width: 80, sortable: true, align: 'center' },
        { field: 'OC', title: 'O.S.', width: 40, sortable: true, align: 'center' },
        { field: 'TIPO', title: 'Tipo', width: 120, sortable: true, align: 'center' },
        { field: 'FECHA_GENERACION', title: 'Fecha', width: 170, sortable: true, align: 'center' },
        { field: 'NOMBRE_DEPENDENCIA', title: 'U. Responsable', width: 230, sortable: true, align: 'center' },
        { field: 'DEPENDENCIA_ID', title: 'ID Dependencia', width: 230, sortable: true, align: 'center' },
        { field: 'ESTATUS_BIEN', title: 'Estatus', width: 110, sortable: true, align: 'center' },
        { field: 'NO_REQUISICION', title: 'RQ', width: 70, sortable: true, align: 'center' },
        { field: 'TOTAL', title: 'Total', width: 60, sortable: true, align: 'center', editor: { type: 'numberbox', options: { precision: 2, min: 0, max: 999999}} },
        { field: 'TOTAL_COTIZADO', title: 'Total Cotizado', width: 60, sortable: true, align: 'center' }
    ]],
        onClickRow: function(rowIndex, rowData) {
            Habilitar_Controles("MODO_INICIAL");
            $('#Dtp_Fecha_Entrada').val("");
            $('#Dtp_Fecha_Envio').val("");
            $('#Dtp_Fecha_Entrega').val("");
            $('#Dtp_Fecha_Recepcion').val("");
            Cargar_Datos_Rq(rowData);
        },
        pageSize: 50,
        pagination: true,
        rownumbers: true,
        remoteSort: false,
        fitColumns: true,
        singleSelect: true,
        showFooter: false,
        striped: true,
        fit: true,
        loadMsg: 'cargando...',
        nowrap: false
    });   // tabla final
    $('#Tbl_Requisiciones').datagrid('hideColumn', 'NO_REQUISICION');
    $('#Tbl_Requisiciones').datagrid('hideColumn', 'DEPENDENCIA_ID');

}

//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Llenar_Tabla_Requisiciones()
//DESCRIPCIÓN: Llena la tabla con informacion de la base de datos
//PARÁMETROS:
//CREO: Jesus Toledo Rodriguez
//FECHA_CREO: 17/Junio/2013
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Llenar_Tabla_Requisiciones() {
    var Estatus;
    var fecha_inicio_reporte;
    var fecha_final_reporte;
    var Unidad_Responsable_ID = $("[id$='Cmb_Dependencia_Panel']").val();
    var Folio = $('#Txt_Busqueda').val();
    var os = $('#Txt_Os').val();
    Estatus = $('#Cmb_Estatus').val();
    if ($("#Dtp_Fecha_Inicial").datepicker('getDate') != null)
        fecha_inicio_reporte = $("#Dtp_Fecha_Inicial").val(); //Convierte_Fecha_Datepicker($("#Dtp_Fecha_Inicial").datepicker('getDate'));
    if ($("#Dtp_Fecha_Final").datepicker('getDate') != null)
        fecha_final_reporte = $("#Dtp_Fecha_Final").val(); //Convierte_Fecha_Datepicker($("#Dtp_Fecha_Final").datepicker('getDate'));
    $('#Tbl_Requisiciones').datagrid('loadData', { total: 0, rows: [] });
    $('#Tbl_Requisiciones').datagrid({ url: 'Frm_Controlador_Seguimiento_Rq_Servicios.aspx', queryParams: {
        Accion: 'Consultar_Requisiciones',
        Fecha_Inicio: fecha_inicio_reporte,
        Fecha_Final: fecha_final_reporte,
        Unidad_Responsable: Unidad_Responsable_ID,
        Folio_Rq: Folio,
        Estatus_Bien: Estatus,
        Os: os
    }, pageNumber: 1
    });
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Crear_Tabla_Historial
//DESCRIPCIÓN: crea un grid de easy ui
//PARÁMETROS:
//CREO: Jesus Toledo Rodriguez
//FECHA_CREO: 19/Jun/2013
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Crear_Tabla_Historial() {
    $('#Tbl_Historial').datagrid({  // tabla inicio
        title: 'Historial de Estatus',
        width: 790,
        height: 800,
        columns: [[
        { field: 'FECHA', title: 'Fecha-Hora', width: 80, sortable: true, align: 'center' },
        { field: 'EMPLEADO', title: 'Modificó', width: 100, sortable: true, align: 'center' },
        { field: 'ESTATUS', title: 'Estatus', width: 70, sortable: true, align: 'center' }
    ]],
        onClickRow: function(rowIndex, rowData) {

        },
        pageSize: 50,
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: true,
        singleSelect: true,
        showFooter: false,
        striped: true,
        fit: true,
        loadMsg: 'cargando...',
        nowrap: false
    });   // tabla final
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Crear_Tabla_Comentarios
//DESCRIPCIÓN: crea un grid de easy ui
//PARÁMETROS:
//CREO: Jesus Toledo Rodriguez
//FECHA_CREO: 19/Jun/2013
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Crear_Tabla_Observaciones() {
    $('#Tbl_Comentarios').datagrid({  // tabla inicio
        title: 'Historial de Comentarios',
        width: 790,
        height: 1500,
        columns: [[
        { field: 'FECHA_CREO', title: 'Fecha-Hora', width: 180, sortable: true, align: 'center' },
        { field: 'USUARIO_CREO', title: 'Usuario', width: 100, sortable: true, align: 'center' },
        { field: 'ESTATUS', title: 'Estatus', width: 70, sortable: true, align: 'center' },
        { field: 'COMENTARIO', title: 'Comentario', width: 250, sortable: true, align: 'center' }
    ]],
        onClickRow: function(rowIndex, rowData) {

        },
        pageSize: 150,
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: true,
        singleSelect: true,
        showFooter: false,
        striped: true,
        fit: true,
        loadMsg: 'cargando...',
        nowrap: false
    });   // tabla final
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Crear_Tabla_Comentarios
//DESCRIPCIÓN: crea un grid de easy ui
//PARÁMETROS:
//CREO: Jesus Toledo Rodriguez
//FECHA_CREO: 19/Jun/2013
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Crear_Tabla_Contrarecibos() {
    $('#Tbl_Contrarecibos').datagrid({  // tabla inicio
        title: 'Contrarecibos',
        width: 790,
        height: 500,
        columns: [[
        { field: 'NO_CONTRA_RECIBO', title: 'No. Contra Recibo', width: 180, sortable: true, align: 'center' },
        { field: 'FECHA_PAGO', title: 'Fecha Pago', width: 140, sortable: true, align: 'center' }
    ]],
        onClickRow: function(rowIndex, rowData) {

        },
        pageSize: 50,
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: true,
        singleSelect: true,
        showFooter: false,
        striped: true,
        fit: true,
        loadMsg: 'cargando...',
        nowrap: false
    });   // tabla final
}


//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Llenar_Tabla_Historial()
//DESCRIPCIÓN: Llena la tabla con informacion de la base de datos
//PARÁMETROS:
//CREO: Jesus Toledo Rodriguez
//FECHA_CREO: 19/Junio/2013
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Llenar_Tabla_Historial() {
    var Folio = $("[id$='Txt_Folio']").val();
    $('#Tbl_Historial').datagrid('loadData', { total: 0, rows: [] });
    $('#Tbl_Historial').datagrid({ url: 'Frm_Controlador_Seguimiento_Rq_Servicios.aspx', queryParams: {
        Accion: 'Consultar_Historial',
        Folio_Rq: Folio
    }, pageNumber: 1
    });
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Llenar_Tabla_Observaciones()
//DESCRIPCIÓN: Llena la tabla con informacion de la base de datos
//PARÁMETROS:
//CREO: Jesus Toledo Rodriguez
//FECHA_CREO: 19/Junio/2013
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Llenar_Tabla_Observaciones() {
    var Folio = $("[id$='Txt_Folio']").val();
    $('#Tbl_Comentarios').datagrid('loadData', { total: 0, rows: [] });
    $('#Tbl_Comentarios').datagrid({ url: 'Frm_Controlador_Seguimiento_Rq_Servicios.aspx', queryParams: {
        Accion: 'Consultar_Observaciones',
        Folio_Rq: Folio
    }, pageNumber: 1
    });
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Llenar_Tabla_Observaciones()
//DESCRIPCIÓN: Llena la tabla con informacion de la base de datos
//PARÁMETROS:
//CREO: Jesus Toledo Rodriguez
//FECHA_CREO: 19/Junio/2013
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Llenar_Tabla_Contrarecibos() {
    var Folio = $("[id$='Txt_Folio']").val();
    $('#Tbl_Contrarecibos').datagrid('loadData', { total: 0, rows: [] });
    $('#Tbl_Contrarecibos').datagrid({ url: 'Frm_Controlador_Seguimiento_Rq_Servicios.aspx', queryParams: {
        Accion: 'Consultar_Contrarecibos',
        Folio_Rq: Folio
    }, pageNumber: 1
    });
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Cargar_Datos_Rq()
//DESCRIPCIÓN: Llena los controles con la informacion de la RQ
//PARÁMETROS: Dr_Requisicion[ Renglon de la Tabla EasyIU de Requisiciones]
//CREO: Jesus Toledo Rodriguez
//FECHA_CREO: 19/Junio/2013
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Cargar_Datos_Rq(Dr_Requisicion) {
    $("[id$='Txt_Folio']").val(Dr_Requisicion.FOLIO);
    $("[id$='Txt_OC']").val(Dr_Requisicion.OC);
    $("[id$='Txt_Total']").val(Dr_Requisicion.TOTAL);
    $("[id$='Txt_Total_Cotizado']").val(Dr_Requisicion.TOTAL_COTIZADO);
    $("[id$='Txt_Tipo']").val(Dr_Requisicion.TIPO);
    $("[id$='Cmb_Dependencia']").val(Dr_Requisicion.DEPENDENCIA_ID);
    if (Dr_Requisicion.FECHA_ENTRADA.toString().length > 0)
        $('#Dtp_Fecha_Entrada').val(convierteFechaSQL2DatePicker(Dr_Requisicion.FECHA_ENTRADA));
    $('#Dtp_Fecha_Envio').val(convierteFechaSQL2DatePicker(Dr_Requisicion.FECHA_ENVIO));
    $('#Dtp_Fecha_Entrega').val(convierteFechaSQL2DatePicker(Dr_Requisicion.FECHA_ENTREGA));
    $('#Dtp_Fecha_Recepcion').val(convierteFechaSQL2DatePicker(Dr_Requisicion.FECHA_RECEPCION));

    $('#Txt_Nombre_Entrega').val(Dr_Requisicion.EMPLEADO_ENTREGA);
    $('#Txt_Nombre_Entrada').val(Dr_Requisicion.EMPLEADO_ENTRADA);
    $('#Txt_Nombre_Envio').val(Dr_Requisicion.EMPLEADO_ENVIO);
    $('#Txt_Nombre_Recepcion').val(Dr_Requisicion.EMPLEADO_RECEPCION);

    if (Dr_Requisicion.OC != "") {
        $.ajax({ //inicio
            type: "POST",
            url: "Frm_Controlador_Seguimiento_Rq_Servicios.aspx/Consulta_Nombre_Proveedor",
            data: "{'P_Numero_Orden':'" + Dr_Requisicion.OC + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                var respuesta = eval(response);
                $("[id$='Txt_Proveedor']").val(respuesta.d);
            },
            error: function(result) {
                alert("ERROR " + result.status + ' ' + result.statusText);
            }
        });      //fin peticion ajax
    }
    Llenar_Tabla_Historial();
    Llenar_Tabla_Observaciones();
    Llenar_Tabla_Contrarecibos();
}
//*******************************************************************************
//NOMBRE DE LA FUNCION:    Convierte_Fecha_Datepicker
//DESCRIPCION:             Convertir la fecha del DatePicker en MM/dd/yyyy
//PARAMETROS:              fecha: Cadena de texto que contiene la fecha en el formato del DatePicker
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              06/Octubre/2011 14:06 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:
//*******************************************************************************
function Consultar_Empleado(Numero_Empleado, Caja_Texto) {
    var Nombre_Empleado = "";
    var Control = Caja_Texto;
    if (Numero_Empleado != "") {
        $.ajax({ //inicio
            type: "POST",
            url: "Frm_Controlador_Seguimiento_Rq_Servicios.aspx/Consultar_Empleado",
            data: "{'No_Empleado':'" + Numero_Empleado + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                var respuesta = eval(response);
                Control.val(respuesta.d);
            },
            error: function(result) {
                alert("ERROR " + result.status + ' ' + result.statusText);
            }
        });      //fin peticion ajax
    }
}
//*******************************************************************************
//NOMBRE DE LA FUNCION:    Convierte_Fecha_Datepicker
//DESCRIPCION:             Convertir la fecha del DatePicker en MM/dd/yyyy
//PARAMETROS:              fecha: Cadena de texto que contiene la fecha en el formato del DatePicker
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              06/Octubre/2011 14:06 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Convierte_Fecha_Datepicker(fecha) {
    //Declaracion de variables
    var resultado = ""; //variable para el resultado
    var vec_datos; //vector para el arreglo
    var indiceDia = 0; //variable para el indice del dia
    var indiceMes = 0; //variable apra el indice del mes
    var indiceAnio = 0; //variable para el indice del año
    var contElementos = 0; //variable para el contador
    var auxMayusculas = ""; //variable auxiliar para las mayusculas

    try {
        //obtener el vector de la fecha
        vec_datos = fecha.toString().split(" ");

        //verificar si el vector tiene elementos
        if (vec_datos.length > 0) {
            //Ciclo para el barrido de los elementos
            for (contElementos = 0; contElementos < vec_datos.length; contElementos++) {
                //Asignar la variable auxiliar para las mayusculas
                auxMayusculas = vec_datos[contElementos].toString().toUpperCase();

                //verificar si ese elemento es del mes
                if (auxMayusculas == "ENE" || auxMayusculas == "JAN" || auxMayusculas == "FEB" || auxMayusculas == "MAR" || auxMayusculas == "ABR" || auxMayusculas == "APR" || auxMayusculas == "MAY" || auxMayusculas == "JUN" || auxMayusculas == "JUL" || auxMayusculas == "AGO" || auxMayusculas == "AUG" || auxMayusculas == "SEP" || auxMayusculas == "OCT" || auxMayusculas == "NOV" || auxMayusculas == "DIC" || auxMayusculas == "DEC") {
                    indiceMes = contElementos;
                }

                //verificar si el elemento es de longitud uno o dos
                if (isFinite(vec_datos[contElementos].toString()) == true && (vec_datos[contElementos].toString().length == 2 || vec_datos[contElementos].toString().length == 1)) {
                    indiceDia = contElementos;
                }

                //Verificar si el elemento es de longitud 4
                if (isFinite(vec_datos[contElementos].toString()) == true && vec_datos[contElementos].toString().length == 4) {
                    indiceAnio = contElementos;
                }
            }

            //Colocar el mes
            if (vec_datos[indiceMes].toString().toUpperCase() == "ENE" || vec_datos[indiceMes].toString().toUpperCase() == "JAN") {
                resultado = "01/";
            }

            if (vec_datos[indiceMes].toString().toUpperCase() == "FEB") {
                resultado = "02/";
            }

            if (vec_datos[indiceMes].toString().toUpperCase() == "MAR") {
                resultado = "03/"
            }

            if (vec_datos[indiceMes].toString().toUpperCase() == "ABR" || vec_datos[indiceMes].toString().toUpperCase() == "APR") {
                resultado = "04/";
            }

            if (vec_datos[indiceMes].toString().toUpperCase() == "MAY") {
                resultado = "05/";
            }

            if (vec_datos[indiceMes].toString().toUpperCase() == "JUN") {
                resultado = "06/";
            }

            if (vec_datos[indiceMes].toString().toUpperCase() == "JUL") {
                resultado = "07/";
            }

            if (vec_datos[indiceMes].toString().toUpperCase() == "AGO" || vec_datos[indiceMes].toString().toUpperCase() == "AUG") {
                resultado = "08/";
            }

            if (vec_datos[indiceMes].toString().toUpperCase() == "SEP") {
                resultado = "09/";
            }

            if (vec_datos[indiceMes].toString().toUpperCase() == "OCT") {
                resultado = "10/";
            }

            if (vec_datos[indiceMes].toString().toUpperCase() == "NOV") {
                resultado = "11/";
            }

            if (vec_datos[indiceMes].toString().toUpperCase() == "DIC" || vec_datos[indiceMes].toString().toUpperCase() == "DEC") {
                resultado = "12/";
            }

            //Colocar el dia
            resultado += vec_datos[indiceDia] + "/";

            //Colocar el año
            resultado += vec_datos[indiceAnio];
        } else {
            resultado = "";
        }

        //Entregar Resultado
        return resultado;
    } catch (ex) {
        return ex.Message.toString();
    }
}
//*******************************************************************************
//NOMBRE DE LA FUNCION:    convierteFechaSQL2DatePicker
//DESCRIPCION:             Convertir la fecha del DatePicker 
//PARAMETROS:              fecha: Cadena de texto que contiene la fecha en el formato del DatePicker
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              06/Octubre/2011 14:06 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function convierteFechaSQL2DatePicker(fecha) {
    //declaracion de variables
    var resultado = ""; //variable para el resultado
    var vecFecha; //vector para el resultado

    try {
        //Crear el vector para los elementos de la fecha
        vecFecha = fecha.toString().split("/");

        //verificar si son 3 elementos
        if (vecFecha.length == 3) {
            //Colocar el dia
            resultado = vecFecha[0] + "/";

            //seleccionar el mes
            switch (Number(vecFecha[1])) {
                case 1:
                    resultado += "Ene";
                    break;

                case 2:
                    resultado += "Feb";
                    break;

                case 3:
                    resultado += "Mar";
                    break;

                case 4:
                    resultado += "Abr";
                    break;

                case 5:
                    resultado += "May";
                    break;

                case 6:
                    resultado += "Jun";
                    break;

                case 7:
                    resultado += "Jul";
                    break;

                case 8:
                    resultado += "Ago";
                    break;

                case 9:
                    resultado += "Sep";
                    break;

                case 10:
                    resultado += "Oct";
                    break;

                case 11:
                    resultado += "Nov";
                    break;

                case 12:
                    resultado += "Dic";
                    break;

                default:
                    break;

            }

            //colocar el año
            resultado += "/" + vecFecha[2].substring(0, 4);
        } else {
            resultado = "";
        }

        //Entregar resultado
        return resultado;
    } catch (ex) {
        return ex.Message.toString();
    }
}

//*******************************************************************************
//NOMBRE DE LA FUNCION:    convertirVector2Cadena
//DESCRIPCION:             Convertir la fecha del DatePicker en MM/dd/yyyy
//PARAMETROS:              fecha: Cadena de texto que contiene la fecha en el formato del DatePicker
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              06/Octubre/2011 14:06 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function convertirVectorCadena(vector) {
    //Declaracion de variables
    var resultado = ""; //variable para el resultado
    var contElementos = 0; //variable para el contador

    try {
        //Ciclo para el barrido de los elementos
        for (contElementos = 0; contElementos < vector.length; contElementos++) {
            //Verificar si el elemento es cero
            if (vector[contElementos] != "0") {
                resultado += vector[contElementos] + ",";
            }
        }

        //Quitarle la ultima coma al resultado
        resultado = resultado.substr(0, resultado.length - 1);

        //Entregar resultado
        return resultado;
    } catch (ex) {
        return ex.Message.toString();
    }
}

function Btn_Buscar_Click() {
    Llenar_Tabla_Requisiciones();
    return false;
}

function Btn_Imprimir_Cambios_Click() {
    try {
        var Folio = $("[id$='Txt_Folio']").val();
        window.open("frm_controlador_crystal.aspx?nombre_reporte=Imprimir_Historico_Cambios&Folio_Rq=" + Folio, "Historico_Cambios", "width=1px,height=1px,scrollbars=NO");
    } catch (ex) {
        $.messager.alert(ex.toString());
    }
}

function Btn_Pdf_Click() {
    try {
        var Folio = $("[id$='Txt_Folio']").val();
        window.open("frm_controlador_crystal.aspx?nombre_reporte=Imprimir_Requisicion&Folio_Rq=" + Folio, "Requisicion_Servicio", "width=1px,height=1px,scrollbars=NO");
    } catch (ex) {
        $.messager.alert(ex.toString());
    }
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Guardar_Fechas()
//DESCRIPCIÓN: Recupera informacion para mandarla al controlador para dar de alta las fechas
//PARÁMETROS: 
//CREO: Jesus Toledo Rodriguez
//FECHA_CREO: 19/Junio/2013
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Guardar_Fechas() {
    var Fecha_Envio = "";
    var Fecha_Recepcion = ""; ;
    var Fecha_Entrada = ""; ;
    var Fecha_Entrega = ""; ;
    var Folio = ""; ;
    var Empleado_Entrada = ""; ;
    var Empleado_Entrega = ""; ;
    var Empleado_Recepcion = ""; ;
    var Empleado_Envio = ""; ;
    var Validar_Nombres = true;
    var Mensaje_Error = "";
    Folio = $("[id$='Txt_Folio']").val();
    if ($("#Dtp_Fecha_Envio").datepicker('getDate') != null) {
        Fecha_Envio = $("#Dtp_Fecha_Envio").val(); //Convierte_Fecha_Datepicker($("#Dtp_Fecha_Envio").datepicker('getDate'));

        if ($("#Txt_Nombre_Envio").val().length > 0) {
            Empleado_Envio = $("#Txt_Nombre_Envio").val();
        }
        else {
            Validar_Nombres = false;
            Mensaje_Error += " - Es necesario definir a quien se Envia el Bien </br>";
        }
        if ($("#Dtp_Fecha_Recepcion").val().length <= 0) {
            Validar_Nombres = false;
            Mensaje_Error += " - Para Asignar Fecha de Envío se necesita haber especificado previamente Fecha de Recepción </br>";
        }

    }
    if ($("#Dtp_Fecha_Recepcion").datepicker('getDate') != null) {
        Fecha_Recepcion = $("#Dtp_Fecha_Recepcion").val(); //Convierte_Fecha_Datepicker($("#Dtp_Fecha_Recepcion").datepicker('getDate'));

        if ($("#Txt_Nombre_Recepcion").val().length > 0) {
            Empleado_Recepcion = $("#Txt_Nombre_Recepcion").val();
        }
        else {
            Validar_Nombres = false;
            Mensaje_Error += " - Es necesario definir quien recibe el Bien</br>";
        }
    }
    if ($("#Dtp_Fecha_Entrada").datepicker('getDate') != null) {
        Fecha_Entrada = $("#Dtp_Fecha_Entrada").val(); //Convierte_Fecha_Datepicker($("#Dtp_Fecha_Entrada").datepicker('getDate'));

        if ($("#Txt_Nombre_Entrada").val().length > 0) {
            Empleado_Entrada = $("#Txt_Nombre_Entrada").val();
        }
        else {
            Validar_Nombres = false;
            Mensaje_Error += " - Es necesario definir quien da entrada al Bien </br>";
        }
        if ($("#Dtp_Fecha_Recepcion").val().length <= 0 || $("#Dtp_Fecha_Envio").val().length <= 0) {
            Validar_Nombres = false;
            Mensaje_Error += " - Para Asignar Fecha de Entrada se necesita haber especificado previamente Fecha de Recepción y Envio </br>";
        }

    }
    if ($("#Dtp_Fecha_Entrega").datepicker('getDate') != null) {
        Fecha_Entrega = $("#Dtp_Fecha_Entrega").val(); //Convierte_Fecha_Datepicker($("#Dtp_Fecha_Entrega").datepicker('getDate'));

        if ($("#Txt_Nombre_Entrega").val().length > 0) {
            Empleado_Entrega = $("#Txt_Nombre_Entrega").val();
        }
        else {
            Validar_Nombres = false;
            Mensaje_Error += " - Es necesario definir a quien se entrega el Bien </br>";
        }
        if ($("#Dtp_Fecha_Recepcion").val().length <= 0 || $("#Dtp_Fecha_Envio").val().length <= 0 || $("#Dtp_Fecha_Entrada").val().length <= 0) {
            Validar_Nombres = false;
            Mensaje_Error += " - Para Asignar Fecha de Entrada se necesita haber especificado previamente Fecha de Recepción, Envio y Entrada </br>";
        }

    }
    if (Validar_Nombres) {

        $.ajax({
            type: "POST",
            url: "Frm_Controlador_Seguimiento_Rq_Servicios.aspx/Alta_Fechas_Requisicion",
            data: "{'Folio_Rq':'" + Folio +
             "','P_Fecha_Envio':'" + Fecha_Envio +
             "','P_Fecha_Recepcion':'" + Fecha_Recepcion +
             "','P_Fecha_Entrada':'" + Fecha_Entrada +
             "','P_Fecha_Entrega':'" + Fecha_Entrega +
             "','P_Empleado_Entrega':'" + Empleado_Entrega +
             "','P_Empleado_Entrada':'" + Empleado_Entrada +
             "','P_Empleado_Recepcion':'" + Empleado_Recepcion +
             "','P_Empleado_Envio':'" + Empleado_Envio +
             "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                $('#ventana_mensaje').window('close');
                var respuesta = eval("(" + response.d + ")");
                if (respuesta == "1") {
                    $.messager.alert('Japami', 'Proceso Terminado', 'info', function() { Habilitar_Controles("MODO_LISTADO"); Llenar_Tabla_Requisiciones(); });
                }
                else {
                    $.messager.alert('Japami', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                }
            },
            error: function(result) {
                $('#ventana_mensaje').window('close');
                $.messager.alert('Japami', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');
            }

        });
        Limpiar_Controles();
    }
    else {
        $('#Img_Warning').attr({
            style: 'display:inline'
        });
        $('#Lbl_Informacion').html('<h1>' + Mensaje_Error + '</h1>');
    }
}