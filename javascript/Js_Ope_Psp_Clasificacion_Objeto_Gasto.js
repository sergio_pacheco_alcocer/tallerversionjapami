﻿//*********************************************************************************************************************************************************
//************************************************************** VARIABLES ********************************************************************************
//*********************************************************************************************************************************************************
var nivel;
var Capitulo;
var Concepto;
var Partida_Generica;
var Partida;
var Tipo_Descripcion;

//*********************************************************************************************************************************************************
//************************************************************** INICIO ***********************************************************************************
//*********************************************************************************************************************************************************

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN :$(document).ready(function
///DESCRIPCIÓN          : Funcion para iniciar con los datos del formulario
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 05/Diciembre/2011 
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///***********************************************************************************************************************
 $(document).ready(function() {
     try
     {
        Llenar_Combo_Anios();
        Llenar_Combo_UR();
        $("#Tbl_Opciones").hide();
        
        //Evento del boton de salir
        $("input[id$=Btn_Salir]").click(function (e) {
            e.preventDefault();
            location.href='../Paginas_Generales/Frm_Apl_Principal.aspx';
        });
        
        //Evento del boton generar clasificacion
        $("input[id$=Btn_Generar]").click(function (e) {
            e.preventDefault();
            if($("select[id$=Cmb_Anios]").get(0).selectedIndex > 0){
                Generar_TreeGrid();
                $("#Tbl_Opciones").show();
            }else{
                $.messager.alert('Mensaje',"Favor de seleccionar un año");
                $("#Tbl_Opciones").hide();
            }
        });

    }catch(Ex){
        $.messager.alert('Mensaje','Error al ejecutar los eventos de la página. Error: [' + Ex + ']');
    }
 });
 
//*********************************************************************************************************************************************************
//************************************************************** METODOS **********************************************************************************
//*********************************************************************************************************************************************************
 
///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Anios
///DESCRIPCIÓN          : Funcion para llenar el combo de los anios
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 05/Diciembre/2011 
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Llenar_Combo_Anios()
{
    try{
        var Cmb =  $("select[id$=Cmb_Anios]").get(0);

        $.ajax({
            url: "Frm_Ope_Psp_Controlador.aspx?Accion=Anios",
            type:'POST',
            async: false,
            cache: false,
            dataType:'json',
            success: function(Datos) {
                 if (Datos != null) {
                     Cmb.options[0] = new Option('<- Seleccione ->', 'Seleccione'); 
                     $.each(Datos.anios, function (Contador, Elemento) {
                        Cmb.options[++Contador] = new Option(Elemento.ANIO, Elemento.ANIO);
                     });
                 }
                 else {
                      $.messager.alert('Mensaje',"No hay Años Presupuestados");
                 }
            }
        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar el combo de años presupuestados. Error: [' + Ex + ']');
    }
}
 
///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Generar_TreeGrid
///DESCRIPCIÓN          : Funcion para llenar el treegrid
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 05/Diciembre/2011 
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///***********************************************************************************************************************
 function Generar_TreeGrid()
 {
    try
    {
        Tipo_Descripcion = "";
        var Anio = $("select[id$=Cmb_Anios] option:selected").val();
        var Dependencia = $("select[id$=Cmb_UR] option:selected").val();
        $('#Grid_COG').treegrid({
		    title:'Estado Analítico de Egresos',
		    width:858,
		    height:530,
		    nowrap: false,
		    rownumbers: false,
		    animate:true, //Define si para mostrar el efecto de animación al nodo expandir o contraer. 	
		    collapsible:true,
		    url:'Frm_Ope_Psp_Controlador.aspx?Accion=Nivel_Capitulos&Anio=' + Anio+ '&UR=' + Dependencia,
		    idField:'id',
		    treeField:'texto',
		    resizable:true,
		    nowrap:true,
		    showFooter:true,
		    frozenColumns:[[
                {title:'Nombre',field:'texto',width:358,
                    formatter:function(value){
                	    return '<span style="color:Navy; font-size:9px">'+value+'</span>';
                    }
                }
		    ]],
		    columns:[[
		        {field:'descripcion6',title:'Aprobado',width:100, align: 'right',
			             formatter:function(value){
                	        return '<span style="font-size:xx-small">'+value+'</span>';
                        }
			    },
			    {field:'descripcion7',title:'Modificado',width:100, align: 'right',
			             formatter:function(value){
                	        return '<span style="font-size:xx-small">'+value+'</span>';
                        }
			    },
		        {field:'descripcion5',title:'Disponible',width:100, align: 'right',
			             formatter:function(value){
                	        return '<span style="font-size:xx-small">'+value+'</span>';
                        }
			    },
			    {field:'descripcion4',title:'Comprometido',width:110, align: 'right',
			         formatter:function(value){
                	    return '<span style="font-size:xx-small">'+value+'</span>';
                    }
			    },
			    {field:'descripcion1',title:'Devengado',width:100, align: 'right', 
			        formatter:function(value){
                	    return '<span  style="font-size:xx-small">'+value+'</span>';
                    }
			    },
			    {field:'descripcion2',title:'Ejercido',width:95, align: 'right',
			         formatter:function(value){
                	    return '<span  style="font-size:xx-small">'+value+'</span>';
                    }
			    },
			    {field:'descripcion3',title:'Pagado',width:95, align: 'right',
			         formatter:function(value){
                	    return '<span  style="font-size:xx-small">'+value+'</span>';
                    }
                }
		    ]],
		    onBeforeExpand:function(node){
		        Limpiar_Variables();
		        if(node.attributes != null)
                {
		            nivel = node.attributes.valor1;
		            Capitulo = node.attributes.valor2;
		            Concepto = node.attributes.valor3;
		            Partida_Generica = node.attributes.valor4;
		            Partida = node.attributes.valor5;
        		    
			        if (nivel == "nivel_Capitulos") {
                        $('#Grid_COG').treegrid('options').url = 'Frm_Ope_Psp_Controlador.aspx?Accion=Nivel_Conceptos&Anio=' + Anio+'&Capitulo=' + Capitulo+ '&UR=' + Dependencia;
                    }
                    if (nivel == "nivel_Concepto") {
                        $('#Grid_COG').treegrid('options').url = 'Frm_Ope_Psp_Controlador.aspx?Accion=Nivel_Partida_Generica&Anio=' + Anio+'&Capitulo=' + Capitulo+'&Concepto=' + Concepto+ '&UR=' + Dependencia;
                    }
                    if (nivel == "nivel_Partida_Generica") {
                        $('#Grid_COG').treegrid('options').url = 'Frm_Ope_Psp_Controlador.aspx?Accion=Nivel_Partida&Anio=' + Anio+'&Capitulo=' + Capitulo + "&Concepto=" + Concepto+'&Partida_Generica=' + Partida_Generica+ '&UR=' + Dependencia;
                    }
                }
           }
	    });
	}catch(Ex){
        $.messager.alert('Mensaje','Error al llenar generar la tabla. Error: [' + Ex + ']');
    }
 }
 
 
///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Limpiar_Variables
///DESCRIPCIÓN          : Funcion para limpiar las variables
///PARAMETROS           :
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 02/Diciembre/2011 
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///***********************************************************************************************************************
 function Limpiar_Variables(){
    nivel = "";
    Capitulo = "";
    Concepto = "";
    Partida_Generica = "";
    Partida = "";
 }
 
 ///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Llenar_Combo_UR
///DESCRIPCIÓN          : Funcion para llenar el combo de las unidades responsables
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 01/Marzo/2012 
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function Llenar_Combo_UR()
{
    try{
        var Cmb =  $("select[id$=Cmb_UR]").get(0);

        $.ajax({
            url: "Frm_Ope_Psp_Controlador.aspx?Accion=Unidades_Responsables",
            type:'POST',
            async: false,
            cache: false,
            dataType:'json',
            success: function(Datos) {
                 if (Datos != null) {
                     Cmb.options[0] = new Option('<- Seleccione ->', 'Seleccione'); 
                     $.each(Datos.ur, function (Contador, Elemento) {
                     Cmb.options[++Contador] = new Option(Elemento.Nombre, Elemento.DEPENDENCIA_ID);
                     });
                 }
                 else {
                      $.messager.alert('Mensaje',"No hay Unidades Responsables");
                 }
            }
        });
     }catch(Ex){
        $.messager.alert('Mensaje','Error al llenar el combo de unidades responsables. Error: [' + Ex + ']');
    }
}
 
  ///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Llenar_Combo_UR
///DESCRIPCIÓN          : Funcion para llenar el combo de las unidades responsables
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 01/Marzo/2012 
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function collapseAll(){
	$('#Grid_COG').treegrid('collapseAll');
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : expandAll
///DESCRIPCIÓN          : Funcion para abrir todos los nodos del treegrid
///PARAMETROS           : 
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 07/Mayo/2012 
///MODIFICO             : 
///FECHA_MODIFICO       : 
///CAUSA_MODIFICACIÓN   : 
///*********************************************************************************************************************** 
function expandAll(){
	$('#Grid_COG').treegrid('expandAll');
}