﻿///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Validar_Cantidad_Caracteres
///DESCRIPCIÓN          : Función para validar la cantidad de caracteres que se introducen al preguntar
///PARAMETROS           1 Obj control que esta haciendo la referencia a la funcion
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 24/Agosto/2011 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///***********************************************************************************************************************
function Validar_Cantidad_Caracteres(Obj) 
{
    var limit = 250;
    var len = Obj.value.length; //cuenta los caracteres conforme se van introduciendo
    
    if (len > limit) 
    {
        Obj.value = Obj.value.substring(0, limit); //elimina los caracteres de mas
    }
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Completar_Clave
///DESCRIPCIÓN          : Función para completar la clave 
///PARAMETROS           :
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 19/Mayo/2011 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///***********************************************************************************************************************
function Completar_Clave()
{
    var Clave = $("input[id$=Txt_Clave]").val();
    
    if($("select[id$=Cmb_Concepto] option:selected").val() == "")
    {
        var Concepto = "";
    }else
    {
        var Concepto = $("select[id$=Cmb_Concepto] option:selected").text();
        Concepto = Concepto.substring(0, Concepto.indexOf(" "));
        Concepto = Concepto.substring(0, 6);
    }
    
    
    if(Clave.length < 4)
    {
        var Complemento = ""; 
        for(i = 0; i<4-Clave.length; i++)
        {
            Complemento = Complemento + "0";
        }
        Clave = Complemento + Clave;
    }
    
    var Clave_Completa = Concepto + Clave;
    $("input[id$=Txt_Clave_Sub]").val(Clave_Completa)
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Completar_Clave_Concepto
///DESCRIPCIÓN          : Función para completar la clave 
///PARAMETROS           :
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 19/Mayo/2011 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///***********************************************************************************************************************
function Completar_Clave_Concepto()
{
    var ClaveCon = $("input[id$=Txt_Clave]").val();
    var ClaveSub = "0000";
    
    if($("select[id$=Cmb_Clase] option:selected").val() == "")
    {
        var Clase = "";
    }else
    {
        var Clase = $("select[id$=Cmb_Clase] option:selected").text();
        Clase = Clase.substring(0, Clase.indexOf(" "));
        Clase = Clase.substring(0,4);
    }
    
    
    if(ClaveCon.length < 2)
    {
        var Complemento = ""; 
        for(i = 0; i<2-ClaveCon.length; i++)
        {
            Complemento = Complemento + "0";
        }
        ClaveCon = Complemento + ClaveCon;
    }
    
    var Clave_Completa = Clase + ClaveCon + ClaveSub;
    $("input[id$=Txt_Clave_Con]").val(Clave_Completa)
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Completar_Clave_Clase
///DESCRIPCIÓN          : Función para completar la clave 
///PARAMETROS           :
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 19/Mayo/2011 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///***********************************************************************************************************************
function Completar_Clave_Clase()
{
    var ClaveClase = $("input[id$=Txt_Clave]").val();
    var ClaveSub = "000000";
    
    if($("select[id$=Cmb_Tipo] option:selected").val() == "")
    {
        var Tipo = "";
    }else
    {
        var Tipo = $("select[id$=Cmb_Tipo] option:selected").text();
        Tipo = Tipo.substring(0, Tipo.indexOf(" "));
        Tipo = Tipo.substring(0,2);
    }
    
    
    if(ClaveClase.length < 2)
    {
        var Complemento = ""; 
        for(i = 0; i<2-ClaveClase.length; i++)
        {
            Complemento = Complemento + "0";
        }
        ClaveClase = Complemento + ClaveClase;
    }
    
    var Clave_Completa = Tipo + ClaveClase + ClaveSub;
    $("input[id$=Txt_Clave_Clase]").val(Clave_Completa)
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Completar_Clave_Tipo
///DESCRIPCIÓN          : Función para completar la clave 
///PARAMETROS           :
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 19/Mayo/2011 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///***********************************************************************************************************************
function Completar_Clave_Tipo()
{
    var ClaveTipo = $("input[id$=Txt_Clave]").val();
    var ClaveSub = "00000000";
    
    if($("select[id$=Cmb_Rubro] option:selected").val() == "")
    {
        var Rubro = "";
    }else
    {
        var Rubro = $("select[id$=Cmb_Rubro] option:selected").text();
        Rubro = Rubro.substring(0, Rubro.indexOf(" "));
        Rubro = Rubro.substring(0,1);
    }
    
    
    if(ClaveTipo.length < 1)
    {
        var Complemento = ""; 
        for(i = 0; i<2-ClaveTipo.length; i++)
        {
            Complemento = Complemento + "0";
        }
        ClaveTipo = Complemento + ClaveTipo;
    }
    
    var Clave_Completa = Rubro + ClaveTipo + ClaveSub;
    $("input[id$=Txt_Clave_Tipo]").val(Clave_Completa)
}

///***********************************************************************************************************************
///NOMBRE DE LA FUNCIÓN : Completar_Clave_Rubro
///DESCRIPCIÓN          : Función para completar la clave 
///PARAMETROS           :
///CREO                 : Leslie González Vázquez
///FECHA_CREO           : 19/Mayo/2011 
///MODIFICO             :
///FECHA_MODIFICO       :
///CAUSA_MODIFICACIÓN   :
///***********************************************************************************************************************
function Completar_Clave_Rubro()
{
    var ClaveRubro = $("input[id$=Txt_Clave]").val();
    var ClaveSub = "000000000";
    
   
    if(ClaveRubro.length < 1)
    {
        var Complemento = ""; 
        for(i = 0; i<2-ClaveRubro.length; i++)
        {
            Complemento = Complemento + "0";
        }
        ClaveRubro = Complemento + ClaveRubro;
    }
    
    var Clave_Completa = ClaveRubro + ClaveSub;
    $("input[id$=Txt_Clave_Rubro]").val(Clave_Completa)
}