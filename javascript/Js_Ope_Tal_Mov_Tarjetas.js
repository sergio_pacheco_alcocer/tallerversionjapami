﻿$(function() {
    Inicializar_Controles();
    //Se definen los eventos de los componentes
    $('#Txt_Economico').keydown(function(event) {
        if (event.which == 13) {
            Llenar_Tabla_Movimientos();
        }
    });
    $('#Txt_Tarjeta').keydown(function(event) {
        if (event.which == 13) {
            Llenar_Tabla_Movimientos();
        }
    });
    $('#Txt_Folio_Factura').keydown(function(event) {
        if (event.which == 13) {
            $('#Txt_Fecha_Factura').focus();
        }
    });
    $('#Txt_Fecha_Factura').keydown(function(event) {
        if (event.which == 13) {
            $('#Txt_Monto').focus();
        }
    });
    $('#Txt_Monto').keydown(function(event) {
        if (event.which == 13) {
            Agregar_Facturas();
        }
    });
    $('#Txt_Monto').change(function() {
        $('#Txt_Monto').val(formatCurrency($('#Txt_Monto').val()));
        Calculo_De_Totales();
    });
    $('#Txt_Comision').change(function() {
        $('#Txt_Comision').val(formatCurrency($('#Txt_Comision').val()));
        Calculo_De_Totales();
    });
    $('#Cmb_Dependencia_Panel').keydown(function(event) {
        if (event.which == 13) {
            Llenar_Tabla_Movimientos();
        }
    });
    $('#Img_Buscar').click(function(event) {
        Btn_Buscar_Click();
    });
    //cierra las ventans del popup
    $('#ventana_imprimir_pagos').window('close');
});


//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Calculo_De_Totales()
//DESCRIPCIÓN: Agrega al Total el valor de la Comision y el IVA
//PARÁMETROS: 
//CREO: Jesus Toledo Rdz
//FECHA_CREO: 18/Jun/2013
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Calculo_De_Totales() {
    var Monto = Number($('#Txt_Monto').val().replace(/[^0-9\.]+/g, ""));
    var Comision = Number($('#Txt_Comision').val().replace(/[^0-9\.]+/g, ""));
    var Total = Monto + Comision;
    var Iva = Total * 0.16;
    $('#Txt_Iva').val(formatCurrency(Iva));
    $('#Txt_Total_Factura').val(formatCurrency((Total+Iva)));
}
    
//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Inicializar_Controles()
//DESCRIPCIÓN: Inicializa los valores de los controles
//PARÁMETROS: 
//CREO: Jesus Toledo Rdz
//FECHA_CREO: 18/Jun/2013
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Inicializar_Controles() {
    var Combo_Dependencia_Panel = $('#Cmb_Dependencia_Panel');
    var Dtp_Fecha_Inicial = $("#Dtp_Fecha_Inicial");
    var Dtp_Fecha_Final = $("#Dtp_Fecha_Final");
    var Dtp_Fecha_Factura = $('#Txt_Fecha_Factura');
    var Fecha = new Date(); //variable para la fecha
    var Fecha_Inicial = new Date(); //variable para la fecha
    Cargar_Combo_Depencia(Combo_Dependencia_Panel);
    Habilitar_Controles("MODO_LISTADO");
    Fecha_Inicial.setDate(1);
    Cargar_Data_Picker(Dtp_Fecha_Inicial, Fecha_Inicial);
    Cargar_Data_Picker(Dtp_Fecha_Final);
    Cargar_Data_Picker(Dtp_Fecha_Factura);
    Crear_Tabla_Movimientos();
    Crear_Tabla_Facturas();
    Llenar_Tabla_Movimientos();
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Crear_Tabla_Movimientos
//DESCRIPCIÓN: crea un grid de easy ui
//PARÁMETROS:
//CREO: Jesus Toledo Rodriguez
//FECHA_CREO: 17/Jun/2013
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Crear_Tabla_Movimientos() {
    $('#Tbl_Movimientos').datagrid({  // tabla inicio
        title: 'Listado Movimientos de Tarjetas',
        width: 790,
        height: 500,
        columns: [[
        { field: 'ck', title: 'Sel', checkbox: true },
        { field: 'numero_tarjeta', title: 'Tarjeta', width:90, sortable: true, align: 'center' },
        { field: 'vehiculo_descripcion', title: 'Bien', width: 200, sortable: true, align: 'center' },
        { field: 'economico', title: 'Económico', width: 120, sortable: true, align: 'center' },
        { field: 'fecha_generacion', title: 'Fecha', width: 170, sortable: true, align: 'center' },
        { field: 'nombre_dependencia', title: 'U. Responsable', width: 230, sortable: true, align: 'center' },
        { field: 'monto', title: 'Monto', width: 100, sortable: true, align: 'right' },
        { field: 'saldo', title: 'Saldo', width: 100, sortable: true, align: 'right' },
        { field: 'reserva', title: 'Reserva', width: 80, sortable: true, align: 'center' },
        { field: 'numero_movimiento', title: 'Movimiento', width: 80, sortable: true, align: 'center' },
        { field: 'dependencia_id', title: 'ID Dependencia', width: 0, sortable: true, align: 'center' }
    ]],
        toolbar: [{
            id: 'btn_pdf_todos',
            text: 'Ingresar Factura',
            iconCls: 'icon-add',
            handler: function() {
                Abrir_Popup();
            }
}],
        pageSize: 50,
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: true,
        singleSelect: false,
        showFooter: true,
        striped: true,
        fit: true,
        loadMsg: 'cargando...',
        nowrap: false
    });   // tabla final
    $('#Tbl_Movimientos').datagrid('hideColumn', 'dependencia_id');
    $('#Tbl_Movimientos').datagrid('hideColumn', 'numero_movimiento');

}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Crear_Tabla_Movimientos
//DESCRIPCIÓN: crea un grid de easy ui
//PARÁMETROS:
//CREO: Jesus Toledo Rodriguez
//FECHA_CREO: 17/Jun/2013
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Crear_Tabla_Facturas() {
    $('#Tbl_Facturas').datagrid({  // tabla inicio
        title: 'Listado Facturas',
        width: 790,
        height: 500,
        columns: [[
        { field: 'folio_factura', title: 'Folio', width:90, sortable: true, align: 'center' },
        { field: 'fecha_factura', title: 'Fecha', width: 200, sortable: true, align: 'center' },
        { field: 'monto_factura', title: 'Monto', width: 120, sortable: true, align: 'center' },
        { field: 'comision_factura', title: 'Comisión', width: 120, sortable: true, align: 'center' },
        { field: 'iva_factura', title: 'Iva', width: 170, sortable: true, align: 'center' },
        { field: 'total_factura', title: 'Total Factura', width: 230, sortable: true, align: 'center' }
    ]],        
    toolbar: [{
            id: 'btneliminar',
            text: 'Eliminar',
            iconCls: 'icon-cancel',
            handler: function() {
                Eliminar_Factura();
            }
    }],
        pageSize: 50,
        pagination: false,
        rownumbers: true,
        remoteSort: false,
        fitColumns: true,
        singleSelect: true,
        showFooter: true,
        striped: true,
        fit: true,
        loadMsg: 'cargando...',
        nowrap: false        
    });   // tabla final
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Llenar_Tabla_Movimientos()
//DESCRIPCIÓN: Llena la tabla con informacion de la base de datos
//PARÁMETROS:
//CREO: Jesus Toledo Rodriguez
//FECHA_CREO: 17/Junio/2013
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Llenar_Tabla_Movimientos() {
    var Tarjeta;
    var Economico;
    var fecha_inicio_reporte;
    var fecha_final_reporte;
    var Unidad_Responsable_ID = $('#Cmb_Dependencia_Panel').val();
    Tarjeta = $('#Txt_Tarjeta').val();
    Economico = $('#Txt_Economico').val();
    if ($("#Dtp_Fecha_Inicial").datepicker('getDate') != null)
        fecha_inicio_reporte = $("#Dtp_Fecha_Inicial").val(); //Convierte_Fecha_Datepicker($("#Dtp_Fecha_Inicial").datepicker('getDate'));
    if ($("#Dtp_Fecha_Final").datepicker('getDate') != null)
        fecha_final_reporte = $("#Dtp_Fecha_Final").val(); //Convierte_Fecha_Datepicker($("#Dtp_Fecha_Final").datepicker('getDate'));
    $('#Tbl_Movimientos').datagrid('loadData', { total: 0, rows: [] });
    $('#Tbl_Movimientos').datagrid({ url: 'Frm_Controlador_Mov_Tarj_Gas.aspx', queryParams: {
        Accion: 'Consultar_Movimientos',
        Fecha_Inicio: fecha_inicio_reporte,
        Fecha_Final: fecha_final_reporte,
        Unidad_Responsable: Unidad_Responsable_ID,
        No_Tarjeta: Tarjeta,
        No_Economico: Economico
    }, pageNumber: 1
    });
}

//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Cargar_Data_Picker()
//DESCRIPCIÓN: Inicializa un Input tipo Text como DataPicker
//PARÁMETROS: P_Selector: El control al que se le pone la extension DataPicker
//CREO: Jesus Toledo Rdz
//FECHA_CREO: 18/Jun/2013
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Cargar_Data_Picker(P_Selector, P_Fecha_Inicial) {
    var Selector = P_Selector; //Variable para obtener el control
    var Fecha = new Date(); //variable para la fecha
    if (P_Fecha_Inicial != null)
        Fecha = P_Fecha_Inicial;
    Selector.datepicker({
        changeMonth: true,
        changeYear: true,
        showOn: "button",
        buttonImage: "../../paginas/imagenes/paginas/SmallCalendar.gif",
        buttonImageOnly: true,
        dateFormat: 'dd/M/yy',
        altFormat: 'dd/M/yy',
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        constrainInput: true,
        defaultDate: Fecha
    });

    //Colocar el calendario en español
    $.datepicker.setDefaults($.datepicker.regional[""]);
    Selector.datepicker($.datepicker.regional["es"]);
    Selector.val(Formato_Fecha(Fecha));
}
//------------------------------------------------------------------------
//*******************************************************************************
//NOMBRE DE LA FUNCION:    Formato_Fecha
//DESCRIPCION:             Dar el formato dd/MMM/yyyy a una fecha
//PARAMETROS:              Fecha: Variable Date que contiene la fecha a convertir
//CREO:                    Noe Mosqueda Valadez
//FECHA_CREO:              13/Agosto/2011 11:40 
//MODIFICO:                
//FECHA_MODIFICO:          
//CAUSA_MODIFICACION:      
//*******************************************************************************
function Formato_Fecha(Fecha) {
    //Declaracion de variables
    var Resultado = "";
    var Fecha_Date = new Date(Fecha);

    try {
        if (Fecha.toString().length > 0) {
            //Construccion de la fecha
            //Verificar si el dia es de un digito
            if (Fecha_Date.getDate().toString().length == 1) {
                Resultado = "0" + Fecha_Date.getDate().toString() + "/";
            } else {
                Resultado = Fecha_Date.getDate().toString() + "/";
            }

            //Seleccionar el mes
            switch (Fecha_Date.getMonth()) {
                case 0:
                    Resultado = Resultado + "Ene/";
                    break;

                case 1:
                    Resultado = Resultado + "Feb/";
                    break;

                case 2:
                    Resultado = Resultado + "Mar/";
                    break;

                case 3:
                    Resultado = Resultado + "Abr/";
                    break;

                case 4:
                    Resultado = Resultado + "May/";
                    break;

                case 5:
                    Resultado = Resultado + "Jun/";
                    break;

                case 6:
                    Resultado = Resultado + "Jul/";
                    break;

                case 7:
                    Resultado = Resultado + "Ago/";
                    break;

                case 8:
                    Resultado = Resultado + "Sep/";
                    break;

                case 9:
                    Resultado = Resultado + "Oct/";
                    break;

                case 10:
                    Resultado = Resultado + "Nov/";
                    break;

                case 11:
                    Resultado = Resultado + "Dic/";
                    break;

                default:
                    break;
            }

            //Colocar el año
            Resultado = Resultado + Fecha_Date.getFullYear().toString();
        }
        //Entregar resultado
        return Resultado;
    } catch (ex) {
        return ex.Message.toString();
    }
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Habilitar_Controles()
//DESCRIPCIÓN: Habilita la configuracion de acuerdo a la operacion
//PARÁMETROS: tipo de informacion atraer
//CREO: Jesus Toledo Rodriguez
//FECHA_CREO: 17/Junio/2013
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Habilitar_Controles(Modo) {
    switch (Modo) {
        case 'MODO_LISTADO':

            $('#Img_Nuevo').attr({
                style: 'display:none;cursor:pointer',
                title: 'Nuevo',
                AlternateText: 'Nuevo',
                src: '../../paginas/imagenes/paginas/icono_nuevo.png'
            });
            $('#Img_Modificar').attr({
                style: 'display:none;cursor:pointer',
                title: 'Modificar',
                AlternateText: 'Modificar',
                src: '../../paginas/imagenes/paginas/icono_modificar.png'
            });
            $('#Img_Eliminar').attr({
                style: 'display:none;cursor:pointer',
                title: 'Eliminar',
                AlternateText: 'Eliminar',
                src: '../../paginas/imagenes/paginas/icono_eliminar.png'
            });
            $('#Img_Listar_Requisiciones').attr({
                style: 'display:none;cursor:pointer',
                title: 'Listar Requisiciones',
                AlternateText: 'Listar_Requisiciones',
                src: '../../paginas/imagenes/paginas/icono_salir.png'
            });
            $('#Img_Salir').attr({
                style: 'display:inline;cursor:pointer',
                title: 'Salir',
                AlternateText: 'Salir',
                src: '../../paginas/imagenes/paginas/icono_salir.png'
            });
            $('#Div_Encabezado').attr({
                style: 'display:inline;'
            });
            $('#Img_Buscar').attr({
                src: '../../paginas/imagenes/paginas/busqueda.png',
                title: 'Consultar',
                style: 'display:inline;cursor:pointer',
                AlternateText: 'Consultar'
            });
            break;
        case 'MODO_INICIAL':
            $("[id$='Div_Listado_Requisiciones']").attr({
                style: 'display:none;'
            });
            $("[id$='Div_Contenido']").attr({
                style: 'display:inline;'
            });
            $('.Cajas').attr({
                disabled: 'disabled'
            });
            $('.Combos').attr({
                disabled: 'disabled'
            });
            $('#Dtp_Fecha_Envio').datepicker('disable');
            $('#Dtp_Fecha_Entrega').datepicker('disable');
            $('#Dtp_Fecha_Entrada').datepicker('disable');
            $('#Dtp_Fecha_Recepcion').datepicker('disable');

            $('#Img_Listar_Requisiciones').attr({
                style: 'display:inline;cursor:pointer',
                title: 'Listar Requisiciones',
                AlternateText: 'Listar_Requisiciones',
                src: '../../paginas/imagenes/paginas/icono_salir.png'
            });
            $('#Img_Salir').attr({
                style: 'display:none;cursor:pointer',
                title: 'Salir',
                AlternateText: 'Salir',
                src: '../../paginas/imagenes/paginas/icono_salir.png'
            });
            $('#Img_Modificar').attr({
                style: 'display:inline;cursor:pointer',
                title: 'Modificar',
                AlternateText: 'Modificar',
                src: '../../paginas/imagenes/paginas/icono_modificar.png'
            });
            break;
        case 'MODO_MODIFICAR':
            $("[id$='Div_Listado_Requisiciones']").attr({
                style: 'display:none;'
            });
            $("[id$='Div_Contenido']").attr({
                style: 'display:inline;'
            });
            $('#Dtp_Fecha_Envio').datepicker('enable');
            $('#Dtp_Fecha_Entrega').datepicker('enable');
            $('#Dtp_Fecha_Entrada').datepicker('enable');
            $('#Dtp_Fecha_Recepcion').datepicker('enable');
            $('#Img_Listar_Requisiciones').attr({
                style: 'display:none;cursor:pointer',
                title: 'Listar Requisiciones',
                AlternateText: 'Listar_Requisiciones',
                src: '../../paginas/imagenes/paginas/icono_salir.png'
            });
            $('#Img_Salir').attr({
                style: 'display:none;cursor:pointer',
                title: 'Salir',
                AlternateText: 'Salir',
                src: '../../paginas/imagenes/paginas/icono_salir.png'
            });
            $('#Img_Modificar').attr({
                style: 'display:inline;cursor:pointer',
                title: 'Guardar',
                AlternateText: 'Guardar',
                src: '../../paginas/imagenes/paginas/icono_guardar.png'
            });
            $('.Cajas').removeAttr('disabled');
            //            $('.Combos').removeAttr('disabled');
            $('#Img_Salir').attr({
                style: 'display:inline;cursor:pointer',
                title: 'Cancelar',
                AlternateText: 'Cancelar',
                src: '../../paginas/imagenes/paginas/icono_cancelar.png'
            });
            break;
        default: break;
    }
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Cargar_Combo()
//DESCRIPCIÓN: trae información del servidor para asignarlos a un combo
//PARÁMETROS: tipo de informacion atraer
//CREO: Sergio Pacheco Alcocer
//FECHA_CREO: 05/04/2012
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Cargar_Combo_Depencia(Combo) {
    $.ajax({
        url: "Frm_Controlador_Mov_Tarj_Gas.aspx?Accion=Cargar_Combo_Dependencia",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            if (data != null) {
                var select = Combo;
                $('option', select).remove();
                var options = '<option value="-1"><-SELECCIONE-></option>';
                $.each(data, function(i, item) {
                    options += '<option value="' + item.dependencia_id + '">' + item.clave_nombre + '</option>';
                });
                Combo.append(options);
            }
        },
        error: function(result) {
            alert("ERROR " + result.status + " " + result.statusText);
        }
    });
}
function Btn_Buscar_Click() {
    Llenar_Tabla_Movimientos();
    return false;
}
//----------------------------------------------------------------------------------
//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Abrir_Popup().
//DESCRIPCIÓN: abre ventana emergente de filtros para obtener reporte de facturas
//PARÁMETROS:
//CREO: Sergio Pacheco Alcocer
//FECHA_CREO: 05/04/2012
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Abrir_Popup() {
    var Renglones_Tabla_Facturas;
    var Total_Factura = 0;
    Renglones_Tabla_Facturas = $('#Tbl_Movimientos').datagrid('getSelections');

    if (Renglones_Tabla_Facturas == null) {
        $.messager.alert('Facturas', 'Debes Seleccionar al menos un movimiento');
        return;
    }

    if (Renglones_Tabla_Facturas.length == 0) {
        $.messager.alert('Facturas', 'Debes Seleccionar al menos un movimiento');
        return;
    }
    else {
        for(var i=0;i<Renglones_Tabla_Facturas.length;i++){
            Total_Factura += parseFloat(Renglones_Tabla_Facturas[i].monto);
        }
        $('#Txt_Total_Factura').val(formatCurrency(Total_Factura));
    }
    $('#Ventana_Agregar_Facturas').window('open');
    $('#Txt_Folio_Factura').val("");
    $('#Txt_Monto').val(formatCurrency(Total_Factura));
    $('#Txt_Folio_Factura').focus();
    $('#Tbl_Facturas').datagrid('loadData', { total: 0, rows: [] });
    $('#Txt_Comision').val('0.00');
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Cerrar_Ventana().
//DESCRIPCIÓN: cierra ventanas emergentes
//PARÁMETROS:
//CREO: Sergio Pacheco Alcocer
//FECHA_CREO: 05/04/2012
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Cerrar_Ventana() {
    $('#Ventana_Agregar_Facturas').window('close');
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN: formatCurrency().
//DESCRIPCIÓN: Para dar formato de monedas a una cantidad
//PARÁMETROS:
//CREO: Jesus Toledo Rodriguez
//FECHA_CREO: 05/05/2012
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function formatCurrency(num) {
    num = num.toString().replace(/\$|\,/g, '');
    if (isNaN(num))
        num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num * 100 + 0.50000000001);
    cents = num % 100;
    num = Math.floor(num / 100).toString();
    if (cents < 10)
        cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
        num = num.substring(0, num.length - (4 * i + 3)) + ',' +
                    num.substring(num.length - (4 * i + 3));
    return (((sign) ? '' : '-') + num + '.' + cents);
}
//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Agregar_Facturas().
//DESCRIPCIÓN: Agrega una factura para su registro
//PARÁMETROS:
//CREO: Jesus Toledo Rodriguez
//FECHA_CREO: 09/Jul/2013
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
function Agregar_Facturas() {
    var Total_Agregado = 0;
    var Total_Factura = 0;
    var Monto_Restante = 0;
    var Monto = 0;
    var Iva = 0;
    var Comision = 0;
    var Folio = $('#Txt_Folio_Factura').val();
    var Fecha = $('#Txt_Fecha_Factura').val();
    Total_Factura = parseFloat($('#Txt_Total_Factura').val().replace(',',''));
    Monto = parseFloat($('#Txt_Monto').val().replace(',', ''));
    Iva = parseFloat($('#Txt_Iva').val().replace(',', ''));
    Comision = parseFloat($('#Txt_Comision').val().replace(',', ''));
    var Renglones_Tabla_Facturas;
    Renglones_Tabla_Facturas = $('#Tbl_Facturas').datagrid('getRows');

    if (isNaN(Monto)) {
        $('#Txt_Monto').focus();
        return;
    }
    if (Monto>Total_Factura) {
        $.messager.alert('Facturas', 'El monto es mayor al total de la Factura', 'error');
        $('#Ventana_Agregar_Facturas').focus();
        return;
    }
    if (Monto<=0) {
        $.messager.alert('Facturas', 'El monto debe ser mayor a 0', 'error');
        $('#Ventana_Agregar_Facturas').focus();
        return;
    }
    if (Folio.length <= 0) {
        $('#Txt_Folio_Factura').focus();
        return;
    }
    
    $('#Tbl_Facturas').datagrid('appendRow',
     {
         folio_factura: Folio,
         fecha_factura: Fecha,
         monto_factura: formatCurrency(Monto),
         iva_factura: formatCurrency(Iva),
         comision_factura: formatCurrency(Comision),
         total_factura: formatCurrency(Total_Factura)
     });
     $('#Txt_Folio_Factura').val("");
     $('#Txt_Folio_Factura').focus();
 }
//*******************************************************************************************************
//NOMBRE_FUNCIÓN: Eliminar_Facturas().
//DESCRIPCIÓN: Elimina una factura para su registro
//PARÁMETROS:
//CREO: Jesus Toledo Rodriguez
//FECHA_CREO: 09/Jul/2013
//MODIFICÓ:
//FECHA_MODIFICÓ:
//CAUSA_MODIFICACIÓN:
//*******************************************************************************************************
 function Eliminar_Factura() {
     var renglon;
     var index;
     var Renglones_Tabla_Facturas;
     var Monto_Restante = 0;
     var Total_Factura = parseFloat($('#Txt_Total_Factura').val().replace(',', ''));
     Renglones_Tabla_Facturas = $('#Tbl_Facturas').datagrid('getRows');

     renglon = $('#Tbl_Facturas').datagrid('getSelected');

     if (renglon == null) {
         $.messager.alert('Facturas', 'Tienes que seleccionar un elemento');
         return;
     }
     index = $('#Tbl_Facturas').datagrid('getRowIndex', renglon);
     $('#Tbl_Facturas').datagrid('deleteRow', index);

     for (var i = 0; i < Renglones_Tabla_Facturas.length; i++) {
         Monto_Restante += parseFloat(Renglones_Tabla_Facturas[i].total_factura.replace(',', ''));
     }
     Monto_Restante = Total_Factura - Monto_Restante;
     $('#Txt_Monto').val(formatCurrency(Monto_Restante));
 }
 //*******************************************************************************************************
 //NOMBRE_FUNCIÓN: Guardar_Facturas().
 //DESCRIPCIÓN: Guarda el registro de facturas para los mov selecccionados
 //PARÁMETROS:
 //CREO: Jesus Toledo Rodriguez
 //FECHA_CREO: 10/Jul/2013
 //MODIFICÓ:
 //FECHA_MODIFICÓ:
 //CAUSA_MODIFICACIÓN:
 //*******************************************************************************************************
 function Guardar_Facturas() {
     var Renglones_Facturas;
     var Obj_Facturas;
     var Obj_Movimientos = Obtener_Movimientos_Seleccionados();
     Renglones_Facturas = $('#Tbl_Facturas').datagrid('getRows');
         
     if (Renglones_Facturas != null) {
         if (Renglones_Facturas.length > 0) {
             Obj_Facturas = toJSON(Renglones_Facturas);             
             //Una peticioncilla AJAX para guardar las facturas
             $.ajax({
                 type: "POST",
                 url: "Frm_Controlador_Mov_Tarj_Gas.aspx/Guardar_Facturas",
                 data: "{'P_Tabla_Facturas':'" + Obj_Facturas +
             "','P_Movimientos':'" + Obj_Movimientos +             
             "'}",
                 contentType: "application/json; charset=utf-8",
                 dataType: "json",
                 success: function(response) {                     
                     var respuesta = "(" + response.d + ")";
                     if (respuesta == "(1)") {
                         $.messager.alert('Facturas', 'Las Factuas se registraron con Exito!!!', 'info', function() {
                         Habilitar_Controles("MODO_LISTADO"); 
                         Llenar_Tabla_Movimientos(); });
                     }
                     else {
                         $.messager.alert('Facturas', 'Ocurrio un error:' + respuesta.mensaje, 'error');
                     }
                 },
                 error: function(result) {
                     $('#ventana_mensaje').window('close');
                     $.messager.alert('Facturas', 'Ocurrio un Error!' + result.status + ' ' + result.statusText, 'error');
                 }

             });
             //Fin peticion             
             Cerrar_Ventana();
         }
         else { $.messager.alert('Facturas', 'Ingresa al menos una Factura', 'error'); $('#Txt_Folio_Factura').focus(); }
     }
     else { $.messager.alert('Facturas', 'Ingresa al menos una Factura', 'error'); $('#Txt_Folio_Factura').focus(); }
 }
  //*******************************************************************************************************
 //NOMBRE_FUNCIÓN: Obtener_Movimientos_Seleccionados().
 //DESCRIPCIÓN: Recorre grid concatenando columna de movimientos
 //PARÁMETROS:
 //CREO: Jesus Toledo Rodriguez
 //FECHA_CREO: 10/Jul/2013
 //MODIFICÓ:
 //FECHA_MODIFICÓ:
 //CAUSA_MODIFICACIÓN:
 //*******************************************************************************************************
 function Obtener_Movimientos_Seleccionados(){
    var Renglones_Movimientos = "";
    var Obj_Movimientos = "";
    Renglones_Movimientos = $('#Tbl_Movimientos').datagrid('getSelections');
     //Ciclo para obtener columna
     for(var Cont_Grid=0;Cont_Grid<Renglones_Movimientos.length;Cont_Grid++){
         Obj_Movimientos += Renglones_Movimientos[Cont_Grid].numero_movimiento + "-";
     }
     Obj_Movimientos = Obj_Movimientos.substring(0, Obj_Movimientos.length-1);
     return Obj_Movimientos;
 }