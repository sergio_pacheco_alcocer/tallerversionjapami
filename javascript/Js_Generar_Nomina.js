﻿var Pagina;
Pagina=$(document);
Pagina.ready(inicializarEventos_Generacion_Nomina);

function inicializarEventos_Generacion_Nomina()
{
  Estilo_Botones_Dinamicos();

  var Pagina=$("#Img_Irapuato");
  Pagina.hover(Cambiar_Imagen_In, Cambiar_Imagen_Out);
  Pagina.mousedown(presionaMouse);
  Pagina.mouseup(sueltaMouse);
  
     $('input[id$=Btn_Generar_Nomina]').hover(
         function(e){
             e.preventDefault();
             $(this).css("background-color", "#2F4E7D");
             $(this).css("color", "#FFFFFF");
         },
        function(e){
             e.preventDefault();
             $(this).css("background-color", "Silver");
             $(this).css("color", "#5656560");
        }
     );
}

function Cambiar_Imagen_Out()
{
  var Pagina=$("#Img_Irapuato");
  Pagina.attr("src","../imagenes/paginas/escudo.jpg");
  Pagina.attr("width", "180");
  Pagina.attr("height", "200");
  Pagina.css("background-color", "Silver");
  Pagina.css("border-style", "none");
}

function Cambiar_Imagen_In()
{
  var Pagina=$("#Img_Irapuato");
  Pagina.attr("src","../imagenes/paginas/fundadores.png");
  Pagina.attr("width", "500");
  Pagina.attr("height", "200");
  Pagina.css("background-color", "White");
  Pagina.css("border-style", "none");
}

function presionaMouse()
{
  var Pagina=$("#Img_Irapuato");
  Pagina.attr("src","../imagenes/paginas/sias_logo_irapuato.jpg");
  Pagina.attr("width", "141");
  Pagina.attr("height", "50");
  Pagina.css("background-color", "Silver");
  Pagina.css("border-style", "none");
  Pagina.css("vertical-align:", "middle");
}

function sueltaMouse()
{
  var Pagina=$("#Img_Irapuato");
  Pagina.attr("src","../imagenes/paginas/fundadores.png");
  Pagina.attr("width", "500");
  Pagina.attr("height", "200");
  Pagina.css("background-color", "White");
  Pagina.css("border-style", "none");
  Pagina.css("vertical-align:", "middle");
}
