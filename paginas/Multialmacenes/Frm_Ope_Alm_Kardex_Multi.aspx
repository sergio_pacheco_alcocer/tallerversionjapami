﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" CodeFile="Frm_Ope_Alm_Kardex_Multi.aspx.cs" Inherits="paginas_Multialmacenes_Frm_Ope_Alm_Kardex_Multi" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript" language="javascript">
        function calendarShown(sender, args){
            sender._popupBehavior._element.style.zIndex = 10000005;
        }
</script> 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization = "true" EnableScriptLocalization = "True"/>
    <asp:UpdatePanel ID="Upd_Panel" runat="server" UpdateMode="Always">
        <ContentTemplate>
        <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
            <ProgressTemplate>
                <%--<div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>--%>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <%--Div de Contenido --%>
        <div id="Div_Contenido" style="width:97%;height:100%;">
        <table width="97%"  border="0" cellspacing="0" class="estilo_fuente">
            <tr>
                <td class="label_titulo">Generar Kardex Productos</td>
            </tr>
             <%--Fila de div de Mensaje de Error --%>
            <tr>
                <td>
                    <div id="Div_Contenedor_Msj_Error" style="width:95%;font-size:9px;" runat="server" visible="false">
                    <table style="width:100%;">
                        <tr>
                            <td align="left" style="font-size:12px;color:Red;font-family:Tahoma;text-align:left;">
                            <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                            Width="24px" Height="24px"/>
                            </td>            
                            <td style="font-size:9px;width:90%;text-align:left;" valign="top">
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" ForeColor="Red" />
                            </td>
                        </tr>
                    </table>
                    </div>
                </td>
            </tr>
            <tr class="barra_busqueda">
                <td style="width:20%;">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:ImageButton ID="Btn_Imprimir_PDF" runat="server" ToolTip="Exportar PDF" CssClass="Img_Button"
                        ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" 
                            AlternateText="NUEVO" Visible="true" Width="24px" onclick="Btn_Imprimir_PDF_Click" 
                           />
                        <asp:ImageButton ID="Btn_Limpiar" runat="server" 
                            ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" 
                            ToolTip="Limpiar" />
                        <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button"
                        ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" />
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            
            
            <tr >
                <td>
                <div id="Div_Datos_Producto" style="background-color:#ffffff; width:100%; height:100%;">
                <table width="99%">
                    <tr style="background-color: #3366CC">
                        <td colspan="4" style="text-align:left; font-size:15px; color:#FFFFFF;" >
                            Buscar Producto
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Almacen
                        </td>
                        <td>
                            <asp:DropDownList ID="Cmb_Almacen" runat="server" Width="80%">
                            </asp:DropDownList>
                        </td>
                        <td>
                            Clave Producto</td>
                        <td>
                            <asp:TextBox ID="Txt_Clave_Busqueda" runat="server" Width="80%"></asp:TextBox>
                            <asp:ImageButton ID="Btn_Buscar" runat="server" AlternateText="Buscar" 
                                CausesValidation="false" ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                ToolTip="Buscar" onclick="Btn_Buscar_Click" />
                        </td>
                        
                        
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:CheckBox ID="Chk_Todos_Productos" runat="server" Text="Todos los Productos"/>
                        </td>
                    
                    </tr>
                    <tr>
                        <td width="20%">
                        Fecha Inicial</td>
                        
                        <td width="30%">
                            <asp:TextBox ID="Txt_Fecha_Inicial" runat="server" Width="80%" Enabled="false"></asp:TextBox>
                            <asp:ImageButton ID="Btn_Fecha_Inicial" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                            <cc1:CalendarExtender ID="Txt_Fecha_Ini_Calendar" runat="server" 
                            Format="dd/MMM/yyyy" OnClientShown="calendarShown" 
                            PopupButtonID="Btn_Fecha_Inicial" TargetControlID="Txt_Fecha_Inicial" />
                        </td>
                        <td width="20%">
                        Fecha Final</td>
                        <td width="30%">
                        <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="80%" Enabled="false"></asp:TextBox>
                            <asp:ImageButton ID="Btn_Fecha_Final" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                 <cc1:CalendarExtender ID="Txt_Fecha_Fin_Calendar" runat="server" 
                                    Format="dd/MMM/yyyy" OnClientShown="calendarShown" 
                                    PopupButtonID="Btn_Fecha_Final" TargetControlID="Txt_Fecha_Final" />
                        </td>
                    </tr>
                     <tr style="background-color: #3366CC">
                        <td colspan="4" style="text-align:left; font-size:15px; color:#FFFFFF;" >
                            Datos Generales
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Clave
                        </td>
                        <td>
                            <asp:TextBox ID="Txt_Clave" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                        </td>
                         <td>
                        
                        </td>
                        <td>
                        
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Descripción
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Descripcion" TextMode="MultiLine" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                        </td>
                       
                    </tr>
                    <tr>
                        <td>
                            Modelo
                        </td>
                        <td>
                            <asp:TextBox ID="Txt_Modelo" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            Marca
                        </td>
                        <td>
                            <asp:TextBox ID="Txt_Marca" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Unidad
                        </td>
                        <td>
                            <asp:TextBox ID="Txt_Unidad" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                            Estatus
                        </td>
                        <td>
                            <asp:TextBox ID="Txt_Estatus" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                                <td  colspan="4"> <hr /> </td>
                            </tr>
                            <tr>
                                <td style="width:18%; text-align:left;">
                                    <asp:Label ID="Lbl_Entradas" runat="server" Text="Entradas [T]"></asp:Label>
                                </td>
                                <td style="width:32%">
                                    <asp:TextBox ID="Txt_Entradas" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                                </td>
                                <td style="width:18%; text-align:left;">
                                    <asp:Label ID="Lbl_Salidas" runat="server" Text="Salidas [O. S.]"></asp:Label>
                                </td>
                                <td style="width:32%">
                                    <asp:TextBox ID="Txt_Salidas" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Existencia
                                </td>
                                <td>
                                    <asp:TextBox ID="Txt_Existencia" runat="server" Width="100%" ReadOnly="true"></asp:TextBox>
                                </td>
                            </tr>
                            <tr style="background-color: #3366CC">
                                <td id="Td2" style="text-align:left; font-size:15px; color:#FFFFFF;" colspan="4" runat="server" >
                                    Detalle de Movimientos
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <table width="100%">
                                        <tr>
                                            <td style="width:50%">
                                                <asp:Panel ID="Pnl_Listado_Entradas" runat="server" Width="100%"  GroupingText="Entradas">                              
                                                    <asp:GridView ID="Grid_Entradas" runat="server" CssClass="GridView_1"
                                                        AutoGenerateColumns="False" Width="96%" EmptyDataText="No hay Entradas"
                                                        GridLines= "Vertical">
                                                        <RowStyle CssClass="GridItem" />
                                                        <Columns>
                                                            <asp:BoundField DataField="FECHA_CREO" HeaderText="Fecha" SortExpression="FECHA" DataFormatString="{0:dd/MMM/yyyy}">
                                                                <ItemStyle Font-Size="Small" HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="CANTIDAD" HeaderText="Cantidad" SortExpression="CANTIDAD">
                                                                <ItemStyle Font-Size="Small" HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="NO_TRASPASO" HeaderText="No. Traspaso" SortExpression="NO_TRASPASO">
                                                                <ItemStyle Font-Size="Small" HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="NO_REQUISICION" HeaderText="No. Requisicion" SortExpression="NO_TRASPASO">
                                                                <ItemStyle Font-Size="Small" HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <PagerStyle CssClass="GridHeader" />
                                                        <SelectedRowStyle CssClass="GridSelected" />
                                                        <HeaderStyle CssClass="GridHeader" />                                
                                                        <AlternatingRowStyle CssClass="GridAltItem" />       
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </td>
                                            <td style="width:"50%">
                                                <asp:Panel ID="Pnl_Listado_Salidas" runat="server" Width="100%" GroupingText="Salidas">                          
                                                    <asp:GridView ID="Grid_Salidas" runat="server" CssClass="GridView_1"
                                                        AutoGenerateColumns="False" Width="96%" EmptyDataText="No hay Salidas"
                                                        GridLines= "Vertical">
                                                        <RowStyle CssClass="GridItem" />
                                                        <Columns>
                                                            <asp:BoundField DataField="FECHA_HORA" HeaderText="Fecha" SortExpression="FECHA" DataFormatString="{0:dd/MMM/yyyy}">
                                                                <ItemStyle Font-Size="Small" HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="CANTIDAD" HeaderText="Cantidad" SortExpression="CANTIDAD" >
                                                                <ItemStyle Font-Size="Small" HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="NO_SALIDA" HeaderText="Orden Salida" SortExpression="NO_SALIDA" >
                                                                <ItemStyle Font-Size="Small" HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <PagerStyle CssClass="GridHeader" />
                                                        <SelectedRowStyle CssClass="GridSelected" />
                                                        <HeaderStyle CssClass="GridHeader" />                                
                                                        <AlternatingRowStyle CssClass="GridAltItem" />       
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </td>
                                           </tr>
                        </table>
                    </td>
                 </tr>
                    
                </table>
                </div>
                </td>
            </tr>
            
            
        </table>
        </div>
         </ContentTemplate>
    </asp:UpdatePanel> 
</asp:Content>