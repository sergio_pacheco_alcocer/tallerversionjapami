﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" CodeFile="Frm_Ope_Alm_Salidas_Multi.aspx.cs" Inherits="paginas_Multialmacenes_Frm_Ope_Alm_Salidas_Multi" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization = "true" EnableScriptLocalization = "True"/>
    <asp:UpdatePanel ID="Upd_Panel" runat="server" UpdateMode="Always">
        <ContentTemplate>
        <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
            <ProgressTemplate>
                <%--<div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>--%>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <%--Div de Contenido --%>
        <div id="Div_Contenido" style="width:97%;height:100%;">
        <table width="97%"  border="0" cellspacing="0" class="estilo_fuente">
            <tr>
                <td class="label_titulo">Orden de Salidas de Multialmacen</td>
            </tr>
            <%--Fila de div de Mensaje de Error --%>
            <tr>
                <td >
                    <div id="Div_Contenedor_Msj_Error" style="width:95%;font-size:9px;" runat="server" visible="false">
                    <table style="width:100%;">
                        <tr>
                            <td align="left" style="font-size:12px;color:Red;font-family:Tahoma;text-align:left;">
                            <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                            Width="24px" Height="24px"/>
                            </td>            
                            <td style="font-size:9px;width:90%;text-align:left;" valign="top">
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" ForeColor="Red" />
                            </td>
                        </tr>
                    </table>
                    </div>
                </td>
           </tr>
            
             <tr class="barra_busqueda">
                <td style="width:20%;">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:ImageButton ID="Btn_Nuevo" runat="server" CssClass="Img_Button" 
                        ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" 
                        ToolTip="Nuevo" OnClick="Btn_Nuevo_Click" />
                        <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button"
                        ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" OnClick="Btn_Salir_Click" 
                           />
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
           <tr>
            <td>
                <div id="Busqueda" style="width:100%;height:100%;" runat="server">
                    
                    
                    
                </div>
            </td>
           </tr>
             
           <tr>
            <td>
                <div id="Div_Grid_Salidas" style="overflow:auto;height:300px;width:99%;vertical-align:top;border-style:outset;border-color:Silver;" runat="server">
                   
                    <asp:GridView ID="Grid_Salidas" runat="server" AllowSorting="True" 
                        AutoGenerateColumns="False" CssClass="GridView_1" DataKeyNames="No_Salida" 
                        Enabled="true" GridLines="None" HeaderStyle-CssClass="tblHead" 
                        onselectedindexchanged="Grid_Salidas_SelectedIndexChanged" Width="100%">
                        <RowStyle CssClass="GridItem" />
                        <Columns>
                            <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                <ItemStyle Width="5%" />
                            </asp:ButtonField>
                            <asp:BoundField DataField="No_Salida" HeaderText="No Salida" Visible="true">
                                <FooterStyle HorizontalAlign="Left" />
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Fecha_Hora" HeaderText="Fecha_Hora" Visible="True">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="15%" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Empleado_Recibio" HeaderText="Empleado Recibio" 
                                Visible="true">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="25%" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Empleado_Almacen" HeaderText="Empleado Almacen" 
                                Visible="True">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="25%" />
                            </asp:BoundField>
                            <asp:BoundField DataField="SUBTOTAL" DataFormatString="{0:C}" 
                                HeaderText="Subtotal" Visible="true">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                            </asp:BoundField>
                            <asp:BoundField DataField="IVA" DataFormatString="{0:C}" HeaderText="IVA" 
                                Visible="True">
                                <FooterStyle HorizontalAlign="Right" />
                                <HeaderStyle HorizontalAlign="Right" />
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" Width="10%" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TOTAL" DataFormatString="{0:C}" HeaderText="Total" 
                                Visible="false">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                            </asp:BoundField>
                        </Columns>
                        <SelectedRowStyle CssClass="GridSelected" />
                        <PagerStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="GridAltItem" />
                    </asp:GridView>
                   
                </div>
            
            </td>
           
           </tr>
           
          <tr>
            <td>
                <div id="Div_Contenido_Salidas" runat="server" style="width:100%;height:100%;" runat="server" visible="true">
                
                    <table width="100%">
                        <tr>
                            <td width="15%">
                                No. Salida
                            </td>
                            <td width="35%">
                                <asp:TextBox ID="Txt_No_Salida" runat="server" Width="100%"></asp:TextBox>
                            </td>
                            <td width="15%">
                            
                            </td>
                            <td width="35%">
                            
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                Almacen ID
                            </td>
                            <td>
                                <asp:TextBox ID="Txt_Almacen_ID" runat="server" Width="100%"></asp:TextBox>
                            </td>
                            <td>
                                Nombre Almacen
                            </td>
                            <td>
                                <asp:TextBox ID="Txt_Nombre_Almacen" runat="server" Width="100%"></asp:TextBox>
                            </td>
                    
                        </tr>
                        
                       
                         <tr>
                            <td>
                                Recibio
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="Cmb_Empleado_Recibio" runat="server" Width="100%">
                                </asp:DropDownList>
                                
                            </td>
                            <td>
                                <asp:ImageButton ID="Btn_Buscar_Empleado" runat="server" ToolTip="Buscar Empleado"
                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" OnClick="Btn_Buscar_Empleado_Click"/>
                            </td>
                    
                        </tr>
                        <tr class="barra_delgada">
                            <td colspan="4" >
                                
                            </td>
                        </tr>
                       
                        <tr>
                            <td align="center" colspan="4">
                                Productos</td>
                        </tr>
                        <tr>
                            <td>
                                Producto</td>
                            <td>
                                <asp:TextBox ID="Txt_Producto" runat="server" Width="97%"></asp:TextBox>
                            </td>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="Btn_Filtro_Productos" runat="server" 
                                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" ToolTip="Buscar Productos"
                                                onclick="Btn_Filtro_Productos_Click" />
                                        </td>
                                        <td align="right">
                                            Cantidad</td>
                                    </tr>
                                </table>
                            </td>
                            <td align="right">
                                &nbsp;<asp:TextBox ID="Txt_Cantidad" runat="server" Height="22px" Width="82%"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" 
                                    TargetControlID="Txt_Cantidad" ValidChars="1234567890">
                                </cc1:FilteredTextBoxExtender>
                                <asp:ImageButton ID="Btn_Agregar" runat="server" 
                                    ImageUrl="~/paginas/imagenes/paginas/accept.png" onclick="Btn_Agregar_Click" ToolTip="Agregar Productos"
                                    Width="24px" />
                            </td>
                        </tr>
                        
                        <tr>
                            <td colspan="4">
                             <div ID="Div_Grid_Productos" runat="server" style="overflow:auto;height:200px;width:99%;vertical-align:top;border-style:outset;border-color:Silver;">
                                <asp:GridView ID="Grid_Productos" runat="server"
                                    AllowSorting="True" AutoGenerateColumns="False" CssClass="GridView_1" 
                                    Enabled="false" GridLines="None" HeaderStyle-CssClass="tblHead" 
                                    Width="100%" onselectedindexchanged="Grid_Productos_SelectedIndexChanged">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                            ImageUrl="~/paginas/imagenes/paginas/delete.png" Text="Quitar">
                                            <ItemStyle Width="5%" />
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="Producto_ID" HeaderText="Producto_ID" 
                                            Visible="false">
                                            <FooterStyle HorizontalAlign="Right" />
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Clave" HeaderText="Clave" SortExpression="Clave" 
                                            Visible="true">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="5%" Font-Size="X-Small"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Producto_Nombre" HeaderText="Producto" 
                                            SortExpression="Producto_Nombre" Visible="True">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" 
                                            SortExpression="Descripcion" Visible="True">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Unidad" HeaderText="Unidad" 
                                            SortExpression="Unidad" Visible="True">
                                            <FooterStyle HorizontalAlign="Right" />
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="X-Small"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Existencia" HeaderText="Existencia" 
                                            SortExpression="Existencia" Visible="true">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Reorden" HeaderText="Punto de Reorden" 
                                            Visible="false">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Font-Size="X-Small"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Cantidad" HeaderText="Cantidad Solicitada" 
                                            SortExpression="Cantidad" Visible="true">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Precio_Unitario" HeaderText="Precio Unitario" 
                                            SortExpression="Precio_Unitario" Visible="True">
                                            <FooterStyle HorizontalAlign="Right" />
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="X-Small"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Importe" HeaderText="Importe Total" 
                                            SortExpression="Importe" Visible="True">
                                            <FooterStyle HorizontalAlign="Right" />
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="X-Small"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Monto_IVA" HeaderText="Monto_IVA" Visible="false">
                                            <FooterStyle HorizontalAlign="Right" />
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="X-Small"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Monto_IEPS" HeaderText="Monto_IEPS" Visible="false">
                                            <FooterStyle HorizontalAlign="Right" />
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="X-Small"/>
                                        </asp:BoundField>
                                        
                                    </Columns>
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <PagerStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right">
                            Subtotal
                                <asp:TextBox ID="Txt_Subtotal" runat="server" Width="20%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right">
                            IVA
                            <asp:TextBox ID="Txt_IVA" runat="server" Width="20%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right">
                            Total
                            <asp:TextBox ID="Txt_Total" runat="server" Width="20%"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                
                </div>
            
            </td>
          
          </tr>
        </table>                   
        </div>
    </td>
    </tr>
        </table>
        </div>
        </ContentTemplate>
         <Triggers>
             <asp:AsyncPostBackTrigger  ControlID="Btn_Buscar_Empleado" EventName="Click"/>
             <asp:AsyncPostBackTrigger  ControlID="Btn_Filtro_Productos" EventName="Click"/>
        </Triggers>    
</asp:UpdatePanel>

<asp:UpdatePanel ID="UPnl_Busqueda" runat ="server" UpdateMode="Conditional" >
        <ContentTemplate>               
                        <cc1:ModalPopupExtender ID="Modal_Buscar_Empleado" runat="server"
                            TargetControlID="Btn_Comodin_2"
                            PopupControlID="Pnl_Busqueda"              
                            Enabled="True"         
                            CancelControlID="Btn_Comodin_Close"
                            DynamicServicePath="" 
                            DropShadow="True"
                            BackgroundCssClass="progressBackgroundFilter"/>   
                        <asp:Button ID="Btn_Comodin_2" runat="server" Text="Button" style="display:none;" />   
                        <asp:Button ID="Btn_Comodin_3" runat="server" Text="Button" style="display:none;" />                     
        </ContentTemplate>
    </asp:UpdatePanel>
    
     <asp:UpdatePanel ID="UpdatePanel10" runat ="server" UpdateMode="Conditional" >
        <ContentTemplate>               
                        <cc1:ModalPopupExtender ID="Modal_Productos" runat="server"
                            TargetControlID="Button1"
                            PopupControlID="Pnl_Busqueda_Contenedor"
                            Enabled="True" 
                            CancelControlID="Btn_Cerrar"
                            DynamicServicePath="" 
                            DropShadow="True"
                            BackgroundCssClass="progressBackgroundFilter"/>   
                        <asp:Button ID="Button1" runat="server" Text="Button" style="display:none;" />   
                        <asp:Button ID="Button2" runat="server" Text="Button" style="display:none;" />                     
        </ContentTemplate>
    </asp:UpdatePanel>
    
     <%-- Panel del ModalPopUp  display:none--%>
    
    <asp:Panel ID="Pnl_Busqueda" runat="server" Width="100%" 
        style="border-style:outset;border-color:Silver;background-repeat:repeat-y;background-color:White;color:White;display:none;">
    <asp:Panel ID="Pnl_Cabecera_Bus_Avanzada" runat="server" Width="99%" 
        style="background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
        <table width="99%">
            <tr>
                <td style="color:Black;font-size:12;font-weight:bold;">
                <asp:Image ID="Image1" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                    Busqueda de Empleados
                </td>
                <td align="right">
                    <asp:ImageButton ID="Btn_Comodin_Close" CausesValidation="false" runat="server"  ToolTip="Cerrar Ventana"
                    ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />  
                </td>
            </tr>
        </table>
        </asp:Panel>              
        <center>
        <asp:UpdatePanel ID="pnlPanel" runat="server">
            <ContentTemplate>
            <table class="estilo_fuente" width="99%">
            <tr>
                <td width="30%"> No. Empleado</td>
                <td width="70%" ID="2">
                    <asp:TextBox ID="Txt_No_Empleado" runat="server" Width="50%" MaxLength="6"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" 
                    TargetControlID="Txt_No_Empleado" FilterType="Numbers"></cc1:FilteredTextBoxExtender>
                    <asp:ImageButton ID="Btn_Filtro_Empleados" runat="server" OnClick="Btn_Filtro_Empleados_Click"
                        ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png" Width="25px"/>
                </td>
            </tr>
            
            <tr>
                <td width="30%"> Nombre</td>
                <td width="70%">
                    <asp:TextBox ID="Txt_Nombre_Empleado" runat="server" Width="100%"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Apellido Paterno</td>
                <td><asp:TextBox ID="Txt_Apellido_Paterno" runat="server" Width="100%"></asp:TextBox></td>
                
            </tr>
            <tr>
                <td>Apellido Materno</td>
                <td><asp:TextBox ID="Txt_Apellido_Materno" runat="server" Width="100%"></asp:TextBox></td>
            </tr>
            
            <tr>
                <td></td>
                <td></td>
            </tr>
            
            <tr>
                <td colspan="2">
                <div id="Div_Grid_Empleados" runat="server" style="overflow:auto;height:100px;width:98%;vertical-align:top;border-style:outset;border-color:Silver;">
                    <asp:GridView ID="Grid_Empleados" runat="server" AutoGenerateColumns="False" 
                        CssClass="GridView_1" DataKeyNames="Empleado_ID" GridLines="none" 
                        Style="white-space:normal" Width="96%" OnSelectedIndexChanged="Grid_Empleados_SelectedIndexChanged">
                        <RowStyle CssClass="GridItem" />
                        <Columns>
                            <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                <ItemStyle Width="5%" />
                            </asp:ButtonField>
                            <asp:BoundField DataField="Empleado_ID">
                                <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                <ItemStyle Font-Size="0px" ForeColor="Transparent" HorizontalAlign="Left" 
                                    Width="0%" />
                            </asp:BoundField>
                            <asp:BoundField DataField="No_Empleado" HeaderText="No. Empleado">
                                <HeaderStyle HorizontalAlign="Left" Width="30%" />
                                <ItemStyle HorizontalAlign="Left" Width="30%" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Nombre" HeaderText="Nombre">
                                <HeaderStyle HorizontalAlign="Left" Width="65%" />
                                <ItemStyle HorizontalAlign="Left" Width="65%" />
                            </asp:BoundField>
                        </Columns>
                        <PagerStyle CssClass="GridHeader" />
                        <SelectedRowStyle CssClass="GridSelected" />
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="GridAltItem" />
                    </asp:GridView>
                </div>
                </td>
                
            </tr>
            
            
            </table>
            </ContentTemplate>
            
        <Triggers>
             <asp:AsyncPostBackTrigger  ControlID="Btn_Buscar_Empleado" EventName="Click"/>
             <asp:AsyncPostBackTrigger  ControlID="Btn_Filtro_Empleados" EventName="Click"/>
             <asp:AsyncPostBackTrigger  ControlID="Grid_Empleados" EventName="SelectedIndexChanged"/>
        </Triggers>    
        </asp:UpdatePanel> 
        </center>
    </asp:Panel>


<%-- Panel del ModalPopUp Pnl_Busqueda_Productos display:none;--%>
<asp:Panel ID="Pnl_Busqueda_Contenedor" runat="server" CssClass="drag" HorizontalAlign="Center" Width="100%" Height="100%" 
    style="border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;display:none;">
    <asp:Panel ID="Pnl_Busqueda_Cabecera_II" runat="server" 
        style="background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;" Width="99%">
        <table width="99%">
            <tr>
                <td style="color:Black;font-size:12;font-weight:bold;">
                   <asp:Image ID="Img_Informatcion_Autorizacion" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                     Buscar Productos
                </td>
                <td align="right" style="width:10%;">
                   <asp:ImageButton ID="Btn_Cerrar" CausesValidation="false" runat="server" ToolTip="Cerrar Ventana"
                        ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png"/>  
                </td>
            </tr>
        </table>            
    </asp:Panel>   
           
           <asp:UpdatePanel ID="UpdatePanel1" runat="server">
           <ContentTemplate>
            <table width="99%" class="estilo_fuente">
                <tr>
                    <td colspan="2">
                        <asp:Label ID="Lbl_Mensaje_Error_Productos" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                    <tr>
                        <td width="20%">
                            Nombre Producto
                        </td>
                        <td width="80%">
                            <asp:TextBox ID="Txt_Nombre_Producto" runat="server" Enabled="True" 
                                Width="80%"></asp:TextBox>
                            <asp:ImageButton ID="Btn_Realizar_Busqueda" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                onclick="Btn_Realizar_Busqueda_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                </tr>
                </table>
               </ContentTemplate> 
           </asp:UpdatePanel> 
           
           
           <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
              <ContentTemplate>
                <table width="99%" class="estilo_fuente">
                <tr>
                    <td>
                        <div ID="Div_1" runat="server" 
                        style="overflow:auto;height:250px;width:98%;vertical-align:top;border-style:outset;border-color:Silver;">
                        <asp:GridView ID="Grid_Productos_Busqueda" runat="server"
                            AutoGenerateColumns="False" CssClass="GridView_1" 
                            DataKeyNames="Producto_Servicio_ID" GridLines="None" 
                            Width="98%" HeaderStyle-CssClass="tblHead" 
                                onselectedindexchanged="Grid_Productos_Busqueda_SelectedIndexChanged">
                            <RowStyle CssClass="GridItem" />
                            <Columns>
                                <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                    ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                    <ItemStyle Width="5%" />
                                </asp:ButtonField>
                                <asp:BoundField DataField="Clave" HeaderText="Clave" 
                                    Visible="True" SortExpression="Clave">
                                    <FooterStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="Producto_Servicio" HeaderText="Producto" 
                                    Visible="True" SortExpression="Producto_Servicio">
                                    <FooterStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="25%" Font-Size="X-Small"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" SortExpression="Descripcion"
                                    Visible="True">
                                    <FooterStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="25%" Font-Size="X-Small"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="Unidad" HeaderText="Unidad" SortExpression="Unidad"
                                    Visible="True">
                                    <FooterStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="Existencia" HeaderText="Existencia" Visible="True" SortExpression="Existencia">
                                    <FooterStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="Precio_Unitario" HeaderText="Precio Unitario" SortExpression="Precio_Unitario"
                                    Visible="True">
                                    <FooterStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="X-Small"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="Producto_Servicio_ID" 
                                    HeaderText="Producto_Servicio_ID" Visible="False">
                                    <FooterStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Font-Size="X-Small"/>
                                </asp:BoundField>
                            </Columns>
                            <SelectedRowStyle CssClass="GridSelected" />
                            <PagerStyle CssClass="GridHeader" />
                            <AlternatingRowStyle CssClass="GridAltItem" />
                        </asp:GridView>
                        </div>
                    </td>
                </tr>
            </table>
           </ContentTemplate>
           <Triggers>
                <asp:AsyncPostBackTrigger  ControlID="Btn_Realizar_Busqueda" EventName="Click"/>
                <asp:AsyncPostBackTrigger  ControlID="Btn_Filtro_Productos" EventName="Click"/>
                <asp:AsyncPostBackTrigger  ControlID="Grid_Productos_Busqueda" EventName="SelectedIndexChanged"/>
           </Triggers>  
           </asp:UpdatePanel> 

        </asp:Panel>
</asp:Content>