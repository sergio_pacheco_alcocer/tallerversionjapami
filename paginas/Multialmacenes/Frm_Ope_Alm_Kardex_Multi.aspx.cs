﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Kardex_Multialmacen.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;

public partial class paginas_Multialmacenes_Frm_Ope_Alm_Kardex_Multi : System.Web.UI.Page
{
    ///*******************************************************************************
    ///PAGE_LOAD
    ///*******************************************************************************
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Cls_Ope_Alm_Kardex_Multi_Negocio Clase_Negocio = new Cls_Ope_Alm_Kardex_Multi_Negocio();
            DataTable Dt_Almacenes = Clase_Negocio.Consultar_Almacenes();
            //Llenamos el combo de almacen 
            Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Almacen, Dt_Almacenes);
            Txt_Fecha_Inicial.Text = DateTime.Now.ToString("dd/MMM/yyyy");
            Txt_Fecha_Final.Text = DateTime.Now.ToString("dd/MMM/yyyy");
           
        }
    }

    #endregion

    ///*******************************************************************************
    ///METODOS
    ///*******************************************************************************

    #region Metodos

    public void Limpiar_Componentes()
    {
        Txt_Clave_Busqueda.Text = "";
        Cmb_Almacen.SelectedIndex = 0;
        Txt_Clave.Text = "";
        Txt_Descripcion.Text = "";
        Txt_Modelo.Text = "";
        Txt_Marca.Text = "";
        Txt_Unidad.Text = "";
        Txt_Entradas.Text = "";
        Txt_Salidas.Text = "";
        Txt_Existencia.Text = "";
        Grid_Entradas.DataSource = new DataTable();
        Grid_Entradas.DataBind();
        Grid_Salidas.DataSource = new DataTable();
        Grid_Salidas.DataBind();
        Txt_Fecha_Inicial.Text = DateTime.Now.ToString("dd/MMM/yyyy");
        Txt_Fecha_Final.Text = DateTime.Now.ToString("dd/MMM/yyyy");
        Cmb_Almacen.SelectedIndex = 0;
        Chk_Todos_Productos.Checked = false;
        Session["Dt_Detalles_Productos"] = null;
        Session["Dt_Salidas"]= null;
        Session["Dt_Traspasos_Entradas"]= null;

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///PARAMETROS: 1.-TextBox Fecha_Inicial 
    ///            2.-TextBox Fecha_Final
    ///            3.-Label Mensaje_Error
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 10/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public Cls_Ope_Alm_Kardex_Multi_Negocio Verificar_Fecha(TextBox Fecha_Inicial, TextBox Fecha_Final, Cls_Ope_Alm_Kardex_Multi_Negocio Clase_Negocio)
    {

        //Variables que serviran para hacer la convecion a datetime las fechas y poder validarlas 
        DateTime Date1 = new DateTime();
        DateTime Date2 = new DateTime();


        if ((Fecha_Inicial.Text.Length == 11) && (Fecha_Final.Text.Length == 11))
        {
            //Convertimos el Texto de los TextBox fecha a dateTime
            Date1 = DateTime.Parse(Fecha_Inicial.Text);
            Date2 = DateTime.Parse(Fecha_Final.Text);
            //Validamos que las fechas sean iguales o la final sea mayor que la inicias, si no se manda un mensaje de error 
            if ((Date1 < Date2) | (Date1 == Date2))
            {
                //Se convierte la fecha seleccionada por el usuario a un formato valido por oracle. 
                Clase_Negocio.P_Fecha_Inicio = Formato_Fecha(Fecha_Inicial.Text);
                Clase_Negocio.P_Fecha_Final = Formato_Fecha(Fecha_Final.Text);

            }
            else
            {
                Div_Contenedor_Msj_Error.Visible = true;
                Lbl_Mensaje_Error.Text += "+ Fecha no valida <br />";
            }
        }
        else
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text += "+ Fecha no valida <br />";
        }

        return Clase_Negocio;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Formato_Fecha
    ///DESCRIPCIÓN: Metodo que cambia el mes dic a dec para que oracle lo acepte
    ///PARAMETROS:  1.- String Fecha, es la fecha a la cual se le cambiara el formato 
    ///                     en caso de que cumpla la condicion del if
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 2/Septiembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public String Formato_Fecha(String Fecha)
    {

        String Fecha_Valida = Fecha;
        //Se le aplica un split a la fecha 
        String[] aux = Fecha.Split('/');
        //Se modifica el es a solo mayusculas para que oracle acepte el formato. 
        switch (aux[1])
        {
            case "dic":
                aux[1] = "DEC";
                break;
        }
        //Concatenamos la fecha, y se cambia el orden a DD-MMM-YYYY para que sea una fecha valida para oracle
        Fecha_Valida = aux[0] + "-" + aux[1] + "-" + aux[2];
        return Fecha_Valida;
    }// fin de Formato_Fecha

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
    ///DESCRIPCIÓN: caraga el data set fisoco con el cual se genera el Reporte especificado
    ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
    ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
    ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 01/Mayo/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Generar_Reporte(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte, string Nombre_PDF)
    {

        ReportDocument Reporte = new ReportDocument();
        String File_Path = Server.MapPath("../Rpt/Multialmacenes/" + Nombre_Reporte);
        Reporte.Load(File_Path);
        Ds_Reporte = Data_Set_Consulta_DB;
        Reporte.SetDataSource(Ds_Reporte);
        ExportOptions Export_Options = new ExportOptions();
        DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
        Disk_File_Destination_Options.DiskFileName = Server.MapPath("../../Reporte/" + Nombre_PDF);
        Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
        Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
        Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
        Reporte.Export(Export_Options);
        String Ruta = "../../Reporte/" + Nombre_PDF;
        Mostrar_Reporte(Nombre_PDF, "PDF");
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
    }

      /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
    /// USUARIO CREO:        Juan Alberto Hernández Negrete.
    /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:    Salvador Hernández Ramírez
    /// FECHA MODIFICO:      16-Mayo-2011
    /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
    {
        String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

        try
        {
            if (Formato == "PDF")
            {
                Pagina = Pagina + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            else if (Formato == "Excel")
            {
                String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    #endregion

    ///*******************************************************************************
    ///GRID
    ///*******************************************************************************
    #region Grid

    #endregion



    ///*******************************************************************************
    ///EVENTOS
    ///*******************************************************************************
    #region Eventos

    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        //limpiamos los componentes
        Lbl_Mensaje_Error.Text = "";
        Div_Contenedor_Msj_Error.Visible = false;
        
        //Validamos que seleccionen el almacen  y la clave
        if (Cmb_Almacen.SelectedIndex == 0)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "Es Obligatorio seleccionar un almacen";

        }

        if (Chk_Todos_Productos.Checked == false)
        {
            if (Txt_Clave_Busqueda.Text.Trim() == String.Empty)
            {
                Div_Contenedor_Msj_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Es Obligatorio seleccionar un almacen";

            }
        }
        if (Div_Contenedor_Msj_Error.Visible == false)
        {
            //Creamos la variable de la clase 
            int Existencia = 0;
            int Entradas = 0;
            int Salidas = 0;
            Cls_Ope_Alm_Kardex_Multi_Negocio Clase_Negocio = new Cls_Ope_Alm_Kardex_Multi_Negocio();
            Clase_Negocio = Verificar_Fecha(Txt_Fecha_Inicial, Txt_Fecha_Final, Clase_Negocio);

            if (Txt_Clave_Busqueda.Text.Trim() != String.Empty)
                Clase_Negocio.P_Clave = Txt_Clave_Busqueda.Text;
            Clase_Negocio.P_Almacen_ID = Cmb_Almacen.SelectedValue;
            Clase_Negocio.P_Todos_Productos = Chk_Todos_Productos.Checked;
            //Obtenemos la consulta de los Productos
            DataTable Dt_Detalles_Productos = Clase_Negocio.Consultar_Productos();
            if (Dt_Detalles_Productos.Rows.Count > 0)
            {
                Session["Dt_Detalles_Productos"] = Dt_Detalles_Productos;
                //Obtenemos el datatable de las entradas k vienen siendo los traspasos
                DataTable Dt_Traspasos_Entradas = Clase_Negocio.Consultar_Traspasos();
                Session["Dt_Traspasos_Entradas"] = Dt_Traspasos_Entradas;
                //obtenemos el datatable de las salidas de o¿los productos en multialmacenes;
                DataTable Dt_Salidas = Clase_Negocio.Consultar_Salidas();
                Session["Dt_Salidas"] = Dt_Salidas;
                //llenamos los componentes 
                Txt_Clave.Text = Dt_Detalles_Productos.Rows[0][Cat_Com_Productos.Campo_Clave].ToString().Trim();
                Txt_Descripcion.Text = Dt_Detalles_Productos.Rows[0][Cat_Com_Productos.Campo_Descripcion].ToString().Trim();
                Txt_Modelo.Text = Dt_Detalles_Productos.Rows[0]["Modelo"].ToString().Trim();
                Txt_Marca.Text = Dt_Detalles_Productos.Rows[0]["Marca"].ToString().Trim();
                Txt_Unidad.Text = Dt_Detalles_Productos.Rows[0]["Unidad"].ToString().Trim();
                if (Dt_Salidas.Rows.Count > 0)
                {
                    Grid_Salidas.DataSource = Dt_Salidas;
                    Grid_Salidas.DataBind();
                    for (int i = 0; i < Dt_Salidas.Rows.Count; i++)
                    {
                        Salidas = Salidas + int.Parse(Dt_Salidas.Rows[i][Ope_Alm_Salidas_Det_Multi.Campo_Cantidad].ToString().Trim());

                    }
                }
                else
                {
                    Grid_Salidas.EmptyDataText = "No se encontraron datos";
                    Grid_Salidas.DataBind();
                }
                if (Dt_Traspasos_Entradas.Rows.Count > 0)
                {
                    Grid_Entradas.DataSource = Dt_Traspasos_Entradas;
                    Grid_Entradas.DataBind();
                    for (int i = 0; i < Dt_Traspasos_Entradas.Rows.Count; i++)
                    {
                        Entradas = Entradas + int.Parse(Dt_Traspasos_Entradas.Rows[i][Ope_Alm_Traspasos.Campo_Cantidad].ToString().Trim());
                    }
                }
                else
                {
                    Grid_Entradas.EmptyDataText = "No se encontraron datos";
                    Grid_Entradas.DataBind();
                    Entradas = 0;
                }
                //Llenamos los datos de salidas totales y entradas total y la existencia final
                Existencia = Entradas - Salidas;

                Txt_Entradas.Text = Entradas.ToString();
                Txt_Salidas.Text = Salidas.ToString();
                Txt_Existencia.Text = Existencia.ToString();
            }
            else
            {
                Div_Contenedor_Msj_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontraron datos";
                Limpiar_Componentes();
            }
        }
   
    }

    protected void Btn_Imprimir_PDF_Click(object sender, ImageClickEventArgs e)
    {
        //Agregamos los datatable al dataset del reporte
        if (Session["Dt_Detalles_Productos"] == null)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "Es necesario realizar una busqueda de producto";
        }
        else
        {
            DataSet Ds_Imprimir_Reporte = new DataSet();
            //Igualamos las variables de session a datatablee.
            DataTable Dt_Detalles_Productos = (DataTable)Session["Dt_Detalles_Productos"];
            DataTable Dt_Salidas = (DataTable)Session["Dt_Salidas"];
            DataTable Dt_Traspasos_Entradas = (DataTable)Session["Dt_Traspasos_Entradas"];
            //Dt_Detalles_Productos.Columns.Add("ALMACEN_ID", typeof(System.String));
            //Dt_Detalles_Productos.Columns.Add("NOMBRE_ALMACEN", typeof(System.String));
            //Dt_Detalles_Productos.Rows[0]["ALMACEN_ID"]= Cmb_Almacen.SelectedValue;
            //Dt_Detalles_Productos.Rows[0]["NOMBRE_ALMACEN"] = Cmb_Almacen.SelectedItem.Text;
            Ds_Imprimir_Reporte.Tables.Add(Dt_Detalles_Productos.Copy());
            Ds_Imprimir_Reporte.Tables[0].TableName = "Dt_Detalles_Productos";
            Ds_Imprimir_Reporte.AcceptChanges();
            Ds_Imprimir_Reporte.Tables.Add(Dt_Salidas.Copy());
            Ds_Imprimir_Reporte.Tables[1].TableName = "Dt_Salidas";
            Ds_Imprimir_Reporte.AcceptChanges();
            Ds_Imprimir_Reporte.Tables.Add(Dt_Traspasos_Entradas.Copy());
            Ds_Imprimir_Reporte.Tables[2].TableName = "Dt_Traspasos_Entradas";
            Ds_Imprimir_Reporte.AcceptChanges();

            Ds_Ope_Alm_Kardex_Multi Obj_Ope_Alm_Kardex_Multi = new Ds_Ope_Alm_Kardex_Multi();
            Generar_Reporte(Ds_Imprimir_Reporte,Obj_Ope_Alm_Kardex_Multi,"Rpt_Ope_Alm_Kardex_Multi.rpt", "Rpt_Ope_Alm_Kardex_Multi.pdf");

        }

    }

    #endregion





  
}
