﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" CodeFile="Frm_Cat_Alm_Multialmacenes.aspx.cs" Inherits="paginas_Multialmacenes_Frm_Cat_Alm_Multialmacenes" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization = "true" EnableScriptLocalization = "True"/>
    <asp:UpdatePanel ID="Upd_Panel" runat="server" UpdateMode="Always">
        <ContentTemplate>
        <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
            <ProgressTemplate>
                <%--<div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>--%>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <%--Div de Contenido --%>
        <div id="Div_Contenido" style="width:97%;height:100%;">
        <table width="97%"  border="0" cellspacing="0" class="estilo_fuente">
            <tr>
                <td class="label_titulo">Catalogo Multialmacenes</td>
            </tr>
            <%--Fila de div de Mensaje de Error --%>
            <tr>
                <td>
                    <div id="Div_Contenedor_Msj_Error" style="width:95%;font-size:9px;" runat="server" visible="false">
                    <table style="width:100%;">
                        <tr>
                            <td align="left" style="font-size:12px;color:Red;font-family:Tahoma;text-align:left;">
                            <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                            Width="24px" Height="24px"/>
                            </td>            
                            <td style="font-size:9px;width:90%;text-align:left;" valign="top">
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" ForeColor="Red" />
                            </td>
                        </tr> 
                    </table>                   
                    </div>
                </td>
            </tr>
             <%--Fila de Busqueda y Botones Generales --%>
            <tr class="barra_busqueda">
                <td style="width:20%;">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:ImageButton ID="Btn_Nuevo" runat="server" CssClass="Img_Button" 
                        ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" 
                        ToolTip="Nuevo" onclick="Btn_Nuevo_Click"/>
                        <asp:ImageButton ID="Btn_Modificar" runat="server" CssClass="Img_Button" 
                        ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" 
                        ToolTip="Modificar" onclick="Btn_Modificar_Click"/>
                        <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button"
                        ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                            onclick="Btn_Salir_Click"/>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
              </tr>
              <tr>
                <td>
                    <div id="Busqueda" style="width:100%;height:100%;" runat="server">
                    
                    </div>
                </td>
              </tr>
              <tr>
                <td>
                    <div id="Div_Multialmacenes" style="width:100%;height:100%;" runat="server">
                        <table width="99%"  border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                                <td width="20%">
                                    Almacen ID
                                </td>
                                <td width="30%">
                                    <asp:TextBox ID="Txt_Almacen_ID" runat="server" Width="50%" Enabled="false"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="Txt_ID_FilteredTextBoxExtender" runat="server" 
                                    TargetControlID="Txt_Almacen_ID" FilterType="Numbers"></cc1:FilteredTextBoxExtender>
                                </td>
                                <td width="20%">
                                
                                </td>
                                <td width="30%">
                                
                                </td>
                                
                            </tr>
                            <tr>
                                <td>
                                    Nombre
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="Txt_Nombre" runat="server" TextMode="MultiLine" MaxLength="100" Width="100%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Descripci&oacute;n
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="Txt_Descripcion" runat="server" TextMode="MultiLine" MaxLength="500" Width="100%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    *Responsable de Almacen
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="Txt_Responsable_Almacen_ID" runat="server" Width="80%" Enabled="false"></asp:TextBox>
                                    <asp:ImageButton ID="Btn_Buscar_Empleado" runat="server" ToolTip="Buscar Empleado"
                                        ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                        onclick="Btn_Buscar_Empleado_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                <asp:GridView ID="Grid_Multialmacenes" runat="server"
                                        AutoGenerateColumns="False" CssClass="GridView_1"
                                        EmptyDataText="&quot;No se encontraron registros&quot;" GridLines="none"
                                        Style="white-space:normal" Width="96%" 
                                        DataKeyNames="Almacen_ID" 
                                        onselectedindexchanged="Grid_Multialmacenes_SelectedIndexChanged">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                <ItemStyle Width="5%" />
                                            </asp:ButtonField>
                                                <asp:BoundField DataField="Almacen_ID">
                                                    <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                    <ItemStyle HorizontalAlign="Left" Width="10%"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Nombre" HeaderText="Nombre" >
                                                    <HeaderStyle HorizontalAlign="Left" Width="25%" />
                                                    <ItemStyle HorizontalAlign="Left" Width="25%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Descripcion" HeaderText="Descripcion">
                                                    <HeaderStyle HorizontalAlign="Left" Width="35%" />
                                                    <ItemStyle HorizontalAlign="Left" Width="35%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Responsable" HeaderText="Responsable">
                                                    <HeaderStyle HorizontalAlign="Left" Width="30%" />
                                                    <ItemStyle HorizontalAlign="Left" Width="30%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Responsable_ID" HeaderText="Responsable_ID" Visible="false">
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left"/>
                                                </asp:BoundField>
                                                
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                
                                </td>
                            
                            </tr>
                            
                        </table>
                    </div>
                  </td>
              </tr>
              
              
              
              
        </table>
        </div>

        </ContentTemplate>
         <Triggers>
             <asp:AsyncPostBackTrigger  ControlID="Btn_Buscar_Empleado" EventName="Click"/>
       
        </Triggers>    
    </asp:UpdatePanel>
    
    <asp:UpdatePanel ID="UPnl_Busqueda" runat ="server" UpdateMode="Conditional" >
        <ContentTemplate>               
                        <cc1:ModalPopupExtender ID="Modal_Buscar_Empleado" runat="server"
                            TargetControlID="Btn_Comodin_2"
                            PopupControlID="Pnl_Busqueda"              
                            Enabled="True"         
                            CancelControlID="Btn_Comodin_Close"
                            PopupDragHandleControlID="Pnl_Busqueda_Cabecera" 
                            DynamicServicePath="" 
                            DropShadow="True"
                            BackgroundCssClass="progressBackgroundFilter"/>   
                        <asp:Button ID="Btn_Comodin_2" runat="server" Text="Button" style="display:none;" />   
                        <asp:Button ID="Btn_Comodin_Close" runat="server" Text="Button" style="display:none;" />                     
        </ContentTemplate>
    </asp:UpdatePanel>
    
    
     <%-- Panel del ModalPopUp  display:none--%>
    
    <asp:Panel ID="Pnl_Busqueda" runat="server" Width="100%" 
        style="border-style:outset;border-color:Silver;background-repeat:repeat-y;background-color:White;color:White;display:none">
    <asp:Panel ID="Pnl_Cabecera_Bus_Avanzada" runat="server" Width="99%" 
        style="background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
        <table width="99%">
            <tr>
                <td style="color:Black;font-size:12;font-weight:bold;">
                <asp:Image ID="Image1" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                    Busqueda de Empleados
                </td>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" CausesValidation="false" runat="server"  ToolTip="Cerrar Ventana"
                    ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />  
                </td>
            </tr>
        </table>
    </asp:Panel>              
        <center>
        <asp:UpdatePanel ID="pnlPanel" runat="server">
            <ContentTemplate>
            <table class="estilo_fuente" width="99%">
            <tr>
                <td width="30%"> No. Empleado</td>
                <td width="70%" ID="2">
                    <asp:TextBox ID="Txt_No_Empleado" runat="server" Width="50%" MaxLength="6"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" 
                    TargetControlID="Txt_No_Empleado" FilterType="Numbers"></cc1:FilteredTextBoxExtender>
                    <asp:ImageButton ID="Btn_Filtro_Empleados" runat="server" 
                        ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png" Width="25px" onclick="Btn_Filtro_Empleados_Click" 
                        />
                </td>
            </tr>
            
            <tr>
                <td width="30%"> Nombre</td>
                <td width="70%">
                    <asp:TextBox ID="Txt_Nombre_Empleado" runat="server" Width="100%"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Apellido Paterno</td>
                <td><asp:TextBox ID="Txt_Apellido_Paterno" runat="server" Width="100%"></asp:TextBox></td>
                
            </tr>
            <tr>
                <td>Apellido Materno</td>
                <td><asp:TextBox ID="Txt_Apellido_Materno" runat="server" Width="100%"></asp:TextBox></td>
            </tr>
            
            <tr>
                <td></td>
                <td></td>
            </tr>
            
            <tr>
                <td colspan="2">
                <div id="Div_Grid_Empleados" runat="server" style="overflow:auto;height:100px;width:98%;vertical-align:top;border-style:outset;border-color:Silver;">
                    <asp:GridView ID="Grid_Empleados" runat="server" AutoGenerateColumns="False" 
                        CssClass="GridView_1" DataKeyNames="Empleado_ID" GridLines="none" 
                        onselectedindexchanged="Grid_Empleados_SelectedIndexChanged" 
                        Style="white-space:normal" Width="96%">
                        <RowStyle CssClass="GridItem" />
                        <Columns>
                            <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                <ItemStyle Width="5%" />
                            </asp:ButtonField>
                            <asp:BoundField DataField="Empleado_ID">
                                <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                <ItemStyle Font-Size="0px" ForeColor="Transparent" HorizontalAlign="Left" 
                                    Width="0%" />
                            </asp:BoundField>
                            <asp:BoundField DataField="No_Empleado" HeaderText="No. Empleado">
                                <HeaderStyle HorizontalAlign="Left" Width="30%" />
                                <ItemStyle HorizontalAlign="Left" Width="30%" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Nombre" HeaderText="Nombre">
                                <HeaderStyle HorizontalAlign="Left" Width="65%" />
                                <ItemStyle HorizontalAlign="Left" Width="65%" />
                            </asp:BoundField>
                        </Columns>
                        <PagerStyle CssClass="GridHeader" />
                        <SelectedRowStyle CssClass="GridSelected" />
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="GridAltItem" />
                    </asp:GridView>
                </div>
                </td>
                
            </tr>
            
            
            </table>
            </ContentTemplate>
            
        <Triggers>
             <asp:AsyncPostBackTrigger  ControlID="Btn_Buscar_Empleado" EventName="Click"/>
             <asp:AsyncPostBackTrigger  ControlID="Btn_Filtro_Empleados" EventName="Click"/>
             <asp:AsyncPostBackTrigger  ControlID="Grid_Empleados" EventName="SelectedIndexChanged"/>
        </Triggers>    
        </asp:UpdatePanel> 
        </center>
    </asp:Panel>
    
    
</asp:Content>
