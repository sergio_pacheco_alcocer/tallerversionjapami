﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using JAPAMI.Multialmacenes.Negocio;

public partial class paginas_Multialmacenes_Frm_Cat_Alm_Multialmacenes : System.Web.UI.Page
{
    ///*******************************************************************************
    ///PAGE_LOAD
    ///*******************************************************************************
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            Cls_Cat_Alm_Multialmacenes_Negocio Clase_Negocio = new Cls_Cat_Alm_Multialmacenes_Negocio();
            Llenar_Grid_Multialmacenes(Clase_Negocio);
            Configurar_Formulario("Inicio");
        }

    }

    #endregion

    ///*******************************************************************************
    ///METODOS
    ///*******************************************************************************

    #region Metodos


  
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Configurar_Formulario
    ///DESCRIPCIÓN: Metodo que ayuda a configuarar el formulario 
    ///PARAMETROS:P_Estatus, Puede Ser Inicio, Nuevo, Modificar 
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 6/DIC/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    ///
    public void Configurar_Formulario(String Estatus)
    {
        switch (Estatus)
        {
            case "Inicio":

                //Boton Nuevo
                Btn_Nuevo.Visible = true;
                Btn_Nuevo.ToolTip = "Nuevo";
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                //Boton Modificar
                Btn_Modificar.Visible = true;
                Btn_Modificar.ToolTip = "Modificar";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                //Boton Salir
                Btn_Salir.Visible = true;
                Btn_Salir.ToolTip = "Inicio";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                HabilitarComponentes(false);
                Grid_Multialmacenes.Enabled = true;
                break;
            case "Nuevo":

                //Boton Nuevo
                Btn_Nuevo.Visible = true;
                Btn_Nuevo.ToolTip = "Dar de Alta";
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                //Boton Modificar
                Btn_Modificar.Visible = false;
                Btn_Modificar.ToolTip = "Modificar";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                //Boton Salir
                Btn_Salir.Visible = true;
                Btn_Salir.ToolTip = "Cancelar";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                HabilitarComponentes(true);
                Grid_Multialmacenes.Enabled = false;
                break;
            case "Modificar":

                //Boton Nuevo
                Btn_Nuevo.Visible = false;
                Btn_Nuevo.ToolTip = "Nuevo";
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                //Boton Modificar
                Btn_Modificar.Visible = true;
                Btn_Modificar.ToolTip = "Actualizar";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                //Boton Salir
                Btn_Salir.Visible = true;
                Btn_Salir.ToolTip = "Cancelar";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                HabilitarComponentes(true);
                Grid_Multialmacenes.Enabled = false;
                break;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Limpiar_Formulario
    ///DESCRIPCIÓN: Metodo que ayuda a limpiar componentes el formulario 
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 6/DIC/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Limpiar_Formulario()
    {
        Txt_Almacen_ID.Text = "";
        Txt_Almacen_ID.Enabled = false;
        Txt_Nombre.Text = "";
        Txt_Descripcion.Text = "";
        Txt_Responsable_Almacen_ID.Text = "";
        Txt_Responsable_Almacen_ID.Enabled = false;
        Session["RESPONSABLE_ID"] = null;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Limpiar_Modal
    ///DESCRIPCIÓN: Metodo que ayuda alimpiar los componentes dentro del Modal POpup
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 6/DIC/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************

    public void Limpiar_Modal()
    {
        Txt_No_Empleado.Text = "";
        Txt_Nombre_Empleado.Text = "";
        Txt_Apellido_Paterno.Text = "";
        Txt_Apellido_Materno.Text = "";
        Grid_Empleados.DataSource = new DataTable();
        Grid_Empleados.DataBind();

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: HabilitarComponentes
    ///DESCRIPCIÓN: Metodo que ayuda hailitar los componentes del fromulario
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 6/DIC/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void HabilitarComponentes(bool Estatus)
    {
        Txt_Almacen_ID.Enabled = false;
        Txt_Nombre.Enabled = Estatus;
        Txt_Descripcion.Enabled = Estatus;
        Txt_Responsable_Almacen_ID.Enabled = false;
        Btn_Buscar_Empleado.Enabled = Estatus;

    }


    public void Verificar_Datos()
    {
        if (Txt_Nombre.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "+ Es necesario indicar el nombre<br/>";

        }

        if (Txt_Descripcion.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "+ Es necesario indicar la descripcion<br/>";
        }

        if (Txt_Responsable_Almacen_ID.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "+ Es necesario indicar el responsable de almacen<br/>";
        }

    }
    public Cls_Cat_Alm_Multialmacenes_Negocio Cargar_Datos(Cls_Cat_Alm_Multialmacenes_Negocio Clase_Negocios)
    {
        if (Txt_Almacen_ID.Text.Trim() != String.Empty)
        {
            Clase_Negocios.P_Almacen_ID = Txt_Almacen_ID.Text.Trim();
        }
        Clase_Negocios.P_Nombre = Txt_Nombre.Text;
        Clase_Negocios.P_Descripcion = Txt_Descripcion.Text;
        Clase_Negocios.P_Responsable_ID = Session["RESPONSABLE_ID"].ToString().Trim();

        return Clase_Negocios;
    }

    #endregion

    ///*******************************************************************************
    ///GRID
    ///*******************************************************************************
    
    #region Grid

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Grid_Multialmacenes_SelectedIndexChanged
    ///DESCRIPCIÓN: Metodo de seleccion del grid view
    ///PARAMETROS:P_Estatus, Puede Ser Inicio, Nuevo, Modificar 
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 6/DIC/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************

    public void Llenar_Grid_Multialmacenes(Cls_Cat_Alm_Multialmacenes_Negocio Clase_Negocio)
    {
        DataTable Dt_Multialmacenes = Clase_Negocio.Consulta_Multialmacenes();
        if (Dt_Multialmacenes.Rows.Count > 0)
        {
            Session["Dt_Multialmacenes"] = Dt_Multialmacenes;
            Grid_Multialmacenes.DataSource = Dt_Multialmacenes;
            Grid_Multialmacenes.DataBind();
        }
        else
        {
            Grid_Multialmacenes.EmptyDataText = "No se han encontrado registros.";
            Grid_Multialmacenes.DataSource = new DataTable();
            Grid_Multialmacenes.DataBind();
        }

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Grid_Multialmacenes_SelectedIndexChanged
    ///DESCRIPCIÓN: Metodo de seleccion del grid view
    ///PARAMETROS:P_Estatus, Puede Ser Inicio, Nuevo, Modificar 
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 6/DIC/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Multialmacenes_SelectedIndexChanged(object sender, EventArgs e)
    {

        Lbl_Mensaje_Error.Text = "";
        Div_Contenedor_Msj_Error.Visible = false;
        int Num_Linea = Grid_Multialmacenes.SelectedIndex;
        //Consultamos los datos del producto seleccionado
        if(Session["Dt_Multialmacenes"] != null)
        {
            DataTable Dt_Multialmacenes = (DataTable)Session["Dt_Multialmacenes"];
            Txt_Almacen_ID.Text = Dt_Multialmacenes.Rows[Num_Linea]["Almacen_ID"].ToString().Trim();
            Txt_Nombre.Text = Dt_Multialmacenes.Rows[Num_Linea]["Nombre"].ToString().Trim();
            Txt_Descripcion.Text = Dt_Multialmacenes.Rows[Num_Linea]["Descripcion"].ToString().Trim();
            Txt_Responsable_Almacen_ID.Text = Dt_Multialmacenes.Rows[Num_Linea]["Responsable"].ToString().Trim();
            Session["RESPONSABLE_ID"] = Dt_Multialmacenes.Rows[Num_Linea]["Responsable_ID"].ToString().Trim();
        }

        
    }


    protected void Grid_Empleados_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridViewRow Row = Grid_Empleados.SelectedRow;
        int Num_Linea = Grid_Empleados.SelectedIndex;
        Session["RESPONSABLE_ID"] = Grid_Empleados.SelectedDataKey["Empleado_ID"].ToString();
        Txt_Responsable_Almacen_ID.Text = Row.Cells[3].Text;
        Modal_Buscar_Empleado.Hide();
    }

    #endregion
    
    ///*******************************************************************************
    ///EVENTOS
    ///*******************************************************************************
    #region Eventos

    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";

        switch(Btn_Nuevo.ToolTip)
        {
            case "Nuevo":
                Configurar_Formulario("Nuevo");
                HabilitarComponentes(true);


                break;
            case "Dar de Alta":
                Verificar_Datos();
                if (Div_Contenedor_Msj_Error.Visible == false)
                {
                    Cls_Cat_Alm_Multialmacenes_Negocio Clase_Negocio = new Cls_Cat_Alm_Multialmacenes_Negocio();
                    Clase_Negocio = Cargar_Datos(Clase_Negocio);
                    String Mensaje = Clase_Negocio.Alta_Multialmacen();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Requisiciones", "alert('" + Mensaje + "');", true);
                    Configurar_Formulario("Inicio");
                    Limpiar_Formulario();
                    Clase_Negocio = new Cls_Cat_Alm_Multialmacenes_Negocio();
                    Llenar_Grid_Multialmacenes(Clase_Negocio);
                }


                break;

        }
    }

    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";

        switch(Btn_Modificar.ToolTip)
        {
            case "Modificar":
                Configurar_Formulario("Modificar");

                break;
            case "Actualizar":
                Cls_Cat_Alm_Multialmacenes_Negocio Clase_Negocio = new Cls_Cat_Alm_Multialmacenes_Negocio();

                Clase_Negocio = Cargar_Datos(Clase_Negocio);
                String Msj = Clase_Negocio.Modificar_Multialmacenes();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Requisiciones", "alert('" + Msj + "');", true);
                Configurar_Formulario("Inicio");
                Limpiar_Formulario();
                Clase_Negocio = new Cls_Cat_Alm_Multialmacenes_Negocio();
                Llenar_Grid_Multialmacenes(Clase_Negocio);

                break;
        }

        
    }

    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        switch(Btn_Salir.ToolTip)
        {
            case "Inicio":
                Limpiar_Formulario();
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                

                break;
            case "Cancelar":
                Limpiar_Formulario();
                Configurar_Formulario("Inicio");

                break;

        }



    }

    protected void Btn_Buscar_Empleado_Click(object sender, ImageClickEventArgs e)
    {

        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Modal_Buscar_Empleado.Show();
        Limpiar_Modal();

    }


    protected void Btn_Filtro_Empleados_Click(object sender, ImageClickEventArgs e)
    {
        Modal_Buscar_Empleado.Show();
        Cls_Cat_Alm_Multialmacenes_Negocio Clase_Negocio = new Cls_Cat_Alm_Multialmacenes_Negocio();
        if (Txt_No_Empleado.Text.Trim() != String.Empty || Txt_Nombre_Empleado.Text.Trim() != String.Empty || Txt_Apellido_Paterno.Text.Trim() != String.Empty || Txt_Apellido_Materno.Text.Trim() != String.Empty)
        {
            if (Txt_No_Empleado.Text.Trim() != String.Empty)
            {
                Clase_Negocio.P_No_Empleado = String.Format("{0:000000}", Convert.ToInt32(Txt_No_Empleado.Text.Trim()));
            }

            if (Txt_Nombre_Empleado.Text.Trim() != String.Empty)
            {
                Clase_Negocio.P_Nombre_Empleado = Txt_Nombre_Empleado.Text.Trim();
            }

            if (Txt_Apellido_Paterno.Text.Trim() != String.Empty)
            {
                Clase_Negocio.P_Apellido_Paterno = Txt_Apellido_Paterno.Text.Trim();
            }

            if (Txt_Apellido_Materno.Text.Trim() != String.Empty)
            {
                Clase_Negocio.P_Apellido_Materno = Txt_Apellido_Materno.Text.Trim();
            }

            //Realizamos la consulta de los empleados
            DataTable Dt_Empleados = Clase_Negocio.Consultar_Empleado();

            if (Dt_Empleados.Rows.Count > 0)
            {
                Grid_Empleados.DataSource = Dt_Empleados;
                Grid_Empleados.DataBind();
                Grid_Empleados.Visible = true;

            }
            else
            {
                Grid_Empleados.EmptyDataText = "No se han encontrado empleados con esos datos.";
                Grid_Empleados.DataSource = new DataTable();
                Grid_Empleados.DataBind();
            }
        }
    }
    #endregion

}
