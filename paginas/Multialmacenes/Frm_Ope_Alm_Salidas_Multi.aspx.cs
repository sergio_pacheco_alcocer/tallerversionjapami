﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Salidas_Multialmacenes.Negocios;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;

public partial class paginas_Multialmacenes_Frm_Ope_Alm_Salidas_Multi : System.Web.UI.Page
{
    #region Variables
    //variable que permite guardar el monto presupuestal de la partida correspondiente
    public static double Monto_Presupuestal;
    //variable que permite guardar el presupuesto disponible de la partida correspondiente
    public static double Monto_Disponible;
    //variable que permite guardar el presupuesto comprometido de la partida correspondienteb
    public static double Monto_Comprometido;
    //Variable que almacena el valor del importe acumulado de los productos agregados al Grid_Productos 
    public static double Importe_Producto_Acumulado;

    #endregion

    ///*******************************************************************************
    ///PAGE_LOAD
    ///*******************************************************************************
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Verificamos k sea valido la persona k se loguea.
            Cls_Ope_Alm_Salidas_Multi_Negocio Obj = new Cls_Ope_Alm_Salidas_Multi_Negocio();
            DataTable Dt_Almacen = Obj.Consultar_Almacen_Logueado();
            if (Dt_Almacen.Rows.Count == 0)
            {
                //deshbilitamos todas las opciones 
                Div_Contenido_Salidas.Visible = false;
                Div_Contenedor_Msj_Error.Visible = true;
                Div_Grid_Salidas.Visible = false;
                Lbl_Mensaje_Error.Text = "+ No tiene permisos para esta opción.";

            }
            else
            {
                Obj.P_Almacen_ID = Dt_Almacen.Rows[0][Cat_Alm_Multialmacenes.Campo_Almacen_ID].ToString().Trim();
                DataTable Dt_Salidas = Obj.Consultar_Salidas();
                if (Dt_Almacen.Rows.Count != 0)
                {
                    //llenamos el grid de salidas 
                    Grid_Salidas.DataSource = Dt_Salidas;
                    Grid_Salidas.DataBind();

                }
                else
                {
                    Grid_Salidas.EmptyDataText = "No se encontraron datos.";
                    Grid_Salidas.DataBind();
                }
                Configurar_Formulario("Inicio");
                Habilitar_Componentes(false);
            }
            
        }
    }

    #endregion


    ///*******************************************************************************
    ///METODOS
    ///*******************************************************************************

    #region Metodos

    public void Configurar_Formulario(String Estatus)
    {

        switch (Estatus)
        {
            case "Inicio":

                //Boton 
                Btn_Nuevo.Visible = true;
                Btn_Nuevo.ToolTip = "Nuevo";
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                Btn_Nuevo.Enabled = true;
                //Boton Salir
                Btn_Salir.Visible = true;
                Btn_Salir.ToolTip = "Inicio";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                Div_Contenido_Salidas.Visible = false;
                Div_Grid_Salidas.Visible = true;

                break;
            case "Nuevo":

                Btn_Nuevo.Visible = true;
                Btn_Nuevo.ToolTip = "Dar de Alta";
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                Btn_Nuevo.Enabled = true;
                //Boton Salir
                Btn_Salir.Visible = true;
                Btn_Salir.ToolTip = "Cancelar";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                Div_Contenido_Salidas.Visible = true;
                Div_Grid_Salidas.Visible = false;
                
                break;
            case "General":

                //Boton 
                Btn_Nuevo.Visible = true;
                Btn_Nuevo.ToolTip = "Nuevo";
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                Btn_Nuevo.Enabled = true;
                //Boton Salir
                Btn_Salir.Visible = true;
                Btn_Salir.ToolTip = "Listado";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                Div_Contenido_Salidas.Visible = true;
                Div_Grid_Salidas.Visible = false;

                break;
        }//fin del switch


    }

    public void Limpiar_Componentes()
    {
        Txt_No_Salida.Text = "";
        Txt_No_Salida.Enabled = false;
        Txt_Almacen_ID.Text = "";
        Txt_Nombre_Almacen.Text = "";
        Txt_Producto.Text = "";
        Txt_Cantidad.Text = "";
        Session["P_Dt_Productos"] = null;
        Session["Producto_ID"] = null;
        Cmb_Empleado_Recibio.Items.Clear();
        Grid_Productos.DataSource = new DataTable();
        Grid_Productos.DataBind();
    }

    public void Habilitar_Componentes(bool Estatus)
    {
        Txt_No_Salida.Enabled = false;
        Txt_Almacen_ID.Enabled = false;
        Txt_Nombre_Almacen.Enabled = false;
        Cmb_Empleado_Recibio.Enabled = false;
        Txt_Producto.Enabled = false;
        Txt_Cantidad.Enabled = Estatus;
        Btn_Buscar_Empleado.Enabled = Estatus;
        Btn_Agregar.Enabled = Estatus;
        Btn_Filtro_Productos.Enabled = Estatus;
        Grid_Productos.Enabled = Estatus;
        Txt_Total.Enabled = false;
        Txt_IVA.Enabled = false;
        Txt_Subtotal.Enabled = false;
        

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Limpiar_Modal
    ///DESCRIPCIÓN: Metodo que ayuda alimpiar los componentes dentro del Modal POpup
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 6/DIC/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************

    public void Limpiar_Modal()
    {
        Txt_No_Empleado.Text = "";
        Txt_Nombre_Empleado.Text = "";
        Txt_Apellido_Paterno.Text = "";
        Txt_Apellido_Materno.Text = "";
        Grid_Empleados.DataSource = new DataTable();
        Grid_Empleados.DataBind();

    }


    public void Limpiar_Modal_Productos()
    {
        Txt_Nombre_Producto.Text = "";
        Session["Producto_ID"] = null;
        Grid_Productos_Busqueda.DataSource = new DataTable();
        Grid_Productos_Busqueda.DataBind();
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Agregar_Producto
    ///DESCRIPCIÓN: Metodo que permite agregar un nuevo producto al Grid_Productos
    ///PARAMETROS: 1.- DataTable _DataTable: Data_Table que contiene el nuevo prducto a agregar
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 8/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Agregar_Producto(DataTable _DataTable)
    {
        //Realizamos la consulta del producto seleccionado

        //Listado_Negocio.P_Partida_ID = Session["Partida_Almacen_ID"].ToString(); 
        String Id = Session["Producto_ID"].ToString();
        DataRow[] Filas;
        DataTable Dt = (DataTable)Session["P_Dt_Productos"];
        Filas = _DataTable.Select("Producto_ID='" + Id + "'");
        if (Filas.Length > 0)
        {
            //Si se encontro algun coincidencia entre el grupo a agregar con alguno agregado anteriormente, se avisa
            //al usuario que elemento ha agregar ya existe en la tabla de grupos.
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "",
                "alert('No se puede agregar el producto, ya que este ya se ha agregado');", true);
            //Limpiamos las cajas de producto
            Txt_Producto.Text = "";
            Txt_Cantidad.Text = "0";
        }
        else
        {
            Cls_Ope_Alm_Salidas_Multi_Negocio Clase_Negocio = new Cls_Ope_Alm_Salidas_Multi_Negocio();
            Clase_Negocio.P_Producto_ID = Id;
            //Obtengo los datos del nuevo producto a insertar en el GridView
            DataTable Dt_Temporal = Clase_Negocio.Consultar_Productos();
            //Obtenemnos el presupuesto de la partida
            if (!(Dt_Temporal == null))
            {
                if (Dt_Temporal.Rows.Count > 0)
                {
                    DataRow Fila_Nueva = Dt.NewRow();
                    Fila_Nueva = Calcular_Importe_Productos(Dt_Temporal);
                    //Calculamos el importe
                    double Importe = double.Parse(Fila_Nueva["Importe"].ToString());
                    
                    Dt.Rows.Add(Fila_Nueva);
                    Dt.AcceptChanges();
                    Grid_Productos.DataSource = Dt;
                    Session["P_Dt_Productos"] = Dt;
                    Grid_Productos.DataBind();
                    Div_Grid_Productos.Visible = true;
                    Grid_Productos.Visible = true;

                   
                }
                //Limpiamos las cajas de texto
                Txt_Producto.Text = "";
                Txt_Cantidad.Text = "0";
                Calcular_Totales();
            }

        }//fin del else

    }

    public void Calcular_Totales()
    {
        //Recorremos el datatable para calcular los totales
        DataTable Dt_Productos = (DataTable)Session["P_Dt_Productos"];
        if (Dt_Productos.Rows.Count != 0)
        {
            Double Total =0;
            Double IVA = 0;
            Double Subtotal = 0;
            for (int i=0; i<Dt_Productos.Rows.Count ;i++)
            {
               Total = Total + double.Parse(Dt_Productos.Rows[i]["Importe"].ToString());
               IVA = IVA + double.Parse(Dt_Productos.Rows[i]["Monto_IVA"].ToString());
               Subtotal = Subtotal +( double.Parse(Dt_Productos.Rows[i]["Precio_Unitario"].ToString()) * double.Parse(Dt_Productos.Rows[i]["Cantidad"].ToString()));
            }
            Txt_Total.Text = Total.ToString();
            Txt_IVA.Text = IVA.ToString();
            Txt_Subtotal.Text = Subtotal.ToString();
        }
    }

    #endregion

    #region 
    public Cls_Ope_Alm_Salidas_Multi_Negocio Cargar_Datos_Negocio()
    {
        Cls_Ope_Alm_Salidas_Multi_Negocio Clase_Negocio = new Cls_Ope_Alm_Salidas_Multi_Negocio();
        Clase_Negocio.P_Almacen_ID = Txt_Almacen_ID.Text.Trim();
        Clase_Negocio.P_Empleado_Recibio_ID = Cmb_Empleado_Recibio.SelectedValue;
        Clase_Negocio.P_Empleado_Almacen_ID = Cls_Sessiones.Empleado_ID;
        Clase_Negocio.P_IVA = Txt_IVA.Text;
        Clase_Negocio.P_Total = Txt_Total.Text;
        Clase_Negocio.P_Subtotal = Txt_Total.Text;
        Clase_Negocio.P_Dt_Productos = (DataTable)Session["P_Dt_Productos"];



        return Clase_Negocio;

    }

    public void Verificar_Contenido()
    {
        if (Cmb_Empleado_Recibio.Items.Count == 0)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "+ Es necesario indicar el Nombre de la Persona que recibio <br/>";
        }
        if (Grid_Productos.Rows.Count == 0)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "+ Es necesario agregar productos a la salida <br/>";
        }

    }
    
    #endregion


    ///*******************************************************************************
    ///GRID
    ///*******************************************************************************
    #region Grid


    protected void Grid_Productos_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataRow[] Renglones;
        DataRow Renglon;
        //Obtenemos el Id del producto seleccionado
        GridViewRow selectedRow = Grid_Productos.Rows[Grid_Productos.SelectedIndex];
        String Id = Convert.ToString(selectedRow.Cells[2].Text);
        Renglones = ((DataTable)Session["P_Dt_Productos"]).Select(Cat_Com_Productos.Campo_Clave + "='" + Id + "'");

        if (Renglones.Length > 0)
        {
            Renglon = Renglones[0];
            DataTable Tabla = (DataTable)Session["P_Dt_Productos"];
            Tabla.Rows.Remove(Renglon);
            Session["P_Dt_Productos"] = Tabla;
            Grid_Productos.SelectedIndex = (-1);
            Grid_Productos.DataSource = Tabla;
            Grid_Productos.DataBind();
            //Calculamos el nuevo importe
            //Session["Importe_Total"] = Importe_Acumulado();
            //Txt_Total.Text = Session["Importe_Total"].ToString();
            //Asignamos el nuevo valor al datatable de Session
            Session["P_Dt_Productos"] = Tabla;
            Calcular_Totales();
        }

    }

    protected void Grid_Salidas_SelectedIndexChanged(object sender, EventArgs e)
    {
        Configurar_Formulario("General");
        Cls_Ope_Alm_Salidas_Multi_Negocio Clase_Negocio = new Cls_Ope_Alm_Salidas_Multi_Negocio();
        DataTable Dt_Almacen = Clase_Negocio.Consultar_Almacen_Logueado();
        Clase_Negocio.P_Almacen_ID = Dt_Almacen.Rows[0][Cat_Alm_Multialmacenes.Campo_Almacen_ID].ToString().Trim();
        Clase_Negocio.P_No_Salida = Grid_Salidas.SelectedDataKey["No_Salida"].ToString();

        DataTable Dt_Salida = Clase_Negocio.Consultar_Salidas();
        
        Txt_Almacen_ID.Text = Dt_Almacen.Rows[0][Cat_Alm_Multialmacenes.Campo_Almacen_ID].ToString().Trim();
        Txt_No_Salida.Text = Dt_Salida.Rows[0][Ope_Alm_Salidas_Multi.Campo_No_Salida].ToString().Trim();
        Txt_Nombre_Almacen.Text = Dt_Almacen.Rows[0][Cat_Alm_Multialmacenes.Campo_Nombre].ToString().Trim();
        Cmb_Empleado_Recibio.Items.Clear();
        Cmb_Empleado_Recibio.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode(Dt_Salida.Rows[0]["EMPLEADO_RECIBIO"].ToString().Trim()), Dt_Salida.Rows[0][Ope_Alm_Salidas_Multi.Campo_Empleado_Recibio_ID].ToString().Trim()));
        Cmb_Empleado_Recibio.SelectedIndex = 0;
        Txt_IVA.Text = Dt_Salida.Rows[0][Ope_Alm_Salidas_Multi.Campo_IVA].ToString().Trim();
        Txt_Subtotal.Text = Dt_Salida.Rows[0][Ope_Alm_Salidas_Multi.Campo_Subtotal].ToString().Trim();
        Txt_Total.Text = Dt_Salida.Rows[0][Ope_Alm_Salidas_Multi.Campo_Total].ToString().Trim();
        //CONSULTAMOS LOS PRODUCTOS
        DataTable Dt_Productos = Clase_Negocio.Consultar_Detalle_Salidas();
        if (Dt_Productos.Rows.Count != 0)
        {
            Grid_Productos.DataSource = Dt_Productos;
            Grid_Productos.DataBind();
        }
        else
        {
            Grid_Productos.EmptyDataText = "No se encontraron Datos";
            Grid_Productos.DataBind();
        }
    }

    protected void Grid_Empleados_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridViewRow Row = Grid_Empleados.SelectedRow;
        int Num_Linea = Grid_Empleados.SelectedIndex;
        Session["RESPONSABLE_ID"] = Grid_Empleados.SelectedDataKey["Empleado_ID"].ToString();
        Cmb_Empleado_Recibio.Items.Clear();
        Cmb_Empleado_Recibio.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode(Row.Cells[3].Text), Grid_Empleados.SelectedDataKey["Empleado_ID"].ToString()));
        Cmb_Empleado_Recibio.SelectedIndex = 0;
        //Obj_Combo.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("<SELECCIONAR>"), "0"));
       
        Modal_Buscar_Empleado.Hide();
    }


    protected void Grid_Productos_Busqueda_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridViewRow Row = Grid_Productos_Busqueda.SelectedRow;
        int Num_Linea = Grid_Productos_Busqueda.SelectedIndex;
        Session["Producto_ID"] = Grid_Productos_Busqueda.SelectedDataKey["Producto_Servicio_ID"].ToString();
        Txt_Producto.Text = Row.Cells[2].Text;
        Session["Existencia"] = Row.Cells[5].Text;
        Modal_Productos.Hide();

    }

    public DataRow Calcular_Importe_Productos(DataTable Data_Productos)
    {
        //Calculamos el impuesto de los productos del DataSet 
        double Impuesto_porcentaje_1 = 0;
        double Impuesto_porcentaje_2 = 0;
        double IEPS = 0;
        double IVA = 0;
        double Mayor = 0;
        double Menor = 0;
        double Importe_Producto = 0;
        double Cantidad = 0;
        double Precio_Unitario = 0;
        double Precio_Unitario_X_Cantidad = 0;
        // Creamos el Data_Aux que contendra la estructura del grid de Productos
        DataTable Data_Aux = (DataTable)Session["P_Dt_Productos"];
        DataRow Fila = Data_Aux.NewRow();
        if (Data_Productos.Rows.Count > 0)
        {
            Impuesto_porcentaje_1 = 0;
            Impuesto_porcentaje_2 = 0;
            Mayor = 0;
            Menor = 0;
            Importe_Producto = 0;
            Cantidad = double.Parse(Txt_Cantidad.Text);
            Precio_Unitario = 0;
            if (Data_Productos.Rows[0]["IMPUESTO_PORCENTAJE_1"].ToString() != "")
            {
                Impuesto_porcentaje_1 = double.Parse(Data_Productos.Rows[0]["IMPUESTO_PORCENTAJE_1"].ToString());

            }
            if (Data_Productos.Rows[0]["IMPUESTO_PORCENTAJE_2"].ToString() != "")
            {
                Impuesto_porcentaje_2 = double.Parse(Data_Productos.Rows[0]["IMPUESTO_PORCENTAJE_2"].ToString());

            }
            //Asignamos valores a Cantidad y precio unitario
            Precio_Unitario = double.Parse(Data_Productos.Rows[0]["PRECIO_UNITARIO"].ToString());
            //Calculas los Impuestos en caso de tener 2 para obtener el importe total del producto
            if (Impuesto_porcentaje_1 != 0 && Impuesto_porcentaje_2 != 0)
            {
                Mayor = Math.Max(Impuesto_porcentaje_1, Impuesto_porcentaje_2);
                Menor = Math.Min(Impuesto_porcentaje_1, Impuesto_porcentaje_2);
                //Calculamos el IEPS 
                IEPS = ((Precio_Unitario * Cantidad) * Mayor) / 100;
                //Calculamos el IVA
                IVA = ((Precio_Unitario * Cantidad) * Menor) / 100;
                //Primero obtenemos el Impuesto IEPS
                Importe_Producto = ((Precio_Unitario * Cantidad) * Mayor) / 100;
                //Despues a lo obtenido del impuesto ieps le sumamos el impuesto Iva
                Importe_Producto = (Importe_Producto * Menor) / 100;
                //Sumamos el impuesto al importe total 
                Importe_Producto = Importe_Producto + (Precio_Unitario * Cantidad);
                //Le asignamos el valor a la columna de importe
                Fila["Importe"] = Importe_Producto;
            }
            //En caso de tener un solo impuesto 
            if (Impuesto_porcentaje_1 != 0 && Impuesto_porcentaje_2 == 0)
            {
                Importe_Producto = ((Precio_Unitario * Cantidad) * Impuesto_porcentaje_1) / 100;
                Importe_Producto = Importe_Producto + (Precio_Unitario * Cantidad);
                Fila["Importe"] = Importe_Producto;
                //Calculamos el monto de IVA o IEPS dependiendo cual le corresponda
                if (Data_Productos.Rows[0]["TIPO_IMPUESTO_1"].ToString() == "IVA")
                {
                    //Asignamos el Monto IVA 
                    IVA = ((Precio_Unitario * Impuesto_porcentaje_1) / 100) * Cantidad;
                    Fila["Porcentaje_IVA"] = Data_Productos.Rows[0]["IMPUESTO_PORCENTAJE_1"].ToString();
                    Fila["Porcentaje_IEPS"] = "0";
                    IEPS = 0;


                }
                if (Data_Productos.Rows[0]["TIPO_IMPUESTO_1"].ToString() == "IEPS")
                {
                    //Asignamos el monto IEPS
                    IEPS = ((Precio_Unitario * Impuesto_porcentaje_1) / 100) * Cantidad;
                    Fila["Porcentaje_IEPS"] = Data_Productos.Rows[0]["IMPUESTO_PORCENTAJE_1"].ToString();
                    Fila["Porcentaje_IVA"] = "0";
                    IVA = 0;
                }
            }
            if (Impuesto_porcentaje_1 == 0 && Impuesto_porcentaje_2 == 0)
            {
                //en caso de no tener impuestos el producto
                Importe_Producto = (Precio_Unitario * Cantidad);
                Fila["Importe"] = Importe_Producto;
                //Asignamos por default 0 los porcentajes de y montos de los impuestos 
                Fila["Monto_IVA"] = "0";
                Fila["Monto_IEPS"] = "0";
                Fila["Porcentaje_IVA"] = "0";
                Fila["Porcentaje_IEPS"] = "0";
            }
            //Asignamos los valores a la fila
            Fila["Producto_ID"] = Data_Productos.Rows[0]["PRODUCTO_ID"].ToString();
            Fila["Clave"] = Data_Productos.Rows[0]["CLAVE"].ToString();
            Fila["Producto_Nombre"] = Data_Productos.Rows[0]["PRODUCTO_NOMBRE"].ToString();
            Fila["Descripcion"] = Data_Productos.Rows[0]["Descripcion"].ToString();
            Fila["Unidad"] = Data_Productos.Rows[0]["Unidad"].ToString();
            Fila["Existencia"] = Data_Productos.Rows[0]["Existencia"].ToString();
            Fila["Reorden"] = Data_Productos.Rows[0]["REORDEN"].ToString();
            Fila["Cantidad"] = Cantidad.ToString();
            Precio_Unitario_X_Cantidad = Precio_Unitario * Cantidad;
            Fila["Precio_Unitario"] = Precio_Unitario.ToString();
            Fila["Monto_IVA"] = IVA.ToString();
            Fila["Monto_IEPS"] = IEPS.ToString();
            //Data_Aux.Rows.Add(Fila);
            //Data_Aux.AcceptChanges();
           
        }//fin del if

        return Fila;
    }
    #endregion


    ///*******************************************************************************
    ///EVENTOS
    ///*******************************************************************************
    #region Eventos

    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Cls_Ope_Alm_Salidas_Multi_Negocio Clase_Negocio = new Cls_Ope_Alm_Salidas_Multi_Negocio();
        switch(Btn_Nuevo.ToolTip)
        {
            case "Nuevo":
                Configurar_Formulario("Nuevo");
                Habilitar_Componentes(true);
                Limpiar_Componentes();
                //Consultamos cual es el Almacen y llenamos los datos 
                DataTable Dt_Almacen = Clase_Negocio.Consultar_Almacen_Logueado();
                if (Dt_Almacen.Rows.Count > 0)
                {
                    Txt_Almacen_ID.Text = Dt_Almacen.Rows[0][Cat_Alm_Multialmacenes.Campo_Almacen_ID].ToString().Trim();
                    Txt_Nombre_Almacen.Text = Dt_Almacen.Rows[0][Cat_Alm_Multialmacenes.Campo_Nombre].ToString().Trim();

                }

                break;
            case "Dar de Alta":
                Verificar_Contenido();
                if(Div_Contenedor_Msj_Error.Visible == false)
                {
                    Clase_Negocio = Cargar_Datos_Negocio();
                    String Resultado = Clase_Negocio.Alta_Salidas();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Requisiciones", "alert('" + Resultado + "');", true);
                    Configurar_Formulario("Inicio");
                    Limpiar_Componentes();

                }
                break;

        }
    }

    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
         Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        switch (Btn_Salir.ToolTip)
        {
            case "Inicio":
                Limpiar_Componentes();
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");

                break;
            case "Cancelar":
                Limpiar_Componentes();
                Configurar_Formulario("Inicio");
                
                break;
        }
    }

    protected void Btn_Filtro_Productos_Click(object sender, ImageClickEventArgs e)
    {
        Modal_Productos.Show();
        Limpiar_Modal_Productos();
        Txt_Producto.Text = "";
        Txt_Cantidad.Text = "";

    }

    protected void Btn_Agregar_Click(object sender, ImageClickEventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        if(Txt_Producto.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "+Es necesario seleccionar un Producto<br /> ";
        }
        if (Txt_Cantidad.Text.Trim() == "0")
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+Es necesario indicar la cantidad solicitada del Producto<br /> ";
        }
        if (int.Parse(Txt_Cantidad.Text.Trim()) > int.Parse(Session["Existencia"].ToString()))
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+No hay existencia suficiento solo existen " + Session["Existencia"].ToString() + " <br /> ";
        }
        if (Div_Contenedor_Msj_Error.Visible == false)
        {
            if (Session["P_Dt_Productos"] != null)
            {
                Agregar_Producto((DataTable)Session["P_Dt_Productos"]);
            }//fin if
            else
            {
                //Creamos la session por primera ves
                DataTable Dt_Producto = new DataTable();
                Dt_Producto.Columns.Add("Producto_ID", typeof(System.String));
                Dt_Producto.Columns.Add("Clave", typeof(System.String));
                Dt_Producto.Columns.Add("Producto_Nombre", typeof(System.String));
                Dt_Producto.Columns.Add("Descripcion", typeof(System.String));
                Dt_Producto.Columns.Add("Unidad", typeof(System.String));
                Dt_Producto.Columns.Add("Existencia", typeof(System.String));
                Dt_Producto.Columns.Add("Reorden", typeof(System.String));
                Dt_Producto.Columns.Add("Cantidad", typeof(System.String));
                Dt_Producto.Columns.Add("Precio_Unitario", typeof(System.String));
                Dt_Producto.Columns.Add("Importe", typeof(System.String));
                Dt_Producto.Columns.Add("Monto_IVA", typeof(System.String));
                Dt_Producto.Columns.Add("Monto_IEPS", typeof(System.String));
                Dt_Producto.Columns.Add("Porcentaje_IVA", typeof(System.String));
                Dt_Producto.Columns.Add("Porcentaje_IEPS", typeof(System.String));
                Session["P_Dt_Productos"] = Dt_Producto;
                //Llenamos el grid
                Grid_Productos.DataSource = (DataTable)Session["P_Dt_Productos"];
                Grid_Productos.DataBind();
                Agregar_Producto(Dt_Producto);

            }//fin else
        }

    }

    protected void Btn_Buscar_Empleado_Click(object sender, ImageClickEventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Modal_Buscar_Empleado.Show();
        Limpiar_Modal();

    }

    protected void Btn_Filtro_Empleados_Click(object sender, ImageClickEventArgs e)
    {
        Modal_Buscar_Empleado.Show();
        Cls_Ope_Alm_Salidas_Multi_Negocio Clase_Negocio = new Cls_Ope_Alm_Salidas_Multi_Negocio();
        if (Txt_No_Empleado.Text.Trim() != String.Empty || Txt_Nombre_Empleado.Text.Trim() != String.Empty || Txt_Apellido_Paterno.Text.Trim() != String.Empty || Txt_Apellido_Materno.Text.Trim() != String.Empty)
        {
            if (Txt_No_Empleado.Text.Trim() != String.Empty)
            {
                Clase_Negocio.P_No_Empleado = String.Format("{0:000000}", Convert.ToInt32(Txt_No_Empleado.Text.Trim()));
            }

            if (Txt_Nombre_Empleado.Text.Trim() != String.Empty)
            {
                Clase_Negocio.P_Nombre_Empleado = Txt_Nombre_Empleado.Text.Trim();
            }

            if (Txt_Apellido_Paterno.Text.Trim() != String.Empty)
            {
                Clase_Negocio.P_Apellido_Paterno = Txt_Apellido_Paterno.Text.Trim();
            }

            if (Txt_Apellido_Materno.Text.Trim() != String.Empty)
            {
                Clase_Negocio.P_Apellido_Materno = Txt_Apellido_Materno.Text.Trim();
            }

            //Realizamos la consulta de los empleados
            DataTable Dt_Empleados = Clase_Negocio.Consultar_Empleado();

            if (Dt_Empleados.Rows.Count > 0)
            {
                Grid_Empleados.DataSource = Dt_Empleados;
                Grid_Empleados.DataBind();
                Grid_Empleados.Visible = true;

            }
            else
            {
                Grid_Empleados.EmptyDataText = "No se han encontrado empleados con esos datos.";
                Grid_Empleados.DataSource = new DataTable();
                Grid_Empleados.DataBind();
            }
        }


    }

    protected void Btn_Realizar_Busqueda_Click(object sender, ImageClickEventArgs e)
    {
        if (Txt_Nombre_Producto.Text.Trim() != String.Empty)
        {
            Cls_Ope_Alm_Salidas_Multi_Negocio Clase_Negocios = new Cls_Ope_Alm_Salidas_Multi_Negocio();
            Clase_Negocios.P_Nombre_Producto = Txt_Nombre_Producto.Text;
            Clase_Negocios.P_Almacen_ID = Txt_Almacen_ID.Text.Trim();
            DataTable Dt_Busqueda_Productos = Clase_Negocios.Consultar_Productos();

            if (Dt_Busqueda_Productos.Rows.Count != 0)
            {
                Grid_Productos_Busqueda.DataSource = Dt_Busqueda_Productos;
                Grid_Productos_Busqueda.DataBind();
            }
            else
            {
                Grid_Productos_Busqueda.EmptyDataText = "No se encontraron datos";
                Grid_Productos_Busqueda.DataSource = new DataTable();
                Grid_Productos_Busqueda.DataBind();
            }
        }
        else
        {
            Grid_Productos_Busqueda.EmptyDataText = "Es necesario indicar el nombre del producto a buscar";
            Grid_Productos_Busqueda.DataSource = new DataTable();
            Grid_Productos_Busqueda.DataBind();
        }

    }

    #endregion



}
