﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Ope_Alertas_Proveedores.aspx.cs" Inherits="paginas_Paginas_Generales_Ope_Alertas_Proveedores" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script>
    function Cerrar_Ventana_Modal() {
        window.close();
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server" style="width: 300px; height: 300px">
    <div>
        <table width="100%">
         <tr>
                <td colspan= "4" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: x-large; font-weight: bold">
                    Alertas PULL Proveedores
                </td>
                                    
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="Grid_Alertas" runat="server" CellPadding="4" ForeColor="#333333" 
                        GridLines="None" AutoGenerateColumns="false" Width="100%">
                        <Columns>
                        <asp:BoundField DataField="Requisicion" HeaderText="Requisicion" Visible="True">
                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Orden_Compra" HeaderText="Orden Compra" Visible="True">
                            <HeaderStyle HorizontalAlign="Left" Width="80%" />
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                        </asp:BoundField>
                        </Columns>                                                                  
                        
                        
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>     
                
                </td>                          
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
