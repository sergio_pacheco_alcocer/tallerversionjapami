﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Apl_Cat_Modulos_SIAG.aspx.cs" Inherits="paginas_Paginas_Generales_Frm_Apl_Cat_Modulos_SIAG" Title="Catálogo de Modulos" UICulture="es-MX" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="SM_Modulos" runat="server" />
    <asp:UpdatePanel ID="UPnl_Modulos" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="UPnl_Modulos"
                    DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">

                        </div>
                        <div class="processMessage" id="div_progress">
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>                   
                </asp:UpdateProgress>              
                
            <div id="Div_Modulos" style="background-color:#ffffff; width:98%;">
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">Catálogo de Modulos</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
                </table>              
            
                <table width="98%"  border="0" cellspacing="0">
                     <tr align="center">
                         <td colspan="2">                
                             <div align="right" class="barra_busqueda">                        
                                  <table style="width:100%;height:28px;">
                                    <tr>
                                      <td align="left" style="width:59%;">                                                  
                                           <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" CssClass="Img_Button" TabIndex="1"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" onclick="Btn_Nuevo_Click" />
                                            <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" CssClass="Img_Button" TabIndex="2"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" onclick="Btn_Modificar_Click" />
                                            <asp:ImageButton ID="Btn_Eliminar" runat="server" ToolTip="Eliminar" CssClass="Img_Button" TabIndex="3"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png" onclick="Btn_Eliminar_Click"
                                                OnClientClick="return confirm('¿Está seguro de eliminar el Menú seleccionado?');"/>
                                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button" TabIndex="4"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" onclick="Btn_Salir_Click"/>
                                      </td>
                                      <td align="right" style="width:41%;">
                                        <table style="width:100%;height:28px;">
                                            <tr>
                                                <td style="width:60%;vertical-align:top;">
                                                    B&uacute;squeda
                                                    <asp:TextBox ID="Txt_Busqueda_Modulos" runat="server" MaxLength="100" TabIndex="5" ToolTip="Buscar Modulo"
                                                        Width="180px"/>
                                                    <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda_Modulos" runat="server" WatermarkCssClass="watermarked"
                                                        WatermarkText="<Ingrese el Modulo>" TargetControlID="Txt_Busqueda_Modulos" />
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Modulos" runat="server" TargetControlID="Txt_Busqueda_Modulos"
                                                        FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ. "/>
                                                    <asp:ImageButton ID="Btn_Buscar_Modulos" runat="server" TabIndex="6" ToolTip="Consultar"
                                                        ImageUrl="~/paginas/imagenes/paginas/busqueda.png" onclick="Btn_Buscar_Modulos_Click" />                                  
                                                </td>
                                            </tr>                                                                          
                                        </table>                                    
                                       </td>       
                                     </tr>         
                                  </table>                      
                                </div>
                         </td>
                     </tr>
                </table>                      

                <br />               
                <table width="98%">
                    <tr>
                        <td style="width:20%; text-align:left;">
                            Modulo ID
                        </td>
                        <td style="width:30%; text-align:left;">
                            <asp:TextBox ID="Txt_Modulo_ID" runat="server" Width="98%" Enabled="false"/>
                        </td>
                        <td style="width:20%; text-align:left;">
                            &nbsp;
                        </td>
                        <td style="width:30%; text-align:left;">
                            &nbsp;
                        </td>                        
                    </tr>                    
                    <tr>
                        <td style="width:20%; text-align:left;">
                            *Nombre
                        </td>
                        <td style="width:80%; text-align:left;" colspan="3">
                            <asp:TextBox ID="Txt_Nombre_Modulo" runat="server" MaxLength="100" TabIndex="7" Width="99.5%"/>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Modulo" runat="server" TargetControlID="Txt_Nombre_Modulo"
                                FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ. " />
                        </td>
                    </tr>
               </table>
               <br />
                            <asp:GridView ID="Grid_Modulos" runat="server" AllowSorting="True" 
                                AutoGenerateColumns="False" CssClass="GridView_1" DataKeyNames="MODULO_ID" 
                                GridLines="None" HeaderStyle-CssClass="tblHead" 
                                onselectedindexchanged="Grid_Modulos_SelectedIndexChanged">
                                <SelectedRowStyle CssClass="GridSelected" />
                                <PagerStyle CssClass="GridHeader" />
                                <HeaderStyle CssClass="GridHeader" ForeColor="White" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                                <Columns>
                                    <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                        ImageUrl="~/paginas/imagenes/paginas/Select_Grid_Inner.png">
                                        <ItemStyle Width="3%" />
                                    </asp:ButtonField>
                                    <asp:TemplateField>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Modulo_ID" Visible="True">
                                        <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                        <ItemStyle Font-Size="0px" ForeColor="Transparent" HorizontalAlign="Left" 
                                            Width="0%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" 
                                        SortExpression="Nombre_Modulo" Visible="True">
                                        <FooterStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left" Width="90%" />
                                        <ItemStyle HorizontalAlign="Left" Width="90%" />
                                    </asp:BoundField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            </td>
                                            </tr>
                                            <tr>
                                                <td colspan="100%">
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                <br />                               
                <br /><br /><br /><br />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel> 
</asp:Content>

