﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Frm_Ope_Alertas_Sistema.aspx.cs" Inherits="paginas_Paginas_Generales_Frm_Ope_Alertas_Sistema" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script>
    function Cerrar_Ventana_Modal() {
        window.close();
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server" style="width: 300px; height: 300px">
    <div>
        <table width="100%">
         <tr>
                <td colspan= "4" align="center" style="background-color: #0000FF; font-family: Arial, Helvetica, sans-serif; color: #FFFFFF; font-size: x-large; font-weight: bold">
                    Alertas Sistema SIAC
                </td>
                                    
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="Grid_Alertas" runat="server" CellPadding="4" ForeColor="#333333" 
                        GridLines="None" AutoGenerateColumns="false" Width="100%">
                        <Columns>
                        <asp:BoundField DataField="Modulo" HeaderText="Modulo" Visible="True" ItemStyle-BackColor="Red">
                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Alerta" HeaderText="Descripcion Alertas" Visible="True" ItemStyle-BackColor="Red">
                            <HeaderStyle HorizontalAlign="Left" Width="80%" />
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                        </asp:BoundField>
                        </Columns>                                                                  
                        
                        <RowStyle BackColor="#EFF3FB" />
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>     
                
                </td>                          
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
