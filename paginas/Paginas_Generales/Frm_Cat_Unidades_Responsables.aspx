﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Cat_Unidades_Responsables.aspx.cs" Inherits="paginas_Presupuestos_Frm_Cat_Unidades_Responsables" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Dependencias" runat="server" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">    
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div id="Div_Unidades_Responsables" style="background-color:#ffffff; width:99%; height:100%;">
                <table width="100%" class="estilo_fuente">
                    <tr align="center">
                        <td colspan="4" class="label_titulo">
                            Cat&aacute;logo de Unidad Responsable
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="98%"  border="0" cellspacing="0">
                    <tr align="center">
                        <td colspan="2">                
                            <div align="right" class="barra_busqueda">                        
                                <table style="width:100%;height:28px;">
                                    <tr>
                                        <td align="left" style="width:59%;">  
                                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" 
                                                CssClass="Img_Button" TabIndex="1"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" 
                                                onclick="Btn_Nuevo_Click" />
                                            <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" 
                                                CssClass="Img_Button" TabIndex="2"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" 
                                                onclick="Btn_Modificar_Click" />
                                            <asp:ImageButton ID="Btn_Eliminar" runat="server" ToolTip="Eliminar" 
                                                CssClass="Img_Button" TabIndex="3"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png" 
                                                
                                                OnClientClick="return confirm('¿Está seguro de dar de baja la unidad responsable seleccionada?');" 
                                                onclick="Btn_Eliminar_Click"/>
                                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" 
                                                CssClass="Img_Button" TabIndex="4"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                                onclick="Btn_Salir_Click" />
                                        </td>
                                        <td align="right" style="width:41%;">
                                            <table style="width:100%;height:28px;">
                                                <tr>
                                                    <td style="vertical-align:middle;text-align:right;width:20%;">B&uacute;squeda:</td>
                                                    <td style="width:55%;">
                                                        <asp:TextBox ID="Txt_Busqueda_Dependencia" runat="server" MaxLength="100" TabIndex="5"  ToolTip = "Buscar por Nombre"
                                                            Width="180px"/>
                                                        <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda_Dependencia" runat="server" WatermarkCssClass="watermarked"
                                                            WatermarkText="<Ingrese Nombre>" TargetControlID="Txt_Busqueda_Dependencia" />
                                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Dependencia" 
                                                            runat="server" TargetControlID="Txt_Busqueda_Dependencia" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ. "/>                                                    
                                                    </td>
                                                    <td style="vertical-align:middle;width:5%;" >
                                                        <asp:ImageButton ID="Btn_Buscar_Dependencia" runat="server" ToolTip="Consultar" TabIndex="6"
                                                            ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                                            onclick="Btn_Buscar_Dependencia_Click" />                                        
                                                    </td>
                                                </tr>                                                                          
                                            </table>                                    
                                        </td>       
                                    </tr>         
                                </table>                      
                            </div>
                        </td>
                    </tr>
                </table>
                <br />
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">  
                    <tr>
                        <td align="left">ID</td>
                        <td align="left"><asp:TextBox ID="Txt_Unidad_Responsable_ID" runat="server" Width="150px" Enabled="false"></asp:TextBox></td>
                        <td align="left">Clave Contabilidad</td>
                        <td align="left"><asp:TextBox ID="Txt_Clave_Contabilidad" runat="server"></asp:TextBox></td>
                    </tr>
                   
                    <tr>
                        <td align="left">Nombre</td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="Txt_Nombre" runat="server" Width="100%" MaxLength="100"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Dependencia" 
                                runat="server" TargetControlID="Txt_Nombre" 
                                FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ. " />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">Clave</td>
                        <td align="left">
                            <asp:TextBox ID="Txt_Clave" runat="server" Width="150px" MaxLength="5"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTxt_Clave_Dependecia" 
                                runat="server" TargetControlID="Txt_Clave" 
                                FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"/>
                        </td>
                        <td align="left">Estatus</td>
                        <td align="left">
                            <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="150px">
                                <asp:ListItem Text="Seleccione" Value=""></asp:ListItem>
                                <asp:ListItem Text="Activo" Value="ACTIVO"></asp:ListItem>
                                <asp:ListItem Text="Inactivo" Value="INACTIVO"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="left">Gerencia</td>
                        <td align="left" colspan="3"><asp:DropDownList ID="Cmb_Grupo_Dependencia" runat="server" Width="100%"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">Área Funcional</td>
                        <td align="left" colspan="3"><asp:DropDownList ID="Cmb_Area_Funcional" runat="server" Width="100%"></asp:DropDownList></td>
                    </tr>
                    
                    <tr>
                        <td align="left">Comentarios</td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="Txt_Comentarios" runat="server" TextMode="MultiLine" Width="100%" MaxLength="250"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Comentarios_Dependencia" runat="server" WatermarkCssClass="watermarked"
                                TargetControlID ="Txt_Comentarios" WatermarkText="L&iacute;mite de Caractes 250"/>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Comentarios_Dependencia" 
                                runat="server" TargetControlID="Txt_Comentarios" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ "/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <div style="overflow: auto; height: 320px; width: 99%; vertical-align: top; border-style: outset;
                                    border-color: Silver;">                       
                            <asp:GridView ID="Grid_Unidades_Responsables" runat="server" Width="100%" 
                                AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" 
                                AllowPaging="false"  HeaderStyle-CssClass="tblHead" 
                                
                                onselectedindexchanged="Grid_Unidades_Responsables_SelectedIndexChanged">
                                <Columns>
                                    <asp:ButtonField CommandName="Select" ButtonType="Image" ImageUrl="~/paginas/imagenes/gridview/blue_button.png" />
                                    <asp:BoundField DataField="Clave" HeaderText="Clave" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="Grupo_Dependencia" HeaderText="Grupo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="Dependencia_ID" HeaderText="Dependencia_ID" />
                                    <asp:BoundField DataField="AREA_FUNCIONAL_ID" />
                                </Columns>
                                <SelectedRowStyle CssClass="GridSelected" />
                                <PagerStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                            </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

