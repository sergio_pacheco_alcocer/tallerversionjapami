﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Area_Funcional.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Grupos_Dependencias.Negocio;
using JAPAMI.Catalogo_Unidades_Responsables.Negocio;
using JAPAMI.Sessiones;

public partial class paginas_Presupuestos_Frm_Cat_Unidades_Responsables : System.Web.UI.Page
{
    #region (Variables)
    private const String P_Dt_Unidades_Responsables = "Dt_Unidades_Responsables";
    #endregion

    #region (Page Load)
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (String.IsNullOrEmpty(Cls_Sessiones.Empleado_ID))
                Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

            if (!IsPostBack)
            {
                Estado_Inicial();
            }
            else
            {
                Mostrar_Mensaje_Error(false, "");
            }
        }
        catch (Exception ex)
        {
            Mostrar_Mensaje_Error(true, ex.Message);
        }
    }
    #endregion

    #region (Metodos)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION:   Mostrar_Mensaje_Error
    /// DESCRIPCION :           Mostrar/ocultar el mensaje de error
    /// PARAMETROS  :           1. Mostrar: Booleano que indica si se tiene que mostrar el mensaje de error
    ///                         2. Mensaje: Cadena de texto con el mensaje a mostrar
    /// CREO        :           Noe Mosqueda Valadez
    /// FECHA_CREO  :           19/Junio/2012 13:43
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Mostrar_Mensaje_Error(Boolean Mostrar, String Mensaje)
    {
        try
        {
            //verificar si se tiene que mostrar
            if (Mostrar == true)
            {
                Lbl_Mensaje_Error.Text = Mensaje;
            }
            else
            {
                Lbl_Mensaje_Error.Text = "";
            }

            Img_Error.Visible = Mostrar;
            Lbl_Mensaje_Error.Visible = Mostrar;
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Text = ex.Message;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION:   Llena_Combo_Grupos_Dependencias
    /// DESCRIPCION :           Llenar el combo de los grupos de las dependencias
    /// PARAMETROS  :           
    /// CREO        :           Noe Mosqueda Valadez
    /// FECHA_CREO  :           19/Junio/2012 17:15
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Llena_Combo_Grupos_Dependencias()
    {
        //Declaracion de variables
        Cls_Cat_Grupos_Dependencias_Negocio Grupos_Dependencia_Negocio = new Cls_Cat_Grupos_Dependencias_Negocio(); //Variable para la capa de negocios
        DataTable Dt_Grupos_Dependencias = new DataTable(); //Tabla para el llenado del combo

        try
        {
            //Ejecutar consulta
            Dt_Grupos_Dependencias = Grupos_Dependencia_Negocio.Consultar_Grupos_Dependencias();

            //Llenar el combo
            Cmb_Grupo_Dependencia.Items.Clear();
            Cmb_Grupo_Dependencia.DataSource = Dt_Grupos_Dependencias;
            Cmb_Grupo_Dependencia.DataTextField = "CLAVE_NOMBRE";
            Cmb_Grupo_Dependencia.DataValueField = Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID;
            Cmb_Grupo_Dependencia.DataBind();
            Cmb_Grupo_Dependencia.Items.Insert(0, new ListItem("Seleccione", ""));
            Cmb_Grupo_Dependencia.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION:   Estado_Inicial
    /// DESCRIPCION :           Colocar la pagina en un estado inciial para su navegacion
    /// PARAMETROS  :           
    /// CREO        :           Noe Mosqueda Valadez
    /// FECHA_CREO  :           19/Junio/2012 17:30
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Estado_Inicial()
    {
        try
        {
            Elimina_Sesiones();
            
            Llena_Combo_Grupos_Dependencias();
            Llena_Grid_Unidades_Responsables("", -1);
            Llenar_Area_Funcional();
            Limpiar_Controles();
            Modo_Operacion("Inicial");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION:   Elimina_Sesiones
    /// DESCRIPCION :           Eliminar las sesiones utilizadas en la pagina
    /// PARAMETROS  :           
    /// CREO        :           Noe Mosqueda Valadez
    /// FECHA_CREO  :           19/Junio/2012 17:35
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Elimina_Sesiones()
    {
        try
        {
            HttpContext.Current.Session.Remove(P_Dt_Unidades_Responsables);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION:   Limpiar_Controles
    /// DESCRIPCION :           Limpiar los controles del formulario
    /// PARAMETROS  :           
    /// CREO        :           Noe Mosqueda Valadez
    /// FECHA_CREO  :           19/Junio/2012 19:00
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Limpiar_Controles()
    {
        try
        {
            Txt_Busqueda_Dependencia.Text = "";
            Txt_Clave.Text = "";
            Txt_Clave_Contabilidad.Text = "";
            Txt_Nombre.Text = "";
            Txt_Unidad_Responsable_ID.Text = "";
            Txt_Comentarios.Text = "";
            Cmb_Estatus.SelectedIndex = 0;
            Cmb_Grupo_Dependencia.SelectedIndex = 0;
            Grid_Unidades_Responsables.SelectedIndex = -1;
            Cmb_Area_Funcional.SelectedIndex = -1;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION:   Modo_Operacion
    /// DESCRIPCION :           Colocar la pagina con controles habilitados de acuerdo al modo
    /// PARAMETROS  :           Modo: Cadena que indica el modo de la operacion a realizar
    /// CREO        :           Noe Mosqueda Valadez
    /// FECHA_CREO  :           21/Junio/2012 19:00
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Modo_Operacion(String Modo)
    {
        //Declaracion de variables
        Boolean Habilitado = false; //variable que indica si los controles del formulario tienen que estar habilitados

        try
        {
            //Seleccionar el modo de operacion
            switch (Modo)
            {
                case "Inicial":
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = true;
                    Btn_Eliminar.Visible = true;
                    Btn_Salir.Visible = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    break;

                case "Nuevo":
                case "Modificar":
                    Habilitado = true;
                    Btn_Eliminar.Visible = false;
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";

                    //Verificar el caso
                    if (Modo == "Nuevo")
                    {
                        Limpiar_Controles();
                        Cmb_Estatus.SelectedIndex = 1;
                        Btn_Modificar.Visible = false;
                        Btn_Nuevo.Visible = true;
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                        Btn_Nuevo.ToolTip = "Guardar";
                    }
                    else
                    {
                        Btn_Modificar.Visible = true;
                        Btn_Nuevo.Visible = false;
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                        Btn_Modificar.ToolTip = "Aceptar";
                    }
                    break;

                default:
                    break;
            }

            //Habilitar/deshabilitar los controles
            Txt_Clave.Enabled = Habilitado;
            Txt_Comentarios.Enabled = Habilitado;
            Txt_Nombre.Enabled = Habilitado;
            Txt_Clave_Contabilidad.Enabled = Habilitado;
            Cmb_Estatus.Enabled = Habilitado;
            Cmb_Grupo_Dependencia.Enabled = Habilitado;
            Txt_Busqueda_Dependencia.Enabled = !Habilitado;
            Btn_Buscar_Dependencia.Enabled = !Habilitado;
            Grid_Unidades_Responsables.Enabled = !Habilitado;
            Cmb_Area_Funcional.Enabled = Habilitado;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION:   Alta_Unidad_Responsable
    /// DESCRIPCION :           Dar de alta una unidad responsable
    /// PARAMETROS  :           
    /// CREO        :           Noe Mosqueda Valadez
    /// FECHA_CREO  :           22/Junio/2012 18:00
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Alta_Unidad_Responsable()
    {
        //Declaracion de variables
        Cls_Cat_Unidades_Responsables_Negocio Unidades_Responsables_Negocio = new Cls_Cat_Unidades_Responsables_Negocio(); //vairable para la capa de negocios

        try
        {
            //Asignar propiedades
            Unidades_Responsables_Negocio.P_Clave = Txt_Clave.Text.Trim();
            
            //Verificar la longitud de los comentarios
            if (Txt_Comentarios.Text.Trim().Length > 250)
            {
                Unidades_Responsables_Negocio.P_Comentarios = Txt_Comentarios.Text.Trim().Substring(0, 250);
            }
            else
            {
                Unidades_Responsables_Negocio.P_Comentarios = Txt_Comentarios.Text.Trim();
            }

            Unidades_Responsables_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;

            //verificar si se tiene un grupo
            if (Cmb_Grupo_Dependencia.SelectedIndex > 0)
            {
                Unidades_Responsables_Negocio.P_Grupo_Dependencia_ID = Cmb_Grupo_Dependencia.SelectedItem.Value;
            }
            
            
            Unidades_Responsables_Negocio.P_Nombre = Txt_Nombre.Text.Trim();
            Unidades_Responsables_Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;
            Unidades_Responsables_Negocio.P_Clave_Contabilidad = Txt_Clave_Contabilidad.Text.Trim();
            Unidades_Responsables_Negocio.P_Area_Funcional_ID = Cmb_Area_Funcional.SelectedItem.Value.Trim();

            //Dar de alta la unidad responsable
            Unidades_Responsables_Negocio.Alta_Unidad_Responsable();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Cat&aacute;logo de Unidades Responsables", "alert('El alta de la Unidad Responsable fue Exitosa');", true);

            //Volver al estado inicial
            Estado_Inicial();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION:   Baja_Unidad_Responsable
    /// DESCRIPCION :           Dar de baja una unidad responsable
    /// PARAMETROS  :           Unidad_Responsable_ID: Cadena de texto que contiene el ID de la unidad responsable
    /// CREO        :           Noe Mosqueda Valadez
    /// FECHA_CREO  :           22/Junio/2012 19:00
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Baja_Unidad_Responsable(String Unidad_Responsable_ID)
    {
        //Declaracion de variables
        Cls_Cat_Unidades_Responsables_Negocio Unidades_Responsables_Negocio = new Cls_Cat_Unidades_Responsables_Negocio();

        try
        {
            //Asignar propiedades
            Unidades_Responsables_Negocio.P_Unidad_Responsable_ID = Unidad_Responsable_ID;

            //Dar de baja la unidad responsable
            Unidades_Responsables_Negocio.Baja_Unidad_Responsable();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Cat&aacute;logo de Unidades Responsables", "alert('La baja de la Unidad Responsable fue Exitosa');", true);

            //Volver al estado inicial
            Estado_Inicial();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION:   Cambio_Unidad_Responsable
    /// DESCRIPCION :           Modificar una unidad responsable
    /// PARAMETROS  :           
    /// CREO        :           Noe Mosqueda Valadez
    /// FECHA_CREO  :           22/Junio/2012 19:00
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Cambio_Unidad_Responsable()
    {
        //Declaracion de variables
        Cls_Cat_Unidades_Responsables_Negocio Unidades_Responsables_Negocio = new Cls_Cat_Unidades_Responsables_Negocio();

        try
        {
            //Asignar propiedades
            Unidades_Responsables_Negocio.P_Unidad_Responsable_ID = Txt_Unidad_Responsable_ID.Text.Trim();
            Unidades_Responsables_Negocio.P_Clave = Txt_Clave.Text.Trim();
            Unidades_Responsables_Negocio.P_Clave_Contabilidad = Txt_Clave_Contabilidad.Text.Trim();
            //Verificar la longitud de los comentarios
            if (Txt_Comentarios.Text.Trim().Length > 250)
            {
                Unidades_Responsables_Negocio.P_Comentarios = Txt_Comentarios.Text.Trim().Substring(0, 250);
            }
            else
            {
                Unidades_Responsables_Negocio.P_Comentarios = Txt_Comentarios.Text.Trim();
            }

            Unidades_Responsables_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;            

            //Verificar si hay un grupo dependencia
            if (Cmb_Grupo_Dependencia.SelectedIndex > 0)
            {
                Unidades_Responsables_Negocio.P_Grupo_Dependencia_ID = Cmb_Grupo_Dependencia.SelectedItem.Value;
            }

            Unidades_Responsables_Negocio.P_Nombre = Txt_Nombre.Text.Trim();
            Unidades_Responsables_Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;

            Unidades_Responsables_Negocio.P_Area_Funcional_ID = Cmb_Area_Funcional.SelectedItem.Value.Trim();

            //Modificar la unidad responsable
            Unidades_Responsables_Negocio.Cambio_Unidad_Responsable();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Cat&aacute;logo de Unidades Responsables", "alert('El cambio de la Unidad Responsable fue Exitosa');", true);

            //Volver al Estado inicial
            Estado_Inicial();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION:   Validacion_Campos
    /// DESCRIPCION :           Validar que esten todos los campos requeridos
    /// PARAMETROS  :           
    /// CREO        :           Noe Mosqueda Valadez
    /// FECHA_CREO  :           22/Junio/2012 17:30
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private String Validacion_Campos()
    {
        //Declaracion de variables
        String Resultado = String.Empty;
        String aux = String.Empty; //variable auxiliar para el mensaje

        try
        {
            //asignar el valor a la variable auxiliar
            aux = "Favor de ingresar todos los datos<br />" +
                "<table border='0'>" +
                "<tr>" +
                "<td align='left' class='estilo_fuente_mensaje_error'>+Clave</td>" +
                "<td align='left' class='estilo_fuente_mensaje_error'>+Nombre</td>" +
                "</tr>" +
                "<tr>" +
                "<td align='left' class='estilo_fuente_mensaje_error'>+Estatus</td>" +
                "<td align='left' class='estilo_fuente_mensaje_error'>+&Aacute;rea Funcional</td>" +
                "</tr>" +
                "</table>";

            //Verificar si se tienen todos los datos
            if (String.IsNullOrEmpty(Txt_Clave.Text.Trim()) == true)
            {
                Resultado = aux;
            }
            if (String.IsNullOrEmpty(Txt_Clave_Contabilidad.Text.Trim()) == true)
            {
                Resultado = aux;
            }

            if (String.IsNullOrEmpty(Txt_Nombre.Text.Trim()) == true)
            {
                Resultado = aux;
            }

            if (Cmb_Estatus.SelectedIndex <= 0)
            {
                Resultado = aux;
            }

          

            //Entregar resultado
            return Resultado;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION:   Validacion_Repetidos
    /// DESCRIPCION :           Valida que no existan valores repetidos antes de ingresar un nuevo elemento
    /// PARAMETROS  :           
    /// CREO        :           Noe Mosqueda Valadez
    /// FECHA_CREO  :           22/Junio/2012 17:45
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private String Validacion_Repetidos()
    {
        //Declaracion de variables
        Cls_Cat_Unidades_Responsables_Negocio Unidad_Responsable_Negocio = new Cls_Cat_Unidades_Responsables_Negocio(); //Variable para la capa de negocios
        String Resultado = String.Empty; //Variable para el resultado
                
        try
        {
            //Asignar propiedades
            if (String.IsNullOrEmpty(Txt_Unidad_Responsable_ID.Text.Trim()) == false)
            {
                Unidad_Responsable_Negocio.P_Unidad_Responsable_ID = Txt_Unidad_Responsable_ID.Text.Trim();
            }

            Unidad_Responsable_Negocio.P_Clave = Txt_Clave.Text.Trim();
            Unidad_Responsable_Negocio.P_Nombre = Txt_Nombre.Text.Trim();
            Unidad_Responsable_Negocio.P_Clave_Contabilidad = Txt_Clave_Contabilidad.Text.Trim();
            //Ejecutar consulta
            Resultado = Unidad_Responsable_Negocio.Valida_Unidad_Responsable();    
            
            //Entregar resultado
            return Resultado;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION:   Colocar_Datos
    /// DESCRIPCION :           Colocar los datos de una unidad responsable seleccionada
    /// PARAMETROS  :           Unidad_Responsable_ID: Cadena de texto que contiene el ID de la unidad responsable seleccionada
    /// CREO        :           Noe Mosqueda Valadez
    /// FECHA_CREO  :           22/Junio/2012 19:00
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Colocar_Datos(String Unidad_Responsable_ID)
    {
        //Declaracion de variables
        Cls_Cat_Unidades_Responsables_Negocio Unidad_Responsable_Negocio = new Cls_Cat_Unidades_Responsables_Negocio(); //Variable para la capa de negocios
        DataTable Dt_Unidad_Responsable = new DataTable(); //tabla para la consulta de la unidad responsable

        try
        {
            //Asignar consulta
            Unidad_Responsable_Negocio.P_Unidad_Responsable_ID = Unidad_Responsable_ID;
            Unidad_Responsable_Negocio.P_Tipo_Consulta = "Detalles";
            Dt_Unidad_Responsable = Unidad_Responsable_Negocio.Consulta_Unidades_Responsables();

            //verificar si la consulta arrojo resultados
            if (Dt_Unidad_Responsable.Rows.Count > 0)
            {
                //Colocar los datos en el formulario
                Txt_Unidad_Responsable_ID.Text = Unidad_Responsable_ID;
                Txt_Clave.Text = Dt_Unidad_Responsable.Rows[0]["Clave"].ToString().Trim();
                Txt_Nombre.Text = Dt_Unidad_Responsable.Rows[0]["Nombre"].ToString().Trim();
                Txt_Clave_Contabilidad.Text = Dt_Unidad_Responsable.Rows[0]["CLAVE_CONTABILIDAD"].ToString().Trim();
                
                if (Dt_Unidad_Responsable.Rows[0]["Comentarios"] != DBNull.Value)
                {
                    Txt_Comentarios.Text = Dt_Unidad_Responsable.Rows[0]["Comentarios"].ToString().Trim();
                }

                //Colocar los indices de los combos
                if (Dt_Unidad_Responsable.Rows[0]["Estatus"].ToString().Trim() == "ACTIVO")
                {
                    Cmb_Estatus.SelectedIndex = 1;
                }
                else
                {
                    Cmb_Estatus.SelectedIndex = 2;
                }


                if (Dt_Unidad_Responsable.Rows[0]["Grupo_Dependencia_ID"] != DBNull.Value)
                {
                    Cmb_Grupo_Dependencia.SelectedIndex = Cls_Util.Entrega_Indice_Combo(Cmb_Grupo_Dependencia, Dt_Unidad_Responsable.Rows[0]["Grupo_Dependencia_ID"].ToString().Trim());
                }
                else
                {
                    Cmb_Grupo_Dependencia.SelectedIndex = 0;
                }

                if (Dt_Unidad_Responsable.Rows[0][Cat_Dependencias.Campo_Area_Funcional_ID] != DBNull.Value)
                {
                    Cmb_Area_Funcional.SelectedIndex = Cmb_Area_Funcional.Items.IndexOf(Cmb_Area_Funcional.Items.FindByValue(Dt_Unidad_Responsable.Rows[0][Cat_Dependencias.Campo_Area_Funcional_ID].ToString()));
                }
                else 
                {
                    Cmb_Area_Funcional.SelectedIndex = -1;
                }

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }
    #endregion

    #region(Grid)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION:   Llena_Grid_Unidades_Responsables
    /// DESCRIPCION :           Llenar el Grid con las unidades responsables de acuerdo a un criterio de busqueda
    /// PARAMETROS  :           1. Busqueda: Cadena de texto que tiene el criterio de busqueda
    ///                         2. Pagina: Entero que contiene la pagina del Grid
    /// CREO        :           Noe Mosqueda Valadez
    /// FECHA_CREO  :           19/Junio/2012 17:45
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Llena_Grid_Unidades_Responsables(String Busqueda, int Pagina)
    {
        //Declaracion de variables
        Cls_Cat_Unidades_Responsables_Negocio Unidades_Responsables_Negocio = new Cls_Cat_Unidades_Responsables_Negocio(); //variable apra la capa de negocios
        DataTable Dt_Unidades_Responsables = new DataTable(); //tabla para el llenado del grid

        try
        {
            //Verificar si existe la variable de sesion
            if (HttpContext.Current.Session[P_Dt_Unidades_Responsables] == null)
            {
                //Ejecutar consulta
                Unidades_Responsables_Negocio.P_Tipo_Consulta = "General";
                Unidades_Responsables_Negocio.P_Busqueda = Busqueda;
                Dt_Unidades_Responsables = Unidades_Responsables_Negocio.Consulta_Unidades_Responsables();
            }
            else
            {
                //Colocar la variable de sesione n la tabla
                Dt_Unidades_Responsables = ((DataTable)HttpContext.Current.Session[P_Dt_Unidades_Responsables]);
            }

            //Llenar el Grid
            Grid_Unidades_Responsables.DataSource = Dt_Unidades_Responsables;

            if (Pagina > -1)
            {
                Grid_Unidades_Responsables.PageIndex = Pagina;
            }

            Grid_Unidades_Responsables.Columns[5].Visible = true;
            Grid_Unidades_Responsables.Columns[6].Visible = true;
            Grid_Unidades_Responsables.DataBind();
            Grid_Unidades_Responsables.Columns[5].Visible = false;
            Grid_Unidades_Responsables.Columns[6].Visible = false;

            //Colocar table en variable de sesion
            HttpContext.Current.Session[P_Dt_Unidades_Responsables] = Dt_Unidades_Responsables;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    protected void Grid_Unidades_Responsables_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Llena_Grid_Unidades_Responsables("", e.NewPageIndex);
        }
        catch (Exception ex)
        {
            Mostrar_Mensaje_Error(true, "Error: (Grid_Unidades_Responsables_PageIndexChanging)" + ex.Message);
        }
    }

    protected void Grid_Unidades_Responsables_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //verificar si se ha seleccionado un indice
            if (Grid_Unidades_Responsables.SelectedIndex > -1)
            {
                Colocar_Datos(Grid_Unidades_Responsables.SelectedRow.Cells[5].Text.Trim());
            }
        }
        catch (Exception ex)
        {
            Mostrar_Mensaje_Error(true, "Error: (Grid_Unidades_Responsables_SelectedIndexChanged)" + ex.Message);
        }
    }

    #endregion

    #region(Eventos)
    protected void Btn_Buscar_Dependencia_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            //Eliminar la sesion
            Elimina_Sesiones();

            //llenar el grid
            Llena_Grid_Unidades_Responsables(Txt_Busqueda_Dependencia.Text.Trim(), -1);
        }
        catch (Exception ex)
        {
            Mostrar_Mensaje_Error(true, "Error: (Btn_Buscar_Dependencia_Click)" + ex.Message);
        }
    }

    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            //Verificar el tooltip del boton
            if (Btn_Salir.ToolTip == "Cancelar")
            {
                Estado_Inicial();
            }
            else
            {
                Elimina_Sesiones();
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception ex)
        {
            Mostrar_Mensaje_Error(true, "Error: (Btn_Salir_Click)" + ex.Message);
        }
    }

    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        //Declaracion de variables
        String Validacion = String.Empty; //variable para la validacion

        try
        {
            //verificar el tooltip del boton
            if (Btn_Nuevo.ToolTip == "Nuevo")
            {
                Modo_Operacion("Nuevo");
            }
            else
            {
                //Validar si esta toto bien
                Validacion = Validacion_Campos();
                if (String.IsNullOrEmpty(Validacion) == true)
                {
                    //Validar si la unidad responsable ya existe
                    Validacion = Validacion_Repetidos();
                    if (String.IsNullOrEmpty(Validacion) == true)
                    {
                        Alta_Unidad_Responsable();
                    }
                    else
                    {
                        Mostrar_Mensaje_Error(true, Validacion);
                    }
                }
                else
                {
                    Mostrar_Mensaje_Error(true, Validacion);
                }
            }
        }
        catch (Exception ex)
        {
            Mostrar_Mensaje_Error(true, "Error: (Btn_Nuevo_Click)" + ex.Message);
        }
    }

    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        //Declaracion de variables
        String Validacion = String.Empty; //variable para la validacion

        try
        {
            //Veriricar el tooltip del boton
            if (Btn_Modificar.ToolTip == "Modificar")
            {
                //Verificar si se ha seleccionado un elemento
                if (Grid_Unidades_Responsables.SelectedIndex > -1)
                {
                    Modo_Operacion("Modificar");
                }
                else
                {
                    Mostrar_Mensaje_Error(true, "Favor de seleccionar el elemento a modificar");
                }
            }
            else
            {
                //Validar si esta toto bien
                Validacion = Validacion_Campos();
                if (String.IsNullOrEmpty(Validacion) == true)
                {
                    //Validar si la unidad responsable ya existe
                    Validacion = Validacion_Repetidos();
                    if (String.IsNullOrEmpty(Validacion) == true)
                    {
                        Cambio_Unidad_Responsable();
                    }
                    else
                    {
                        Mostrar_Mensaje_Error(true, Validacion);
                    }
                }
                else
                {
                    Mostrar_Mensaje_Error(true, Validacion);
                }
            }
        }
        catch (Exception ex)
        {
            Mostrar_Mensaje_Error(true, "Error: (Btn_Modificar_Click)" + ex.Message);
        }
    }

    protected void Btn_Eliminar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            //Verificar si se ha seleccionado un elemento
            if (Grid_Unidades_Responsables.SelectedIndex > -1)
            {
                Baja_Unidad_Responsable(Txt_Unidad_Responsable_ID.Text.Trim());
            }
            else
            {
                Mostrar_Mensaje_Error(true, "Favor de seleccionar el elemento a modificar");
            }
        }
        catch (Exception ex)
        {
            Mostrar_Mensaje_Error(true, "Error: (Btn_Eliminar_Click)" + ex.Message);
        }
    }

    #endregion

    #region(Area Funcional)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION : Llenar_Area_Funcional
    /// DESCRIPCION          : Metodo para llenar el combo de las areas funcionales del catalogo
    /// PARAMETROS           :           
    /// CREO                 : Leslie González Vázquez
    /// FECHA_CREO           : 05/Agosto/2013
    ///*******************************************************************************
    public void Llenar_Area_Funcional() 
    {
        Cls_Cat_Unidades_Responsables_Negocio Negocio = new Cls_Cat_Unidades_Responsables_Negocio();
        DataTable Dt_Registros = new DataTable();

        try
        {
            Cmb_Area_Funcional.Items.Clear();

            Dt_Registros = Negocio.Consultar_Area_Funcional();
            
            if (Dt_Registros != null)
            {
                if (Dt_Registros.Rows.Count > 0)
                {
                    Cmb_Area_Funcional.DataValueField = Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID;
                    Cmb_Area_Funcional.DataTextField = "CLAVE_NOMBRE";
                    Cmb_Area_Funcional.DataSource = Dt_Registros;
                    Cmb_Area_Funcional.DataBind();
                }
            }

            Cmb_Area_Funcional.Items.Insert(0, new ListItem("Seleccione", ""));
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al llenar el combo de las Areas Funcinales. Error[" + Ex.Message + "]");
        }
    }
    #endregion
}