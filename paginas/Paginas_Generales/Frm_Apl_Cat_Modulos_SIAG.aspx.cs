﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using JAPAMI.Modulos.Negocio;
using JAPAMI.Sessiones;

public partial class paginas_Paginas_Generales_Frm_Apl_Cat_Modulos_SIAG : System.Web.UI.Page
{
    #region(Eventos)
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Validan que exista una session activa al ingresar a la página de antiguedad sindicatos.
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

            if (!IsPostBack)
            {
                Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                ViewState["SortDirection"] = "ASC";
            }
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (Btn_Nuevo.ToolTip == "Nuevo")
            {
                Habilitar_Controles("Nuevo"); //Habilita los controles para la introducción de datos por parte del usuario
                Limpia_Controles();           //Limpia los controles de la forma para poder introducir nuevos datos
            }
            else
            {
                //Si todos los campos requeridos fueron proporcionados por el usuario entonces da de alta los mismo en la base de datos
                if (Txt_Nombre_Modulo.Text != "")
                {
                    Alta_Modulo(); //Da de alta los datos proporcionados por el usuario
                }
                //Si faltaron campos por capturar envia un mensaje al usuario indicando que campos faltaron de proporcionar
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
                    if (Txt_Nombre_Modulo.Text == "") Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + El Nombre del Modulo <br>";
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Btn_Buscar_Modulos_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Consulta_Modulos();    //Consulta los Menus que coincidan con la /// DESCRIPCION porporcionada por el usuario
            //Limpia_Controles();  //Limpia los controles de la forma
            //Si no se encontraron Menus con una /// DESCRIPCION similar a la proporcionada por el usuario entonces manda un mensaje al usuario
            if (Grid_Modulos.Rows.Count <= 0)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontraron Modulos con el /// NOMBRE proporcionado <br>";
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }

    }
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (Btn_Modificar.ToolTip == "Modificar")
            {
                if (Txt_Modulo_ID.Text != "")
                {
                    Habilitar_Controles("Modificar"); //Habilita los controles para la modificación de los datos
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Seleccione el Modulo que desea modificar sus datos <br>";
                }
            }
            else
            {
                //Si todos los campos requeridos fueron proporcionados por el usuario entonces modifica estos en la BD
                if (Txt_Nombre_Modulo.Text != "")
                {
                    Modificar_Modulo(); //Modifica los datos del Menu con los datos proporcionados por el usuario
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
                    Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + El nombre del modulo";
                    return;
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Btn_Eliminar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            //Si el usuario selecciono un Menu entonces lo elimina de la base de datos
            if (Txt_Modulo_ID.Text != "")
            {
                Eliminar_Modulo(); //Elimina el Menu que fue seleccionado por el usuario
            }
            //Si el usuario no selecciono algún Menu manda un mensaje indicando que es necesario que seleccione algun para
            //poder eliminar
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Seleccione el Modulo que desea eliminar <br>";
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }

    }
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Salir.ToolTip == "Salir")
            {
                Session.Remove("Consulta_Modulos");
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Inicializa_Controles();//Habilita los controles para la siguiente operación del usuario en el catálogo
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Menus_SelectedIndexChanged
    /// DESCRIPCION : Consulta los datos del Menú que selecciono el usuario
    /// PARAMETROS  : 
    /// CREO        : Juan Alberto Hernández Negrete
    /// FECHA_CREO  : 27/Abril/2011
    /// MODIFICO          :Fernando Gonzalez B
    /// FECHA_MODIFICO    :07/Mayo/2012
    /// CAUSA_MODIFICACION:se adecuo los resultados al formulario actual.
    ///*******************************************************************************
    protected void Grid_Modulos_SelectedIndexChanged(object sender, EventArgs e)
    {
        String Modulo_ID = String.Empty;
        Cls_Apl_Cat_Modulos_SIAG_Negocios Obj_Modulos = new Cls_Apl_Cat_Modulos_SIAG_Negocios();
        DataTable Dt_Modulos = null;

        try
        {
            Modulo_ID = Grid_Modulos.SelectedRow.Cells[2].Text;

            if (!String.IsNullOrEmpty(Modulo_ID))
            {
                Obj_Modulos.P_Modulo_ID = Modulo_ID.ToString();
                Dt_Modulos = Obj_Modulos.Consulta_Modulo();

                if (Dt_Modulos is DataTable)
                {
                    if (Dt_Modulos.Rows.Count > 0)
                    {
                        foreach (DataRow _Modulo in Dt_Modulos.Rows)
                        {
                            if (_Modulo is DataRow)
                            {
                                if (!String.IsNullOrEmpty(_Modulo[Apl_Cat_Modulos_Siag.Campo_Modulo_ID].ToString()))
                                    Txt_Modulo_ID.Text = _Modulo[Apl_Cat_Modulos_Siag.Campo_Modulo_ID].ToString();
                                else Txt_Modulo_ID.Text = String.Empty;

                                if (!String.IsNullOrEmpty(_Modulo[Apl_Cat_Modulos_Siag.Campo_Nombre].ToString()))
                                    Txt_Nombre_Modulo.Text = _Modulo[Apl_Cat_Modulos_Siag.Campo_Nombre].ToString();
                                else Txt_Nombre_Modulo.Text = String.Empty;

                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion
    #region (Funciones)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Alta_Menu
    /// DESCRIPCION : Da de Alta el Menu con los datos proporcionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Fernando Gonzalez
    /// FECHA_CREO  : 05-Mayo-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Alta_Modulo()
    {
        Cls_Apl_Cat_Modulos_SIAG_Negocios Rs_Apl_Cat_Modulos = new Cls_Apl_Cat_Modulos_SIAG_Negocios(); //Variable de conexión hacia la capa de negocios para envio de los datos a dar de alta
        try
        {
            Rs_Apl_Cat_Modulos.P_Modulo_ID = Txt_Modulo_ID.Text;
            Rs_Apl_Cat_Modulos.P_Nombre = Txt_Nombre_Modulo.Text;
            Rs_Apl_Cat_Modulos.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
            Rs_Apl_Cat_Modulos.Alta_Modulo(); //Da de alta los datos del Menu proporcionados por el usuario en la BD
            Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Modulos del Sistema", "alert('El Alta del Modulo fue Exitosa');", true);
        }
        catch (Exception ex)
        {
            throw new Exception("Alta_Modulo " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Modificar_Modulo
    /// DESCRIPCION : Modifica los datos del Modulo con los proporcionados por el usuario en la BD
    /// PARAMETROS  : 
    /// CREO        : Fernando Gonzalez Bautista
    /// FECHA_CREO  : 07-mayo-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Modificar_Modulo()
    {
        Cls_Apl_Cat_Modulos_SIAG_Negocios Rs_Modificar_Apl_Cat_Modulos = new Cls_Apl_Cat_Modulos_SIAG_Negocios(); //Variable de conexión hacia la capa de Negocios para envio de datos a modificar
        try
        {
            Rs_Modificar_Apl_Cat_Modulos.P_Modulo_ID = Txt_Modulo_ID.Text;
            Rs_Modificar_Apl_Cat_Modulos.P_Nombre = Txt_Nombre_Modulo.Text;
            Rs_Modificar_Apl_Cat_Modulos.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
            Rs_Modificar_Apl_Cat_Modulos.Modificar_Modulo(); //Sustituye los datos que se encuentran en la BD por lo que introdujo el usuario
            Consulta_Modulos();
            Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Modulos", "alert('La Modificación del Modulo fue Exitosa');", true);
        }
        catch (Exception ex)
        {
            throw new Exception("Modificar_Modulo " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Eliminar_Modulo
    /// DESCRIPCION : Elimina los datos del Modulo que fue seleccionado por el Usuario
    /// PARAMETROS  : 
    /// CREO        : Fernando Gonzalez B
    /// FECHA_CREO  : 07-mayo-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Eliminar_Modulo()
    {
        Cls_Apl_Cat_Modulos_SIAG_Negocios Rs_Eliminar_Apl_Cat_Modulo = new Cls_Apl_Cat_Modulos_SIAG_Negocios(); //Variable de conexión hacia la capa de Negocios para la eliminación de los datos
        try
        {
            Rs_Eliminar_Apl_Cat_Modulo.P_Modulo_ID = Txt_Modulo_ID.Text;
            Rs_Eliminar_Apl_Cat_Modulo.Eliminar_Modulo(); //Elimina el Menu que selecciono el usuario de la BD
            Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Modulos", "alert('La Eliminación del Modulo fue Exitosa');", true);
        }
        catch (Exception ex)
        {
            throw new Exception("Eliminar_Modulo " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Inicializa_Controles
    /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda realizar
    ///               diferentes operaciones
    /// PARAMETROS  : 
    /// CREO        : Fernando Gonzalez B.
    /// FECHA_CREO  : 05-Mayo-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Inicializa_Controles()
    {
        try
        {
            Habilitar_Controles("Inicial"); //Habilita los controles de la forma para que el usuario pueda indica que operación desea realizar
            Limpia_Controles();             //Limpia los controles del forma
            Consulta_Modulos();             //Consulta todos los menus y submenus que estan dados de alta
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message.ToString());
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Controles
    /// DESCRIPCION : Limpia los controles que se encuentran en la forma
    /// PARAMETROS  : 
    /// CREO        : Fernando Gonzalez B
    /// FECHA_CREO  : 04-Mayo-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Limpia_Controles()
    {
        try
        {
            Txt_Modulo_ID.Text = "";
            Txt_Nombre_Modulo.Text = "";
            Txt_Busqueda_Modulos.Text = "";
            Grid_Modulos.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception("Limpiar_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Habilitar_Controles
    /// DESCRIPCION : Habilita y Deshabilita los controles de la forma para preparar la página
    ///               para la siguiente operación
    /// PARAMETROS  : Operacion: Indica la operación que se desea realizar por parte del usuario
    ///                          si es una alta, modificacion
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 14-Septiembre-2010
    /// MODIFICO          :Fernando Gonzalez
    /// FECHA_MODIFICO    :05/Mayo/2012
    /// CAUSA_MODIFICACION:Sobraban algunos campos que en este formulario no se ocupan
    ///*******************************************************************************
    private void Habilitar_Controles(String Operacion)
    {
        Boolean Habilitado; ///Indica si el control de la forma va hacer habilitado para utilización del usuario

        try
        {
            Habilitado = false;
            switch (Operacion)
            {
                case "Inicial":
                    Habilitado = false;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = true;
                    Btn_Eliminar.Visible = true;
                    Btn_Nuevo.CausesValidation = false;
                    Btn_Modificar.CausesValidation = false;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";

                    Configuracion_Acceso("Frm_Apl_Cat_Modulos_SIAG.aspx");
                    break;

                case "Nuevo":
                    Habilitado = true;
                    Btn_Nuevo.ToolTip = "Dar de Alta";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = false;
                    Btn_Eliminar.Visible = false;
                    Btn_Nuevo.CausesValidation = true;
                    Btn_Modificar.CausesValidation = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    break;

                case "Modificar":
                    Habilitado = true;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Nuevo.Visible = false;
                    Btn_Modificar.Visible = true;
                    Btn_Eliminar.Visible = false;
                    Btn_Nuevo.CausesValidation = true;
                    Btn_Modificar.CausesValidation = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                    break;
            }
            Txt_Nombre_Modulo.Enabled = Habilitado;
            Txt_Busqueda_Modulos.Enabled = !Habilitado;
            Btn_Buscar_Modulos.Enabled = !Habilitado;
            Grid_Modulos.Enabled = !Habilitado;
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
        }
        catch (Exception ex)
        {
            throw new Exception("Habilitar_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Modulos
    /// DESCRIPCION : Consulta los Modulos de la BD
    /// PARAMETROS  : 
    /// CREO        : Fernando Gonzalez B
    /// FECHA_CREO  : 04-Mayo-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consulta_Modulos()
    {
        DataTable Dt_Modulos;   //Variable que obtendra los datos de la consulta 
        Cls_Apl_Cat_Modulos_SIAG_Negocios Rs_Consulta_Apl_Cat_Modulos = new Cls_Apl_Cat_Modulos_SIAG_Negocios(); //Variable de conexión hacia la capa de Negocios        
        //Int32 totalPages = 0;
        try
        {
            if (Txt_Busqueda_Modulos.Text != "")
            {
                Rs_Consulta_Apl_Cat_Modulos.P_Nombre = Txt_Busqueda_Modulos.Text;
                Dt_Modulos = Rs_Consulta_Apl_Cat_Modulos.Consulta_Modulo_By_Name(); //Consulta todos los Menus que coindican con lo proporcionado por el usuario
                Grid_Modulos.DataSource = Dt_Modulos;
                Grid_Modulos.DataBind();
            }
            else
            {
                Dt_Modulos = Rs_Consulta_Apl_Cat_Modulos.Consulta_Modulo(); //Consulta todos los Menus que coindican con lo proporcionado por el usuario
                Grid_Modulos.DataSource = Dt_Modulos;
                Grid_Modulos.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Consulta_Modulos " + ex.Message.ToString(), ex);
        }
    }
    #endregion

    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// 
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// 
    /// PARÁMETROS: No Áplica.
    /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
    /// FECHA CREÓ: 23/Mayo/2011 10:43 a.m.
    /// USUARIO MODIFICO:Fernando Gonzalez B        
    /// FECHA MODIFICO:07/Mayo/2012
    /// CAUSA MODIFICACIÓN:se adecuo a la pagina del catalago de modulos. 
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.       
        DataRow[] Dr_Modulos= null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            Botones.Add(Btn_Nuevo);
            Botones.Add(Btn_Modificar);
            Botones.Add(Btn_Eliminar);
            Botones.Add(Btn_Buscar_Modulos);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Modulos = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Modulos.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Modulos[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Modulos[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 29/Noviembre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion

}

