﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;

public partial class paginas_Paginas_Generales_Frm_Ejecutar_Query : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }




    protected void Btn_Ejecutar_Query_Click(object sender, EventArgs e)
    {
        if (Txt_Sentencia.Text.Trim() != String.Empty)
        {
            String Mi_SQL = Txt_Sentencia.Text;
            try
            {
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Ejecutar Query", "alert('Se ejecuto con exito la sentencia Oracle');", true);
            }
            catch (Exception Ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Ejecutar Query", "alert('No se pudo ejecuto con exito la sentencia Oracle genero el problema:"+ Ex.Message +"');", true); 
            }
        }
    }
}
