﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JAPAMI.Alertas.Negocio;
using System.Data;

public partial class paginas_Paginas_Generales_Frm_Ope_Alertas_Sistema : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Cls_Ope_Alertas_Sistema_Negocio Datos = new Cls_Ope_Alertas_Sistema_Negocio();
        //Consultamos las Requisiciones
        DataTable Dt_Alertas = Datos.Consultar_Requisiciones_Entregadas();
        if (Dt_Alertas.Rows.Count == 0)
        {
            string script = "Cerrar_Ventana_Modal();";

            ScriptManager.RegisterStartupScript(this, typeof(Page), "Cerrar_Ventana_Modal", script, true);
        }
        else
        {
            Grid_Alertas.DataSource = Dt_Alertas;
            Grid_Alertas.DataBind();
        }

    }
}
