﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Seguimiento_Ordenes_Compra_Taller.Negocio;
using JAPAMI.Taller_Mecanico.Listado_Ordenes_Compra.Negocio;
using JAPAMI.Taller_Mecanico.Reporte_Solicitud_Servicio.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Orden_Compra.Negocio;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;
using JAPAMI.Reportes;

public partial class paginas_Compras_Frm_Ope_Tal_Seguimiento_Ordenes_Compra : System.Web.UI.Page
{

    #region PAGE LOAD / INIT

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Page_Load
    ///DESCRIPCIÓN:          Pagina Inicial
    ///PARAMETROS:            
    ///CREO:                 Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO:           05/Septiembre/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        Lbl_Informacion.Visible = false;
        if (String.IsNullOrEmpty(Cls_Sessiones.Nombre_Empleado)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        if (!IsPostBack)
        {
            Habilitar_Components(true);
            Txt_Fecha_Inicial.Text = String.Format("{0:dd/MMM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 1));
            Txt_Fecha_Final.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Now);
            Llenar_Listado_Ordenes_Compra();
        }
    }

    #endregion

    #region METODOS

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Ordenes_Compra
    ///DESCRIPCIÓN:          Llena el Listado de las Ordenes de Compra
    ///PARAMETROS:            
    ///CREO:                 Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO:           05/Septiembre/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_Listado_Ordenes_Compra()
    {
        Limpiar_Formulario();
        Cls_Ope_Tal_Seguimiento_Ordenes_Compra_Taller_Negocio Seguimiento_Negocio = new Cls_Ope_Tal_Seguimiento_Ordenes_Compra_Taller_Negocio();
        Seguimiento_Negocio.P_No_OC = Txt_Busqueda.Text.Trim();
        Seguimiento_Negocio.P_Fecha_Inicio = Convert.ToDateTime(Txt_Fecha_Inicial.Text);
        Seguimiento_Negocio.P_Fecha_Fin = Convert.ToDateTime(Txt_Fecha_Final.Text);
        DataTable Dt_Datos = Seguimiento_Negocio.Consultar_Listado_Ordenes_Compra();
        Grid_Requisiciones.DataSource = Dt_Datos;
        Grid_Requisiciones.DataBind();
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
    ///DESCRIPCIÓN:          Carga el data set físico con el cual se genera el Reporte especificado
    ///PARAMETROS:           1.- Dt_Cabecera.- Contiene la informacion general de la orden de compra
    ///                      2.- Dt_Detalles.- Contiene los productos de la orden de compra
    ///                      3.- Ds_Recibo.- Objeto que contiene la instancia del Data set fisico del Reporte a generar
    ///CREO:                 Salvador Hernández Ramírez
    ///FECHA_CREO:           19/Abril/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Generar_Reporte(DataTable Dt_Cabecera, DataTable Dt_Detalles, DataTable Dt_Servicio, DataSet Ds_Reporte, String Formato)
    {

        DataRow Renglon;
        String Ruta_Reporte_Crystal = "";
        String Nombre_Reporte_Generar = "";

        DataTable Dt_Dir;
        Cls_Ope_Tal_Listado_Ordenes_Compra_Negocio Listado = new Cls_Ope_Tal_Listado_Ordenes_Compra_Negocio();
        Dt_Dir = Listado.Consulta_Directores();

        if (Dt_Dir.Rows.Count > 0)
        {
            Dt_Cabecera.Columns.Add("DIRECTOR", typeof(String));
            Dt_Cabecera.Columns.Add("COORDINADOR", typeof(String));
            Dt_Cabecera.Columns.Add("ALMACEN", typeof(String));

            Dt_Cabecera.Rows[0]["DIRECTOR"] = Dt_Dir.Rows[0]["DIRECTOR"];
            Dt_Cabecera.Rows[0]["COORDINADOR"] = Dt_Dir.Rows[0]["COORDINADOR"];
            Dt_Cabecera.Rows[0]["ALMACEN"] = Dt_Dir.Rows[0]["ALMACEN"];

            Dt_Cabecera.Columns.Remove("DEPENDENCIA_ID");
            Dt_Cabecera.Columns.Remove("COMENT");

            Renglon = Dt_Cabecera.Rows[0];
            Ds_Reporte.Tables[0].ImportRow(Renglon);
            Renglon = Dt_Servicio.Rows[0];
            Ds_Reporte.Tables[3].ImportRow(Renglon);
            String Folio = Dt_Cabecera.Rows[0]["FOLIO"].ToString();

            for (int Cont_Elementos = 0; Cont_Elementos < Dt_Detalles.Rows.Count; Cont_Elementos++)
            {
                Renglon = Dt_Detalles.Rows[Cont_Elementos]; //Instanciar renglon e importarlo
                Ds_Reporte.Tables[1].ImportRow(Renglon);
                Ds_Reporte.Tables[1].Rows[Cont_Elementos].SetField("FOLIO", Folio);
            }

            Dt_Cabecera.TableName = "Cabecera";
            Dt_Detalles.TableName = "Dt_Detalles";

            DataSet Ds_1 = new DataSet();
            Ds_1.Tables.Add(Dt_Cabecera.Copy());
            Ds_1.Tables.Add(Dt_Detalles.Copy());

            DataSet Ds_Datos = Ds_Reporte;
            Ds_Datos.Tables.Remove("DT_LOGOS");
            Ds_Datos.Tables.Add(new Cls_Tal_Parametros_Negocio().Obtener_Tabla_Reporte(this.Server));
            Ruta_Reporte_Crystal = "../Rpt/Servicios_Generales/Rpt_Tal_Orden_Compra.rpt";

            // Se crea el nombre del reporte
            String Nombre_Reporte = "Rpt_Tal_Orden_Compra_" + Cls_Sessiones.No_Empleado + "_" + Convert.ToString(DateTime.Now.ToString("yyyy'-'MM'-'dd'_t'HH'-'mm'-'ss"));

            // Se da el nombre del reporte que se va generar
            if (Formato == "PDF")
                Nombre_Reporte_Generar = Nombre_Reporte + ".pdf";  // Es el nombre del reporte PDF que se va a generar
            else if (Formato == "Excel")
                Nombre_Reporte_Generar = Nombre_Reporte + ".xls";  // Es el nombre del repote en Excel que se va a generar

            Cls_Reportes Reportes = new Cls_Reportes();
            Reportes.Generar_Reporte(ref Ds_Datos, Ruta_Reporte_Crystal, Nombre_Reporte_Generar, Formato);
            Mostrar_Reporte(Nombre_Reporte_Generar, Formato);
        }
        else
        {
            throw new Exception("Error al Intentar consultar los Directores que autorizarán la Orden de compra ");
        }
    }

    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
    /// USUARIO CREO:        Juan Alberto Hernández Negrete.
    /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:    Salvador Hernández Ramírez
    /// FECHA MODIFICO:      16-Mayo-2011
    /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
    {
        String Pagina = "../../Reporte/";

        try
        {
            if (Formato == "PDF")
            {
                Pagina = Pagina + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            else if (Formato == "Excel")
            {
                String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
    ///DESCRIPCIÓN:          Limpia el formulario
    ///PARAMETROS:            
    ///CREO:                 Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO:           05/Septiembre/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Limpiar_Formulario()
    {
        Txt_Folio.Text = "";
        Txt_Dependencia.Text = "";
        Txt_Proveedor.Text = "";
        Grid_Productos_Servicios.DataSource = new DataTable();
        Grid_Productos_Servicios.DataBind();
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Habilitar_Components
    ///DESCRIPCIÓN:          Habilita o inhabilita los Componentes.
    ///PARAMETROS:            
    ///CREO:                 Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO:           05/Septiembre/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Habilitar_Components(Boolean Habilitar_Listado)
    {
        Div_Listado_Requisiciones.Visible = Habilitar_Listado;
        Btn_Salir.Visible = Habilitar_Listado;
        Div_Contenido.Visible = !Habilitar_Listado;
        Btn_Listar_Requisiciones.Visible = !Habilitar_Listado;
    }

    #endregion

    #region EVENTOS

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Click
    ///DESCRIPCIÓN:          Ejecuta una nueva busqueda para llenar el listado de las OC
    ///PARAMETROS:            
    ///CREO:                 Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO:           05/Septiembre/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        Llenar_Listado_Ordenes_Compra();
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Seleccionar_OC_Click
    ///DESCRIPCIÓN:          Seleccionar una Orden de Compra
    ///PARAMETROS:            
    ///CREO:                 Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO:           05/Septiembre/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Seleccionar_OC_Click(object sender, ImageClickEventArgs e)
    {
        String No_OC = ((ImageButton)sender).CommandArgument.Trim();
        Limpiar_Formulario();
        Cls_Ope_Tal_Seguimiento_Ordenes_Compra_Taller_Negocio Negocio = new Cls_Ope_Tal_Seguimiento_Ordenes_Compra_Taller_Negocio();
        Negocio.P_No_OC = No_OC.Trim();
        DataTable Dt_Datos = Negocio.Consultar_Listado_Ordenes_Compra();
        if (Dt_Datos != null)
        {
            if (Dt_Datos.Rows.Count > 0)
            {
                Txt_Folio.Text = Dt_Datos.Rows[0]["FOLIO"].ToString().Trim();
                Txt_Dependencia.Text = Dt_Datos.Rows[0]["DEPENDENCIA"].ToString().Trim();
                Txt_Proveedor.Text = Dt_Datos.Rows[0]["NOMBRE_PROVEEDOR"].ToString().Trim();
                Dt_Datos = Negocio.Consultar_Historial_Ordenes_Compra();
                if (Dt_Datos != null)
                {
                    if (Dt_Datos.Rows.Count > 0)
                    {
                        Grid_Productos_Servicios.DataSource = Dt_Datos;
                        Grid_Productos_Servicios.DataBind();
                    }
                }
                Habilitar_Components(false);
            }
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Imprimir_OC_Click
    ///DESCRIPCIÓN:          Imprimir una Orden de Compra
    ///PARAMETROS:            
    ///CREO:                 Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO:           05/Septiembre/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Imprimir_OC_Click(object sender, ImageClickEventArgs e)
    {
        DataTable Dt_Cabecera_OC = new DataTable();
        DataTable Dt_Detalles_OC = new DataTable();
        ImageButton Btn_Imprimir_Orden_Compra = null;
        String Formato = "PDF";
        DataTable Dt_Solicitud = new DataTable();
        Cls_Ope_Tal_Listado_Ordenes_Compra_Negocio Listado_Negocio = new Cls_Ope_Tal_Listado_Ordenes_Compra_Negocio();
        try
        {
            Btn_Imprimir_Orden_Compra = (ImageButton)sender;
            String No_Orden_Compra = Btn_Imprimir_Orden_Compra.CommandArgument;
            if (!String.IsNullOrEmpty(No_Orden_Compra))
            {
                Listado_Negocio.P_No_Orden_Compra = No_Orden_Compra.Trim();
                Dt_Cabecera_OC = Listado_Negocio.Consulta_Cabecera_Orden_Compra();
                Dt_Detalles_OC = Listado_Negocio.Consulta_Detalles_Orden_Compra();
                Ds_Rpt_Tal_Orden_Compra Ds_Orden_Compra = new Ds_Rpt_Tal_Orden_Compra();
                if (!String.IsNullOrEmpty(Dt_Cabecera_OC.Rows[0][Ope_Tal_Requisiciones.Campo_No_Solicitud].ToString()))
                {
                    Cls_Rpt_Tal_Solicitud_Servicio_Negocio Rpt = new Cls_Rpt_Tal_Solicitud_Servicio_Negocio();
                    Rpt.P_No_Solicitud = Convert.ToInt32(Dt_Cabecera_OC.Rows[0][Ope_Tal_Requisiciones.Campo_No_Solicitud].ToString());
                    Rpt.P_Tipo_Bien = Dt_Cabecera_OC.Rows[0]["TIPO_BIEN"].ToString();
                    Dt_Solicitud = Rpt.Consulta_Datos_Solicitud();
                }
                else
                {
                    Dt_Solicitud = Ds_Orden_Compra.Tables["DT_SERVICIO"].Clone();
                }
                Dt_Solicitud.TableName = "DT_SERVICIO";
                Generar_Reporte(Dt_Cabecera_OC, Dt_Detalles_OC, Dt_Solicitud, Ds_Orden_Compra, Formato);
            }
        }
        catch (Exception ex)
        {
            Lbl_Informacion.Visible = true;
            Lbl_Informacion.Text = (ex.Message);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN:          Sale de la pantalla.
    ///PARAMETROS:            
    ///CREO:                 Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO:           05/Septiembre/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e) {
        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Listar_Requisiciones_Click
    ///DESCRIPCIÓN:          Sale de la pantalla.
    ///PARAMETROS:            
    ///CREO:                 Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO:           05/Septiembre/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Listar_Requisiciones_Click(object sender, ImageClickEventArgs e)
    {
        Limpiar_Formulario();
        Habilitar_Components(true);
    }

    #endregion

}