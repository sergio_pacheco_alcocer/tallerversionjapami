<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Tal_Asignacion_Proveedor_Verificacion.aspx.cs" Inherits="paginas_Taller_Mecanico_Frm_Ope_Asignacion_Proveedor_Servicio"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style type="text/css">
        .Tabla_Comentarios
        {
            border-collapse: collapse;
            margin-left: 25px;
            color: red;
            font-family: Verdana,Geneva,MS Sans Serif;
            font-size: small;
            text-align: left;
        }
        .Tabla_Comentarios th, .Tabla_Comentarios td
        {
            border: 1px solid #999999;
            padding: 2px 10px;
        }
        .Tabla_Comentarios_Head
        {
            border-collapse: collapse;
            margin-left: 25px;
            color:#000000;
            background-color:#D6EBFF;
            font-family: Verdana,Geneva,MS Sans Serif;
            font-size: small;
            text-align: left;
        }   
        .style1
        {
            width: 20%;
            height: 48px;
        }
        .style2
        {
            width: 30%;
            height: 48px;
        }
        .style3
        {
            height: 48px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <script type="text/javascript">
    window.onerror = new Function("return true");
    //Abrir una ventana modal
    function Abrir_Ventana_Modal(Url, Propiedades) {
        window.showModalDialog(Url, null, Propiedades);
    }
    function Validar_Longitud_Texto(Text_Box, Max_Longitud) {
        if (Text_Box.value.length > Max_Longitud) {
            Text_Box.value = Text_Box.value.substring(0, Max_Longitud);
        }
    }
    </script>
    <script type="text/javascript" language="javascript">
    //El nombre del controlador que mantiene la sesi�n
        var CONTROLADOR = "../../Mantenedor_sesiones.ashx";

        //Ejecuta el script en segundo plano evitando as� que caduque la sesi�n de esta p�gina
        function MantenSesion()
        {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para matener la sesi�n activa
        setInterval('MantenSesion()', <%=(int)(0.9*(Session.Timeout * 60000))%>);
        function formatCurrency(num) {
            var Combo = "";
            var Beneficio;
                num = num.toString().replace(/\$|\,/g,'');
                if(isNaN(num))
                num = "0";
                sign = (num == (num = Math.abs(num)));
                num = Math.floor(num*100+0.50000000001);
                cents = num%100;
                num = Math.floor(num/100).toString();
                if(cents<10)
                    cents = "0" + cents;
                    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
                    num = num.substring(0,num.length-(4*i+3))+','+
                    num.substring(num.length-(4*i+3));
                    return (((sign)?'':'-') + num + '.' + cents);
            }
             </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" EnablePartialRendering="true" AsyncPostBackTimeout="9000">
    </cc1:ToolkitScriptManager>
    <%--Inicio formulario--%>
    <asp:UpdatePanel ID="Upd_Parametros_Predial" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--update progrees--%>
            <asp:UpdateProgress ID="Uprg_Progress" runat="server" AssociatedUpdatePanelID="Upd_Parametros_Predial" DisplayAfter="0">
            <ProgressTemplate>
            <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
            <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
            </ProgressTemplate>
            </asp:UpdateProgress>
            
            <div id="Div_Generales" style="background-color:#ffffff; width:100%; height:100%">
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                    <td colspan ="2" class="label_titulo">Asignaci�n de Proveedor Verificaci�n</td>
                    </tr>
                    <tr>
                    <td colspan="2">
                     <asp:Image ID="Img_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" />
                     <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" CssClass="estilo_fuente_mensaje_error" Text="" /><br />
                     <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                    </td>                        
                    </tr>
                    <tr class="barra_busqueda">
                    <td align="left" style="width:20%">
                            <asp:ImageButton ID="Btn_Asignacion" runat="server" ImageUrl="~/paginas/imagenes/paginas/accept.png" Width="24px" CssClass="Img_Button" AlternateText="Asignar Mecanico Seleccionado" ToolTip="Asignar Mecanico Seleccionado" OnClick="Btn_Asignacion_Click"/>
                            <asp:ImageButton ID="Btn_Cancelar_Servicio" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_load.png" Width="24px" CssClass="Img_Button" AlternateText="Cancelar_Servicio" ToolTip="Cancelar Servicio Externo" OnClientClick="return confirm('�Esta seguro de Cancelar el presente Servicio Externo? \n Este pasar� a Diagnostico de Mecanico');" OnClick="Btn_Cancelar_Servicio_Click"/>
                            <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" Width="24px" CssClass="Img_Button" AlternateText="Salir" ToolTip="Salir" OnClick="Btn_Salir_Click" />                            
                     </td>
                     <td align="right" valign="top" style="width:80%">
                        <table style="width: 80%;">
                            <tr>
                                <td style="vertical-align: top; text-align: right; width: 5%">                                    
                                </td>
                                <td style="vertical-align: top; text-align: right; width: 90%">
                                    B�squeda:
                                    <asp:TextBox ID="Txt_Buscar" runat="server" MaxLength="100" TabIndex="5" ToolTip="Buscar"
                                        Width="180px" AutoPostBack="true"  />
                                    <cc1:TextBoxWatermarkExtender ID="WTE_Txt_Buscar" runat="server" WatermarkCssClass="watermarked"
                                        WatermarkText="<Folio>" TargetControlID="Txt_Buscar" />
                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Buscar" runat="server" TargetControlID="Txt_Buscar"
                                        FilterType="Numbers" />
                                </td>
                                <td style="vertical-align: top; text-align: right; width: 5%">
                                    <asp:ImageButton ID="Btn_Buscar" runat="server" TabIndex="6" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                        ToolTip="Buscar" OnClick="Btn_Buscar_Click"/>
                                </td>
                            </tr>
                        </table>
                     </td>
                    </tr>    
                    </table>

                    </div>                    
            <div ID="Div_Listado_Servicios" runat="server" style="width:100%;">
                <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                    <%-- <tr>
                             <td style="width:15%;"><asp:Label ID="Lbl_Filtrado_Estatus" runat="server" Text="Filtrar [Estatus]"></asp:Label></td>
                             <td style="width:35%;">
                                 <asp:DropDownList ID="Cmb_Filtrado_Estatus" runat="server" Width="98%">
                                    <asp:ListItem Value="">&lt; - TODAS - &gt;</asp:ListItem>
                                    <asp:ListItem Value="PENDIENTE">POR ASIGNAR</asp:ListItem>
                                    <asp:ListItem Value="PROCESO">ASIGNADO</asp:ListItem>
                                 </asp:DropDownList>
                             </td>
                             <td style="width:15%;">
                                 <asp:ImageButton ID="Btn_Actualizar_Listado" runat="server" ToolTip="Actualizar Listado" AlternateText="Actualizar Listado" ImageUrl="~/paginas/imagenes/paginas/actualizar_detalle.png" Width="16px" />
                             </td>
                             <td style="width:35%;">&nbsp;&nbsp;&nbsp;</td>
                        </tr>--%>
                </table>
                <br />
                <asp:GridView ID="Grid_Listado_Servicios" runat="server" AllowPaging="True" 
                    AllowSorting="true" AutoGenerateColumns="False" CssClass="GridView_1" 
                    DataKeyNames="MECANICO_ID,NO_ENTRADA,NO_SERVICIO,NO_SOLICITUD" 
                    EmptyDataText="No se Encontrar�n Servicios Pendientes por Asignar Proveedor" 
                    GridLines="None" OnPageIndexChanging="Grid_Listado_Servicios_PageIndexChanging"                     
                    OnSelectedIndexChanged="Grid_Listado_Servicios_SelectedIndexChanged" 
                    OnSorting="Grid_Listado_Servicios_Sorting" PageSize="20" Width="99%" 
                    onrowcreated="Grid_Listado_Servicios_RowCreated">
                    <RowStyle CssClass="GridItem" />
                    <Columns>
                        <asp:ButtonField ButtonType="Image" CommandName="Select" 
                            ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                            <ItemStyle Width="30px" />
                        </asp:ButtonField>
                        <asp:BoundField DataField="NO_ENTRADA" HeaderText="NO_ENTRADA" 
                            SortExpression="NO_ENTRADA">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="90px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NO_SERVICIO" HeaderText="NO_SERVICIO" 
                            SortExpression="NO_SERVICIO">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="90px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NO_SOLICITUD" HeaderText="NO_SOLICITUD" 
                            SortExpression="NO_SOLICITUD">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="90px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FOLIO" HeaderText="Folio" SortExpression="FOLIO">
                            <ItemStyle Font-Bold="true" Font-Size="X-Small" HorizontalAlign="Center" 
                                Width="90px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FECHA_RECEPCION" DataFormatString="{0:dd/MMM/yyyy}" 
                            HeaderText="Fecha Recepci�n" SortExpression="FECHA_RECEPCION">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="140px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TIPO_SERVICIO" HeaderText="Tipo Servicio" 
                            SortExpression="TIPO_SERVICIO">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="140px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DESCRIPCION_SERVICIO" HeaderText="Descripci�n" 
                            SortExpression="DESCRIPCION_SERVICIO">
                            <ItemStyle Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" 
                            SortExpression="DEPENDENCIA">
                            <ItemStyle Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NO_INVENTARIO" HeaderText="No. Inventario" 
                            SortExpression="NO_INVENTARIO">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="120px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NO_ECONOMICO" HeaderText="No. Economico" 
                            SortExpression="NO_ECONOMICO">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="120px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ESTATUS" HeaderText="" SortExpression="ESTATUS">
                            <ItemStyle Font-Bold="true" Font-Size="X-Small" HorizontalAlign="Center" 
                                Width="120px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MECANICO_ID" HeaderText="MECANICO_ID" 
                            Visible="false">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="35%" />
                        </asp:BoundField>
                    </Columns>
                    <PagerStyle CssClass="GridHeader" />
                    <SelectedRowStyle CssClass="GridSelected" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAltItem" />
                </asp:GridView>
            </div>
            <div ID="Div_Campos" runat="server" style="width:100%;">
                <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                    <tr>
                        <td colspan="4">
                            <asp:HiddenField ID="Hdf_No_Entrada" runat="server" />
                            <asp:HiddenField ID="Hdf_No_Solicitud" runat="server" />
                            <asp:HiddenField ID="Hdf_No_Servicio" runat="server" />
                            <asp:HiddenField ID="Hdf_No_Reserva" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width:15%;">
                            <asp:Label ID="Lbl_Folio" runat="server" Font-Bold="true" ForeColor="Black" 
                                Text="Folio"></asp:Label>
                        </td>
                        <td style="width:35%;">
                            <asp:TextBox ID="Txt_Folio" runat="server" Font-Bold="true" ForeColor="Red" 
                                style="text-align:right;" Width="98%"></asp:TextBox>
                        </td>
                        <td colspan="2">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width:15%;">
                            <asp:Label ID="Lbl_Fecha_Elaboracion" runat="server" Text="Fecha Elaboraci�n"></asp:Label>
                        </td>
                        <td style="width:35%;">
                            <asp:TextBox ID="Txt_Fecha_Elaboracion" runat="server" Enabled="false" 
                                Width="98%"></asp:TextBox>
                        </td>
                        <td style="width:15%;">
                            &nbsp;&nbsp;
                            <asp:Label ID="Lbl_Fecha_Recepcion" runat="server" Text="Fecha Recepci�n"></asp:Label>
                        </td>
                        <td style="width:35%;">
                            <asp:TextBox ID="Txt_Fecha_Recepcion" runat="server" Enabled="false" 
                                Width="98%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:15%;">
                            <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text="Unidad Responsable"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="Cmb_Unidad_Responsable" runat="server" Enabled="false" 
                                Width="100%">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:15%;">
                            <asp:Label ID="Lbl_Kilometraje" runat="server" Text="Kilometraje"></asp:Label>
                        </td>
                        <td style="width:16%;">
                            <asp:TextBox ID="Txt_Kilometraje" runat="server" Enabled="false" Width="95%"></asp:TextBox>
                        </td>
                        <td style="width:15%;">
                            &nbsp;&nbsp;
                            <asp:Label ID="Lbl_Tipo_Servicio" runat="server" Text="Tipo Servicio"></asp:Label>
                        </td>
                        <td style="width:35%;">
                            <asp:DropDownList ID="Cmb_Tipo_Servicio" runat="server" Enabled="false" 
                                Width="100%">
                                <asp:ListItem Value="">&lt; - - SELECCIONE - - &gt;</asp:ListItem>
                                <asp:ListItem Value="SERVICIO_CORRECTIVO">SERVICIO CORRECTIVO</asp:ListItem>
                                <asp:ListItem Value="SERVICIO_PREVENTIVO">SERVICIO PREVENTIVO</asp:ListItem>
                                <asp:ListItem Value="VERIFICACION">VERIFICACI�N</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Panel ID="Pnl_Vehiculo_Seleccionado" runat="server" 
                                GroupingText="Veh�culo para el Servicio" Width="99%">
                                <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                                    <tr>
                                        <td style="width:15%;">
                                            <asp:HiddenField ID="Hdf_Vehiculo_ID" runat="server" />
                                            <asp:Label ID="Lbl_No_Inventario" runat="server" Text="No. Inventario"></asp:Label>
                                        </td>
                                        <td style="width:35%;">
                                            <asp:TextBox ID="Txt_No_Inventario" runat="server" Enabled="false" 
                                                MaxLength="7" Width="70%"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Inventario" runat="server" 
                                                FilterType="Numbers" TargetControlID="Txt_No_Inventario">
                                            </cc1:FilteredTextBoxExtender>
                                        </td>
                                        <td style="width:15%;">
                                            &nbsp;&nbsp;
                                            <asp:Label ID="Lbl_No_Economico" runat="server" Text="No. Economico"></asp:Label>
                                        </td>
                                        <td style="width:35%;">
                                            <asp:TextBox ID="Txt_No_Economico" runat="server" Enabled="false" Width="98%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:15%;">
                                            <asp:Label ID="Lbl_Datos_Vehiculo" runat="server" Text="Veh�culo"></asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="Txt_Datos_Vehiculo" runat="server" Enabled="false" Width="99%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:15%;">
                                            <asp:Label ID="Lbl_Placas" runat="server" Text="Placas"></asp:Label>
                                        </td>
                                        <td style="width:35%;">
                                            <asp:TextBox ID="Txt_Placas" runat="server" Enabled="false" Width="98%"></asp:TextBox>
                                        </td>
                                        <td style="width:15%;">
                                            &nbsp;&nbsp;
                                            <asp:Label ID="Lbl_Anio" runat="server" Text="A�o"></asp:Label>
                                        </td>
                                        <td style="width:35%;">
                                            <asp:TextBox ID="Txt_Anio" runat="server" Enabled="false" Width="98%"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="Pnl_Bien_Mueble_Seleccionado" runat="server" 
                                        GroupingText="Bien Mueble para el Servicio" Width="99%">
                                        <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                                            <tr>
                                                <td style="width:15%;">
                                                    <asp:HiddenField ID="Hdf_Bien_Mueble_ID" runat="server" />
                                                    <asp:Label ID="Lbl_No_Inventario_BM" runat="server" Text="No. Inventario"></asp:Label>
                                                </td>
                                                <td style="width:35%;">
                                                    <asp:TextBox ID="Txt_No_Inventario_BM" runat="server" MaxLength="7" Width="98%" Enabled="false"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Inventario_BM" runat="server" 
                                                        FilterType="Numbers" TargetControlID="Txt_No_Inventario_BM">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width:15%;">
                                                    &nbsp;
                                                </td>
                                                <td style="width:35%;">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:15%;">
                                                    <asp:Label ID="Lbl_Descripcion_Bien" runat="server" Text="Descripci�n Bien"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Descripcion_Bien" runat="server" Enabled="false" Rows="2" 
                                                        TextMode="MultiLine" Width="99%"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:15%;">
                                                    <asp:Label ID="Lbl_Numero_Serie_Bien" runat="server" Text="No. Serie"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Numero_Serie_Bien" runat="server" Enabled="false" 
                                                        Width="99%"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Panel ID="Pnl_Descripcion_Servicio" runat="server" 
                                GroupingText="Descripci�n del Servicio" Width="99%">
                                <asp:TextBox ID="Txt_Descripcion_Servicio" runat="server" Enabled="false" 
                                    Rows="5" TextMode="MultiLine" Width="99%"></asp:TextBox>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Panel ID="Pnl_Diagnostico_Servicio" runat="server" 
                                GroupingText="Diagnostico del Servicio" Width="99%">
                                <asp:TextBox ID="Txt_Diagnostico_Servicio" runat="server" Enabled="false" 
                                    Rows="5" TextMode="MultiLine" Width="99%"></asp:TextBox>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:GridView ID="Grid_Tarjeta_Informativa" runat="server" AllowPaging="false" 
                                AutoGenerateColumns="False" CssClass="Tabla_Comentarios" 
                                HeaderStyle-CssClass="tblHead" PageSize="5" Style="white-space: normal;" 
                                Width="93%">
                                <Columns>
                                    <asp:BoundField DataField="CAMBIO" HeaderText="Tarjeta Informativa">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Width="30%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FECHA_CAMBIO" HeaderText="Fecha">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle Width="70%" />
                                    </asp:BoundField>
                                </Columns>
                                <SelectedRowStyle CssClass="GridSelected" />
                                <PagerStyle CssClass="GridHeader" />
                                <HeaderStyle CssClass="Tabla_Comentarios_Head" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <hr style="width:98%;" />
                <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                    <tr>
                        <td style="width:15%;">
                            Proveedor
                        </td>
                        <td>
                            <asp:TextBox ID="Txt_Nombre_Proveedor" runat="server" Enabled="false" 
                                style="float:left" Width="95%"></asp:TextBox>
                            <asp:ImageButton ID="Btn_Busqueda_Proveedores" runat="server" Height="22px" 
                                ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png" 
                                OnClick="Btn_Busqueda_Proveedores_Click" style="float:right" TabIndex="10" 
                                ToolTip="B�squeda Avanzada" Width="22px" />
                        </td>
                    </tr>
                </table>
                <hr style="width:98%;" />
                <asp:Panel ID="Pnl_Historico" runat="server" 
                    GroupingText="Historial de Proveedores para el Servicio" 
                    style="overflow:auto;height:200px;width:99%;vertical-align:top;border-style:outset;border-color:Silver;" 
                    Width="99%">
                    <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                        <%------------------ Historico ------------------%>
                        <tr>
                            <td style="text-align: left; vertical-align: top;">
                                <asp:GridView ID="Grid_Historial_Proveedores" runat="server" AllowPaging="True" 
                                    AutoGenerateColumns="False" CssClass="Tabla_Comentarios" 
                                    HeaderStyle-CssClass="tblHead" PageSize="5" Style="white-space: normal;" 
                                    Width="97%">
                                    <Columns>
                                        <asp:BoundField DataField="NO_ASIGNACION_PROV_SERV" 
                                            HeaderText="No. Observaci�n">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="15%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NO_SERVICIO" HeaderText="No. Servicio">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="10%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NOMBRE_PROVEEDOR" HeaderText="Proveedor">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="20%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="OBSERVACIONES" HeaderText="Observaciones">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="35%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ESTATUS" HeaderText="Estatus">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="35%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="USUARIO_CREO" HeaderText="Usuario">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="35%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FECHA_CREO" DataFormatString="{0:dd/MMM/yyyy}" 
                                            HeaderText="Fecha Creacion" SortExpression="FECHA_CREO">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="15%" />
                                        </asp:BoundField>
                                    </Columns>
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <PagerStyle CssClass="GridHeader" />
                                    <HeaderStyle CssClass="tblHead" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
            <asp:HiddenField ID="Hdf_Proveedor_ID" runat="server" />
            <asp:HiddenField ID="Hdf_Mecanico_ID" runat="server" />
            </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

