﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Unidades_Responsables_Rol.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Taller_Mecanico.Operacion_Revista_Mecanica.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Entradas_Vehiculos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Salidas_Vehiculos.Negocio;
using JAPAMI.Taller_Mecanico.Catalogo_Detalles_Rev_Mec.Negocio;
using JAPAMI.Taller_Mecanico.Reporte_Solicitud_Servicio.Negocio;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;

public partial class paginas_Taller_Mecanico_Frm_Ope_Tal_Historico_Revista_Mecanica : System.Web.UI.Page {

    #region Page_Load

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN: Carga la Pagina Inicial
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 25/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e) {
            Div_Contenedor_Msj_Error.Visible = false;
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack) {
                Llenar_Combo_Unidades_Responsables();
                Grid_Listado_Solicitudes.PageIndex = 0;
                Configuracion_Formulario("INICIAL");
                Cmb_Unidad_Responsable.Enabled = false;
                Cmb_Unidad_Responsable_Busqueda.Enabled = Validar_Acceso_Unidades_Responsables();
                Cmb_Unidad_Responsable_Busqueda.SelectedIndex = Cmb_Unidad_Responsable_Busqueda.Items.IndexOf(Cmb_Unidad_Responsable_Busqueda.Items.FindByValue(Cls_Sessiones.Dependencia_ID_Empleado));
                Llenar_Listado_Solicitudes();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Validar_Acceso_Unidades_Responsables
        ///DESCRIPCIÓN: Se valida el Acceso a Otras UR.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected Boolean Validar_Acceso_Unidades_Responsables() {
            Boolean Accesos_Totales = false;
            Cls_Ope_Tal_Unidades_Responsables_Rol_Negocio Cls_Negocio = new Cls_Ope_Tal_Unidades_Responsables_Rol_Negocio();
            Cls_Negocio.P_Estatus = "ACTIVO";
            DataTable Dt_UR = Cls_Negocio.Consultar_Listado_UR();
            DataRow[] Filas = Dt_UR.Select("DEPENDENCIA_ID = '" + Cls_Sessiones.Dependencia_ID_Empleado.Trim() + "'");
            if (Filas.Length > 0) { Accesos_Totales = true; }
            return Accesos_Totales;
        }

    #endregion

    #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Unidades_Responsables
        ///DESCRIPCIÓN: Se llena el Combo de las Unidades Responsables.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 25/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Llenar_Combo_Unidades_Responsables() {
            Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            Negocio.P_Estatus = "ACTIVO";
            DataTable Dt_Dependencias = Negocio.Consultar_Unidades_Responsables();
            Cmb_Unidad_Responsable_Busqueda.DataSource = Dt_Dependencias;
            Cmb_Unidad_Responsable_Busqueda.DataTextField = "CLAVE_NOMBRE";
            Cmb_Unidad_Responsable_Busqueda.DataValueField = "DEPENDENCIA_ID";
            Cmb_Unidad_Responsable_Busqueda.DataBind();
            Cmb_Unidad_Responsable_Busqueda.Items.Insert(0, new ListItem("< - - TODAS - - >", ""));
            Cmb_Unidad_Responsable.DataSource = Dt_Dependencias;
            Cmb_Unidad_Responsable.DataTextField = "CLAVE_NOMBRE";
            Cmb_Unidad_Responsable.DataValueField = "DEPENDENCIA_ID";
            Cmb_Unidad_Responsable.DataBind();
            Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("< - - TODAS - - >", ""));
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Solicitudes
        ///DESCRIPCIÓN: Se llena el Listado de las Solicitudes.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 25/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Llenar_Listado_Solicitudes() {
            Cls_Ope_Tal_Solicitud_Servicio_Negocio Negocio = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
            if (Cmb_Unidad_Responsable_Busqueda.SelectedIndex > 0) { Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable_Busqueda.SelectedItem.Value.Trim(); }
            if (Cmb_Filtrado_Estatus.SelectedIndex > 0) { Negocio.P_Estatus = Cmb_Filtrado_Estatus.SelectedItem.Value; }
            if (Txt_Numero_Inventario_Busqueda.Text.Trim().Length > 0) { Negocio.P_No_Inventario = Convert.ToInt32(Txt_Numero_Inventario_Busqueda.Text.Trim()); }
            if (Txt_Numero_Economico_Busqueda.Text.Trim().Length > 0) { Negocio.P_No_Economico = Txt_Numero_Economico_Busqueda.Text.Trim(); }
            if (Txt_Folio_Solicitud_Busqueda.Text.Trim().Length > 0) { Negocio.P_Folio_Solicitud = String.Format("{0:0000000000}", Convert.ToInt32(Txt_Folio_Solicitud_Busqueda.Text.Trim())); }
            Negocio.P_Tipo_Servicio = "REVISTA_MECANICA";
            if (Txt_Fecha_Recepcion_Inicial.Text.Trim().Length > 0) { Negocio.P_Busq_Fecha_Recepcion_Inicial = Convert.ToDateTime(Txt_Fecha_Recepcion_Inicial.Text.Trim()); }
            if (Txt_Fecha_Recepcion_Final.Text.Trim().Length > 0) { Negocio.P_Busq_Fecha_Recepcion_Final = Convert.ToDateTime(Txt_Fecha_Recepcion_Final.Text.Trim()); }
            Negocio.P_Procedencia = "SOLICITUD";
            DataTable Dt_Resultados = Negocio.Consultar_Listado_Solicitudes_Servicio();
            Grid_Listado_Solicitudes.Columns[1].Visible = true;
            Grid_Listado_Solicitudes.DataSource = Dt_Resultados;
            Grid_Listado_Solicitudes.DataBind();
            Grid_Listado_Solicitudes.Columns[1].Visible = false;
            Cambiar_Estatus_Grid();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cambiar_Estatus_Grid
        ///DESCRIPCIÓN          : Cambiar el Estatus que aparece en el Grid
        ///PARAMETROS           : 
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 25/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Cambiar_Estatus_Grid() {
            if (Grid_Listado_Solicitudes.Rows.Count > 0) {
                foreach (GridViewRow Fila_Grid in Grid_Listado_Solicitudes.Rows) {
                    Fila_Grid.Cells[7].Text = Estatus_Solicitudes(HttpUtility.HtmlDecode(Fila_Grid.Cells[7].Text.Trim()).Trim());
                }
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Estatus_Solicitudes
        ///DESCRIPCIÓN          : Regresa el Estatus a Mostrar.
        ///PARAMETROS           : 
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 25/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private String Estatus_Solicitudes(String Estatus_Operativo) {
            String Estatus_Mostrar = "";
            switch (Estatus_Operativo) {
                case "PENDIENTE": Estatus_Mostrar = "PENDIENTE"; break;
                case "AUTORIZADA": Estatus_Mostrar = "AUTORIZADA"; break;
                case "CANCELADA": Estatus_Mostrar = "CANCELADA"; break;
                case "RECHAZADA": Estatus_Mostrar = "RECHAZADA"; break;
                case "EN_REPARACION": Estatus_Mostrar = "EN REPARACIÓN"; break;
                case "TERMINADO": Estatus_Mostrar = "TERMINADO"; break;
                case "ENTREGADO": Estatus_Mostrar = "ENTREGADO"; break;
            }
            return Estatus_Mostrar;
        }
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
        ///DESCRIPCIÓN: Se Habilitan y/o inhabilitan los campos dependendiendo de la Condicion.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Configuracion_Formulario(String Operacion) {
            switch (Operacion) {
                case "INICIAL":
                    Btn_Salir.AlternateText = "Salir";
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Salir.Visible = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Imprimir_Solicitud_Servicio.Visible = true;
                    Txt_No_Inventario.Enabled = false;
                    Txt_Kilometraje.Enabled = false;
                    Txt_Descripcion_Servicio.Enabled = false;
                    Txt_Correo_Electronico.Enabled = false;
                    Div_Listado_Solicitudes.Visible = true;
                    Div_Campos.Visible = false;
                    Btn_Imprimir_Solicitud_Servicio.Visible = false;
                    break;
                case "OPERACION":
                    Btn_Salir.AlternateText = "Cancelar";
                    Btn_Salir.ToolTip = "Cancelar Operación";
                    Btn_Salir.Visible = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Imprimir_Solicitud_Servicio.Visible = true;
                    Txt_No_Inventario.Enabled = true;
                    Txt_Kilometraje.Enabled = true;
                    Txt_Descripcion_Servicio.Enabled = true;
                    Txt_Correo_Electronico.Enabled = true;
                    Div_Listado_Solicitudes.Visible = false;
                    Div_Campos.Visible = true;
                    Btn_Imprimir_Solicitud_Servicio.Visible = true;
                    break;
                case "MOSTRAR INFORMACION":
                    Btn_Salir.AlternateText = "Salir";
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Salir.Visible = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Imprimir_Solicitud_Servicio.Visible = true;
                    Txt_No_Inventario.Enabled = false;
                    Txt_Kilometraje.Enabled = false;
                    Txt_Descripcion_Servicio.Enabled = false;
                    Txt_Correo_Electronico.Enabled = false;
                    Div_Listado_Solicitudes.Visible = false;
                    Div_Campos.Visible = true;
                    Btn_Imprimir_Solicitud_Servicio.Visible = true;
                    break;
            }
        }
            
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
        ///DESCRIPCIÓN: Limpia los campos del Formulario.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Limpiar_Formulario() {
            Hdf_No_Solicitud.Value = "";
            Hdf_Folio_Solicitud.Value = "";
            Hdf_Empleado_Solicito_ID.Value = "";
            Hdf_Email.Value = "";
            Txt_Folio_Solicitud.Text = "";
            Txt_Fecha_Elaboracion.Text = "";
            Txt_Fecha_Recepcion.Text = "";
            Txt_Estatus.Text = "";
            Txt_Kilometraje.Text = "";
            Cmb_Unidad_Responsable.SelectedIndex = 0;
            Txt_Descripcion_Servicio.Text = "";
            Txt_Correo_Electronico.Text = "";
            Txt_Diagnostico_Servicio.Text = "";
            Txt_Empleado_Presento.Text = "";
            Limpiar_Formulario_Vehiculo();
            Cargar_Estilo_Estatus();
            Session.Remove("Tabla_imagenes_Entrada");
            Session.Remove("Tabla_imagenes_Salida");
            Cargar_Imagenes_Entrada();
            Cargar_Imagenes_Salida();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario_Vehiculo
        ///DESCRIPCIÓN: Limpia los campos del Formulario [Seccion de Vehiculos].
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Limpiar_Formulario_Vehiculo() {
            Hdf_Vehiculo_ID.Value = "";
            Txt_No_Inventario.Text = "";
            Txt_No_Economico.Text = "";
            Txt_Datos_Vehiculo.Text = "";
            Txt_Placas.Text = "";
            Txt_Anio.Text = "";
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cargar_Estilo_Estatus
        ///DESCRIPCIÓN          : Carga el Estilo para el Estatus
        ///PARAMETROS           : 
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 25/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Cargar_Estilo_Estatus() {
            String Estatus = Txt_Estatus.Text.Trim();
            switch (Estatus) {
                case "PENDIENTE":
                    Txt_Estatus.BackColor = System.Drawing.Color.FromArgb(169, 188, 245);
                    Txt_Estatus.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
                    break;
                case "RECHAZADA":
                    Txt_Estatus.BackColor = System.Drawing.Color.FromArgb(255, 46, 56);
                    Txt_Estatus.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
                    break;
                case "CANCELADA":
                    Txt_Estatus.BackColor = System.Drawing.Color.FromArgb(184, 184, 184);
                    Txt_Estatus.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
                    break;
                case "AUTORIZADA":
                    Txt_Estatus.BackColor = System.Drawing.Color.FromArgb(156, 255, 186);
                    Txt_Estatus.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
                    break;
                case "EN_REPARACION":
                    Txt_Estatus.BackColor = System.Drawing.Color.FromArgb(156, 255, 186);
                    Txt_Estatus.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
                    break;
                case "TERMINADO":
                    Txt_Estatus.BackColor = System.Drawing.Color.White;
                    Txt_Estatus.ForeColor = System.Drawing.Color.FromArgb(17, 80, 17);
                    break;
                default:
                    Txt_Estatus.BackColor = System.Drawing.Color.White;
                    Txt_Estatus.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
                    break;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Mostrar_Registro
        ///DESCRIPCIÓN: Muestra el Registro en los campos.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Mostrar_Registro()  {
            Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
            Solicitud.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
            Solicitud = Solicitud.Consultar_Detalles_Solicitud_Servicio();
            if (Solicitud.P_No_Solicitud > (-1)) {
                Txt_Fecha_Elaboracion.Text = String.Format("{0:dd/MMM/yyyy}", Solicitud.P_Fecha_Elaboracion);
                Txt_Folio_Solicitud.Text = Solicitud.P_Folio_Solicitud.Trim();
                Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Solicitud.P_Dependencia_ID));
                Hdf_Vehiculo_ID.Value = Solicitud.P_Bien_ID;
                Hdf_Empleado_Solicito_ID.Value = Solicitud.P_Empleado_Solicito_ID;
                Hdf_Email.Value = Solicitud.P_Correo_Electronico;
                Txt_Descripcion_Servicio.Text = Solicitud.P_Descripcion_Servicio;
                if (!String.Format("{0:ddMMyyyy}", Solicitud.P_Fecha_Recepcion_Real).Equals(String.Format("{0:ddMMyyyy}", new DateTime()))) {
                    Txt_Fecha_Recepcion.Text = String.Format("{0:dd/MMM/yyyy}", Solicitud.P_Fecha_Recepcion_Real);    
                }
                if (Solicitud.P_Kilometraje > (-1.0)) {
                    Txt_Kilometraje.Text = String.Format("{0:############0.00}", Solicitud.P_Kilometraje);
                }
                Cargar_Datos_Vehiculo(Hdf_Vehiculo_ID.Value, "IDENTIFICADOR");
                Txt_Estatus.Text = Solicitud.P_Estatus;
                Txt_Correo_Electronico.Text = Solicitud.P_Correo_Electronico;
                Hdf_Folio_Solicitud.Value = Solicitud.P_Folio_Solicitud;
                Cargar_Estilo_Estatus();
                Txt_Estatus.Text = Estatus_Solicitudes(Txt_Estatus.Text);
                Cls_Ope_Tal_Revista_Mecanica_Negocio Rev_Negocio = new Cls_Ope_Tal_Revista_Mecanica_Negocio();
                Rev_Negocio.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                Rev_Negocio = Rev_Negocio.Consultar_Detalles_Revista_Mecanica();
                Llenar_Grid_Listado_Detalles_Revision();
                Cargar_Grid_Detalles_Revision(Rev_Negocio.P_Dt_Detalles_Revista);
                if (Rev_Negocio.P_No_Registro > (-1)) {
                    Txt_Diagnostico_Servicio.Text = Rev_Negocio.P_Diagnostico;
                    if (Rev_Negocio.P_No_Entrada > (-1)) { 
                        Cls_Ope_Tal_Entradas_Vehiculos_Negocio Entrada_Negocio = new Cls_Ope_Tal_Entradas_Vehiculos_Negocio();
                        Entrada_Negocio.P_No_Entrada = Rev_Negocio.P_No_Entrada;
                        Entrada_Negocio = Entrada_Negocio.Consultar_Detalles_Entrada_Vehiculo();
                        if (Entrada_Negocio.P_No_Entrada > (-1)) {
                            if (!String.IsNullOrEmpty(Entrada_Negocio.P_Empleado_Entrego_ID)) { 
                                Cls_Ope_Tal_Consultas_Generales_Negocio Con_Generales = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                                Con_Generales.P_Empleado_ID = Entrada_Negocio.P_Empleado_Entrego_ID;
                                DataTable Dt_Empleados = Con_Generales.Consultar_Empleados();
                                if (Dt_Empleados != null && Dt_Empleados.Rows.Count > 0) {
                                    Txt_Empleado_Presento.Text = "[" + Dt_Empleados.Rows[0]["NO_EMPLEADO"].ToString().Trim() + "] " + Dt_Empleados.Rows[0]["NOMBRE"].ToString().Trim();
                                }
                            }
                            if (Entrada_Negocio.P_Dt_Archivos != null) {
                                if (Entrada_Negocio.P_Dt_Archivos.Rows.Count > 0) {
                                    Session["Tabla_imagenes_Entrada"] = Entrada_Negocio.P_Dt_Archivos;
                                    Cargar_Imagenes_Entrada();
                                }
                            }
                        } 
                        Cls_Ope_Tal_Salidas_Vehiculos_Negocio Salida_Negocio = new Cls_Ope_Tal_Salidas_Vehiculos_Negocio();
                        Salida_Negocio.P_No_Entrada = Rev_Negocio.P_No_Entrada;
                        Salida_Negocio = Salida_Negocio.Consultar_Detalles_Salida_Vehiculo();
                        if (Salida_Negocio.P_No_Salida > (-1)) {
                            if (Salida_Negocio.P_Dt_Archivos != null) {
                                if (Salida_Negocio.P_Dt_Archivos.Rows.Count > 0) {
                                    Session["Tabla_imagenes_Salida"] = Salida_Negocio.P_Dt_Archivos;
                                    Cargar_Imagenes_Salida();
                                }
                            }
                        } 
                    }
                }

            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Cargar_Imagenes_Entrada
        ///DESCRIPCIÓN: Carga las imagenes a mostrar de entrada
        ///PROPIEDADES:     
        ///CREO: Jesus Toledo Rdz.
        ///FECHA_CREO: 12/Julio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Cargar_Imagenes_Entrada() {
            DataTable Img;
            if (Session["Tabla_imagenes_Entrada"] != null) {
                Img = (DataTable)Session["Tabla_imagenes_Entrada"];
                foreach (DataRow Renglon in Img.Rows) {
                    if (Renglon["RUTA_ARCHIVO"] != null) {
                        switch (Renglon["TIPO_FOTO"].ToString()) {
                            case "FRENTE":
                                if (!String.IsNullOrEmpty(Renglon["RUTA_ARCHIVO"].ToString())) {
                                    Liga_Entrada_Frente.HRef = Renglon["RUTA_ARCHIVO"].ToString();
                                    Liga_Imagen_Entrada_Frente.Src = Renglon["RUTA_ARCHIVO"].ToString();
                                } else {
                                    Liga_Entrada_Frente.HRef = "../imagenes/paginas/vehicle_front_view.png";
                                    Liga_Imagen_Entrada_Frente.Src = "../imagenes/paginas/vehicle_front_view.png";
                                }
                                break;
                            case "ATRAS":
                                if (!String.IsNullOrEmpty(Renglon["RUTA_ARCHIVO"].ToString())) {
                                    Liga_Entrada_Atras.HRef = Renglon["RUTA_ARCHIVO"].ToString();
                                    Liga_Imagen_Entrada_Atras.Src = Renglon["RUTA_ARCHIVO"].ToString();
                                } else {
                                    Liga_Entrada_Atras.HRef = "../imagenes/paginas/vehicle_back_view.png";
                                    Liga_Imagen_Entrada_Atras.Src = "../imagenes/paginas/vehicle_back_view.png";
                                }
                                break;
                            case "IZQUIERDA":
                                if (!String.IsNullOrEmpty(Renglon["RUTA_ARCHIVO"].ToString())) {
                                    Liga_Entrada_Lado_Izquierdo.HRef = Renglon["RUTA_ARCHIVO"].ToString();
                                    Liga_Imagen_Entrada_Lado_Izquierdo.Src = Renglon["RUTA_ARCHIVO"].ToString();
                                } else {
                                    Liga_Entrada_Lado_Izquierdo.HRef = "../imagenes/paginas/vehicle_left_view.png";
                                    Liga_Imagen_Entrada_Lado_Izquierdo.Src = "../imagenes/paginas/vehicle_left_view.png";
                                }
                                break;
                            case "DERECHA":
                                if (!String.IsNullOrEmpty(Renglon["RUTA_ARCHIVO"].ToString())) {
                                    Liga_Entrada_Lado_Derecho.HRef = Renglon["RUTA_ARCHIVO"].ToString();
                                    Liga_Imagen_Entrada_Lado_Derecho.Src = Renglon["RUTA_ARCHIVO"].ToString();
                                } else {
                                    Liga_Entrada_Lado_Derecho.HRef = "../imagenes/paginas/vehicle_rigth_view.png";
                                    Liga_Imagen_Entrada_Lado_Derecho.Src = "../imagenes/paginas/vehicle_rigth_view.png";
                                }
                                break; 
                        }
                    }
                }
            } else {
                Liga_Entrada_Frente.HRef = "../imagenes/paginas/vehicle_front_view.png";
                Liga_Imagen_Entrada_Frente.Src = "../imagenes/paginas/vehicle_front_view.png";
                Liga_Entrada_Atras.HRef = "../imagenes/paginas/vehicle_back_view.png";
                Liga_Imagen_Entrada_Atras.Src = "../imagenes/paginas/vehicle_back_view.png";
                Liga_Entrada_Lado_Izquierdo.HRef = "../imagenes/paginas/vehicle_left_view.png";
                Liga_Imagen_Entrada_Lado_Izquierdo.Src = "../imagenes/paginas/vehicle_left_view.png";
                Liga_Entrada_Lado_Derecho.HRef = "../imagenes/paginas/vehicle_rigth_view.png";
                Liga_Imagen_Entrada_Lado_Derecho.Src = "../imagenes/paginas/vehicle_rigth_view.png";
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Cargar_Imagenes_Salida
        ///DESCRIPCIÓN: Carga las imagenes a mostrar de salida
        ///PROPIEDADES:     
        ///CREO: Jesus Toledo Rdz.
        ///FECHA_CREO: 12/Julio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Cargar_Imagenes_Salida() {
            DataTable Img;
            if (Session["Tabla_imagenes_Salida"] != null) {
                Img = (DataTable)Session["Tabla_imagenes_Salida"];
                foreach (DataRow Renglon in Img.Rows) {
                    if (Renglon["RUTA_ARCHIVO"] != null) {
                        switch (Renglon["TIPO_FOTO"].ToString()) {
                            case "FRENTE":
                                if (!String.IsNullOrEmpty(Renglon["RUTA_ARCHIVO"].ToString())) {
                                    Liga_Salida_Frente.HRef = Renglon["RUTA_ARCHIVO"].ToString();
                                    Liga_Imagen_Salida_Frente.Src = Renglon["RUTA_ARCHIVO"].ToString();
                                } else {
                                    Liga_Salida_Frente.HRef = "../imagenes/paginas/vehicle_front_view.png";
                                    Liga_Imagen_Salida_Frente.Src = "../imagenes/paginas/vehicle_front_view.png";
                                }
                                break;
                            case "ATRAS":
                                if (!String.IsNullOrEmpty(Renglon["RUTA_ARCHIVO"].ToString())) {
                                    Liga_Salida_Atras.HRef = Renglon["RUTA_ARCHIVO"].ToString();
                                    Liga_Imagen_Salida_Atras.Src = Renglon["RUTA_ARCHIVO"].ToString();
                                } else {
                                    Liga_Salida_Atras.HRef = "../imagenes/paginas/vehicle_back_view.png";
                                    Liga_Imagen_Salida_Atras.Src = "../imagenes/paginas/vehicle_back_view.png";
                                }
                                break;
                            case "IZQUIERDA":
                                if (!String.IsNullOrEmpty(Renglon["RUTA_ARCHIVO"].ToString())) {
                                    Liga_Salida_Lado_Izquierdo.HRef = Renglon["RUTA_ARCHIVO"].ToString();
                                    Liga_Imagen_Salida_Lado_Izquierdo.Src = Renglon["RUTA_ARCHIVO"].ToString();
                                } else {
                                    Liga_Salida_Lado_Izquierdo.HRef = "../imagenes/paginas/vehicle_left_view.png";
                                    Liga_Imagen_Salida_Lado_Izquierdo.Src = "../imagenes/paginas/vehicle_left_view.png";
                                }
                                break;
                            case "DERECHA":
                                if (!String.IsNullOrEmpty(Renglon["RUTA_ARCHIVO"].ToString())) {
                                    Liga_Salida_Lado_Derecho.HRef = Renglon["RUTA_ARCHIVO"].ToString();
                                    Liga_Imagen_Salida_Lado_Derecho.Src = Renglon["RUTA_ARCHIVO"].ToString();
                                } else {
                                    Liga_Salida_Lado_Derecho.HRef = "../imagenes/paginas/vehicle_rigth_view.png";
                                    Liga_Imagen_Salida_Lado_Derecho.Src = "../imagenes/paginas/vehicle_rigth_view.png";
                                }
                                break; 
                        }
                    }
                }
            } else {
                Liga_Salida_Frente.HRef = "../imagenes/paginas/vehicle_front_view.png";
                Liga_Imagen_Salida_Frente.Src = "../imagenes/paginas/vehicle_front_view.png";
                Liga_Salida_Atras.HRef = "../imagenes/paginas/vehicle_back_view.png";
                Liga_Imagen_Salida_Atras.Src = "../imagenes/paginas/vehicle_back_view.png";
                Liga_Salida_Lado_Izquierdo.HRef = "../imagenes/paginas/vehicle_left_view.png";
                Liga_Imagen_Salida_Lado_Izquierdo.Src = "../imagenes/paginas/vehicle_left_view.png";
                Liga_Salida_Lado_Derecho.HRef = "../imagenes/paginas/vehicle_rigth_view.png";
                Liga_Imagen_Salida_Lado_Derecho.Src = "../imagenes/paginas/vehicle_rigth_view.png";
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Vehiculo
        ///DESCRIPCIÓN: Se cargan los Datos del Vehiculo Seleccionado.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 25/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Cargar_Datos_Vehiculo(String Vehiculo, String Tipo_Busqueda) {
            Limpiar_Formulario_Vehiculo();
            Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            switch (Tipo_Busqueda) { 
                case "NO_INVENTARIO":
                    Consulta_Negocio.P_No_Inventario = Vehiculo;
                    break;
                case "IDENTIFICADOR":
                    Consulta_Negocio.P_Vehiculo_ID = Vehiculo;
                    break;
                default: break;
            }
            if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Consulta_Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value; }
            DataTable Dt_Vehiculo = Consulta_Negocio.Consultar_Vehiculos();
            if (Dt_Vehiculo.Rows.Count > 0) {
                Hdf_Vehiculo_ID.Value = Dt_Vehiculo.Rows[0]["VEHICULO_ID"].ToString().Trim();
                Txt_No_Inventario.Text = Dt_Vehiculo.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                Txt_No_Economico.Text = Dt_Vehiculo.Rows[0]["NO_ECONOMICO"].ToString().Trim();
                Txt_Datos_Vehiculo.Text = Dt_Vehiculo.Rows[0]["VEHICULO_DESCRIPCION"].ToString().Trim();
                Txt_Placas.Text = Dt_Vehiculo.Rows[0]["PLACAS"].ToString().Trim();
                Txt_Anio.Text = Dt_Vehiculo.Rows[0]["ANIO"].ToString().Trim();
                Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString()));
            } else {
                Lbl_Ecabezado_Mensaje.Text = "El Vehículo no se encontro.";
                if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Lbl_Mensaje_Error.Text = "[Puede ser que no este asignado a la Unidad Responsable Seleccionada]."; }
                else { Lbl_Mensaje_Error.Text = ""; }
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Cargar_Grid_Detalles_Recepcion
        ///DESCRIPCIÓN: Carga el Grid de los detalles con los predeterminados de Entrada
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 01/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Cargar_Grid_Detalles_Revision(DataTable Dt_Detalles) {
            if (Dt_Detalles.Rows.Count > 0) { 
                foreach (GridViewRow Fila_Grid in Grid_Listado_Detalles_Recepcion.Rows) {
                    String Valor = "---";
                    String SubParte = HttpUtility.HtmlDecode(Fila_Grid.Cells[1].Text.Trim());
                    DataRow[] Filas = Dt_Detalles.Select("SUBPARTE_ID = '" + SubParte.Trim() + "'");
                    if (Filas.Length > 0) {
                        Valor = Filas[0]["VALOR"].ToString().Trim();
                    }
                    Fila_Grid.Cells[5].Text = Valor.Trim();
                }
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Listado_Detalles_Recepcion
        ///DESCRIPCIÓN: Llena la Tabla con los detalles de las Partes a Revisar
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 05/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Llenar_Grid_Listado_Detalles_Revision() {
            Grid_Listado_Detalles_Recepcion.Columns[0].Visible = true;
            Grid_Listado_Detalles_Recepcion.Columns[1].Visible = true;
            Grid_Listado_Detalles_Recepcion.Columns[2].Visible = true;
            Grid_Listado_Detalles_Recepcion.DataSource = Crear_Tabla_Detalles();
            Grid_Listado_Detalles_Recepcion.DataBind();
            Agrupar_Partes_Listado_Detalles(ref Grid_Listado_Detalles_Recepcion);
            Grid_Listado_Detalles_Recepcion.Columns[0].Visible = false;
            Grid_Listado_Detalles_Recepcion.Columns[1].Visible = false;
            Grid_Listado_Detalles_Recepcion.Columns[2].Visible = false;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Agrupar_Partes_Listado_Detalles
        ///DESCRIPCIÓN: Unifica las Celdas
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 05/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Agrupar_Partes_Listado_Detalles(ref GridView Grid_Listado) {
            Boolean Primer_Color = true;
            foreach (GridViewRow Fila_Actual in Grid_Listado.Rows) { 
                if (!String.IsNullOrEmpty(HttpUtility.HtmlDecode(Fila_Actual.Cells[2].Text).Trim())) {
                    Int32 Cant_Unificar = Convert.ToInt32(HttpUtility.HtmlDecode(Fila_Actual.Cells[2].Text).Trim());
                    if (Cant_Unificar > 0) {
                        TableCell Celda = Fila_Actual.Cells[3];
                        Celda.RowSpan = Cant_Unificar;
                        if (Primer_Color) { Celda.BackColor = System.Drawing.Color.White; Primer_Color = false; }
                        else { Celda.BackColor = System.Drawing.Color.FromArgb(230, 230, 230); Primer_Color = true; }
                        Celda.BorderColor = System.Drawing.Color.FromArgb(47, 78, 125);
                        Celda.BorderWidth = Convert.ToInt32(2);
                    } else {
                        TableCell Celda = Fila_Actual.Cells[3];
                        Celda.Visible = false;
                    } 
                    Fila_Actual.BorderColor = System.Drawing.Color.FromArgb(47, 78, 125);
                    Fila_Actual.BorderWidth = Convert.ToInt32(1);
                }
            }
        }
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Crear_Tabla_Detalles
        ///DESCRIPCIÓN: Se crea la Tabla DataTable de los Detalles
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 09/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private DataTable Crear_Tabla_Detalles()
        {
            DataTable Dt_Detalles = new DataTable();
            Dt_Detalles.Columns.Add("DETALLE_ID", Type.GetType("System.String"));
            Dt_Detalles.Columns.Add("SUBDETALLE_ID", Type.GetType("System.String"));
            Dt_Detalles.Columns.Add("CANT_UNIFICAR", Type.GetType("System.Int32"));
            Dt_Detalles.Columns.Add("NOMBRE_DETALLE", Type.GetType("System.String"));
            Dt_Detalles.Columns.Add("NOMBRE_SUBDETALLE", Type.GetType("System.String"));
            Dt_Detalles.Columns.Add("VALOR", Type.GetType("System.String"));

            Cls_Cat_Tal_Detalles_Rev_Mec_Negocio Negocio = new Cls_Cat_Tal_Detalles_Rev_Mec_Negocio();
            Negocio.P_Estatus = "VIGENTE";
            Negocio.P_Tipo = "PARTE";
            DataTable Dt_Parents = Negocio.Consultar_Detalles_Rev_Mec();
            foreach (DataRow Fila_Parent in Dt_Parents.Rows)
            {
                String Parte_ID = Fila_Parent["DETALLE_ID"].ToString();
                String Nombre_Parte = Fila_Parent["NOMBRE"].ToString();
                Cls_Cat_Tal_Detalles_Rev_Mec_Negocio Tmp = new Cls_Cat_Tal_Detalles_Rev_Mec_Negocio();
                Tmp.P_Estatus = "VIGENTE";
                Tmp.P_Tipo = "SUBPARTE";
                Tmp.P_Parent = Parte_ID;
                DataTable Dt_Childs = Tmp.Consultar_Detalles_Rev_Mec();
                Boolean Cant_Unificar = true;
                foreach (DataRow Fila_Child in Dt_Childs.Rows)
                {
                    DataRow Fila_Nueva = Dt_Detalles.NewRow();
                    Fila_Nueva["DETALLE_ID"] = Parte_ID;
                    Fila_Nueva["SUBDETALLE_ID"] = Fila_Child["DETALLE_ID"].ToString();
                    if (Cant_Unificar)
                    {
                        Fila_Nueva["CANT_UNIFICAR"] = Dt_Childs.Rows.Count;
                        Cant_Unificar = false;
                    }
                    Fila_Nueva["NOMBRE_DETALLE"] = Nombre_Parte;
                    Fila_Nueva["NOMBRE_SUBDETALLE"] = Fila_Child["NOMBRE"].ToString();
                    Dt_Detalles.Rows.Add(Fila_Nueva);
                }
            }
            return Dt_Detalles;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
        ///DESCRIPCIÓN: caraga el data set fisico con el cual se genera el Reporte especificado
        ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
        ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
        ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
        ///CREO: Susana Trigueros Armenta.
        ///FECHA_CREO: 01/Mayo/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        private void Generar_Reporte(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, String Nombre_Reporte) {
            ReportDocument Reporte = new ReportDocument();
            String File_Path = Server.MapPath("../Rpt/Taller_Mecanico/" + Nombre_Reporte);
            Reporte.Load(File_Path);
            String Nombre_Reporte_Generar = "Rpt_Tal_Solicitud_Servicio_" + Cls_Sessiones.No_Empleado + "_" + Convert.ToString(DateTime.Now.ToString("yyyy'-'MM'-'dd'_t'HH'-'mm'-'ss")) + ".pdf";
            String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
            Ds_Reporte = Data_Set_Consulta_DB;
            Reporte.SetDataSource(Ds_Reporte);
            ExportOptions Export_Options = new ExportOptions();
            DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
            Disk_File_Destination_Options.DiskFileName = Server.MapPath(Ruta);
            Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
            Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
            Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
            Reporte.Export(Export_Options);
            Mostrar_Reporte(Nombre_Reporte_Generar, "PDF");
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
        }

        /// *************************************************************************************
        /// NOMBRE:              Mostrar_Reporte
        /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
        /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
        ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
        /// USUARIO CREO:        Juan Alberto Hernández Negrete.
        /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
        /// USUARIO MODIFICO:    Salvador Hernández Ramírez
        /// FECHA MODIFICO:      23-Mayo-2011
        /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
        /// *************************************************************************************
        protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato) {
            String Pagina = "../../Reporte/";
            try {
                    Pagina = Pagina + Nombre_Reporte_Generar;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                    "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            } catch (Exception Ex) {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
        }

    #endregion
    
    #region Grids
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Solicitudes_PageIndexChanging
        ///DESCRIPCIÓN: Maneja la paginación del Listado
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Solicitudes_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            try {
                Grid_Listado_Solicitudes.SelectedIndex = (-1);
                Grid_Listado_Solicitudes.PageIndex = e.NewPageIndex;
                Llenar_Listado_Solicitudes();
            } catch(Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Solicitudes_SelectedIndexChanged
        ///DESCRIPCIÓN: Obtiene los datos de una Solicitud 
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Solicitudes_SelectedIndexChanged(object sender, EventArgs e) {
            try{
                if (Grid_Listado_Solicitudes.SelectedIndex > (-1)){
                    Limpiar_Formulario();
                    Hdf_No_Solicitud.Value = Grid_Listado_Solicitudes.SelectedRow.Cells[1].Text.Trim();
                    Configuracion_Formulario("MOSTRAR INFORMACION");
                    Mostrar_Registro();
                    Grid_Listado_Solicitudes.SelectedIndex = -1;
                    System.Threading.Thread.Sleep(500);
                }
            } catch(Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }

    #endregion

    #region Eventos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Actualizar_Listado_Click
        ///DESCRIPCIÓN: Ejecuta el Proceso para Salir.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Actualizar_Listado_Click(object sender, ImageClickEventArgs e) {
            try {
                Llenar_Listado_Solicitudes();
            }catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                Lbl_Mensaje_Error.Text = "Excepción ['" + Ex.Message + "']";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Ejecuta el Proceso para Salir.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e) {
            try {
                if (Btn_Salir.AlternateText.Trim().Equals("Salir")) {
                    if (Div_Campos.Visible) {
                        Limpiar_Formulario();
                        Configuracion_Formulario("INICIAL");
                    } else {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                } else {
                    Limpiar_Formulario();
                    Configuracion_Formulario("INICIAL"); 
                }
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                Lbl_Mensaje_Error.Text = "Excepción ['" + Ex.Message + "']";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Mostrar_Detalle_Recepcion_Click
        ///DESCRIPCIÓN: Muestra los detalles de la Recepción.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 05/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Mostrar_Detalle_Recepcion_Click(object sender, EventArgs e) {
            MPE_Listado_Detalles.Show();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Imprimir_Solicitud_Servicio_Click
        ///DESCRIPCIÓN: Imprime la Solicitud
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 23/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Imprimir_Solicitud_Servicio_Click(object sender, ImageClickEventArgs e) { 
            try {
                if (Hdf_No_Solicitud.Value.Trim().Length > 0) {
                    Cls_Rpt_Tal_Solicitud_Servicio_Negocio Rpt_Solicitud = new Cls_Rpt_Tal_Solicitud_Servicio_Negocio();
                    Rpt_Solicitud.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value.Trim());
                    Rpt_Solicitud.P_Server_ = this.Server;
                    DataSet Ds_Consulta = Rpt_Solicitud.Crear_Reporte_Solicitud_Servicio();
                    if (Ds_Consulta != null) {
                        Generar_Reporte(Ds_Consulta, new Ds_Rpt_Tal_Revista_Mecanica_Mejorada(), "Rpt_Tal_Revista_Mecanica_Mejorada.rpt");
                    }
                } else {
                    Lbl_Ecabezado_Mensaje.Text = "No hay Solicitud Seleccionada para Imprimir.";
                    Lbl_Mensaje_Error.Text = ""; 
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                Lbl_Mensaje_Error.Text = "Excepción ['" + Ex.Message + "']";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

    #endregion

}