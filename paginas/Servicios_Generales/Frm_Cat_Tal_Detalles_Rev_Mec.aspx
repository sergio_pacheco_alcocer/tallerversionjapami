<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Cat_Tal_Detalles_Rev_Mec.aspx.cs" Inherits="paginas_Taller_Mecanico_Frm_Cat_Tal_Detalles_Rev_Mec" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script language="javascript" type="text/javascript">
        function expandcollapse(obj,row){
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);
    
            if (div.style.display == "none"){
                div.style.display = "inline";
                if (row == 'alt'){
                    img.src = "../imagenes/paginas/minus.gif";
                }else{
                    img.src = "../imagenes/paginas/minus.gif";
                }
                img.alt = "Ocultar";
            }else{
                div.style.display = "none";
                if (row == 'alt'){
                    img.src = "../imagenes/paginas/plus.gif";
                }else{
                    img.src = "../imagenes/paginas/plus.gif";
                }
                img.alt = "Mostrar";
            }
        } 
    </script>        
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">

    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>                     
            </asp:UpdateProgress>
            <div id="Div_Requisitos" style="background-color:#ffffff; width:100%; height:100%;">
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo" colspan="2">Cat�logo Detalles Revista Mec�nica</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                          <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;">              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top">
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                            </table>                   
                          </div>                
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>                        
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" style="width:50%;">
                            <asp:ImageButton ID="Btn_Nuevo" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" Width="24px" CssClass="Img_Button" 
                                AlternateText="Nuevo" OnClick="Btn_Nuevo_Click"/>
                            <asp:ImageButton ID="Btn_Modificar" runat="server"
                                ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" Width="24px" CssClass="Img_Button"
                                AlternateText="Modificar" onclick="Btn_Modificar_Click" />
                            <asp:ImageButton ID="Btn_Eliminar" runat="server"
                                ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png" Width="24px" CssClass="Img_Button"
                                AlternateText="Eliminar" OnClientClick="return confirm('�Esta seguro que desea eliminar el registro de la Base de Datos?');"
                                onclick="Btn_Eliminar_Click"/>
                            <asp:ImageButton ID="Btn_Salir" runat="server"
                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" Width="24px" CssClass="Img_Button"
                                AlternateText="Salir" onclick="Btn_Salir_Click"/>
                        </td>
                        <td style="width:50%;">Busqueda
                            <asp:TextBox ID="Txt_Busqueda" runat="server" Width="130px"></asp:TextBox>
                            <asp:ImageButton ID="Btn_Buscar" runat="server" CausesValidation="false"
                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" ToolTip="Buscar" 
                                onclick="Btn_Buscar_Click"  AlternateText="Consultar" />
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda" runat="server" 
                                WatermarkText="<Descripcion>" TargetControlID="Txt_Busqueda" 
                                WatermarkCssClass="watermarked"></cc1:TextBoxWatermarkExtender>    
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda" runat="server" 
                                TargetControlID="Txt_Busqueda" InvalidChars="<,>,&,',!," 
                                FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                ValidChars="��.,:;()���������� ">
                            </cc1:FilteredTextBoxExtender>                                  
                        </td>                        
                    </tr>
                </table>   
                <br />
                <center>
                    <table width="98%" class="estilo_fuente">
                        <tr>
                            <td colspan="2">
                                <asp:HiddenField ID="Hdf_Detalle_ID" runat="server" />
                            </td>
                        </tr>                                
                        <tr>
                            <td style="width:15%; text-align:left; ">
                               <asp:Label ID="Lbl_Detalle_ID" runat="server" Text="Detalle ID" ></asp:Label>
                            </td>
                            <td style="width:35%; text-align:left;">
                                <asp:TextBox ID="Txt_Detalle_ID" runat="server" Width="98%" MaxLength="50" Enabled="false"></asp:TextBox>
                            </td>
                            <td style="width:15%;">
                                &nbsp;&nbsp;
                               <asp:Label ID="Lbl_Estatus" runat="server" Text="Estatus"></asp:Label>
                            </td>
                            <td style="width:35%; text-align:left; vertical-align:top;">
                                <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="98%">
                                    <asp:ListItem Text ="&lt;SELECCIONE&gt;" Value="SELECCIONE"></asp:ListItem>
                                    <asp:ListItem Text ="VIGENTE" Value="VIGENTE"></asp:ListItem>
                                    <asp:ListItem Text ="BAJA" Value="BAJA"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>                          
                        <tr>
                            <td style="width:15%; text-align:left; ">
                               <asp:Label ID="Lbl_Nombre" runat="server" Text="Nombre" ></asp:Label>
                            </td>
                            <td style="text-align:left;" colspan="3">
                                <asp:TextBox ID="Txt_Nombre" runat="server" Width="98%" MaxLength="50" Enabled="false"></asp:TextBox>
                            </td>
                        </tr> 
                        <tr>
                            <td style="width:13%; text-align:left; vertical-align:top;">
                               <asp:Label ID="Lbl_Tipo" runat="server" Text="Tipo"></asp:Label>
                            </td>
                            <td style="width:37%; text-align:left; vertical-align:top;">
                                <asp:DropDownList ID="Cmb_Tipo" runat="server" Width="100%" OnSelectedIndexChanged="Cmb_Tipo_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Text ="&lt;SELECCIONE&gt;" Value=""></asp:ListItem>
                                    <asp:ListItem Text ="PARTE" Value="PARTE"></asp:ListItem>
                                    <asp:ListItem Text ="SUBPARTE" Value="SUBPARTE"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width:13%; text-align:left; vertical-align:top;">
                                &nbsp;&nbsp;
                               <asp:Label ID="Lbl_Parent" runat="server" Text="Parent"></asp:Label>
                            </td>
                            <td style="width:37%; text-align:left; vertical-align:top;">
                                <asp:DropDownList ID="Cmb_Parent" runat="server" Width="100%">
                                    <asp:ListItem Text ="&lt;SELECCIONE&gt;" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                    <br />                                
                    <asp:GridView ID="Grid_Listado" runat="server" CssClass="GridView_1"
                        AutoGenerateColumns="False" AllowPaging="True" PageSize="10" Width="97%"
                        GridLines= "None"
                        onselectedindexchanged="Grid_Listado_SelectedIndexChanged" 
                        OnRowDataBound="Grid_Listado_RowDataBound"
                        onpageindexchanging="Grid_Listado_PageIndexChanging">
                        <RowStyle CssClass="GridItem" />
                        <Columns>
                            <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png" >
                                <ItemStyle Width="30px" />
                            </asp:ButtonField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <a href="javascript:expandcollapse('div<%# Eval("DETALLE_ID") %>', 'one');">
                                        <img id="imgdiv<%# Eval("DETALLE_ID") %>" alt="Mostrar Entradas"  width="9px" border="0" src="../imagenes/paginas/plus.gif"/>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="DETALLE_ID" HeaderText="Detalle ID" SortExpression="DETALLE_ID" NullDisplayText="-" >
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="100px"  />
                            </asp:BoundField>
                            <asp:BoundField DataField="NOMBRE" HeaderText="Nombre" SortExpression="NOMBRE" NullDisplayText="-" >
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS" NullDisplayText="-"  >
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                <tr>
                                    <td colspan= "100%" style="left:auto;">
                                        <div id="div<%# Eval("DETALLE_ID") %>" style="display:none; left:115px; width:97%; "  >
                                            <asp:GridView ID="Grid_SubListado" runat="server" CellPadding="5" OnSelectedIndexChanged="Grid_SubListado_SelectedIndexChanged"
                                                          CssClass="GridView" AutoGenerateColumns="False" GridLines="None" Width="50%"
                                                           >
                                                <Columns>
                                                    <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                        ImageUrl="~/paginas/imagenes/gridview/blue_button.png"  >
                                                        <ItemStyle Width="30px" />
                                                    </asp:ButtonField>
                                                    <asp:BoundField DataField="DETALLE_ID" HeaderText="Detalle ID" 
                                                        SortExpression="DETALLE_ID"  >
                                                        <ItemStyle Width="90px" HorizontalAlign="Center" Font-Size="X-Small" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="NOMBRE" HeaderText="Nombre" 
                                                        SortExpression="NOMBRE"  >
                                                        <ItemStyle Font-Size="X-Small" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" 
                                                        SortExpression="ESTATUS" >
                                                        <ItemStyle Width="70px" HorizontalAlign="Center" Font-Size="X-Small" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="GridAltItem" /> 
                                                <FooterStyle CssClass="GridPager" />
                                                <HeaderStyle CssClass="GridHeader" />                                
                                                <PagerStyle CssClass="GridPager" />
                                                <RowStyle CssClass="GridItem" /> 
                                                <SelectedRowStyle CssClass="GridSelected" />
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle CssClass="GridHeader" />
                        <SelectedRowStyle CssClass="GridSelected" />
                        <HeaderStyle CssClass="GridHeader" />                                
                        <AlternatingRowStyle CssClass="GridAltItem" />       
                    </asp:GridView>
                </center>        
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

