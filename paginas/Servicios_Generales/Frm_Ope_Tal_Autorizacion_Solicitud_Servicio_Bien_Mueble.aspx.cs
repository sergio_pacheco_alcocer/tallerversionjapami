﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Control_Patrimonial_Operacion_Vehiculos.Negocio;
using JAPAMI.Registro_Peticion.Datos;

public partial class paginas_Servicios_Generales_Frm_Ope_Tal_Autorizacion_Solicitud_Servicio_Bien_Mueble : System.Web.UI.Page {

    #region Page_Load
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN: Carga la Pagina Inicial
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e) {
            Div_Contenedor_Msj_Error.Visible = false;
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack) {
                Llenar_Combo_Gpos_Unidades_Responsables();
                Llenar_Combo_Unidades_Responsables();
                Grid_Listado_Solicitudes.PageIndex = 0;
                Llenar_Listado_Solicitudes();
                Configuracion_Formulario("INICIAL");
                Cmb_Unidad_Responsable.Enabled = false;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Grupo_Dependencia
        ///DESCRIPCIÓN: Consulta el Grupo al que pertenece una dependencia
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected String Consultar_Grupo_Dependencia(String Dependencia_ID) {
            String Grupo_ID = String.Empty;
            Cls_Ope_Tal_Consultas_Generales_Negocio Cls_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            Cls_Negocio.P_Dependencia_ID = Dependencia_ID;
            DataTable Dt_Grupos_Dependencias = Cls_Negocio.Consultar_Grupos_Unidades_Responsables();
            if (Dt_Grupos_Dependencias.Rows.Count > 0) Grupo_ID = Dt_Grupos_Dependencias.Rows[0]["GRUPO_DEPENDENCIA_ID"].ToString().Trim();
            return Grupo_ID;
        }

    #endregion

    #region Metodos

        #region Llenado de Campos [Combos, Listados, Vehiculos]

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Gpos_Unidades_Responsables
            ///DESCRIPCIÓN: Se llena el Combo de los Gpos. Unidades Responsables.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 30/Abril/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Combo_Gpos_Unidades_Responsables()
            {
                Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                Negocio.P_Estatus = "ACTIVO";
                DataTable Dt_Gpos_Dependencias = Negocio.Consultar_Grupos_Unidades_Responsables();
                Cmb_Grupo_UR.DataSource = Dt_Gpos_Dependencias;
                Cmb_Grupo_UR.DataTextField = "CLAVE_NOMBRE";
                Cmb_Grupo_UR.DataValueField = "GRUPO_DEPENDENCIA_ID";
                Cmb_Grupo_UR.DataBind();
                Cmb_Grupo_UR.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Generar_Tabla_Informativa
            /// DESCRIPCION: Genera la tabla para almacenar los datos de las fechas de los cambios de llantas o baterias
            /// PARAMETROS: 
            /// CREO: Jesus Toledo Rodriguez
            /// FECHA_CREO: 10-Oct-2012
            /// MODIFICO:
            /// FECHA_MODIFICO:
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private DataTable Generar_Tabla_Informativa()
            {
                DataTable Tabla_Nueva = new DataTable();
                DataColumn Columna0_Cambio;
                DataColumn Columna1_Fecha_Cambio;

                try
                {
                    // ---------- Inicializar columnas
                    Columna0_Cambio = new DataColumn();
                    Columna0_Cambio.DataType = System.Type.GetType("System.String");
                    Columna0_Cambio.ColumnName = "CAMBIO";
                    Tabla_Nueva.Columns.Add(Columna0_Cambio);
                    Columna1_Fecha_Cambio = new DataColumn();
                    Columna1_Fecha_Cambio.DataType = System.Type.GetType("System.String");
                    Columna1_Fecha_Cambio.ColumnName = "FECHA_CAMBIO";
                    Tabla_Nueva.Columns.Add(Columna1_Fecha_Cambio);

                    return Tabla_Nueva;
                }
                catch (Exception ex)
                {
                    throw new Exception("Generar_Tabla_Documentos " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Unidades_Responsables
            ///DESCRIPCIÓN: Se llena el Combo de las Unidades Responsables.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Combo_Unidades_Responsables() {
                Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                Negocio.P_Estatus = "ACTIVO";
                Cmb_Unidad_Responsable.DataSource = Negocio.Consultar_Unidades_Responsables();
                Cmb_Unidad_Responsable.DataTextField = "CLAVE_NOMBRE";
                Cmb_Unidad_Responsable.DataValueField = "DEPENDENCIA_ID";
                Cmb_Unidad_Responsable.DataBind();
                Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Solicitudes
            ///DESCRIPCIÓN: Se llena el Listado de las Solicitudes.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Listado_Solicitudes() {
                Cls_Ope_Tal_Solicitud_Servicio_Negocio Negocio = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                Negocio.P_Estatus = "GENERADA";
                Negocio.P_Tipo_Bien = "BIEN_MUEBLE";
                DataTable Dt_Resultados = Negocio.Consultar_Listado_Solicitudes_Servicio();
                Grid_Listado_Solicitudes.Columns[1].Visible = true;
                Grid_Listado_Solicitudes.DataSource = Dt_Resultados;
                Grid_Listado_Solicitudes.DataBind();
                Grid_Listado_Solicitudes.Columns[1].Visible = false;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Bien_Mueble
            ///DESCRIPCIÓN: Se cargan los Datos del Bien Mueble Seleccionado.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Cargar_Datos_Bien_Mueble(String Bien_Mueble, String Tipo_Busqueda) {
                Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                switch (Tipo_Busqueda) { 
                    case "NO_INVENTARIO":
                        Consulta_Negocio.P_No_Inventario = Bien_Mueble;
                        break;
                    case "IDENTIFICADOR":
                        Consulta_Negocio.P_Bien_Mueble_ID = Bien_Mueble;
                        break;
                    default: break;
                }
                DataTable Dt_Bienes_Muebles = Consulta_Negocio.Consultar_Bien_Mueble();
                if (Dt_Bienes_Muebles.Rows.Count > 0) {
                    Hdf_Bien_Mueble_ID.Value = Dt_Bienes_Muebles.Rows[0]["BIEN_MUEBLE_ID"].ToString().Trim();
                    Txt_No_Inventario_BM.Text = Dt_Bienes_Muebles.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                    Txt_Numero_Serie_Bien.Text = Dt_Bienes_Muebles.Rows[0]["NUMERO_SERIE"].ToString().Trim();
                    Txt_Descripcion_Bien.Text = Dt_Bienes_Muebles.Rows[0]["DESCRIPCION_BIEN_MUEBLE"].ToString().Trim();
                    if (!String.IsNullOrEmpty(Dt_Bienes_Muebles.Rows[0]["DEPENDENCIA_ID"].ToString())) {
                        Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Bienes_Muebles.Rows[0]["DEPENDENCIA_ID"].ToString()));
                        Cmb_Grupo_UR.SelectedIndex = Cmb_Grupo_UR.Items.IndexOf(Cmb_Grupo_UR.Items.FindByValue(Consultar_Grupo_Dependencia(Cmb_Unidad_Responsable.SelectedItem.Value.Trim())));
                    } else {
                        Lbl_Mensaje_Error.Text = "ERROR [Puede ser que el Bien esté dado de Baja].";
                        Div_Contenedor_Msj_Error.Visible = true;
                        Cmb_Unidad_Responsable.SelectedIndex = 0;
                    }
                } else {
                    Lbl_Ecabezado_Mensaje.Text = "El Bien no se encontro.";
                    if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Lbl_Mensaje_Error.Text = "[Puede ser que no este asignado a la Unidad Responsable Seleccionada]."; }
                    else { Lbl_Mensaje_Error.Text = ""; }
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }

        #endregion

        #region Generales [Configuracion, Limpiar]
                
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
            ///DESCRIPCIÓN: Limpia los campos del Formulario.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Limpiar_Formulario() {
                Hdf_No_Solicitud.Value = "";
                Hdf_Tipo_Solicitud.Value = "";
                Hdf_Correo_Electronico.Value = "";
                Txt_Fecha_Elaboracion.Text = "";
                Cmb_Tipo_Servicio.SelectedIndex = 0;
                Cmb_Unidad_Responsable.SelectedIndex = 0;
                Hdf_Bien_Mueble_ID.Value = "";
                Txt_No_Inventario_BM.Text = "";
                Txt_Descripcion_Bien.Text = "";
                Txt_Numero_Serie_Bien.Text = "";
                Txt_Descripcion_Servicio.Text = "";
                Cmb_Resultado.SelectedIndex = 0;
                Txt_Comentarios.Text = "";
                Txt_Tipo_Bien.Text = "";
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
            ///DESCRIPCIÓN: Se Habilitan y/o inhabilitan los campos dependendiendo de la Condicion.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Configuracion_Formulario(String Operacion) {
                switch (Operacion) {
                    case "INICIAL":
                        Div_Campos.Visible = false;
                        Div_Listado_Solicitudes.Visible = true;
                        Btn_Ejecutar_Autorizacion.Visible = false;
                        break;
                    case "OPERACION":
                        Div_Campos.Visible = true;
                        Div_Listado_Solicitudes.Visible = false;
                        Btn_Ejecutar_Autorizacion.Visible = false;
                        break;
                }
            }
    
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Enviar_Correo
            ///DESCRIPCIÓN: Envia un correo a el encargado de Almacen para notificar que una solicitud ha sido creada 
            ///             o que hay alguna que no ha sido revisada.
            ///PROPIEDADES: 
            ///             1.  cabecera.       Número de Solicitud de la cual se quieren obtener sus detalles.
            ///CREO: 
            ///FECHA_CREO: 
            ///MODIFICO:    Francisco Antonio Gallardo Castañeda.
            ///FECHA_MODIFICO:  Junio 2010.
            ///CAUSA_MODIFICACIÓN:  Se adapto para que el funcionamiento del Catalogo de Solicitud de Apartado. 
            ///*******************************************************************************
            public void Enviar_Correo(Cls_Ope_Tal_Solicitud_Servicio_Negocio Parametros) {
                String Contenido = "";
                if (Parametros.P_Estatus.Trim().Equals("AUTORIZADA")) {
                    Contenido = "La Solicitud ya fue revisada y ha sido AUTORIZADA para atenderse quedando como No. de Folio: " + Parametros.P_Folio_Solicitud.Trim() + ", programandosele la cita el día: ";
                    Contenido = Contenido + String.Format("{0:dd 'de' MMMMMMMMMMMMMMMMMMMMMMMMMMMM 'de' yyyy}", Parametros.P_Fecha_Recepcion_Programada).ToUpper();
                } else {
                    Contenido = "La Solicitud ya fue revisada y ha sido RECHAZADA para atenderse, el motivo del Rechazo es por: ";
                    Contenido = Contenido + Parametros.P_Motivo_Rechazo.Trim();
                }
                try {
                    if (Hdf_Correo_Electronico.Value.Trim().Length > 0) { 
                        Cls_Mail mail = new Cls_Mail();
                        mail.P_Servidor = Cls_Cat_Ate_Peticiones_Datos.Consulta_Parametros().Rows[0][Apl_Parametros.Campo_Servidor_Correo].ToString();
                        mail.P_Envia = Cls_Cat_Ate_Peticiones_Datos.Consulta_Parametros().Rows[0][Apl_Parametros.Campo_Correo_Saliente].ToString();
                        mail.P_Password = Cls_Cat_Ate_Peticiones_Datos.Consulta_Parametros().Rows[0][Apl_Parametros.Campo_Password_Correo].ToString();
                        mail.P_Recibe = Hdf_Correo_Electronico.Value.Trim();
                        mail.P_Subject = "Respuesta a Solicitud para Bien con No. Inventario: " + Txt_No_Inventario_BM.Text.Trim();
                        mail.P_Texto = Contenido;
                        mail.P_Adjunto = null;//Hacer_Pdf();
                        mail.Enviar_Correo();
                    }
                } catch (Exception Ex) {
                    Lbl_Ecabezado_Mensaje.Text = "No se pudo enviar el Correo.";
                    Lbl_Mensaje_Error.Text = "[En la Bandeja del Usuario Solicito el Servicio aparecera como AUTORIZADA la Solicitud].";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }

        #endregion

        #region Clase de Negocio de Solicitudes [Autorizar, Rechazar y Consulta]
            
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Ejecutar_Autorizacion
            ///DESCRIPCIÓN: Carga los Datos para hacer la autorizacion de la solicitud
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private Cls_Ope_Tal_Solicitud_Servicio_Negocio Ejecutar_Autorizacion() {
                Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                Solicitud.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                Solicitud.P_Estatus = Cmb_Resultado.SelectedItem.Value.Trim();
                Solicitud.P_Motivo_Rechazo = Txt_Comentarios.Text.Trim();
                Solicitud.P_Empleado_Autorizo_ID = Cls_Sessiones.Empleado_ID;
                Solicitud.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                Solicitud.P_Tipo_Servicio = Cmb_Tipo_Servicio.SelectedItem.Value;
                if (Hdf_Tipo_Solicitud.Value.Trim().Equals("SOLICITUD")) {
                    Solicitud.Autorizacion_Solicitud_Servicio();
                } else if (Hdf_Tipo_Solicitud.Value.Trim().Equals("INTERNA")) {
                    Solicitud.Autorizacion_Solicitud_Servicio_Interno();
                }
                return Solicitud;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Mostrar_Registro
            ///DESCRIPCIÓN: Muestra el Registro en los campos.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Mostrar_Registro() {
                Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                Solicitud.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                Solicitud = Solicitud.Consultar_Detalles_Solicitud_Servicio(); 
                if (Solicitud.P_No_Solicitud > (-1)) {
                    Hdf_No_Solicitud.Value = Solicitud.P_No_Solicitud.ToString();
                    Txt_Fecha_Elaboracion.Text = String.Format("{0:dd/MMM/yyyy}", Solicitud.P_Fecha_Elaboracion);
                    Cmb_Tipo_Servicio.SelectedIndex = Cmb_Tipo_Servicio.Items.IndexOf(Cmb_Tipo_Servicio.Items.FindByValue(Solicitud.P_Tipo_Servicio));
                    Cmb_Grupo_UR.SelectedIndex = Cmb_Grupo_UR.Items.IndexOf(Cmb_Grupo_UR.Items.FindByValue(Consultar_Grupo_Dependencia(Solicitud.P_Dependencia_ID)));
                    Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Solicitud.P_Dependencia_ID));
                    Hdf_Bien_Mueble_ID.Value = Solicitud.P_Bien_ID;
                    Txt_Descripcion_Servicio.Text = Solicitud.P_Descripcion_Servicio;
                    Hdf_Correo_Electronico.Value = Solicitud.P_Correo_Electronico.Trim();
                    Txt_Tipo_Bien.Text = "BIEN MUEBLE";
                    Cargar_Datos_Bien_Mueble(Hdf_Bien_Mueble_ID.Value, "IDENTIFICADOR");
                    Hdf_Tipo_Solicitud.Value = (Solicitud.P_Procedencia.Trim().Equals("SOLICITUD")) ? "SOLICITUD" : "INTERNA";
                }
            }

        #endregion

        #region Validaciones

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Autorizacion
            ///DESCRIPCIÓN: Valida la Solicitud de Servicio antes de ser Autorizada
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private Boolean Validar_Autorizacion() { 
                Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                String Mensaje_Error = "";
                Boolean Validacion = true;
                if (Cmb_Resultado.SelectedIndex == 0) { 
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar el Resultado de la Autorizacion.";
                    Mensaje_Error = Mensaje_Error + " <br />";
                    Validacion = false;
                }
                if (Cmb_Resultado.SelectedItem.Value.Equals("AUTORIZADA") && Hdf_Tipo_Solicitud.Value.Trim().Equals("SOLICITUD")) { 
                    if (Cmb_Tipo_Servicio.SelectedIndex == 0) {
                        Mensaje_Error = Mensaje_Error + "+ Seleccionar el Tipo de Servicio.";
                        Mensaje_Error = Mensaje_Error + " <br />";
                        Validacion = false;
                    }
                }
                if (Cmb_Resultado.SelectedItem.Value.Equals("RECHAZADA")) { 
                    if (Txt_Comentarios.Text.Trim().Length == 0) {
                        Mensaje_Error = Mensaje_Error + "+ Introducir los Comentarios del porque se rechaza.";
                        Mensaje_Error = Mensaje_Error + " <br />";
                        Validacion = false;
                    }
                }
                
                if (!Validacion) {
                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
                    Div_Contenedor_Msj_Error.Visible = true;
                }
                return Validacion;
            }

        #endregion

    #endregion
    
    #region Grids
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Solicitudes_PageIndexChanging
        ///DESCRIPCIÓN: Maneja la paginación del Listado
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Solicitudes_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            try {
                Grid_Listado_Solicitudes.SelectedIndex = (-1);
                Grid_Listado_Solicitudes.PageIndex = e.NewPageIndex;
                Llenar_Listado_Solicitudes();
            } catch(Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Solicitudes_SelectedIndexChanged
        ///DESCRIPCIÓN: Obtiene los datos de una Solicitud 
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Solicitudes_SelectedIndexChanged(object sender, EventArgs e) {
            try{
                if (Grid_Listado_Solicitudes.SelectedIndex > (-1)){
                    Limpiar_Formulario();
                    Hdf_No_Solicitud.Value = Grid_Listado_Solicitudes.SelectedRow.Cells[1].Text.Trim();
                    Configuracion_Formulario("OPERACION");
                    Mostrar_Registro();
                    Btn_Ejecutar_Autorizacion.Visible = true;
                    Grid_Listado_Solicitudes.SelectedIndex = -1;
                    System.Threading.Thread.Sleep(500);
                }
            } catch(Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }

    #endregion

    #region Eventos
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Controla las operaciones del Boton de Salir
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Ejecutar_Autorizacion_Click(object sender, ImageClickEventArgs e) {
            if (Validar_Autorizacion()) {
                Cls_Ope_Tal_Solicitud_Servicio_Negocio Resultado = Ejecutar_Autorizacion();
                if (Hdf_Tipo_Solicitud.Value.Trim().Equals("SOLICITUD")) { Enviar_Correo(Resultado); }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Operación de Autorización de Solicitud Exitosa: Solicitud " + Resultado.P_Estatus + "');", true);
                Limpiar_Formulario();
                Llenar_Listado_Solicitudes();
                Configuracion_Formulario("INICIAL");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Controla las operaciones del Boton de Salir
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e) {
            if (Div_Campos.Visible) {
                Limpiar_Formulario();
                Configuracion_Formulario("INICIAL");
            } else {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Actualizar_Listado_Click
        ///DESCRIPCIÓN: Ejecuta el Proceso para Salir.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Actualizar_Listado_Click(object sender, ImageClickEventArgs e) {
            Llenar_Listado_Solicitudes();
        }

    #endregion

}
