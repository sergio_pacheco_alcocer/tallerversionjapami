﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Operacion_Distribuir_RQ_Proveedores.Negocio;
using System.Net.Mail;
using JAPAMI.Constantes;
using JAPAMI.Registro_Peticion.Datos;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using JAPAMI.Sessiones;
using JAPAMI.Taller_Mecanico.Catalogo_Cotizadores.Negocio;
using JAPAMI.Taller_Mecanico.Impresion_Requisiciones.Negocio;
using JAPAMI.Taller_Mecanico.Reporte_Solicitud_Servicio.Negocio;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;

public partial class paginas_Taller_Mecanico_Frm_Ope_Tal_Distribuir_Requisiciones_Prov : System.Web.UI.Page {
    
    ///*******************************************************************************
    ///PAGE_LOAD
    ///*******************************************************************************
    #region Page_Load

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN:Carga Inicial de Pagina
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 14/Oct/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///********************************************************************************/
        protected void Page_Load(object sender, EventArgs e) {
            Div_Contenedor_Msj_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack) {
                ViewState["SortDirection"] = "ASC";
                Llenar_Combo_Cotizadores();
                Configurar_Formulario("Inicio");
                Llenar_Grid_Requisiciones();
                Cargar_Ventana_Emergente_Busqueda_Prov();
                Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio Clase = new Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio();
                //Verificar si su rol es jefe de dependencia, admin de modulo o admin de sistema
                DataTable Dt_Grupo_Rol = Cls_Util.Consultar_Grupo_Rol_ID(Cls_Sessiones.Rol_ID.ToString());
                if (Dt_Grupo_Rol != null) {
                    String Grupo_Rol = Dt_Grupo_Rol.Rows[0][Apl_Cat_Roles.Campo_Grupo_Roles_ID].ToString();
                    if (Grupo_Rol == "00001" || Grupo_Rol == "00002") {
                        Cmb_Cotizadores.Enabled = true;
                    } else {
                        Cmb_Cotizadores.Enabled = false;
                    }
                }            
            }
        }

    #endregion

    ///*******************************************************************************
    ///METODOS
    ///*******************************************************************************
    #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Cotizadores
        ///DESCRIPCIÓN:Llenar_Combo_Cotizadores
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 14/Oct/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///********************************************************************************/
        private void Llenar_Combo_Cotizadores() {
            Cls_Cat_Tal_Cotizadores_Negocio Cot_Negocio = new Cls_Cat_Tal_Cotizadores_Negocio();
            DataTable Dt_Cotizadores = Cot_Negocio.Consulta_Cotizadores();
            if (Dt_Cotizadores != null && Dt_Cotizadores.Rows.Count > 0) {
                Cls_Util.Llenar_Combo_Con_DataTable_Generico
                    (Cmb_Cotizadores, Dt_Cotizadores, Cat_Com_Cotizadores.Campo_Nombre_Completo, Cat_Com_Cotizadores.Campo_Empleado_ID);
                Cmb_Cotizadores.SelectedValue = Cls_Sessiones.Empleado_ID;
            } else {
                Cmb_Cotizadores.Items.Clear();
                Cmb_Cotizadores.Items.Add("COTIZADORES");
                Cmb_Cotizadores.SelectedIndex = 0;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Configurar_Formulario
        ///DESCRIPCIÓN: Metodo que configura el formulario con respecto al estado de habilitado o visible
        ///´de los componentes de la pagina
        ///PARAMETROS: 1.- String Estatus: Estatus que puede tomar el formulario con respecto a sus componentes, ya sea "Inicio" o "Nuevo"
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 01/JULIO/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public void Configurar_Formulario(String Estatus) { 
            switch (Estatus) {
                case "Inicio": 
                    Div_Detalle_Requisicion.Visible = false;
                    Div_Grid_Requisiciones.Visible = true;

                    //Boton Modificar
                    Btn_Nuevo.Visible = true;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Nuevo.Enabled = true;
                    //Boton Salir
                    Btn_Salir.Visible = true;
                    Btn_Salir.ToolTip = "Inicio";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    //
                    Btn_Imprimir.Visible = true;
                    Grid_Requisiciones.Visible = true;
                    Grid_Requisiciones.Enabled = true;
                    Div_Detalle_Requisicion.Visible = false;
                    Btn_Agregar_Proveedor.Enabled = false;
                    Cmb_Proveedores.Enabled = false;
                    Grid_Proveedores.Enabled = false;
                    Btn_Imprimir.Visible = true;
                    Btn_Imprimir.Enabled = true;
                    Chk_Enviar_Correo.Checked = false;
                    Chk_Enviar_Correo.Enabled = false;

                    break;

                case "Nuevo": 
                    Btn_Nuevo.Visible = true;
                    Btn_Nuevo.ToolTip = "Guardar";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Nuevo.Enabled = true;
                    //Boton Salir
                    Btn_Salir.Visible = true;
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    //
                    Btn_Imprimir.Visible = false;
                    Div_Grid_Requisiciones.Visible = false;
                    Div_Detalle_Requisicion.Visible = true;
                    Btn_Agregar_Proveedor.Enabled = true;
                    Cmb_Proveedores.Enabled = true;
                    Grid_Proveedores.Enabled = true;
                    Btn_Imprimir.Visible = true;
                    Btn_Imprimir.Enabled = true;
                    Chk_Enviar_Correo.Checked = false;
                    Chk_Enviar_Correo.Enabled = true;

                    break;
            } 
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Proveedores
        ///DESCRIPCIÓN: Metodo que Consulta los proveedores dados de alta en la tabla CAT_COM_PROVEEDORES
        ///PARAMETROS: 1.- DropDownList Cmb_Combo: combo dentro de la pagina a llenar 
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 01/JULIO/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public void Llenar_Combo_Proveedores(DropDownList Cmb_Combo) {
            Cmb_Combo.Items.Clear();
            Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio Negocios = new Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio();
            //Negocios.P_Concepto_ID = Session["Concepto_ID"].ToString().Trim();
            DataTable Data_Table = Negocios.Consultar_Proveedores();
            Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Proveedores, Data_Table);
            Cmb_Proveedores.SelectedIndex = 0;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Limpiar_Variables_Session
        ///DESCRIPCIÓN: Limpia las Variables de Session.
        ///PARAMETROS: 
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 01/JULIO/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public void Limpiar_Variables_Session() {
            Session["Concepto_ID"] = null;
            Session["No_Requisicion"] = null;
            Session["Dt_Producto_Servicio"] = null;
            Session["Dt_Proveedores_Grid"] = null;
            Session["Dt_Requisiciones"] = null;
            Session["Proveedor_ID"] = null;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Enviar_Correo
        ///DESCRIPCIÓN: Envia un correo a el encargado de Almacen para notificar que una solicitud ha sido creada 
        ///             o que hay alguna que no ha sido revisada.
        ///PROPIEDADES: 
        ///             1.  cabecera.       Número de Solicitud de la cual se quieren obtener sus detalles.
        ///             2.  no_apartado.    Número de Apartado de la cual se le quiere notificar al encargado
        ///                                 del almacen.
        ///             3.  solicito.       Nombre de quien hizo la solicitud del apartado.
        ///             4.  fecha.          Fecha del Proyecto del cual se hace la solicitud de Apartado.
        ///             5.  proyecto.       Proyecto del cual se hace la solicitud de apartado.
        ///             6.  descripcion.    Descripción del Proyecto del cual se hizó la solicitud de apartado.
        ///CREO: 
        ///FECHA_CREO: 
        ///MODIFICO:    Francisco Antonio Gallardo Castañeda.
        ///FECHA_MODIFICO:  Junio 2010.
        ///CAUSA_MODIFICACIÓN:  Se adapto para que el funcionamiento del Catalogo de Solicitud de Apartado. 
        ///*******************************************************************************
        public void Enviar_Correo(String Email,String Correo,String Password,String Direccion_IP) {
            String Texto = "";
            try {
                if (Email != "" && Email != null) {
                    Cls_Mail mail = new Cls_Mail();

                    mail.P_Servidor = Direccion_IP;
                    mail.P_Envia = Correo;
                    mail.P_Password = Password;
                    mail.P_Recibe = Email;
                    mail.P_Subject = "Municipio de Irapuato ( Petición de Cotización )";
                    mail.P_Texto = Texto;
                    mail.P_Adjunto = null;//Hacer_Pdf();
                    mail.Enviar_Correo();
                }
            } catch (Exception Ex) {
                Lbl_Mensaje_Error.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

    #endregion


    ///*******************************************************************************
    ///GRID
    ///*******************************************************************************
    #region Grid

        #region Grid_Requisiciones

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Grid_Requisiciones_SelectedIndexChanged
            ///DESCRIPCIÓN: Evento del Grid_Requisiciones al seleccionar
            ///PARAMETROS:   
            ///CREO: Susana Trigueros Armenta
            ///FECHA_CREO: 01/JULIO/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            protected void Grid_Requisiciones_SelectedIndexChanged(object sender, EventArgs e) {
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = false;
                //creamos la instancia de la clase de negocios
                Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio Clase_Negocio = new Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio();
                
                //Consultamos los datos del producto seleccionado
                GridViewRow Row = Grid_Requisiciones.SelectedRow;
                Clase_Negocio.P_No_Requisicion = Grid_Requisiciones.SelectedDataKey["No_Requisicion"].ToString();
                Session["No_Requisicion"] = Clase_Negocio.P_No_Requisicion;
                //Consultamos los detalles del producto seleccionado 
                DataTable Dt_Detalle_Requisicion = Clase_Negocio.Consultar_Detalle_Requisicion();
                Dt_Detalle_Requisicion.Columns.Add("Elaboro", typeof(System.String));
                Dt_Detalle_Requisicion.AcceptChanges();
                Dt_Detalle_Requisicion.Rows[0]["Elaboro"] = Cls_Sessiones.Nombre_Empleado;
                Session["Dt_Detalle_Requisicion"] = Dt_Detalle_Requisicion;
                //Mostramos el div de detalle y el grid de Requisiciones
                Div_Grid_Requisiciones.Visible = false;
                Div_Detalle_Requisicion.Visible = true;
               
                //llenamos la informacion del detalle de la requisicion seleccionada
                Txt_Dependencia.Text = Dt_Detalle_Requisicion.Rows[0]["DEPENDENCIA"].ToString().Trim();
                Txt_Folio.Text = Dt_Detalle_Requisicion.Rows[0]["FOLIO"].ToString().Trim();
                Txt_Concepto.Text = Dt_Detalle_Requisicion.Rows[0]["CONCEPTO"].ToString().Trim();
                if(!String.IsNullOrEmpty(Dt_Detalle_Requisicion.Rows[0]["FECHA_GENERACION"].ToString().Trim())){
                    Txt_Fecha_Generacion.Text = String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(Dt_Detalle_Requisicion.Rows[0]["FECHA_GENERACION"].ToString().Trim())); 
                }
                Txt_Estatus.Text = Dt_Detalle_Requisicion.Rows[0]["ESTATUS"].ToString().Trim();
                Txt_Justificacion.Text = Dt_Detalle_Requisicion.Rows[0]["JUSTIFICACION_COMPRA"].ToString().Trim();
                Txt_Especificacion.Text = Dt_Detalle_Requisicion.Rows[0]["ESPECIFICACION_PROD_SERV"].ToString().Trim();
                Txt_Subtotal.Text = Dt_Detalle_Requisicion.Rows[0]["SUBTOTAL"].ToString().Trim();
                Txt_IVA.Text = Dt_Detalle_Requisicion.Rows[0]["IVA"].ToString().Trim();
                Txt_Total.Text = Dt_Detalle_Requisicion.Rows[0]["TOTAL"].ToString().Trim();
                Session["Concepto_ID"] = Dt_Detalle_Requisicion.Rows[0]["CONCEPTO_ID"].ToString().Trim();
                
                //VALIDAMOS EL CAMPO DE VERIFICAR CARACTERISTICAS, GARANTIA Y POLIZAS
                if (Dt_Detalle_Requisicion.Rows[0]["VERIFICACION_ENTREGA"].ToString().Trim() == "NO" || Dt_Detalle_Requisicion.Rows[0]["VERIFICACION_ENTREGA"].ToString().Trim() == String.Empty) {
                    Chk_Verificacion.Checked = false;
                }
                if (Dt_Detalle_Requisicion.Rows[0]["VERIFICACION_ENTREGA"].ToString().Trim() == "SI") {
                    Chk_Verificacion.Checked = true;
                }

                //CONSULTAMOS LOS PRODUCTOS DE LA REQUISICION SELECCIONADA
                Clase_Negocio.P_No_Requisicion = Session["No_Requisicion"].ToString().Trim();
                Clase_Negocio.P_Tipo_Articulo = "PRODUCTO";
                DataTable Dt_Producto_Servicio = Clase_Negocio.Consultar_Productos_Servicios();

                if (Dt_Producto_Servicio.Rows.Count != 0) {
                    Session["Dt_Producto_Servicio"] = Dt_Producto_Servicio;
                    Grid_Productos.DataSource = Dt_Producto_Servicio;
                    Grid_Productos.DataBind();
                } else {
                    Grid_Productos.DataSource = new DataTable();
                    Grid_Productos.DataBind();
                }
                //Llenamos el combo de Proveedores
                
                Llenar_Combo_Proveedores(Cmb_Proveedores);

                //CONSULTAMOS LOS PROVEEDORES A LOS CUALES SE LES ENVIO LA SOLICITUD DE COTIZACION
                Clase_Negocio.P_Concepto_ID = Dt_Detalle_Requisicion.Rows[0]["CONCEPTO_ID"].ToString().Trim();
                DataTable Dt_Proveedores = Clase_Negocio.Consultar_Proveedores_Asignados();
                if (Dt_Proveedores.Rows.Count != 0) {
                    //llenamos el grid de proveedores
                    Grid_Proveedores.DataSource = Dt_Proveedores;
                    Grid_Proveedores.DataBind();
                    Session["Dt_Proveedores_Grid"] = Dt_Proveedores;
                } else {
                    Grid_Proveedores.DataSource = new DataTable();
                    Grid_Proveedores.DataBind();
                    Session["Dt_Proveedores_Grid"] = null;
                }

                //Llenamos el Grid de los Comentarios 
                DataTable Dt_Comentarios = Clase_Negocio.Consultar_Comentarios();
                if(Dt_Comentarios.Rows.Count != 0) {
                    Grid_Comentarios.DataSource = Dt_Comentarios;
                    Grid_Comentarios.DataBind();
                } else {
                    Grid_Comentarios.EmptyDataText = "No se han encontrado registros."; 
                    Grid_Comentarios.DataSource = new DataTable();
                    Grid_Comentarios.DataBind();
                }

                Btn_Salir.ToolTip = "Listado";
                Tab_Detalle_Requisicion.ActiveTabIndex = 1;
                Btn_Nuevo_Click(Btn_Nuevo, null);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN:Grid_Requisiciones_Sorting
            ///DESCRIPCIÓN: Evento para ordenar por columna seleccionada en el Grid_Requisiciones
            ///PARAMETROS:
            ///CREO: Susana Trigueros Armenta
            ///FECHA_CREO: 01/JULIO/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            protected void Grid_Requisiciones_Sorting(object sender, GridViewSortEventArgs e) {

                DataTable Dt_Requisiciones = (DataTable)Session["Dt_Requisiciones"];

                if (Dt_Requisiciones != null) {
                    DataView Dv_Requisiciones = new DataView(Dt_Requisiciones);
                    String Orden = ViewState["SortDirection"].ToString();

                    if (Orden.Equals("ASC")) {
                        Dv_Requisiciones.Sort = e.SortExpression + " " + "DESC";
                        ViewState["SortDirection"] = "DESC";
                    } else {
                        Dv_Requisiciones.Sort = e.SortExpression + " " + "ASC";
                        ViewState["SortDirection"] = "ASC";
                    }

                    Grid_Requisiciones.DataSource = Dv_Requisiciones;
                    Grid_Requisiciones.DataBind();
                    //Guardamos el cambio dentro de la variable de session de Dt_Requisiciones
                    Session["Dt_Requisiciones"] = (DataTable)Dv_Requisiciones.Table;
                    Dt_Requisiciones = (DataTable)Session["Dt_Requisiciones"];
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN:Llenar_Grid_Requisiciones
            ///DESCRIPCIÓN: Metodo que permite llenar el Grid_Requisiciones
            ///PARAMETROS:
            ///CREO: Susana Trigueros Armenta
            ///FECHA_CREO: 01/JULIO/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            public void Llenar_Grid_Requisiciones() {
                Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio Clase_Negocio = new Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio();
                //Concultamos las requisiciones
                Clase_Negocio.P_Cotizador_ID = Cmb_Cotizadores.SelectedValue;
                DataTable Dt_Requisiciones = Clase_Negocio.Consultar_Requisiciones();
                if (Dt_Requisiciones.Rows.Count != 0) {
                    Session["Dt_Requisiciones"] = Dt_Requisiciones;
                    Grid_Requisiciones.DataSource = Dt_Requisiciones;
                    Grid_Requisiciones.DataBind();
                } else {
                    Session["Dt_Requisiciones"] = null;
                    Grid_Requisiciones.DataSource = new DataTable();
                    Grid_Requisiciones.DataBind();
                }

            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN:Grid_Requisiciones_RowDataBound
            ///DESCRIPCIÓN: Metodo que permite llenar el Grid_Requisiciones
            ///PARAMETROS:
            ///CREO: Susana Trigueros Armenta
            ///FECHA_CREO: 01/JULIO/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            protected void Grid_Requisiciones_RowDataBound(object sender, GridViewRowEventArgs e) {
                if (e.Row.RowType == DataControlRowType.DataRow) {
                    String Folio = e.Row.Cells[2].Text.Trim();
                    DataRow[] Renglon = ((DataTable)Session["Dt_Requisiciones"]).Select("FOLIO = '" + Folio + "'");
                    if (Renglon.Length > 0) {
                        ImageButton Boton = (ImageButton)e.Row.FindControl("Btn_Alerta");
                        String Estatus = Renglon[0]["ESTATUS"].ToString().Trim();
                       
                        if (Renglon[0]["ALERTA"].ToString().Trim() == "ROJA2") {
                            Boton.ImageUrl = "../imagenes/gridview/circle_red.png";
                            Boton.Visible = true;
                            Boton.ToolTip = "Req. Por Cancelacion Parcial de OC";
                            
                        } else if (Renglon[0]["ALERTA"].ToString().Trim() == "AMARILLO2") {
                            Boton.ImageUrl = "../imagenes/gridview/circle_yellow.png";
                            Boton.Visible = true;
                            Boton.ToolTip = "Req. Cotizada-Rechazada";
                            
                        } else {
                            Boton.Visible = false;
                        } 
                    } 
                }
            }

        #endregion

        #region Grid_Productos

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN:Grid_Productos_Sorting
            ///DESCRIPCIÓN: Evento para ordenar por columna seleccionada en el Grid_Productos
            ///PARAMETROS:
            ///CREO: Susana Trigueros Armenta
            ///FECHA_CREO: 01/JULIO/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            protected void Grid_Productos_Sorting(object sender, GridViewSortEventArgs e) {

                DataTable Dt_Productos = (DataTable)Session["Dt_Producto_Servicio"];

                if (Dt_Productos != null) {
                    DataView Dv_Productos = new DataView(Dt_Productos);
                    String Orden = ViewState["SortDirection"].ToString();

                    if (Orden.Equals("ASC")) {
                        Dv_Productos.Sort = e.SortExpression + " " + "DESC";
                        ViewState["SortDirection"] = "DESC";
                    } else {
                        Dv_Productos.Sort = e.SortExpression + " " + "ASC";
                        ViewState["SortDirection"] = "ASC";
                    }

                    Grid_Productos.DataSource = Dv_Productos;
                    Grid_Productos.DataBind();
                }
            }

        #endregion 

        #region Proveedores

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN:Grid_Proveedores_Sorting
            ///DESCRIPCIÓN: Evento para ordenar por columna seleccionada en el Grid_Proveedores
            ///PARAMETROS:
            ///CREO: Susana Trigueros Armenta
            ///FECHA_CREO: 01/JULIO/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            protected void Grid_Proveedores_Sorting(object sender, GridViewSortEventArgs e) {

                DataTable Dt_Proveedores = (DataTable)Session["Dt_Proveedores_Grid"];

                if (Dt_Proveedores != null) {
                    DataView Dv_Proveedores = new DataView(Dt_Proveedores);
                    String Orden = ViewState["SortDirection"].ToString();

                    if (Orden.Equals("ASC")) {
                        Dv_Proveedores.Sort = e.SortExpression + " " + "DESC";
                        ViewState["SortDirection"] = "DESC";
                    } else {
                        Dv_Proveedores.Sort = e.SortExpression + " " + "ASC";
                        ViewState["SortDirection"] = "ASC";
                    }

                    Grid_Proveedores.DataSource = Dv_Proveedores;
                    Grid_Proveedores.DataBind();
                    //Guardamos el cambio dentro de la variable de session de Dt_Requisiciones
                    Session["Dt_Proveedores_Grid"] = (DataTable)Dv_Proveedores.Table;
                    Dt_Proveedores = (DataTable)Session["Dt_Proveedores_Grid"];
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN:Grid_Proveedores_SelectedIndexChanged
            ///DESCRIPCIÓN: Evento para ordenar por columna seleccionada en el Grid_Proveedores
            ///PARAMETROS:
            ///CREO: Susana Trigueros Armenta
            ///FECHA_CREO: 01/JULIO/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            protected void Grid_Proveedores_SelectedIndexChanged(object sender, EventArgs e) {
                DataRow[] Renglones;
                DataRow Renglon;
                //Obtenemos el Id del producto seleccionado
                GridViewRow selectedRow = Grid_Proveedores.Rows[Grid_Proveedores.SelectedIndex];
                Session["Proveedor_ID"] = Grid_Proveedores.SelectedDataKey["Proveedor_ID"].ToString();
                Renglones = ((DataTable)Session["Dt_Proveedores_Grid"]).Select("PROVEEDOR_ID='" + Session["Proveedor_ID"].ToString() + "'");

                if (Renglones.Length > 0) {
                    Renglon = Renglones[0];
                    DataTable Tabla = (DataTable)Session["Dt_Proveedores_Grid"];
                    Tabla.Rows.Remove(Renglon);
                    //Asignamos el nuevo valor al datatable de Session
                    Session["Dt_Proveedores_Grid"] = Tabla;
                    Grid_Proveedores.SelectedIndex = (-1);
                    Grid_Proveedores.DataSource = Tabla;
                    Grid_Proveedores.DataBind();
                    
                } 
            }

        #endregion

    #endregion

    ///*******************************************************************************
    ///EVENTOS
    ///*******************************************************************************
    #region Eventos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Cmb_Cotizadores_SelectedIndexChanged
        ///DESCRIPCIÓN: Evento del Boton Salir
        ///PARAMETROS:
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 01/JULIO/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Cmb_Cotizadores_SelectedIndexChanged(object sender, EventArgs e) {
            Llenar_Grid_Requisiciones();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Evento del Boton Salir
        ///PARAMETROS:
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 01/JULIO/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            Div_Contenedor_Msj_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            switch (Btn_Salir.ToolTip)
            {
                case "Inicio":
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    //LIMPIAMOS VARIABLES DE SESSION
                    Session["Dt_Requisiciones"] = null;

                    Session["No_Requisicion"] = null;
                    Limpiar_Variables_Session();
                    
                    break;
                case "Cancelar":
                    Configurar_Formulario("Inicio");
                    Llenar_Grid_Requisiciones();
                    Limpiar_Variables_Session();
                    break;
                case "Listado":
                    Configurar_Formulario("Inicio");
                    Llenar_Grid_Requisiciones();
                    Limpiar_Variables_Session();
                    break;
            }
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN : Cargar_Ventana_Emergente_Busqueda_Prov
        ///DESCRIPCIÓN          : Establece el evento onclik del control para abrir la ventana emergente
        ///PARAMETROS: 
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 21/Otubre/2011
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        private void Cargar_Ventana_Emergente_Busqueda_Prov()
        {
            String Ventana_Modal = "Abrir_Ventana_Modal('Ventanas_Emergentes/Frm_Busqueda_Proveedores.aspx";
            String Propiedades = ", 'center:yes;resizable:no;status:no;dialogWidth:680px;dialogHide:true;help:no;scroll:no');";
            Btn_Busqueda_Proveedores.Attributes.Add("onclick", Ventana_Modal + "?Fecha=False'" + Propiedades);
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Borrar_Ventana_Emergente_Busqueda_Prov
        ///DESCRIPCIÓN:  
        ///PARAMETROS: 
        ///CREO: 
        ///FECHA_CREO: 10/Mayo/2011 12:55:33 p.m.
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************   
        private void Borrar_Ventana_Emergente_Busqueda_Prov() {
            Btn_Busqueda_Proveedores.Attributes.Clear();
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
        ///DESCRIPCIÓN:  
        ///PARAMETROS: 
        ///CREO: 
        ///FECHA_CREO: 10/Mayo/2011 12:55:33 p.m.
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************   
        protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
        {
            Div_Contenedor_Msj_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            switch (Btn_Nuevo.ToolTip) {
                case "Nuevo":
                    if (Session["No_Requisicion"] != null) {
                        Configurar_Formulario("Nuevo");
                    } else {
                        Lbl_Mensaje_Error.Text = "Es necesario seleccionar primero la RQ para asignar Proveedor.";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                    break;
                case "Guardar":
                    // Validamos que agregara Proveedores
                    if (Grid_Proveedores.Rows.Count == 0) {
                        Lbl_Mensaje_Error.Text = "Es necesario agregar los provedores";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }

                    if (Div_Contenedor_Msj_Error.Visible == false) {
                        Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio Clase_Negocio = new Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio();
                        Clase_Negocio.P_No_Requisicion = Session["No_Requisicion"].ToString().Trim();
                        Clase_Negocio.P_Tipo_Articulo = "PRODUCTO";
                        Clase_Negocio.P_Dt_Proveedores = (DataTable)Session["Dt_Proveedores_Grid"];

                        //PRIMERO ELIMINAMOS LOS PROVEEDORES QUE HABIAN SIDO AGREGADOS
                        Clase_Negocio.Eliminar_Proveedores();
                        //DAMOS DE ALTA LOS PROVEEDORES
                        Boolean Operacion_Realizada = Clase_Negocio.Alta_Proveedores_Asignados(); 

                        if (Operacion_Realizada) {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Distribuir a Provedores", "alert('Se asignarón las requisiciones a los proveedores exitosamente');", true);
                            //Enviamos los correos al proveedor
                            if (Chk_Enviar_Correo.Checked == true) {
                               //CONSULTAMOS EL CORREO, EL PASSWORD Y LA DIRECCION IP DE LA COTIZADORA
                                DataTable Dt_Datos_Cotizadora = Clase_Negocio.Consultar_Datos_Cotizador();
                                String Correo = Dt_Datos_Cotizadora.Rows[0][Cat_Com_Cotizadores.Campo_Correo].ToString().Trim();
                                String Password = Dt_Datos_Cotizadora.Rows[0][Cat_Com_Cotizadores.Campo_Password_Correo].ToString().Trim();
                                String Direccion_IP = Dt_Datos_Cotizadora.Rows[0][Cat_Com_Cotizadores.Campo_IP_Correo_Saliente].ToString().Trim();

                                if (Clase_Negocio.P_Dt_Proveedores.Rows.Count != 0) { 
                                    for (int i = 0; i < Clase_Negocio.P_Dt_Proveedores.Rows.Count; i++) {
                                        //Realizamos un for para enviar el correo
                                        try {
                                            if (Clase_Negocio.P_Dt_Proveedores.Rows[i][Cat_Com_Proveedores.Campo_Correo_Electronico].ToString().Trim() != String.Empty)
                                                Enviar_Correo(Clase_Negocio.P_Dt_Proveedores.Rows[i][Cat_Com_Proveedores.Campo_Correo_Electronico].ToString().Trim(), Correo,Password,Direccion_IP );
                                        } catch {
                                            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + " El correo no pudo ser enviado a " + Clase_Negocio.P_Dt_Proveedores.Rows[i][Cat_Com_Proveedores.Campo_Correo_Electronico].ToString().Trim() + " </br>";
                                        }
                                    }//Fin del For
                                }//fin del IF
                            }//fin del if Checked
                        }//FIN DEL IF
                        if (!Operacion_Realizada) ScriptManager.RegisterStartupScript(this, this.GetType(), "Distribuir a Proveedores", "alert('No se pudieron enviar las requisiciones a los proveedores');", true);
                        Txt_Folio.Text = "";
                        Limpiar_Variables_Session();
                        Grid_Productos.Enabled = false;
                        Cmb_Proveedores.Enabled = false;
                        Chk_Enviar_Correo.Enabled = false;
                        Grid_Proveedores.Enabled = false;
                        Btn_Imprimir.Visible = true;
                        Btn_Imprimir.Enabled = true; 
                        Btn_Nuevo.Visible = true;
                        Btn_Nuevo.ToolTip = "Nuevo";
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                        Btn_Nuevo.Enabled = false; 
                        Btn_Salir.Visible = true;
                        Btn_Salir.ToolTip = "Listado";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                        ViewState["SortDirection"] = "ASC";
                        //Llenar_Combo_Cotizadores();
                        Configurar_Formulario("Inicio");
                        Llenar_Grid_Requisiciones();
                    } 
                    break;
            }//fin del switch
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Evento que agarra la session de las refacciones seleccionadas
        ///PARAMETROS: 
        ///CREO: jtoledo
        ///FECHA_CREO: 10/Mayo/2011 12:55:33 p.m.
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************   
        protected void Btn_Busqueda_Proveedores_Click(object sender, ImageClickEventArgs e) {
            if (Session["Proveedor_ID"] != null && Session["Nombre_Proveedor"] != null) {
                Cmb_Proveedores.SelectedValue = Session["Proveedor_ID"].ToString();
                Btn_Agregar_Proveedor_Click(Btn_Agregar_Proveedor, null);
            }
            Session["Proveedor_ID"] = null;
            Session["Nombre_Proveedor"] = null;
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Btn_Agregar_Proveedor_Click
        ///DESCRIPCIÓN:  
        ///PARAMETROS: 
        ///CREO: 
        ///FECHA_CREO: 10/Mayo/2011 12:55:33 p.m.
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************   
        protected void Btn_Agregar_Proveedor_Click(object sender, ImageClickEventArgs e)
        {
            Div_Contenedor_Msj_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            if (Cmb_Proveedores.SelectedIndex == 0)
            {
                Lbl_Mensaje_Error.Text = "Es necesario seleccionar un Proveedor";
                Div_Contenedor_Msj_Error.Visible = true;

            }
            //En caso de pasar las validaciones
            if (Div_Contenedor_Msj_Error.Visible == false)
            {

                if (Session["Dt_Proveedores_Grid"] != null)
                {
                    Agregar_Proveedores();
                }//fin if
                else
                {
                    //Creamos la session por primera ves
                    DataTable Dt_Proveedores = new DataTable();
                    Dt_Proveedores.Columns.Add("Proveedor_ID", typeof(System.String));
                    Dt_Proveedores.Columns.Add("Nombre", typeof(System.String));
                    Dt_Proveedores.Columns.Add("Compania", typeof(System.String));
                    Dt_Proveedores.Columns.Add("Telefonos", typeof(System.String));
                    Dt_Proveedores.Columns.Add("E_MAIL", typeof(System.String));
                    Session["Dt_Proveedores_Grid"] = Dt_Proveedores;
                    //Llenamos el grid
                    Grid_Proveedores.DataSource = (DataTable)Session["Dt_Proveedores_Grid"];
                    Grid_Proveedores.DataBind();
                    //Mandamos llamar el metodo para agregar los proveedores
                    Agregar_Proveedores();
                }
            }


        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Agregar_Proveedores
        ///DESCRIPCIÓN:  
        ///PARAMETROS: 
        ///CREO: 
        ///FECHA_CREO: 10/Mayo/2011 12:55:33 p.m.
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************   
        public void Agregar_Proveedores()
        {
            String Id = Cmb_Proveedores.SelectedValue;
            DataRow[] Filas;
            DataTable Dt = (DataTable)Session["Dt_Proveedores_Grid"];
            Filas = Dt.Select("Proveedor_ID='" + Id + "'");
            if (Filas.Length > 0)
            {
                //Si se encontro algun coincidencia entre el grupo a agregar con alguno agregado anteriormente, se avisa
                //al usuario que elemento ha agregar ya existe en la tabla de grupos.
                Lbl_Mensaje_Error.Text += "+ No se puede agregar el Proveedor " + Id + " ya que este ya se ha agregado<br/>";
                Div_Contenedor_Msj_Error.Visible = true;
            }
            else
            {
                //Creamos el objeto de la clase de negocios
                Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio Clase_Negocio = new Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio();
                //cONSULTAMOS EL PROVEEDOR ASIGNADO
                Clase_Negocio.P_Proveedor_ID = Cmb_Proveedores.SelectedValue;
                DataTable Dt_Datos_Proveedor = Clase_Negocio.Consultar_Proveedores_Asignados();
                if (!(Dt_Datos_Proveedor == null))
                {
                    if (Dt_Datos_Proveedor.Rows.Count > 0)
                    {
                        DataRow Fila_Nueva = Dt.NewRow();
                        //Asignamos los valores a la fila
                        Fila_Nueva["Proveedor_ID"] = Dt_Datos_Proveedor.Rows[0]["Proveedor_ID"].ToString();
                        Fila_Nueva["Nombre"] = Dt_Datos_Proveedor.Rows[0]["Nombre"].ToString();
                        Fila_Nueva["Compania"] = Dt_Datos_Proveedor.Rows[0]["Compania"].ToString();
                        Fila_Nueva["Telefonos"] = Dt_Datos_Proveedor.Rows[0]["Telefonos"].ToString();
                        Fila_Nueva["E_MAIL"] = Dt_Datos_Proveedor.Rows[0]["E_MAIL"].ToString();
                        Dt.Rows.Add(Fila_Nueva);
                        Dt.AcceptChanges();
                        Session["Dt_Proveedores_Grid"] = Dt;
                        Grid_Proveedores.DataSource = Dt;
                        Grid_Proveedores.DataBind();
                        Grid_Proveedores.Visible = true;
                    }
                }

            }//fin del else


        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Btn_Imprimir_Click
        ///DESCRIPCIÓN:  
        ///PARAMETROS: 
        ///CREO: 
        ///FECHA_CREO: 10/Mayo/2011 12:55:33 p.m.
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************   
        protected void Btn_Imprimir_Click(object sender, ImageClickEventArgs e) {
            Div_Contenedor_Msj_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            if (Txt_Folio.Text.Trim().Length > 0) {
                DataSet Ds_Reporte = null;
                Ds_Rpt_Tal_Requisicion Ds_Req = new Ds_Rpt_Tal_Requisicion();
                //DataTable Dt_Requisicion = null;
                Cls_Ope_Tal_Impresion_Requisiciones_Negocio Req_Negocio = new Cls_Ope_Tal_Impresion_Requisiciones_Negocio();
                DataTable Dt_Cabecera = new DataTable();
                DataTable Dt_Detalles = new DataTable();
                DataTable Dt_Solicitud = new DataTable();
                try {
                    String Requisicion_ID = Txt_Folio.Text.Replace("RQ-", "");
                    Req_Negocio.P_Requisicion_ID = Requisicion_ID.Trim();
                    Dt_Cabecera = Req_Negocio.Consultar_Requisiciones();
                    Dt_Detalles = Req_Negocio.Consultar_Requisiciones_Detalles();

                    if (!String.IsNullOrEmpty(Dt_Cabecera.Rows[0]["NO_SOLICITUD"].ToString())) {
                        Cls_Rpt_Tal_Solicitud_Servicio_Negocio Rpt = new Cls_Rpt_Tal_Solicitud_Servicio_Negocio();
                        Rpt.P_No_Solicitud = Convert.ToInt32(Dt_Cabecera.Rows[0]["NO_SOLICITUD"].ToString());
                        Rpt.P_Tipo_Bien = Dt_Cabecera.Rows[0]["TIPO_BIEN"].ToString().Trim();
                        Dt_Solicitud = Rpt.Consulta_Datos_Solicitud();
                    } else {
                        Dt_Solicitud = Ds_Req.Tables["DT_SERVICIO"].Clone();
                    }

                    Ds_Reporte = new DataSet();
                    Dt_Cabecera.TableName = "REQUISICION";
                    Dt_Detalles.TableName = "DETALLES";
                    Dt_Solicitud.TableName = "DT_SERVICIO";
                    Ds_Reporte.Tables.Add(Dt_Cabecera.Copy());
                    Ds_Reporte.Tables.Add(Dt_Detalles.Copy());
                    Ds_Reporte.Tables.Add(Dt_Solicitud.Copy());
                    Ds_Reporte.Tables.Add(new Cls_Tal_Parametros_Negocio().Obtener_Tabla_Reporte(this.Server).Copy());
                    Generar_Reporte(ref Ds_Reporte, "Rpt_Tal_Requisiciones.rpt", "Requisicion.pdf");
                } catch (Exception Ex) {
                    Lbl_Mensaje_Error.Text = Ex.Message.Trim();
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            } else {
                Lbl_Mensaje_Error.Text = "Seleccione una Requisición";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

    #endregion

        /// *****************************************************************************************
        /// NOMBRE: Generar_Reporte
        /// DESCRIPCIÓN: 
        /// CREÓ: Gustavo Angeles Cruz
        /// FECHA CREÓ: 11/Junio/2011
        ///MODIFICO:Jesus Toledo
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        /// *****************************************************************************************
        protected void Generar_Reporte(ref DataSet Ds_Datos, String Nombre_Plantilla_Reporte, String Nombre_Reporte_Generar) {
            ReportDocument Reporte = new ReportDocument();//Variable de tipo reporte.
            String Ruta = String.Empty;//Variable que almacenara la ruta del archivo del crystal report. 
            try {
                Ruta = @Server.MapPath("../Rpt/Servicios_Generales/" + Nombre_Plantilla_Reporte);
                Reporte.Load(Ruta);

                if (Ds_Datos is DataSet) {
                    if (Ds_Datos.Tables.Count > 0) {
                        Reporte.SetDataSource(Ds_Datos);
                        Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar);
                        Mostrar_Reporte(Nombre_Reporte_Generar);
                    }
                }
            } catch (Exception Ex) {
                throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
            }
        }

        /// *****************************************************************************************
        /// NOMBRE: Exportar_Reporte_PDF
        /// DESCRIPCIÓN:  
        /// CREÓ: Gustavo Angeles Cruz
        /// FECHA CREÓ: 11/Junio/2011
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        /// *****************************************************************************************
        protected void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte) {
            ExportOptions Opciones_Exportacion = new ExportOptions();
            DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
            PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

            try {
                if (Reporte is ReportDocument) {
                    Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte);
                    Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                    Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                    Reporte.Export(Opciones_Exportacion);
                }
            }  catch (Exception Ex) {
                throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
            }
        }

        /// *****************************************************************************************
        /// NOMBRE: Mostrar_Reporte
        /// DESCRIPCIÓN:  
        /// CREÓ: Gustavo Angeles Cruz
        /// FECHA CREÓ: 11/Junio/2011
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        /// *****************************************************************************************
        protected void Mostrar_Reporte(String Nombre_Reporte) {
            String Pagina = "../../Reporte/";

            try {
                Pagina = Pagina + Nombre_Reporte;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window",
                    "window.open('" + Pagina + "', 'Requisición','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            } catch (Exception Ex) {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
        }

}