﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using JAPAMI.Taller_Mecanico.Catalogo_Tipos_Refacciones.Negocio;
using JAPAMI.Taller_Mecanico.Reporte_Listado_Refacciones_Stock.Negocio;
using JAPAMI.Constantes;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using JAPAMI.Sessiones;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;

public partial class paginas_Taller_Mecanico_Frm_Rpt_Tal_Listado_Refacciones_Stock : System.Web.UI.Page {

    #region PAGE LOAD

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN: Evento al Cargar la Pagina 
        ///PARAMETROS: 
        ///CREO: jtoledo
        ///FECHA_CREO: 01/Dic/2011
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e) {
            Div_Contenedor_Msj_Error.Visible = false;
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack) {
                Cargar_Combo_Tipo_Refaccion();
                Cargar_Combo_Capitulos();
                Cmb_Capitulo_SelectedIndexChanged(Cmb_Capitulo, null);
            }
        }

    #endregion

    #region METODOS

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Cargar_Combo_Tipo_Refaccion
        ///DESCRIPCIÓN:  
        ///PARAMETROS: 
        ///CREO: jtoledo
        ///FECHA_CREO: 01/Dic/2011
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///******************************************************************************* 
        private void Cargar_Combo_Tipo_Refaccion() {
            Cls_Cat_Tal_Tipos_Refacciones_Negocio Consultas_Negocio = new Cls_Cat_Tal_Tipos_Refacciones_Negocio();
            Consultas_Negocio.P_Estatus = "VIGENTE";
            DataTable Dt_Datos = Consultas_Negocio.Consultar_Nivel();
            Cmb_Tipo_Refaccion.DataSource = Dt_Datos;
            Cmb_Tipo_Refaccion.DataValueField = Cat_Tal_Tipos_Refacciones.Campo_Tipo_Refaccion_ID;
            Cmb_Tipo_Refaccion.DataTextField = Cat_Tal_Tipos_Refacciones.Campo_Nombre;
            Cmb_Tipo_Refaccion.DataBind();
            Cmb_Tipo_Refaccion.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Cargar_Combo_Capitulos
        ///DESCRIPCIÓN: 
        ///PARAMETROS: 
        ///CREO: jtoledo
        ///FECHA_CREO: 01/Dic/2011
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///******************************************************************************* 
        private void Cargar_Combo_Capitulos() {
            Cls_Ope_Tal_Consultas_Generales_Negocio Consultas_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            Consultas_Negocio.P_Estatus = "ACTIVO";
            DataTable Dt_Capitulos = Consultas_Negocio.Consultar_Capitulos();
            Cmb_Capitulo.DataSource = Dt_Capitulos;
            Cmb_Capitulo.DataValueField = "CAPITULO_ID";
            Cmb_Capitulo.DataTextField = "CLAVE_NOMBRE";
            Cmb_Capitulo.DataBind();
            Cmb_Capitulo.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Cargar_Combo_Conceptos
        ///DESCRIPCIÓN:  
        ///PARAMETROS: 
        ///CREO: jtoledo
        ///FECHA_CREO: 01/Dic/2011
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///******************************************************************************* 
        private void Cargar_Combo_Conceptos() {
            if (Cmb_Capitulo.SelectedIndex > 0) {
                Cls_Ope_Tal_Consultas_Generales_Negocio Consultas_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                Consultas_Negocio.P_Estatus = "ACTIVO";
                Consultas_Negocio.P_Capitulo_ID = Cmb_Capitulo.SelectedItem.Value.Trim();
                DataTable Dt_Conceptos = Consultas_Negocio.Consultar_Conceptos();
                Cmb_Conceptos.DataSource = Dt_Conceptos;
                Cmb_Conceptos.DataValueField = "CONCEPTO_ID";
                Cmb_Conceptos.DataTextField = "CLAVE_NOMBRE";
                Cmb_Conceptos.DataBind();
                Cmb_Conceptos.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
            } else {
                Cmb_Conceptos.Items.Clear();
                Cmb_Conceptos.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
            }
            
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Cargar_Combo_Partidas_Genericas
        ///DESCRIPCIÓN:  
        ///PARAMETROS: 
        ///CREO: jtoledo
        ///FECHA_CREO: 01/Dic/2011
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///******************************************************************************* 
        private void Cargar_Combo_Partidas_Genericas() {
            if (Cmb_Conceptos.SelectedIndex > 0) {
                Cls_Ope_Tal_Consultas_Generales_Negocio Consultas_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                Consultas_Negocio.P_Estatus = "ACTIVO";
                Consultas_Negocio.P_Concepto_ID = Cmb_Conceptos.SelectedItem.Value.Trim();
                DataTable Dt_Partidas = Consultas_Negocio.Consultar_Partidas_Genericas();
                Cmb_Partida_General.DataSource = Dt_Partidas;
                Cmb_Partida_General.DataValueField = "PARTIDA_ID";
                Cmb_Partida_General.DataTextField = "CLAVE_NOMBRE";
                Cmb_Partida_General.DataBind();
                Cmb_Partida_General.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
            } else {
                Cmb_Partida_General.Items.Clear();
                Cmb_Partida_General.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
            }
            
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Cargar_Combo_Partidas_Especificas
        ///DESCRIPCIÓN:  
        ///PARAMETROS: 
        ///CREO: jtoledo
        ///FECHA_CREO: 01/Dic/2011
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///******************************************************************************* 
        private void Cargar_Combo_Partidas_Especificas() {
            if (Cmb_Partida_General.SelectedIndex > 0) {
                Cls_Ope_Tal_Consultas_Generales_Negocio Consultas_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                Consultas_Negocio.P_Estatus = "ACTIVO";
                Consultas_Negocio.P_Partida_Generica_ID = Cmb_Partida_General.SelectedItem.Value.Trim();
                DataTable Dt_Partidas = Consultas_Negocio.Consultar_Partidas_Especificas();
                Cmb_Partida_Especifica.DataSource = Dt_Partidas;
                Cmb_Partida_Especifica.DataValueField = "PARTIDA_ID";
                Cmb_Partida_Especifica.DataTextField = "CLAVE_NOMBRE";
                Cmb_Partida_Especifica.DataBind();
                Cmb_Partida_Especifica.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
            } else {
                Cmb_Partida_Especifica.Items.Clear();
                Cmb_Partida_Especifica.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
            }
            
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Obtener_Filtros_Seleccionados_Reporte
        ///DESCRIPCIÓN:  Obtener Filtros
        ///PARAMETROS: 
        ///CREO: jtoledo
        ///FECHA_CREO: 01/Dic/2011
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///******************************************************************************* 
        private DataTable Obtener_Filtros_Seleccionados_Reporte() {
            DataTable Dt_Filtros = new DataTable();
            Dt_Filtros.Columns.Add("Filtro", Type.GetType("System.String"));
            Dt_Filtros.Columns.Add("Valor", Type.GetType("System.String"));
            if (Cmb_Capitulo.SelectedIndex > 0) {
                DataRow Dr_Fila = Dt_Filtros.NewRow();
                Dr_Fila["Filtro"] = "Capítulo";
                Dr_Fila["Valor"] = Cmb_Capitulo.SelectedItem.Text.Trim();
                Dt_Filtros.Rows.Add(Dr_Fila);
            }
            if (Cmb_Conceptos.SelectedIndex > 0) {
                DataRow Dr_Fila = Dt_Filtros.NewRow();
                Dr_Fila["Filtro"] = "Concepto";
                Dr_Fila["Valor"] = Cmb_Conceptos.SelectedItem.Text.Trim();
                Dt_Filtros.Rows.Add(Dr_Fila);
            }
            if (Cmb_Partida_General.SelectedIndex > 0) {
                DataRow Dr_Fila = Dt_Filtros.NewRow();
                Dr_Fila["Filtro"] = "Partida Genérica";
                Dr_Fila["Valor"] = Cmb_Partida_General.SelectedItem.Text.Trim();
                Dt_Filtros.Rows.Add(Dr_Fila);
            }
            if (Cmb_Partida_Especifica.SelectedIndex > 0) {
                DataRow Dr_Fila = Dt_Filtros.NewRow();
                Dr_Fila["Filtro"] = "Partida Especifica";
                Dr_Fila["Valor"] = Cmb_Partida_Especifica.SelectedItem.Text.Trim();
                Dt_Filtros.Rows.Add(Dr_Fila);
            }
            if (Txt_Ubicacion.Text.Trim().Length > 0) {
                DataRow Dr_Fila = Dt_Filtros.NewRow();
                Dr_Fila["Filtro"] = "Ubicación";
                Dr_Fila["Valor"] = Txt_Ubicacion.Text.Trim();
                Dt_Filtros.Rows.Add(Dr_Fila);
            }
            if (Cmb_Tipo_Refaccion.SelectedIndex > 0) {
                DataRow Dr_Fila = Dt_Filtros.NewRow();
                Dr_Fila["Filtro"] = "Tipo de Refacción";
                Dr_Fila["Valor"] = Cmb_Tipo_Refaccion.SelectedItem.Text.Trim();
                Dt_Filtros.Rows.Add(Dr_Fila);
            }
            return Dt_Filtros;
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Obtener_Filtros_Seleccionados_Reporte
        ///DESCRIPCIÓN:  Obtener Filtros
        ///PARAMETROS: 
        ///CREO: jtoledo
        ///FECHA_CREO: 01/Dic/2011
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///******************************************************************************* 
        private DataTable Obtener_Consulta_Datos() {
            Cls_Rpt_Tal_Listado_Refacciones_Stock_Negocio Negocio = new Cls_Rpt_Tal_Listado_Refacciones_Stock_Negocio();
            if (Cmb_Capitulo.SelectedIndex > 0) { Negocio.P_Capitulo_ID = Cmb_Capitulo.SelectedItem.Value; }
            if (Cmb_Conceptos.SelectedIndex > 0) { Negocio.P_Concepto_ID = Cmb_Conceptos.SelectedItem.Value; }
            if (Cmb_Partida_General.SelectedIndex > 0) { Negocio.P_Partida_Generica_ID = Cmb_Partida_General.SelectedItem.Value; }
            if (Cmb_Partida_Especifica.SelectedIndex > 0) { Negocio.P_Partida_Especifica_ID = Cmb_Partida_Especifica.SelectedItem.Value; }
            if (Txt_Ubicacion.Text.Trim().Length > 0) { Negocio.P_Ubicacion = Txt_Ubicacion.Text.Trim(); }
            if (Cmb_Tipo_Refaccion.SelectedIndex > 0) { Negocio.P_Tipo_Refaccion_ID = Cmb_Tipo_Refaccion.SelectedItem.Value; }
            DataTable Dt_Datos = Negocio.Consultar_Listado_Refacciones_Stock();
            return Dt_Datos;
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
        ///DESCRIPCIÓN: Genera los Datos para el Reporte.
        ///PARAMETROS: 
        ///CREO: jtoledo
        ///FECHA_CREO: 01/Dic/2011
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///******************************************************************************* 
        private void Generar_Reporte(String Tipo) {
            DataTable Dt_Filtros = Obtener_Filtros_Seleccionados_Reporte();
            Dt_Filtros.TableName = "DT_FILTROS";

            DataTable Dt_Registros = Obtener_Consulta_Datos();
            Dt_Registros.TableName = "DT_REFACCIONES";

            DataSet Ds_Consulta = new DataSet();
            Ds_Consulta.Tables.Add(Dt_Registros.Copy());
            Ds_Consulta.Tables.Add(Dt_Filtros.Copy());

            Ds_Consulta.Tables.Add(new Cls_Tal_Parametros_Negocio().Obtener_Tabla_Reporte(this.Server).Copy());
            Ds_Rpt_Tal_Listado_Refacciones_Stock Ds_Reporte = new Ds_Rpt_Tal_Listado_Refacciones_Stock();
            Generar_Reporte(Ds_Consulta, Ds_Reporte, "Rpt_Tal_Listado_Refacciones_Stock.rpt");
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
        ///DESCRIPCIÓN: caraga el data set fisico con el cual se genera el Reporte especificado
        ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
        ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
        ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
        ///CREO: Susana Trigueros Armenta.
        ///FECHA_CREO: 01/Mayo/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        private void Generar_Reporte(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, String Nombre_Reporte) {
            ReportDocument Reporte = new ReportDocument();
            String File_Path = Server.MapPath("../Rpt/Servicios_Generales/" + Nombre_Reporte);
            Reporte.Load(File_Path);
            String Nombre_Reporte_Generar = "Rpt_Tal_Lis_Ref_Stck_" + Cls_Sessiones.No_Empleado + "_" + Convert.ToString(DateTime.Now.ToString("yyyy'-'MM'-'dd'_t'HH'-'mm'-'ss")) + ".pdf";
            String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
            Ds_Reporte = Data_Set_Consulta_DB;
            Reporte.SetDataSource(Ds_Reporte);
            Reporte.SetParameterValue("NUMERO_REGISTROS", Data_Set_Consulta_DB.Tables["DT_REFACCIONES"].Rows.Count);
            Reporte.SetParameterValue("PERSONA_GENERA", Cls_Sessiones.Nombre_Empleado);
            ExportOptions Export_Options = new ExportOptions();
            DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
            Disk_File_Destination_Options.DiskFileName = Server.MapPath(Ruta);
            Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
            Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
            Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
            Reporte.Export(Export_Options);
            Mostrar_Reporte(Nombre_Reporte_Generar, "PDF");
        }

        /// *************************************************************************************
        /// NOMBRE:              Mostrar_Reporte
        /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
        /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
        ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
        /// USUARIO CREO:        Juan Alberto Hernández Negrete.
        /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
        /// USUARIO MODIFICO:    Salvador Hernández Ramírez
        /// FECHA MODIFICO:      23-Mayo-2011
        /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
        /// *************************************************************************************
        protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
        {
            String Pagina = "../../Reporte/";
            try
            {
                Pagina = Pagina + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
        }

    #endregion

    #region EVENTOS

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Cmb_Capitulo_SelectedIndexChanged
        ///DESCRIPCIÓN: Paginar grid de ordenes de variacion
        ///PARAMETROS: 
        ///CREO: jtoledo
        ///FECHA_CREO: 01/Dic/2011
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************    
        protected void Cmb_Capitulo_SelectedIndexChanged(object sender, EventArgs e)
        {
            Cargar_Combo_Conceptos();
            Cmb_Conceptos_SelectedIndexChanged(Cmb_Conceptos, null);
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Cmb_Conceptos_SelectedIndexChanged
        ///DESCRIPCIÓN: Paginar grid de ordenes de variacion
        ///PARAMETROS: 
        ///CREO: jtoledo
        ///FECHA_CREO: 01/Dic/2011
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************    
        protected void Cmb_Conceptos_SelectedIndexChanged(object sender, EventArgs e)
        {
            Cargar_Combo_Partidas_Genericas();
            Cmb_Partida_General_SelectedIndexChanged(Cmb_Partida_General, null);
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Cmb_Partida_General_SelectedIndexChanged
        ///DESCRIPCIÓN: Paginar grid de ordenes de variacion
        ///PARAMETROS: 
        ///CREO: jtoledo
        ///FECHA_CREO: 01/Dic/2011
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************    
        protected void Cmb_Partida_General_SelectedIndexChanged(object sender, EventArgs e)
        {
            Cargar_Combo_Partidas_Especificas();
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Btn_Reporte_PDF_Click
        ///DESCRIPCIÓN: Genera el Reporte PDF
        ///PARAMETROS: 
        ///CREO: jtoledo
        ///FECHA_CREO: 01/Dic/2011
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************    
        protected void Btn_Reporte_PDF_Click(object sender, ImageClickEventArgs e) {
            try {
                Generar_Reporte("PDF");
            } catch(Exception Ex){
                Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                Lbl_Mensaje_Error.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Btn_Limpiar_Todo_Click
        ///DESCRIPCIÓN: Limpiar todos los filtros
        ///PARAMETROS: 
        ///CREO: jtoledo
        ///FECHA_CREO: 01/Dic/2011
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************    
        protected void Btn_Limpiar_Todo_Click(object sender, ImageClickEventArgs e) {
            Cargar_Combo_Capitulos();
            Cmb_Capitulo_SelectedIndexChanged(Cmb_Capitulo, null);
            Txt_Ubicacion.Text = "";
            Cmb_Tipo_Refaccion.SelectedIndex = 0;
        }

    #endregion

}