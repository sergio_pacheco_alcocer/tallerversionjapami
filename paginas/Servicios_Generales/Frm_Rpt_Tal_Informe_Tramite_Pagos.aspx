﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Rpt_Tal_Informe_Tramite_Pagos.aspx.cs" Inherits="paginas_Taller_Mecanico_Frm_Rpt_Tal_Informe_Tramite_Pagos" Title="Reporte de Informe Tramite de Pagos " %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" language="javascript">
        function Limpiar_Ctlr_General(){
            document.getElementById("<%=Txt_Fecha_Recepcion_Inicial.ClientID%>").value="";  
            document.getElementById("<%=Txt_Fecha_Recepcion_Final.ClientID%>").value="";                       
            return false;
        }        
    </script>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <asp:ScriptManager ID="ScptM_Catalogo" runat="server" />  
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>                     
            </asp:UpdateProgress>
            <div id="Div_Requisitos" style="background-color:#ffffff; width:100%; height:100%;">
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo" colspan="2">Informe Tramite de Pagos</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                          <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;">              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top">
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                            </table>                   
                          </div>                
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>                        
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" style="width:50%;">
                            <asp:ImageButton ID="Btn_Reporte_PDF" runat="server" OnClick="Btn_Reporte_PDF_Click"
                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" Width="24px" CssClass="Img_Button"
                                AlternateText="Modificar" />
                            <asp:ImageButton ID="Btn_Reporte_EXCEL" runat="server" OnClick="Btn_Reporte_EXCEL_Click"
                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png" Width="24px" CssClass="Img_Button"
                                AlternateText="Salir" />
                        </td>
                        <td style="width:50%;">&nbsp;&nbsp;
                             <asp:ImageButton ID="Btn_Limpiar_Todo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_Limpiar.png" AlternateText="Limpiar Todo" ToolTip="Limpiar Todo" Width="16px" Height="16px" OnClientClick="javascript:return Limpiar_Ctlr_General();"/>
                        </td>                        
                    </tr>
                </table>  
                <div id="Div_Datos_Vehiculo" runat="server" style="width:100%;">
                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                            <td colspan="4">
                                <asp:Panel ID="Pnl_Fecha_Recepcion" runat="server" Width="99%" GroupingText="Fecha de Recepción">
                                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:Label ID="Lbl_Fecha_Recepcion_Inicial" runat="server" Text="Fecha Inicial"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_Fecha_Recepcion_Inicial" runat="server" Width="80%" Enabled="false"></asp:TextBox>
                                                <asp:ImageButton ID="Btn_Fecha_Recepcion_Inicial" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                                <cc1:CalendarExtender ID="CE_Txt_Fecha_Recepcion_Inicial" runat="server" TargetControlID="Txt_Fecha_Recepcion_Inicial" PopupButtonID="Btn_Fecha_Recepcion_Inicial" Format="dd/MMM/yyyy">
                                                </cc1:CalendarExtender>  
                                            </td>
                                            <td style="width:15%;">
                                                <asp:Label ID="Lbl_Fecha_Recepcion_Final" runat="server" Text="Fecha Final"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_Fecha_Recepcion_Final" runat="server" Width="80%" Enabled="false"></asp:TextBox>
                                                <asp:ImageButton ID="Btn_Fecha_Recepcion_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                                <cc1:CalendarExtender ID="CE_Txt_Fecha_Recepcion_Final" runat="server" TargetControlID="Txt_Fecha_Recepcion_Final" PopupButtonID="Btn_Fecha_Recepcion_Final" Format="dd/MMM/yyyy">
                                                </cc1:CalendarExtender>  
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <br />
            <br />
            <br />
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

