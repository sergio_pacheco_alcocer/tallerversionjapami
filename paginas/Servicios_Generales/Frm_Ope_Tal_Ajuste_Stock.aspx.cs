﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Ajustar_Stock.Negocio;
using JAPAMI.Taller_Mecanico.Autorizar_Ajuste_Stock.Negocio;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Taller_Mecanico.Control_Stock;
using CrystalDecisions.ReportSource;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;


public partial class paginas_Taller_Mecanico_Frm_Ope_Tal_Ajuste_Stock : System.Web.UI.Page {

    #region VARIABLES

        private static String P_Dt_Producto_Buscado = "Dt_Producto_Buscado";
        private static String P_Dt_Productos_Ajustados = "Dt_Productos_Ajustados";
        private static String P_Dt_Ajustes_Stock = "Dt_Ajustes_Stock";
        private const String Operacion_Quitar_Renglon = "QUITAR";
        private const String Operacion_Agregar_Renglon_Nuevo = "AGREGAR_NUEVO";
        private const String Operacion_Agregar_Renglon_Copia = "AGREGAR_COPIA";
        private const String MODO_LISTADO = "LISTADO";
        private const String MODO_NUEVO = "NUEVO";

    #endregion

    #region PAGE LOAD

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN: Pagina Inicial del Formulario
        ///PARAMETROS: 
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 16/Julio/2012
        ///CAUSA_MODIFICACIÓN: Adaptar a Taller Mecanico Municipal
        ///*******************************************************************************
        protected void Page_Load(object sender, EventArgs e) {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack) {
                Cargar_Ventana_Emergente_Busqueda_Refacciones();
                Session["Activa"] = true;
                ViewState["SortDirection"] = "DESC";
                DateTime _DateTime = DateTime.Now;
                int dias = _DateTime.Day;
                dias = dias * -1;
                dias++;
                int meses = _DateTime.Month;
                meses = meses * -1;
                meses++;
                _DateTime = _DateTime.AddDays(dias);
                _DateTime = _DateTime.AddMonths(meses);
                Txt_Fecha_Inicial.Text = _DateTime.ToString("dd/MMM/yyyy").ToUpper();
                Txt_Fecha_Final.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
                Cmb_Movimiento.Items.Clear();
                Cmb_Movimiento.Items.Add("ENTRADA");
                Cmb_Movimiento.Items.Add("SALIDA");
                Cmb_Movimiento.Items[0].Selected = true;
                Session[P_Dt_Productos_Ajustados] = Construir_Tabla_Productos_Ajustados();
                Habilitar_Botones(MODO_LISTADO);
                Llenar_Grid_Ajustes_Inventario();
            }
            if (Txt_Producto.Text.Trim().Length == 0) {
                Txt_Conteo_Fisico.Enabled = false;
            } else {
                Txt_Conteo_Fisico.Enabled = true;
            }
            Mostrar_Informacion("", false);
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Cargar_Ventana_Emergente_Busqueda_Refacciones
        ///DESCRIPCIÓN         : Establece el evento onclik del control para abrir la ventana emergente
        ///PARAMETROS          :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Cargar_Ventana_Emergente_Busqueda_Refacciones() {
            String Ventana_Modal = "Abrir_Ventana_Modal('Ventanas_Emergentes/Frm_Busqueda_Refacciones_Stock.aspx";
            String Propiedades = ", 'center:yes;resizable:no;status:no;dialogWidth:680px;dialogHide:true;help:no;scroll:no');";
            Btn_Busqueda_Refaccion.Attributes.Add("onclick", Ventana_Modal + "?Fecha=False'" + Propiedades);
        }

    #endregion

    #region METODOS

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Habilitar_Botones
        ///DESCRIPCIÓN: Habilita la Vista de los Campos dependiendo del Estado
        ///PARAMETROS: 
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 16/Julio/2012
        ///CAUSA_MODIFICACIÓN: Adaptar a Taller Mecanico Municipal
        ///*******************************************************************************
        private void Habilitar_Botones(String Caso)
        {
            switch (Caso)
            {
                case MODO_LISTADO:
                    Btn_Salir.ToolTip = "Inicio";
                    Btn_Guardar.Visible = false;
                    Btn_Nuevo.Visible = true;
                    Div_Listado_Ajustes.Visible = true;
                    Div_Contenido.Visible = false;
                    Limpiar_Form();
                    Session[P_Dt_Productos_Ajustados] = Construir_Tabla_Productos_Ajustados();
                    Grid_Productos_Ajustados.DataSource = new DataTable();
                    Grid_Productos_Ajustados.DataBind();
                    break;
                case MODO_NUEVO:
                    Btn_Salir.ToolTip = "Regresar";
                    Btn_Guardar.Visible = true;
                    Btn_Nuevo.Visible = false;
                    Div_Listado_Ajustes.Visible = false;
                    Div_Contenido.Visible = true;
                    Limpiar_Form();
                    break;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Producto
        ///DESCRIPCIÓN: Carga los Datos del Producto Seleccionado
        ///PARAMETROS: 
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 16/Julio/2012
        ///CAUSA_MODIFICACIÓN: Adaptar a Taller Mecanico Municipal
        ///*******************************************************************************
        private void Cargar_Datos_Producto()
        {
            DataTable Dt_Producto = Session[P_Dt_Producto_Buscado] as DataTable;
            if (Dt_Producto != null && Dt_Producto.Rows.Count > 0)
            {
                DataRow[] Producto = Dt_Producto.Select("CLAVE = '" + Txt_Clave.Text.Trim() + "'");
                Hdf_Refaccion_ID.Value = Producto[0]["REFACCION_ID"].ToString();
                Txt_Producto.Text = Producto[0]["NOMBRE"].ToString();
                Txt_Descripcion.Text = Producto[0]["DESCRIPCION"].ToString();
                Txt_Costo.Text = Producto[0]["COSTO_UNITARIO"].ToString();
                Txt_Existencia.Text = Producto[0]["EXISTENCIA"].ToString();
                Hdn_Comprometido.Value = Producto[0]["COMPROMETIDO"].ToString();
            }
            else
            {
                Mostrar_Informacion("No se encontró en producto", true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Ajustes_Inventario
        ///DESCRIPCIÓN: Llena el Grid de los Ajustes de Inventario
        ///PARAMETROS: 
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 16/Julio/2012
        ///CAUSA_MODIFICACIÓN: Adaptar a Taller Mecanico Municipal
        ///*******************************************************************************
        private void Llenar_Grid_Ajustes_Inventario()
        {
            Cls_Ope_Tal_Ajustar_Stock_Negocio Negocio = new Cls_Ope_Tal_Ajustar_Stock_Negocio();
            Negocio.P_Fecha_Inicial = Txt_Fecha_Inicial.Text.Trim();
            Negocio.P_Fecha_Final = Txt_Fecha_Final.Text.Trim();
            Negocio.P_No_Ajuste = Txt_Busqueda.Text.Trim();
            DataTable Dt_Ajustes = Negocio.Consultar_Ajustes_Inventario();
            if (Dt_Ajustes != null && Dt_Ajustes.Rows.Count > 0)
            {
                Session[P_Dt_Ajustes_Stock] = Dt_Ajustes;
                Grid_Ajustes_Inventario.DataSource = Dt_Ajustes;
                Grid_Ajustes_Inventario.DataBind();
            }
            else
            {
                Grid_Ajustes_Inventario.DataSource = null;
                Grid_Ajustes_Inventario.DataBind();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Construir_Tabla_Productos_Ajustados
        ///DESCRIPCIÓN: Productos Ajustados
        ///PARAMETROS: 
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 16/Julio/2012
        ///CAUSA_MODIFICACIÓN: Adaptar a Taller Mecanico Municipal
        ///*******************************************************************************
        private DataTable Construir_Tabla_Productos_Ajustados()
        {
            DataTable Tabla = new DataTable();
            DataColumn Columna = null;
            DataTable Tabla_Base_Datos = JAPAMI.Taller_Mecanico.Requisiciones.Datos.Cls_Ope_Tal_Requisiciones_Datos.Consultar_Columnas_De_Tabla_BD(Ope_Tal_Ajustes_Detalles.Tabla_Ope_Tal_Ajustes_Almacen);
            foreach (DataRow Renglon in Tabla_Base_Datos.Rows) {
                Columna = new DataColumn(Renglon["COLUMNA"].ToString(), System.Type.GetType("System.String"));
                Tabla.Columns.Add(Columna);
            }
            Columna = new DataColumn("CLAVE", System.Type.GetType("System.String"));
            Tabla.Columns.Add(Columna);
            return Tabla;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Agregar_Quitar_Renglones_A_DataTable
        ///DESCRIPCIÓN: 
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 1 Oct 2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 16/Julio/2012
        ///CAUSA_MODIFICACIÓN: Adaptar a Taller Mecanico Municipal
        ///*******************************************************************************
        private DataTable Agregar_Quitar_Renglones_A_DataTable(DataTable _DataTable, DataRow _DataRow, String Operacion)
        {
            if (Operacion == Operacion_Agregar_Renglon_Nuevo)
            {
                _DataTable.Rows.Add(_DataRow);
            }
            else if (Operacion == Operacion_Agregar_Renglon_Copia)
            {
                _DataTable.ImportRow(_DataRow);
                _DataTable.AcceptChanges();
            }
            else if (Operacion == Operacion_Quitar_Renglon)
            {
                ((DataTable)Session[P_Dt_Productos_Ajustados]).Rows.Remove(_DataRow);
            }
            return _DataTable;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:Mostrar_Informacion
        ///DESCRIPCIÓN: 
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 1 Oct 2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 16/Julio/2012
        ///CAUSA_MODIFICACIÓN: Adaptar a Taller Mecanico Municipal
        ///*******************************************************************************
        private void Mostrar_Informacion(String txt, Boolean mostrar)
        {
            Lbl_Informacion.Style.Add("color", "#990000");
            Lbl_Informacion.Visible = mostrar;
            Img_Warning.Visible = mostrar;
            Lbl_Informacion.Text = txt;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
        ///DESCRIPCIÓN: caraga el data set fisoco con el cual se genera el Reporte especificado
        ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
        ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
        ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 01/Mayo/2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 16/Julio/2012
        ///CAUSA_MODIFICACIÓN: Adaptar a Taller Mecanico Municipal
        ///*******************************************************************************
        private void Generar_Reporte(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte, string Nombre_PDF)
        {

            ReportDocument Reporte = new ReportDocument();
            String File_Path = Server.MapPath("../Rpt/Almacen/" + Nombre_Reporte);
            Reporte.Load(File_Path);
            Ds_Reporte = Data_Set_Consulta_DB;
            Reporte.SetDataSource(Ds_Reporte);
            ExportOptions Export_Options = new ExportOptions();
            DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
            Disk_File_Destination_Options.DiskFileName = Server.MapPath("../../Reporte/" + Nombre_PDF);
            Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
            Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
            Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
            Reporte.Export(Export_Options);
            String Ruta = "../../Reporte/" + Nombre_PDF;
            Mostrar_Reporte(Nombre_PDF, "PDF");
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }

        /// *************************************************************************************
        /// NOMBRE:              Mostrar_Reporte
        /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
        /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
        ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
        /// USUARIO CREO:        Juan Alberto Hernández Negrete.
        /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
        /// USUARIO MODIFICO:    Salvador Hernández Ramírez
        /// FECHA MODIFICO:      16-Mayo-2011
        /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
        /// MODIFICO:            Francisco Antonio Gallardo Castañeda
        /// FECHA_MODIFICO:      16/Julio/2012
        /// CAUSA_MODIFICACIÓN:  Adaptar a Taller Mecanico Municipal
        /// *************************************************************************************
        protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
        {
            String Pagina = "../../Reporte/";

            try
            {
                if (Formato == "PDF")
                {
                    Pagina = Pagina + Nombre_Reporte_Generar;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                    "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
                }
                else if (Formato == "Excel")
                {
                    String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Duplicado
        ///DESCRIPCIÓN: Busca un Elemento Duplicado
        ///PARAMETROS: 
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 16/Julio/2012
        ///CAUSA_MODIFICACIÓN: Adaptar a Taller Mecanico Municipal
        ///*******************************************************************************
        private Boolean Duplicado(DataTable Dt_Tabla, String Producto_ID)
        {
            DataRow[] _DataRow = Dt_Tabla.Select("REFACCION_ID = '" + Producto_ID + "'");
            bool duplex = (_DataRow != null && _DataRow.Length > 0) ? true : false;
            return duplex;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Resumen_Movimientos
        ///DESCRIPCIÓN: 
        ///PARAMETROS: 
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 16/Julio/2012
        ///CAUSA_MODIFICACIÓN: Adaptar a Taller Mecanico Municipal
        ///*******************************************************************************
        private void Resumen_Movimientos()
        {
            int Total_Ajustes = 0;
            int Total_Entradas = 0;
            int Total_Salidas = 0;
            int Total_Ajustes_Unidad = 0;
            int Total_Entradas_Unidad = 0;
            int Total_Salidas_Unidas = 0;
            double Total_Importe_Entradas = 0;
            double Total_Importe_Salidas = 0;
            double Saldo = 0;
            //int Total_Ajustes = 0;
            if (Session[P_Dt_Productos_Ajustados] != null)
            {
                DataTable Dt_Productos = Session[P_Dt_Productos_Ajustados] as DataTable;
                if (Dt_Productos != null && Dt_Productos.Rows.Count > 0)
                {
                    foreach (DataRow Producto in Dt_Productos.Rows)
                    {
                        Total_Ajustes++;
                        if (Producto["TIPO_MOVIMIENTO"].ToString().Trim() == "ENTRADA")
                        {
                            Total_Entradas++;
                            Total_Importe_Entradas += double.Parse(Producto["IMPORTE_DIFERENCIA"].ToString());
                            Total_Entradas_Unidad += int.Parse(Producto["DIFERENCIA"].ToString());
                        }
                        else
                        {
                            Total_Salidas++;
                            Total_Importe_Salidas += double.Parse(Producto["IMPORTE_DIFERENCIA"].ToString());
                            Total_Salidas_Unidas += Math.Abs(int.Parse(Producto["DIFERENCIA"].ToString()));
                        }
                    }//for
                }
                else
                {
                    Limpiar_Form();
                }
            }
            else
            {
                Limpiar_Form();
            }
            Lbl_Importe_Entradas.Text = Total_Importe_Entradas.ToString();
            Lbl_Total_Entradas.Text = Total_Entradas.ToString();
            Lbl_Entradas_Unidad.Text = Total_Entradas_Unidad.ToString();

            Lbl_Total_Salidas.Text = Total_Salidas.ToString();
            Lbl_Importe_Salidas.Text = Total_Importe_Salidas.ToString();
            Lbl_Salidas_Unidad.Text = Total_Salidas_Unidas.ToString();

            Lbl_Total_Ajustes.Text = Total_Ajustes.ToString();
            Total_Ajustes_Unidad = Total_Entradas_Unidad + Total_Salidas_Unidas;
            Lbl_Unidades_Ajustadas.Text = Total_Ajustes_Unidad.ToString();
            //Total_Ajustes_Unidad = Total_Entradas_Unidad + Total_Salidas_Unidas;
            Saldo = Total_Importe_Entradas - Total_Importe_Salidas;
            Lbl_Importe_Saldo.Text = Saldo.ToString();
            Lbl_Importe_Saldo.ForeColor = (Saldo > 0) ? System.Drawing.Color.Blue : System.Drawing.Color.Red;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Limpiar_Form
        ///DESCRIPCIÓN: Limpia el Formulario
        ///PARAMETROS: 
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 16/Julio/2012
        ///CAUSA_MODIFICACIÓN: Adaptar a Taller Mecanico Municipal
        ///*******************************************************************************
        private void Limpiar_Form()
        {
            Hdf_Refaccion_ID.Value = "";
            Txt_Importe.Text = "";
            Txt_Costo.Text = "";
            Txt_Diferencia.Text = "";
            Txt_Existencia.Text = "";
            Txt_Conteo_Fisico.Text = "";
            Txt_Clave.Text = "";
            Txt_Producto.Text = "";
            Txt_Descripcion.Text = "";
        }

    #endregion

    #region EVENTOS

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Seleccionar_Producto_Click
        ///DESCRIPCIÓN: Elimina el Producto Seleccionado
        ///PARAMETROS: 
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 16/Julio/2012
        ///CAUSA_MODIFICACIÓN: Adaptar a Taller Mecanico Municipal
        ///*******************************************************************************
        protected void Btn_Seleccionar_Producto_Click(object sender, ImageClickEventArgs e)
        {
            String Producto_ID = ((ImageButton)sender).CommandArgument;
            DataTable Dt_Productos = (DataTable)Session[P_Dt_Productos_Ajustados];
            DataRow[] _DataRow = Dt_Productos.Select("REFACCION_ID = '" + Producto_ID + "'");
            if (_DataRow != null && _DataRow.Length > 0)
            {
                Dt_Productos = Agregar_Quitar_Renglones_A_DataTable(Dt_Productos, _DataRow[0], Operacion_Quitar_Renglon);
                Session[P_Dt_Productos_Ajustados] = Dt_Productos;
                Resumen_Movimientos();
                Grid_Productos_Ajustados.DataSource = Dt_Productos;
                Grid_Productos_Ajustados.DataBind();
            }
            else
            {
                Mostrar_Informacion("Seleccione producto para eliminar de la lista", false);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Click
        ///DESCRIPCIÓN: Busca el Ajuste
        ///PARAMETROS: 
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 16/Julio/2012
        ///CAUSA_MODIFICACIÓN: Adaptar a Taller Mecanico Municipal
        ///*******************************************************************************
        protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e) {
            Llenar_Grid_Ajustes_Inventario();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Producto_Click
        ///DESCRIPCIÓN: Busca el Producto
        ///PARAMETROS: 
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 16/Julio/2012
        ///CAUSA_MODIFICACIÓN: Adaptar a Taller Mecanico Municipal
        ///*******************************************************************************
        protected void Btn_Buscar_Producto_Click(object sender, ImageClickEventArgs e) {

            if (Txt_Clave.Text.Trim().Length > 0) {
                Cls_Ope_Tal_Ajustar_Stock_Negocio Negocio = new Cls_Ope_Tal_Ajustar_Stock_Negocio();
                Negocio.P_Clave = Txt_Clave.Text.Trim();
                DataTable Dt_Productos = Negocio.Consultar_Productos();
                Session[P_Dt_Producto_Buscado] = Dt_Productos;
                Cargar_Datos_Producto();
                Txt_Conteo_Fisico.Enabled = true;
                Txt_Importe.Text = "";
                Txt_Diferencia.Text = "";
                Txt_Conteo_Fisico.Text = "";
                Txt_Conteo_Fisico.Focus();
            }  else {
                Mostrar_Informacion("Ingrese una clave para buscar", true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Sale del Formulario
        ///PARAMETROS: 
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 16/Julio/2012
        ///CAUSA_MODIFICACIÓN: Adaptar a Taller Mecanico Municipal
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e) {
            if (Btn_Salir.ToolTip == "Regresar") {
                Habilitar_Botones(MODO_LISTADO);
                Div_Listado_Ajustes.Visible = true;
                Div_Contenido.Visible = false;
                Llenar_Grid_Ajustes_Inventario();
            } else {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Seleccionar_Inventario_Click
        ///DESCRIPCIÓN: 
        ///PARAMETROS: 
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 16/Julio/2012
        ///CAUSA_MODIFICACIÓN: Adaptar a Taller Mecanico Municipal
        ///*******************************************************************************
        protected void Btn_Seleccionar_Inventario_Click(object sender, ImageClickEventArgs e) {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Requisiciones", "alert('Opción temporalmente deshabilitada');", true);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Imprimir_Ajuste_Inventario_Click
        ///DESCRIPCIÓN: 
        ///PARAMETROS: 
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 16/Julio/2012
        ///CAUSA_MODIFICACIÓN: Adaptar a Taller Mecanico Municipal
        ///*******************************************************************************
        protected void Btn_Imprimir_Ajuste_Inventario_Click(object sender, ImageClickEventArgs e) {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Requisiciones", "alert('Opción temporalmente deshabilitada');", true);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
        ///DESCRIPCIÓN: Ejecuta el Evento para un Nuevo Ajuste
        ///PARAMETROS: 
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 16/Julio/2012
        ///CAUSA_MODIFICACIÓN: Adaptar a Taller Mecanico Municipal
        ///*******************************************************************************
        protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e) {
            Session[P_Dt_Productos_Ajustados] = Construir_Tabla_Productos_Ajustados();
            Habilitar_Botones(MODO_NUEVO);
            Txt_Justificacion.Text = "Ajuste de Stock.";
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Guardar_Click
        ///DESCRIPCIÓN: Guarda el Ajuste
        ///PARAMETROS: 
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 16/Julio/2012
        ///CAUSA_MODIFICACIÓN: Adaptar a Taller Mecanico Municipal
        ///*******************************************************************************
        protected void Btn_Guardar_Click(object sender, ImageClickEventArgs e) {
            if (Session[P_Dt_Productos_Ajustados] != null) {
                DataTable Dt_Productos = Session[P_Dt_Productos_Ajustados] as DataTable;
                if (Dt_Productos.Rows.Count > 0) {
                    if (Txt_Justificacion.Text.Trim().Length > 0) {
                        Cls_Ope_Tal_Ajustar_Stock_Negocio Negocio = new Cls_Ope_Tal_Ajustar_Stock_Negocio();
                        Negocio.P_Dt_Productos_Ajustados = Session[P_Dt_Productos_Ajustados] as DataTable;
                        Negocio.P_Estatus = "GENERADO";
                        Negocio.P_Motivo_Ajuste_Coordinador = Txt_Justificacion.Text.Trim();
                        try {
                            int Consecutivo = Negocio.Guardar_Ajustes_Inventario();
                            //comprometer productos que su operación es SALIDA
                            if (Consecutivo > 0) {
                                DataTable Dt_Ajuste = (DataTable)Session[P_Dt_Productos_Ajustados];
                                foreach (DataRow Dr_Producto in Dt_Ajuste.Rows) {
                                    if (Dr_Producto["TIPO_MOVIMIENTO"].ToString() == "SALIDA") {
                                        Cls_Ope_Tal_Control_Stock.Comprometer_Producto
                                            (Dr_Producto["REFACCION_ID"].ToString(), int.Parse(Dr_Producto["DIFERENCIA"].ToString()));
                                    }
                                }
                            }
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Requisiciones", "alert('Ajuste registrado con el folio: AI-" + Consecutivo + "');", true);
                            Session[P_Dt_Ajustes_Stock] = null;
                            Session[P_Dt_Producto_Buscado] = null;
                            Limpiar_Form();
                            Llenar_Grid_Ajustes_Inventario();
                            Habilitar_Botones(MODO_LISTADO);
                        } catch (Exception Ex) {
                            Mostrar_Informacion(Ex.ToString(), true);
                        }
                    } else {
                        Mostrar_Informacion("Escriba la justificación para el ajuste de stock", true);
                    }
                } else {
                    Mostrar_Informacion("No se puede guardar un ajuste sin productos", true);
                }
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Agregar_Click
        ///DESCRIPCIÓN: Agrega el Producto al Ajuste
        ///PARAMETROS: 
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 16/Julio/2012
        ///CAUSA_MODIFICACIÓN: Adaptar a Taller Mecanico Municipal
        ///*******************************************************************************
        protected void Btn_Agregar_Click(object sender, EventArgs e)
        {
            if (Txt_Producto.Text.Trim().Length > 0)
            {
                if (Txt_Diferencia.Text.Trim().Length > 0)
                {
                    if (int.Parse(Txt_Diferencia.Text.Trim()) != 0)
                    {
                        if (!Duplicado(((DataTable)Session[P_Dt_Productos_Ajustados]), Hdf_Refaccion_ID.Value))
                        {
                            int Disponible_Stock = Cls_Ope_Tal_Control_Stock.Consultar_Disponible(Hdf_Refaccion_ID.Value.Trim());
                            if (Math.Abs(int.Parse(Txt_Diferencia.Text.Trim())) > Disponible_Stock && Cmb_Movimiento.SelectedItem.Text.Trim() == "SALIDA")
                            {
                                Mostrar_Informacion("No se puede realizar salida del producto seleccionado, " +
                                    "es posible que existan productos comprometidos", true);
                            }
                            else
                            {
                                DataRow Renglon_Producto = ((DataTable)Session[P_Dt_Productos_Ajustados]).NewRow();
                                Renglon_Producto["REFACCION_ID"] = Hdf_Refaccion_ID.Value.Trim();
                                Renglon_Producto["CLAVE"] = Txt_Clave.Text.Trim();
                                Renglon_Producto["NOMBRE_DESCRIPCION"] = Txt_Producto.Text + ", " + Txt_Descripcion.Text.Trim();
                                Renglon_Producto["IMPORTE_DIFERENCIA"] = Math.Abs(double.Parse(Txt_Importe.Text.Trim())).ToString();// Txt_Importe.Text.Trim();
                                Renglon_Producto["DIFERENCIA"] = Math.Abs(int.Parse(Txt_Diferencia.Text.Trim())).ToString();
                                Renglon_Producto["TIPO_MOVIMIENTO"] = Cmb_Movimiento.SelectedItem.Text.Trim();
                                Renglon_Producto["EXISTENCIA_SISTEMA"] = Txt_Existencia.Text.Trim();
                                Renglon_Producto["CONTEO_FISICO"] = Txt_Conteo_Fisico.Text.Trim();
                                Renglon_Producto["PRECIO_PROMEDIO"] = Txt_Costo.Text.Trim();
                                Agregar_Quitar_Renglones_A_DataTable(((DataTable)Session[P_Dt_Productos_Ajustados]), Renglon_Producto, Operacion_Agregar_Renglon_Nuevo);
                                Grid_Productos_Ajustados.DataSource = (DataTable)Session[P_Dt_Productos_Ajustados];
                                Grid_Productos_Ajustados.DataBind();
                                Txt_Conteo_Fisico.Enabled = false;
                                Resumen_Movimientos();
                                Limpiar_Form();
                                Txt_Clave.Focus();
                            }
                        }
                        else
                        {
                            Mostrar_Informacion("El producto que desea agregar ya se encuentra en la lista", true);
                        }
                    }
                    else
                    {
                        Mostrar_Informacion("No se encontraron diferencias para ajuste", true);
                    }
                }
                else
                {
                    Mostrar_Informacion("Debe Ingresar el Conteo Físico del Producto", true);
                }
            }
            else
            {
                Mostrar_Informacion("Debe buscar un producto", true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Txt_Conteo_Fisico_TextChanged
        ///DESCRIPCIÓN: Conteo Fisico
        ///PARAMETROS: 
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 16/Julio/2012
        ///CAUSA_MODIFICACIÓN: Adaptar a Taller Mecanico Municipal
        ///*******************************************************************************
        protected void Txt_Conteo_Fisico_TextChanged(object sender, EventArgs e)
        {
            int Diferencia = int.Parse(Txt_Conteo_Fisico.Text.Trim()) - int.Parse(Txt_Existencia.Text.Trim());
            Txt_Diferencia.Text = Diferencia.ToString();
            double Importe = (double.Parse(Txt_Costo.Text.Trim()) * Diferencia);
            Txt_Importe.Text = String.Format("{0:n}", Importe);
            if (Diferencia > 0)
            {
                Txt_Diferencia.ForeColor = System.Drawing.Color.Blue;
                Txt_Importe.ForeColor = System.Drawing.Color.Blue;
                Cmb_Movimiento.SelectedValue = "ENTRADA";
            }
            else
            {
                Txt_Diferencia.ForeColor = System.Drawing.Color.Red;
                Txt_Importe.ForeColor = System.Drawing.Color.Red;
                Cmb_Movimiento.SelectedValue = "SALIDA";
            }
            Btn_Agregar.Focus();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Refaccion_Click
        ///DESCRIPCIÓN         : Lanza la Busqueda de la Refaccion
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  : 
        ///*******************************************************************************
        protected void Btn_Busqueda_Refaccion_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["Tabla_Refacciones_Seleccionados"] != null)
            {
                DataTable Dt_Tabla_Refacciones_Seleccionados = (DataTable)Session["Tabla_Refacciones_Seleccionados"];
                if (Dt_Tabla_Refacciones_Seleccionados.Rows.Count > 0)
                {
                    Txt_Clave.Text = Dt_Tabla_Refacciones_Seleccionados.Rows[0]["CLAVE"].ToString();
                    Btn_Buscar_Producto_Click(Btn_Buscar_Producto, null); 
                }
            }
            Session.Remove("Tabla_Refacciones_Seleccionados");
        }

    #endregion

}