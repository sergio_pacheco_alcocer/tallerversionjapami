﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Entradas_Vehiculos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Salidas_Vehiculos.Negocio;
using JAPAMI.Taller_Mecanico.Catalogo_Partes_Vehiculos.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Control_Patrimonial_Operacion_Vehiculos.Negocio;
using JAPAMI.Empleados.Negocios;
using System.Text.RegularExpressions;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Preventivos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Correctivos.Negocio;

public partial class paginas_Taller_Mecanico_Frm_Ope_Tal_Entrega_Vehiculo : System.Web.UI.Page {

    #region Page_Load
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN: Carga la Pagina Inicial
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 05/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e) {
            Div_Contenedor_Msj_Error.Visible = false;
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack) {
                Llenar_Combo_Unidades_Responsables();
                Grid_Listado_Solicitudes.PageIndex = 0;
                Llenar_Listado_Solicitudes();
                Configuracion_Formulario("INICIAL");
                Cmb_Unidad_Responsable.Enabled = false;
                Llenar_Grid_Listado_Detalles();
                Llenar_Grid_Listado_Detalles_Recepcion();
            }
        }

    #endregion

    #region Metodos

        #region Llenado de Campos [Combos, Listados, Vehiculos]

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Unidades_Responsables
            ///DESCRIPCIÓN: Se llena el Combo de las Unidades Responsables.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Combo_Unidades_Responsables() {
                Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                Negocio.P_Estatus = "ACTIVO";
                DataTable Dt_Dependencias = Negocio.Consultar_Unidades_Responsables();
                Cmb_Unidad_Responsable.DataSource = Dt_Dependencias;
                Cmb_Unidad_Responsable.DataTextField = "CLAVE_NOMBRE";
                Cmb_Unidad_Responsable.DataValueField = "DEPENDENCIA_ID";
                Cmb_Unidad_Responsable.DataBind();
                Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
                Cmb_Busqueda_Dependencia.DataSource = Dt_Dependencias;
                Cmb_Busqueda_Dependencia.DataTextField = "CLAVE_NOMBRE";
                Cmb_Busqueda_Dependencia.DataValueField = "DEPENDENCIA_ID";
                Cmb_Busqueda_Dependencia.DataBind();
                Cmb_Busqueda_Dependencia.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Solicitudes
            ///DESCRIPCIÓN: Se llena el Listado de las Solicitudes.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Listado_Solicitudes() {
                Cls_Ope_Tal_Servicios_Preventivos_Negocio Serv_Prev = new Cls_Ope_Tal_Servicios_Preventivos_Negocio();
                Serv_Prev.P_Estatus_Solicitud = "TERMINADO";
                Serv_Prev.P_Procedencia = "SOLICITUD";
                DataTable Dt_Resultados_Preventivos = Serv_Prev.Consultar_Servicios_Preventivos();

                Cls_Ope_Tal_Servicios_Correctivos_Negocio Serv_Correc = new Cls_Ope_Tal_Servicios_Correctivos_Negocio();
                Serv_Correc.P_Estatus_Solicitud = "TERMINADO";
                Serv_Correc.P_Procedencia = "SOLICITUD";
                DataTable Dt_Resultados_Correctivos = Serv_Correc.Consultar_Servicios_Correctivos();

                Dt_Resultados_Preventivos.Merge(Dt_Resultados_Correctivos);

                Grid_Listado_Solicitudes.Columns[1].Visible = true;
                Grid_Listado_Solicitudes.Columns[2].Visible = true;
                Grid_Listado_Solicitudes.Columns[3].Visible = true;
                Grid_Listado_Solicitudes.DataSource = Dt_Resultados_Preventivos;
                Grid_Listado_Solicitudes.DataBind();
                Grid_Listado_Solicitudes.Columns[1].Visible = false;
                Grid_Listado_Solicitudes.Columns[2].Visible = false;
                Grid_Listado_Solicitudes.Columns[3].Visible = false;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Vehiculo
            ///DESCRIPCIÓN: Se cargan los Datos del Vehiculo Seleccionado.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Cargar_Datos_Vehiculo(String Vehiculo, String Tipo_Busqueda) {
                Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                switch (Tipo_Busqueda) { 
                    case "NO_INVENTARIO":
                        Consulta_Negocio.P_No_Inventario = Vehiculo;
                        break;
                    case "IDENTIFICADOR":
                        Consulta_Negocio.P_Vehiculo_ID = Vehiculo;
                        break;
                    default: break;
                }
                if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Consulta_Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value; }
                DataTable Dt_Vehiculo = Consulta_Negocio.Consultar_Vehiculos();
                if (Dt_Vehiculo.Rows.Count > 0) {
                    Hdf_Vehiculo_ID.Value = Dt_Vehiculo.Rows[0]["VEHICULO_ID"].ToString().Trim();
                    Txt_No_Inventario.Text = Dt_Vehiculo.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                    Txt_No_Economico.Text = Dt_Vehiculo.Rows[0]["NO_ECONOMICO"].ToString().Trim();
                    Txt_Datos_Vehiculo.Text = Dt_Vehiculo.Rows[0]["VEHICULO_DESCRIPCION"].ToString().Trim();
                    Txt_Placas.Text = Dt_Vehiculo.Rows[0]["PLACAS"].ToString().Trim();
                    Txt_Anio.Text = Dt_Vehiculo.Rows[0]["ANIO"].ToString().Trim();
                    Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString()));
                } else {
                    Lbl_Ecabezado_Mensaje.Text = "El Vehículo no se encontro.";
                    if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Lbl_Mensaje_Error.Text = "[Puede ser que no este asignado a la Unidad Responsable Seleccionada]."; }
                    else { Lbl_Mensaje_Error.Text = ""; }
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Bien_Mueble
            ///DESCRIPCIÓN: Se cargan los Datos del Bien Mueble Seleccionado.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Cargar_Datos_Bien_Mueble(String Bien_Mueble, String Tipo_Busqueda)
            {
                Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                switch (Tipo_Busqueda)
                {
                    case "NO_INVENTARIO":
                        Consulta_Negocio.P_No_Inventario = Bien_Mueble;
                        break;
                    case "IDENTIFICADOR":
                        Consulta_Negocio.P_Bien_Mueble_ID = Bien_Mueble;
                        break;
                    default: break;
                }
                DataTable Dt_Bienes_Muebles = Consulta_Negocio.Consultar_Bien_Mueble();
                if (Dt_Bienes_Muebles.Rows.Count > 0)
                {
                    Hdf_Bien_Mueble_ID.Value = Dt_Bienes_Muebles.Rows[0]["BIEN_MUEBLE_ID"].ToString().Trim();
                    Txt_No_Inventario_BM.Text = Dt_Bienes_Muebles.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                    Txt_Numero_Serie_Bien.Text = Dt_Bienes_Muebles.Rows[0]["NUMERO_SERIE"].ToString().Trim();
                    Txt_Descripcion_Bien.Text = Dt_Bienes_Muebles.Rows[0]["DESCRIPCION_BIEN_MUEBLE"].ToString().Trim();
                    if (!String.IsNullOrEmpty(Dt_Bienes_Muebles.Rows[0]["DEPENDENCIA_ID"].ToString()))
                    {
                        Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Bienes_Muebles.Rows[0]["DEPENDENCIA_ID"].ToString()));
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Text = "ERROR [Puede ser que el Bien esté dado de Baja].";
                        Cmb_Unidad_Responsable.SelectedIndex = 0;
                    }
                }
                else
                {
                    Lbl_Ecabezado_Mensaje.Text = "El Bien no se encontro.";
                    if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Lbl_Mensaje_Error.Text = "[Puede ser que no este asignado a la Unidad Responsable Seleccionada]."; }
                    else { Lbl_Mensaje_Error.Text = ""; }
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Crear_Tabla_Detalles
            ///DESCRIPCIÓN: Se crea la Tabla DataTable de los Detalles
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private DataTable Crear_Tabla_Detalles() {
                DataTable Dt_Detalles = new DataTable();
                Dt_Detalles.Columns.Add("PARTE_ID", Type.GetType("System.String"));
                Dt_Detalles.Columns.Add("SUBPARTE_ID", Type.GetType("System.String"));
                Dt_Detalles.Columns.Add("CANT_UNIFICAR", Type.GetType("System.Int32"));
                Dt_Detalles.Columns.Add("NOMBRE_PARTE", Type.GetType("System.String"));
                Dt_Detalles.Columns.Add("NOMBRE_SUBPARTE", Type.GetType("System.String"));
                Dt_Detalles.Columns.Add("VALOR", Type.GetType("System.String"));

                Cls_Cat_Tal_Partes_Vehiculos_Negocio Negocio = new Cls_Cat_Tal_Partes_Vehiculos_Negocio();
                Negocio.P_Estatus = "VIGENTE";
                Negocio.P_Tipo = "PARTE";
                DataTable Dt_Parents = Negocio.Consultar_Partes();
                foreach (DataRow Fila_Parent in Dt_Parents.Rows) {
                    String Parte_ID = Fila_Parent["PARTE_ID"].ToString();
                    String Nombre_Parte = Fila_Parent["NOMBRE"].ToString();
                    Cls_Cat_Tal_Partes_Vehiculos_Negocio Tmp = new Cls_Cat_Tal_Partes_Vehiculos_Negocio();
                    Tmp.P_Estatus = "VIGENTE";
                    Tmp.P_Tipo = "SUBPARTE";
                    Tmp.P_Parent = Parte_ID;
                    DataTable Dt_Childs = Tmp.Consultar_Partes();
                    Boolean Cant_Unificar = true;
                    foreach (DataRow Fila_Child in Dt_Childs.Rows) {
                        DataRow Fila_Nueva = Dt_Detalles.NewRow();
                        Fila_Nueva["PARTE_ID"] = Parte_ID;
                        Fila_Nueva["SUBPARTE_ID"] = Fila_Child["PARTE_ID"].ToString();
                        if (Cant_Unificar) {
                            Fila_Nueva["CANT_UNIFICAR"] = Dt_Childs.Rows.Count;
                            Cant_Unificar = false;
                        }
                        Fila_Nueva["NOMBRE_PARTE"] = Nombre_Parte;
                        Fila_Nueva["NOMBRE_SUBPARTE"] = Fila_Child["NOMBRE"].ToString();
                        Fila_Nueva["VALOR"] = "---";
                        Dt_Detalles.Rows.Add(Fila_Nueva);
                    }
                }
                return Dt_Detalles;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Listado_Detalles
            ///DESCRIPCIÓN: Llena la Tabla con los detalles de las Partes a Revisar
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Grid_Listado_Detalles() {
                Grid_Listado_Detalles.Columns[0].Visible = true;
                Grid_Listado_Detalles.Columns[1].Visible = true;
                Grid_Listado_Detalles.Columns[2].Visible = true;
                Grid_Listado_Detalles.DataSource = Crear_Tabla_Detalles();
                Grid_Listado_Detalles.DataBind();
                Agrupar_Partes_Listado_Detalles(ref Grid_Listado_Detalles);
                Grid_Listado_Detalles.Columns[0].Visible = false;
                Grid_Listado_Detalles.Columns[1].Visible = false;
                Grid_Listado_Detalles.Columns[2].Visible = false;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Listado_Detalles_Recepcion
            ///DESCRIPCIÓN: Llena la Tabla con los detalles de las Partes a Revisar
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Grid_Listado_Detalles_Recepcion(){
                Grid_Listado_Detalles_Recepcion.Columns[0].Visible = true;
                Grid_Listado_Detalles_Recepcion.Columns[1].Visible = true;
                Grid_Listado_Detalles_Recepcion.Columns[2].Visible = true;
                Grid_Listado_Detalles_Recepcion.DataSource = Crear_Tabla_Detalles();
                Grid_Listado_Detalles_Recepcion.DataBind();
                Agrupar_Partes_Listado_Detalles(ref Grid_Listado_Detalles_Recepcion);
                Grid_Listado_Detalles_Recepcion.Columns[0].Visible = false;
                Grid_Listado_Detalles_Recepcion.Columns[1].Visible = false;
                Grid_Listado_Detalles_Recepcion.Columns[2].Visible = false;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Agrupar_Partes_Listado_Detalles
            ///DESCRIPCIÓN: Unifica las Celdas
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Agrupar_Partes_Listado_Detalles(ref GridView Grid_Listado) {
                Boolean Primer_Color = true;
                foreach (GridViewRow Fila_Actual in Grid_Listado.Rows) { 
                    if (!String.IsNullOrEmpty(HttpUtility.HtmlDecode(Fila_Actual.Cells[2].Text).Trim())) {
                        Int32 Cant_Unificar = Convert.ToInt32(HttpUtility.HtmlDecode(Fila_Actual.Cells[2].Text).Trim());
                        if (Cant_Unificar > 0) {
                            TableCell Celda = Fila_Actual.Cells[3];
                            Celda.RowSpan = Cant_Unificar;
                            if (Primer_Color) { Celda.BackColor = System.Drawing.Color.White; Primer_Color = false; }
                            else { Celda.BackColor = System.Drawing.Color.FromArgb(230, 230, 230); Primer_Color = true; }
                            Celda.BorderColor = System.Drawing.Color.FromArgb(47, 78, 125);
                            Celda.BorderWidth = Convert.ToInt32(2);
                        } else {
                            TableCell Celda = Fila_Actual.Cells[3];
                            Celda.Visible = false;
                        } 
                        Fila_Actual.BorderColor = System.Drawing.Color.FromArgb(47, 78, 125);
                        Fila_Actual.BorderWidth = Convert.ToInt32(1);
                    }
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Busqueda_Empleados_Resguardo
            ///DESCRIPCIÓN: Llena el Grid con los empleados que cumplan el filtro
            ///PROPIEDADES:     
            ///CREO:                 
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private void Llenar_Grid_Busqueda_Empleados_Resguardo() {
                Grid_Busqueda_Empleados_Resguardo.SelectedIndex = (-1);
                Grid_Busqueda_Empleados_Resguardo.Columns[1].Visible = true;
                Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                if (Txt_Busqueda_No_Empleado.Text.Trim().Length > 0) { Negocio.P_No_Empleado = Txt_Busqueda_No_Empleado.Text.Trim(); }
                if (Txt_Busqueda_RFC.Text.Trim().Length > 0) { Negocio.P_RFC_Empleado = Txt_Busqueda_RFC.Text.Trim(); }
                if (Txt_Busqueda_Nombre_Empleado.Text.Trim().Length > 0) { Negocio.P_Nombre_Empleado = Txt_Busqueda_Nombre_Empleado.Text.Trim().ToUpper(); }
                if (Cmb_Busqueda_Dependencia.SelectedIndex > 0) { Negocio.P_Dependencia_ID = Cmb_Busqueda_Dependencia.SelectedItem.Value; }
                Grid_Busqueda_Empleados_Resguardo.DataSource = Negocio.Consultar_Empleados();
                Grid_Busqueda_Empleados_Resguardo.DataBind();
                Grid_Busqueda_Empleados_Resguardo.Columns[1].Visible = false;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cargar_Grid_Detalles_Entrega
            ///DESCRIPCIÓN: Carga el Grid de los detalles con los predeterminados de Entrada
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Cargar_Grid_Detalles_Entrega(DataTable Dt_Detalles) {
                foreach (GridViewRow Fila_Grid in Grid_Listado_Detalles.Rows) {
                    RadioButton RBtn_SI = null;
                    if (Fila_Grid.FindControl("RBtn_SI") != null) {
                        RBtn_SI = (RadioButton)Fila_Grid.FindControl("RBtn_SI");
                    }
                    RadioButton RBtn_NO = null;
                    if (Fila_Grid.FindControl("RBtn_NO") != null) {
                        RBtn_NO = (RadioButton)Fila_Grid.FindControl("RBtn_NO");
                    }
                    String SubParte = HttpUtility.HtmlDecode(Fila_Grid.Cells[1].Text.Trim());
                    DataRow[] Filas = Dt_Detalles.Select("SUBPARTE_ID = '" + SubParte.Trim() + "'");
                    if (Filas.Length > 0) {
                        if (Filas[0]["VALOR"].ToString().Trim().Equals("SI")) {
                            RBtn_NO.Checked = false;
                            RBtn_SI.Checked = true;
                        } else if (Filas[0]["VALOR"].ToString().Trim().Equals("NO")) {
                            RBtn_NO.Checked = true;
                            RBtn_SI.Checked = false;
                        }
                    }
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cargar_Grid_Detalles_Recepcion
            ///DESCRIPCIÓN: Carga el Grid de los detalles con los predeterminados de Entrada
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 01/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Cargar_Grid_Detalles_Recepcion(DataTable Dt_Detalles) {
                foreach (GridViewRow Fila_Grid in Grid_Listado_Detalles_Recepcion.Rows) {
                    String Valor = "---";
                    String SubParte = HttpUtility.HtmlDecode(Fila_Grid.Cells[1].Text.Trim());
                    DataRow[] Filas = Dt_Detalles.Select("SUBPARTE_ID = '" + SubParte.Trim() + "'");
                    if (Filas.Length > 0) {
                        Valor = Filas[0]["VALOR"].ToString().Trim();
                    }
                    Fila_Grid.Cells[5].Text = Valor.Trim();
                }
            }

        #endregion

        #region Generales [Configuracion, Limpiar]
                
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
            ///DESCRIPCIÓN: Limpia los campos del Formulario.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Limpiar_Formulario() {
                Hdf_No_Solicitud.Value = "";
                Hdf_No_Servicio.Value = "";
                Hdf_No_Entrada.Value = "";
                Txt_Folio.Text = "";
                Txt_Fecha_Elaboracion.Text = "";
                Cmb_Tipo_Servicio.SelectedIndex = 0;
                Cmb_Unidad_Responsable.SelectedIndex = 0;
                Txt_Km_Solicitud.Text = "";
                Txt_Descripcion_Servicio.Text = "";
                Hdf_Vehiculo_ID.Value = "";
                Txt_No_Inventario.Text = "";
                Txt_No_Economico.Text = "";
                Txt_Datos_Vehiculo.Text = "";
                Txt_Placas.Text = "";
                Txt_Anio.Text = "";
                Txt_Fecha_Recepcion_Programada.Text = "";
                Txt_Fecha_Recepcion_Real.Text = "";
                Hdf_Empleado_Entrega_ID.Value = "";
                Txt_Empleado_Entrega.Text = "";
                Txt_Kilometraje.Text = "";
                Txt_Comentarios_Recepción.Text = "";
                Txt_Fecha_Entrega.Text = "";
                Hdf_Empleado_Recibe.Value = "";
                Txt_Empleado_Recibe.Text = "";
                Txt_Kilometraje_Entrega.Text = "";
                Txt_Comentarios_Entrega.Text = "";
                Limpiar_Grid_Detalles();
                Llenar_Grid_Listado_Detalles_Recepcion();
            }
    
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Limpiar_Grid_Detalles
            ///DESCRIPCIÓN: Limpia el Grid de Detalles.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Limpiar_Grid_Detalles() {
                foreach (GridViewRow Fila_Actual in Grid_Listado_Detalles.Rows) {
                    if (Fila_Actual.FindControl("RBtn_SI") != null) {
                        RadioButton RBtn_SI = (RadioButton)Fila_Actual.FindControl("RBtn_SI");
                        RBtn_SI.Checked = true;
                    }
                    if (Fila_Actual.FindControl("RBtn_NO") != null) {
                        RadioButton RBtn_NO = (RadioButton)Fila_Actual.FindControl("RBtn_NO");
                        RBtn_NO.Checked = false;
                    }
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
            ///DESCRIPCIÓN: Se Habilitan y/o inhabilitan los campos dependendiendo de la Condicion.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Configuracion_Formulario(String Operacion) {
                switch (Operacion) {
                    case "INICIAL":
                        Div_Campos.Visible = false;
                        Div_Listado_Solicitudes.Visible = true;
                        Btn_Hacer_Entrega.Visible = false;
                        break;
                    case "OPERACION":
                        Div_Campos.Visible = true;
                        Div_Listado_Solicitudes.Visible = false;
                        Btn_Hacer_Entrega.Visible = true;
                        break;
                }
            }

        #endregion

        #region Clase de Negocio de Solicitudes [Registrar y Consulta]

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Mostrar_Registro
            ///DESCRIPCIÓN: Muestra el Registro en los campos.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Mostrar_Registro() {
                Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                Solicitud.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                Solicitud = Solicitud.Consultar_Detalles_Solicitud_Servicio();
                if (Solicitud.P_No_Solicitud > (-1)) {
                    Txt_Folio.Text = Solicitud.P_Folio_Solicitud.Trim();
                    Txt_Fecha_Elaboracion.Text = String.Format("{0:dd/MMM/yyyy}", Solicitud.P_Fecha_Elaboracion);
                    Cmb_Tipo_Servicio.SelectedIndex = Cmb_Tipo_Servicio.Items.IndexOf(Cmb_Tipo_Servicio.Items.FindByValue(Solicitud.P_Tipo_Servicio));
                    Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Solicitud.P_Dependencia_ID));
                    Hdf_Vehiculo_ID.Value = Solicitud.P_Bien_ID;
                    Txt_Descripcion_Servicio.Text = Solicitud.P_Descripcion_Servicio;
                    if (Solicitud.P_Kilometraje > (-1.0)) {
                        Txt_Km_Solicitud.Text = String.Format("{0:############0}", Solicitud.P_Kilometraje);
                    }
                    if (Solicitud.P_Tipo_Bien.Equals("BIEN_MUEBLE"))
                    {
                        Pnl_Bien_Mueble_Seleccionado.Visible = true;
                        Pnl_Vehiculo_Seleccionado.Visible = false;
                        Hdf_Bien_Mueble_ID.Value = Solicitud.P_Bien_ID;
                        Hdf_Tipo_Bien.Value = "BIEN_MUEBLE";
                        Cargar_Datos_Bien_Mueble(Hdf_Bien_Mueble_ID.Value, "IDENTIFICADOR");
                    }
                    else if (Solicitud.P_Tipo_Bien.Equals("VEHICULO"))
                    {
                        Pnl_Vehiculo_Seleccionado.Visible = true;
                        Pnl_Bien_Mueble_Seleccionado.Visible = false;
                        Hdf_Vehiculo_ID.Value = Solicitud.P_Bien_ID;
                        Hdf_Tipo_Bien.Value = "VEHICULO";
                        Cargar_Datos_Vehiculo(Hdf_Vehiculo_ID.Value, "IDENTIFICADOR");
                    }
                    Txt_Fecha_Recepcion_Programada.Text = String.Format("{0:dd/MMM/yyyy}", Solicitud.P_Fecha_Recepcion_Programada);
                    Txt_Fecha_Recepcion_Real.Text = String.Format("{0:dd/MMM/yyyy}", Solicitud.P_Fecha_Recepcion_Real);
                }
                if (Hdf_No_Entrada.Value.Trim().Length > 0) {
                    Cls_Ope_Tal_Entradas_Vehiculos_Negocio Entr_Negocio = new Cls_Ope_Tal_Entradas_Vehiculos_Negocio();
                    Entr_Negocio.P_No_Entrada = Convert.ToInt32(Hdf_No_Entrada.Value);
                    Entr_Negocio = Entr_Negocio.Consultar_Detalles_Entrada_Vehiculo();
                    if (Entr_Negocio.P_No_Entrada > (-1)) {
                        if (Entr_Negocio.P_Kilometraje > (-1.0)) { Txt_Kilometraje.Text = String.Format("{0:######0}", Entr_Negocio.P_Kilometraje); Txt_Kilometraje_Entrega.Text = String.Format("{0:######0}", Entr_Negocio.P_Kilometraje); }
                        Mostrar_Informacion_Empleado(Entr_Negocio.P_Empleado_Entrego_ID.Trim());
                        Txt_Comentarios_Recepción.Text = Entr_Negocio.P_Comentarios.Trim();
                        Cargar_Grid_Detalles_Recepcion(Entr_Negocio.P_Dt_Detalles);
                        Cargar_Grid_Detalles_Entrega(Entr_Negocio.P_Dt_Detalles);
                    }
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Mostrar_Informacion_Empleado
            ///DESCRIPCIÓN: Muestra los Generales del Empleado
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Mostrar_Informacion_Empleado(String Empleado_ID) {
                ///EMPLEADO QUE ENTREGA
                Hdf_Empleado_Entrega_ID.Value = "";
                Txt_Empleado_Entrega.Text = "";
                Cls_Cat_Empleados_Negocios Empleado_Negocio = new Cls_Cat_Empleados_Negocios();
                Empleado_Negocio.P_Empleado_ID = Empleado_ID;
                DataTable Dt_Datos_Empleado = Empleado_Negocio.Consulta_Empleados_General();
                Hdf_Empleado_Entrega_ID.Value = ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString().Trim() : null);
                Txt_Empleado_Entrega.Text += ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString().Trim() : null);
                Txt_Empleado_Entrega.Text += " - " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString().Trim() : null);
                Txt_Empleado_Entrega.Text = Txt_Empleado_Entrega.Text.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString().Trim() : null);
                Txt_Empleado_Entrega.Text = Txt_Empleado_Entrega.Text.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString().Trim() : null);
                ///EMPLEADO QUE RECIBE
                Hdf_Empleado_Entrega_ID.Value = "";
                Txt_Empleado_Recibe.Text = "";
                Hdf_Empleado_Recibe.Value = ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString().Trim() : null);
                Txt_Empleado_Recibe.Text += ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString().Trim() : null);
                Txt_Empleado_Recibe.Text += " - " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString().Trim() : null);
                Txt_Empleado_Recibe.Text = Txt_Empleado_Entrega.Text.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString().Trim() : null);
                Txt_Empleado_Recibe.Text = Txt_Empleado_Entrega.Text.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString().Trim() : null);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Mostrar_Informacion_Empleado_Recibe
            ///DESCRIPCIÓN: Muestra los Generales del Empleado
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Mostrar_Informacion_Empleado_Recibe(String Empleado_ID) {
                Hdf_Empleado_Recibe.Value = "";
                Txt_Empleado_Recibe.Text = "";
                Cls_Cat_Empleados_Negocios Empleado_Negocio = new Cls_Cat_Empleados_Negocios();
                Empleado_Negocio.P_Empleado_ID = Empleado_ID;
                DataTable Dt_Datos_Empleado = Empleado_Negocio.Consulta_Empleados_General();
                Hdf_Empleado_Recibe.Value = ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString().Trim() : null);
                Txt_Empleado_Recibe.Text += ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString().Trim() : null);
                Txt_Empleado_Recibe.Text += " - " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString().Trim() : null);
                Txt_Empleado_Recibe.Text = Txt_Empleado_Recibe.Text.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString().Trim() : null);
                Txt_Empleado_Recibe.Text = Txt_Empleado_Recibe.Text.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString().Trim() : null);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Registrar_Entrega_Registro
            ///DESCRIPCIÓN: Registrar Entrega de Registro.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Registrar_Entrega_Registro() {
                Cls_Ope_Tal_Salidas_Vehiculos_Negocio Sal_Negocio = new Cls_Ope_Tal_Salidas_Vehiculos_Negocio();
                Sal_Negocio.P_Fecha_Salida = Convert.ToDateTime(Txt_Fecha_Entrega.Text);
                Sal_Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                Sal_Negocio.P_Tipo_Salida = "SOLICITUD";
                Sal_Negocio.P_Tipo_Solicitud = Cmb_Tipo_Servicio.SelectedItem.Value;
                Sal_Negocio.P_No_Entrada = Convert.ToInt32(Hdf_No_Entrada.Value);
                Sal_Negocio.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                Sal_Negocio.P_Vehiculo_ID = Hdf_Vehiculo_ID.Value.Trim();
                Sal_Negocio.P_Tipo_Bien = Hdf_Tipo_Bien.Value.Trim();
                Sal_Negocio.P_Empleado_Entrego_ID = Cls_Sessiones.Empleado_ID;
                Sal_Negocio.P_Comentarios = Txt_Comentarios_Recepción.Text.Trim();
                if (Txt_Kilometraje_Entrega.Text.Trim().Length > 0) Sal_Negocio.P_Kilometraje = Convert.ToDouble(Txt_Kilometraje_Entrega.Text);
                Sal_Negocio.P_Empleado_Recibio_ID = Hdf_Empleado_Recibe.Value.Trim();
                Sal_Negocio.P_Dt_Detalles = Obtener_Datos_Detalles();
                Sal_Negocio.Alta_Salida_Vehiculo();
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Obtener_Datos_Detalles
            ///DESCRIPCIÓN: Regresa un DataTable Con los Datos de los Detalles.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private DataTable Obtener_Datos_Detalles() {
                DataTable Dt_Detalles = new DataTable();
                Dt_Detalles.Columns.Add("PARTE_ID", Type.GetType("System.String"));
                Dt_Detalles.Columns.Add("SUBPARTE_ID", Type.GetType("System.String"));
                Dt_Detalles.Columns.Add("VALOR", Type.GetType("System.String"));
                foreach (GridViewRow Fila_Actual in Grid_Listado_Detalles.Rows) {
                    DataRow Nueva_Fila = Dt_Detalles.NewRow();
                    Nueva_Fila["PARTE_ID"] = HttpUtility.HtmlDecode(Fila_Actual.Cells[0].Text).Trim();
                    Nueva_Fila["SUBPARTE_ID"] = HttpUtility.HtmlDecode(Fila_Actual.Cells[1].Text).Trim();
                    if (Fila_Actual.FindControl("RBtn_SI") != null && Fila_Actual.FindControl("RBtn_NO") != null) {
                        RadioButton RBtn_SI = (RadioButton)Fila_Actual.FindControl("RBtn_SI");
                        RadioButton RBtn_NO = (RadioButton)Fila_Actual.FindControl("RBtn_NO");
                        if (RBtn_SI.Checked) Nueva_Fila["VALOR"] = "SI";
                        else if (RBtn_NO.Checked) Nueva_Fila["VALOR"] = "NO";
                    }
                    Dt_Detalles.Rows.Add(Nueva_Fila);
                }
                return Dt_Detalles;
            }

        #endregion

        #region Validaciones

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Autorizacion
            ///DESCRIPCIÓN: Valida la Solicitud de Servicio antes de ser Autorizada
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private Boolean Validar_Autorizacion() { 
                Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                String Mensaje_Error = "";
                Boolean Validacion = true;
                if (Txt_Fecha_Entrega.Text.Trim().Length == 0) {
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar la Fecha de Entrega del Vehiculo.";
                    Mensaje_Error = Mensaje_Error + " <br />";
                    Validacion = false;
                }
                if (Txt_Kilometraje_Entrega.Text.Trim().Length > 0) {
                    if (!Validar_Valores_Decimales(Txt_Kilometraje_Entrega.Text)) {
                        Mensaje_Error = Mensaje_Error + "+ El Formato del Kilometraje no es Correcto [Correcto: '12345', '12353.0' ó '12254.33'].";
                        Mensaje_Error = Mensaje_Error + " <br />";
                        Validacion = false;
                    }
                }
                if (Hdf_Empleado_Recibe.Value.Trim().Length == 0) {
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar al Empleado que Recibirá el Vehículo.";
                    Mensaje_Error = Mensaje_Error + " <br />";
                    Validacion = false;
                }
                
                if (!Validacion) {
                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
                    Div_Contenedor_Msj_Error.Visible = true;
                } else {
                    Validacion = Validar_Detalles();
                }
                return Validacion;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Detalles
            ///DESCRIPCIÓN: Valida los Detalles Completos
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private Boolean Validar_Detalles() { 
                Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                String Mensaje_Error = "";
                Boolean Validacion = true;
                foreach (GridViewRow Fila_Actual in Grid_Listado_Detalles.Rows) { 
                    if (Fila_Actual.FindControl("RBtn_SI") != null && Fila_Actual.FindControl("RBtn_NO") != null) {
                        RadioButton RBtn_SI = (RadioButton)Fila_Actual.FindControl("RBtn_SI");
                        RadioButton RBtn_NO = (RadioButton)Fila_Actual.FindControl("RBtn_NO");
                        if (!(RBtn_SI.Checked || RBtn_NO.Checked)) {
                            Mensaje_Error = Mensaje_Error + "+ No se ha seleccionado [Si/No] dentro de la Parte " + HttpUtility.HtmlDecode(Fila_Actual.Cells[3].Text).Trim() + " en la SubParte " + HttpUtility.HtmlDecode(Fila_Actual.Cells[4].Text).Trim() + ".";
                            Mensaje_Error = Mensaje_Error + " <br />";
                            Validacion = false;
                            break;
                        }
                    }
                }
                if (!Validacion) {
                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
                    Div_Contenedor_Msj_Error.Visible = true;
                }
                return Validacion;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Valores_Decimales
            ///DESCRIPCIÓN: Valida los valores decimales para un Anexo.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private Boolean Validar_Valores_Decimales(String Valor) {
                Boolean Validacion = true;
                Regex Expresion_Regular = new Regex(@"^[0-9]{1,50}(\.[0-9]{0,2})?$");
                Validacion = Expresion_Regular.IsMatch(Valor);
                return Validacion;
            }

        #endregion

    #endregion
    
    #region Grids
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Solicitudes_PageIndexChanging
        ///DESCRIPCIÓN: Maneja la paginación del Listado
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Solicitudes_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            try {
                Grid_Listado_Solicitudes.SelectedIndex = (-1);
                Grid_Listado_Solicitudes.PageIndex = e.NewPageIndex;
                Llenar_Listado_Solicitudes();
            } catch(Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Solicitudes_SelectedIndexChanged
        ///DESCRIPCIÓN: Obtiene los datos de una Solicitud 
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Solicitudes_SelectedIndexChanged(object sender, EventArgs e) {
            try{
                if (Grid_Listado_Solicitudes.SelectedIndex > (-1)){
                    Limpiar_Formulario();
                    Hdf_No_Entrada.Value = HttpUtility.HtmlDecode(Grid_Listado_Solicitudes.SelectedRow.Cells[1].Text.Trim()).Trim();
                    Hdf_No_Servicio.Value = HttpUtility.HtmlDecode(Grid_Listado_Solicitudes.SelectedRow.Cells[2].Text.Trim()).Trim();
                    Hdf_No_Solicitud.Value = HttpUtility.HtmlDecode(Grid_Listado_Solicitudes.SelectedRow.Cells[3].Text.Trim()).Trim();
                    Mostrar_Registro();
                    Txt_Fecha_Entrega.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Today);
                    Configuracion_Formulario("OPERACION");
                    Grid_Listado_Solicitudes.SelectedIndex = -1;
                    System.Threading.Thread.Sleep(500);
                }
            } catch(Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Empleados_Resguardo_PageIndexChanging
        ///DESCRIPCIÓN: Maneja el evento de cambio de Página del GridView de Busqueda
        ///             de empleados.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 09/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Busqueda_Empleados_Resguardo_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            try {
                Grid_Busqueda_Empleados_Resguardo.PageIndex = e.NewPageIndex;
                Llenar_Grid_Busqueda_Empleados_Resguardo();
                MPE_Resguardante.Show();
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged
        ///DESCRIPCIÓN: Maneja el evento de cambio de Selección del GridView de Busqueda
        ///             de empleados.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 09/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged(object sender, EventArgs e) { 
            try {
                if (Grid_Busqueda_Empleados_Resguardo.SelectedIndex > (-1)) {
                    String Empleado_Seleccionado_ID = Grid_Busqueda_Empleados_Resguardo.SelectedRow.Cells[1].Text.Trim();
                    Mostrar_Informacion_Empleado_Recibe(Empleado_Seleccionado_ID);
                    Grid_Busqueda_Empleados_Resguardo.SelectedIndex = (-1);
                    MPE_Resguardante.Hide();
                }
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

    #endregion

    #region Eventos
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Controla las operaciones del Boton de Salir
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 05/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Hacer_Entrega_Click(object sender, ImageClickEventArgs e) {
            if (Validar_Autorizacion()) {
                Registrar_Entrega_Registro();
                Llenar_Listado_Solicitudes();
                Configuracion_Formulario("INICIAL");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "GACO", "alert('Operacion Exitosa: Entrega de Vehículo a Unidad Responsable');", true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Controla las operaciones del Boton de Salir
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 05/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e) {
            if (Div_Campos.Visible) {
                Limpiar_Formulario();
                Configuracion_Formulario("INICIAL");
            } else {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Empleado_Entrega_Click
        ///DESCRIPCIÓN: Lanza la Busqueda del Empleado
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 05/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Empleado_Entrega_Click(object sender, ImageClickEventArgs e) {
            Grid_Busqueda_Empleados_Resguardo.PageIndex = 0;
            Llenar_Grid_Busqueda_Empleados_Resguardo();
            MPE_Resguardante.Show();
        }
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Empleados_Click
        ///DESCRIPCIÓN: Ejecuta la Busqueda Avanzada para el Resguardante.
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 05/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************    
        protected void Btn_Busqueda_Empleados_Click(object sender, EventArgs e) {
            try {
                Grid_Busqueda_Empleados_Resguardo.PageIndex = 0;
                Llenar_Grid_Busqueda_Empleados_Resguardo();
                MPE_Resguardante.Show();
            }  catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Actualizar_Listado_Click
        ///DESCRIPCIÓN: Ejecuta el Proceso para Salir.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 05/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Actualizar_Listado_Click(object sender, ImageClickEventArgs e) {
            Llenar_Listado_Solicitudes();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Mostrar_Detalle_Recepcion_Click
        ///DESCRIPCIÓN: Muestra los detalles de la Recepción.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 05/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Mostrar_Detalle_Recepcion_Click(object sender, EventArgs e) {
            MPE_Listado_Detalles.Show();
        }

    #endregion

}