﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Ope_Tal_Bitacora_Servicios_Vehiculo.aspx.cs"
    Inherits="paginas_Servicios_Generales_Frm_Ope_Tal_Bitacora_Servicios_Vehiculo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">
    <link href="../../jquery-easyui/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../../jquery-easyui/themes/icon.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">

    <script src="../../easyui/ui.datepicker-es.js" type="text/javascript"></script>

    <script src="../../jquery-easyui/jquery.easyui.min.js" type="text/javascript"></script>

    <script src="../../javascript/Taller_Mecanico/Js_Ope_Tal_Bitacora_Servicios_Vehiculos.js"
        type="text/javascript"></script>

    <div id="Div_Area_Trabajo" style="background-color: #ffffff; width: 100%; height: 100%;">
        <center>
            <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                <tr align="center">
                    <td class="label_titulo" colspan="2">
                        Bitacora de Seguimiento de Solicitudes
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div id="Div_Contenedor_Msj_Error" style="width: 98%; display: none;">
                            <table style="width: 100%;">
                                <tr>
                                    <td colspan="2" align="left">
                                        <img id="IBtn_Imagen_Error" src="../imagenes/paginas/sias_warning.png" width="24px"
                                            height="24px" />
                                        <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%;">
                                    </td>
                                    <td style="width: 90%; text-align: left;" valign="top">
                                        <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr style="background-color: #C0C0C0; text-align: left;">
                    <td align="left" style="width: 50%;">
                        <img id="Btn_Imprimir_Solicitud_Servicio" alt="" title="Imprimir Solicitud de Servicio"
                            src="../imagenes/gridview/grid_print.png" width="24px" height="24px" style="cursor: pointer;" />
                        <img id="Btn_Imprimir_Bitacora_Seguimiento" alt="" title="Imprimir Bitacora de Servicio"
                            src="../imagenes/paginas/icono_rep_pdf.png" width="24px" height="24px" style="cursor: pointer;" />
                        <img id="Btn_Cancelar" alt="" title="Cancelar Solicitud Servicio" src="../imagenes/paginas/icono_eliminar.png"
                            width="24px" height="24px" style="cursor: pointer;" />
                        <img id="Btn_Dianostico" alt="" title="Diagnosticar/Rediagnosticar Servicio" src="../imagenes/paginas/sias_load.png"
                            width="24px" height="24px" style="cursor: pointer;" />
                        <img id="Btn_Salir" alt="" title="Salir" src="../imagenes/paginas/icono_salir.png"
                            width="24px" height="24px" style="cursor: pointer;" />
                    </td>
                    <td style="width: 50%;">
                        &nbsp;
                    </td>
                </tr>
            </table>
            <br />
            <div id="Div_Listado_Solicitudes" runat="server" style="width: 100%;">
                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td colspan="4">
                            <div id="Div_Filtros_Busqueda" runat="server" style="border-style: outset; width: 99.5%;">
                                <table width="100%" border="0" cellspacing="0" class="estilo_fuente">
                                    <tr>
                                        <td colspan="4">
                                            <asp:Label ID="Lbl_Leyenda_Busqueda" runat="server" Width="100%" Text="FILTROS PARA EL LISTADO"
                                                ForeColor="White" BackColor="Black" Font-Bold="true" Style="text-align: center;"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">
                                            <asp:Label ID="Lbl_Folio_Solicitud_Busqueda" runat="server" Text="Folio Solicitud"></asp:Label>
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:TextBox ID="Txt_Folio_Solicitud_Busqueda" runat="server" Width="96%" MaxLength="10"></asp:TextBox>
                                        </td>
                                        <td colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">
                                            <asp:Label ID="Lbl_Unidad_Responsable_Busqueda" runat="server" Text="U. Responsable"></asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:DropDownList ID="Cmb_Unidad_Responsable_Busqueda" runat="server" Width="99.2%">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">
                                            <asp:Label ID="Lbl_Numero_Inventario_Busqueda" runat="server" Text="No. Inventario"></asp:Label>
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:TextBox ID="Txt_Numero_Inventario_Busqueda" runat="server" Width="96%"></asp:TextBox>
                                        </td>
                                        <td style="width: 15%;">
                                            &nbsp;&nbsp;
                                            <asp:Label ID="Lbl_Numero_Economico_Busqueda" runat="server" Text="No. Económico"></asp:Label>
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:TextBox ID="Txt_Numero_Economico_Busqueda" runat="server" Width="96%" MaxLength="6"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="text-align: center;">
                                            <asp:Label ID="Lbl_Leyenda_Fecha_Recepcion" runat="server" Text="Rango en Fecha de Recepción"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">
                                            <asp:Label ID="Lbl_Fecha_Recepcion_Inicial" runat="server" Text="Fecha Inicial"></asp:Label>
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:TextBox ID="Txt_Fecha_Recepcion_Inicial" runat="server" Width="95%"></asp:TextBox>
                                        </td>
                                        <td style="width: 15%;">
                                            &nbsp;&nbsp;<asp:Label ID="Lbl_Fecha_Recepcion_Final" runat="server" Text="Fecha Final"></asp:Label>
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:TextBox ID="Txt_Fecha_Recepcion_Final" runat="server" Width="95%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">
                                            <asp:Label ID="Lbl_Filtrado_Estatus" runat="server" Text="Estatus"></asp:Label>
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:DropDownList ID="Cmb_Filtrado_Estatus" runat="server" Width="98%" />
                                        </td>
                                        <td style="width: 15%;">
                                            &nbsp;&nbsp;Tipo Reparación&nbsp;
                                        </td>
                                        <td style="text-align: left; width: 35%;">
                                            <asp:DropDownList ID="Cmb_Reparacion_Busqueda" runat="server" Width="98%">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="right">
                                            <img id="Btn_Actualizar_Listado" alt="" title="Actualizar Listado" src="../imagenes/paginas/actualizar_detalle.png"
                                                width="24px" height="24px" style="cursor: pointer;" />&nbsp;&nbsp;
                                            <img id="Btn_Limpiar_Filtros_Listado" alt="" title="Limpiar Filtros Busqueda" src="../imagenes/paginas/icono_limpiar.png"
                                                width="24px" height="24px" style="cursor: pointer;" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="width: 100%;">
                            <div id="Div_Contenedor_Resultados" style="height: 350px;">
                                <div id="TAB_REVISTA_MECANICA" title="Revistas Mécanicas">
                                    <table id="Grid_Revistas_Mecanicas">
                                    </table>
                                </div>
                                <div id="TAB_SERVICIO_CORRECTIVO" title="Servicios Correctivos" style="width: 100%;">
                                    <table id="Grid_Servicios_Correctivos">
                                    </table>
                                </div>
                                <div id="TAB_SERVICIO_PREVENTIVO" title="Servicios Preventivos" style="width: 100%;">
                                    <table id="Grid_Servicios_Preventivos">
                                    </table>
                                </div>
                                <div id="TAB_VERIFICACION" title="Verificaciones" style="width: 100%;">
                                    <table id="Grid_Verificaciones">
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <br />
            </div>
        </center>
    </div>
</asp:Content>
