﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Dependencias.Datos;
using JAPAMI.Ayudante_JQuery;
using JAPAMI.Parametros.EasyUI;
using JAPAMI.Taller_Mecanico.Catalogo_Tarjetas_Gasolina.Negocio;
using Newtonsoft.Json;
using System.Web.Services;
using JAPAMI.Taller_Mecanico.Ope_Facturas_Tarjetas_Gasolina.Negocio;
using JAPAMI.Sessiones;

public partial class paginas_Servicios_Generales_Frm_Controlador_Mov_Tarj_Gas : System.Web.UI.Page
{
    int pagina, renglon;
    string nombres_datos, valores;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.controlador(this.Response, this.Request);
    }
    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN: controlador
    //DESCRIPCIÓN: distribuye peticiones
    //PARÁMETROS:
    //CREO: Sergio Pacheco Alcocer
    //FECHA_CREO: 05/04/2012
    //MODIFICÓ:
    //FECHA_MODIFICÓ:
    //CAUSA_MODIFICACIÓN:
    //*******************************************************************************************************
    public void controlador(HttpResponse response, HttpRequest request)
    {
        String accion = "";
        String strdatos = "";

        accion = HttpContext.Current.Request["Accion"] == null ? string.Empty : HttpContext.Current.Request["Accion"].ToString().Trim();//Para recuperar Query String Accion

        switch (accion)
        {
            case "Cargar_Combo_Dependencia":
                strdatos = Consulta_Combo_Dependencia();
                break;
            case "Consultar_Movimientos":
                strdatos = Consulta_Movimientos();
                break;  
        }

        response.Clear();
        response.ContentType = "application/json";
        response.Flush();
        response.Write(strdatos);
        response.End();
    }
    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN: Consulta_Combo_Dependencia()
    //DESCRIPCIÓN: Inicializa las propiedades de capa de Negocio 
    //             y Llama Metodo para consultar lista de Dependencias
    //PARÁMETROS: 
    //para Relizar la consulta en la tabla OPE_TAL_ORDENES_COMPRA
    //CREO: Jesus Toledo Rdz
    //FECHA_CREO: 20/Jun/2013
    //MODIFICÓ:
    //FECHA_MODIFICÓ:
    //CAUSA_MODIFICACIÓN:
    //*******************************************************************************************************
    private string Consulta_Combo_Dependencia()
    {
        String strdatos = "{[]}";
        String Catalogo = String.Empty;
        DataTable Dt_Resultado = new DataTable();
        Cls_Cat_Dependencias_Negocio Dependencia_Negocio = new Cls_Cat_Dependencias_Negocio();
        try
        {
            pagina = ParamsofEasyUI.page;
            renglon = ParamsofEasyUI.rows;
            Catalogo = HttpContext.Current.Request["Catalogo"] == null ? string.Empty : HttpContext.Current.Request["Catalogo"].ToString().Trim();//Para recuperar Query String Nombre Catalogo
            Dt_Resultado = Cls_Cat_Dependencias_Datos.Consulta_Dependencias(Dependencia_Negocio);
            //strdatos = Ayudante_JQuery.dataTableToJSON(Dt_Resultado);
            strdatos = Ayudante_JQuery.dataTableToJSONsintotalrenglones(Dt_Resultado);
            return strdatos;
        }
        catch (Exception Ex) { return Ex.ToString(); };
    }
    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN: Consulta_Movimientos()
    //DESCRIPCIÓN: Inicializa las propiedades de capa de Negocio 
    //             y Llama Metodo para consultar lista de Movimientos
    //PARÁMETROS: 
    //para Relizar la consulta en la tabla OPE_TAL_MOV_TARJ_GAS
    //CREO: Jesus Toledo Rdz
    //FECHA_CREO: 20/Jun/2013
    //MODIFICÓ:
    //FECHA_MODIFICÓ:
    //CAUSA_MODIFICACIÓN:
    //*******************************************************************************************************
    private string Consulta_Movimientos()
    {
        String Str_Datos = "{[]}";
        String Filtro_Unidad_Responsable = String.Empty;
        String Filtro_Fecha_Inicial = String.Empty;
        String Filtro_Fecha_Final = String.Empty;
        String Filtro_Tarjeta = String.Empty;
        String Estatus_Bien = String.Empty;
        String Filtro_Economico = String.Empty;
        Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Obj_Negocio = new Cls_Cat_Tal_Tarjetas_Gasolina_Negocio();
        try
        {
            ////Se recuperan los Parametros del QueryString
            Filtro_Unidad_Responsable = HttpContext.Current.Request["Unidad_Responsable"] == null ? string.Empty : HttpContext.Current.Request["Unidad_Responsable"].ToString().Trim();
            Filtro_Tarjeta = HttpContext.Current.Request["No_Tarjeta"] == null ? string.Empty : HttpContext.Current.Request["No_Tarjeta"].ToString().Trim();
            Filtro_Fecha_Inicial = HttpContext.Current.Request["Fecha_Inicio"] == null ? string.Empty : HttpContext.Current.Request["Fecha_Inicio"].ToString().Trim();
            Filtro_Fecha_Final = HttpContext.Current.Request["Fecha_Final"] == null ? string.Empty : HttpContext.Current.Request["Fecha_Final"].ToString().Trim();
            Filtro_Economico = HttpContext.Current.Request["No_Economico"] == null ? string.Empty : HttpContext.Current.Request["No_Economico"].ToString().Trim();
            //Se validan parametros y se asignan a la capa de negocio
            
            if (!String.IsNullOrEmpty(Filtro_Unidad_Responsable))
            {
                if (Filtro_Unidad_Responsable != "null" && Filtro_Unidad_Responsable != "-1")
                    Obj_Negocio.P_Dependencia_ID = Filtro_Unidad_Responsable;
            }
            if (!String.IsNullOrEmpty(Filtro_Fecha_Inicial)) { if (Filtro_Fecha_Inicial != "null") Obj_Negocio.P_Fecha_Inicial = Filtro_Fecha_Inicial; }//string.Format("{0:MM/dd/yyyy}",Filtro_Fecha_Inicial); }
            if (!String.IsNullOrEmpty(Filtro_Fecha_Final)) { if (Filtro_Fecha_Final != "null") Obj_Negocio.P_Fecha_Final = Filtro_Fecha_Final; }//string.Format("{0:MM/dd/yyyy}",Filtro_Fecha_Final); }
            if (!String.IsNullOrEmpty(Filtro_Economico)) { if (Filtro_Economico != "null") Obj_Negocio.P_Numero_Economico = Filtro_Economico; }
            if (!String.IsNullOrEmpty(Filtro_Tarjeta)) { if (Filtro_Tarjeta != "null") Obj_Negocio.P_Numero_Tarjeta = Filtro_Tarjeta; }

            pagina = ParamsofEasyUI.page;
            renglon = ParamsofEasyUI.rows;
            DataTable Dt_Resultado = Obj_Negocio.Consultar_Movimientos_Detalles();
            Str_Datos = Ayudante_JQuery.dataTableToJSONRengloes(Dt_Resultado);
        }
        catch (Exception ex)
        {
            Str_Datos = "{[]}";
        }
        return Str_Datos;
    }
    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN: Guardar_Facturas()
    //DESCRIPCIÓN: Inicializa las propiedades de capa de Negocio 
    //             y Llama Metodo para insertar valores en la Base de datos
    //PARÁMETROS: Folio de la Requisiscion, P_Fecha_Envio, P_Fecha_Recepcion, P_Fecha_Salida, P_Entrega
    //CREO: Jesus Toledo Rdz
    //FECHA_CREO: 10/Jul/2013
    //MODIFICÓ:
    //FECHA_MODIFICÓ:
    //CAUSA_MODIFICACIÓN:
    //*******************************************************************************************************
    [WebMethod]
    public static string Guardar_Facturas(string P_Tabla_Facturas, string P_Movimientos)
    {
        Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Facturas_Negocio = new Cls_Cat_Tal_Tarjetas_Gasolina_Negocio();
        JsonSerializerSettings configuracionJson = new JsonSerializerSettings();
        configuracionJson.NullValueHandling = NullValueHandling.Ignore;
        List<Cls_Ope_Tal_Facturas_Movimientos_Tarjetas_Negocio> obj_Bean_Facturas = new List<Cls_Ope_Tal_Facturas_Movimientos_Tarjetas_Negocio>();
        try
        {
            obj_Bean_Facturas = JsonConvert.DeserializeObject<List<Cls_Ope_Tal_Facturas_Movimientos_Tarjetas_Negocio>>(P_Tabla_Facturas);
            if (obj_Bean_Facturas != null)
            {
                Facturas_Negocio.P_obj_Bean_Facturas = obj_Bean_Facturas;
                Facturas_Negocio.P_Movimientos = P_Movimientos;
                Facturas_Negocio.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                Facturas_Negocio.Alta_Facturas_Movimientos_Tarjetas();
            }
        }
        catch (Exception Ex) { return "0"; };
        return "1";
    }
}