﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Control_Patrimonial_Operacion_Vehiculos.Negocio;
using System.Text.RegularExpressions;
using JAPAMI.Reportes;
using JAPAMI.Taller_Mecanico.Catalogo_Partes_Vehiculos.Negocio;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using JAPAMI.Empleados.Negocios;
using JAPAMI.Taller_Mecanico.Operacion_Unidades_Responsables_Rol.Negocio;
using JAPAMI.Taller_Mecanico.Reporte_Solicitud_Servicio.Negocio;
using JAPAMI.Control_Patrimonial_Operacion_Bienes_Muebles.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Unidades_Responsables_Rol.Datos;

public partial class paginas_Taller_Mecanico_Frm_Ope_Tal_Generacion_Solicitud_Servicio : System.Web.UI.Page {

    #region Page_Load
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN: Carga la Pagina Inicial
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Abril/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e) {
            Div_Contenedor_Msj_Error.Visible = false;
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack) {
                Cmb_Filtrado_Estatus.SelectedIndex = Cmb_Filtrado_Estatus.Items.IndexOf(Cmb_Filtrado_Estatus.Items.FindByValue("PENDIENTE','AUTORIZADA"));
                Llenar_Combo_Gpos_Unidades_Responsables();
                Cmb_Entrada_Gpo_UR.SelectedIndex = Cmb_Entrada_Gpo_UR.Items.IndexOf(Cmb_Entrada_Gpo_UR.Items.FindByValue(Consultar_Grupo_Dependencia(Cls_Sessiones.Dependencia_ID_Empleado)));
                Llenar_Combo_Unidades_Responsables_Entrada();
                Llenar_Combo_Unidades_Responsables();
                Llenar_Combos_Independientes();
                Grid_Listado_Solicitudes.PageIndex = 0;
                Configuracion_Formulario("INICIAL");
                Boolean Accesos = Validar_Acceso_Unidades_Responsables();
                Cmb_Entrada_UR.SelectedIndex = Cmb_Entrada_UR.Items.IndexOf(Cmb_Entrada_UR.Items.FindByValue(Cls_Sessiones.Dependencia_ID_Empleado));
                Cmb_Entrada_UR.Enabled = Accesos;
                Cmb_Entrada_Gpo_UR.Enabled = Accesos;
                Cmb_Busqueda_Vehiculo_Dependencias.Enabled = Accesos;
                Llenar_Listado_Solicitudes();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Validar_Acceso_Unidades_Responsables
        ///DESCRIPCIÓN: Se valida el Acceso a Otras UR.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected Boolean Validar_Acceso_Unidades_Responsables() {
            Boolean Accesos_Totales = false;
            Cls_Ope_Tal_Unidades_Responsables_Rol_Negocio Cls_Negocio = new Cls_Ope_Tal_Unidades_Responsables_Rol_Negocio();
            Cls_Negocio.P_Estatus = "ACTIVO";
            DataTable Dt_UR = Cls_Negocio.Consultar_Listado_UR();
            DataRow[] Filas = Dt_UR.Select("DEPENDENCIA_ID = '" + Cls_Sessiones.Dependencia_ID_Empleado.Trim() + "'");
            if (Filas.Length > 0) { Accesos_Totales = true; }
            return Accesos_Totales;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Grupo_Dependencia
        ///DESCRIPCIÓN: Consulta el Grupo al que pertenece una dependencia
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected String Consultar_Grupo_Dependencia(String Dependencia_ID) {
            String Grupo_ID = String.Empty;
            Cls_Ope_Tal_Consultas_Generales_Negocio Cls_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            Cls_Negocio.P_Dependencia_ID = Dependencia_ID;
            DataTable Dt_Grupos_Dependencias = Cls_Negocio.Consultar_Grupos_Unidades_Responsables();
            if (Dt_Grupos_Dependencias.Rows.Count > 0) Grupo_ID = Dt_Grupos_Dependencias.Rows[0]["GRUPO_DEPENDENCIA_ID"].ToString().Trim();
            return Grupo_ID;
        }

    #endregion

    #region Metodos

        #region Llenado de Campos [Combos, Listados, Vehiculos]

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cargar_Combo_Tipo_Servicio
            ///DESCRIPCIÓN: Carga los tipos de Servicio Dependiendo del Tipo de Bien
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 30/Abril/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Cargar_Combo_Tipo_Servicio(DropDownList Combo_Tipo_Bien, ref DropDownList Combo_Tipo_Servicio) {
                if (Combo_Tipo_Bien.SelectedIndex > 0) { 
                    switch (Combo_Tipo_Bien.SelectedValue.Trim()){
                        case "BIEN_MUEBLE":
                            Combo_Tipo_Servicio.Items.Clear();
                            Combo_Tipo_Servicio.Items.Insert(0, new ListItem("REPARACIÓN", "REPARACION"));
                            Combo_Tipo_Servicio.Items.Insert(0, new ListItem("GARANTIA", "GARANTIA"));
                            Combo_Tipo_Servicio.Items.Insert(0, new ListItem("< - - SELECCIONE - ->", ""));
                            break;
                        case "VEHICULO":
                            Combo_Tipo_Servicio.Items.Clear();
                            Combo_Tipo_Servicio.Items.Insert(0, new ListItem("REVISTA MECANICA", "REVISTA_MECANICA"));
                            Combo_Tipo_Servicio.Items.Insert(0, new ListItem("SERVICIO GENERAL", "SERVICIO_GENERAL"));
                            Combo_Tipo_Servicio.Items.Insert(0, new ListItem("VERIFICACIÓN", "VERIFICACION"));
                            Combo_Tipo_Servicio.Items.Insert(0, new ListItem("< - - SELECCIONE - ->", ""));
                            break;
                        case "OTRO":
                            Combo_Tipo_Servicio.Items.Clear();
                            Combo_Tipo_Servicio.Items.Insert(0, new ListItem("SERVICIO DE MANTENIMIENTO", "MANTENIMIENTO"));
                            Combo_Tipo_Servicio.Items.Insert(0, new ListItem("< - - SELECCIONE - ->", ""));
                            break;
                    }
                } else {
                    Combo_Tipo_Servicio.Items.Clear();
                    Combo_Tipo_Servicio.Items.Insert(0, new ListItem("< - - SELECCIONE - ->", ""));
                }    
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Gpos_Unidades_Responsables
            ///DESCRIPCIÓN: Se llena el Combo de los Gpos. Unidades Responsables.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 30/Abril/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Combo_Gpos_Unidades_Responsables() {
                Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                Negocio.P_Estatus = "ACTIVO";
                DataTable Dt_Gpos_Dependencias = Negocio.Consultar_Grupos_Unidades_Responsables();
                Cmb_Grupo_UR.DataSource = Dt_Gpos_Dependencias;
                Cmb_Grupo_UR.DataTextField = "CLAVE_NOMBRE";
                Cmb_Grupo_UR.DataValueField = "GRUPO_DEPENDENCIA_ID";
                Cmb_Grupo_UR.DataBind();
                Cmb_Grupo_UR.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
                Cmb_Entrada_Gpo_UR.DataSource = Dt_Gpos_Dependencias;
                Cmb_Entrada_Gpo_UR.DataTextField = "CLAVE_NOMBRE";
                Cmb_Entrada_Gpo_UR.DataValueField = "GRUPO_DEPENDENCIA_ID";
                Cmb_Entrada_Gpo_UR.DataBind();
                Cmb_Entrada_Gpo_UR.Items.Insert(0, new ListItem("< - - TODOS - - >", ""));
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Proyectos_Dependecia
            ///DESCRIPCIÓN: Se llena el Combo de los de proyectos por dependencia
            ///PROPIEDADES:     
            ///CREO: Jesus Toledo Rdz
            ///FECHA_CREO: 22/Febrero/2013
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Combo_Proyectos_Dependecia()
            {
                Cmb_Programa_Proyecto.DataSource = null;
                Cmb_Programa_Proyecto.DataBind();
                Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedValue.Trim();
                DataTable Dt_Proyectos_Programas = Negocio.Consultar_Proyectos_Programas();
                Cmb_Programa_Proyecto.DataSource = Dt_Proyectos_Programas;
                Cmb_Programa_Proyecto.DataTextField = "CLAVE_NOMBRE";
                Cmb_Programa_Proyecto.DataValueField = "PROGRAMA_ID";
                Cmb_Programa_Proyecto.DataBind();
                Cmb_Programa_Proyecto.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Unidades_Responsables
            ///DESCRIPCIÓN: Se llena el Combo de las Unidades Responsables.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 30/Abril/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Combo_Unidades_Responsables() {
                Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                Negocio.P_Estatus = "ACTIVO";
                DataTable Dt_Dependencias = Negocio.Consultar_Unidades_Responsables();
                Cmb_Unidad_Responsable.DataSource = Dt_Dependencias;
                Cmb_Unidad_Responsable.DataTextField = "CLAVE_NOMBRE";
                Cmb_Unidad_Responsable.DataValueField = "DEPENDENCIA_ID";
                Cmb_Unidad_Responsable.DataBind();
                Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
                Cmb_Busqueda_Vehiculo_Dependencias.DataSource = Dt_Dependencias;
                Cmb_Busqueda_Vehiculo_Dependencias.DataTextField = "CLAVE_NOMBRE";
                Cmb_Busqueda_Vehiculo_Dependencias.DataValueField = "DEPENDENCIA_ID";
                Cmb_Busqueda_Vehiculo_Dependencias.DataBind();
                Cmb_Busqueda_Vehiculo_Dependencias.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
                Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.DataSource = Dt_Dependencias;
                Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.DataTextField = "CLAVE_NOMBRE";
                Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.DataValueField = "DEPENDENCIA_ID";
                Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.DataBind();
                Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Unidades_Responsables
            ///DESCRIPCIÓN: Se llena el Combo de las Unidades Responsables.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 30/Abril/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Combo_Unidades_Responsables_Entrada() {
                Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                Cls_Ope_Tal_Unidades_Responsables_Rol_Negocio UR_Negocio = new Cls_Ope_Tal_Unidades_Responsables_Rol_Negocio();
                DataTable Dt_Dependencias = new DataTable();
                Negocio.P_Estatus = "ACTIVO";
                if (Cmb_Entrada_Gpo_UR.SelectedIndex > 0) Negocio.P_Grupo_Dependencia_ID = Cmb_Entrada_Gpo_UR.SelectedValue.Trim();
                else Negocio.P_Grupo_Dependencia_ID = "-";
                Dt_Dependencias = Negocio.Consultar_Unidades_Responsables();
                Cmb_Entrada_UR.DataSource = Dt_Dependencias;
                Cmb_Entrada_UR.DataTextField = "CLAVE_NOMBRE";
                Cmb_Entrada_UR.DataValueField = "DEPENDENCIA_ID";
                Cmb_Entrada_UR.DataBind();
                Cmb_Entrada_UR.Items.Insert(0, new ListItem("< - - TODAS - - >", ""));
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Solicitudes
            ///DESCRIPCIÓN: Se llena el Listado de las Solicitudes.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Listado_Solicitudes() {
                Cls_Ope_Tal_Solicitud_Servicio_Negocio Negocio = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                //Negocio.P_Dependencia_ID = Cmb_Entrada_UR.SelectedItem.Value;
                Negocio.P_Dependencia_ID = Cls_Ope_Tal_Unidades_Responsables_Rol_Datos.Consultar_Accesos_UR(Cls_Sessiones.No_Empleado);
                Negocio.P_Estatus = "PENDIENTE','RECHAZADA','CANCELADA','AUTORIZADA";
                if (Cmb_Filtrado_Estatus.SelectedIndex > 0) Negocio.P_Estatus = Cmb_Filtrado_Estatus.SelectedItem.Value;
                Negocio.P_Tipo_Bien = "TODOS";
                if (Cmb_Entrada_Tipo_Servicio.SelectedIndex > 0) Negocio.P_Tipo_Servicio = Cmb_Entrada_Tipo_Servicio.SelectedItem.Value;
                Negocio.P_Procedencia = "SOLICITUD";
                DataTable Dt_Resultados = Negocio.Consultar_Listado_Solicitudes_Servicio();
                Grid_Listado_Solicitudes.Columns[1].Visible = true;
                Grid_Listado_Solicitudes.DataSource = Dt_Resultados;
                Grid_Listado_Solicitudes.DataBind();
                Grid_Listado_Solicitudes.Columns[1].Visible = false;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Vehiculo
            ///DESCRIPCIÓN: Se cargan los Datos del Vehiculo Seleccionado.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Cargar_Datos_Vehiculo(String Vehiculo, String Tipo_Busqueda, String Tipo_Bien)
            {
                double Disponible_Partida = 0;
                DataTable Dt_Vehiculo = new DataTable();
                Limpiar_Formulario_Vehiculo();
                Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                switch (Tipo_Busqueda) { 
                    case "NO_INVENTARIO":
                        Consulta_Negocio.P_No_Inventario = Vehiculo;
                        break;
                    case "IDENTIFICADOR":
                        Consulta_Negocio.P_Vehiculo_ID = Vehiculo;
                        Consulta_Negocio.P_Bien_Mueble_ID = Vehiculo;
                        break;
                    case "NO_ECONOMICO":
                        Consulta_Negocio.P_No_Economico = Vehiculo;
                        break;
                    case "NO_SERIE":
                        Consulta_Negocio.P_No_Serie = Vehiculo;
                        break;
                    default: break;
                }
                //if (!Validar_Acceso_Unidades_Responsables())
                //{
                //    if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Consulta_Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value; }
                //}
                Consulta_Negocio.P_Dependencia_ID = Cls_Ope_Tal_Unidades_Responsables_Rol_Datos.Consultar_Accesos_UR(Cls_Sessiones.No_Empleado);
                if (Tipo_Bien == "VEHICULO")
                    Dt_Vehiculo = Consulta_Negocio.Consultar_Vehiculos();
                else if (Tipo_Bien == "BIEN_MUEBLE")
                    Dt_Vehiculo = Consulta_Negocio.Consultar_Bien_Mueble();
                if (Dt_Vehiculo.Rows.Count > 0) {
                    if (Tipo_Bien == "VEHICULO")
                    {
                        Pnl_Vehiculo_Seleccionado.Visible = true;
                        Pnl_Bien_Mueble_Seleccionado.Visible = false;
                        Hdf_Vehiculo_ID.Value = Dt_Vehiculo.Rows[0]["VEHICULO_ID"].ToString().Trim();
                        Txt_No_Inventario.Text = Dt_Vehiculo.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                        Txt_No_Economico.Text = Dt_Vehiculo.Rows[0]["NO_ECONOMICO"].ToString().Trim();
                        Txt_Datos_Vehiculo.Text = Dt_Vehiculo.Rows[0]["VEHICULO_DESCRIPCION"].ToString().Trim();
                        Txt_Placas.Text = Dt_Vehiculo.Rows[0]["PLACAS"].ToString().Trim();
                        Txt_Anio.Text = Dt_Vehiculo.Rows[0]["ANIO"].ToString().Trim();
                    }
                    else if (Tipo_Bien == "BIEN_MUEBLE")
                    {
                        Pnl_Bien_Mueble_Seleccionado.Visible = true;
                        Pnl_Vehiculo_Seleccionado.Visible = false;
                        Hdf_Vehiculo_ID.Value = Dt_Vehiculo.Rows[0]["BIEN_MUEBLE_ID"].ToString().Trim();
                        Hdf_Bien_Mueble_ID.Value = Dt_Vehiculo.Rows[0]["BIEN_MUEBLE_ID"].ToString().Trim();
                        Txt_No_Inventario_BM .Text = Dt_Vehiculo.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                        Txt_Numero_Serie_Bien.Text = Dt_Vehiculo.Rows[0]["NUMERO_SERIE"].ToString().Trim();
                        Txt_Descripcion_Bien.Text = Dt_Vehiculo.Rows[0]["DESCRIPCION_BIEN_MUEBLE"].ToString().Trim();
                    }
                    
                    if (!String.IsNullOrEmpty(Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString()))
                    {
                        Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString()));
                        Cmb_Grupo_UR.SelectedIndex = Cmb_Grupo_UR.Items.IndexOf(Cmb_Grupo_UR.Items.FindByValue(Consultar_Grupo_Dependencia(Cmb_Unidad_Responsable.SelectedItem.Value.Trim())));                        
                        Llenar_Combo_Proyectos_Dependecia();
                        if (Cmb_Programa_Proyecto.Items.Count > 2)
                        {
                            Cmb_Programa_Proyecto.Enabled = true;
                            Cmb_Programa_Proyecto.SelectedIndex = 0;
                        }
                        else
                        {
                            Cmb_Programa_Proyecto.SelectedIndex = 1;
                        }
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Text = "ERROR [Puede ser que el Bien esté dado de Baja].";
                        Div_Contenedor_Msj_Error.Visible = true;
                        Cmb_Unidad_Responsable.SelectedIndex = 0;
                    }

                } else {
                    Lbl_Ecabezado_Mensaje.Text = "El Bien no se encontro.";
                    if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Lbl_Mensaje_Error.Text = "[Puede ser que no este asignado a la Unidad Responsable Seleccionada]."; }
                    else { Lbl_Mensaje_Error.Text = ""; }
                    Div_Contenedor_Msj_Error.Visible = true;
                }
                Disponible_Partida = Cargar_Presupuesto_Disponible();
                Txt_Presupuesto.Text = Disponible_Partida.ToString("#,###,#0.00");
                if(Disponible_Partida<=500)
                    Txt_Presupuesto.ForeColor = System.Drawing.Color.FromArgb(255, 46, 80);
                else
                    Txt_Presupuesto.ForeColor = System.Drawing.Color.FromArgb(0, 160, 60);
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Bien_Mueble
            ///DESCRIPCIÓN: Se cargan los Datos del Bien Mueble Seleccionado.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Cargar_Datos_Bien_Mueble(String Bien_Mueble, String Tipo_Busqueda)
            {
                Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                switch (Tipo_Busqueda)
                {
                    case "NO_INVENTARIO":
                        Consulta_Negocio.P_No_Inventario = Bien_Mueble;
                        break;
                    case "IDENTIFICADOR":
                        Consulta_Negocio.P_Bien_Mueble_ID = Bien_Mueble;
                        break;
                    default: break;
                }
                DataTable Dt_Bienes_Muebles = Consulta_Negocio.Consultar_Bien_Mueble();
                if (Dt_Bienes_Muebles.Rows.Count > 0)
                {
                    Hdf_Bien_Mueble_ID.Value = Dt_Bienes_Muebles.Rows[0]["BIEN_MUEBLE_ID"].ToString().Trim();
                    Txt_No_Inventario_BM.Text = Dt_Bienes_Muebles.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                    Txt_Numero_Serie_Bien.Text = Dt_Bienes_Muebles.Rows[0]["NUMERO_SERIE"].ToString().Trim();
                    Txt_Descripcion_Bien.Text = Dt_Bienes_Muebles.Rows[0]["DESCRIPCION_BIEN_MUEBLE"].ToString().Trim();
                    if (!String.IsNullOrEmpty(Dt_Bienes_Muebles.Rows[0]["DEPENDENCIA_ID"].ToString()))
                    {
                        Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Bienes_Muebles.Rows[0]["DEPENDENCIA_ID"].ToString()));
                        Cmb_Grupo_UR.SelectedIndex = Cmb_Grupo_UR.Items.IndexOf(Cmb_Grupo_UR.Items.FindByValue(Consultar_Grupo_Dependencia(Cmb_Unidad_Responsable.SelectedItem.Value.Trim())));
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Text = "ERROR [Puede ser que el Bien esté dado de Baja].";
                        Div_Contenedor_Msj_Error.Visible = true;
                        Cmb_Unidad_Responsable.SelectedIndex = 0;
                    }
                }
                else
                {
                    Lbl_Ecabezado_Mensaje.Text = "El Bien no se encontro.";
                    if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Lbl_Mensaje_Error.Text = "[Puede ser que no este asignado a la Unidad Responsable Seleccionada]."; }
                    else { Lbl_Mensaje_Error.Text = ""; }
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Cargar_Presupuesto_Disponible
            ///DESCRIPCIÓN          : Consulta el presupuesto Disponible.
            ///PARAMETROS           : 
            ///CREO                 : Jesus Toledo
            ///FECHA_CREO           : 15/Feb/2013 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private double Cargar_Presupuesto_Disponible()
            {
                Cls_Tal_Parametros_Negocio Parametros_Negocio = new Cls_Tal_Parametros_Negocio();
                double Disponible_Partida = 0;
                try
                {
                    if (!String.IsNullOrEmpty(Txt_No_Economico.Text.Trim()))
                    {
                        Parametros_Negocio = Parametros_Negocio.Consulta_Parametros_Presupuesto(Txt_No_Economico.Text.Trim(), Cmb_Tipo_Bien.SelectedValue);
                        Disponible_Partida = Cls_Ope_Psp_Manejo_Presupuesto.Consultar_Disponible_Partida(Parametros_Negocio.P_Fuente_Financiamiento_ID, Cmb_Programa_Proyecto.SelectedValue.Trim(), Cmb_Unidad_Responsable.SelectedValue.Trim(), Parametros_Negocio.P_Partida_ID, DateTime.Now.Year.ToString(), Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE);
                    }
                    else if(!String.IsNullOrEmpty(Txt_No_Inventario_BM.Text.Trim()) && Cmb_Tipo_Bien.SelectedValue == "BIEN_MUEBLE"){
                        Parametros_Negocio = Parametros_Negocio.Consulta_Parametros_Presupuesto(Txt_No_Inventario_BM.Text.Trim(), Cmb_Tipo_Bien.SelectedValue);
                        Disponible_Partida = Cls_Ope_Psp_Manejo_Presupuesto.Consultar_Disponible_Partida(Parametros_Negocio.P_Fuente_Financiamiento_ID, Cmb_Programa_Proyecto.SelectedValue.Trim(), Cmb_Unidad_Responsable.SelectedValue.Trim(), Parametros_Negocio.P_Partida_ID, DateTime.Now.Year.ToString(), Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE);
                    }
                }
                catch (Exception Ex)
                {
                    Lbl_Ecabezado_Mensaje.Text = "No se pudo consultar el Presupuesto.";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
                return Disponible_Partida;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Listado_Vehiculos
            ///DESCRIPCIÓN          : Llena el Grid de los Vehiculos.
            ///PARAMETROS           : 
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 15/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Llenar_Grid_Listado_Bienes_Vehiculos(Int32 Pagina) { 
               try {
                    Grid_Listado_Busqueda_Vehiculo.Columns[1].Visible = true;
                    Cls_Ope_Pat_Com_Vehiculos_Negocio Vehiculos = new Cls_Ope_Pat_Com_Vehiculos_Negocio();
                    Vehiculos.P_Tipo_DataTable = "VEHICULOS";
                    if (Session["FILTRO_BUSQUEDA"] != null) {
                        Vehiculos.P_Tipo_Filtro_Busqueda = Session["FILTRO_BUSQUEDA"].ToString();
                        Vehiculos.P_Estatus = "VIGENTE";
                        if (Session["FILTRO_BUSQUEDA"].ToString().Trim().Equals("DATOS_GENERALES")) {
                            if (Txt_Busqueda_Vehiculo_Numero_Inventario.Text.Trim().Length > 0) { Vehiculos.P_Numero_Inventario = Convert.ToInt64(Txt_Busqueda_Vehiculo_Numero_Inventario.Text.Trim()); }
                            if (Txt_Busqueda_Vehiculo_Numero_Economico.Text.Trim().Length > 0) { Vehiculos.P_Numero_Economico_ = Txt_Busqueda_Vehiculo_Numero_Economico.Text.Trim(); }
                            if (Txt_Busqueda_Vehiculo_Anio_Fabricacion.Text.Trim().Length > 0) { Vehiculos.P_Anio_Fabricacion = Convert.ToInt32(Txt_Busqueda_Vehiculo_Anio_Fabricacion.Text.Trim()); }
                            Vehiculos.P_Modelo_ID = Txt_Busqueda_Vehiculo_Modelo.Text.Trim();
                            if (Cmb_Busqueda_Vehiculo_Marca.SelectedIndex > 0) {
                                Vehiculos.P_Marca_ID = Cmb_Busqueda_Vehiculo_Marca.SelectedItem.Value.Trim();
                            }
                            if (Cmb_Busqueda_Vehiculo_Color.SelectedIndex > 0) {
                                Vehiculos.P_Color_ID = Cmb_Busqueda_Vehiculo_Color.SelectedItem.Value.Trim();
                            }
                            if (Cmb_Busqueda_Vehiculo_Dependencias.SelectedIndex > 0) {
                                Vehiculos.P_Dependencia_ID = Cmb_Busqueda_Vehiculo_Dependencias.SelectedItem.Value.Trim();
                            }
                        } else if (Session["FILTRO_BUSQUEDA"].ToString().Trim().Equals("RESGUARDANTES")) {
                            Vehiculos.P_RFC_Resguardante = Txt_Busqueda_Vehiculo_RFC_Resguardante.Text.Trim();
                            Vehiculos.P_No_Empleado = Txt_Busqueda_Vehiculo_No_Empleado.Text.Trim();
                            if (Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.SelectedIndex > 0) {
                                Vehiculos.P_Dependencia_ID = Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.SelectedItem.Value.Trim();
                            }
                            if (Cmb_Busqueda_Vehiculo_Nombre_Resguardante.SelectedIndex > 0) {
                                Vehiculos.P_Resguardante_ID = Cmb_Busqueda_Vehiculo_Nombre_Resguardante.SelectedItem.Value.Trim();
                            }
                        }
                    }
                    Grid_Listado_Busqueda_Vehiculo.DataSource = Vehiculos.Consultar_DataTable();
                    Grid_Listado_Busqueda_Vehiculo.PageIndex = Pagina;
                    Grid_Listado_Busqueda_Vehiculo.DataBind();
                    Grid_Listado_Busqueda_Vehiculo.Columns[1].Visible = false;
                    MPE_Busqueda_Vehiculo.Show();
                } catch (Exception Ex) {
                    Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Empleados
            ///DESCRIPCIÓN          : Llena el Combo con los empleados de una dependencia.
            ///PARAMETROS           : 
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 15/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Llenar_Combo_Empleados(String Dependencia_ID, ref DropDownList Combo_Empleados) {
                Combo_Empleados.Items.Clear();
                if (Dependencia_ID != null && Dependencia_ID.Trim().Length > 0) {
                    Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                    Negocio.P_Estatus = "ACTIVO";
                    Negocio.P_Dependencia_ID = Dependencia_ID.Trim();
                    DataTable Dt_Datos = Negocio.Consultar_Empleados();
                    Combo_Empleados.DataSource = Dt_Datos;
                    Combo_Empleados.DataValueField = "EMPLEADO_ID";
                    Combo_Empleados.DataTextField = "NOMBRE";
                    Combo_Empleados.DataBind();
                } 
                Combo_Empleados.Items.Insert(0, new ListItem("< TODOS >", ""));
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Cargar_Estilo_Estatus
            ///DESCRIPCIÓN          : Carga el Estilo para el Estatus
            ///PARAMETROS           : 
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 15/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Cargar_Estilo_Estatus() {
                String Estatus = Txt_Estatus.Text.Trim();
                switch (Estatus) {
                    case "PENDIENTE":
                        Txt_Estatus.BackColor = System.Drawing.Color.FromArgb(169,188,245);
                        break;
                    case "RECHAZADA":
                        Txt_Estatus.BackColor = System.Drawing.Color.FromArgb(255, 46, 56);
                        break;
                    case "CANCELADA":
                        Txt_Estatus.BackColor = System.Drawing.Color.FromArgb(184, 184, 184);
                        break;
                    case "AUTORIZADA":
                        Txt_Estatus.BackColor = System.Drawing.Color.FromArgb(156, 255, 186);
                        break;
                    default: 
                        Txt_Estatus.BackColor = System.Drawing.Color.White;
                        break;
                }
            }
    
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combos_Independientes
            ///DESCRIPCIÓN: Se llenan los Combos Generales Independientes.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 06/Diciembre/2010 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private void Llenar_Combos_Independientes()
            {
                try
                {
                    Cls_Ope_Pat_Com_Vehiculos_Negocio Combos = new Cls_Ope_Pat_Com_Vehiculos_Negocio();
                    //SE LLENA EL COMBO DE MARCAS DE LAS BUSQUEDAS
                    Combos.P_Tipo_DataTable = "MARCAS";
                    DataTable Marcas = Combos.Consultar_DataTable();
                    DataRow Fila_Marca = Marcas.NewRow();
                    Fila_Marca["MARCA_ID"] = "TODAS";
                    Fila_Marca["NOMBRE"] = HttpUtility.HtmlDecode("&lt;TODAS&gt;");
                    Marcas.Rows.InsertAt(Fila_Marca, 0);
                    Cmb_Busqueda_Vehiculo_Marca.DataSource = Marcas;
                    Cmb_Busqueda_Vehiculo_Marca.DataTextField = "NOMBRE";
                    Cmb_Busqueda_Vehiculo_Marca.DataValueField = "MARCA_ID";
                    Cmb_Busqueda_Vehiculo_Marca.DataBind();

                    //SE LLENA EL COMBO DE COLORES DE LAS BUSQUEDAS
                    Combos.P_Tipo_DataTable = "COLORES";
                    DataTable Tipos_Colores = Combos.Consultar_DataTable();
                    DataRow Fila_Color = Tipos_Colores.NewRow();
                    Fila_Color["COLOR_ID"] = "TODOS";
                    Fila_Color["DESCRIPCION"] = HttpUtility.HtmlDecode("&lt;TODOS&gt;");
                    Tipos_Colores.Rows.InsertAt(Fila_Color, 0);
                    Cmb_Busqueda_Vehiculo_Color.DataSource = Tipos_Colores;
                    Cmb_Busqueda_Vehiculo_Color.DataTextField = "DESCRIPCION";
                    Cmb_Busqueda_Vehiculo_Color.DataValueField = "COLOR_ID";
                    Cmb_Busqueda_Vehiculo_Color.DataBind();

                }
                catch (Exception Ex)
                {
                    Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }

        #endregion

        #region Generales [Configuracion, Limpiar]
                
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
            ///DESCRIPCIÓN: Limpia los campos del Formulario.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Limpiar_Formulario() {
                Hdf_No_Solicitud.Value = "";
                Hdf_Folio_Solicitud.Value = "";
                Txt_Fecha_Elaboracion.Text = "";
                Txt_Estatus.Text = "";
                Cmb_Tipo_Servicio.SelectedIndex = 0;
                Cmb_Unidad_Responsable.SelectedIndex = 0;
                Cmb_Programa_Proyecto.Enabled = false;
                Txt_Descripcion_Servicio.Text = "";
                Txt_Correo_Electronico.Text = "";
                Txt_Motivo_Rechazo.Text = "";
                Limpiar_Formulario_Vehiculo();
                Cargar_Estilo_Estatus();
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario_Vehiculo
            ///DESCRIPCIÓN: Limpia los campos del Formulario [Seccion de Vehiculos].
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Limpiar_Formulario_Vehiculo() {
                Hdf_Vehiculo_ID.Value = "";
                Txt_No_Inventario.Text = "";
                Txt_No_Economico.Text = "";
                Txt_Datos_Vehiculo.Text = "";
                Txt_Placas.Text = "";
                Txt_Anio.Text = "";
                Txt_Kilometraje.Text = "";
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
            ///DESCRIPCIÓN: Se Habilitan y/o inhabilitan los campos dependendiendo de la Condicion.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Configuracion_Formulario(String Operacion) {
                switch (Operacion) {
                    case "INICIAL":
                        Btn_Nuevo.AlternateText = "Nuevo";
                        Btn_Nuevo.ToolTip = "Nueva Solicitud";
                        Btn_Nuevo.Visible = true;
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                        Btn_Modificar.AlternateText = "Modificar";
                        Btn_Modificar.ToolTip = "Modificar Solicitud";
                        Btn_Modificar.Visible = true;
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                        Btn_Salir.AlternateText = "Salir";
                        Btn_Salir.ToolTip = "Salir";
                        Btn_Salir.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                        Btn_Cancelar.Visible = true;
                        Btn_Imprimir_Solicitud_Servicio.Visible = true;
                        Cmb_Tipo_Servicio.Enabled = false;
                        Cmb_Tipo_Bien.Enabled = false;
                        Txt_No_Economico.Enabled = false;
                        Txt_No_Inventario.Enabled = false;
                        Txt_Kilometraje.Enabled = false;
                        Btn_Busqueda_Directa.Visible = false;
                        Btn_Busqueda_Avanzada.Visible = false;
                        Txt_Descripcion_Servicio.Enabled = false;
                        Txt_Correo_Electronico.Enabled = false;
                        Div_Listado_Solicitudes.Visible = true;
                        Div_Campos.Visible = false;
                        Pnl_Motivo_Rechazo.Visible = false;
                        Cmb_Tipo_Bien.SelectedIndex = 2;
                        Txt_No_Economico.Enabled = true;
                        break;
                    case "OPERACION":
                        Btn_Nuevo.AlternateText = "Alta";
                        Btn_Nuevo.ToolTip = "Alta Solicitud";
                        Btn_Nuevo.Visible = true;
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                        Btn_Modificar.AlternateText = "Actualizar";
                        Btn_Modificar.ToolTip = "Actualizar Solicitud";
                        Btn_Modificar.Visible = true;
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                        Btn_Salir.AlternateText = "Cancelar";
                        Btn_Salir.ToolTip = "Cancelar Operación";
                        Btn_Salir.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        Btn_Cancelar.Visible = true;
                        Btn_Imprimir_Solicitud_Servicio.Visible = true;
                        Cmb_Tipo_Servicio.Enabled = true;
                        Cmb_Tipo_Bien.Enabled = true;
                        Txt_No_Economico.Enabled = true;
                        Txt_No_Inventario.Enabled = false;
                        Txt_Kilometraje.Enabled = true;
                        Btn_Busqueda_Directa.Visible = true;
                        Btn_Busqueda_Avanzada.Visible = true;
                        Txt_Descripcion_Servicio.Enabled = true;
                        Txt_Correo_Electronico.Enabled = true;
                        Div_Listado_Solicitudes.Visible = false;
                        Div_Campos.Visible = true;
                        break;
                    case "MOSTRAR INFORMACION":
                        Btn_Nuevo.AlternateText = "Nuevo";
                        Btn_Nuevo.ToolTip = "Nueva Solicitud";
                        Btn_Nuevo.Visible = true;
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                        Btn_Modificar.AlternateText = "Modificar";
                        Btn_Modificar.ToolTip = "Modificar Solicitud";
                        Btn_Modificar.Visible = true;
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                        Btn_Salir.AlternateText = "Salir";
                        Btn_Salir.ToolTip = "Salir";
                        Btn_Salir.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                        Btn_Cancelar.Visible = true;
                        Btn_Imprimir_Solicitud_Servicio.Visible = true;
                        Cmb_Tipo_Servicio.Enabled = false;
                        Cmb_Tipo_Bien.Enabled = false;
                        Txt_No_Economico.Enabled = false;
                        Txt_No_Inventario.Enabled = false;
                        Txt_Kilometraje.Enabled = false;
                        Btn_Busqueda_Directa.Visible = false;
                        Btn_Busqueda_Avanzada.Visible = false;
                        Txt_Descripcion_Servicio.Enabled = false;
                        Txt_Correo_Electronico.Enabled = false;
                        Div_Listado_Solicitudes.Visible = false;
                        Div_Campos.Visible = true;
                        Pnl_Motivo_Rechazo.Visible = false;        
                        break;
                }                
            }

        #endregion

        #region Clase de Negocio de Solicitudes [Alta, Modificacion y Consulta]

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Alta_Registro
            ///DESCRIPCIÓN: Se Crea y Carga un Objeto y se Manda al Alta.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 03/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Alta_Registro() {
                Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                Solicitud.P_Folio_Solicitud = "";
                Solicitud.P_Fecha_Elaboracion = DateTime.Today;
                Solicitud.P_Tipo_Bien = Cmb_Tipo_Bien.SelectedValue;
                Solicitud.P_Tipo_Servicio = Cmb_Tipo_Servicio.SelectedItem.Value;
                Solicitud.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value;
                Solicitud.P_Programa_Proyecto_ID = Cmb_Programa_Proyecto.SelectedItem.Value;
                Solicitud.P_Bien_ID = Hdf_Vehiculo_ID.Value;
                Solicitud.P_Descripcion_Servicio = Txt_Descripcion_Servicio.Text.Trim();
                Solicitud.P_Correo_Electronico = Txt_Correo_Electronico.Text.Trim();
                if (Txt_Kilometraje.Text.Length > 0) { Solicitud.P_Kilometraje = Convert.ToDouble(Txt_Kilometraje.Text.Trim()); }
                Solicitud.P_Estatus = "PENDIENTE";
                Solicitud.P_Procedencia = "SOLICITUD";
                Solicitud.P_Empleado_Solicito_ID = Cls_Sessiones.Empleado_ID;
                Solicitud.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                Solicitud.Alta_Solicitud_Servicio();
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Modifica_Registro
            ///DESCRIPCIÓN: Modifica el Registro.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Modifica_Registro() {
                Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                Solicitud.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                Solicitud.P_Folio_Solicitud = "";
                Solicitud.P_Fecha_Elaboracion = DateTime.Today;
                Solicitud.P_Tipo_Bien = "VEHICULO";
                Solicitud.P_Tipo_Servicio = Cmb_Tipo_Servicio.SelectedItem.Value;
                Solicitud.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value;
                Solicitud.P_Bien_ID = Hdf_Vehiculo_ID.Value;
                Solicitud.P_Descripcion_Servicio = Txt_Descripcion_Servicio.Text.Trim();
                Solicitud.P_Correo_Electronico = Txt_Correo_Electronico.Text.Trim();
                if (Txt_Kilometraje.Text.Length > 0) { Solicitud.P_Kilometraje = Convert.ToDouble(Txt_Kilometraje.Text.Trim()); }
                Solicitud.P_Estatus = "PENDIENTE";
                Solicitud.P_Procedencia = "SOLICITUD";
                Solicitud.P_Empleado_Solicito_ID = Cls_Sessiones.Empleado_ID;
                Solicitud.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                Solicitud.Modifica_Solicitud_Servicio();
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cancelar_Registro
            ///DESCRIPCIÓN: Cancela el Registro.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Cancelar_Registro() {
                Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                Solicitud.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                Solicitud = Solicitud.Consultar_Detalles_Solicitud_Servicio();
                Solicitud.P_Estatus = "CANCELADA";
                Solicitud.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                Solicitud.Modifica_Solicitud_Servicio();
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Mostrar_Registro
            ///DESCRIPCIÓN: Muestra el Registro en los campos.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Mostrar_Registro() {
                Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                Solicitud.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                Solicitud.P_Tipo_Bien = Hdf_Tipo_Bien.Value;
                Solicitud = Solicitud.Consultar_Detalles_Solicitud_Servicio();
                if (Solicitud.P_No_Solicitud > (-1)) {
                    Txt_Fecha_Elaboracion.Text = String.Format("{0:dd/MMM/yyyy}", Solicitud.P_Fecha_Elaboracion);
                    Cmb_Tipo_Servicio.SelectedIndex = Cmb_Tipo_Servicio.Items.IndexOf(Cmb_Tipo_Servicio.Items.FindByValue(Solicitud.P_Tipo_Servicio));
                    Cmb_Grupo_UR.SelectedIndex = Cmb_Grupo_UR.Items.IndexOf(Cmb_Grupo_UR.Items.FindByValue(Consultar_Grupo_Dependencia(Solicitud.P_Dependencia_ID)));
                    Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Solicitud.P_Dependencia_ID));
                    Cmb_Tipo_Bien.SelectedValue = Hdf_Tipo_Bien.Value.Replace(" ","_");
                    if (Hdf_Tipo_Bien.Value.Replace(" ", "_") == "VEHICULO")
                    {
                        Pnl_Vehiculo_Seleccionado.Visible = true;
                        Pnl_Bien_Mueble_Seleccionado.Visible = false;
                        Hdf_Vehiculo_ID.Value = Solicitud.P_Bien_ID;
                        Cargar_Datos_Vehiculo(Hdf_Vehiculo_ID.Value, "IDENTIFICADOR","VEHICULO");
                        if (Solicitud.P_Kilometraje > (-1.0)) Txt_Kilometraje.Text = String.Format("{0:############0.00}", Solicitud.P_Kilometraje);
                    }
                    if (Hdf_Tipo_Bien.Value.Replace(" ", "_") == "BIEN_MUEBLE")
                    {
                        Pnl_Bien_Mueble_Seleccionado.Visible = true;
                        Pnl_Vehiculo_Seleccionado.Visible = false;
                        Hdf_Bien_Mueble_ID.Value = Solicitud.P_Bien_ID;
                        Cargar_Datos_Vehiculo(Hdf_Bien_Mueble_ID.Value, "IDENTIFICADOR", "BIEN_MUEBLE");
                    }
                    
                    Txt_Descripcion_Servicio.Text = Solicitud.P_Descripcion_Servicio;
                    if (Solicitud.P_Estatus.Trim().Equals("RECHAZADA")) {
                        Pnl_Motivo_Rechazo.Visible = true;
                        Txt_Motivo_Rechazo.Text = Solicitud.P_Motivo_Rechazo;
                    }
                    Txt_Estatus.Text = Solicitud.P_Estatus;
                    Hdf_Estatus.Value = Solicitud.P_Estatus;
                    Txt_Correo_Electronico.Text = Solicitud.P_Correo_Electronico;
                    Hdf_Folio_Solicitud.Value = Solicitud.P_Folio_Solicitud;
                    Cargar_Estilo_Estatus();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Datos_Empleado_Logueado
            ///DESCRIPCIÓN: Hace una consulta con los datos del empleado que esta logueado.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 29/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Consultar_Datos_Empleado_Logueado() {
                Cls_Cat_Empleados_Negocios Empleado_Negocio = new Cls_Cat_Empleados_Negocios();
                Empleado_Negocio.P_Empleado_ID = Cls_Sessiones.Empleado_ID;
                DataTable Dt_Resultados = Empleado_Negocio.Consulta_Datos_Empleado();
                if (Dt_Resultados != null && Dt_Resultados.Rows.Count > 0) {
                    Txt_Correo_Electronico.Text = Dt_Resultados.Rows[0][Cat_Empleados.Campo_Correo_Electronico].ToString();
                }
            }

        #endregion

        #region Validaciones

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Solicitud
            ///DESCRIPCIÓN: Valida la Solicitud de Servicio antes de dar una Alta
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private Boolean Validar_Solicitud() { 
                Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                String Mensaje_Error = "";
                Boolean Validacion = true;
                if (Cmb_Unidad_Responsable.SelectedIndex == 0) {                    
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar la Unidad Responsable del Vehículo.";
                    Mensaje_Error = Mensaje_Error + " <br />";
                    Validacion = false;
                }
                if (Cmb_Programa_Proyecto.SelectedIndex == 0)
                {
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar el Programa proyecto del Vehículo.";
                    Mensaje_Error = Mensaje_Error + " <br />";
                    Validacion = false;
                }
                if (Txt_Kilometraje.Text.Trim().Length > 0) {
                    if (!Validar_Valores_Decimales(Txt_Kilometraje.Text)) {
                        Mensaje_Error = Mensaje_Error + "+ El Formato del Kilometraje no es Correcto [Correcto: '12345', '12353.0' ó '12254.33'].";
                        Mensaje_Error = Mensaje_Error + " <br />";
                        Validacion = false;
                    }
                }
                if (Txt_Correo_Electronico.Text.Trim().Length > 0) {
                    if (!Validar_Email(Txt_Correo_Electronico.Text.Trim())) {
                        Mensaje_Error = Mensaje_Error + "+ El Formato del Correo Electronico no es Correcto.";
                        Mensaje_Error = Mensaje_Error + " <br />";
                        Validacion = false;
                    }
                }
                if (Hdf_Vehiculo_ID.Value.Trim().Length == 0) {
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar el Vehículo.";
                    Mensaje_Error = Mensaje_Error + " <br />";
                    Validacion = false;
                }
                if (Cmb_Tipo_Servicio.SelectedIndex == 0) {
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar el Tipo de Servicio.";
                    Mensaje_Error = Mensaje_Error + " <br />";
                    Validacion = false;
                }
                if (Txt_Descripcion_Servicio.Text.Trim().Length == 0) {
                    Mensaje_Error = Mensaje_Error + "+ Introducir la Descripción del Servicio.";
                    Mensaje_Error = Mensaje_Error + " <br />";
                    Validacion = false;
                }
                //if (Txt_Presupuesto.Text.Trim() == "0.00")
                //{
                //    Mensaje_Error = Mensaje_Error + "+ No cuenta con presupuesto Disponible para el Servicio.";
                //    Mensaje_Error = Mensaje_Error + " <br />";
                //    Validacion = false;
                //}
                if (!Validacion) {
                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
                    Div_Contenedor_Msj_Error.Visible = true;
                }
                return Validacion;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Valores_Decimales
            ///DESCRIPCIÓN: Valida los valores decimales para un Anexo.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO: Marzo/2012 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private Boolean Validar_Valores_Decimales(String Valor) {
                Boolean Validacion = true;
                Regex Expresion_Regular = new Regex(@"^[0-9]{1,50}(\.[0-9]{0,2})?$");
                Validacion = Expresion_Regular.IsMatch(Valor);
                return Validacion;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Email
            ///DESCRIPCIÓN: Valida el E-Mail.
            ///PARAMETROS: Metodo que permite validar el correo ingresado por el usuario
            ///CREO: Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO: Marzo/2012 
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            public Boolean Validar_Email(String Correo) {
                Boolean Validacion = true;
                Regex Expresion_Regular = new Regex("^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$");
                Validacion = Expresion_Regular.IsMatch(Correo);
                return Validacion;
            }

        #endregion

        #region Reporte Orden de Trabajo

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Generar_Reporte_Solicitud_Servicio
            ///DESCRIPCIÓN: Se Generan las Tablas para el Reporte de Orden de Trabajo
            ///PARAMETROS:  
            ///CREO: Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO: 24/Mayo/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            private void Generar_Reporte_Solicitud_Servicio() {
                //Dataset esqueleto del Reporte
                Ds_Rpt_Tal_Solicitud_Servicio Ds_Reporte = new Ds_Rpt_Tal_Solicitud_Servicio();

                DataTable Dt_Generales = new DataTable("DT_GENERALES");
                Dt_Generales = Ds_Reporte.Tables["DT_GENERALES"].Clone();
                Obtener_Datos_Generales(ref Dt_Generales);

                DataTable Dt_SubPartes = new Cls_Ope_Tal_Consultas_Generales_Negocio().Consultar_Partes_SubPartes_Vehiculo();
                Dt_SubPartes.TableName = "DT_SUBPARTES";

                Cls_Cat_Tal_Partes_Vehiculos_Negocio Parte_Negocio = new Cls_Cat_Tal_Partes_Vehiculos_Negocio();
                Parte_Negocio.P_Tipo = "PARTE";
                Parte_Negocio.P_Estatus = "VIGENTE";
                DataTable Dt_Partes_Completo = Parte_Negocio.Consultar_Partes();
                Dt_Partes_Completo.Columns["NOMBRE"].ColumnName = "PARTE_NOMBRE";
                //Dt_Partes.TableName = "DT_PARTES";

                DataTable Dt_Partes = Separar_Partes_Columnas(Dt_Partes_Completo);
                Dt_Partes.TableName = "DT_PARTES";
                //Se crea El DataSet de los Datos
                DataSet Ds_Consulta = new DataSet();
                Ds_Consulta.Tables.Add(Dt_Generales.Copy());
                Ds_Consulta.Tables.Add(Dt_SubPartes.Copy());
                Ds_Consulta.Tables.Add(Dt_Partes.Copy());

                //Generar y lanzar el reporte
                String Ruta_Reporte_Crystal = "Rpt_Tal_Solicitud_Servicio.rpt";
                Generar_Reporte(Ds_Consulta, Ds_Reporte, Ruta_Reporte_Crystal);

            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
            ///DESCRIPCIÓN: caraga el data set fisico con el cual se genera el Reporte especificado
            ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
            ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
            ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
            ///CREO: Susana Trigueros Armenta.
            ///FECHA_CREO: 01/Mayo/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            private void Generar_Reporte(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte) {
                ReportDocument Reporte = new ReportDocument();
                String File_Path = Server.MapPath("../Rpt/Servicios_Generales/" + Nombre_Reporte);
                Reporte.Load(File_Path); 
                String Nombre_Reporte_Generar = "Rpt_Tal_Solicitud_Servicio_" + Cls_Sessiones.No_Empleado + "_" + Convert.ToString(DateTime.Now.ToString("yyyy'-'MM'-'dd'_t'HH'-'mm'-'ss")) + ".pdf";
                String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
                Ds_Reporte = Data_Set_Consulta_DB;
                Reporte.SetDataSource(Ds_Reporte);
                ExportOptions Export_Options = new ExportOptions();
                DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
                Disk_File_Destination_Options.DiskFileName = Server.MapPath(Ruta);
                Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
                Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
                Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Export_Options);
                Mostrar_Reporte(Nombre_Reporte_Generar, "PDF");
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Obtener_Datos_Generales
            ///DESCRIPCIÓN: Obtiene los datos generales del Reporte
            ///PARAMETROS:  
            ///CREO: Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO: 24/Mayo/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            private void Obtener_Datos_Generales(ref DataTable Dt_Generales) {
                DataRow Fila_Nueva = Dt_Generales.NewRow();
                Fila_Nueva["NO_SOLICITUD"] = Convert.ToInt32(Hdf_No_Solicitud.Value.Trim());
                Fila_Nueva["FECHA_ELABORACION"] = Convert.ToDateTime(Txt_Fecha_Elaboracion.Text);
                //Fila_Nueva["FECHA_RECIBO"] = "";
                Fila_Nueva["DEPENDENCIA"] = Cmb_Unidad_Responsable.SelectedItem.Text.Trim();
                Fila_Nueva["VEHICULO"] = Txt_Datos_Vehiculo.Text.Trim();
                Fila_Nueva["NO_INVENTARIO"] = Convert.ToInt32(Txt_No_Inventario.Text.Trim());
                Fila_Nueva["NO_ECONOMICO"] = Txt_No_Economico.Text.Trim();
                Fila_Nueva["PLACAS"] = Txt_Placas.Text.Trim();
                if (Txt_Anio.Text.Trim().Length > 0) {
                    Fila_Nueva["ANIO"] = Convert.ToInt32(Txt_Anio.Text.Trim());
                }
                //Fila_Nueva["KILOMETRAJE"] = "";
                //Fila_Nueva["PROVEEDOR_ASIGNADO"] = "";
                Fila_Nueva["DESCRIPCION_SERVICIO"] = Txt_Descripcion_Servicio.Text.Trim();
                Fila_Nueva["FOLIO_SOLICITUD"] = Hdf_Folio_Solicitud.Value.Trim();
                Dt_Generales.Rows.Add(Fila_Nueva);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Separar_Partes_Columnas
            ///DESCRIPCIÓN: Separa en 3 columnas los datos de las tablas
            ///PARAMETROS:  
            ///CREO: Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO: 24/Mayo/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            private DataTable Separar_Partes_Columnas(DataTable Dt_Datos) {
                DataTable Dt_Partes_Separadas = new DataTable();
                Dt_Partes_Separadas.Columns.Add("PARTE_ID_1", Type.GetType("System.String"));
                Dt_Partes_Separadas.Columns.Add("PARTE_NOMBRE_1", Type.GetType("System.String"));
                Dt_Partes_Separadas.Columns.Add("PARTE_ID_2", Type.GetType("System.String"));
                Dt_Partes_Separadas.Columns.Add("PARTE_NOMBRE_2", Type.GetType("System.String"));
                Dt_Partes_Separadas.Columns.Add("PARTE_ID_3", Type.GetType("System.String"));
                Dt_Partes_Separadas.Columns.Add("PARTE_NOMBRE_3", Type.GetType("System.String"));
                Int32 Fila_Actual = -1;
                Int32 Columna_Toca = 1;
                foreach (DataRow Fila_Leida in Dt_Datos.Rows) {
                    if (Columna_Toca == 1) {
                        DataRow Fila_Nueva = Dt_Partes_Separadas.NewRow();
                        Fila_Nueva["PARTE_ID_1"] = Fila_Leida["PARTE_ID"].ToString().Trim();
                        Fila_Nueva["PARTE_NOMBRE_1"] = Fila_Leida["PARTE_NOMBRE"].ToString().Trim();
                        Dt_Partes_Separadas.Rows.Add(Fila_Nueva);
                        Fila_Actual++;
                        Columna_Toca = 2;
                    } else if (Columna_Toca == 2) {
                        Dt_Partes_Separadas.Rows[Fila_Actual].SetField("PARTE_ID_2", Fila_Leida["PARTE_ID"].ToString().Trim());
                        Dt_Partes_Separadas.Rows[Fila_Actual].SetField("PARTE_NOMBRE_2", Fila_Leida["PARTE_NOMBRE"].ToString().Trim());
                        Columna_Toca = 3;
                    } else if (Columna_Toca == 3) {
                        Dt_Partes_Separadas.Rows[Fila_Actual].SetField("PARTE_ID_3", Fila_Leida["PARTE_ID"].ToString().Trim());
                        Dt_Partes_Separadas.Rows[Fila_Actual].SetField("PARTE_NOMBRE_3", Fila_Leida["PARTE_NOMBRE"].ToString().Trim());
                        Columna_Toca = 1;
                    }
                }
                return Dt_Partes_Separadas;
            }        
        

        #endregion

    #endregion
    
    #region Grids
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Solicitudes_PageIndexChanging
        ///DESCRIPCIÓN: Maneja la paginación del Listado
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Solicitudes_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            try {
                Grid_Listado_Solicitudes.SelectedIndex = (-1);
                Grid_Listado_Solicitudes.PageIndex = e.NewPageIndex;
                Llenar_Listado_Solicitudes();
            } catch(Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Solicitudes_SelectedIndexChanged
        ///DESCRIPCIÓN: Obtiene los datos de una Solicitud 
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Solicitudes_SelectedIndexChanged(object sender, EventArgs e) {
            try{
                if (Grid_Listado_Solicitudes.SelectedIndex > (-1)){
                    Limpiar_Formulario();
                    Hdf_No_Solicitud.Value = Grid_Listado_Solicitudes.SelectedDataKey["NO_SOLICITUD"].ToString();
                    Hdf_Tipo_Bien.Value = Grid_Listado_Solicitudes.SelectedDataKey["TIPO_BIEN"].ToString();
                    Configuracion_Formulario("MOSTRAR INFORMACION");
                    Mostrar_Registro();
                    Grid_Listado_Solicitudes.SelectedIndex = -1;
                    System.Threading.Thread.Sleep(500);
                }
            } catch(Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Productos_PageIndexChanging
        ///DESCRIPCIÓN: Maneja la paginación del GridView de los Vehiculos
        ///PROPIEDADES:     
        ///CREO : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO: 15/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Busqueda_Vehiculo_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            try {
                Llenar_Grid_Listado_Bienes_Vehiculos(e.NewPageIndex);
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Busqueda_Vehiculo_SelectedIndexChanged
        ///DESCRIPCIÓN: Maneja el evento de cambio de Seleccion del GridView de Vehiculos del
        ///             Modal de Busqueda.
        ///PROPIEDADES:     
        ///CREO : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO: 15/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Busqueda_Vehiculo_SelectedIndexChanged(object sender, EventArgs e) {
            try {
                if (Grid_Listado_Busqueda_Vehiculo.SelectedIndex > (-1)) {
                    String Vehiculo_ID = Grid_Listado_Busqueda_Vehiculo.SelectedRow.Cells[1].Text.Trim();
                    Cargar_Datos_Vehiculo(Vehiculo_ID, "IDENTIFICADOR",Cmb_Tipo_Bien.SelectedValue);
                }
                MPE_Busqueda_Vehiculo.Hide();
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

    #endregion

    #region Eventos

        #region Busquedas

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Directa_Click
            ///DESCRIPCIÓN: Busca el Vehiculo por el No. Inventario.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            protected void Btn_Busqueda_Directa_Click(object sender, ImageClickEventArgs e) {
                try {
                    if (Cmb_Tipo_Bien.SelectedIndex > 0)
                    {
                        if (Cmb_Tipo_Bien.SelectedValue == "VEHICULO")
                        {
                            if (Txt_No_Economico.Text.Trim().Length > 0)
                            {
                                Cargar_Datos_Vehiculo(Txt_No_Economico.Text.Trim(), "NO_ECONOMICO", Cmb_Tipo_Bien.SelectedValue);
                            }                            
                        }
                        else if (Txt_No_Inventario.Text.Trim().Length > 0)
                        {
                            Cargar_Datos_Vehiculo(Txt_No_Inventario.Text.Trim(), "NO_INVENTARIO", Cmb_Tipo_Bien.SelectedValue);
                        }
                        else if (Txt_No_Serie.Text.Trim().Length > 0)
                            {
                                Cargar_Datos_Vehiculo(Txt_No_Serie.Text.Trim(), "NO_SERIE", Cmb_Tipo_Bien.SelectedValue);
                            }
                    }
                    else
                    {
                        Limpiar_Formulario_Vehiculo();
                        Lbl_Ecabezado_Mensaje.Text = "Es necesario introducir el No. Economico del Vehículo.";
                        Lbl_Mensaje_Error.Text = "";
                        Div_Contenedor_Msj_Error.Visible = true;

                    }
                } catch (Exception Ex) {
                    Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                    Lbl_Mensaje_Error.Text = "Se presento una Excepcion al Buscar [" + Ex.Message + "]";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Avanzada_Click
            ///DESCRIPCIÓN: Busca el Vehiculo.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 15/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            protected void Btn_Busqueda_Avanzada_Click(object sender, ImageClickEventArgs e) {
                try {
                    MPE_Busqueda_Vehiculo.Show();
                } catch (Exception Ex) {
                    Lbl_Ecabezado_Mensaje.Text = "";
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }

            #region Vehiculo

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Datos_Vehiculo_Click
                ///DESCRIPCIÓN: Ejecuta la Busqueda de los Vehiculos por datos generales.
                ///PARAMETROS:     
                ///CREO: Francisco Antonio Gallardo Castañeda.
                ///FECHA_CREO: 15/Mayo/2012 
                ///MODIFICO:
                ///FECHA_MODIFICO
                ///CAUSA_MODIFICACIÓN
                ///*******************************************************************************
                protected void Btn_Buscar_Datos_Vehiculo_Click(object sender, ImageClickEventArgs e) {
                    try {
                        Session["FILTRO_BUSQUEDA"] = "DATOS_GENERALES";
                        Llenar_Grid_Listado_Bienes_Vehiculos(0);
                    } catch (Exception Ex) {
                        Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                        Lbl_Mensaje_Error.Text = "";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Btn_Limpiar_Filtros_Buscar_Datos_Vehiculo_Click
                ///DESCRIPCIÓN          : Hace la limpieza de los campos.
                ///PARAMETROS           : 
                ///CREO                 : Francisco Antonio Gallardo Castañeda
                ///FECHA_CREO           : 15/Mayo/2012 
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                protected void Btn_Limpiar_Filtros_Buscar_Datos_Vehiculo_Click(object sender, ImageClickEventArgs e) {
                    try {
                        Txt_Busqueda_Vehiculo_Numero_Inventario.Text = "";
                        Txt_Busqueda_Vehiculo_Numero_Economico.Text = "";
                        Txt_Busqueda_Vehiculo_Modelo.Text = "";
                        Cmb_Busqueda_Vehiculo_Marca.SelectedIndex = 0;
                        Txt_Busqueda_Vehiculo_Anio_Fabricacion.Text = "";
                        Cmb_Busqueda_Vehiculo_Color.SelectedIndex = 0;
                        Cmb_Busqueda_Vehiculo_Dependencias.SelectedIndex = 0;
                        MPE_Busqueda_Vehiculo.Show();
                    } catch (Exception Ex) {
                        Lbl_Ecabezado_Mensaje.Text = "Excepción.";
                        Lbl_Mensaje_Error.Text = "Ex:['" + Ex.Message + "']";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                }
        
                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias_SelectedIndexChanged
                ///DESCRIPCIÓN          : Maneja el evento del Combo de Dependencias.
                ///PARAMETROS           : 
                ///CREO                 : Francisco Antonio Gallardo Castañeda
                ///FECHA_CREO           : 15/Mayo/2012 
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                protected void Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias_SelectedIndexChanged(object sender, EventArgs e) {
                    if (Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.SelectedIndex > 0)
                    {
                        Llenar_Combo_Empleados(Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.SelectedItem.Value.Trim(), ref Cmb_Busqueda_Vehiculo_Nombre_Resguardante);
                    } else {
                        Llenar_Combo_Empleados(null, ref Cmb_Busqueda_Vehiculo_Nombre_Resguardante);
                    }
                }
        
                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Resguardante_Vehiculo_Click
                ///DESCRIPCIÓN: Ejecuta la Busqueda de los vehiculos por resguardos.
                ///PARAMETROS:     
                ///CREO: Francisco Antonio Gallardo Castañeda.
                ///FECHA_CREO: 15/Mayo/2012 
                ///MODIFICO:
                ///FECHA_MODIFICO
                ///CAUSA_MODIFICACIÓN
                ///*******************************************************************************
                protected void Btn_Buscar_Resguardante_Vehiculo_Click(object sender, ImageClickEventArgs e) {
                    try {
                        Session["FILTRO_BUSQUEDA"] = "RESGUARDANTES";
                        Llenar_Grid_Listado_Bienes_Vehiculos(0);
                    } catch (Exception Ex) {
                        Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                        Lbl_Mensaje_Error.Text = "";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                }
    
                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Btn_Limpiar_Filtros_Buscar_Resguardante_Vehiculo_Click
                ///DESCRIPCIÓN          : Hace la limpieza de los campos.
                ///PARAMETROS           : 
                ///CREO                 : Francisco Antonio Gallardo Castañeda
                ///FECHA_CREO           : 15/Mayo/2012 
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                protected void Btn_Limpiar_Filtros_Buscar_Resguardante_Vehiculo_Click(object sender, ImageClickEventArgs e)  {
                    try {
                        Txt_Busqueda_Vehiculo_RFC_Resguardante.Text = "";
                        Txt_Busqueda_Vehiculo_No_Empleado.Text = "";
                        Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.SelectedIndex = 0;
                        Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias_SelectedIndexChanged(Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias, null);
                        Cmb_Busqueda_Vehiculo_Nombre_Resguardante.SelectedIndex = 0;
                        MPE_Busqueda_Vehiculo.Show();
                    } catch (Exception Ex) {
                        Lbl_Ecabezado_Mensaje.Text = "Excepción.";
                        Lbl_Mensaje_Error.Text = "Ex:['" + Ex.Message + "']";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                }

            #endregion

        #endregion

        #region Operaciones

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
            ///DESCRIPCIÓN: Ejecuta el Proceso para hacer una Nueva solicitud.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e) {
                try { 
                    if (Btn_Nuevo.AlternateText.Trim().Equals("Nuevo")) {
                        if (Cmb_Entrada_UR.SelectedIndex > 0) {
                            Limpiar_Formulario();
                            Configuracion_Formulario("OPERACION");
                            Btn_Modificar.Visible = false;
                            Btn_Cancelar.Visible = false;
                            Btn_Imprimir_Solicitud_Servicio.Visible = false;
                            Txt_Fecha_Elaboracion.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Today);
                            Pnl_Motivo_Rechazo.Visible = false;
                            Cmb_Entrada_UR_SelectedIndexChanged(Cmb_Entrada_UR, null);
                            Consultar_Datos_Empleado_Logueado();
                            Cmb_Tipo_Servicio.SelectedIndex = Cmb_Tipo_Servicio.Items.IndexOf(Cmb_Tipo_Servicio.Items.FindByValue("SERVICIO_GENERAL"));
                            Txt_No_Economico.Focus();
                        } else {
                            Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                            Lbl_Mensaje_Error.Text = "Seleccionar la UR para la cual se solicitará el Servicio";
                            Div_Contenedor_Msj_Error.Visible = true;
                        }
                    } else {
                        if (Validar_Solicitud()) {
                            Alta_Registro();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Operación de Alta de Solicitud Exitosa');", true);
                            Limpiar_Formulario();
                            Configuracion_Formulario("INICIAL");
                            Llenar_Listado_Solicitudes();
                        }
                    }
                } catch (Exception Ex) {
                    Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                    Lbl_Mensaje_Error.Text = "Excepción ['" + Ex.Message + "']";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
    
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
            ///DESCRIPCIÓN: Ejecuta el Proceso para hacer una Modificación de una Solicitud
            ///             existente.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e) { 
                try {
                    if (Btn_Modificar.AlternateText.Trim().Equals("Modificar")) {
                        if (Hdf_No_Solicitud.Value.Trim().Length > 0) {
                            if (!Txt_Estatus.Text.Trim().Equals("CANCELADA")) {
                                if (!Txt_Estatus.Text.Trim().Equals("AUTORIZADA")) {
                                    Configuracion_Formulario("OPERACION");
                                    Btn_Nuevo.Visible = false;
                                    Btn_Cancelar.Visible = false;
                                    Btn_Imprimir_Solicitud_Servicio.Visible = false;
                                } else {
                                    Lbl_Ecabezado_Mensaje.Text = "Una Solicitud AUTORIZADA no puede Modificarse.";
                                    Lbl_Mensaje_Error.Text = "";
                                    Div_Contenedor_Msj_Error.Visible = true;
                                }
                            } else {
                                Lbl_Ecabezado_Mensaje.Text = "Una Solicitud CANCELADA no puede Modificarse.";
                                Lbl_Mensaje_Error.Text = ""; 
                                Div_Contenedor_Msj_Error.Visible = true;
                            }
                        } else {
                            Lbl_Ecabezado_Mensaje.Text = "No hay Solicitud Seleccionada para Modificar.";
                            Lbl_Mensaje_Error.Text = ""; 
                            Div_Contenedor_Msj_Error.Visible = true;
                        }
                    } else {
                        if (Validar_Solicitud()) {
                            Modifica_Registro();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Operación de Actualización de Solicitud Exitosa');", true);
                            Mostrar_Registro();
                            Configuracion_Formulario("MOSTRAR INFORMACION");
                            Llenar_Listado_Solicitudes();
                        }
                    }
                } catch (Exception Ex) {
                    Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                    Lbl_Mensaje_Error.Text = "Excepción ['" + Ex.Message + "']";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
    
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
            ///DESCRIPCIÓN: Ejecuta el Proceso para hacer una Modificación de una Solicitud
            ///             existente.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            protected void Btn_Cancelar_Click(object sender, ImageClickEventArgs e) { 
                try {
                    if (Hdf_No_Solicitud.Value.Trim().Length > 0) {
                        if (!Txt_Estatus.Text.Trim().Equals("CANCELADA")) {
                        Cancelar_Registro();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Operación de Cancelación de Solicitud Exitosa');", true);
                        Limpiar_Formulario();
                        Configuracion_Formulario("INICIAL");
                        Llenar_Listado_Solicitudes();
                        } else {
                            Lbl_Ecabezado_Mensaje.Text = "Una Solicitud Cancelada no puede Cancelarse de nueva Cuenta.";
                            Lbl_Mensaje_Error.Text = ""; 
                            Div_Contenedor_Msj_Error.Visible = true;
                        }
                    } else {
                        Lbl_Ecabezado_Mensaje.Text = "No hay Solicitud Seleccionada para Cancelar.";
                        Lbl_Mensaje_Error.Text = ""; 
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                } catch (Exception Ex) {
                    Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                    Lbl_Mensaje_Error.Text = "Excepción ['" + Ex.Message + "']";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
    
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Btn_Imprimir_Solicitud_Servicio_Click
            ///DESCRIPCIÓN: Imprime la Solicitud
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 23/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            protected void Btn_Imprimir_Solicitud_Servicio_Click(object sender, ImageClickEventArgs e) { 
                try {
                    if (Hdf_No_Solicitud.Value.Trim().Length > 0) {
                        DataTable Dt_Refacciones = new DataTable("DT_REFACCIONES");                        
                        Dt_Refacciones.TableName = "DT_REFACCIONES";
                        Cls_Rpt_Tal_Solicitud_Servicio_Negocio Rpt_Solicitud = new Cls_Rpt_Tal_Solicitud_Servicio_Negocio();
                        Rpt_Solicitud.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value.Trim());
                        if (!String.IsNullOrEmpty(Hdf_Tipo_Bien.Value))
                            Rpt_Solicitud.P_Tipo_Bien = Hdf_Tipo_Bien.Value;
                        else
                            Rpt_Solicitud.P_Tipo_Bien = Cmb_Tipo_Bien.SelectedValue.Trim();
                        Rpt_Solicitud.P_Server_ = this.Server;
                        DataSet Ds_Consulta = Rpt_Solicitud.Crear_Reporte_Solicitud_Servicio();
                        Ds_Consulta.Tables.Add(Dt_Refacciones.Copy());
                        if (Ds_Consulta != null) {
                            if (!Cmb_Tipo_Servicio.SelectedItem.Value.Equals("REVISTA_MECANICA")) Generar_Reporte(Ds_Consulta, new Ds_Rpt_Tal_Solicitud_Servicio(), "Rpt_Tal_Solicitud_Servicio.rpt");
                            else Generar_Reporte(Ds_Consulta, new Ds_Rpt_Tal_Revista_Mecanica_Mejorada(), "Rpt_Tal_Revista_Mecanica_Mejorada.rpt");
                        }
                    } else {
                        Lbl_Ecabezado_Mensaje.Text = "No hay Solicitud Seleccionada para Imprimir.";
                        Lbl_Mensaje_Error.Text = ""; 
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                } catch (Exception Ex) {
                    Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                    Lbl_Mensaje_Error.Text = "Excepción ['" + Ex.Message + "']";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }            
            /// *************************************************************************************
            /// NOMBRE:              Mostrar_Reporte
            /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
            /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
            ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
            /// USUARIO CREO:        Juan Alberto Hernández Negrete.
            /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
            /// USUARIO MODIFICO:    Salvador Hernández Ramírez
            /// FECHA MODIFICO:      23-Mayo-2011
            /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
            /// *************************************************************************************
            protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato) {
                String Pagina = "../../Reporte/";
                try {
                        Pagina = Pagina + Nombre_Reporte_Generar;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                        "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
                } catch (Exception Ex) {
                    throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
            ///DESCRIPCIÓN: Ejecuta el Proceso para Salir.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            protected void Btn_Salir_Click(object sender, ImageClickEventArgs e) {
                try {
                    if (Btn_Salir.AlternateText.Trim().Equals("Salir")) {
                        if (Div_Campos.Visible) {
                            Limpiar_Formulario();
                            Configuracion_Formulario("INICIAL");
                            Cmb_Entrada_UR.SelectedIndex = Cmb_Entrada_UR.Items.IndexOf(Cmb_Entrada_UR.Items.FindByValue(Cls_Sessiones.Dependencia_ID_Empleado));
                        } else {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    } else {
                        if (Btn_Nuevo.Visible) {
                            Limpiar_Formulario();
                            Configuracion_Formulario("INICIAL");
                            Cmb_Entrada_UR.SelectedIndex = Cmb_Entrada_UR.Items.IndexOf(Cmb_Entrada_UR.Items.FindByValue(Cls_Sessiones.Dependencia_ID_Empleado));
                        } else {
                            Configuracion_Formulario("MOSTRAR INFORMACION");
                            Mostrar_Registro();
                        }
                    }
                } catch (Exception Ex) {
                    Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                    Lbl_Mensaje_Error.Text = "Excepción ['" + Ex.Message + "']";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Btn_Actualizar_Listado_Click
            ///DESCRIPCIÓN: Ejecuta el Proceso para Salir.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            protected void Btn_Actualizar_Listado_Click(object sender, ImageClickEventArgs e) {
                try {
                    Llenar_Listado_Solicitudes();
                } catch (Exception Ex) {
                    Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                    Lbl_Mensaje_Error.Text = "Excepción ['" + Ex.Message + "']";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }

        #endregion

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Entrada_UR_SelectedIndexChanged
        ///DESCRIPCIÓN          : Maneja el evento del Combo de Dependencias.
        ///PARAMETROS           : 
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 15/Mayo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Cmb_Entrada_UR_SelectedIndexChanged(object sender, EventArgs e) {
            Cmb_Grupo_UR.SelectedIndex = Cmb_Grupo_UR.Items.IndexOf(Cmb_Grupo_UR.Items.FindByValue(Cmb_Entrada_Gpo_UR.SelectedValue));
            Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Cmb_Entrada_UR.SelectedItem.Value));
            if (!Validar_Acceso_Unidades_Responsables())
            {
                Cmb_Busqueda_Vehiculo_Dependencias.SelectedIndex = Cmb_Busqueda_Vehiculo_Dependencias.Items.IndexOf(Cmb_Busqueda_Vehiculo_Dependencias.Items.FindByValue(Cmb_Entrada_UR.SelectedItem.Value));
                Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.SelectedIndex = Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.Items.IndexOf(Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.Items.FindByValue(Cmb_Entrada_UR.SelectedItem.Value));
                Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias_SelectedIndexChanged(Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias, null);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Entrada_Gpo_UR
        ///DESCRIPCIÓN          : Maneja el evento del Combo de Dependencias.
        ///PARAMETROS           : 
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 15/Mayo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Cmb_Entrada_Gpo_UR_SelectedIndexChanged(object sender, EventArgs e) {
            Llenar_Combo_Unidades_Responsables_Entrada();
        }    

        protected void Txt_No_Inventario_TextChanged(object sender, EventArgs e)
        {
            
            ImageClickEventArgs Ie = null;
            Btn_Busqueda_Directa_Click(sender, Ie);
        }
    #endregion
}