﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Ope_Tal_Generacion_Lista_Refacciones.aspx.cs"
    Inherits="paginas_Taller_Municipal_Frm_Ope_Tal_Generacion_Lista_Refacciones" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">
    <script type="text/javascript">
        window.onerror = new Function("return true");
        //Abrir una ventana modal
        function Abrir_Ventana_Modal(Url, Propiedades) {
            window.showModalDialog(Url, null, Propiedades);
        }
        function Validar_Longitud_Texto(Text_Box, Max_Longitud) {
            if (Text_Box.value.length > Max_Longitud) {
                Text_Box.value = Text_Box.value.substring(0, Max_Longitud);
            }
        }
    </script>
    <script type="text/javascript" language="javascript">
    //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_sesiones.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion()
        {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para matener la sesión activa
        setInterval('MantenSesion()', <%=(int)(0.9*(Session.Timeout * 60000))%>);

        
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScptM_Catalogo" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" EnablePartialRendering="true" AsyncPostBackTimeout="9000">
    </cc1:ToolkitScriptManager>
    <%--Inicio formulario--%>
    <asp:UpdatePanel ID="Upd_Lista_Refacciones" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--update progrees--%>
            <asp:UpdateProgress ID="Uprg_Progress" runat="server" AssociatedUpdatePanelID="Upd_Lista_Refacciones"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Servicios" style="background-color: #ffffff; width: 100%; height: 100%">
                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td class="label_titulo" colspan="4">
                            Generación de Lista Automática de Refacciones
                        </td>
                    </tr>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" />
                            <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" CssClass="estilo_fuente_mensaje_error"
                                Text="" /><br />
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
                    <tr class="barra_busqueda">
                        <td colspan="2" align="left" style="width: 20%">
                            <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                CssClass="Img_Button" OnClick="Btn_Modificar_Click" />
                            <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                CssClass="Img_Button" OnClick="Btn_Salir_Click" />
                        </td>
                        <td colspan="2" align="right" valign="top" style="width:80%">
                            <table style="width: 70%;">
                                <tr>
                                    <td style="vertical-align: top; text-align: right; width: 45%">
                                        Búsqueda:
                                        <asp:TextBox ID="Txt_Buscar" runat="server" MaxLength="100" TabIndex="5" ToolTip="Buscar"
                                            Width="55%" />
                                        <cc1:TextBoxWatermarkExtender ID="WTE_Txt_Buscar" runat="server" WatermarkCssClass="watermarked"
                                            WatermarkText="<Folio Solicitud>" TargetControlID="Txt_Buscar" />
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Buscar" runat="server" TargetControlID="Txt_Buscar"
                                            FilterType="Numbers" />
                                    </td>
                                    <td style="vertical-align: top; text-align: right; width: 50%">
                                        Estatus:
                                        <asp:DropDownList ID="Cmb_Busqueda_Estatus" runat="server" Width="60%">
                                            <asp:ListItem>&lt; Seleccione &gt;</asp:ListItem>
                                            <asp:ListItem>REPARACION</asp:ListItem>
                                            <asp:ListItem>LISTADO</asp:ListItem>
                                            <asp:ListItem>PROCESO</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td style="vertical-align: top; text-align: right; width: 5%">
                                        <asp:ImageButton ID="Btn_Buscar" runat="server" TabIndex="6" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                            OnClick="Btn_Buscar_Click" ToolTip="Buscar" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            &nbsp;
            <br />
            <div id="Div_Servicios_Preventivos" runat="server" style="background-color: #ffffff;
                width: 100%; height: 100%">
                <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                    <tr>
                        <td align="center" colspan="4">
                            <asp:GridView ID="Grid_Listado_Servicios" runat="server" AllowPaging="True" AllowSorting="true"
                                AutoGenerateColumns="False" CssClass="GridView_1" EmptyDataText="No se Encontrarón Servicios Pendientes"
                                GridLines="None" OnPageIndexChanging="Grid_Listado_Servicios_PageIndexChanging"
                                OnSelectedIndexChanged="Grid_Listado_Servicios_SelectedIndexChanged" OnSorting="Grid_Listado_Servicios_Sorting"
                                DataKeyNames="NO_ENTRADA,NO_SERVICIO,NO_SOLICITUD"
                                PageSize="20" Width="99%">
                                <RowStyle CssClass="GridItem" />
                                <Columns>
                                    <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                        <ItemStyle Width="30px" />
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="NO_ENTRADA" HeaderText="NO_ENTRADA" SortExpression="NO_ENTRADA">
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="90px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NO_SERVICIO" HeaderText="NO_SERVICIO" SortExpression="NO_SERVICIO">
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="90px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NO_SOLICITUD" HeaderText="NO_SOLICITUD" SortExpression="NO_SOLICITUD">
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="90px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FOLIO" HeaderText="Folio" SortExpression="FOLIO">
                                        <ItemStyle Font-Bold="true" Font-Size="X-Small" HorizontalAlign="Center" Width="90px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FECHA_RECEPCION" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Fecha Recepción"
                                        SortExpression="FECHA_RECEPCION">
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="140px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="TIPO_SERVICIO" HeaderText="Tipo Servicio" SortExpression="TIPO_SERVICIO">
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="140px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" SortExpression="DEPENDENCIA">
                                        <ItemStyle Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NO_INVENTARIO" HeaderText="No. Inventario" SortExpression="NO_INVENTARIO">
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="120px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ESTATUS_REAL" HeaderText="Estatus" SortExpression="ESTATUS_REAL">
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="120px" />
                                    </asp:BoundField>
                                </Columns>
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="Div_Datos_Solicitud" runat="server">
                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td style="width: 15%;">
                            Unidad Responsable
                        </td>
                        <td colspan="3">
                            <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            Kilometraje
                        </td>
                        <td style="width: 16%;">
                            <asp:Label ID="Lbl_Kilometraje" runat="server" Text=""></asp:Label>
                        </td>
                        <td style="width: 15%;">
                            &nbsp;&nbsp; Tipo Servicio
                        </td>
                        <td style="width: 35%;">
                            <asp:Label ID="Lbl_Tipo_Servicio" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Panel ID="Pnl_Vehiculo_Seleccionado" runat="server" Width="99%" GroupingText="Vehículo para el Servicio">
                                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                                    <tr>
                                        <td style="width: 15%;">
                                            <asp:HiddenField ID="Hdf_Vehiculo_ID" runat="server" />
                                            No. Inventario
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="Lbl_No_Inventario" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td style="width: 15%;">
                                            &nbsp;&nbsp; No. Economico
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="Lbl_No_Economico" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">
                                            Vehículo
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="Lbl_Datos_Vehiculo" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">
                                            Placas
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="Lbl_Placas" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td style="width: 15%;">
                                            &nbsp;&nbsp; Año
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="Lbl_Anio" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Panel ID="Pnl_Descripcion_Servicio" runat="server" Width="99%" GroupingText="Descripción del Servicio">
                                <asp:TextBox ID="Txt_Descripcion_Servicio" runat="server" Rows="5" TextMode="MultiLine"
                                    Width="99%" Enabled="false"></asp:TextBox>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
                <hr style="width: 98%;" />
                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td style="width: 35%;">
                            Mecanico Asignado
                        </td>
                        <td colspan="3" style="width: 65%;">
                            <asp:Label ID="Lbl_Mecanicos" runat="server" Text="Mecanico Asignado" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                </table>
                <hr style="width: 98%;" />
                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td colspan="4">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Panel ID="Pnl_Diagnostico_Mecanico" runat="server" Width="99%" GroupingText="Descripción del Diagnostico del Mecanico">
                                <asp:TextBox ID="Txt_Diagnostico_Mecanico" runat="server" Rows="6" TextMode="MultiLine"
                                    Width="99%" Enabled="false"></asp:TextBox>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="Div_Asignar_Refacciones" runat="server" style="background-color: #ffffff;
                width: 100%; height: 100%">
                <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                    <%------------------ Asignacion de Refacciones ------------------%>
                    <tr style="background-color: #3366CC">
                        <td id="Barra_Generales" runat="server" colspan="3" style="text-align: left; font-size: 15px;
                            color: #FFFFFF;">
                            Lista de Refacciones
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%">
                            Servicio Preventivo
                        </td>
                        <td style="width: 32%">
                            <asp:DropDownList ID="Cmb_Servicio_Preventivo" runat="server" AutoPostBack="true"
                                OnSelectedIndexChanged="Cmb_Servicio_Preventivo_SelectedIndexChanged" Width="94%">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 50%">
                            Búsqueda Avanzada de Refacciones
                            <asp:ImageButton ID="Btn_Busqueda_Refacciones" runat="server" Height="22px" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png"
                                OnClick="Btn_Busqueda_Refacciones_Click" TabIndex="10" ToolTip="Búsqueda Avanzada"
                                Width="22px" />
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="Pnl_Refacciones" runat="server" BorderStyle="None" GroupingText="Lista de Refacciones"
                    Width="97%">
                    <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                        <tr>
                            <td align="center" colspan="4">
                                <asp:GridView ID="Grid_Refacciones" runat="server" AllowPaging="true" AutoGenerateColumns="False"
                                    CssClass="GridView_1" DataKeyNames="REFACCION_ID,DESCRIPCION" GridLines="none"
                                    OnDataBound="Grid_Refacciones_DataBound" OnPageIndexChanging="Grid_Refacciones_PageIndexChanging"
                                    OnSelectedIndexChanged="Grid_Refaccion_SelectedIndexChanged" PageSize="5" Style="white-space: normal"
                                    Width="96%">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Seleccionar">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="Chk_Seleccionar" runat="server" AutoPostBack="false" OnCheckedChanged="Chk_Seleccionar_CheckedChanged" />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="17%" />
                                            <ItemStyle HorizontalAlign="Center" Width="17%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-Width="12%" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox ID="Txt_Grid_Cantidad" runat="server" MaxLength="7" OnTextChanged="Txt_Grid_Cantidad_TextChanged"
                                                    Style="width: 30%; text-align: left;" Text='<%# Bind("CANTIDAD", "{0:0.##}") %>'>
                                                </asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="Fte_Txt_Grid_Cantidad" runat="server" FilterType="Numbers,Custom"
                                                    TargetControlID="Txt_Grid_Cantidad" ValidChars="," />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="REFACCION_ID" HeaderText="Id" Visible="false">
                                            <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                            <ItemStyle HorizontalAlign="Left" Width="5%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CLAVE" HeaderText="Clave">
                                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NOMBRE" HeaderText="Nombre">
                                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="TIPO" HeaderText="Tipo">
                                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ESTATUS" HeaderText="Estatus">
                                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripcion" Visible="false" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="Btn_Grid_Eliminar" runat="server" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png"
                                                    ToolTip="Quitar Refacción" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Right" Width="5%" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 18%">
                                &nbsp;
                            </td>
                            <td style="width: 32%">
                                &nbsp;
                            </td>
                            <td style="width: 18%">
                                &nbsp;
                            </td>
                            <td style="width: 32%">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 18%">
                                &nbsp;
                            </td>
                            <td style="width: 32%">
                                &nbsp;
                            </td>
                            <td style="width: 18%">
                                &nbsp;
                            </td>
                            <td style="width: 32%">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
            <asp:HiddenField ID="Hdf_No_Entrada" runat="server" />
            <asp:HiddenField ID="Hdf_No_Servicio" runat="server" />
            <asp:HiddenField ID="Hdf_No_Solicitud" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
