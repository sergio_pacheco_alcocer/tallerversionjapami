﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Ope_Tal_Diagnostico_Proveedor_Servicio.aspx.cs"
    Inherits="paginas_Taller_Mecanico_Frm_Ope_Tal_Diagnostico_Proveedor_Servicio" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">

    <script type="text/javascript">
        window.onerror = new Function("return true");
        //Abrir una ventana modal
        function Abrir_Ventana_Modal(Url, Propiedades) {
            window.showModalDialog(Url, null, Propiedades);
        }
        function Validar_Longitud_Texto(Text_Box, Max_Longitud) {
            if (Text_Box.value.length > Max_Longitud) {
                Text_Box.value = Text_Box.value.substring(0, Max_Longitud);
            }
        }
    </script>

    <script type="text/javascript" language="javascript">
    //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_sesiones.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion()
        {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para matener la sesión activa
        setInterval('MantenSesion()', <%=(int)(0.9*(Session.Timeout * 60000))%>);
        function formatCurrency(num) {
            var Combo = "";
            var Beneficio;
                num = num.toString().replace(/\$|\,/g,'');
                if(isNaN(num))
                num = "0";
                sign = (num == (num = Math.abs(num)));
                num = Math.floor(num*100+0.50000000001);
                cents = num%100;
                num = Math.floor(num/100).toString();
                if(cents<10)
                    cents = "0" + cents;
                    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
                    num = num.substring(0,num.length-(4*i+3))+','+
                    num.substring(num.length-(4*i+3));
                    return (((sign)?'':'-') + num + '.' + cents);
            }
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" EnablePartialRendering="true" AsyncPostBackTimeout="9000" />
    <%--Inicio formulario--%>
    <asp:UpdatePanel ID="Upd_Parametros_Predial" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--update progrees--%>
            <asp:UpdateProgress ID="Uprg_Progress" runat="server" AssociatedUpdatePanelID="Upd_Parametros_Predial"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Generales" style="background-color: #ffffff; width: 100%; height: 100%">
                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td colspan="4" class="label_titulo">
                            Diagnostico de Proveedor
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" />
                            <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" CssClass="estilo_fuente_mensaje_error"
                                Text="" /><br />
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
                    <tr class="barra_busqueda">
                        <td colspan="2" align="left" style="width: 20%">
                            <asp:ImageButton ID="Btn_Asignacion" runat="server" ImageUrl="~/paginas/imagenes/paginas/accept.png"
                                Width="24px" CssClass="Img_Button" AlternateText="Asignar Mecanico Seleccionado"
                                ToolTip="Asignar Mecanico Seleccionado" OnClick="Btn_Asignacion_Click" />
                            <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                Width="24px" CssClass="Img_Button" AlternateText="Salir" ToolTip="Salir" OnClick="Btn_Salir_Click" />
                        </td>
                        <td colspan="2" align="right" valign="top" style="width: 80%">
                            <table style="width: 80%;">
                                <tr>
                                    <td style="vertical-align: top; text-align: right; width: 5%">
                                    </td>
                                    <td style="vertical-align: top; text-align: right; width: 90%">
                                        Búsqueda:
                                        <asp:TextBox ID="Txt_Buscar" runat="server" MaxLength="100" TabIndex="5" ToolTip="Buscar"
                                            Width="180px" AutoPostBack="true" />
                                        <cc1:TextBoxWatermarkExtender ID="WTE_Txt_Buscar" runat="server" WatermarkCssClass="watermarked"
                                            WatermarkText="<No. Servicio>" TargetControlID="Txt_Buscar" />
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Buscar" runat="server" TargetControlID="Txt_Buscar"
                                            FilterType="UppercaseLetters,LowercaseLetters,Numbers" />
                                    </td>
                                    <td style="vertical-align: top; text-align: right; width: 5%">
                                        <asp:ImageButton ID="Btn_Buscar" runat="server" TabIndex="6" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                            ToolTip="Buscar" OnClick="Btn_Buscar_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="Div_Listado_Servicios" runat="server" style="width: 100%;">
                <asp:GridView ID="Grid_Listado_Servicios" runat="server" CssClass="GridView_1" AutoGenerateColumns="False"
                    AllowPaging="True" PageSize="20" Width="99%" GridLines="None" EmptyDataText="No se Encontrarón Servicios Pendientes por Diagnosticar"
                    OnPageIndexChanging="Grid_Listado_Servicios_PageIndexChanging" OnSelectedIndexChanged="Grid_Listado_Servicios_SelectedIndexChanged">
                    <RowStyle CssClass="GridItem" />
                    <Columns>
                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                            <ItemStyle Width="5%" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:ButtonField>
                        <asp:BoundField DataField="NO_ENTRADA" HeaderText="NO_ENTRADA" SortExpression="NO_ENTRADA">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NO_SERVICIO" HeaderText="NO_SERVICIO" SortExpression="NO_SERVICIO">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NO_SOLICITUD" HeaderText="NO_SOLICITUD" SortExpression="NO_SOLICITUD">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FOLIO" HeaderText="Folio" SortExpression="FOLIO">
                            <ItemStyle Font-Bold="true" Font-Size="X-Small" HorizontalAlign="Center" Width="10%" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FECHA_RECEPCION" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Recepción"
                            SortExpression="FECHA_RECEPCION">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="10%" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TIPO_SERVICIO" HeaderText="Tipo de Servicio" SortExpression="TIPO_SERVICIO">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="15%" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DESCRIPCION_SERVICIO" HeaderText="Descripción" SortExpression="DESCRIPCION_SERVICIO">
                            <ItemStyle Font-Size="X-Small" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" SortExpression="DEPENDENCIA">
                            <ItemStyle Font-Size="X-Small" Width="20%" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NO_INVENTARIO" HeaderText="No. Inv." SortExpression="NO_INVENTARIO">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="10%" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NO_ECONOMICO" HeaderText="Economico" SortExpression="NO_ECONOMICO">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="10%" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ESTATUS" HeaderText="" SortExpression="ESTATUS">
                            <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Font-Bold="true" />
                        </asp:BoundField>
                    </Columns>
                    <PagerStyle CssClass="GridHeader" />
                    <SelectedRowStyle CssClass="GridSelected" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAltItem" />
                </asp:GridView>
            </div>
            <div id="Div_Campos" runat="server" style="width: 100%;">
                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td colspan="4">
                            <asp:HiddenField ID="Hdf_No_Entrada" runat="server" />
                            <asp:HiddenField ID="Hdf_No_Solicitud" runat="server" />
                            <asp:HiddenField ID="Hdf_No_Servicio" runat="server" />
                            <asp:HiddenField ID="Hdf_No_Reserva" runat="server" />
                            <asp:HiddenField ID="Hdf_No_Asignacion" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Folio" runat="server" Text="Folio" ForeColor="Black" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Folio" runat="server" Width="98%" ForeColor="Red" Font-Bold="true"
                                Style="text-align: right;"></asp:TextBox>
                        </td>
                        <td colspan="2">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Fecha_Elaboracion" runat="server" Text="Fecha Elaboración"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Fecha_Elaboracion" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                        </td>
                        <td style="width: 15%;">
                            &nbsp;&nbsp;
                            <asp:Label ID="Lbl_Fecha_Recepcion" runat="server" Text="Fecha Recepción"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Fecha_Recepcion" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text="Unidad Responsable"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="Cmb_Unidad_Responsable" runat="server" Width="100%" Enabled="false">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Kilometraje" runat="server" Text="Kilometraje"></asp:Label>
                        </td>
                        <td style="width: 16%;">
                            <asp:TextBox ID="Txt_Kilometraje" runat="server" Width="95%" Enabled="false"></asp:TextBox>
                        </td>
                        <td style="width: 15%;">
                            &nbsp;&nbsp;
                            <asp:Label ID="Lbl_Tipo_Servicio" runat="server" Text="Tipo Servicio"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:DropDownList ID="Cmb_Tipo_Servicio" runat="server" Width="100%" Enabled="false">
                                <asp:ListItem Value="">&lt; - - SELECCIONE - - &gt;</asp:ListItem>
                                <asp:ListItem Value="SERVICIO_CORRECTIVO">SERVICIO CORRECTIVO</asp:ListItem>
                                <asp:ListItem Value="SERVICIO_PREVENTIVO">SERVICIO PREVENTIVO</asp:ListItem>
                                <asp:ListItem Value="VERIFICACION">VERIFICACI&Oacute;N</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Panel ID="Pnl_Vehiculo_Seleccionado" runat="server" Width="99%" GroupingText="Vehículo para el Servicio">
                                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                                    <tr>
                                        <td style="width: 15%;">
                                            <asp:HiddenField ID="Hdf_Vehiculo_ID" runat="server" />
                                            <asp:Label ID="Lbl_No_Inventario" runat="server" Text="No. Inventario"></asp:Label>
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:TextBox ID="Txt_No_Inventario" runat="server" Width="70%" MaxLength="7" Enabled="false"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Inventario" runat="server" TargetControlID="Txt_No_Inventario"
                                                FilterType="Numbers">
                                            </cc1:FilteredTextBoxExtender>
                                        </td>
                                        <td style="width: 15%;">
                                            &nbsp;&nbsp;
                                            <asp:Label ID="Lbl_No_Economico" runat="server" Text="No. Economico"></asp:Label>
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:TextBox ID="Txt_No_Economico" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">
                                            <asp:Label ID="Lbl_Datos_Vehiculo" runat="server" Text="Vehículo"></asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="Txt_Datos_Vehiculo" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">
                                            <asp:Label ID="Lbl_Placas" runat="server" Text="Placas"></asp:Label>
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:TextBox ID="Txt_Placas" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                        </td>
                                        <td style="width: 15%;">
                                            &nbsp;&nbsp;
                                            <asp:Label ID="Lbl_Anio" runat="server" Text="Año"></asp:Label>
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:TextBox ID="Txt_Anio" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="Pnl_Bien_Mueble_Seleccionado" runat="server" GroupingText="Bien Mueble para el Servicio"
                                Width="99%">
                                <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                                    <tr>
                                        <td style="width: 15%;">
                                            <asp:HiddenField ID="Hdf_Bien_Mueble_ID" runat="server" />
                                            <asp:Label ID="Lbl_No_Inventario_BM" runat="server" Text="No. Inventario"></asp:Label>
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:TextBox ID="Txt_No_Inventario_BM" runat="server" MaxLength="7" Width="98%" Enabled="false"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Inventario_BM" runat="server" FilterType="Numbers"
                                                TargetControlID="Txt_No_Inventario_BM">
                                            </cc1:FilteredTextBoxExtender>
                                        </td>
                                        <td style="width: 15%;">
                                            &nbsp;
                                        </td>
                                        <td style="width: 35%;">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">
                                            <asp:Label ID="Lbl_Descripcion_Bien" runat="server" Text="Descripción Bien"></asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="Txt_Descripcion_Bien" runat="server" Enabled="false" Rows="2" TextMode="MultiLine"
                                                Width="99%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">
                                            <asp:Label ID="Lbl_Numero_Serie_Bien" runat="server" Text="No. Serie"></asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="Txt_Numero_Serie_Bien" runat="server" Enabled="false" Width="99%"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Panel ID="Pnl_Descripcion_Servicio" runat="server" Width="99%" GroupingText="Descripción del Servicio">
                                <asp:TextBox ID="Txt_Descripcion_Servicio" runat="server" Rows="5" TextMode="MultiLine"
                                    Width="99%" Enabled="false"></asp:TextBox>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
                <hr style="width: 98%;" />
                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td style="width: 15%;">
                            Proveedor
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Nombre_Proveedor" runat="server" Width="99%" Enabled="false"
                                Style="float: left"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <hr style="width: 98%;" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%">
                            Diagnostico
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Descripcion" runat="server" Width="97%" Height="60px" TextMode="MultiLine"
                                Style="text-transform: uppercase"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="Twe_Txt_Comentarios" runat="server" WatermarkCssClass="watermarked"
                                WatermarkText="Límite de Caractes 1024" TargetControlID="Txt_Descripcion">
                            </cc1:TextBoxWatermarkExtender>
                            <cc1:FilteredTextBoxExtender ID="Txt_Comentarios_FilteredTextBoxExtender" runat="server"
                                FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" TargetControlID="Txt_Descripcion"
                                ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%">
                            Costo Servicio
                        </td>
                        <td style="width: 32%">
                            <asp:TextBox ID="Txt_Costo_Unitario" runat="server" Width="92%" Text="" MaxLength="50"
                                onBlur="this.value=formatCurrency(this.value);"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="Txt_Costo_Unitario_FilteredTextBoxExtender" runat="server"
                                Enabled="True" TargetControlID="Txt_Costo_Unitario" FilterType="Custom,Numbers"
                                ValidChars=".,">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                        <td style="width: 18%">
                        </td>
                        <td style="width: 32%">
                        </td>
                    </tr>
                </table>
            </div>
            <asp:HiddenField ID="Hdf_Proveedor_ID" runat="server" />
            <asp:HiddenField ID="Hdf_Tipo_Bien" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
