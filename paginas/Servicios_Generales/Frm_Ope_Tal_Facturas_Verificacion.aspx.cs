﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Catalogo_Taller_Servicios_Preventivos.Negocio;
using JAPAMI.Catalogo_Taller_Refacciones.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Entradas_Vehiculos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Preventivos.Negocio;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Correctivos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Asignacion_Proveedor.Negocio;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;
using AjaxControlToolkit;
using System.Security.Cryptography;
using System.IO;
using JAPAMI.Taller_Mecanico.Operacion_Facturas_Servicio.Negocio;
using JAPAMI.Solicitud_Pagos.Negocio;
using JAPAMI.Generar_Reservas.Negocio;
using JAPAMI.Catalogo_Compras_Proveedores.Negocio;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Solicitud_Pagos.Datos;

public partial class paginas_Taller_Mecanico_Frm_Ope_Tal_Entrada_Facturas : System.Web.UI.Page
{
    #region Variables
    private const int Const_Estado_Inicial = 0;
    private const int Const_Estado_Nuevo = 1;
    private const int Const_Estado_Modificar = 2;
    private bool _Grid_Archivo = false;
    protected bool Grid_Archivo
    {
        get { return this._Grid_Archivo; }
        set { this._Grid_Archivo = value; }
    }
    #endregion

    #region Page Load / Init
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!Page.IsPostBack)
            {
                Llenar_Combo_Unidades_Responsables();
                Grid_Listado_Servicios.PageIndex = 0;
                Llenar_Listado_Servicios();
                Configuracion_Formulario("INICIAL");
                Cmb_Unidad_Responsable.Enabled = false;
                Txt_Observaciones.Attributes.Add("onkeypress", " Validar_Longitud_Texto(this, 1000);");
                Txt_Observaciones.Attributes.Add("onkeyup", " Validar_Longitud_Texto(this, 1000);");
                Btn_Generar_Solicitud_Pago.Visible = false;
            }
            Mensaje_Error();
            //Fup_Factura_Recibida.UploadedComplete += new EventHandler<AsyncFileUploadEventArgs>(Fup_Factura_Recibida_UploadedComplete);
            //Fup_Factura_Recibida.UploadedFileError += new EventHandler<AsyncFileUploadEventArgs>(Fup_Factura_Recibida_UploadedFileError);
        }
        catch (Exception Ex)
        {
            //Mensaje_Error(Txt_Pagos.Text.Trim() +" - "+ Ex.Message);
            Estado_Botones(Const_Estado_Inicial);
        }
    }
    #endregion

    #region [Metodos]
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Mostrar_Registro
    ///DESCRIPCIÓN: Muestra el Registro en los campos.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 09/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Mostrar_Registro()
    {
        Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
        Solicitud.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
        Solicitud = Solicitud.Consultar_Detalles_Solicitud_Servicio();
        if (Solicitud.P_No_Solicitud > (-1))
        {
            Txt_Folio.Text = Solicitud.P_Folio_Solicitud.Trim();
            Txt_Fecha_Elaboracion.Text = String.Format("{0:dd/MMM/yyyy}", Solicitud.P_Fecha_Elaboracion);
            Cmb_Tipo_Servicio.SelectedIndex = Cmb_Tipo_Servicio.Items.IndexOf(Cmb_Tipo_Servicio.Items.FindByValue(Solicitud.P_Tipo_Servicio));
            Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Solicitud.P_Dependencia_ID));
            Hdf_Vehiculo_ID.Value = Solicitud.P_Bien_ID;
            Txt_Descripcion_Servicio.Text = Solicitud.P_Descripcion_Servicio.ToUpper();
            if (Solicitud.P_Tipo_Bien.Equals("BIEN_MUEBLE"))
            {
                Pnl_Bien_Mueble_Seleccionado.Visible = true;
                Pnl_Vehiculo_Seleccionado.Visible = false;
                Hdf_Bien_Mueble_ID.Value = Solicitud.P_Bien_ID;
                Hdf_Tipo_Bien.Value = "BIEN_MUEBLE";
                Cargar_Datos_Bien_Mueble(Hdf_Bien_Mueble_ID.Value, "IDENTIFICADOR");
            }
            else if (Solicitud.P_Tipo_Bien.Equals("VEHICULO"))
            {
                Pnl_Vehiculo_Seleccionado.Visible = true;
                Pnl_Bien_Mueble_Seleccionado.Visible = false;
                Hdf_Vehiculo_ID.Value = Solicitud.P_Bien_ID;
                Hdf_Tipo_Bien.Value = "VEHICULO";
                Cargar_Datos_Vehiculo(Hdf_Vehiculo_ID.Value, "IDENTIFICADOR");
            }
        }
        if (Hdf_No_Entrada.Value.Trim().Length > 0)
        {
            Cls_Ope_Tal_Entradas_Vehiculos_Negocio Entrada = new Cls_Ope_Tal_Entradas_Vehiculos_Negocio();
            Entrada.P_No_Entrada = Convert.ToInt32(Hdf_No_Entrada.Value);
            Entrada = Entrada.Consultar_Detalles_Entrada_Vehiculo();
            if (Entrada.P_Kilometraje > (-1.0))
            {
                Txt_Kilometraje.Text = String.Format("{0:########0.00}", Entrada.P_Kilometraje);
                Txt_Fecha_Recepcion.Text = String.Format("{0:dd/MMM/yyyy}", Entrada.P_Fecha_Entrada);
            }
        }
        Mostrar_Registro_Servicio();
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Mostrar_Registro_Servicio
    ///DESCRIPCIÓN: Muestra el Registro en los campos.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 09/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Mostrar_Registro_Servicio()
    {
        DataTable Dt_Proveedores = new DataTable();
        Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio Negocio = new Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio();
        Negocio.P_Tipo = "SERVICIO_PREVENTIVO";
        if (Cmb_Tipo_Servicio.SelectedItem.Value.Trim().Equals("SERVICIO_CORRECTIVO")) Negocio.P_Tipo = "SERVICIO_CORRECTIVO";
        Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value.Trim());
        Negocio.P_Estatus = "ACEPTADO','REPARADO','TERMINADO";

        Dt_Proveedores = Negocio.Consulta_Proveedor_Servicio();
        if (Dt_Proveedores.Rows.Count > 0)
        {
            Hdf_No_Asignacion.Value = Dt_Proveedores.Rows[0][Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion].ToString();
            Txt_Nombre_Proveedor.Text = Dt_Proveedores.Rows[0]["NOMBRE_PROVEEDOR"].ToString();
            Hdf_Proveedor_ID.Value = Dt_Proveedores.Rows[0]["PROVEEDOR_ID"].ToString();
            if (Dt_Proveedores.Rows[0]["DIAGNOSTICO"] != null)
                Txt_Descripcion.Text = Dt_Proveedores.Rows[0]["DIAGNOSTICO"].ToString();
            if (Dt_Proveedores.Rows[0]["COSTO"] != null)
            {
                if (!String.IsNullOrEmpty(Dt_Proveedores.Rows[0]["COSTO"].ToString()))
                    Txt_Costo_Unitario.Text = Convert.ToDouble(Dt_Proveedores.Rows[0]["COSTO"]).ToString("#,###,0.00");
            }
        }
    }

    #region Metodos Generales
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Configurar_Formulario
    ///DESCRIPCIÓN: Cargar Combos y Datos del formulario
    ///CREO: jtoledo
    ///FECHA_CREO: 17/May/2012
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    private void Configurar_Formulario()
    {
        try
        {

        }
        catch (Exception ex) { Mensaje_Error(ex.Message); }

    }
    ///*******************************************************************************
    ///NOMBRE DE LA METODO: LLenar_Combo_Id
    ///        DESCRIPCIÓN: llena todos los combos
    ///         PARAMETROS: 1.- Obj_DropDownList: Combo a llenar
    ///                     2.- Dt_Temporal: DataTable genarada por una consulta a la base de datos
    ///                     3.- Texto: nombre de la columna del dataTable que mostrara el texto en el combo
    ///                     3.- Valor: nombre de la columna del dataTable que mostrara el valor en el combo
    ///                     3.- Seleccion: Id del combo el cual aparecera como seleccionado por default
    ///               CREO: Jesus S. Toledo Rdz.
    ///         FECHA_CREO: 06/9/2010
    ///           MODIFICO:
    ///     FECHA_MODIFICO:
    /// CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_Combo_ID(DropDownList Obj_DropDownList, DataTable Dt_Temporal, String _Texto, String _Valor, String Seleccion)
    {
        String Texto = "";
        try
        {
            Obj_DropDownList.Items.Clear();
            Obj_DropDownList.Items.Add(new ListItem("< SELECCIONAR >", "0"));
            foreach (DataRow row in Dt_Temporal.Rows)
            {
                if (_Texto.Contains("+"))
                {
                    String[] Array_Texto = _Texto.Split('+');

                    foreach (String Campo in Array_Texto)
                    {
                        Texto = Texto + row[Campo].ToString();
                        Texto = Texto + "  ";
                    }
                }
                else
                {
                    Texto = row[_Texto].ToString();
                }
                Obj_DropDownList.Items.Add(new ListItem(Texto, row[_Valor].ToString()));
                Texto = "";
            }
            Obj_DropDownList.SelectedValue = Seleccion;
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }

    }
    public void Llenar_Combo_ID(DropDownList Obj_DropDownList)
    {
        try
        {
            Obj_DropDownList.Items.Clear();
            Obj_DropDownList.Items.Add(new ListItem("< SELECCIONAR >", "0"));
            Obj_DropDownList.SelectedValue = "0";
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }

    }
    ///****************************************************************************************
    ///NOMBRE DE LA FUNCION:Mensaje_Error
    ///DESCRIPCION : Muestra el error
    ///PARAMETROS  : P_Texto: texto de un TextBox
    ///CREO        : Toledo Rodriguez Jesus S.
    ///FECHA_CREO  : 04-Septiembre-2010
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACION:
    ///****************************************************************************************
    private void Mensaje_Error(String P_Mensaje)
    {
        Img_Error.Visible = true;
        Lbl_Mensaje_Error.Text += P_Mensaje + "</br>";
    }
    private void Mensaje_Error()
    {

        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Lbl_Ecabezado_Mensaje.Text = "";
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Estado_Botones
    ///DESCRIPCIÓN: Metodo para establecer el estado de los botones y componentes del formulario
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 02/02/2011 05:49:53 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Estado_Botones(int P_Estado)
    {
        Boolean Estado = false;
        switch (P_Estado)
        {
            case 0: //Estado inicial  
                Btn_Salir.AlternateText = "Inicio";

                Btn_Salir.ToolTip = "Inicio";

                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";

                Btn_Salir.Visible = true;
                Div_Campos.Style.Value = "display:none;";
                Div_Listado_Servicios.Style.Value = "display:none;";

                Estado = false;
                //Configuracion_Acceso("Frm_Cat_Tal_Servicios_Preventivos.aspx");
                break;

            case 1: //Seleccionado  
                Btn_Salir.AlternateText = "Cancelar";

                Btn_Salir.ToolTip = "Cancelar";

                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";

                Btn_Salir.Visible = true;
                Div_Campos.Style.Value = "display:none;";
                Div_Listado_Servicios.Style.Value = "display:none;";
                Estado = true;

                break;

            case 2: //Modificar                    

                Btn_Salir.AlternateText = "Cancelar";

                Btn_Salir.ToolTip = "Cancelar";

                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";

                Div_Campos.Style.Value = "display:none;";
                Div_Listado_Servicios.Style.Value = "display:none;";

                Btn_Salir.Visible = true;

                Estado = true;
                break;

        }

        //Txt_Descripcion.Enabled = Estado;
        //Txt_Nombre.Enabled = Estado;
        //Cmb_Estatus.Enabled = Estado;
        Txt_Fecha_Factura.Enabled = Estado;

    }

    #endregion

    #region Metodos Operacion

    #endregion

    #region Validaciones

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Asignacion
    ///DESCRIPCIÓN: Valida la Solicitud de Servicio antes de ser Asignada a un mecanico
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private Boolean Validar_Asignacion()
    {
        String Mensaje_Error_ = "";
        Boolean Validacion = true;
        if (Hdf_No_Servicio.Value.Trim().Length == 0)
        {
            Mensaje_Error_ = Mensaje_Error_ + "+ Seleccionar el Servicio que se realizará.";
            Mensaje_Error_ = Mensaje_Error_ + " <br />";
            Validacion = false;
        }
        if (String.IsNullOrEmpty(Hdf_Proveedor_ID.Value))
        {
            Mensaje_Error_ = Mensaje_Error_ + "+ Seleccionar el Proveedor que hará el Servicio.";
            Mensaje_Error_ = Mensaje_Error_ + " <br />";
            Validacion = false;
        }

        if (!Validacion)
        {
            Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
            Mensaje_Error(Mensaje_Error_);
        }
        return Validacion;
    }

    #endregion

    #region Llenado de Campos [Combos, Listados, Vehiculos]

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Unidades_Responsables
    ///DESCRIPCIÓN: Se llena el Combo de las Unidades Responsables.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Llenar_Combo_Unidades_Responsables()
    {
        Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
        Negocio.P_Estatus = "ACTIVO";
        DataTable Dt_Dependencias = Negocio.Consultar_Unidades_Responsables();
        Cmb_Unidad_Responsable.DataSource = Dt_Dependencias;
        Cmb_Unidad_Responsable.DataTextField = "CLAVE_NOMBRE";
        Cmb_Unidad_Responsable.DataValueField = "DEPENDENCIA_ID";
        Cmb_Unidad_Responsable.DataBind();
        Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Servicios
    ///DESCRIPCIÓN: Se llena el Listado de los Servicios para asignar el Mecanico.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Llenar_Listado_Servicios()
    {
        DataTable Dt_Resultados = new DataTable();
        DataView Dv_Resultados = new DataView();
        if (Session["Frm_Ope_Tal_Entrada_Facturas_Refacciones_Listado_Vista"] == null)
        {
            Cls_Ope_Tal_Servicios_Preventivos_Negocio Serv_Prev = new Cls_Ope_Tal_Servicios_Preventivos_Negocio();
            Serv_Prev.P_Reparacion = "EXTERNA";
            Serv_Prev.P_Estatus_Solicitud = "ENTREGADO','TERMINADO";
            Serv_Prev.P_Estatus = "REPARADO";
            Serv_Prev.P_Tipo_Servicio = "VERIFICACION";
            if (!String.IsNullOrEmpty(Txt_Buscar.Text.Trim()))
                Serv_Prev.P_No_Inventario = Convert.ToInt32(Txt_Buscar.Text.Trim());
            DataTable Dt_Resultados_Preventivos = Serv_Prev.Consultar_Servicios_Preventivos();

            Cls_Ope_Tal_Servicios_Correctivos_Negocio Serv_Correc = new Cls_Ope_Tal_Servicios_Correctivos_Negocio();

            Serv_Correc.P_Estatus_Solicitud = "ENTREGADO','TERMINADO";
            Serv_Correc.P_Estatus = "REPARADO";
            Serv_Correc.P_Reparacion = "EXTERNA";
            Serv_Correc.P_Tipo_Servicio = "VERIFICACION";
            if (!String.IsNullOrEmpty(Txt_Buscar.Text.Trim()))
                Serv_Correc.P_No_Inventario = Convert.ToInt32(Txt_Buscar.Text.Trim());
            DataTable Dt_Resultados_Correctivos = Serv_Correc.Consultar_Servicios_Correctivos();

            Dt_Resultados_Preventivos.Merge(Dt_Resultados_Correctivos);
            Dt_Resultados_Preventivos.AcceptChanges();
            Dt_Resultados = Dt_Resultados_Preventivos.Copy();

            Grid_Listado_Servicios.Columns[1].Visible = true;
            Grid_Listado_Servicios.Columns[2].Visible = true;
            Grid_Listado_Servicios.Columns[3].Visible = true;
            Grid_Listado_Servicios.DataSource = Dt_Resultados_Preventivos;
            Grid_Listado_Servicios.DataBind();
            Grid_Listado_Servicios.Columns[1].Visible = false;
            Grid_Listado_Servicios.Columns[2].Visible = false;
            Grid_Listado_Servicios.Columns[3].Visible = false;
            Session["Frm_Ope_Tal_Entrada_Facturas_Refacciones_Listado"] = Dt_Resultados;
        }
        else
        {
            Dv_Resultados = (DataView)Session["Frm_Ope_Tal_Entrada_Facturas_Refacciones_Listado_Vista"];
            Grid_Listado_Servicios.Columns[1].Visible = true;
            Grid_Listado_Servicios.Columns[2].Visible = true;
            Grid_Listado_Servicios.Columns[3].Visible = true;
            Grid_Listado_Servicios.DataSource = Dv_Resultados;
            Grid_Listado_Servicios.DataBind();
            Grid_Listado_Servicios.Columns[1].Visible = false;
            Grid_Listado_Servicios.Columns[2].Visible = false;
            Grid_Listado_Servicios.Columns[3].Visible = false;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Vehiculo
    ///DESCRIPCIÓN: Se cargan los Datos del Vehiculo Seleccionado.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Cargar_Datos_Vehiculo(String Vehiculo, String Tipo_Busqueda)
    {
        Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
        switch (Tipo_Busqueda)
        {
            case "NO_INVENTARIO":
                Consulta_Negocio.P_No_Inventario = Vehiculo;
                break;
            case "IDENTIFICADOR":
                Consulta_Negocio.P_Vehiculo_ID = Vehiculo;
                break;
            default: break;
        }
        if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Consulta_Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value; }
        DataTable Dt_Vehiculo = Consulta_Negocio.Consultar_Vehiculos();
        if (Dt_Vehiculo.Rows.Count > 0)
        {
            Hdf_Vehiculo_ID.Value = Dt_Vehiculo.Rows[0]["VEHICULO_ID"].ToString().Trim();
            Txt_No_Inventario.Text = Dt_Vehiculo.Rows[0]["NO_INVENTARIO"].ToString().Trim();
            Txt_No_Economico.Text = Dt_Vehiculo.Rows[0]["NO_ECONOMICO"].ToString().Trim();
            Txt_Datos_Vehiculo.Text = Dt_Vehiculo.Rows[0]["VEHICULO_DESCRIPCION"].ToString().Trim();
            Txt_Placas.Text = Dt_Vehiculo.Rows[0]["PLACAS"].ToString().Trim();
            Txt_Anio.Text = Dt_Vehiculo.Rows[0]["ANIO"].ToString().Trim();
            Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString()));
        }
        else
        {
            Lbl_Ecabezado_Mensaje.Text = "El Vehículo no se encontro.";
            if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Mensaje_Error("[Puede ser que no este asignado a la Unidad Responsable Seleccionada]."); }
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Bien_Mueble
    ///DESCRIPCIÓN: Se cargan los Datos del Bien Mueble Seleccionado.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Cargar_Datos_Bien_Mueble(String Bien_Mueble, String Tipo_Busqueda)
    {
        Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
        switch (Tipo_Busqueda)
        {
            case "NO_INVENTARIO":
                Consulta_Negocio.P_No_Inventario = Bien_Mueble;
                break;
            case "IDENTIFICADOR":
                Consulta_Negocio.P_Bien_Mueble_ID = Bien_Mueble;
                break;
            default: break;
        }
        DataTable Dt_Bienes_Muebles = Consulta_Negocio.Consultar_Bien_Mueble();
        if (Dt_Bienes_Muebles.Rows.Count > 0)
        {
            Hdf_Bien_Mueble_ID.Value = Dt_Bienes_Muebles.Rows[0]["BIEN_MUEBLE_ID"].ToString().Trim();
            Txt_No_Inventario_BM.Text = Dt_Bienes_Muebles.Rows[0]["NO_INVENTARIO"].ToString().Trim();
            Txt_Numero_Serie_Bien.Text = Dt_Bienes_Muebles.Rows[0]["NUMERO_SERIE"].ToString().Trim();
            Txt_Descripcion_Bien.Text = Dt_Bienes_Muebles.Rows[0]["DESCRIPCION_BIEN_MUEBLE"].ToString().Trim();
            if (!String.IsNullOrEmpty(Dt_Bienes_Muebles.Rows[0]["DEPENDENCIA_ID"].ToString()))
            {
                Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Bienes_Muebles.Rows[0]["DEPENDENCIA_ID"].ToString()));
            }
            else
            {
                Lbl_Mensaje_Error.Text = "ERROR [Puede ser que el Bien esté dado de Baja].";
                Cmb_Unidad_Responsable.SelectedIndex = 0;
            }
        }
        else
        {
            Lbl_Ecabezado_Mensaje.Text = "El Bien no se encontro.";
            if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Lbl_Mensaje_Error.Text = "[Puede ser que no este asignado a la Unidad Responsable Seleccionada]."; }
            else { Lbl_Mensaje_Error.Text = ""; }
        }
    }
    #endregion

    #region Generales [Configuracion, Limpiar]

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
    ///DESCRIPCIÓN: Limpia los campos del Formulario.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Limpiar_Formulario()
    {
        Hdf_No_Entrada.Value = "";
        Hdf_No_Servicio.Value = "";
        Hdf_No_Solicitud.Value = "";
        Hdf_Proveedor_ID.Value = "";
        Hdf_No_Asignacion.Value = "";
        Txt_Folio.Text = "";
        Txt_Fecha_Elaboracion.Text = "";
        Txt_Fecha_Recepcion.Text = "";
        Txt_Kilometraje.Text = "";
        Cmb_Tipo_Servicio.SelectedIndex = 0;
        Cmb_Unidad_Responsable.SelectedIndex = 0;
        Txt_Descripcion_Servicio.Text = "";
        Hdf_Vehiculo_ID.Value = "";
        Txt_No_Inventario.Text = "";
        Txt_No_Economico.Text = "";
        Txt_Datos_Vehiculo.Text = "";
        Txt_Placas.Text = "";
        Txt_Anio.Text = "";
        Txt_Nombre_Proveedor.Text = "";
        Txt_Nombre_Proveedor.Text = "";
        Txt_Monto.Text = "";
        Txt_No_Factura.Text = "1";
        Txt_Factura.Text = "";
        Txt_Fecha_Factura.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Today);
        Txt_Observaciones.Text = "";
        Session["Tabla_Documentos"] = null;
        Session["Diccionario_Archivos"] = null;
        Session["Frm_Ope_Tal_Entrada_Facturas_Refacciones_Listado"] = null;
        Session["Frm_Ope_Tal_Entrada_Facturas_Refacciones_Listado_Vista"] = null;
        Grid_Facturas.DataSource = null;
        Grid_Facturas.DataBind();
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
    ///DESCRIPCIÓN: Se Habilitan y/o inhabilitan los campos dependendiendo de la Condicion.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Configuracion_Formulario(String Operacion)
    {
        switch (Operacion)
        {
            case "INICIAL":
                Grid_Archivo = false;
                Div_Campos.Visible = false;
                Div_Listado_Servicios.Visible = true;
                //Tbl_Agregar_Factura.Style.Add("display", "none");
                //Tbl_File_Upload.Style.Add("display", "none");
                Btn_Modificar.AlternateText = "Modificar";
                Btn_Modificar.ToolTip = "Modificar";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                Btn_Generar_Solicitud_Pago.Visible = false;                
                break;
            case "OPERACION":
                Grid_Archivo = false;
                Div_Campos.Visible = true;
                Div_Listado_Servicios.Visible = false;
                //Tbl_Agregar_Factura.Style.Add("display", "inline");
                //Tbl_File_Upload.Style.Add("display", "inline");
                Btn_Modificar.AlternateText = "Guardar";
                Btn_Modificar.ToolTip = "Guardar";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                Btn_Generar_Solicitud_Pago.Visible = true;
                break;
        }
    }

    #endregion

    #endregion

    #region [Eventos]


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Controla las operaciones del Boton de Salir
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        if (Div_Campos.Visible)
        {
            Limpiar_Formulario();
            Configuracion_Formulario("INICIAL");
            Btn_Modificar.Visible = false;
        }
        else
        {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Evento que agarra la session de las refacciones seleccionadas
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 10/Mayo/2011 12:55:33 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    protected void Btn_Busqueda_Proveedores_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["Proveedor_ID"] != null && Session["Nombre_Proveedor"] != null)
        {
            Hdf_Proveedor_ID.Value = Session["Proveedor_ID"].ToString();
            Txt_Nombre_Proveedor.Text = Session["Nombre_Proveedor"].ToString();
        }
        Session["Proveedor_ID"] = null;
        Session["Nombre_Proveedor"] = null;
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Click
    ///DESCRIPCIÓN: Evento que busca los registros
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 10/Mayo/2011 12:55:33 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        Session["Frm_Ope_Tal_Generacion_Vista_Refacciones_Listado"] = null;
        Llenar_Listado_Servicios();
    }
    protected void Img_Btn_Cargar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            //HttpContext.Current.Session.Remove(P_Archivo_XML);
            //HttpContext.Current.Session.Remove(P_Comentarios);
        }
        catch (Exception ex)
        {
            //Mostrar_Error("Error: (Img_Btn_Buscar_Click)" + ex.Message, true);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Servicios_PageIndexChanging
    ///DESCRIPCIÓN: Maneja la paginación del Listado
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Listado_Servicios_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Grid_Listado_Servicios.SelectedIndex = (-1);
            Grid_Listado_Servicios.PageIndex = e.NewPageIndex;
            Llenar_Listado_Servicios();
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Servicios_SelectedIndexChanged
    ///DESCRIPCIÓN: Obtiene los datos de un Servicio 
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Listado_Servicios_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (Grid_Listado_Servicios.SelectedIndex > (-1))
            {
                Limpiar_Formulario();
                Hdf_No_Entrada.Value = HttpUtility.HtmlDecode(Grid_Listado_Servicios.SelectedDataKey["NO_ENTRADA"].ToString()).Trim();
                Hdf_No_Servicio.Value = HttpUtility.HtmlDecode(Grid_Listado_Servicios.SelectedDataKey["NO_SERVICIO"].ToString()).Trim();
                Hdf_No_Solicitud.Value = HttpUtility.HtmlDecode(Grid_Listado_Servicios.SelectedDataKey["NO_SOLICITUD"].ToString()).Trim();
                Mostrar_Registro();
                if (!String.IsNullOrEmpty(Hdf_No_Asignacion.Value))
                    Mostrar_Facturas();
                Configuracion_Formulario("OPERACION");
                Grid_Listado_Servicios.SelectedIndex = -1;
                System.Threading.Thread.Sleep(500);
                Btn_Modificar.Visible = true;
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }

    #endregion

    #region Metodos Reportes
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Imprimir
    ///DESCRIPCIÓN: Imprime la solicitud
    ///PROPIEDADES:     
    ///CREO: Sergio Manuel Gallardo
    ///FECHA_CREO: 06/Enero/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Imprimir(String Numero_Solicitud)
    {
        DataSet Ds_Reporte = null;
        DataTable Dt_Pagos = null;
        try
        {
            Cls_Ope_Con_Solicitud_Pagos_Negocio Solicitud_Pago = new Cls_Ope_Con_Solicitud_Pagos_Negocio();
            Ds_Reporte = new DataSet();
            Solicitud_Pago.P_No_Solicitud_Pago = Numero_Solicitud;
            Dt_Pagos = Solicitud_Pago.Consulta_Solicitud_Pagos_con_Detalles();
            if (Dt_Pagos.Rows.Count > 0)
            {
                Dt_Pagos.TableName = "Dt_Solicitud_Pago";
                Ds_Reporte.Tables.Add(Dt_Pagos.Copy());
                //Se llama al método que ejecuta la operación de generar el reporte.
                Generar_Reporte(ref Ds_Reporte, "Rpt_Con_Solicitud_Pago.rpt", "Reporte_Solicitud_Pagos" + Numero_Solicitud, ".pdf");
            }
        }
        //}
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
            Lbl_Mensaje_Error.Visible = true;
        }

    }

    /// *************************************************************************************
    /// NOMBRE:             Generar_Reporte
    /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
    ///              
    /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
    ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
    ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
    ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
    /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
    /// FECHA MODIFICO:     16/Mayo/2011
    /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
    ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
    /// *************************************************************************************
    public void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte_Crystal, String Nombre_Reporte_Generar, String Formato)
    {
        ReportDocument Reporte = new ReportDocument(); // Variable de tipo reporte.
        String Ruta = String.Empty;  // Variable que almacenará la ruta del archivo del crystal report. 
        try
        {
            Ruta = @Server.MapPath("../Rpt/Contabilidad/" + Ruta_Reporte_Crystal);
            Reporte.Load(Ruta);

            if (Ds_Reporte_Crystal is DataSet)
            {
                if (Ds_Reporte_Crystal.Tables.Count > 0)
                {
                    Reporte.SetDataSource(Ds_Reporte_Crystal);
                    Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar + Formato);
                    Mostrar_Reporte(Nombre_Reporte_Generar, Formato);
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE:             Exportar_Reporte_PDF
    /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
    ///                     especificada.
    /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
    ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    public void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte_Generar)
    {
        ExportOptions Opciones_Exportacion = new ExportOptions();
        DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
        PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

        try
        {
            if (Reporte is ReportDocument)
            {
                Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
                Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
        }
    }


    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
    /// USUARIO CREO:        Juan Alberto Hernández Negrete.
    /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:    Salvador Hernández Ramírez
    /// FECHA MODIFICO:      16-Mayo-2011
    /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
    {
        String Pagina = "../../Reporte/";

        try
        {
            Pagina = Pagina + Nombre_Reporte_Generar + Formato;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "open",
                "window.open('" + Pagina + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    #endregion

    ///*******************************************************************************************************
    /// 	NOMBRE_FUNCIÓN: Obtener_Diccionario_Archivos
    /// 	DESCRIPCIÓN: Regresa el diccionario checksum-archivo si se encuentra en variable de sesion y si no,
    /// 	            regresa un diccionario vacio
    /// 	PARÁMETROS:
    /// 	CREO: Roberto González Oseguera
    /// 	FECHA_CREO: 04-may-2011
    /// 	MODIFICÓ: 
    /// 	FECHA_MODIFICÓ: 
    /// 	CAUSA_MODIFICACIÓN: 
    ///*******************************************************************************************************
    private Dictionary<String, Byte[]> Obtener_Diccionario_Archivos()
    {
        Dictionary<String, Byte[]> Diccionario_Archivos = new Dictionary<String, Byte[]>();

        // si existe el diccionario en variable de sesion
        if (Session["Diccionario_Archivos"] != null)
        {
            Diccionario_Archivos = (Dictionary<String, Byte[]>)Session["Diccionario_Archivos"];
        }

        return Diccionario_Archivos;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Generar_Tabla_Documentos
    /// DESCRIPCION: Genera la tabla de Documentos, el esquema para guardar los tipos de document a recibir
    /// PARAMETROS: 
    /// CREO: Jesus Toledo Rodriguez
    /// FECHA_CREO: 04-may-2012
    /// MODIFICO:
    /// FECHA_MODIFICO:
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private DataTable Generar_Tabla_Documentos()
    {
        DataTable Tabla_Nueva = new DataTable();
        DataColumn Columna0_Clave_Documento;
        DataColumn Columna1_Nombre_Documento;
        DataColumn Columna2_Nombre_Archivo;
        DataColumn Columna3_Ruta_Archivo;
        DataColumn Columna4_Archivo;
        DataColumn Columna5_Checksum;
        DataColumn Columna6;
        DataColumn Columna7;
        DataColumn Columna8;
        DataColumn Columna9;
        DataColumn Columna10;
        DataColumn Columna11;
        DataColumn Columna12;

        try
        {
            // ---------- Inicializar columnas
            Columna0_Clave_Documento = new DataColumn();
            Columna0_Clave_Documento.DataType = System.Type.GetType("System.String");
            Columna0_Clave_Documento.ColumnName = "NO_FACTURA_SERVICIO";
            Tabla_Nueva.Columns.Add(Columna0_Clave_Documento);
            Columna1_Nombre_Documento = new DataColumn();
            Columna1_Nombre_Documento.DataType = System.Type.GetType("System.String");
            Columna1_Nombre_Documento.ColumnName = "NUMERO_FACTURA";
            Tabla_Nueva.Columns.Add(Columna1_Nombre_Documento);
            Columna2_Nombre_Archivo = new DataColumn();
            Columna2_Nombre_Archivo.DataType = System.Type.GetType("System.String");
            Columna2_Nombre_Archivo.ColumnName = "NOMBRE_ARCHIVO";
            Tabla_Nueva.Columns.Add(Columna2_Nombre_Archivo);
            Columna3_Ruta_Archivo = new DataColumn();
            Columna3_Ruta_Archivo.DataType = System.Type.GetType("System.String");
            Columna3_Ruta_Archivo.ColumnName = "RUTA_ARCHIVO";
            Tabla_Nueva.Columns.Add(Columna3_Ruta_Archivo);
            Columna4_Archivo = new DataColumn();
            Columna4_Archivo.DataType = System.Type.GetType("System.String");
            Columna4_Archivo.ColumnName = "ARCHIVO";
            Tabla_Nueva.Columns.Add(Columna4_Archivo);
            Columna5_Checksum = new DataColumn();
            Columna5_Checksum.DataType = System.Type.GetType("System.String");
            Columna5_Checksum.ColumnName = "CHECKSUM";
            Tabla_Nueva.Columns.Add(Columna5_Checksum);
            Columna6 = new DataColumn();
            Columna6.DataType = System.Type.GetType("System.String");
            Columna6.ColumnName = "MONTO_FACTURA";
            Tabla_Nueva.Columns.Add(Columna6);
            Columna7 = new DataColumn();
            Columna7.DataType = System.Type.GetType("System.String");
            Columna7.ColumnName = "COMENTARIOS";
            Tabla_Nueva.Columns.Add(Columna7);
            Columna8 = new DataColumn();
            Columna8.DataType = System.Type.GetType("System.String");
            Columna8.ColumnName = "NO_ASIGNACION_PROV_SERV";
            Tabla_Nueva.Columns.Add(Columna8);
            Columna9 = new DataColumn();
            Columna9.DataType = System.Type.GetType("System.String");
            Columna9.ColumnName = "NO_SERVICIO";
            Tabla_Nueva.Columns.Add(Columna9);
            Columna10 = new DataColumn();
            Columna10.DataType = System.Type.GetType("System.String");
            Columna10.ColumnName = "TIPO_SERVICIO";
            Tabla_Nueva.Columns.Add(Columna10);
            Columna11 = new DataColumn();
            Columna11.DataType = System.Type.GetType("System.String");
            Columna11.ColumnName = "FACTURA";
            Tabla_Nueva.Columns.Add(Columna11);
            Columna12 = new DataColumn();
            Columna12.DataType = System.Type.GetType("System.DateTime");
            Columna12.ColumnName = "FECHA_FACTURA";
            Tabla_Nueva.Columns.Add(Columna12);


            return Tabla_Nueva;
        }
        catch (Exception ex)
        {
            throw new Exception("Generar_Tabla_Documentos " + ex.Message.ToString(), ex);
        }
    }

    protected void Btn_Agregar_Factura_Click(object sender, ImageClickEventArgs e)
    {
        DataTable Tabla_Documentos;//Crea DT para guardar resultado
        if (Session["Tabla_Documentos"] != null) { Tabla_Documentos = (DataTable)Session["Tabla_Documentos"]; }
        else { Tabla_Documentos = Generar_Tabla_Documentos(); }
        String Nombre_Archivo_Subir = "";
        String Checksum_Archivo = "";
        Boolean Cargado = false;
        if (Validar_Archivo())
        {
                if (Session["Nombre_Archivo_Subir"]!=null)
                    Nombre_Archivo_Subir = Session["Nombre_Archivo_Subir"].ToString();
                if (Session["Cargado"] != null)
                    Cargado = (Boolean)Session["Cargado"];
                if (Session["Checksum_Archivo"] != null)
            Checksum_Archivo = Session["Checksum_Archivo"].ToString();
            
            DataRow Nueva_Fila = Tabla_Documentos.NewRow();
            Nueva_Fila["NO_FACTURA_SERVICIO"] = 0;
            Nueva_Fila["NUMERO_FACTURA"] = (Tabla_Documentos.Rows.Count + 1).ToString();
            Nueva_Fila["FACTURA"] = Txt_Factura.Text.Trim();
            Nueva_Fila["FECHA_FACTURA"] = Convert.ToDateTime(Txt_Fecha_Factura.Text.Trim());
            Nueva_Fila["MONTO_FACTURA"] = Txt_Monto.Text.Trim();
            Nueva_Fila["COMENTARIOS"] = Txt_Observaciones.Text.Trim();
            if (!Cargado)
            {
                Nueva_Fila["NOMBRE_ARCHIVO"] = Fup_Factura_Recibida.FileName;
                Nueva_Fila["RUTA_ARCHIVO"] = Nombre_Archivo_Subir;
                Nueva_Fila["CHECKSUM"] = Checksum_Archivo;
            }
            else
            {
                Nueva_Fila["NOMBRE_ARCHIVO"] = "";
                Nueva_Fila["RUTA_ARCHIVO"] = "";
                Nueva_Fila["CHECKSUM"] = null;
            }

            Tabla_Documentos.Rows.Add(Nueva_Fila);

            Session["Tabla_Documentos"] = Tabla_Documentos;
            //Fup_Factura_Recibida.
            Llenar_Listado_Facturas();
            Txt_Observaciones.Text = "";
            Txt_No_Factura.Text = (Tabla_Documentos.Rows.Count + 1).ToString();
            //Txt_Monto.Text = "";
            Txt_Factura.Text = "";
            Txt_Fecha_Factura.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Today);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Servicios
    ///DESCRIPCIÓN: Se llena el Listado de los Servicios para asignar el Mecanico.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Llenar_Listado_Facturas()
    {
        DataTable Dt_Resultados = new DataTable();
        if (Session["Tabla_Documentos"] != null)
        {
            Dt_Resultados = (DataTable)Session["Tabla_Documentos"];
        }
        else
        {
            Dt_Resultados = Generar_Tabla_Documentos();
        }
        //Grid_Facturas.Columns[6].Visible = true;
        Grid_Facturas.DataSource = Dt_Resultados;
        Grid_Facturas.DataBind();
        //Grid_Facturas.Columns[6].Visible = false;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Asignacion
    ///DESCRIPCIÓN: Valida la Solicitud de Servicio antes de ser Asignada a un mecanico
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private Boolean
        Validar_Archivo()
    {
        String Mensaje_Error_ = "";
        Boolean Validacion = true;
        if (String.IsNullOrEmpty(Txt_Factura.Text.Trim()))
        {
            Mensaje_Error_ = Mensaje_Error_ + "+ No se especifico el Folio de Factura.";
            Mensaje_Error_ = Mensaje_Error_ + " <br />";
            Validacion = false;
        }
        if (String.IsNullOrEmpty(Txt_Fecha_Factura.Text.Trim()))
        {
            Mensaje_Error_ = Mensaje_Error_ + "+ No se especifico la Fecha de Factura.";
            Mensaje_Error_ = Mensaje_Error_ + " <br />";
            Validacion = false;
        }
        if (String.IsNullOrEmpty(Txt_No_Factura.Text.Trim()))
        {
            Mensaje_Error_ = Mensaje_Error_ + "+ No se especifico el numero de Factura.";
            Mensaje_Error_ = Mensaje_Error_ + " <br />";
            Validacion = false;
        }
        if (String.IsNullOrEmpty(Txt_Monto.Text.Trim()))
        {
            Mensaje_Error_ = Mensaje_Error_ + "+ Ingresar el Monto de la factura.";
            Mensaje_Error_ = Mensaje_Error_ + " <br />";
            Validacion = false;
        }

        if (!Validacion)
        {
            Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
            Mensaje_Error(Mensaje_Error_);
        }
        return Validacion;
    }
    ///*******************************************************************************************************
    /// 	NOMBRE_FUNCIÓN: Guardar_Archivos
    /// 	DESCRIPCIÓN: Guardar en el servidor los archivos que se hayan recibido
    /// 	PARÁMETROS:
    /// 	CREO: Roberto González Oseguera
    /// 	FECHA_CREO: 10-may-2011
    /// 	MODIFICÓ: 
    /// 	FECHA_MODIFICÓ: 
    /// 	CAUSA_MODIFICACIÓN: 
    ///*******************************************************************************************************
    private void Guardar_Archivos()
    {
        DataTable Tabla_Tramites = (DataTable)Session["Tabla_Documentos"];
        Dictionary<String, Byte[]> Diccionario_Archivos = Obtener_Diccionario_Archivos();
        String Nombre_Directorio;
        String Ruta_Archivo;

        try
        {
            if (Tabla_Tramites != null)     //si la tabla tramites contiene datos
            {
                foreach (DataRow Fila_Tramite in Tabla_Tramites.Rows)   // recorrer la tabla
                {
                    if (!String.IsNullOrEmpty(Fila_Tramite["CHECKSUM"].ToString()))
                    {
                        Nombre_Directorio = MapPath(Path.GetDirectoryName(Fila_Tramite["RUTA_ARCHIVO"].ToString()));
                        Ruta_Archivo = MapPath(HttpUtility.HtmlDecode(Fila_Tramite["RUTA_ARCHIVO"].ToString()));
                        if (!Directory.Exists(Nombre_Directorio))                       //si el directorio no existe, crearlo
                            Directory.CreateDirectory(Nombre_Directorio);
                        //crear filestream y binarywriter para guardar archivo
                        FileStream Escribir_Archivo = new FileStream(Ruta_Archivo, FileMode.Create, FileAccess.Write);
                        BinaryWriter Datos_Archivo = new BinaryWriter(Escribir_Archivo);

                        // Guardar archivo (escribir datos en el filestream)                            
                        Datos_Archivo.Write(Diccionario_Archivos[Fila_Tramite["CHECKSUM"].ToString()]);
                    }
                }
            }

        }
        catch (Exception Ex)
        {
            throw new Exception("Guardar_Archivos " + Ex.Message.ToString(), Ex);
        }
    }
    ///*******************************************************************************************************
    /// 	NOMBRE_FUNCIÓN: Btn_Modificar_Click
    /// 	DESCRIPCIÓN: Guarda las Facturas
    /// 	PARÁMETROS:
    /// 	CREO: Jesus Toledo Rodriguez
    /// 	FECHA_CREO: 11-Jun-2011
    /// 	MODIFICÓ: 
    /// 	FECHA_MODIFICÓ: 
    /// 	CAUSA_MODIFICACIÓN: 
    ///*******************************************************************************************************
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        DataTable Dt_Facturas;
        Boolean Recarga = false;
        if (Grid_Listado_Servicios.Rows.Count > 0)
        {
            Guardar_Faturas();
            Llenar_Listado_Facturas();
            if (Session["Tabla_Documentos"] != null)
            {
                Dt_Facturas = (DataTable)Session["Tabla_Documentos"];
            }
            else
            {
                Dt_Facturas = Generar_Tabla_Documentos();
            }
            foreach (DataRow Dr_Contador in Dt_Facturas.Rows)
            {
                if (!String.IsNullOrEmpty(Dr_Contador["RUTA_ARCHIVO"].ToString()))
                {
                    Recarga = true;
                }
            }
            if (Recarga)
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Verificar Domicilio Foraneo", "EnviarPostbackConParametros(null);", true);
        }
        else
        {
            Mensaje_Error("No se cuentan con facturas");
        }
    }
    ///*******************************************************************************************************
    /// 	NOMBRE_FUNCIÓN: Guardar_Faturas
    /// 	DESCRIPCIÓN: Guarda las Facturas
    /// 	PARÁMETROS:
    /// 	CREO: Jesus Toledo Rodriguez
    /// 	FECHA_CREO: 11-Jun-2011
    /// 	MODIFICÓ: 
    /// 	FECHA_MODIFICÓ: 
    /// 	CAUSA_MODIFICACIÓN: 
    ///*******************************************************************************************************
    private void Guardar_Faturas()
    {
        Cls_Ope_Tal_Facturas_Servicio_Negocio Negocio = new Cls_Ope_Tal_Facturas_Servicio_Negocio();
        DataTable Dt_Facturas = new DataTable();
        if (Session["Tabla_Documentos"] != null)
        {
            Dt_Facturas = (DataTable)Session["Tabla_Documentos"];
        }
        else
        {
            Dt_Facturas = Generar_Tabla_Documentos();
        }
        try
        {
            if (Btn_Modificar.AlternateText.Equals("Guardar"))
            {
                //if (Grid_Facturas.Rows.Count > 0)
                //{
                    Negocio.P_Dt_Archivos = Dt_Facturas;
                    if (!String.IsNullOrEmpty(Hdf_No_Servicio.Value))
                        Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value);
                    if (!String.IsNullOrEmpty(Hdf_No_Asignacion.Value))
                        Negocio.P_No_Asignacion_Proveedor = Convert.ToInt32(Hdf_No_Asignacion.Value);
                    Negocio.P_Tipo = "SERVICIO_PREVENTIVO";
                    if (Cmb_Tipo_Servicio.SelectedItem.Value.Trim().Equals("SERVICIO_CORRECTIVO")) Negocio.P_Tipo = "SERVICIO_CORRECTIVO";
                    Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;

                    Negocio.Alta_Factura();
                    Guardar_Archivos();
                    //Limpiar_Formulario();
                    //Configuracion_Formulario("INICIAL");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Operacion_Captura_Facturas", "alert('Facturas capturadas Exitosamente!')", true);
                //}
                //else
                //{
                //    Mensaje_Error("Agregar facturas");
                //}
            }
            else
            {
                Mensaje_Error("Seleccione Primero un Registro");
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message.ToString());
        }
    }
    ///*******************************************************************************************************
    /// 	NOMBRE_FUNCIÓN: Btn_Generar_Solicitud_Pago_Click
    /// 	DESCRIPCIÓN: Cierra Completamente el Servicio.
    /// 	PARÁMETROS:
    /// 	CREO: Fco Gallardo.
    /// 	FECHA_CREO: 04/Julio/2012
    /// 	MODIFICÓ: 
    /// 	FECHA_MODIFICÓ: 
    /// 	CAUSA_MODIFICACIÓN: 
    ///*******************************************************************************************************
    protected void Btn_Generar_Solicitud_Pago_Click(object sender, ImageClickEventArgs e)
    {
        Cls_Ope_Tal_Facturas_Servicio_Negocio Negocio_Factura = new Cls_Ope_Tal_Facturas_Servicio_Negocio();
        Cls_Ope_Tal_Consultas_Generales_Negocio Cosnultas_Generales_Ng = new Cls_Ope_Tal_Consultas_Generales_Negocio();
        DataTable Dt_Facturas = new DataTable();
        //Transaccion
        SqlConnection Cn = new SqlConnection();
        SqlTransaction Trans = null;
        SqlCommand Cmmd = new SqlCommand();
        String Partida_ID = "0";
        String Proyecto_ID = "0";
        String FF = "0";
        String Dependencia_ID = "0";
        String Tipo_Solicitud_Pago_ID = "0";
        Double Monto_Facturas = 0;
        try
        {
            if (Session["Tabla_Documentos"] != null)
            {
                Dt_Facturas = (DataTable)Session["Tabla_Documentos"];
            }
            else
            {
                Dt_Facturas = Generar_Tabla_Documentos();
            }
            //Inicializacion de Variables de la Transaccion
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Cn;
            Cmmd.Transaction = Trans;
            Negocio_Factura.P_Cmmd = Cmmd;
            //Inicializacion de Negocio
            if (Grid_Facturas.Rows.Count > 0)
            {
                Monto_Facturas = Obtener_Monto_Total_Facturas();
                Negocio_Factura.P_Dt_Archivos = Dt_Facturas;
                if (!String.IsNullOrEmpty(Hdf_No_Servicio.Value))
                    Negocio_Factura.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value);
                if (!String.IsNullOrEmpty(Hdf_No_Asignacion.Value))
                    Negocio_Factura.P_No_Asignacion_Proveedor = Convert.ToInt32(Hdf_No_Asignacion.Value);
                Negocio_Factura.P_Tipo = "SERVICIO_PREVENTIVO";
                if (Cmb_Tipo_Servicio.SelectedItem.Value.Trim().Equals("SERVICIO_CORRECTIVO")) Negocio_Factura.P_Tipo = "SERVICIO_CORRECTIVO";
                Negocio_Factura.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                Negocio_Factura.P_Estatus_Servicio = "SURTIDA_FACTURA";
                Negocio_Factura.P_Cerrar_Servicio = true;
                Negocio_Factura.Alta_Factura();
                Guardar_Archivos();
                //if (Hdf_No_Solicitud.Value.Trim().Length > 0)
                //{
                //    Cls_Ope_Tal_Solicitud_Servicio_Negocio Sol_Neg = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                //    Cls_Tal_Parametros_Negocio Negocio_Parametros = new Cls_Tal_Parametros_Negocio();
                //    Sol_Neg.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                //    Sol_Neg.P_Cmmd = Cmmd;
                //    //Consultamos Valores del Catalogo de Parametros
                //    Negocio_Parametros.P_Cmmd = Cmmd;
                //    Negocio_Parametros = Negocio_Parametros.Consulta_Parametros();
                //    //Se consulta Programa de la solicitud
                //    Cosnultas_Generales_Ng.P_Cmmd = Cmmd;
                //    Cosnultas_Generales_Ng.P_No_Solicitud = Int32.Parse(Hdf_No_Solicitud.Value);
                //    Proyecto_ID = Cosnultas_Generales_Ng.Consultar_Proyecto_Programa_Solicitud();
                //    //Se asignan a las Variables para los datos de la partida
                //    Partida_ID = Negocio_Parametros.P_Partida_ID;
                //    FF = Negocio_Parametros.P_Fuente_Financiamiento_ID;
                //    //Se obtiene La dependencia del objeto de Negocio y no de los parametros
                //    Dependencia_ID = Cmb_Unidad_Responsable.SelectedValue.ToString();
                //    Tipo_Solicitud_Pago_ID = Negocio_Parametros.P_Tipo_Solicitud_Pago_ID;
                //    //Se crea la Tabla pra la Partida
                //    DataTable Dt_Partidas_Reserva = new DataTable();
                //    Dt_Partidas_Reserva = new DataTable("Dt_Partidas_Reserva");
                //    Dt_Partidas_Reserva.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                //    Dt_Partidas_Reserva.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                //    Dt_Partidas_Reserva.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                //    Dt_Partidas_Reserva.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
                //    Dt_Partidas_Reserva.Columns.Add("FECHA_CREO", System.Type.GetType("System.DateTime"));
                //    Dt_Partidas_Reserva.Columns.Add("ANIO", System.Type.GetType("System.String"));
                //    Dt_Partidas_Reserva.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                //    DataRow Dr_Partida = Dt_Partidas_Reserva.NewRow();
                //    Dr_Partida["FUENTE_FINANCIAMIENTO_ID"] = FF;
                //    Dr_Partida["PROGRAMA_ID"] = Proyecto_ID;
                //    Dr_Partida["PARTIDA_ID"] = Partida_ID;
                //    Dr_Partida["FECHA_CREO"] = DateTime.Today;
                //    Dr_Partida["DEPENDENCIA_ID"] = Dependencia_ID;
                //    Dr_Partida["IMPORTE"] = Monto_Facturas;
                //    Dr_Partida["ANIO"] = DateTime.Now.Year.ToString();
                //    Dt_Partidas_Reserva.Rows.Add(Dr_Partida);
                //    Sol_Neg = Sol_Neg.Consultar_Detalles_Solicitud_Servicio();
                //    if (Sol_Neg.P_No_Solicitud > (-1))
                //    {
                //        String No_Solicitud_Pago = Crear_Solicitud_Pago(Sol_Neg.P_No_Reserva,Negocio_Parametros.P_Tipo_Solicitud_Pago_ID, ref Cmmd);
                //        Cls_Ope_Tal_Facturas_Servicio_Negocio Negocio = new Cls_Ope_Tal_Facturas_Servicio_Negocio();
                //        Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value.Trim());
                //        Negocio.P_Tipo = "SERVICIO_PREVENTIVO";
                //        if (Cmb_Tipo_Servicio.SelectedItem.Value.Trim().Equals("SERVICIO_CORRECTIVO")) Negocio.P_Tipo = "SERVICIO_CORRECTIVO";
                //        Negocio.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value.Trim());
                //        Negocio.P_No_Solicitud_Pago = No_Solicitud_Pago;
                //        Negocio.P_Monto_Solicitud_Pago = Obtener_Monto_Total_Facturas();
                //        Negocio = Negocio.Crear_Contrarecibo();
                //        //Realizar movimiento Presupuestal a DEVENGADO
                //        int Registro_Afectado;
                //        int Registro_Movimiento;
                //        //Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud(Numero_Solicitud);
                //        //Limpiamos la Variable Mi_SQL                        
                //        Registro_Afectado = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("DEVENGADO", "COMPROMETIDO",Dt_Partidas_Reserva, ref Cmmd);
                //        Registro_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Sol_Neg.P_No_Reserva.ToString(), "DEVENGADO", "COMPROMETIDO", Monto_Facturas, "", "", "", "", ref Cmmd);
                        Trans.Commit();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Operación Exitosa');", true);
                        //Imprimir(No_Solicitud_Pago.ToString());
                        Limpiar_Formulario();
                        Grid_Listado_Servicios.PageIndex = 0;
                        Llenar_Listado_Servicios();
                        Configuracion_Formulario("INICIAL");
                        Btn_Modificar.Visible = false;
                }
                else
                {
                    Mensaje_Error("- El servicio no contiene Facturas");
                }            
        }
        catch (Exception Ex)
        {
            Trans.Rollback();
            Mensaje_Error("Verificar: " + Ex.Message);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN     : Grid_Facturas_SelectedIndexChanged
    ///DESCRIPCIÓN              : Evento SelectedIndexChanged del Grid de Facturas
    ///PROPIEDADES:
    ///CREO                     : Jesus Toledo Rodriguez
    ///FECHA_CREO               : 12/Junio/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Facturas_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DataTable Tabla_Tramites = Generar_Tabla_Documentos();
            DataRow[] Dr_Seleccionado;
            if (Session["Tabla_Documentos"] != null)
            {
                Tabla_Tramites = (DataTable)Session["Tabla_Documentos"];
                Dr_Seleccionado = Tabla_Tramites.Select("NUMERO_FACTURA = " + Grid_Facturas.SelectedDataKey["NUMERO_FACTURA"].ToString());
                if (Dr_Seleccionado.Length > 0)
                {
                    String Observaciones = Dr_Seleccionado[0]["COMENTARIOS"].ToString().Trim();
                    if (Observaciones.Length > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Observaciones: " + Observaciones + "');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('No hay Observaciones');", true);
                    }

                }
            }
            Grid_Facturas.SelectedIndex = -1;
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Mostrar_Facturas
    /// DESCRIPCION : Mostrar_Facturas
    /// PARAMETROS  : 
    /// CREO        : 
    /// FECHA_CREO  : 
    /// MODIFICO          :
    /// FECHA_MODIFICO    
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************     
    private void Mostrar_Facturas()
    {
        Cls_Ope_Tal_Facturas_Servicio_Negocio Negocio = new Cls_Ope_Tal_Facturas_Servicio_Negocio();
        DataTable Dt_Facturas = Generar_Tabla_Documentos();
        try
        {
            Negocio.P_No_Asignacion_Proveedor = Convert.ToInt32(Hdf_No_Asignacion.Value);
            Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value);
            Negocio.P_Tipo = "SERVICIO_PREVENTIVO";
            if (Cmb_Tipo_Servicio.SelectedItem.Value.Trim().Equals("SERVICIO_CORRECTIVO")) Negocio.P_Tipo = "SERVICIO_CORRECTIVO";
            Negocio.P_Campos_Dinamicos = Ope_Tal_Facturas_Servicio.Campo_Comentarios;
            Negocio.P_Campos_Dinamicos += "," + Ope_Tal_Facturas_Servicio.Campo_Comentarios;
            Negocio.P_Campos_Dinamicos += "," + Ope_Tal_Facturas_Servicio.Campo_Monto_Factura;
            Negocio.P_Campos_Dinamicos += "," + Ope_Tal_Facturas_Servicio.Campo_No_Asignacion;
            Negocio.P_Campos_Dinamicos += "," + Ope_Tal_Facturas_Servicio.Campo_No_Factura;
            Negocio.P_Campos_Dinamicos += "," + Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio;
            Negocio.P_Campos_Dinamicos += "," + Ope_Tal_Facturas_Servicio.Campo_No_Servicio;
            Negocio.P_Campos_Dinamicos += "," + Ope_Tal_Facturas_Servicio.Campo_Nombre_Archivo;
            Negocio.P_Campos_Dinamicos += "," + Ope_Tal_Facturas_Servicio.Campo_Ruta_Archivo;
            Negocio.P_Campos_Dinamicos += "," + Ope_Tal_Facturas_Servicio.Campo_Tipo_Servicio;
            Negocio.P_Campos_Dinamicos += ",'' AS CHECKSUM";
            Negocio.P_Campos_Dinamicos += "," + Ope_Tal_Facturas_Servicio.Campo_Factura;
            Negocio.P_Campos_Dinamicos += "," + Ope_Tal_Facturas_Servicio.Campo_Fecha_Factura;
            Dt_Facturas = Negocio.Consulta_Factura();
            Grid_Archivo = true;
            Session["Tabla_Documentos"] = Dt_Facturas;
            foreach (DataRow Dr_Renglon in Dt_Facturas.Rows)
            {
                Txt_No_Factura.Text = Dr_Renglon[Ope_Tal_Facturas_Servicio.Campo_No_Factura].ToString();
                Txt_No_Factura.Text = (Convert.ToInt32(Txt_No_Factura.Text) + 1).ToString();
            }
            Llenar_Listado_Facturas();
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Sorting
    ///DESCRIPCIÓN: se obtienen los datos del movimiento
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 07/26/2011 06:22:19 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Grid_Listado_Servicios_Sorting(object sender, GridViewSortEventArgs e)
    {
        String Orden = "";
        //Tabla que contendra la Lista de los servicios
        DataTable Dt_Listado = null;
        if (Session["Frm_Ope_Tal_Entrada_Facturas_Refacciones_Listado"] != null)
            Dt_Listado = (DataTable)Session["Frm_Ope_Tal_Entrada_Facturas_Refacciones_Listado"];
        //Si hay Datos
        if (Dt_Listado != null)
        {
            if (Dt_Listado.Rows.Count > 0)
            {
                //Se realiza el Ordenamiento segun el campo seleccionado en nuestro Grid
                //Se Define la Vista donde quedara el ordenamiento
                DataView Dv_Listado = new DataView(Dt_Listado);
                if (ViewState["SortDirection"] != null)
                    Orden = ViewState["SortDirection"].ToString();

                if (Orden.Equals("ASC"))
                {
                    Dv_Listado.Sort = e.SortExpression + " " + "DESC";
                    ViewState["SortDirection"] = "DESC";
                }
                else
                {
                    Dv_Listado.Sort = e.SortExpression + " " + "ASC";
                    ViewState["SortDirection"] = "ASC";
                }
                //Asignar Vista al Origen de Datos de nuestro Grid
                Grid_Listado_Servicios.DataSource = Dv_Listado;
                Grid_Listado_Servicios.DataBind();
                Session["Frm_Ope_Tal_Entrada_Facturas_Refacciones_Listado_Vista"] = Dv_Listado;
            }
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN     : Grid_Facturas_RowDeleting
    ///DESCRIPCIÓN              : Evento RowDeleting del Grid de Facturas
    ///PROPIEDADES:
    ///CREO                     : Jesus Toledo Rodriguez
    ///FECHA_CREO               : 12/Junio/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Facturas_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            DataTable Tabla_Tramites = Generar_Tabla_Documentos();
            DataRow[] Dr_Seleccionado;
            int Grid_Id = Convert.ToInt32(e.RowIndex);
            if (Session["Tabla_Documentos"] != null)
            {
                Tabla_Tramites = (DataTable)Session["Tabla_Documentos"];
                Dr_Seleccionado = Tabla_Tramites.Select("NUMERO_FACTURA = " + Grid_Facturas.DataKeys[Grid_Id]["NUMERO_FACTURA"].ToString());
                if (Dr_Seleccionado.Length > 0)
                {
                    Tabla_Tramites.Rows.Remove(Dr_Seleccionado[0]);
                    Tabla_Tramites.AcceptChanges();
                }
            }
            Session["Tabla_Documentos"] = Tabla_Tramites;
            Llenar_Listado_Facturas();
            Grid_Facturas.SelectedIndex = (-1);
            //Txt_Monto.Text = "";
            Txt_Observaciones.Text = "";
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Crear_Solicitud_Pago
    /// DESCRIPCION : Da de Alta la Solicitud del Pago con los datos proporcionados por 
    ///               el usuario
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 24-Abril-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************      
    private String Crear_Solicitud_Pago(Double Reserva,String P_Tipo_Solicitud_Pago_ID, ref SqlCommand P_Cmd)
    {
        Cls_Tal_Parametros_Negocio Parametros_Neg = new Cls_Tal_Parametros_Negocio();
        
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Alta_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de negocio
        //Cls_Ope_Con_Reservas_Negocio Reserva_Negocio = new Cls_Ope_Con_Reservas_Negocio();
        //Cls_Ope_Tal_Contrarecibo_Negocio Rt_Orden_Compra = new Cls_Ope_Tal_Contrarecibo_Negocio();
        //Cls_Cat_Com_Proveedores_Negocio Rt_Proveedor = new Cls_Cat_Com_Proveedores_Negocio();
        DataTable Dt_Orden_Compra = new DataTable();
        DataTable Dt_Doc_Contrarecibo = new DataTable();
        DataTable Dt_Reserva = new DataTable();
        DataTable Dt_Partidas = new DataTable();
        String Numero_Solicitud;
        String Monto = "";
        String Fuente_Financiamiento = "";
        String Programa = "";
        String Partida = "";
        String Proveedor = "";
        String RFC = "";
        DataTable Dt_Documentos = new DataTable();
        DataTable Dt_Proveedor = new DataTable();

        try
        {
            if (Dt_Documentos.Rows.Count <= 0)
            {
                //Agrega los campos que va a contener el DataTable
                Dt_Documentos.Columns.Add("No_Documento", typeof(System.String));
                Dt_Documentos.Columns.Add("Fecha_Documento", typeof(System.String));
                Dt_Documentos.Columns.Add("Monto", typeof(System.String));
                Dt_Documentos.Columns.Add("Partida", typeof(System.String));
                Dt_Documentos.Columns.Add("Partida_Id", typeof(System.String));
                Dt_Documentos.Columns.Add("Fte_Financiamiento_Id", typeof(System.String));
                Dt_Documentos.Columns.Add("Proyecto_Programa_Id", typeof(System.String));
                Dt_Documentos.Columns.Add("Iva", typeof(System.String));
                Dt_Documentos.Columns.Add("IEPS", typeof(System.Double));
                Dt_Documentos.Columns.Add("Archivo", typeof(System.String));
                Dt_Documentos.Columns.Add("Ruta", typeof(System.String));
                Dt_Documentos.Columns.Add("Nombre_Proveedor_Fact", typeof(System.String));
                Dt_Documentos.Columns.Add("RFC", typeof(System.String));
                Dt_Documentos.Columns.Add("CURP", typeof(System.String));
                Dt_Documentos.Columns.Add("Operacion", typeof(System.String));
                Dt_Documentos.Columns.Add("Retencion_ISR", typeof(System.Double));
                Dt_Documentos.Columns.Add("Retencion_IVA", typeof(System.Double));
                Dt_Documentos.Columns.Add("Retencion_Celula", typeof(System.Double));
                Dt_Documentos.Columns.Add("ISH", typeof(System.Double));
            }
            DataTable Dt_Facturas = new DataTable();
            if (Session["Tabla_Documentos"] != null) { Dt_Facturas = (DataTable)Session["Tabla_Documentos"]; }
            Monto = Obtener_Monto_Total_Facturas().ToString();
            if (Session["Tabla_Documentos"] != null) { Cargar_Documentos(ref Dt_Documentos, (DataTable)Session["Tabla_Documentos"],ref P_Cmd); }
            //Parametros_Neg = Parametros_Neg.Consulta_Parametros();
            //Reserva_Negocio.P_No_Reserva = Convert.ToString(Reserva);
            //Dt_Reserva = Reserva_Negocio.Consultar_Reservas_Detalladas();
            //if (Dt_Reserva.Rows.Count > 0)
            //{
            //    Fuente_Financiamiento = Dt_Reserva.Rows[0]["FUENTE_FINANCIAMIENTO_ID"].ToString();
            //    Programa = Dt_Reserva.Rows[0]["PROGRAMA_ID"].ToString();
            //    Partida = Dt_Reserva.Rows[0]["PARTIDA_ID"].ToString();
            //}
            //Reserva_Negocio.P_No_Reserva = Convert.ToString(Reserva);
            //Dt_Reserva = Reserva_Negocio.Consultar_Reservas();
            //if (Dt_Reserva.Rows.Count > 0)
            //{
            //    Rs_Alta_Ope_Con_Solicitud_Pagos.P_Proveedor_ID = Dt_Reserva.Rows[0]["PROVEEDOR_ID"].ToString();
            //    Rs_Alta_Ope_Con_Solicitud_Pagos.P_Beneficiario = "P-" + Dt_Reserva.Rows[0]["BENEFICIARIO"].ToString();
            //    Rt_Proveedor.P_Proveedor_ID = Dt_Reserva.Rows[0]["PROVEEDOR_ID"].ToString();
            //    Dt_Proveedor = Rt_Proveedor.Consulta_Datos_Proveedores();
            //    if (Dt_Proveedor.Rows.Count > 0)
            //    {
            //        Proveedor = Dt_Proveedor.Rows[0]["COMPANIA"].ToString();
            //        RFC = Dt_Proveedor.Rows[0]["RFC"].ToString();
            //    }
            //}
            Rs_Alta_Ope_Con_Solicitud_Pagos.P_Cmmd = P_Cmd;
            Rs_Alta_Ope_Con_Solicitud_Pagos.P_Concepto = "SERVICIO DE REPARACIÓN DE VEHICULO";
            Rs_Alta_Ope_Con_Solicitud_Pagos.P_Tipo_Solicitud_Pago_ID = P_Tipo_Solicitud_Pago_ID;
            Rs_Alta_Ope_Con_Solicitud_Pagos.P_No_Reserva = Convert.ToDouble(Reserva);
            Rs_Alta_Ope_Con_Solicitud_Pagos.P_Estatus = "PRE-PREAUTORIZADO";
            Rs_Alta_Ope_Con_Solicitud_Pagos.P_Tipo_Documento = "DOCUMENTO";
            Rs_Alta_Ope_Con_Solicitud_Pagos.P_Monto = Convert.ToDecimal(Monto);
            Rs_Alta_Ope_Con_Solicitud_Pagos.P_Dt_Detalles_Solicitud = Dt_Documentos;
            Rs_Alta_Ope_Con_Solicitud_Pagos.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
            Numero_Solicitud = Rs_Alta_Ope_Con_Solicitud_Pagos.Alta_Solicitud_Pago_Sin_Poliza();
        }
        catch (Exception ex)
        {
            throw new Exception("Alta_Solicitud_Pagos " + ex.Message.ToString(), ex);
        }
        return Numero_Solicitud;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Obtener_Monto_Total_Facturas
    /// DESCRIPCION : Obtener_Monto_Total_Facturas
    /// PARAMETROS  : 
    /// CREO        : 
    /// FECHA_CREO  : 
    /// MODIFICO          :
    /// FECHA_MODIFICO    
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************     
    private Double Obtener_Monto_Total_Facturas()
    {
        Double Monto = 0;
        DataTable Dt_Facturas = new DataTable();
        if (Session["Tabla_Documentos"] != null) { Dt_Facturas = (DataTable)Session["Tabla_Documentos"]; }

        foreach (DataRow Fila in Dt_Facturas.Rows)
        {
            if (!String.IsNullOrEmpty(Fila["MONTO_FACTURA"].ToString()))
            {
                Monto = Monto + Convert.ToDouble(Fila["MONTO_FACTURA"]);
            }
        }
        return Monto;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cargar_Documentos
    /// DESCRIPCION : Cargar_Documentos
    /// PARAMETROS  : Dt_Documentos, Dt_Datos, P_Cmd
    /// CREO        : Jesus Toledo Rodriguez
    /// FECHA_CREO  : 
    /// MODIFICO          :
    /// FECHA_MODIFICO    
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************  
    private void Cargar_Documentos(ref DataTable Dt_Documentos, DataTable Dt_Datos, ref SqlCommand P_Cmd)
    {
        String Proyecto_ID = "";
        Cls_Tal_Parametros_Negocio Parametros_Neg = new Cls_Tal_Parametros_Negocio();
        Cls_Ope_Tal_Consultas_Generales_Negocio Cosnultas_Generales_Ng = new Cls_Ope_Tal_Consultas_Generales_Negocio();
        Parametros_Neg.P_Cmmd = P_Cmd;
        Parametros_Neg = Parametros_Neg.Consulta_Parametros();
        //Se consulta Programa de la solicitud
        Cosnultas_Generales_Ng.P_Cmmd = P_Cmd;
        Cosnultas_Generales_Ng.P_No_Solicitud = Int32.Parse(Hdf_No_Solicitud.Value);
        Proyecto_ID = Cosnultas_Generales_Ng.Consultar_Proyecto_Programa_Solicitud();
        Cls_Cat_Com_Proveedores_Negocio Proveedores_Neg = new Cls_Cat_Com_Proveedores_Negocio();
        Proveedores_Neg.P_Proveedor_ID = Hdf_Proveedor_ID.Value;
        DataTable Dt_Proveedor = Proveedores_Neg.Consulta_Datos_Proveedores();
        if (Dt_Documentos != null && Dt_Datos != null)
        {
            if (Dt_Datos.Rows.Count > 0)
            {
                foreach (DataRow Dr_ in Dt_Datos.Rows)
                {
                    DataRow Fila_Nueva = Dt_Documentos.NewRow();
                    Fila_Nueva["No_Documento"] = "1";
                    Fila_Nueva["Fecha_Documento"] = String.Format("{0:dd/MM/yyyy}", Dr_["FECHA_FACTURA"]);
                    Fila_Nueva["Monto"] = Dr_["MONTO_FACTURA"].ToString();
                    Fila_Nueva["Partida"] = "";
                    Fila_Nueva["Partida_Id"] = Parametros_Neg.P_Partida_ID;
                    Fila_Nueva["Fte_Financiamiento_Id"] = Parametros_Neg.P_Fuente_Financiamiento_ID;
                    Fila_Nueva["Proyecto_Programa_Id"] = Proyecto_ID;
                    Fila_Nueva["Iva"] = (Convert.ToDouble(Dr_["MONTO_FACTURA"]) * 0.16).ToString();
                    Fila_Nueva["IEPS"] = "0";
                    Fila_Nueva["Archivo"] = "";
                    Fila_Nueva["Ruta"] = "";
                    Fila_Nueva["Nombre_Proveedor_Fact"] = Txt_Nombre_Proveedor.Text;
                    Fila_Nueva["RFC"] = ((Dt_Proveedor.Rows.Count > 0) ? Dt_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_RFC].ToString() : "");
                    Fila_Nueva["CURP"] = "";// ((Dt_Proveedor.Rows.Count > 0) ? Dt_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_CURP].ToString() : "");
                    Fila_Nueva["Operacion"] = "Otros";
                    Fila_Nueva["Retencion_ISR"] = 0;
                    Fila_Nueva["Retencion_IVA"] = 0;
                    Fila_Nueva["Retencion_Celula"] = 0;
                    Fila_Nueva["ISH"] = 0;
                    Dt_Documentos.Rows.Add(Fila_Nueva);
                }
            }
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Fup_Factura_Recibida_UploadedComplete
    /// DESCRIPCION : Evento Complete del File Upload
    /// PARAMETROS  : 
    /// CREO        : Jesus Toledo Rodriguez
    /// FECHA_CREO  : 
    /// MODIFICO          :
    /// FECHA_MODIFICO    
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Fup_Factura_Recibida_UploadedComplete(object sender, AsyncFileUploadEventArgs e)
    {
        String Nombre_Archivo_Subir = null;
        String Checksum_Archivo = null;
        Boolean Cargado = false;
        if (Fup_Factura_Recibida.HasFile)
        {
            HashAlgorithm sha = HashAlgorithm.Create("SHA1");
            Checksum_Archivo = BitConverter.ToString(sha.ComputeHash(Fup_Factura_Recibida.FileBytes));       //obtener checksum del archivo
            Dictionary<String, Byte[]> Diccionario_Archivos = Obtener_Diccionario_Archivos();   //obtener diccionario checksum-archivo
            String Extension_Archivo = Path.GetExtension(Fup_Factura_Recibida.FileName).ToLower();//Aqui obtiene el Nombre del Archivo CAMBIAR POR FUP DE MI FORM
            String[] Extensiones_Permitidas = { ".jpg", ".jpeg", ".png", ".gif", ".doc", ".docx", ".ppt", ".pptx", ".pdf" };//Esto esta bien

            if (Array.IndexOf(Extensiones_Permitidas, Extension_Archivo) < 0)
            {
                Mensaje_Error(" No se permite subir archivos con extensión: " + Extension_Archivo);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Error_Extension_Archivo", "top.$get(\"" +
                Fup_Factura_Recibida.ClientID + "\").style.background-color = 'red!important';", true);
                return;
            }

            if (Fup_Factura_Recibida.FileBytes.Length > 2048000) // si la longitud del archivo recibido es mayor que 2MB, mostrar mensaje
            {
                Mensaje_Error(" El tamaño del archivo excede el limite permitido: " + Fup_Factura_Recibida.FileName);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Error_Tamanio_Archivo", "top.$get(\"" +
                Fup_Factura_Recibida.ClientID + "\").style.background-color = 'red!important';", true);
                return;
            }
            if (!Diccionario_Archivos.ContainsKey(Checksum_Archivo)) //si el checksum no esta en el diccionario, agregarlo y guardar en variable de sesion
            {
                Diccionario_Archivos.Add(Checksum_Archivo, Fup_Factura_Recibida.FileBytes);
                Session["Diccionario_Archivos"] = Diccionario_Archivos;
            }
            else
            {
                Cargado = true;
            }

            Nombre_Archivo_Subir = @"../../ARCHIVOS_TALLER_MUNICIPAL/FACTURAS_SERVICIOS/" + Cmb_Tipo_Servicio.SelectedValue.ToLower() + @"/Servicio_No_" + Hdf_No_Servicio.Value + @"/Factura_No_" + Txt_No_Factura.Text + @"/" + Fup_Factura_Recibida.FileName;
            Session["Nombre_Archivo_Subir"] = Nombre_Archivo_Subir;
            Session["Cargado"] = Cargado;
            Session["Checksum_Archivo"] = Checksum_Archivo;
        }
    }
    protected void Grid_Facturas_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        
    }
    protected void Grid_Facturas_DataBound(object sender, EventArgs e)
    {
        double Monto = 0.0;
        for (Int32 Contador = 0; Contador < Grid_Facturas.Rows.Count; Contador++)
        {
            if (Grid_Facturas.Rows[Contador].Cells[5].Text != null)
            {                
                Monto += Convert.ToDouble(Grid_Facturas.Rows[Contador].Cells[5].Text.Replace("$", ""));
                Txt_Monto.Text = (Convert.ToDouble(Txt_Costo_Unitario.Text) - Monto).ToString();
            }
        }
        if (Grid_Facturas.Rows.Count <= 0) { Txt_Monto.Text = Txt_Costo_Unitario.Text; }
    }
}