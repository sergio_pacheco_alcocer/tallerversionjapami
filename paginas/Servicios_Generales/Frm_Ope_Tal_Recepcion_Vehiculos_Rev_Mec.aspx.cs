﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Entradas_Vehiculos.Negocio;
using JAPAMI.Taller_Mecanico.Catalogo_Partes_Vehiculos.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Control_Patrimonial_Operacion_Vehiculos.Negocio;
using JAPAMI.Empleados.Negocios;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.IO;

public partial class paginas_Taller_Mecanico_Frm_Ope_Tal_Recepcion_Vehiculos_Rev_Mec : System.Web.UI.Page {

    #region Page_Load
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN: Carga la Pagina Inicial
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 07/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e) {
            Cargar_Imagenes();
            Div_Contenedor_Msj_Error.Visible = false;
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");            
            if (!IsPostBack) {
                Llenar_Combo_Unidades_Responsables();
                Grid_Listado_Solicitudes.PageIndex = 0;
                Llenar_Listado_Solicitudes();
                Configuracion_Formulario("INICIAL");
                Cmb_Unidad_Responsable.Enabled = false;
                Cmb_Busqueda_Dependencia.Enabled = false;                
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Cargar_Imagenes
        ///DESCRIPCIÓN: Carga las imagenes a mostrar
        ///PROPIEDADES:     
        ///CREO: Jesus Toledo Rdz.
        ///FECHA_CREO: 12/Julio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Cargar_Imagenes()
        {
            DataTable Img;
            if (Session["Tabla_imagenes_Entrada"] != null)
            {
                Img = (DataTable)Session["Tabla_imagenes_Entrada"];
                foreach (DataRow Renglon in Img.Rows)
                {
                    if (Renglon["RUTA_ARCHIVO"] != null)
                    {
                        switch (Renglon["FOTO"].ToString())
                        {
                            case "FRENTE":
                                if (!String.IsNullOrEmpty(Renglon["RUTA_ARCHIVO"].ToString()))
                                {
                                    Liga_Frente.HRef = Renglon["RUTA_ARCHIVO"].ToString();
                                    Liga_Imagen_Frente.Src = Renglon["RUTA_ARCHIVO"].ToString();
                                }
                                else
                                {
                                    Liga_Frente.HRef = "../imagenes/paginas/vehicle_front_view.png";
                                    Liga_Imagen_Frente.Src = "../imagenes/paginas/vehicle_front_view.png";
                                }
                                break;
                            case "ATRAS":
                                if (!String.IsNullOrEmpty(Renglon["RUTA_ARCHIVO"].ToString()))
                                {
                                    Liga_Atras.HRef = Renglon["RUTA_ARCHIVO"].ToString();
                                    Liga_Imagen_Atras.Src = Renglon["RUTA_ARCHIVO"].ToString();
                                }
                                else
                                {
                                    Liga_Atras.HRef = "../imagenes/paginas/vehicle_back_view.png";
                                    Liga_Imagen_Atras.Src = "../imagenes/paginas/vehicle_back_view.png";
                                }
                                break;
                            case "IZQUIERDA":
                                if (!String.IsNullOrEmpty(Renglon["RUTA_ARCHIVO"].ToString()))
                                {
                                    Liga_Izquierdo.HRef = Renglon["RUTA_ARCHIVO"].ToString();
                                    Liga_Imagen_Izquierdo.Src = Renglon["RUTA_ARCHIVO"].ToString();
                                }
                                else
                                {
                                    Liga_Izquierdo.HRef = "../imagenes/paginas/vehicle_left_view.png";
                                    Liga_Imagen_Izquierdo.Src = "../imagenes/paginas/vehicle_left_view.png";
                                }
                                break;
                            case "DERECHA":
                                if (!String.IsNullOrEmpty(Renglon["RUTA_ARCHIVO"].ToString()))
                                {
                                    Liga_Derecho.HRef = Renglon["RUTA_ARCHIVO"].ToString();
                                    Liga_Imagen_Derecho.Src = Renglon["RUTA_ARCHIVO"].ToString();
                                }
                                else
                                {
                                    Liga_Derecho.HRef = "../imagenes/paginas/vehicle_rigth_view.png";
                                    Liga_Imagen_Derecho.Src = "../imagenes/paginas/vehicle_rigth_view.png";
                                }
                                break;

                        }
                    }
                }
            }
            else
            {
                Liga_Frente.HRef = "../imagenes/paginas/vehicle_front_view.png";
                Liga_Imagen_Frente.Src = "../imagenes/paginas/vehicle_front_view.png";
                Liga_Atras.HRef = "../imagenes/paginas/vehicle_back_view.png";
                Liga_Imagen_Atras.Src = "../imagenes/paginas/vehicle_back_view.png";
                Liga_Izquierdo.HRef = "../imagenes/paginas/vehicle_left_view.png";
                Liga_Imagen_Izquierdo.Src = "../imagenes/paginas/vehicle_left_view.png";
                Liga_Derecho.HRef = "../imagenes/paginas/vehicle_rigth_view.png";
                Liga_Imagen_Derecho.Src = "../imagenes/paginas/vehicle_rigth_view.png";
            }
        }

    #endregion

    #region Metodos

        #region Llenado de Campos [Combos, Listados, Vehiculos]

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Unidades_Responsables
            ///DESCRIPCIÓN: Se llena el Combo de las Unidades Responsables.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Combo_Unidades_Responsables() {
                Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                Negocio.P_Estatus = "ACTIVO";
                DataTable Dt_Dependencias = Negocio.Consultar_Unidades_Responsables();
                Cmb_Unidad_Responsable.DataSource = Dt_Dependencias;
                Cmb_Unidad_Responsable.DataTextField = "CLAVE_NOMBRE";
                Cmb_Unidad_Responsable.DataValueField = "DEPENDENCIA_ID";
                Cmb_Unidad_Responsable.DataBind();
                Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
                Cmb_Busqueda_Dependencia.DataSource = Dt_Dependencias;
                Cmb_Busqueda_Dependencia.DataTextField = "CLAVE_NOMBRE";
                Cmb_Busqueda_Dependencia.DataValueField = "DEPENDENCIA_ID";
                Cmb_Busqueda_Dependencia.DataBind();
                Cmb_Busqueda_Dependencia.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Solicitudes
            ///DESCRIPCIÓN: Se llena el Listado de las Solicitudes.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Listado_Solicitudes() {
                Cls_Ope_Tal_Solicitud_Servicio_Negocio Negocio = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                Negocio.P_Tipo_Servicio = "REVISTA_MECANICA";
                Negocio.P_Estatus = "AUTORIZADA";
                Negocio.P_Procedencia = "SOLICITUD";
                DataTable Dt_Resultados = Negocio.Consultar_Listado_Solicitudes_Servicio();
                Grid_Listado_Solicitudes.Columns[1].Visible = true;
                Grid_Listado_Solicitudes.DataSource = Dt_Resultados;
                Grid_Listado_Solicitudes.DataBind();
                Grid_Listado_Solicitudes.Columns[1].Visible = false;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Vehiculo
            ///DESCRIPCIÓN: Se cargan los Datos del Vehiculo Seleccionado.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Cargar_Datos_Vehiculo(String Vehiculo, String Tipo_Busqueda) {
                Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                switch (Tipo_Busqueda) { 
                    case "NO_INVENTARIO":
                        Consulta_Negocio.P_No_Inventario = Vehiculo;
                        break;
                    case "IDENTIFICADOR":
                        Consulta_Negocio.P_Vehiculo_ID = Vehiculo;
                        break;
                    default: break;
                }
                if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Consulta_Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value; }
                DataTable Dt_Vehiculo = Consulta_Negocio.Consultar_Vehiculos();
                if (Dt_Vehiculo.Rows.Count > 0) {
                    Hdf_Vehiculo_ID.Value = Dt_Vehiculo.Rows[0]["VEHICULO_ID"].ToString().Trim();
                    Txt_No_Inventario.Text = Dt_Vehiculo.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                    Txt_No_Economico.Text = Dt_Vehiculo.Rows[0]["NO_ECONOMICO"].ToString().Trim();
                    Txt_Datos_Vehiculo.Text = Dt_Vehiculo.Rows[0]["VEHICULO_DESCRIPCION"].ToString().Trim();
                    Txt_Placas.Text = Dt_Vehiculo.Rows[0]["PLACAS"].ToString().Trim();
                    Txt_Anio.Text = Dt_Vehiculo.Rows[0]["ANIO"].ToString().Trim();
                    Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString()));
                } else {
                    Lbl_Ecabezado_Mensaje.Text = "El Vehículo no se encontro.";
                    if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Lbl_Mensaje_Error.Text = "[Puede ser que no este asignado a la Unidad Responsable Seleccionada]."; }
                    else { Lbl_Mensaje_Error.Text = ""; }
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Busqueda_Empleados_Resguardo
            ///DESCRIPCIÓN: Llena el Grid con los empleados que cumplan el filtro
            ///PROPIEDADES:     
            ///CREO:                 
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 09/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private void Llenar_Grid_Busqueda_Empleados_Resguardo() {
                Grid_Busqueda_Empleados_Resguardo.SelectedIndex = (-1);
                Grid_Busqueda_Empleados_Resguardo.Columns[1].Visible = true;
                Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                if (Txt_Busqueda_No_Empleado.Text.Trim().Length > 0) { Negocio.P_No_Empleado = Txt_Busqueda_No_Empleado.Text.Trim(); }
                if (Txt_Busqueda_RFC.Text.Trim().Length > 0) { Negocio.P_RFC_Empleado = Txt_Busqueda_RFC.Text.Trim(); }
                if (Txt_Busqueda_Nombre_Empleado.Text.Trim().Length > 0) { Negocio.P_Nombre_Empleado = Txt_Busqueda_Nombre_Empleado.Text.Trim().ToUpper(); }
                if (Cmb_Busqueda_Dependencia.SelectedIndex > 0) { Negocio.P_Dependencia_ID = Cmb_Busqueda_Dependencia.SelectedItem.Value; }
                Grid_Busqueda_Empleados_Resguardo.DataSource = Negocio.Consultar_Empleados();
                Grid_Busqueda_Empleados_Resguardo.DataBind();
                Grid_Busqueda_Empleados_Resguardo.Columns[1].Visible = false;
            }

        #endregion

        #region Generales [Configuracion, Limpiar]
                
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
            ///DESCRIPCIÓN: Limpia los campos del Formulario.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Limpiar_Formulario() {
                Hdf_No_Solicitud.Value = "";
                Txt_Folio.Text = "";
                Txt_Fecha_Elaboracion.Text = "";
                Cmb_Tipo_Servicio.SelectedIndex = 0;
                Cmb_Unidad_Responsable.SelectedIndex = 0;
                Txt_Km_Solicitud.Text = "";
                Txt_Descripcion_Servicio.Text = "";
                Hdf_Vehiculo_ID.Value = "";
                Txt_No_Inventario.Text = "";
                Txt_No_Economico.Text = "";
                Txt_Datos_Vehiculo.Text = "";
                Txt_Placas.Text = "";
                Txt_Anio.Text = "";
                Txt_Fecha_Recepcion_Programada.Text = "";
                Txt_Fecha_Recepcion_Real.Text = "";
                Hdf_Empleado_Entrega_ID.Value = "";
                Txt_Empleado_Entrega.Text = "";
                Txt_Kilometraje.Text = "";
                Txt_Comentarios_Recepción.Text = "";                
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
            ///DESCRIPCIÓN: Se Habilitan y/o inhabilitan los campos dependendiendo de la Condicion.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Configuracion_Formulario(String Operacion) {
                switch (Operacion) {
                    case "INICIAL":
                        Div_Campos.Visible = false;
                        Div_Listado_Solicitudes.Visible = true;
                        Btn_Hacer_Entrada.Visible = false;
                        //Pnl_Fotos_Vehiculo.Visible = false;
                        Tbl_Fotos_Vehiculo.Style.Add("display", "none");
                        break;
                    case "OPERACION":
                        Div_Campos.Visible = true;
                        Div_Listado_Solicitudes.Visible = false;
                        Btn_Hacer_Entrada.Visible = true;
                        //Pnl_Fotos_Vehiculo.Visible = true;
                        Tbl_Fotos_Vehiculo.Style.Add("display", "inline");
                        break;
                }
            }

        #endregion

        #region Clase de Negocio de Solicitudes [Registrar y Consulta]

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Mostrar_Registro
            ///DESCRIPCIÓN: Muestra el Registro en los campos.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 28/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Mostrar_Registro() {
                Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                Solicitud.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                Solicitud = Solicitud.Consultar_Detalles_Solicitud_Servicio();
                if (Solicitud.P_No_Solicitud > (-1)) {
                    Txt_Folio.Text = Solicitud.P_Folio_Solicitud.Trim();
                    Txt_Fecha_Elaboracion.Text = String.Format("{0:dd/MMM/yyyy}", Solicitud.P_Fecha_Elaboracion);
                    Cmb_Tipo_Servicio.SelectedIndex = Cmb_Tipo_Servicio.Items.IndexOf(Cmb_Tipo_Servicio.Items.FindByValue(Solicitud.P_Tipo_Servicio));
                    Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Solicitud.P_Dependencia_ID));
                    Cmb_Busqueda_Dependencia.SelectedIndex = Cmb_Busqueda_Dependencia.Items.IndexOf(Cmb_Busqueda_Dependencia.Items.FindByValue(Solicitud.P_Dependencia_ID));
                    Hdf_Vehiculo_ID.Value = Solicitud.P_Bien_ID;
                    Txt_Descripcion_Servicio.Text = Solicitud.P_Descripcion_Servicio;
                    if (Solicitud.P_Kilometraje > (-1.0)) {
                        Txt_Km_Solicitud.Text = String.Format("{0:############0.00}", Solicitud.P_Kilometraje);
                    }
                    Cargar_Datos_Vehiculo(Hdf_Vehiculo_ID.Value, "IDENTIFICADOR");
                    Txt_Fecha_Recepcion_Programada.Text = String.Format("{0:dd/MMM/yyyy}", Solicitud.P_Fecha_Recepcion_Programada);
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Mostrar_Informacion_Empleado
            ///DESCRIPCIÓN: Muestra los Generales del Empleado
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 28/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Mostrar_Informacion_Empleado(String Empleado_ID) {
                Hdf_Empleado_Entrega_ID.Value = "";
                Txt_Empleado_Entrega.Text = "";
                Cls_Cat_Empleados_Negocios Empleado_Negocio = new Cls_Cat_Empleados_Negocios();
                Empleado_Negocio.P_Empleado_ID = Empleado_ID;
                DataTable Dt_Datos_Empleado = Empleado_Negocio.Consulta_Empleados_General();
                Hdf_Empleado_Entrega_ID.Value = ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString().Trim() : null);
                Txt_Empleado_Entrega.Text += ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString().Trim() : null);
                Txt_Empleado_Entrega.Text += " - " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString().Trim() : null);
                Txt_Empleado_Entrega.Text = Txt_Empleado_Entrega.Text.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString().Trim() : null);
                Txt_Empleado_Entrega.Text = Txt_Empleado_Entrega.Text.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString().Trim() : null);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Registrar_Recepcion_Registro
            ///DESCRIPCIÓN: Registrar Recepción de Registro.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 28/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Registrar_Recepcion_Registro() {
                DataTable Dt_Archivos;
                if (Session["Tabla_imagenes_Entrada"] != null)
                {
                    Dt_Archivos = (DataTable)Session["Tabla_imagenes_Entrada"];
                }
                else
                {
                    Dt_Archivos = Generar_Tabla_Documentos();
                }
                Cls_Ope_Tal_Entradas_Vehiculos_Negocio Negocio = new Cls_Ope_Tal_Entradas_Vehiculos_Negocio();
                Negocio.P_Fecha_Entrada = Convert.ToDateTime(Txt_Fecha_Recepcion_Real.Text);
                Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                Negocio.P_Tipo_Entrada = "SOLICITUD";
                Negocio.P_Tipo_Solicitud = Cmb_Tipo_Servicio.SelectedItem.Value;
                Negocio.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                Negocio.P_Vehiculo_ID = Hdf_Vehiculo_ID.Value.Trim();
                Negocio.P_Empleado_Recibio_ID = Cls_Sessiones.Empleado_ID;
                Negocio.P_Estatus = "PENDIENTE";
                Negocio.P_Comentarios = Txt_Comentarios_Recepción.Text.Trim();
                if (Txt_Kilometraje.Text.Trim().Length > 0) { Negocio.P_Kilometraje = Convert.ToDouble(Txt_Kilometraje.Text); }
                Negocio.P_Empleado_Entrego_ID = Hdf_Empleado_Entrega_ID.Value.Trim();
                Negocio.P_Dt_Detalles = new DataTable();
                Negocio.P_Dt_Archivos = Dt_Archivos;
                Negocio.Alta_Entrada_Vehiculo();
            }

        #endregion

        #region Validaciones

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Autorizacion
            ///DESCRIPCIÓN: Valida la Solicitud de Servicio antes de ser Autorizada
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 07/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private Boolean Validar_Autorizacion() { 
                Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                String Mensaje_Error = "";
                Boolean Validacion = true;
                if (Txt_Fecha_Recepcion_Real.Text.Trim().Length == 0) {
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar la Fecha de Recepción Real del Vehiculo.";
                    Mensaje_Error = Mensaje_Error + " <br />";
                    Validacion = false;
                }
                if (Txt_Kilometraje.Text.Trim().Length > 0) {
                    if (!Validar_Valores_Decimales(Txt_Kilometraje.Text)) {
                        Mensaje_Error = Mensaje_Error + "+ El Formato del Kilometraje no es Correcto [Correcto: '12345', '12353.0' ó '12254.33'].";
                        Mensaje_Error = Mensaje_Error + " <br />";
                        Validacion = false;
                    }
                }
                if (Hdf_Empleado_Entrega_ID.Value.Trim().Length == 0) {
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar al Empleado que Entrega el Vehículo.";
                    Mensaje_Error = Mensaje_Error + " <br />";
                    Validacion = false;
                }
                if (!Validacion) {
                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
                    Div_Contenedor_Msj_Error.Visible = true;
                }
                return Validacion;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Valores_Decimales
            ///DESCRIPCIÓN: Valida los valores decimales para un Anexo.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO: Marzo/2012 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private Boolean Validar_Valores_Decimales(String Valor) {
                Boolean Validacion = true;
                Regex Expresion_Regular = new Regex(@"^[0-9]{1,50}(\.[0-9]{0,2})?$");
                Validacion = Expresion_Regular.IsMatch(Valor);
                return Validacion;
            }

        #endregion

    #endregion
    
    #region Grids
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Solicitudes_PageIndexChanging
        ///DESCRIPCIÓN: Maneja la paginación del Listado
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Solicitudes_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            try {
                Grid_Listado_Solicitudes.SelectedIndex = (-1);
                Grid_Listado_Solicitudes.PageIndex = e.NewPageIndex;
                Llenar_Listado_Solicitudes();
            } catch(Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Solicitudes_SelectedIndexChanged
        ///DESCRIPCIÓN: Obtiene los datos de una Solicitud 
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Solicitudes_SelectedIndexChanged(object sender, EventArgs e) {
            try{
                if (Grid_Listado_Solicitudes.SelectedIndex > (-1)){
                    Limpiar_Formulario();
                    Hdf_No_Solicitud.Value = Grid_Listado_Solicitudes.SelectedRow.Cells[1].Text.Trim();
                    Mostrar_Registro();
                    Txt_Fecha_Recepcion_Real.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Today);
                    Configuracion_Formulario("OPERACION");
                    Grid_Listado_Solicitudes.SelectedIndex = -1;
                    System.Threading.Thread.Sleep(500);
                }
            } catch(Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Empleados_Resguardo_PageIndexChanging
        ///DESCRIPCIÓN: Maneja el evento de cambio de Página del GridView de Busqueda
        ///             de empleados.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 09/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Busqueda_Empleados_Resguardo_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            try {
                Grid_Busqueda_Empleados_Resguardo.PageIndex = e.NewPageIndex;
                Llenar_Grid_Busqueda_Empleados_Resguardo();
                MPE_Resguardante.Show();
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged
        ///DESCRIPCIÓN: Maneja el evento de cambio de Selección del GridView de Busqueda
        ///             de empleados.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 09/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged(object sender, EventArgs e) { 
            try {
                if (Grid_Busqueda_Empleados_Resguardo.SelectedIndex > (-1)) {
                    String Empleado_Seleccionado_ID = Grid_Busqueda_Empleados_Resguardo.SelectedRow.Cells[1].Text.Trim();
                    Mostrar_Informacion_Empleado(Empleado_Seleccionado_ID);
                    Grid_Busqueda_Empleados_Resguardo.SelectedIndex = (-1);
                    MPE_Resguardante.Hide();
                }
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

    #endregion

    #region Eventos
    
        ///****************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Controla las operaciones del Boton de Salir
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Hacer_Entrada_Click(object sender, ImageClickEventArgs e) {
            if (Validar_Autorizacion()) {
                Registrar_Recepcion_Registro();
                Llenar_Listado_Solicitudes();
                Configuracion_Formulario("INICIAL");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "GACO", "alert('Operacion Exitosa: Recepción de Unidad');", true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Controla las operaciones del Boton de Salir
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e) {
            if (Div_Campos.Visible) {
                Session["Tabla_imagenes_Entrada"] = null;
                Session["Diccionario_Archivos"] = null;
                Limpiar_Formulario();
                Configuracion_Formulario("INICIAL");
            } else {
                Session["Tabla_imagenes_Entrada"] = null;
                Session["Diccionario_Archivos"] = null;
                Limpiar_Formulario();
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Empleado_Entrega_Click
        ///DESCRIPCIÓN: Lanza la Busqueda del Empleado
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 09/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Empleado_Entrega_Click(object sender, ImageClickEventArgs e) {            
            Grid_Busqueda_Empleados_Resguardo.PageIndex = 0;
            Llenar_Grid_Busqueda_Empleados_Resguardo();
            MPE_Resguardante.Show();
        }
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Empleados_Click
        ///DESCRIPCIÓN: Ejecuta la Busqueda Avanzada para el Resguardante.
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 09/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************    
        protected void Btn_Busqueda_Empleados_Click(object sender, EventArgs e) {
            try {                
                Grid_Busqueda_Empleados_Resguardo.PageIndex = 0;
                Llenar_Grid_Busqueda_Empleados_Resguardo();
                MPE_Resguardante.Show();
            }  catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Actualizar_Listado_Click
        ///DESCRIPCIÓN: Ejecuta el Proceso para Salir.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Actualizar_Listado_Click(object sender, ImageClickEventArgs e) {
            Llenar_Listado_Solicitudes();
        }

    #endregion

    #region Files Upload
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Fup_Frente_UploadedComplete
        ///DESCRIPCIÓN: 
        ///PROPIEDADES:     
        ///CREO: Jesus Toledo Rdz
        ///FECHA_CREO: 04/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
            protected void Fup_Frente_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
            {
                HashAlgorithm sha = HashAlgorithm.Create("SHA1");
                String Checksum_Archivo = BitConverter.ToString(sha.ComputeHash(Fup_Frente.FileBytes));       //obtener checksum del archivo
                Dictionary<String, Byte[]> Diccionario_Archivos = Obtener_Diccionario_Archivos();   //obtener diccionario checksum-archivo
                DataTable Tabla_Documentos;//Crea DT para guardar resultado
                bool Bnd_Documento_Existente = false;//Boleano para ver si se repite documento LO PUEDO OMITIR
                String Extension_Archivo = Path.GetExtension(Fup_Frente.FileName).ToLower();//Aqui obtiene el Nombre del Archivo CAMBIAR POR FUP DE MI FORM
                // arreglo con las extensiones de archivo permitidas
                String[] Extensiones_Permitidas = { ".jpg" };//Esto esta bien
                String Nombre_Directorio;
                String Ruta_Archivo;
                // si la extension del archivo recibido no es valida, regresar
                
                    if (Array.IndexOf(Extensiones_Permitidas, Extension_Archivo) < 0)
                    {
                        //Mensaje_Error(" No se permite subir archivos con extensión: " + Extension_Archivo);
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Error_Extension_Archivo", "top.$get(\"" +
                        Fup_Frente.ClientID + "\").style.background-color = 'red!important';", true);
                        return;
                    }

                    if (Fup_Frente.FileBytes.Length > 2048000) // si la longitud del archivo recibido es mayor que 2MB, mostrar mensaje
                    {
                        //Mensaje_Error(" El tamaño del archivo excede el limite permitido: " + Fup_Frente.FileName);
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Error_Tamanio_Archivo", "top.$get(\"" +
                        Fup_Frente.ClientID + "\").style.background-color = 'red!important';", true);
                        return;
                    }
                    if (Session["Tabla_imagenes_Entrada"] != null)
                    {
                        Tabla_Documentos = (DataTable)Session["Tabla_imagenes_Entrada"];
                    }
                    else
                    {
                        Tabla_Documentos = Generar_Tabla_Documentos();
                    }

                    if (!Diccionario_Archivos.ContainsKey(Checksum_Archivo)) //si el checksum no esta en el diccionario, agregarlo y guardar en variable de sesion
                    {
                        Diccionario_Archivos.Add(Checksum_Archivo, Fup_Frente.FileBytes);
                        Session["Diccionario_Archivos"] = Diccionario_Archivos;
                    }

                    // recorrer la tabla documentos
                    foreach (DataRow Fila_Tabla_Documento in Tabla_Documentos.Rows)
                    {
                        //si ya existe un elemento con el tipo de documento, actualizarlo con el nuevo archivo
                        if (Fila_Tabla_Documento["FOTO"].ToString() == "FRENTE")
                        {
                            Fila_Tabla_Documento["FOTO"] = "FRENTE";
                            Fila_Tabla_Documento["ARCHIVO"] = Fup_Frente.PostedFile.FileName;
                            Fila_Tabla_Documento["RUTA_ARCHIVO"] = @"../../ARCHIVOS_TALLER_MUNICIPAL\REVISTA_MECANICA\" + Txt_No_Economico.Text.Trim() + @"\Solicitud_No_" + Hdf_No_Solicitud.Value + @"\Entrada\Foto_Frente" + Path.GetExtension(Fup_Frente.FileName).ToLower();
                            Fila_Tabla_Documento["CHECKSUM"] = Checksum_Archivo;
                            Bnd_Documento_Existente = true;     // bandera a verdadero para indicar que se actualizo un registro existente en la tablaString Ruta = Server.MapPath("../../ARCHIVOS_TALLER_MUNICIPAL/PARAMETROS");
                            break;
                        }
                    }
                    //si la bandera es falso, no se encontro registro del tipo de documento, asi que se crea uno nuevo
                    if (Bnd_Documento_Existente == false)
                    {                        
                    DataRow Nueva_Fila = Tabla_Documentos.NewRow();
                    Nueva_Fila["FOTO"] = "FRENTE";
                    Nueva_Fila["ARCHIVO"] = Fup_Frente.PostedFile.FileName;
                    Nueva_Fila["RUTA_ARCHIVO"] = @"../../ARCHIVOS_TALLER_MUNICIPAL\REVISTA_MECANICA\" + Txt_No_Economico.Text.Trim() + @"\Solicitud_No_" + Hdf_No_Solicitud.Value + @"\Entrada\Foto_Frente" + Path.GetExtension(Fup_Frente.FileName).ToLower();
                    Nueva_Fila["CHECKSUM"] = Checksum_Archivo;
                        
                    Tabla_Documentos.Rows.Add(Nueva_Fila);
                    }
                    Guardar_Archivos(Diccionario_Archivos, Checksum_Archivo, @"../../ARCHIVOS_TALLER_MUNICIPAL\REVISTA_MECANICA\" + Txt_No_Economico.Text.Trim() + @"\Solicitud_No_" + Hdf_No_Solicitud.Value + @"\Entrada\Foto_Frente" + Path.GetExtension(Fup_Frente.FileName).ToLower());
                    Session["Tabla_imagenes_Entrada"] = Tabla_Documentos;
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Fup_Frente_UploadedComplete
            ///DESCRIPCIÓN: 
            ///PROPIEDADES:     
            ///CREO: Jesus Toledo Rdz
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            protected void Fup_Atras_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
            {
                HashAlgorithm sha = HashAlgorithm.Create("SHA1");
                String Checksum_Archivo = BitConverter.ToString(sha.ComputeHash(Fup_Atras.FileBytes));       //obtener checksum del archivo
                Dictionary<String, Byte[]> Diccionario_Archivos = Obtener_Diccionario_Archivos();   //obtener diccionario checksum-archivo
                DataTable Tabla_Documentos;//Crea DT para guardar resultado
                bool Bnd_Documento_Existente = false;//Boleano para ver si se repite documento LO PUEDO OMITIR
                String Extension_Archivo = Path.GetExtension(Fup_Atras.FileName).ToLower();//Aqui obtiene el Nombre del Archivo CAMBIAR POR FUP DE MI FORM
                // arreglo con las extensiones de archivo permitidas
                String[] Extensiones_Permitidas = { ".jpg" };//Esto esta bien
                String Nombre_Directorio;
                String Ruta_Archivo;
                // si la extension del archivo recibido no es valida, regresar

                if (Array.IndexOf(Extensiones_Permitidas, Extension_Archivo) < 0)
                {
                    //Mensaje_Error(" No se permite subir archivos con extensión: " + Extension_Archivo);
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Error_Extension_Archivo", "top.$get(\"" +
                    Fup_Atras.ClientID + "\").style.background-color = 'red!important';", true);
                    return;
                }

                if (Fup_Atras.FileBytes.Length > 2048000) // si la longitud del archivo recibido es mayor que 2MB, mostrar mensaje
                {
                    //Mensaje_Error(" El tamaño del archivo excede el limite permitido: " + Fup_Frente.FileName);
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Error_Tamanio_Archivo", "top.$get(\"" +
                    Fup_Atras.ClientID + "\").style.background-color = 'red!important';", true);
                    return;
                }
                if (Session["Tabla_imagenes_Entrada"] != null)
                {
                    Tabla_Documentos = (DataTable)Session["Tabla_imagenes_Entrada"];
                }
                else
                {
                    Tabla_Documentos = Generar_Tabla_Documentos();
                }

                if (!Diccionario_Archivos.ContainsKey(Checksum_Archivo)) //si el checksum no esta en el diccionario, agregarlo y guardar en variable de sesion
                {
                    Diccionario_Archivos.Add(Checksum_Archivo, Fup_Atras.FileBytes);
                    Session["Diccionario_Archivos"] = Diccionario_Archivos;
                }

                // recorrer la tabla documentos
                foreach (DataRow Fila_Tabla_Documento in Tabla_Documentos.Rows)
                {
                    //si ya existe un elemento con el tipo de documento, actualizarlo con el nuevo archivo
                    if (Fila_Tabla_Documento["FOTO"].ToString() == "ATRAS")
                    {
                        Fila_Tabla_Documento["FOTO"] = "ATRAS";
                        Fila_Tabla_Documento["ARCHIVO"] = Fup_Atras.PostedFile.FileName;
                        Fila_Tabla_Documento["RUTA_ARCHIVO"] = @"../../ARCHIVOS_TALLER_MUNICIPAL\REVISTA_MECANICA\" + Txt_No_Economico.Text.Trim() + @"\Solicitud_No_" + Hdf_No_Solicitud.Value + @"\Entrada\Foto_Atras" + Path.GetExtension(Fup_Atras.FileName).ToLower();
                        Fila_Tabla_Documento["CHECKSUM"] = Checksum_Archivo;
                        Bnd_Documento_Existente = true;     // bandera a verdadero para indicar que se actualizo un registro existente en la tabla
                        break;
                    }
                }
                //si la bandera es falso, no se encontro registro del tipo de documento, asi que se crea uno nuevo
                if (Bnd_Documento_Existente == false)
                {
                    DataRow Nueva_Fila = Tabla_Documentos.NewRow();
                    Nueva_Fila["FOTO"] = "ATRAS";
                    Nueva_Fila["ARCHIVO"] = Fup_Atras.PostedFile.FileName;
                    Nueva_Fila["RUTA_ARCHIVO"] = @"../../ARCHIVOS_TALLER_MUNICIPAL\REVISTA_MECANICA\" + Txt_No_Economico.Text.Trim() + @"\Solicitud_No_" + Hdf_No_Solicitud.Value + @"\Entrada\Foto_Atras" + Path.GetExtension(Fup_Atras.FileName).ToLower();
                    Nueva_Fila["CHECKSUM"] = Checksum_Archivo;

                    Tabla_Documentos.Rows.Add(Nueva_Fila);
                }
                Guardar_Archivos(Diccionario_Archivos, Checksum_Archivo, @"../../ARCHIVOS_TALLER_MUNICIPAL\REVISTA_MECANICA\" + Txt_No_Economico.Text.Trim() + @"\Solicitud_No_" + Hdf_No_Solicitud.Value + @"\Entrada\Foto_Atras" + Path.GetExtension(Fup_Atras.FileName).ToLower());
                Session["Tabla_imagenes_Entrada"] = Tabla_Documentos;
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Fup_Frente_UploadedComplete
            ///DESCRIPCIÓN: 
            ///PROPIEDADES:     
            ///CREO: Jesus Toledo Rdz
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            protected void Fup_Izquierda_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
            {
                HashAlgorithm sha = HashAlgorithm.Create("SHA1");
                String Checksum_Archivo = BitConverter.ToString(sha.ComputeHash(Fup_Izquierda.FileBytes));       //obtener checksum del archivo
                Dictionary<String, Byte[]> Diccionario_Archivos = Obtener_Diccionario_Archivos();   //obtener diccionario checksum-archivo
                DataTable Tabla_Documentos;//Crea DT para guardar resultado
                bool Bnd_Documento_Existente = false;//Boleano para ver si se repite documento LO PUEDO OMITIR
                String Extension_Archivo = Path.GetExtension(Fup_Izquierda.FileName).ToLower();//Aqui obtiene el Nombre del Archivo CAMBIAR POR FUP DE MI FORM
                // arreglo con las extensiones de archivo permitidas
                String[] Extensiones_Permitidas = { ".jpg" };//Esto esta bien
                String Nombre_Directorio;
                String Ruta_Archivo;
                // si la extension del archivo recibido no es valida, regresar

                if (Array.IndexOf(Extensiones_Permitidas, Extension_Archivo) < 0)
                {
                    //Mensaje_Error(" No se permite subir archivos con extensión: " + Extension_Archivo);
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Error_Extension_Archivo", "top.$get(\"" +
                    Fup_Izquierda.ClientID + "\").style.background-color = 'red!important';", true);
                    return;
                }

                if (Fup_Izquierda.FileBytes.Length > 2048000) // si la longitud del archivo recibido es mayor que 2MB, mostrar mensaje
                {
                    //Mensaje_Error(" El tamaño del archivo excede el limite permitido: " + Fup_Frente.FileName);
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Error_Tamanio_Archivo", "top.$get(\"" +
                    Fup_Izquierda.ClientID + "\").style.background-color = 'red!important';", true);
                    return;
                }
                if (Session["Tabla_imagenes_Entrada"] != null)
                {
                    Tabla_Documentos = (DataTable)Session["Tabla_imagenes_Entrada"];
                }
                else
                {
                    Tabla_Documentos = Generar_Tabla_Documentos();
                }

                if (!Diccionario_Archivos.ContainsKey(Checksum_Archivo)) //si el checksum no esta en el diccionario, agregarlo y guardar en variable de sesion
                {
                    Diccionario_Archivos.Add(Checksum_Archivo, Fup_Izquierda.FileBytes);
                    Session["Diccionario_Archivos"] = Diccionario_Archivos;
                }

                // recorrer la tabla documentos
                foreach (DataRow Fila_Tabla_Documento in Tabla_Documentos.Rows)
                {
                    //si ya existe un elemento con el tipo de documento, actualizarlo con el nuevo archivo
                    if (Fila_Tabla_Documento["FOTO"].ToString() == "IZQUIERDA")
                    {
                        Fila_Tabla_Documento["FOTO"] = "IZQUIERDA";
                        Fila_Tabla_Documento["ARCHIVO"] = Fup_Izquierda.PostedFile.FileName;
                        Fila_Tabla_Documento["RUTA_ARCHIVO"] = @"../../ARCHIVOS_TALLER_MUNICIPAL\REVISTA_MECANICA\" + Txt_No_Economico.Text.Trim() + @"\Solicitud_No_" + Hdf_No_Solicitud.Value + @"\Entrada\Foto_lado_izq" + Path.GetExtension(Fup_Izquierda.FileName).ToLower();
                        Fila_Tabla_Documento["CHECKSUM"] = Checksum_Archivo;
                        Bnd_Documento_Existente = true;     // bandera a verdadero para indicar que se actualizo un registro existente en la tabla
                        break;
                    }
                }
                //si la bandera es falso, no se encontro registro del tipo de documento, asi que se crea uno nuevo
                if (Bnd_Documento_Existente == false)
                {
                    DataRow Nueva_Fila = Tabla_Documentos.NewRow();
                    Nueva_Fila["FOTO"] = "IZQUIERDA";
                    Nueva_Fila["ARCHIVO"] = Fup_Izquierda.PostedFile.FileName;
                    Nueva_Fila["RUTA_ARCHIVO"] = @"../../ARCHIVOS_TALLER_MUNICIPAL\REVISTA_MECANICA\" + Txt_No_Economico.Text.Trim() + @"\Solicitud_No_" + Hdf_No_Solicitud.Value + @"\Entrada\Foto_lado_izq" + Path.GetExtension(Fup_Izquierda.FileName).ToLower();
                    Nueva_Fila["CHECKSUM"] = Checksum_Archivo;

                    Tabla_Documentos.Rows.Add(Nueva_Fila);
                }
                Guardar_Archivos(Diccionario_Archivos, Checksum_Archivo, @"../../ARCHIVOS_TALLER_MUNICIPAL\REVISTA_MECANICA\" + Txt_No_Economico.Text.Trim() + @"\Solicitud_No_" + Hdf_No_Solicitud.Value + @"\Entrada\Foto_lado_izq" + Path.GetExtension(Fup_Izquierda.FileName).ToLower());
                Session["Tabla_imagenes_Entrada"] = Tabla_Documentos;
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Fup_Frente_UploadedComplete
            ///DESCRIPCIÓN: 
            ///PROPIEDADES:     
            ///CREO: Jesus Toledo Rdz
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            protected void Fup_Derecha_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
            {
                HashAlgorithm sha = HashAlgorithm.Create("SHA1");
                String Checksum_Archivo = BitConverter.ToString(sha.ComputeHash(Fup_Derecha.FileBytes));       //obtener checksum del archivo
                Dictionary<String, Byte[]> Diccionario_Archivos = Obtener_Diccionario_Archivos();   //obtener diccionario checksum-archivo
                DataTable Tabla_Documentos;//Crea DT para guardar resultado
                bool Bnd_Documento_Existente = false;//Boleano para ver si se repite documento LO PUEDO OMITIR
                String Extension_Archivo = Path.GetExtension(Fup_Derecha.FileName).ToLower();//Aqui obtiene el Nombre del Archivo CAMBIAR POR FUP DE MI FORM
                // arreglo con las extensiones de archivo permitidas
                String[] Extensiones_Permitidas = { ".jpg" };//Esto esta bien
                String Nombre_Directorio;
                String Ruta_Archivo;
                // si la extension del archivo recibido no es valida, regresar

                if (Array.IndexOf(Extensiones_Permitidas, Extension_Archivo) < 0)
                {
                    //Mensaje_Error(" No se permite subir archivos con extensión: " + Extension_Archivo);
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Error_Extension_Archivo", "top.$get(\"" +
                    Fup_Derecha.ClientID + "\").style.background-color = 'red!important';", true);
                    return;
                }

                if (Fup_Derecha.FileBytes.Length > 2048000) // si la longitud del archivo recibido es mayor que 2MB, mostrar mensaje
                {
                    //Mensaje_Error(" El tamaño del archivo excede el limite permitido: " + Fup_Frente.FileName);
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Error_Tamanio_Archivo", "top.$get(\"" +
                    Fup_Derecha.ClientID + "\").style.background-color = 'red!important';", true);
                    return;
                }
                if (Session["Tabla_imagenes_Entrada"] != null)
                {
                    Tabla_Documentos = (DataTable)Session["Tabla_imagenes_Entrada"];
                }
                else
                {
                    Tabla_Documentos = Generar_Tabla_Documentos();
                }

                if (!Diccionario_Archivos.ContainsKey(Checksum_Archivo)) //si el checksum no esta en el diccionario, agregarlo y guardar en variable de sesion
                {
                    Diccionario_Archivos.Add(Checksum_Archivo, Fup_Derecha.FileBytes);
                    Session["Diccionario_Archivos"] = Diccionario_Archivos;
                }

                // recorrer la tabla documentos
                foreach (DataRow Fila_Tabla_Documento in Tabla_Documentos.Rows)
                {
                    //si ya existe un elemento con el tipo de documento, actualizarlo con el nuevo archivo
                    if (Fila_Tabla_Documento["FOTO"].ToString() == "DERECHA")
                    {
                        Fila_Tabla_Documento["FOTO"] = "DERECHA";
                        Fila_Tabla_Documento["ARCHIVO"] = Fup_Derecha.PostedFile.FileName;
                        Fila_Tabla_Documento["RUTA_ARCHIVO"] = @"../../ARCHIVOS_TALLER_MUNICIPAL/REVISTA_MECANICA\" + Txt_No_Economico.Text.Trim() + @"\Solicitud_No_" + Hdf_No_Solicitud.Value + @"\Entrada\Foto_lado_der" + Path.GetExtension(Fup_Derecha.FileName).ToLower();
                        Fila_Tabla_Documento["CHECKSUM"] = Checksum_Archivo;
                        Bnd_Documento_Existente = true;     // bandera a verdadero para indicar que se actualizo un registro existente en la tabla
                        break;
                    }
                }
                //si la bandera es falso, no se encontro registro del tipo de documento, asi que se crea uno nuevo
                if (Bnd_Documento_Existente == false)
                {
                    DataRow Nueva_Fila = Tabla_Documentos.NewRow();
                    Nueva_Fila["FOTO"] = "DERECHA";
                    Nueva_Fila["ARCHIVO"] = Fup_Derecha.PostedFile.FileName;
                    Nueva_Fila["RUTA_ARCHIVO"] = @"../../ARCHIVOS_TALLER_MUNICIPAL/REVISTA_MECANICA\" + Txt_No_Economico.Text.Trim() + @"\Solicitud_No_" + Hdf_No_Solicitud.Value + @"\Entrada\Foto_lado_der" + Path.GetExtension(Fup_Derecha.FileName).ToLower();
                    Nueva_Fila["CHECKSUM"] = Checksum_Archivo;

                    Tabla_Documentos.Rows.Add(Nueva_Fila);
                }
                Guardar_Archivos(Diccionario_Archivos, Checksum_Archivo, @"../../ARCHIVOS_TALLER_MUNICIPAL/REVISTA_MECANICA\" + Txt_No_Economico.Text.Trim() + @"\Solicitud_No_" + Hdf_No_Solicitud.Value + @"\Entrada\Foto_lado_der" + Path.GetExtension(Fup_Derecha.FileName).ToLower());
                Session["Tabla_imagenes_Entrada"] = Tabla_Documentos;
            }
            protected void FileUploadComplete(object sender, EventArgs e)
            {
                string filename = System.IO.Path.GetFileName(Fup_Frente.FileName);
                Fup_Frente.SaveAs(Server.MapPath("../../ARCHIVOS_TALLER_MUNICIPAL/") + filename);
            }
            ///*******************************************************************************************************
            /// 	NOMBRE_FUNCIÓN: Guardar_Archivos
            /// 	DESCRIPCIÓN: Guardar en el servidor los archivos que se hayan recibido
            /// 	PARÁMETROS:
            /// 	CREO: Roberto González Oseguera
            /// 	FECHA_CREO: 10-may-2011
            /// 	MODIFICÓ: 
            /// 	FECHA_MODIFICÓ: 
            /// 	CAUSA_MODIFICACIÓN: 
            ///*******************************************************************************************************
            private void Guardar_Archivos(Dictionary<String, Byte[]> P_Dicc,String P_Checksum, String P_Ruta_Archivo)
            {
                DataTable Tabla_Tramites = (DataTable)Session["Tabla_imagenes_Entrada"];
                Dictionary<String, Byte[]> Diccionario_Archivos = Obtener_Diccionario_Archivos();
                String Nombre_Directorio;
                String Ruta_Archivo;

                try
                {                    
                        Nombre_Directorio = MapPath(Path.GetDirectoryName(P_Ruta_Archivo));
                        Ruta_Archivo = MapPath(HttpUtility.HtmlDecode(P_Ruta_Archivo));
                                if (!Directory.Exists(Nombre_Directorio))                       //si el directorio no existe, crearlo
                                    Directory.CreateDirectory(Nombre_Directorio);
                                //crear filestream y binarywriter para guardar archivo
                                FileStream Escribir_Archivo = new FileStream(Ruta_Archivo, FileMode.Create, FileAccess.Write);
                                BinaryWriter Datos_Archivo = new BinaryWriter(Escribir_Archivo);
                                Datos_Archivo.Write(P_Dicc[P_Checksum]);
                                // Guardar archivo (escribir datos en el filestream)                            
                                //Cerrar Objetos
                                Escribir_Archivo.Flush();
                                //Escribir_Archivo.Dispose();
                                Datos_Archivo.Flush();
                                Datos_Archivo.Close();
                                Escribir_Archivo.Close();
                }
                catch (Exception Ex)
                {
                    throw new Exception("Guardar_Archivos " + Ex.Message.ToString(), Ex);
                }
            }
            ///*******************************************************************************************************
            /// 	NOMBRE_FUNCIÓN: Obtener_Diccionario_Archivos
            /// 	DESCRIPCIÓN: Regresa el diccionario checksum-archivo si se encuentra en variable de sesion y si no,
            /// 	            regresa un diccionario vacio
            /// 	PARÁMETROS:
            /// 	CREO: Roberto González Oseguera
            /// 	FECHA_CREO: 04-may-2011
            /// 	MODIFICÓ: 
            /// 	FECHA_MODIFICÓ: 
            /// 	CAUSA_MODIFICACIÓN: 
            ///*******************************************************************************************************
            private Dictionary<String, Byte[]> Obtener_Diccionario_Archivos()
            {
                Dictionary<String, Byte[]> Diccionario_Archivos = new Dictionary<String, Byte[]>();

                // si existe el diccionario en variable de sesion
                if (Session["Diccionario_Archivos"] != null)
                {
                    Diccionario_Archivos = (Dictionary<String, Byte[]>)Session["Diccionario_Archivos"];
                }

                return Diccionario_Archivos;
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Generar_Tabla_Documentos
            /// DESCRIPCION: Genera la tabla de Documentos, el esquema para guardar los tipos de document a recibir
            /// PARAMETROS: 
            /// CREO: Jesus Toledo Rodriguez
            /// FECHA_CREO: 04-may-2012
            /// MODIFICO:
            /// FECHA_MODIFICO:
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private DataTable Generar_Tabla_Documentos()
            {
                DataTable Tabla_Nueva = new DataTable();
                DataColumn Columna0_Foto;
                DataColumn Columna1_Archivo;
                DataColumn Columna2_Ruta_Archivo;
                DataColumn Columna3_Checksum;
                //Nueva_Fila["FOTO"]
                //Nueva_Fila["ARCHIVO"]
                //Nueva_Fila["RUTA_ARCHIVO"]
                //Nueva_Fila["CHECKSUM"]
                try
                {
                    // ---------- Inicializar columnas
                    Columna0_Foto = new DataColumn();
                    Columna0_Foto.DataType = System.Type.GetType("System.String");
                    Columna0_Foto.ColumnName = "FOTO";
                    Tabla_Nueva.Columns.Add(Columna0_Foto);
                    Columna1_Archivo = new DataColumn();
                    Columna1_Archivo.DataType = System.Type.GetType("System.String");
                    Columna1_Archivo.ColumnName = "ARCHIVO";
                    Tabla_Nueva.Columns.Add(Columna1_Archivo);
                    Columna2_Ruta_Archivo = new DataColumn();
                    Columna2_Ruta_Archivo.DataType = System.Type.GetType("System.String");
                    Columna2_Ruta_Archivo.ColumnName = "RUTA_ARCHIVO";
                    Tabla_Nueva.Columns.Add(Columna2_Ruta_Archivo);
                    Columna3_Checksum = new DataColumn();
                    Columna3_Checksum.DataType = System.Type.GetType("System.String");
                    Columna3_Checksum.ColumnName = "CHECKSUM";
                    Tabla_Nueva.Columns.Add(Columna3_Checksum);
                    
                    return Tabla_Nueva;
                }
                catch (Exception ex)
                {
                    throw new Exception("Generar_Tabla_Documentos " + ex.Message.ToString(), ex);
                }
            }
        #endregion
}
