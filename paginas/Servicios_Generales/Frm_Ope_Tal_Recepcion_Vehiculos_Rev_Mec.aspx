﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Tal_Recepcion_Vehiculos_Rev_Mec.aspx.cs" Inherits="paginas_Taller_Mecanico_Frm_Ope_Tal_Recepcion_Vehiculos_Rev_Mec" Title="Recepción de Unidades" EnableEventValidation="false" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">
   <script type = "text/javascript">
       window.onerror = new Function("return true");
       function Limpiar_Ctlr_Busqueda_Resguardante() {
           document.getElementById("<%=Txt_Busqueda_No_Empleado.ClientID%>").value = "";
           document.getElementById("<%=Txt_Busqueda_RFC.ClientID%>").value = "";
           document.getElementById("<%=Txt_Busqueda_Nombre_Empleado.ClientID%>").value = "";
           return false;
       }    
       function uploadComplete(sender, args) {
           $get("<%=lblMesg.ClientID%>").style.color = "green";
           $get("<%=lblMesg.ClientID%>").innerHTML = args.get_fileName();
           $get("<%=Liga_Imagen_Frente.ClientID%>").src = '..\\..\\ARCHIVOS_TALLER_MUNICIPAL\\REVISTA_MECANICA\\' + $get("<%=Txt_No_Economico.ClientID%>").value + "\\Solicitud_No_" + $get("<%=Hdf_No_Solicitud.ClientID%>").value + "\\Entrada\\Foto_Frente.jpg";
           $get("<%=Liga_Frente.ClientID%>").href = '..\\..\\ARCHIVOS_TALLER_MUNICIPAL\\REVISTA_MECANICA\\' + $get("<%=Txt_No_Economico.ClientID%>").value + "\\Solicitud_No_" + $get("<%=Hdf_No_Solicitud.ClientID%>").value + "\\Entrada\\Foto_Frente.jpg";
       }
       function uploadComplete_Back(sender, args) {
           $get("<%=lblMesg_Back.ClientID%>").style.color = "green";
           $get("<%=lblMesg_Back.ClientID%>").innerHTML = args.get_fileName();
           $get("<%=Liga_Imagen_Atras.ClientID%>").src = '..\\..\\ARCHIVOS_TALLER_MUNICIPAL\\REVISTA_MECANICA\\' + $get("<%=Txt_No_Economico.ClientID%>").value + "\\Solicitud_No_" + $get("<%=Hdf_No_Solicitud.ClientID%>").value + "\\Entrada\\Foto_Atras.jpg";
           $get("<%=Liga_Atras.ClientID%>").href = '..\\..\\ARCHIVOS_TALLER_MUNICIPAL\\REVISTA_MECANICA\\' + $get("<%=Txt_No_Economico.ClientID%>").value + "\\Solicitud_No_" + $get("<%=Hdf_No_Solicitud.ClientID%>").value + "\\Entrada\\Foto_Atras.jpg";
       }
       function uploadComplete_Left(sender, args) {
           $get("<%=lblMesg_Left.ClientID%>").style.color = "green";
           $get("<%=lblMesg_Left.ClientID%>").innerHTML = args.get_fileName();
           $get("<%=Liga_Imagen_Izquierdo.ClientID%>").src = '..\\..\\ARCHIVOS_TALLER_MUNICIPAL\\REVISTA_MECANICA\\' + $get("<%=Txt_No_Economico.ClientID%>").value + "\\Solicitud_No_" + $get("<%=Hdf_No_Solicitud.ClientID%>").value + "\\Entrada\\Foto_lado_izq.jpg";
           $get("<%=Liga_Izquierdo.ClientID%>").href = '..\\..\\ARCHIVOS_TALLER_MUNICIPAL\\REVISTA_MECANICA\\' + $get("<%=Txt_No_Economico.ClientID%>").value + "\\Solicitud_No_" + $get("<%=Hdf_No_Solicitud.ClientID%>").value + "\\Entrada\\Foto_lado_izq.jpg";
       }
       function uploadComplete_Rigth(sender, args) {
           $get("<%=lblMesg_Rigth.ClientID%>").style.color = "green";
           $get("<%=lblMesg_Rigth.ClientID%>").innerHTML = args.get_fileName();
           $get("<%=Liga_Imagen_Derecho.ClientID%>").src = '..\\..\\ARCHIVOS_TALLER_MUNICIPAL\\REVISTA_MECANICA\\' + $get("<%=Txt_No_Economico.ClientID%>").value + "\\Solicitud_No_" + $get("<%=Hdf_No_Solicitud.ClientID%>").value + "\\Entrada\\Foto_lado_der.jpg";
           $get("<%=Liga_Derecho.ClientID%>").href = '..\\..\\ARCHIVOS_TALLER_MUNICIPAL\\REVISTA_MECANICA\\' + $get("<%=Txt_No_Economico.ClientID%>").value + "\\Solicitud_No_" + $get("<%=Hdf_No_Solicitud.ClientID%>").value + "\\Entrada\\Foto_lado_der.jpg";
       }
       function uploadError(sender, args) {
           $get("<%=lblMesg.ClientID%>").style.color = "red";
           $get("<%=lblMesg.ClientID%>").innerHTML = "File upload failed.";
       }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScptM_Catalogo" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />    
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Loading" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            
            <div id="Div_Area_Trabajo" style="background-color: #ffffff; width: 100%; height: 100%;
                overflow: hidden">
                <center>
                    <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                        <tr align="center">
                            <td class="label_titulo" colspan="2">
                                Recepción de Unidades Revista Mecanica
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div id="Div_Contenedor_Msj_Error" style="width: 98%;" runat="server" visible="false">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td colspan="2" align="left">
                                                <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                    Width="24px" Height="24px" />
                                                <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 10%;">
                                            </td>
                                            <td style="width: 90%; text-align: left;" valign="top">
                                                <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left" style="width: 50%;">
                                <asp:ImageButton ID="Btn_Hacer_Entrada" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_aceptarplan.png"
                                    Width="24px" CssClass="Img_Button" AlternateText="Ejecutar Operación de Entrada"
                                    ToolTip="Ejecutar Operación de Entrada" OnClick="Btn_Hacer_Entrada_Click" />
                                <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                    Width="24px" CssClass="Img_Button" AlternateText="Salir" ToolTip="Salir" OnClick="Btn_Salir_Click" />
                            </td>
                            <td style="width: 50%;">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                    <br />
                    <div id="Div_Listado_Solicitudes" runat="server" style="width: 100%;">
                        <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                                <td style="text-align: right;">
                                    <asp:ImageButton ID="Btn_Actualizar_Listado" runat="server" ToolTip="Actualizar Listado"
                                        AlternateText="Actualizar Listado" OnClick="Btn_Actualizar_Listado_Click" ImageUrl="~/paginas/imagenes/paginas/actualizar_detalle.png"
                                        Width="16px" />
                                </td>
                            </tr>
                        </table>
                        <br />
                        <asp:GridView ID="Grid_Listado_Solicitudes" runat="server" CssClass="GridView_1"
                            AutoGenerateColumns="False" AllowPaging="True" PageSize="20" Width="99%" GridLines="None"
                            EmptyDataText="No se Encontrarón Solicitudes Pendientes." OnSelectedIndexChanged="Grid_Listado_Solicitudes_SelectedIndexChanged"
                            OnPageIndexChanging="Grid_Listado_Solicitudes_PageIndexChanging">
                            <RowStyle CssClass="GridItem" />
                            <Columns>
                                <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                    <ItemStyle Width="30px" />
                                </asp:ButtonField>
                                <asp:BoundField DataField="NO_SOLICITUD" HeaderText="NO_SOLICITUD" SortExpression="NO_SOLICITUD">
                                    <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FOLIO" HeaderText="Folio" SortExpression="FOLIO">
                                    <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FECHA_ELABORACION" HeaderText="Fecha Elaboración" SortExpression="FECHA_ELABORACION"
                                    DataFormatString="{0:dd/MMM/yyyy}">
                                    <ItemStyle Width="140px" HorizontalAlign="Center" Font-Size="X-Small" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FECHA_RECEPCION_PROG" HeaderText="Fecha Recepción [Programada]"
                                    SortExpression="FECHA_RECEPCION_PROG" DataFormatString="{0:dd/MMM/yyyy}">
                                    <ItemStyle Width="140px" HorizontalAlign="Center" Font-Size="X-Small" />
                                </asp:BoundField>
                                <asp:BoundField DataField="TIPO_SERVICIO" HeaderText="Tipo Servicio" SortExpression="TIPO_SERVICIO">
                                    <ItemStyle Width="140px" HorizontalAlign="Center" Font-Size="X-Small" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" SortExpression="DEPENDENCIA">
                                    <ItemStyle Font-Size="X-Small" />
                                </asp:BoundField>
                                <asp:BoundField DataField="NO_INVENTARIO" HeaderText="No. Inventario" SortExpression="NO_INVENTARIO">
                                    <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small" />
                                </asp:BoundField>
                            </Columns>
                            <PagerStyle CssClass="GridHeader" />
                            <SelectedRowStyle CssClass="GridSelected" />
                            <HeaderStyle CssClass="GridHeader" />
                            <AlternatingRowStyle CssClass="GridAltItem" />
                        </asp:GridView>
                    </div>
                    <div id="Div_Campos" runat="server" style="width: 100%;">
                        <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                                <td colspan="4">
                                    <asp:HiddenField ID="Hdf_No_Solicitud" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Folio" runat="server" Text="Folio" ForeColor="Black" Font-Bold="true"></asp:Label>
                                </td>
                                <td style="width: 35%;">
                                    <asp:TextBox ID="Txt_Folio" runat="server" Width="98%" ForeColor="Red" Font-Bold="true"
                                        Style="text-align: right;"></asp:TextBox>
                                </td>
                                <td colspan="2">
                                    &nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Fecha_Elaboracion" runat="server" Text="Fecha Elaboración"></asp:Label>
                                </td>
                                <td style="width: 35%;">
                                    <asp:TextBox ID="Txt_Fecha_Elaboracion" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text="Unidad Responsable"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="Cmb_Unidad_Responsable" runat="server" Width="100%" Enabled="false">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Km_Solicitud" runat="server" Text="Km Solicitud"></asp:Label>
                                </td>
                                <td style="width: 16%;">
                                    <asp:TextBox ID="Txt_Km_Solicitud" runat="server" Width="95%" Enabled="false"></asp:TextBox>
                                </td>
                                <td style="width: 15%;">
                                    &nbsp;&nbsp;
                                    <asp:Label ID="Lbl_Tipo_Servicio" runat="server" Text="Tipo Servicio"></asp:Label>
                                </td>
                                <td style="width: 35%;">
                                    <asp:DropDownList ID="Cmb_Tipo_Servicio" runat="server" Width="100%" Enabled="false">
                                        <asp:ListItem Value="">&lt; - - SELECCIONE - - &gt;</asp:ListItem>
                                        <asp:ListItem Value="SERVICIO_CORRECTIVO">SERVICIO CORRECTIVO</asp:ListItem>
                                        <asp:ListItem Value="SERVICIO_PREVENTIVO">SERVICIO PREVENTIVO</asp:ListItem>
                                        <asp:ListItem Value="REVISTA_MECANICA">REVISTA MECANICA</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    &nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:Panel ID="Pnl_Descripcion_Servicio" runat="server" Width="99%" GroupingText="Descripción del Servicio">
                                        <asp:TextBox ID="Txt_Descripcion_Servicio" runat="server" Rows="5" TextMode="MultiLine"
                                            Width="99%" Enabled="false"></asp:TextBox>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:Panel ID="Pnl_Vehiculo_Seleccionado" runat="server" GroupingText="Vehículo para el Servicio"
                                        Width="99%">
                                        <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:HiddenField ID="Hdf_Vehiculo_ID" runat="server" />
                                                    <asp:Label ID="Lbl_No_Inventario" runat="server" Text="No. Inventario"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_No_Inventario" runat="server" Enabled="false" MaxLength="7"
                                                        Width="70%"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Inventario" runat="server" FilterType="Numbers"
                                                        TargetControlID="Txt_No_Inventario">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 15%;">
                                                    &nbsp;&nbsp;
                                                    <asp:Label ID="Lbl_No_Economico" runat="server" Text="No. Economico"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_No_Economico" runat="server" Enabled="false" Width="98%"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Datos_Vehiculo" runat="server" Text="Vehículo"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Datos_Vehiculo" runat="server" Enabled="false" Width="99%"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Placas" runat="server" Text="Placas"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Placas" runat="server" Enabled="false" Width="98%"></asp:TextBox>
                                                </td>
                                                <td style="width: 15%;">
                                                    &nbsp;&nbsp;
                                                    <asp:Label ID="Lbl_Anio" runat="server" Text="Año"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Anio" runat="server" Enabled="false" Width="98%"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                        <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                                <td colspan="6" style="text-align: center;">
                                    <asp:Label ID="Lbl_Leyenda_Datos_Recepción" runat="server" Text="DATOS DE RECEPCIÓN"
                                        Font-Bold="true"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 17%;">
                                    <asp:Label ID="Lbl_Fecha_Recepcion_Programada" runat="server" Text="Fecha Programada"></asp:Label>
                                </td>
                                <td style="width: 16%;">
                                    <asp:TextBox ID="Txt_Fecha_Recepcion_Programada" runat="server" Width="95%" Enabled="false"></asp:TextBox>
                                </td>
                                <td style="width: 17%;">
                                    &nbsp;&nbsp;
                                    <asp:Label ID="Lbl_Fecha_Recepcion_Real" runat="server" Text="Fecha Real" Font-Bold="true"></asp:Label>
                                </td>
                                <td style="width: 17%;">
                                    <asp:TextBox ID="Txt_Fecha_Recepcion_Real" runat="server" Width="80%" Enabled="false"></asp:TextBox>
                                    <asp:ImageButton ID="Btn_Fecha_Recepcion_Real" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                    <cc1:CalendarExtender ID="CE_Txt_Fecha_Recepcion_Real" runat="server" TargetControlID="Txt_Fecha_Recepcion_Real"
                                        PopupButtonID="Btn_Fecha_Recepcion_Real" Format="dd/MMM/yyyy">
                                    </cc1:CalendarExtender>
                                </td>
                                <td style="width: 17%;">
                                    &nbsp;&nbsp;
                                    <asp:Label ID="Lbl_Kilometraje" runat="server" Text="Km Entrada" Font-Bold="true"></asp:Label>
                                </td>
                                <td style="width: 16%;">
                                    <asp:TextBox ID="Txt_Kilometraje" runat="server" Width="95%"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Kilometraje" runat="server" TargetControlID="Txt_Kilometraje"
                                        ValidChars="." FilterType="Custom, Numbers">
                                    </cc1:FilteredTextBoxExtender>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 17%;">
                                    <asp:Label ID="Lbl_Empleado_Entrega" runat="server" Text="Empleado Entrega" Font-Bold="true"></asp:Label>
                                    <asp:HiddenField runat="server" ID="Hdf_Empleado_Entrega_ID" />
                                </td>
                                <td colspan="5">
                                    <asp:TextBox ID="Txt_Empleado_Entrega" runat="server" Width="90%" Enabled="false"></asp:TextBox>
                                    <asp:ImageButton ID="Btn_Empleado_Entrega" runat="server" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png"
                                        AlternateText="Buscar Empleado" Height="16px" Width="16px" OnClick="Btn_Empleado_Entrega_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 17%;">
                                    <asp:Label ID="Lbl_Comentarios_Recepción" runat="server" Text="Comentarios" Font-Bold="true"></asp:Label>
                                </td>
                                <td colspan="5">
                                    <asp:TextBox ID="Txt_Comentarios_Recepción" runat="server" Width="98%" Rows="4" TextMode="MultiLine"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Comentarios_Recepción" runat="server" TargetControlID="Txt_Comentarios_Recepción"
                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ /*-+$%&_" Enabled="True">
                                    </cc1:FilteredTextBoxExtender>
                                </td>
                            </tr>
                        </table>
                    </div>
                </center>
            </div>
            <asp:HiddenField ID="Hdn_Frente" runat="server" />
            <asp:HiddenField ID="Hdn_Atras" runat="server" />
            <asp:HiddenField ID="Hdn_Izquierda" runat="server" />
            <asp:HiddenField ID="Hdn_Derecha" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
        <ContentTemplate>
                    <table id="Tbl_Fotos_Vehiculo" border="0" cellspacing="0" runat="server" width=600px>
                        <tr>
                            <td style="width:50%">
                                <asp:Panel ID="Pnl_Frente" runat="server" GroupingText="Frente">
                                    <table border="0" cellspacing="0">
                                        <tr>
                                            <td rowspan="2" style="width: 30%">
                                                <a href="" rel="facybox" runat="server" id="Liga_Frente" target="_blank">
                                                    <img src="" id="Liga_Imagen_Frente" alt="" runat="server" width="64" height="64" /></a>
                                            </td>
                                            <td style="text-align: center; width: 70%">
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left; width: 70%">
                                                <cc1:AsyncFileUpload OnClientUploadError="uploadError" OnClientUploadComplete="uploadComplete"
                                                    runat="server" ID="Fup_Frente" CompleteBackColor="White" UploadingBackColor="#CCFFFF" Width="130px"
                                                    ThrobberID="imgLoader" OnUploadedComplete="Fup_Frente_UploadedComplete" />
                                                <asp:Image ID="imgLoader" runat="server" ImageUrl="~/paginas/imagenes/paginas/Sias_Roler.gif" />
                                                <br />
                                                <asp:Label ID="lblMesg" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                            <td style="width: 50%">
                                <asp:Panel ID="Pnl_Atras" runat="server" GroupingText="Atras">
                                    <table border="0" cellspacing="0">
                                        <tr>
                                            <td rowspan="2" style="width:30%">
                                                <a href="" rel="facybox" runat="server" id="Liga_Atras" target="_blank">
                                                    <img src="" id="Liga_Imagen_Atras" alt="" runat="server" width="64" height="64" /></a>
                                            </td>
                                            <td style="text-align: center; width:70%">
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left; width: 70%">
                                                <cc1:AsyncFileUpload OnClientUploadError="uploadError" OnClientUploadComplete="uploadComplete_Back"
                                                    runat="server" ID="Fup_Atras" CompleteBackColor="White" UploadingBackColor="#CCFFFF" Width="130px"
                                                    ThrobberID="imgLoader_Back" OnUploadedComplete="Fup_Atras_UploadedComplete" />
                                                <asp:Image ID="imgLoader_Back" runat="server" ImageUrl="~/paginas/imagenes/paginas/Sias_Roler.gif" />
                                                <br />
                                                <asp:Label ID="lblMesg_Back" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 50%">
                                <asp:Panel ID="Pnl_Lado_Izq" runat="server" GroupingText="Lado Izquierdo">
                                    <table border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td rowspan="2" style="width: 50%">
                                                <a href="" rel="facybox" runat="server" id="Liga_Izquierdo" target="_blank">
                                                    <img src="" id="Liga_Imagen_Izquierdo" alt="" runat="server" width="64" height="64" /></a>
                                            </td>
                                            <td style="text-align: center; width: 70%">
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left; width: 50%">
                                                <cc1:AsyncFileUpload OnClientUploadError="uploadError" OnClientUploadComplete="uploadComplete_Left"
                                                    runat="server" ID="Fup_Izquierda" CompleteBackColor="White" UploadingBackColor="#CCFFFF" Width="130px"
                                                    ThrobberID="imgLoader_left" OnUploadedComplete="Fup_Izquierda_UploadedComplete" />
                                                <asp:Image ID="imgLoader_left" runat="server" ImageUrl="~/paginas/imagenes/paginas/Sias_Roler.gif" />
                                                <br />
                                                <asp:Label ID="lblMesg_Left" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                            <td style="width:50%">
                                <asp:Panel ID="Pnl_Lado_Der" runat="server" GroupingText="Lado Derecho">
                                    <table border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td rowspan="2" style="width: 50%">
                                                <a href="" rel="facybox" runat="server" id="Liga_Derecho" target="_blank">
                                                    <img src="" id="Liga_Imagen_Derecho" alt="" runat="server" width="64" height="64" /></a>
                                            </td>
                                            <td style="text-align: center; width: 50%">
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left; width:50%">
                                                <cc1:AsyncFileUpload OnClientUploadError="uploadError" OnClientUploadComplete="uploadComplete_Rigth"
                                                    runat="server" ID="Fup_Derecha" CompleteBackColor="White" UploadingBackColor="#CCFFFF"
                                                    ThrobberID="imgLoader_rigth" Width="130px"
                                                    OnUploadedComplete="Fup_Derecha_UploadedComplete" EnableViewState="False" />
                                                <asp:Image ID="imgLoader_rigth" runat="server" ImageUrl="~/paginas/imagenes/paginas/Sias_Roler.gif" />
                                                <br />
                                                <asp:Label ID="lblMesg_Rigth" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>            
        </ContentTemplate>
    </asp:UpdatePanel>    
    <asp:UpdatePanel ID="UpPnl_aux_Busqueda_Resguardante" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_MPE_Resguardante" runat="server" Text="" Style="display: none;" />
            <cc1:ModalPopupExtender ID="MPE_Resguardante" runat="server" TargetControlID="Btn_Comodin_MPE_Resguardante"
                PopupControlID="Pnl_Busqueda_Contenedor" CancelControlID="Btn_Cerrar_Ventana"
                 DropShadow="True"
                BackgroundCssClass="progressBackgroundFilter" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Pnl_Busqueda_Contenedor" runat="server" CssClass="drag" HorizontalAlign="Center"
        Width="850px" Style="display: none; border-style: outset; border-color: Silver;
        background-image: url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG); background-repeat: repeat-y;">
        <asp:Panel ID="Pnl_Busqueda_Resguardante_Cabecera" runat="server" Style="cursor: move;
            background-color: Silver; color: Black; font-size: 12; font-weight: bold; border-style: outset;">
            <table width="99%">
                <tr>
                    <td style="color: Black; font-size: 12; font-weight: bold;">
                        <asp:Image ID="Img_Informatcion_Autorizacion" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                        B&uacute;squeda: Empleados
                    </td>
                    <td align="right" style="width: 10%;">
                        <asp:ImageButton ID="Btn_Cerrar_Ventana" CausesValidation="false" runat="server"
                            Style="cursor: pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <div style="color: #5D7B9D">
            <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;">
                        <asp:UpdatePanel ID="Upnl_Filtros_Busqueda_Prestamos" runat="server">
                            <ContentTemplate>
                                <asp:UpdateProgress ID="Progress_Upnl_Filtros_Busqueda_Prestamos" runat="server"
                                    AssociatedUpdatePanelID="Upnl_Filtros_Busqueda_Prestamos" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                        </div>
                                        <div style="background-color: Transparent; position: fixed; top: 50%; left: 47%;
                                            padding: 10px; z-index: 1002;" id="div_progress">
                                            <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <table width="100%">
                                    <tr>
                                        <td style="width: 100%" colspan="4" align="right">
                                            <asp:ImageButton ID="Btn_Limpiar_Ctlr_Busqueda" runat="server" OnClientClick="javascript:return Limpiar_Ctlr_Busqueda_Resguardante();"
                                                ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de Busqueda" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%" colspan="4">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: left; font-size: 11px;">
                                            No Empleado
                                        </td>
                                        <td style="width: 30%; text-align: left; font-size: 11px;">
                                            <asp:TextBox ID="Txt_Busqueda_No_Empleado" runat="server" Width="98%" />
                                            <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_No_Empleado" runat="server" FilterType="Numbers"
                                                TargetControlID="Txt_Busqueda_No_Empleado" />
                                            <cc1:TextBoxWatermarkExtender ID="Twm_Txt_Busqueda_No_Empleado" runat="server" TargetControlID="Txt_Busqueda_No_Empleado"
                                                WatermarkText="Busqueda por No Empleado" WatermarkCssClass="watermarked" />
                                        </td>
                                        <td style="width: 20%; text-align: left; font-size: 11px;">
                                            RFC
                                        </td>
                                        <td style="width: 30%; text-align: left; font-size: 11px;">
                                            <asp:TextBox ID="Txt_Busqueda_RFC" runat="server" Width="98%" />
                                            <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_RFC" runat="server" FilterType="Numbers, UppercaseLetters"
                                                TargetControlID="Txt_Busqueda_RFC" />
                                            <cc1:TextBoxWatermarkExtender ID="Twm_Txt_Busqueda_RFC" runat="server" TargetControlID="Txt_Busqueda_RFC"
                                                WatermarkText="Busqueda por RFC" WatermarkCssClass="watermarked" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: left; font-size: 11px;">
                                            Nombre
                                        </td>
                                        <td style="width: 30%; text-align: left;" colspan="3">
                                            <asp:TextBox ID="Txt_Busqueda_Nombre_Empleado" runat="server" Width="99.5%" />
                                            <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_Nombre_Empleado" runat="server"
                                                FilterType="Custom, LowercaseLetters, Numbers, UppercaseLetters" TargetControlID="Txt_Busqueda_Nombre_Empleado"
                                                ValidChars="áéíóúÁÉÍÓÚ ñÑ" />
                                            <cc1:TextBoxWatermarkExtender ID="Twm_Nombre_Empleado" runat="server" TargetControlID="Txt_Busqueda_Nombre_Empleado"
                                                WatermarkText="Busqueda por Nombre" WatermarkCssClass="watermarked" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: left; font-size: 11px;">
                                            Unidad Responsable
                                        </td>
                                        <td style="width: 30%; text-align: left; font-size: 11px;" colspan="3">
                                            <asp:DropDownList ID="Cmb_Busqueda_Dependencia" runat="server" Width="100%" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%" colspan="4">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%; text-align: left;" colspan="4">
                                            <center>
                                                <asp:Button ID="Btn_Busqueda_Empleados" runat="server" Text="Busqueda de Empleados"
                                                    CssClass="button" CausesValidation="false" Width="200px" OnClick="Btn_Busqueda_Empleados_Click" />
                                            </center>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <div id="Div_Resultados_Busqueda_Resguardantes" runat="server" style="border-style: outset;
                                    width: 99%; height: 250px; overflow: auto;">
                                    <asp:GridView ID="Grid_Busqueda_Empleados_Resguardo" runat="server" AutoGenerateColumns="False"
                                        CellPadding="4" ForeColor="#333333" GridLines="None" AllowPaging="True" Width="100%"
                                        PageSize="100" EmptyDataText="No se encontrarón resultados para los filtros de la busqueda"
                                        OnSelectedIndexChanged="Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged"
                                        OnPageIndexChanging="Grid_Busqueda_Empleados_Resguardo_PageIndexChanging">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                <ItemStyle Width="30px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="EMPLEADO_ID" HeaderText="EMPLEADO_ID" SortExpression="EMPLEADO_ID">
                                                <ItemStyle Width="3px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                <HeaderStyle Width="3px" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NO_EMPLEADO" HeaderText="No. Empleado" SortExpression="NO_EMPLEADO">
                                                <ItemStyle Width="70px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                <HeaderStyle Width="70px" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NOMBRE" HeaderText="Nombre" SortExpression="NOMBRE" NullDisplayText="-">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" SortExpression="DEPENDENCIA"
                                                NullDisplayText="-">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>    
</asp:Content>
