﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Tal_Autorizacion_Solicitud_Verificacion.aspx.cs" Inherits="paginas_Taller_Mecanico_Frm_Ope_Tal_Autorizacion_Solicitud_Servicio" Title="Autorización de Solicitud de Servicio" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style type="text/css">
        .Tabla_Comentarios
        {
            border-collapse: collapse;
            margin-left: 25px;
            color:red;
            font-family: Verdana,Geneva,MS Sans Serif;
            font-size: small;
            text-align: left;
        }
        .Tabla_Comentarios, .Tabla_Comentarios th, .Tabla_Comentarios td
        {
            border: 1px solid #999999;
            padding: 2px 10px;
        }    
        .Tabla_Comentarios_Head
        {
            border-collapse: collapse;
            margin-left: 25px;
            color:#000000;
            background-color:#D6EBFF;
            font-family: Verdana,Geneva,MS Sans Serif;
            font-size: small;
            text-align: left;
        }    
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
   <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" 
    EnableScriptGlobalization="true"
        EnableScriptLocalization="true"
        EnablePartialRendering="true" 
        AsyncPostBackTimeout="9000">
</cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Loading" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>                     
            </asp:UpdateProgress>
            <div id="Div_Area_Trabajo" style="background-color:#ffffff; width:100%; height:100%;">
                <center>
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo" colspan="2">Autorización de Solicitudes de Verificación</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                          <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;">              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top">
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                            </table>                   
                          </div>                
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>                        
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" style="width:50%;">
                            <asp:ImageButton ID="Btn_Ejecutar_Autorizacion" runat="server" ImageUrl="~/paginas/imagenes/paginas/accept.png" Width="24px" CssClass="Img_Button" AlternateText="Ejecutar Operación" ToolTip="Ejecutar Operación" OnClick="Btn_Ejecutar_Autorizacion_Click" />
                            <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" Width="24px" CssClass="Img_Button" AlternateText="Salir" ToolTip="Salir" OnClick="Btn_Salir_Click"/>
                        </td>
                        <td style="width:50%;">&nbsp;                                 
                        </td>                        
                    </tr>
                </table>   
                <br />
                <div id="Div_Listado_Solicitudes" runat="server" style="width:100%;">
                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                             <td style="text-align:right;" colspan="4">
                                 <asp:ImageButton ID="Btn_Actualizar_Listado" runat="server" ToolTip="Actualizar Listado" AlternateText="Actualizar Listado" OnClick="Btn_Actualizar_Listado_Click" ImageUrl="~/paginas/imagenes/paginas/actualizar_detalle.png" Width="16px" />
                             </td>
                        </tr>
                    </table>
                    <br />
                    <asp:GridView ID="Grid_Listado_Solicitudes" runat="server" CssClass="GridView_1"
                        AutoGenerateColumns="False" AllowPaging="True" PageSize="20" Width="99%"
                        GridLines= "None" EmptyDataText="No se Encontrarón Solicitudes Pendientes."
                        onselectedindexchanged="Grid_Listado_Solicitudes_SelectedIndexChanged" 
                        onpageindexchanging="Grid_Listado_Solicitudes_PageIndexChanging">
                        <RowStyle CssClass="GridItem" />
                        <Columns>
                            <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png" >
                                <ItemStyle Width="30px" />
                            </asp:ButtonField>
                            <asp:BoundField DataField="NO_SOLICITUD" HeaderText="NO_SOLICITUD" SortExpression="NO_SOLICITUD">
                                <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FECHA_ELABORACION" HeaderText="F. Elaboración" SortExpression="FECHA_ELABORACION" DataFormatString="{0:dd/MMM/yyyy}">
                                <ItemStyle Width="110px" HorizontalAlign="Center" Font-Size="X-Small"/>
                            </asp:BoundField>
                            <asp:BoundField DataField="TIPO_SERVICIO" HeaderText="Tipo Servicio" SortExpression="TIPO_SERVICIO" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle Width="160px" HorizontalAlign="Center" Font-Size="X-Small"/>
                            </asp:BoundField>
                            <asp:BoundField DataField="NO_INVENTARIO" HeaderText="No. Inventario" SortExpression="NO_INVENTARIO">
                                <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small"/>
                            </asp:BoundField>
                            <asp:BoundField DataField="NO_ECONOMICO" HeaderText="No. Economico" SortExpression="NO_ECONOMICO">
                                <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small"/>
                            </asp:BoundField>
                            <asp:BoundField DataField="DESCRIPCION_SERVICIO" HeaderText="Descripción" SortExpression="DESCRIPCION_SERVICIO">
                                <ItemStyle Font-Size="X-Small"/>
                            </asp:BoundField>
                            <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS">
                                <ItemStyle Width="80px" HorizontalAlign="Center" Font-Size="X-Small"/>
                            </asp:BoundField>
                        </Columns>
                        <PagerStyle CssClass="GridHeader" />
                        <SelectedRowStyle CssClass="GridSelected" />
                        <HeaderStyle CssClass="GridHeader" />                                
                        <AlternatingRowStyle CssClass="GridAltItem" />       
                    </asp:GridView>
                </div>                
                <div id="Div_Campos" runat="server" style="width:100%;">                
                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                            <td colspan="4">
                                <asp:HiddenField ID="Hdf_No_Solicitud" runat="server" />
                                <asp:HiddenField ID="Hdf_Correo_Electronico" runat="server" />
                                <asp:HiddenField ID="Hdf_Tipo_Solicitud" runat="server" />
                                <asp:HiddenField ID="Hdf_Numero_Servicio" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%;">
                                <asp:Label ID="Lbl_Fecha_Elaboracion" runat="server" Text="Fecha Elaboración"></asp:Label>
                            </td>
                            <td style="width:35%;">
                                <asp:TextBox ID="Txt_Fecha_Elaboracion" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%;">
                                <asp:Label ID="Lbl_Grupo_UR" runat="server" Text="Gerencia"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="Cmb_Grupo_UR" runat="server" Width="100%" Enabled="false">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%;">
                                <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text="Unidad Responsable"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="Cmb_Unidad_Responsable" runat="server" Width="100%" Enabled="false">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%;">
                                <asp:Label ID="Lbl_Tipo_Bien" runat="server" Text="Tipo Bien"></asp:Label>
                            </td>
                            <td style="width:35%;">
                                <asp:TextBox ID="Txt_Tipo_Bien" runat="server" Width="98%" Enabled="false">
                                </asp:TextBox>
                            </td>
                            <td style="width:15%;">
                                &nbsp;&nbsp;  
                                <asp:Label ID="Lbl_Tipo_Servicio" runat="server" Text="Tipo Servicio"></asp:Label>
                            </td>
                            <td style="width:35%;">
                                <asp:DropDownList ID="Cmb_Tipo_Servicio" runat="server" Width="100%" Enabled="false">
                                    <asp:ListItem Value="">&lt; - - SELECCIONE - - &gt;</asp:ListItem>
                                    <asp:ListItem Value="REVISTA_MECANICA">REVISTA MECANICA</asp:ListItem>
                                    <asp:ListItem Value="SERVICIO_CORRECTIVO">SERVICIO CORRECTIVO</asp:ListItem>
                                    <asp:ListItem Value="SERVICIO_PREVENTIVO">SERVICIO PREVENTIVO</asp:ListItem>
                                    <asp:ListItem Value="VERIFICACION">VERIFICACI&Oacute;N</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Panel ID="Pnl_Vehiculo_Seleccionado" runat="server" Width="99%" GroupingText="Vehículo para el Servicio">
                                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:HiddenField ID="Hdf_Vehiculo_ID" runat="server" />
                                                <asp:Label ID="Lbl_No_Inventario" runat="server" Text="No. Inventario"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_No_Inventario" runat="server" Width="70%" MaxLength="7" Enabled="false"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Inventario" runat="server" TargetControlID="Txt_No_Inventario" FilterType="Numbers">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width:15%;">
                                                &nbsp;&nbsp;  
                                                <asp:Label ID="Lbl_No_Economico" runat="server" Text="No. Economico"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_No_Economico" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:Label ID="Lbl_Datos_Vehiculo" runat="server" Text="Vehículo"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="Txt_Datos_Vehiculo" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:Label ID="Lbl_Placas" runat="server" Text="Placas"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_Placas" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                            </td>
                                            <td style="width:15%;">
                                                &nbsp;&nbsp;  
                                                <asp:Label ID="Lbl_Anio" runat="server" Text="Año"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_Anio" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:Label ID="Lbl_Kilometraje" runat="server" Text="Kilometraje [Km]" ></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_Kilometraje" runat="server" Width="95%" Enabled="false"></asp:TextBox>   
                                            </td>
                                            <td style="width:15%;">
                                            </td>
                                            <td style="width:35%;">
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="Pnl_Bien_Mueble_Seleccionado" runat="server" 
                                        GroupingText="Bien Mueble para el Servicio" Width="99%">
                                        <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                                            <tr>
                                                <td style="width:15%;">
                                                    <asp:HiddenField ID="Hdf_Bien_Mueble_ID" runat="server" />
                                                    <asp:Label ID="Lbl_No_Inventario_BM" runat="server" Text="No. Inventario"></asp:Label>
                                                </td>
                                                <td style="width:35%;">
                                                    <asp:TextBox ID="Txt_No_Inventario_BM" runat="server" MaxLength="7" Width="98%" Enabled="false"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Inventario_BM" runat="server" 
                                                        FilterType="Numbers" TargetControlID="Txt_No_Inventario_BM">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width:15%;">
                                                    &nbsp;
                                                </td>
                                                <td style="width:35%;">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:15%;">
                                                    <asp:Label ID="Lbl_Descripcion_Bien" runat="server" Text="Descripción Bien"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Descripcion_Bien" runat="server" Enabled="false" Rows="2" 
                                                        TextMode="MultiLine" Width="99%"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:15%;">
                                                    <asp:Label ID="Lbl_Numero_Serie_Bien" runat="server" Text="No. Serie"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Numero_Serie_Bien" runat="server" Enabled="false" 
                                                        Width="99%"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                             <td colspan="4">&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Panel ID="Pnl_Descripcion_Servicio" runat="server" Width="99%" GroupingText="Descripción del Servicio">
                                    <asp:TextBox ID="Txt_Descripcion_Servicio" runat="server" Rows="5" TextMode="MultiLine" Width="99%" Enabled="false"></asp:TextBox>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                             <td colspan="4">&nbsp;&nbsp;</td>
                        </tr>
                         <tr>
                            <td colspan="4" align="left">                            
                                <asp:GridView ID="Grid_Tarjeta_Informativa" runat="server" AllowPaging="false"
                                    AutoGenerateColumns="False" CssClass="Tabla_Comentarios" HeaderStyle-CssClass="tblHead"
                                    PageSize="5" Style="white-space: normal;"
                                    Width="93%">
                                    <Columns>
                                        <asp:BoundField DataField="CAMBIO" HeaderText="Tarjeta Informativa">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="30%" HorizontalAlign="Left" />
                                        </asp:BoundField>                                        
                                        <asp:BoundField DataField="FECHA_CAMBIO" HeaderText="Fecha">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="70%"/>
                                        </asp:BoundField>
                                    </Columns>
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <PagerStyle CssClass="GridHeader" />
                                    <HeaderStyle CssClass="Tabla_Comentarios_Head" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>                                
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Panel ID="Pnl_Datos_Autorizacion" runat="server" Width="99%" GroupingText="Autorización">
                                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:Label ID="Lbl_Resultado" runat="server" Text="Resultado" Font-Bold="true" ></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:DropDownList ID="Cmb_Resultado" runat="server" Width="80%" AutoPostBack="true" OnSelectedIndexChanged="Cmb_Resultado_SelectedIndexChanged"> 
                                                    <asp:ListItem Value="">&lt; - - SELECCIONE - - &gt;</asp:ListItem>
                                                    <asp:ListItem Value="AUTORIZADA">AUTORIZADA</asp:ListItem>
                                                    <asp:ListItem Value="RECHAZADA">RECHAZADA</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width:15%;">
                                                <asp:Label ID="Lbl_Fecha_Recepcion" runat="server" Text="Fecha Recepción" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_Fecha_Recepcion" runat="server" Width="90%" Enabled="false"></asp:TextBox>
                                                <asp:ImageButton ID="Btn_Fecha_Recepcion" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                                <cc1:CalendarExtender ID="CE_Txt_Fecha_Recepcion" runat="server" TargetControlID="Txt_Fecha_Recepcion" PopupButtonID="Btn_Fecha_Recepcion" Format="dd/MMM/yyyy">
                                                </cc1:CalendarExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:Label ID="Lbl_Comentarios" runat="server" Text="Comentarios" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="Txt_Comentarios" runat="server" Width="99%" Rows="2" TextMode="MultiLine" ></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Comentarios" runat="server" TargetControlID="Txt_Comentarios" InvalidChars="<,>,&,',!," 
                                                                            FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ -*/_">
                                                </cc1:FilteredTextBoxExtender>    
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </div>
                </center>        
            </div>
            <br />
            <br />
            <br />
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

