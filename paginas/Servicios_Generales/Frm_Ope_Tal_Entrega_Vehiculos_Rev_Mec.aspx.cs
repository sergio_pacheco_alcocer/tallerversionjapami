﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Entradas_Vehiculos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Salidas_Vehiculos.Negocio;
using JAPAMI.Taller_Mecanico.Catalogo_Partes_Vehiculos.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Control_Patrimonial_Operacion_Vehiculos.Negocio;
using JAPAMI.Empleados.Negocios;
using System.Text.RegularExpressions;
using JAPAMI.Taller_Mecanico.Operacion_Revista_Mecanica.Negocio;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Text;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;

public partial class paginas_Taller_Mecanico_Frm_Ope_Tal_Entrega_Vehiculos_Rev_Mec : System.Web.UI.Page{
  
    #region Page_Load
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN: Carga la Pagina Inicial
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 05/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e) {
            Cargar_Imagenes();
            Div_Contenedor_Msj_Error.Visible = false;
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                Llenar_Combo_Unidades_Responsables();
                Grid_Listado_Solicitudes.PageIndex = 0;
                Llenar_Listado_Solicitudes();
                Configuracion_Formulario("INICIAL");
                Cmb_Unidad_Responsable.Enabled = false;
                Cmb_Busqueda_Dependencia.Enabled = false;
            }
                            
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Cargar_Imagenes
        ///DESCRIPCIÓN: Carga las imagenes a mostrar
        ///PROPIEDADES:     
        ///CREO: Jesus Toledo Rdz.
        ///FECHA_CREO: 12/Julio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Cargar_Imagenes()
        {
            DataTable Img;
            if (Session["Tabla_imagenes_Salida"] != null)
            {
                Img = (DataTable)Session["Tabla_imagenes_Salida"];
                foreach (DataRow Renglon in Img.Rows)
                {
                    if (Renglon["RUTA_ARCHIVO"] != null)
                    {
                        switch (Renglon["FOTO"].ToString())
                        {
                            case "FRENTE":
                                if (!String.IsNullOrEmpty(Renglon["RUTA_ARCHIVO"].ToString()))
                                {
                                    Liga_Frente.HRef = Renglon["RUTA_ARCHIVO"].ToString();
                                    Liga_Imagen_Frente.Src = Renglon["RUTA_ARCHIVO"].ToString();
                                }
                                else
                                {
                                    Liga_Frente.HRef = "../imagenes/paginas/vehicle_front_view.png";
                                    Liga_Imagen_Frente.Src = "../imagenes/paginas/vehicle_front_view.png";
                                }
                                break;
                            case "ATRAS":
                                if (!String.IsNullOrEmpty(Renglon["RUTA_ARCHIVO"].ToString()))
                                {
                                    Liga_Atras.HRef = Renglon["RUTA_ARCHIVO"].ToString();
                                    Liga_Imagen_Atras.Src = Renglon["RUTA_ARCHIVO"].ToString();
                                }
                                else
                                {
                                    Liga_Atras.HRef = "../imagenes/paginas/vehicle_back_view.png";
                                    Liga_Imagen_Atras.Src = "../imagenes/paginas/vehicle_back_view.png";
                                }
                                break;
                            case "IZQUIERDA":
                                if (!String.IsNullOrEmpty(Renglon["RUTA_ARCHIVO"].ToString()))
                                {
                                    Liga_Izquierdo.HRef = Renglon["RUTA_ARCHIVO"].ToString();
                                    Liga_Imagen_Izquierdo.Src = Renglon["RUTA_ARCHIVO"].ToString();
                                }
                                else
                                {
                                    Liga_Izquierdo.HRef = "../imagenes/paginas/vehicle_left_view.png";
                                    Liga_Imagen_Izquierdo.Src = "../imagenes/paginas/vehicle_left_view.png";
                                }
                                break;
                            case "DERECHA":
                                if (!String.IsNullOrEmpty(Renglon["RUTA_ARCHIVO"].ToString()))
                                {
                                    Liga_Derecho.HRef = Renglon["RUTA_ARCHIVO"].ToString();
                                    Liga_Imagen_Derecho.Src = Renglon["RUTA_ARCHIVO"].ToString();
                                }
                                else
                                {
                                    Liga_Derecho.HRef = "../imagenes/paginas/vehicle_rigth_view.png";
                                    Liga_Imagen_Derecho.Src = "../imagenes/paginas/vehicle_rigth_view.png";
                                }
                                break;

                        }
                    }
                }
            }
            else
            {
                Liga_Frente.HRef = "../imagenes/paginas/vehicle_front_view.png";
                Liga_Imagen_Frente.Src = "../imagenes/paginas/vehicle_front_view.png";
                Liga_Atras.HRef = "../imagenes/paginas/vehicle_back_view.png";
                Liga_Imagen_Atras.Src = "../imagenes/paginas/vehicle_back_view.png";
                Liga_Izquierdo.HRef = "../imagenes/paginas/vehicle_left_view.png";
                Liga_Imagen_Izquierdo.Src = "../imagenes/paginas/vehicle_left_view.png";
                Liga_Derecho.HRef = "../imagenes/paginas/vehicle_rigth_view.png";
                Liga_Imagen_Derecho.Src = "../imagenes/paginas/vehicle_rigth_view.png";
            }
        }
    #endregion

    #region Metodos

        #region Llenado de Campos [Combos, Listados, Vehiculos]

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Unidades_Responsables
            ///DESCRIPCIÓN: Se llena el Combo de las Unidades Responsables.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Combo_Unidades_Responsables() {
                Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                Negocio.P_Estatus = "ACTIVO";
                DataTable Dt_Dependencias = Negocio.Consultar_Unidades_Responsables();
                Cmb_Unidad_Responsable.DataSource = Dt_Dependencias;
                Cmb_Unidad_Responsable.DataTextField = "CLAVE_NOMBRE";
                Cmb_Unidad_Responsable.DataValueField = "DEPENDENCIA_ID";
                Cmb_Unidad_Responsable.DataBind();
                Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
                Cmb_Busqueda_Dependencia.DataSource = Dt_Dependencias;
                Cmb_Busqueda_Dependencia.DataTextField = "CLAVE_NOMBRE";
                Cmb_Busqueda_Dependencia.DataValueField = "DEPENDENCIA_ID";
                Cmb_Busqueda_Dependencia.DataBind();
                Cmb_Busqueda_Dependencia.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Solicitudes
            ///DESCRIPCIÓN: Se llena el Listado de las Solicitudes.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Listado_Solicitudes() {
                Cls_Ope_Tal_Revista_Mecanica_Negocio Rev_Correc = new Cls_Ope_Tal_Revista_Mecanica_Negocio();
                Rev_Correc.P_Estatus = "TERMINADO";
                Rev_Correc.P_Estatus_Solicitud = "EN_REPARACION";
                DataTable Dt_Resultados = Rev_Correc.Consultar_Revistas_Mecanicas();

                Grid_Listado_Solicitudes.Columns[1].Visible = true;
                Grid_Listado_Solicitudes.Columns[2].Visible = true;
                Grid_Listado_Solicitudes.Columns[3].Visible = true;
                Grid_Listado_Solicitudes.DataSource = Dt_Resultados;
                Grid_Listado_Solicitudes.DataBind();
                Grid_Listado_Solicitudes.Columns[1].Visible = false;
                Grid_Listado_Solicitudes.Columns[2].Visible = false;
                Grid_Listado_Solicitudes.Columns[3].Visible = false;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Vehiculo
            ///DESCRIPCIÓN: Se cargan los Datos del Vehiculo Seleccionado.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Cargar_Datos_Vehiculo(String Vehiculo, String Tipo_Busqueda) {
                Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                switch (Tipo_Busqueda) { 
                    case "NO_INVENTARIO":
                        Consulta_Negocio.P_No_Inventario = Vehiculo;
                        break;
                    case "IDENTIFICADOR":
                        Consulta_Negocio.P_Vehiculo_ID = Vehiculo;
                        break;
                    default: break;
                }
                if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Consulta_Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value; }
                DataTable Dt_Vehiculo = Consulta_Negocio.Consultar_Vehiculos();
                if (Dt_Vehiculo.Rows.Count > 0) {
                    Hdf_Vehiculo_ID.Value = Dt_Vehiculo.Rows[0]["VEHICULO_ID"].ToString().Trim();
                    Txt_No_Inventario.Text = Dt_Vehiculo.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                    Txt_No_Economico.Text = Dt_Vehiculo.Rows[0]["NO_ECONOMICO"].ToString().Trim();
                    Txt_Datos_Vehiculo.Text = Dt_Vehiculo.Rows[0]["VEHICULO_DESCRIPCION"].ToString().Trim();
                    Txt_Placas.Text = Dt_Vehiculo.Rows[0]["PLACAS"].ToString().Trim();
                    Txt_Anio.Text = Dt_Vehiculo.Rows[0]["ANIO"].ToString().Trim();
                    Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString()));
                } else {
                    Lbl_Ecabezado_Mensaje.Text = "El Vehículo no se encontro.";
                    if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Lbl_Mensaje_Error.Text = "[Puede ser que no este asignado a la Unidad Responsable Seleccionada]."; }
                    else { Lbl_Mensaje_Error.Text = ""; }
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Busqueda_Empleados_Resguardo
            ///DESCRIPCIÓN: Llena el Grid con los empleados que cumplan el filtro
            ///PROPIEDADES:     
            ///CREO:                 
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private void Llenar_Grid_Busqueda_Empleados_Resguardo() {
                Grid_Busqueda_Empleados_Resguardo.SelectedIndex = (-1);
                Grid_Busqueda_Empleados_Resguardo.Columns[1].Visible = true;
                Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                if (Txt_Busqueda_No_Empleado.Text.Trim().Length > 0) { Negocio.P_No_Empleado = Txt_Busqueda_No_Empleado.Text.Trim(); }
                if (Txt_Busqueda_RFC.Text.Trim().Length > 0) { Negocio.P_RFC_Empleado = Txt_Busqueda_RFC.Text.Trim(); }
                if (Txt_Busqueda_Nombre_Empleado.Text.Trim().Length > 0) { Negocio.P_Nombre_Empleado = Txt_Busqueda_Nombre_Empleado.Text.Trim().ToUpper(); }
                if (Cmb_Busqueda_Dependencia.SelectedIndex > 0) { Negocio.P_Dependencia_ID = Cmb_Busqueda_Dependencia.SelectedItem.Value; }
                Grid_Busqueda_Empleados_Resguardo.DataSource = Negocio.Consultar_Empleados();
                Grid_Busqueda_Empleados_Resguardo.DataBind();
                Grid_Busqueda_Empleados_Resguardo.Columns[1].Visible = false;
            }

        #endregion

        #region Generales [Configuracion, Limpiar]
                
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
            ///DESCRIPCIÓN: Limpia los campos del Formulario.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Limpiar_Formulario() {
                Hdf_No_Solicitud.Value = "";
                Hdf_No_Servicio.Value = "";
                Hdf_No_Entrada.Value = "";
                Txt_Folio.Text = "";
                Txt_Fecha_Elaboracion.Text = "";
                Cmb_Tipo_Servicio.SelectedIndex = 0;
                Cmb_Unidad_Responsable.SelectedIndex = 0;
                Txt_Km_Solicitud.Text = "";
                Txt_Descripcion_Servicio.Text = "";
                Hdf_Vehiculo_ID.Value = "";
                Txt_No_Inventario.Text = "";
                Txt_No_Economico.Text = "";
                Txt_Datos_Vehiculo.Text = "";
                Txt_Placas.Text = "";
                Txt_Anio.Text = "";
                Txt_Fecha_Recepcion_Programada.Text = "";
                Txt_Fecha_Recepcion_Real.Text = "";
                Hdf_Empleado_Entrega_ID.Value = "";
                Txt_Empleado_Entrega.Text = "";
                Txt_Kilometraje.Text = "";
                Txt_Comentarios_Recepción.Text = "";
                Txt_Fecha_Entrega.Text = "";
                Hdf_Empleado_Recibe.Value = "";
                Txt_Empleado_Recibe.Text = "";
                Txt_Kilometraje_Entrega.Text = "";
                Txt_Comentarios_Entrega.Text = "";
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
            ///DESCRIPCIÓN: Se Habilitan y/o inhabilitan los campos dependendiendo de la Condicion.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Configuracion_Formulario(String Operacion) {
                switch (Operacion) {
                    case "INICIAL":
                        Div_Campos.Visible = false;
                        Div_Listado_Solicitudes.Visible = true;
                        Btn_Hacer_Entrega.Visible = false;
                        Tbl_Fotos_Vehiculo.Style.Add("display", "none");
                        break;
                    case "OPERACION":
                        Div_Campos.Visible = true;
                        Div_Listado_Solicitudes.Visible = false;
                        Btn_Hacer_Entrega.Visible = true;
                        Tbl_Fotos_Vehiculo.Style.Add("display", "inline");
                        break;
                }
            }

        #endregion

        #region Clase de Negocio de Solicitudes [Registrar y Consulta]

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Mostrar_Registro
            ///DESCRIPCIÓN: Muestra el Registro en los campos.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Mostrar_Registro() {
                Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();                
                Solicitud.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                Solicitud = Solicitud.Consultar_Detalles_Solicitud_Servicio();
                
                if (Solicitud.P_No_Solicitud > (-1)) 
                {
                    Txt_Folio.Text = Solicitud.P_Folio_Solicitud.Trim();
                    Txt_Fecha_Elaboracion.Text = String.Format("{0:dd/MMM/yyyy}", Solicitud.P_Fecha_Elaboracion);
                    Cmb_Tipo_Servicio.SelectedIndex = Cmb_Tipo_Servicio.Items.IndexOf(Cmb_Tipo_Servicio.Items.FindByValue(Solicitud.P_Tipo_Servicio));
                    Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Solicitud.P_Dependencia_ID));
                    //Cmb_Busqueda_Dependencia.SelectedIndex = Cmb_Busqueda_Dependencia.Items.IndexOf(Cmb_Busqueda_Dependencia.Items.FindByValue(Solicitud.P_Dependencia_ID));
                    Hdf_Vehiculo_ID.Value = Solicitud.P_Bien_ID;
                    Txt_Descripcion_Servicio.Text = Solicitud.P_Descripcion_Servicio;
                    if (Solicitud.P_Kilometraje > (-1.0)) {
                        Txt_Km_Solicitud.Text = String.Format("{0:############0.00}", Solicitud.P_Kilometraje);
                    }
                    Cargar_Datos_Vehiculo(Hdf_Vehiculo_ID.Value, "IDENTIFICADOR");
                    Txt_Fecha_Recepcion_Programada.Text = String.Format("{0:dd/MMM/yyyy}", Solicitud.P_Fecha_Recepcion_Programada);
                    Txt_Fecha_Recepcion_Real.Text = String.Format("{0:dd/MMM/yyyy}", Solicitud.P_Fecha_Recepcion_Real);
                }
                if (Hdf_No_Entrada.Value.Trim().Length > 0) {
                    Cls_Ope_Tal_Entradas_Vehiculos_Negocio Entr_Negocio = new Cls_Ope_Tal_Entradas_Vehiculos_Negocio();
                    Entr_Negocio.P_No_Entrada = Convert.ToInt32(Hdf_No_Entrada.Value);
                    Entr_Negocio = Entr_Negocio.Consultar_Detalles_Entrada_Vehiculo();
                    if (Entr_Negocio.P_No_Entrada > (-1)) {
                        Txt_Kilometraje.Text = String.Format("{0:#,###,##0.00}", Entr_Negocio.P_Kilometraje);
                        Mostrar_Informacion_Empleado(Entr_Negocio.P_Empleado_Entrego_ID.Trim());
                        Txt_Comentarios_Recepción.Text = Entr_Negocio.P_Comentarios.Trim();
                    }
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Mostrar_Informacion_Empleado
            ///DESCRIPCIÓN: Muestra los Generales del Empleado
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Mostrar_Informacion_Empleado(String Empleado_ID) {
                Hdf_Empleado_Entrega_ID.Value = "";
                Txt_Empleado_Entrega.Text = "";
                Cls_Cat_Empleados_Negocios Empleado_Negocio = new Cls_Cat_Empleados_Negocios();
                Empleado_Negocio.P_Empleado_ID = Empleado_ID;
                DataTable Dt_Datos_Empleado = Empleado_Negocio.Consulta_Empleados_General();
                Hdf_Empleado_Entrega_ID.Value = ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString().Trim() : null);
                Txt_Empleado_Entrega.Text += ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString().Trim() : null);
                Txt_Empleado_Entrega.Text += " - " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString().Trim() : null);
                Txt_Empleado_Entrega.Text = Txt_Empleado_Entrega.Text.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString().Trim() : null);
                Txt_Empleado_Entrega.Text = Txt_Empleado_Entrega.Text.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString().Trim() : null);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Mostrar_Informacion_Empleado_Recibe
            ///DESCRIPCIÓN: Muestra los Generales del Empleado
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Mostrar_Informacion_Empleado_Recibe(String Empleado_ID) {
                Hdf_Empleado_Recibe.Value = "";
                Txt_Empleado_Recibe.Text = "";
                Cls_Cat_Empleados_Negocios Empleado_Negocio = new Cls_Cat_Empleados_Negocios();
                Empleado_Negocio.P_Empleado_ID = Empleado_ID;
                DataTable Dt_Datos_Empleado = Empleado_Negocio.Consulta_Empleados_General();
                Hdf_Empleado_Recibe.Value = ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString().Trim() : null);
                Txt_Empleado_Recibe.Text += ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString().Trim() : null);
                Txt_Empleado_Recibe.Text += " - " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString().Trim() : null);
                Txt_Empleado_Recibe.Text = Txt_Empleado_Recibe.Text.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString().Trim() : null);
                Txt_Empleado_Recibe.Text = Txt_Empleado_Recibe.Text.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString().Trim() : null);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Registrar_Entrega_Registro
            ///DESCRIPCIÓN: Registrar Entrega de Registro.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Registrar_Entrega_Registro() {
                DataTable Dt_Archivos;
                if (Session["Tabla_imagenes_Salida"] != null)
                {
                    Dt_Archivos = (DataTable)Session["Tabla_imagenes_Salida"];
                }
                else
                {
                    Dt_Archivos = Generar_Tabla_Documentos();
                }
                Cls_Ope_Tal_Salidas_Vehiculos_Negocio Sal_Negocio = new Cls_Ope_Tal_Salidas_Vehiculos_Negocio();                
                Sal_Negocio.P_Fecha_Salida = Convert.ToDateTime(Txt_Fecha_Entrega.Text);
                Sal_Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                Sal_Negocio.P_Tipo_Salida = "SOLICITUD";
                Sal_Negocio.P_Tipo_Solicitud = Cmb_Tipo_Servicio.SelectedItem.Value;
                Sal_Negocio.P_No_Entrada = Convert.ToInt32(Hdf_No_Entrada.Value);
                Sal_Negocio.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                Sal_Negocio.P_Vehiculo_ID = Hdf_Vehiculo_ID.Value.Trim();
                Sal_Negocio.P_Empleado_Entrego_ID = Cls_Sessiones.Empleado_ID;
                Sal_Negocio.P_Comentarios = Txt_Comentarios_Recepción.Text.Trim();
                if (Txt_Kilometraje_Entrega.Text.Trim().Length > 0) { Sal_Negocio.P_Kilometraje = Convert.ToDouble(Txt_Kilometraje_Entrega.Text); }
                Sal_Negocio.P_Empleado_Recibio_ID = Hdf_Empleado_Recibe.Value.Trim();
                Sal_Negocio.P_Dt_Detalles = new DataTable();
                Sal_Negocio.P_Dt_Archivos = Dt_Archivos;
                Sal_Negocio.Alta_Salida_Vehiculo();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "GACO", "alert('Operacion Exitosa: Entrega de Vehículo a Unidad Responsable');", true);
                try
                {
                    //disparara el reporte
                    String Ruta = "";
                    Sal_Negocio.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                    DataSet Ds_Consulta_Reporte = Sal_Negocio.Consultar_Datos_Reporte_Salida_Revista();
                    DataTable Dt_Salidas = new DataTable();
                    DataTable Dt_Entradas = new DataTable();
                    DataSet Ds_Reporte = new Ds_Rpt_Tal_Salidas_Revista();
                    Dt_Salidas.TableName = "DT_FOTOS_SALIDA";
                    DataSet Ds_Final = new DataSet();
                    foreach (DataRow Fila_Actual in Ds_Consulta_Reporte.Tables["DT_FOTOS_SALIDA"].Rows)
                    {
                        try
                        {
                            DataRow Fila = Ds_Reporte.Tables["DT_FOTOS_SALIDA"].NewRow();
                            try
                            {
                                Ruta = Server.MapPath(Fila_Actual["FOTO_SALIDA_FRENTE"].ToString());
                                Fila["FOTO_SALIDA_FRENTE"] = Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image.FromFile(Ruta));
                            }
                            catch { Fila["FOTO_SALIDA_FRENTE"] = Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image.FromFile(Server.MapPath("../imagenes/paginas/vehicle_front_view.png"))); }
                            try
                            {
                                Ruta = Server.MapPath(Fila_Actual["FOTO_SALIDA_ATRAS"].ToString());
                                Fila["FOTO_SALIDA_ATRAS"] = Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image.FromFile(Ruta));
                            }
                            catch { Fila["FOTO_SALIDA_ATRAS"] = Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image.FromFile(Server.MapPath("../imagenes/paginas/vehicle_back_view.png"))); }
                            try
                            {
                                Ruta = Server.MapPath(Fila_Actual["FOTO_SALIDA_IZQUIERDA"].ToString());
                                Fila["FOTO_SALIDA_IZQUIERDA"] = Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image.FromFile(Ruta));
                            }
                            catch { Fila["FOTO_SALIDA_IZQUIERDA"] = Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image.FromFile(Server.MapPath("../imagenes/paginas/vehicle_left_view.png"))); }
                            try
                            {
                                Ruta = Server.MapPath(Fila_Actual["FOTO_SALIDA_DERECHA"].ToString());
                                Fila["FOTO_SALIDA_DERECHA"] = Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image.FromFile(Ruta));
                            }
                            catch { Fila["FOTO_SALIDA_FRENTE"] = Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image.FromFile(Server.MapPath("../imagenes/paginas/vehicle_rigth_view.png"))); }

                            Ds_Reporte.Tables["DT_FOTOS_SALIDA"].Rows.Add(Fila);
                        }
                        catch { }
                    }

                    foreach (DataRow Fila_Actual in Ds_Consulta_Reporte.Tables["DT_FOTOS_ENTRADA"].Rows)
                    {
                        DataRow Fila = Ds_Reporte.Tables["DT_FOTOS_ENTRADA"].NewRow();
                        try
                        {
                            Ruta = Server.MapPath(Fila_Actual["FOTO_ENTRADA_FRENTE"].ToString());
                            Fila["FOTO_ENTRADA_FRENTE"] = Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image.FromFile(Ruta));
                        }
                        catch { Fila["FOTO_ENTRADA_FRENTE"] = Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image.FromFile(Server.MapPath("../imagenes/paginas/vehicle_front_view.png"))); }
                        try
                        {
                            Ruta = Server.MapPath(Fila_Actual["FOTO_ENTRADA_ATRAS"].ToString());
                            Fila["FOTO_ENTRADA_ATRAS"] = Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image.FromFile(Ruta));
                        }
                        catch { Fila["FOTO_ENTRADA_ATRAS"] = Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image.FromFile(Server.MapPath("../imagenes/paginas/vehicle_back_view.png"))); }
                        try
                        {
                            Ruta = Server.MapPath(Fila_Actual["FOTO_ENTRADA_IZQUIERDA"].ToString());
                            Fila["FOTO_ENTRADA_IZQUIERDA"] = Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image.FromFile(Ruta));
                        }
                        catch { Fila["FOTO_ENTRADA_IZQUIERDA"] = Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image.FromFile(Server.MapPath("../imagenes/paginas/vehicle_left_view.png"))); }
                        try
                        {
                            Ruta = Server.MapPath(Fila_Actual["FOTO_ENTRADA_DERECHA"].ToString());
                            Fila["FOTO_ENTRADA_DERECHA"] = Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image.FromFile(Ruta));
                        }
                        catch { Fila["FOTO_ENTRADA_DERECHA"] = Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image.FromFile(Server.MapPath("../imagenes/paginas/vehicle_rigth_view.png"))); }
                        Ds_Reporte.Tables["DT_FOTOS_ENTRADA"].Rows.Add(Fila);
                    }
                    Ds_Final.Tables.Add(Ds_Consulta_Reporte.Tables["DT_SOLICITUD"].Copy());
                    Ds_Final.Tables[0].TableName = "DT_SOLICITUD";
                    Ds_Final.Tables.Add(Ds_Reporte.Tables["DT_FOTOS_SALIDA"].Copy());
                    Ds_Final.Tables[1].TableName = "DT_FOTOS_SALIDA";
                    Ds_Final.Tables.Add(Ds_Reporte.Tables["DT_FOTOS_ENTRADA"].Copy());
                    Ds_Final.Tables[2].TableName = "DT_FOTOS_ENTRADA";
                    Ds_Final.Tables.Add(new Cls_Tal_Parametros_Negocio().Obtener_Tabla_Reporte(this.Server));
                    Ds_Final.Tables[3].TableName = "DT_LOGOS";

                    Generar_Reporte(ref Ds_Final, "Rpt_Tal_Salidas_Revista.rpt", "Reporte_Salidas_Revista" + Session.SessionID + ".pdf", ".pdf");
                }
                catch
                {
                    Lbl_Ecabezado_Mensaje.Text = "No fue posible imprimir salida";
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }

        #endregion

        #region Validaciones

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Autorizacion
            ///DESCRIPCIÓN: Valida la Solicitud de Servicio antes de ser Autorizada
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private Boolean Validar_Autorizacion() { 
                Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                String Mensaje_Error = "";
                Boolean Validacion = true;
                if (Txt_Fecha_Entrega.Text.Trim().Length == 0) {
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar la Fecha de Entrega del Vehiculo.";
                    Mensaje_Error = Mensaje_Error + " <br />";
                    Validacion = false;
                }
                if (Txt_Kilometraje_Entrega.Text.Trim().Length > 0) {
                    if (!Validar_Valores_Decimales(Txt_Kilometraje_Entrega.Text)) {
                        Mensaje_Error = Mensaje_Error + "+ El Formato del Kilometraje no es Correcto [Correcto: '12345', '12353.0' ó '12254.33'].";
                        Mensaje_Error = Mensaje_Error + " <br />";
                        Validacion = false;
                    }
                }
                if (Hdf_Empleado_Recibe.Value.Trim().Length == 0) {
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar al Empleado que Recibirá el Vehículo.";
                    Mensaje_Error = Mensaje_Error + " <br />";
                    Validacion = false;
                }
                if (Txt_Comentarios_Entrega.Text.Trim().Length == 0) {
                    Mensaje_Error = Mensaje_Error + "+ Introducir los Comentarios de la Entrega.";
                    Mensaje_Error = Mensaje_Error + " <br />";
                    Validacion = false;
                }
                if (!Validacion) {
                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
                    Div_Contenedor_Msj_Error.Visible = true;
                }
                return Validacion;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Valores_Decimales
            ///DESCRIPCIÓN: Valida los valores decimales para un Anexo.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO: 05/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private Boolean Validar_Valores_Decimales(String Valor) {
                Boolean Validacion = true;
                Regex Expresion_Regular = new Regex(@"^[0-9]{1,50}(\.[0-9]{0,2})?$");
                Validacion = Expresion_Regular.IsMatch(Valor);
                return Validacion;
            }

        #endregion

    #endregion
    
    #region Grids
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Solicitudes_PageIndexChanging
        ///DESCRIPCIÓN: Maneja la paginación del Listado
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Solicitudes_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            try {
                Grid_Listado_Solicitudes.SelectedIndex = (-1);
                Grid_Listado_Solicitudes.PageIndex = e.NewPageIndex;
                Llenar_Listado_Solicitudes();
            } catch(Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Solicitudes_SelectedIndexChanged
        ///DESCRIPCIÓN: Obtiene los datos de una Solicitud 
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Solicitudes_SelectedIndexChanged(object sender, EventArgs e) {
            try{
                if (Grid_Listado_Solicitudes.SelectedIndex > (-1)){
                    Limpiar_Formulario();
                    Hdf_No_Entrada.Value = HttpUtility.HtmlDecode(Grid_Listado_Solicitudes.SelectedRow.Cells[1].Text.Trim()).Trim();
                    Hdf_No_Servicio.Value = HttpUtility.HtmlDecode(Grid_Listado_Solicitudes.SelectedRow.Cells[2].Text.Trim()).Trim();
                    Hdf_No_Solicitud.Value = HttpUtility.HtmlDecode(Grid_Listado_Solicitudes.SelectedRow.Cells[3].Text.Trim()).Trim();
                    Mostrar_Registro();
                    Txt_Fecha_Entrega.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Today);
                    Configuracion_Formulario("OPERACION");
                    Grid_Listado_Solicitudes.SelectedIndex = -1;
                }
            } catch(Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Empleados_Resguardo_PageIndexChanging
        ///DESCRIPCIÓN: Maneja el evento de cambio de Página del GridView de Busqueda
        ///             de empleados.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 09/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Busqueda_Empleados_Resguardo_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            try {
                Grid_Busqueda_Empleados_Resguardo.PageIndex = e.NewPageIndex;
                Llenar_Grid_Busqueda_Empleados_Resguardo();
                MPE_Resguardante.Show();
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged
        ///DESCRIPCIÓN: Maneja el evento de cambio de Selección del GridView de Busqueda
        ///             de empleados.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 09/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged(object sender, EventArgs e) { 
            try {
                if (Grid_Busqueda_Empleados_Resguardo.SelectedIndex > (-1)) {
                    String Empleado_Seleccionado_ID = Grid_Busqueda_Empleados_Resguardo.SelectedRow.Cells[1].Text.Trim();
                    Mostrar_Informacion_Empleado_Recibe(Empleado_Seleccionado_ID);
                    Grid_Busqueda_Empleados_Resguardo.SelectedIndex = (-1);
                    MPE_Resguardante.Hide();
                }
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

    #endregion

    #region Eventos
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Controla las operaciones del Boton de Salir
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 05/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Hacer_Entrega_Click(object sender, ImageClickEventArgs e) {
            if (Validar_Autorizacion()) {
                Registrar_Entrega_Registro();
                Llenar_Listado_Solicitudes();
                Configuracion_Formulario("INICIAL");                
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Controla las operaciones del Boton de Salir
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 05/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e) {
            if (Div_Campos.Visible) {
                Limpiar_Formulario();
                Configuracion_Formulario("INICIAL");
            } else {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Empleado_Entrega_Click
        ///DESCRIPCIÓN: Lanza la Busqueda del Empleado
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 05/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Empleado_Entrega_Click(object sender, ImageClickEventArgs e) {
            Grid_Busqueda_Empleados_Resguardo.PageIndex = 0;
            Llenar_Grid_Busqueda_Empleados_Resguardo();
            MPE_Resguardante.Show();
        }
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Empleados_Click
        ///DESCRIPCIÓN: Ejecuta la Busqueda Avanzada para el Resguardante.
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 05/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************    
        protected void Btn_Busqueda_Empleados_Click(object sender, EventArgs e) {
            try {
                Grid_Busqueda_Empleados_Resguardo.PageIndex = 0;
                Llenar_Grid_Busqueda_Empleados_Resguardo();
                MPE_Resguardante.Show();
            }  catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Actualizar_Listado_Click
        ///DESCRIPCIÓN: Ejecuta el Proceso para Salir.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 05/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Actualizar_Listado_Click(object sender, ImageClickEventArgs e) {
            Llenar_Listado_Solicitudes();
            Cls_Ope_Tal_Salidas_Vehiculos_Negocio Sal_Negocio = new Cls_Ope_Tal_Salidas_Vehiculos_Negocio();            
        }

    #endregion

        #region Files Upload
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Fup_Frente_UploadedComplete
        ///DESCRIPCIÓN: 
        ///PROPIEDADES:     
        ///CREO: Jesus Toledo Rdz
        ///FECHA_CREO: 04/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Fup_Frente_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            HashAlgorithm sha = HashAlgorithm.Create("SHA1");
            String Checksum_Archivo = BitConverter.ToString(sha.ComputeHash(Fup_Frente.FileBytes));       //obtener checksum del archivo
            Dictionary<String, Byte[]> Diccionario_Archivos = Obtener_Diccionario_Archivos();   //obtener diccionario checksum-archivo
            DataTable Tabla_Documentos;//Crea DT para guardar resultado
            bool Bnd_Documento_Existente = false;//Boleano para ver si se repite documento
            String Extension_Archivo = Path.GetExtension(Fup_Frente.FileName).ToLower();//Aqui obtiene el Nombre del Archivo CAMBIAR POR FUP DE MI FORM
            // arreglo con las extensiones de archivo permitidas
            String[] Extensiones_Permitidas = { ".jpg", ".JPG" };//Esto esta bien
            String Nombre_Directorio;
            String Ruta_Archivo;
            // si la extension del archivo recibido no es valida, regresar

            if (Array.IndexOf(Extensiones_Permitidas, Extension_Archivo) < 0)
            {
                Lbl_Ecabezado_Mensaje.Text = "No se permite subir archivos con extensión: " + Extension_Archivo;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
                //Mensaje_Error(" ;
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Error_Extension_Archivo", "top.$get(\"" +
                Fup_Frente.ClientID + "\").style.background-color = 'red!important';", true);
                return;
            }

            if (Fup_Frente.FileBytes.Length > 2048000) // si la longitud del archivo recibido es mayor que 2MB, mostrar mensaje
            {
                Lbl_Ecabezado_Mensaje.Text = "El tamaño del archivo excede el limite permitido: " + Fup_Frente.FileName;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
                //Mensaje_Error(" El tamaño del archivo excede el limite permitido: " + Fup_Frente.FileName);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Error_Tamanio_Archivo", "top.$get(\"" +
                Fup_Frente.ClientID + "\").style.background-color = 'red!important';", true);
                return;
            }
            if (Session["Tabla_imagenes_Salida"] != null)
            {
                Tabla_Documentos = (DataTable)Session["Tabla_imagenes_Salida"];
            }
            else
            {
                Tabla_Documentos = Generar_Tabla_Documentos();
            }

            if (!Diccionario_Archivos.ContainsKey(Checksum_Archivo)) //si el checksum no esta en el diccionario, agregarlo y guardar en variable de sesion
            {
                Diccionario_Archivos.Add(Checksum_Archivo, Fup_Frente.FileBytes);
                Session["Diccionario_Archivos"] = Diccionario_Archivos;
            }

            // recorrer la tabla documentos
            foreach (DataRow Fila_Tabla_Documento in Tabla_Documentos.Rows)
            {
                //si ya existe un elemento con el tipo de documento, actualizarlo con el nuevo archivo
                if (Fila_Tabla_Documento["FOTO"].ToString() == "FRENTE")
                {
                    Fila_Tabla_Documento["FOTO"] = "FRENTE";
                    Fila_Tabla_Documento["ARCHIVO"] = Fup_Frente.PostedFile.FileName;
                    Fila_Tabla_Documento["RUTA_ARCHIVO"] = @"../../ARCHIVOS_TALLER_MUNICIPAL\REVISTA_MECANICA\" + Txt_No_Economico.Text.Trim() + @"\Solicitud_No_" + Hdf_No_Solicitud.Value + @"\Salida\Foto_Frente" + Path.GetExtension(Fup_Frente.FileName).ToLower();
                    Fila_Tabla_Documento["CHECKSUM"] = Checksum_Archivo;
                    Bnd_Documento_Existente = true;     // bandera a verdadero para indicar que se actualizo un registro existente en la tabla
                    break;
                }
            }
            //si la bandera es falso, no se encontro registro del tipo de documento, asi que se crea uno nuevo
            if (Bnd_Documento_Existente == false)
            {
                DataRow Nueva_Fila = Tabla_Documentos.NewRow();
                Nueva_Fila["FOTO"] = "FRENTE";
                Nueva_Fila["ARCHIVO"] = Fup_Frente.PostedFile.FileName;
                Nueva_Fila["RUTA_ARCHIVO"] = @"../../ARCHIVOS_TALLER_MUNICIPAL\REVISTA_MECANICA\" + Txt_No_Economico.Text.Trim() + @"\Solicitud_No_" + Hdf_No_Solicitud.Value + @"\Salida\Foto_Frente" + Path.GetExtension(Fup_Frente.FileName).ToLower();
                Nueva_Fila["CHECKSUM"] = Checksum_Archivo;

                Tabla_Documentos.Rows.Add(Nueva_Fila);
            }
            Guardar_Archivos(Diccionario_Archivos, Checksum_Archivo, @"../../ARCHIVOS_TALLER_MUNICIPAL\REVISTA_MECANICA\" + Txt_No_Economico.Text.Trim() + @"\Solicitud_No_" + Hdf_No_Solicitud.Value + @"\Salida\Foto_Frente" + Path.GetExtension(Fup_Frente.FileName).ToLower());
            Session["Tabla_imagenes_Salida"] = Tabla_Documentos;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Fup_Frente_UploadedComplete
        ///DESCRIPCIÓN: 
        ///PROPIEDADES:     
        ///CREO: Jesus Toledo Rdz
        ///FECHA_CREO: 04/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Fup_Atras_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            HashAlgorithm sha = HashAlgorithm.Create("SHA1");
            String Checksum_Archivo = BitConverter.ToString(sha.ComputeHash(Fup_Atras.FileBytes));       //obtener checksum del archivo
            Dictionary<String, Byte[]> Diccionario_Archivos = Obtener_Diccionario_Archivos();   //obtener diccionario checksum-archivo
            DataTable Tabla_Documentos;//Crea DT para guardar resultado
            bool Bnd_Documento_Existente = false;//Boleano para ver si se repite documento
            String Extension_Archivo = Path.GetExtension(Fup_Atras.FileName).ToLower();//Aqui obtiene el Nombre del Archivo CAMBIAR POR FUP DE MI FORM
            // arreglo con las extensiones de archivo permitidas
            String[] Extensiones_Permitidas = { ".jpg", ".JPG" };//Esto esta bien
            String Nombre_Directorio;
            String Ruta_Archivo;
            // si la extension del archivo recibido no es valida, regresar

            if (Array.IndexOf(Extensiones_Permitidas, Extension_Archivo) < 0)
            {
                //Mensaje_Error(" No se permite subir archivos con extensión: " + Extension_Archivo);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Error_Extension_Archivo", "top.$get(\"" +
                Fup_Atras.ClientID + "\").style.background-color = 'red!important';", true);
                return;
            }

            if (Fup_Atras.FileBytes.Length > 2048000) // si la longitud del archivo recibido es mayor que 2MB, mostrar mensaje
            {
                //Mensaje_Error(" El tamaño del archivo excede el limite permitido: " + Fup_Frente.FileName);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Error_Tamanio_Archivo", "top.$get(\"" +
                Fup_Atras.ClientID + "\").style.background-color = 'red!important';", true);
                return;
            }
            if (Session["Tabla_imagenes_Salida"] != null)
            {
                Tabla_Documentos = (DataTable)Session["Tabla_imagenes_Salida"];
            }
            else
            {
                Tabla_Documentos = Generar_Tabla_Documentos();
            }

            if (!Diccionario_Archivos.ContainsKey(Checksum_Archivo)) //si el checksum no esta en el diccionario, agregarlo y guardar en variable de sesion
            {
                Diccionario_Archivos.Add(Checksum_Archivo, Fup_Atras.FileBytes);
                Session["Diccionario_Archivos"] = Diccionario_Archivos;
            }

            // recorrer la tabla documentos
            foreach (DataRow Fila_Tabla_Documento in Tabla_Documentos.Rows)
            {
                //si ya existe un elemento con el tipo de documento, actualizarlo con el nuevo archivo
                if (Fila_Tabla_Documento["FOTO"].ToString() == "ATRAS")
                {
                    Fila_Tabla_Documento["FOTO"] = "ATRAS";
                    Fila_Tabla_Documento["ARCHIVO"] = Fup_Atras.PostedFile.FileName;
                    Fila_Tabla_Documento["RUTA_ARCHIVO"] = @"../../ARCHIVOS_TALLER_MUNICIPAL\REVISTA_MECANICA\" + Txt_No_Economico.Text.Trim() + @"\Solicitud_No_" + Hdf_No_Solicitud.Value + @"\Salida\Foto_Atras" + Path.GetExtension(Fup_Atras.FileName).ToLower();
                    Fila_Tabla_Documento["CHECKSUM"] = Checksum_Archivo;
                    Bnd_Documento_Existente = true;     // bandera a verdadero para indicar que se actualizo un registro existente en la tabla
                    break;
                }
            }
            //si la bandera es falso, no se encontro registro del tipo de documento, asi que se crea uno nuevo
            if (Bnd_Documento_Existente == false)
            {
                DataRow Nueva_Fila = Tabla_Documentos.NewRow();
                Nueva_Fila["FOTO"] = "ATRAS";
                Nueva_Fila["ARCHIVO"] = Fup_Atras.PostedFile.FileName;
                Nueva_Fila["RUTA_ARCHIVO"] = @"../../ARCHIVOS_TALLER_MUNICIPAL\REVISTA_MECANICA\" + Txt_No_Economico.Text.Trim() + @"\Solicitud_No_" + Hdf_No_Solicitud.Value + @"\Salida\Foto_Atras" + Path.GetExtension(Fup_Atras.FileName).ToLower();
                Nueva_Fila["CHECKSUM"] = Checksum_Archivo;

                Tabla_Documentos.Rows.Add(Nueva_Fila);
            }
            Guardar_Archivos(Diccionario_Archivos, Checksum_Archivo, @"../../ARCHIVOS_TALLER_MUNICIPAL\REVISTA_MECANICA\" + Txt_No_Economico.Text.Trim() + @"\Solicitud_No_" + Hdf_No_Solicitud.Value + @"\Salida\Foto_Atras" + Path.GetExtension(Fup_Atras.FileName).ToLower());
            Session["Tabla_imagenes_Salida"] = Tabla_Documentos;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Fup_Frente_UploadedComplete
        ///DESCRIPCIÓN: 
        ///PROPIEDADES:     
        ///CREO: Jesus Toledo Rdz
        ///FECHA_CREO: 04/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Fup_Izquierda_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            HashAlgorithm sha = HashAlgorithm.Create("SHA1");
            String Checksum_Archivo = BitConverter.ToString(sha.ComputeHash(Fup_Izquierda.FileBytes));       //obtener checksum del archivo
            Dictionary<String, Byte[]> Diccionario_Archivos = Obtener_Diccionario_Archivos();   //obtener diccionario checksum-archivo
            DataTable Tabla_Documentos;//Crea DT para guardar resultado
            bool Bnd_Documento_Existente = false;//Boleano para ver si se repite documento LO PUEDO OMITIR
            String Extension_Archivo = Path.GetExtension(Fup_Izquierda.FileName).ToLower();//Aqui obtiene el Nombre del Archivo CAMBIAR POR FUP DE MI FORM
            // arreglo con las extensiones de archivo permitidas
            String[] Extensiones_Permitidas = { ".jpg", ".JPG" };//Esto esta bien
            String Nombre_Directorio;
            String Ruta_Archivo;
            // si la extension del archivo recibido no es valida, regresar

            if (Array.IndexOf(Extensiones_Permitidas, Extension_Archivo) < 0)
            {
                //Mensaje_Error(" No se permite subir archivos con extensión: " + Extension_Archivo);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Error_Extension_Archivo", "top.$get(\"" +
                Fup_Izquierda.ClientID + "\").style.background-color = 'red!important';", true);
                return;
            }

            if (Fup_Izquierda.FileBytes.Length > 2048000) // si la longitud del archivo recibido es mayor que 2MB, mostrar mensaje
            {
                //Mensaje_Error(" El tamaño del archivo excede el limite permitido: " + Fup_Frente.FileName);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Error_Tamanio_Archivo", "top.$get(\"" +
                Fup_Izquierda.ClientID + "\").style.background-color = 'red!important';", true);
                return;
            }
            if (Session["Tabla_imagenes_Salida"] != null)
            {
                Tabla_Documentos = (DataTable)Session["Tabla_imagenes_Salida"];
            }
            else
            {
                Tabla_Documentos = Generar_Tabla_Documentos();
            }

            if (!Diccionario_Archivos.ContainsKey(Checksum_Archivo)) //si el checksum no esta en el diccionario, agregarlo y guardar en variable de sesion
            {
                Diccionario_Archivos.Add(Checksum_Archivo, Fup_Izquierda.FileBytes);
                Session["Diccionario_Archivos"] = Diccionario_Archivos;
            }

            // recorrer la tabla documentos
            foreach (DataRow Fila_Tabla_Documento in Tabla_Documentos.Rows)
            {
                //si ya existe un elemento con el tipo de documento, actualizarlo con el nuevo archivo
                if (Fila_Tabla_Documento["FOTO"].ToString() == "IZQUIERDA")
                {
                    Fila_Tabla_Documento["FOTO"] = "IZQUIERDA";
                    Fila_Tabla_Documento["ARCHIVO"] = Fup_Izquierda.PostedFile.FileName;
                    Fila_Tabla_Documento["RUTA_ARCHIVO"] = @"../../ARCHIVOS_TALLER_MUNICIPAL\REVISTA_MECANICA\" + Txt_No_Economico.Text.Trim() + @"\Solicitud_No_" + Hdf_No_Solicitud.Value + @"\Salida\Foto_lado_izq" + Path.GetExtension(Fup_Izquierda.FileName).ToLower();
                    Fila_Tabla_Documento["CHECKSUM"] = Checksum_Archivo;
                    Bnd_Documento_Existente = true;     // bandera a verdadero para indicar que se actualizo un registro existente en la tabla
                    break;
                }
            }
            //si la bandera es falso, no se encontro registro del tipo de documento, asi que se crea uno nuevo
            if (Bnd_Documento_Existente == false)
            {
                DataRow Nueva_Fila = Tabla_Documentos.NewRow();
                Nueva_Fila["FOTO"] = "IZQUIERDA";
                Nueva_Fila["ARCHIVO"] = Fup_Izquierda.PostedFile.FileName;
                Nueva_Fila["RUTA_ARCHIVO"] = @"../../ARCHIVOS_TALLER_MUNICIPAL\REVISTA_MECANICA\" + Txt_No_Economico.Text.Trim() + @"\Solicitud_No_" + Hdf_No_Solicitud.Value + @"\Salida\Foto_lado_izq" + Path.GetExtension(Fup_Izquierda.FileName).ToLower();
                Nueva_Fila["CHECKSUM"] = Checksum_Archivo;

                Tabla_Documentos.Rows.Add(Nueva_Fila);
            }
            Guardar_Archivos(Diccionario_Archivos, Checksum_Archivo, @"../../ARCHIVOS_TALLER_MUNICIPAL\REVISTA_MECANICA\" + Txt_No_Economico.Text.Trim() + @"\Solicitud_No_" + Hdf_No_Solicitud.Value + @"\Salida\Foto_lado_izq" + Path.GetExtension(Fup_Izquierda.FileName).ToLower());
            Session["Tabla_imagenes_Salida"] = Tabla_Documentos;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Fup_Frente_UploadedComplete
        ///DESCRIPCIÓN: 
        ///PROPIEDADES:     
        ///CREO: Jesus Toledo Rdz
        ///FECHA_CREO: 04/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Fup_Derecha_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            HashAlgorithm sha = HashAlgorithm.Create("SHA1");
            String Checksum_Archivo = BitConverter.ToString(sha.ComputeHash(Fup_Derecha.FileBytes));       //obtener checksum del archivo
            Dictionary<String, Byte[]> Diccionario_Archivos = Obtener_Diccionario_Archivos();   //obtener diccionario checksum-archivo
            DataTable Tabla_Documentos;//Crea DT para guardar resultado
            bool Bnd_Documento_Existente = false;//Boleano para ver si se repite documento LO PUEDO OMITIR
            String Extension_Archivo = Path.GetExtension(Fup_Derecha.FileName).ToLower();//Aqui obtiene el Nombre del Archivo CAMBIAR POR FUP DE MI FORM
            // arreglo con las extensiones de archivo permitidas
            String[] Extensiones_Permitidas = { ".jpg", ".JPG" };//Esto esta bien
            String Nombre_Directorio;
            String Ruta_Archivo;
            // si la extension del archivo recibido no es valida, regresar

            if (Array.IndexOf(Extensiones_Permitidas, Extension_Archivo) < 0)
            {
                //Mensaje_Error(" No se permite subir archivos con extensión: " + Extension_Archivo);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Error_Extension_Archivo", "top.$get(\"" +
                Fup_Derecha.ClientID + "\").style.background-color = 'red!important';", true);
                return;
            }

            if (Fup_Derecha.FileBytes.Length > 2048000) // si la longitud del archivo recibido es mayor que 2MB, mostrar mensaje
            {
                //Mensaje_Error(" El tamaño del archivo excede el limite permitido: " + Fup_Frente.FileName);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Error_Tamanio_Archivo", "top.$get(\"" +
                Fup_Derecha.ClientID + "\").style.background-color = 'red!important';", true);
                return;
            }
            if (Session["Tabla_imagenes_Salida"] != null)
            {
                Tabla_Documentos = (DataTable)Session["Tabla_imagenes_Salida"];
            }
            else
            {
                Tabla_Documentos = Generar_Tabla_Documentos();
            }

            if (!Diccionario_Archivos.ContainsKey(Checksum_Archivo)) //si el checksum no esta en el diccionario, agregarlo y guardar en variable de sesion
            {
                Diccionario_Archivos.Add(Checksum_Archivo, Fup_Derecha.FileBytes);
                Session["Diccionario_Archivos"] = Diccionario_Archivos;
            }

            // recorrer la tabla documentos
            foreach (DataRow Fila_Tabla_Documento in Tabla_Documentos.Rows)
            {
                //si ya existe un elemento con el tipo de documento, actualizarlo con el nuevo archivo
                if (Fila_Tabla_Documento["FOTO"].ToString() == "DERECHA")
                {
                    Fila_Tabla_Documento["FOTO"] = "DERECHA";
                    Fila_Tabla_Documento["ARCHIVO"] = Fup_Derecha.PostedFile.FileName;
                    Fila_Tabla_Documento["RUTA_ARCHIVO"] = @"../../ARCHIVOS_TALLER_MUNICIPAL\REVISTA_MECANICA\" + Txt_No_Economico.Text.Trim() + @"\Solicitud_No_" + Hdf_No_Solicitud.Value + @"\Salida\Foto_lado_der" + Path.GetExtension(Fup_Derecha.FileName).ToLower();
                    Fila_Tabla_Documento["CHECKSUM"] = Checksum_Archivo;
                    Bnd_Documento_Existente = true;     // bandera a verdadero para indicar que se actualizo un registro existente en la tabla
                    break;
                }
            }
            //si la bandera es falso, no se encontro registro del tipo de documento, asi que se crea uno nuevo
            if (Bnd_Documento_Existente == false)
            {
                DataRow Nueva_Fila = Tabla_Documentos.NewRow();
                Nueva_Fila["FOTO"] = "DERECHA";
                Nueva_Fila["ARCHIVO"] = Fup_Derecha.PostedFile.FileName;
                Nueva_Fila["RUTA_ARCHIVO"] = @"../../ARCHIVOS_TALLER_MUNICIPAL\REVISTA_MECANICA\" + Txt_No_Economico.Text.Trim() + @"\Solicitud_No_" + Hdf_No_Solicitud.Value + @"\Salida\Foto_lado_der" + Path.GetExtension(Fup_Derecha.FileName).ToLower();
                Nueva_Fila["CHECKSUM"] = Checksum_Archivo;

                Tabla_Documentos.Rows.Add(Nueva_Fila);
            }
            Guardar_Archivos(Diccionario_Archivos, Checksum_Archivo, @"../../ARCHIVOS_TALLER_MUNICIPAL\REVISTA_MECANICA\" + Txt_No_Economico.Text.Trim() + @"\Solicitud_No_" + Hdf_No_Solicitud.Value + @"\Salida\Foto_lado_der" + Path.GetExtension(Fup_Derecha.FileName).ToLower());
            Session["Tabla_imagenes_Salida"] = Tabla_Documentos;
        }

        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCIÓN: Guardar_Archivos
        /// 	DESCRIPCIÓN: Guardar en el servidor los archivos que se hayan recibido
        /// 	PARÁMETROS:
        /// 	CREO: Roberto González Oseguera
        /// 	FECHA_CREO: 10-may-2011
        /// 	MODIFICÓ: 
        /// 	FECHA_MODIFICÓ: 
        /// 	CAUSA_MODIFICACIÓN: 
        ///*******************************************************************************************************
        private void Guardar_Archivos(Dictionary<String, Byte[]> P_Dicc, String P_Checksum, String P_Ruta_Archivo)
        {
            DataTable Tabla_Tramites = (DataTable)Session["Tabla_imagenes_Salida"];
            Dictionary<String, Byte[]> Diccionario_Archivos = Obtener_Diccionario_Archivos();
            String Nombre_Directorio;
            String Ruta_Archivo;

            try
            {
                Nombre_Directorio = MapPath(Path.GetDirectoryName(P_Ruta_Archivo));
                Ruta_Archivo = MapPath(HttpUtility.HtmlDecode(P_Ruta_Archivo));
                if (!Directory.Exists(Nombre_Directorio))                       //si el directorio no existe, crearlo
                    Directory.CreateDirectory(Nombre_Directorio);
                //crear filestream y binarywriter para guardar archivo
                FileStream Escribir_Archivo = new FileStream(Ruta_Archivo, FileMode.Create, FileAccess.Write);
                BinaryWriter Datos_Archivo = new BinaryWriter(Escribir_Archivo);
                Datos_Archivo.Write(P_Dicc[P_Checksum]);
                // Guardar archivo (escribir datos en el filestream)                            
                //Cerrar Objetos
                Escribir_Archivo.Flush();
                //Escribir_Archivo.Dispose();
                Datos_Archivo.Flush();
                Datos_Archivo.Close();
                Escribir_Archivo.Close();
            }
            catch (Exception Ex)
            {
                throw new Exception("Guardar_Archivos " + Ex.Message.ToString(), Ex);
            }
        }
        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCIÓN: Obtener_Diccionario_Archivos
        /// 	DESCRIPCIÓN: Regresa el diccionario checksum-archivo si se encuentra en variable de sesion y si no,
        /// 	            regresa un diccionario vacio
        /// 	PARÁMETROS:
        /// 	CREO: Roberto González Oseguera
        /// 	FECHA_CREO: 04-may-2011
        /// 	MODIFICÓ: 
        /// 	FECHA_MODIFICÓ: 
        /// 	CAUSA_MODIFICACIÓN: 
        ///*******************************************************************************************************
        private Dictionary<String, Byte[]> Obtener_Diccionario_Archivos()
        {
            Dictionary<String, Byte[]> Diccionario_Archivos = new Dictionary<String, Byte[]>();

            // si existe el diccionario en variable de sesion
            if (Session["Diccionario_Archivos"] != null)
            {
                Diccionario_Archivos = (Dictionary<String, Byte[]>)Session["Diccionario_Archivos"];
            }

            return Diccionario_Archivos;
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Generar_Tabla_Documentos
        /// DESCRIPCION: Genera la tabla de Documentos, el esquema para guardar los tipos de document a recibir
        /// PARAMETROS: 
        /// CREO: Jesus Toledo Rodriguez
        /// FECHA_CREO: 04-may-2012
        /// MODIFICO:
        /// FECHA_MODIFICO:
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private DataTable Generar_Tabla_Documentos()
        {
            DataTable Tabla_Nueva = new DataTable();
            DataColumn Columna0_Foto;
            DataColumn Columna1_Archivo;
            DataColumn Columna2_Ruta_Archivo;
            DataColumn Columna3_Checksum;
            //Nueva_Fila["FOTO"]
            //Nueva_Fila["ARCHIVO"]
            //Nueva_Fila["RUTA_ARCHIVO"]
            //Nueva_Fila["CHECKSUM"]
            try
            {
                // ---------- Inicializar columnas
                Columna0_Foto = new DataColumn();
                Columna0_Foto.DataType = System.Type.GetType("System.String");
                Columna0_Foto.ColumnName = "FOTO";
                Tabla_Nueva.Columns.Add(Columna0_Foto);
                Columna1_Archivo = new DataColumn();
                Columna1_Archivo.DataType = System.Type.GetType("System.String");
                Columna1_Archivo.ColumnName = "ARCHIVO";
                Tabla_Nueva.Columns.Add(Columna1_Archivo);
                Columna2_Ruta_Archivo = new DataColumn();
                Columna2_Ruta_Archivo.DataType = System.Type.GetType("System.String");
                Columna2_Ruta_Archivo.ColumnName = "RUTA_ARCHIVO";
                Tabla_Nueva.Columns.Add(Columna2_Ruta_Archivo);
                Columna3_Checksum = new DataColumn();
                Columna3_Checksum.DataType = System.Type.GetType("System.String");
                Columna3_Checksum.ColumnName = "CHECKSUM";
                Tabla_Nueva.Columns.Add(Columna3_Checksum);

                return Tabla_Nueva;
            }
            catch (Exception ex)
            {
                throw new Exception("Generar_Tabla_Documentos " + ex.Message.ToString(), ex);
            }
        }
        #endregion
        #region Reporte
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN : Convertir_Imagen_A_Cadena_Bytes
        ///DESCRIPCIÓN          : Obtine la ruta de la imagen y la convierte a bytes 
        ///                       para mostrarla en pantalla
        ///PARAMETROS: 
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 22/05/2012 06:43:00 p.m.
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        private Byte[] Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image P_Imagen)
        {
            Byte[] Img_Bytes = null;
            try
            {
                if (P_Imagen != null)
                {
                    MemoryStream MS_Tmp = new MemoryStream();
                    P_Imagen.Save(MS_Tmp, P_Imagen.RawFormat);
                    Img_Bytes = MS_Tmp.GetBuffer();
                    MS_Tmp.Close();
                }
            }
            catch (Exception Ex)
            {
                Div_Contenedor_Msj_Error.Visible = true;
                Lbl_Mensaje_Error.Text = Ex.Message.ToString();                
            }
            return Img_Bytes;
        }
        /// *************************************************************************************
        /// NOMBRE:             Exportar_Reporte_PDF
        /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
        ///                     especificada.
        /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
        ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
        /// USUARIO CREO:       Salvador Hernandez Ramírez.
        /// FECHA CREO:         16/Mayo/2011
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACIÓN:
        /// *************************************************************************************
        public void Exportar_Reporte_Excel(ReportDocument Reporte, String Nombre_Reporte_Generar)
        {
            if (Reporte is ReportDocument)
            {
                ExportOptions CrExportOptions = new ExportOptions();

                DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                CrDiskFileDestinationOptions.DiskFileName = HttpContext.Current.Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
                CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;

                CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                CrExportOptions.ExportFormatType = ExportFormatType.Excel;
                Reporte.Export(CrExportOptions);
            }
        }
        /// *************************************************************************************
        /// NOMBRE:             Exportar_Reporte_PDF
        /// DESCRIPCIÓN:        Sobrecarga de Método que guarda el reporte generado en archivo XLS en la ruta
        ///                     especificada.
        /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
        ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
        /// USUARIO CREO:       Antonio Salvador Benavides Guardado
        /// FECHA CREO:         21/Noviembre/2011
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACIÓN:
        /// *************************************************************************************
        public void Exportar_Reporte_Excel(ReportDocument Reporte, String Ruta_Reporte_Generar, String Nombre_Reporte_Generar)
        {
            if (Reporte is ReportDocument)
            {
                ExportOptions CrExportOptions = new ExportOptions();

                DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                CrDiskFileDestinationOptions.DiskFileName = HttpContext.Current.Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
                CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;

                CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                CrExportOptions.ExportFormatType = ExportFormatType.Excel;
                Reporte.Export(CrExportOptions);
            }
        }
        /// *********************************************** **************************************
        /// NOMBRE:             Generar_Reporte
        /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
        ///              
        /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
        ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
        ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
        ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
        /// USUARIO CREO:       Juan Alberto Hernández Negrete.
        /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
        /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
        /// FECHA MODIFICO:     16/Mayo/2011
        /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
        ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
        /// *************************************************************************************
        public void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte_Crystal, String Nombre_Reporte_Generar, String Formato)
        {            
            try
            {
                ReportDocument Reporte = new ReportDocument();
                String File_Path = Server.MapPath("../Rpt/Servicios_Generales/" + Ruta_Reporte_Crystal);
                Reporte.Load(File_Path);
                String Nombre_Reporte_Generar_ = "Rpt_Tal_Solicitud_Servicio_" + Cls_Sessiones.No_Empleado + "_" + Convert.ToString(DateTime.Now.ToString("yyyy'-'MM'-'dd'_t'HH'-'mm'-'ss"));
                String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
                //Ds_Reporte = Data_Set_Consulta_DB;
                Reporte.SetDataSource(Ds_Reporte_Crystal);
                ExportOptions Export_Options = new ExportOptions();
                DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
                Disk_File_Destination_Options.DiskFileName = Server.MapPath(Ruta);
                Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
                Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
                Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Export_Options);
                //Mostrar_Reporte(Nombre_Reporte_Generar, "PDF");

                if (Ds_Reporte_Crystal is DataSet)
                {
                    if (Ds_Reporte_Crystal.Tables.Count > 0)
                    {
                        if (Formato == ".pdf")
                        {
                            Reporte.SetDataSource(Ds_Reporte_Crystal);
                            Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar);
                            Mostrar_Reporte(Nombre_Reporte_Generar, ".pdf");
                        }
                        else if (Formato == ".xls")
                        {
                            Reporte.SetDataSource(Ds_Reporte_Crystal);
                            Exportar_Reporte_Excel(Reporte, Nombre_Reporte_Generar);
                            Mostrar_Reporte(Nombre_Reporte_Generar, ".xls");
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
            }
        }
        /// *************************************************************************************
        /// NOMBRE:             Exportar_Reporte_PDF
        /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
        ///                     especificada.
        /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
        ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
        /// USUARIO CREO:       Juan Alberto Hernández Negrete.
        /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACIÓN:
        /// *************************************************************************************
        public void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte_Generar)
        {
            ExportOptions Opciones_Exportacion = new ExportOptions();
            DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
            PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

            try
            {
                if (Reporte is ReportDocument)
                {
                    Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
                    Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                    Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                    Reporte.Export(Opciones_Exportacion);
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
            }
        }


        /// *************************************************************************************
        /// NOMBRE:              Mostrar_Reporte
        /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
        /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
        ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
        /// USUARIO CREO:        Juan Alberto Hernández Negrete.
        /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
        /// USUARIO MODIFICO:    Salvador Hernández Ramírez
        /// FECHA MODIFICO:      16-Mayo-2011
        /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
        /// *************************************************************************************
        protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
        {
            String Pagina = "../../Reporte/";

            try
            {
                if (Formato == ".pdf")
                {
                    Pagina = Pagina + Nombre_Reporte_Generar;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                    "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
                }
                else if (Formato == ".xls")
                {
                    String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
        }
    #endregion        
}
