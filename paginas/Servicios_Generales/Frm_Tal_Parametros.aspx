﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Tal_Parametros.aspx.cs" Inherits="paginas_Taller_Mecanico_Frm_Tal_Parametros" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">

    <script type="text/javascript">
        window.onerror = new Function("return true");
        //Abrir una ventana modal
        function Abrir_Ventana_Modal(Url, Propiedades) {
            window.showModalDialog(Url, null, Propiedades);
        }
        function Validar_Longitud_Texto(Text_Box, Max_Longitud) {
            if (Text_Box.value.length > Max_Longitud) {
                Text_Box.value = Text_Box.value.substring(0, Max_Longitud);
            }
        }
    </script>

    <script type="text/javascript" language="javascript">
    //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_sesiones.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion()
        {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para matener la sesión activa
        setInterval('MantenSesion()', <%=(int)(0.9*(Session.Timeout * 60000))%>);
        function formatCurrency(num) {
            var Combo = "";
            var Beneficio;
                num = num.toString().replace(/\$|\,/g,'');
                if(isNaN(num))
                num = "0";
                sign = (num == (num = Math.abs(num)));
                num = Math.floor(num*100+0.50000000001);
                cents = num%100;
                num = Math.floor(num/100).toString();
                if(cents<10)
                    cents = "0" + cents;
                    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
                    num = num.substring(0,num.length-(4*i+3))+','+
                    num.substring(num.length-(4*i+3));
                    return (((sign)?'':'-') + num + '.' + cents);
            }
            
            
            
            
             function StartUpload(sender, args) {
            try {
                var filename = args.get_fileName();

                if (filename != "") {
                    // code to get File Extension..   
                    var arr1 = new Array;
                    arr1 = filename.split("\\");
                    var len = arr1.length;
                    var img1 = arr1[len - 1];
                    var filext = img1.substring(img1.lastIndexOf(".") + 1);


                    if (filext == "txt" || filext == "doc" || filext == "pdf" || filext == "docx" || filext == "jpg" || filext == "JPG" || filext == "jpeg" || filext == "JPEG" || filext == "png" || filext == "PNG" || filext == "gif" || filext == "GIF" || filext == "xlsx") {
                        if (args.get_length() > 2621440) {
                            var mensaje = "\n\nTabla de Documentos Anexos se limpiara completamente. Favor de volver a seleccionar los\narchivos a subir.";
                            alert("El Archivo " + filename + ". Excedio el Tamaño Permitido:\n\nTamaño del Archivo: [" + args.get_length() + " Bytes]\nTamaño Permitido: [2621440 Bytes o 2.5 Mb]" + mensaje);
                            return false;

                        }
                        return true;
                    } else {
                        var mensaje = "\n\nTabla de Documentos Anexos se limpiara completamente. Favor de volver a seleccionar los\narchivos a subir.";
                        alert("Tipo de archivo inválido " + filename + "\n\nFormatos Validos [.txt, .doc, .docx, .zip, .rar, .pdf, .jpg, .jpeg, .png, .gif, .xlsx]" + mensaje);
                        return false;
                    }
                }
            } catch (e) {

            }
        }

        function uploadError(sender, args) {
            try {
                var mensaje = "\n\nLa Tabla de Documentos Anexos se limpiara completamente. Favor de volver a seleccionar los\narchivos a subir.";
                alert("Error al Intentar cargar el archivo. [ " + args.get_fileName() + " ]\n\nTamaño Válido de los Archivos:\n + El Archivo debé ser mayor a 1Kb.\n + El Archivo debé ser Menor a 2.5 Mb" + mensaje);
                //refreshGridView();
            } catch (e) {

            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" EnablePartialRendering="true" AsyncPostBackTimeout="9000">
    </cc1:ToolkitScriptManager>
    <%--Inicio formulario--%>
    <asp:UpdatePanel ID="Upd_Parametros_Predial" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--update progrees--%>
            <asp:UpdateProgress ID="Uprg_Progress" runat="server" AssociatedUpdatePanelID="Upd_Parametros_Predial"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Generales" style="background-color: #ffffff; width: 100%; height: 100%">
                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td colspan="4" class="label_titulo">
                            Parámetros
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" />
                            <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" CssClass="estilo_fuente_mensaje_error"
                                Text="" /><br />
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
                    <tr class="barra_busqueda">
                        <td colspan="2" align="left" style="width: 20%">
                            <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                CssClass="Img_Button" OnClick="Btn_Modificar_Click" />
                            <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                CssClass="Img_Button" OnClick="Btn_Salir_Click" />
                        </td>
                        <td colspan="2" align="right" valign="top" style="width: 80%">
                            <table style="width: 80%;">
                                <tr>
                                    <td style="vertical-align: top; text-align: right; width: 5%">
                                    </td>
                                    <td style="vertical-align: top; text-align: right; width: 90%">
                                    </td>
                                    <td style="vertical-align: top; text-align: right; width: 5%">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            &nbsp;
            <br />
            <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                <tr>
                    <td style="text-align: left;">
                        <asp:Label ID="Lbl_Fuente_Financiamiento" runat="server" 
                            Text="F. de Financiamiento"></asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:DropDownList ID="Cmb_FF" runat="server" Width="98%">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">
                        <asp:Label ID="Lbl_Programa" runat="server" Text="Programa"></asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:DropDownList ID="Cmb_Programa" runat="server" Width="98%">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">
                        <asp:Label ID="Lbl_Capitulo" runat="server" Text="Capítulo"></asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:DropDownList ID="Cmb_Capitulo" runat="server" AutoPostBack="True" 
                            OnSelectedIndexChanged="Cmb_Capitulo_SelectedIndexChanged" Width="98%">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; width: 15%;">
                        <asp:Label ID="Lbl_Concepto" runat="server" Text="Concepto"></asp:Label>
                    </td>
                    <td colspan="3" style="text-align: left; width: 30%;">
                        <asp:DropDownList ID="Cmb_Conceptos" runat="server" AutoPostBack="true" 
                            OnSelectedIndexChanged="Cmb_Conceptos_SelectedIndexChanged" Width="98%">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">
                        <asp:Label ID="Lbl_Partida_General" runat="server" Text="Partida Generica"></asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:DropDownList ID="Cmb_Partida_General" runat="server" AutoPostBack="True" 
                            OnSelectedIndexChanged="Cmb_Partida_General_SelectedIndexChanged" Width="98%">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">
                        <asp:Label ID="Lbl_Partida_Especifica" runat="server" Text="Partida Especifica"></asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:DropDownList ID="Cmb_Partida_Especifica" runat="server"  
                            Width="98%">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">
                        <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text="Unidad Responsable"></asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:DropDownList ID="Cmb_Unidad_Responsable" runat="server" Width="98%">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">
                        <asp:Label ID="Lbl_Tipo_Solicitud_Pago" runat="server" 
                            Text="Tipo Solicitud Pago"></asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:DropDownList ID="Cmb_Tipo_Solicitud_Pago" runat="server" Width="98%">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">
                        <asp:Label ID="Lbl_Oficial_Mayor" runat="server" Text="Gerencia Administrativa"></asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="Txt_Oficial_Mayor" runat="server" Width="98%">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">
                        <asp:Label ID="Lbl_Director" runat="server" Text="Jefe Servicios Generales"></asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="Txt_Director" runat="server" Width="98%">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">
                        <asp:Label ID="Lbl_Coordinador" runat="server" Text="Coordinador"></asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="Txt_Coordinador" runat="server" Width="98%">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">
                        <asp:Label ID="Lbl_Almacen" runat="server" Text="Jefe Taller"></asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="Txt_Almacen" runat="server" Width="98%">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <hr style="width: 98%;" />
                        <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                            <tr>
                                <td style="width: 15%;">
                                    Proveedor Combustibles
                                </td>
                                <td>
                                    <asp:TextBox ID="Txt_Nombre_Proveedor" runat="server" Enabled="false" 
                                        Style="float: left" Width="95%"></asp:TextBox>
                                    <asp:ImageButton ID="Btn_Busqueda_Proveedores" runat="server" Height="22px" 
                                        ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png" 
                                        OnClick="Btn_Busqueda_Proveedores_Click" Style="float: right" TabIndex="10" 
                                        ToolTip="Búsqueda Avanzada" Width="22px" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">
                        <asp:HiddenField ID="Hdf_Logo_Municipal" runat="server" />
                        <asp:Label ID="Lbl_Logo_Municipio" runat="server" Text="Logo del Municipio"></asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:ImageButton ID="Btn_Logo_Municipio" runat="server" 
                            CausesValidation="False" Height="24px" 
                            ImageUrl="~/paginas/imagenes/paginas/icono_consultar.png" 
                            OnClick="Btn_Logo_Municipio_Click" ToolTip="Ver Logo Municipio" Width="24px" />
                        &nbsp;
                        <cc1:AsyncFileUpload ID="AFU_Logo_Municipio" runat="server" 
                            CompleteBackColor="LightBlue" FailedValidation="False" Font-Bold="True" 
                            ForeColor="White" OnClientUploadComplete="StartUpload" 
                            OnClientUploadError="uploadError" 
                            OnUploadedComplete="Asy_Cargar_Archivo_Complete" ThrobberID="Throbber" 
                            UploadingBackColor="LightGray" Width="500px" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">
                        <asp:HiddenField ID="Hdf_Logo_Estado" runat="server" />
                        <asp:Label ID="Lbl_Logo_Estado" runat="server" Text="Logo del Estado"></asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:ImageButton ID="Btn_Logo_Estado" runat="server" CausesValidation="False" 
                            Height="24px" ImageUrl="~/paginas/imagenes/paginas/icono_consultar.png" 
                            OnClick="Btn_Logo_Estado_Click" ToolTip="Ver Logo del Estado" Width="24px" />
                        &nbsp;
                        <cc1:AsyncFileUpload ID="AFU_Logo_Estado" runat="server" 
                            CompleteBackColor="LightBlue" FailedValidation="False" Font-Bold="True" 
                            ForeColor="White" OnClientUploadComplete="StartUpload" 
                            OnClientUploadError="uploadError" 
                            OnUploadedComplete="Asy_Cargar_Archivo_Logo_Estado" ThrobberID="Throbber" 
                            UploadingBackColor="LightGray" Width="500px" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 18%">
                        <asp:HiddenField ID="Hdf_Proveedor_ID" runat="server" />
                    </td>
                    <td style="width: 32%">
                        &nbsp;
                    </td>
                    <td style="width: 18%">
                        &nbsp;
                    </td>
                    <td style="width: 32%">
                        <asp:Label ID="Throbber" runat="server" Text="wait" Width="30px">                                                                     
                                    <div id="Div1" class="progressBackgroundFilter"></div>
                                    <div  class="processMessage" id="div2">
                                        <img alt="" src="../imagenes/paginas/Updating.gif" />
                                    </div>
                        </asp:Label>
                    </td>
                </tr>
            </table>
            <br>
            <br></br>
            </br>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
