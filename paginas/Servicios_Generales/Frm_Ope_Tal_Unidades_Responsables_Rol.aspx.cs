﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Unidades_Responsables_Rol.Negocio;
using JAPAMI.Asignar_Password.Negocio;

public partial class paginas_Taller_Mecanico_Frm_Ope_Tal_Unidades_Responsables_Rol : System.Web.UI.Page {
    
    #region Page_Load

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN: Evento Inicial de Cargar Pagina.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Page_Load(object sender, EventArgs e){
            Div_Contenedor_Msj_Error.Visible = false;
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack) {
                Llenar_Combos();
                Llenar_Grid_Busqueda_UR();
                Configuracion_Formulario("INICIAL");
            }
        }

    #endregion

    #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Busqueda_UR
        ///DESCRIPCIÓN: Llena el Grid de la Busqueda de UR
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private void Llenar_Grid_UR() {
            Cls_Ope_Tal_Unidades_Responsables_Rol_Negocio Cls_Negocio = new Cls_Ope_Tal_Unidades_Responsables_Rol_Negocio();
            DataTable Dt_Resultados = new DataTable();
            Cls_Negocio.P_Estatus = "ACTIVO";
            if (!String.IsNullOrEmpty(Txt_No_Empleado.Text.Trim()))
            {
                Cls_Negocio.P_No_Empleado = String.Format("{0:000000}", Convert.ToInt32(Txt_No_Empleado.Text.Trim()));
                Dt_Resultados = Cls_Negocio.Consultar_Listado_UR();
            }
            Grid_UR.Columns[0].Visible = true;
            Grid_UR.DataSource = Dt_Resultados;
            Grid_UR.DataBind();
            Grid_UR.Columns[0].Visible = false;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Busqueda_UR
        ///DESCRIPCIÓN: Llena el Grid de la Busqueda de UR
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private void Llenar_Grid_Busqueda_UR() {
            Cls_Ope_Tal_Consultas_Generales_Negocio Cls_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            Cls_Negocio.P_Nombre_UR = Txt_Busqueda_Nombre.Text.Trim();
            Cls_Negocio.P_Clave_UR = Txt_Busqueda_Clave.Text.Trim();
            Cls_Negocio.P_Estatus = "ACTIVO";
            DataTable Dt_Resultados = Cls_Negocio.Consultar_Unidades_Responsables();
            Grid_Busqueda_UR.Columns[1].Visible = true;
            Grid_Busqueda_UR.Columns[4].Visible = true;
            Grid_Busqueda_UR.DataSource = Dt_Resultados;
            Grid_Busqueda_UR.DataBind();
            Grid_Busqueda_UR.Columns[1].Visible = false;
            Grid_Busqueda_UR.Columns[4].Visible = false;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
        ///DESCRIPCIÓN: Limpiar Formulario de UR.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private void Limpiar_Formulario() {
            Hdf_Dependencia_ID.Value = "";
            Txt_Clave.Text = "";
            Txt_Nombre.Text = "";
            Txt_Comentarios.Text = "";
        }
        private void Llenar_Combos()
        {
            Cls_Ope_Psw_Negocio Negocio = new Cls_Ope_Psw_Negocio();
            DataTable Dt_Dependencias = Negocio.Consultar_Dependencias();
            Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_UR_Empleado, Dt_Dependencias, 1, 0);

        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
        ///DESCRIPCIÓN: Habilita o Inhabilita dependiendo de la Operacion.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private void Configuracion_Formulario(String Operacion) {
            switch (Operacion) {
                case "INICIAL":
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Modificar.AlternateText = "Modificar";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Salir.AlternateText = "Salir";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Lanzar_Busqueda_UR.Enabled = false;
                    Grid_UR.Columns[3].Visible = false;
                    Btn_Agregar_Dependencia.Visible = false;
                    Btn_Todas_Dependencias.Visible = false;
                    break;
                case "OPERACION":
                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Modificar.AlternateText = "Actualizar";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Salir.AlternateText = "Cancelar";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Lanzar_Busqueda_UR.Enabled = true;
                    Grid_UR.Columns[3].Visible = true;
                    Btn_Agregar_Dependencia.Visible = true;
                    Btn_Todas_Dependencias.Visible = true;
                    break;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Actualizar_Listado
        ///DESCRIPCIÓN: Actualiza el Listado.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private void Actualizar_Listado() {
            Cls_Ope_Tal_Unidades_Responsables_Rol_Negocio Cls_Negocio = new Cls_Ope_Tal_Unidades_Responsables_Rol_Negocio();
            if (!String.IsNullOrEmpty(Txt_No_Empleado.Text.Trim()))
            {
                Cls_Negocio.P_No_Empleado = String.Format("{0:000000}", Convert.ToInt32(Txt_No_Empleado.Text.Trim()));
                Cls_Negocio.P_Dt_Unidades_Responsables = Obtener_Dt_Grid_UR();
                Cls_Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                Cls_Negocio.Actualizar_Listado_UR();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Dt_UR
        ///DESCRIPCIÓN: Saca el Listado de las UR Seleccionadas.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private DataTable Obtener_Dt_Grid_UR() {
            DataTable Dt_Listado_UR = new DataTable();
            Dt_Listado_UR.Columns.Add("DEPENDENCIA_ID", Type.GetType("System.String"));
            Dt_Listado_UR.Columns.Add("CLAVE", Type.GetType("System.String"));
            Dt_Listado_UR.Columns.Add("NOMBRE", Type.GetType("System.String"));
            foreach (GridViewRow Fila in Grid_UR.Rows) {
                DataRow Fila_Nueva = Dt_Listado_UR.NewRow();
                Fila_Nueva["DEPENDENCIA_ID"] = HttpUtility.HtmlDecode(Fila.Cells[0].Text).Trim();
                Fila_Nueva["CLAVE"] = HttpUtility.HtmlDecode(Fila.Cells[1].Text).Trim();
                Fila_Nueva["NOMBRE"] = HttpUtility.HtmlDecode(Fila.Cells[2].Text).Trim();
                Dt_Listado_UR.Rows.Add(Fila_Nueva);
            }
            return Dt_Listado_UR;
        }

    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Buscar_Clave_DataTable
        ///DESCRIPCIÓN: Busca una Clave en un DataTable, si la encuentra Retorna 'true'
        ///             en caso contrario 'false'.
        ///PROPIEDADES:  
        ///             1.  Clave.  Clave que se buscara en el DataTable
        ///             2.  Tabla.  Datatable donde se va a buscar la clave.
        ///             3.  Columna.Columna del DataTable donde se va a buscar la clave.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 03/Diciembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private Boolean Buscar_Clave_DataTable(String Clave, DataTable Tabla, Int32 Columna)
        {
            Boolean Resultado_Busqueda = false;
            if (Tabla != null && Tabla.Rows.Count > 0 && Tabla.Columns.Count > 0)
            {
                if (Tabla.Columns.Count > Columna)
                {
                    for (Int32 Contador = 0; Contador < Tabla.Rows.Count; Contador++)
                    {
                        if (Tabla.Rows[Contador][Columna].ToString().Trim().Equals(Clave.Trim()))
                        {
                            Resultado_Busqueda = true;
                            break;
                        }
                    }
                }
            }
            return Resultado_Busqueda;
        }

    #endregion   

    #region Grids

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_UR_PageIndexChanging
        ///DESCRIPCIÓN: Maneja el evento de cambio de Página del GridView de Busqueda
        ///             de UR.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Busqueda_UR_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            try {
                Grid_Busqueda_UR.PageIndex = e.NewPageIndex;
                Llenar_Grid_Busqueda_UR();
                MPE_UR.Show();
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_UR_SelectedIndexChanged
        ///DESCRIPCIÓN: Maneja el evento de cambio de Selección del GridView de Busqueda
        ///             de UR.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Busqueda_UR_SelectedIndexChanged(object sender, EventArgs e) { 
            try {
                if (Grid_Busqueda_UR.SelectedIndex > (-1)) {
                    Limpiar_Formulario();
                    GridViewRow Fila_Seleccionada = Grid_Busqueda_UR.SelectedRow;
                    Hdf_Dependencia_ID.Value = HttpUtility.HtmlDecode(Fila_Seleccionada.Cells[1].Text).Trim();
                    Txt_Clave.Text = HttpUtility.HtmlDecode(Fila_Seleccionada.Cells[2].Text).Trim();
                    Txt_Nombre.Text = HttpUtility.HtmlDecode(Fila_Seleccionada.Cells[3].Text).Trim();
                    Txt_Comentarios.Text = HttpUtility.HtmlDecode(Fila_Seleccionada.Cells[4].Text).Trim();
                    Grid_Busqueda_UR.SelectedIndex = -1;
                    MPE_UR.Hide();
                }
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

    #endregion

    #region Eventos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Lanzar_Busqueda_UR_Click
        ///DESCRIPCIÓN: Lanza la Ventana para la Busqueda de la UR.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Lanzar_Busqueda_UR_Click(object sender, ImageClickEventArgs e) {
            MPE_UR.Show();
        }
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_UR_Click
        ///DESCRIPCIÓN: Lanza la Ventana para la Busqueda de la UR.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Busqueda_UR_Click(object sender, EventArgs e) {
            Grid_Busqueda_UR.PageIndex = 1;
            Llenar_Grid_Busqueda_UR();
        }
        protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
        {
            Grid_UR.DataSource = new DataTable();
            Grid_UR.DataBind();
            Session["Dt_Dependencias"] = null;
            Cls_Ope_Psw_Negocio Negocio = new Cls_Ope_Psw_Negocio();
            if (!String.IsNullOrEmpty(Txt_No_Empleado.Text.Trim()))
            {
                Negocio.P_No_Empleado = String.Format("{0:000000}", Convert.ToInt32(Txt_No_Empleado.Text));
                DataTable Dt_Empleado = Negocio.Consultar_Empleado();
                if (Dt_Empleado != null && Dt_Empleado.Rows.Count > 0)
                {
                    try
                    {
                        String Psw = (Dt_Empleado.Rows[0]["PASSWORD"].ToString().Trim());
                        Txt_Nombre_Empleado.Text = Dt_Empleado.Rows[0]["NOMBRE_EMPLEADO"].ToString().Trim();
                        if (Dt_Empleado.Rows[0]["DEPENDENCIA_ID"]!=null)
                            if (Dt_Empleado.Rows[0]["DEPENDENCIA_ID"].ToString().Trim().Length>0)
                        Cmb_UR_Empleado.SelectedValue = Dt_Empleado.Rows[0]["DEPENDENCIA_ID"].ToString().Trim();
                        Cmb_UR_Empleado.Enabled = false;
                        Llenar_Grid_UR();

                        //DataTable Dt_Dependencias = Negocio.Consultar_Detalle_UR_Empleado();
                        //Session["Dt_Dependencias"] = Dt_Dependencias;
                        //if (Dt_Dependencias.Rows.Count > 0)
                        //{
                        //    Grid_UR.DataSource = Dt_Dependencias;
                        //    Grid_UR.DataBind();
                        //    Session["Dt_Dependencias"] = Dt_Dependencias;
                        //}
                        //else
                        //{
                        //    Grid_UR.DataSource = new DataTable();
                        //    Grid_UR.DataBind();

                        //}
                    }
                    catch (Exception Ex)
                    {
                        Lbl_Ecabezado_Mensaje.Text = Ex.ToString();
                        Lbl_Mensaje_Error.Text = "";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                }
                else
                {
                    Lbl_Ecabezado_Mensaje.Text = "No se encontró información con el número de empleado proporcionado.";
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                    Txt_Nombre.Text = "";
                }
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
        ///DESCRIPCIÓN: Ejecuta la Modificacion
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e) {
            if (Btn_Modificar.AlternateText.Trim().Equals("Modificar")) {
                Configuracion_Formulario("OPERACION");
            } else {
                Limpiar_Formulario();
                Actualizar_Listado();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Listado Actualizado');", true);
                Llenar_Grid_UR();
                Configuracion_Formulario("INICIAL");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Ejecuta la Salida del Formulario
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e) {
            if (Btn_Salir.AlternateText.Trim().Equals("Salir")) {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            } else {
                Limpiar_Formulario();
                Llenar_Grid_UR();
                Configuracion_Formulario("INICIAL");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Agregar_Dependencia_Click
        ///DESCRIPCIÓN: Agrega la UR al Listado.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Agregar_Dependencia_Click(object sender, EventArgs e) {
            if (Hdf_Dependencia_ID.Value.Trim().Length > 0) { 
                DataTable Dt_Listado = Obtener_Dt_Grid_UR();
                if (!Buscar_Clave_DataTable(Hdf_Dependencia_ID.Value.Trim(), Dt_Listado, 0)) {
                    DataRow Fila_Nueva = Dt_Listado.NewRow();
                    Fila_Nueva["DEPENDENCIA_ID"] = Hdf_Dependencia_ID.Value.Trim();
                    Fila_Nueva["CLAVE"] = Txt_Clave.Text.Trim();
                    Fila_Nueva["NOMBRE"] = Txt_Nombre.Text.Trim();
                    Dt_Listado.Rows.Add(Fila_Nueva);
                    Limpiar_Formulario();
                    Grid_UR.Columns[0].Visible = true;
                    Grid_UR.DataSource = Dt_Listado;
                    Grid_UR.DataBind();
                    Grid_UR.Columns[0].Visible = false;
                } else{
                    Lbl_Ecabezado_Mensaje.Text = "Esa Unidad Responsable ya se encuentra Agregada en el Listado.";
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            } else {
                Lbl_Ecabezado_Mensaje.Text = "Se debe Seleccionar la Unidad Responsable a Agregar.";
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Agregar_Dependencia_Click
        ///DESCRIPCIÓN: Agrega la UR al Listado.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Todas_Dependencias_Click(object sender, EventArgs e)
        {
            DataTable Dt_Listado = new DataTable();
            DataTable Dt_Listado_Final = new DataTable();
                Cls_Ope_Psw_Negocio Negocio = new Cls_Ope_Psw_Negocio();
                if (!String.IsNullOrEmpty(Txt_No_Empleado.Text.Trim()))
                {
                    Dt_Listado = Negocio.Consultar_Dependencias();
                    Dt_Listado_Final = Obtener_Dt_Grid_UR().Clone();
                    Grid_UR.DataBind();
                    foreach (DataRow Dr_Cont_Renglones in Dt_Listado.Rows)
                    {
                        DataRow Fila_Nueva = Dt_Listado_Final.NewRow();
                        Fila_Nueva["DEPENDENCIA_ID"] = Dr_Cont_Renglones[0].ToString();
                        Fila_Nueva["CLAVE"] = Dr_Cont_Renglones[1].ToString().Substring(0, 5);
                        Fila_Nueva["NOMBRE"] = Dr_Cont_Renglones[1].ToString().Substring(5);
                        Dt_Listado_Final.Rows.Add(Fila_Nueva);
                    }
                    Limpiar_Formulario();
                    Grid_UR.Columns[0].Visible = true;
                    Grid_UR.DataSource = Dt_Listado_Final;
                    Grid_UR.DataBind();
                    Grid_UR.Columns[0].Visible = false;
                }
                else
                {
                    Lbl_Ecabezado_Mensaje.Text = "Se debe Seleccionar el Empleado a Asignar.";
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Quitar_Unidad_Responsable_Click
        ///DESCRIPCIÓN: Quita la UR del Listado
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Quitar_Unidad_Responsable_Click(object sender, ImageClickEventArgs e) {
            if (sender != null) {
                ImageButton Btn_Quitar_Unidad_Responsable = (ImageButton)sender;
                String Dependencia_ID = Btn_Quitar_Unidad_Responsable.CommandArgument.Trim();
                DataTable Dt_Listado = Obtener_Dt_Grid_UR();
                for (Int32 Contador = 0; Contador < Dt_Listado.Rows.Count; Contador++) {
                    if (Dt_Listado.Rows[Contador]["DEPENDENCIA_ID"].ToString().Trim().Equals(Dependencia_ID.Trim())) {
                        Dt_Listado.Rows.RemoveAt(Contador);
                        break;
                    }
                }
                Grid_UR.Columns[0].Visible = true;
                Grid_UR.DataSource = Dt_Listado;
                Grid_UR.DataBind();
                Grid_UR.Columns[0].Visible = false;
            }
        }

    #endregion

}
