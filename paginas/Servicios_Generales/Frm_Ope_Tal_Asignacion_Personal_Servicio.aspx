﻿<%@ Page Title="Asignación de Personal a Solicitud" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Tal_Asignacion_Personal_Servicio.aspx.cs" Inherits="paginas_Servicios_Generales_Frm_Ope_Tal_Asignacion_Personal_Servicio" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScptM_Operacion" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />  
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Loading" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>                     
            </asp:UpdateProgress>
            <div id="Div_Area_Trabajo" style="background-color:#ffffff; width:100%; height:100%;">
                <center>
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo" colspan="2">Asignación de Personal a Servicio</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                          <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;">              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top">
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                            </table>                   
                          </div>                
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>                        
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" style="width:50%;">
                            <asp:ImageButton ID="Btn_Asignacion_Mecanico" runat="server" ImageUrl="~/paginas/imagenes/paginas/accept.png" Width="24px" CssClass="Img_Button" AlternateText="Asignar Mecanico Seleccionado" ToolTip="Asignar Mecanico Seleccionado" OnClick="Btn_Asignacion_Mecanico_Click"/>
                            <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" Width="24px" CssClass="Img_Button" AlternateText="Salir" ToolTip="Salir" OnClick="Btn_Salir_Click" />
                        </td>
                        <td style="width:50%;">&nbsp;                                 
                        </td>                        
                    </tr>
                </table>   
                <br />
                <div id="Div_Listado_Servicios" runat="server" style="width:100%;">
                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                             <td style="width:15%;"><asp:Label ID="Lbl_Filtrado_Estatus" runat="server" Text="Filtrar [Estatus]"></asp:Label></td>
                             <td style="width:35%;">
                                 <asp:DropDownList ID="Cmb_Filtrado_Estatus" runat="server" Width="98%">
                                    <asp:ListItem Value="">&lt; - TODAS - &gt;</asp:ListItem>
                                    <asp:ListItem Value="AUTORIZADA">POR ASIGNAR</asp:ListItem>
                                    <asp:ListItem Value="PRE_COTIZACION">ASIGNADO</asp:ListItem>
                                 </asp:DropDownList>
                             </td>
                             <td style="width:15%;">
                                 <asp:ImageButton ID="Btn_Actualizar_Listado" runat="server" ToolTip="Actualizar Listado" AlternateText="Actualizar Listado" OnClick="Btn_Actualizar_Listado_Click" ImageUrl="~/paginas/imagenes/paginas/actualizar_detalle.png" Width="16px" />
                             </td>
                             <td style="width:35%;">&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                    </table>
                    <br />
                    <asp:GridView ID="Grid_Listado_Servicios" runat="server" CssClass="GridView_1"
                        AutoGenerateColumns="False" AllowPaging="True" PageSize="20" Width="99%"
                        GridLines= "None" EmptyDataText="No se Encontrarón Servicios Pendientes por Asignar"
                        OnPageIndexChanging="Grid_Listado_Servicios_PageIndexChanging"
                        OnSelectedIndexChanged="Grid_Listado_Servicios_SelectedIndexChanged"
                        >
                        <RowStyle CssClass="GridItem" />
                        <Columns>
                            <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png" >
                                <ItemStyle Width="30px" />
                            </asp:ButtonField>
                            <asp:BoundField DataField="NO_SOLICITUD" HeaderText="NO_SOLICITUD" SortExpression="NO_SOLICITUD">
                                <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FECHA_ELABORACION" HeaderText="F. Elaboración" SortExpression="FECHA_ELABORACION" DataFormatString="{0:dd/MMM/yyyy}">
                                <ItemStyle Width="110px" HorizontalAlign="Center" Font-Size="X-Small"/>
                            </asp:BoundField>
                            <asp:BoundField DataField="TIPO_SERVICIO" HeaderText="Tipo Servicio" SortExpression="TIPO_SERVICIO">
                                <ItemStyle Width="160px" HorizontalAlign="Center" Font-Size="X-Small"/>
                            </asp:BoundField>
                            <asp:BoundField DataField="NO_INVENTARIO" HeaderText="No. Inventario" SortExpression="NO_INVENTARIO">
                                <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small"/>
                            </asp:BoundField>
                            <asp:BoundField DataField="DESCRIPCION_SERVICIO" HeaderText="Descripción del Servicio" SortExpression="DESCRIPCION_SERVICIO">
                                <ItemStyle Font-Size="X-Small"/>
                            </asp:BoundField>
                        </Columns>
                        <PagerStyle CssClass="GridHeader" />
                        <SelectedRowStyle CssClass="GridSelected" />
                        <HeaderStyle CssClass="GridHeader" />                                
                        <AlternatingRowStyle CssClass="GridAltItem" />       
                    </asp:GridView>
                </div>                
                <div id="Div_Campos" runat="server" style="width:100%;">                  
                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                            <td colspan="4">
                                <asp:HiddenField ID="Hdf_No_Solicitud" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%;">
                                <asp:Label ID="Lbl_Folio" runat="server" Text="Folio" ForeColor="Black" Font-Bold="true"></asp:Label>
                            </td>
                            <td style="width:35%;">
                                <asp:TextBox ID="Txt_Folio" runat="server" Width="98%" ForeColor="Red" Font-Bold="true" style="text-align:right;"></asp:TextBox>
                            </td>
                            <td colspan="2">
                                &nbsp;&nbsp;
                            </td>  
                        </tr>
                        <tr>
                            <td style="width:15%;">
                                <asp:Label ID="Lbl_Fecha_Elaboracion" runat="server" Text="Fecha Elaboración"></asp:Label>
                            </td>
                            <td style="width:35%;">
                                <asp:TextBox ID="Txt_Fecha_Elaboracion" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                            </td>
                            <td colspan="2">
                                &nbsp;&nbsp;  
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%;">
                                <asp:Label ID="Lbl_Grupo_UR" runat="server" Text="Gerencia"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="Cmb_Grupo_UR" runat="server" Width="100%" Enabled="false">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%;">
                                <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text="Unidad Responsable"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="Cmb_Unidad_Responsable" runat="server" Width="100%" Enabled="false">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%;">
                                <asp:Label ID="Lbl_Tipo_Bien" runat="server" Text="Tipo Bien"></asp:Label>
                            </td>
                            <td style="width:35%;">
                                <asp:TextBox ID="Txt_Tipo_Bien" runat="server" Width="98%" Enabled="false">
                                </asp:TextBox>
                            </td>
                            <td style="width:15%;">
                                &nbsp;&nbsp;  
                                <asp:Label ID="Lbl_Tipo_Servicio" runat="server" Text="Tipo Servicio"></asp:Label>
                            </td>
                            <td style="width:35%;">
                                <asp:DropDownList ID="Cmb_Tipo_Servicio" runat="server" Width="100%" Enabled="false">
                                    <asp:ListItem Value="">&lt; - - SELECCIONE - - &gt;</asp:ListItem>
                                    <asp:ListItem Value="REPARACION">REPARACIÓN</asp:ListItem>
                                    <asp:ListItem Value="GARANTIA">GARANTIA</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4"> &nbsp;&nbsp; </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Panel ID="Pnl_Bien_Mueble_Seleccionado" runat="server" 
                                    GroupingText="Bien Mueble para el Servicio" Width="99%">
                                    <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:HiddenField ID="Hdf_Bien_Mueble_ID" runat="server" />
                                                <asp:Label ID="Lbl_No_Inventario_BM" runat="server" Text="No. Inventario"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_No_Inventario_BM" runat="server" MaxLength="7" Width="98%" Enabled="false"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Inventario_BM" runat="server" 
                                                    FilterType="Numbers" TargetControlID="Txt_No_Inventario_BM">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width:15%;">
                                                &nbsp;
                                            </td>
                                            <td style="width:35%;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:Label ID="Lbl_Descripcion_Bien" runat="server" Text="Descripción Bien"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="Txt_Descripcion_Bien" runat="server" Enabled="false" Rows="2" 
                                                    TextMode="MultiLine" Width="99%"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:Label ID="Lbl_Numero_Serie_Bien" runat="server" Text="No. Serie"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="Txt_Numero_Serie_Bien" runat="server" Enabled="false" 
                                                    Width="99%"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                             <td colspan="4">&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Panel ID="Pnl_Descripcion_Servicio" runat="server" Width="99%" GroupingText="Descripción del Servicio">
                                    <asp:TextBox ID="Txt_Descripcion_Servicio" runat="server" Rows="5" TextMode="MultiLine" Width="99%" Enabled="false"></asp:TextBox>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                             <td colspan="4">&nbsp;&nbsp;</td>
                        </tr>
                    </table>
                    <hr style="width:98%;"/>
                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                            <td style="width:15%;">
                                <asp:Label ID="Lbl_Mecanicos" runat="server" Text="Lista de Personal" Font-Bold="true"></asp:Label>
                            </td>
                            <td colspan="3" style="width:85%;">
                                <asp:DropDownList ID="Cmb_Mecanicos" runat="server" Width="100%" >
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </div>
                </center>        
            </div>
            <br />
            <br />
            <br />
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

