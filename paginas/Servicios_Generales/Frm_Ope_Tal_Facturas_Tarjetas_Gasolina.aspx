<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Ope_Tal_Facturas_Tarjetas_Gasolina.aspx.cs"
    Inherits="paginas_Taller_Frm_Ope_Tal_Facturas_Tarjetas_Gasolina" Title="Seguimiento a Requisiciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">
    <link href="../../jquery-easyui/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../../jquery-easyui/themes/icon.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">

    <script src="../../easyui/ui.datepicker-es.js" type="text/javascript"></script>
    <script src="../../jquery-easyui/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="../../javascript/validacion/json.js" type="text/javascript"></script>
    <script src="../../javascript/Js_Ope_Tal_Mov_Tarjetas.js" type="text/javascript"></script>

    <div id="Div_General" style="width: 98%;" visible="true" runat="server">
        <%--Div Encabezado--%>
        <div id="Div_Encabezado">
            <table style="width: 100%;" border="0" cellspacing="0">
                <tr align="center">
                    <td colspan="2" class="label_titulo">
                        Facturas de Movimientos de Tarjetas de Gasolina
                    </td>
                </tr>
                <tr align="left">
                    <td colspan="2">
                        <img id="Img_Warning" style="display: none" src="../../paginas/imagenes/paginas/sias_warning.png"
                            alt="" />
                        <span id="Lbl_Informacion" style="color: #990000"></span>
                    </td>
                </tr>
                <tr class="barra_busqueda" align="right">
                    <td align="left" valign="middle">
                        <img id="Img_Nuevo" alt="" src="" class="Img_Button" />
                        <img id="Img_Modificar" alt="" src="" class="Img_Button" />
                        <img id="Img_Eliminar" alt="" src="" class="Img_Button" />
                        <img id="Img_Listar_Requisiciones" alt="" src="" class="Img_Button" />
                        <img id="Img_Salir" alt="" src="" class="Img_Button" />
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
        <%--Div listado de requisiciones--%>
        <div id="Div_Listado_Movimientos">
            <table style="width: 100%;">
                <tr>
                    <td colspan="5">
                        <br />
                    </td>
                </tr>
                <tr>
                    <td style="width: 15%;">
                        Unidad Responsable
                    </td>
                    <td colspan="4" align="left">
                        <select id="Cmb_Dependencia_Panel" style="width: 100%" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 15%;">
                        Fecha
                    </td>
                    <td style="width: 17.5%;" align="left">
                        <input type="text" id="Dtp_Fecha_Inicial" style="width: 70%" />
                    </td>
                    <td style="width: 17.5%;" align="left">
                        <input type="text" id="Dtp_Fecha_Final" style="width: 70%" />
                    </td>
                    <td align="right" style="width: 15%;">
                        Numero Econ�mico
                    </td>
                    <td align="left" style="width: 35%;">
                        <input id="Txt_Economico" style="width: 99%" type="text" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 15;">
                    </td>
                    <td style="width: 35;" colspan="2">
                    </td>
                    <td style="width: 15;" align="right">
                        Tarjeta
                    </td>
                    <td align="left" style="width: 35%">
                        <input type="text" id="Txt_Tarjeta" style="width: 80%" />
                        <img id="Img_Buscar" alt="" src="" />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="5" style="width: 99%">
                        <br />
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 99%">
                        <div style="overflow: visible; height: 320px; width: 99%; vertical-align: top;">
                            <table id="Tbl_Movimientos">
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <%--popup Agregar Facturas--%> 
 <div  class="easyui-window" shadow="false" id="Ventana_Agregar_Facturas" modal="true" minimizable="false" maximizable="false" closed="true" iconCls="icon-add" title="Agregar Facturas a los movimientos Seleccinados" style="width:570px;height:auto;padding:5px">
     <table width="550px">
         <tr>
             <td>
                 Folio Factura
             </td>
             <td>
                 <input type="text" id="Txt_Folio_Factura" style="border-style: groove; width: 100px"
                     maxlength="10" class="validacion" />
                 &nbsp;&nbsp;
             </td>
             <td>
                 Fecha Factura
             </td>
             <td>
                 <input type="text" id="Txt_Fecha_Factura" style="border-style: groove; width: 100px"
                     maxlength="10" class="validacion" tipo="fecha" />
             </td>
         </tr>
         <tr>
             <td>
                 Monto
             </td>
             <td>
                 <input type="text" id="Txt_Monto" style="border-style: groove; width: 100px" />
             </td>
             <td>
                 Comisi�n Factura
             </td>
             <td>
                 <input type="text" id="Txt_Comision" style="border-style: groove; width: 100px" />
             </td>
         </tr>
         <tr>
             <td>
                 IVA
             </td>
             <td>
                 <input type="text" id="Txt_Iva" style="border-style: groove; width: 100px" />
             </td>
             <td>
                 Total Factura
             </td>
             <td>
                 <input type="text" id="Txt_Total_Factura" style="border-style: groove; width: 100px" />
                 <a href="#" class="easyui-linkbutton" plain="true" title="Agregar Factura" icon="icon-add"
                     onclick="Agregar_Facturas();"></a>             
             </td>
         </tr>
         <tr>
             <td align="right" colspan="4">
                 <br />
             </td>
         </tr>
         <tr>
             <td colspan="4">
                 <div style="overflow:visible; height:400px; width: 99%; vertical-align: top;">
                     <table id="Tbl_Facturas">
                     </table>
                 </div>
             </td>
         </tr>
         <tr>
             <td align="right" colspan="4">
                 <a href="#" class="easyui-linkbutton" plain="true" iconcls="icon-save" onclick="Guardar_Facturas();">
                     Guardar</a> <a href="#" class="easyui-linkbutton" plain="true" iconcls="icon-cancel"
                         onclick="Cerrar_Ventana();">Cancelar</a>
             </td>
         </tr>
     </table>
   
 </div>
</asp:Content>
