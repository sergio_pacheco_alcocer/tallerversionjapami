﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Catalogo_Taller_Refacciones.Negocio;
using JAPAMI.Catalogo_Compras_Unidades.Negocio;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using JAPAMI.Catalogo_Compras_Impuestos.Negocio;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;
using JAPAMI.Taller_Mecanico.Catalogo_Tipos_Refacciones.Negocio;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;

public partial class paginas_Taller_Municipal_Frm_Cat_Tal_Refacciones : System.Web.UI.Page
{

    #region Variables
    private const int Const_Estado_Inicial = 0;
    private const int Const_Estado_Nuevo = 1;
    private const int Const_Estado_Modificar = 2;
    #endregion

    #region Page Load / Init
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!Page.IsPostBack)
            {
                Txt_Descripcion.Attributes.Add("onkeypress", " Validar_Longitud_Texto(this, 250);");
                Txt_Descripcion.Attributes.Add("onkeyup", " Validar_Longitud_Texto(this, 250);");
                Estado_Botones(Const_Estado_Inicial);
                Configurar_Formulario();
                Cargar_Combo_Clasificacion();
                Cargar_Combo_Capitulos();
                Cargar_Combo_Impuesto();
                Cargar_Combo_Impuesto_2();
                Cmb_Capitulo_SelectedIndexChanged(Cmb_Capitulo, null);
                Limpiar_Formulario();
                Cargar_Refacciones(0);
            }
            Mensaje_Error();
        }
        catch (Exception Ex)
        {
            Estado_Botones(Const_Estado_Inicial);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Configurar_Formulario
    ///DESCRIPCIÓN: Cargar Combos y Datos del formulario
    ///CREO: jtoledo
    ///FECHA_CREO: 17/May/2012
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    private void Configurar_Formulario()
    {
        try
        {
            Limpiar_Formulario();
            Cls_Cat_Com_Unidades_Negocio Unidades_Negocio = new Cls_Cat_Com_Unidades_Negocio();
            Llenar_Combo_ID(Cmb_Unidad, Unidades_Negocio.Consulta_Unidades(), Cat_Com_Unidades.Campo_Nombre + "+" + Cat_Com_Unidades.Campo_Abreviatura, Cat_Com_Unidades.Campo_Unidad_ID, "0");
        }
        catch { }

    }
    #endregion

    #region Metodos

    #region Metodos Generales
    ///*******************************************************************************
    ///NOMBRE DE LA METODO: LLenar_Combo_Id
    ///        DESCRIPCIÓN: llena todos los combos
    ///         PARAMETROS: 1.- Obj_DropDownList: Combo a llenar
    ///                     2.- Dt_Temporal: DataTable genarada por una consulta a la base de datos
    ///                     3.- Texto: nombre de la columna del dataTable que mostrara el texto en el combo
    ///                     3.- Valor: nombre de la columna del dataTable que mostrara el valor en el combo
    ///                     3.- Seleccion: Id del combo el cual aparecera como seleccionado por default
    ///               CREO: Jesus S. Toledo Rdz.
    ///         FECHA_CREO: 06/9/2010
    ///           MODIFICO:
    ///     FECHA_MODIFICO:
    /// CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_Combo_ID(DropDownList Obj_DropDownList, DataTable Dt_Temporal, String _Texto, String _Valor, String Seleccion)
    {
        String Texto = "";
        try
        {
            Obj_DropDownList.Items.Clear();
            Obj_DropDownList.Items.Add(new ListItem("<-- SELECCIONE -->", "0"));
            foreach (DataRow row in Dt_Temporal.Rows)
            {
                if (_Texto.Contains("+"))
                {
                    String[] Array_Texto = _Texto.Split('+');

                    foreach (String Campo in Array_Texto)
                    {
                        Texto = Texto + row[Campo].ToString();
                        Texto = Texto + "  ";
                    }
                }
                else
                {
                    Texto = row[_Texto].ToString();
                }
                Obj_DropDownList.Items.Add(new ListItem(Texto, row[_Valor].ToString()));
                Texto = "";
            }
            Obj_DropDownList.SelectedValue = Seleccion;
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }

    }
    public void Llenar_Combo_ID(DropDownList Obj_DropDownList)
    {
        try
        {
            Obj_DropDownList.Items.Clear();
            Obj_DropDownList.Items.Add(new ListItem("< SELECCIONAR >", "0"));
            Obj_DropDownList.SelectedValue = "0";
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }

    }
    ///****************************************************************************************
    ///NOMBRE DE LA FUNCION:Mensaje_Error
    ///DESCRIPCION : Muestra el error
    ///PARAMETROS  : P_Texto: texto de un TextBox
    ///CREO        : Toledo Rodriguez Jesus S.
    ///FECHA_CREO  : 04-Septiembre-2010
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACION:
    ///****************************************************************************************
    private void Mensaje_Error(String P_Mensaje)
    {
        Img_Error.Visible = true;
        Lbl_Mensaje_Error.Text += P_Mensaje + "</br>";
    }
    private void Mensaje_Error()
    {
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Lbl_Ecabezado_Mensaje.Text = "";
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Estado_Botones
    ///DESCRIPCIÓN: Metodo para establecer el estado de los botones y componentes del formulario
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 02/02/2011 05:49:53 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Estado_Botones(int P_Estado)
    {
        Boolean Estado = false;
        switch (P_Estado)
        {
            case 0: //Estado inicial  
                Btn_Nuevo.AlternateText = "Nuevo";
                Btn_Modificar.AlternateText = "Modificar";
                Btn_Eliminar.AlternateText = "Eliminar";
                Btn_Salir.AlternateText = "Inicio";

                Btn_Nuevo.ToolTip = "Nuevo";
                Btn_Modificar.ToolTip = "Modificar";
                Btn_Eliminar.ToolTip = "Eliminar";
                Btn_Salir.ToolTip = "Inicio";

                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar.png";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";

                Btn_Nuevo.Visible = true;
                Btn_Modificar.Visible = true;
                Btn_Eliminar.Visible = true;
                Btn_Salir.Visible = true;
                Txt_Buscar.Enabled = true;
                Btn_Buscar.Visible = true;
                Estado = false;
                break;

            case 1: //Nuevo  
                Btn_Nuevo.AlternateText = "Guardar";
                Btn_Modificar.AlternateText = "Modificar";
                Btn_Eliminar.AlternateText = "Eliminar";
                Btn_Salir.AlternateText = "Cancelar";

                Btn_Nuevo.ToolTip = "Guardar";
                Btn_Modificar.ToolTip = "Modificar";
                Btn_Eliminar.ToolTip = "Eliminar";
                Btn_Salir.ToolTip = "Cancelar";

                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar.png";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";

                Btn_Nuevo.Visible = true;
                Btn_Modificar.Visible = false;
                Btn_Eliminar.Visible = false;
                Btn_Salir.Visible = true;
                Txt_Buscar.Enabled = false;
                Btn_Buscar.Visible = false;
                Cmb_Estatus.SelectedIndex = 1;
                Cmb_Tipo.SelectedIndex = 2;
                if (Cmb_Clasificacion.Items.Count > 1)
                    Cmb_Clasificacion.SelectedIndex = 1;
                String Id_Refaccion = Obtener_Dato_Consulta(Cat_Com_Unidades.Campo_Unidad_ID, Cat_Com_Unidades.Tabla_Cat_Com_Unidades, Cat_Com_Unidades.Campo_Nombre + " LIKE 'PIEZA'");
                Cmb_Unidad.SelectedIndex = Cmb_Unidad.Items.IndexOf(Cmb_Unidad.Items.FindByValue(Id_Refaccion));
                Txt_Nombre.Focus();

                Estado = true;
                break;

            case 2: //Modificar                    

                Btn_Nuevo.AlternateText = "Nuevo";
                Btn_Modificar.AlternateText = "Actualizar";
                Btn_Eliminar.AlternateText = "Eliminar";
                Btn_Salir.AlternateText = "Cancelar";

                Btn_Nuevo.ToolTip = "Nuevo";
                Btn_Modificar.ToolTip = "Actualizar";
                Btn_Eliminar.ToolTip = "Eliminar";
                Btn_Salir.ToolTip = "Cancelar";

                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar.png";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";

                Btn_Nuevo.Visible = false;
                Btn_Modificar.Visible = true;
                Btn_Eliminar.Visible = false;
                Btn_Salir.Visible = true;
                Txt_Buscar.Enabled = false;
                Btn_Buscar.Visible = false;

                Estado = true;
                break;

        }

        Txt_Descripcion.Enabled = Estado;
        Txt_Nombre.Enabled = Estado;
        Cmb_Clasificacion.Enabled = Estado;
        Cmb_Estatus.Enabled = Estado;
        Cmb_Tipo.Enabled = Estado;
        Cmb_Unidad.Enabled = Estado;
        Txt_Costo_Unitario.Enabled = Estado;

        Cmb_Capitulo.Enabled = Estado;
        Cmb_Conceptos.Enabled = Estado;
        Cmb_Partida_General.Enabled = Estado;
        Cmb_Partida_Especifica.Enabled = Estado;
        Cmb_Impuesto.Enabled = Estado;
        Cmb_Impuesto_2.Enabled = Estado;

        Txt_Existencia.Enabled = Estado;
        Txt_Minimo.Enabled = Estado;
        Txt_Maximo.Enabled = Estado;
        Txt_Ubicacion.Enabled = Estado;
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
    ///DESCRIPCIÓN: Metodo para establecer el estado de los botones y componentes del formulario
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 02/02/2011 05:49:53 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Limpiar_Formulario()
    {
        //Session["Tabla_Cat_Refacciones"] = null;
        Txt_Clave.Text = "";
        Txt_Nombre.Text = "";
        Cmb_Clasificacion.SelectedIndex = 0;
        Txt_Descripcion.Text = "";
        Cmb_Estatus.SelectedIndex = 0;
        Txt_Costo_Unitario.Text = "";
        Cmb_Unidad.SelectedIndex = 0;
        Cmb_Tipo.SelectedIndex = 0;
        Grid_Refacciones.PageIndex = 0;
        Grid_Refacciones.SelectedIndex = (-1);
        Cmb_Capitulo.SelectedIndex = 0;
        Cmb_Capitulo_SelectedIndexChanged(Cmb_Capitulo, null);
        //Cmb_Impuesto.SelectedIndex = 0;
        Cmb_Impuesto_SelectedIndexChanged(Cmb_Impuesto, null);
        //Cmb_Impuesto_2.SelectedIndex = 0;
        Txt_Existencia.Text = "";
        Txt_Minimo.Text = "";
        Txt_Comprometido.Text = "";
        Txt_Maximo.Text = "";
        Txt_Disponible.Text = "";
        Txt_Reorden.Text = "";
        Txt_Ubicacion.Text = "";
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cargar_Combo_Impuesto
    /// DESCRIPCION: Consulta los Impuesto en la base de datos (Cat_Com_Impuestos)
    ///             Y carga el combo Impuesto
    /// PARAMETROS: 
    /// CREO: Roberto González Oseguera
    /// FECHA_CREO: 03-Feb-2011
    /// MODIFICO: Francisco Antonio Gallardo Castañeda
    /// FECHA_MODIFICO: 26/Junio/2012
    /// CAUSA_MODIFICACION: 
    ///*******************************************************************************
    private void Cargar_Combo_Impuesto()
    {
        DataTable Dt_Impuesto; //Variable que obtendrá los datos de la consulta        
        Cls_Cat_Com_Impuestos_Negocio Rs_Consulta_Cat_Impuestos = new Cls_Cat_Com_Impuestos_Negocio(); //Variable de conexión hacia la capa de Negocios

        try
        {
            Dt_Impuesto = Rs_Consulta_Cat_Impuestos.Consulta_Impuestos(); //Consulta todos los Impuestos que estan dadas de alta en la BD
            Cmb_Impuesto.DataSource = Dt_Impuesto;
            Cmb_Impuesto.DataValueField = "Impuesto_ID";
            Cmb_Impuesto.DataTextField = "Nombre";
            Cmb_Impuesto.DataBind();
            Cmb_Impuesto.Items.Insert(0, "<-- SELECCIONE -->");
            Cmb_Impuesto.SelectedIndex = 0;
            if (Dt_Impuesto.Rows.Count > 0)
                Cmb_Impuesto.SelectedIndex = 1;
        }
        catch (Exception ex)
        {
            throw new Exception("Cargar_Combo_Impuesto " + ex.Message.ToString(), ex);
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cargar_Combo_Impuesto_2
    /// DESCRIPCION: Consulta los Impuesto en la base de datos (Cat_Com_Impuestos)
    /// 			para poblar el combo Impuesto_2
    /// PARAMETROS: 
    /// CREO: Roberto González Oseguera
    /// FECHA_CREO: 03-Feb-2011
    /// MODIFICO:
    /// FECHA_MODIFICO:
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Cargar_Combo_Impuesto_2()
    {
        DataTable Dt_Impuesto; //Variable que obtendrá los datos de la consulta        
        Cls_Cat_Com_Impuestos_Negocio Rs_Consulta_Cat_Impuestos = new Cls_Cat_Com_Impuestos_Negocio(); //Variable de conexión hacia la capa de Negocios

        try
        {
            Dt_Impuesto = Rs_Consulta_Cat_Impuestos.Consulta_Impuestos(); //Consulta todos los Impuestos que estan dadas de alta en la BD
            Cmb_Impuesto_2.DataSource = Dt_Impuesto;
            Cmb_Impuesto_2.DataValueField = "Impuesto_ID";
            Cmb_Impuesto_2.DataTextField = "Nombre";
            Cmb_Impuesto_2.DataBind();
            Cmb_Impuesto_2.Items.Insert(0, "<-- SELECCIONE -->");
            Cmb_Impuesto_2.SelectedIndex = 0;
            //if (Dt_Impuesto.Rows.Count > 0)
            //Cmb_Impuesto_2.SelectedIndex = Dt_Impuesto.Rows.Count;

        }
        catch (Exception ex)
        {
            throw new Exception("Cargar_Combo_Impuesto_2 " + ex.Message.ToString(), ex);
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cargar_Combo_Clasificacion
    /// DESCRIPCION: Consulta la Clasificacion
    /// PARAMETROS: 
    /// CREO:  
    /// FECHA_CREO: 03-Feb-2011
    /// MODIFICO:
    /// FECHA_MODIFICO:
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Cargar_Combo_Clasificacion()
    {
        DataTable Dt_Datos; //Variable que obtendrá los datos de la consulta        
        Cls_Cat_Tal_Tipos_Refacciones_Negocio Rs_Consulta = new Cls_Cat_Tal_Tipos_Refacciones_Negocio(); //Variable de conexión hacia la capa de Negocios

        try
        {
            Dt_Datos = Rs_Consulta.Consultar_Nivel(); //Consulta todos los Impuestos que estan dadas de alta en la BD
            Cmb_Clasificacion.DataSource = Dt_Datos;
            Cmb_Clasificacion.DataValueField = Cat_Tal_Tipos_Refacciones.Campo_Tipo_Refaccion_ID;
            Cmb_Clasificacion.DataTextField = Cat_Tal_Tipos_Refacciones.Campo_Nombre;
            Cmb_Clasificacion.DataBind();
            Cmb_Clasificacion.Items.Insert(0, "<-- SELECCIONE -->");
            Cmb_Clasificacion.SelectedIndex = 0;

        }
        catch (Exception ex)
        {
            throw new Exception("Cargar_Combo_Impuesto_2 " + ex.Message.ToString(), ex);
        }
    }

    #endregion

    #region Metodos ABC

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Modificar_Refaccion
    ///DESCRIPCIÓN: se obtienen los datos para modificar la refaccion
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 06/27/2011 11:12:18 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    private void Modificar_Refaccion()
    {
        Cls_Cat_Tal_Refacciones_Negocio Refacciones_Negocio = new Cls_Cat_Tal_Refacciones_Negocio();
        try
        {
            if (Validar_Campos())
            {
                Refacciones_Negocio.P_Refaccion_ID = Grid_Refacciones.SelectedDataKey["REFACCION_ID"].ToString();
                Refacciones_Negocio.P_Nombre = Txt_Nombre.Text.Trim().ToUpper();
                Refacciones_Negocio.P_Tipo_Refaccion_ID = Cmb_Clasificacion.SelectedItem.Value.Trim();
                Refacciones_Negocio.P_Descripcion = ((Txt_Descripcion.Text.Trim().ToUpper().Length > 0) ? Txt_Descripcion.Text.Trim().ToUpper() : Txt_Nombre.Text.Trim().ToUpper());
                Refacciones_Negocio.P_Estatus = Cmb_Estatus.SelectedValue.ToString().Trim().ToUpper();
                Refacciones_Negocio.P_Tipo = Cmb_Tipo.SelectedValue.ToString().Trim().ToUpper();
                Refacciones_Negocio.P_Costo_Unitario = (Txt_Costo_Unitario.Text.Trim().Length > 0) ? Txt_Costo_Unitario.Text.Trim() : "0";
                if (Cmb_Unidad.SelectedIndex > 0) Refacciones_Negocio.P_Unidad_ID = Cmb_Unidad.SelectedValue.ToString().Trim().ToUpper();
                if (Cmb_Partida_Especifica.SelectedIndex > 0) Refacciones_Negocio.P_Partida_ID = Cmb_Partida_Especifica.SelectedValue.ToString().Trim().ToUpper();
                if (Cmb_Impuesto.SelectedIndex > 0) Refacciones_Negocio.P_Impuesto_ID = Cmb_Impuesto.SelectedValue.ToString().Trim().ToUpper();
                if (Cmb_Impuesto_2.SelectedIndex > 0) Refacciones_Negocio.P_Impuesto_2_ID = Cmb_Impuesto_2.SelectedValue.ToString().Trim().ToUpper();
                if (Cmb_Tipo.SelectedItem.Value.Trim().Equals("STOCK"))
                {
                    if (Txt_Existencia.Text.Trim().Length > 0) Refacciones_Negocio.P_Existencia = Convert.ToInt32(Txt_Existencia.Text);
                    if (Txt_Maximo.Text.Trim().Length > 0) Refacciones_Negocio.P_Maximo = Convert.ToInt32(Txt_Maximo.Text);
                    if (Txt_Minimo.Text.Trim().Length > 0) Refacciones_Negocio.P_Minimo = Convert.ToInt32(Txt_Minimo.Text);
                    if (Txt_Reorden.Text.Trim().Length > 0) Refacciones_Negocio.P_Reorden = Convert.ToInt32(Txt_Reorden.Text);
                }
                Refacciones_Negocio.P_Ubicacion = Txt_Ubicacion.Text.Trim();
                Refacciones_Negocio.Modificar_Refaccion();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Refaciones", "alert('La modificacion de la Refacción fue Exitosa');", true);
                Estado_Botones(Const_Estado_Inicial);
                Limpiar_Formulario();
                Cargar_Refacciones(0);
            }

        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Alta_Refaccion
    ///DESCRIPCIÓN: se obtienen los datos para dar de alta la refaccion
    ///PARAMETROS: 
    ///CREO: jesus toledo
    ///FECHA_CREO: 05/Mayo/2012 11:12:18 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    private void Alta_Refaccion()
    {
        Cls_Cat_Tal_Refacciones_Negocio Refacciones_Negocio = new Cls_Cat_Tal_Refacciones_Negocio();
        try
        {
            if (Validar_Campos())
            {
                Refacciones_Negocio.P_Nombre = Txt_Nombre.Text.Trim().ToUpper();
                Refacciones_Negocio.P_Tipo_Refaccion_ID = Cmb_Clasificacion.SelectedItem.Value.Trim();
                Refacciones_Negocio.P_Descripcion = ((Txt_Descripcion.Text.Trim().ToUpper().Length > 0) ? Txt_Descripcion.Text.Trim().ToUpper() : Txt_Nombre.Text.Trim().ToUpper());
                Refacciones_Negocio.P_Estatus = Cmb_Estatus.SelectedValue.ToString().Trim().ToUpper();
                Refacciones_Negocio.P_Tipo = Cmb_Tipo.SelectedValue.ToString().Trim().ToUpper();
                Refacciones_Negocio.P_Costo_Unitario = (Txt_Costo_Unitario.Text.Trim().Length > 0) ? Txt_Costo_Unitario.Text.Trim() : "0";
                if (Cmb_Unidad.SelectedIndex > 0) Refacciones_Negocio.P_Unidad_ID = Cmb_Unidad.SelectedValue.ToString().Trim().ToUpper();
                if (Cmb_Partida_Especifica.SelectedIndex > 0) Refacciones_Negocio.P_Partida_ID = Cmb_Partida_Especifica.SelectedValue.ToString().Trim().ToUpper();
                if (Cmb_Impuesto.SelectedIndex > 0) Refacciones_Negocio.P_Impuesto_ID = Cmb_Impuesto.SelectedValue.ToString().Trim().ToUpper();
                if (Cmb_Impuesto_2.SelectedIndex > 0) Refacciones_Negocio.P_Impuesto_2_ID = Cmb_Impuesto_2.SelectedValue.ToString().Trim().ToUpper();
                if (Cmb_Tipo.SelectedItem.Value.Trim().Equals("STOCK"))
                {
                    if (Txt_Existencia.Text.Trim().Length > 0) Refacciones_Negocio.P_Existencia = Convert.ToInt32(Txt_Existencia.Text);
                    if (Txt_Comprometido.Text.Trim().Length > 0) Refacciones_Negocio.P_Comprometido = Convert.ToInt32(Txt_Comprometido.Text);
                    if (Txt_Disponible.Text.Trim().Length > 0) Refacciones_Negocio.P_Disponible = Convert.ToInt32(Txt_Disponible.Text);
                    if (Txt_Maximo.Text.Trim().Length > 0) Refacciones_Negocio.P_Maximo = Convert.ToInt32(Txt_Maximo.Text);
                    if (Txt_Minimo.Text.Trim().Length > 0) Refacciones_Negocio.P_Minimo = Convert.ToInt32(Txt_Minimo.Text);
                    if (Txt_Reorden.Text.Trim().Length > 0) Refacciones_Negocio.P_Reorden = Convert.ToInt32(Txt_Reorden.Text);
                }
                Refacciones_Negocio.P_Ubicacion = Txt_Ubicacion.Text.Trim();
                Refacciones_Negocio.Alta_Refaccion();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Refaciones", "alert('El alta de la Refacción fue Exitosa');", true);
                Estado_Botones(Const_Estado_Inicial);
                Limpiar_Formulario();
                Cargar_Refacciones(0);
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Eliminar_Refaccion
    ///DESCRIPCIÓN: se obtienen los datos para dar de alta la refaccion
    ///PARAMETROS: 
    ///CREO: jesus toledo
    ///FECHA_CREO: 05/Mayo/2012 11:12:18 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    private void Eliminar_Refaccion()
    {
        Cls_Cat_Tal_Refacciones_Negocio Refacciones_Negocio = new Cls_Cat_Tal_Refacciones_Negocio();
        try
        {
            if (Grid_Refacciones.SelectedIndex >= 0)
            {
                Refacciones_Negocio.P_Refaccion_ID = Grid_Refacciones.SelectedDataKey["REFACCION_ID"].ToString();
                Refacciones_Negocio.Eliminar_Refaccion();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Refaciones", "alert('La refacción se dio de baja exitosamente');", true);
                Estado_Botones(Const_Estado_Inicial);
                Limpiar_Formulario();
                Cargar_Refacciones(0);
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA CLASE: Cargar_Refacciones
    ///DESCRIPCIÓN: consulta y muestra los datos, instancia la clase de negocios para acceder a la consulta
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 15/May/2012 06:24:15 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    private void Cargar_Refacciones(int Page_Index)
    {
        DataTable Dt_Refacciones = new DataTable();
        Cls_Cat_Tal_Refacciones_Negocio Refacciones_Negocio = new Cls_Cat_Tal_Refacciones_Negocio();
        try
        {
            Refacciones_Negocio.P_Filtro = Txt_Buscar.Text.Trim().ToUpper();
            Dt_Refacciones = Refacciones_Negocio.Consultar_Refaccion();

            if (Dt_Refacciones.Rows.Count > 0)
            {
                Session["Tabla_Cat_Refacciones"] = Dt_Refacciones;
                Grid_Refacciones.PageIndex = Page_Index;
                Grid_Refacciones.DataSource = Dt_Refacciones;
                Grid_Refacciones.DataBind();
            }
            else
            {
                Grid_Refacciones.DataSource = null;
                Grid_Refacciones.DataBind();
                Mensaje_Error("No se encontraron refacciones con la clave proporcionada");
            }
            Grid_Refacciones.SelectedIndex = (-1);
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }

    #endregion

    #region Metodos/Validaciones
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Validar_Campos
    ///DESCRIPCIÓN: valdia que se ingresen los campos obligatorios
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 06/27/2011 11:31:53 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private Boolean Validar_Campos()
    {
        Boolean Resultado = true;

        if (Txt_Nombre.Text.Trim() == "")
        {
            Lbl_Ecabezado_Mensaje.Text = "Los siguientes datos son necesarios:";
            Mensaje_Error("- Nombre de la refacción");
            Resultado = false;
        }
        if (Cmb_Unidad.SelectedIndex <= 0)
        {
            Lbl_Ecabezado_Mensaje.Text = "Los siguientes datos son necesarios:";
            Mensaje_Error("- Unidad");
            Resultado = false;
        }
        if (Cmb_Clasificacion.SelectedIndex <= 0)
        {
            Lbl_Ecabezado_Mensaje.Text = "Los siguientes datos son necesarios:";
            Mensaje_Error("- Clasificación");
            Resultado = false;
        }
        if (Cmb_Estatus.SelectedIndex <= 0)
        {
            Lbl_Ecabezado_Mensaje.Text = "Los siguientes datos son necesarios:";
            Mensaje_Error("- Estatus de la refacción");
            Resultado = false;
        }
        if (Cmb_Tipo.SelectedIndex <= 0)
        {
            Lbl_Ecabezado_Mensaje.Text = "Los siguientes datos son necesarios:";
            Mensaje_Error("- Tipo de la refacción");
            Resultado = false;
        }
        if (Cmb_Impuesto.SelectedIndex <= 0 && Cmb_Impuesto_2.SelectedIndex <= 0)
        {
            Lbl_Ecabezado_Mensaje.Text = "Los siguientes datos son necesarios:";
            Mensaje_Error("- Impuesto");
            Resultado = false;
        }

        if (Txt_Costo_Unitario.Text.Trim() == "")
        {
            Lbl_Ecabezado_Mensaje.Text = "Los siguientes datos son necesarios:";
            Mensaje_Error("- Costo");
            Resultado = false;
        }
        else
        {
            if (Convert.ToDouble(Txt_Costo_Unitario.Text) == 0)
            {
                Lbl_Ecabezado_Mensaje.Text = "Los siguientes datos son necesarios:";
                Mensaje_Error("- Costo");
                Resultado = false;
            }
        }
        if (Cmb_Tipo.SelectedItem.Value.Trim().Equals("STOCK"))
        {
            if (Txt_Existencia.Text.Trim() == "")
            {
                Lbl_Ecabezado_Mensaje.Text = "Los siguientes datos son necesarios:";
                Mensaje_Error("- Existencia");
                Resultado = false;
            }
            if (Txt_Minimo.Text.Trim() == "")
            {
                Lbl_Ecabezado_Mensaje.Text = "Los siguientes datos son necesarios:";
                Mensaje_Error("- Minimo");
                Resultado = false;
            }
            if (Txt_Maximo.Text.Trim() == "")
            {
                Lbl_Ecabezado_Mensaje.Text = "Los siguientes datos son necesarios:";
                Mensaje_Error("- Maximo");
                Resultado = false;
            }
        }

        return Resultado;
    }
    #endregion

    #region Metodos Operacion
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Mostrar_Refaccion
    ///DESCRIPCIÓN: Cargar datos de Refaccion
    ///PARAMETROS: 
    ///CREO: Jesus Toledo
    ///FECHA_CREO: 05/May/2012
    ///MODIFICO: v
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************  
    private void Mostrar_Refaccion()
    {
        String Index_Refaccion;
        DataTable Dt_Refacciones;
        DataRow[] Dr_Seleccionado;
        try
        {
            if (Grid_Refacciones.SelectedIndex >= 0)
            {
                if (!String.IsNullOrEmpty(Grid_Refacciones.SelectedDataKey["REFACCION_ID"].ToString()))
                {
                    Index_Refaccion = Grid_Refacciones.SelectedDataKey["REFACCION_ID"].ToString();
                    if (Session["Tabla_Cat_Refacciones"] != null)
                    {
                        Dt_Refacciones = (DataTable)Session["Tabla_Cat_Refacciones"];
                        Dr_Seleccionado = Dt_Refacciones.Select(Cat_Tal_Refacciones.Campo_Refaccion_ID + " = " + Index_Refaccion);
                        if (Dr_Seleccionado.Length > 0)
                        {
                            Txt_Clave.Text = Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Clave].ToString();
                            Txt_Nombre.Text = Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Nombre].ToString();
                            Cmb_Clasificacion.SelectedIndex = Cmb_Clasificacion.Items.IndexOf(Cmb_Clasificacion.Items.FindByValue(Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Tipo_Refaccion_ID].ToString()));
                            Txt_Descripcion.Text = Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Descripcion].ToString();
                            Cmb_Estatus.SelectedValue = Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Estatus].ToString();
                            Cmb_Tipo.SelectedValue = Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Tipo].ToString();
                            if (!String.IsNullOrEmpty(Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Costo_Unitario].ToString())) Txt_Costo_Unitario.Text = Convert.ToDouble(Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Costo_Unitario]).ToString("#,###,#0.00");
                            else Txt_Costo_Unitario.Text = "";
                            if (!String.IsNullOrEmpty(Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Unidad_ID].ToString())) Cmb_Unidad.SelectedValue = Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Unidad_ID].ToString();
                            else Cmb_Unidad.SelectedValue = "0";
                            Cargar_Datos_Combos_SGC(Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Partida_ID].ToString());
                            Cmb_Impuesto.SelectedIndex = (!String.IsNullOrEmpty(Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Impuesto_ID].ToString())) ? Cmb_Impuesto.Items.IndexOf(Cmb_Impuesto.Items.FindByValue(Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Impuesto_ID].ToString())) : 0;
                            Cmb_Impuesto_2.SelectedIndex = (!String.IsNullOrEmpty(Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Impuesto_2_ID].ToString())) ? Cmb_Impuesto_2.Items.IndexOf(Cmb_Impuesto_2.Items.FindByValue(Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Impuesto_2_ID].ToString())) : 0;
                            Txt_Ubicacion.Text = (!String.IsNullOrEmpty(Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Ubicacion].ToString())) ? Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Ubicacion].ToString().Trim() : "";
                            Txt_Existencia.Text = (!String.IsNullOrEmpty(Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Existencia].ToString())) ? Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Existencia].ToString().Trim() : "0";
                            Txt_Minimo.Text = (!String.IsNullOrEmpty(Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Minimo].ToString())) ? Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Minimo].ToString().Trim() : "0";
                            Txt_Comprometido.Text = (!String.IsNullOrEmpty(Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Comprometido].ToString())) ? Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Comprometido].ToString().Trim() : "0";
                            Txt_Maximo.Text = (!String.IsNullOrEmpty(Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Maximo].ToString())) ? Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Maximo].ToString().Trim() : "0";
                            Txt_Disponible.Text = (!String.IsNullOrEmpty(Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Disponible].ToString())) ? Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Disponible].ToString().Trim() : "0";
                            Txt_Reorden.Text = (!String.IsNullOrEmpty(Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Reorden].ToString())) ? Dr_Seleccionado[0][Cat_Tal_Refacciones.Campo_Reorden].ToString().Trim() : "0";
                        }
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    #endregion

    #endregion

    #region Eventos/Botones
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
    ///DESCRIPCIÓN: se obtienen los datos para dar de alta refaccion
    ///PARAMETROS: 
    ///CREO: Jesus Toledo Rodriguez
    ///FECHA_CREO: 05/Mayo/2012
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Nuevo.AlternateText == "Nuevo")
            {
                Limpiar_Formulario();
                Estado_Botones(Const_Estado_Nuevo);

                Cls_Tal_Parametros_Negocio Par_Nego = new Cls_Tal_Parametros_Negocio();
                Par_Nego = Par_Nego.Consulta_Parametros();
                if (!String.IsNullOrEmpty(Par_Nego.P_Partida_ID))
                {
                    Cargar_Datos_Combos_SGC(Par_Nego.P_Partida_ID);
                }

            }
            else if (Btn_Nuevo.AlternateText == "Guardar")
            {
                Alta_Refaccion();
            }

        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message.ToString());
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
    ///DESCRIPCIÓN: se obtienen los datos para modificar los paramentros
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 06/27/2011 11:10:44 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Grid_Refacciones.SelectedIndex >= 0)
            {
                if (Btn_Modificar.AlternateText == "Modificar")
                {
                    Estado_Botones(Const_Estado_Modificar);
                }
                else if (Btn_Modificar.AlternateText == "Actualizar")
                {
                    Modificar_Refaccion();
                }
            }
            else
            {
                Mensaje_Error("Seleccione una refacción");
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message.ToString());
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Eliminar_Click
    ///DESCRIPCIÓN: se obtienen los datos para eliminar la refaccion
    ///PARAMETROS: 
    ///CREO: Jesus Toledo Rdz
    ///FECHA_CREO: 05/Mayo/2012 11:10:44 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Btn_Eliminar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Grid_Refacciones.SelectedIndex >= 0)
            {
                Eliminar_Refaccion();
            }
            else
            {
                Mensaje_Error("Seleccione una refacción");
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message.ToString());
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Salir/Cancelar
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 10/Mayo/2011 12:55:33 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Salir.AlternateText.Equals("Inicio"))
            {
                Limpiar_Formulario();
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Estado_Botones(Const_Estado_Inicial);
                Limpiar_Formulario();
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Aceptar_Busqueda_Av_Click
    ///DESCRIPCIÓN: Evento para buscar la constancia
    ///PARAMETROS: 
    ///CREO: jesus toledo
    ///FECHA_CREO: 27/junio/2011 01:38:02 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Estado_Botones(Const_Estado_Inicial);
            //Limpiar_Formulario();
            Session["Tabla_Cat_Refacciones"] = null;
            Cargar_Refacciones(0);
            //Txt_Buscar.Text = "";
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Txt_Minimo_TextChanged
    ///DESCRIPCIÓN: Carga el Reorden
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 03/Ago/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    protected void Txt_Minimo_TextChanged(object sender, EventArgs e)
    {
        try
        {
            Txt_Reorden.Text = Obtener_Reorden().ToString();
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Txt_Maximo_TextChanged
    ///DESCRIPCIÓN: Carga el Reorden
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 03/Ago/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    protected void Txt_Maximo_TextChanged(object sender, EventArgs e)
    {
        try
        {
            Txt_Reorden.Text = Obtener_Reorden().ToString();
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }

    protected void Txt_Buscar_TextChanged(object sender, EventArgs e)
    {

    }


    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Grid_Ordenes_Variacion_PageIndexChanging
    ///DESCRIPCIÓN: Paginar grid de ordenes de variacion
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 01/Dic/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Grid_Refacciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Cargar_Refacciones(e.NewPageIndex);
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Grid_Ordenes_Variacion_PageIndexChanging
    ///DESCRIPCIÓN: Paginar grid de ordenes de variacion
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 01/Dic/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Grid_Refaccion_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Grid_Refacciones.SelectedIndex > (-1))
        {
            Int32 Seleccion = Grid_Refacciones.SelectedIndex;
            Limpiar_Formulario();
            Grid_Refacciones.SelectedIndex = Seleccion;
            Mostrar_Refaccion();
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cmb_Capitulo_SelectedIndexChanged
    ///DESCRIPCIÓN: Paginar grid de ordenes de variacion
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 01/Dic/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Cmb_Capitulo_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cargar_Combo_Conceptos();
        Cmb_Conceptos_SelectedIndexChanged(Cmb_Conceptos, null);
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cmb_Conceptos_SelectedIndexChanged
    ///DESCRIPCIÓN: Paginar grid de ordenes de variacion
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 01/Dic/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Cmb_Conceptos_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cargar_Combo_Partidas_Genericas();
        Cmb_Partida_General_SelectedIndexChanged(Cmb_Partida_General, null);
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cmb_Partida_General_SelectedIndexChanged
    ///DESCRIPCIÓN: Paginar grid de ordenes de variacion
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 01/Dic/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Cmb_Partida_General_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cargar_Combo_Partidas_Especificas();
    }

    ///*******************************************************************************************************
    /// 	NOMBRE_FUNCIÓN: Cmb_Impuesto_SelectedIndexChanged
    /// 	DESCRIPCIÓN: Cuando cambie el índice seleccionado del combo, volver a poblar el combo Cmb_Impuesto_2
    /// 	PARÁMETROS:
    /// 	CREO: Roberto González Oseguera
    /// 	FECHA_CREO: 09-feb-2011
    /// 	MODIFICÓ: 
    /// 	FECHA_MODIFICÓ: 
    /// 	CAUSA_MODIFICACIÓN: 
    ///*******************************************************************************************************
    protected void Cmb_Impuesto_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Volver a cargar el combo Impuesto_2
        Cargar_Combo_Impuesto_2();
        // Si hay un valor seleccionado en el combo Cmb_Impuesto, qutarlo del combo Cmb_Impuesto_2
        if (Cmb_Impuesto.SelectedIndex > 0 && Cmb_Impuesto.SelectedItem.ToString() != "TASA 0")
        {
            Cmb_Impuesto_2.Items.RemoveAt(Cmb_Impuesto.SelectedIndex);
            Cmb_Impuesto_2.Focus();
        }
    }

    ///*******************************************************************************************************
    /// 	NOMBRE_FUNCIÓN: Txt_Existencia_TextChanged
    /// 	DESCRIPCIÓN: Asigna valores a los campos Comprometido y Disponible, de 0 e igual a Existencia 
    /// 	respectivamente, cuando un poducto se da de alta. Y cuando un producto se modifica, sólo permite
    /// 	aumentar la existencia (el nuevo valor no puede ser menor)
    /// 	PARÁMETROS:
    /// 	CREO: Roberto González Oseguera
    /// 	FECHA_CREO: 11-feb-2011
    /// 	MODIFICÓ: 
    /// 	FECHA_MODIFICÓ: 
    /// 	CAUSA_MODIFICACIÓN: 
    ///*******************************************************************************************************
    protected void Txt_Existencia_TextChanged(object sender, EventArgs e)
    {
        Txt_Disponible.Text = Obtener_Disponible().ToString();
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cargar_Combo_Partidas_Especificas
    ///DESCRIPCIÓN: Paginar grid de ordenes de variacion
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 01/Dic/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************* 
    private void Cargar_Combo_Capitulos()
    {
        Cls_Ope_Tal_Consultas_Generales_Negocio Consultas_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
        Consultas_Negocio.P_Estatus = "ACTIVO";
        DataTable Dt_Capitulos = Consultas_Negocio.Consultar_Capitulos();
        Cmb_Capitulo.DataSource = Dt_Capitulos;
        Cmb_Capitulo.DataValueField = "CAPITULO_ID";
        Cmb_Capitulo.DataTextField = "CLAVE_NOMBRE";
        Cmb_Capitulo.DataBind();
        Cmb_Capitulo.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cmb_Partida_General_SelectedIndexChanged
    ///DESCRIPCIÓN: Paginar grid de ordenes de variacion
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 01/Dic/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************* 
    private void Cargar_Combo_Conceptos()
    {
        if (Cmb_Capitulo.SelectedIndex > 0)
        {
            Cls_Ope_Tal_Consultas_Generales_Negocio Consultas_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            Consultas_Negocio.P_Estatus = "ACTIVO";
            Consultas_Negocio.P_Capitulo_ID = Cmb_Capitulo.SelectedItem.Value.Trim();
            DataTable Dt_Conceptos = Consultas_Negocio.Consultar_Conceptos();
            Cmb_Conceptos.DataSource = Dt_Conceptos;
            Cmb_Conceptos.DataValueField = "CONCEPTO_ID";
            Cmb_Conceptos.DataTextField = "CLAVE_NOMBRE";
            Cmb_Conceptos.DataBind();
            Cmb_Conceptos.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        }
        else
        {
            Cmb_Conceptos.Items.Clear();
            Cmb_Conceptos.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        }

    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cargar_Combo_Partidas_Genericas
    ///DESCRIPCIÓN: Paginar grid de ordenes de variacion
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 01/Dic/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************* 
    private void Cargar_Combo_Partidas_Genericas()
    {
        if (Cmb_Conceptos.SelectedIndex > 0)
        {
            Cls_Ope_Tal_Consultas_Generales_Negocio Consultas_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            Consultas_Negocio.P_Estatus = "ACTIVO";
            Consultas_Negocio.P_Concepto_ID = Cmb_Conceptos.SelectedItem.Value.Trim();
            DataTable Dt_Partidas = Consultas_Negocio.Consultar_Partidas_Genericas();
            Cmb_Partida_General.DataSource = Dt_Partidas;
            Cmb_Partida_General.DataValueField = "PARTIDA_ID";
            Cmb_Partida_General.DataTextField = "CLAVE_NOMBRE";
            Cmb_Partida_General.DataBind();
            Cmb_Partida_General.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        }
        else
        {
            Cmb_Partida_General.Items.Clear();
            Cmb_Partida_General.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        }

    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cargar_Combo_Partidas_Especificas
    ///DESCRIPCIÓN: Paginar grid de ordenes de variacion
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 01/Dic/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************* 
    private void Cargar_Combo_Partidas_Especificas()
    {
        if (Cmb_Partida_General.SelectedIndex > 0)
        {
            Cls_Ope_Tal_Consultas_Generales_Negocio Consultas_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            Consultas_Negocio.P_Estatus = "ACTIVO";
            Consultas_Negocio.P_Partida_Generica_ID = Cmb_Partida_General.SelectedItem.Value.Trim();
            DataTable Dt_Partidas = Consultas_Negocio.Consultar_Partidas_Especificas();
            Cmb_Partida_Especifica.DataSource = Dt_Partidas;
            Cmb_Partida_Especifica.DataValueField = "PARTIDA_ID";
            Cmb_Partida_Especifica.DataTextField = "CLAVE_NOMBRE";
            Cmb_Partida_Especifica.DataBind();
            Cmb_Partida_Especifica.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        }
        else
        {
            Cmb_Partida_Especifica.Items.Clear();
            Cmb_Partida_Especifica.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        }

    }

    #endregion

    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// 
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// 
    /// PARÁMETROS: No Áplica.
    /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
    /// FECHA CREÓ: 23/Mayo/2011 10:43 a.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.            
            Botones.Add(Btn_Modificar);


            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 29/Noviembre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Combos_SGC
    ///DESCRIPCIÓN: Carga los Datos de los Combos de acuerdo a una Partida
    ///PARAMETROS: Partida_ID. El identificador de la Partida
    ///CREO: Fco. Gallardo
    ///FECHA_CREO: Junio/2012
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    private void Cargar_Datos_Combos_SGC(String Partida_ID)
    {
        if (!String.IsNullOrEmpty(Partida_ID))
        {
            Cls_Ope_Tal_Consultas_Generales_Negocio C_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            C_Negocio.P_Partida_Especifica_ID = Partida_ID;
            DataTable Dt_SGC = C_Negocio.Consultar_Tabla_Relacionada_SGC();
            if (Dt_SGC != null)
            {
                if (Dt_SGC.Rows.Count > 0)
                {
                    String Capitulo_ID = Dt_SGC.Rows[0]["CAPITULO_ID"].ToString();
                    String Concepto_ID = Dt_SGC.Rows[0]["CONCEPTO_ID"].ToString();
                    String Partida_Generica_ID = Dt_SGC.Rows[0]["PARTIDA_GENERICA_ID"].ToString();
                    Cmb_Capitulo.SelectedIndex = Cmb_Capitulo.Items.IndexOf(Cmb_Capitulo.Items.FindByValue(Capitulo_ID));
                    Cmb_Capitulo_SelectedIndexChanged(Cmb_Capitulo, null);
                    Cmb_Conceptos.SelectedIndex = Cmb_Conceptos.Items.IndexOf(Cmb_Conceptos.Items.FindByValue(Concepto_ID));
                    Cmb_Conceptos_SelectedIndexChanged(Cmb_Conceptos, null);
                    Cmb_Partida_General.SelectedIndex = Cmb_Partida_General.Items.IndexOf(Cmb_Partida_General.Items.FindByValue(Partida_Generica_ID));
                    Cmb_Partida_General_SelectedIndexChanged(Cmb_Partida_General, null);
                    Cmb_Partida_Especifica.SelectedIndex = Cmb_Partida_Especifica.Items.IndexOf(Cmb_Partida_Especifica.Items.FindByValue(Partida_ID));
                }
            }
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Obtener_Reorden
    ///DESCRIPCIÓN: Obtiene el Reorden a partir del Max y minimo.
    ///PARAMETROS:  
    ///CREO: Fco. Gallardo
    ///FECHA_CREO: Julio/2012
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    private Int32 Obtener_Reorden()
    {

        Int32 Max = 0;
        Int32 Min = 0;
        Int32 Reo = 0;
        try
        {
            if (Txt_Maximo.Text.Trim().Length > 0) Max = Convert.ToInt32(Txt_Maximo.Text.Trim());
            if (Txt_Minimo.Text.Trim().Length > 0) Min = Convert.ToInt32(Txt_Minimo.Text.Trim());
            if (Min < Max)
            {
                Reo = Convert.ToInt32(Math.Round(Convert.ToDecimal(((Max + Min) / 2)), 0));
            }
            else
            {
                Txt_Minimo.Text = "0";
                //Txt_Minimo_TextChanged(Txt_Minimo, null);
                Lbl_Ecabezado_Mensaje.Text = "No puede ser mayor el Minimo al Maximo.";
                Lbl_Mensaje_Error.Text = "";
                Img_Error.Visible = true;
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
        return Reo;
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Obtener_Disponible
    ///DESCRIPCIÓN: Obtiene el Disponible a partir de la Existencia y el Comprometido.
    ///PARAMETROS:  
    ///CREO: Fco. Gallardo
    ///FECHA_CREO: Julio/2012
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    private Int32 Obtener_Disponible()
    {
        Int32 Existencia = 0;
        Int32 Comprometido = 0;
        Int32 Disponible = 0;
        if (Txt_Existencia.Text.Trim().Length > 0) Existencia = Convert.ToInt32(Txt_Existencia.Text);
        if (Txt_Comprometido.Text.Trim().Length > 0) Comprometido = Convert.ToInt32(Txt_Comprometido.Text);
        Disponible = Existencia - Comprometido;
        if (Disponible < (0))
        {
            Mensaje_Error("La Existencia no puede ser menor al Comprometido");
            Txt_Existencia.Text = Comprometido.ToString();
            Disponible = 0;
        }
        Txt_Comprometido.Text = Comprometido.ToString();
        return Disponible;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Obtener_Dato_Consulta
    ///DESCRIPCIÓN          : Consulta el Campo dado de la Tabla Indicada
    ///PARAMETROS:     
    ///CREO                 : Antonio Salvador Benvides Guardado
    ///FECHA_CREO           : 24/Agosto/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private String Obtener_Dato_Consulta(String Campo, String Tabla, String Condiciones)
    {
        String Mi_SQL;
        String Dato_Consulta = "";

        try
        {
            Mi_SQL = "SELECT " + Campo;
            if (Tabla != "")
            {
                Mi_SQL += " FROM " + Tabla;
            }
            if (Condiciones != "")
            {
                Mi_SQL += " WHERE " + Condiciones;
            }

            SqlDataReader Dr_Dato = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

            if (Dr_Dato.Read())
            {
                if (Dr_Dato[0] != null)
                {
                    Dato_Consulta = Dr_Dato[0].ToString();
                }
                else
                {
                    Dato_Consulta = "";
                }
                Dr_Dato.Close();
            }
            else
            {
                Dato_Consulta = "";
            }
            if (Dr_Dato != null)
            {
                Dr_Dato.Close();
            }
            Dr_Dato = null;
        }
        catch
        {
        }
        finally
        {
        }

        return Dato_Consulta;
    }

}


