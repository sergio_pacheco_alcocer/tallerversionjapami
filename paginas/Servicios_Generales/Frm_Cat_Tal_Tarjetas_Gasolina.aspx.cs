﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using System.Collections.Generic;
using JAPAMI.Taller_Mecanico.Catalogo_Tarjetas_Gasolina.Negocio;
using JAPAMI.Empleados.Negocios;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using System.Text.RegularExpressions;
using JAPAMI.Dependencias.Negocios;
using CarlosAg.ExcelXmlWriter;
using System.Text;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;

public partial class paginas_Taller_Mecanico_Frm_Cat_Tal_Tarjetas_Gasolina : System.Web.UI.Page
{
    #region Page_Load

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN: Metodo que se carga cada que ocurre un PostBack de la Página
        ///PROPIEDADES:     
        ///CREO: Salvador Vázquez Camacho.
        ///FECHA_CREO: 14/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                Llenar_Combo_Unidades_Responsables();
                Configuracion_Formulario(true);
                Llenar_Grid_Listado(0);
            }
            Div_Contenedor_Msj_Error.Visible = false;
        }

    #endregion

    #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
        ///DESCRIPCIÓN: Carga una configuracion de los controles del Formulario
        ///PROPIEDADES: 1. Estatus. Estatus en el que se cargara la configuración de los
        ///              controles.
        ///CREO: Salvador Vázquez Camacho.
        ///FECHA_CREO: 14/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private void Configuracion_Formulario(Boolean Estatus)
        {
            Btn_Nuevo_Tarjeta_Gasolina.Visible = true;
            Btn_Nuevo_Tarjeta_Gasolina.AlternateText = "Nuevo";
            Btn_Nuevo_Tarjeta_Gasolina.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
            Btn_Modificar_Tarjeta_Gasolina.Visible = true;
            Btn_Modificar_Tarjeta_Gasolina.AlternateText = "Modificar";
            Btn_Modificar_Tarjeta_Gasolina.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
            Btn_Eliminar_Tarjeta_Gasolina.Visible = Estatus;
            Btn_Exportar_Excel.Visible = Estatus;
            Btn_Reporte_Listado.Visible = Estatus;
            Cmb_Estatus.Enabled = !Estatus;
            Cmb_Programa_Proyecto.Enabled = !Estatus;
            Grid_Listado.Enabled = Estatus;
            Grid_Listado.SelectedIndex = (-1);
            Txt_Nuemero_Tarjeta.Enabled = !Estatus;
            //Btn_Busqueda_Avanzada_Asignada_A.Visible = !Estatus;
            Pnl_Resguardantes.Visible = !Estatus;
            Btn_Busqueda_Directa_Vehiculo.Visible = !Estatus;
            Txt_Saldo_inicial.Enabled = false;
            Txt_Datos_Vehiculo.Enabled = false;
            Txt_No_Inventario.Enabled = !Estatus;
            Txt_No_Economico.Enabled = !Estatus;
            Txt_Partida.Enabled = !Estatus;
            Cmb_Tipo_Bien.Enabled = !Estatus;
            //Configuracion_Acceso("Frm_Cat_Pat_Com_Zonas.aspx");
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Limpiar_Catalogo
        ///DESCRIPCIÓN: Limpia los controles del Formulario
        ///PROPIEDADES:     
        ///CREO: Salvador Vázquez Camacho.
        ///FECHA_CREO: 14/Junio/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private void Limpiar_Catalogo()
        {
            Hdf_Empleado_ID.Value = "";
            Hdf_Tarjeta_ID.Value = "";
            Hdf_Vehiculo_ID.Value = "";
            Txt_Anio.Text = "";
            Txt_Asignada_A.Text = "";
            Txt_No_Economico.Text = "";
            Txt_No_Inventario.Text = "";
            Txt_Nuemero_Tarjeta.Text = "";
            Txt_Placas.Text = "";
            Txt_Saldo_inicial.Text = "";
            Txt_Unidad_Responsable.Text = "";
            Txt_Datos_Vehiculo.Text = "";
            Txt_Partida.Text = "";
            Txt_Nombre_Partida.Text = "";
            Cmb_Estatus.SelectedIndex = 0;
            Cmb_Tipo_Bien.SelectedIndex = 0;
            Cmb_Programa_Proyecto.SelectedIndex = -1;
            Grid_Resguardante.DataBind();
            Hdf_UR_ID.Value = "";
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Listado
        ///DESCRIPCIÓN: Llena el Listado con una consulta que puede o no
        ///             tener Filtros.
        ///PROPIEDADES:     
        ///             1. Pagina.  Pagina en la cual se mostrará el Grid_VIew
        ///CREO: Salvador Vázquez Camacho.
        ///FECHA_CREO: 14/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private void Llenar_Grid_Listado(int Pagina)
        {
            try
            {
                Grid_Listado.SelectedIndex = (-1);
                Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Negocio = new Cls_Cat_Tal_Tarjetas_Gasolina_Negocio();
                Negocio.P_Numero_Tarjeta = Txt_Busqueda.Text.Trim();
                Grid_Listado.Columns[1].Visible = true;
                Grid_Listado.DataSource = Negocio.Consultar_Tarjetas_Gasolina();
                Grid_Listado.PageIndex = Pagina;
                Grid_Listado.DataBind();
                Grid_Listado.Columns[1].Visible = false;
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Busqueda_Empleados_Resguardo
        ///DESCRIPCIÓN: Llena el Grid con los empleados que cumplan el filtro
        ///PROPIEDADES:     
        ///CREO:                 
        ///CREO: Salvador Vázquez Camacho.
        ///FECHA_CREO: 14/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private void Llenar_Grid_Busqueda_Empleados_Resguardo()
        {
            Grid_Busqueda_Empleados_Resguardo.SelectedIndex = (-1);
            Grid_Busqueda_Empleados_Resguardo.Columns[1].Visible = true;
            Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            if (Txt_Busqueda_No_Empleado.Text.Trim().Length > 0) { Negocio.P_No_Empleado = Txt_Busqueda_No_Empleado.Text.Trim(); }
            if (Txt_Busqueda_RFC.Text.Trim().Length > 0) { Negocio.P_RFC_Empleado = Txt_Busqueda_RFC.Text.Trim(); }
            if (Txt_Busqueda_Nombre_Empleado.Text.Trim().Length > 0) { Negocio.P_Nombre_Empleado = Txt_Busqueda_Nombre_Empleado.Text.Trim().ToUpper(); }
            if (Cmb_Busqueda_Dependencia.SelectedIndex > 0) { Negocio.P_Dependencia_ID = Cmb_Busqueda_Dependencia.SelectedItem.Value; }
            Grid_Busqueda_Empleados_Resguardo.DataSource = Negocio.Consultar_Empleados();
            Grid_Busqueda_Empleados_Resguardo.DataBind();
            Grid_Busqueda_Empleados_Resguardo.Columns[1].Visible = false;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Mostrar_Informacion_Empleado
        ///DESCRIPCIÓN: Muestra los Generales del Empleado
        ///PROPIEDADES:     
        ///CREO: Salvador Vázquez Camacho.
        ///FECHA_CREO: 14/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Mostrar_Informacion_Empleado(String Empleado_ID)
        {
            Hdf_Empleado_ID.Value = "";
            Txt_Asignada_A.Text = "";
            Cls_Cat_Empleados_Negocios Empleado_Negocio = new Cls_Cat_Empleados_Negocios();
            Empleado_Negocio.P_Empleado_ID = Empleado_ID;
            DataTable Dt_Datos_Empleado = Empleado_Negocio.Consulta_Empleados_General();
            Hdf_Empleado_ID.Value = ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString().Trim() : null);
            Txt_Asignada_A.Text += ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString().Trim() : null);
            Txt_Asignada_A.Text += " - " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString().Trim() : null);
            Txt_Asignada_A.Text = Txt_Asignada_A.Text.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString().Trim() : null);
            Txt_Asignada_A.Text = Txt_Asignada_A.Text.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString().Trim() : null);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Unidades_Responsables
        ///DESCRIPCIÓN: Se llena el Combo de las Unidades Responsables.
        ///PROPIEDADES:     
        ///CREO: Salvador Vázquez Camacho.
        ///FECHA_CREO: 14/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Llenar_Combo_Unidades_Responsables()
        {
            Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            Negocio.P_Estatus = "ACTIVO";
            DataTable Dt_Dependencias = Negocio.Consultar_Unidades_Responsables();
            Cmb_Busqueda_Dependencia.DataSource = Dt_Dependencias;
            Cmb_Busqueda_Dependencia.DataTextField = "CLAVE_NOMBRE";
            Cmb_Busqueda_Dependencia.DataValueField = "DEPENDENCIA_ID";
            Cmb_Busqueda_Dependencia.DataBind();
            Cmb_Busqueda_Dependencia.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Vehiculo
        ///DESCRIPCIÓN: Se cargan los Datos del Vehiculo Seleccionado.
        ///PROPIEDADES:     
        ///CREO: Salvador Vázquez Camacho.
        ///FECHA_CREO: 14/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Cargar_Datos_Vehiculo(String Vehiculo, String Tipo_Busqueda, String Tipo_Bien)
        {
            Limpiar_Formulario_Vehiculo();
            DataTable Dt_Vehiculo = new DataTable();
            Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            switch (Tipo_Busqueda)
            {
                case "NO_INVENTARIO":
                    Consulta_Negocio.P_No_Inventario = Vehiculo;
                    break;
                case "IDENTIFICADOR":
                    Consulta_Negocio.P_Vehiculo_ID = Vehiculo;
                    Consulta_Negocio.P_Bien_Mueble_ID = Vehiculo;
                    break;
                case "NO_ECONOMICO":
                    Consulta_Negocio.P_No_Economico = Vehiculo;
                    break;
                default: break;
            }
            if(Tipo_Bien=="VEHICULO")
                Dt_Vehiculo = Consulta_Negocio.Consultar_Vehiculos();
            else if (Tipo_Bien == "BIEN_MUEBLE")
                Dt_Vehiculo = Consulta_Negocio.Consultar_Bien_Mueble();
            if (Dt_Vehiculo.Rows.Count > 0)
            {
                if (Tipo_Bien == "VEHICULO")
                {
                    Hdf_Vehiculo_ID.Value = Dt_Vehiculo.Rows[0]["VEHICULO_ID"].ToString().Trim();
                    Txt_No_Inventario.Text = Dt_Vehiculo.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                    Txt_No_Economico.Text = Dt_Vehiculo.Rows[0]["NO_ECONOMICO"].ToString().Trim();
                    Txt_Datos_Vehiculo.Text = Dt_Vehiculo.Rows[0]["VEHICULO_DESCRIPCION"].ToString().Trim();
                    Txt_Placas.Text = Dt_Vehiculo.Rows[0]["PLACAS"].ToString().Trim();
                    Txt_Anio.Text = Dt_Vehiculo.Rows[0]["ANIO"].ToString().Trim();
                }
                else if (Tipo_Bien == "BIEN_MUEBLE")
                {
                    Hdf_Vehiculo_ID.Value = Dt_Vehiculo.Rows[0]["BIEN_MUEBLE_ID"].ToString().Trim();
                    Txt_No_Inventario.Text = Dt_Vehiculo.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                    Txt_No_Economico.Text = "";
                    Txt_Datos_Vehiculo.Text = Dt_Vehiculo.Rows[0]["DESCRIPCION_BIEN_MUEBLE"].ToString().Trim();
                    Txt_Placas.Text = Dt_Vehiculo.Rows[0]["NUMERO_SERIE"].ToString().Trim();
                    Txt_Anio.Text = "";
                }
                if (!String.IsNullOrEmpty(Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString()))
                {
                    Cls_Cat_Dependencias_Negocio Dep_Negocio = new Cls_Cat_Dependencias_Negocio();
                    Dep_Negocio.P_Dependencia_ID = Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString().Trim();
                    Hdf_UR_ID.Value = Dep_Negocio.P_Dependencia_ID;
                    DataTable Dt_Dep = Dep_Negocio.Consulta_Dependencias();
                    if (Dt_Dep.Rows.Count > 0)
                    {
                        Txt_Unidad_Responsable.Text = Dt_Dep.Rows[0]["CLAVE_NOMBRE"].ToString();
                        Llenar_Combo_Proyectos_Dependecia();
                        if (Cmb_Programa_Proyecto.Items.Count > 2)
                        {
                            Cmb_Programa_Proyecto.Enabled = true;
                            Cmb_Programa_Proyecto.SelectedIndex = 0;
                        }
                        else
                        {
                            Cmb_Programa_Proyecto.Enabled = false;
                            Cmb_Programa_Proyecto.SelectedIndex = 1;                            
                        }
                    }

                }                
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = "El Vehículo no se encontro.";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Proyectos_Dependecia
        ///DESCRIPCIÓN: Se llena el Combo de los de proyectos por dependencia
        ///PROPIEDADES:     
        ///CREO: Jesus Toledo Rdz
        ///FECHA_CREO: 22/Febrero/2013
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Llenar_Combo_Proyectos_Dependecia()
        {
            Cmb_Programa_Proyecto.DataSource = null;            
            Cmb_Programa_Proyecto.DataBind();
            Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            Negocio.P_Dependencia_ID = Hdf_UR_ID.Value;
            DataTable Dt_Proyectos_Programas = Negocio.Consultar_Proyectos_Programas();
            Cmb_Programa_Proyecto.DataSource = Dt_Proyectos_Programas;
            Cmb_Programa_Proyecto.DataTextField = "CLAVE_NOMBRE";
            Cmb_Programa_Proyecto.DataValueField = "PROGRAMA_ID";
            Cmb_Programa_Proyecto.DataBind();
            Cmb_Programa_Proyecto.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
        }
        private void Cargar_Datos_Resguardante_Vehiculo(string Vehiculo, string Tipo_Busqueda, string Tipo_Bien)
        {
            Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            DataTable Dt_Vehiculo = new DataTable();
            switch (Tipo_Busqueda)
            {
                case "NO_INVENTARIO":
                    Consulta_Negocio.P_No_Inventario = Vehiculo;
                    break;
                case "IDENTIFICADOR":
                    Consulta_Negocio.P_Vehiculo_ID = Vehiculo;
                    break;
                default: break;
            }
            Consulta_Negocio.P_Tipo = Tipo_Bien;
            Consulta_Negocio.P_Estatus = "VIGENTE";
            if (Tipo_Bien=="BIEN_MUEBLE")
                Dt_Vehiculo = Consulta_Negocio.Consultar_Resguardantes_Bienes();
            if (Tipo_Bien == "VEHICULO")
                Dt_Vehiculo = Consulta_Negocio.Consultar_Resguardantes_Vehiculos();
            if (Dt_Vehiculo.Rows.Count > 0)
            {
                Grid_Resguardante.DataSource = Dt_Vehiculo;
                Grid_Resguardante.DataBind();
            }
            else
            {
                Grid_Resguardante.DataSource = null;
                Grid_Resguardante.DataBind();
                Lbl_Ecabezado_Mensaje.Text = "No existen resguardantes.";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario_Vehiculo
        ///DESCRIPCIÓN: Limpia los campos del Formulario [Seccion de Vehiculos].
        ///PROPIEDADES:     
        ///CREO: Salvador Vázquez Camacho.
        ///FECHA_CREO: 14/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Limpiar_Formulario_Vehiculo()
        {
            Hdf_Vehiculo_ID.Value = "";
            Txt_No_Inventario.Text = "";
            Txt_No_Economico.Text = "";
            Txt_Placas.Text = "";
            Txt_Anio.Text = "";
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Mostrar_Datos
        ///DESCRIPCIÓN         : Mostrar Datos.
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 18/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Mostrar_Datos(String Tarjeta_Seleccionada_ID)
        {
            Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Tarjeta_Negocio = new Cls_Cat_Tal_Tarjetas_Gasolina_Negocio();
            Tarjeta_Negocio.P_Tarjeta_ID = Tarjeta_Seleccionada_ID;
            Tarjeta_Negocio = Tarjeta_Negocio.Consultar_Detalles_Tarjetas_Gasolina();
            Hdf_Tarjeta_ID.Value = Tarjeta_Negocio.P_Tarjeta_ID;
            Txt_Nuemero_Tarjeta.Text = Tarjeta_Negocio.P_Numero_Tarjeta;
            Txt_Saldo_inicial.Text = Tarjeta_Negocio.P_Saldo_Inicial.ToString();
            Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(Tarjeta_Negocio.P_Estatus));
            Cls_Ope_Tal_Consultas_Generales_Negocio Generales = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            Generales.P_Partida_Especifica_ID = Tarjeta_Negocio.P_Partida_ID.ToString().Trim();
            if (!String.IsNullOrEmpty(Generales.P_Partida_Especifica_ID))
            {
                DataTable Dt_Partida = Generales.Consultar_Partidas_Especificas();
                if (Dt_Partida.Rows.Count > 0)
                {
                    Txt_Partida.Text = Dt_Partida.Rows[0]["CLAVE"].ToString();
                    Txt_Nombre_Partida.Text = Dt_Partida.Rows[0]["CLAVE_NOMBRE"].ToString();
                }
            }

            if (!String.IsNullOrEmpty(Tarjeta_Negocio.P_Vehiculo_ID))
            {
                Txt_No_Inventario.Text = Tarjeta_Negocio.P_Vehiculo_ID;
                Cmb_Tipo_Bien.SelectedValue = "VEHICULO";
            }
            else if (!String.IsNullOrEmpty(Tarjeta_Negocio.P_Bien_ID))
            {
                Txt_No_Inventario.Text = Tarjeta_Negocio.P_Bien_ID;
                Cmb_Tipo_Bien.SelectedValue = "BIEN_MUEBLE";
            }
            Cargar_Datos_Vehiculo(Txt_No_Inventario.Text, "IDENTIFICADOR", Cmb_Tipo_Bien.SelectedValue);
        }

        #region Validaciones

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Componentes
            ///DESCRIPCIÓN: Hace una validacion de que haya datos en los componentes antes de hacer
            ///             una operación.
            ///PROPIEDADES:     
            ///CREO: Salvador Vázquez Camacho.
            ///FECHA_CREO: 14/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private Boolean Validar_Componentes()
            {
                Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                String Mensaje_Error = "";
                Boolean Validacion = true;
               
                if (Cmb_Estatus.SelectedIndex == 0)
                {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar una opcion del Combo de Estatus.";
                    Validacion = false;
                }
                if (Cmb_Programa_Proyecto.SelectedIndex == 0)
                {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar el Programa.";
                    Validacion = false;
                }
                if (Txt_Nuemero_Tarjeta.Text.Trim().Length == 0)
                {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Introducir el Numero de Tarjeta.";
                    Validacion = false;
                }
                if (Txt_Saldo_inicial.Text.Trim().Length == 0)
                {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Introducir el Saldo Inicial [$].";
                    Validacion = false;
                }
                else {
                    if (!Validar_Valores_Decimales(Txt_Saldo_inicial.Text.Trim())) {
                        Mensaje_Error = Mensaje_Error + " <br />";
                        Mensaje_Error = Mensaje_Error + "+ El Formato del Saldo Inicial no es Correcto [Correcto: '12345', '12353.0' ó '12254.33'].";
                        Validacion = false;
                    }
                }
                if (!Validacion)
                {
                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
                    Div_Contenedor_Msj_Error.Visible = true;
                }
                return Validacion;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Valores_Decimales
            ///DESCRIPCIÓN: Valida los valores decimales para un Anexo.
            ///PROPIEDADES:     
            ///CREO: Salvador Vázquez Camacho.
            ///FECHA_CREO: 14/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private Boolean Validar_Valores_Decimales(String Valor)
            {
                Boolean Validacion = true;
                Regex Expresion_Regular = new Regex(@"^[0-9]{1,50}(\.[0-9]{0,2})?$");
                Validacion = Expresion_Regular.IsMatch(Valor);
                return Validacion;
            }

        #endregion

    #endregion

    #region Grids

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_PageIndexChanging
        ///DESCRIPCIÓN: Maneja la paginación del GridView
        ///PROPIEDADES:     
        ///CREO: Salvador Vázquez Camacho.
        ///FECHA_CREO: 14/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Grid_Listado.SelectedIndex = (-1);
                Llenar_Grid_Listado(e.NewPageIndex);
                Limpiar_Catalogo();
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_SelectedIndexChanged
        ///DESCRIPCIÓN: Obtiene los datos de un Listado Seleccionada para mostrarlos a detalle
        ///PROPIEDADES:     
        ///CREO: Salvador Vázquez Camacho.
        ///FECHA_CREO: 14/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Grid_Listado.SelectedIndex > (-1))
                {
                    Limpiar_Catalogo();
                    String Tarjeta_ID = HttpUtility.HtmlDecode(Grid_Listado.SelectedRow.Cells[1].Text.Trim());
                    Mostrar_Datos(Tarjeta_ID);
                    Pnl_Resguardantes.Visible = true;
                    Cargar_Datos_Resguardante_Vehiculo(Txt_No_Inventario.Text.Trim(), "NO_INVENTARIO", Cmb_Tipo_Bien.SelectedValue);
                    System.Threading.Thread.Sleep(500);
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Empleados_Resguardo_PageIndexChanging
        ///DESCRIPCIÓN: Maneja el evento de cambio de Página del GridView de Busqueda
        ///             de empleados.
        ///PROPIEDADES:     
        ///CREO: Salvador Vázquez Camacho.
        ///FECHA_CREO: 14/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Busqueda_Empleados_Resguardo_PageIndexChanging(object sender, GridViewPageEventArgs e)//--- se utiliza
        {
            try
            {
                Grid_Busqueda_Empleados_Resguardo.PageIndex = e.NewPageIndex;
                Llenar_Grid_Busqueda_Empleados_Resguardo();
                MPE_Listado_Empleados.Show();
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged
        ///DESCRIPCIÓN: Maneja el evento de cambio de Selección del GridView de Busqueda
        ///             de empleados.
        ///PROPIEDADES:     
        ///CREO: Salvador Vázquez Camacho.
        ///FECHA_CREO: 14/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Grid_Busqueda_Empleados_Resguardo.SelectedIndex > (-1))
                {
                    String Empleado_Seleccionado_ID = Grid_Busqueda_Empleados_Resguardo.SelectedRow.Cells[1].Text.Trim();
                    Mostrar_Informacion_Empleado(Empleado_Seleccionado_ID);
                    Grid_Busqueda_Empleados_Resguardo.SelectedIndex = (-1);
                    MPE_Listado_Empleados.Hide();
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*************************************************************************************************************************
        ///Nombre: Pasar_DataTable_A_Excel
        ///Descripción: Pasa DataTable a Excel. 
        ///Parámetros: Dt_Reporte.- DataTable que se pasara a excel. 
        ///CREO: Salvador Vázquez Camacho.
        ///FECHA_CREO: 14/Junio/2012
        ///Usuario Modifico:
        ///Fecha Modifico:
        ///Causa Modificación:
        ///*************************************************************************************************************************
        public void Pasar_DataTable_A_Excel(System.Data.DataTable Dt_Reporte)
        {
            String Ruta = "Listado de Tarjetas de Gasolina " + (String.Format("{0:dd_MMM_yyyy_hh_mm_ss}", DateTime.Now)) + "].xls";//Variable que almacenara el nombre del archivo. 
            try
            {
                //Creamos el libro de Excel.
                CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();

                Libro.Properties.Title = "Reporte de Tarjeta de Gasolina";
                Libro.Properties.Created = DateTime.Now;
                Libro.Properties.Author = "Patrimonio";

                //Creamos una hoja que tendrá el libro.
                CarlosAg.ExcelXmlWriter.Worksheet Hoja = Libro.Worksheets.Add("Registros");
                //Agregamos un renglón a la hoja de excel.
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon = Hoja.Table.Rows.Add();
                //Creamos el estilo titulo para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Titulo = Libro.Styles.Add("TitleStyle");
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");

                Estilo_Cabecera.Font.FontName = "Tahoma";
                Estilo_Cabecera.Font.Size = 10;
                Estilo_Cabecera.Font.Bold = true;
                Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Cabecera.Font.Color = "#FFFFFF";
                Estilo_Cabecera.Interior.Color = "#000000";
                Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Alignment.WrapText = true;

                Estilo_Contenido.Font.FontName = "Tahoma";
                Estilo_Contenido.Font.Size = 8;
                Estilo_Contenido.Font.Bold = true;
                Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Contenido.Font.Color = "#000000";
                Estilo_Contenido.Interior.Color = "White";
                Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Alignment.WrapText = true;

                //Agregamos las columnas que tendrá la hoja de excel.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Tarjeta
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(250));//U. Responsable
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(110));//No. Inventario
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(110));//No. Económico

                String Leyenda_Japami = "Junta de Agua Potable, Drenaje, Alcantarillado y Saneamiento del Municipio de Irapuato, Gto.";
                String Leyenda_Gerencia_Depto = "Gerencia Administrativa / Departamento de Mantenimiento y Servicios Generales";
                String Leyenda_Titulo_Reporte = "Relación de Tarjetas de Combustible";

                WorksheetCell Celda_Japami = new CarlosAg.ExcelXmlWriter.WorksheetCell(Leyenda_Japami, DataType.String, "BodyStyle");
                Celda_Japami.MergeAcross = 3;
                Renglon.Cells.Add(Celda_Japami);
                Renglon = Hoja.Table.Rows.Add();

                WorksheetCell Celda_Gerencia_Depto = new CarlosAg.ExcelXmlWriter.WorksheetCell(Leyenda_Gerencia_Depto, DataType.String, "BodyStyle");
                Celda_Gerencia_Depto.MergeAcross = 3;
                Renglon.Cells.Add(Celda_Gerencia_Depto);
                Renglon = Hoja.Table.Rows.Add();

                WorksheetCell Celda_Titulo_Reporte = new CarlosAg.ExcelXmlWriter.WorksheetCell(Leyenda_Titulo_Reporte, DataType.String, "BodyStyle");
                Celda_Titulo_Reporte.MergeAcross = 3;
                Renglon.Cells.Add(Celda_Titulo_Reporte);
                Renglon = Hoja.Table.Rows.Add();

                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Tarjeta", "HeaderStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("U. Responsable", "HeaderStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("No. Inventario", "HeaderStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("No. Económico", "HeaderStyle"));
                Renglon.Height = 20;
                Renglon = Hoja.Table.Rows.Add();

                if (Dt_Reporte is System.Data.DataTable)
                {
                    if (Dt_Reporte.Rows.Count > 0)
                    {
                        foreach (System.Data.DataRow FILA in Dt_Reporte.Rows)
                        {
                            if (FILA is System.Data.DataRow)
                            {

                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(FILA["Number_TARJETA"].ToString(), DataType.String, "BodyStyle"));
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(FILA["DEPENDENCIA"].ToString(), DataType.String, "BodyStyle"));
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(FILA["NO_INVENTARIO"].ToString(), DataType.String, "BodyStyle"));
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(FILA["NO_ECONOMICO"].ToString(), DataType.String, "BodyStyle"));
                                Renglon.Height = 20;
                                Renglon.AutoFitHeight = true; 
                                Renglon = Hoja.Table.Rows.Add();
                            }
                        }
                    }
                }

                //Abre el archivo de excel
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + Ruta);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;
                Libro.Save(Response.OutputStream);
                Response.End();
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
            }
        }

    #endregion

    #region Eventos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
        ///DESCRIPCIÓN: Deja los componentes listos para dar de Alta un registro.
        ///PROPIEDADES:     
        ///CREO: Salvador Vázquez Camacho.
        ///FECHA_CREO: 14/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Nuevo_Click(object sender, EventArgs e)
        {
            try
            {
                if (Btn_Nuevo_Tarjeta_Gasolina.AlternateText.Equals("Nuevo"))
                {
                    Configuracion_Formulario(false);
                    Limpiar_Catalogo();
                    Txt_Saldo_inicial.Text = "0.00";
                    Btn_Nuevo_Tarjeta_Gasolina.AlternateText = "Dar de Alta";
                    Btn_Nuevo_Tarjeta_Gasolina.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Salir_Tarjeta_Gasolina.AlternateText = "Cancelar";
                    Btn_Salir_Tarjeta_Gasolina.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Modificar_Tarjeta_Gasolina.Visible = false;
                    Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue("ACTIVO"));
                }
                else
                {
                    if (Validar_Componentes())
                    {
                        Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Negocio = new Cls_Cat_Tal_Tarjetas_Gasolina_Negocio();
                        Negocio.P_Numero_Tarjeta = Txt_Nuemero_Tarjeta.Text;
                        Negocio.P_Emp_Asig_Tarjeta_ID = Hdf_Empleado_ID.Value.Trim();
                        Negocio.P_Vehiculo_ID = Hdf_Vehiculo_ID.Value;
                        Negocio.P_Saldo_Inicial = Double.Parse("0");
                        Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                        Negocio.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                        Negocio.P_Partida_ID = Hdf_Partida_ID.Value.Trim();
                        Negocio.P_Programa_Proyecto_ID = Cmb_Programa_Proyecto.SelectedItem.Value;
                        Negocio.P_Tipo_Bien = Cmb_Tipo_Bien.SelectedValue;
                        Negocio.Alta_Tarjetas_Gasolina();
                        Configuracion_Formulario(true);
                        Limpiar_Catalogo();
                        Llenar_Grid_Listado(Grid_Listado.PageIndex);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo", "alert('Alta Exitosa');", true);
                        Btn_Nuevo_Tarjeta_Gasolina.AlternateText = "Nuevo";
                        Btn_Nuevo_Tarjeta_Gasolina.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                        Btn_Salir_Tarjeta_Gasolina.AlternateText = "Salir";
                        Btn_Salir_Tarjeta_Gasolina.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    }
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
        ///DESCRIPCIÓN: Deja los componentes listos para hacer la modificacion.
        ///PROPIEDADES:     
        ///CREO: Salvador Vázquez Camacho.
        ///FECHA_CREO: 14/Junio/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Modificar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Btn_Modificar_Tarjeta_Gasolina.AlternateText.Equals("Modificar"))
                {
                    if (Grid_Listado.Rows.Count > 0 && Grid_Listado.SelectedIndex > (-1))
                    {
                        Configuracion_Formulario(false);
                        Btn_Modificar_Tarjeta_Gasolina.AlternateText = "Actualizar";
                        Btn_Modificar_Tarjeta_Gasolina.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                        Btn_Salir_Tarjeta_Gasolina.AlternateText = "Cancelar";
                        Btn_Salir_Tarjeta_Gasolina.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        Btn_Nuevo_Tarjeta_Gasolina.Visible = false;
                    }
                    else
                    {
                        Lbl_Ecabezado_Mensaje.Text = "Debe seleccionar el Registro que se desea Modificar.";
                        Lbl_Mensaje_Error.Text = "";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                }
                else
                {
                    if (Validar_Componentes())
                    {
                        Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Negocio = new Cls_Cat_Tal_Tarjetas_Gasolina_Negocio();
                        Negocio.P_Tarjeta_ID = Hdf_Tarjeta_ID.Value;
                        Negocio.P_Numero_Tarjeta = Txt_Nuemero_Tarjeta.Text.Trim();
                        Negocio.P_Vehiculo_ID = Hdf_Vehiculo_ID.Value;
                        Negocio.P_Saldo_Inicial = Double.Parse("0");
                        Negocio.P_Emp_Asig_Tarjeta_ID = Hdf_Empleado_ID.Value.Trim();
                        Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                        Negocio.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado;
                        Negocio.P_Programa_Proyecto_ID = Cmb_Programa_Proyecto.SelectedItem.Value;
                        Negocio.P_Tipo_Bien = Cmb_Tipo_Bien.SelectedValue;
                        if (!String.IsNullOrEmpty(Txt_Partida.Text.Trim()))
                        {
                            Cls_Ope_Tal_Consultas_Generales_Negocio Partidas_Especificas = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                            Partidas_Especificas.P_Clave = Txt_Partida.Text.Trim();
                            DataTable Dt_Partidas_Especificas = Partidas_Especificas.Consultar_Partidas_Especificas();

                            if (Dt_Partidas_Especificas.Rows.Count > 0)
                            {
                                Negocio.P_Partida_ID = Dt_Partidas_Especificas.Rows[0]["PARTIDA_ID"].ToString().Trim();
                            }
                        }

                        Negocio.Modificar_Tarjetas_Gasolina();
                        Configuracion_Formulario(true);
                        Limpiar_Catalogo();
                        Llenar_Grid_Listado(Grid_Listado.PageIndex);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo", "alert('Actualización Exitosa');", true);
                        Btn_Modificar_Tarjeta_Gasolina.AlternateText = "Modificar";
                        Btn_Modificar_Tarjeta_Gasolina.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                        Btn_Salir_Tarjeta_Gasolina.AlternateText = "Salir";
                        Btn_Salir_Tarjeta_Gasolina.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    }
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Exportar_Excel_Click
        ///DESCRIPCIÓN: Muestra los resultado en Excel.
        ///PROPIEDADES:     
        ///CREO: Salvador Vázquez Camacho.
        ///FECHA_CREO: 14/Junio/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Exportar_Excel_Click(object sender, ImageClickEventArgs e) {
            try
            {
                Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Tarjeta_Negocio = new Cls_Cat_Tal_Tarjetas_Gasolina_Negocio();
                Tarjeta_Negocio.P_Numero_Tarjeta = Txt_Busqueda.Text.Trim();
                DataTable Dt_Datos_Tarjeta = Tarjeta_Negocio.Consultar_Tarjetas_Gasolina();
                Pasar_DataTable_A_Excel(Dt_Datos_Tarjeta);
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Click
        ///DESCRIPCIÓN: Llena la Tabla con la opcion buscada
        ///PROPIEDADES:     
        ///CREO: Salvador Vázquez Camacho.
        ///FECHA_CREO: 14/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Limpiar_Catalogo();
                Grid_Listado.SelectedIndex = (-1);
                Grid_Listado.SelectedIndex = (-1);
                Llenar_Grid_Listado(0);
                if (Grid_Listado.Rows.Count == 0 && Txt_Busqueda_No_Empleado.Text.Trim().Length > 0)
                {
                    Lbl_Ecabezado_Mensaje.Text = "Para la Busqueda con el nombre \"" + Txt_Busqueda_No_Empleado.Text + "\" no se encontrarón coincidencias";
                    Lbl_Mensaje_Error.Text = "(Se cargarón todos los registros almacenados)";
                    Div_Contenedor_Msj_Error.Visible = true;
                    Txt_Busqueda_No_Empleado.Text = "";
                    Llenar_Grid_Listado(0);
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Eliminar_Click
        ///DESCRIPCIÓN: Elimina un registro de la Base de Datos
        ///PROPIEDADES:     
        ///CREO: Salvador Vázquez Camacho.
        ///FECHA_CREO: 14/Junio/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Eliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Grid_Listado.Rows.Count > 0 && Grid_Listado.SelectedIndex > (-1))
                {
                    Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Negocio = new Cls_Cat_Tal_Tarjetas_Gasolina_Negocio();
                    Negocio.P_Tarjeta_ID = Hdf_Tarjeta_ID.Value;
                    Negocio.Eliminar_Tarjetas_Gasolina();
                    Grid_Listado.SelectedIndex = (-1);
                    Llenar_Grid_Listado(Grid_Listado.PageIndex);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo", "alert('Eliminación Exitosa');", true);
                    Limpiar_Catalogo();
                }
                else
                {
                    Lbl_Ecabezado_Mensaje.Text = "Debe seleccionar el Registro que se desea Eliminar.";
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Cancela la operación que esta en proceso (Alta o Actualizar) o Sale del Formulario.
        ///PROPIEDADES:     
        ///CREO: Salvador Vázquez Camacho.
        ///FECHA_CREO: 14/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, EventArgs e)
        {
            if (Btn_Salir_Tarjeta_Gasolina.AlternateText.Equals("Salir"))
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Configuracion_Formulario(true);
                Limpiar_Catalogo();
                Btn_Salir_Tarjeta_Gasolina.AlternateText = "Salir";
                Btn_Salir_Tarjeta_Gasolina.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Empleado_Entrega_Click
        ///DESCRIPCIÓN: Lanza la Busqueda del Empleado
        ///PROPIEDADES:     
        ///CREO: Salvador Vázquez Camacho.
        ///FECHA_CREO: 14/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Empleado_Entrega_Click(object sender, ImageClickEventArgs e)
        {
            Grid_Busqueda_Empleados_Resguardo.PageIndex = 0;
            Llenar_Grid_Busqueda_Empleados_Resguardo();
            MPE_Listado_Empleados.Show();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Empleados_Click
        ///DESCRIPCIÓN: Ejecuta la Busqueda Avanzada para el Resguardante.
        ///PARAMETROS:     
        ///CREO: Salvador Vázquez Camacho.
        ///FECHA_CREO: 14/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************    
        protected void Btn_Busqueda_Empleados_Click(object sender, EventArgs e)
        {
            try
            {
                Grid_Busqueda_Empleados_Resguardo.PageIndex = 0;
                Llenar_Grid_Busqueda_Empleados_Resguardo();
                MPE_Listado_Empleados.Show();
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Directa_Vehiculo_Click
        ///DESCRIPCIÓN: Busca el Vehiculo por el No. Inventario.
        ///PARAMETROS:     
        ///CREO: Salvador Vázquez Camacho.
        ///FECHA_CREO: 14/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Busqueda_Directa_Vehiculo_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Cmb_Tipo_Bien.SelectedIndex > 0)
                {
                    if (Cmb_Tipo_Bien.SelectedValue == "VEHICULO")
                    {
                        if (Txt_No_Economico.Text.Trim().Length > 0)
                        {
                            Cargar_Datos_Vehiculo(Txt_No_Economico.Text.Trim(), "NO_ECONOMICO", Cmb_Tipo_Bien.SelectedValue);
                            Cargar_Datos_Resguardante_Vehiculo(Txt_No_Inventario.Text.Trim(), "NO_INVENTARIO",Cmb_Tipo_Bien.SelectedValue);
                        }

                        else if (Txt_No_Inventario.Text.Trim().Length > 0)
                        {
                            Cargar_Datos_Vehiculo(Txt_No_Inventario.Text.Trim(), "NO_INVENTARIO",Cmb_Tipo_Bien.SelectedValue);
                            Cargar_Datos_Resguardante_Vehiculo(Txt_No_Inventario.Text.Trim(), "NO_INVENTARIO", Cmb_Tipo_Bien.SelectedValue);
                        }
                        else
                        {
                            Limpiar_Formulario_Vehiculo();
                            Lbl_Ecabezado_Mensaje.Text = "Es necesario introducir el No. de Inventario del Vehículo.";
                            Lbl_Mensaje_Error.Text = "";
                            Div_Contenedor_Msj_Error.Visible = true;
                        }
                    }
                    else if (Cmb_Tipo_Bien.SelectedValue == "BIEN_MUEBLE")
                    {                        

                        if (Txt_No_Inventario.Text.Trim().Length > 0)
                        {
                            Cargar_Datos_Vehiculo(Txt_No_Inventario.Text.Trim(), "NO_INVENTARIO",Cmb_Tipo_Bien.SelectedValue);
                            Cargar_Datos_Resguardante_Vehiculo(Txt_No_Inventario.Text.Trim(), "NO_INVENTARIO", Cmb_Tipo_Bien.SelectedValue);
                        }
                        else
                        {
                            Limpiar_Formulario_Vehiculo();
                            Lbl_Ecabezado_Mensaje.Text = "Es necesario introducir el No. de Inventario del Vehículo.";
                            Lbl_Mensaje_Error.Text = "";
                            Div_Contenedor_Msj_Error.Visible = true;
                        }
                    }
                }
                  else
                {
                    Limpiar_Formulario_Vehiculo();
                    Lbl_Ecabezado_Mensaje.Text = "Es necesario introducir tipo de Bien.";
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                    Cmb_Tipo_Bien.Focus();
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                Lbl_Mensaje_Error.Text = "Se presento una Excepcion al Buscar [" + Ex.Message + "]";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Avanzada_Asignada_A_Click
        ///DESCRIPCIÓN: Busqueda avanzada
        ///PROPIEDADES:     
        ///CREO: Salvador Vázquez Camacho.
        ///FECHA_CREO: 14/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Btn_Busqueda_Avanzada_Asignada_A_Click(object sender, ImageClickEventArgs e)
        {
            MPE_Listado_Empleados.Show();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Directa_Tarjeta_Click
        ///DESCRIPCIÓN         : Ejecuta la Busqueda rapida de una tarjeta de gasolina.
        ///PARAMETROS          :     
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 20/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Busqueda_Directa_Tarjeta_Click(object sender, ImageClickEventArgs e) {
            Limpiar_Catalogo();
            if (Txt_Busqueda.Text.Trim().Length == 0) { 
                Lbl_Ecabezado_Mensaje.Text = "Debe introducir un valor para buscar.";
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
                Llenar_Grid_Listado(0);
            Txt_Busqueda.Text = "";
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Reporte_Listado_Click
        ///DESCRIPCIÓN         : Lanza el Reporte del Listado.
        ///PARAMETROS          :     
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 20/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Reporte_Listado_Click(object sender, ImageClickEventArgs e) {
            try {
                Generar_Reporte_Listado();
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                Lbl_Mensaje_Error.Text = "Excepción [" + Ex.Message + "]";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Directa_Partida_Click
        ///DESCRIPCIÓN         : Ejecuta la Busqueda rapida de una Partida Especifica.
        ///PARAMETROS          :     
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 20/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Busqueda_Directa_Partida_Click(object sender, ImageClickEventArgs e)
        {
            if (!String.IsNullOrEmpty(Txt_Partida.Text.Trim()))
            {
                Cls_Ope_Tal_Consultas_Generales_Negocio Partidas_Especificas = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                Partidas_Especificas.P_Clave = Txt_Partida.Text.Trim();
                DataTable Dt_Partidas_Especificas = Partidas_Especificas.Consultar_Partidas_Especificas();

                if (Dt_Partidas_Especificas.Rows.Count > 0)
                {
                    Txt_Nombre_Partida.Text = Dt_Partidas_Especificas.Rows[0]["CLAVE_NOMBRE"].ToString();
                    Hdf_Partida_ID.Value = Dt_Partidas_Especificas.Rows[0]["PARTIDA_ID"].ToString();
                }
            }
        }

    #endregion

    #region Reporte

        /// *************************************************************************************
        /// NOMBRE:              Mostrar_Reporte
        /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
        /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
        ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
        /// USUARIO CREO:        Juan Alberto Hernández Negrete.
        /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
        /// USUARIO MODIFICO:    Salvador Hernández Ramírez
        /// FECHA MODIFICO:      23-Mayo-2011
        /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
        /// *************************************************************************************
        protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato) {
            String Pagina = "../../Reporte/";
            try {
                    Pagina = Pagina + Nombre_Reporte_Generar;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                    "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            } catch (Exception Ex) {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Generar_Reporte_Listado
        ///DESCRIPCIÓN: Se Generan las Tablas para el Reporte de Listado
        ///PARAMETROS:  
        ///CREO: Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO: 24/Mayo/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        private void Generar_Reporte_Listado() {
            //Dataset esqueleto del Reporte
            Ds_Rpt_Tal_Listado_General_Tarjetas_Gasolina Ds_Reporte = new Ds_Rpt_Tal_Listado_General_Tarjetas_Gasolina();

            DataSet Ds_Consulta = new DataSet();
            Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Tmp_Neg = new Cls_Cat_Tal_Tarjetas_Gasolina_Negocio();
            Tmp_Neg.P_Numero_Tarjeta = Txt_Busqueda.Text.Trim();
            DataTable Dt_Datos = Tmp_Neg.Consultar_Tarjetas_Gasolina();
            Dt_Datos.TableName = "DT_DATOS_GENERALES";
            Ds_Consulta.Tables.Add(Dt_Datos.Copy());
            Ds_Consulta.Tables.Add(new Cls_Tal_Parametros_Negocio().Obtener_Tabla_Reporte(this.Server));

            //Generar y lanzar el reporte
            String Ruta_Reporte_Crystal = "Rpt_Tal_Listado_General_Tarjetas_Gasolina.rpt";
            Generar_Reporte(Ds_Consulta, Ds_Reporte, Ruta_Reporte_Crystal);

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
        ///DESCRIPCIÓN: caraga el data set fisico con el cual se genera el Reporte especificado
        ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
        ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
        ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
        ///CREO: Susana Trigueros Armenta.
        ///FECHA_CREO: 01/Mayo/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        private void Generar_Reporte(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte)
        {
            ReportDocument Reporte = new ReportDocument();
            String File_Path = Server.MapPath("../Rpt/Servicios_Generales/" + Nombre_Reporte);
            Reporte.Load(File_Path);
            String Nombre_Reporte_Generar = "Rpt_Tal_List_Gral_Tarj_Gas_" + Cls_Sessiones.No_Empleado + "_" + Convert.ToString(DateTime.Now.ToString("yyyy'-'MM'-'dd'_t'HH'-'mm'-'ss")) + ".pdf";
            String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
            Ds_Reporte = Data_Set_Consulta_DB;
            Reporte.SetDataSource(Ds_Reporte);
            ExportOptions Export_Options = new ExportOptions();
            DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
            Disk_File_Destination_Options.DiskFileName = Server.MapPath(Ruta);
            Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
            Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
            Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
            Reporte.Export(Export_Options);
            Mostrar_Reporte(Nombre_Reporte_Generar, "PDF");
        }

    #endregion

}
