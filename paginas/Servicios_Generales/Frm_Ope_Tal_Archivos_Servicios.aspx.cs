﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Control_Patrimonial_Operacion_Vehiculos.Negocio;
using System.Text.RegularExpressions;
using JAPAMI.Reportes;
using JAPAMI.Taller_Mecanico.Catalogo_Partes_Vehiculos.Negocio;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using JAPAMI.Taller_Mecanico.Operacion_Unidades_Responsables_Rol.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Correctivos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Preventivos.Negocio;
using JAPAMI.Taller_Mecanico.Reporte_Solicitud_Servicio.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Asignacion_Proveedor.Negocio;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;
using AjaxControlToolkit;
using System.IO;
using System.Security.Cryptography;
using System.Collections.Generic;
using JAPAMI.Taller_Mecanico.Operacion_Archivos_Servicios.Negocio;

public partial class paginas_Taller_Mecanico_Frm_Ope_Tal_Archivos_Servicios : System.Web.UI.Page {

    private bool _Grid_Archivo = true;
    protected bool Grid_Archivo
    {
        get { return this._Grid_Archivo; }
        set { this._Grid_Archivo = value; }
    }

    #region Page_Load

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Page_Load
    ///DESCRIPCIÓN: Carga la Pagina Inicial
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 25/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    protected void Page_Load(object sender, EventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        if (!IsPostBack)
        {
            Cmb_Filtrado_Estatus.SelectedIndex = Cmb_Filtrado_Estatus.Items.IndexOf(Cmb_Filtrado_Estatus.Items.FindByValue("EN_REPARACION"));
            Llenar_Combo_Unidades_Responsables();
            Grid_Listado_Solicitudes.PageIndex = 0;
            Configuracion_Formulario("INICIAL");
            Cmb_Unidad_Responsable.Enabled = false;
            Cmb_Unidad_Responsable_Busqueda.Enabled = Validar_Acceso_Unidades_Responsables();
            Cmb_Unidad_Responsable_Busqueda.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Cls_Sessiones.Dependencia_ID_Empleado));
            Llenar_Listado_Solicitudes();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Acceso_Unidades_Responsables
    ///DESCRIPCIÓN: Se valida el Acceso a Otras UR.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 30/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    protected Boolean Validar_Acceso_Unidades_Responsables()
    {
        Boolean Accesos_Totales = false;
        Cls_Ope_Tal_Unidades_Responsables_Rol_Negocio Cls_Negocio = new Cls_Ope_Tal_Unidades_Responsables_Rol_Negocio();
        Cls_Negocio.P_Estatus = "ACTIVO";
        DataTable Dt_UR = Cls_Negocio.Consultar_Listado_UR();
        DataRow[] Filas = Dt_UR.Select("DEPENDENCIA_ID = '" + Cls_Sessiones.Dependencia_ID_Empleado.Trim() + "'");
        if (Filas.Length > 0) { Accesos_Totales = true; }
        return Accesos_Totales;
    }

    #endregion

    #region Metodos

    #region Llenado de Campos [Combos, Listados, Vehiculos]

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Unidades_Responsables
    ///DESCRIPCIÓN: Se llena el Combo de las Unidades Responsables.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 25/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Llenar_Combo_Unidades_Responsables()
    {
        Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
        Negocio.P_Estatus = "ACTIVO";
        DataTable Dt_Dependencias = Negocio.Consultar_Unidades_Responsables();
        Cmb_Unidad_Responsable.DataSource = Dt_Dependencias;
        Cmb_Unidad_Responsable.DataTextField = "CLAVE_NOMBRE";
        Cmb_Unidad_Responsable.DataValueField = "DEPENDENCIA_ID";
        Cmb_Unidad_Responsable.DataBind();
        Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("< - - TODAS - - >", ""));
        Cmb_Unidad_Responsable_Busqueda.DataSource = Dt_Dependencias;
        Cmb_Unidad_Responsable_Busqueda.DataTextField = "CLAVE_NOMBRE";
        Cmb_Unidad_Responsable_Busqueda.DataValueField = "DEPENDENCIA_ID";
        Cmb_Unidad_Responsable_Busqueda.DataBind();
        Cmb_Unidad_Responsable_Busqueda.Items.Insert(0, new ListItem("< - - TODAS - - >", ""));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Solicitudes
    ///DESCRIPCIÓN: Se llena el Listado de las Solicitudes.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 25/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Llenar_Listado_Solicitudes()
    {
        Cls_Ope_Tal_Solicitud_Servicio_Negocio Negocio = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
        DataView Dv_Resultados = new DataView();
        if (Session["Archivos_Servicios_Vista"] == null)
        {
            if (Cmb_Unidad_Responsable_Busqueda.SelectedIndex > 0) { Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable_Busqueda.SelectedItem.Value.Trim(); }
            if (Cmb_Filtrado_Estatus.SelectedIndex > 0) { Negocio.P_Estatus = Cmb_Filtrado_Estatus.SelectedItem.Value; }
            if (Txt_Numero_Inventario_Busqueda.Text.Trim().Length > 0) { Negocio.P_No_Inventario = Convert.ToInt32(Txt_Numero_Inventario_Busqueda.Text.Trim()); }
            if (Txt_Numero_Economico_Busqueda.Text.Trim().Length > 0) { Negocio.P_No_Economico = Txt_Numero_Economico_Busqueda.Text.Trim(); }
            if (Txt_Folio_Solicitud_Busqueda.Text.Trim().Length > 0) { Negocio.P_Folio_Solicitud = String.Format("{0:0000000000}", Convert.ToInt32(Txt_Folio_Solicitud_Busqueda.Text.Trim())); }
            if (Cmb_Tipo_Solicitud_Busqueda.SelectedIndex > 0) { Negocio.P_Tipo_Servicio = Cmb_Tipo_Solicitud_Busqueda.SelectedItem.Value.Trim(); }
            if (Cmb_Reparacion_Busqueda.SelectedIndex > 0) { Negocio.P_Reparacion = Cmb_Reparacion_Busqueda.SelectedItem.Value.Trim(); }
            if (Txt_Fecha_Recepcion_Inicial.Text.Trim().Length > 0) { Negocio.P_Busq_Fecha_Recepcion_Inicial = Convert.ToDateTime(Txt_Fecha_Recepcion_Inicial.Text.Trim()); }
            if (Txt_Fecha_Recepcion_Final.Text.Trim().Length > 0) { Negocio.P_Busq_Fecha_Recepcion_Final = Convert.ToDateTime(Txt_Fecha_Recepcion_Final.Text.Trim()); }
            Negocio.P_Procedencia = "SOLICITUD";
            DataTable Dt_Resultados = Negocio.Consultar_Listado_Solicitudes_Servicio();
            Grid_Listado_Solicitudes.Columns[1].Visible = true;
            Grid_Listado_Solicitudes.DataSource = Dt_Resultados;
            Grid_Listado_Solicitudes.DataBind();
            Grid_Listado_Solicitudes.Columns[1].Visible = false;
            Session["Archivos_Servicios"] = Dt_Resultados;
            Cambiar_Estatus_Grid();
        }
        else
        {
            Dv_Resultados = (DataView)Session["Archivos_Servicios_Vista"];
            Grid_Listado_Solicitudes.Columns[1].Visible = true;
            Grid_Listado_Solicitudes.DataSource = Dv_Resultados;
            Grid_Listado_Solicitudes.DataBind();
            Grid_Listado_Solicitudes.Columns[1].Visible = false;
            Cambiar_Estatus_Grid();
            
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Vehiculo
    ///DESCRIPCIÓN: Se cargan los Datos del Vehiculo Seleccionado.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 25/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Cargar_Datos_Vehiculo(String Vehiculo, String Tipo_Busqueda)
    {
        Limpiar_Formulario_Vehiculo();
        Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
        switch (Tipo_Busqueda)
        {
            case "NO_INVENTARIO":
                Consulta_Negocio.P_No_Inventario = Vehiculo;
                break;
            case "IDENTIFICADOR":
                Consulta_Negocio.P_Vehiculo_ID = Vehiculo;
                break;
            default: break;
        }
        if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Consulta_Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value; }
        DataTable Dt_Vehiculo = Consulta_Negocio.Consultar_Vehiculos();
        if (Dt_Vehiculo.Rows.Count > 0)
        {
            Hdf_Vehiculo_ID.Value = Dt_Vehiculo.Rows[0]["VEHICULO_ID"].ToString().Trim();
            Txt_No_Inventario.Text = Dt_Vehiculo.Rows[0]["NO_INVENTARIO"].ToString().Trim();
            Txt_No_Economico.Text = Dt_Vehiculo.Rows[0]["NO_ECONOMICO"].ToString().Trim();
            Txt_Datos_Vehiculo.Text = Dt_Vehiculo.Rows[0]["VEHICULO_DESCRIPCION"].ToString().Trim();
            Txt_Placas.Text = Dt_Vehiculo.Rows[0]["PLACAS"].ToString().Trim();
            Txt_Anio.Text = Dt_Vehiculo.Rows[0]["ANIO"].ToString().Trim();
            Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString()));
        }
        else
        {
            Lbl_Ecabezado_Mensaje.Text = "El Vehículo no se encontro.";
            if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Lbl_Mensaje_Error.Text = "[Puede ser que no este asignado a la Unidad Responsable Seleccionada]."; }
            else { Lbl_Mensaje_Error.Text = ""; }
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Bien_Mueble
    ///DESCRIPCIÓN: Se cargan los Datos del Bien Mueble Seleccionado.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Cargar_Datos_Bien_Mueble(String Bien_Mueble, String Tipo_Busqueda)
    {
        Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
        switch (Tipo_Busqueda)
        {
            case "NO_INVENTARIO":
                Consulta_Negocio.P_No_Inventario = Bien_Mueble;
                break;
            case "IDENTIFICADOR":
                Consulta_Negocio.P_Bien_Mueble_ID = Bien_Mueble;
                break;
            default: break;
        }
        DataTable Dt_Bienes_Muebles = Consulta_Negocio.Consultar_Bien_Mueble();
        if (Dt_Bienes_Muebles.Rows.Count > 0)
        {
            Hdf_Bien_Mueble_ID.Value = Dt_Bienes_Muebles.Rows[0]["BIEN_MUEBLE_ID"].ToString().Trim();
            Txt_No_Inventario_BM.Text = Dt_Bienes_Muebles.Rows[0]["NO_INVENTARIO"].ToString().Trim();
            Txt_Numero_Serie_Bien.Text = Dt_Bienes_Muebles.Rows[0]["NUMERO_SERIE"].ToString().Trim();
            Txt_Descripcion_Bien.Text = Dt_Bienes_Muebles.Rows[0]["DESCRIPCION_BIEN_MUEBLE"].ToString().Trim();
            if (!String.IsNullOrEmpty(Dt_Bienes_Muebles.Rows[0]["DEPENDENCIA_ID"].ToString()))
            {
                Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Bienes_Muebles.Rows[0]["DEPENDENCIA_ID"].ToString()));
            }
            else
            {
                Lbl_Mensaje_Error.Text = "ERROR [Puede ser que el Bien esté dado de Baja].";
                Cmb_Unidad_Responsable.SelectedIndex = 0;
            }
        }
        else
        {
            Lbl_Ecabezado_Mensaje.Text = "El Bien no se encontro.";
            if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Lbl_Mensaje_Error.Text = "[Puede ser que no este asignado a la Unidad Responsable Seleccionada]."; }
            else { Lbl_Mensaje_Error.Text = ""; }
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cargar_Estilo_Estatus
    ///DESCRIPCIÓN          : Carga el Estilo para el Estatus
    ///PARAMETROS           : 
    ///CREO                 : Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO           : 25/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Cargar_Estilo_Estatus()
    {
        String Estatus = Txt_Estatus.Text.Trim();
        switch (Estatus)
        {
            case "PENDIENTE":
                Txt_Estatus.BackColor = System.Drawing.Color.FromArgb(169, 188, 245);
                Txt_Estatus.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
                break;
            case "RECHAZADA":
                Txt_Estatus.BackColor = System.Drawing.Color.FromArgb(255, 46, 56);
                Txt_Estatus.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
                break;
            case "CANCELADA":
                Txt_Estatus.BackColor = System.Drawing.Color.FromArgb(184, 184, 184);
                Txt_Estatus.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
                break;
            case "AUTORIZADA":
                Txt_Estatus.BackColor = System.Drawing.Color.FromArgb(156, 255, 186);
                Txt_Estatus.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
                break;
            case "EN_REPARACION":
                Txt_Estatus.BackColor = System.Drawing.Color.FromArgb(156, 255, 186);
                Txt_Estatus.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
                break;
            case "TERMINADO":
                Txt_Estatus.BackColor = System.Drawing.Color.White;
                Txt_Estatus.ForeColor = System.Drawing.Color.FromArgb(17, 80, 17);
                break;
            default:
                Txt_Estatus.BackColor = System.Drawing.Color.White;
                Txt_Estatus.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
                break;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cambiar_Estatus_Grid
    ///DESCRIPCIÓN          : Cambiar el Estatus que aparece en el Grid
    ///PARAMETROS           : 
    ///CREO                 : Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO           : 25/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Cambiar_Estatus_Grid()
    {
        if (Grid_Listado_Solicitudes.Rows.Count > 0)
        {
            foreach (GridViewRow Fila_Grid in Grid_Listado_Solicitudes.Rows)
            {
                Fila_Grid.Cells[7].Text = Estatus_Solicitudes(HttpUtility.HtmlDecode(Fila_Grid.Cells[7].Text.Trim()).Trim());
            }
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Estatus_Solicitudes
    ///DESCRIPCIÓN          : Regresa el Estatus a Mostrar.
    ///PARAMETROS           : 
    ///CREO                 : Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO           : 25/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private String Estatus_Solicitudes(String Estatus_Operativo)
    {
        String Estatus_Mostrar = "";
        switch (Estatus_Operativo)
        {
            case "PENDIENTE": Estatus_Mostrar = "PENDIENTE"; break;
            case "AUTORIZADA": Estatus_Mostrar = "AUTORIZADA"; break;
            case "CANCELADA": Estatus_Mostrar = "CANCELADA"; break;
            case "RECHAZADA": Estatus_Mostrar = "RECHAZADA"; break;
            case "EN_REPARACION": Estatus_Mostrar = "EN REPARACIÓN"; break;
            case "TERMINADO": Estatus_Mostrar = "TERMINADO"; break;
            case "ENTREGADO": Estatus_Mostrar = "ENTREGADO"; break;
        }
        return Estatus_Mostrar;
    }

    #endregion

    #region Generales [Configuracion, Limpiar]

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
    ///DESCRIPCIÓN: Limpia los campos del Formulario.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Limpiar_Formulario()
    {
        Hdf_No_Solicitud.Value = "";
        Hdf_Folio_Solicitud.Value = "";
        Txt_Folio_Solicitud.Text = "";
        Txt_Fecha_Elaboracion.Text = "";
        Txt_Estatus.Text = "";
        Cmb_Tipo_Servicio.SelectedIndex = 0;
        Txt_Kilometraje.Text = "";
        Cmb_Unidad_Responsable.SelectedIndex = 0;
        Txt_Descripcion_Servicio.Text = "";
        Txt_Correo_Electronico.Text = "";
        Txt_Diagnostico_Servicio.Text = "";
        Limpiar_Formulario_Vehiculo();
        Cargar_Estilo_Estatus();
        Grid_Facturas.DataSource = null;
        Grid_Facturas.DataBind();
        Session["Tabla_Documentos"] = null;
        Session["Diccionario_Archivos"] = null;
        Session["Archivos_Servicios_Vista"] = null;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario_Vehiculo
    ///DESCRIPCIÓN: Limpia los campos del Formulario [Seccion de Vehiculos].
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Limpiar_Formulario_Vehiculo()
    {
        Hdf_Vehiculo_ID.Value = "";
        Txt_No_Inventario.Text = "";
        Txt_No_Economico.Text = "";
        Txt_Datos_Vehiculo.Text = "";
        Txt_Placas.Text = "";
        Txt_Anio.Text = "";
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
    ///DESCRIPCIÓN: Se Habilitan y/o inhabilitan los campos dependendiendo de la Condicion.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Configuracion_Formulario(String Operacion)
    {
        switch (Operacion)
        {
            case "INICIAL":
                Btn_Salir.AlternateText = "Salir";
                Btn_Salir.ToolTip = "Salir";
                Btn_Salir.Visible = true;
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                Btn_Imprimir_Solicitud_Servicio.Visible = false;
                Cmb_Tipo_Servicio.Enabled = false;
                Txt_No_Inventario.Enabled = false;
                Txt_Kilometraje.Enabled = false;
                //Txt_Descripcion_Servicio.Enabled = false;
                Txt_Correo_Electronico.Enabled = false;
                Div_Listado_Solicitudes.Style["Display"] = "block";    
                //Div_Listado_Solicitudes.Visible = true;
                Div_Campos.Style["Display"] = "none";
                Afu_Archivos_Servicios.Style["Display"] = "none";
                Btn_Guardar.Visible = false;
                break;
            case "OPERACION":
                Btn_Salir.AlternateText = "Cancelar";
                Btn_Salir.ToolTip = "Cancelar Operación";
                Btn_Salir.Visible = true;
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                Btn_Guardar.Visible = false;
                Btn_Imprimir_Solicitud_Servicio.Visible = true;
                Cmb_Tipo_Servicio.Enabled = true;
                Txt_No_Inventario.Enabled = true;
                Txt_Kilometraje.Enabled = true;
                //Txt_Descripcion_Servicio.Enabled = true;
                Txt_Correo_Electronico.Enabled = true;
                Div_Listado_Solicitudes.Visible = false;
                Div_Listado_Solicitudes.Style["Display"] = "none";
                Div_Campos.Style["Display"] = "block";
                Afu_Archivos_Servicios.Style["Display"] = "block";
                break;
            case "MOSTRAR INFORMACION":
                Btn_Salir.AlternateText = "Salir";
                Btn_Salir.ToolTip = "Salir";
                Btn_Salir.Visible = true;
                Btn_Guardar.Visible = true;
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                Btn_Imprimir_Solicitud_Servicio.Visible = true;
                Cmb_Tipo_Servicio.Enabled = false;
                Txt_No_Inventario.Enabled = false;
                Txt_Kilometraje.Enabled = false;
                //Txt_Descripcion_Servicio.Enabled = false;
                Txt_Correo_Electronico.Enabled = false;
                Div_Listado_Solicitudes.Style["Display"] = "none";    
            //Div_Listado_Solicitudes.Visible = false;
                Div_Campos.Style["Display"] = "block";
                Afu_Archivos_Servicios.Style["Display"] = "block";
                Btn_Imprimir_Solicitud_Servicio.Visible = true;
                break;
        }
    }

    #endregion

    #region Clase de Negocio de Solicitudes [Alta, Modificacion y Consulta]

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Mostrar_Registro
    ///DESCRIPCIÓN: Muestra el Registro en los campos.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Mostrar_Registro()
    {
        Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
        Solicitud.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
        Solicitud = Solicitud.Consultar_Detalles_Solicitud_Servicio();
        if (Solicitud.P_No_Solicitud > (-1))
        {
            Txt_Fecha_Elaboracion.Text = String.Format("{0:dd/MMM/yyyy}", Solicitud.P_Fecha_Elaboracion);
            Txt_Folio_Solicitud.Text = Solicitud.P_Folio_Solicitud.Trim();
            Cmb_Tipo_Servicio.SelectedIndex = Cmb_Tipo_Servicio.Items.IndexOf(Cmb_Tipo_Servicio.Items.FindByValue(Solicitud.P_Tipo_Servicio));
            Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Solicitud.P_Dependencia_ID));
            Hdf_Vehiculo_ID.Value = Solicitud.P_Bien_ID;
            Hdf_Empleado_Solicito_ID.Value = Solicitud.P_Empleado_Solicito_ID;
            Hdf_Email.Value = Solicitud.P_Correo_Electronico;
            Txt_Descripcion_Servicio.Text = Solicitud.P_Descripcion_Servicio;
            if (Solicitud.P_Kilometraje > (-1.0))
            {
                Txt_Kilometraje.Text = String.Format("{0:############0.00}", Solicitud.P_Kilometraje);
            }
            if (Solicitud.P_Tipo_Bien.Equals("BIEN_MUEBLE"))
            {
                Pnl_Bien_Mueble_Seleccionado.Visible = true;
                Pnl_Vehiculo_Seleccionado.Visible = false;
                Hdf_Bien_Mueble_ID.Value = Solicitud.P_Bien_ID;
                Hdf_Tipo_Bien.Value = "BIEN_MUEBLE";
                Cargar_Datos_Bien_Mueble(Hdf_Bien_Mueble_ID.Value, "IDENTIFICADOR");
            }
            else if (Solicitud.P_Tipo_Bien.Equals("VEHICULO"))
            {
                Pnl_Vehiculo_Seleccionado.Visible = true;
                Pnl_Bien_Mueble_Seleccionado.Visible = false;
                Hdf_Vehiculo_ID.Value = Solicitud.P_Bien_ID;
                Hdf_Tipo_Bien.Value = "VEHICULO";
                Cargar_Datos_Vehiculo(Hdf_Vehiculo_ID.Value, "IDENTIFICADOR");
            }
            
            Txt_Estatus.Text = Solicitud.P_Estatus;
            Txt_Correo_Electronico.Text = Solicitud.P_Correo_Electronico;
            Hdf_Folio_Solicitud.Value = Solicitud.P_Folio_Solicitud;
            Cargar_Estilo_Estatus();
            Txt_Estatus.Text = Estatus_Solicitudes(Txt_Estatus.Text);
            if (Cmb_Tipo_Servicio.SelectedItem.Value.Equals("SERVICIO_CORRECTIVO"))
            {
                Cls_Ope_Tal_Servicios_Correctivos_Negocio Servicio_Negocio = new Cls_Ope_Tal_Servicios_Correctivos_Negocio();
                Servicio_Negocio.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                Servicio_Negocio = Servicio_Negocio.Consultar_Detalles_Servicio_Correctivo();
                Txt_Diagnostico_Servicio.Text = Servicio_Negocio.P_Diagnostico.Trim();
                Txt_Reparacion.Text = Servicio_Negocio.P_Reparacion.Trim();
                Hdf_Estatus_Servicio.Value = Servicio_Negocio.P_Estatus.Trim();
                Hdf_No_Servicio.Value = Servicio_Negocio.P_No_Servicio.ToString();
                Hdf_Mecanico_ID.Value = Servicio_Negocio.P_Mecanico_ID.Trim();
            }
            else if (Cmb_Tipo_Servicio.SelectedItem.Value.Equals("SERVICIO_PREVENTIVO"))
            {
                Cls_Ope_Tal_Servicios_Preventivos_Negocio Servicio_Negocio = new Cls_Ope_Tal_Servicios_Preventivos_Negocio();
                Servicio_Negocio.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                Servicio_Negocio = Servicio_Negocio.Consultar_Detalles_Servicio_Preventivo();
                Txt_Diagnostico_Servicio.Text = Servicio_Negocio.P_Diagnostico.Trim();
                Txt_Reparacion.Text = Servicio_Negocio.P_Reparacion.Trim();
                Hdf_Estatus_Servicio.Value = Servicio_Negocio.P_Estatus.Trim();
                Hdf_No_Servicio.Value = Servicio_Negocio.P_No_Servicio.ToString();
                Hdf_Mecanico_ID.Value = Servicio_Negocio.P_Mecanico_ID.Trim();
            }
        }
    }

    #endregion

    #region Reporte Orden de Trabajo

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte_Solicitud_Servicio
    ///DESCRIPCIÓN: Se Generan las Tablas para el Reporte de Orden de Trabajo
    ///PARAMETROS:  
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 24/Mayo/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Generar_Reporte_Solicitud_Servicio()
    {
        //Dataset esqueleto del Reporte
        Ds_Rpt_Tal_Archivos_Servicio Ds_Reporte = new Ds_Rpt_Tal_Archivos_Servicio();
        DataTable Tabla_Tramites = new DataTable();
        DataTable Tabla_Imagenes = new DataTable("IMAGENES_SERVICIO");
        DataTable Dt_Generales = new DataTable("DT_GENERALES");
        Dt_Generales = Ds_Reporte.Tables["DT_GENERALES"].Clone();
        Cls_Rpt_Tal_Solicitud_Servicio_Negocio Rpt_Solicitud = new Cls_Rpt_Tal_Solicitud_Servicio_Negocio();
        Rpt_Solicitud.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value.Trim());
        Rpt_Solicitud.P_Server_ = this.Server;
        Rpt_Solicitud.P_No_Servicio = Hdf_No_Servicio.Value;
        Rpt_Solicitud.P_Tipo_Servicio = Cmb_Tipo_Servicio.SelectedValue;
        DataSet Ds_Datos = Rpt_Solicitud.Crear_Reporte_Solicitud_Servicio();
        if (Ds_Datos != null)
        {
            Dt_Generales = Ds_Datos.Tables[0].Copy();
        }
        Tabla_Imagenes = Generar_Tabla_Reporte();
        if (Session["Tabla_Documentos"] != null)
        {
            Tabla_Tramites = (DataTable)Session["Tabla_Documentos"];
        }
        foreach (DataRow Dr_Renglon in Tabla_Tramites.Rows)
        {
            DataRow Renglon = Tabla_Imagenes.NewRow();
            Renglon["IMAGEN_SERVICIO_ID"] = Dr_Renglon["IMAGEN_SERVICIO_ID"];
            Renglon["EMPLEADO"] = Dr_Renglon["EMPLEADO"];
            Renglon["ESTATUS"] = Dr_Renglon["ESTATUS"];
            Renglon["RUTA_ARCHIVO"] = Dr_Renglon["RUTA_ARCHIVO"];
            Renglon["NOMBRE_ARCHIVO"] = Dr_Renglon["NOMBRE_ARCHIVO"];
            Renglon["FECHA_ARCHIVO"] = Dr_Renglon["FECHA_ARCHIVO"];

            if (!String.IsNullOrEmpty(Dr_Renglon["NOMBRE_ARCHIVO"].ToString()))
            {
                String Archivo = Server.MapPath("../../ARCHIVOS_TALLER_MUNICIPAL/FOTOS_SERVICIOS") + "/" + Dr_Renglon["NOMBRE_ARCHIVO"].ToString();
                if (System.IO.File.Exists(Archivo)) Renglon["CHECKSUM"] = Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image.FromFile(Archivo));
            }
            Tabla_Imagenes.Rows.Add(Renglon);
        }
        Tabla_Imagenes.AcceptChanges();
        //Se crea El DataSet de los Datos
        DataSet Ds_Consulta = new DataSet();
        Ds_Consulta.Tables.Add(Dt_Generales.Copy());
        Ds_Consulta.Tables.Add(new Cls_Tal_Parametros_Negocio().Obtener_Tabla_Reporte(this.Server));
        Ds_Consulta.Tables.Add(Tabla_Imagenes);
        Ds_Consulta.Tables[2].TableName = "IMAGENES_SERVICIO";
        //Ds_Consulta.Tables.Add(Dt_SubPartes.Copy());
        //Ds_Consulta.Tables.Add(Dt_Partes.Copy());

        //Generar y lanzar el reporte
        String Ruta_Reporte_Crystal = "Rpt_Tal_Imagenes_Servicio.rpt";
        Generar_Reporte(Ds_Consulta, Ds_Reporte, Ruta_Reporte_Crystal);

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
    ///DESCRIPCIÓN: caraga el data set fisico con el cual se genera el Reporte especificado
    ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
    ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
    ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
    ///CREO: Susana Trigueros Armenta.
    ///FECHA_CREO: 01/Mayo/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Generar_Reporte(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte)
    {
        ReportDocument Reporte = new ReportDocument();
        String File_Path = Server.MapPath("../Rpt/Servicios_Generales/" + Nombre_Reporte);
        Reporte.Load(File_Path);
        String Nombre_Reporte_Generar = "Rpt_Tal_Imagenes_Servicio_" + Cls_Sessiones.No_Empleado + "_" + Convert.ToString(DateTime.Now.ToString("yyyy'-'MM'-'dd'_t'HH'-'mm'-'ss")) + ".pdf";
        String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
        Ds_Reporte = Data_Set_Consulta_DB;
        Reporte.SetDataSource(Ds_Reporte);
        ExportOptions Export_Options = new ExportOptions();
        DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
        Disk_File_Destination_Options.DiskFileName = Server.MapPath(Ruta);
        Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
        Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
        Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
        Reporte.Export(Export_Options);
        Mostrar_Reporte(Nombre_Reporte_Generar, "PDF");
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Convertir_Imagen_A_Cadena_Bytes
    ///DESCRIPCIÓN: Convierte la Imagen a una Cadena de Bytes.
    ///PROPIEDADES:   1.  P_Imagen.  Imagen a Convertir.    
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private Byte[] Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image P_Imagen)
    {
        Byte[] Img_Bytes = null;
        try
        {
            if (P_Imagen != null)
            {
                MemoryStream MS_Tmp = new MemoryStream();
                P_Imagen.Save(MS_Tmp, P_Imagen.RawFormat);
                Img_Bytes = MS_Tmp.GetBuffer();
                MS_Tmp.Close();
            }
        }
        catch (Exception Ex)
        {

        }
        return Img_Bytes;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Obtener_Datos_Generales
    ///DESCRIPCIÓN: Obtiene los datos generales del Reporte
    ///PARAMETROS:  
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 24/Mayo/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Obtener_Datos_Generales(ref DataTable Dt_Generales)
    {
        DataRow Fila_Nueva = Dt_Generales.NewRow();
        Fila_Nueva["NO_SOLICITUD"] = Convert.ToInt32(Hdf_No_Solicitud.Value.Trim());
        Fila_Nueva["FECHA_ELABORACION"] = Convert.ToDateTime(Txt_Fecha_Elaboracion.Text);
        //Fila_Nueva["FECHA_RECIBO"] = "";
        Fila_Nueva["DEPENDENCIA"] = Cmb_Unidad_Responsable.SelectedItem.Text.Trim();
        Fila_Nueva["VEHICULO"] = Txt_Datos_Vehiculo.Text.Trim();
        Fila_Nueva["NO_INVENTARIO"] = Convert.ToInt32(Txt_No_Inventario.Text.Trim());
        Fila_Nueva["NO_ECONOMICO"] = Txt_No_Economico.Text.Trim();
        Fila_Nueva["PLACAS"] = Txt_Placas.Text.Trim();
        if (Txt_Anio.Text.Trim().Length > 0)
        {
            Fila_Nueva["ANIO"] = Convert.ToInt32(Txt_Anio.Text.Trim());
        }
        //Fila_Nueva["KILOMETRAJE"] = "";
        Fila_Nueva["PROVEEDOR_ASIGNADO"] = Txt_Estatus.Text.Trim();
        Fila_Nueva["DESCRIPCION_SERVICIO"] = Txt_Descripcion_Servicio.Text.Trim();
        Fila_Nueva["FOLIO_SOLICITUD"] = Hdf_Folio_Solicitud.Value.Trim();
        Fila_Nueva["DIAGNISTICO_MECANICO"] = Txt_Diagnostico_Servicio.Text.Trim();
        Dt_Generales.Rows.Add(Fila_Nueva);
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Separar_Partes_Columnas
    ///DESCRIPCIÓN: Separa en 3 columnas los datos de las tablas
    ///PARAMETROS:  
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 24/Mayo/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private DataTable Separar_Partes_Columnas(DataTable Dt_Datos)
    {
        DataTable Dt_Partes_Separadas = new DataTable();
        Dt_Partes_Separadas.Columns.Add("PARTE_ID_1", Type.GetType("System.String"));
        Dt_Partes_Separadas.Columns.Add("PARTE_NOMBRE_1", Type.GetType("System.String"));
        Dt_Partes_Separadas.Columns.Add("PARTE_ID_2", Type.GetType("System.String"));
        Dt_Partes_Separadas.Columns.Add("PARTE_NOMBRE_2", Type.GetType("System.String"));
        Dt_Partes_Separadas.Columns.Add("PARTE_ID_3", Type.GetType("System.String"));
        Dt_Partes_Separadas.Columns.Add("PARTE_NOMBRE_3", Type.GetType("System.String"));
        Int32 Fila_Actual = -1;
        Int32 Columna_Toca = 1;
        foreach (DataRow Fila_Leida in Dt_Datos.Rows)
        {
            if (Columna_Toca == 1)
            {
                DataRow Fila_Nueva = Dt_Partes_Separadas.NewRow();
                Fila_Nueva["PARTE_ID_1"] = Fila_Leida["PARTE_ID"].ToString().Trim();
                Fila_Nueva["PARTE_NOMBRE_1"] = Fila_Leida["PARTE_NOMBRE"].ToString().Trim();
                Dt_Partes_Separadas.Rows.Add(Fila_Nueva);
                Fila_Actual++;
                Columna_Toca = 2;
            }
            else if (Columna_Toca == 2)
            {
                Dt_Partes_Separadas.Rows[Fila_Actual].SetField("PARTE_ID_2", Fila_Leida["PARTE_ID"].ToString().Trim());
                Dt_Partes_Separadas.Rows[Fila_Actual].SetField("PARTE_NOMBRE_2", Fila_Leida["PARTE_NOMBRE"].ToString().Trim());
                Columna_Toca = 3;
            }
            else if (Columna_Toca == 3)
            {
                Dt_Partes_Separadas.Rows[Fila_Actual].SetField("PARTE_ID_3", Fila_Leida["PARTE_ID"].ToString().Trim());
                Dt_Partes_Separadas.Rows[Fila_Actual].SetField("PARTE_NOMBRE_3", Fila_Leida["PARTE_NOMBRE"].ToString().Trim());
                Columna_Toca = 1;
            }
        }
        return Dt_Partes_Separadas;
    }


    #endregion

    #region Reporte Bitacora
    
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
    ///DESCRIPCIÓN: caraga el data set fisico con el cual se genera el Reporte especificado
    ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
    ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
    ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
    ///CREO: Susana Trigueros Armenta.
    ///FECHA_CREO: 01/Mayo/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Generar_Reporte_Bitacora(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte)
    {
        ReportDocument Reporte = new ReportDocument();
        String File_Path = Server.MapPath("../Rpt/Servicios_Generales/" + Nombre_Reporte);
        Reporte.Load(File_Path);
        String Nombre_Reporte_Generar = "Rpt_Tal_Bit_Seg_Serv_" + Cls_Sessiones.No_Empleado + "_" + Convert.ToString(DateTime.Now.ToString("yyyy'-'MM'-'dd'_t'HH'-'mm'-'ss")) + ".pdf";
        String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
        Ds_Reporte = Data_Set_Consulta_DB;
        Reporte.SetDataSource(Ds_Reporte);
        ExportOptions Export_Options = new ExportOptions();
        DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
        Disk_File_Destination_Options.DiskFileName = Server.MapPath(Ruta);
        Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
        Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
        Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
        Reporte.Export(Export_Options);
        Mostrar_Reporte(Nombre_Reporte_Generar, "PDF");
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Obtener_Datos_Generales
    ///DESCRIPCIÓN: Obtiene los datos generales del Reporte
    ///PARAMETROS:  
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 24/Mayo/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Obtener_Datos_Generales_Servicio(ref DataTable Dt_Generales)
    {
        DataRow Fila_Nueva = Dt_Generales.NewRow();
        Fila_Nueva["NO_SOLICITUD"] = Convert.ToInt32(Hdf_No_Solicitud.Value.Trim());
        Fila_Nueva["FECHA_ELABORACION"] = Convert.ToDateTime(Txt_Fecha_Elaboracion.Text);
        //Fila_Nueva["FECHA_RECIBO"] = "";
        Fila_Nueva["DEPENDENCIA"] = Cmb_Unidad_Responsable.SelectedItem.Text.Trim();
        Fila_Nueva["VEHICULO"] = Txt_Datos_Vehiculo.Text.Trim();
        Fila_Nueva["NO_INVENTARIO"] = Convert.ToInt32(Txt_No_Inventario.Text.Trim());
        Fila_Nueva["NO_ECONOMICO"] = Txt_No_Economico.Text.Trim();
        Fila_Nueva["PLACAS"] = Txt_Placas.Text.Trim();
        if (Txt_Anio.Text.Trim().Length > 0)
        {
            Fila_Nueva["ANIO"] = Convert.ToInt32(Txt_Anio.Text.Trim());
        }
        //Fila_Nueva["KILOMETRAJE"] = "";
        //Fila_Nueva["PROVEEDOR_ASIGNADO"] = "";
        Fila_Nueva["DESCRIPCION_SERVICIO"] = Txt_Descripcion_Servicio.Text.Trim();
        Fila_Nueva["FOLIO_SOLICITUD"] = Hdf_Folio_Solicitud.Value.Trim();
        Dt_Generales.Rows.Add(Fila_Nueva);
    }

    #endregion

    #endregion

    #region Grids

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Solicitudes_PageIndexChanging
    ///DESCRIPCIÓN: Maneja la paginación del Listado
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Mayo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Listado_Solicitudes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Grid_Listado_Solicitudes.SelectedIndex = (-1);
            Grid_Listado_Solicitudes.PageIndex = e.NewPageIndex;
            Llenar_Listado_Solicitudes();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Solicitudes_SelectedIndexChanged
    ///DESCRIPCIÓN: Obtiene los datos de una Solicitud 
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Mayo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Listado_Solicitudes_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (Grid_Listado_Solicitudes.SelectedIndex > (-1))
            {
                Limpiar_Formulario();
                Hdf_No_Solicitud.Value = Grid_Listado_Solicitudes.SelectedDataKey["NO_SOLICITUD"].ToString().Trim();
                Configuracion_Formulario("MOSTRAR INFORMACION");
                Mostrar_Registro();
                Llenar_Grid_Archivos();
                Grid_Listado_Solicitudes.SelectedIndex = -1;
                System.Threading.Thread.Sleep(500);
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Sorting
    ///DESCRIPCIÓN: se obtienen los datos del movimiento
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 07/26/2011 06:22:19 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Grid_Listado_Servicios_Sorting(object sender, GridViewSortEventArgs e)
    {
        String Orden = "";
        //Tabla que contendra la Lista de los servicios
        DataTable Dt_Listado = null;
        if (Session["Archivos_Servicios"] != null)
            Dt_Listado = (DataTable)Session["Archivos_Servicios"];
        //Si hay Datos
        if (Dt_Listado != null)
        {
            //Se realiza el Ordenamiento segun el campo seleccionado en nuestro Grid
            //Se Define la Vista donde quedara el ordenamiento
            DataView Dv_Listado = new DataView(Dt_Listado);
            if (ViewState["SortDirection"] != null)
                Orden = ViewState["SortDirection"].ToString();

            if (Orden.Equals("ASC"))
            {
                Dv_Listado.Sort = e.SortExpression + " " + "DESC";
                ViewState["SortDirection"] = "DESC";
            }
            else
            {
                Dv_Listado.Sort = e.SortExpression + " " + "ASC";
                ViewState["SortDirection"] = "ASC";
            }
            //Asignar Vista al Origen de Datos de nuestro Grid
            Grid_Listado_Solicitudes.DataSource = Dv_Listado;
            Grid_Listado_Solicitudes.DataBind();
            Session["Archivos_Servicios_Vista"] = Dv_Listado;
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN     : Llenar_Grid_Archivos
    ///DESCRIPCIÓN              : Llenar los archivos por solicitud
    ///PROPIEDADES:
    ///CREO                     : Jesus Toledo Rodriguez
    ///FECHA_CREO               : 19/Marzo/2013
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Grid_Archivos()
    {
        DataTable Resultado_Consulta_Imagenes = new DataTable();
        try
        {
            Cls_Ope_Tal_Archivos_Servicios_Negocio Objeto_Negocio = new Cls_Ope_Tal_Archivos_Servicios_Negocio();
            Objeto_Negocio.P_No_Solicitud = Int32.Parse(Hdf_No_Solicitud.Value);
            Resultado_Consulta_Imagenes = Objeto_Negocio.Consulta_Imagenes_Servicio();
            if (Resultado_Consulta_Imagenes.Rows.Count > 0)
                Session["Tabla_Documentos"] = Resultado_Consulta_Imagenes;
            Llenar_Listado_Facturas();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN     : Grid_Facturas_RowDeleting
    ///DESCRIPCIÓN              : Evento RowDeleting del Grid de Facturas
    ///PROPIEDADES:
    ///CREO                     : Jesus Toledo Rodriguez
    ///FECHA_CREO               : 12/Junio/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Facturas_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            DataTable Tabla_Tramites = Generar_Tabla_Documentos();
            DataRow[] Dr_Seleccionado;
            String Ruta = "";
            Response.Clear();
            Response.ClearHeaders();
            Response.ContentType = "text/html";

            int Grid_Id = Convert.ToInt32(e.RowIndex);
            if (Session["Tabla_Documentos"] != null)
            {
                Tabla_Tramites = (DataTable)Session["Tabla_Documentos"];
                Dr_Seleccionado = Tabla_Tramites.Select("NOMBRE_ARCHIVO = '" + Grid_Facturas.DataKeys[Grid_Id]["NOMBRE_ARCHIVO"].ToString()+"'");
                if (Dr_Seleccionado.Length > 0)
                {
                    Ruta = @Server.MapPath("../../ARCHIVOS_TALLER_MUNICIPAL/FOTOS_SERVICIOS/") + Grid_Facturas.DataKeys[Grid_Id]["NOMBRE_ARCHIVO"].ToString();
                    Tabla_Tramites.Rows.Remove(Dr_Seleccionado[0]);
                    Tabla_Tramites.AcceptChanges();
                    //Verificar si existe
                    if (System.IO.File.Exists(Ruta) == true)
                    {
                        Response.WriteFile(Ruta);
                        Response.Flush();
                        Response.Close();
                        System.IO.File.Delete(Ruta);                        
                }
                }
            }
            Session["Tabla_Documentos"] = Tabla_Tramites;
            Llenar_Listado_Facturas();
            Grid_Facturas.SelectedIndex = (-1);
            
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Mensaje_Error.Text = "Excepción ['" + Ex.Message + "']";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN     : Btn_Guardar_Click
    ///DESCRIPCIÓN              : Evento del boton Guardar para ejecutar la operacion de registro de imagenes
    ///PROPIEDADES              : Object sender, ImageClickEventArgs e
    ///CREO                     : Jesus Toledo Rodriguez
    ///FECHA_CREO               : 19/Marzo/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Guardar_Click(object sender, ImageClickEventArgs e)
    {
        Cls_Ope_Tal_Archivos_Servicios_Negocio Objeto_Negocio = new Cls_Ope_Tal_Archivos_Servicios_Negocio();
        try
        {
            DataTable Tabla_Tramites = Generar_Tabla_Documentos();
            if (Session["Tabla_Documentos"] != null)
            {
                Tabla_Tramites = (DataTable)Session["Tabla_Documentos"];
            }
            if(Tabla_Tramites.Rows.Count>0)
            {
                Objeto_Negocio.P_No_Solicitud = Int32.Parse(Hdf_No_Solicitud.Value);
                Objeto_Negocio.P_Dt_Imagenes_Servicio = Tabla_Tramites;
                Objeto_Negocio.Alta_Imagenes_Servicio();
                Llenar_Grid_Archivos();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Archivos Servicios", "alert('Se guardaron las imágenes correctamente');", true);
            }

        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Mensaje_Error.Text = "Excepción ['" + Ex.Message + "']";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Eliminar_Click
    ///DESCRIPCIÓN: Elimina imagen del grid y fisicamente
    ///PROPIEDADES:     
    ///CREO: Jesus Toledo Rodriguez.
    ///FECHA_CREO: 04/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    protected void Btn_Eliminar_Click(object sender, ImageClickEventArgs e)
        {
            Cls_Ope_Tal_Archivos_Servicios_Negocio Objeto_Negocio = new Cls_Ope_Tal_Archivos_Servicios_Negocio();
            try
            {
                Grid_Facturas.SelectedIndex = int.Parse(((ImageButton)sender).CommandArgument);
                DataTable Tabla_Tramites = Generar_Tabla_Documentos();
                DataRow[] Dr_Seleccionado;
                String Ruta = "";

                if (Session["Tabla_Documentos"] != null)
                {
                    Tabla_Tramites = (DataTable)Session["Tabla_Documentos"];
                    Dr_Seleccionado = Tabla_Tramites.Select("NOMBRE_ARCHIVO = '" + Grid_Facturas.SelectedDataKey["NOMBRE_ARCHIVO"].ToString() + "'");
                    if (Dr_Seleccionado.Length > 0)
                    {
                        //Se borra registro de Base de Datos
                        Objeto_Negocio.P_No_Solicitud = Int32.Parse(Hdf_No_Solicitud.Value);
                        if (!String.IsNullOrEmpty(Grid_Facturas.SelectedDataKey["IMAGEN_SERVICIO_ID"].ToString()))
                        {
                            Objeto_Negocio.P_Imagen_Servicio_ID = Int32.Parse(Grid_Facturas.SelectedDataKey["IMAGEN_SERVICIO_ID"].ToString());
                            Objeto_Negocio.Eliminar_Imagen();
                        }
                        //Se Borra fisicamente del servidor
                        Ruta = @Server.MapPath("../../ARCHIVOS_TALLER_MUNICIPAL/FOTOS_SERVICIOS/") + Grid_Facturas.SelectedDataKey["NOMBRE_ARCHIVO"].ToString();
                        Tabla_Tramites.Rows.Remove(Dr_Seleccionado[0]);
                        Tabla_Tramites.AcceptChanges();
                        //Verificar si existe
                        if (System.IO.File.Exists(Ruta) == true)
                            System.IO.File.Delete(Ruta);
                    }
                    Session["Tabla_Documentos"] = Tabla_Tramites;
                    Llenar_Listado_Facturas();
                    Grid_Facturas.SelectedIndex = (-1);

                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                Lbl_Mensaje_Error.Text = "Excepción ['" + Ex.Message + "']";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
    #endregion

    #region Eventos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Imprimir_Solicitud_Servicio_Click
    ///DESCRIPCIÓN: Imprime la Solicitud
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 23/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Imprimir_Solicitud_Servicio_Click(object sender, ImageClickEventArgs e)
    {
        Cls_Ope_Tal_Archivos_Servicios_Negocio Objeto_Negocio = new Cls_Ope_Tal_Archivos_Servicios_Negocio();
        try
        {
            if (Hdf_No_Solicitud.Value.Trim().Length > 0)
            {
                if (Grid_Facturas.Rows.Count > 0)
                {
                    DataTable Tabla_Tramites = Generar_Tabla_Documentos();
                    if (Session["Tabla_Documentos"] != null)
                    {
                        Tabla_Tramites = (DataTable)Session["Tabla_Documentos"];
                    }
                    if (Tabla_Tramites.Rows.Count > 0)
                    {
                        Objeto_Negocio.P_No_Solicitud = Int32.Parse(Hdf_No_Solicitud.Value);
                        Objeto_Negocio.P_Dt_Imagenes_Servicio = Tabla_Tramites;
                        Objeto_Negocio.Alta_Imagenes_Servicio();
                        Llenar_Grid_Archivos();
                        Generar_Reporte_Solicitud_Servicio();
                    }
                    else
                    {
                        Lbl_Ecabezado_Mensaje.Text = "No hay imagenes guardadas en la Solicitud Seleccionada.";
                        Lbl_Mensaje_Error.Text = "";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                }
                else
                {
                    Lbl_Ecabezado_Mensaje.Text = "No hay imagenes en la Solicitud Seleccionada.";
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = "No hay Solicitud Seleccionada para Imprimir.";
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }            
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Mensaje_Error.Text = "Excepción ['" + Ex.Message + "']";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
    /// USUARIO CREO:        Juan Alberto Hernández Negrete.
    /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:    Salvador Hernández Ramírez
    /// FECHA MODIFICO:      23-Mayo-2011
    /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
    {
        String Pagina = "../../Reporte/";
        try
        {
            Pagina = Pagina + Nombre_Reporte_Generar;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
            "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Ejecuta el Proceso para Salir.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Salir.AlternateText.Trim().Equals("Salir"))
            {
                if (Div_Campos.Visible)
                {
                    Limpiar_Formulario();
                    Configuracion_Formulario("INICIAL");
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Limpiar_Formulario();
                Configuracion_Formulario("INICIAL");
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Mensaje_Error.Text = "Excepción ['" + Ex.Message + "']";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Actualizar_Listado_Click
    ///DESCRIPCIÓN: Ejecuta el Proceso para Salir.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Actualizar_Listado_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Llenar_Listado_Solicitudes();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Mensaje_Error.Text = "Excepción ['" + Ex.Message + "']";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    protected void FileUploadComplete(object sender, EventArgs e)
    {
        AsyncFileUpload archivo;
        archivo = (AsyncFileUpload)sender;
        String Filename;
        String Nombre_Final;
        Filename = archivo.FileName;
        String[] arr1;
        arr1 = Filename.Split('\\');
        int len = arr1.Length;
        String img1 = arr1[len - 1];
        String filext = img1.Substring(img1.LastIndexOf(".") + 1);
        if (filext == "jpg" || filext == "JPG" || filext == "jpeg" || filext == "JPEG" || filext == "png" || filext == "PNG" || filext == "gif" || filext == "GIF")
        {
            if (archivo.FileContent.Length > 2621440) { return; }
            HashAlgorithm sha = HashAlgorithm.Create("SHA1");
            String Checksum_Archivo = null;
            string filename = System.IO.Path.GetFileName(Afu_Archivos_Servicios.FileName);
            Checksum_Archivo = BitConverter.ToString(sha.ComputeHash(Afu_Archivos_Servicios.FileBytes));//obtener checksum del archivo
            Nombre_Final = "F-" + Int32.Parse(Txt_Folio_Solicitud.Text.Trim()).ToString() +"_"+ Txt_Estatus.Text.Trim().Replace(" ", "") + "_" + (Grid_Facturas.Rows.Count + 1) + "." + filext;
            Afu_Archivos_Servicios.SaveAs(Server.MapPath("../../ARCHIVOS_TALLER_MUNICIPAL/FOTOS_SERVICIOS/") + Nombre_Final);
            Dictionary<String, Byte[]> Diccionario_Archivos = Obtener_Diccionario_Archivos();   //obtener diccionario checksum-archivo
            DataTable Tabla_Documentos;//Crea DT para guardar resultado
            if (Session["Tabla_Documentos"] != null) { Tabla_Documentos = (DataTable)Session["Tabla_Documentos"]; }
            else { Tabla_Documentos = Generar_Tabla_Documentos(); }
            if (!Diccionario_Archivos.ContainsKey(Checksum_Archivo)) //si el checksum no esta en el diccionario, agregarlo y guardar en variable de sesion
            {
                DataRow Nueva_Fila = Tabla_Documentos.NewRow();
                Nueva_Fila["EMPLEADO"] = Cls_Sessiones.Nombre_Empleado;
                Nueva_Fila["ESTATUS"] = Txt_Estatus.Text.Trim();
                Nueva_Fila["FECHA_ARCHIVO"] = DateTime.Now;
                Nueva_Fila["NOMBRE_ARCHIVO"] = Nombre_Final;//Subir Archivo
                Nueva_Fila["RUTA_ARCHIVO"] = "../../ARCHIVOS_TALLER_MUNICIPAL/FOTOS_SERVICIOS/" + Nombre_Final;
                Nueva_Fila["CHECKSUM"] = Checksum_Archivo;
                Tabla_Documentos.Rows.Add(Nueva_Fila);
                Diccionario_Archivos.Add(Checksum_Archivo, Afu_Archivos_Servicios.FileBytes);
                Session["Diccionario_Archivos"] = Diccionario_Archivos;
            }

            Session["Tabla_Documentos"] = Tabla_Documentos;
            //Fup_Factura_Recibida.
            Llenar_Listado_Facturas();
        }
        
        
    }
    ///*******************************************************************************************************
    /// 	NOMBRE_FUNCIÓN: Obtener_Diccionario_Archivos
    /// 	DESCRIPCIÓN: Regresa el diccionario checksum-archivo si se encuentra en variable de sesion y si no,
    /// 	            regresa un diccionario vacio
    /// 	PARÁMETROS:
    /// 	CREO: Roberto González Oseguera
    /// 	FECHA_CREO: 04-may-2011
    /// 	MODIFICÓ: 
    /// 	FECHA_MODIFICÓ: 
    /// 	CAUSA_MODIFICACIÓN: 
    ///*******************************************************************************************************
    private Dictionary<String, Byte[]> Obtener_Diccionario_Archivos()
    {
        Dictionary<String, Byte[]> Diccionario_Archivos = new Dictionary<String, Byte[]>();

        // si existe el diccionario en variable de sesion
        if (Session["Diccionario_Archivos"] != null)
        {
            Diccionario_Archivos = (Dictionary<String, Byte[]>)Session["Diccionario_Archivos"];
        }

        return Diccionario_Archivos;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Servicios
    ///DESCRIPCIÓN: Se llena el Listado de archivos.
    ///PROPIEDADES:     
    ///CREO: Jesus Toledo Rodriguez
    ///FECHA_CREO: 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Llenar_Listado_Facturas()
    {
        DataTable Dt_Resultados = new DataTable();
        if (Session["Tabla_Documentos"] != null)
        {
            Dt_Resultados = (DataTable)Session["Tabla_Documentos"];
        }
        else
        {
            Dt_Resultados = Generar_Tabla_Documentos();
        }
        //Grid_Facturas.Visible = true;
        //Grid_Facturas.Columns[6].Visible = true;
        Grid_Facturas.DataSource = Dt_Resultados;
        Grid_Facturas.DataBind();
        //Grid_Facturas.Columns[6].Visible = false;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Generar_Tabla_Documentos
    /// DESCRIPCION: Genera la tabla de Documentos, el esquema para guardar los tipos de document a recibir
    /// PARAMETROS: 
    /// CREO: Jesus Toledo Rodriguez
    /// FECHA_CREO: 04-may-2012
    /// MODIFICO:
    /// FECHA_MODIFICO:
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private DataTable Generar_Tabla_Documentos()
    {
        DataTable Tabla_Nueva = new DataTable();
        DataColumn Columna0_Clave_Documento;
        DataColumn Columna1_Nombre_Documento;
        DataColumn Columna2_Nombre_Archivo;
        DataColumn Columna3_Ruta_Archivo;
        DataColumn Columna5_Checksum;
        DataColumn Columna7;
        DataColumn Columna12;
        DataColumn Columna13;

        try
        {
            // ---------- Inicializar columnas
            Columna13 = new DataColumn();
            Columna13.DataType = System.Type.GetType("System.String");
            Columna13.ColumnName = "IMAGEN_SERVICIO_ID";
            Tabla_Nueva.Columns.Add(Columna13);
            Columna0_Clave_Documento = new DataColumn();
            Columna0_Clave_Documento.DataType = System.Type.GetType("System.String");
            Columna0_Clave_Documento.ColumnName = "EMPLEADO";
            Tabla_Nueva.Columns.Add(Columna0_Clave_Documento);
            Columna1_Nombre_Documento = new DataColumn();
            Columna1_Nombre_Documento.DataType = System.Type.GetType("System.String");
            Columna1_Nombre_Documento.ColumnName = "ESTATUS";
            Tabla_Nueva.Columns.Add(Columna1_Nombre_Documento);
            Columna2_Nombre_Archivo = new DataColumn();
            Columna2_Nombre_Archivo.DataType = System.Type.GetType("System.String");
            Columna2_Nombre_Archivo.ColumnName = "NOMBRE_ARCHIVO";
            Tabla_Nueva.Columns.Add(Columna2_Nombre_Archivo);
            Columna3_Ruta_Archivo = new DataColumn();
            Columna3_Ruta_Archivo.DataType = System.Type.GetType("System.String");
            Columna3_Ruta_Archivo.ColumnName = "RUTA_ARCHIVO";
            Tabla_Nueva.Columns.Add(Columna3_Ruta_Archivo);
            Columna5_Checksum = new DataColumn();
            Columna5_Checksum.DataType = System.Type.GetType("System.String");
            Columna5_Checksum.ColumnName = "CHECKSUM";
            Tabla_Nueva.Columns.Add(Columna5_Checksum);
            Columna7 = new DataColumn();
            Columna7.DataType = System.Type.GetType("System.String");
            Columna7.ColumnName = "COMENTARIOS";
            Tabla_Nueva.Columns.Add(Columna7);
            Columna12 = new DataColumn();
            Columna12.DataType = System.Type.GetType("System.DateTime");
            Columna12.ColumnName = "FECHA_ARCHIVO";
            Tabla_Nueva.Columns.Add(Columna12);


            return Tabla_Nueva;
        }
        catch (Exception ex)
        {
            throw new Exception("Generar_Tabla_Documentos " + ex.Message.ToString(), ex);
        }
    }
    private DataTable Generar_Tabla_Reporte()
    {
        DataTable Tabla_Nueva = new DataTable();
        DataColumn Columna0_Clave_Documento;
        DataColumn Columna1_Nombre_Documento;
        DataColumn Columna2_Nombre_Archivo;
        DataColumn Columna3_Ruta_Archivo;
        DataColumn Columna5_Checksum;
        DataColumn Columna7;
        DataColumn Columna12;
        DataColumn Columna13;

        try
        {
            // ---------- Inicializar columnas
            Columna13 = new DataColumn();
            Columna13.DataType = System.Type.GetType("System.String");
            Columna13.ColumnName = "IMAGEN_SERVICIO_ID";
            Tabla_Nueva.Columns.Add(Columna13);
            Columna0_Clave_Documento = new DataColumn();
            Columna0_Clave_Documento.DataType = System.Type.GetType("System.String");
            Columna0_Clave_Documento.ColumnName = "EMPLEADO";
            Tabla_Nueva.Columns.Add(Columna0_Clave_Documento);
            Columna1_Nombre_Documento = new DataColumn();
            Columna1_Nombre_Documento.DataType = System.Type.GetType("System.String");
            Columna1_Nombre_Documento.ColumnName = "ESTATUS";
            Tabla_Nueva.Columns.Add(Columna1_Nombre_Documento);
            Columna2_Nombre_Archivo = new DataColumn();
            Columna2_Nombre_Archivo.DataType = System.Type.GetType("System.String");
            Columna2_Nombre_Archivo.ColumnName = "NOMBRE_ARCHIVO";
            Tabla_Nueva.Columns.Add(Columna2_Nombre_Archivo);
            Columna3_Ruta_Archivo = new DataColumn();
            Columna3_Ruta_Archivo.DataType = System.Type.GetType("System.String");
            Columna3_Ruta_Archivo.ColumnName = "RUTA_ARCHIVO";
            Tabla_Nueva.Columns.Add(Columna3_Ruta_Archivo);
            Columna5_Checksum = new DataColumn();
            Columna5_Checksum.DataType = System.Type.GetType("System.Byte[]");
            Columna5_Checksum.ColumnName = "CHECKSUM";
            Tabla_Nueva.Columns.Add(Columna5_Checksum);
            Columna7 = new DataColumn();
            Columna7.DataType = System.Type.GetType("System.String");
            Columna7.ColumnName = "COMENTARIOS";
            Tabla_Nueva.Columns.Add(Columna7);
            Columna12 = new DataColumn();
            Columna12.DataType = System.Type.GetType("System.DateTime");
            Columna12.ColumnName = "FECHA_ARCHIVO";
            Tabla_Nueva.Columns.Add(Columna12);


            return Tabla_Nueva;
        }
        catch (Exception ex)
        {
            throw new Exception("Generar_Tabla_Documentos " + ex.Message.ToString(), ex);
        }
    }
    #endregion


       
}