﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Tal_Calendarizacion_Rev_Mec.aspx.cs" Inherits="paginas_Taller_Mecanico_Frm_Ope_Tal_Calendarizacion_Rev_Mec" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScptM_Catalogo" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />  
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>                     
            </asp:UpdateProgress>
            <div id="Div_Requisitos" style="background-color:#ffffff; width:100%; height:100%;">
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo" colspan="2">Calendarizacion de Revista Mecanica</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                          <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;">              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top">
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                            </table>                   
                          </div>                
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>                        
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" style="width:50%;">
                            <asp:ImageButton ID="Btn_Modificar" runat="server"
                                ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" Width="24px" CssClass="Img_Button"
                                AlternateText="Modificar" OnClick="Btn_Modificar_Click"/>
                            <asp:ImageButton ID="Btn_Salir" runat="server"
                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" Width="24px" CssClass="Img_Button"
                                AlternateText="Salir" OnClick="Btn_Salir_Click" />
                        </td>
                        <td style="width:50%;">
                            <div id="Div_Busqueda" runat="server">
                                Busqueda
                                <asp:TextBox ID="Txt_Busqueda" runat="server" Width="130px" style="text-align:center;"></asp:TextBox>
                                <asp:ImageButton ID="Btn_Buscar" runat="server" CausesValidation="false"
                                    ImageUrl="~/paginas/imagenes/paginas/busqueda.png" ToolTip="Buscar" 
                                    AlternateText="Buscar" OnClick="Btn_Buscar_Click" />
                                <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda" runat="server" 
                                    WatermarkText="<No. Inventario>" TargetControlID="Txt_Busqueda" 
                                    WatermarkCssClass="watermarked"></cc1:TextBoxWatermarkExtender>    
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda" runat="server" 
                                    TargetControlID="Txt_Busqueda" InvalidChars="<,>,&,',!," 
                                    FilterType="Numbers" 
                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                </cc1:FilteredTextBoxExtender> 
                                <asp:ImageButton ID="Btn_Busqueda_Avanzada" runat="server" OnClick="Btn_Busqueda_Avanzada_Click"
                                    ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png" ToolTip="Busqueda Avanzada" 
                                    AlternateText="Busqueda Avanzada" Height="16px" Width="16px"/>  
                            </div>
                        </td>                        
                    </tr>
                </table>  
                <div id="Div_Datos_Vehiculo" runat="server" style="width:100%;">
                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                            <td colspan="4">
                                <asp:Panel ID="Pnl_Vehiculo_Seleccionado" runat="server" Width="99%" GroupingText="">
                                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:HiddenField ID="Hdf_Vehiculo_ID" runat="server" />
                                                <asp:Label ID="Lbl_No_Inventario" runat="server" Text="No. Inventario"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_No_Inventario" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                            </td>
                                            <td style="width:15%;">
                                                &nbsp;&nbsp;  
                                                <asp:Label ID="Lbl_No_Economico" runat="server" Text="No. Economico"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_No_Economico" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:Label ID="Lbl_Datos_Vehiculo" runat="server" Text="Vehículo"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="Txt_Datos_Vehiculo" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text="U. Responsable"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="Txt_Unidad_Responsable" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:Label ID="Lbl_Placas" runat="server" Text="Placas"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_Placas" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                            </td>
                                            <td style="width:15%;">
                                                &nbsp;&nbsp;  
                                                <asp:Label ID="Lbl_Anio" runat="server" Text="Año"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_Anio" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:Label ID="Lbl_Periodo_Calendarizacion" runat="server" Text="Periodo"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:DropDownList ID="Cmb_Periodo_Calendarizacion" runat="server" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="Cmb_Periodo_Calendarizacion_SelectedIndexChanged">
                                                    <asp:ListItem Value="">&lt;-- SELECCIONE --&gt;</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width:15%;">
                                                &nbsp;&nbsp;  
                                                <asp:Label ID="Lbl_Retraso" runat="server" Text="Retraso"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_Retraso" runat="server" Width="98%" ReadOnly="true" Font-Bold="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:Label ID="Lbl_Fecha_Ultima_Revision" runat="server" Text="Ultima Revisión"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_Fecha_Ultima_Revision" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                            </td>
                                            <td style="width:15%;">
                                                &nbsp;&nbsp;  
                                                <asp:Label ID="Lbl_Fecha_Proxima_Revision" runat="server" Text="Proxima Revisión"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_Fecha_Proxima_Revision" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </div>
                <br /><br />
                <hr style="text-align:center; width:98%;"/>
                <asp:Label ID="Lbl_Leyenda_Revistas_Mecanicas_Vencidas" runat="server" Text="Revistas Mecanicas Vencidas" CssClass="estilo_fuente" Width="98%" style="text-align:center;" Font-Bold="true"></asp:Label>
                <hr style="text-align:center; width:98%;"/>
                <br /><br />   
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td colspan="2" style="width:50%">&nbsp;</td>
                        <td style="width:18%">
                            <asp:Label ID="Lbl_Fecha_Vencimiento" runat="server" Text="Fecha Vencimiento" CssClass="estilo_fuente" Width="98%" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="width:32%">
                            <asp:TextBox ID="Txt_Fecha_Vencimiento" runat="server" Width="70%" Enabled="false"></asp:TextBox>
                            <asp:ImageButton ID="Btn_Fecha_Vencimiento" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"/>
                            <cc1:CalendarExtender ID="CE_Txt_Fecha_Vencimiento" runat="server" TargetControlID="Txt_Fecha_Vencimiento" PopupButtonID="Btn_Fecha_Vencimiento" Format="dd/MMM/yyyy" >
                            </cc1:CalendarExtender> 
                            &nbsp;
                            <asp:ImageButton ID="Btn_Actualizar_Listado" runat="server" ImageUrl="~/paginas/imagenes/paginas/actualizar_detalle.png" OnClick="Btn_Actualizar_Listado_Click" Width="16px"/>
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="Grid_List_Rev_Mec_Ven" runat="server" CssClass="GridView_1"
                    AutoGenerateColumns="False" AllowPaging="True" PageSize="10" Width="99%"
                    EmptyDataText="No hay Revistas Mecanicas Vencidas para esta Fecha."
                    OnPageIndexChanging="Grid_List_Rev_Mec_Ven_PageIndexChanging"
                    OnSelectedIndexChanged="Grid_List_Rev_Mec_Ven_SelectedIndexChanged"
                    GridLines= "None">
                    <RowStyle CssClass="GridItem" />
                    <Columns>
                        <asp:ButtonField ButtonType="Image" CommandName="Select" 
                            ImageUrl="~/paginas/imagenes/gridview/blue_button.png"  >
                            <ItemStyle Width="30px" />
                        </asp:ButtonField>
                        <asp:BoundField DataField="VEHICULO_ID" HeaderText="VEHICULO_ID" SortExpression="VEHICULO_ID" />
                        <asp:BoundField DataField="PROXIMA_REVISION" HeaderText="Proxima" SortExpression="PROXIMA_REVISION" DataFormatString="{0:dd/MMM/yyyy}">
                            <ItemStyle Width="80px" Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ULTIMA_REVISION" HeaderText="Ultima" SortExpression="ULTIMA_REVISION" DataFormatString="{0:dd/MMM/yyyy}">
                            <ItemStyle Width="80px" Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NO_INVENTARIO" HeaderText="No. Inven." SortExpression="NO_INVENTARIO" >
                            <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CLAVE_NOMBRE_DEPENDENCIA" HeaderText="Unidad Responsable" SortExpression="CLAVE_NOMBRE_DEPENDENCIA" >
                            <ItemStyle  Font-Size="X-Small"  />
                        </asp:BoundField>
                        <asp:BoundField DataField="VEHICULO_DESCRIPCION" HeaderText="Datos Generales del Vehículo" SortExpression="VEHICULO_DESCRIPCION" >
                            <ItemStyle Font-Size="X-Small" />
                        </asp:BoundField>
                    </Columns>
                    <PagerStyle CssClass="GridHeader" />
                    <SelectedRowStyle CssClass="GridSelected" />
                    <HeaderStyle CssClass="GridHeader" />                                
                    <AlternatingRowStyle CssClass="GridAltItem" />       
                </asp:GridView>
            </div>
            <br />
            <br />
            <br />
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpPnl_MPE_Busqueda_Vehiculo" runat="server"  UpdateMode="Conditional"> 
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_Busqueda_Vehiculo" runat="server" Text="Button" style="display:none;"/> 
            <cc1:ModalPopupExtender ID="MPE_Busqueda_Vehiculo" runat="server" TargetControlID="Btn_Comodin_Busqueda_Vehiculo" 
                PopupControlID="Pnl_Busqueda_Vehiculo" CancelControlID="Btn_Cerrar_Busqueda_Vehiculo" 
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter"/>
        </ContentTemplate>
    </asp:UpdatePanel>   
    <asp:Panel ID="Pnl_Busqueda_Vehiculo" runat="server" CssClass="drag" HorizontalAlign="Center" style="display:none;border-style:outset;border-color:Silver;width:760px;" >
        <center>
            <asp:Panel ID="Pnl_Busqueda_Vehiculo_Interno" runat="server" CssClass="estilo_fuente" style="cursor: move;background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
                <table class="estilo_fuente" width="100%">
                    <tr>
                        <td style="color:Black;font-size:12;font-weight:bold;width:90%">
                           <asp:Image ID="Img_Encabezado_Busqueda_Vehiculos" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                             Busqueda de Vehículos
                        </td>
                        <td align="right">
                           <asp:ImageButton ID="Btn_Cerrar_Busqueda_Vehiculo" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png"/>  
                        </td>
                    </tr>
                </table>   
            </asp:Panel>
            <asp:UpdatePanel ID="UpPnl_Busqueda_Vehiculo" runat="server"> 
                <ContentTemplate>
                <asp:UpdateProgress ID="UpPrg_Busqueda_Vehiculo" runat="server" AssociatedUpdatePanelID="UpPnl_Busqueda_Vehiculo" DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                        <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                    </ProgressTemplate>                     
                </asp:UpdateProgress>
                <br />
                <cc1:TabContainer ID="Tab_Contenedor_Pestagnas_Busqueda_Vehiculo" runat="server" Width="100%" ActiveTabIndex="0" CssClass="Tab">
                    <cc1:TabPanel  runat="server" HeaderText="Tab_Panel_Datos_Generales_Busqueda_Vehiculo" ID="Tab_Panel_Datos_Generales_Busqueda_Vehiculo" Width="100%" Height="150px" BackColor="White">
                        <HeaderTemplate>Generales</HeaderTemplate>
                            <ContentTemplate>
                            <div style="border-style:outset; width:99.5%; height:150px; background-color:White;" >
                                <table width="100%">
                                    <tr>
                                        <td style="text-align:left;" colspan="4">&nbsp;</td>
                                    </tr>                                 
                                    <tr>
                                        <td style="width:20%; text-align:left; ">
                                            <asp:Label ID="Lbl_Busqueda_Vehiculo_Numero_Inventario" runat="server" Text="Número Inventario" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width:30%; text-align:left;">
                                            <asp:TextBox ID="Txt_Busqueda_Vehiculo_Numero_Inventario" runat="server" Width="97%" ></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_Numero_Inventario" runat="server" TargetControlID="Txt_Busqueda_Vehiculo_Numero_Inventario" FilterType="Numbers" Enabled="True">        
                                            </cc1:FilteredTextBoxExtender>  
                                        </td>
                                        <td style="width:20%; text-align:left; ">
                                            <asp:Label ID="Lbl_Busqueda_Vehiculo_Numero_Economico" runat="server" Text="Número Económico"  CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width:30%; text-align:left;">
                                            <asp:TextBox ID="Txt_Busqueda_Vehiculo_Numero_Economico" runat="server" Width="97%"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_Numero_Economico" runat="server" TargetControlID="Txt_Busqueda_Vehiculo_Numero_Economico" InvalidChars="<,>,&,',!,"  FilterType="Numbers"  ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ -_" Enabled="True">        
                                            </cc1:FilteredTextBoxExtender>  
                                        </td>
                                    </tr>                                
                                    <tr>
                                        <td style="width:20%; text-align:left; ">
                                            <asp:Label ID="Lbl_Busqueda_Vehiculo_Modelo" runat="server" Text="Modelo" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width:30%; text-align:left;">
                                            <asp:TextBox ID="Txt_Busqueda_Vehiculo_Modelo" runat="server" Width="97%"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_Modelo" runat="server" TargetControlID="Txt_Busqueda_Vehiculo_Modelo"
                                                InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ -_" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>                               
                                        </td>
                                        <td style="width:20%; text-align:left; ">
                                            <asp:Label ID="Lbl_Busqueda_Vehiculo_Marca" runat="server" Text="Marca" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width:30%; text-align:left;">
                                            <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Marca" runat="server"  Width="100%">
                                                <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>            
                                            </asp:DropDownList>                             
                                        </td>
                                    </tr>        
                                    <tr>
                                        <td style="width:20%; text-align:left; ">
                                            <asp:Label ID="Lbl_Busqueda_Vehiculo_Anio_Fabricacion" runat="server" Text="Año Fabricación" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width:30%; text-align:left;">
                                            <asp:TextBox ID="Txt_Busqueda_Vehiculo_Anio_Fabricacion" runat="server" Width="97%" MaxLength="4"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_Anio_Fabricacion" runat="server" TargetControlID="Txt_Busqueda_Vehiculo_Anio_Fabricacion" FilterType="Numbers" Enabled="True">        
                                            </cc1:FilteredTextBoxExtender>  
                                        </td>
                                        <td style="width:20%; text-align:left; ">
                                            <asp:Label ID="Lbl_Busqueda_Vehiculo_Color" runat="server" Text="Color" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width:30%; text-align:left;">
                                            <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Color" runat="server" Width="100%">
                                                <asp:ListItem Text="&lt; TODOS &gt;" Value="TODOS"></asp:ListItem>
                                            </asp:DropDownList>                                                 
                                        </td>
                                    </tr>         
                                    <tr>
                                        <td style="width:20%; text-align:left;">
                                            <asp:Label ID="Lbl_Busqueda_Vehiculo_Dependencias" runat="server" Text="U. Responsable" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width:80%; text-align:left;" colspan="3">
                                            <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Dependencias" runat="server" Width="85%">
                                                <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>
                                            </asp:DropDownList>                                                 
                                            <asp:ImageButton ID="Btn_Buscar_Datos_Vehiculo" runat="server" 
                                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" CausesValidation="False" 
                                                OnClick="Btn_Buscar_Datos_Vehiculo_Click"
                                                ToolTip="Buscar Contrarecibos"/>
                                            <asp:ImageButton ID="Btn_Limpiar_Filtros_Buscar_Datos_Vehiculo" runat="server" 
                                                OnClick="Btn_Limpiar_Filtros_Buscar_Datos_Vehiculo_Click"
                                                CausesValidation="False" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png" 
                                                ToolTip="Limpiar Filtros" Width="20px" />                                      
                                        </td>
                                    </tr>                                     
                                </table>
                            </div>                                 
                        </ContentTemplate>
                    </cc1:TabPanel>   
                    
                    <cc1:TabPanel  runat="server" HeaderText="Tab_Panel_Reguardantes_Busqueda_Vehiculo" ID="Tab_Panel_Resguardantes_Busqueda_Vehiculo" Width="100%" BackColor="White" Height="150px">
                        <HeaderTemplate>Resguardantes</HeaderTemplate>
                        <ContentTemplate>    
                            <div style="border-style:outset; width:99.5%; height:150px; background-color:White;" >
                                <table width="100%">
                                    <tr>
                                        <td style="text-align:left;" colspan="2">&nbsp;</td>
                                    </tr>                                 
                                    <tr>
                                        <td style="width:20%; text-align:left; ">
                                            <asp:Label ID="Lbl_Busqueda_Vehiculo_RFC_Resguardante" runat="server" Text="RFC Reguardante" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width:30%; text-align:left;">
                                            <asp:TextBox ID="Txt_Busqueda_Vehiculo_RFC_Resguardante" runat="server" Width="200px"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_RFC_Resguardante" runat="server" TargetControlID="Txt_Busqueda_Vehiculo_RFC_Resguardante" InvalidChars="<,>,&,',!,"  FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ " Enabled="True">        
                                            </cc1:FilteredTextBoxExtender>  
                                        </td>
                                        <td style="width:20%; text-align:left; ">
                                            <asp:Label ID="Lbl_Busqueda_Vehiculo_No_Empleado" runat="server" Text="No. Empleado" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width:30%; text-align:left;">
                                            <asp:TextBox ID="Txt_Busqueda_Vehiculo_No_Empleado" runat="server" Width="97%"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_No_Empleado" runat="server" TargetControlID="Txt_Busqueda_Vehiculo_No_Empleado" InvalidChars="<,>,&,',!,"  FilterType="Numbers" Enabled="True">        
                                            </cc1:FilteredTextBoxExtender>  
                                        </td>
                                    </tr>                          
                                    <tr>
                                        <td style="width:20%; text-align:left; ">
                                            <asp:Label ID="Lbl_Busqueda_Vehiculo_Resguardantes_Dependencias" runat="server" Text="U. Responsable" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td colspan="3" style="text-align:left;">
                                            <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias" runat="server" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias_SelectedIndexChanged" >
                                                <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>   
                                            </asp:DropDownList>                                   
                                        </td>
                                    </tr>                          
                                    <tr>
                                        <td style="width:20%; text-align:left; ">
                                            <asp:Label ID="Lbl_Busqueda_Vehiculo_Nombre_Resguardante" runat="server" Text="Resguardante" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td colspan="3" style="text-align:left;" colspan="3">
                                            <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Nombre_Resguardante" runat="server" Width="100%" >
                                                <asp:ListItem Text="&lt;TODOS&gt;" Value="TODOS"></asp:ListItem>
                                            </asp:DropDownList>     
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td colspan="4" style="text-align:right">
                                            <asp:ImageButton ID="Btn_Buscar_Resguardante_Vehiculo" runat="server" 
                                                CausesValidation="False" ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                                OnClick="Btn_Buscar_Resguardante_Vehiculo_Click"
                                                ToolTip="Buscar Listados"/>                                      
                                            <asp:ImageButton ID="Btn_Limpiar_Filtros_Buscar_Resguardante_Vehiculo" runat="server"  
                                                CausesValidation="False"  OnClick="Btn_Limpiar_Filtros_Buscar_Resguardante_Vehiculo_Click"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png" Width="20px"  
                                                ToolTip="Limpiar Filtros" />   
                                            &nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>                                      
                                </table>
                            </div>
                        </ContentTemplate>
                    </cc1:TabPanel>
                </cc1:TabContainer>  
                <div style="width:99%; height:150px; overflow:auto; border-style:outset; background-color:White;">
                    <center>
                        <caption>
                            <asp:GridView ID="Grid_Listado_Busqueda_Vehiculo" runat="server" AllowPaging="true"
                                AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None"
                                OnPageIndexChanging="Grid_Listado_Busqueda_Vehiculo_PageIndexChanging"
                                OnSelectedIndexChanged="Grid_Listado_Busqueda_Vehiculo_SelectedIndexChanged"
                                Width="98%" PageSize="100" >
                                <RowStyle CssClass="GridItem" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                                <Columns>
                                    <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                        ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                        <ItemStyle Width="30px" />
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="VEHICULO_ID" HeaderText="VEHICULO_ID" SortExpression="VEHICULO_ID" />
                                    <asp:BoundField DataField="NUMERO_INVENTARIO" HeaderText="No. Inven." SortExpression="NUMERO_INVENTARIO" >
                                        <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NUMERO_ECONOMICO" HeaderText="No. Eco." SortExpression="NUMERO_ECONOMICO" >
                                        <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="VEHICULO" HeaderText="Vehículo" SortExpression="VEHICULO" >
                                        <ItemStyle Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MARCA" HeaderText="Marca" SortExpression="MARCA" >
                                        <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MODELO" HeaderText="Modelo" SortExpression="MODELO">
                                        <ItemStyle Width="150px" Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ANIO" HeaderText="Año" SortExpression="ANIO">
                                        <ItemStyle Width="70px" Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS" >
                                        <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                </Columns>
                                <HeaderStyle CssClass="GridHeader" />
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                            </asp:GridView>
                        </caption>
                    </center>   
                </div>                                                  
                </ContentTemplate>
            </asp:UpdatePanel>
        </center>
    </asp:Panel>    
</asp:Content>

