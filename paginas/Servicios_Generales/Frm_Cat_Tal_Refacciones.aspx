﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Cat_Tal_Refacciones.aspx.cs" Inherits="paginas_Taller_Municipal_Frm_Cat_Tal_Refacciones" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">

    <script type="text/javascript">
        window.onerror = new Function("return true");
        //Abrir una ventana modal
        function Abrir_Ventana_Modal(Url, Propiedades) {
            window.showModalDialog(Url, null, Propiedades);
        }
        function Validar_Longitud_Texto(Text_Box, Max_Longitud) {
            if (Text_Box.value.length > Max_Longitud) {
                Text_Box.value = Text_Box.value.substring(0, Max_Longitud);
            }
        }
    </script>

    <script type="text/javascript" language="javascript">
    //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_sesiones.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion()
        {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para matener la sesión activa
        setInterval('MantenSesion()', <%=(int)(0.9*(Session.Timeout * 60000))%>);

        function formatCurrency(num) {
            var Combo = "";
            var Beneficio;
                num = num.toString().replace(/\$|\,/g,'');
                if(isNaN(num))
                num = "0";
                sign = (num == (num = Math.abs(num)));
                num = Math.floor(num*100+0.50000000001);
                cents = num%100;
                num = Math.floor(num/100).toString();
                if(cents<10)
                    cents = "0" + cents;
                    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
                    num = num.substring(0,num.length-(4*i+3))+','+
                    num.substring(num.length-(4*i+3));
                    return (((sign)?'':'-') + num + '.' + cents);
            }
        
            function Escribir_Nombre(){
                var Nombre = document.getElementById("<%=Txt_Nombre.ClientID %>").value;
                var Descripcion = document.getElementById("<%=Txt_Descripcion.ClientID %>").value;
                if(Descripcion.length==0){
                    document.getElementById("<%=Txt_Descripcion.ClientID %>").value=Nombre;
                }
                return false;
            }
        
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" EnablePartialRendering="true" AsyncPostBackTimeout="9000">
    </cc1:ToolkitScriptManager>
    <%--Inicio formulario--%>
    <asp:UpdatePanel ID="Upd_Parametros_Predial" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--update progrees--%>
            <asp:UpdateProgress ID="Uprg_Progress" runat="server" AssociatedUpdatePanelID="Upd_Parametros_Predial"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Refacciones" style="background-color: #ffffff; width: 100%; height: 100%">
                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td colspan="4" class="label_titulo">
                            Catálogo de Refacciones
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" />
                            <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" CssClass="estilo_fuente_mensaje_error"
                                Text="" /><br />
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
                    <tr class="barra_busqueda">
                        <td colspan="2" align="left" style="width: 20%">
                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png"
                                CssClass="Img_Button" OnClick="Btn_Nuevo_Click" />
                            <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                CssClass="Img_Button" OnClick="Btn_Modificar_Click" />
                            <asp:ImageButton ID="Btn_Eliminar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png"
                                CssClass="Img_Button" OnClick="Btn_Eliminar_Click" />
                            <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                CssClass="Img_Button" OnClick="Btn_Salir_Click" />
                        </td>
                        <td colspan="2" align="right" valign="top" style="width: 80%">
                            <table style="width: 80%;">
                                <tr>
                                    <td style="vertical-align: top; text-align: right; width: 5%">
                                    </td>
                                    <td style="vertical-align: top; text-align: right; width: 90%">
                                        Búsqueda:
                                        <asp:TextBox ID="Txt_Buscar" runat="server" MaxLength="300" ToolTip="Buscar" Width="180px" />
                                        <cc1:TextBoxWatermarkExtender ID="WTE_Txt_Buscar" runat="server" WatermarkCssClass="watermarked"
                                            WatermarkText="<- Clave ó Nombre ->" TargetControlID="Txt_Buscar" />
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Buscar" runat="server" TargetControlID="Txt_Buscar"
                                            FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" ValidChars=" " />
                                    </td>
                                    <td style="vertical-align: top; text-align: right; width: 5%">
                                        <asp:ImageButton ID="Btn_Buscar" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                            ToolTip="Buscar" OnClick="Btn_Buscar_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                &nbsp;
                <br />
                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td style="width: 18%">
                            Clave
                        </td>
                        <td style="width: 32%">
                            <asp:TextBox ID="Txt_Clave" Enabled="false" runat="server" Width="92%" Text="" MaxLength="20"
                                Style="text-transform: uppercase"></asp:TextBox>
                        </td>
                        <td colspan="2">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%">
                            Nombre
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Nombre" runat="server" Width="97%" Text="" MaxLength="50" Style="text-transform: uppercase"
                                onblur="javascript:return Escribir_Nombre();"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="Txt_Nombre_FilteredTextBoxExtender" runat="server"
                                Enabled="True" TargetControlID="Txt_Nombre" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                ValidChars="´+*][{}-_/!#%&=?¡¿Ññ.,:;()áéíóúÁÉÍÓÚ ">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">
                            <asp:Label ID="Lbl_Clasificacion" runat="server" Text="Clasificación"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="Cmb_Clasificacion" runat="server" Width="98%">
                            </asp:DropDownList>
                        </td>
                        <tr>
                            <td style="width: 18%">
                                Tipo
                            </td>
                            <td style="width: 32%">
                                <asp:DropDownList ID="Cmb_Tipo" runat="server" Width="94%">
                                    <asp:ListItem Text="&lt;-- SELECCIONE --&gt;" Value="SELECCIONE" />
                                    <asp:ListItem Text="STOCK" Value="STOCK" />
                                    <asp:ListItem Text="TRANSITORIO" Value="TRANSITORIO" />
                                </asp:DropDownList>
                            </td>
                            <td style="width: 18%">
                                Estatus
                            </td>
                            <td style="width: 32%">
                                <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="94%">
                                    <asp:ListItem Text="&lt;-- SELECCIONE --&gt;" Value="SELECCIONE" />
                                    <asp:ListItem Text="VIGENTE" Value="VIGENTE" />
                                    <asp:ListItem Text="BAJA" Value="BAJA" />
                                </asp:DropDownList>
                            </td>
                            <tr>
                                <td style="width: 18%">
                                    Descripcion
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="Txt_Descripcion" runat="server" Height="60px" Style="text-transform: uppercase"
                                        TextMode="MultiLine" Width="97%"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="Txt_Comentarios_FilteredTextBoxExtender" runat="server"
                                        FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" TargetControlID="Txt_Descripcion"
                                        ValidChars="´+*][{}-_/!#%&amp;=?¡¿Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                    </cc1:FilteredTextBoxExtender>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    <asp:Label ID="Lbl_Capitulo" runat="server" Text="Capítulo"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="Cmb_Capitulo" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Cmb_Capitulo_SelectedIndexChanged"
                                        Width="98%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 15%;">
                                    <asp:Label ID="Lbl_Concepto" runat="server" Text="Concepto"></asp:Label>
                                </td>
                                <td colspan="3" style="text-align: left; width: 30%;">
                                    <asp:DropDownList ID="Cmb_Conceptos" runat="server" AutoPostBack="true" OnSelectedIndexChanged="Cmb_Conceptos_SelectedIndexChanged"
                                        Width="98%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    <asp:Label ID="Lbl_Partida_General" runat="server" Text="Partida Generica"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="Cmb_Partida_General" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Cmb_Partida_General_SelectedIndexChanged"
                                        Width="98%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    <asp:Label ID="Lbl_Partida_Especifica" runat="server" Text="Partida Especifica"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="Cmb_Partida_Especifica" runat="server" Width="98%">
                                    </asp:DropDownList>
                                </td>
                                <tr>
                                    <td style="width: 18%">
                                        Costo unitario
                                    </td>
                                    <td style="width: 32%">
                                        <asp:TextBox ID="Txt_Costo_Unitario" runat="server" MaxLength="50" onBlur="this.value=formatCurrency(this.value);"
                                            Text="" Width="92%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="Txt_Costo_Unitario_FilteredTextBoxExtender" runat="server"
                                            Enabled="True" FilterType="Custom,Numbers" TargetControlID="Txt_Costo_Unitario"
                                            ValidChars=".,">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td style="width: 18%">
                                        Unidad
                                    </td>
                                    <td style="width: 32%">
                                        <asp:DropDownList ID="Cmb_Unidad" runat="server" Width="94%">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        &nbsp;
                                    </td>
                                </tr>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div id="Div_Datos_Especificos" runat="server" style="background-color: #ffffff;
                                        width: 100%; height: 100%;">
                                        <table border="0" cellspacing="0" class="estilo_fuente" width="99%">
                                            <tr>
                                                <td colspan="4">
                                                    <hr id="Hr3" runat="server" onclick="return Hr3_onclick() " style="width: 99%; text-align: left;" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 18%">
                                                    Impuesto
                                                </td>
                                                <td style="text-align: left; width: 32%;">
                                                    <asp:DropDownList ID="Cmb_Impuesto" runat="server" AutoPostBack="true" OnSelectedIndexChanged="Cmb_Impuesto_SelectedIndexChanged"
                                                        Style="width: 98%;">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="text-align: left; width: 18%;">
                                                    &nbsp;&nbsp; Impuesto 2
                                                </td>
                                                <td style="text-align: left; width: 32%;">
                                                    <asp:DropDownList ID="Cmb_Impuesto_2" runat="server" Style="width: 98%;">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <!-- Campos existencia de producto -->
                                            <tr visible="false">
                                                <td style="width: 18%">
                                                    <asp:Label ID="Lbl_Txt_Existencia" runat="server" AssociatedControlID="Txt_Existencia"
                                                        Text="Existencia"></asp:Label>
                                                </td>
                                                <td style="text-align: left; width: 32%;">
                                                    <asp:TextBox ID="Txt_Existencia" runat="server" AutoPostBack="true" MaxLength="15"
                                                        OnTextChanged="Txt_Existencia_TextChanged" Style="width: 96%;" />
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Existencia" runat="server" FilterType="Numbers"
                                                        TargetControlID="Txt_Existencia" />
                                                </td>
                                                <td style="text-align: left; width: 18%;">
                                                    &nbsp;&nbsp;
                                                    <asp:Label ID="Lbl_Txt_Maximo" runat="server" AssociatedControlID="Txt_Maximo" Text="Máximo"></asp:Label>
                                                </td>
                                                <td style="text-align: left; width: 32%;">
                                                    <asp:TextBox ID="Txt_Maximo" runat="server" AutoPostBack="true" MaxLength="15" OnTextChanged="Txt_Maximo_TextChanged"
                                                        Style="width: 96%;" />
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Maximo" runat="server" FilterType="Numbers"
                                                        TargetControlID="Txt_Maximo" />
                                                </td>
                                            </tr>
                                            <tr visible="false">
                                                <td style="text-align: left; width: 18%;">
                                                    <asp:Label ID="Lbl_Txt_Comprometido" runat="server" AssociatedControlID="Txt_Comprometido"
                                                        Text="Comprometido"></asp:Label>
                                                </td>
                                                <td style="text-align: left; width: 32%;">
                                                    <asp:TextBox ID="Txt_Comprometido" runat="server" BackColor="#F3F3F3" Enabled="false"
                                                        MaxLength="22" Style="width: 96%;" />
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Comprometido" runat="server" FilterType="Numbers"
                                                        TargetControlID="Txt_Comprometido" />
                                                </td>
                                                <td style="text-align: left; width: 18%;">
                                                    &nbsp;&nbsp;
                                                    <asp:Label ID="Lbl_Txt_Minimo" runat="server" AssociatedControlID="Txt_Minimo" Text="Mínimo"></asp:Label>
                                                </td>
                                                <td style="text-align: left; width: 32%;">
                                                    <asp:TextBox ID="Txt_Minimo" runat="server" AutoPostBack="true" MaxLength="15" OnTextChanged="Txt_Minimo_TextChanged"
                                                        Style="width: 96%;" />
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Minimo" runat="server" FilterType="Numbers"
                                                        TargetControlID="Txt_Minimo" />
                                                </td>
                                            </tr>
                                            <tr visible="false">
                                                <td style="text-align: left; width: 18%;">
                                                    <asp:Label ID="Lbl_Txt_Disponible" runat="server" AssociatedControlID="Txt_Disponible"
                                                        Text="Disponible"></asp:Label>
                                                </td>
                                                <td style="text-align: left; width: 32%;">
                                                    <asp:TextBox ID="Txt_Disponible" runat="server" BackColor="#F3F3F3" Enabled="false"
                                                        ReadOnly="true" Style="width: 96%;" />
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Disponible" runat="server" FilterType="Numbers"
                                                        TargetControlID="Txt_Disponible" />
                                                </td>
                                                <td style="text-align: left; width: 18%;">
                                                    &nbsp;&nbsp;
                                                    <asp:Label ID="Lbl_Txt_Reorden" runat="server" AssociatedControlID="Txt_Reorden"
                                                        Text="Reorden"></asp:Label>
                                                </td>
                                                <td style="text-align: left; width: 32%;">
                                                    <asp:TextBox ID="Txt_Reorden" runat="server" BackColor="#F3F3F3" Enabled="false"
                                                        MaxLength="10" ReadOnly="true" Style="width: 96%;" />
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Reorden" runat="server" FilterType="Numbers"
                                                        TargetControlID="Txt_Reorden" />
                                                </td>
                                            </tr>
                                            <!-- Campo Ubicacion de producto -->
                                            <tr visible="false">
                                                <td style="text-align: left; width: ">
                                                    <asp:Label ID="Lbl_Txt_Ubicacion" runat="server" AssociatedControlID="Txt_Ubicacion"
                                                        Text="Ubicación"></asp:Label>
                                                </td>
                                                <td colspan="3" style="text-align: left; width: 30%;">
                                                    <asp:TextBox ID="Txt_Ubicacion" runat="server" MaxLength="255" Style="width: 98.5%;" />
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Ubicacion" runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                                        TargetControlID="Txt_Ubicacion" ValidChars="ÑñáéíóúÁÉÍÓÚ. " />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4">
                                    <asp:GridView ID="Grid_Refacciones" runat="server" AllowPaging="true" AutoGenerateColumns="False"
                                        CssClass="GridView_1" DataKeyNames="REFACCION_ID" GridLines="none" OnPageIndexChanging="Grid_Refacciones_PageIndexChanging"
                                        OnSelectedIndexChanged="Grid_Refaccion_SelectedIndexChanged" PageSize="10" Style="white-space: normal"
                                        Width="96%">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                <ItemStyle Width="5%" />
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="REFACCION_ID" HeaderText="Id" Visible="false">
                                                <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                                <ItemStyle HorizontalAlign="Left" Width="5%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CLAVE" HeaderText="Clave">
                                                <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NOMBRE" HeaderText="Nombre">
                                                <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ESTATUS" HeaderText="Estatus">
                                                <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripción">
                                                <HeaderStyle HorizontalAlign="Left" Width="40%" />
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="40%" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 18%">
                                    &nbsp;
                                </td>
                                <td style="width: 32%">
                                    &nbsp;
                                </td>
                                <td style="width: 18%">
                                    &nbsp;
                                </td>
                                <td style="width: 32%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 18%">
                                    &nbsp;
                                </td>
                                <td style="width: 32%">
                                    &nbsp;
                                </td>
                                <td style="width: 18%">
                                    &nbsp;
                                </td>
                                <td style="width: 32%">
                                    &nbsp;
                                </td>
                            </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
