<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Rpt_Tal_Bitacora_Vehiculo.aspx.cs" Inherits="paginas_Taller_Mecanico_Frm_Rpt_Tal_Bitacora_Vehiculo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" language="javascript">
        function Limpiar_Ctlr_Busqueda(){
            document.getElementById("<%=Txt_Busqueda_Vehiculo_Numero_Inventario.ClientID%>").value = "";
            document.getElementById("<%=Cmb_Busqueda_Vehiculo_Dependencias.ClientID%>").value = "";                               
            return false;
        } 
    </script>  
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScptM_Operacion" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" />  
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>         
            <asp:UpdateProgress ID="Uprg_Loading" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>                     
            </asp:UpdateProgress>
            <div id="Div_Area_Trabajo" style="background-color:#ffffff; width:100%; height:100%;">
                <center>
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo" colspan="2">Bitacora de Veh�culo</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                          <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;">              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top">
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                            </table>                   
                          </div>                
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>                        
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" style="width:50%;">
                            <asp:ImageButton ID="Btn_Reporte_Execel" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png" Width="24px" 
                                CssClass="Img_Button" AlternateText="Imprimir" 
                                ToolTip="Imprimir Reprte en Execel" onclick="Btn_Reporte_Execel_Click" />
                            <asp:ImageButton ID="Btn_Reporte_PDF" runat="server"
                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" Width="24px" 
                                CssClass="Img_Button" AlternateText="Salir"
                                ToolTip="Imprimir Reprte en PDF" onclick="Btn_Reporte_PDF_Click" />
                        </td>
                        <td align="right" style="width:50%;">
                           <asp:ImageButton ID="Btn_Limpiar" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png" Width="24px" 
                                CssClass="Img_Button" AlternateText="Limpiar"
                                ToolTip="Limpiar Campos" onclick="Btn_Limpiar_Click" />                           
                        </td>                        
                    </tr>
                </table>
                </center>
            </div>
            <div id="Div_Campos" runat="server" style="width:100%;">                
                <table width="99%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td >
                            <asp:HiddenField ID="Hdf_Vehiculo_ID" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Pnl_Vehiculo_Seleccionado" runat="server" Width="99%" GroupingText="Datos del Veh�culo">
                            <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                                <tr>
                                    <td style="width:15%;">
                                        <asp:Label ID="Lbl_No_Inventario" runat="server" Text="No. Inventario"></asp:Label>
                                    </td>
                                    <td style="width:35%;">
                                        <asp:TextBox ID="Txt_No_Inventario" runat="server" Width="70%" MaxLength="7"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Inventario" runat="server" TargetControlID="Txt_No_Inventario" FilterType="Numbers">
                                        </cc1:FilteredTextBoxExtender>
                                        <asp:ImageButton ID="Btn_Busqueda_Directa_Vehiculo" runat="server" Width="24px"
                                            ImageUrl="~/paginas/imagenes/paginas/busqueda.png" AlternateText="Buscar" 
                                            ToolTip="Buscar Veh�culo" onclick="Btn_Busqueda_Directa_Vehiculo_Click" />
                                        <asp:ImageButton ID="Btn_Busqueda_Avanzada_Vehiculo" runat="server" Width="24px"
                                            ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png" AlternateText="Buscar" 
                                            ToolTip="Buscar" onclick="Btn_Busqueda_Avanzada_Vehiculo_Click" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Lbl_No_Economico" runat="server" Text="No. Economico"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_No_Economico" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text="Unidad Responsable"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="Txt_Unidad_Responsable" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Lbl_Datos_Vehiculo" runat="server" Text="Datos Vehiculo"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="Txt_Datos_Vehiculo" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:15%;">
                                        <asp:Label ID="Lbl_Placas" runat="server" Text="Placas"></asp:Label>
                                    </td>
                                    <td style="width:35%;">
                                        <asp:TextBox ID="Txt_Placas" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td style="width:15%;">
                                        &nbsp;&nbsp;  
                                        <asp:Label ID="Lbl_Anio" runat="server" Text="A�o"></asp:Label>
                                    </td>
                                    <td style="width:35%;">
                                        <asp:TextBox ID="Txt_Anio" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr style="height:10px;">
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="Grid_Listado_Solicitudes" runat="server" CssClass="GridView_1"
                    AutoGenerateColumns="False" AllowPaging="True" PageSize="10" Width="99%"
                    GridLines= "None" OnPageIndexChanging="Grid_Listado_Solicitudes_PageIndexChanging" >
                    <RowStyle CssClass="GridItem" />
                    <Columns>
                        <asp:BoundField DataField="NO_SOLICITUD" HeaderText="No. Solicitud" SortExpression="NO_SOLICITUD" NullDisplayText="-" >
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DESCRIPCION_SERVICIO" HeaderText="Descripccion" SortExpression="DESCRIPCION_SERVICIO" NullDisplayText="-" >
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FECHA_ELABORACION" HeaderText="Fecha de Elaboraci�n" SortExpression="FECHA_ELABORACION" NullDisplayText="-" DataFormatString="{0:dd/MMM/yyyy}" >
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FECHA_RECEPCION_REAL" HeaderText="Fecha de Recepci�n" SortExpression="FECHA_RECEPCION_REAL" NullDisplayText="-" DataFormatString="{0:dd/MMM/yyyy}" >
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS" NullDisplayText="-" >
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                    <PagerStyle CssClass="GridHeader" />
                    <SelectedRowStyle CssClass="GridSelected" />
                    <HeaderStyle CssClass="GridHeader" />                                
                    <AlternatingRowStyle CssClass="GridAltItem" />       
                </asp:GridView>
                <table>
                    <tr style="height:10px;">
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="Grid_Listado_Revista" runat="server" CssClass="GridView_1"
                    AutoGenerateColumns="False" AllowPaging="True" PageSize="10" Width="99%"
                    GridLines= "None" OnPageIndexChanging="Grid_Listado_Revista_PageIndexChanging"  >
                    <RowStyle CssClass="GridItem" />
                    <Columns>
                        <asp:BoundField DataField="NO_SOLICITUD" HeaderText="No. Solicitud" SortExpression="NO_SOLICITUD" NullDisplayText="-" >
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DESCRIPCION_SERVICIO" HeaderText="Descripccion" SortExpression="DESCRIPCION_SERVICIO" NullDisplayText="-" >
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FECHA_ELABORACION" HeaderText="Fecha de Elaboraci�n" SortExpression="FECHA_ELABORACION" NullDisplayText="-" DataFormatString="{0:dd/MMM/yyyy}" >
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FECHA_RECEPCION_REAL" HeaderText="Fecha de Recepci�n" SortExpression="FECHA_RECEPCION_REAL" NullDisplayText="-" DataFormatString="{0:dd/MMM/yyyy}" >
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS" NullDisplayText="-" >
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                    <PagerStyle CssClass="GridHeader" />
                    <SelectedRowStyle CssClass="GridSelected" />
                    <HeaderStyle CssClass="GridHeader" />                                
                    <AlternatingRowStyle CssClass="GridAltItem" />       
                </asp:GridView>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Btn_Reporte_Execel" />
        </Triggers>
    </asp:UpdatePanel>
    
    <asp:UpdatePanel ID="Aux_MPE_Listado_Vehiculos" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_MPE_Listado_Vehiculos" runat="server" Text="Button" style="display:none;" />
            <cc1:ModalPopupExtender ID="MPE_Listado_Vehiculos" runat="server"
                TargetControlID="Btn_Comodin_MPE_Listado_Vehiculos" PopupControlID="Pnl_Busqueda_Vehiculo"                 
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter" >
            </cc1:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:Panel ID="Pnl_Busqueda_Vehiculo" runat="server" CssClass="drag" HorizontalAlign="Center" style="display:none;border-style:outset;border-color:Silver;width:760px;" >
        <center>
            <asp:Panel ID="Pnl_Busqueda_Vehiculo_Interno" runat="server" CssClass="estilo_fuente" style="cursor: move;background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
                <table class="estilo_fuente" width="100%">
                    <tr>
                        <td style="color:Black;font-size:12;font-weight:bold;width:90%">
                           <asp:Image ID="Img_Encabezado_Busqueda_Vehiculos" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                             Busqueda de Veh�culos
                        </td>
                        <td align="right">
                           <asp:ImageButton ID="Btn_Cerrar_Busqueda_Vehiculo" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png"/>  
                        </td>
                    </tr>
                </table>   
            </asp:Panel>
            <asp:UpdatePanel ID="UpPnl_Busqueda_Vehiculo" runat="server"> 
                <ContentTemplate>
                <asp:UpdateProgress ID="UpPrg_Busqueda_Vehiculo" runat="server" AssociatedUpdatePanelID="UpPnl_Busqueda_Vehiculo" DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                        <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                    </ProgressTemplate>                     
                </asp:UpdateProgress>
                <br />
                <cc1:TabContainer ID="Tab_Contenedor_Pestagnas_Busqueda_Vehiculo" runat="server" Width="100%" ActiveTabIndex="0" CssClass="Tab">
                    <cc1:TabPanel  runat="server" HeaderText="Tab_Panel_Datos_Generales_Busqueda_Vehiculo" ID="Tab_Panel_Datos_Generales_Busqueda_Vehiculo" Width="100%" Height="400px" BackColor="White">
                        <HeaderTemplate>Generales</HeaderTemplate>
                            <ContentTemplate>
                            <div style="border-style:outset; width:99.5%; height:200px; background-color:White;" >
                                <table width="100%">
                                    <tr>
                                        <td style="text-align:left;" colspan="4">&nbsp;</td>
                                    </tr>                                 
                                    <tr>
                                        <td style="width:20%; text-align:left; ">
                                            <asp:Label ID="Lbl_Busqueda_Vehiculo_Numero_Inventario" runat="server" Text="N�mero Inventario" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width:30%; text-align:left;">
                                            <asp:TextBox ID="Txt_Busqueda_Vehiculo_Numero_Inventario" runat="server" Width="97%" ></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_Numero_Inventario" runat="server" TargetControlID="Txt_Busqueda_Vehiculo_Numero_Inventario" FilterType="Numbers" Enabled="True">        
                                            </cc1:FilteredTextBoxExtender>  
                                        </td>
                                        <td style="width:20%; text-align:left; ">
                                            <asp:Label ID="Lbl_Busqueda_Vehiculo_Numero_Economico" runat="server" Text="N�mero Econ�mico"  CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width:30%; text-align:left;">
                                            <asp:TextBox ID="Txt_Busqueda_Vehiculo_Numero_Economico" runat="server" Width="97%"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_Numero_Economico" runat="server" TargetControlID="Txt_Busqueda_Vehiculo_Numero_Economico" InvalidChars="<,>,&,',!,"  FilterType="Numbers"  ValidChars="��.,:;()���������� -_" Enabled="True">        
                                            </cc1:FilteredTextBoxExtender>  
                                        </td>
                                    </tr>                                
                                    <tr>
                                        <td style="width:20%; text-align:left; ">
                                            <asp:Label ID="Lbl_Busqueda_Vehiculo_Modelo" runat="server" Text="Modelo" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width:30%; text-align:left;">
                                            <asp:TextBox ID="Txt_Busqueda_Vehiculo_Modelo" runat="server" Width="97%"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_Modelo" runat="server" TargetControlID="Txt_Busqueda_Vehiculo_Modelo"
                                                InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                ValidChars="��.,:;()���������� -_" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>                               
                                        </td>
                                        <td style="width:20%; text-align:left; ">
                                            <asp:Label ID="Lbl_Busqueda_Vehiculo_Marca" runat="server" Text="Marca" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width:30%; text-align:left;">
                                            <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Marca" runat="server"  Width="100%">
                                                <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>            
                                            </asp:DropDownList>                             
                                        </td>
                                    </tr>        
                                    <tr>
                                        <td style="width:20%; text-align:left; ">
                                            <asp:Label ID="Lbl_Busqueda_Vehiculo_Anio_Fabricacion" runat="server" Text="A�o Fabricaci�n" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width:30%; text-align:left;">
                                            <asp:TextBox ID="Txt_Busqueda_Vehiculo_Anio_Fabricacion" runat="server" Width="97%" MaxLength="4"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_Anio_Fabricacion" runat="server" TargetControlID="Txt_Busqueda_Vehiculo_Anio_Fabricacion" FilterType="Numbers" Enabled="True">        
                                            </cc1:FilteredTextBoxExtender>  
                                        </td>
                                        <td style="width:20%; text-align:left; ">
                                            <asp:Label ID="Lbl_Busqueda_Vehiculo_Color" runat="server" Text="Color" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width:30%; text-align:left;">
                                            <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Color" runat="server" Width="100%">
                                                <asp:ListItem Text="&lt; TODOS &gt;" Value="TODOS"></asp:ListItem>
                                            </asp:DropDownList>                                                 
                                        </td>
                                    </tr>         
                                    <tr>
                                        <td style="width:20%; text-align:left;">
                                            <asp:Label ID="Lbl_Busqueda_Vehiculo_Dependencias" runat="server" Text="U. Responsable" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width:80%; text-align:left;" colspan="3">
                                            <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Dependencias" runat="server" Width="85%">
                                                <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>
                                            </asp:DropDownList>                                                 
                                            <asp:ImageButton ID="Btn_Buscar_Datos_Vehiculo" runat="server" 
                                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" CausesValidation="False" 
                                                OnClick="Btn_Buscar_Datos_Vehiculo_Click"
                                                ToolTip="Buscar Contrarecibos"/>
                                            <asp:ImageButton ID="Btn_Limpiar_Filtros_Buscar_Datos_Vehiculo" runat="server" 
                                                OnClick="Btn_Limpiar_Filtros_Buscar_Datos_Vehiculo_Click"
                                                CausesValidation="False" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png" 
                                                ToolTip="Limpiar Filtros" Width="20px" />                                      
                                        </td>
                                    </tr>                                     
                                </table>
                            </div>                                 
                        </ContentTemplate>
                    </cc1:TabPanel>   
                    
                    <cc1:TabPanel  runat="server" HeaderText="Tab_Panel_Reguardantes_Busqueda_Vehiculo" ID="Tab_Panel_Resguardantes_Busqueda_Vehiculo" Width="100%" BackColor="White">
                        <HeaderTemplate>Resguardantes</HeaderTemplate>
                        <ContentTemplate>    
                            <div style="border-style:outset; width:99.5%; height:200px; background-color:White;" >
                                <table width="100%">
                                    <tr>
                                        <td style="text-align:left;" colspan="2">&nbsp;</td>
                                    </tr>                                 
                                    <tr>
                                        <td style="width:20%; text-align:left; ">
                                            <asp:Label ID="Lbl_Busqueda_Vehiculo_RFC_Resguardante" runat="server" Text="RFC Reguardante" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width:30%; text-align:left;">
                                            <asp:TextBox ID="Txt_Busqueda_Vehiculo_RFC_Resguardante" runat="server" Width="200px"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_RFC_Resguardante" runat="server" TargetControlID="Txt_Busqueda_Vehiculo_RFC_Resguardante" InvalidChars="<,>,&,',!,"  FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="��.,:;()���������� " Enabled="True">        
                                            </cc1:FilteredTextBoxExtender>  
                                        </td>
                                        <td style="width:20%; text-align:left; ">
                                            <asp:Label ID="Lbl_Busqueda_Vehiculo_No_Empleado" runat="server" Text="No. Empleado" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width:30%; text-align:left;">
                                            <asp:TextBox ID="Txt_Busqueda_Vehiculo_No_Empleado" runat="server" Width="97%"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_No_Empleado" runat="server" TargetControlID="Txt_Busqueda_Vehiculo_No_Empleado" InvalidChars="<,>,&,',!,"  FilterType="Numbers" Enabled="True">        
                                            </cc1:FilteredTextBoxExtender>  
                                        </td>
                                    </tr>                          
                                    <tr>
                                        <td style="width:20%; text-align:left; ">
                                            <asp:Label ID="Lbl_Busqueda_Vehiculo_Resguardantes_Dependencias" runat="server" Text="U. Responsable" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td colspan="3" style="text-align:left;">
                                            <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias" runat="server" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias_SelectedIndexChanged" >
                                                <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>   
                                            </asp:DropDownList>                                   
                                        </td>
                                    </tr>                          
                                    <tr>
                                        <td style="width:20%; text-align:left; ">
                                            <asp:Label ID="Lbl_Busqueda_Vehiculo_Nombre_Resguardante" runat="server" Text="Resguardante" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td colspan="3" style="text-align:left;" colspan="3">
                                            <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Nombre_Resguardante" runat="server" Width="100%" >
                                                <asp:ListItem Text="&lt;TODOS&gt;" Value="TODOS"></asp:ListItem>
                                            </asp:DropDownList>     
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td colspan="4" style="text-align:right">
                                            <asp:ImageButton ID="Btn_Buscar_Resguardante_Vehiculo" runat="server" 
                                                CausesValidation="False" ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                                OnClick="Btn_Buscar_Resguardante_Vehiculo_Click"
                                                ToolTip="Buscar Listados"/>                                      
                                            <asp:ImageButton ID="Btn_Limpiar_Filtros_Buscar_Resguardante_Vehiculo" runat="server"  
                                                CausesValidation="False"  OnClick="Btn_Limpiar_Filtros_Buscar_Resguardante_Vehiculo_Click"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png" Width="20px"  
                                                ToolTip="Limpiar Filtros" />   
                                            &nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>                                      
                                </table>
                            </div>
                        </ContentTemplate>
                    </cc1:TabPanel>
                </cc1:TabContainer>  
                <div style="width:99%; height:150px; overflow:auto; border-style:outset; background-color:White;">
                    <center>
                        <caption>
                            <asp:GridView ID="Grid_Listado_Busqueda_Vehiculo" runat="server" AllowPaging="true"
                                AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None"
                                OnPageIndexChanging="Grid_Listado_Busqueda_Vehiculo_PageIndexChanging"
                                OnSelectedIndexChanged="Grid_Listado_Busqueda_Vehiculo_SelectedIndexChanged"
                                Width="98%" PageSize="100" >
                                <RowStyle CssClass="GridItem" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                                <Columns>
                                    <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                        ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                        <ItemStyle Width="30px" />
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="VEHICULO_ID" HeaderText="VEHICULO_ID" SortExpression="VEHICULO_ID" />
                                    <asp:BoundField DataField="NUMERO_INVENTARIO" HeaderText="No. Inven." SortExpression="NUMERO_INVENTARIO" >
                                        <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NUMERO_ECONOMICO" HeaderText="No. Eco." SortExpression="NUMERO_ECONOMICO" >
                                        <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="VEHICULO" HeaderText="Veh�culo" SortExpression="VEHICULO" >
                                        <ItemStyle Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MARCA" HeaderText="Marca" SortExpression="MARCA" >
                                        <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MODELO" HeaderText="Modelo" SortExpression="MODELO">
                                        <ItemStyle Width="150px" Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ANIO" HeaderText="A�o" SortExpression="ANIO">
                                        <ItemStyle Width="70px" Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS" >
                                        <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                </Columns>
                                <HeaderStyle CssClass="GridHeader" />
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                            </asp:GridView>
                        </caption>
                    </center>   
                </div>                                                  
                </ContentTemplate>
            </asp:UpdatePanel>
        </center>
    </asp:Panel>
    
</asp:Content>

