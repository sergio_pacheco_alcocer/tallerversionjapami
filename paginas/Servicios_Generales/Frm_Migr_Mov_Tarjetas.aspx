﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Migr_Mov_Tarjetas.aspx.cs" Inherits="paginas_Servicios_Generales_Mig_Mov_Gas" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">

    <script type="text/javascript">
        function uploadComplete(sender, args) {
            $get("<%=lblMesg.ClientID%>").style.color = "green";            
            $get("<%=lblMesg.ClientID%>").innerHTML = "Archivo correctamente cargado: " + args.get_fileName();
            $get("<%=Hdf_Nombre.ClientID%>").val = args.get_fileName();
        }
        function uploadError(sender, args) {
            $get("<%=lblMesg.ClientID%>").style.color = "red";
            $get("<%=lblMesg.ClientID%>").innerHTML = "File upload failed.";
            $get("<%=Hdf_Nombre.ClientID%>").val = "";
        }
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScptM_Catalogo" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Requisitos" style="background-color: #ffffff; width: 100%; height: 100%;">
                <center>
                    <table width="99%" border="0" cellspacing="0" class="estilo_fuente">
                        <tr align="center">
                            <td class="label_titulo" colspan="2">
                                Carga Masiva de Movimientos para Tarjetas de Combustible
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div id="Div_Contenedor_Msj_Error" style="width: 98%;" runat="server" visible="false">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td colspan="2" align="left">
                                                <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                    Width="24px" Height="24px" />
                                                <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 10%;">
                                            </td>
                                            <td style="width: 90%; text-align: left;" valign="top">
                                                <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left" style="width: 50%;">
                                <asp:ImageButton ID="Btn_Salir_Tarjeta_Gasolina" runat="server" ToolTip="Salir" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                    Width="24px" CssClass="Img_Button" AlternateText="Salir" OnClick="Btn_Salir_Click" />
                            </td>
                            <td align="right" style="width: 50%;">
                                <asp:ImageButton ID="Btn_Limpiar_Formulario" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png"
                                    Width="24px" CssClass="Img_Button" AlternateText="Limpiar Formulario" ToolTip="Limpiar_Formulario"
                                    OnClick="Btn_Limpiar_Formulario_Click" />
                            </td>
                        </tr>
                    </table>
                    <table width="99%" border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                            <td colspan="2" style="width: 50%; text-align: left; border-style: outset;">
                                <b>Descarga de Layout...</b> <a href="" rel="facybox" runat="server" id="Liga_Frente"
                                    target="_blank">
                                    <img src="~/paginas/imagenes/mime/xlsx.ico" id="Img_Bajar_Archivo" alt="" title="Bajar Layout de Carga"
                                        runat="server" width="32" height="32" /></a>
                            </td>
                            <td colspan="2" style="width: 50%; text-align: left; border-style: outset;">
                                <asp:HiddenField ID="Hdf_Nombre" runat="server" />
                                <asp:HiddenField ID="Hdf_Nombre_Guardado" runat="server" />
                                <b>Carga de Movimientos...</b>
                                <cc1:AsyncFileUpload OnClientUploadError="uploadError" OnClientUploadComplete="uploadComplete"
                                    runat="server" ID="Fup_Frente" Width="350px" CompleteBackColor="#CCFFFF" UploadingBackColor="White"
                                    ThrobberID="Lbl_Throbber" OnUploadedComplete="Fup_Frente_UploadedComplete" />
                                <asp:Label ID="Lbl_Throbber" runat="server" Text="Espere" Width="30px">
                                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                    </div>
                                    <div class="processMessage" id="div_progress">
                                        <center>
                                            <img alt="" src="../Imagenes/paginas/Sias_Roler.gif" />
                                            <br />
                                            <br />
                                            <span id="spanUploading" runat="server" style="color: White; font-size: 30px; font-weight: bold;
                                                font-family: Lucida Calligraphy; font-style: italic;">Cargando... </span>
                                        </center>
                                    </div>
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div style="width: 99%; border-style: outset;">
                                    <table width="100%">
                                        <tr style="background-color: #C0C0C0; text-align: right;">
                                            <td>
                                                <asp:ImageButton ID="Btn_Mostrar_Movimientos" runat="server" ImageUrl="~/paginas/imagenes/paginas/Listado.png"
                                                    Width="24px" CssClass="Img_Button" AlternateText="Mostrar Movimientos" ToolTip="Mostrar Movimientos"
                                                    OnClick="Btn_Mostrar_Movimientos_Click" />
                                                &nbsp; &nbsp;
                                                <asp:ImageButton ID="Btn_Guardar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_guardar.png"
                                                    Width="24px" CssClass="Img_Button" AlternateText="Guardar Movimientos" ToolTip="Guardar movimientos"
                                                    OnClick="Btn_Guardar_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <asp:Label ID="lblMesg" runat="server" Text=""></asp:Label>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="width: 99%; height: 450px; overflow: auto; text-align:center;">
                                                    <asp:GridView ID="Grid_Movimientos" runat="server" AllowPaging="False" AutoGenerateColumns="False"
                                                        CssClass="GridView_1" GridLines="None" Width="100%" PageSize="100">
                                                        <RowStyle CssClass="GridItem" />
                                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                                        <Columns>
                                                            <asp:BoundField DataField="NUMERO_TARJETA" HeaderText="No. Tarjeta">
                                                                <HeaderStyle Width="15%" HorizontalAlign="Center" />
                                                                <ItemStyle Width="15%" Font-Size="X-Small" HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="FECHA_MOVIMIENTO" HeaderText="Fecha Movimiento" DataFormatString="{0:dd/MMM/yyyy}">
                                                                <HeaderStyle Width="25%" HorizontalAlign="Center" />
                                                                <ItemStyle Width="25%" Font-Size="X-Small" HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="MONTO" HeaderText="Monto">
                                                                <HeaderStyle Width="15%" HorizontalAlign="Center" />
                                                                <ItemStyle Width="15%" Font-Size="X-Small" HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="COMENTARIOS" HeaderText="Comentarios">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle Font-Size="X-Small" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="GridHeader" />
                                                        <PagerStyle CssClass="GridHeader" />
                                                        <SelectedRowStyle CssClass="GridSelected" />
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <asp:Label ID="Lbl_Total" runat="server"></asp:Label>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </center>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
