﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Seguimiento_Requisiciones_Taller.Negocio;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Areas.Negocios;
using JAPAMI.Administrar_Requisiciones.Negocios;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using JAPAMI.Compras.Impresion_Requisiciones.Negocio;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using JAPAMI.Seguimiento_Requisicion.Negocio;
using JAPAMI.Taller_Mecanico.Impresion_Requisiciones.Negocio;
using JAPAMI.Taller_Mecanico.Reporte_Solicitud_Servicio.Negocio;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;

public partial class paginas_Servicios_Generales_Frm_Ope_Tal_Seguimiento_Requisiciones : System.Web.UI.Page
{

    #region VARIABLES / CONSTANTES

        //objeto de la clase de negocio de dependencias para acceder a la clase de datos y realizar copnexion
        private Cls_Cat_Dependencias_Negocio Dependencia_Negocio;
        //objeto de la clase de negocio de Requisicion para acceder a la clase de datos y realizar copnexion
        private Cls_Ope_Tal_Seguimiento_Requisiciones_Negocio Requisicion_Negocio;
        //objeto en donde se guarda un id de producto o servicio para siempre tener referencia
        private int Contador_Columna;
        private String Informacion;

        private static String P_Dt_Productos_Servicios = "P_Dt_Productos_Servicios_Seguimiento_Compras";
        //private static String P_Dt_Partidas = "P_Dt_Partidas";
        private static String P_Dt_Requisiciones = "P_Dt_Requisiciones_Transitorias_Seguimiento_Compras";
        //private static String P_Dt_Productos_Servicios_Modal = "P_Dt_Productos_Servicios_Modal";

        private const String Operacion_Comprometer = "COMPROMETER";
        private const String Operacion_Descomprometer = "DESCOMPROMETER";
        private const String Operacion_Quitar_Renglon = "QUITAR";
        private const String Operacion_Agregar_Renglon_Nuevo = "AGREGAR_NUEVO";
        private const String Operacion_Agregar_Renglon_Copia = "AGREGAR_COPIA";

        private const String SubFijo_Requisicion = "RQ-";
        private const String EST_EN_CONSTRUCCION = "EN CONSTRUCCION";
        private const String EST_GENERADA = "GENERADA";
        private const String EST_CANCELADA = "CANCELADA";
        private const String EST_REVISAR = "REVISAR";
        private const String EST_RECHAZADA = "RECHAZADA";
        private const String EST_AUTORIZADA = "AUTORIZADA";
        private const String EST_FILTRADA = "FILTRADA";

        private const String TIPO_STOCK = "STOCK";
        private const String TIPO_TRANSITORIA = "TRANSITORIA";

        private const String MODO_LISTADO = "LISTADO";
        private const String MODO_INICIAL = "INICIAL";
        private const String MODO_MODIFICAR = "MODIFICAR";
        private const String MODO_NUEVO = "NUEVO";

    #endregion

    #region PAGE LOAD / INIT

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Page_Load
    ///DESCRIPCIÓN: Carga la Página de Inicio
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 02/Septiembre/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Cls_Sessiones.Nombre_Empleado) || String.IsNullOrEmpty(Cls_Sessiones.No_Empleado)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        if (!IsPostBack)
        {
            ViewState["SortDirection"] = "DESC";
            DateTime _DateTime = DateTime.Now;
            int dias = _DateTime.Day;
            dias = dias * -1;
            dias++;
            _DateTime = _DateTime.AddDays(dias);
            _DateTime = _DateTime.AddMonths(-1);
            Txt_Fecha_Inicial.Text = _DateTime.ToString("dd/MMM/yyyy").ToUpper();
            Txt_Fecha_Final.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
            Dependencia_Negocio = new Cls_Cat_Dependencias_Negocio();
            DataTable Dt_Dependencias = Dependencia_Negocio.Consulta_Dependencias();
            Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Dependencia_Panel, Dt_Dependencias, 1, 0);
            Cmb_Dependencia_Panel.SelectedIndex = 0;
            Cmb_Dependencia_Panel.Enabled = true;
            Llenar_Combos_Busqueda();

            Habilitar_Controles(MODO_LISTADO);
            DataTable Dt_Grupo_Rol = Cls_Util.Consultar_Grupo_Rol_ID(Cls_Sessiones.Rol_ID.ToString());
            if (Dt_Grupo_Rol != null)
            {
                DataTable Dt_URs = Cls_Util.Consultar_URs_De_Empleado(Cls_Sessiones.Empleado_ID);
                if (Dt_URs.Rows.Count > 1)
                {
                    Cmb_Dependencia_Panel.Enabled = true;
                    Cls_Util.Llenar_Combo_Con_DataTable_Generico
                        (Cmb_Dependencia_Panel, Dt_URs, 1, 0);
                    Cmb_Dependencia_Panel.SelectedValue = Cls_Sessiones.Dependencia_ID_Empleado;
                }
            }
            Cmb_Dependencia_Panel.Items[0].Text = "<- TODAS ->";
            Llenar_Grid_Requisiciones();
        }

        Mostrar_Informacion("", false);

    }

    #endregion

    #region METODOS

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo
    ///DESCRIPCIÓN: Llena un Combo con una Lista de Items
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 02/Septiembre/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Llenar_Combo(DropDownList Combo, String[] Items)
    {
        Combo.Items.Clear();
        Combo.Items.Add("<<SELECCIONAR>>");
        foreach (String _Item in Items)
        {
            Combo.Items.Add(_Item);
        }
        Combo.Items[0].Value = "0";
        Combo.Items[0].Selected = true;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
    ///DESCRIPCIÓN: Metodo que permite limpiar controles
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 02/Septiembre/2013 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Limpiar_Formulario()
    {
        Txt_Busqueda.Text = "";
        Txt_Fecha_Final.Text = "";
        Txt_Fecha_Inicial.Text = "";
        DateTime _DateTime = DateTime.Now;
        int dias = _DateTime.Day;
        dias = dias * -1;
        dias++;
        _DateTime = _DateTime.AddDays(dias);
        _DateTime = _DateTime.AddMonths(-1);
        Txt_Fecha_Inicial.Text = _DateTime.ToString("dd/MMM/yyyy").ToUpper();
        Txt_Fecha_Final.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
        Txt_Folio.Text = "";
        Txt_OC.Text = "";
        Txt_Proveedor.Text = "";
        Txt_Total.Text = "";
        Txt_Total_Cotizado.Text = "";
        Txt_Dependencia.Text = "";
        Cmb_Tipo_Busqueda.SelectedIndex = 0;
        Session[P_Dt_Requisiciones] = new DataTable();
        Session["Dt_Contra_Recibo"] = new DataTable();
        Session[P_Dt_Productos_Servicios] = new DataTable();
        Grid_Productos_Servicios.DataSource = new DataTable();
        Grid_Productos_Servicios.DataBind();
        Grid_Comentarios.DataSource = new DataTable();
        Grid_Comentarios.DataBind();
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCIÓN: Obtener_UR_Empleado
    /// DESCRIPCIÓN: Obtiene la UR del Empleado
    /// RETORNA: 
    /// CREO: Francisco Antonio Gallardo Castañeda
    /// FECHA_CREO: 02/Septiembre/2013 
    /// MODIFICO:
    /// FECHA_MODIFICO
    /// CAUSA_MODIFICACIÓN   
    /// *******************************************************************************/
    private String Obtener_UR_Empleado()
    {
        String UR = "";
        DataTable Dt_URs = Cls_Util.Consultar_URs_De_Empleado(Cls_Sessiones.Empleado_ID);
        foreach (DataRow Fila in Dt_URs.Rows)
        {
            UR += Fila[0].ToString().Trim() + "','";
        }
        return UR.TrimEnd('\'').TrimEnd(',').TrimEnd('\'');
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCIÓN: Habilitar_Controles
    /// DESCRIPCIÓN: Habilita la configuracion de acuerdo a la operacion     
    /// RETORNA: 
    /// CREO: Francisco Antonio Gallardo Castañeda
    /// FECHA_CREO: 02/Septiembre/2013 
    /// MODIFICO:
    /// FECHA_MODIFICO
    /// CAUSA_MODIFICACIÓN   
    /// *******************************************************************************/
    private void Habilitar_Controles(String Modo)
    {
        try
        {
            switch (Modo)
            {
                case MODO_LISTADO:

                    Btn_Nuevo.Visible = false;
                    Btn_Modificar.Visible = false;
                    Btn_Eliminar.Visible = false;
                    Btn_Salir.Visible = true;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Eliminar.ToolTip = "Eliminar";
                    Btn_Salir.ToolTip = "Inicio";
                    Btn_Listar_Requisiciones.Visible = false;
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar.png";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";

                    break;

                case MODO_INICIAL:
                    Btn_Nuevo.Visible = false;
                    Btn_Modificar.Visible = false;
                    Btn_Nuevo.Visible = false;
                    Btn_Eliminar.Visible = false;
                    Btn_Salir.Visible = false;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Eliminar.ToolTip = "Eliminar";
                    Btn_Salir.ToolTip = "Inicio";

                    Btn_Listar_Requisiciones.Visible = true;
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar.png";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";

                    break;
                //Estado de Nuevo
                case MODO_NUEVO:
                    Btn_Listar_Requisiciones.Visible = false;
                    Btn_Nuevo.ToolTip = "Dar de Alta";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Modificar.Visible = false;
                    Btn_Eliminar.Visible = false;
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Salir.Visible = true;

                    break;
                //Estado de Modificar
                case MODO_MODIFICAR:
                    Btn_Listar_Requisiciones.Visible = false;
                    Btn_Nuevo.Visible = false;
                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Eliminar.Visible = false;
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Salir.Visible = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";

                    break;
                default: break;
            }
        }
        catch (Exception ex)
        {
            Mostrar_Informacion(ex.ToString(), true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Mostrar_Información
    ///DESCRIPCIÓN: Llena las areas de texto con el registro seleccionado del grid
    ///RETORNA: 
    /// CREO: Francisco Antonio Gallardo Castañeda
    /// FECHA_CREO: 02/Septiembre/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///********************************************************************************/
    private void Mostrar_Informacion(String txt, Boolean mostrar)
    {
        Lbl_Informacion.Visible = mostrar;
        Img_Warning.Visible = mostrar;
        Lbl_Informacion.Text = txt;
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCIÓN: Llenar_Grid_Requisiciones
    /// DESCRIPCIÓN: Llena el grid principal de requisiciones
    /// RETORNA: 
    /// CREO: Francisco Antonio Gallardo Castañeda
    /// FECHA_CREO: 02/Septiembre/2013 
    /// MODIFICO:
    /// FECHA_MODIFICO:
    /// CAUSA_MODIFICACIÓN:
    ///********************************************************************************/
    public void Llenar_Grid_Requisiciones()
    {
        try
        {
            Div_Contenido.Visible = false;
            Div_Listado_Requisiciones.Visible = true;
            Requisicion_Negocio = new Cls_Ope_Tal_Seguimiento_Requisiciones_Negocio();
            Requisicion_Negocio.P_Dependencia_ID = ((Cmb_Dependencia_Panel.SelectedIndex > 0) ? Cmb_Dependencia_Panel.SelectedValue.Trim() : "");
            Requisicion_Negocio.P_Fecha_Inicio = Convert.ToDateTime(Txt_Fecha_Inicial.Text);
            Requisicion_Negocio.P_Fecha_Fin = Convert.ToDateTime(Txt_Fecha_Final.Text);
            if (Txt_Busqueda.Text.Trim().Length > 0)
            {
                String No_Requisa = Txt_Busqueda.Text;
                No_Requisa = No_Requisa.ToUpper();
                No_Requisa = No_Requisa.Replace("RQ-", "");
                int Int_No_Requisa = 0;
                try
                {
                    Int_No_Requisa = int.Parse(No_Requisa);
                }
                catch (Exception Ex)
                {
                    String Str = Ex.ToString();
                    No_Requisa = "0";
                }
                Requisicion_Negocio.P_Requisicion_ID = No_Requisa;
            }
            Session[P_Dt_Requisiciones] = Requisicion_Negocio.Consultar_Listado_Requisiciones();
            if (Session[P_Dt_Requisiciones] != null && ((DataTable)Session[P_Dt_Requisiciones]).Rows.Count > 0)
            {
                Div_Contenido.Visible = false;
                Grid_Requisiciones.DataSource = Session[P_Dt_Requisiciones] as DataTable;
                Grid_Requisiciones.DataBind();
            }
            else
            {
                Mostrar_Informacion("No se encontraron requisiciones con los criterios de búsqueda", true);
                Grid_Requisiciones.DataSource = Session[P_Dt_Requisiciones] as DataTable;
                Grid_Requisiciones.DataBind();
            }
        }
        catch (Exception Ex)
        {
            Lbl_Informacion.Text = "Verificar. [" + Ex.Message + "]";
            Lbl_Informacion.Visible = true;
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCIÓN: Llenar_Combos_Generales()
    /// DESCRIPCIÓN: Llena los combos principales de la interfaz de usuario
    /// RETORNA: 
    /// CREO: Francisco Antonio Gallardo Castañeda
    /// FECHA_CREO: 02/Septiembre/2013 
    /// MODIFICO:
    /// FECHA_MODIFICO:
    /// CAUSA_MODIFICACIÓN:
    ///********************************************************************************/
    public void Llenar_Combos_Generales()
    {
        Requisicion_Negocio = new Cls_Ope_Tal_Seguimiento_Requisiciones_Negocio();
        Requisicion_Negocio.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado.ToString();
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCIÓN: Llenar_Combos_Busqueda
    /// DESCRIPCIÓN: Llena los combos principales de la interfaz de usuario
    /// RETORNA: 
    /// CREO: Francisco Antonio Gallardo Castañeda
    /// FECHA_CREO: 02/Septiembre/2013 
    /// MODIFICO:
    /// FECHA_MODIFICO:
    /// CAUSA_MODIFICACIÓN:
    ///********************************************************************************/
    public void Llenar_Combos_Busqueda()
    {
        Cmb_Tipo_Busqueda.Items.Clear();
        Cmb_Tipo_Busqueda.Items.Add("TODOS");
        Cmb_Tipo_Busqueda.Items.Add(TIPO_TRANSITORIA);
        Cmb_Tipo_Busqueda.Items.Add(TIPO_STOCK);
        Cmb_Tipo_Busqueda.Items[0].Selected = true;
        Cmb_Tipo_Busqueda.SelectedIndex = Cmb_Tipo_Busqueda.Items.IndexOf(Cmb_Tipo_Busqueda.Items.FindByValue(TIPO_TRANSITORIA));
        Cmb_Tipo_Busqueda.Enabled = false;
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCIÓN: Generar_Tabla_Informacion
    /// DESCRIPCIÓN: Crea una tabla con la informacion que se requiere ingresar al formulario
    /// RETORNA: 
    /// CREO: Francisco Antonio Gallardo Castañeda
    /// FECHA_CREO: 02/Septiembre/2013 
    /// MODIFICO:
    /// FECHA_MODIFICO:
    /// CAUSA_MODIFICACIÓN:
    ///********************************************************************************/
    private void Generar_Tabla_Informacion()
    {
        Contador_Columna = Contador_Columna + 1;
        if (Contador_Columna > 2)
        {
            Contador_Columna = 0;
            Informacion += "</tr><tr>";
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Agregar_Tooltip_Combo
    ///DESCRIPCIÓN: Agrega Tooltip a Combo
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 02/Septiembre/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Agregar_Tooltip_Combo(DropDownList Combo)
    {
        foreach (ListItem Item in Combo.Items)
        {
            Item.Attributes.Add("Title", Item.Text);
        }
    }

    #endregion

    #region EVENTOS

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Sale y/o cancela.
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 02/Septiembre/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {        
        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Click
    ///DESCRIPCIÓN: Metodo que buscar
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 02/Septiembre/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        Habilitar_Controles(MODO_INICIAL);
        Llenar_Grid_Requisiciones();
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Listar_Requisiciones_Click
    ///DESCRIPCIÓN: Metodo de Listar Requisiones.
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 02/Septiembre/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Listar_Requisiciones_Click(object sender, ImageClickEventArgs e)
    {
        Limpiar_Formulario();
        Llenar_Grid_Requisiciones();
        Habilitar_Controles(MODO_LISTADO);
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Imprimir_Requisicion_Click
    ///DESCRIPCIÓN: Imprime las RQ
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 02/Septiembre/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Imprimir_Requisicion_Click(object sender, ImageClickEventArgs e)
    {
        String No_Requisicion = ((ImageButton)sender).CommandArgument;
        DataSet Ds_Reporte = null;
        Ds_Rpt_Tal_Requisicion Ds_Req = new Ds_Rpt_Tal_Requisicion();
        Cls_Ope_Tal_Impresion_Requisiciones_Negocio Req_Negocio = new Cls_Ope_Tal_Impresion_Requisiciones_Negocio();
        DataTable Dt_Cabecera = new DataTable();
        DataTable Dt_Detalles = new DataTable();
        DataTable Dt_Solicitud = new DataTable();
        try
        {
            String Requisicion_ID = No_Requisicion.Replace("RQ-", "");
            Req_Negocio.P_Requisicion_ID = Requisicion_ID.Trim();
            Dt_Cabecera = Req_Negocio.Consultar_Requisiciones();
            Dt_Detalles = Req_Negocio.Consultar_Requisiciones_Detalles();

            if (!String.IsNullOrEmpty(Dt_Cabecera.Rows[0]["NO_SOLICITUD"].ToString()))
            {
                Cls_Rpt_Tal_Solicitud_Servicio_Negocio Rpt = new Cls_Rpt_Tal_Solicitud_Servicio_Negocio();
                Rpt.P_No_Solicitud = Convert.ToInt32(Dt_Cabecera.Rows[0]["NO_SOLICITUD"].ToString());
                Rpt.P_Tipo_Bien = Dt_Cabecera.Rows[0]["TIPO_BIEN"].ToString().Trim();
                Dt_Solicitud = Rpt.Consulta_Datos_Solicitud();
            }
            else
            {
                Dt_Solicitud = Ds_Req.Tables["DT_SERVICIO"].Clone();
            }

            Ds_Reporte = new DataSet();
            Dt_Cabecera.TableName = "REQUISICION";
            Dt_Detalles.TableName = "DETALLES";
            Dt_Solicitud.TableName = "DT_SERVICIO";
            Ds_Reporte.Tables.Add(Dt_Cabecera.Copy());
            Ds_Reporte.Tables.Add(Dt_Detalles.Copy());
            Ds_Reporte.Tables.Add(Dt_Solicitud.Copy());
            Ds_Reporte.Tables.Add(new Cls_Tal_Parametros_Negocio().Obtener_Tabla_Reporte(this.Server).Copy());
            Generar_Reporte(ref Ds_Reporte, "Rpt_Tal_Requisiciones.rpt", "Requisicion" + String.Format("{0:ddMMyyyyhhmmss}", DateTime.Now) + ".pdf");
        }
        catch (Exception Ex)
        {
            Mostrar_Informacion("Verificar. " + Ex.Message, true);
        }

    }

    #endregion

    #region EVENTOS GRID

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Evento_Grid_Requisiciones_Seleccionar
    ///DESCRIPCIÓN: Ejecuta las Operaciones al Seleccionar la RQ.
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 02/Septiembre/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Evento_Grid_Requisiciones_Seleccionar(String Dato)
    {
        Habilitar_Controles(MODO_INICIAL);
        Llenar_Combos_Generales();
        Div_Listado_Requisiciones.Visible = false;
        Div_Contenido.Visible = true;
        Dependencia_Negocio = new Cls_Cat_Dependencias_Negocio();
        Requisicion_Negocio = new Cls_Ope_Tal_Seguimiento_Requisiciones_Negocio();
        String No_Requisicion = Dato;
        Limpiar_Formulario();
        Cls_Ope_Tal_Seguimiento_Requisiciones_Negocio Seguimiento_Negocio = new Cls_Ope_Tal_Seguimiento_Requisiciones_Negocio();
        Seguimiento_Negocio.P_Requisicion_ID = No_Requisicion;
        DataTable Dt_Datos = Seguimiento_Negocio.Consultar_Listado_Requisiciones();
        if (Dt_Datos != null)
        {
            if (Dt_Datos.Rows.Count > 0) {
                Txt_Folio.Text = Dt_Datos.Rows[0]["FOLIO_RQ"].ToString().Trim();
                Txt_OC.Text = Dt_Datos.Rows[0]["FOLIO_OC"].ToString().Trim();
                Txt_Dependencia.Text = Dt_Datos.Rows[0]["DEPENDENCIA"].ToString().Trim();
                Txt_Proveedor.Text = Dt_Datos.Rows[0]["NOMBRE_PROVEEDOR"].ToString().Trim();
                Txt_Total.Text = ((!String.IsNullOrEmpty(Dt_Datos.Rows[0]["TOTAL"].ToString())) ? String.Format("{0:c}", Convert.ToDouble(Dt_Datos.Rows[0]["TOTAL"].ToString())) : "$ 0.00");
                Txt_Total_Cotizado.Text = ((!String.IsNullOrEmpty(Dt_Datos.Rows[0]["TOTAL_COTIZADO"].ToString())) ? String.Format("{0:c}", Convert.ToDouble(Dt_Datos.Rows[0]["TOTAL_COTIZADO"].ToString())) : "$ 0.00");
                Seguimiento_Negocio = new Cls_Ope_Tal_Seguimiento_Requisiciones_Negocio();
                Seguimiento_Negocio.P_Requisicion_ID = No_Requisicion;
                Dt_Datos = Seguimiento_Negocio.Consultar_Historial_Requisiciones();
                Grid_Productos_Servicios.DataSource = Dt_Datos;
                Grid_Productos_Servicios.DataBind();
                Seguimiento_Negocio = new Cls_Ope_Tal_Seguimiento_Requisiciones_Negocio();
                Seguimiento_Negocio.P_Requisicion_ID = No_Requisicion;
                Dt_Datos = Seguimiento_Negocio.Consultar_Historial_Observaciones();
                Grid_Comentarios.DataSource = Dt_Datos;
                Grid_Comentarios.DataBind();
            }
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Seleccionar_Requisicion_Click
    ///DESCRIPCIÓN: Captura el Evento de Seleccionar en el Grid de Requisiciones
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 02/Septiembre/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Seleccionar_Requisicion_Click(object sender, ImageClickEventArgs e)
    {
        String No_Requisicion = ((ImageButton)sender).CommandArgument;
        Evento_Grid_Requisiciones_Seleccionar(No_Requisicion);
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Requisiciones_PageIndexChanging
    ///DESCRIPCIÓN: Captura el Metodo de Paginacion en el Grid de Seleccionar RQ.
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 02/Septiembre/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************* 
    protected void Grid_Requisiciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Grid_Requisiciones.DataSource = ((DataTable)Session[P_Dt_Requisiciones]);
        Grid_Requisiciones.PageIndex = e.NewPageIndex;
        Grid_Requisiciones.DataBind();
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Productos_Servicios_PageIndexChanging
    ///DESCRIPCIÓN: Captura el Evento de Paginacion en los Productos
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 02/Septiembre/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Productos_Servicios_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Grid_Productos_Servicios.DataSource = ((DataTable)Session[P_Dt_Productos_Servicios]);
        Grid_Productos_Servicios.PageIndex = e.NewPageIndex;
        Grid_Productos_Servicios.DataBind();
    }

    /// ******************************************************************************************
    /// NOMBRE: Grid_Requisiciones_Sorting
    /// DESCRIPCIÓN: Captura el Evento de Sorting en los Productos
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 02/Septiembre/2013 
    /// MODIFICÓ:
    /// FECHA MODIFICÓ:
    /// CAUSA MODIFICACIÓN:
    /// ******************************************************************************************
    protected void Grid_Requisiciones_Sorting(object sender, GridViewSortEventArgs e)
    {
        Grid_Sorting(Grid_Requisiciones, ((DataTable)Session[P_Dt_Requisiciones]), e);
    }

    /// *****************************************************************************************
    /// NOMBRE: Grid_Sorting
    /// DESCRIPCIÓN: Ordena las columnas en orden ascendente o descendente.
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 02/Septiembre/2013 
    /// MODIFICÓ:
    /// FECHA MODIFICÓ:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************
    private void Grid_Sorting(GridView Grid, DataTable Dt_Table, GridViewSortEventArgs e)
    {
        if (Dt_Table != null)
        {
            DataView Dv_Vista = new DataView(Dt_Table);
            String Orden = ViewState["SortDirection"].ToString();
            if (Orden.Equals("ASC"))
            {
                Dv_Vista.Sort = e.SortExpression + " DESC";
                ViewState["SortDirection"] = "DESC";
            }
            else
            {
                Dv_Vista.Sort = e.SortExpression + " ASC";
                ViewState["SortDirection"] = "ASC";
            }
            Grid.DataSource = Dv_Vista;
            Grid.DataBind();
        }
    }

    #endregion

    #region REPORTES

    /// *****************************************************************************************
    /// NOMBRE: Generar_Reporte
    /// DESCRIPCIÓN: Genera Reporte
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 02/Septiembre/2013 
    /// MODIFICÓ:
    /// FECHA MODIFICÓ:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************
    protected void Generar_Reporte(ref DataSet Ds_Datos, String Nombre_Plantilla_Reporte, String Nombre_Reporte_Generar)
    {
        ReportDocument Reporte = new ReportDocument();//Variable de tipo reporte.
        String Ruta = String.Empty;//Variable que almacenara la ruta del archivo del crystal report. 

        try
        {
            Ruta = @Server.MapPath("../Rpt/Servicios_Generales/" + Nombre_Plantilla_Reporte);
            Reporte.Load(Ruta);

            if (Ds_Datos is DataSet)
            {
                if (Ds_Datos.Tables.Count > 0)
                {
                    Reporte.SetDataSource(Ds_Datos);
                    Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar);
                    Mostrar_Reporte(Nombre_Reporte_Generar);
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    /// *****************************************************************************************
    /// NOMBRE: Exportar_Reporte_PDF
    /// DESCRIPCIÓN: Exporta a PDF
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 02/Septiembre/2013 
    /// MODIFICÓ:
    /// FECHA MODIFICÓ:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************
    protected void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte)
    {
        ExportOptions Opciones_Exportacion = new ExportOptions();
        DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
        PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

        try
        {
            if (Reporte is ReportDocument)
            {
                Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte);
                Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    /// *****************************************************************************************
    /// NOMBRE: Mostrar_Reporte
    /// DESCRIPCIÓN: Muestra el Reporte
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 02/Septiembre/2013 
    /// MODIFICÓ:
    /// FECHA MODIFICÓ:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte)
    {
        String Pagina = "../../Reporte/";

        try
        {
            Pagina = Pagina + Nombre_Reporte;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window",
                "window.open('" + Pagina + "', 'Requisición','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    #endregion

}