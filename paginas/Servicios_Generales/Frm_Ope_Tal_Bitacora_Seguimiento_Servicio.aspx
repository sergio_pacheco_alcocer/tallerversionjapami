﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Ope_Tal_Bitacora_Seguimiento_Servicio.aspx.cs"
    Inherits="paginas_Taller_Mecanico_Frm_Ope_Tal_Bitacora_Seguimiento_Servicio"
    Title="Bitacora de Seguimiento" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">

    <script type="text/javascript" language="javascript">
        function Limpiar_Ctlr_Busqueda() {
            $("[id$='Txt_Folio_Solicitud_Busqueda']").val("");
            $("[id$='Cmb_Tipo_Solicitud_Busqueda']").val("");
            $("[id$='Cmb_Unidad_Responsable_Busqueda']").val("");
            $("[id$='Txt_Numero_Inventario_Busqueda']").val("");
            $("[id$='Txt_Numero_Economico_Busqueda']").val("");
            $("[id$='Txt_Fecha_Recepcion_Inicial']").val("");
            $("[id$='Txt_Fecha_Recepcion_Final']").val("");
            $("[id$='Cmb_Filtrado_Estatus']").val("");
            $("[id$='Cmb_Reparacion_Busqueda']").val("");
            return false;
        }
        $(function() {
            if ($("[id$='Txt_Numero_Economico_Busqueda']").val().Length > 0) {
                $("[id$='Txt_Numero_Economico_Busqueda']").live({ 'keyup': function() {
                    var Str_Economico = $("[id$='Txt_Numero_Economico_Busqueda']").val();
                    var Longitud_Palabra = Str_Economico.length;
                    if (!isNaN(Str_Economico[0]))
                        Str_Economico = 'U' + Str_Economico;
                    else
                        Str_Economico = Str_Economico.toUpperCase();
                    $("[id$='Txt_Numero_Economico_Busqueda']").val(Str_Economico);
                }, 'blur': function() {
                    var Str_Economico = $("[id$='Txt_Numero_Economico_Busqueda']").val();
                    var Str_Economico_Copia = new Array(6);
                    Str_Economico_Copia[0] = Str_Economico[0];
                    Str_Economico_Copia[1] = '0';
                    Str_Economico_Copia[2] = '0';
                    Str_Economico_Copia[3] = '0';
                    Str_Economico_Copia[4] = '0';
                    Str_Economico_Copia[5] = '0';
                    Str_Economico_Copia[6] = '0';
                    Longitud_Palabra = Str_Economico.length;
                    if (Longitud_Palabra < 6) {
                        for (var i = 1; i < Longitud_Palabra; i++) {
                            Str_Economico_Copia[6 - (Longitud_Palabra - i)] = Str_Economico[i];
                        }
                    }
                    $("[id$='Txt_Numero_Economico_Busqueda']").val(Str_Economico_Copia[0] + Str_Economico_Copia[1] + Str_Economico_Copia[2] + Str_Economico_Copia[3] + Str_Economico_Copia[4] + Str_Economico_Copia[5]);
                }
                });
            }
        });
    </script>

    <cc1:ToolkitScriptManager ID="ScptM_Operacion" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" EnablePartialRendering="true" AsyncPostBackTimeout="9000">
    </cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Loading" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Area_Trabajo" style="background-color: #ffffff; width: 100%; height: 100%;">
                <center>
                    <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                        <tr align="center">
                            <td class="label_titulo" colspan="2">
                                Bitacora de Seguimiento de Solicitudes
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div id="Div_Contenedor_Msj_Error" style="width: 98%;" runat="server" visible="false">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td colspan="2" align="left">
                                                <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                    Width="24px" Height="24px" />
                                                <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 10%;">
                                            </td>
                                            <td style="width: 90%; text-align: left;" valign="top">
                                                <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left" style="width: 50%;">
                                <asp:ImageButton ID="Btn_Imprimir_Solicitud_Servicio" runat="server" ImageUrl="~/paginas/imagenes/gridview/grid_print.png"
                                    Width="24px" CssClass="Img_Button" AlternateText="Imprimir" ToolTip="Imprimir Solicitud"
                                    OnClick="Btn_Imprimir_Solicitud_Servicio_Click" />
                                <asp:ImageButton ID="Btn_Imprimir_Bitacora_Seguimiento" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png"
                                    Width="24px" CssClass="Img_Button" AlternateText="Imprimir" ToolTip="Imprimir Bitacora"
                                    OnClick="Btn_Imprimir_Bitacora_Seguimiento_Click" />
                                <asp:ImageButton ID="Btn_Cancelar" runat="server" ToolTip="Cancelar Servicio" ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png"
                                    Width="24px" CssClass="Img_Button" AlternateText="Cancelar Solicitud" OnClientClick="return confirm('¿Esta seguro de Cancelar la presente Solicitud?');"
                                    OnClick="Btn_Cancelar_Click" />
                                <asp:ImageButton ID="Btn_Dianostico" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_load.png"
                                    Width="24px" CssClass="Img_Button" AlternateText="Diagnostico_Servicio" ToolTip="Diagnostico Servicio"
                                    OnClientClick="return confirm('¿Esta seguro de Re-Diagnosticar el presente Servicio? \n Este pasará a Diagnostico de Mecanico');"
                                    OnClick="Btn_Dianostico_Click" />
                                <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                    Width="24px" CssClass="Img_Button" AlternateText="Salir" OnClick="Btn_Salir_Click" />
                            </td>
                            <td style="width: 50%;">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                    <br />
                    <div id="Div_Listado_Solicitudes" runat="server" style="width: 100%;">
                        <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                                <td colspan="4">
                                    <div id="Div_Filtros_Busqueda" runat="server" style="border-style: outset; width: 98%;">
                                        <table width="100%" border="0" cellspacing="0" class="estilo_fuente">
                                            <tr>
                                                <td colspan="4">
                                                    <asp:Label ID="Lbl_Leyenda_Busqueda" runat="server" Width="100%" Text="FILTROS PARA EL LISTADO"
                                                        ForeColor="White" BackColor="Black" Font-Bold="true" Style="text-align: center;"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Folio_Solicitud_Busqueda" runat="server" Text="Folio"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Folio_Solicitud_Busqueda" runat="server" Width="96%" MaxLength="10"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Folio_Solicitud_Busqueda" runat="server"
                                                        TargetControlID="Txt_Folio_Solicitud_Busqueda" FilterType="Numbers">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 15%;">
                                                    &nbsp;&nbsp;
                                                    <asp:Label ID="Lbl_Tipo_Solicitud_Busqueda" runat="server" Text="Tipo Solicitud"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:DropDownList ID="Cmb_Tipo_Solicitud_Busqueda" runat="server" Width="98%">
                                                        <asp:ListItem Value="">&lt; - - TODOS - - &gt;</asp:ListItem>
                                                        <asp:ListItem Value="REVISTA_MECANICA">REVISTA MECANICA</asp:ListItem>
                                                        <asp:ListItem Value="SERVICIO_CORRECTIVO">SERVICIO CORRECTIVO</asp:ListItem>
                                                        <asp:ListItem Value="SERVICIO_PREVENTIVO">SERVICIO PREVENTIVO</asp:ListItem>
                                                        <asp:ListItem Value="VERIFICACION">VERIFICACI&Oacute;N</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Unidad_Responsable_Busqueda" runat="server" Text="Unidad Responsable"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:DropDownList ID="Cmb_Unidad_Responsable_Busqueda" runat="server" Width="99.2%">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Numero_Inventario_Busqueda" runat="server" Text="No. Inventario"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Numero_Inventario_Busqueda" runat="server" Width="96%"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Numero_Inventario_Busqueda" runat="server"
                                                        TargetControlID="Txt_Numero_Inventario_Busqueda" FilterType="Numbers">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 15%;">
                                                    &nbsp;&nbsp;
                                                    <asp:Label ID="Lbl_Numero_Economico_Busqueda" runat="server" Text="No. Económico"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Numero_Economico_Busqueda" runat="server" Width="96%" MaxLength="6"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Numero_Economico_Busqueda" runat="server"
                                                        TargetControlID="Txt_Numero_Economico_Busqueda" FilterType="Numbers,Custom" ValidChars="UMum">
                                                    </cc1:FilteredTextBoxExtender>
                                                    <cc1:TextBoxWatermarkExtender ID="Tbw_Economico" runat="server" TargetControlID="Txt_Numero_Economico_Busqueda"
                                                        WatermarkCssClass="watermarked" WatermarkText="&lt;U00000&gt;" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <hr />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" style="text-align: center;">
                                                    <asp:Label ID="Lbl_Leyenda_Fecha_Recepcion" runat="server" Text="Rango en Fecha de Recepción"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Fecha_Recepcion_Inicial" runat="server" Text="Fecha Inicial"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Fecha_Recepcion_Inicial" runat="server" Width="85%" Enabled="false"></asp:TextBox>
                                                    <asp:ImageButton ID="Btn_Fecha_Recepcion_Inicial" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                                    <cc1:CalendarExtender ID="CE_Txt_Fecha_Recepcion_Inicial" runat="server" TargetControlID="Txt_Fecha_Recepcion_Inicial"
                                                        PopupButtonID="Btn_Fecha_Recepcion_Inicial" Format="dd/MMM/yyyy">
                                                    </cc1:CalendarExtender>
                                                </td>
                                                <td style="width: 15%;">
                                                    &nbsp;&nbsp;<asp:Label ID="Lbl_Fecha_Recepcion_Final" runat="server" Text="Fecha Final"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Fecha_Recepcion_Final" runat="server" Width="85%" Enabled="false"></asp:TextBox>
                                                    <asp:ImageButton ID="Btn_Fecha_Recepcion_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                                    <cc1:CalendarExtender ID="CE_Txt_Fecha_Recepcion_Final" runat="server" TargetControlID="Txt_Fecha_Recepcion_Final"
                                                        PopupButtonID="Btn_Fecha_Recepcion_Final" Format="dd/MMM/yyyy">
                                                    </cc1:CalendarExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <hr />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Filtrado_Estatus" runat="server" Text="Estatus"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:DropDownList ID="Cmb_Filtrado_Estatus" runat="server" Width="98%">
                                                        <asp:ListItem Value="">&lt; - TODAS - &gt;</asp:ListItem>
                                                        <asp:ListItem Value="PENDIENTE">PENDIENTE</asp:ListItem>
                                                        <asp:ListItem Value="RECHAZADA">RECHAZADA</asp:ListItem>
                                                        <asp:ListItem Value="CANCELADA">CANCELADA</asp:ListItem>
                                                        <asp:ListItem Value="AUTORIZADA">AUTORIZADA</asp:ListItem>
                                                        <asp:ListItem Value="EN_REPARACION">EN REPARACIÓN</asp:ListItem>
                                                        <asp:ListItem Value="ENTREGADO">ENTREGADO</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 15%;">
                                                    &nbsp;&nbsp;Reparación&nbsp;
                                                </td>
                                                <td style="text-align: left; width: 35%;">
                                                    <asp:DropDownList ID="Cmb_Reparacion_Busqueda" runat="server" Width="98%">
                                                        <asp:ListItem Value="">&lt; - - TODOS - - &gt;</asp:ListItem>
                                                        <asp:ListItem Value="INTERNA">INTERNO</asp:ListItem>
                                                        <asp:ListItem Value="EXTERNA">EXTERNO</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="right">
                                                    <asp:ImageButton ID="Btn_Actualizar_Listado" runat="server" ToolTip="Actualizar Listado"
                                                        AlternateText="Actualizar Listado" OnClick="Btn_Actualizar_Listado_Click" ImageUrl="~/paginas/imagenes/paginas/actualizar_detalle.png"
                                                        Width="16px" />
                                                    <asp:ImageButton ID="Btn_Limpiar_Filtros_Listado" runat="server" ToolTip="Limpiar Filtros Listado"
                                                        AlternateText="Limpiar Filtros Listado" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png"
                                                        Width="16px" Height="16px" OnClientClick="javascript:return Limpiar_Ctlr_Busqueda();" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <asp:GridView ID="Grid_Listado_Solicitudes" runat="server" CssClass="GridView_1"
                            AutoGenerateColumns="False" AllowPaging="True" PageSize="20" Width="99%" GridLines="None"
                            EmptyDataText="No se Encontrarón Solicitudes." OnSelectedIndexChanged="Grid_Listado_Solicitudes_SelectedIndexChanged"
                            OnPageIndexChanging="Grid_Listado_Solicitudes_PageIndexChanging" DataKeyNames="NO_SOLICITUD,ESTATUS">
                            <RowStyle CssClass="GridItem" />
                            <Columns>
                                <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                    <ItemStyle Width="30px" />
                                </asp:ButtonField>
                                <asp:BoundField DataField="NO_SOLICITUD" HeaderText="NO_SOLICITUD" SortExpression="NO_SOLICITUD">
                                    <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FOLIO" HeaderText="FOLIO" SortExpression="FOLIO">
                                    <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FECHA_ELABORACION" HeaderText="Fecha Elaboración" SortExpression="FECHA_ELABORACION"
                                    DataFormatString="{0:dd/MMM/yyyy}">
                                    <ItemStyle Width="160px" HorizontalAlign="Center" Font-Size="X-Small" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FECHA_RECEPCION_REAL" HeaderText="Fecha Recepción" SortExpression="FECHA_RECEPCION_REAL"
                                    DataFormatString="{0:dd/MMM/yyyy}">
                                    <ItemStyle Width="160px" HorizontalAlign="Center" Font-Size="X-Small" />
                                </asp:BoundField>
                                <asp:BoundField DataField="TIPO_SERVICIO" HeaderText="Tipo Servicio" SortExpression="TIPO_SERVICIO">
                                    <ItemStyle Width="160px" HorizontalAlign="Center" Font-Size="X-Small" />
                                </asp:BoundField>
                                <asp:BoundField DataField="NO_INVENTARIO" HeaderText="No. Inventario" SortExpression="NO_INVENTARIO">
                                    <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS">
                                    <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small" />
                                </asp:BoundField>   
                                <asp:BoundField DataField="NO_ECONOMICO" HeaderText="No. Economico" SortExpression="NO_ECONOMICO">
                                    <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small" />
                                </asp:BoundField>
                            </Columns>
                            <PagerStyle CssClass="GridHeader" />
                            <SelectedRowStyle CssClass="GridSelected" />
                            <HeaderStyle CssClass="GridHeader" />
                            <AlternatingRowStyle CssClass="GridAltItem" />
                        </asp:GridView>
                    </div>
                    <div id="Div_Campos" runat="server" style="width: 100%;">
                        <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                                <td colspan="4">
                                    <asp:HiddenField ID="Hdf_No_Solicitud" runat="server" />
                                    <asp:HiddenField ID="Hdf_Folio_Solicitud" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;&nbsp;&nbsp;
                                </td>
                                <td style="width: 15%;">
                                    &nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="Lbl_Folio_Solicitud" runat="server" Text="Folio Solicitud" Font-Bold="true"
                                        ForeColor="Black"></asp:Label>
                                </td>
                                <td style="width: 35%;">
                                    <asp:TextBox ID="Txt_Folio_Solicitud" runat="server" Width="98%" ReadOnly="true"
                                        BorderStyle="Outset" Style="text-align: center;" Font-Bold="true" ForeColor="Red"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Fecha_Elaboracion" runat="server" Text="Fecha Elaboración"></asp:Label>
                                </td>
                                <td style="width: 35%;">
                                    <asp:TextBox ID="Txt_Fecha_Elaboracion" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                </td>
                                <td style="width: 15%;">
                                    &nbsp;&nbsp;&nbsp;
                                </td>
                                </td>
                                <td style="width: 35%;">
                                    <asp:TextBox ID="Txt_Estatus" runat="server" Width="98%" ReadOnly="true" BorderStyle="Outset"
                                        Style="text-align: center;" Font-Bold="true"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text="Unidad Responsable"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="Cmb_Unidad_Responsable" runat="server" Width="100%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Kilometraje" runat="server" Text="Kilometraje [Km]"></asp:Label>
                                </td>
                                <td style="width: 16%;">
                                    <asp:TextBox ID="Txt_Kilometraje" runat="server" Width="95%"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Kilometraje" runat="server" TargetControlID="Txt_Kilometraje"
                                        ValidChars="." FilterType="Custom, Numbers">
                                    </cc1:FilteredTextBoxExtender>
                                </td>
                                <td style="width: 15%;">
                                    &nbsp;&nbsp;
                                    <asp:Label ID="Lbl_Tipo_Servicio" runat="server" Text="Tipo Servicio"></asp:Label>
                                </td>
                                <td style="width: 35%;">
                                    <asp:DropDownList ID="Cmb_Tipo_Servicio" runat="server" Width="100%">
                                        <asp:ListItem Value="">&lt; - - SELECCIONE - - &gt;</asp:ListItem>
                                        <asp:ListItem Value="REVISTA_MECANICA">REVISTA MECANICA</asp:ListItem>
                                        <asp:ListItem Value="SERVICIO_CORRECTIVO">SERVICIO CORRECTIVO</asp:ListItem>
                                        <asp:ListItem Value="SERVICIO_PREVENTIVO">SERVICIO PREVENTIVO</asp:ListItem>
                                        <asp:ListItem Value="VERIFICACION">VERIFICACI&Oacute;N</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Correo_Electronico" runat="server" Text="Correo Electronico"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="Txt_Correo_Electronico" runat="server" Width="99%">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:Panel ID="Pnl_Vehiculo_Seleccionado" runat="server" Width="99%" GroupingText="Vehículo para el Servicio">
                                        <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:HiddenField ID="Hdf_Vehiculo_ID" runat="server" />
                                                    <asp:Label ID="Lbl_No_Inventario" runat="server" Text="No. Inventario"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_No_Inventario" runat="server" Width="98%" MaxLength="7"></asp:TextBox>
                                                </td>
                                                <td style="width: 15%;">
                                                    &nbsp;&nbsp;
                                                    <asp:Label ID="Lbl_No_Economico" runat="server" Text="No. Economico"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_No_Economico" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Datos_Vehiculo" runat="server" Text="Vehículo"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Datos_Vehiculo" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Placas" runat="server" Text="Placas"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Placas" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                                </td>
                                                <td style="width: 15%;">
                                                    &nbsp;&nbsp;
                                                    <asp:Label ID="Lbl_Anio" runat="server" Text="Año"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Anio" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="1">
                                                    Reparación
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Reparacion" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="Pnl_Bien_Mueble_Seleccionado" runat="server" GroupingText="Bien Mueble para el Servicio"
                                        Width="99%">
                                        <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:HiddenField ID="Hdf_Bien_Mueble_ID" runat="server" />
                                                    <asp:HiddenField ID="Hdf_Tipo_Bien" runat="server" />
                                                    <asp:Label ID="Lbl_No_Inventario_BM" runat="server" Text="No. Inventario"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_No_Inventario_BM" runat="server" MaxLength="7" Width="98%" Enabled="false"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Inventario_BM" runat="server" FilterType="Numbers"
                                                        TargetControlID="Txt_No_Inventario_BM">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 15%;">
                                                    &nbsp;
                                                </td>
                                                <td style="width: 35%;">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Descripcion_Bien" runat="server" Text="Descripción Bien"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Descripcion_Bien" runat="server" Enabled="false" Rows="2" TextMode="MultiLine"
                                                        Width="99%"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Numero_Serie_Bien" runat="server" Text="No. Serie"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Numero_Serie_Bien" runat="server" Enabled="false" Width="99%"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:Panel ID="Pnl_Descripcion_Servicio" runat="server" Width="99%" GroupingText="Descripción del Servicio">
                                        <asp:TextBox ID="Txt_Descripcion_Servicio" runat="server" Rows="2" Style="text-transform: uppercase"
                                            TextMode="MultiLine" Width="99%" Height="90px" ReadOnly="True"></asp:TextBox>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:Panel ID="Pnl_Diagnostico_Servicio" runat="server" Width="99%" GroupingText="Diagnostico del Servicio">
                                        <asp:TextBox ID="Txt_Diagnostico_Servicio" runat="server" Rows="3" Style="text-transform: uppercase"
                                            TextMode="MultiLine" Width="99%" Height="90px" ReadOnly="True"></asp:TextBox>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="text-align: right;">
                                    <asp:ImageButton ID="Btn_Ver_Servicios_Adicionales" runat="server" ToolTip="Ver Servicios Adicionales"
                                        AlternateText="Ver Servicios Adicionales" ImageUrl="~/paginas/imagenes/paginas/sias_revisarplan.png"
                                        Width="24px" OnClick="Btn_Ver_Servicios_Adicionales_Click" />
                                    <asp:ImageButton ID="Btn_Ver_Listado_Refacciones" runat="server" ToolTip="Ver Listado Refacciones"
                                        AlternateText="Ver Listado Refacciones" ImageUrl="~/paginas/imagenes/paginas/Listado.png"
                                        Width="24px" OnClick="Btn_Ver_Listado_Refacciones_Click" />
                                    &nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:Panel ID="Pnl_Motivo_Rechazo" runat="server" Width="99%" GroupingText="Motivo del Rechazo">
                                        <asp:TextBox ID="Txt_Motivo_Rechazo" runat="server" Rows="2" Style="text-transform: uppercase"
                                            TextMode="MultiLine" Width="99%" Height="90px" ReadOnly="True"></asp:TextBox>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <asp:GridView ID="Grid_Seguimiento_Solicitud" runat="server" CssClass="GridView_1"
                            AutoGenerateColumns="False" AllowPaging="True" PageSize="100" Width="99%" GridLines="None"
                            EmptyDataText="No hay seguimiento.">
                            <RowStyle CssClass="GridItem" />
                            <Columns>
                                <asp:BoundField DataField="NO_REGISTRO" HeaderText="NO_REGISTRO" SortExpression="NO_REGISTRO">
                                    <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS">
                                    <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FECHA_ASIGNO" HeaderText="Fecha / Hora" SortExpression="FECHA_ASIGNO"
                                    DataFormatString="{0:dd/MMM/yyyy hh:mm:ss tt}">
                                    <ItemStyle Width="140px" HorizontalAlign="Center" Font-Size="X-Small" />
                                </asp:BoundField>
                                <asp:BoundField DataField="EMPLEADO_ASIGNO" HeaderText="Empleado Asigno" SortExpression="EMPLEADO_ASIGNO">
                                    <ItemStyle Width="300px" Font-Size="X-Small" />
                                </asp:BoundField>
                                <asp:BoundField DataField="MOTIVO_RECHAZO" HeaderText="Comentarios" SortExpression="MOTIVO_RECHAZO">
                                    <ItemStyle Font-Size="X-Small" />
                                </asp:BoundField>
                            </Columns>
                            <PagerStyle CssClass="GridHeader" />
                            <SelectedRowStyle CssClass="GridSelected" />
                            <HeaderStyle CssClass="GridHeader" />
                            <AlternatingRowStyle CssClass="GridAltItem" />
                        </asp:GridView>
                    </div>
                </center>
            </div>
            <br />
            <br />
            <br />
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpPnl_Aux_Listado_Refacciones" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_MPE_Listado_Refacciones" runat="server" Text="" Style="display: none;" />
            <cc1:ModalPopupExtender ID="MPE_Listado_Refacciones" runat="server" TargetControlID="Btn_Comodin_MPE_Listado_Refacciones"
                PopupControlID="Pnl_Busqueda_Contenedor" CancelControlID="Btn_Cerrar_Ventana"
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Pnl_Busqueda_Contenedor" runat="server" CssClass="drag" HorizontalAlign="Center"
        Width="850px" Style="display: none; border-style: outset; border-color: Silver;
        background-image: url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG); background-repeat: repeat-y;">
        <asp:Panel ID="Pnl_Busqueda_Resguardante_Cabecera" runat="server" Style="cursor: move;
            background-color: Silver; color: Black; font-size: 12; font-weight: bold; border-style: outset;">
            <table width="99%">
                <tr>
                    <td style="color: Black; font-size: 12; font-weight: bold;">
                        <asp:Image ID="Img_Informatcion_Autorizacion" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                        Listado de Refacciones Solicitadas para el Servicio
                    </td>
                    <td align="right" style="width: 10%;">
                        <asp:ImageButton ID="Btn_Cerrar_Ventana" CausesValidation="false" runat="server"
                            Style="cursor: pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <div style="color: #5D7B9D">
            <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;">
                        <asp:UpdatePanel ID="Upnl_Listado_Refacciones" runat="server">
                            <ContentTemplate>
                                <asp:UpdateProgress ID="Progress_Upnl_Listado_Refacciones" runat="server" AssociatedUpdatePanelID="Upnl_Listado_Refacciones"
                                    DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                        </div>
                                        <div style="background-color: Transparent; position: fixed; top: 50%; left: 47%;
                                            padding: 10px; z-index: 1002;" id="div_progress">
                                            <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <br />
                                <div id="Div_Listado_Refacciones" runat="server" style="border-style: outset; width: 99%;
                                    height: 450px; overflow: auto;">
                                    <asp:GridView ID="Grid_Listado_Refacciones" runat="server" AutoGenerateColumns="False"
                                        CellPadding="4" ForeColor="#333333" GridLines="None" AllowPaging="True" Width="100%"
                                        PageSize="100" EmptyDataText="No se encontrarón resultados">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:BoundField DataField="CANTIDAD" HeaderText="Cantidad" SortExpression="CANTIDAD">
                                                <ItemStyle Font-Size="X-Small" Width="50px" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CLAVE" HeaderText="Clave" SortExpression="CLAVE">
                                                <ItemStyle Font-Size="X-Small" Width="150px" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NOMBRE" HeaderText="Nombre" SortExpression="NOMBRE">
                                                <ItemStyle Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TOTAL" HeaderText="Toltal" SortExpression="TOTAL">
                                                <ItemStyle Font-Size="X-Small" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:UpdatePanel ID="UpPnl_Aux_Listado_Servicios_Adicionales" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_MPE_Servicios_Adicionales" runat="server" Text="" Style="display: none;" />
            <cc1:ModalPopupExtender ID="MPE_Servicios_Adicionales" runat="server" TargetControlID="Btn_Comodin_MPE_Servicios_Adicionales"
                PopupControlID="Pnl_Servicios_Adicionales" CancelControlID="Btn_Cerrar_Ventana_Servicios_Adicionales"
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Pnl_Servicios_Adicionales" runat="server" CssClass="drag" HorizontalAlign="Center"
        Width="850px" Style="display: none; border-style: outset; border-color: Silver;
        background-image: url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG); background-repeat: repeat-y;">
        <asp:Panel ID="Pnl_Servicios_Adicionales_Interno" runat="server" Style="cursor: move;
            background-color: Silver; color: Black; font-size: 12; font-weight: bold; border-style: outset;">
            <table width="99%">
                <tr>
                    <td style="color: Black; font-size: 12; font-weight: bold;">
                        <asp:Image ID="Img_Icono_Servicios_Adicionales" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                        Listado de Servicios Adicionales
                    </td>
                    <td align="right" style="width: 10%;">
                        <asp:ImageButton ID="Btn_Cerrar_Ventana_Servicios_Adicionales" CausesValidation="false"
                            runat="server" Style="cursor: pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <div style="color: #5D7B9D">
            <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;">
                        <asp:UpdatePanel ID="UpPnl_Servicios_Adicionales" runat="server">
                            <ContentTemplate>
                                <asp:UpdateProgress ID="UpPgr_Servicios_Adicionales" runat="server" AssociatedUpdatePanelID="UpPnl_Servicios_Adicionales"
                                    DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                        </div>
                                        <div style="background-color: Transparent; position: fixed; top: 50%; left: 47%;
                                            padding: 10px; z-index: 1002;" id="div_progress">
                                            <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <br />
                                <div id="Div_Listado_Servicios_Adicionales" runat="server" style="border-style: outset;
                                    width: 99%; height: 450px; overflow: auto;">
                                    <asp:GridView ID="Grid_Listado_Servicios_Adicionales" runat="server" AutoGenerateColumns="False"
                                        CellPadding="4" ForeColor="#333333" GridLines="None" AllowPaging="True" Width="100%"
                                        PageSize="100" EmptyDataText="No se encontrarón resultados.">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:BoundField DataField="FOLIO" HeaderText="FOLIO" SortExpression="FOLIO">
                                                <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TIPO_SERVICIO" HeaderText="Tipo Servicio" SortExpression="TIPO_SERVICIO">
                                                <ItemStyle Width="160px" HorizontalAlign="Center" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DESCRIPCION_SERVICIO" HeaderText="Descripción Servicio"
                                                SortExpression="DESCRIPCION_SERVICIO">
                                                <ItemStyle Width="160px" HorizontalAlign="Center" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS">
                                                <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </div>
                                <asp:HiddenField ID="Hdf_Empleado_Solicito_ID" runat="server" />
                                <asp:HiddenField ID="Hdf_Email" runat="server" />
                                <asp:HiddenField ID="Hdf_Estatus_Servicio" runat="server" />
                                <asp:HiddenField ID="Hdf_Mecanico_ID" runat="server" />
                                <asp:HiddenField ID="Hdf_No_Servicio" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>
