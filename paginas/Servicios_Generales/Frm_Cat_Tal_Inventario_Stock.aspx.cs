﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CarlosAg.ExcelXmlWriter;
using System.Text;
using JAPAMI.Sessiones;
using JAPAMI.Taller_Mecanico.Catalogo_Inventario_Stock.Negocio;
using JAPAMI.Catalogo_Taller_Refacciones.Negocio;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;

public partial class paginas_Taller_Mecanico_Frm_Cat_Tal_Inventario_Stock : System.Web.UI.Page
{
    #region Variables

        public Decimal Sumatoria = 0;

    #endregion
    
    #region Page_Load

    ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN         : Metodo que se carga cada que ocurre un PostBack de la Página
        ///PROPIEDADES         :
        ///CREO                : Salvador Vazquez Camacho
        ///FECHA_CREO          : 29/Junio/2012 
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                //Llenar_Grid_Salidas_Express(new DataTable());
                //Llenar_Grid_Salidas_Almacen(new DataTable());
            }
            Div_Contenedor_Msj_Error.Visible = false;
        }

    #endregion

    #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Salidas_Express
        ///DESCRIPCIÓN         : Llena el Grid con las salidas express que cumplan el filtro
        ///PROPIEDADES         : 
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 29/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Llenar_Grid_Salidas_Express(DataTable Dt_Salidas_Express)
        {
            Grid_Salidas_Express.SelectedIndex = (-1);
            Grid_Salidas_Express.DataSource = Dt_Salidas_Express;
            Grid_Salidas_Express.DataBind();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Salidas
        ///DESCRIPCIÓN         : Llena el Grid con las salidas express que cumplan el filtro
        ///PROPIEDADES         : 
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 29/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Llenar_Grid_Salidas_Almacen(DataTable Dt_Salidas_Almacen)
        {
            Grid_Salidas_Almacen.SelectedIndex = (-1);
            Grid_Salidas_Almacen.DataSource = Dt_Salidas_Almacen;
            Grid_Salidas_Almacen.DataBind();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Pasar_DataSet_A_Excel
        ///DESCRIPCION         : Pasa DataTable a Excel. 
        ///PARÁMETROS          : Dt_Reporte.- DataTable que se pasara a excel. 
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 29/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        public void Pasar_DataSet_A_Excel(DataSet Ds_Reportes)
        {
            String Ruta = "Vales de Gasolina [" + (String.Format("{0:dd_MMM_yyyy}", DateTime.Now)) + "].xls";//Variable que almacenara el nombre del archivo. 

            try
            {
                //Creamos el libro de Excel.
                CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();

                Libro.Properties.Title = "Reporte de Tarjeta de Gasolina";
                Libro.Properties.Created = DateTime.Now;
                Libro.Properties.Author = "Patrimonio";

                //Creamos una hoja que tendrá el libro.
                CarlosAg.ExcelXmlWriter.Worksheet Hoja = Libro.Worksheets.Add("Registros");
                //Agregamos un renglón a la hoja de excel.
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon = Hoja.Table.Rows.Add();
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");
                //Creamos el estilo clave para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Clave = Libro.Styles.Add("Clave");

                Estilo_Cabecera.Font.FontName = "Tahoma";
                Estilo_Cabecera.Font.Size = 10;
                Estilo_Cabecera.Font.Bold = true;
                Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Cabecera.Font.Color = "#FFFFFF";
                Estilo_Cabecera.Interior.Color = "#193d61";
                Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Alignment.WrapText = true;

                Estilo_Contenido.Font.FontName = "Tahoma";
                Estilo_Contenido.Font.Size = 8;
                Estilo_Contenido.Font.Bold = true;
                Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Contenido.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Contenido.Font.Color = "#000000";
                Estilo_Contenido.Interior.Color = "White";
                Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Alignment.WrapText = true;

                Estilo_Clave.Font.FontName = "Tahoma";
                Estilo_Clave.Font.Size = 8;
                Estilo_Clave.Font.Bold = true;
                Estilo_Clave.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Clave.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Clave.Font.Color = "White";
                Estilo_Clave.Interior.Color = "#000000";
                Estilo_Clave.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Clave.Alignment.WrapText = true;

                //Agregamos las columnas que tendrá la hoja de excel.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));

                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CLAVE", "Clave"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Txt_Clave.Text, "BodyStyle"));
                
                WorksheetCell cell = Renglon.Cells.Add(Hdf_Nombre.Value);
                cell.MergeAcross = 3;            // Merge two cells together
                cell.StyleID = "BodyStyle";

                Renglon.Height = 23;
                Renglon = Hoja.Table.Rows.Add();
                Renglon = Hoja.Table.Rows.Add();

                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Existencia", "HeaderStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Comprometido", "HeaderStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Disponible", "HeaderStyle"));
                Renglon.Height = 23;
                Renglon = Hoja.Table.Rows.Add();
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Txt_Existencia.Text, "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Txt_Comprometido.Text, "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Txt_Disponible.Text, "BodyStyle"));
                Renglon = Hoja.Table.Rows.Add();
                Renglon = Hoja.Table.Rows.Add();

                foreach (DataTable Dt_Reporte in Ds_Reportes.Tables)
                {
                    if (Dt_Reporte is System.Data.DataTable)
                    {
                        if (Dt_Reporte.Rows.Count > 0)
                        {
                            foreach (DataColumn COLUMNA in Dt_Reporte.Columns)
                            {
                                if (COLUMNA is System.Data.DataColumn)
                                {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(COLUMNA.ColumnName, "HeaderStyle"));
                                }
                                Renglon.Height = 23;
                            }

                            foreach (DataRow FILA in Dt_Reporte.Rows)
                            {
                                if (FILA is System.Data.DataRow)
                                {
                                    Renglon = Hoja.Table.Rows.Add();

                                    foreach (DataColumn COLUMNA in Dt_Reporte.Columns)
                                    {
                                        if (COLUMNA is System.Data.DataColumn)
                                        {
                                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(FILA[COLUMNA.ColumnName].ToString(), DataType.String, "BodyStyle"));
                                        }
                                    }
                                    Renglon.Height = 15;
                                    Renglon.AutoFitHeight = true;
                                }
                            }
                            Renglon = Hoja.Table.Rows.Add();
                            Renglon = Hoja.Table.Rows.Add();
                        }
                    }
                }
                //Abre el archivo de excel
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + Ruta);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;
                Libro.Save(Response.OutputStream);
                Response.End();
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
        ///DESCRIPCIÓN         : caraga el data set fisico con el cual se genera el Reporte especificado
        ///PARAMETROS          :  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
        ///                       2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
        ///                       3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 29/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Generar_Reporte(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte)
        {
            ReportDocument Reporte = new ReportDocument();
            String File_Path = Server.MapPath("../Rpt/Taller_Mecanico/" + Nombre_Reporte);
            Reporte.Load(File_Path);
            String Nombre_Reporte_Generar = "Rpt_Tal_Inventario_Stock_" + Cls_Sessiones.No_Empleado + "_" + Convert.ToString(DateTime.Now.ToString("yyyy'-'MM'-'dd'_t'HH'-'mm'-'ss")) + ".pdf";
            String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
            Ds_Reporte = Data_Set_Consulta_DB;
            Reporte.SetDataSource(Ds_Reporte);
            ExportOptions Export_Options = new ExportOptions();
            DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
            Disk_File_Destination_Options.DiskFileName = Server.MapPath(Ruta);
            Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
            Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
            Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
            Reporte.Export(Export_Options);
            Mostrar_Reporte(Nombre_Reporte_Generar, "PDF");
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
        }

        /// *************************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Mostrar_Reporte
        ///DESCRIPCIÓN         : Muestra el reporte en pantalla.
        ///PARÁMETROS          : Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
        ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 29/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        /// *************************************************************************************
        protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
        {
            String Pagina = "../../Reporte/";
            try
            {
                Pagina = Pagina + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
        }

    #endregion

    #region Grids

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Salidas_Express_RowDataBound
        ///DESCRIPCIÓN         : Calcula la sumatoria de una columna del DataGrid
        ///PROPIEDADES         : 
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 29/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Grid_Salidas_Express_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                try
                {
                    Sumatoria += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CANTIDAD"));
                }
                catch (Exception Ex)
                {
                }
            }
            else
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    e.Row.Cells[1].Text = "Total";
                    e.Row.Cells[2].Text = Sumatoria.ToString();
                }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Salidas_Almacen_RowDataBound
        ///DESCRIPCIÓN         : Calcula la sumatoria de una columna del DataGrid
        ///PROPIEDADES         : 
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 29/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Grid_Salidas_Almacen_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                try
                {
                    Sumatoria += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CANTIDAD"));
                }
                catch (Exception Ex)
                {
                }
            }
            else
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    e.Row.Cells[1].Text = "Total";
                    e.Row.Cells[2].Text = Sumatoria.ToString();
                }
        }

    #endregion

    #region Eventos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Reporte_Execel_Click
        ///DESCRIPCIÓN         : Genera un reporte de tipo Excel
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 29/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  : 
        ///*******************************************************************************
        protected void Btn_Reporte_Execel_Click(object sender, ImageClickEventArgs e)
        {
            if (Hdf_Clave.Value.ToString().Trim() != "")
            {
                try
                {
                    DataSet Ds_Consultas = new DataSet();
                    Cls_Cat_Tal_Inventario_Stock_Negocio Tarjeta_Negocio = new Cls_Cat_Tal_Inventario_Stock_Negocio();
                    Tarjeta_Negocio.P_Refaccion_ID = Hdf_Clave.Value;
                    
                    DataTable Dt_Salidas_Express = Tarjeta_Negocio.Consultar_Salidas_Express();
                    Dt_Salidas_Express.TableName = "DT_SALIDAS_EXPRESS";
                    Dt_Salidas_Express.Columns["COSTO_UNITARIO"].ColumnName = "COSTO UNITARIO";
                    Dt_Salidas_Express.Columns["COSTO_TOTAL"].ColumnName = "COSTO TOTAL";
                    Dt_Salidas_Express.Columns["FECHA"].ColumnName = "FECHA";
                    Dt_Salidas_Express.Columns["REALIZO_MOVIMIENTO"].ColumnName = "REALIZO";
                    Dt_Salidas_Express.Columns["REFACCIONES_CLAVE_NOMBRE"].ColumnName = "REFACCION";
                    Dt_Salidas_Express.Columns.RemoveAt(2);
                    Dt_Salidas_Express.Columns.RemoveAt(1);
                    Dt_Salidas_Express.Columns.RemoveAt(0);

                    DataTable Dt_Salidas_Almacen = Tarjeta_Negocio.Consultar_Salidas_Almacen();
                    Dt_Salidas_Almacen.TableName = "DT_SALIDAS_ALMACEN";
                    Dt_Salidas_Almacen.Columns["COSTO"].ColumnName = "COSTO UNITARIO";
                    Dt_Salidas_Almacen.Columns["IMPORTE"].ColumnName = "COSTO TOTAL";
                    Dt_Salidas_Almacen.Columns["REFACCIONES_CLAVE_NOMBRE"].ColumnName = "REFACCION";
                    Dt_Salidas_Almacen.Columns["REALIZO_MOVIMIENTO"].ColumnName = "REALIZO";
                    Dt_Salidas_Almacen.Columns.RemoveAt(7);
                    Dt_Salidas_Almacen.Columns.RemoveAt(6);
                    Dt_Salidas_Almacen.Columns.RemoveAt(4);
                    Dt_Salidas_Almacen.Columns.RemoveAt(1);
                    Dt_Salidas_Almacen.Columns.RemoveAt(0);

                    Ds_Consultas.Tables.Add(Dt_Salidas_Express.Copy());
                    Ds_Consultas.Tables.Add(Dt_Salidas_Almacen.Copy());
                    Pasar_DataSet_A_Excel(Ds_Consultas);
                }
                catch (Exception Ex)
                {
                    Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = "Es necesario introducir la clave para generar el reporte.";
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Reporte_PDF_Click
        ///DESCRIPCIÓN         : Genera un reporte de tipo PDF
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 29/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  : 
        ///*******************************************************************************
        protected void Btn_Reporte_PDF_Click(object sender, ImageClickEventArgs e)
        {
            if (Hdf_Clave.Value.ToString().Trim() != "")
            {
                try
                {
                    //Dataset esqueleto del Reporte
                    Ds_Rpt_Tal_Inventario_Stock Ds_Reporte = new Ds_Rpt_Tal_Inventario_Stock();

                    Cls_Cat_Tal_Refacciones_Negocio Refacciones_Negocio = new Cls_Cat_Tal_Refacciones_Negocio();
                    Refacciones_Negocio.P_Filtro = Hdf_Clave.Value;
                    DataTable Dt_Generales = Refacciones_Negocio.Consultar_Refaccion();
                    Dt_Generales.Columns["NOMBRE"].ColumnName = "NOMBR";
                    Dt_Generales.Columns["DESCRIPCION"].ColumnName = "NOMBRE";
                    Dt_Generales.TableName = "DT_GENERALES";

                    Cls_Cat_Tal_Inventario_Stock_Negocio Inventario_Negocio = new Cls_Cat_Tal_Inventario_Stock_Negocio();
                    Inventario_Negocio.P_Refaccion_ID = Hdf_Clave.Value.ToString().Trim();
                    DataTable Dt_Salidas_Express = Inventario_Negocio.Consultar_Salidas_Express();
                    Dt_Salidas_Express.Columns["REFACCIONES_CLAVE_NOMBRE"].ColumnName = "REFACCION";
                    Dt_Salidas_Express.Columns["REALIZO_MOVIMIENTO"].ColumnName = "REALIZO";
                    Dt_Salidas_Express.TableName = "DT_SALIDAS_EXPESS";

                    DataTable Dt_Salidas_Almacen = Inventario_Negocio.Consultar_Salidas_Almacen();
                    Dt_Salidas_Almacen.Columns["REFACCIONES_CLAVE_NOMBRE"].ColumnName = "REFACCION";
                    Dt_Salidas_Almacen.Columns["REALIZO_MOVIMIENTO"].ColumnName = "REALIZO";
                    Dt_Salidas_Almacen.Columns["COSTO"].ColumnName = "COSTO_UNITARIO";
                    Dt_Salidas_Almacen.Columns["IMPORTE"].ColumnName = "COSTO_TOTAL";
                    Dt_Salidas_Almacen.TableName = "DT_SALIDAS_ALMACEN";

                    DataTable Dt_Salidas = Ds_Reporte.Tables["DT_SALIDAS"].Clone();
                    //Dt_Salidas = Dt_Salidas_Express.Clone();
                    Dt_Salidas.TableName = "DT_SALIDAS";

                    DataTable Dt_Entradas = Ds_Reporte.Tables["DT_ENTRADAS"].Clone();
                    //Dt_Entradas = Dt_Salidas_Express.Clone();
                    Dt_Entradas.TableName = "DT_ENTRADAS";

                   
                    DataSet Ds_Consulta = new DataSet();
                    Ds_Consulta.Tables.Add(Dt_Generales.Copy());
                    Ds_Consulta.Tables.Add(Dt_Salidas_Express.Copy());
                    Ds_Consulta.Tables.Add(Dt_Salidas_Almacen.Copy());
                    Ds_Consulta.Tables.Add(Dt_Salidas.Copy());
                    Ds_Consulta.Tables.Add(Dt_Entradas.Copy());
                    Ds_Consulta.Tables.Add(new Cls_Tal_Parametros_Negocio().Obtener_Tabla_Reporte(this.Server));

                    ////Generar y lanzar el reporte
                    String Ruta_Reporte_Crystal = "Rpt_Tal_Inventario_Stock.rpt";
                    Generar_Reporte(Ds_Consulta, Ds_Reporte, Ruta_Reporte_Crystal);
                }
                catch (Exception Ex)
                {
                    Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = "Es necesario introducir la clave para generar el reporte.";
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Limpiar_Click
        ///DESCRIPCIÓN         : Limpia los controles del catalogo.
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 29/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  : 
        ///*******************************************************************************
        protected void Btn_Limpiar_Click(object sender, ImageClickEventArgs e)
        {
            Txt_Clave.Text = "";
            Txt_Comprometido.Text = "";
            Txt_Disponible.Text = "";
            Txt_Existencia.Text = "";
            Hdf_Clave.Value = "";
            Hdf_Nombre.Value = "";
            Grid_Salidas_Express.DataSource = new DataTable();
            Grid_Salidas_Express.DataBind();
            Grid_Salidas_Almacen.DataSource = new DataTable();
            Grid_Salidas_Almacen.DataBind();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Click
        ///DESCRIPCIÓN         : Lanza la Busqueda de acurdo al campo clave
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 29/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  : 
        ///*******************************************************************************
        protected void Btn_Busqueda_Click(object sender, ImageClickEventArgs e)
        {
            if (Txt_Clave.Text.Trim() != "")
            {
                try
                {
                    Hdf_Clave.Value = Txt_Clave.Text.Trim();
                    Cls_Cat_Tal_Inventario_Stock_Negocio Negocio = new Cls_Cat_Tal_Inventario_Stock_Negocio();
                    Negocio.P_Refaccion_ID = Hdf_Clave.Value;
                    Llenar_Grid_Salidas_Express(Negocio.Consultar_Salidas_Express());
                    Sumatoria = 0;
                    Llenar_Grid_Salidas_Almacen(Negocio.Consultar_Salidas_Almacen());

                    Cls_Cat_Tal_Refacciones_Negocio Refacciones_Negocio = new Cls_Cat_Tal_Refacciones_Negocio();
                    Refacciones_Negocio.P_Filtro = Hdf_Clave.Value;
                    DataTable Dt_Refaccion = Refacciones_Negocio.Consultar_Refaccion();

                    if (Dt_Refaccion.Rows.Count > 0)
                    {
                        Txt_Comprometido.Text = Dt_Refaccion.Rows[0]["COMPROMETIDO"].ToString();
                        Txt_Disponible.Text = Dt_Refaccion.Rows[0]["DISPONIBLE"].ToString();
                        Txt_Existencia.Text = Dt_Refaccion.Rows[0]["EXISTENCIA"].ToString();
                        Hdf_Nombre.Value = Dt_Refaccion.Rows[0]["NOMBRE"].ToString();
                    }
                }
                catch (Exception Ex)
                {
                    Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                    Lbl_Mensaje_Error.Text = "Se presento una Excepcion al Buscar [" + Ex.Message + "]";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = "Es necesario introducir la clave para buscar.";
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

    #endregion

}
