﻿<%@ Page Title="Niveles Mecanico" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Cat_Tal_Niveles_Mecanico.aspx.cs" Inherits="paginas_Taller_Municipal_Frm_Cat_Tal_Niveles_Mecanicos" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">

    <script type="text/javascript">
    window.onerror = new Function("return true");
        //Abrir una ventana modal
        function Abrir_Ventana_Modal(Url, Propiedades)
        {
            window.showModalDialog(Url, null, Propiedades);
        }
        function Validar_Longitud_Texto(Text_Box, Max_Longitud)
        {
            if (Text_Box.value.length > Max_Longitud) 
            {
                Text_Box.value = Text_Box.value.substring(0, Max_Longitud);
            }
        }
    </script>

    <script type="text/javascript" language="javascript">
    //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_sesiones.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion()
        {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para matener la sesión activa
        setInterval('MantenSesion()', <%=(int)(0.9*(Session.Timeout * 60000))%>);

        function formatCurrency(num) {
            var Combo = "";
            var Beneficio;
                num = num.toString().replace(/\$|\,/g,'');
                if(isNaN(num))
                num = "0";
                sign = (num == (num = Math.abs(num)));
                num = Math.floor(num*100+0.50000000001);
                cents = num%100;
                num = Math.floor(num/100).toString();
                if(cents<10)
                    cents = "0" + cents;
                    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
                    num = num.substring(0,num.length-(4*i+3))+','+
                    num.substring(num.length-(4*i+3));
                    return (((sign)?'':'-') + num + '.' + cents);
            }
        
    </script>
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" EnablePartialRendering="true" AsyncPostBackTimeout="9000">
    </cc1:ToolkitScriptManager>
    <%--Inicio formulario--%>
    <asp:UpdatePanel ID="Upd_Parametros_Predial" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--update progrees--%>
            <asp:UpdateProgress ID="Uprg_Progress" runat="server" AssociatedUpdatePanelID="Upd_Parametros_Predial" DisplayAfter="0">
            <ProgressTemplate>
            <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
            <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
            </ProgressTemplate>
            </asp:UpdateProgress>
            
            <div id="Div_Refacciones" style="background-color:#ffffff; width:100%; height:100%">
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                    <td colspan ="4" class="label_titulo">Catálogo de Niveles de Personal</td>
                    </tr>
                    <tr>
                    <td colspan="4">
                     <asp:Image ID="Img_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" />
                     <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" CssClass="estilo_fuente_mensaje_error" Text="" /><br />
                     <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                    </td>                        
                    </tr>
                    <tr class="barra_busqueda">
                    <td colspan="2" align="left" style="width:20%">
                    <asp:ImageButton ID="Btn_Nuevo" runat="server" 
                            ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" CssClass="Img_Button" 
                            onclick="Btn_Nuevo_Click"/>
                     <asp:ImageButton ID="Btn_Modificar" runat="server" 
                            ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" CssClass="Img_Button" 
                            onclick="Btn_Modificar_Click"/>
                     <asp:ImageButton ID="Btn_Eliminar" runat="server" 
                            ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png" CssClass="Img_Button" 
                            onclick="Btn_Eliminar_Click"/>
                     <asp:ImageButton ID="Btn_Salir" runat="server" 
                            ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" CssClass="Img_Button" 
                            onclick="Btn_Salir_Click"/>
                     </td>
                     <td colspan="2" align="right" valign="top" style="width:80%">
                        <table style="width: 80%;">
                            <tr>
                                <td style="vertical-align: top; text-align: right; width: 5%">                                    
                                </td>
                                <td style="vertical-align: top; text-align: right; width: 90%">
                                    Búsqueda:
                                    <asp:TextBox ID="Txt_Buscar" runat="server" MaxLength="100" ToolTip="Buscar"
                                        Width="180px"  />
                                    <cc1:TextBoxWatermarkExtender ID="WTE_Txt_Buscar" runat="server" WatermarkCssClass="watermarked"
                                        WatermarkText="<- Clave ó Nombre ->" TargetControlID="Txt_Buscar" />
                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Buscar" runat="server" TargetControlID="Txt_Buscar"
                                        FilterType="UppercaseLetters,LowercaseLetters,Numbers" />
                                </td>
                                <td style="vertical-align: top; text-align: right; width: 5%">
                                    <asp:ImageButton ID="Btn_Buscar" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                        ToolTip="Buscar" OnClick="Btn_Buscar_Click" />
                                </td>
                            </tr>
                        </table>
                     </td>
                    </tr>    
                    </table>

                    &nbsp;
                    <br />
                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                    <td style="width:18%">
                                Clave</td>
                            <td style="width:32%">
                                <asp:TextBox ID="Txt_Clave" runat="server" MaxLength="50" Enabled="false" 
                                    Style="text-transform: uppercase" Text="" Width="97%"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="Txt_Clave_FilteredTextBoxExtender" 
                                    runat="server" Enabled="True" 
                                    FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                    TargetControlID="Txt_Clave" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                </cc1:FilteredTextBoxExtender>
                        </td>
                            <td colspan="2">&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:18%">
                                Nombre</td>
                            <td colspan="3">
                                <asp:TextBox ID="Txt_Nombre" runat="server" Width="97%" Text="" MaxLength="50" Style="text-transform: uppercase"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="Txt_Nombre_FilteredTextBoxExtender" 
                                    runat="server" Enabled="True" TargetControlID="Txt_Nombre" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:18%">Estatus</td>
                            <td style="width:32%">
                                <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="94%">
                                    <asp:ListItem Text="&lt;SELECCIONE&gt;" Value="SELECCIONE" />
                                    <asp:ListItem Text="VIGENTE" Value="VIGENTE" />
                                    <asp:ListItem Text="BAJA" Value="BAJA" />
                                </asp:DropDownList>
                            </td>
                            <td style="width:18%">
                                &nbsp;</td>
                            <td style="width:32%">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:18%">
                                Descripcion</td>
                            <td colspan="3">
                                <asp:TextBox ID="Txt_Descripcion" runat="server" Height="60px" 
                                    Style="text-transform: uppercase" TextMode="MultiLine" Width="97%"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="Twe_Txt_Comentarios" runat="server" 
                                    TargetControlID="Txt_Descripcion" WatermarkCssClass="watermarked" 
                                    WatermarkText="Límite de Caractes 250">
                                </cc1:TextBoxWatermarkExtender>
                                <cc1:FilteredTextBoxExtender ID="Txt_Comentarios_FilteredTextBoxExtender" 
                                    runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                    TargetControlID="Txt_Descripcion" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div ID="Div_Datos_Especificos" runat="server" 
                                    style="background-color:#ffffff; width:100%; height:100%;">
                                    <table border="0" cellspacing="0" class="estilo_fuente" width="99%">
                                        <tr>
                                            <td>
                                                <hr ID="Hr3" runat="server" onclick="return Hr3_onclick() " style="width:99%; text-align:left;" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <asp:GridView ID="Grid_Niveles" runat="server" AllowPaging="true" 
                                    AutoGenerateColumns="False" CssClass="GridView_1" 
                                    GridLines="none" 
                                    DataKeyNames="NIVEL_ID"
                                    PageSize="10" Style="white-space:normal" Width="96%" 
                                    onpageindexchanging="Grid_Niveles_PageIndexChanging" 
                                    onselectedindexchanged="Grid_Niveles_SelectedIndexChanged">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                            ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                            <ItemStyle Width="5%" />
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="NIVEL_ID" HeaderText="Id" Visible="false">
                                            <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                            <ItemStyle HorizontalAlign="Left" Width="5%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CLAVE" HeaderText="Clave">
                                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                            <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NOMBRE" HeaderText="Nombre">
                                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                            <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ESTATUS" HeaderText="Estatus">
                                        <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                            <ItemStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                            </asp:BoundField>
                                        <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripción">
                                        <HeaderStyle HorizontalAlign="Left" Width="40%" />
                                            <ItemStyle HorizontalAlign="Left" Width="40%" Font-Size="X-Small"/>
                                        </asp:BoundField>
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:18%">&nbsp;</td>
                            <td style="width:32%">&nbsp;</td>
                            <td style="width:18%">&nbsp;</td>
                            <td style="width:32%">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:18%">&nbsp;</td>
                            <td style="width:32%">&nbsp;</td>
                            <td style="width:18%">&nbsp;</td>
                            <td style="width:32%">&nbsp;</td>
                        </tr>
                    </table>                    
                </div>
            
            </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

