﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Taller_Mecanico.Operacion_Manejo_Vales_Express.Negocio;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Catalogo_Taller_Servicios_Preventivos.Negocio;
using JAPAMI.Empleados.Negocios;
using JAPAMI.Catalogo_Taller_Refacciones.Negocio;
using JAPAMI.Catalogo_Compras_Unidades.Negocio;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;

public partial class paginas_Taller_Mecanico_Frm_Ope_Tal_Salidas_Express_Refacciones : System.Web.UI.Page
{
    #region Variables

        private const String Dt_Refacciones = "Dt_Refacciones";

    #endregion

    #region Page_Load
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN         : Metodo que se carga cada que ocurre un PostBack de la Página
        ///PROPIEDADES         :
        ///CREO                : Salvador Vazquez Camacho
        ///FECHA_CREO          : 26/Junio/2012 
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                Llenar_Combo_Unidades_Responsables();
                Cargar_Ventana_Emergente_Busqueda_Refacciones();
                Configuracion_Formulario(true);
                Llenar_Grid_Listado(0);
            }
            Div_Contenedor_Msj_Error.Visible = false;
        }
        
    #endregion

    #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
        ///DESCRIPCIÓN         : Carga una configuracion de los controles del Formulario
        ///PROPIEDADES         : 1. Estatus. Estatus en el que se cargara la configuración de los
        ///                         controles.
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Configuracion_Formulario(Boolean Estatus)
        {
            Btn_Nuevo.Visible = true;
            Btn_Nuevo.AlternateText = "Nuevo";
            Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
            Btn_Cancelar.Visible = true;
            Btn_Cancelar.AlternateText = "Cancelar";
            Btn_Cancelar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar.png";
            Btn_Salir.Enabled = true;

            Txt_Refaccion.Enabled = false;
            Txt_No_Economico.Enabled = false;
            Txt_Unidad_Responsable.Enabled = false;
            Txt_Datos_Vehiculo.Enabled = false;
            Txt_Placas.Enabled = false;
            Txt_Anio.Enabled = false;
            Txt_Usuario_Recibe.Enabled = false;

            Txt_No_Inventario.Enabled = !Estatus;
            Txt_Cantidad.Enabled = !Estatus;
            Txt_Observaciones.Enabled = !Estatus;
            Btn_Busqueda_Refaccion.Visible = !Estatus;
            Btn_Busqueda_Usuario.Visible = !Estatus;
            Btn_Busqueda_Vehiculo.Visible = !Estatus;
            Btn_Agregar_Refaccion.Visible = !Estatus;
            Btn_Imprimir.Visible = Estatus;
            Grid_Listado_Refacciones.Columns[4].Visible = !Estatus;

            Grid_Listado.SelectedIndex = (-1);
            if (Estatus) Tab_Contenedor_Salidas_Refacciones_Express.ActiveTabIndex = 0;
            else Tab_Contenedor_Salidas_Refacciones_Express.ActiveTabIndex = 1;
            //Configuracion_Acceso("Frm_Cat_Pat_Com_Zonas.aspx");
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Listado
        ///DESCRIPCIÓN         : Llena el Listado con una consulta que puede o no
        ///                      tener Filtros.
        ///PROPIEDADES         : 1. Pagina.  Pagina en la cual se mostrará el Grid_VIew
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Llenar_Grid_Listado(int Pagina)
        {
            try
            {
                Cls_Ope_Tal_Manejo_Vales_Express_Negocio Negocio = new Cls_Ope_Tal_Manejo_Vales_Express_Negocio();
                Grid_Listado.DataSource = Negocio.Consultar_Vales_Express();
                Grid_Listado.PageIndex = Pagina;
                Grid_Listado.DataBind();
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Refacciones
        ///DESCRIPCIÓN         : Llena el Grid de Refacciones
        ///PROPIEDADES         : 1. Pagina.  Pagina en la cual se mostrará el Grid_VIew
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************  
        private void Llenar_Listado_Refacciones(DataTable Dt_Refacciones_Agregar)
        {
            if (Dt_Refacciones_Agregar != null && Dt_Refacciones_Agregar.Rows.Count > 0)
            {
                Session[Dt_Refacciones] = Dt_Refacciones_Agregar;
            }
            else
            {
                Session.Remove(Dt_Refacciones);
            }
            Grid_Listado_Refacciones.DataSource = Dt_Refacciones_Agregar;
            Grid_Listado_Refacciones.DataBind();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Busqueda_Empleados
        ///DESCRIPCIÓN         : Llena el Grid con los empleados que cumplan el filtro
        ///PROPIEDADES         : 1. Pagina.  Pagina en la cual se mostrará el Grid_VIew
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Llenar_Grid_Busqueda_Empleados()
        {
            Grid_Busqueda_Empleados.SelectedIndex = (-1);
            Grid_Busqueda_Empleados.Columns[1].Visible = true;
            Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            if (Txt_Busqueda_No_Empleado.Text.Trim().Length > 0) { Negocio.P_No_Empleado = Txt_Busqueda_No_Empleado.Text.Trim(); }
            if (Txt_Busqueda_RFC.Text.Trim().Length > 0) { Negocio.P_RFC_Empleado = Txt_Busqueda_RFC.Text.Trim(); }
            if (Txt_Busqueda_Nombre_Empleado.Text.Trim().Length > 0) { Negocio.P_Nombre_Empleado = Txt_Busqueda_Nombre_Empleado.Text.Trim().ToUpper(); }
            if (Cmb_Busqueda_Dependencia.SelectedIndex > 0) { Negocio.P_Dependencia_ID = Cmb_Busqueda_Dependencia.SelectedItem.Value; }
            Grid_Busqueda_Empleados.DataSource = Negocio.Consultar_Empleados();
            Grid_Busqueda_Empleados.DataBind();
            Grid_Busqueda_Empleados.Columns[1].Visible = false;
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Cargar_Ventana_Emergente_Busqueda_Refacciones
        ///DESCRIPCIÓN         : Establece el evento onclik del control para abrir la ventana emergente
        ///PARAMETROS          :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Cargar_Ventana_Emergente_Busqueda_Refacciones()
        {
            String Ventana_Modal = "Abrir_Ventana_Modal('Ventanas_Emergentes/Frm_Busqueda_Refacciones_Stock.aspx";
            String Propiedades = ", 'center:yes;resizable:no;status:no;dialogWidth:680px;dialogHide:true;help:no;scroll:no');";
            Btn_Busqueda_Refaccion.Attributes.Add("onclick", Ventana_Modal + "?Fecha=False'" + Propiedades);
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Borrar_Ventana_Emergente_Busqueda_Refacciones
        ///DESCRIPCIÓN         : Ocultar la ventana emergente.
        ///PARAMETROS          :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Borrar_Ventana_Emergente_Busqueda_Refacciones()
        {
            Btn_Busqueda_Refaccion.Attributes.Clear();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Mostrar_Informacion_Empleado
        ///DESCRIPCIÓN         : Muestra los Generales del Empleado
        ///PARAMETROS          :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///******************************************************************************* 
        private void Mostrar_Informacion_Empleado(String Empleado_ID)
        {
            Hdf_Usuario_Recibe_ID.Value = "";
            Txt_Usuario_Recibe.Text = "";
            Cls_Cat_Empleados_Negocios Empleado_Negocio = new Cls_Cat_Empleados_Negocios();
            Empleado_Negocio.P_Empleado_ID = Empleado_ID;
            DataTable Dt_Datos_Empleado = Empleado_Negocio.Consulta_Empleados_General();
            Hdf_Usuario_Recibe_ID.Value = ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString().Trim() : null);
            Txt_Usuario_Recibe.Text += ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString().Trim() : null);
            Txt_Usuario_Recibe.Text += " - " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString().Trim() : null);
            Txt_Usuario_Recibe.Text = Txt_Usuario_Recibe.Text.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString().Trim() : null);
            Txt_Usuario_Recibe.Text = Txt_Usuario_Recibe.Text.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString().Trim() : null);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Unidades_Responsables
        ///DESCRIPCIÓN         : Se llena el Combo de las Unidades Responsables.
        ///PARAMETROS          :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///******************************************************************************* 
        private void Llenar_Combo_Unidades_Responsables()
        {
            Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            Negocio.P_Estatus = "ACTIVO";
            DataTable Dt_Dependencias = Negocio.Consultar_Unidades_Responsables();
            Cmb_Busqueda_Dependencia.DataSource = Dt_Dependencias;
            Cmb_Busqueda_Dependencia.DataTextField = "CLAVE_NOMBRE";
            Cmb_Busqueda_Dependencia.DataValueField = "DEPENDENCIA_ID";
            Cmb_Busqueda_Dependencia.DataBind();
            Cmb_Busqueda_Dependencia.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Limpiar_Catalogo
        ///DESCRIPCIÓN         : Limpia los controles del Formulario
        ///PROPIEDADES         : 
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Limpiar_Catalogo()
        {
            Hdf_No_Registro.Value = "";
            Hdf_Usuario_Recibe_ID.Value = "";
            Txt_Usuario_Recibe.Text = "";
            Txt_No_Registro.Text = "";
            Txt_Estatus.Text = "";
            Lbl_Unidad.Text = "";
            Txt_Observaciones.Text = "";
            Session.Remove(Dt_Refacciones);
            Limpiar_Datos_Vehiculo();
            Limpiar_Datos_Refacciones();
            Llenar_Listado_Refacciones(new DataTable());
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Limpiar_Datos_Vehiculo
        ///DESCRIPCIÓN         : Limpia los controles del Formulario de Vehiculos
        ///PROPIEDADES         : 
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Limpiar_Datos_Vehiculo() {
            Hdf_Dependencia_ID.Value = "";
            Hdf_Vehiculo_ID.Value = "";
            Txt_Anio.Text = "";
            Txt_Datos_Vehiculo.Text = "";
            Txt_No_Economico.Text = "";
            Txt_No_Inventario.Text = "";
            Txt_Placas.Text = "";
            Txt_Unidad_Responsable.Text = "";
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Limpiar_Datos_Refacciones
        ///DESCRIPCIÓN         : Limpia los controles de las Refacciones
        ///PROPIEDADES         : 
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Limpiar_Datos_Refacciones(){
            Hdf_Refaccion_ID.Value = "";
            Txt_Cantidad.Text = "";
            Txt_Refaccion.Text = "";
            Lbl_Unidad.Text = "";
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Vehiculo
        ///DESCRIPCIÓN         : Se cargan los Datos del Vehiculo Seleccionado.
        ///PROPIEDADES         : 
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Cargar_Datos_Vehiculo(String Vehiculo, String Tipo_Busqueda)
        {
            Limpiar_Datos_Vehiculo();
            Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            switch (Tipo_Busqueda)
            {
                case "NO_INVENTARIO":
                    Consulta_Negocio.P_No_Inventario = Vehiculo;
                    break;
                case "IDENTIFICADOR":
                    Consulta_Negocio.P_Vehiculo_ID = Vehiculo;
                    break;
                default: break;
            }
            Consulta_Negocio.P_Estatus = "VIGENTE";
            DataTable Dt_Vehiculo = Consulta_Negocio.Consultar_Vehiculos();
            if (Dt_Vehiculo.Rows.Count > 0)
            {
                Hdf_Vehiculo_ID.Value = Dt_Vehiculo.Rows[0]["VEHICULO_ID"].ToString().Trim();
                Txt_No_Inventario.Text = Dt_Vehiculo.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                Txt_No_Economico.Text = Dt_Vehiculo.Rows[0]["NO_ECONOMICO"].ToString().Trim();
                Txt_Datos_Vehiculo.Text = Dt_Vehiculo.Rows[0]["VEHICULO_DESCRIPCION"].ToString().Trim();
                Txt_Placas.Text = Dt_Vehiculo.Rows[0]["PLACAS"].ToString().Trim();
                Txt_Anio.Text = Dt_Vehiculo.Rows[0]["ANIO"].ToString().Trim();
                if (!String.IsNullOrEmpty(Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString()))
                {
                    Cls_Cat_Dependencias_Negocio Dep_Negocio = new Cls_Cat_Dependencias_Negocio();
                    Hdf_Dependencia_ID.Value = Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString().Trim();
                    Dep_Negocio.P_Dependencia_ID = Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString().Trim();
                    DataTable Dt_Dep = Dep_Negocio.Consulta_Dependencias();
                    if (Dt_Dep.Rows.Count > 0)
                    {
                        Txt_Unidad_Responsable.Text = Dt_Dep.Rows[0]["CLAVE_NOMBRE"].ToString();
                    }
                }
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = "El Vehículo no se encontro o no esta vigente.";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Mostrar_Datos
        ///DESCRIPCIÓN         : Mostrar Datos.
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Mostrar_Datos(String No_Registro_Seleccionado)
        {
            Cls_Ope_Tal_Manejo_Vales_Express_Negocio Vales_Negocio = new Cls_Ope_Tal_Manejo_Vales_Express_Negocio();
            Vales_Negocio.P_No_Registro = No_Registro_Seleccionado;
            Vales_Negocio = Vales_Negocio.Consultar_Detalles_Vales_Express();

            Txt_No_Registro.Text = Vales_Negocio.P_No_Registro;
            Txt_Estatus.Text = Vales_Negocio.P_Estatus;
            Hdf_Dependencia_ID.Value = Vales_Negocio.P_Dependencia_ID;
            Hdf_Vehiculo_ID.Value = Vales_Negocio.P_Vehiculo_ID;
            Hdf_Usuario_Recibe_ID.Value = Vales_Negocio.P_Usuario_Recibe_ID;

            Cls_Cat_Empleados_Negocios Empleado_Negocio = new Cls_Cat_Empleados_Negocios();
            Empleado_Negocio.P_Empleado_ID = Vales_Negocio.P_Usuario_Recibe_ID;
            DataTable Dt_Datos_Empleado = Empleado_Negocio.Consulta_Empleados_General();
            Hdf_Usuario_Recibe_ID.Value = ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString().Trim() : null);
            Txt_Usuario_Recibe.Text += ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString().Trim() : null);
            Txt_Usuario_Recibe.Text += " - " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString().Trim() : null);
            Txt_Usuario_Recibe.Text = Txt_Usuario_Recibe.Text.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString().Trim() : null);
            Txt_Usuario_Recibe.Text = Txt_Usuario_Recibe.Text.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString().Trim() : null);

            Txt_No_Inventario.Text = Vales_Negocio.P_Vehiculo_ID;
            Txt_Observaciones.Text = Vales_Negocio.P_Observaciones;
            Cargar_Datos_Vehiculo(Vales_Negocio.P_Vehiculo_ID, "IDENTIFICADOR");

            Cls_Ope_Tal_Manejo_Vales_Express_Negocio Detalle_Negocio = new Cls_Ope_Tal_Manejo_Vales_Express_Negocio();
            Detalle_Negocio.P_No_Registro = Vales_Negocio.P_No_Registro;
            Detalle_Negocio = Detalle_Negocio.Consultar_Detalles_Vales_Express();
            Llenar_Listado_Refacciones(Detalle_Negocio.P_Dt_Refacciones);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Datos_Generales
        ///DESCRIPCIÓN         : Obtiene los datos generales del Reporte
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Obtener_Datos_Generales(ref DataTable Dt_Generales)
        {
            DataRow Fila_Nueva = Dt_Generales.NewRow();
            Fila_Nueva["DEPENDENCIA"] = Txt_Unidad_Responsable.Text;
            Fila_Nueva["TIPO_VEHICULO"] = Txt_Datos_Vehiculo.Text.Trim();
            Fila_Nueva["NO_ECONOMICO"] = Txt_No_Economico.Text.Trim();
            Fila_Nueva["KILOMETRAJE"] = Txt_No_Economico.Text.Trim();
            Fila_Nueva["MODELO"] = Txt_Placas.Text.Trim();
            Fila_Nueva["PLACAS"] = Txt_Placas.Text.Trim();
            Fila_Nueva["NO_INVENTARIO"] = Convert.ToInt32(Txt_No_Inventario.Text.Trim());
            Fila_Nueva["COSTO"] = "";
            Fila_Nueva["OBSERVACIONES"] = Txt_Observaciones.Text;
            Fila_Nueva["FECHA"] = "";
            Fila_Nueva["AUTORIZO"] = "";
            Fila_Nueva["RECIBE"] = "";
            Dt_Generales.Rows.Add(Fila_Nueva);
        }

        #region Validaciones

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Componentes
            ///DESCRIPCIÓN         : Hace una validacion de que haya datos en los componentes antes de hacer
            ///                      una operación.
            ///PROPIEDADES         : 
            ///CREO                : Salvador Vázquez Camacho.
            ///FECHA_CREO          : 26/Junio/2012
            ///MODIFICO            :
            ///FECHA_MODIFICO      :
            ///CAUSA_MODIFICACIÓN  :
            ///*******************************************************************************
            private Boolean Validar_Componentes()
            {
                Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                String Mensaje_Error = "";
                Boolean Validacion = true;
                if (Hdf_Vehiculo_ID.Value.Trim().Length == 0)
                {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar al Vehiculo.";
                    Validacion = false;
                }
                if (Hdf_Dependencia_ID.Value.Trim().Length == 0)
                {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar una Dependencia.";
                    Validacion = false;
                }
                if (Hdf_Usuario_Recibe_ID.Value.Trim().Length == 0)
                {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar un Usuario.";
                    Validacion = false;
                }
                if (Session[Dt_Refacciones] == null)
                {
                    {
                        if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                        Mensaje_Error = Mensaje_Error + "+ Tener Refacciones seleccionadas para hacer la Operacion";
                        Validacion = false;
                    }
                    if (!Validacion)
                    {
                        Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                    return Validacion;
                }
                return Validacion;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Componentes
            ///DESCRIPCIÓN         : Hace una validacion de que haya datos en los componentes antes de hacer
            ///                      una operación.
            ///PROPIEDADES         : 
            ///CREO                : Salvador Vázquez Camacho.
            ///FECHA_CREO          : 26/Junio/2012
            ///MODIFICO            :
            ///FECHA_MODIFICO      :
            ///CAUSA_MODIFICACIÓN  :
            ///*******************************************************************************
            private Boolean Validar_Agregar_Refaccion()
            {
                Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                String Mensaje_Error = "";
                Boolean Validacion = true;
                if (Txt_Cantidad.Text.Trim().Length == 0)
                {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Intruducir una Cantidad";
                    Validacion = false;
                }
                if (Hdf_Refaccion_ID.Value.ToString().Trim() == "")
                {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar una Refacción";
                    Validacion = false;
                }
                if (!Validacion)
                {
                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
                    Div_Contenedor_Msj_Error.Visible = true;
                }
                return Validacion;
            }

        #endregion

    #endregion

    #region Grids

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_SelectedIndexChanged
        ///DESCRIPCIÓN         : Obtiene los datos de un Listado Seleccionada para mostrarlos a detalle
        ///PARAMETROS          :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Grid_Listado_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Grid_Listado.SelectedIndex > (-1))
                {
                    Limpiar_Catalogo();
                    String No_Registro = HttpUtility.HtmlDecode(Grid_Listado.SelectedRow.Cells[1].Text.Trim());
                    Hdf_No_Registro.Value = No_Registro;
                    Mostrar_Datos(No_Registro);
                    System.Threading.Thread.Sleep(500);
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_PageIndexChanging
        ///DESCRIPCIÓN         : Maneja la paginación del GridView
        ///PARAMETROS          :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Grid_Listado_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Grid_Listado.SelectedIndex = (-1);
                Llenar_Grid_Listado(e.NewPageIndex);
                Limpiar_Catalogo();
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged
        ///DESCRIPCIÓN         : Maneja el evento de cambio de Selección del GridView de Busqueda
        ///                      de empleados.
        ///PARAMETROS          :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Grid_Busqueda_Empleados_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Grid_Busqueda_Empleados.SelectedIndex > (-1))
                {
                    String Empleado_Seleccionado_ID = Grid_Busqueda_Empleados.SelectedRow.Cells[1].Text.Trim();
                    Mostrar_Informacion_Empleado(Empleado_Seleccionado_ID);
                    Grid_Busqueda_Empleados.SelectedIndex = (-1);
                    MPE_Listado_Empleados.Hide();
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Empleados_PageIndexChanging
        ///DESCRIPCIÓN         : Maneja el evento de cambio de Página del GridView de Busqueda
        ///                      de empleados.
        ///PARAMETROS          :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Grid_Busqueda_Empleados_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Grid_Busqueda_Empleados.PageIndex = e.NewPageIndex;
                Llenar_Grid_Busqueda_Empleados();
                MPE_Listado_Empleados.Show();
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Refacciones_RowDataBound
        ///DESCRIPCIÓN         : Evento de llenado de datos en el Grid
        ///PARAMETROS          :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Grid_Listado_Refacciones_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                Int32 No_Fila = e.Row.RowIndex;
                if (e.Row.FindControl("Btn_Quitar_Refaccion") != null)
                {
                    ImageButton Btn_Quitar_Refaccion = (ImageButton)e.Row.FindControl("Btn_Quitar_Refaccion");
                    Btn_Quitar_Refaccion.CommandArgument = No_Fila.ToString();
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                Lbl_Mensaje_Error.Text = "[" + Ex.Message + "]";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

    #endregion

    #region Eventos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
        ///DESCRIPCIÓN         : Deja los componentes listos para dar de Alta un registro.
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  : 
        ///*******************************************************************************
        protected void Btn_Nuevo_Click(object sender, EventArgs e)
        {
            try
            {
                if (Btn_Nuevo.AlternateText.Equals("Nuevo"))
                {
                    Configuracion_Formulario(false);
                    Limpiar_Catalogo();
                    Btn_Nuevo.AlternateText = "Dar de Alta";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Salir.AlternateText = "Cancelar";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Cancelar.Visible = false;
                }
                else
                {
                    if (Validar_Componentes())
                    {
                        Cls_Ope_Tal_Manejo_Vales_Express_Negocio Negocio = new Cls_Ope_Tal_Manejo_Vales_Express_Negocio();
                        Negocio.P_Vehiculo_ID = Hdf_Vehiculo_ID.Value.Trim();
                        Negocio.P_Dependencia_ID = Hdf_Dependencia_ID.Value.Trim();
                        Negocio.P_Observaciones = Txt_Observaciones.Text.Trim();
                        Negocio.P_Usuario_Entrega_ID = Cls_Sessiones.Empleado_ID;
                        Negocio.P_Usuario_Recibe_ID = Hdf_Usuario_Recibe_ID.Value.Trim();
                        Negocio.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                        Negocio.P_Estatus = "APLICADO";
                        Negocio.P_Dt_Refacciones = (Session[Dt_Refacciones] != null) ? (DataTable)Session[Dt_Refacciones] : new DataTable();
                        Negocio = Negocio.Alta_Vales_Express();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo", "alert('Alta Exitosa');", true);
                        Hdf_No_Registro.Value = Negocio.P_No_Registro;
                        Btn_Imprimir_Click(Btn_Imprimir, null);
                        Limpiar_Catalogo();
                        Configuracion_Formulario(true);
                        Llenar_Grid_Listado(Grid_Listado.PageIndex);
                        Btn_Nuevo.AlternateText = "Nuevo";
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                        Btn_Salir.AlternateText = "Salir";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    }
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Cancelar_Click
        ///DESCRIPCIÓN         : Deja los componentes listos para hacer la cancelacion.
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Cancelar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Btn_Cancelar.AlternateText.Equals("Cancelar"))
                {
                    if (Grid_Listado.Rows.Count > 0 && Grid_Listado.SelectedIndex > (-1))
                    {
                        if (Txt_Estatus.Text.Trim().Equals("APLICADO"))
                        {
                            Cls_Ope_Tal_Manejo_Vales_Express_Negocio Negocio = new Cls_Ope_Tal_Manejo_Vales_Express_Negocio();
                            Negocio.P_No_Registro = Hdf_No_Registro.Value.Trim();
                            Negocio.P_Vehiculo_ID = Hdf_Vehiculo_ID.Value.Trim();
                            Negocio.P_Usuario_Entrega_ID = Cls_Sessiones.Empleado_ID;
                            Negocio.P_Usuario_Recibe_ID = Hdf_Usuario_Recibe_ID.Value.Trim();
                            Negocio.P_Dependencia_ID = Hdf_Dependencia_ID.Value.Trim();
                            Negocio.P_Observaciones = Txt_Observaciones.Text.Trim();
                            Negocio.P_Estatus = "CANCELADO";
                            Negocio.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado;
                            Negocio.P_Dt_Refacciones = (Session[Dt_Refacciones] != null) ? (DataTable)Session[Dt_Refacciones] : new DataTable();
                            Negocio = Negocio.Cancelar_Vales_Express();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo", "alert('Cancelación Exitosa');", true);
                            Hdf_No_Registro.Value = Negocio.P_No_Registro;

                            Configuracion_Formulario(true);
                            Limpiar_Catalogo();
                            Llenar_Grid_Listado(Grid_Listado.PageIndex);
                        } else {
                            Lbl_Ecabezado_Mensaje.Text = "El Movimiento ya esta Cancelado";
                            Lbl_Mensaje_Error.Text = "";
                            Div_Contenedor_Msj_Error.Visible = true;
                        }
                    } else {
                        Lbl_Ecabezado_Mensaje.Text = "Se debe Selecionar el Movimiento a Cancelar";
                        Lbl_Mensaje_Error.Text = "";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Imprimir
        ///DESCRIPCIÓN         : Muestra un reporte del vale de salida.
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Imprimir_Click(object sender, ImageClickEventArgs e)
        {

            if (Hdf_No_Registro.Value.ToString().Trim() != "")
            {
                try
                {
                    //Dataset esqueleto del Reporte
                    Ds_Rpt_Tal_Salidas_Express Ds_Reporte = new Ds_Rpt_Tal_Salidas_Express();

                    Cls_Ope_Tal_Manejo_Vales_Express_Negocio Neg = new Cls_Ope_Tal_Manejo_Vales_Express_Negocio();
                    Neg.P_No_Registro = Hdf_No_Registro.Value.ToString().Trim();

                    DataTable Dt_Generales = Neg.Consultar_Vales_Express();
                    Dt_Generales.TableName = "DT_GENERALES";
                    //Obtener_Datos_Generales(ref Dt_Generales);

                    DataTable Dt_Detalles = Neg.Consultar_Detalles_Vales_Express().P_Dt_Refacciones;
                    Dt_Detalles.Columns["REFACCIONES_CLAVE_NOMBRE"].ColumnName = "REFACCION_NOMBRE_CLAVE";
                    Dt_Detalles.TableName = "DT_REFACCIONES";

                    DataSet Ds_Consulta = new DataSet();
                    Ds_Consulta.Tables.Add(Dt_Generales.Copy());
                    Ds_Consulta.Tables.Add(Dt_Detalles.Copy());

                    ////Generar y lanzar el reporte
                    String Ruta_Reporte_Crystal = "Rtp_Tal_Salidas_Express.rpt";
                    Generar_Reporte(Ds_Consulta, Ds_Reporte, Ruta_Reporte_Crystal);
                }
                catch (Exception Ex)
                {
                    Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                Lbl_Mensaje_Error.Text = "+ Seleccinar un Movimiento";
                Div_Contenedor_Msj_Error.Visible = true;
            }
            
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN         : Cancela la operación que esta en proceso (Alta o Actualizar) o Sale del Formulario.
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, EventArgs e)
        {
            if (Btn_Salir.AlternateText.Equals("Salir"))
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Configuracion_Formulario(true);
                Limpiar_Catalogo();
                Btn_Nuevo.AlternateText = "Nuevo";
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                Btn_Salir.AlternateText = "Salir";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Refaccion_Click
        ///DESCRIPCIÓN         : Lanza la Busqueda de la Refaccion
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  : 
        ///*******************************************************************************
        protected void Btn_Busqueda_Refaccion_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["Tabla_Refacciones_Seleccionados"] != null) {
                DataTable Dt_Tabla_Refacciones_Seleccionados = (DataTable)Session["Tabla_Refacciones_Seleccionados"];
                if (Dt_Tabla_Refacciones_Seleccionados.Rows.Count > 0) {
                    Hdf_Refaccion_ID.Value = Dt_Tabla_Refacciones_Seleccionados.Rows[0]["REFACCION_ID"].ToString();
                    Txt_Refaccion.Text = Dt_Tabla_Refacciones_Seleccionados.Rows[0]["CLAVE"].ToString();
                    Txt_Refaccion.Text += " - " + Dt_Tabla_Refacciones_Seleccionados.Rows[0]["NOMBRE"].ToString();
                    Txt_Cantidad.Text = Dt_Tabla_Refacciones_Seleccionados.Rows[0]["CANTIDAD"].ToString();
                    if (!String.IsNullOrEmpty(Dt_Tabla_Refacciones_Seleccionados.Rows[0]["UNIDAD_ID"].ToString())) {
                        Cls_Cat_Com_Unidades_Negocio Uni_Neg = new Cls_Cat_Com_Unidades_Negocio();
                        Uni_Neg.P_Unidad_ID = Dt_Tabla_Refacciones_Seleccionados.Rows[0]["UNIDAD_ID"].ToString();
                        DataTable Dt_Unidades = Uni_Neg.Consulta_Unidades();
                        if (Dt_Unidades != null && Dt_Unidades.Rows.Count > 0) {
                            Lbl_Unidad.Text = Dt_Unidades.Rows[0][Cat_Com_Unidades.Campo_Abreviatura].ToString();
                        }
                    }
                }
            }
            Session.Remove("Tabla_Refacciones_Seleccionados");
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Vehiculo_Click
        ///DESCRIPCIÓN         : Busca el Vehiculo por el No. Inventario.
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Busqueda_Vehiculo_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Txt_No_Inventario.Text.Trim().Length > 0)
                {
                    Cargar_Datos_Vehiculo(Txt_No_Inventario.Text.Trim(), "NO_INVENTARIO");
                }
                else
                {
                    Lbl_Ecabezado_Mensaje.Text = "Es necesario introducir el No. de Inventario del Vehículo.";
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                Lbl_Mensaje_Error.Text = "Se presento una Excepcion al Buscar [" + Ex.Message + "]";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Usuario_Click
        ///DESCRIPCIÓN         : Ejecuta la Busqueda Avanzada para el Usuario que recibe.
        ///PARAMETROS          :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************    
        protected void Btn_Busqueda_Usuario_Click(object sender, EventArgs e)
        {
            try
            {
                Grid_Busqueda_Empleados.PageIndex = 0;
                Llenar_Grid_Busqueda_Empleados();
                MPE_Listado_Empleados.Show();
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Empleados_Click
        ///DESCRIPCIÓN         : Ejecuta la Busqueda Avanzada para el Usuario que Recibe.
        ///PARAMETROS          :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 26/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************    
        protected void Btn_Busqueda_Empleados_Click(object sender, EventArgs e)
        {
            try
            {
                Grid_Busqueda_Empleados.PageIndex = 0;
                Llenar_Grid_Busqueda_Empleados();
                MPE_Listado_Empleados.Show();
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Quitar_Refaccion_Click
        ///DESCRIPCIÓN         : Quita la UR del Listado
        ///PARAMETROS          :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 27/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Quitar_Refaccion_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton Btn_Quitar_Refaccion= (ImageButton)sender;
                if (!String.IsNullOrEmpty(Btn_Quitar_Refaccion.CommandArgument))
                {
                    Int32 No_Fila = Convert.ToInt32(Btn_Quitar_Refaccion.CommandArgument);
                    DataTable Dt_Refacciones_Quitar = (Session[Dt_Refacciones] != null) ? ((DataTable)Session[Dt_Refacciones]) : new DataTable();
                    if (Dt_Refacciones_Quitar.Rows.Count > 0)
                    {
                        Dt_Refacciones_Quitar.Rows.RemoveAt(No_Fila);
                        Llenar_Listado_Refacciones(Dt_Refacciones_Quitar);
                    }
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                Lbl_Mensaje_Error.Text = "[" + Ex.Message + "]";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Agregar_Refaccion_Click
        ///DESCRIPCIÓN         : Agraga la UR al Listado
        ///PARAMETROS          :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 27/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void  Btn_Agregar_Refaccion_Click(object sender, EventArgs e)
        {
            if (Validar_Agregar_Refaccion())
            {
                DataTable Tabla = (DataTable)Grid_Listado_Refacciones.DataSource;
                if (Tabla == null)
                {
                    if (Session[Dt_Refacciones] == null)
                    {
                        Tabla = new DataTable(Dt_Refacciones);
                        Tabla.Columns.Add("NO_DETALLE", Type.GetType("System.String"));
                        Tabla.Columns.Add("NO_REGISTRO", Type.GetType("System.String"));
                        Tabla.Columns.Add("REFACCION_ID", Type.GetType("System.String"));
                        Tabla.Columns.Add("CANTIDAD", Type.GetType("System.Int32"));
                        Tabla.Columns.Add("COSTO_UNITARIO", Type.GetType("System.Double"));
                        Tabla.Columns.Add("COSTO_TOTAL", Type.GetType("System.Double"));
                        Tabla.Columns.Add("REFACCIONES_CLAVE_NOMBRE", Type.GetType("System.String"));
                    }
                    else
                    {
                        Tabla = (DataTable)Session[Dt_Refacciones];
                    }
                }
                DataRow Fila = Tabla.NewRow();


                String Mi_SQL = "SELECT " + Cat_Tal_Refacciones.Campo_Costo_Unitario;
                Mi_SQL = Mi_SQL + " FROM " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Hdf_Refaccion_ID.Value + "'";
                SqlDataReader Data_Reader;
                Double Unitario = 0;

                Data_Reader = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                while (Data_Reader.Read())
                {
                    Unitario = Convert.ToDouble(Data_Reader[Cat_Tal_Refacciones.Campo_Costo_Unitario].ToString());
                }

                Fila["NO_DETALLE"] = 0;
                Fila["NO_REGISTRO"] = 0;
                Fila["REFACCION_ID"] = Hdf_Refaccion_ID.Value;
                Fila["CANTIDAD"] = Convert.ToInt32(Txt_Cantidad.Text);
                Fila["COSTO_UNITARIO"] = Unitario;
                Fila["COSTO_TOTAL"] = Convert.ToDouble(Convert.ToDouble(Txt_Cantidad.Text) * Unitario);
                Fila["REFACCIONES_CLAVE_NOMBRE"] = Txt_Refaccion.Text.Trim();

                bool bandera = true;
                foreach (DataRow Dr_Renglon in Tabla.Rows)
                {
                    if (Dr_Renglon["REFACCION_ID"].ToString() == Hdf_Refaccion_ID.Value.ToString())
                        bandera = false;
                }

                if (bandera)
                    Tabla.Rows.Add(Fila);
                else
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "aa", "alert('La Refaccion ya se encuetra agregada');", true);
                Llenar_Listado_Refacciones(Tabla);
                Limpiar_Datos_Refacciones();
            }
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
        ///DESCRIPCIÓN         : caraga el data set fisico con el cual se genera el Reporte especificado
        ///PARAMETROS          :  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
        ///                       2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
        ///                       3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 27/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Generar_Reporte(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte)
        {
            ReportDocument Reporte = new ReportDocument();
            String File_Path = Server.MapPath("../Rpt/Taller_Mecanico/" + Nombre_Reporte);
            Reporte.Load(File_Path);
            String Nombre_Reporte_Generar = "Rpt_Tal_Salida_Express_" + Cls_Sessiones.No_Empleado + "_" + Convert.ToString(DateTime.Now.ToString("yyyy'-'MM'-'dd'_t'HH'-'mm'-'ss")) + ".pdf";
            String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
            Ds_Reporte = Data_Set_Consulta_DB;
            Reporte.SetDataSource(Ds_Reporte);
            ExportOptions Export_Options = new ExportOptions();
            DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
            Disk_File_Destination_Options.DiskFileName = Server.MapPath(Ruta);
            Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
            Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
            Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
            Reporte.Export(Export_Options);
            Mostrar_Reporte(Nombre_Reporte_Generar, "PDF");
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
        }

        /// *************************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Mostrar_Reporte
        ///DESCRIPCIÓN         : Muestra el reporte en pantalla.
        ///PARÁMETROS          : Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
        ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 27/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        /// *************************************************************************************
        protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
        {
            String Pagina = "../../Reporte/";
            try
            {
                Pagina = Pagina + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
        }

    #endregion
}
