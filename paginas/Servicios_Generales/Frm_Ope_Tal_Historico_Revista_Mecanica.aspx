﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Tal_Historico_Revista_Mecanica.aspx.cs" Inherits="paginas_Taller_Mecanico_Frm_Ope_Tal_Historico_Revista_Mecanica" Title="Historico de Revistas Mécanicas" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" language="javascript">
        function Limpiar_Ctlr_Busqueda(){
            document.getElementById("<%=Txt_Folio_Solicitud_Busqueda.ClientID%>").value="";
            document.getElementById("<%=Cmb_Unidad_Responsable_Busqueda.ClientID%>").value="";     
            document.getElementById("<%=Txt_Numero_Inventario_Busqueda.ClientID%>").value="";          
            document.getElementById("<%=Txt_Numero_Economico_Busqueda.ClientID%>").value="";            
            document.getElementById("<%=Txt_Fecha_Recepcion_Inicial.ClientID%>").value="";              
            document.getElementById("<%=Txt_Fecha_Recepcion_Final.ClientID%>").value="";      
            document.getElementById("<%=Cmb_Filtrado_Estatus.ClientID%>").value="";                         
            return false;
        } 
    </script>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    
        <cc1:ToolkitScriptManager ID="ScptM_Catalogo" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />  
     
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Loading" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>                     
            </asp:UpdateProgress>
            <div id="Div_Area_Trabajo" style="background-color:#ffffff; width:100%; height:100%;">
                <center>
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo" colspan="2">Historico de Revistas Mecanicas</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                          <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;">              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top">
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                            </table>                   
                          </div>                
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>                        
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" style="width:50%;">
                            <asp:ImageButton ID="Btn_Imprimir_Solicitud_Servicio" runat="server" ImageUrl="~/paginas/imagenes/gridview/grid_print.png" Width="24px" CssClass="Img_Button" AlternateText="Imprimir" ToolTip="Imprimir Solicitud" OnClick="Btn_Imprimir_Solicitud_Servicio_Click" />
                            <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" Width="24px" CssClass="Img_Button" AlternateText="Salir" OnClick="Btn_Salir_Click" />
                        </td>
                        <td style="width:50%;">&nbsp;</td>                        
                    </tr>
                </table>   
                <br />
                <div id="Div_Listado_Solicitudes" runat="server" style="width:100%;">
                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                            <td colspan="4">
                                <div id="Div_Filtros_Busqueda" runat="server" style="border-style:outset; width:98%;">
                                    <table width="100%"  border="0" cellspacing="0" class="estilo_fuente">
                                         <tr>
                                            <td colspan="4">
                                                <asp:Label ID="Lbl_Leyenda_Busqueda" runat="server" Width="100%" Text="FILTROS PARA EL LISTADO" ForeColor="White" BackColor="Black" Font-Bold="true" style="text-align:center;"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                             <td style="width:15%;"><asp:Label ID="Lbl_Folio_Solicitud_Busqueda" runat="server" Text="Folio"></asp:Label></td>
                                             <td style="width:35%;">
                                                 <asp:TextBox ID="Txt_Folio_Solicitud_Busqueda" runat="server" Width="96%" MaxLength="10"></asp:TextBox>
                                                 <cc1:FilteredTextBoxExtender ID="FTE_Txt_Folio_Solicitud_Busqueda" runat="server" TargetControlID="Txt_Folio_Solicitud_Busqueda" FilterType="Numbers">
                                                 </cc1:FilteredTextBoxExtender>
                                             </td>
                                             <td style="width:15%;">&nbsp;</td>
                                             <td style="width:35%;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:Label ID="Lbl_Unidad_Responsable_Busqueda" runat="server" Text="Unidad Responsable"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:DropDownList ID="Cmb_Unidad_Responsable_Busqueda" runat="server" Width="99.5%">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                             <td style="width:15%;"><asp:Label ID="Lbl_Numero_Inventario_Busqueda" runat="server" Text="No. Inventario"></asp:Label></td>
                                             <td style="width:35%;">
                                                 <asp:TextBox ID="Txt_Numero_Inventario_Busqueda" runat="server" Width="96%"></asp:TextBox>
                                                 <cc1:FilteredTextBoxExtender ID="FTE_Txt_Numero_Inventario_Busqueda" runat="server" TargetControlID="Txt_Numero_Inventario_Busqueda" FilterType="Numbers">
                                                 </cc1:FilteredTextBoxExtender>
                                             </td>
                                             <td style="width:15%;"> &nbsp;&nbsp; <asp:Label ID="Lbl_Numero_Economico_Busqueda" runat="server" Text="No. Económico"></asp:Label>
                                            </td>
                                             <td style="width:35%;">
                                                 <asp:TextBox ID="Txt_Numero_Economico_Busqueda" runat="server" Width="96%"></asp:TextBox>
                                                 <cc1:FilteredTextBoxExtender ID="FTE_Txt_Numero_Economico_Busqueda" runat="server" TargetControlID="Txt_Numero_Economico_Busqueda" FilterType="UppercaseLetters, LowercaseLetters, Custom, Numbers" ValidChars="ñÑ -_*" InvalidChars="'%">
                                                 </cc1:FilteredTextBoxExtender>
                                             </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4"><hr /></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="text-align:center;">
                                                <asp:Label ID="Lbl_Leyenda_Fecha_Recepcion" runat="server" Text="Rango en Fecha de Recepción"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                             <td style="width:15%;"><asp:Label ID="Lbl_Fecha_Recepcion_Inicial" runat="server" Text="Fecha Inicial"></asp:Label></td>
                                             <td style="width:35%;">
                                                <asp:TextBox ID="Txt_Fecha_Recepcion_Inicial" runat="server" Width="85%" Enabled="false"></asp:TextBox>
                                                <asp:ImageButton ID="Btn_Fecha_Recepcion_Inicial" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                                <cc1:CalendarExtender ID="CE_Txt_Fecha_Recepcion_Inicial" runat="server" TargetControlID="Txt_Fecha_Recepcion_Inicial" PopupButtonID="Btn_Fecha_Recepcion_Inicial" Format="dd/MMM/yyyy">
                                                </cc1:CalendarExtender>  
                                             </td>
                                             <td style="width:15%;"> &nbsp;&nbsp;<asp:Label ID="Lbl_Fecha_Recepcion_Final" runat="server" Text="Fecha Final"></asp:Label></td>
                                             <td style="width:35%;">
                                                <asp:TextBox ID="Txt_Fecha_Recepcion_Final" runat="server" Width="85%" Enabled="false"></asp:TextBox>
                                                <asp:ImageButton ID="Btn_Fecha_Recepcion_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                                <cc1:CalendarExtender ID="CE_Txt_Fecha_Recepcion_Final" runat="server" TargetControlID="Txt_Fecha_Recepcion_Final" PopupButtonID="Btn_Fecha_Recepcion_Final" Format="dd/MMM/yyyy">
                                                </cc1:CalendarExtender>  
                                             </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4"><hr /></td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%;"><asp:Label ID="Lbl_Filtrado_Estatus" runat="server" Text="Estatus"></asp:Label></td>
                                            <td style="width:35%;">
                                                 <asp:DropDownList ID="Cmb_Filtrado_Estatus" runat="server" Width="98%">
                                                    <asp:ListItem Value="">&lt; - TODAS - &gt;</asp:ListItem>
                                                    <asp:ListItem Value="PENDIENTE">PENDIENTE</asp:ListItem>
                                                    <asp:ListItem Value="RECHAZADA">RECHAZADA</asp:ListItem>
                                                    <asp:ListItem Value="CANCELADA">CANCELADA</asp:ListItem>
                                                    <asp:ListItem Value="AUTORIZADA">AUTORIZADA</asp:ListItem>
                                                    <asp:ListItem Value="EN_REPARACION">EN REPARACIÓN</asp:ListItem>
                                                    <asp:ListItem Value="ENTREGADO">ENTREGADO</asp:ListItem>
                                                 </asp:DropDownList>
                                            </td>
                                            <td colspan="2" style="text-align:right;">
                                                <asp:ImageButton ID="Btn_Actualizar_Listado" runat="server" ToolTip="Actualizar Listado" AlternateText="Actualizar Listado" ImageUrl="~/paginas/imagenes/paginas/actualizar_detalle.png" Width="16px" OnClick="Btn_Actualizar_Listado_Click" />
                                                <asp:ImageButton ID="Btn_Limpiar_Filtros_Listado" runat="server" ToolTip="Limpiar Filtros Listado" AlternateText="Limpiar Filtros Listado" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png" Width="16px" Height="16px"  OnClientClick="javascript:return Limpiar_Ctlr_Busqueda();"/> 
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <asp:GridView ID="Grid_Listado_Solicitudes" runat="server" CssClass="GridView_1"
                        AutoGenerateColumns="False" AllowPaging="True" PageSize="20" Width="99%"
                        GridLines= "None" EmptyDataText="No se Encontrarón Solicitudes."
                        OnPageIndexChanging="Grid_Listado_Solicitudes_PageIndexChanging"
                        OnSelectedIndexChanged="Grid_Listado_Solicitudes_SelectedIndexChanged"
                        DataKeyNames="NO_SOLICITUD,ESTATUS">
                        <RowStyle CssClass="GridItem" />
                        <Columns>
                            <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png" >
                                <ItemStyle Width="30px" />
                            </asp:ButtonField>
                            <asp:BoundField DataField="NO_SOLICITUD" HeaderText="NO_SOLICITUD" SortExpression="NO_SOLICITUD">
                                <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FOLIO" HeaderText="FOLIO" SortExpression="FOLIO">
                                <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FECHA_ELABORACION" HeaderText="Fecha Elaboración" SortExpression="FECHA_ELABORACION" DataFormatString="{0:dd/MMM/yyyy}">
                                <ItemStyle Width="160px" HorizontalAlign="Center" Font-Size="X-Small"/>
                            </asp:BoundField>
                            <asp:BoundField DataField="FECHA_RECEPCION_REAL" HeaderText="Fecha Recepción" SortExpression="FECHA_RECEPCION_REAL" DataFormatString="{0:dd/MMM/yyyy}">
                                <ItemStyle Width="160px" HorizontalAlign="Center" Font-Size="X-Small"/>
                            </asp:BoundField>
                            <asp:BoundField DataField="TIPO_SERVICIO" HeaderText="Tipo Servicio" SortExpression="TIPO_SERVICIO">
                                <ItemStyle Width="160px" HorizontalAlign="Center" Font-Size="X-Small"/>
                            </asp:BoundField>
                            <asp:BoundField DataField="NO_INVENTARIO" HeaderText="No. Inventario" SortExpression="NO_INVENTARIO">
                                <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small"/>
                            </asp:BoundField>
                            <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS">
                                <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small"/>
                            </asp:BoundField>
                        </Columns>
                        <PagerStyle CssClass="GridHeader" />
                        <SelectedRowStyle CssClass="GridSelected" />
                        <HeaderStyle CssClass="GridHeader" />                                
                        <AlternatingRowStyle CssClass="GridAltItem" />       
                    </asp:GridView>
                </div>          
                <div id="Div_Campos" runat="server" style="width:100%;">                
                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                            <td colspan="4">
                                <asp:HiddenField ID="Hdf_No_Solicitud" runat="server" />
                                <asp:HiddenField ID="Hdf_Folio_Solicitud" runat="server" />
                                <asp:HiddenField ID="Hdf_Empleado_Solicito_ID" runat="server" />
                                <asp:HiddenField ID="Hdf_Email" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;&nbsp;&nbsp;</td>
                            <td style="width:15%;">&nbsp;&nbsp;&nbsp;
                                <asp:Label ID="Lbl_Folio_Solicitud" runat="server" Text="Folio Solicitud" Font-Bold="true" ForeColor="Black"></asp:Label>
                            </td>
                            <td style="width:35%;">
                                <asp:TextBox ID="Txt_Folio_Solicitud" runat="server" Width="98%" ReadOnly="true" BorderStyle="Outset" style="text-align:center;" Font-Bold="true" ForeColor="Red"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%;">
                                <asp:Label ID="Lbl_Fecha_Elaboracion" runat="server" Text="Fecha Elaboración"></asp:Label>
                            </td>
                            <td style="width:35%;">
                                <asp:TextBox ID="Txt_Fecha_Elaboracion" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                            </td>
                            <td style="width:15%;">&nbsp;&nbsp;&nbsp;</td></td>
                            <td style="width:35%;">
                                <asp:TextBox ID="Txt_Estatus" runat="server" Width="98%" ReadOnly="true" BorderStyle="Outset" style="text-align:center;" Font-Bold="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%;">
                                <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text="Unidad Responsable"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="Cmb_Unidad_Responsable" runat="server" Width="100%">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%;">
                                <asp:Label ID="Lbl_Fecha_Recepcion" runat="server" Text="Recepción"></asp:Label>
                            </td>
                            <td style="width:35%;">
                                <asp:TextBox ID="Txt_Fecha_Recepcion" runat="server" Width="95%" Enabled="false"></asp:TextBox>
                            </td>
                            <td style="width:15%;">
                                &nbsp;&nbsp;  
                                <asp:Label ID="Lbl_Kilometraje" runat="server" Text="Kilometraje [Km]" ></asp:Label>
                            </td>
                            <td style="width:16%;">
                                <asp:TextBox ID="Txt_Kilometraje" runat="server" Width="95%"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Kilometraje" runat="server" TargetControlID="Txt_Kilometraje" ValidChars="." FilterType="Custom, Numbers">
                                </cc1:FilteredTextBoxExtender>    
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%;">
                                <asp:Label ID="Lbl_Correo_Electronico" runat="server" Text="Correo Electronico"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="Txt_Correo_Electronico" runat="server" Width="97%">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Panel ID="Pnl_Vehiculo_Seleccionado" runat="server" Width="99%" GroupingText="Vehículo para el Servicio">
                                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:HiddenField ID="Hdf_Vehiculo_ID" runat="server" />
                                                <asp:Label ID="Lbl_No_Inventario" runat="server" Text="No. Inventario"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_No_Inventario" runat="server" Width="98%" MaxLength="7"></asp:TextBox>
                                            </td>
                                            <td style="width:15%;">
                                                &nbsp;&nbsp;  
                                                <asp:Label ID="Lbl_No_Economico" runat="server" Text="No. Economico"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_No_Economico" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:Label ID="Lbl_Datos_Vehiculo" runat="server" Text="Vehículo"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="Txt_Datos_Vehiculo" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:Label ID="Lbl_Placas" runat="server" Text="Placas"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_Placas" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                            </td>
                                            <td style="width:15%;">
                                                &nbsp;&nbsp;  
                                                <asp:Label ID="Lbl_Anio" runat="server" Text="Año"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_Anio" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Panel ID="Pnl_Descripcion_Servicio" runat="server" Width="99%" GroupingText="Descripción del Servicio">
                                    <asp:TextBox ID="Txt_Descripcion_Servicio" runat="server" Rows="2" TextMode="MultiLine" Width="99%"></asp:TextBox>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%;">
                                &nbsp;<asp:Label ID="Lbl_Empleado_Presento" runat="server" Text="Presento"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="Txt_Empleado_Presento" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Panel ID="Pnl_Diagnostico_Servicio" runat="server" Width="99%" GroupingText="Diagnostico del Servicio">
                                    <asp:TextBox ID="Txt_Diagnostico_Servicio" runat="server" Rows="3" TextMode="MultiLine" Width="99%" Enabled="false"></asp:TextBox>
                                </asp:Panel>
                            </td>
                        </tr>                      
                        <tr>
                            <td colspan="4">
                                <center>
                                   <asp:Button ID="Btn_Mostrar_Detalle_Recepcion" runat="server"  Text="Mostrar Detalle Revisión" CssClass="button" CausesValidation="false"  Width="200px" OnClick="Btn_Mostrar_Detalle_Recepcion_Click" /> 
                                </center>
                            </td>                                                     
                        </tr>                    
                        <tr>
                            <td colspan="4">&nbsp;</td>                                                     
                        </tr>    
                    </table>
                    <br />
                    <table width="98%"  border="0" cellspacing="1" class="estilo_fuente">     
                        <tr>
                            <td colspan="2" style="background-color:Black; color:White; text-align:center;">FOTOS DE ENTRADA</td>   
                            <td colspan="2" style="background-color:Black; color:White; text-align:center;">FOTOS DE SALIDA</td>                                                     
                        </tr>                    
                        <tr>
                            <td style="color:White; background-color:Navy; text-align:center;">FRENTE</td>   
                            <td style="color:White; background-color:Navy; text-align:center;">ATRAS</td>  
                            <td style="color:White; background-color:Navy; text-align:center;">FRENTE</td>   
                            <td style="color:White; background-color:Navy; text-align:center;">ATRAS</td>                                                     
                        </tr>                       
                        <tr>
                            <td style="text-align:center;">
                                <a ID="Liga_Entrada_Frente" runat="server" href="" rel="facybox" target="_blank" style="text-align:center;">
                                    <img ID="Liga_Imagen_Entrada_Frente" runat="server" alt="" height="128" src="" width="128" />
                                </a>
                            </td>   
                            <td style="text-align:center;">
                                <a ID="Liga_Entrada_Atras" runat="server" href="" rel="facybox" target="_blank" style="text-align:center;">
                                    <img ID="Liga_Imagen_Entrada_Atras" runat="server" alt="" height="128" src="" width="128" />
                                </a>
                            </td>  
                            <td style="text-align:center;">
                                <a ID="Liga_Salida_Frente" runat="server" href="" rel="facybox" target="_blank" style="text-align:center;">
                                    <img ID="Liga_Imagen_Salida_Frente" runat="server" alt="" height="128" src="" width="128" />
                                </a>
                            </td>   
                            <td style="text-align:center;">
                                <a ID="Liga_Salida_Atras" runat="server" href="" rel="facybox" target="_blank" style="text-align:center;">
                                    <img ID="Liga_Imagen_Salida_Atras" runat="server" alt="" height="128" src="" width="128" />
                                </a>
                            </td>                                                    
                        </tr>                  
                        <tr>
                            <td style="color:White; background-color:Navy; text-align:center;">LADO IZQUIERDO</td>   
                            <td style="color:White; background-color:Navy; text-align:center;">LADO DERECHO</td>  
                            <td style="color:White; background-color:Navy; text-align:center;">LADO IZQUIERDO</td>   
                            <td style="color:White; background-color:Navy; text-align:center;">LADO DERECHO</td>                                                
                        </tr>                      
                        <tr>
                            <td style="text-align:center;">
                                <a ID="Liga_Entrada_Lado_Izquierdo" runat="server" href="" rel="facybox" target="_blank" style="text-align:center;">
                                    <img ID="Liga_Imagen_Entrada_Lado_Izquierdo" runat="server" alt="" height="128" src="" width="128" />
                                </a>
                            </td>   
                            <td style="text-align:center;">
                                <a ID="Liga_Entrada_Lado_Derecho" runat="server" href="" rel="facybox" target="_blank" style="text-align:center;">
                                    <img ID="Liga_Imagen_Entrada_Lado_Derecho" runat="server" alt="" height="128" src="" width="128" />
                                </a>
                            </td>  
                            <td style="text-align:center;">
                                <a ID="Liga_Salida_Lado_Izquierdo" runat="server" href="" rel="facybox" target="_blank" style="text-align:center;">
                                    <img ID="Liga_Imagen_Salida_Lado_Izquierdo" runat="server" alt="" height="128" src="" width="128" />
                                </a>
                            </td>   
                            <td style="text-align:center;">
                                <a ID="Liga_Salida_Lado_Derecho" runat="server" href="" rel="facybox" target="_blank" style="text-align:center;">
                                    <img ID="Liga_Imagen_Salida_Lado_Derecho" runat="server" alt="" height="128" src="" width="128" />
                                </a>
                            </td>                                                    
                        </tr> 
                    </table>
                </div>
                </center>
            </div>
            <br />
            <br />
            <br />
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:UpdatePanel ID="UpPnl_Aux_Listado_Detalles" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_MPE_Listado_Detalles" runat="server" Text="" style="display:none;"/>
                <cc1:ModalPopupExtender ID="MPE_Listado_Detalles" runat="server" 
                TargetControlID="Btn_Comodin_MPE_Listado_Detalles" PopupControlID="Pnl_Listado_Detalles" 
                CancelControlID="Btn_Cerrar_Ventana_Detalles" 
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter"/>  
        </ContentTemplate>
    </asp:UpdatePanel>   
    
    <asp:Panel ID="Pnl_Listado_Detalles" runat="server" CssClass="drag"  HorizontalAlign="Center" Width="850px" style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">                         
        <asp:Panel ID="Pnl_Listado_Detalles_Interno" runat="server" 
                style="cursor: move;background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
                <table width="99%">
                    <tr>
                        <td style="color:Black;font-size:12;font-weight:bold;">
                           <asp:Image ID="Img_Pnl_Listado_Detalles" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                             Detalles de Revista Mecanica
                        </td>
                        <td align="right" style="width:10%;">
                           <asp:ImageButton ID="Btn_Cerrar_Ventana_Detalles" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana"
                                ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png"/>  
                        </td>
                    </tr>
                </table>            
            </asp:Panel>                                                                          
           <div style="color: #5D7B9D">
             <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;" >                                    
                        <asp:UpdatePanel ID="Upnl_Listado_Detalles" runat="server">
                            <ContentTemplate>
                            
                                <asp:UpdateProgress ID="Progress_Upnl_Listado_Detalles" runat="server" AssociatedUpdatePanelID="Upnl_Listado_Detalles" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                        <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress"><img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>  
                                  <br />
                                  <div id="Div_Listado_Detalles" runat="server" style="border-style:outset; width:99%; height: 450px; overflow:auto;">
                                      <asp:GridView ID="Grid_Listado_Detalles_Recepcion" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                            ForeColor="#333333" GridLines="None" AllowPaging="True" Width="100%" 
                                            PageSize="100" EmptyDataText="No hay registros" >
                                            <RowStyle CssClass="GridItem" />
                                                <Columns>
                                                <asp:BoundField DataField="DETALLE_ID" HeaderText="DETALLE_ID" SortExpression="DETALLE_ID">
                                                    <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SUBDETALLE_ID" HeaderText="SUBDETALLE_ID" SortExpression="SUBDETALLE_ID">
                                                    <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CANT_UNIFICAR" HeaderText="CANT_UNIFICAR" SortExpression="CANT_UNIFICAR" NullDisplayText="0">
                                                    <ItemStyle Width="10px" HorizontalAlign="Center" Font-Size="X-Small"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="NOMBRE_DETALLE" HeaderText="Clasificación" SortExpression="NOMBRE_DETALLE">
                                                    <ItemStyle Width="140px" HorizontalAlign="Center" Font-Size="X-Small"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="NOMBRE_SUBDETALLE" HeaderText="Elemento Especifico" SortExpression="NOMBRE_SUBDETALLE">
                                                    <ItemStyle Width="140px" HorizontalAlign="Center" Font-Size="X-Small"/>
                                                </asp:BoundField>
                                                    <asp:BoundField DataField="VALOR" HeaderText="Revisión" SortExpression="VALOR">
                                                        <ItemStyle Width="140px" HorizontalAlign="Center" Font-Size="X-Small"/>
                                                    </asp:BoundField>
                                                </Columns>
                                            <PagerStyle CssClass="GridHeader" />
                                            <SelectedRowStyle CssClass="GridSelected" />
                                            <HeaderStyle CssClass="GridHeader" />
                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView> 
                                </div>                                                                                                                                                          
                            </ContentTemplate>                                                                   
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>                                                      
                    </td>
                </tr>
             </table>                                                   
           </div>                 
    </asp:Panel>  
    
</asp:Content>