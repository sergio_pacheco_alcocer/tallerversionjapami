﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Entradas_Vehiculos.Negocio;
using JAPAMI.Taller_Mecanico.Catalogo_Partes_Vehiculos.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Control_Patrimonial_Operacion_Vehiculos.Negocio;
using JAPAMI.Empleados.Negocios;
using System.Text.RegularExpressions;
using JAPAMI.Taller_Mecanico.Catalogo_Mecanicos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Preventivos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Correctivos.Negocio;
using JAPAMI.Taller_Mecanico.Administracion_Proveedores_Servicios_Bien_Mueble.Negocio;

public partial class paginas_Servicios_Generales_Frm_Ope_Tal_Asignacion_Proveedor_Servicio_Bien_Mueble : System.Web.UI.Page {
    
    #region Page_Load
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN: Carga la Pagina Inicial
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 17/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e) {
            Div_Contenedor_Msj_Error.Visible = false;
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack) {
                Llenar_Combo_Proveedores();
                Llenar_Combo_Gpos_Unidades_Responsables();
                Llenar_Combo_Unidades_Responsables();
                Llenar_Combo_Personal();
                Cmb_Mecanicos.SelectedIndex = Cmb_Mecanicos.Items.IndexOf(Cmb_Mecanicos.Items.FindByValue(Cls_Sessiones.Empleado_ID));
                Grid_Listado_Servicios.PageIndex = 0;
                Llenar_Listado_Servicios();
                Configuracion_Formulario("INICIAL");
                Cmb_Unidad_Responsable.Enabled = false;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Grupo_Dependencia
        ///DESCRIPCIÓN: Consulta el Grupo al que pertenece una dependencia
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected String Consultar_Grupo_Dependencia(String Dependencia_ID) {
            String Grupo_ID = String.Empty;
            Cls_Ope_Tal_Consultas_Generales_Negocio Cls_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            Cls_Negocio.P_Dependencia_ID = Dependencia_ID;
            DataTable Dt_Grupos_Dependencias = Cls_Negocio.Consultar_Grupos_Unidades_Responsables();
            if (Dt_Grupos_Dependencias.Rows.Count > 0) Grupo_ID = Dt_Grupos_Dependencias.Rows[0]["GRUPO_DEPENDENCIA_ID"].ToString().Trim();
            return Grupo_ID;
        }


    #endregion

    #region Metodos

        #region Llenado de Campos [Combos, Listados, Vehiculos]
    
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Proveedores
            ///DESCRIPCIÓN: Se llena el Combo de los Proveedores
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 30/Abril/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Combo_Proveedores() {
                Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                DataTable Dt_Proveedores = Negocio.Consultar_Proveedores();
                Cmb_Proveedores.DataSource = Dt_Proveedores;
                Cmb_Proveedores.DataValueField = "PROVEEDOR_ID";
                Cmb_Proveedores.DataTextField = "NOMBRE_PROVEEDOR";
                Cmb_Proveedores.DataBind();
                Cmb_Proveedores.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
            }    

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Gpos_Unidades_Responsables
            ///DESCRIPCIÓN: Se llena el Combo de los Gpos. Unidades Responsables.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 30/Abril/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Combo_Gpos_Unidades_Responsables() {
                Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                Negocio.P_Estatus = "ACTIVO";
                DataTable Dt_Gpos_Dependencias = Negocio.Consultar_Grupos_Unidades_Responsables();
                Cmb_Grupo_UR.DataSource = Dt_Gpos_Dependencias;
                Cmb_Grupo_UR.DataTextField = "CLAVE_NOMBRE";
                Cmb_Grupo_UR.DataValueField = "GRUPO_DEPENDENCIA_ID";
                Cmb_Grupo_UR.DataBind();
                Cmb_Grupo_UR.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Unidades_Responsables
            ///DESCRIPCIÓN: Se llena el Combo de las Unidades Responsables.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Combo_Unidades_Responsables() {
                Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                Negocio.P_Estatus = "ACTIVO";
                DataTable Dt_Dependencias = Negocio.Consultar_Unidades_Responsables();
                Cmb_Unidad_Responsable.DataSource = Dt_Dependencias;
                Cmb_Unidad_Responsable.DataTextField = "CLAVE_NOMBRE";
                Cmb_Unidad_Responsable.DataValueField = "DEPENDENCIA_ID";
                Cmb_Unidad_Responsable.DataBind();
                Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Servicios
            ///DESCRIPCIÓN: Se llena el Listado de los Servicios para asignar el Mecanico.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Listado_Servicios() {
                Cls_Ope_Tal_Solicitud_Servicio_Negocio Negocio = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                Negocio.P_Estatus = "PRE_COTIZACION";
                if (Cmb_Mecanicos.SelectedIndex > 0) Negocio.P_Cotizador_ID = Cmb_Mecanicos.SelectedItem.Value;
                Negocio.P_Tipo_Bien = "BIEN_MUEBLE";
                DataTable Dt_Resultados = Negocio.Consultar_Listado_Solicitudes_Servicio();
                Grid_Listado_Servicios.Columns[1].Visible = true;
                Grid_Listado_Servicios.DataSource = Dt_Resultados;
                Grid_Listado_Servicios.DataBind();
                Grid_Listado_Servicios.Columns[1].Visible = false;                            
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Personal
            ///DESCRIPCIÓN: Se llena el Listado del Personal.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Combo_Personal() {
                Cls_Cat_Tal_Mecanicos_Negocio Negocio = new Cls_Cat_Tal_Mecanicos_Negocio();
                Negocio.P_Estatus = "VIGENTE";
                DataTable Dt_Resultados = Negocio.Consultar_Mecanicos();
                Cmb_Mecanicos.DataSource = Dt_Resultados;
                Cmb_Mecanicos.DataTextField = "NOMBRE_EMPLEADO";
                Cmb_Mecanicos.DataValueField = "MECANICO_ID";
                Cmb_Mecanicos.DataBind();
                Cmb_Mecanicos.Items.Insert(0, new ListItem("< - - - - - TODOS - - - - - >", ""));
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Bien_Mueble
            ///DESCRIPCIÓN: Se cargan los Datos del Bien Mueble Seleccionado.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Cargar_Datos_Bien_Mueble(String Bien_Mueble, String Tipo_Busqueda) {
                Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                switch (Tipo_Busqueda) { 
                    case "NO_INVENTARIO":
                        Consulta_Negocio.P_No_Inventario = Bien_Mueble;
                        break;
                    case "IDENTIFICADOR":
                        Consulta_Negocio.P_Bien_Mueble_ID = Bien_Mueble;
                        break;
                    default: break;
                }
                DataTable Dt_Bienes_Muebles = Consulta_Negocio.Consultar_Bien_Mueble();
                if (Dt_Bienes_Muebles.Rows.Count > 0) {
                    Hdf_Bien_Mueble_ID.Value = Dt_Bienes_Muebles.Rows[0]["BIEN_MUEBLE_ID"].ToString().Trim();
                    Txt_No_Inventario_BM.Text = Dt_Bienes_Muebles.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                    Txt_Numero_Serie_Bien.Text = Dt_Bienes_Muebles.Rows[0]["NUMERO_SERIE"].ToString().Trim();
                    Txt_Descripcion_Bien.Text = Dt_Bienes_Muebles.Rows[0]["DESCRIPCION_BIEN_MUEBLE"].ToString().Trim();
                    if (!String.IsNullOrEmpty(Dt_Bienes_Muebles.Rows[0]["DEPENDENCIA_ID"].ToString())) {
                        Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Bienes_Muebles.Rows[0]["DEPENDENCIA_ID"].ToString()));
                        Cmb_Grupo_UR.SelectedIndex = Cmb_Grupo_UR.Items.IndexOf(Cmb_Grupo_UR.Items.FindByValue(Consultar_Grupo_Dependencia(Cmb_Unidad_Responsable.SelectedItem.Value.Trim())));
                    } else {
                        Lbl_Mensaje_Error.Text = "ERROR [Puede ser que el Bien esté dado de Baja].";
                        Div_Contenedor_Msj_Error.Visible = true;
                        Cmb_Unidad_Responsable.SelectedIndex = 0;
                    }
                } else {
                    Lbl_Ecabezado_Mensaje.Text = "El Bien no se encontro.";
                    if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Lbl_Mensaje_Error.Text = "[Puede ser que no este asignado a la Unidad Responsable Seleccionada]."; }
                    else { Lbl_Mensaje_Error.Text = ""; }
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }

        #endregion

        #region Generales [Configuracion, Limpiar]
                
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
            ///DESCRIPCIÓN: Limpia los campos del Formulario.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Limpiar_Formulario() {
                Hdf_No_Solicitud.Value = "";
                Txt_Folio.Text = "";
                Txt_Fecha_Elaboracion.Text = "";
                Cmb_Tipo_Servicio.SelectedIndex = 0;
                Cmb_Unidad_Responsable.SelectedIndex = 0;
                Txt_Descripcion_Servicio.Text = "";
                Hdf_Bien_Mueble_ID.Value = "";
                Txt_No_Inventario_BM.Text = "";
                Txt_Descripcion_Bien.Text = "";
                Txt_Numero_Serie_Bien.Text = "";
                Grid_Listado_Proveedores.DataSource = new DataTable();
                Grid_Listado_Proveedores.DataBind();
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
            ///DESCRIPCIÓN: Se Habilitan y/o inhabilitan los campos dependendiendo de la Condicion.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Configuracion_Formulario(String Operacion) {
                switch (Operacion) {
                    case "INICIAL":
                        Div_Campos.Visible = false;
                        Div_Listado_Servicios.Visible = true;
                        Btn_Asignacion_Mecanico.Visible = false;
                        break;
                    case "OPERACION":
                        Div_Campos.Visible = true;
                        Div_Listado_Servicios.Visible = false;
                        Btn_Asignacion_Mecanico.Visible = true;
                        break;
                }
            }

        #endregion

        #region Clase de Negocio de Solicitudes [Registrar y Consulta]

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Mostrar_Registro
            ///DESCRIPCIÓN: Muestra el Registro en los campos.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 09/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Mostrar_Registro() {
                Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                Solicitud.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                Solicitud = Solicitud.Consultar_Detalles_Solicitud_Servicio();
                if (Solicitud.P_No_Solicitud > (-1)) {
                    Hdf_No_Solicitud.Value = Solicitud.P_No_Solicitud.ToString();
                    Txt_Folio.Text = Solicitud.P_Folio_Solicitud;
                    Txt_Fecha_Elaboracion.Text = String.Format("{0:dd/MMM/yyyy}", Solicitud.P_Fecha_Elaboracion);
                    Cmb_Tipo_Servicio.SelectedIndex = Cmb_Tipo_Servicio.Items.IndexOf(Cmb_Tipo_Servicio.Items.FindByValue(Solicitud.P_Tipo_Servicio));
                    Cmb_Grupo_UR.SelectedIndex = Cmb_Grupo_UR.Items.IndexOf(Cmb_Grupo_UR.Items.FindByValue(Consultar_Grupo_Dependencia(Solicitud.P_Dependencia_ID)));
                    Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Solicitud.P_Dependencia_ID));
                    Hdf_Bien_Mueble_ID.Value = Solicitud.P_Bien_ID;
                    Txt_Descripcion_Servicio.Text = Solicitud.P_Descripcion_Servicio;
                    Txt_Tipo_Bien.Text = "BIEN MUEBLE";
                    Cargar_Datos_Bien_Mueble(Hdf_Bien_Mueble_ID.Value, "IDENTIFICADOR");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Asignar_Mecanico_Servicio
            ///DESCRIPCIÓN: Asigna el Mecanico al Servicio.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Asignar_Mecanico_Servicio() {
                //Cls
            }

        #endregion

        #region Validaciones

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Asignacion
            ///DESCRIPCIÓN: Valida la Solicitud de Servicio antes de ser Asignada a un mecanico
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private Boolean Validar_Asignacion() { 
                Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                String Mensaje_Error = "";
                Boolean Validacion = true;
                if (Hdf_No_Solicitud.Value.Trim().Length == 0) { 
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar la Solicitud que se realizará.";
                    Mensaje_Error = Mensaje_Error + " <br />";
                    Validacion = false;
                }
                if (Grid_Listado_Proveedores.Rows.Count == 0) {
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar el(los) Proveedores.";
                    Mensaje_Error = Mensaje_Error + " <br />";
                    Validacion = false;
                }
                if (!Validacion) {
                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
                    Div_Contenedor_Msj_Error.Visible = true;
                } 
                return Validacion;
            }

        #endregion

        #region Grid Proveedores

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Obtener_DataTable_Proveedores
            ///DESCRIPCIÓN: Obtiene el DataTable del Grid de Proveedores
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private DataTable Obtener_DataTable_Proveedores() {
                DataTable Dt_Proveedores = new DataTable();
                Dt_Proveedores.Columns.Add("PROVEEDOR_ID", Type.GetType("System.String"));
                Dt_Proveedores.Columns.Add("NOMBRE_PROVEEDOR", Type.GetType("System.String"));
                foreach (GridViewRow Fila_Grid in Grid_Listado_Proveedores.Rows) {
                    DataRow Dr_Proveedor = Dt_Proveedores.NewRow();
                    Dr_Proveedor["PROVEEDOR_ID"] = HttpUtility.HtmlDecode(Fila_Grid.Cells[0].Text.Trim()).Trim();
                    Dr_Proveedor["NOMBRE_PROVEEDOR"] = HttpUtility.HtmlDecode(Fila_Grid.Cells[1].Text.Trim()).Trim();
                    Dt_Proveedores.Rows.Add(Dr_Proveedor);
                }
                return Dt_Proveedores;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Proveedores
            ///DESCRIPCIÓN: Llena el Grid de Proveedores
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Grid_Proveedores(DataTable Dt_Proveedores) {
                Grid_Listado_Proveedores.Columns[0].Visible = true;
                Grid_Listado_Proveedores.DataSource = Dt_Proveedores;
                Grid_Listado_Proveedores.DataBind();
                Grid_Listado_Proveedores.Columns[0].Visible = false;
            }

        #endregion

    #endregion
        
    #region Grids
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Servicios_PageIndexChanging
        ///DESCRIPCIÓN: Maneja la paginación del Listado
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 17/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Servicios_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            try {
                Grid_Listado_Servicios.SelectedIndex = (-1);
                Grid_Listado_Servicios.PageIndex = e.NewPageIndex;
                Llenar_Listado_Servicios();
            } catch(Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Servicios_SelectedIndexChanged
        ///DESCRIPCIÓN: Obtiene los datos de un Servicio 
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 17/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Servicios_SelectedIndexChanged(object sender, EventArgs e) {
            try{
                if (Grid_Listado_Servicios.SelectedIndex > (-1)){
                    Limpiar_Formulario();
                    Hdf_No_Solicitud.Value = HttpUtility.HtmlDecode(Grid_Listado_Servicios.SelectedRow.Cells[1].Text.Trim()).Trim();
                    Mostrar_Registro();
                    Configuracion_Formulario("OPERACION");
                    Grid_Listado_Servicios.SelectedIndex = -1;
                    System.Threading.Thread.Sleep(500);
                }
            } catch(Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }
 
    #endregion

    #region Eventos
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Asignacion_Mecanico_Click
        ///DESCRIPCIÓN: Controla las operaciones del Boton Asignacion
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 17/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Asignacion_Mecanico_Click(object sender, ImageClickEventArgs e) {
            if (Validar_Asignacion()) {
                Asignar_Mecanico_Servicio();
                Llenar_Listado_Servicios();
                Configuracion_Formulario("INICIAL");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "GACO", "alert('Operacion Exitosa: Asignación de Cotizacor a Servicio.\\nServicio Folio: " + Txt_Folio.Text.Trim() + "\\nPersonal Asignado: " + Cmb_Mecanicos.SelectedItem.Text.Trim() + "');", true);
                Limpiar_Formulario();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Controla las operaciones del Boton de Salir
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 17/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e) {
            if (Div_Campos.Visible) {
               Limpiar_Formulario();
               Configuracion_Formulario("INICIAL");
            } else {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Actualizar_Listado_Click
        ///DESCRIPCIÓN: Ejecuta el Proceso para Salir.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Actualizar_Listado_Click(object sender, ImageClickEventArgs e) {
            Llenar_Listado_Servicios();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Agregar_Proveedor_Click
        ///DESCRIPCIÓN: Ejecuta el Proceso para agregar un proveedor.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Agregar_Proveedor_Click(object sender, ImageClickEventArgs e) {
            if (Cmb_Proveedores.SelectedIndex > 0) {
                DataTable Dt_Proveedores = Obtener_DataTable_Proveedores();
                DataRow[] Filas = Dt_Proveedores.Select("PROVEEDOR_ID = '" + Cmb_Proveedores.SelectedItem.Value.Trim() + "'");
                if (Filas.Length > 0) {
                    Lbl_Ecabezado_Mensaje.Text = "Verificar";
                    Lbl_Mensaje_Error.Text = "* El Proveedor ya se encuentra agregado.";
                    Div_Contenedor_Msj_Error.Visible = true;
                    return;
                } else {
                    DataRow Fila_Nueva = Dt_Proveedores.NewRow();
                    Fila_Nueva["PROVEEDOR_ID"] = Cmb_Proveedores.SelectedItem.Value.Trim();
                    Fila_Nueva["NOMBRE_PROVEEDOR"] = Cmb_Proveedores.SelectedItem.Text.Trim();
                    Dt_Proveedores.Rows.Add(Fila_Nueva);
                    Llenar_Grid_Proveedores(Dt_Proveedores);
                }
            } else {
                Lbl_Ecabezado_Mensaje.Text = "Verificar";
                Lbl_Mensaje_Error.Text = "* Debe Seleccionarse un proveedor del Listado.";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Quitar_Proveedor_Click
        ///DESCRIPCIÓN: Ejecuta el Proceso para quitar un proveedor.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Quitar_Proveedor_Click(object sender, ImageClickEventArgs e) {
            ImageButton Btn_Quitar_Proveedor = (ImageButton)sender;
            if (!String.IsNullOrEmpty(Btn_Quitar_Proveedor.CommandArgument)) {
                String Proveedor_ID = Btn_Quitar_Proveedor.CommandArgument;
                DataTable Dt_Proveedores = Obtener_DataTable_Proveedores();
                Int32 Fila = (-1);
                for (Int32 Contador = 0; Contador < Dt_Proveedores.Rows.Count; Contador++)
                    if (Dt_Proveedores.Rows[Contador]["PROVEEDOR_ID"].ToString().Trim().Equals(Proveedor_ID.Trim())) { Fila = Contador; break; }
                if (Fila > (-1)) {
                    Dt_Proveedores.Rows.RemoveAt(Fila);
                    Llenar_Grid_Proveedores(Dt_Proveedores);
                }
            }
        }

    #endregion

}