﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Control_Patrimonial_Operacion_Vehiculos.Negocio;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Taller_Mecanico.Operacion_Calendarizacion_Revista_Mecanica.Negocio;
using JAPAMI.Taller_Mecanico.Catalogo_Periodos_Revista.Negocio;

public partial class paginas_Taller_Mecanico_Frm_Ope_Tal_Calendarizacion_Rev_Mec : System.Web.UI.Page {
    
    #region Page_Load

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN: Carga la Pagina Inicial
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 22/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e) {
            Div_Contenedor_Msj_Error.Visible = false;
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack) {
                Txt_Fecha_Vencimiento.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Today);
                Llenar_Grid_List_Rev_Mec_Ven(0);
                Llenar_Combo_Unidades_Responsables();
                Llenar_Combos_Independientes();
                Llenar_Combo_Periodos_Revista();
                Configuracion_Formulario(true);
            }
        }

    #endregion
    
    #region Metodos
    
        #region Llenado de Campos [Combos, Listados, Vehiculos]
    
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_List_Rev_Mec_Ven
            ///DESCRIPCIÓN          : Llena el Grid de las Revistas.
            ///PARAMETROS           : 
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 10/Julio/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Llenar_Grid_List_Rev_Mec_Ven(Int32 Pagina) { 
               try {
                    Grid_List_Rev_Mec_Ven.Columns[1].Visible = true;
                    Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Negocio Negocio = new Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Negocio();
                    Negocio.P_Fecha_Proxima_Revision = Convert.ToDateTime(Txt_Fecha_Vencimiento.Text);
                    Grid_List_Rev_Mec_Ven.DataSource = Negocio.Consulta_General_Calendarizacion_Revistas_Mecanicas();
                    Grid_List_Rev_Mec_Ven.PageIndex = Pagina;
                    Grid_List_Rev_Mec_Ven.DataBind();
                    Grid_List_Rev_Mec_Ven.Columns[1].Visible = false;
                } catch (Exception Ex) {
                    Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Listado_Vehiculos
            ///DESCRIPCIÓN          : Llena el Grid de los Vehiculos.
            ///PARAMETROS           : 
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 15/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Llenar_Grid_Listado_Bienes_Vehiculos(Int32 Pagina) { 
               try {
                    Grid_Listado_Busqueda_Vehiculo.Columns[1].Visible = true;
                    Cls_Ope_Pat_Com_Vehiculos_Negocio Vehiculos = new Cls_Ope_Pat_Com_Vehiculos_Negocio();
                    Vehiculos.P_Tipo_DataTable = "VEHICULOS";
                    if (Session["FILTRO_BUSQUEDA"] != null) {
                        Vehiculos.P_Tipo_Filtro_Busqueda = Session["FILTRO_BUSQUEDA"].ToString();
                        Vehiculos.P_Estatus = "VIGENTE";
                        if (Session["FILTRO_BUSQUEDA"].ToString().Trim().Equals("DATOS_GENERALES")) {
                            if (Txt_Busqueda_Vehiculo_Numero_Inventario.Text.Trim().Length > 0) { Vehiculos.P_Numero_Inventario = Convert.ToInt64(Txt_Busqueda_Vehiculo_Numero_Inventario.Text.Trim()); }
                            if (Txt_Busqueda_Vehiculo_Numero_Economico.Text.Trim().Length > 0) { Vehiculos.P_Numero_Economico_ = Txt_Busqueda_Vehiculo_Numero_Economico.Text.Trim(); }
                            if (Txt_Busqueda_Vehiculo_Anio_Fabricacion.Text.Trim().Length > 0) { Vehiculos.P_Anio_Fabricacion = Convert.ToInt32(Txt_Busqueda_Vehiculo_Anio_Fabricacion.Text.Trim()); }
                            Vehiculos.P_Modelo_ID = Txt_Busqueda_Vehiculo_Modelo.Text.Trim();
                            if (Cmb_Busqueda_Vehiculo_Marca.SelectedIndex > 0) {
                                Vehiculos.P_Marca_ID = Cmb_Busqueda_Vehiculo_Marca.SelectedItem.Value.Trim();
                            }
                            if (Cmb_Busqueda_Vehiculo_Color.SelectedIndex > 0) {
                                Vehiculos.P_Color_ID = Cmb_Busqueda_Vehiculo_Color.SelectedItem.Value.Trim();
                            }
                            if (Cmb_Busqueda_Vehiculo_Dependencias.SelectedIndex > 0) {
                                Vehiculos.P_Dependencia_ID = Cmb_Busqueda_Vehiculo_Dependencias.SelectedItem.Value.Trim();
                            }
                        } else if (Session["FILTRO_BUSQUEDA"].ToString().Trim().Equals("RESGUARDANTES")) {
                            Vehiculos.P_RFC_Resguardante = Txt_Busqueda_Vehiculo_RFC_Resguardante.Text.Trim();
                            Vehiculos.P_No_Empleado = Txt_Busqueda_Vehiculo_No_Empleado.Text.Trim();
                            if (Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.SelectedIndex > 0) {
                                Vehiculos.P_Dependencia_ID = Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.SelectedItem.Value.Trim();
                            }
                            if (Cmb_Busqueda_Vehiculo_Nombre_Resguardante.SelectedIndex > 0) {
                                Vehiculos.P_Resguardante_ID = Cmb_Busqueda_Vehiculo_Nombre_Resguardante.SelectedItem.Value.Trim();
                            }
                        }
                    }
                    Grid_Listado_Busqueda_Vehiculo.DataSource = Vehiculos.Consultar_DataTable();
                    Grid_Listado_Busqueda_Vehiculo.PageIndex = Pagina;
                    Grid_Listado_Busqueda_Vehiculo.DataBind();
                    Grid_Listado_Busqueda_Vehiculo.Columns[1].Visible = false;
                    MPE_Busqueda_Vehiculo.Show();
                } catch (Exception Ex) {
                    Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Unidades_Responsables
            ///DESCRIPCIÓN: Se llena el Combo de las Unidades Responsables.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 22/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Combo_Unidades_Responsables() {
                Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                Negocio.P_Estatus = "ACTIVO";
                DataTable Dt_Dependencias = Negocio.Consultar_Unidades_Responsables();
                Cmb_Busqueda_Vehiculo_Dependencias.DataSource = Dt_Dependencias;
                Cmb_Busqueda_Vehiculo_Dependencias.DataTextField = "CLAVE_NOMBRE";
                Cmb_Busqueda_Vehiculo_Dependencias.DataValueField = "DEPENDENCIA_ID";
                Cmb_Busqueda_Vehiculo_Dependencias.DataBind();
                Cmb_Busqueda_Vehiculo_Dependencias.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
                Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.DataSource = Dt_Dependencias;
                Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.DataTextField = "CLAVE_NOMBRE";
                Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.DataValueField = "DEPENDENCIA_ID";
                Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.DataBind();
                Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Periodos_Revista
            ///DESCRIPCIÓN: Se llena el Combo de las Unidades Responsables.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 23/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Combo_Periodos_Revista() {
                Cls_Cat_Tal_Periodos_Revista_Negocio Negocio = new Cls_Cat_Tal_Periodos_Revista_Negocio();
                Negocio.P_Estatus = "VIGENTE";
                DataTable Dt_Resultados = Negocio.Consultar_Periodos_Revista();
                Cmb_Periodo_Calendarizacion.DataSource = Dt_Resultados;
                Cmb_Periodo_Calendarizacion.DataTextField = "NOMBRE";
                Cmb_Periodo_Calendarizacion.DataValueField = "PERIODO_ID";
                Cmb_Periodo_Calendarizacion.DataBind();
                Cmb_Periodo_Calendarizacion.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
            }
            
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Vehiculo
            ///DESCRIPCIÓN: Se cargan los Datos del Vehiculo Seleccionado.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 22/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Cargar_Datos_Vehiculo(String Vehiculo, String Tipo_Busqueda) {
                Limpiar_Componentes();
                Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                switch (Tipo_Busqueda) { 
                    case "NO_INVENTARIO":
                        Consulta_Negocio.P_No_Inventario = Vehiculo;
                        break;
                    case "IDENTIFICADOR":
                        Consulta_Negocio.P_Vehiculo_ID = Vehiculo;
                        break;
                    default: break;
                }
                DataTable Dt_Vehiculo = Consulta_Negocio.Consultar_Vehiculos();
                if (Dt_Vehiculo.Rows.Count > 0) {
                    Hdf_Vehiculo_ID.Value = Dt_Vehiculo.Rows[0]["VEHICULO_ID"].ToString().Trim();
                    Txt_No_Inventario.Text = Dt_Vehiculo.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                    Txt_No_Economico.Text = Dt_Vehiculo.Rows[0]["NO_ECONOMICO"].ToString().Trim();
                    Txt_Datos_Vehiculo.Text = Dt_Vehiculo.Rows[0]["VEHICULO_DESCRIPCION"].ToString().Trim();
                    Txt_Placas.Text = Dt_Vehiculo.Rows[0]["PLACAS"].ToString().Trim();
                    Txt_Anio.Text = Dt_Vehiculo.Rows[0]["ANIO"].ToString().Trim();
                    if (!String.IsNullOrEmpty(Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString())) {
                        Cls_Cat_Dependencias_Negocio Dep_Negocio = new Cls_Cat_Dependencias_Negocio();
                        Dep_Negocio.P_Dependencia_ID = Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString().Trim();
                        DataTable Dt_Resultado_Dep = Dep_Negocio.Consulta_Dependencias();
                        if (Dt_Resultado_Dep != null) {
                            if (Dt_Resultado_Dep.Rows.Count > 0) {
                                Txt_Unidad_Responsable.Text = Dt_Resultado_Dep.Rows[0]["CLAVE_NOMBRE"].ToString().Trim();
                            }
                        }
                    }
                    Cargar_Datos_Calendarizacion(Hdf_Vehiculo_ID.Value.Trim());
                } else {
                    Lbl_Ecabezado_Mensaje.Text = "El Vehículo no se encontro.";
                    Lbl_Mensaje_Error.Text = ""; 
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Empleados
            ///DESCRIPCIÓN          : Llena el Combo con los empleados de una dependencia.
            ///PARAMETROS           : 
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO: 22/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Llenar_Combo_Empleados(String Dependencia_ID, ref DropDownList Combo_Empleados) {
                Combo_Empleados.Items.Clear();
                if (Dependencia_ID != null && Dependencia_ID.Trim().Length > 0) {
                    Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                    Negocio.P_Estatus = "ACTIVO";
                    Negocio.P_Dependencia_ID = Dependencia_ID.Trim();
                    DataTable Dt_Datos = Negocio.Consultar_Empleados();
                    Combo_Empleados.DataSource = Dt_Datos;
                    Combo_Empleados.DataValueField = "EMPLEADO_ID";
                    Combo_Empleados.DataTextField = "NOMBRE";
                    Combo_Empleados.DataBind();
                } 
                Combo_Empleados.Items.Insert(0, new ListItem("< TODOS >", ""));
            }
            
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Calendarizacion
            ///DESCRIPCIÓN: Se cargan los Datos del Vehiculo Seleccionado.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 22/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Cargar_Datos_Calendarizacion(String Vehiculo_ID) {
                Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Negocio Negocio = new Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Negocio();
                Negocio.P_Vehiculo_ID = Vehiculo_ID.Trim();
                Negocio = Negocio.Consultar_Detalles_Registro();
                if (!String.IsNullOrEmpty(Negocio.P_Vehiculo_ID)) {
                    Cmb_Periodo_Calendarizacion.SelectedIndex = Cmb_Periodo_Calendarizacion.Items.IndexOf(Cmb_Periodo_Calendarizacion.Items.FindByValue(Negocio.P_Periodo_ID));
                    if (!String.Format("{0:ddMMyyyy}", Negocio.P_Fecha_Ultima_Revision).Trim().Equals(String.Format("{0:ddMMyyyy}", new DateTime()).Trim())) {
                        Txt_Fecha_Ultima_Revision.Text = String.Format("{0:dd/MMM/yyyy}", Negocio.P_Fecha_Ultima_Revision);
                    }
                    if (!String.Format("{0:ddMMyyyy}", Negocio.P_Fecha_Proxima_Revision).Trim().Equals(String.Format("{0:ddMMyyyy}", new DateTime()).Trim())) {
                        Txt_Fecha_Proxima_Revision.Text = String.Format("{0:dd/MMM/yyyy}", Negocio.P_Fecha_Proxima_Revision);
                        Calcular_Retraso_Revista();
                    }
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Actualizar_Fecha_Revision_Proxima
            ///DESCRIPCIÓN: Actualiza la Proxima Fecha de Acuerdo al Periodo.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 22/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Actualizar_Fecha_Revision_Proxima() {
                if (Cmb_Periodo_Calendarizacion.SelectedIndex > 0 && Txt_Fecha_Ultima_Revision.Text.Trim().Length > 0) {
                    Cls_Cat_Tal_Periodos_Revista_Negocio Per_Neg = new Cls_Cat_Tal_Periodos_Revista_Negocio();
                    Per_Neg.P_Periodo_ID = Cmb_Periodo_Calendarizacion.SelectedItem.Value.Trim();
                    Per_Neg = Per_Neg.Consultar_Detalles_Periodo_Revista();
                    Txt_Fecha_Proxima_Revision.Text = String.Format("{0:dd/MMM/yyyy}", (Convert.ToDateTime(Txt_Fecha_Ultima_Revision.Text.Trim()).AddMonths(Convert.ToInt32(Per_Neg.P_Periodo_Mes))));
                } else {
                    Txt_Fecha_Proxima_Revision.Text = "";
                }
            }
    
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Cargar_Estilo_Estatus
            ///DESCRIPCIÓN          : Carga el Estilo para el Estatus
            ///PARAMETROS           : 
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 15/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Cargar_Estilo_Estatus() {
                String Estatus = Txt_Retraso.Text.Trim();
                switch (Estatus) {
                    case "NO HAY RETRASO":
                        Txt_Retraso.BackColor = System.Drawing.Color.White;
                        Txt_Retraso.ForeColor = System.Drawing.Color.Black;
                        break;
                    default:
                        Txt_Retraso.BackColor = System.Drawing.Color.White;
                        Txt_Retraso.ForeColor = System.Drawing.Color.Red;
                        break;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Calcular_Retraso_Revista
            ///DESCRIPCIÓN          : Carga retraso de la Revista
            ///PARAMETROS           : 
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 15/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Calcular_Retraso_Revista() {
                String Retraso = Obtener_Diferencia_Fechas_Texto(Convert.ToDateTime(Txt_Fecha_Proxima_Revision.Text), DateTime.Today, true);
                Txt_Retraso.Text = Retraso;
                Cargar_Estilo_Estatus();
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Obtener_Diferencia_Fechas_Texto
            ///DESCRIPCIÓN: Obtiene en Texto la Diferencia de Fechas
            ///PARAMETROS:
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Abril/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            private String Obtener_Diferencia_Fechas_Texto(DateTime Fecha_Inicial, DateTime Fecha_Final, Boolean Completo)
            {
                String Resultado = "NO HAY RETRASO";
                TimeSpan Diferencia = Fecha_Final.Subtract(Fecha_Inicial);
                if (Diferencia.TotalMilliseconds > 0) {
                    DateTime Temporal = new DateTime().Add(TimeSpan.FromMilliseconds(Diferencia.TotalMilliseconds));
                    Int32 Anio = Temporal.Year - 1;
                    Int32 Mes = Temporal.Month - 1;
                    if (Anio > 0) { Resultado = Anio + ((Anio > 1) ? " AÑOS " : " AÑO"); }
                    if (Completo) {
                        if (!Resultado.Trim().Equals("NO HAY RETRASO")) Resultado = Resultado + " "; else { Resultado = ""; }
                        if (Mes > 0) { Resultado = Resultado + Mes + ((Mes > 1) ? " MESES " : " MES"); }
                    }
                }
                return Resultado.Trim();
            }
    
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combos_Independientes
            ///DESCRIPCIÓN: Se llenan los Combos Generales Independientes.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 06/Diciembre/2010 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private void Llenar_Combos_Independientes()
            {
                try
                {
                    Cls_Ope_Pat_Com_Vehiculos_Negocio Combos = new Cls_Ope_Pat_Com_Vehiculos_Negocio();
                    //SE LLENA EL COMBO DE MARCAS DE LAS BUSQUEDAS
                    Combos.P_Tipo_DataTable = "MARCAS";
                    DataTable Marcas = Combos.Consultar_DataTable();
                    DataRow Fila_Marca = Marcas.NewRow();
                    Fila_Marca["MARCA_ID"] = "TODAS";
                    Fila_Marca["NOMBRE"] = HttpUtility.HtmlDecode("&lt;TODAS&gt;");
                    Marcas.Rows.InsertAt(Fila_Marca, 0);
                    Cmb_Busqueda_Vehiculo_Marca.DataSource = Marcas;
                    Cmb_Busqueda_Vehiculo_Marca.DataTextField = "NOMBRE";
                    Cmb_Busqueda_Vehiculo_Marca.DataValueField = "MARCA_ID";
                    Cmb_Busqueda_Vehiculo_Marca.DataBind();

                    //SE LLENA EL COMBO DE COLORES DE LAS BUSQUEDAS
                    Combos.P_Tipo_DataTable = "COLORES";
                    DataTable Tipos_Colores = Combos.Consultar_DataTable();
                    DataRow Fila_Color = Tipos_Colores.NewRow();
                    Fila_Color["COLOR_ID"] = "TODOS";
                    Fila_Color["DESCRIPCION"] = HttpUtility.HtmlDecode("&lt;TODOS&gt;");
                    Tipos_Colores.Rows.InsertAt(Fila_Color, 0);
                    Cmb_Busqueda_Vehiculo_Color.DataSource = Tipos_Colores;
                    Cmb_Busqueda_Vehiculo_Color.DataTextField = "DESCRIPCION";
                    Cmb_Busqueda_Vehiculo_Color.DataValueField = "COLOR_ID";
                    Cmb_Busqueda_Vehiculo_Color.DataBind();

                }
                catch (Exception Ex)
                {
                    Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }

        #endregion

        #region Generales

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Limpiar_Componentes
            ///DESCRIPCIÓN: Limpiar Formulario
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 22/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Limpiar_Componentes() {
                Hdf_Vehiculo_ID.Value = "";
                Txt_No_Inventario.Text = "";
                Txt_No_Economico.Text = "";
                Txt_Datos_Vehiculo.Text = "";
                Txt_Placas.Text = "";
                Txt_Anio.Text = "";
                Cmb_Periodo_Calendarizacion.SelectedIndex = 0;
                Txt_Fecha_Ultima_Revision.Text = "";
                Txt_Fecha_Proxima_Revision.Text = "";
                Txt_Unidad_Responsable.Text = "";
                Txt_Retraso.Text = "";
            }
  
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
            ///DESCRIPCIÓN: Carga una configuracion de los controles del Formulario
            ///PROPIEDADES: 1. Estatus. Estatus en el que se cargara la configuración de los
            ///              controles.
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 23/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private void Configuracion_Formulario( Boolean Estatus ) {
                if (Estatus) { 
                    Btn_Modificar.AlternateText = "Modificar";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Btn_Salir.AlternateText = "Salir";
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";                
                } else {
                    Btn_Modificar.AlternateText = "Guardar Actualización";
                    Btn_Modificar.ToolTip = "Guardar Actualización";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Salir.AlternateText = "Cancelar Actualización";
                    Btn_Salir.ToolTip = "Cancelar Actualización";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";        
                }
                Div_Busqueda.Visible = Estatus;
                Cmb_Periodo_Calendarizacion.Enabled = !Estatus;
            }

        #endregion

        #region Validacion

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Page_Load
            ///DESCRIPCIÓN: Carga la Pagina Inicial
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 22/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private Boolean Validar_Actualizacion() {
                Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                String Mensaje_Error = "";
                Boolean Validacion = true;
                if (Hdf_Vehiculo_ID.Value.Trim().Length == 0) {
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar al Vehículo.";
                    Validacion = false;
                }
                if (Cmb_Periodo_Calendarizacion.SelectedIndex == 0) {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar una opcion del Combo de Peridos para Revisión.";
                    Validacion = false;
                }
                if (!Validacion) {
                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
                    Div_Contenedor_Msj_Error.Visible = true;
                }
                return Validacion;
            }

        #endregion

    #endregion

    #region Grids

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Productos_PageIndexChanging
        ///DESCRIPCIÓN: Maneja la paginación del GridView de los Revistass
        ///PROPIEDADES:     
        ///CREO : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO: 10/Julio/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_List_Rev_Mec_Ven_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            try {
                Llenar_Grid_List_Rev_Mec_Ven(e.NewPageIndex);
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_List_Rev_Mec_Ven_SelectedIndexChanged
        ///DESCRIPCIÓN: Maneja el evento de cambio de Seleccion del GridView de Vehiculos del
        ///             Modal de Busqueda.
        ///PROPIEDADES:     
        ///CREO : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO: 10/Julio/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_List_Rev_Mec_Ven_SelectedIndexChanged(object sender, EventArgs e) { 
            try {
                if (Grid_List_Rev_Mec_Ven.SelectedIndex > (-1)) {
                    String Vehiculo_ID = Grid_List_Rev_Mec_Ven.SelectedRow.Cells[1].Text.Trim();
                    Cargar_Datos_Vehiculo(Vehiculo_ID, "IDENTIFICADOR");
                }
                MPE_Busqueda_Vehiculo.Hide();
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Productos_PageIndexChanging
        ///DESCRIPCIÓN: Maneja la paginación del GridView de los Vehiculos
        ///PROPIEDADES:     
        ///CREO : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO: 22/Junio/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Busqueda_Vehiculo_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            try {
                Llenar_Grid_Listado_Bienes_Vehiculos(e.NewPageIndex);
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Busqueda_Vehiculo_SelectedIndexChanged
        ///DESCRIPCIÓN: Maneja el evento de cambio de Seleccion del GridView de Vehiculos del
        ///             Modal de Busqueda.
        ///PROPIEDADES:     
        ///CREO : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO: 22/Junio/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Busqueda_Vehiculo_SelectedIndexChanged(object sender, EventArgs e) {
            try {
                if (Grid_Listado_Busqueda_Vehiculo.SelectedIndex > (-1)) {
                    String Vehiculo_ID = Grid_Listado_Busqueda_Vehiculo.SelectedRow.Cells[1].Text.Trim();
                    Cargar_Datos_Vehiculo(Vehiculo_ID, "IDENTIFICADOR");
                }
                MPE_Busqueda_Vehiculo.Hide();
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

    #endregion
    
    #region Eventos

        #region Busquedas

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Click
            ///DESCRIPCIÓN: Busca el Vehiculo por el No. Inventario.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 22/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e) {
                try {
                    Grid_List_Rev_Mec_Ven.SelectedIndex = (-1);
                    Limpiar_Componentes();
                    if (Txt_Busqueda.Text.Trim().Length > 0) {
                        Cargar_Datos_Vehiculo(Txt_Busqueda.Text.Trim(), "NO_INVENTARIO");
                    } else {
                        Lbl_Ecabezado_Mensaje.Text = "Es necesario introducir el No. de Inventario del Vehículo.";
                        Lbl_Mensaje_Error.Text = "";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                } catch (Exception Ex) {
                    Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                    Lbl_Mensaje_Error.Text = "Se presento una Excepcion al Buscar [" + Ex.Message + "]";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Avanzada_Click
            ///DESCRIPCIÓN: Busca el Vehiculo por el No. Inventario.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 22/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            protected void Btn_Busqueda_Avanzada_Click(object sender, ImageClickEventArgs e) {
                try {
                    MPE_Busqueda_Vehiculo.Show();
                } catch (Exception Ex) {
                    Lbl_Ecabezado_Mensaje.Text = "";
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            
            #region Vehiculo

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Datos_Vehiculo_Click
                ///DESCRIPCIÓN: Ejecuta la Busqueda de los Vehiculos por datos generales.
                ///PARAMETROS:
                ///CREO: Francisco Antonio Gallardo Castañeda.
                ///FECHA_CREO: 22/Junio/2012
                ///MODIFICO:
                ///FECHA_MODIFICO
                ///CAUSA_MODIFICACIÓN
                ///*******************************************************************************
                protected void Btn_Buscar_Datos_Vehiculo_Click(object sender, ImageClickEventArgs e) {
                    try {
                        Session["FILTRO_BUSQUEDA"] = "DATOS_GENERALES";
                        Llenar_Grid_Listado_Bienes_Vehiculos(0);
                    } catch (Exception Ex) {
                        Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                        Lbl_Mensaje_Error.Text = "";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                }

                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Btn_Limpiar_Filtros_Buscar_Datos_Vehiculo_Click
                ///DESCRIPCIÓN          : Hace la limpieza de los campos.
                ///PARAMETROS           : 
                ///CREO: Francisco Antonio Gallardo Castañeda.
                ///FECHA_CREO: 22/Junio/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                protected void Btn_Limpiar_Filtros_Buscar_Datos_Vehiculo_Click(object sender, ImageClickEventArgs e) {
                    try {
                        Txt_Busqueda_Vehiculo_Numero_Inventario.Text = "";
                        Txt_Busqueda_Vehiculo_Numero_Economico.Text = "";
                        Txt_Busqueda_Vehiculo_Modelo.Text = "";
                        Cmb_Busqueda_Vehiculo_Marca.SelectedIndex = 0;
                        Txt_Busqueda_Vehiculo_Anio_Fabricacion.Text = "";
                        Cmb_Busqueda_Vehiculo_Color.SelectedIndex = 0;
                        Cmb_Busqueda_Vehiculo_Dependencias.SelectedIndex = 0;
                        MPE_Busqueda_Vehiculo.Show();
                    } catch (Exception Ex) {
                        Lbl_Ecabezado_Mensaje.Text = "Excepción.";
                        Lbl_Mensaje_Error.Text = "Ex:['" + Ex.Message + "']";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                }
        
                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias_SelectedIndexChanged
                ///DESCRIPCIÓN          : Maneja el evento del Combo de Dependencias.
                ///PARAMETROS           : 
                ///CREO: Francisco Antonio Gallardo Castañeda.
                ///FECHA_CREO: 22/Junio/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                protected void Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias_SelectedIndexChanged(object sender, EventArgs e) {
                    if (Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.SelectedIndex > 0)
                    {
                        Llenar_Combo_Empleados(Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.SelectedItem.Value.Trim(), ref Cmb_Busqueda_Vehiculo_Nombre_Resguardante);
                    } else {
                        Llenar_Combo_Empleados(null, ref Cmb_Busqueda_Vehiculo_Nombre_Resguardante);
                    }
                }
        
                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Resguardante_Vehiculo_Click
                ///DESCRIPCIÓN: Ejecuta la Busqueda de los vehiculos por resguardos.
                ///PARAMETROS:     
                ///CREO: Francisco Antonio Gallardo Castañeda.
                ///FECHA_CREO: 22/Junio/2012
                ///MODIFICO:
                ///FECHA_MODIFICO
                ///CAUSA_MODIFICACIÓN
                ///*******************************************************************************
                protected void Btn_Buscar_Resguardante_Vehiculo_Click(object sender, ImageClickEventArgs e) {
                    try {
                        Session["FILTRO_BUSQUEDA"] = "RESGUARDANTES";
                        Llenar_Grid_Listado_Bienes_Vehiculos(0);
                    } catch (Exception Ex) {
                        Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                        Lbl_Mensaje_Error.Text = "";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                }
    
                ///*******************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Btn_Limpiar_Filtros_Buscar_Resguardante_Vehiculo_Click
                ///DESCRIPCIÓN          : Hace la limpieza de los campos.
                ///PARAMETROS           : 
                ///CREO: Francisco Antonio Gallardo Castañeda.
                ///FECHA_CREO: 22/Junio/2012
                ///MODIFICO             :
                ///FECHA_MODIFICO       :
                ///CAUSA_MODIFICACIÓN   :
                ///*******************************************************************************
                protected void Btn_Limpiar_Filtros_Buscar_Resguardante_Vehiculo_Click(object sender, ImageClickEventArgs e)  {
                    try {
                        Txt_Busqueda_Vehiculo_RFC_Resguardante.Text = "";
                        Txt_Busqueda_Vehiculo_No_Empleado.Text = "";
                        Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.SelectedIndex = 0;
                        Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias_SelectedIndexChanged(Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias, null);
                        Cmb_Busqueda_Vehiculo_Nombre_Resguardante.SelectedIndex = 0;
                        MPE_Busqueda_Vehiculo.Show();
                    } catch (Exception Ex) {
                        Lbl_Ecabezado_Mensaje.Text = "Excepción.";
                        Lbl_Mensaje_Error.Text = "Ex:['" + Ex.Message + "']";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                }

            #endregion

        #endregion

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
        ///DESCRIPCIÓN: Deja los componentes listos para hacer la modificacion.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 23/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e){
            try{
                if (Btn_Modificar.AlternateText.Equals("Modificar")){
                    if (Hdf_Vehiculo_ID.Value.Trim().Length> 0) {
                        Configuracion_Formulario(false);
                    } else {
                        Lbl_Ecabezado_Mensaje.Text = "Debe seleccionar el Vehiculo que se desea Actualizar.";
                        Lbl_Mensaje_Error.Text = "";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                } else {
                   // if (Validar_Actualizacion()){
                        Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Negocio Negocio = new Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Negocio();
                        Negocio.P_Vehiculo_ID = Hdf_Vehiculo_ID.Value.Trim();
                        Negocio.P_Periodo_ID = Cmb_Periodo_Calendarizacion.SelectedItem.Value; //"00001";
                        if (Txt_Fecha_Ultima_Revision.Text.Trim().Length > 0) { Negocio.P_Fecha_Ultima_Revision = Convert.ToDateTime(Txt_Fecha_Ultima_Revision.Text); }
                        if (Txt_Fecha_Proxima_Revision.Text.Trim().Length > 0) { Negocio.P_Fecha_Proxima_Revision = Convert.ToDateTime(Txt_Fecha_Proxima_Revision.Text); }
                        Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                        Negocio.Actualizar_Registro();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('¡¡¡Actualización Exitosa!!!');", true);
                        Configuracion_Formulario(true);
                        Limpiar_Componentes();
                        Cargar_Datos_Vehiculo(Negocio.P_Vehiculo_ID, "IDENTIFICADOR");
                    //}
                }
            }catch(Exception Ex){
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Cancela la operación que esta en proceso (Alta o Actualizar) o Sale del Formulario.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 23/Junio/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e){
            if (Btn_Salir.AlternateText.Equals("Salir")){
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }else {
                Configuracion_Formulario(true);
                Limpiar_Componentes();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Cmb_Periodo_Calendarizacion_SelectedIndexChanged
        ///DESCRIPCIÓN: Actuliza la Fecha Proxima
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 23/Junio/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Cmb_Periodo_Calendarizacion_SelectedIndexChanged(object sender, EventArgs e) {
            Actualizar_Fecha_Revision_Proxima();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Actualizar_Listado_Click
        ///DESCRIPCIÓN: Actualiza el Grid
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Julio/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Actualizar_Listado_Click(object sender, ImageClickEventArgs e) {
            Llenar_Grid_List_Rev_Mec_Ven(0);
        }

    #endregion

}