﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Ope_Tal_Seguimiento_Servicio_Interno.aspx.cs"
    Inherits="paginas_Taller_Mecanico_Frm_Ope_Seguimiento_Proveedor" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">

    <script type="text/javascript">
        window.onerror = new Function("return true");
        //Abrir una ventana modal
        function Abrir_Ventana_Modal(Url, Propiedades) {
            window.showModalDialog(Url, null, Propiedades);
        }
        function Validar_Longitud_Texto(Text_Box, Max_Longitud) {
            if (Text_Box.value.length > Max_Longitud) {
                Text_Box.value = Text_Box.value.substring(0, Max_Longitud);
            }
        }
    </script>

    <script type="text/javascript" language="javascript">
    //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_sesiones.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion()
        {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para matener la sesión activa
        setInterval('MantenSesion()', <%=(int)(0.9*(Session.Timeout * 60000))%>);
        function formatCurrency(num) {
            var Combo = "";
            var Beneficio;
                num = num.toString().replace(/\$|\,/g,'');
                if(isNaN(num))
                num = "0";
                sign = (num == (num = Math.abs(num)));
                num = Math.floor(num*100+0.50000000001);
                cents = num%100;
                num = Math.floor(num/100).toString();
                if(cents<10)
                    cents = "0" + cents;
                    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
                    num = num.substring(0,num.length-(4*i+3))+','+
                    num.substring(num.length-(4*i+3));
                    return (((sign)?'':'-') + num + '.' + cents);
            }
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" EnablePartialRendering="true" AsyncPostBackTimeout="9000" />
    <%--Inicio formulario--%>
    <asp:UpdatePanel ID="Upd_Parametros_Predial" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--update progrees--%>
            <asp:UpdateProgress ID="Uprg_Progress" runat="server" AssociatedUpdatePanelID="Upd_Parametros_Predial"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Generales" style="background-color: #ffffff; width: 100%; height: 100%">
                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td colspan="4" class="label_titulo">
                            Seguimiento a Mecánico
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" />
                            <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" CssClass="estilo_fuente_mensaje_error"
                                Text="" /><br />
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
                    <tr class="barra_busqueda">
                        <td colspan="2" align="left" style="width: 20%">
                            <asp:ImageButton ID="Btn_Asignacion" runat="server" ImageUrl="~/paginas/imagenes/paginas/accept.png"
                                Width="24px" CssClass="Img_Button" AlternateText="Agregar Seguimiento al Servicio"
                                ToolTip="Agregar Seguimiento al Servicio" OnClick="Btn_Asignacion_Click" />
                            <asp:ImageButton ID="Btn_Cerrar_Reparacion" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_aceptarplan.png"
                                Width="24px" CssClass="Img_Button" AlternateText="Cerrar Servicio" ToolTip="Cerrar Servicio"
                                OnClientClick="return confirm('¿Esta seguro de cerrar el Servicio?');" OnClick="Btn_Cerrar_Reparacion_Click" />
                            <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                Width="24px" CssClass="Img_Button" AlternateText="Salir" ToolTip="Salir" OnClick="Btn_Salir_Click" />
                        </td>
                        <td colspan="2" align="right" valign="top" style="width: 80%">
                            <table style="width: 80%;">
                                <tr>
                                    <td style="vertical-align: top; text-align: right; width: 5%">
                                    </td>
                                    <td style="vertical-align: top; text-align: right; width: 90%">
                                        Búsqueda:
                                        <asp:TextBox ID="Txt_Buscar" runat="server" MaxLength="100" TabIndex="5" ToolTip="Buscar"
                                            Width="180px" AutoPostBack="true" />
                                        <cc1:TextBoxWatermarkExtender ID="WTE_Txt_Buscar" runat="server" WatermarkCssClass="watermarked"
                                            WatermarkText="<Clave>" TargetControlID="Txt_Buscar" />
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Buscar" runat="server" TargetControlID="Txt_Buscar"
                                            FilterType="UppercaseLetters,LowercaseLetters,Numbers" />
                                    </td>
                                    <td style="vertical-align: top; text-align: right; width: 5%">
                                        <asp:ImageButton ID="Btn_Buscar" runat="server" TabIndex="6" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                            ToolTip="Buscar" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="Div_Listado_Servicios" runat="server" style="width: 100%;">
                <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                </table>
                <br />
                <asp:GridView ID="Grid_Listado_Servicios" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    CssClass="GridView_1" AllowSorting="True" EmptyDataText="No se Encontrarón Servicios Pendientes"
                    GridLines="None" OnPageIndexChanging="Grid_Listado_Servicios_PageIndexChanging"
                    DataKeyNames="NO_ENTRADA,NO_SERVICIO,NO_SOLICITUD" OnSelectedIndexChanged="Grid_Listado_Servicios_SelectedIndexChanged"
                    OnSorting="Grid_Listado_Servicios_Sorting" PageSize="20" Width="99%">
                    <RowStyle CssClass="GridItem" />
                    <Columns>
                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                            <ItemStyle Width="3%" />
                        </asp:ButtonField>
                        <asp:BoundField DataField="NO_ENTRADA" HeaderText="NO_ENTRADA" SortExpression="NO_ENTRADA">
                            <ItemStyle Width="0%" Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NO_SERVICIO" HeaderText="NO_SERVICIO" SortExpression="NO_SERVICIO">
                            <ItemStyle Width="0%" Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NO_SOLICITUD" HeaderText="NO_SOLICITUD" SortExpression="NO_SOLICITUD">
                            <ItemStyle Width="0%" Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FOLIO" HeaderText="Folio" SortExpression="FOLIO">
                            <ItemStyle Width="5%" Font-Size="X-Small" HorizontalAlign="Center" Font-Bold="true" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FECHA_RECEPCION" HeaderText="Fecha Recepción" SortExpression="FECHA_RECEPCION"
                            DataFormatString="{0:dd/MMM/yyyy}">
                            <ItemStyle Width="15%" HorizontalAlign="Center" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TIPO_SERVICIO" HeaderText="Tipo Servicio" SortExpression="TIPO_SERVICIO">
                            <ItemStyle Width="15%" HorizontalAlign="Center" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DESCRIPCION_SERVICIO" HeaderText="Descripción" SortExpression="DESCRIPCION_SERVICIO"
                            HeaderStyle-HorizontalAlign="Center">
                            <ItemStyle Font-Size="X-Small" Width="32%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" SortExpression="DEPENDENCIA">
                            <ItemStyle Font-Size="X-Small" Width="20%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NO_INVENTARIO" HeaderText="No. Inventario" SortExpression="NO_INVENTARIO"
                            Visible="false">
                            <ItemStyle Width="0%" HorizontalAlign="Center" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NO_ECONOMICO" HeaderText="Economico" SortExpression="NO_ECONOMICO">
                            <ItemStyle Width="10%" HorizontalAlign="Center" Font-Size="X-Small" />
                        </asp:BoundField>
                    </Columns>
                    <PagerStyle CssClass="GridHeader" />
                    <SelectedRowStyle CssClass="GridSelected" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAltItem" />
                </asp:GridView>
            </div>
            <div id="Div_Campos" runat="server" style="width: 100%;">
                <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                    <tr>
                        <td colspan="4">
                            <asp:HiddenField ID="Hdf_No_Entrada" runat="server" />
                            <asp:HiddenField ID="Hdf_No_Solicitud" runat="server" />
                            <asp:HiddenField ID="Hdf_No_Servicio" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Folio" runat="server" Font-Bold="true" ForeColor="Black" Text="Folio"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Folio" runat="server" Font-Bold="true" ForeColor="Red" Style="text-align: right;"
                                Width="98%"></asp:TextBox>
                        </td>
                        <td colspan="2">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Fecha_Elaboracion" runat="server" Text="Fecha Elaboración"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Fecha_Elaboracion" runat="server" Enabled="false" Width="98%"></asp:TextBox>
                        </td>
                        <td style="width: 15%;">
                            &nbsp;&nbsp;
                            <asp:Label ID="Lbl_Fecha_Recepcion" runat="server" Text="Fecha Recepción"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Fecha_Recepcion" runat="server" Enabled="false" Width="98%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text="Unidad Responsable"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="Cmb_Unidad_Responsable" runat="server" Enabled="false" Width="100%">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Kilometraje" runat="server" Text="Kilometraje"></asp:Label>
                        </td>
                        <td style="width: 16%;">
                            <asp:TextBox ID="Txt_Kilometraje" runat="server" Enabled="false" Width="95%"></asp:TextBox>
                        </td>
                        <td style="width: 15%;">
                            &nbsp;&nbsp;
                            <asp:Label ID="Lbl_Tipo_Servicio" runat="server" Text="Tipo Servicio"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:DropDownList ID="Cmb_Tipo_Servicio" runat="server" Enabled="false" Width="100%">
                                <asp:ListItem Value="">&lt; - - SELECCIONE - - &gt;</asp:ListItem>
                                <asp:ListItem Value="SERVICIO_CORRECTIVO">SERVICIO CORRECTIVO</asp:ListItem>
                                <asp:ListItem Value="SERVICIO_PREVENTIVO">SERVICIO PREVENTIVO</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Panel ID="Pnl_Vehiculo_Seleccionado" runat="server" GroupingText="Vehículo para el Servicio"
                                Width="99%">
                                <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                                    <tr>
                                        <td style="width: 15%;">
                                            <asp:HiddenField ID="Hdf_Vehiculo_ID" runat="server" />
                                            <asp:Label ID="Lbl_No_Inventario" runat="server" Text="No. Inventario"></asp:Label>
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:TextBox ID="Txt_No_Inventario" runat="server" Enabled="false" MaxLength="7"
                                                Width="70%"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Inventario" runat="server" FilterType="Numbers"
                                                TargetControlID="Txt_No_Inventario">
                                            </cc1:FilteredTextBoxExtender>
                                        </td>
                                        <td style="width: 15%;">
                                            &nbsp;&nbsp;
                                            <asp:Label ID="Lbl_No_Economico" runat="server" Text="No. Economico"></asp:Label>
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:TextBox ID="Txt_No_Economico" runat="server" Enabled="false" Width="98%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">
                                            <asp:Label ID="Lbl_Datos_Vehiculo" runat="server" Text="Vehículo"></asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="Txt_Datos_Vehiculo" runat="server" Enabled="false" Width="99%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">
                                            <asp:Label ID="Lbl_Placas" runat="server" Text="Placas"></asp:Label>
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:TextBox ID="Txt_Placas" runat="server" Enabled="false" Width="98%"></asp:TextBox>
                                        </td>
                                        <td style="width: 15%;">
                                            &nbsp;&nbsp;
                                            <asp:Label ID="Lbl_Anio" runat="server" Text="Año"></asp:Label>
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:TextBox ID="Txt_Anio" runat="server" Enabled="false" Width="98%"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="Pnl_Bien_Mueble_Seleccionado" runat="server" GroupingText="Bien Mueble para el Servicio"
                                Width="99%">
                                <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                                    <tr>
                                        <td style="width: 15%;">
                                            <asp:HiddenField ID="Hdf_Bien_Mueble_ID" runat="server" />
                                            <asp:HiddenField ID="Hdf_Tipo_Bien" runat="server" />
                                            <asp:Label ID="Lbl_No_Inventario_BM" runat="server" Text="No. Inventario"></asp:Label>
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:TextBox ID="Txt_No_Inventario_BM" runat="server" MaxLength="7" Width="98%" Enabled="false"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Inventario_BM" runat="server" FilterType="Numbers"
                                                TargetControlID="Txt_No_Inventario_BM">
                                            </cc1:FilteredTextBoxExtender>
                                        </td>
                                        <td style="width: 15%;">
                                            &nbsp;
                                        </td>
                                        <td style="width: 35%;">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">
                                            <asp:Label ID="Lbl_Descripcion_Bien" runat="server" Text="Descripción Bien"></asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="Txt_Descripcion_Bien" runat="server" Enabled="false" Rows="2" TextMode="MultiLine"
                                                Width="99%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">
                                            <asp:Label ID="Lbl_Numero_Serie_Bien" runat="server" Text="No. Serie"></asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="Txt_Numero_Serie_Bien" runat="server" Enabled="false" Width="99%"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Panel ID="Pnl_Descripcion_Servicio" runat="server" GroupingText="Descripción del Servicio"
                                Width="99%">
                                <asp:TextBox ID="Txt_Descripcion_Servicio" runat="server" Enabled="false" Rows="5"
                                    TextMode="MultiLine" Width="99%"></asp:TextBox>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
                <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                    <tr>
                        <td style="width: 20%;">
                            <b>Mecanico Asignado</b>
                        </td>
                        <td style="width: 80%;">
                            <asp:TextBox ID="Lbl_Mecanicos" runat="server" Enabled="false" Style="text-transform: uppercase"
                                Text="Mecanico Asignado" Width="95%"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <hr style="width: 98%;" />
                <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                    <tr style="background-color: #3366CC">
                        <td id="Td2" colspan="4" style="text-align: left; font-size: 15px; color: #FFFFFF;">
                            Diagnostico
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%">
                            Diagnostico
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Descripcion" runat="server" Enabled="false" Height="60px" Style="text-transform: uppercase"
                                TextMode="MultiLine" Width="97%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%">
                            &nbsp;
                        </td>
                        <td style="width: 32%">
                            &nbsp;
                        </td>
                        <td style="width: 18%">
                            &nbsp;
                        </td>
                        <td style="width: 32%">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%">
                            &nbsp;
                        </td>
                        <td style="width: 32%">
                            &nbsp;
                        </td>
                        <td style="width: 18%">
                            &nbsp;
                        </td>
                        <td style="width: 32%">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="width: 100%">
                            <hr style="width: 98%;" />
                        </td>
                    </tr>
                    <tr style="background-color: #3366CC">
                        <td id="Td1" colspan="4" style="text-align: left; font-size: 15px; color: #FFFFFF;
                            text-align: right;">
                            <label>
                            </label>
                            Capturar Seguimiento
                            <asp:ImageButton ID="Btn_Validar" runat="server" AlternateText="Validar" CssClass="Img_Button"
                                Height="18px" ImageUrl="~/paginas/imagenes/paginas/accept.png" OnClick="Btn_Asignacion_Click"
                                Style="float: right" ToolTip="Validar" Width="18px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%">
                            Seguimiento
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Seguimiento" runat="server" Enabled="true" Height="60px" Style="text-transform: uppercase"
                                TextMode="MultiLine" Width="95%"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="Txt_Seguimiento"
                                WatermarkCssClass="watermarked" WatermarkText="Límite de Caractes 1024">
                            </cc1:TextBoxWatermarkExtender>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                TargetControlID="Txt_Seguimiento" ValidChars="/*-+!#$%&amp;=?¡°!|/Ññ.,:;()áéíóúÁÉÍÓÚ ">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                </table>
                <hr style="width: 98%;" />
                <asp:Panel ID="Pnl_Historico" runat="server" GroupingText="Historial para el Servicio"
                    Style="overflow: auto; height: 200px; width: 99%; vertical-align: top; border-style: outset;
                    border-color: Silver;" Width="99%">
                    <center>
                        <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                            <%------------------ Historico ------------------%>
                            <tr>
                                <td style="text-align: left; vertical-align: top;">
                                    <asp:GridView ID="Grid_Historial_Proveedores" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                        CssClass="Tabla_Comentarios" HeaderStyle-CssClass="tblHead" OnPageIndexChanging="Grid_Historial_Proveedores_PageIndexChanging"
                                        PageSize="10" Style="white-space: normal;" Width="97%">
                                        <Columns>
                                            <asp:BoundField DataField="NO_SEGUIMIENTO_MECANICO" HeaderText="No. Observación">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="15%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="OBSERVACIONES" HeaderText="Observaciones">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="35%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="USUARIO_CREO" HeaderText="Usuario">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="35%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FECHA_CREO" DataFormatString="{0:dd/MMM/yyyy HH:mm:ss}"
                                                HeaderText="Fecha Creacion" SortExpression="FECHA_CREO">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="15%" />
                                            </asp:BoundField>
                                        </Columns>
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <PagerStyle CssClass="GridHeader" />
                                        <HeaderStyle CssClass="tblHead" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </center>
                </asp:Panel>
            </div>
            <asp:HiddenField ID="Hdf_Proveedor_ID" runat="server" />
            <asp:HiddenField ID="Hdf_No_Asignacion" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
