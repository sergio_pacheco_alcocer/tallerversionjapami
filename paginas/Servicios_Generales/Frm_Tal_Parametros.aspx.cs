﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;
using AjaxControlToolkit;
using System.IO;
public partial class paginas_Taller_Mecanico_Frm_Tal_Parametros : System.Web.UI.Page
{
    #region Variables
    private const int Const_Estado_Inicial = 0;
    private const int Const_Estado_Nuevo = 1;
    private const int Const_Estado_Modificar = 2;
    #endregion

    #region Page Load / Init
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!Page.IsPostBack)
            {
                Estado_Botones(Const_Estado_Inicial);
                Configurar_Formulario();
                cargar_combos();
            }
            Mensaje_Error();
        }
        catch (Exception Ex)
        {
            //Mensaje_Error(Ex.Message);
            Estado_Botones(Const_Estado_Inicial);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Configurar_Formulario
    ///DESCRIPCIÓN: Cargar Combos y Datos del formulario
    ///CREO: jtoledo
    ///FECHA_CREO: 17/May/2012
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    private void Configurar_Formulario()
    {
        try
        {
            Cargar_Combo_Partidas_Especificas();
            Cargar_Combo_Partidas_Genericas();
            Cargar_Combo_Conceptos();
            Cargar_Combo_Capitulos();
            Cargar_Combo_Programas();
            Borrar_Ventana_Emergente_Busqueda_Proveedores();
            Llenar_Combo_Unidades_Responsables();
            Llenar_Combo_Tipos_Solicitud_Pagos();
            Cargar_Combo_FF();
        }
        catch { }

    }
    private void cargar_combos()
    {
        Cls_Tal_Parametros_Negocio Parametro = new Cls_Tal_Parametros_Negocio();
        try
        {
            Parametro = Parametro.Consulta_Parametros();
            if (!String.IsNullOrEmpty(Parametro.P_Capitulo_ID))
            {
                Cmb_Capitulo.SelectedValue = Parametro.P_Capitulo_ID;
                Cmb_Capitulo_SelectedIndexChanged(null, EventArgs.Empty);
                Cmb_Conceptos.SelectedValue = Parametro.P_Concepto_ID;
                Cmb_Conceptos_SelectedIndexChanged(null, EventArgs.Empty);
                Cmb_Partida_General.SelectedValue = Parametro.P_Partida_Generica_ID;
                Cmb_Partida_General_SelectedIndexChanged(null, EventArgs.Empty);
                Cmb_Partida_Especifica.SelectedValue = Parametro.P_Partida_ID;
                Cmb_FF.SelectedValue = Parametro.P_Fuente_Financiamiento_ID;
                Cmb_Programa.SelectedValue = Parametro.P_Programa_ID;
                Cmb_Tipo_Solicitud_Pago.SelectedValue = Parametro.P_Tipo_Solicitud_Pago_ID;
                Cmb_Unidad_Responsable.SelectedValue = Parametro.P_Dependencia_ID;
                Txt_Oficial_Mayor.Text = Parametro.P_Oficial_Mayor;
                Txt_Director.Text = Parametro.P_Director;
                Txt_Coordinador.Text = Parametro.P_Coordinador;
                Txt_Almacen.Text = Parametro.P_Almacen;
                Txt_Nombre_Proveedor.Text = Parametro.P_Nombre_Proveedor;
                Hdf_Logo_Municipal.Value = Parametro.P_Logo_Municipio;
                Hdf_Logo_Estado.Value = Parametro.P_Logo_Estado;
            }
            else
            {
                Configurar_Formulario();
            }
        }
        catch { }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Unidades_Responsables
    ///DESCRIPCIÓN: Se llena el Combo de las Unidades Responsables.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 30/Abril/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Llenar_Combo_Unidades_Responsables()
    {
        Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
        Negocio.P_Estatus = "ACTIVO";
        DataTable Dt_Dependencias = Negocio.Consultar_Unidades_Responsables();
        Cmb_Unidad_Responsable.DataSource = Dt_Dependencias;
        Cmb_Unidad_Responsable.DataTextField = "CLAVE_NOMBRE";
        Cmb_Unidad_Responsable.DataValueField = "DEPENDENCIA_ID";
        Cmb_Unidad_Responsable.DataBind();
        Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("< -- SELECCIONE -- >", ""));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Tipos_Solicitud_Pagos
    ///DESCRIPCIÓN: Se llena el Combo de los tipos de solicitud de pago.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 30/Abril/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Llenar_Combo_Tipos_Solicitud_Pagos()
    {
        Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
        Negocio.P_Estatus = "ACTIVO";
        DataTable Dt_Tipos_Solicitud_Pagos = Negocio.Consultar_Tipos_Solicitud_Pago();
        Cmb_Tipo_Solicitud_Pago.DataSource = Dt_Tipos_Solicitud_Pagos;
        Cmb_Tipo_Solicitud_Pago.DataTextField = "DESCRIPCION";
        Cmb_Tipo_Solicitud_Pago.DataValueField = "TIPO_SOLICITUD_PAGO_ID";
        Cmb_Tipo_Solicitud_Pago.DataBind();
        Cmb_Tipo_Solicitud_Pago.Items.Insert(0, new ListItem("< -- SELECCIONE -- >", ""));
    }

    #endregion

    #region Metodos

    #region Metodos Generales
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN : Cargar_Ventana_Emergente_Busqueda_Proveedores
    ///DESCRIPCIÓN          : Establece el evento onclik del control para abrir la ventana emergente
    ///PARAMETROS: 
    ///CREO                 : Jesus Toledo Rodriguez
    ///FECHA_CREO           : 21/Otubre/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Cargar_Ventana_Emergente_Busqueda_Proveedores()
    {
        String Ventana_Modal = "Abrir_Ventana_Modal('Ventanas_Emergentes/Frm_Busqueda_Proveedores.aspx";
        String Propiedades = ", 'center:yes;resizable:no;status:no;dialogWidth:680px;dialogHide:true;help:no;scroll:no');";
        Btn_Busqueda_Proveedores.Attributes.Add("onclick", Ventana_Modal + "?Fecha=False'" + Propiedades);
    }
    private void Borrar_Ventana_Emergente_Busqueda_Proveedores()
    {
        Btn_Busqueda_Proveedores.Attributes.Clear();
    }
    ///****************************************************************************************
    ///NOMBRE DE LA FUNCION:Mensaje_Error
    ///DESCRIPCION : Muestra el error
    ///PARAMETROS  : P_Texto: texto de un TextBox
    ///CREO        : Toledo Rodriguez Jesus S.
    ///FECHA_CREO  : 04-Septiembre-2010
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACION:
    ///****************************************************************************************
    private void Mensaje_Error(String P_Mensaje)
    {
        Img_Error.Visible = true;
        Lbl_Mensaje_Error.Text += P_Mensaje + "</br>";
    }
    private void Mensaje_Error()
    {
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Lbl_Ecabezado_Mensaje.Text = "";
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Estado_Botones
    ///DESCRIPCIÓN: Metodo para establecer el estado de los botones y componentes del formulario
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 02/02/2011 05:49:53 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Estado_Botones(int P_Estado)
    {

        Boolean Estado = false;
        switch (P_Estado)
        {
            case 0: //Estado inicial  
                Btn_Modificar.AlternateText = "Modificar";
                Btn_Salir.AlternateText = "Inicio";

                Btn_Modificar.ToolTip = "Modificar";
                Btn_Salir.ToolTip = "Inicio";

                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";

                Btn_Modificar.Visible = true;
                Btn_Salir.Visible = true;

                Estado = false;

                AFU_Logo_Municipio.Style["Display"] = "none";
                AFU_Logo_Estado.Style["Display"] = "none";
                Borrar_Ventana_Emergente_Busqueda_Proveedores();
                break;

            case 1: //Nuevo  
                Borrar_Ventana_Emergente_Busqueda_Proveedores();
                Btn_Modificar.AlternateText = "Modificar";
                Btn_Salir.AlternateText = "Cancelar";

                Btn_Modificar.ToolTip = "Modificar";
                Btn_Salir.ToolTip = "Cancelar";

                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";

                Btn_Modificar.Visible = false;
                Btn_Salir.Visible = true;

                Estado = true;
                break;

            case 2: //Modificar                    
                Cargar_Ventana_Emergente_Busqueda_Proveedores();
                Btn_Modificar.AlternateText = "Actualizar";
                Btn_Salir.AlternateText = "Cancelar";

                Btn_Modificar.ToolTip = "Actualizar";
                Btn_Salir.ToolTip = "Cancelar";

                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";

                Btn_Modificar.Visible = true;
                Btn_Salir.Visible = true;

                Estado = true;

                AFU_Logo_Municipio.Style["Display"] = "block";
                AFU_Logo_Estado.Style["Display"] = "block";

                break;

        }
        Cmb_Conceptos.Enabled = Estado;
        Cmb_Tipo_Solicitud_Pago.Enabled = Estado;
        Cmb_Capitulo.Enabled = Estado;
        Cmb_Partida_Especifica.Enabled = Estado;
        Cmb_Partida_General.Enabled = Estado;
        Cmb_FF.Enabled = Estado;
        Cmb_Programa.Enabled = Estado;
        Cmb_Unidad_Responsable.Enabled = Estado;
        Txt_Oficial_Mayor.Enabled = Estado;
        Txt_Director.Enabled = Estado;
        Txt_Coordinador.Enabled = Estado;
        Txt_Almacen.Enabled = Estado;


        Btn_Logo_Municipio.Enabled = !Estado;
        Btn_Logo_Estado.Enabled = !Estado;


    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
    ///DESCRIPCIÓN: Metodo para establecer el estado de los botones y componentes del formulario
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 02/02/2011 05:49:53 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Limpiar_Formulario()
    {
        Hdf_Logo_Municipal.Value = "";
        Hdf_Logo_Estado.Value = "";
        Hdf_Proveedor_ID.Value = "";
        Remover_Sesiones_Control_AsyncFileUpload(AFU_Logo_Municipio.ClientID);
        Remover_Sesiones_Control_AsyncFileUpload(AFU_Logo_Estado.ClientID);
        cargar_combos();
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Remover_Sesiones_Control_AsyncFileUpload
    ///DESCRIPCIÓN: Limpia un control de AsyncFileUpload
    ///PROPIEDADES:     
    ///CREO: Juan Alberto Hernandez Negrete
    ///FECHA_CREO: 16/Febrero/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Remover_Sesiones_Control_AsyncFileUpload(String Cliente_ID)
    {
        HttpContext Contexto;
        if (HttpContext.Current != null && HttpContext.Current.Session != null)
        {
            Contexto = HttpContext.Current;
        }
        else
        {
            Contexto = null;
        }
        if (Contexto != null)
        {
            foreach (String key in Contexto.Session.Keys)
            {
                if (key.Contains(Cliente_ID))
                {
                    Contexto.Session.Remove(key);
                    break;
                }
            }
        }
    }

    #endregion

    #region metodos Combos_Sap
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cmb_Capitulo_SelectedIndexChanged
    ///DESCRIPCIÓN: Paginar grid de ordenes de variacion
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 01/Dic/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Cmb_Capitulo_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cargar_Combo_Conceptos();
        Cmb_Conceptos_SelectedIndexChanged(Cmb_Conceptos, null);
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cmb_Conceptos_SelectedIndexChanged
    ///DESCRIPCIÓN: Paginar grid de ordenes de variacion
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 01/Dic/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Cmb_Conceptos_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cargar_Combo_Partidas_Genericas();
        Cmb_Partida_General_SelectedIndexChanged(Cmb_Partida_General, null);
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cmb_Partida_General_SelectedIndexChanged
    ///DESCRIPCIÓN: Paginar grid de ordenes de variacion
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 01/Dic/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Cmb_Partida_General_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cargar_Combo_Partidas_Especificas();
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cargar_Combo_Programas
    ///DESCRIPCIÓN: Cargar_Combo_Programas
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 01/Dic/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************* 
    private void Cargar_Combo_Programas()
    {
        Cls_Ope_Tal_Consultas_Generales_Negocio Consultas_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
        Consultas_Negocio.P_Estatus = "ACTIVO";
        DataTable Dt_Programas = Consultas_Negocio.Consultar_Proyectos_Programas();
        Cmb_Programa.DataSource = Dt_Programas;
        Cmb_Programa.DataValueField = "PROGRAMA_ID";
        Cmb_Programa.DataTextField = "CLAVE_NOMBRE";
        Cmb_Programa.DataBind();
        Cmb_Programa.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cargar_Combo_Partidas_Especificas
    ///DESCRIPCIÓN: Cargar_Combo_Partidas_Especificas
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 01/Dic/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************* 
    private void Cargar_Combo_Capitulos()
    {
        Cls_Ope_Tal_Consultas_Generales_Negocio Consultas_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
        Consultas_Negocio.P_Estatus = "ACTIVO";
        DataTable Dt_Capitulos = Consultas_Negocio.Consultar_Capitulos();
        Cmb_Capitulo.DataSource = Dt_Capitulos;
        Cmb_Capitulo.DataValueField = "CAPITULO_ID";
        Cmb_Capitulo.DataTextField = "CLAVE_NOMBRE";
        Cmb_Capitulo.DataBind();
        Cmb_Capitulo.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cmb_Partida_General_SelectedIndexChanged
    ///DESCRIPCIÓN: Accion de Seleccion de Cmb_Partida_General
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 01/Dic/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************* 
    private void Cargar_Combo_Conceptos()
    {
        if (Cmb_Capitulo.SelectedIndex > 0)
        {
            Cls_Ope_Tal_Consultas_Generales_Negocio Consultas_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            Consultas_Negocio.P_Estatus = "ACTIVO";
            Consultas_Negocio.P_Capitulo_ID = Cmb_Capitulo.SelectedItem.Value.Trim();
            DataTable Dt_Conceptos = Consultas_Negocio.Consultar_Conceptos();
            Cmb_Conceptos.DataSource = Dt_Conceptos;
            Cmb_Conceptos.DataValueField = "CONCEPTO_ID";
            Cmb_Conceptos.DataTextField = "CLAVE_NOMBRE";
            Cmb_Conceptos.DataBind();
            Cmb_Conceptos.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        }
        else
        {
            Cmb_Conceptos.Items.Clear();
            Cmb_Conceptos.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        }

    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cargar_Combo_Partidas_Genericas
    ///DESCRIPCIÓN: Cargar_Combo_Partidas_Genericas
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 01/Dic/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************* 
    private void Cargar_Combo_Partidas_Genericas()
    {
        if (Cmb_Conceptos.SelectedIndex > 0)
        {
            Cls_Ope_Tal_Consultas_Generales_Negocio Consultas_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            Consultas_Negocio.P_Estatus = "ACTIVO";
            Consultas_Negocio.P_Concepto_ID = Cmb_Conceptos.SelectedItem.Value.Trim();
            DataTable Dt_Partidas = Consultas_Negocio.Consultar_Partidas_Genericas();
            Cmb_Partida_General.DataSource = Dt_Partidas;
            Cmb_Partida_General.DataValueField = "PARTIDA_ID";
            Cmb_Partida_General.DataTextField = "CLAVE_NOMBRE";
            Cmb_Partida_General.DataBind();
            Cmb_Partida_General.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        }
        else
        {
            Cmb_Partida_General.Items.Clear();
            Cmb_Partida_General.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        }

    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cargar_Combo_Partidas_Especificas
    ///DESCRIPCIÓN: Cargar_Combo_Partidas_Especificas
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 01/Dic/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************* 
    private void Cargar_Combo_Partidas_Especificas()
    {
        if (Cmb_Partida_General.SelectedIndex > 0)
        {
            Cls_Ope_Tal_Consultas_Generales_Negocio Consultas_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            Consultas_Negocio.P_Estatus = "ACTIVO";
            Consultas_Negocio.P_Partida_Generica_ID = Cmb_Partida_General.SelectedItem.Value.Trim();
            DataTable Dt_Partidas = Consultas_Negocio.Consultar_Partidas_Especificas();
            Cmb_Partida_Especifica.DataSource = Dt_Partidas;
            Cmb_Partida_Especifica.DataValueField = "PARTIDA_ID";
            Cmb_Partida_Especifica.DataTextField = "CLAVE_NOMBRE";
            Cmb_Partida_Especifica.DataBind();
            Cmb_Partida_Especifica.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        }
        else
        {
            Cmb_Partida_Especifica.Items.Clear();
            Cmb_Partida_Especifica.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        }

    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cargar_Combo_Partidas_Especificas
    ///DESCRIPCIÓN: Cargar_Combo_Partidas_Especificas
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 01/Dic/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************* 
    private void Cargar_Combo_FF()
    {

        Cls_Ope_Tal_Consultas_Generales_Negocio Consultas_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
        Consultas_Negocio.P_Estatus = "ACTIVO";
        DataTable Dt_Partidas = Consultas_Negocio.Consultar_Fuentes_Financiamiento();
        Cmb_FF.DataSource = Dt_Partidas;
        Cmb_FF.DataValueField = "FUENTE_FINANCIAMIENTO_ID";
        Cmb_FF.DataTextField = "CLAVE_NOMBRE";
        Cmb_FF.DataBind();
        Cmb_FF.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));

    }
    #endregion
    #endregion

    #region Eventos
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Evento que agarra la session de las refacciones seleccionadas
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 10/Mayo/2011 12:55:33 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    protected void Btn_Busqueda_Proveedores_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["Proveedor_ID"] != null && Session["Nombre_Proveedor"] != null)
        {
            Hdf_Proveedor_ID.Value = Session["Proveedor_ID"].ToString();
            Txt_Nombre_Proveedor.Text = Session["Nombre_Proveedor"].ToString();
        }
        Session["Proveedor_ID"] = null;
        Session["Nombre_Proveedor"] = null;
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Salir/Cancelar
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 10/Mayo/2011 12:55:33 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Salir.AlternateText.Equals("Inicio"))
            {
                Limpiar_Formulario();
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Estado_Botones(Const_Estado_Inicial);
                Limpiar_Formulario();
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
    ///DESCRIPCIÓN: se obtienen los datos para modificar los paramentros
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 06/27/2011 11:10:44 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Modificar.AlternateText == "Modificar")
            {
                Estado_Botones(Const_Estado_Modificar);
                Session["Nombre_Archivo"] = "";
                Session["Nombre_Archivo_Logo_Estado"] = "";

            }
            else if (Btn_Modificar.AlternateText == "Actualizar")
            {
                Alta_Parametros();
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message.ToString());
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Alta_Parametros
    ///DESCRIPCIÓN: se obtienen los datos para modificar los paramentros
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 06/27/2012
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    private void Alta_Parametros()
    {
        Cls_Tal_Parametros_Negocio Parametro = new Cls_Tal_Parametros_Negocio();
        try
        {
            if (Cmb_FF.SelectedIndex > 0 && Cmb_Programa.SelectedIndex > 0)
            {
                Parametro.P_Programa_ID = Cmb_Programa.SelectedValue.Trim();
                Parametro.P_Partida_ID = Cmb_Partida_Especifica.SelectedValue.Trim();
                Parametro.P_Fuente_Financiamiento_ID = Cmb_FF.SelectedValue.Trim();
                Parametro.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedValue.Trim();
                Parametro.P_Tipo_Solicitud_Pago_ID = Cmb_Tipo_Solicitud_Pago.SelectedValue.Trim();
                Parametro.P_Oficial_Mayor = Txt_Oficial_Mayor.Text.Trim();
                Parametro.P_Director = Txt_Director.Text.Trim();
                Parametro.P_Coordinador = Txt_Coordinador.Text.Trim();
                Parametro.P_Almacen = Txt_Almacen.Text.Trim();
                Parametro.P_Proveedor_Gasolina_Id = Hdf_Proveedor_ID.Value.Trim();
                if (Session["Nombre_Archivo"].ToString() == "")
                {
                    Parametro.P_Logo_Municipio = "";
                }
                else
                {

                    Parametro.P_Logo_Municipio = "Logo_Municipio." + Session["Nombre_Archivo"];

                }
                if (Session["Nombre_Archivo_Logo_Estado"].ToString() == "")
                {
                    Parametro.P_Logo_Estado = "";
                }
                else
                {

                    Parametro.P_Logo_Estado = "Logo_Estado." + Session["Nombre_Archivo_Logo_Estado"];

                }
                if (AFU_Logo_Municipio.HasFile) Parametro.P_Logo_Municipio = "Logo_Municipio." + Session["Nombre_Archivo"];
                if (AFU_Logo_Estado.HasFile) Parametro.P_Logo_Estado = "Logo_Estado." + System.IO.Path.GetFileName(AFU_Logo_Estado.FileName);
                Parametro.Alta_Parametros();
                String Ruta = Server.MapPath("../../ARCHIVOS_TALLER_MUNICIPAL/PARAMETROS");
                if (!System.IO.Directory.Exists(Ruta)) { System.IO.Directory.CreateDirectory(Ruta); }
                if (AFU_Logo_Municipio.HasFile) AFU_Logo_Municipio.SaveAs(Ruta + "/" + "Logo_Municipio." + Session["Nombre_Archivo"]);
                if (AFU_Logo_Estado.HasFile) AFU_Logo_Estado.SaveAs(Ruta + "/" + "Logo_Estado." + System.IO.Path.GetFileName(AFU_Logo_Estado.FileName));
                Estado_Botones(Const_Estado_Inicial);
                Limpiar_Formulario();
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Parametros", "alert('Parametros Capturados Exitosamente!')", true);
            }
            else
            {
                Mensaje_Error("Faltan Datos por Capturar");
            }
        }
        catch (Exception ex)
        {
            Mensaje_Error(ex.Message);
        }

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Logo_Municipio_Click
    ///DESCRIPCIÓN:  
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 16/Febrero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Logo_Municipio_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (!String.IsNullOrEmpty(Hdf_Logo_Municipal.Value))
            {
                String Archivo = "../../ARCHIVOS_TALLER_MUNICIPAL/PARAMETROS/" + Hdf_Logo_Municipal.Value.Trim();
                if (System.IO.File.Exists(Server.MapPath(Archivo)))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Archivo_Archivos", "window.open('" + Archivo + "','Window_Archivo','left=0,top=0')", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('El Archivo no Existe o fue eliminado fisicamente.');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('No hay Archivo Cargado');", true);
            }
        }
        catch (Exception Ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('No se puede mostrar el Archivo');", true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Logo_Estado_Click
    ///DESCRIPCIÓN:  
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 16/Febrero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Logo_Estado_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (!String.IsNullOrEmpty(Hdf_Logo_Estado.Value))
            {
                String Archivo = "../../ARCHIVOS_TALLER_MUNICIPAL/PARAMETROS/" + Hdf_Logo_Estado.Value.Trim();
                if (System.IO.File.Exists(Server.MapPath(Archivo)))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Archivo_Archivos", "window.open('" + Archivo + "','Window_Archivo','left=0,top=0')", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('El Archivo no Existe o fue eliminado fisicamente.');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('No hay Archivo Cargado');", true);
            }
        }
        catch (Exception Ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('No se puede mostrar el Archivo');", true);
        }
    }

    #endregion




    ///****************************************************************************************
    ///NOMBRE DE LA FUNCION: Asy_Cargar_Archivo_Complete
    ///DESCRIPCION         : Verifica la extension del archivo que se agrego al boton AsyncFileUpload
    ///PARAMETROS         : 
    ///CREO               :Yazmin Flores Ramirez
    ///FECHA_CREO         : 11/Junio/2011 09:19 am
    ///MODIFICO           :
    ///FECHA_MODIFICO     :
    ///CAUSA_MODIFICACION :
    ///****************************************************************************************
    protected void Asy_Cargar_Archivo_Complete(Object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        System.Threading.Thread.Sleep(1000);
        try
        {
            AsyncFileUpload archivo;
            archivo = (AsyncFileUpload)sender;
            String Filename;
            Filename = archivo.FileName;
            if (!String.IsNullOrEmpty(Filename))
            {
                String[] arr1;
                arr1 = Filename.Split('\\');
                int len = arr1.Length;
                String img1 = arr1[len - 1];
                String filext = img1.Substring(img1.LastIndexOf(".") + 1);
                if (filext == "txt" || filext == "doc" || filext == "zip" || filext == "pdf" || filext == "rar" || filext == "docx" || filext == "jpg" || filext == "JPG" || filext == "jpeg" || filext == "JPEG" || filext == "png" || filext == "PNG" || filext == "gif" || filext == "GIF" || filext == "xlsx")
                {
                    if (archivo.FileContent.Length > 2621440) { return; }
                }
                else
                {
                    //Return
                }
                Guardar_Documentos_Anexos();
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }


    /// *************************************************************************************
    /// NOMBRE:              ClearSession_AsyncFileUpload
    /// DESCRIPCIÓN:         limpia el control de asuncfileupload.
    /// PARÁMETROS:
    /// USUARIO CREO:        Sergio Manuel Gallardo Andrade
    /// FECHA CREO:          28-febrero-2012
    /// USUARIO MODIFICO:    
    /// FECHA MODIFICO:      
    /// CAUSA MODIFICACIÓN:  
    /// *************************************************************************************
    protected void ClearSession_AsyncFileUpload(String ClientID)
    {
        HttpContext currentContext;
        if (HttpContext.Current != null && HttpContext.Current.Session != null)
        {
            currentContext = HttpContext.Current;
        }
        else
        {
            currentContext = null;
        }
        if (currentContext != null)
        {
            foreach (String Key in currentContext.Session.Keys)
            {
                if (Key.Contains(ClientID))
                {
                    currentContext.Session.Remove(Key);
                    break;
                }
            }
        }
    }
    /// *************************************************************************************
    /// NOMBRE:              Guardar_Documentos_Anexos
    /// DESCRIPCIÓN:         Guarda el archivo en una carpeta temporal y devuelve la extension
    /// PARÁMETROS:
    /// USUARIO CREO:        Sergio Manuel Gallardo Andrade
    /// FECHA CREO:          28-febrero-2012
    /// USUARIO MODIFICO:    
    /// FECHA MODIFICO:      
    /// CAUSA MODIFICACIÓN:  
    /// *************************************************************************************
    protected String Guardar_Documentos_Anexos()
    {
        DataTable Dt_Documentos_Anexos = new DataTable();
        AsyncFileUpload Asy_FileUpload;
        String appPath = "";
        String Nombre_Directorio = "";
        string Ruta = "";
        string Nombre_Archivo = "";
        string A = "";
        try
        {
            //Se obtiene la direccion en donde se va a guardar el archivo
            appPath = Server.MapPath("~");
            //Crear el Directorio de los archivos
            if (!Directory.Exists(appPath))
            {
                System.IO.Directory.CreateDirectory(appPath);
            }
            //Se establece el nombre del directorio
            Nombre_Directorio = "ARCHIVOS_TALLER_MUNICIPAL\\PARAMETROS";
            if (Directory.Exists(appPath))
            {
                //Se asigna el control AsyncFileUpload1 a la variable Async_FileUpload
                Asy_FileUpload = AFU_Logo_Municipio;
                if (!String.IsNullOrEmpty(Asy_FileUpload.FileName))
                {
                    //Valida que no exista el directorio, si no existe lo crea
                    DirectoryInfo Directorio;
                    if (Directory.Exists(appPath + "\\" + Nombre_Directorio) == false)
                    {
                        Directorio = Directory.CreateDirectory(appPath + "\\" + Nombre_Directorio);
                    }
                    //Se asigna el directorio en donde se va a guardar los documentos
                    String saveDir = Nombre_Directorio + "\\";
                    String savePath = appPath + "\\" + saveDir + "" + "Logo_Municipio." + Asy_FileUpload.FileName;
                    Server.HtmlEncode(Asy_FileUpload.FileName);
                    if (Asy_FileUpload.HasFile)
                    {
                        //Se guarda el archivo
                        Asy_FileUpload.SaveAs(HttpUtility.HtmlEncode(savePath));
                        Ruta = HttpUtility.HtmlEncode(savePath);
                        Nombre_Archivo = HttpUtility.HtmlEncode(Asy_FileUpload.FileName);
                        Session["Nombre_Archivo"] = Nombre_Archivo;
                    }
                }
            }
            return (appPath + "\\" + Nombre_Directorio + "\\" + Nombre_Archivo);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }

    }


    //****************************************************************************************
    //NOMBRE DE LA FUNCION: Asy_Cargar_Archivo
    ///DESCRIPCION         : Verifica la extension del archivo que se agrego al boton AsyncFileUpload
    //PARAMETROS         : 
    //CREO               :Yazmin Flores Ramirez
    //FECHA_CREO         : 11/Junio/2011 09:19 am
    //MODIFICO           :
    //FECHA_MODIFICO     :
    //CAUSA_MODIFICACION :
    //****************************************************************************************
    protected void Asy_Cargar_Archivo_Logo_Estado(Object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        System.Threading.Thread.Sleep(1000);
        try
        {
            AsyncFileUpload archivo;
            archivo = (AsyncFileUpload)sender;
            String Filename;
            Filename = archivo.FileName;
            if (!String.IsNullOrEmpty(Filename))
            {
                String[] arr1;
                arr1 = Filename.Split('\\');
                int len = arr1.Length;
                String img1 = arr1[len - 1];
                String filext = img1.Substring(img1.LastIndexOf(".") + 1);
                if (filext == "txt" || filext == "doc" || filext == "zip" || filext == "pdf" || filext == "rar" || filext == "docx" || filext == "jpg" || filext == "JPG" || filext == "jpeg" || filext == "JPEG" || filext == "png" || filext == "PNG" || filext == "gif" || filext == "GIF" || filext == "xlsx")
                {
                    if (archivo.FileContent.Length > 2621440) { return; }
                }
                else
                {
                    //Return
                }
                Guardar_Documentos_Anexos_Logo_Estado();
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }


    /// *************************************************************************************
    /// NOMBRE:              ClearSession_AsyncFileUpload
    /// DESCRIPCIÓN:         limpia el control de asuncfileupload.
    /// PARÁMETROS:
    /// USUARIO CREO:        Sergio Manuel Gallardo Andrade
    /// FECHA CREO:          28-febrero-2012
    /// USUARIO MODIFICO:    
    /// FECHA MODIFICO:      
    /// CAUSA MODIFICACIÓN:  
    /// *************************************************************************************
    protected void ClearSession_AsyncFileUpload_Logo_Estado(String ClientID)
    {
        HttpContext currentContext;
        if (HttpContext.Current != null && HttpContext.Current.Session != null)
        {
            currentContext = HttpContext.Current;
        }
        else
        {
            currentContext = null;
        }
        if (currentContext != null)
        {
            foreach (String Key in currentContext.Session.Keys)
            {
                if (Key.Contains(ClientID))
                {
                    currentContext.Session.Remove(Key);
                    break;
                }
            }
        }
    }
    /// *************************************************************************************
    /// NOMBRE:              Guardar_Documentos_Anexos
    /// DESCRIPCIÓN:         Guarda el archivo en una carpeta temporal y devuelve la extension
    /// PARÁMETROS:
    /// USUARIO CREO:        Sergio Manuel Gallardo Andrade
    /// FECHA CREO:          28-febrero-2012
    /// USUARIO MODIFICO:    
    /// FECHA MODIFICO:      
    /// CAUSA MODIFICACIÓN:  
    /// *************************************************************************************
    protected String Guardar_Documentos_Anexos_Logo_Estado()
    {
        DataTable Dt_Documentos_Anexos = new DataTable();
        AsyncFileUpload Asy_FileUpload;
        String appPath = "";
        String Nombre_Directorio = "";
        string Ruta = "";
        string Nombre_Archivo = "";
        string A = "";
        try
        {

            //Se obtiene la direccion en donde se va a guardar el archivo
            appPath = Server.MapPath("~");
            //Crear el Directorio de los archivos

            if (!Directory.Exists(appPath))
            {
                System.IO.Directory.CreateDirectory(appPath);
            }

            //Se establece el nombre del directorio
            Nombre_Directorio = "ARCHIVOS_TALLER_MUNICIPAL\\PARAMETROS";
            if (Directory.Exists(appPath))
            {
                //Se asigna el control AsyncFileUpload1 a la variable Async_FileUpload
                Asy_FileUpload = AFU_Logo_Estado;
                if (!String.IsNullOrEmpty(Asy_FileUpload.FileName))
                {
                    //Valida que no exista el directorio, si no existe lo crea
                    DirectoryInfo Directorio;
                    if (Directory.Exists(appPath + "\\" + Nombre_Directorio) == false)
                    {
                        Directorio = Directory.CreateDirectory(appPath + "\\" + Nombre_Directorio);
                    }
                    //Se asigna el directorio en donde se va a guardar los documentos
                    String saveDir = Nombre_Directorio + "\\";
                    String savePath = appPath + "\\" + saveDir + "" + "Logo_Estado." + Asy_FileUpload.FileName;
                    Server.HtmlEncode(Asy_FileUpload.FileName);
                    if (Asy_FileUpload.HasFile)
                    {
                        //Se guarda el archivo
                        Asy_FileUpload.SaveAs(HttpUtility.HtmlEncode(savePath));
                        Ruta = HttpUtility.HtmlEncode(savePath);
                        Nombre_Archivo = HttpUtility.HtmlEncode(Asy_FileUpload.FileName);
                        Session["Nombre_Archivo_Logo_Estado"] = Nombre_Archivo;
                    }
                }
            }
            return (appPath + "\\" + Nombre_Directorio + "\\" + Nombre_Archivo);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }

    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Generar_Tabla_Documentos
    /// DESCRIPCION: Genera la tabla de Documentos, el esquema para guardar los tipos de document a recibir
    /// PARAMETROS: 
    /// CREO: Roberto González Oseguera
    /// FECHA_CREO: 04-may-2011
    /// MODIFICO:
    /// FECHA_MODIFICO:
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private DataTable Generar_Tabla_Documentos()
    {
        DataTable Tabla_Nueva = new DataTable();
        DataColumn Columna0_Clave_Documento;
        DataColumn Columna1_Nombre_Documento;
        DataColumn Columna2_Nombre_Archivo;
        DataColumn Columna3_Ruta_Archivo;
        DataColumn Columna4_Archivo;
        DataColumn Columna5_Checksum;
        DataColumn Columna6_No_Movimiento;

        try
        {
            // ---------- Inicializar columnas
            Columna0_Clave_Documento = new DataColumn();
            Columna0_Clave_Documento.DataType = System.Type.GetType("System.String");
            Columna0_Clave_Documento.ColumnName = "CLAVE_DOCUMENTO";
            Tabla_Nueva.Columns.Add(Columna0_Clave_Documento);
            Columna1_Nombre_Documento = new DataColumn();
            Columna1_Nombre_Documento.DataType = System.Type.GetType("System.String");
            Columna1_Nombre_Documento.ColumnName = "NOMBRE_DOCUMENTO";
            Tabla_Nueva.Columns.Add(Columna1_Nombre_Documento);
            Columna2_Nombre_Archivo = new DataColumn();
            Columna2_Nombre_Archivo.DataType = System.Type.GetType("System.String");
            Columna2_Nombre_Archivo.ColumnName = "NOMBRE_ARCHIVO";
            Tabla_Nueva.Columns.Add(Columna2_Nombre_Archivo);
            Columna3_Ruta_Archivo = new DataColumn();
            Columna3_Ruta_Archivo.DataType = System.Type.GetType("System.String");
            Columna3_Ruta_Archivo.ColumnName = "RUTA_ARCHIVO";
            Tabla_Nueva.Columns.Add(Columna3_Ruta_Archivo);
            Columna4_Archivo = new DataColumn();
            Columna4_Archivo.DataType = System.Type.GetType("System.String");
            Columna4_Archivo.ColumnName = "ARCHIVO";
            Tabla_Nueva.Columns.Add(Columna4_Archivo);
            Columna5_Checksum = new DataColumn();
            Columna5_Checksum.DataType = System.Type.GetType("System.String");
            Columna5_Checksum.ColumnName = "CHECKSUM";
            Tabla_Nueva.Columns.Add(Columna5_Checksum);
            Columna6_No_Movimiento = new DataColumn();
            Columna6_No_Movimiento.DataType = System.Type.GetType("System.String");
            Columna6_No_Movimiento.ColumnName = "NO_MOVIMIENTO";
            Tabla_Nueva.Columns.Add(Columna6_No_Movimiento);

            return Tabla_Nueva;
        }
        catch (Exception ex)
        {
            throw new Exception("Generar_Tabla_Documentos " + ex.Message.ToString(), ex);
        }
    }
}