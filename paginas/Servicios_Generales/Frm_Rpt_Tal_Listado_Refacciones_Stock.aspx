﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Rpt_Tal_Listado_Refacciones_Stock.aspx.cs" Inherits="paginas_Taller_Mecanico_Frm_Rpt_Tal_Listado_Refacciones_Stock" Title="Listado de Refacciones de Stock" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script language="javascript" type="text/javascript">
    <!--
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_Session.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para matener la sesión activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);
        
    //-->
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"  AsyncPostBackTimeout="36000" EnableScriptLocalization="true" EnableScriptGlobalization="true"/> 
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>                     
            </asp:UpdateProgress>
            <div id="Div_Requisitos" style="background-color:#ffffff; width:100%; height:100%;">
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo" colspan="2">Listado de Stock</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                          <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;">              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top">
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                            </table>                   
                          </div>                
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>                        
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" style="width:50%;">
                            <asp:ImageButton ID="Btn_Reporte_PDF" runat="server" OnClick = "Btn_Reporte_PDF_Click"
                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" Width="24px" CssClass="Img_Button"
                                AlternateText="Modificar" />
                            <asp:ImageButton ID="Btn_Reporte_EXCEL" runat="server"
                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png" Width="24px" CssClass="Img_Button"
                                AlternateText="Salir" />
                        </td>
                        <td style="width:50%;">&nbsp;&nbsp;
                             <asp:ImageButton ID="Btn_Limpiar_Todo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_Limpiar.png" AlternateText="Limpiar Todo" ToolTip="Limpiar Todo" Width="16px" Height="16px" 
                              OnClick = "Btn_Limpiar_Todo_Click"/>
                        </td>                        
                    </tr>
                </table>  
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">                                     
                    <tr>
                        <td style="text-align:left; width: 18%;">
                            <asp:Label ID="Lbl_Capitulo" runat="server" Text="Capítulo"></asp:Label>
                        </td>
                        <td colspan="3" style="text-align:left; width: 82%;">
                            <asp:DropDownList ID="Cmb_Capitulo" runat="server" AutoPostBack="True" Width="98%" OnSelectedIndexChanged="Cmb_Capitulo_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>                                                                                                                                                                                                                
                    </tr>
                    <tr>
                        <td style="text-align:left;width:18%;">
                            <asp:Label ID="Lbl_Concepto" runat="server" Text="Concepto"></asp:Label>
                        </td>
                        <td colspan="3" style="text-align:left; width: 82%;">
                            <asp:DropDownList runat="server" ID="Cmb_Conceptos" AutoPostBack="true" Width="98%" OnSelectedIndexChanged="Cmb_Conceptos_SelectedIndexChanged"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:left; width: 18%;">
                            <asp:Label ID="Lbl_Partida_General" runat="server" Text="Partida Generica"></asp:Label>
                        </td>
                        <td colspan="3" style="text-align:left; width: 82%;">
                            <asp:DropDownList ID="Cmb_Partida_General" runat="server" AutoPostBack="True" Width="98%" OnSelectedIndexChanged="Cmb_Partida_General_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        
                    </tr>
                    <tr>
                        <td style="text-align:left; width: 18%;">
                            <asp:Label ID="Lbl_Partida_Especifica" runat="server" Text="Partida Especifica"></asp:Label>
                        </td>
                        <td colspan="3" style="text-align:left; width: 82%;">
                            <asp:DropDownList ID="Cmb_Partida_Especifica" runat="server" Width="98%">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:left; width:">
                            <asp:Label ID="Lbl_Txt_Ubicacion" runat="server" AssociatedControlID="Txt_Ubicacion" Text="Ubicación"></asp:Label>
                        </td>
                        <td colspan="3" style="text-align:left;width:82%;">
                            <asp:TextBox ID="Txt_Ubicacion" runat="server" MaxLength="255" style="width:97%;" />
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Ubicacion" runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" TargetControlID="Txt_Ubicacion" 
                                ValidChars="ÑñáéíóúÁÉÍÓÚ. " />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:left; width: 18%;">
                            <asp:Label ID="Lbl_Tipo_Refaccion" runat="server" Text="Tipo Refaccion"></asp:Label>
                        </td>
                        <td colspan="3" style="text-align:left; width: 82%;">
                            <asp:DropDownList ID="Cmb_Tipo_Refaccion" runat="server" Width="98%">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <br />
            <br />
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

