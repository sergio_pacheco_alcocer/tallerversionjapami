<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Cat_Tal_Tarjetas_Gasolina.aspx.cs" Inherits="paginas_Taller_Mecanico_Frm_Cat_Tal_Tarjetas_Gasolina"
    Title="Catalogo de Tarjetas de Gasolina" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript" language="javascript">
        function Limpiar_Ctlr_Busqueda_Resguardante() {
            document.getElementById("<%=Txt_Busqueda_No_Empleado.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_RFC.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_Nombre_Empleado.ClientID%>").value = "";
            document.getElementById("<%=Cmb_Busqueda_Dependencia.ClientID%>").value = "";
            return false;
        }
        function Tipo_Bien() {
            var Combo = "";
            Combo = document.getElementById('<%= Cmb_Tipo_Bien.ClientID %>');
            Beneficio = Combo.options[Combo.selectedIndex].text.toUpperCase();
            if (Beneficio.indexOf("VEHICULO") != -1) {
                document.getElementById('<%= Txt_No_Economico.ClientID %>').disabled = false;
                document.getElementById('<%= Txt_No_Inventario.ClientID %>').disabled = false;
            }
            else if (Beneficio.indexOf("MUEBLE") != -1) {
                document.getElementById('<%= Txt_No_Economico.ClientID %>').value = "";
                document.getElementById('<%= Txt_No_Economico.ClientID %>').disabled = true;
                document.getElementById('<%= Txt_No_Inventario.ClientID %>').disabled = false;
            }

        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" EnablePartialRendering="true" AsyncPostBackTimeout="9000">
    </cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Loading" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Area_Trabajo" style="background-color: #ffffff; width: 100%; height: 100%;">
                <center>
                    <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                        <tr align="center">
                            <td class="label_titulo" colspan="2">
                                Tarjetas de Gasolina
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div id="Div_Contenedor_Msj_Error" style="width: 98%;" runat="server" visible="false">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td colspan="2" align="left">
                                                <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                    Width="24px" Height="24px" />
                                                <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 10%;">
                                            </td>
                                            <td style="width: 90%; text-align: left;" valign="top">
                                                <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left" style="width: 50%;">
                                <asp:ImageButton ID="Btn_Nuevo_Tarjeta_Gasolina" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png"
                                    Width="24px" CssClass="Img_Button" AlternateText="Nuevo" ToolTip="Nueva Tarjeta de Gasolina"
                                    OnClick="Btn_Nuevo_Click" />
                                <asp:ImageButton ID="Btn_Modificar_Tarjeta_Gasolina" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                    Width="24px" CssClass="Img_Button" AlternateText="Modificar" ToolTip="Modificar Tarjeta"
                                    OnClick="Btn_Modificar_Click" />
                                <asp:ImageButton ID="Btn_Eliminar_Tarjeta_Gasolina" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png"
                                    Width="24px" CssClass="Img_Button" AlternateText="Cancelar" ToolTip="Cancelar Tarjeta"
                                    OnClientClick="return confirm('�Esta seguro que desea CANCELAR la Tarjeta?');"
                                    OnClick="Btn_Eliminar_Click" />
                                <asp:ImageButton ID="Btn_Salir_Tarjeta_Gasolina" runat="server" ToolTip="Salir" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                    Width="24px" CssClass="Img_Button" AlternateText="Salir" OnClick="Btn_Salir_Click" />
                            </td>
                            <td style="width: 50%;">
                                <asp:Panel ID="Pnl_Busqueda" runat="server" DefaultButton="Btn_Busqueda_Directa_Tarjeta">
                                    B&uacute;squeda
                                    <asp:TextBox ID="Txt_Busqueda" runat="server" Width="200px"></asp:TextBox>
                                    <asp:ImageButton ID="Btn_Busqueda_Directa_Tarjeta" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                        AlternateText="Buscar" OnClick="Btn_Busqueda_Directa_Tarjeta_Click" ToolTip="Buscar" />
                                    <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda" runat="server" WatermarkText="<No. Tarjeta>"
                                        TargetControlID="Txt_Busqueda" WatermarkCssClass="watermarked">
                                    </cc1:TextBoxWatermarkExtender>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </center>
            </div>
            <div id="Div_Campos" runat="server" style="width: 100%;">
                <table width="99%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td colspan="2">
                            <asp:HiddenField ID="Hdf_Tarjeta_ID" runat="server" />
                            <asp:HiddenField ID="Hdf_Empleado_ID" runat="server" />
                            <asp:HiddenField ID="Hdf_Vehiculo_ID" runat="server" />
                            <asp:HiddenField ID="Hdf_Partida_ID" runat="server" />
                            <asp:HiddenField ID="Hdf_UR_ID" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            &nbsp;&nbsp;
                            <asp:Label ID="Lbl_Numero_Tarjeta" runat="server" Text="N�mero de Tarjeta"></asp:Label>
                        </td>
                        <td style="width: 33%;" valign="middle">
                            <asp:TextBox ID="Txt_Nuemero_Tarjeta" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nuemero_Tarjeta" runat="server" TargetControlID="Txt_Nuemero_Tarjeta"
                                FilterType="Numbers">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                        <td style="width: 14%;">
                            Tipo de Bien
                        </td>
                        <td style="width: 36%;" align="left">
                            <asp:DropDownList ID="Cmb_Tipo_Bien" Width="90%" runat="server" onchange="javascript:Tipo_Bien();">
                                <asp:ListItem Value="0">&lt;-- SELECCIONE --&gt;</asp:ListItem>
                                <asp:ListItem Value="BIEN_MUEBLE">BIEN MUEBLE</asp:ListItem>
                                <asp:ListItem Value="VEHICULO">VEHICULO</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Asignada_A" runat="server" Text="Asignada a" Visible="false"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Asignada_A" runat="server" Width="95%" Enabled="false" Visible="false"></asp:TextBox>
                            <asp:ImageButton ID="Btn_Busqueda_Avanzada_Asignada_A" runat="server" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png"
                                Width="24px" AlternateText="Busqueda Avanzada" ToolTip="Busqueda Avanzada" OnClick="Btn_Busqueda_Avanzada_Asignada_A_Click"
                                Visible="false" />
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="Pnl_Vehiculo_Seleccionado" runat="server" Width="99%" GroupingText="Bien para la Tarjeta"
                    DefaultButton="Btn_Busqueda_Directa_Vehiculo">
                    <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                            <td style="width: 15%;">
                                <asp:Label ID="Lbl_No_Inventario" runat="server" Text="No. Inventario"></asp:Label>
                            </td>
                            <td style="width: 35%;">
                                <asp:TextBox ID="Txt_No_Inventario" runat="server" MaxLength="7" Width="98%"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Inventario" runat="server" FilterType="Numbers"
                                    TargetControlID="Txt_No_Inventario">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td>
                                <asp:Label ID="Lbl_No_Economico" runat="server" Text="No. Economico"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="Txt_No_Economico" runat="server" Enabled="true" Width="70%"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="Tbw_Economico" runat="server" TargetControlID="Txt_No_Economico"
                                    WatermarkCssClass="watermarked" WatermarkText="&lt;U00000&gt;" />
                                <asp:ImageButton ID="Btn_Busqueda_Directa_Vehiculo" runat="server" AlternateText="Buscar"
                                    ImageUrl="~/paginas/imagenes/paginas/busqueda.png" OnClick="Btn_Busqueda_Directa_Vehiculo_Click"
                                    ToolTip="Buscar" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text="U. Responsable"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="Txt_Unidad_Responsable" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;">
                                <asp:Label ID="Lbl_Programa_Proyecto" runat="server" Text="Programa Proyecto"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="Cmb_Programa_Proyecto" runat="server" Width="100%" Enabled="false">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Lbl_Datos_Vehiculo" runat="server" Text="Datos Generales"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="Txt_Datos_Vehiculo" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;">
                                <asp:Label ID="Lbl_Placas" runat="server" Text="Placas/Serie"></asp:Label>
                            </td>
                            <td style="width: 35%;">
                                <asp:TextBox ID="Txt_Placas" runat="server" Width="70%" Enabled="false"></asp:TextBox>
                            </td>
                            <td style="width: 15%;">
                                &nbsp;&nbsp;
                                <asp:Label ID="Lbl_Anio" runat="server" Text="A�o"></asp:Label>
                            </td>
                            <td style="width: 35%;">
                                <asp:TextBox ID="Txt_Anio" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <table width="99%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Saldo_Incial" runat="server" Text="Saldo Inicial"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="Txt_Saldo_inicial" runat="server" Width="68%" Enabled="false"></asp:TextBox>
                        </td>
                        <td style="width: 14%;">
                            <asp:Label ID="Lbl_Estatus" runat="server" Text="Estatus"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="93%">
                                <asp:ListItem Value="">&lt; - - SELECCIONE - - &gt;</asp:ListItem>
                                <asp:ListItem Value="VIGENTE">VIGENTE</asp:ListItem>
                                <asp:ListItem Value="BAJA">BAJA</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Partida" runat="server" Text="Partida"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Partida" runat="server" Width="68%" Enabled="false"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Partida" runat="server" TargetControlID="Txt_Partida"
                                FilterType="Numbers">
                            </cc1:FilteredTextBoxExtender>
                            <asp:ImageButton ID="Btn_Busqueda_Directa_Partida" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                AlternateText="Buscar" ToolTip="Buscar" OnClick="Btn_Busqueda_Directa_Partida_Click" />
                        </td>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="Txt_Nombre_Partida" runat="server" Width="92%" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="Pnl_Resguardantes" runat="server" Width="99%" GroupingText="Resguardantes"
                    Visible="false">
                    <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                            <td>
                                <asp:GridView ID="Grid_Resguardante" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                    CssClass="GridView_Nested" EmptyDataText="No hay Resguardantes para esta Tarjeta."
                                    GridLines="None" PageSize="10" Width="99%">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:BoundField DataField="NO_EMPLEADO" HeaderText="No. Empleado" SortExpression="NO_EMPLEADO">
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="10%" />
                                            <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" Width="10%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="RESGUARDANTE" HeaderText="Resguardante" SortExpression="RESGUARDANTE">
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="55%" />
                                            <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" Width="55%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="TIPO_UNIDAD" HeaderText="Tipo unidad" NullDisplayText="-"
                                            SortExpression="TIPO_UNIDAD">
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="15%" />
                                            <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" Width="15%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NUMERO_INVENTARIO" HeaderText="Inventario" NullDisplayText="-"
                                            SortExpression="NUMERO_INVENTARIO">
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="10%" />
                                            <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" Width="10%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NUMERO_ECONOMICO" HeaderText="Econ�mico" NullDisplayText="-"
                                            SortExpression="NUMERO_ECONOMICO">
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="10%" />
                                            <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" Width="10%" />
                                        </asp:BoundField>
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <table width="99%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr style="background-color: #C0C0C0; border-color:Black;">
                        <td style="color: Black; text-align: right;">
                            Exportar Listado Completo de Tarjetas &nbsp;&nbsp;
                            <asp:ImageButton ID="Btn_Reporte_Listado" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png"
                                Width="16px" Height="16px" CssClass="Img_Button" AlternateText="Reporte del Listado" ToolTip="Reporte del Listado"
                                OnClick="Btn_Reporte_Listado_Click"  />
                            <asp:ImageButton ID="Btn_Exportar_Excel" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png"
                                Width="16px" Height="16px" CssClass="Img_Button" AlternateText="Exportar Listado a Excel" ToolTip="Exportar Listado a Excel"
                                OnClick="Btn_Exportar_Excel_Click" />
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="Grid_Busqueda_Empleados_Resguardo" runat="server" AllowPaging="True"
                    AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No se encontrar�n resultados para los filtros de la busqueda"
                    ForeColor="#333333" GridLines="None" OnPageIndexChanging="Grid_Busqueda_Empleados_Resguardo_PageIndexChanging"
                    OnSelectedIndexChanged="Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged"
                    PageSize="100" Width="100%">
                    <RowStyle CssClass="GridItem" />
                    <Columns>
                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="30px" />
                            <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:ButtonField>
                        <asp:BoundField DataField="EMPLEADO_ID" HeaderText="EMPLEADO_ID" SortExpression="EMPLEADO_ID">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="3px" />
                            <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" Width="3px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NO_EMPLEADO" HeaderText="No. Empleado" SortExpression="NO_EMPLEADO">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="70px" />
                            <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" Width="70px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NOMBRE" HeaderText="Nombre" NullDisplayText="-" SortExpression="NOMBRE">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                            <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" NullDisplayText="-"
                            SortExpression="DEPENDENCIA">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                            <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                    <PagerStyle CssClass="GridHeader" />
                    <SelectedRowStyle CssClass="GridSelected" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAltItem" />
                </asp:GridView>
                <asp:GridView ID="Grid_Listado" runat="server" CssClass="GridView_1" AutoGenerateColumns="False"
                    AllowPaging="True" PageSize="10" Width="99%" GridLines="None" EmptyDataText="No hay Tarjetas registradas."
                    OnPageIndexChanging="Grid_Listado_PageIndexChanging" OnSelectedIndexChanged="Grid_Listado_SelectedIndexChanged">
                    <RowStyle CssClass="GridItem" />
                    <Columns>
                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                            <ItemStyle Width="5%" />
                        </asp:ButtonField>
                        <asp:BoundField DataField="TARJETA_ID" HeaderText="ID Tarjeta" SortExpression="TARJETA_ID">
                            <ItemStyle Width="1%" Font-Size="X-Small" HorizontalAlign="Center" />
                            <HeaderStyle Width="1%" Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Number_TARJETA" HeaderText="No. Tarjeta" SortExpression="NUMERO_TARJETA">
                            <ItemStyle Width="10%" Font-Size="X-Small" HorizontalAlign="Center" />
                            <HeaderStyle Width="10%" Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NO_INVENTARIO" HeaderText="No. Inventario" SortExpression="NO_INVENTARIO"
                            NullDisplayText="-">
                            <ItemStyle Width="10%" Font-Size="X-Small" HorizontalAlign="Center" />
                            <HeaderStyle Width="10%" Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NO_ECONOMICO" HeaderText="No. Econ�mico" SortExpression="NO_ECONOMICO"
                            NullDisplayText="-">
                            <ItemStyle Width="10%" Font-Size="X-Small" HorizontalAlign="Center" />
                            <HeaderStyle Width="10%" Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" SortExpression="DEPENDENCIA"
                            NullDisplayText="-">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                            <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS"
                            NullDisplayText="-">
                            <ItemStyle Width="10%" Font-Size="X-Small" HorizontalAlign="Center" />
                            <HeaderStyle Width="10%" Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                    <PagerStyle CssClass="GridHeader" />
                    <SelectedRowStyle CssClass="GridSelected" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAltItem" />
                </asp:GridView>
            </div>
            </center> </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Btn_Exportar_Excel" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="Aux_MPE_Listado_Empleados" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_MPE_Listado_Empleados" runat="server" Text="Button" Style="display: none;" />
            <cc1:ModalPopupExtender ID="MPE_Listado_Empleados" runat="server" TargetControlID="Btn_Comodin_MPE_Listado_Empleados"
                PopupControlID="Pnl_Busqueda_Contenedor" PopupDragHandleControlID="Pnl_Busqueda_Resguardante_Cabecera"
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter">
            </cc1:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Pnl_Busqueda_Contenedor" runat="server" CssClass="drag" HorizontalAlign="Center"
        Width="850px" Style="display: none; border-style: outset; border-color: Silver;
        background-image: url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG); background-repeat: repeat-y;">
        <asp:Panel ID="Pnl_Busqueda_Resguardante_Cabecera" runat="server" Style="cursor: move;
            background-color: Silver; color: Black; font-size: 12; font-weight: bold; border-style: outset;">
            <table width="99%">
                <tr>
                    <td style="color: Black; font-size: 12; font-weight: bold;">
                        <asp:Image ID="Img_Informatcion_Autorizacion" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                        B&uacute;squeda: Empleados
                    </td>
                    <td align="right" style="width: 10%;">
                        <asp:ImageButton ID="Btn_Cerrar_Ventana" CausesValidation="false" runat="server"
                            Style="cursor: pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <div style="color: #5D7B9D">
            <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;">
                        <asp:UpdatePanel ID="Upnl_Filtros_Busqueda_Prestamos" runat="server">
                            <ContentTemplate>
                                <asp:UpdateProgress ID="Progress_Upnl_Filtros_Busqueda_Prestamos" runat="server"
                                    AssociatedUpdatePanelID="Upnl_Filtros_Busqueda_Prestamos" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                        </div>
                                        <div style="background-color: Transparent; position: fixed; top: 50%; left: 47%;
                                            padding: 10px; z-index: 1002;" id="div_progress">
                                            <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <table width="100%">
                                    <tr>
                                        <td style="width: 100%" colspan="4" align="right">
                                            <asp:ImageButton ID="Btn_Limpiar_Ctlr_Busqueda" runat="server" OnClientClick="javascript:return Limpiar_Ctlr_Busqueda_Resguardante();"
                                                ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de Busqueda" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%" colspan="4">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: left; font-size: 11px;">
                                            No Empleado
                                        </td>
                                        <td style="width: 30%; text-align: left; font-size: 11px;">
                                            <asp:TextBox ID="Txt_Busqueda_No_Empleado" runat="server" Width="98%" />
                                            <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_No_Empleado" runat="server" FilterType="Numbers"
                                                TargetControlID="Txt_Busqueda_No_Empleado" />
                                            <cc1:TextBoxWatermarkExtender ID="Twm_Txt_Busqueda_No_Empleado" runat="server" TargetControlID="Txt_Busqueda_No_Empleado"
                                                WatermarkText="Busqueda por No Empleado" WatermarkCssClass="watermarked" />
                                        </td>
                                        <td style="width: 20%; text-align: left; font-size: 11px;">
                                            RFC
                                        </td>
                                        <td style="width: 30%; text-align: left; font-size: 11px;">
                                            <asp:TextBox ID="Txt_Busqueda_RFC" runat="server" Width="98%" />
                                            <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_RFC" runat="server" FilterType="Numbers, UppercaseLetters"
                                                TargetControlID="Txt_Busqueda_RFC" />
                                            <cc1:TextBoxWatermarkExtender ID="Twm_Txt_Busqueda_RFC" runat="server" TargetControlID="Txt_Busqueda_RFC"
                                                WatermarkText="Busqueda por RFC" WatermarkCssClass="watermarked" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: left; font-size: 11px;">
                                            Nombre
                                        </td>
                                        <td style="width: 30%; text-align: left;" colspan="3">
                                            <asp:TextBox ID="Txt_Busqueda_Nombre_Empleado" runat="server" Width="99.5%" />
                                            <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_Nombre_Empleado" runat="server"
                                                FilterType="Custom, LowercaseLetters, Numbers, UppercaseLetters" TargetControlID="Txt_Busqueda_Nombre_Empleado"
                                                ValidChars="���������� ��" />
                                            <cc1:TextBoxWatermarkExtender ID="Twm_Nombre_Empleado" runat="server" TargetControlID="Txt_Busqueda_Nombre_Empleado"
                                                WatermarkText="Busqueda por Nombre" WatermarkCssClass="watermarked" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: left; font-size: 11px;">
                                            Unidad Responsable
                                        </td>
                                        <td style="width: 30%; text-align: left; font-size: 11px;" colspan="3">
                                            <asp:DropDownList ID="Cmb_Busqueda_Dependencia" runat="server" Width="100%" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%" colspan="4">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%; text-align: left;" colspan="4">
                                            <center>
                                                <asp:Button ID="Btn_Busqueda_Empleados" runat="server" Text="Busqueda de Empleados"
                                                    CssClass="button" CausesValidation="false" OnClick="Btn_Busqueda_Empleados_Click"
                                                    Width="200px" />
                                            </center>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <div id="Div_Resultados_Busqueda_Resguardantes" runat="server" style="border-style: outset;
                                    width: 99%; height: 250px; overflow: auto;">
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>
