<%@ Page Title="Inventario Stock" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Cat_Tal_Inventario_Stock.aspx.cs" Inherits="paginas_Taller_Mecanico_Frm_Cat_Tal_Inventario_Stock" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <asp:ScriptManager ID="ScptM_Catalogo" runat="server" />
  
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>         
            <asp:UpdateProgress ID="Uprg_Loading" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>                     
            </asp:UpdateProgress>
            <div id="Div_Area_Trabajo" style="background-color:#ffffff; width:100%; height:100%;">
                <center>
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo" colspan="2">Inventario Stock</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                          <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;">              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top">
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                            </table>                   
                          </div>                
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>                        
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" style="width:50%;">
                            <asp:ImageButton ID="Btn_Reporte_Execel" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png" Width="24px" 
                                CssClass="Img_Button" AlternateText="Imprimir" 
                                ToolTip="Imprimir Reprte en Execel" onclick="Btn_Reporte_Execel_Click" />
                            <asp:ImageButton ID="Btn_Reporte_PDF" runat="server"
                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" Width="24px" 
                                CssClass="Img_Button" AlternateText="Salir"
                                ToolTip="Imprimir Reprte en PDF" onclick="Btn_Reporte_PDF_Click" />
                        </td>
                        <td align="right" style="width:50%;">
                           <asp:ImageButton ID="Btn_Limpiar" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png" Width="24px" 
                                CssClass="Img_Button" AlternateText="Limpiar"
                                ToolTip="Limpiar Campos" onclick="Btn_Limpiar_Click" />                           
                        </td>                        
                    </tr>
                </table>
                </center>
            </div>
       
            <div id="Div_Campos" runat="server" style="width:100%;">                
                    <table width="99%"  border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                            <td colspan="3">
                                <asp:HiddenField ID="Hdf_Clave" runat="server" />
                                <asp:HiddenField ID="Hdf_Nombre" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" >
                                <asp:Label ID="Lbl_Clave" runat="server" Text="Clave"></asp:Label>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:TextBox ID="Txt_Clave" runat="server" Width="10%" ></asp:TextBox>
                                <asp:ImageButton ID="Btn_Busqueda" runat="server" 
                                    ImageUrl="~/paginas/imagenes/paginas/busqueda.png" AlternateText="Buscar" 
                                    ToolTip="Buscar" onclick="Btn_Busqueda_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <hr />
                            </td>
                        </tr>
                        <tr align ="center">
                            <td style="width:33%;">
                                <asp:Label ID="Lbl_Existencia" runat="server" Text="Existencia"></asp:Label>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:TextBox ID="Txt_Existencia" runat="server" Width="15%" Enabled="false" ></asp:TextBox>
                            </td>
                            <td style="width:33%;">
                                <asp:Label ID="Lbl_Comprometido" runat="server" Text="Comprometido"></asp:Label>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:TextBox ID="Txt_Comprometido" runat="server" Width="15%" Enabled="false" ></asp:TextBox>
                            </td>
                            <td style="width:33%;">
                                <asp:Label ID="Lbl_Disponible" runat="server" Text="Disponible"></asp:Label>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:TextBox ID="Txt_Disponible" runat="server" Width="15%" Enabled="false" ></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <table width="99%"  border="0" cellspacing="0" class="estilo_fuente">
                        <tr style="height: 20px;">
                            <td colspan ="2">
                                &nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr valign="top">
                            <td style="width: 50%;">
                                <asp:Panel ID="Pnl_Salidas_Express" runat="server" GroupingText="Salidas Express">
                                    <asp:GridView ID="Grid_Salidas_Express" runat="server" CssClass="GridView_1"
                                        AutoGenerateColumns="False" AllowPaging="True" PageSize="10" Width="99%"
                                        GridLines= "None" EmptyDataText="No hay Salidas Express." FooterStyle-HorizontalAlign="Center"
                                        onrowdatabound="Grid_Salidas_Express_RowDataBound" ShowFooter="True" >
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:BoundField DataField="NO_DETALLE" HeaderText="No Mov" SortExpression="NO_DETALLE" NullDisplayText="-" >
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FECHA" HeaderText="Fecha" SortExpression="FECHA" NullDisplayText="-" DataFormatString="{0:dd/MMM/yyyy hh:mm:ss tt}" >
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CANTIDAD" HeaderText="Cantidad" SortExpression="CANTIDAD" NullDisplayText="-" >
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />                                
                                        <AlternatingRowStyle CssClass="GridAltItem" />       
                                    </asp:GridView>  
                                </asp:Panel>                          
                            </td>
                            <td style="width: 50%;">
                                <asp:Panel ID="Pnl_Salidas_Almacen" runat="server" GroupingText="Salidas Almacen">
                                    <asp:GridView ID="Grid_Salidas_Almacen" runat="server" CssClass="GridView_1"
                                    AutoGenerateColumns="False" AllowPaging="True" PageSize="10" Width="99%"
                                    GridLines= "None" EmptyDataText="No hay Salidas de Almacen." FooterStyle-HorizontalAlign="Center"
                                    onrowdatabound="Grid_Salidas_Almacen_RowDataBound" ShowFooter="True" >
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:BoundField DataField="NO_SALIDA" HeaderText="No Salida" SortExpression="NO_SALIDA" NullDisplayText="-" >
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FECHA" HeaderText="Fecha" SortExpression="FECHA" NullDisplayText="-" DataFormatString="{0:dd/MMM/yyyy hh:mm:ss tt}" >
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CANTIDAD" HeaderText="Cantidad" SortExpression="CANTIDAD" NullDisplayText="-" >
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />                                
                                    <AlternatingRowStyle CssClass="GridAltItem" />       
                                </asp:GridView>
                                </asp:Panel>                          
                            </td>
                        </tr>
                        <tr style="height: 40px;">
                            <td colspan ="2">
                                &nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr valign="top">
                            <td style="width: 50%;">
                                <asp:Panel ID="Pnl_Entradas" runat="server" GroupingText="Entradas" Visible="false">
                                    <asp:GridView ID="Grid_Entradas" runat="server" CssClass="GridView_1" FooterStyle-HorizontalAlign="Center"
                                        AutoGenerateColumns="False" AllowPaging="True" PageSize="10" Width="99%"
                                        GridLines= "None" EmptyDataText="No hay Entradas." ShowFooter="True" >
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:BoundField DataField="NO_DETALLE" HeaderText="No Mov" SortExpression="NO_DETALLE" NullDisplayText="-" >
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FECHA" HeaderText="Fecha" SortExpression="FECHA" NullDisplayText="-" DataFormatString="{0:dd/MMM/yyyy hh:mm:ss tt}" >
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CANTIDAD" HeaderText="Cantidad" SortExpression="CANTIDAD" NullDisplayText="-" >
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                        <FooterStyle HorizontalAlign="Center" />
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />                                
                                        <AlternatingRowStyle CssClass="GridAltItem" />       
                                    </asp:GridView>  
                                </asp:Panel>                          
                            </td>
                            <td style="width: 50%;">
                                <asp:Panel ID="Pnl_Salidas" runat="server" GroupingText="Salidas" Visible="false">
                                    <asp:GridView ID="Grid_Salidas" runat="server" CssClass="GridView_1" FooterStyle-HorizontalAlign="Center"
                                    AutoGenerateColumns="False" AllowPaging="True" PageSize="10" Width="99%"
                                    GridLines= "None" EmptyDataText="No hay Salidas." ShowFooter="True" >
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:BoundField DataField="NO_SALIDA" HeaderText="No Salida" SortExpression="NO_SALIDA" NullDisplayText="-" >
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FECHA" HeaderText="Fecha" SortExpression="FECHA" NullDisplayText="-" DataFormatString="{0:dd/MMM/yyyy hh:mm:ss tt}" >
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CANTIDAD" HeaderText="Cantidad" SortExpression="CANTIDAD" NullDisplayText="-" >
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                    </Columns>
                                        <FooterStyle HorizontalAlign="Center" />
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />                                
                                    <AlternatingRowStyle CssClass="GridAltItem" />       
                                </asp:GridView>
                                </asp:Panel>                          
                            </td>
                        </tr>
                    </table>
            </div>
            
        </ContentTemplate>  
        <Triggers>
            <asp:PostBackTrigger ControlID="Btn_Reporte_Execel" />
        </Triggers>           
    </asp:UpdatePanel>
</asp:Content>

