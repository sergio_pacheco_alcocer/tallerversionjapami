﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Tal_Contrarecibos.aspx.cs" Inherits="paginas_Almacen_Frm_Ope_Tal_Contrarecibos" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
   <!--SCRIPT PARA LA VALIDACION QUE NO EXPiRE LA SESSION-->  
   <script language="javascript" type="text/javascript">
    <!--
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_Session.ashx";
        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        function Abrir_Carga_PopUp() 
        {
            $find('Archivos_Requisicion').show();
            return false;
        }

        //Temporizador para matener la sesión activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);        
    //-->
   </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true">
    </cc1:ToolkitScriptManager>
    <div id="Div_General" style="width: 98%; height:900px;" visible="true" runat="server">
        <asp:UpdatePanel ID="Upl_Contenedor" runat="server">
            <ContentTemplate>
                <%--<asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upl_Contenedor"
                    DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                        </div>
                        <div class="processMessage" id="div_progress">
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>--%>
                <!--Div del encabezado-->
                <div id="Div_Encabezado" runat="server">
                    <table style="width: 100%;" border="0" cellspacing="0">
                        <tr align="center">
                            <td class="label_titulo">Elaborar Contra Recibo</td>
                        </tr>
                        <tr align="left">
                            <td align="left">
                                <div id="Div_Contenedor_Msj_Error" runat="server">
                                    <asp:Image ID="Img_Warning" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" />
                                    <asp:Label ID="Lbl_Informacion" runat="server" ForeColor="#990000"></asp:Label>
                                </div>
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left" valign="middle">
                                <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                    CssClass="Img_Button" AlternateText="Modificar" ToolTip="Modificar" 
                                    onclick="Btn_Modificar_Click" />
                                <asp:ImageButton ID="Btn_Imprimir_Contrarecibo" runat="server" 
                                    CssClass="Img_Button" ImageUrl="~/paginas/imagenes/gridview/grid_print.png"
                                    ToolTip="Imprimir Contrarecibo" />
                                <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                    ToolTip="Inicio" onclick="Btn_Salir_Click" />
                                <cc1:ModalPopupExtender ID="MPE_Archivos_Requisicion" runat="server" BackgroundCssClass="popUpStyle" BehaviorID="Archivos_Requisicion" PopupControlID="Pnl_Seleccionar_Archivo" TargetControlID="Btn_Open" CancelControlID="Btn_Cerrar" DropShadow="true" DynamicServicePath="" Enabled="true"></cc1:ModalPopupExtender>
                                <asp:Button ID="Btn_Open" runat="server" Text="" style="background-color: transparent; border-style:none; visibility:hidden" />
                                <asp:Button Style="background-color: transparent; border-style:none; visibility:hidden" ID="Btn_Cerrar" runat="server" Text="" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                               <div id="Div_Busquedas_Avanzadas" runat="server" visible="true">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
                                        <tr>
                                            <td align="left">N. C.</td>
                                            <td align="left">
                                                <asp:TextBox ID="Txt_No_Contra_Recibo_Busqueda" runat="server" Width="150px"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="Txt_No_Contra_Recibo_Busqueda_TWE" runat="server" TargetControlID="Txt_No_Contra_Recibo_Busqueda" WatermarkCssClass="watermarked" WatermarkText="&lt;NC-&gt;"></cc1:TextBoxWatermarkExtender>
                                                <cc1:FilteredTextBoxExtender ID="Txt_No_Contra_Recibo_Busqueda_FTE" runat="server" TargetControlID="Txt_No_Contra_Recibo_Busqueda" FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9" Enabled="true" InvalidChars="<,>,&,',!"></cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td align="left">O. C.</td>
                                            <td align="left">
                                                <asp:TextBox ID="Txt_No_Orden_Compra_Busqueda" runat="server" Width="150px"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="Txt_No_Orden_Compra_Busqueda_TWE" runat="server" TargetControlID="Txt_No_Orden_Compra_Busqueda" WatermarkCssClass="watermarked" WatermarkText="&lt;OC-&gt;"></cc1:TextBoxWatermarkExtender>
                                                <cc1:FilteredTextBoxExtender ID="Txt_No_Orden_Compra_Busqueda_FTE" runat="server" TargetControlID="Txt_No_Orden_Compra_Busqueda" FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9" Enabled="true" InvalidChars="<,>,&,',!"></cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td>&nbsp;</td>
                                            <td align="left"><asp:ImageButton ID="Img_Btn_Buscar" runat="server" 
                                                    ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                                    onclick="Img_Btn_Buscar_Click" /></td>
                                        </tr>
                                        <tr>
                                            <td align="left">Proveedor</td>
                                            <td align="left"><asp:TextBox ID="Txt_Busqueda_Proveedor" runat="server"></asp:TextBox></td>
                                            <td>&nbsp;</td>
                                            <td colspan="3" align="left"><asp:DropDownList ID="Cmb_Proveedores" runat="server" Width="100%"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td align="left">De</td>                                    
                                            <td align="left">
                                                <asp:TextBox ID="Txt_Fecha_Inicio" runat="server" Enabled="false" Width="150px"></asp:TextBox>
                                                <asp:ImageButton ID="Img_Btn_Fecha_Inicio" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la fecha inicial" />
                                                <cc1:CalendarExtender ID="Txt_Fecha_Inicio_CE" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Img_Btn_Fecha_Inicio" TargetControlID="Txt_Fecha_Inicio"></cc1:CalendarExtender>
                                            </td>
                                            <td align="left">Al</td>
                                            <td align="left">
                                                <asp:TextBox ID="Txt_Fecha_Fin" runat="server" Enabled="false" Width="150px"></asp:TextBox>
                                                <asp:ImageButton ID="Img_Btn_Fecha_Fin" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la fecha final" />
                                                <cc1:CalendarExtender ID="Txt_Fecha_Fin_CE" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Img_Btn_Fecha_Fin" TargetControlID="Txt_Fecha_Fin"></cc1:CalendarExtender>                                        
                                            </td>
                                            <td align="left">Estatus</td>
                                            <td align="right">
                                                <asp:DropDownList ID="Cmb_Estatus_Busqueda" runat="server" Width="150px">
                                                    <asp:ListItem Text="Seleccione" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="Generada" Value="GENERADA"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="6">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="6" align="left">
                                                <asp:GridView ID="Grid_Contrarecibos" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                    CssClass="GridView_1" GridLines="None" Width="100%" HeaderStyle-CssClass="tblHead" 
                                                    Font-Size="X-Small" EmptyDataText="Lista de Contrarecibos" 
                                                    onselectedindexchanged="Grid_Contrarecibos_SelectedIndexChanged">
                                                    <RowStyle CssClass="GridItem" />
                                                    <Columns>
                                                        <asp:ButtonField HeaderText="" ButtonType="Image" ImageUrl="~/paginas/imagenes/gridview/blue_button.png" ItemStyle-HorizontalAlign="Center" CommandName="Select" />
                                                        <asp:BoundField DataField="OC" HeaderText="O. C." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="NO_CONTRA_RECIBO" HeaderText="C. R." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="FECHA_RECEPCION" HeaderText="Fecha<br />Recepci&oacute;n" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:/dd/MMM/yyyy}" HtmlEncode="false" />
                                                        <asp:BoundField DataField="FECHA_PAGO" HeaderText="Fecha Pago" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:/dd/MMM/yyyy}" />
                                                        <asp:BoundField DataField="PROVEEDOR" HeaderText="Proveedor" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField DataField="IMPORTE_TOTAL" HeaderText="Importe" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField DataField="NO_ORDEN_COMPRA" HeaderText="NO_ORDEN_COMPRA" />
                                                    </Columns>
                                                    <PagerStyle CssClass="GridHeader" />
                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                    <HeaderStyle CssClass="GridHeader" />
                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                </asp:GridView>                                            
                                            </td>
                                        </tr>
                                    </table>
                                </div>                            
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <div id="Div_Detalles" runat="server">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td align="left">No Contrarecibo</td>
                                            <td align="left"><asp:TextBox ID="Txt_No_Contrarecibo" runat="server" Width="150px" Enabled="false"></asp:TextBox></td>
                                            <td align="left">No Orden Compra</td>
                                            <td align="left"><asp:TextBox ID="Txt_No_Orden_Compra" runat="server" Width="150px" Enabled="false"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td align="left">Estatus</td>
                                            <td align="left">
                                                <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="150px">
                                                    <asp:ListItem Text="Seleccione" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="Generado" Value="GENERADO"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td colspan="2">
                                                <asp:HiddenField ID="Txt_No_Contrarecibo_Escondido" runat="server" />
                                                <asp:HiddenField ID="Txt_Proveedor_ID" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">Proveedor</td>
                                            <td align="left" colspan="3"><asp:TextBox ID="Txt_Proveedor" runat="server" Width="100%" Enabled="false"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td align="left">Fecha Recepci&oacute;n</td>
                                            <td align="left">
                                                <asp:TextBox ID="Txt_Fecha_Recepcion" runat="server" Width="115px" Enabled="false"></asp:TextBox>
                                                <cc1:calendarextender id="Txt_Fecha_Recepcion_CalendarExtender" runat="server" targetcontrolid="Txt_Fecha_Recepcion"
                                                    popupbuttonid="Btn_Fecha_Recepcion" format="dd/MMM/yyyy" />
                                                <asp:ImageButton ID="Btn_Fecha_Recepcion" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                            </td>
                                            <td align="left">Fecha Pago</td>
                                            <td align="left"><asp:TextBox ID="Txt_Fecha_Pago" runat="server" Width="115px" Enabled="false"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="left">Facturas</td>
                                            <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="left">No Factura</td>
                                            <td align="left">Fecha</td>
                                            <td align="left">Monto</td>
                                            <td align="left">Archivo XML</td>
                                        </tr>
                                        <tr>
                                            <td align="left"><asp:TextBox ID="Txt_No_Factura" runat="server" Width="150px"></asp:TextBox></td>
                                            <td align="left">
                                                <asp:TextBox ID="Txt_Fecha_Factura" runat="server" Width="150px" Enabled="false"></asp:TextBox>
                                                <cc1:CalendarExtender ID="Txt_Fecha_Factura_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Factura" PopupButtonID="Img_Btn_Fecha_Factura" Format="dd/MMM/yyyy"></cc1:CalendarExtender>
                                                <asp:ImageButton ID="Img_Btn_Fecha_Factura" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha de la Factura" />
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="Txt_Monto_Factura" runat="server" Width="150px"></asp:TextBox>
                                            </td>
                                            <td align="left">
                                                <asp:ImageButton ID="Img_Btn_Cargar" runat="server" CssClass="Img_Button" CausesValidation="false"
                                                    ImageUrl="~/paginas/imagenes/paginas/subir.png" ToolTip="Cargar Archivo" 
                                                    OnClientClick="javascript:return Abrir_Carga_PopUp();" 
                                                    onclick="Img_Btn_Cargar_Click" />
                                                <asp:ImageButton ID="Img_Btn_Agregar" runat="server" 
                                                    ImageUrl="~/paginas/imagenes/paginas/sias_add.png" 
                                                    ToolTip="Agregar Factura" onclick="Img_Btn_Agregar_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="4">
                                                <asp:GridView ID="Grid_Facturas" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                    CssClass="GridView_1" GridLines="None" Width="100%" 
                                                    HeaderStyle-CssClass="tblHead" Font-Size="X-Small" 
                                                    onrowdatabound="Grid_Facturas_RowDataBound">
                                                    <RowStyle CssClass="GridItem" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="Btn_Seleccionar_Archivo" runat="server" ImageUrl="~/paginas/imagenes/paginas/delete.png" CommandArgument='<%# Eval("Factura_ID") %>' />
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                            <ItemStyle HorizontalAlign="Center" Width="3%" />
                                                        </asp:TemplateField>                                                        
                                                        <asp:BoundField DataField="NO_FACTURA_PROVEEDOR" HeaderText="No Factura" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField DataField="FECHA_FACTURA" HeaderText="Fecha" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MMM/yyyy}" />
                                                        <asp:BoundField DataField="IMPORTE" HeaderText="Monto" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:c}" />
                                                        <asp:TemplateField HeaderText="Archivo XML" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Lbl_Archivo_XML" runat="server"></asp:Label>
                                                                <asp:HyperLink ID="Hyp_Lnk_Archivo" runat="server"></asp:HyperLink>
                                                                <asp:ImageButton ID="Img_Btn_Cargar_Archivo_Factura" runat="server" ImageUrl="~/paginas/imagenes/paginas/subir.png" ToolTip="Cargar Archivo" CommandArgument='<%# Eval("Factura_ID") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripci&oacute;n" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField DataField="ARCHIVO_XML" HeaderText="Factura XML" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="left" />
                                                        <asp:BoundField DataField="FACTURA_ID" HeaderText="Factura_ID" />
                                                        <asp:BoundField DataField="ESTATUS" HeaderText="ESTATUS" />
                                                    </Columns>
                                                    <PagerStyle CssClass="GridHeader" />
                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                    <HeaderStyle CssClass="GridHeader" />
                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                </asp:GridView>                                            
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>            
            </ContentTemplate>
        </asp:UpdatePanel>
        <!--Modal para la seleccion del archivo-->
        <asp:Panel ID="Pnl_Seleccionar_Archivo" runat="server" CssClass="drag" HorizontalAlign="Center" Width="650px" style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">
            <asp:Panel ID="Pnl_Cabecera_Seleccionar_Archivo" runat="server">
                <table width="100%">
                    <tr>
                        <td style="color:Black;font-size:12;font-weight:bold;"><asp:Image ID="Image1" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />Seleccionar el Archivo para la Requisic&oacute;n</td>
                    </tr>
                </table>
            </asp:Panel>
            <div style="color: #5D7B9D">
                <table width="100%">
                    <tr>
                        <td align="left" style="text-align:left;">
                            <asp:UpdatePanel ID="Upnl_Archivos_Requisicion" runat="server">
                                <ContentTemplate>
                                    <asp:UpdateProgress ID="Progress_Upnl_Archivos_Requisicion" runat="server" AssociatedUpdatePanelID="Upnl_Archivos_Requisicion" DisplayAfter="0">
                                        <ProgressTemplate>
                                            <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                            <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress"><img alt="" src="../Imagenes/paginas/Sias_Roler.gif" /></div>                                        
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <table width="100%">
                                        <tr>
                                            <td colspan="4" style="width:100%;"><hr /></td>
                                        </tr>
                                        <tr>
                                            <td style="width:20%;text-align:left;font-size:11px;">
                                                <cc1:AsyncFileUpload ID="AFil_Archivo" runat="server" ThrobberID="Lbl_Throbber" 
                                                    onuploadedcomplete="AFil_Archivo_UploadedComplete" />
                                                <asp:Label ID="Lbl_Throbber" runat="server" Text="Espere" Width="30px">
                                                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                                    <div  class="processMessage" id="div_progress">
                                                        <center>
                                                            <img alt="" src="../Imagenes/paginas/Sias_Roler.gif" />
                                                            <br /><br />
                                                            <span id="spanUploading" runat="server" style="color:White;font-size:30px;font-weight:bold;font-family:Lucida Calligraphy;font-style:italic;">
                                                                Cargando...
                                                            </span>
                                                        </center>
                                                    </div>                                                
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="4">Comentarios<br />                                           
                                                <asp:TextBox ID="Txt_Comentarios_Archivo" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
                                                <cc1:textboxwatermarkextender id="TextBoxWatermarkExtender3" runat="server" targetcontrolid="Txt_Comentarios_Archivo"
                                                    watermarktext="&lt;Límite de Caracteres 500&gt;" watermarkcssclass="watermarked">
                                                    </cc1:textboxwatermarkextender>
                                                <cc1:filteredtextboxextender id="FTE_Justificacion" runat="server" targetcontrolid="Txt_Comentarios_Archivo"
                                                    filtertype="Custom, UppercaseLetters, LowercaseLetters, Numbers" validchars="ÑñáéíóúÁÉÍÓÚ./*-!$%&()=,[]{}+<>@?¡?¿# ">
                                                    </cc1:filteredtextboxextender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="width:100%;text-align:left;">
                                                <center>
                                                    <asp:Button ID="Btn_Archivos_Requisicion" runat="server" Text="Aceptar" 
                                                        CssClass="button" CausesValidation="false" Width="200px" />
                                                </center>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger EventName="Click" ControlID="Btn_Archivos_Requisicion" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>
</asp:Content>

