﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Control_Patrimonial_Operacion_Vehiculos.Negocio;
using System.Text.RegularExpressions;
using JAPAMI.Reportes;
using JAPAMI.Taller_Mecanico.Catalogo_Partes_Vehiculos.Negocio;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using JAPAMI.Taller_Mecanico.Operacion_Unidades_Responsables_Rol.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Correctivos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Preventivos.Negocio;
using JAPAMI.Taller_Mecanico.Reporte_Solicitud_Servicio.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Asignacion_Proveedor.Negocio;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Unidades_Responsables_Rol.Datos;

public partial class paginas_Taller_Mecanico_Frm_Ope_Tal_Bitacora_Seguimiento_Servicio : System.Web.UI.Page {

    #region Page_Load
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN: Carga la Pagina Inicial
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 25/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e) {
            Div_Contenedor_Msj_Error.Visible = false;
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack) {
                Cmb_Filtrado_Estatus.SelectedIndex = Cmb_Filtrado_Estatus.Items.IndexOf(Cmb_Filtrado_Estatus.Items.FindByValue("EN_REPARACION"));
                Llenar_Combo_Unidades_Responsables();
                Grid_Listado_Solicitudes.PageIndex = 0;
                Configuracion_Formulario("INICIAL");
                Cmb_Unidad_Responsable.Enabled = false;
                Cmb_Unidad_Responsable_Busqueda.Enabled = Validar_Acceso_Unidades_Responsables();
                Cmb_Unidad_Responsable_Busqueda.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Cls_Sessiones.Dependencia_ID_Empleado));
                Llenar_Listado_Solicitudes();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Validar_Acceso_Unidades_Responsables
        ///DESCRIPCIÓN: Se valida el Acceso a Otras UR.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected Boolean Validar_Acceso_Unidades_Responsables() {
            Boolean Accesos_Totales = false;
            Cls_Ope_Tal_Unidades_Responsables_Rol_Negocio Cls_Negocio = new Cls_Ope_Tal_Unidades_Responsables_Rol_Negocio();
            Cls_Negocio.P_Estatus = "ACTIVO";
            DataTable Dt_UR = Cls_Negocio.Consultar_Listado_UR();
            DataRow[] Filas = Dt_UR.Select("DEPENDENCIA_ID = '" + Cls_Sessiones.Dependencia_ID_Empleado.Trim() + "'");
            if (Filas.Length > 0) { Accesos_Totales = true; }
            return Accesos_Totales;
        }

    #endregion

    #region Metodos

        #region Llenado de Campos [Combos, Listados, Vehiculos]

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Unidades_Responsables
            ///DESCRIPCIÓN: Se llena el Combo de las Unidades Responsables.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 25/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Combo_Unidades_Responsables() {
                Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                Cls_Ope_Tal_Unidades_Responsables_Rol_Negocio Ur_Negocio = new Cls_Ope_Tal_Unidades_Responsables_Rol_Negocio();
                DataTable Dt_Dependencias = new DataTable();
                Negocio.P_Estatus = "ACTIVO";
                Ur_Negocio.P_No_Empleado = Cls_Sessiones.No_Empleado;
                Dt_Dependencias = Ur_Negocio.Consultar_Listado_UR();
                //Dt_Dependencias = Negocio.Consultar_Unidades_Responsables();
                Cmb_Unidad_Responsable.DataSource = Dt_Dependencias;
                Cmb_Unidad_Responsable.DataTextField = "CLAVE_NOMBRE";
                Cmb_Unidad_Responsable.DataValueField = "DEPENDENCIA_ID";
                Cmb_Unidad_Responsable.DataBind();
                Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("< - - TODAS - - >", ""));
                Cmb_Unidad_Responsable_Busqueda.DataSource = Dt_Dependencias;
                Cmb_Unidad_Responsable_Busqueda.DataTextField = "CLAVE_NOMBRE";
                Cmb_Unidad_Responsable_Busqueda.DataValueField = "DEPENDENCIA_ID";
                Cmb_Unidad_Responsable_Busqueda.DataBind();
                Cmb_Unidad_Responsable_Busqueda.Items.Insert(0, new ListItem("< - - TODAS - - >", ""));
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Solicitudes
            ///DESCRIPCIÓN: Se llena el Listado de las Solicitudes.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 25/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Listado_Solicitudes() {
                Cls_Ope_Tal_Solicitud_Servicio_Negocio Negocio = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                if (Cmb_Unidad_Responsable_Busqueda.SelectedIndex > 0) { Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable_Busqueda.SelectedItem.Value.Trim(); }
                else
                {
                    Negocio.P_Dependencia_ID = Cls_Ope_Tal_Unidades_Responsables_Rol_Datos.Consultar_Accesos_UR(Cls_Sessiones.No_Empleado);
                }                
                if (Cmb_Filtrado_Estatus.SelectedIndex > 0) {  Negocio.P_Estatus = Cmb_Filtrado_Estatus.SelectedItem.Value; }
                if (Txt_Numero_Inventario_Busqueda.Text.Trim().Length > 0) { Negocio.P_No_Inventario = Convert.ToInt32(Txt_Numero_Inventario_Busqueda.Text.Trim()); }
                if (Txt_Numero_Economico_Busqueda.Text.Trim().Length > 0) { Negocio.P_No_Economico = Txt_Numero_Economico_Busqueda.Text.Trim(); }
                if (Txt_Folio_Solicitud_Busqueda.Text.Trim().Length > 0) { Negocio.P_Folio_Solicitud = String.Format("{0:0000000000}", Convert.ToInt32(Txt_Folio_Solicitud_Busqueda.Text.Trim())); }
                if (Cmb_Tipo_Solicitud_Busqueda.SelectedIndex > 0) { Negocio.P_Tipo_Servicio = Cmb_Tipo_Solicitud_Busqueda.SelectedItem.Value.Trim(); }
                if (Cmb_Reparacion_Busqueda.SelectedIndex > 0) { Negocio.P_Reparacion = Cmb_Reparacion_Busqueda.SelectedItem.Value.Trim(); }
                if (Txt_Fecha_Recepcion_Inicial.Text.Trim().Length > 0) { Negocio.P_Busq_Fecha_Recepcion_Inicial = Convert.ToDateTime(Txt_Fecha_Recepcion_Inicial.Text.Trim()); }
                if (Txt_Fecha_Recepcion_Final.Text.Trim().Length > 0) { Negocio.P_Busq_Fecha_Recepcion_Final = Convert.ToDateTime(Txt_Fecha_Recepcion_Final.Text.Trim()); }
                
                DataTable Dt_Resultados = Negocio.Consultar_Listado_Solicitudes_Servicio();
                Dt_Resultados.DefaultView.Sort = "FOLIO DESC";
                Grid_Listado_Solicitudes.Columns[1].Visible = true;
                Grid_Listado_Solicitudes.DataSource = Dt_Resultados;
                Grid_Listado_Solicitudes.DataBind();
                Grid_Listado_Solicitudes.Columns[1].Visible = false;
                Cambiar_Estatus_Grid();
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Seguimiento_Solicitud
            ///DESCRIPCIÓN: Se llena el Listado del seguimiento de las Solicitudes.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 25/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Grid_Seguimiento_Solicitud(DataTable Dt_Datos) {
                Grid_Seguimiento_Solicitud.Columns[0].Visible = true;
                Grid_Seguimiento_Solicitud.DataSource = Dt_Datos;
                Grid_Seguimiento_Solicitud.DataBind();
                Grid_Seguimiento_Solicitud.Columns[0].Visible = false;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Refacciones
            ///DESCRIPCIÓN: Se llena el Listado de las refacciones
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 25/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Grid_Refacciones(DataTable Dt_Datos) {
                Grid_Listado_Refacciones.DataSource = Dt_Datos;
                Grid_Listado_Refacciones.DataBind();
                if (Grid_Listado_Refacciones.Rows.Count > 0) Btn_Ver_Listado_Refacciones.Visible = true;
                else Btn_Ver_Listado_Refacciones.Visible = false;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Listado_Servicios_Adicionales
            ///DESCRIPCIÓN: Se llena el Listado de los Servicios Adicionales
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 06/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Grid_Listado_Servicios_Adicionales(DataTable Dt_Datos) {
                Grid_Listado_Servicios_Adicionales.DataSource = Dt_Datos;
                Grid_Listado_Servicios_Adicionales.DataBind();
                if (Grid_Listado_Servicios_Adicionales.Rows.Count > 0) Btn_Ver_Servicios_Adicionales.Visible = true;
                else Btn_Ver_Servicios_Adicionales.Visible = false;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Vehiculo
            ///DESCRIPCIÓN: Se cargan los Datos del Vehiculo Seleccionado.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 25/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Cargar_Datos_Vehiculo(String Vehiculo, String Tipo_Busqueda) {
                Limpiar_Formulario_Vehiculo();
                Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                switch (Tipo_Busqueda) { 
                    case "NO_INVENTARIO":
                        Consulta_Negocio.P_No_Inventario = Vehiculo;
                        break;
                    case "IDENTIFICADOR":
                        Consulta_Negocio.P_Vehiculo_ID = Vehiculo;
                        break;
                    default: break;
                }
                if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Consulta_Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value; }
                DataTable Dt_Vehiculo = Consulta_Negocio.Consultar_Vehiculos();
                if (Dt_Vehiculo.Rows.Count > 0) {
                    Hdf_Vehiculo_ID.Value = Dt_Vehiculo.Rows[0]["VEHICULO_ID"].ToString().Trim();
                    Txt_No_Inventario.Text = Dt_Vehiculo.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                    Txt_No_Economico.Text = Dt_Vehiculo.Rows[0]["NO_ECONOMICO"].ToString().Trim();
                    Txt_Datos_Vehiculo.Text = Dt_Vehiculo.Rows[0]["VEHICULO_DESCRIPCION"].ToString().Trim();
                    Txt_Placas.Text = Dt_Vehiculo.Rows[0]["PLACAS"].ToString().Trim();
                    Txt_Anio.Text = Dt_Vehiculo.Rows[0]["ANIO"].ToString().Trim();
                    Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString()));
                } else {
                    Lbl_Ecabezado_Mensaje.Text = "El Vehículo no se encontro.";
                    if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Lbl_Mensaje_Error.Text = "[Puede ser que no este asignado a la Unidad Responsable Seleccionada]."; }
                    else { Lbl_Mensaje_Error.Text = ""; }
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Bien_Mueble
            ///DESCRIPCIÓN: Se cargan los Datos del Bien Mueble Seleccionado.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Cargar_Datos_Bien_Mueble(String Bien_Mueble, String Tipo_Busqueda)
            {
                Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                switch (Tipo_Busqueda)
                {
                    case "NO_INVENTARIO":
                        Consulta_Negocio.P_No_Inventario = Bien_Mueble;
                        break;
                    case "IDENTIFICADOR":
                        Consulta_Negocio.P_Bien_Mueble_ID = Bien_Mueble;
                        break;
                    default: break;
                }
                DataTable Dt_Bienes_Muebles = Consulta_Negocio.Consultar_Bien_Mueble();
                if (Dt_Bienes_Muebles.Rows.Count > 0)
                {
                    Hdf_Bien_Mueble_ID.Value = Dt_Bienes_Muebles.Rows[0]["BIEN_MUEBLE_ID"].ToString().Trim();
                    Txt_No_Inventario_BM.Text = Dt_Bienes_Muebles.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                    Txt_Numero_Serie_Bien.Text = Dt_Bienes_Muebles.Rows[0]["NUMERO_SERIE"].ToString().Trim();
                    Txt_Descripcion_Bien.Text = Dt_Bienes_Muebles.Rows[0]["DESCRIPCION_BIEN_MUEBLE"].ToString().Trim();
                    if (!String.IsNullOrEmpty(Dt_Bienes_Muebles.Rows[0]["DEPENDENCIA_ID"].ToString()))
                    {
                        Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Bienes_Muebles.Rows[0]["DEPENDENCIA_ID"].ToString()));
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Text = "ERROR [Puede ser que el Bien esté dado de Baja].";
                        Cmb_Unidad_Responsable.SelectedIndex = 0;
                    }
                }
                else
                {
                    Lbl_Ecabezado_Mensaje.Text = "El Bien no se encontro.";
                    if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Lbl_Mensaje_Error.Text = "[Puede ser que no este asignado a la Unidad Responsable Seleccionada]."; }
                    else { Lbl_Mensaje_Error.Text = ""; }
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Cargar_Estilo_Estatus
            ///DESCRIPCIÓN          : Carga el Estilo para el Estatus
            ///PARAMETROS           : 
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 25/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Cargar_Estilo_Estatus() {
                String Estatus = Txt_Estatus.Text.Trim();
                switch (Estatus) {
                    case "PENDIENTE":
                        Txt_Estatus.BackColor = System.Drawing.Color.FromArgb(169, 188, 245);
                        Txt_Estatus.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
                        break;
                    case "RECHAZADA":
                        Txt_Estatus.BackColor = System.Drawing.Color.FromArgb(255, 46, 56);
                        Txt_Estatus.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
                        break;
                    case "CANCELADA":
                        Txt_Estatus.BackColor = System.Drawing.Color.FromArgb(184, 184, 184);
                        Txt_Estatus.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
                        break;
                    case "AUTORIZADA":
                        Txt_Estatus.BackColor = System.Drawing.Color.FromArgb(156, 255, 186);
                        Txt_Estatus.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
                        break;
                    case "EN_REPARACION":
                        Txt_Estatus.BackColor = System.Drawing.Color.FromArgb(156, 255, 186);
                        Txt_Estatus.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
                        break;
                    case "TERMINADO":
                        Txt_Estatus.BackColor = System.Drawing.Color.White;
                        Txt_Estatus.ForeColor = System.Drawing.Color.FromArgb(17, 80, 17);
                        break;
                    default:
                        Txt_Estatus.BackColor = System.Drawing.Color.White;
                        Txt_Estatus.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
                        break;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Cambiar_Estatus_Grid
            ///DESCRIPCIÓN          : Cambiar el Estatus que aparece en el Grid
            ///PARAMETROS           : 
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 25/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Cambiar_Estatus_Grid() {
                if (Grid_Listado_Solicitudes.Rows.Count > 0) {
                    foreach (GridViewRow Fila_Grid in Grid_Listado_Solicitudes.Rows) {
                        Fila_Grid.Cells[7].Text = Estatus_Solicitudes(HttpUtility.HtmlDecode(Fila_Grid.Cells[7].Text.Trim()).Trim());
                    }
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Estatus_Solicitudes
            ///DESCRIPCIÓN          : Regresa el Estatus a Mostrar.
            ///PARAMETROS           : 
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 25/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private String Estatus_Solicitudes(String Estatus_Operativo) {
                String Estatus_Mostrar = "";
                switch (Estatus_Operativo) {
                    case "PENDIENTE": Estatus_Mostrar = "PENDIENTE"; break;
                    case "AUTORIZADA": Estatus_Mostrar = "AUTORIZADA"; break;
                    case "CANCELADA": Estatus_Mostrar = "CANCELADA"; break;
                    case "RECHAZADA": Estatus_Mostrar = "RECHAZADA"; break;
                    case "EN_REPARACION": Estatus_Mostrar = "EN REPARACIÓN"; break;
                    case "TERMINADO": Estatus_Mostrar = "TERMINADO"; break;
                    case "ENTREGADO": Estatus_Mostrar = "ENTREGADO"; break;
                }
                return Estatus_Mostrar;
            }
                
        #endregion

        #region Generales [Configuracion, Limpiar]
                
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
            ///DESCRIPCIÓN: Limpia los campos del Formulario.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Limpiar_Formulario() {
                Hdf_No_Solicitud.Value = "";
                Hdf_Folio_Solicitud.Value = "";
                Txt_Folio_Solicitud.Text = "";
                Txt_Fecha_Elaboracion.Text = "";
                Txt_Estatus.Text = "";
                Cmb_Tipo_Servicio.SelectedIndex = 0;
                Txt_Kilometraje.Text = "";
                Cmb_Unidad_Responsable.SelectedIndex = 0;
                Txt_Descripcion_Servicio.Text = "";
                Txt_Correo_Electronico.Text = "";
                Txt_Diagnostico_Servicio.Text = "";
                Txt_Motivo_Rechazo.Text = "";
                Llenar_Grid_Seguimiento_Solicitud(new DataTable());
                Limpiar_Formulario_Vehiculo();
                Cargar_Estilo_Estatus();
                Llenar_Grid_Refacciones(new DataTable());
                Llenar_Grid_Listado_Servicios_Adicionales(new DataTable());
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario_Vehiculo
            ///DESCRIPCIÓN: Limpia los campos del Formulario [Seccion de Vehiculos].
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Limpiar_Formulario_Vehiculo() {
                Hdf_Vehiculo_ID.Value = "";
                Txt_No_Inventario.Text = "";
                Txt_No_Economico.Text = "";
                Txt_Datos_Vehiculo.Text = "";
                Txt_Placas.Text = "";
                Txt_Anio.Text = "";
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
            ///DESCRIPCIÓN: Se Habilitan y/o inhabilitan los campos dependendiendo de la Condicion.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Configuracion_Formulario(String Operacion) {
                switch (Operacion) {
                    case "INICIAL":
                        Btn_Salir.AlternateText = "Salir";
                        Btn_Salir.ToolTip = "Salir";
                        Btn_Salir.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                        Btn_Imprimir_Solicitud_Servicio.Visible = false;
                        Btn_Imprimir_Bitacora_Seguimiento.Visible = false;
                        Btn_Cancelar.Visible = false;
                        Btn_Dianostico.Visible = false;
                        Cmb_Tipo_Servicio.Enabled = false;
                        Txt_No_Inventario.Enabled = false;
                        Txt_Kilometraje.Enabled = false;
                        //Txt_Descripcion_Servicio.Enabled = false;
                        Txt_Correo_Electronico.Enabled = false;
                        Div_Listado_Solicitudes.Visible = true;
                        Div_Campos.Visible = false;
                        Pnl_Motivo_Rechazo.Visible = false;
                        Btn_Cancelar.Visible = false;
                        break;
                    case "OPERACION":
                        Btn_Salir.AlternateText = "Cancelar";
                        Btn_Salir.ToolTip = "Cancelar Operación";
                        Btn_Salir.Visible = true;                        
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        Btn_Imprimir_Solicitud_Servicio.Visible = true;
                        Cmb_Tipo_Servicio.Enabled = true;
                        Txt_No_Inventario.Enabled = true;
                        Txt_Kilometraje.Enabled = true;
                        //Txt_Descripcion_Servicio.Enabled = true;
                        Txt_Correo_Electronico.Enabled = true;
                        Div_Listado_Solicitudes.Visible = false;
                        Div_Campos.Visible = true;
                        break;
                    case "MOSTRAR INFORMACION":
                        Btn_Salir.AlternateText = "Salir";
                        Btn_Salir.ToolTip = "Salir";
                        Btn_Salir.Visible = true;
                        Btn_Cancelar.Visible = false;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                        Btn_Imprimir_Solicitud_Servicio.Visible = true;
                        Cmb_Tipo_Servicio.Enabled = false;
                        Txt_No_Inventario.Enabled = false;
                        Txt_Kilometraje.Enabled = false;
                        //Txt_Descripcion_Servicio.Enabled = false;
                        Txt_Correo_Electronico.Enabled = false;
                        Div_Listado_Solicitudes.Visible = false;
                        Div_Campos.Visible = true;
                        Pnl_Motivo_Rechazo.Visible = false;
                        Btn_Imprimir_Solicitud_Servicio.Visible = true;
                        Btn_Imprimir_Bitacora_Seguimiento.Visible = true;
                        Btn_Cancelar.Visible = true;
                        Btn_Dianostico.Visible = true;
                        break;
                }
            }

        #endregion

        #region Clase de Negocio de Solicitudes [Alta, Modificacion y Consulta]

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Mostrar_Registro
            ///DESCRIPCIÓN: Muestra el Registro en los campos.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Mostrar_Registro() {
                Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                Solicitud.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                Solicitud = Solicitud.Consultar_Detalles_Solicitud_Servicio();
                if (Solicitud.P_No_Solicitud > (-1)) {
                    Txt_Fecha_Elaboracion.Text = String.Format("{0:dd/MMM/yyyy}", Solicitud.P_Fecha_Elaboracion);
                    Txt_Folio_Solicitud.Text = Solicitud.P_Folio_Solicitud.Trim();
                    Cmb_Tipo_Servicio.SelectedIndex = Cmb_Tipo_Servicio.Items.IndexOf(Cmb_Tipo_Servicio.Items.FindByValue(Solicitud.P_Tipo_Servicio));
                    Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Solicitud.P_Dependencia_ID));
                    Hdf_Vehiculo_ID.Value = Solicitud.P_Bien_ID;
                    Hdf_Empleado_Solicito_ID.Value = Solicitud.P_Empleado_Solicito_ID;
                    Hdf_Email.Value = Solicitud.P_Correo_Electronico;
                    Txt_Descripcion_Servicio.Text = Solicitud.P_Descripcion_Servicio;
                    if (Solicitud.P_Kilometraje > (-1.0)) {
                        Txt_Kilometraje.Text = String.Format("{0:############0.00}", Solicitud.P_Kilometraje);
                    }
                    if (Solicitud.P_Tipo_Bien.Equals("BIEN_MUEBLE"))
                    {
                        Pnl_Bien_Mueble_Seleccionado.Visible = true;
                        Pnl_Vehiculo_Seleccionado.Visible = false;
                        Hdf_Bien_Mueble_ID.Value = Solicitud.P_Bien_ID;
                        Hdf_Tipo_Bien.Value = "BIEN_MUEBLE";
                        Cargar_Datos_Bien_Mueble(Hdf_Bien_Mueble_ID.Value, "IDENTIFICADOR");
                    }
                    else if (Solicitud.P_Tipo_Bien.Equals("VEHICULO"))
                    {
                        Pnl_Vehiculo_Seleccionado.Visible = true;
                        Pnl_Bien_Mueble_Seleccionado.Visible = false;
                        Hdf_Vehiculo_ID.Value = Solicitud.P_Bien_ID;
                        Hdf_Tipo_Bien.Value = "VEHICULO";
                        Cargar_Datos_Vehiculo(Hdf_Vehiculo_ID.Value, "IDENTIFICADOR");
                    }
                    if (Solicitud.P_Estatus.Trim().Equals("RECHAZADA")) {
                        Pnl_Motivo_Rechazo.Visible = true;
                        Txt_Motivo_Rechazo.Text = Solicitud.P_Motivo_Rechazo;
                    }
                    Txt_Estatus.Text = Solicitud.P_Estatus;
                    Txt_Correo_Electronico.Text = Solicitud.P_Correo_Electronico;
                    Hdf_Folio_Solicitud.Value = Solicitud.P_Folio_Solicitud;
                    Cargar_Estilo_Estatus();
                    Txt_Estatus.Text = Estatus_Solicitudes(Txt_Estatus.Text);
                    Llenar_Grid_Seguimiento_Solicitud(Solicitud.P_Dt_Seguimiento);
                    if (Cmb_Tipo_Servicio.SelectedItem.Value.Equals("SERVICIO_CORRECTIVO")) {
                        Cls_Ope_Tal_Servicios_Correctivos_Negocio Servicio_Negocio = new Cls_Ope_Tal_Servicios_Correctivos_Negocio();
                        Servicio_Negocio.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                        Servicio_Negocio = Servicio_Negocio.Consultar_Detalles_Servicio_Correctivo();
                        Txt_Diagnostico_Servicio.Text = Servicio_Negocio.P_Diagnostico.Trim();
                        Txt_Reparacion.Text = Servicio_Negocio.P_Reparacion.Trim();
                        Hdf_Estatus_Servicio.Value = Servicio_Negocio.P_Estatus.Trim();
                        Hdf_No_Servicio.Value = Servicio_Negocio.P_No_Servicio.ToString();
                        Hdf_Mecanico_ID.Value = Servicio_Negocio.P_Mecanico_ID.Trim();
                        Llenar_Grid_Refacciones(Servicio_Negocio.P_Dt_Refacciones);
                    } else if (Cmb_Tipo_Servicio.SelectedItem.Value.Equals("SERVICIO_PREVENTIVO")) {
                        Cls_Ope_Tal_Servicios_Preventivos_Negocio Servicio_Negocio = new Cls_Ope_Tal_Servicios_Preventivos_Negocio();
                        Servicio_Negocio.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                        Servicio_Negocio = Servicio_Negocio.Consultar_Detalles_Servicio_Preventivo();
                        Txt_Diagnostico_Servicio.Text = Servicio_Negocio.P_Diagnostico.Trim();
                        Txt_Reparacion.Text = Servicio_Negocio.P_Reparacion.Trim();
                        Hdf_Estatus_Servicio.Value = Servicio_Negocio.P_Estatus.Trim();
                        Hdf_No_Servicio.Value = Servicio_Negocio.P_No_Servicio.ToString();
                        Hdf_Mecanico_ID.Value = Servicio_Negocio.P_Mecanico_ID.Trim();
                        Llenar_Grid_Refacciones(Servicio_Negocio.P_Dt_Refacciones);
                    }
                    Mostrar_Servicios_Adicionales();
                }
            }

            private void Mostrar_Servicios_Adicionales() {
                Cls_Ope_Tal_Solicitud_Servicio_Negocio Sol_Negocio = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                Sol_Negocio.P_Procedencia = Hdf_No_Solicitud.Value.Trim();
                DataTable Dt_Resultados = Sol_Negocio.Consultar_Listado_Solicitudes_Servicio();
                Llenar_Grid_Listado_Servicios_Adicionales(Dt_Resultados);
            }

        #endregion

        #region Reporte Orden de Trabajo

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Generar_Reporte_Solicitud_Servicio
            ///DESCRIPCIÓN: Se Generan las Tablas para el Reporte de Orden de Trabajo
            ///PARAMETROS:  
            ///CREO: Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO: 24/Mayo/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            private void Generar_Reporte_Solicitud_Servicio() {
                //Dataset esqueleto del Reporte
                Ds_Rpt_Tal_Solicitud_Servicio Ds_Reporte = new Ds_Rpt_Tal_Solicitud_Servicio();

                DataTable Dt_Generales = new DataTable("DT_GENERALES");
                Dt_Generales = Ds_Reporte.Tables["DT_GENERALES"].Clone();
                Obtener_Datos_Generales(ref Dt_Generales);

                DataTable Dt_SubPartes = new Cls_Ope_Tal_Consultas_Generales_Negocio().Consultar_Partes_SubPartes_Vehiculo();
                Dt_SubPartes.TableName = "DT_SUBPARTES";

                Cls_Cat_Tal_Partes_Vehiculos_Negocio Parte_Negocio = new Cls_Cat_Tal_Partes_Vehiculos_Negocio();
                Parte_Negocio.P_Tipo = "PARTE";
                Parte_Negocio.P_Estatus = "VIGENTE";
                DataTable Dt_Partes_Completo = Parte_Negocio.Consultar_Partes();
                Dt_Partes_Completo.Columns["NOMBRE"].ColumnName = "PARTE_NOMBRE";
                //Dt_Partes.TableName = "DT_PARTES";

                DataTable Dt_Partes = Separar_Partes_Columnas(Dt_Partes_Completo);
                Dt_Partes.TableName = "DT_PARTES";
                //Se crea El DataSet de los Datos
                DataSet Ds_Consulta = new DataSet();
                Ds_Consulta.Tables.Add(Dt_Generales.Copy());
                Ds_Consulta.Tables.Add(Dt_SubPartes.Copy());
                Ds_Consulta.Tables.Add(Dt_Partes.Copy());

                //Generar y lanzar el reporte
                String Ruta_Reporte_Crystal = "Rpt_Tal_Solicitud_Servicio.rpt";
                Generar_Reporte(Ds_Consulta, Ds_Reporte, Ruta_Reporte_Crystal);

            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
            ///DESCRIPCIÓN: caraga el data set fisico con el cual se genera el Reporte especificado
            ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
            ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
            ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
            ///CREO: Susana Trigueros Armenta.
            ///FECHA_CREO: 01/Mayo/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            private void Generar_Reporte(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte)
            {                
                ReportDocument Reporte = new ReportDocument();
                String File_Path = Server.MapPath("../Rpt/Servicios_Generales/" + Nombre_Reporte);
                Reporte.Load(File_Path); 
                String Nombre_Reporte_Generar = "Rpt_Tal_Solicitud_Servicio_" + Cls_Sessiones.No_Empleado + "_" + Convert.ToString(DateTime.Now.ToString("yyyy'-'MM'-'dd'_t'HH'-'mm'-'ss")) + ".pdf";
                String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
                Ds_Reporte = Data_Set_Consulta_DB;
                Reporte.SetDataSource(Ds_Reporte);
                if (Data_Set_Consulta_DB.Tables["DT_REFACCIONES"].Rows.Count == 0 && !Nombre_Reporte.Contains("Revista"))
                    Reporte.ReportDefinition.ReportObjects["Srpt_Refacciones"].ObjectFormat.EnableSuppress = true;
                ExportOptions Export_Options = new ExportOptions();
                DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
                Disk_File_Destination_Options.DiskFileName = Server.MapPath(Ruta);
                Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
                Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
                Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Export_Options);
                //Reporte.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, Nombre_Reporte_Generar);
                Mostrar_Reporte(Nombre_Reporte_Generar, "PDF");
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Obtener_Datos_Generales
            ///DESCRIPCIÓN: Obtiene los datos generales del Reporte
            ///PARAMETROS:  
            ///CREO: Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO: 24/Mayo/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            private void Obtener_Datos_Generales(ref DataTable Dt_Generales) {
                DataRow Fila_Nueva = Dt_Generales.NewRow();
                Fila_Nueva["NO_SOLICITUD"] = Convert.ToInt32(Hdf_No_Solicitud.Value.Trim());
                Fila_Nueva["FECHA_ELABORACION"] = Convert.ToDateTime(Txt_Fecha_Elaboracion.Text);
                //Fila_Nueva["FECHA_RECIBO"] = "";
                Fila_Nueva["DEPENDENCIA"] = Cmb_Unidad_Responsable.SelectedItem.Text.Trim();
                Fila_Nueva["VEHICULO"] = Txt_Datos_Vehiculo.Text.Trim();
                Fila_Nueva["NO_INVENTARIO"] = Convert.ToInt32(Txt_No_Inventario.Text.Trim());
                Fila_Nueva["NO_ECONOMICO"] = Txt_No_Economico.Text.Trim();
                Fila_Nueva["PLACAS"] = Txt_Placas.Text.Trim();
                if (Txt_Anio.Text.Trim().Length > 0) {
                    Fila_Nueva["ANIO"] = Convert.ToInt32(Txt_Anio.Text.Trim());
                }
                //Fila_Nueva["KILOMETRAJE"] = "";
                Fila_Nueva["PROVEEDOR_ASIGNADO"] = Txt_Estatus.Text.Trim();
                Fila_Nueva["DESCRIPCION_SERVICIO"] = Txt_Descripcion_Servicio.Text.Trim();
                Fila_Nueva["FOLIO_SOLICITUD"] = Hdf_Folio_Solicitud.Value.Trim();
                Fila_Nueva["DIAGNOSTICO_SERVICIO"] = Txt_Diagnostico_Servicio.Text.Trim();
                Dt_Generales.Rows.Add(Fila_Nueva);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Separar_Partes_Columnas
            ///DESCRIPCIÓN: Separa en 3 columnas los datos de las tablas
            ///PARAMETROS:  
            ///CREO: Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO: 24/Mayo/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            private DataTable Separar_Partes_Columnas(DataTable Dt_Datos) {
                DataTable Dt_Partes_Separadas = new DataTable();
                Dt_Partes_Separadas.Columns.Add("PARTE_ID_1", Type.GetType("System.String"));
                Dt_Partes_Separadas.Columns.Add("PARTE_NOMBRE_1", Type.GetType("System.String"));
                Dt_Partes_Separadas.Columns.Add("PARTE_ID_2", Type.GetType("System.String"));
                Dt_Partes_Separadas.Columns.Add("PARTE_NOMBRE_2", Type.GetType("System.String"));
                Dt_Partes_Separadas.Columns.Add("PARTE_ID_3", Type.GetType("System.String"));
                Dt_Partes_Separadas.Columns.Add("PARTE_NOMBRE_3", Type.GetType("System.String"));
                Int32 Fila_Actual = -1;
                Int32 Columna_Toca = 1;
                foreach (DataRow Fila_Leida in Dt_Datos.Rows) {
                    if (Columna_Toca == 1) {
                        DataRow Fila_Nueva = Dt_Partes_Separadas.NewRow();
                        Fila_Nueva["PARTE_ID_1"] = Fila_Leida["PARTE_ID"].ToString().Trim();
                        Fila_Nueva["PARTE_NOMBRE_1"] = Fila_Leida["PARTE_NOMBRE"].ToString().Trim();
                        Dt_Partes_Separadas.Rows.Add(Fila_Nueva);
                        Fila_Actual++;
                        Columna_Toca = 2;
                    } else if (Columna_Toca == 2) {
                        Dt_Partes_Separadas.Rows[Fila_Actual].SetField("PARTE_ID_2", Fila_Leida["PARTE_ID"].ToString().Trim());
                        Dt_Partes_Separadas.Rows[Fila_Actual].SetField("PARTE_NOMBRE_2", Fila_Leida["PARTE_NOMBRE"].ToString().Trim());
                        Columna_Toca = 3;
                    } else if (Columna_Toca == 3) {
                        Dt_Partes_Separadas.Rows[Fila_Actual].SetField("PARTE_ID_3", Fila_Leida["PARTE_ID"].ToString().Trim());
                        Dt_Partes_Separadas.Rows[Fila_Actual].SetField("PARTE_NOMBRE_3", Fila_Leida["PARTE_NOMBRE"].ToString().Trim());
                        Columna_Toca = 1;
                    }
                }
                return Dt_Partes_Separadas;
            }        
        

        #endregion

        #region Reporte Bitacora

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Generar_Reporte_Bitacora_SeguimientoS
            ///DESCRIPCIÓN: Se Generan las Tablas para el Reporte de Orden de Trabajo
            ///PARAMETROS:  
            ///CREO: Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO: 24/Mayo/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            private void Generar_Reporte_Bitacora() {
                //Dataset esqueleto del Reporte
                Ds_Rpt_Tal_Bitacora_Seguimiento_Servicio Ds_Reporte = new Ds_Rpt_Tal_Bitacora_Seguimiento_Servicio();

                DataTable Dt_Generales = new DataTable("DT_GENERALES");
                Dt_Generales = Ds_Reporte.Tables["DT_GENERALES"].Clone();
                Obtener_Datos_Generales(ref Dt_Generales);

                DataTable Dt_Refacciones = new DataTable("DT_REFACCIONES");
                Dt_Refacciones = Obtener_Refacciones_Servicio();
                Dt_Refacciones.TableName = "DT_REFACCIONES";

                DataTable Dt_Seguimiento = new DataTable("DT_COMENTARIOS");
                Dt_Seguimiento = Obtener_Comentarios_Servicio();
                Dt_Seguimiento.TableName = "DT_COMENTARIOS";

                //Se crea El DataSet de los Datos
                DataSet Ds_Consulta = new DataSet();
                Ds_Consulta.Tables.Add(Dt_Generales.Copy());
                Ds_Consulta.Tables.Add(Dt_Refacciones.Copy());
                Ds_Consulta.Tables.Add(Dt_Seguimiento.Copy());
                Ds_Consulta.Tables.Add(new Cls_Tal_Parametros_Negocio().Obtener_Tabla_Reporte(this.Server));

                //Generar y lanzar el reporte
                String Ruta_Reporte_Crystal = "Rpt_Tal_Bitacora_Seguimiento_Servicio.rpt";
                Generar_Reporte_Bitacora(Ds_Consulta, Ds_Reporte, Ruta_Reporte_Crystal);

            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
            ///DESCRIPCIÓN: caraga el data set fisico con el cual se genera el Reporte especificado
            ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
            ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
            ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
            ///CREO: Susana Trigueros Armenta.
            ///FECHA_CREO: 01/Mayo/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            private void Generar_Reporte_Bitacora(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte)
            {
                ReportDocument Reporte = new ReportDocument();
                String File_Path = Server.MapPath("../Rpt/Servicios_Generales/" + Nombre_Reporte);
                Reporte.Load(File_Path);
                String Nombre_Reporte_Generar = "Rpt_Tal_Bit_Seg_Serv_" + Cls_Sessiones.No_Empleado + "_" + Convert.ToString(DateTime.Now.ToString("yyyy'-'MM'-'dd'_t'HH'-'mm'-'ss")) + ".pdf";
                String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
                Ds_Reporte = Data_Set_Consulta_DB;
                Reporte.SetDataSource(Ds_Reporte);
                ExportOptions Export_Options = new ExportOptions();
                DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
                Disk_File_Destination_Options.DiskFileName = Server.MapPath(Ruta);
                Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
                Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
                Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Export_Options);
                Mostrar_Reporte(Nombre_Reporte_Generar, "PDF");
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Obtener_Datos_Generales
            ///DESCRIPCIÓN: Obtiene los datos generales del Reporte
            ///PARAMETROS:  
            ///CREO: Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO: 24/Mayo/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            private void Obtener_Datos_Generales_Servicio(ref DataTable Dt_Generales) {
                DataRow Fila_Nueva = Dt_Generales.NewRow();
                Fila_Nueva["NO_SOLICITUD"] = Convert.ToInt32(Hdf_No_Solicitud.Value.Trim());
                Fila_Nueva["FECHA_ELABORACION"] = Convert.ToDateTime(Txt_Fecha_Elaboracion.Text);
                //Fila_Nueva["FECHA_RECIBO"] = "";
                Fila_Nueva["DEPENDENCIA"] = Cmb_Unidad_Responsable.SelectedItem.Text.Trim();
                Fila_Nueva["VEHICULO"] = Txt_Datos_Vehiculo.Text.Trim();
                Fila_Nueva["NO_INVENTARIO"] = Convert.ToInt32(Txt_No_Inventario.Text.Trim());
                Fila_Nueva["NO_ECONOMICO"] = Txt_No_Economico.Text.Trim();
                Fila_Nueva["PLACAS"] = Txt_Placas.Text.Trim();
                if (Txt_Anio.Text.Trim().Length > 0) {
                    Fila_Nueva["ANIO"] = Convert.ToInt32(Txt_Anio.Text.Trim());
                }
                //Fila_Nueva["KILOMETRAJE"] = "";
                //Fila_Nueva["PROVEEDOR_ASIGNADO"] = "";
                Fila_Nueva["DESCRIPCION_SERVICIO"] = Txt_Descripcion_Servicio.Text.Trim();
                Fila_Nueva["FOLIO_SOLICITUD"] = Hdf_Folio_Solicitud.Value.Trim();
                Dt_Generales.Rows.Add(Fila_Nueva);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Separar_Partes_Columnas
            ///DESCRIPCIÓN: Separa en 3 columnas los datos de las tablas
            ///PARAMETROS:  
            ///CREO: Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO: 24/Mayo/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            private DataTable Obtener_Comentarios_Servicio() {
                DataTable Dt_Comentarios = new DataTable();
                Dt_Comentarios.Columns.Add("ESTATUS", Type.GetType("System.String"));
                Dt_Comentarios.Columns.Add("FECHA_HORA", Type.GetType("System.String"));
                Dt_Comentarios.Columns.Add("EMPLEADO_ASIGNO", Type.GetType("System.String"));
                Dt_Comentarios.Columns.Add("COMENTARIOS", Type.GetType("System.String"));
                foreach (GridViewRow Gr_Fila in Grid_Seguimiento_Solicitud.Rows) {
                    DataRow Fila_Nueva = Dt_Comentarios.NewRow();
                    Fila_Nueva["ESTATUS"] = HttpUtility.HtmlDecode(Gr_Fila.Cells[1].Text).Trim();
                    Fila_Nueva["FECHA_HORA"] = HttpUtility.HtmlDecode(Gr_Fila.Cells[2].Text).Trim();
                    Fila_Nueva["EMPLEADO_ASIGNO"] = HttpUtility.HtmlDecode(Gr_Fila.Cells[3].Text).Trim();
                    Fila_Nueva["COMENTARIOS"] = HttpUtility.HtmlDecode(Gr_Fila.Cells[4].Text).Trim();
                    Dt_Comentarios.Rows.Add(Fila_Nueva);
                }   
                return Dt_Comentarios;
            }        

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Separar_Partes_Columnas
            ///DESCRIPCIÓN: Separa en 3 columnas los datos de las tablas
            ///PARAMETROS:  
            ///CREO: Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO: 24/Mayo/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            private DataTable Obtener_Refacciones_Servicio() {
                DataTable Dt_Comentarios = new DataTable();
                Dt_Comentarios.Columns.Add("CANTIDAD", Type.GetType("System.Int32"));
                Dt_Comentarios.Columns.Add("CLAVE", Type.GetType("System.String"));
                Dt_Comentarios.Columns.Add("NOMBRE", Type.GetType("System.String"));
                Dt_Comentarios.Columns.Add("TOTAL", Type.GetType("System.Double"));
                foreach (GridViewRow Gr_Fila in Grid_Listado_Refacciones.Rows) {
                    DataRow Fila_Nueva = Dt_Comentarios.NewRow();
                    Fila_Nueva["CANTIDAD"] = Convert.ToDouble(HttpUtility.HtmlDecode(Gr_Fila.Cells[0].Text).Trim());
                    Fila_Nueva["CLAVE"] = HttpUtility.HtmlDecode(Gr_Fila.Cells[1].Text).Trim();
                    Fila_Nueva["NOMBRE"] = HttpUtility.HtmlDecode(Gr_Fila.Cells[2].Text).Trim();
                    Fila_Nueva["TOTAL"] = Double.Parse(HttpUtility.HtmlDecode(Gr_Fila.Cells[3].Text).Trim());
                    Dt_Comentarios.Rows.Add(Fila_Nueva);
                }
                return Dt_Comentarios;
            }        
        

        #endregion

    #endregion
    
    #region Grids
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Solicitudes_PageIndexChanging
        ///DESCRIPCIÓN: Maneja la paginación del Listado
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Solicitudes_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            try {
                Grid_Listado_Solicitudes.SelectedIndex = (-1);
                Grid_Listado_Solicitudes.PageIndex = e.NewPageIndex;
                Llenar_Listado_Solicitudes();
            } catch(Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Solicitudes_SelectedIndexChanged
        ///DESCRIPCIÓN: Obtiene los datos de una Solicitud 
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Solicitudes_SelectedIndexChanged(object sender, EventArgs e) {
            try{
                if (Grid_Listado_Solicitudes.SelectedIndex > (-1)){
                    Limpiar_Formulario();
                    Hdf_No_Solicitud.Value = Grid_Listado_Solicitudes.SelectedRow.Cells[1].Text.Trim();
                    Configuracion_Formulario("MOSTRAR INFORMACION");
                    Mostrar_Registro();
                    if (Grid_Listado_Solicitudes.SelectedDataKey["ESTATUS"].ToString() != "CANCELADA")
                        Btn_Cancelar.Visible = true;
                    else
                        Btn_Cancelar.Visible = false;
                    if (Hdf_Estatus_Servicio.Value == "REPARACION" || Hdf_Estatus_Servicio.Value == "SALIDA_PROVEEDOR" || Hdf_Estatus_Servicio.Value == "ASIGNADO_PROVEEDOR")
                        Btn_Dianostico.Visible = true;
                    else
                        Btn_Dianostico.Visible = false;
                    Grid_Listado_Solicitudes.SelectedIndex = -1;
                    System.Threading.Thread.Sleep(500);
                }
            } catch(Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }
    #endregion

    #region Eventos
  
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Imprimir_Solicitud_Servicio_Click
        ///DESCRIPCIÓN: Imprime la Solicitud
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 23/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Imprimir_Solicitud_Servicio_Click(object sender, ImageClickEventArgs e) { 
            try {
                DataTable Dt_Refacciones = new DataTable("DT_REFACCIONES");
                Dt_Refacciones = Obtener_Refacciones_Servicio();
                Dt_Refacciones.TableName = "DT_REFACCIONES";
                if (Hdf_No_Solicitud.Value.Trim().Length > 0) 
                {
                    if (!Txt_Estatus.Text.Trim().Equals("CANCELADA") && !Txt_Estatus.Text.Trim().Equals("RECHAZADA") && !Txt_Estatus.Text.Trim().Equals("PENDIENTE")) {
                        Cls_Rpt_Tal_Solicitud_Servicio_Negocio Rpt_Solicitud = new Cls_Rpt_Tal_Solicitud_Servicio_Negocio();
                        Rpt_Solicitud.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value.Trim());
                        Rpt_Solicitud.P_No_Servicio = Hdf_No_Servicio.Value.Trim();
                        Rpt_Solicitud.P_Tipo_Servicio = Cmb_Tipo_Servicio.SelectedValue.Trim();
                        Rpt_Solicitud.P_Tipo_Bien = Hdf_Tipo_Bien.Value;
                        Rpt_Solicitud.P_Server_ = this.Server;
                        DataSet Ds_Consulta = Rpt_Solicitud.Crear_Reporte_Solicitud_Servicio();
                        if (Ds_Consulta != null)
                        {
                            Ds_Consulta.Tables.Add(Dt_Refacciones.Copy());

                            if (!Cmb_Tipo_Servicio.SelectedItem.Value.Equals("REVISTA_MECANICA")) Generar_Reporte(Ds_Consulta, new Ds_Rpt_Tal_Solicitud_Servicio(), "Rpt_Tal_Solicitud_Servicio.rpt");
                            else Generar_Reporte(Ds_Consulta, new Ds_Rpt_Tal_Revista_Mecanica(), "Rpt_Tal_Revista_Mecanica_Mejorada.rpt");
                        }
                    } else {
                        Lbl_Ecabezado_Mensaje.Text = "La solicitud debe estar Autorizada para Imprimirla.";
                        Lbl_Mensaje_Error.Text = ""; 
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                } else {
                    Lbl_Ecabezado_Mensaje.Text = "No hay Solicitud Seleccionada para Imprimir.";
                    Lbl_Mensaje_Error.Text = ""; 
                    Div_Contenedor_Msj_Error.Visible = true;
                }
                } catch (Exception Ex) {
                    Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                    Lbl_Mensaje_Error.Text = "Excepción ['" + Ex.Message + "']";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
        }

        /// *************************************************************************************
        /// NOMBRE:              Mostrar_Reporte
        /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
        /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
        ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
        /// USUARIO CREO:        Juan Alberto Hernández Negrete.
        /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
        /// USUARIO MODIFICO:    Salvador Hernández Ramírez
        /// FECHA MODIFICO:      23-Mayo-2011
        /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
        /// *************************************************************************************
        protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato) {
            String Pagina = "../../Reporte/";
            try {
                    Pagina = Pagina + Nombre_Reporte_Generar;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                    "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            } catch (Exception Ex) {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Ejecuta el Proceso para Salir.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e) {
            try {
                if (Btn_Salir.AlternateText.Trim().Equals("Salir")) {
                    if (Div_Campos.Visible) {
                        Limpiar_Formulario();
                        Configuracion_Formulario("INICIAL");
                    } else {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                } else {
                    Limpiar_Formulario();
                    Configuracion_Formulario("INICIAL"); 
                }
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                Lbl_Mensaje_Error.Text = "Excepción ['" + Ex.Message + "']";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Actualizar_Listado_Click
        ///DESCRIPCIÓN: Ejecuta el Proceso para Salir.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Actualizar_Listado_Click(object sender, ImageClickEventArgs e) {
            try {
                Llenar_Listado_Solicitudes();
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                Lbl_Mensaje_Error.Text = "Excepción ['" + Ex.Message + "']";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Cancelar_Servicio_Click
        ///DESCRIPCIÓN: Controla las operaciones del Boton Cerrar Reparacion
        ///PROPIEDADES:     
        ///CREO: Jesus Toledo Rdz
        ///FECHA_CREO: 21/Nov/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Dianostico_Click(object sender, ImageClickEventArgs e)
        {
            Regresar_Servicio();
            //Llenar_Listado_Observaciones();
            Configuracion_Formulario("INICIAL");
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "GACO", "alert('Operacion Exitosa: Servicio: " + Hdf_No_Servicio.Value.ToString() + "\n El servicio se pasó a Diagnóstico Mecánico. " + "');", true);
            Limpiar_Formulario();
            //Llenar_Listado_Servicios();
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Regresar_Servicio
        ///DESCRIPCIÓN:Cancela el Servicio para el proveedor asignado.
        ///PROPIEDADES:     
        ///CREO: Jesus Toledo Rodriguez.
        ///FECHA_CREO: 12/Octubre/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Regresar_Servicio()
        {
            //Transaccion
            //OracleConnection Cn = new OracleConnection();
            //OracleTransaction Trans = null;
            //OracleCommand Cmmd = new OracleCommand();
            //Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio Asignacion_Negocio = new Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio();
            try
            {
                //Inicializacion de Variables de la Transaccion
                //Cn.ConnectionString = Presidencia.Constantes.Cls_Constantes.Str_Conexion;
                //Cn.Open();
                //Trans = Cn.BeginTransaction();
                //Cmmd.Connection = Cn;
                //Cmmd.Transaction = Trans;
                //Asignacion_Negocio.P_Cmmd = Cmmd;
                //Inicializacion de Negocio
                //Asignacion_Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value.Trim());
                //Asignacion_Negocio.P_Proveedor_ID = Hdf_Proveedor_ID.Value.Trim();

                if (Cmb_Tipo_Servicio.SelectedItem.Value.Trim().Equals("SERVICIO_PREVENTIVO"))
                {
                    Cls_Ope_Tal_Servicios_Preventivos_Negocio Negocio = new Cls_Ope_Tal_Servicios_Preventivos_Negocio();
                    Negocio.P_Mecanico_ID = Hdf_Mecanico_ID.Value.Trim();
                    Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value.Trim());
                    Negocio.P_Estatus = "ASIGNADO_MECANICO";
                    Negocio.P_Estatus_Solicitud = null;
                    Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                    Negocio.Modifica_Servicio_Preventivo();
                }
                else if (Cmb_Tipo_Servicio.SelectedItem.Value.Trim().Equals("SERVICIO_CORRECTIVO"))
                {
                    Cls_Ope_Tal_Servicios_Correctivos_Negocio Negocio = new Cls_Ope_Tal_Servicios_Correctivos_Negocio();
                    Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value.Trim());
                    Negocio.P_Mecanico_ID = Hdf_Mecanico_ID.Value.Trim();
                    Negocio.P_Estatus = "ASIGNADO_MECANICO";
                    Negocio.P_Estatus_Solicitud = null;
                    Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                    Negocio.Modifica_Servicio_Correctivo();
                }
                //Asignacion_Negocio.P_Abono = "PRE_COMPROMETIDO";
                //Asignacion_Negocio.P_Cargo = "DISPONIBLE";
                //Asignacion_Negocio.P_Costo = Convert.ToDouble(Txt_Costo_Aproximado.Text.Trim());
                //Asignacion_Negocio.P_Nombre_Proveedor = Txt_Nombre_Proveedor.Text.Trim();
                //Asignacion_Negocio.P_No_Inventario = Txt_No_Inventario.Text.Trim();
                //Asignacion_Negocio.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value.Trim());
                //Asignacion_Negocio.Cancelar_Reserva_Servicio();
                ////Ejecucucion de la Transaccion
                //Trans.Commit();
            }
            catch (Exception Ex)
            {
                //Trans.Rollback();
                //Mensaje_Error("Generar_Orden " + Ex.Message);
            }
            finally
            {
                //Cn.Close();
            }

        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Ver_Listado_Refacciones
        ///DESCRIPCIÓN: Muestra el Listado de las Refacciones.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Ver_Listado_Refacciones_Click(object sender, ImageClickEventArgs e) {
            MPE_Listado_Refacciones.Show();
        }
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Ver_Servicios_Adicionales_Click
        ///DESCRIPCIÓN: Muestra el Listado de los Servicios Adicionales.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 06/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Ver_Servicios_Adicionales_Click(object sender, ImageClickEventArgs e) {
            MPE_Servicios_Adicionales.Show();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Imprimir_Bitacora_Seguimiento_Click
        ///DESCRIPCIÓN: Imprime la Bitacora de Seguimiento
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 06/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Imprimir_Bitacora_Seguimiento_Click(object sender, ImageClickEventArgs e) { 
            try {
                if (Hdf_No_Solicitud.Value.Trim().Length > 0) {
                    Generar_Reporte_Bitacora();
                } else {
                    Lbl_Ecabezado_Mensaje.Text = "No hay Solicitud Seleccionada para Imprimir.";
                    Lbl_Mensaje_Error.Text = ""; 
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                Lbl_Mensaje_Error.Text = "Excepción ['" + Ex.Message + "']";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Cancelar_Click
        ///DESCRIPCIÓN: Cancela el Servicio
        ///PROPIEDADES:     
        ///CREO: Jesus Toledo Rodriguez
        ///FECHA_CREO: 16/Octubre/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Cancelar_Click(object sender, ImageClickEventArgs e)
        {
            //Transaccion
            SqlConnection Cn = new SqlConnection();
            SqlTransaction Trans = null;
            SqlCommand Cmmd = new SqlCommand();
            //Variables de Negocio
            Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
            Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio Cancelar_Reserva = new Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio();
            try
            {                
                Solicitud.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                Solicitud.P_Folio_Solicitud = Txt_Folio_Solicitud.Text.Trim();
                Solicitud.P_Empleado_Solicito_ID = Hdf_Empleado_Solicito_ID.Value;
                Solicitud.P_Descripcion_Servicio = Txt_Descripcion_Servicio.Text.Trim();
                Solicitud.P_Estatus = "CANCELADA";
                Solicitud.P_Fecha_Elaboracion = Convert.ToDateTime(Txt_Fecha_Elaboracion.Text.Trim());
                Solicitud.P_Tipo_Servicio = Cmb_Tipo_Servicio.SelectedValue;
                Solicitud.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedValue;
                Solicitud.P_Bien_ID = Hdf_Vehiculo_ID.Value;
                Solicitud.P_Tipo_Bien = Hdf_Tipo_Bien.Value.Trim();
                Solicitud.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                Solicitud.P_Motivo_Rechazo = "";
                Solicitud.P_Correo_Electronico = Hdf_Email.Value;
                //Inicializacion de Variables de la Transaccion
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Cn;
                Cmmd.Transaction = Trans;
                Solicitud.P_Cmmd = Cmmd;
                Solicitud.Modifica_Solicitud_Servicio();
                //Se cancela Servicio Correctivo/Preventivo
                if (Cmb_Tipo_Servicio.SelectedItem.Value.Trim().Equals("SERVICIO_PREVENTIVO"))
                {
                    Cls_Ope_Tal_Servicios_Preventivos_Negocio Negocio = new Cls_Ope_Tal_Servicios_Preventivos_Negocio();
                    Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value.Trim());
                    Negocio.P_Mecanico_ID = Hdf_Mecanico_ID.Value.Trim();
                    Negocio.P_Estatus = "CANCELADO";
                    Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                    if (Hdf_No_Servicio.Value.Trim().Length > 0)
                    {
                        Negocio.P_Cmmd = Cmmd;
                        Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value.Trim());
                        Negocio.Modifica_Servicio_Preventivo();
                    }
                }
                else if (Cmb_Tipo_Servicio.SelectedItem.Value.Trim().Equals("SERVICIO_CORRECTIVO"))
                {
                    Cls_Ope_Tal_Servicios_Correctivos_Negocio Negocio = new Cls_Ope_Tal_Servicios_Correctivos_Negocio();
                    Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value.Trim());
                    Negocio.P_Mecanico_ID = Hdf_Mecanico_ID.Value.Trim();
                    Negocio.P_Estatus = "CANCELADO";
                    Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                    if (Hdf_No_Servicio.Value.Trim().Length > 0)
                    {
                        Negocio.P_Cmmd = Cmmd;
                        Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value.Trim());
                        Negocio.Modifica_Servicio_Correctivo();
                    }
                }
                Cancelar_Reserva.P_Tipo_Bien = Hdf_Tipo_Bien.Value.Trim();
                Cancelar_Reserva.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedValue;
                Cancelar_Reserva.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                Cancelar_Reserva.P_Cmmd = Cmmd;
                Cancelar_Reserva.P_Abono = (Hdf_Estatus_Servicio.Value == "COTIZADO_PROVEEDOR") ? "PRE_COMPROMETIDO" : "COMPROMETIDO";
                Cancelar_Reserva.P_Cargo = "DISPONIBLE";
                Cancelar_Reserva.P_No_Inventario = Txt_No_Inventario.Text.Trim();
                Cancelar_Reserva.P_No_Economico = Txt_No_Economico.Text.Trim();
                Cancelar_Reserva.Cancelar_Reserva_Servicio();
                Trans.Commit();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "GACO", "alert('Operacion Exitosa: Se canceló la Solicitud');", true);
                Configuracion_Formulario("INICIAL");
                Llenar_Listado_Solicitudes();
            }
            catch (Exception Ex)
            {
                Trans.Rollback();
                Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                Lbl_Mensaje_Error.Text = "Excepción ['" + Ex.Message + "']";
                Div_Contenedor_Msj_Error.Visible = true;
                
            }
            finally
            {
                Cn.Close();
            }
        }        

    
    #endregion

}