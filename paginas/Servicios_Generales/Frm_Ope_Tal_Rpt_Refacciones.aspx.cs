﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Text;
using System.Data;
using JAPAMI.Constantes;
using JAPAMI.Taller_Mecanico.Reporte.Refacciones.Negocio;
using JAPAMI.Sessiones;
using JAPAMI.Empleados.Negocios;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;
using JAPAMI.Taller_Mecanico.Reporte_Gastos.Negocio;
using CarlosAg.ExcelXmlWriter;
using JAPAMI.Control_Patrimonial_Operacion_Vehiculos.Negocio;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using JAPAMI.Taller_Mecanico.Catalogo_Mecanicos.Negocio;
using JAPAMI.Taller_Mecanico.Catalogo_Tipos_Trabajo.Negocio;

public partial class paginas_Taller_Mecanico_Frm_Rpt_Refacciones : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Txt_Fecha_Recepcion_Final.Attributes.Add("readonly", "true");
        Txt_Fecha_Recepcion_Inicial.Attributes.Add("readonly", "true");
        Div_Contenedor_Msj_Error.Visible = false;
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        if (!IsPostBack)
        {
            Llenar_Combo_Unidades_Responsables();
            Llenar_Combo_Trabajos();
            Llenar_Combo_Mecanicos();
            Rdb_Tipo_Reporte.SelectedValue = "1";
            Pnl_Vehiculo_Seleccionado.Style.Add("display", "none");
            Pnl_Mecanicos.Style.Add("display", "none");
            Pnl_Unidad_Responsable.Style.Add("display", "inline");
            Cmb_Reparacion.SelectedIndex = 1;
            Cmb_Reparacion.Enabled = false;
        }
    }
    #region Metodos
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Unidades_Responsables
    ///DESCRIPCIÓN: Se llena el Combo de las Unidades Responsables.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Junio/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Llenar_Combo_Unidades_Responsables()
    {
        Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
        Negocio.P_Estatus = "ACTIVO";
        DataTable Dt_Dependencias = Negocio.Consultar_Unidades_Responsables();
        Cmb_Unidad_Responsable.DataSource = Dt_Dependencias;
        Cmb_Unidad_Responsable.DataTextField = "CLAVE_NOMBRE";
        Cmb_Unidad_Responsable.DataValueField = "DEPENDENCIA_ID";
        Cmb_Unidad_Responsable.DataBind();
        Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("< - - TODAS - - >", ""));
        Cmb_Busqueda_Vehiculo_Dependencias.DataSource = Dt_Dependencias;
        Cmb_Busqueda_Vehiculo_Dependencias.DataTextField = "CLAVE_NOMBRE";
        Cmb_Busqueda_Vehiculo_Dependencias.DataValueField = "DEPENDENCIA_ID";
        Cmb_Busqueda_Vehiculo_Dependencias.DataBind();
        Cmb_Busqueda_Vehiculo_Dependencias.Items.Insert(0, new ListItem("< - - TODAS - - >", ""));
        Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.DataSource = Dt_Dependencias;
        Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.DataTextField = "CLAVE_NOMBRE";
        Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.DataValueField = "DEPENDENCIA_ID";
        Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.DataBind();
        Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.Items.Insert(0, new ListItem("< - - TODAS - - >", ""));
    }

    private void Llenar_Combo_Trabajos()
    {
        Cls_Cat_Tal_Tipos_Trabajo_Negocio Tipos_Trabajo_Negocio = new Cls_Cat_Tal_Tipos_Trabajo_Negocio();
        Tipos_Trabajo_Negocio.P_Estatus = "VIGENTE";
        DataTable Dt_Consulta_T_T = Tipos_Trabajo_Negocio.Consultar_Tipos_Trabajo();
        Cmb_Tipos_Trabajo.DataSource = Dt_Consulta_T_T;
        Cmb_Tipos_Trabajo.DataTextField = Cat_Tal_Tipos_Trabajo.Campo_Nombre;
        Cmb_Tipos_Trabajo.DataValueField = Cat_Tal_Tipos_Trabajo.Campo_Tipo_Trabajo_ID;
        Cmb_Tipos_Trabajo.DataBind();
        Cmb_Tipos_Trabajo.Items.Insert(0, new ListItem("< - - TODAS - - >", ""));
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Mecanicos
    ///DESCRIPCIÓN: Se llena el Listado de los Mecanicos.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Llenar_Combo_Mecanicos()
    {
        Cls_Cat_Tal_Mecanicos_Negocio Negocio = new Cls_Cat_Tal_Mecanicos_Negocio();
        //Negocio.P_Estatus = "VIGENTE";
        DataTable Dt_Resultados = Negocio.Consultar_Mecanicos();
        Cmb_Lista_Mecanicos.DataSource = Dt_Resultados;
        Cmb_Lista_Mecanicos.DataTextField = "NOMBRE_EMPLEADO";
        Cmb_Lista_Mecanicos.DataValueField = "MECANICO_ID";
        Cmb_Lista_Mecanicos.DataBind();
        Cmb_Lista_Mecanicos.Items.Insert(0, new ListItem("< - SELECCIONE - >", ""));
    }
    ///*************************************************************************************************************************
    ///Nombre: Generar_Reporte
    ///Descripción: Generar_Reporte
    ///Parámetros: Tipo
    ///CREO: Salvador Jesus Toledo
    ///FECHA_CREO: 14/Junio/2012
    ///Usuario Modifico:
    ///Fecha Modifico:
    ///Causa Modificación:
    ///*************************************************************************************************************************
    private void Generar_Reporte(String Tipo)
    {
        bool Filtros = true;
        Cls_Rpt_Tal_Reporte_Gastos_Negocio Rpt_Negocio = new Cls_Rpt_Tal_Reporte_Gastos_Negocio();
        DataTable Dt_Resultados = new DataTable();
        if (Cmb_Tipo_Servicio.SelectedIndex > 0) { Rpt_Negocio.P_Tipo_Servicio = Cmb_Tipo_Servicio.SelectedItem.Value.Trim(); } else { Rpt_Negocio.P_Tipo_Servicio = "TODOS"; }
        if (Cmb_Reparacion.SelectedIndex > 0) { Rpt_Negocio.P_Tipo_Reparacion = Cmb_Reparacion.SelectedItem.Value.Trim(); } else { Rpt_Negocio.P_Tipo_Reparacion = "TODOS"; }
        if (Cmb_Tipos_Trabajo.SelectedIndex > 0) { Rpt_Negocio.P_Tipo_Trabajo = Cmb_Tipos_Trabajo.SelectedItem.Text.Trim(); } else { Rpt_Negocio.P_Tipo_Trabajo = "TODOS"; }
        if (!String.IsNullOrEmpty(Txt_Fecha_Recepcion_Inicial.Text)) { Rpt_Negocio.P_F_Recep_Ini = Convert.ToDateTime(Txt_Fecha_Recepcion_Inicial.Text); }
        if (!String.IsNullOrEmpty(Txt_Fecha_Recepcion_Final.Text)) { Rpt_Negocio.P_F_Recep_Fin = Convert.ToDateTime(Txt_Fecha_Recepcion_Final.Text); }

        if (Rdb_Tipo_Reporte.SelectedValue == "1")
        {
            if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Rpt_Negocio.P_Filtro = " Unidad Responsable: " + Cmb_Unidad_Responsable.SelectedItem.Text.Trim(); } else { Filtros = true; }
            if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Rpt_Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value.Trim(); }
        }
        else if (Rdb_Tipo_Reporte.SelectedValue == "2")
        {
            if (!String.IsNullOrEmpty(Txt_No_Inventario.Text)) { Rpt_Negocio.P_Filtro = "Economico No: " + Txt_No_Economico.Text; } else { Filtros = false; }
            if (!String.IsNullOrEmpty(Txt_No_Inventario.Text.Trim())) { Rpt_Negocio.P_Numero_Inventario = Int32.Parse(Txt_No_Inventario.Text.Trim()); }

        }
        else if (Rdb_Tipo_Reporte.SelectedValue == "3")
        {
            if (Cmb_Lista_Mecanicos.SelectedIndex > 0) { Rpt_Negocio.P_Filtro = "Mecanico: " + Cmb_Lista_Mecanicos.SelectedItem.Text.Trim(); } else { Filtros = false; }
            if (Cmb_Lista_Mecanicos.SelectedIndex > 0) { Rpt_Negocio.P_Nombre_Mecanico = (Cmb_Lista_Mecanicos.SelectedItem.Text.Trim().Split('-'))[1].Trim(); }

        }

        //if (Cmb_Tipos_Trabajo.SelectedIndex > 0) { Rpt_Negocio.P_Tipo_Trabajo = Cmb_Tipos_Trabajo.SelectedItem.Value.Trim(); } else { Rpt_Negocio.P_Tipo_Trabajo = "TODOS"; }
        if (Filtros)
        {
            Dt_Resultados = Rpt_Negocio.Consulta_Reporte_Refacciones();
            if (Tipo.Trim().Equals("PDF"))
            {
                Ds_Rpt_Tal_Refacciones Ds_Reporte = new Ds_Rpt_Tal_Refacciones();

                DataSet Ds_Consulta = new DataSet();
                Dt_Resultados.TableName = "DT_GENERALES";
                Ds_Consulta.Tables.Add(Dt_Resultados.Copy());
                Ds_Consulta.Tables.Add(new Cls_Tal_Parametros_Negocio().Obtener_Tabla_Reporte(this.Server));
                //Generar y lanzar el reporte
                String Ruta_Reporte_Crystal = "Rpt_Tal_Refacciones.rpt";
                Generar_Reporte(Ds_Consulta, Ds_Reporte, Ruta_Reporte_Crystal);
            }
            else
            {
                Pasar_DataTable_A_Excel(Dt_Resultados);
            }
        }
        else
        {
            Lbl_Ecabezado_Mensaje.Text = "Seleccione los Filtros Primero";
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Listado_Vehiculos
    ///DESCRIPCIÓN          : Llena el Grid de los Vehiculos.
    ///PARAMETROS           : 
    ///CREO                 : Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO           : 15/Mayo/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Grid_Listado_Bienes_Vehiculos(Int32 Pagina)
    {
        try
        {
            Grid_Listado_Busqueda_Vehiculo.Columns[1].Visible = true;
            Cls_Ope_Pat_Com_Vehiculos_Negocio Vehiculos = new Cls_Ope_Pat_Com_Vehiculos_Negocio();
            Vehiculos.P_Tipo_DataTable = "VEHICULOS";
            if (Session["FILTRO_BUSQUEDA"] != null)
            {
                Vehiculos.P_Tipo_Filtro_Busqueda = Session["FILTRO_BUSQUEDA"].ToString();
                Vehiculos.P_Estatus = "VIGENTE";
                if (Session["FILTRO_BUSQUEDA"].ToString().Trim().Equals("DATOS_GENERALES"))
                {
                    if (Txt_Busqueda_Vehiculo_Numero_Inventario.Text.Trim().Length > 0) { Vehiculos.P_Numero_Inventario = Convert.ToInt64(Txt_Busqueda_Vehiculo_Numero_Inventario.Text.Trim()); }
                    if (Txt_Busqueda_Vehiculo_Numero_Economico.Text.Trim().Length > 0) { Vehiculos.P_Numero_Economico_ = Txt_Busqueda_Vehiculo_Numero_Economico.Text.Trim(); }
                    if (Txt_Busqueda_Vehiculo_Anio_Fabricacion.Text.Trim().Length > 0) { Vehiculos.P_Anio_Fabricacion = Convert.ToInt32(Txt_Busqueda_Vehiculo_Anio_Fabricacion.Text.Trim()); }
                    Vehiculos.P_Modelo_ID = Txt_Busqueda_Vehiculo_Modelo.Text.Trim();
                    if (Cmb_Busqueda_Vehiculo_Marca.SelectedIndex > 0)
                    {
                        Vehiculos.P_Marca_ID = Cmb_Busqueda_Vehiculo_Marca.SelectedItem.Value.Trim();
                    }
                    if (Cmb_Busqueda_Vehiculo_Color.SelectedIndex > 0)
                    {
                        Vehiculos.P_Color_ID = Cmb_Busqueda_Vehiculo_Color.SelectedItem.Value.Trim();
                    }
                    if (Cmb_Busqueda_Vehiculo_Dependencias.SelectedIndex > 0)
                    {
                        Vehiculos.P_Dependencia_ID = Cmb_Busqueda_Vehiculo_Dependencias.SelectedItem.Value.Trim();
                    }
                }
                else if (Session["FILTRO_BUSQUEDA"].ToString().Trim().Equals("RESGUARDANTES"))
                {
                    Vehiculos.P_RFC_Resguardante = Txt_Busqueda_Vehiculo_RFC_Resguardante.Text.Trim();
                    Vehiculos.P_No_Empleado = Txt_Busqueda_Vehiculo_No_Empleado.Text.Trim();
                    if (Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.SelectedIndex > 0)
                    {
                        Vehiculos.P_Dependencia_ID = Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.SelectedItem.Value.Trim();
                    }
                    if (Cmb_Busqueda_Vehiculo_Nombre_Resguardante.SelectedIndex > 0)
                    {
                        Vehiculos.P_Resguardante_ID = Cmb_Busqueda_Vehiculo_Nombre_Resguardante.SelectedItem.Value.Trim();
                    }
                }
            }
            Grid_Listado_Busqueda_Vehiculo.DataSource = Vehiculos.Consultar_DataTable();
            Grid_Listado_Busqueda_Vehiculo.PageIndex = Pagina;
            Grid_Listado_Busqueda_Vehiculo.DataBind();
            Grid_Listado_Busqueda_Vehiculo.Columns[1].Visible = false;
            MPE_Busqueda_Vehiculo.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Empleados
    ///DESCRIPCIÓN          : Llena el Combo con los empleados de una dependencia.
    ///PARAMETROS           : 
    ///CREO                 : Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO           : 15/Mayo/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Combo_Empleados(String Dependencia_ID, ref DropDownList Combo_Empleados)
    {
        Combo_Empleados.Items.Clear();
        if (Dependencia_ID != null && Dependencia_ID.Trim().Length > 0)
        {
            Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            Negocio.P_Estatus = "ACTIVO";
            Negocio.P_Dependencia_ID = Dependencia_ID.Trim();
            DataTable Dt_Datos = Negocio.Consultar_Empleados();
            Combo_Empleados.DataSource = Dt_Datos;
            Combo_Empleados.DataValueField = "EMPLEADO_ID";
            Combo_Empleados.DataTextField = "NOMBRE";
            Combo_Empleados.DataBind();
        }
        Combo_Empleados.Items.Insert(0, new ListItem("< TODOS >", ""));
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Vehiculo
    ///DESCRIPCIÓN: Se cargan los Datos del Vehiculo Seleccionado.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Cargar_Datos_Vehiculo(String Vehiculo, String Tipo_Busqueda)
    {
        Limpiar_Formulario_Vehiculo();
        Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
        switch (Tipo_Busqueda)
        {
            case "NO_INVENTARIO":
                Consulta_Negocio.P_No_Inventario = Vehiculo;
                break;
            case "IDENTIFICADOR":
                Consulta_Negocio.P_Vehiculo_ID = Vehiculo;
                break;
            default: break;
        }
        if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Consulta_Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value; }
        DataTable Dt_Vehiculo = Consulta_Negocio.Consultar_Vehiculos();
        if (Dt_Vehiculo.Rows.Count > 0)
        {
            Hdf_Vehiculo_ID.Value = Dt_Vehiculo.Rows[0]["VEHICULO_ID"].ToString().Trim();
            Txt_No_Inventario.Text = Dt_Vehiculo.Rows[0]["NO_INVENTARIO"].ToString().Trim();
            Txt_No_Economico.Text = Dt_Vehiculo.Rows[0]["NO_ECONOMICO"].ToString().Trim();
            Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString()));
        }
        else
        {
            Lbl_Ecabezado_Mensaje.Text = "El Vehículo no se encontro.";
            if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Lbl_Mensaje_Error.Text = "[Puede ser que no este asignado a la Unidad Responsable Seleccionada]."; }
            else { Lbl_Mensaje_Error.Text = ""; }
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    private void Limpiar_Formulario_Vehiculo()
    {
        Hdf_Vehiculo_ID.Value = "";
        Txt_No_Inventario.Text = "";
        Txt_No_Economico.Text = "";
    }

    #endregion
    #region Eventos
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Reporte_PDF_Click
    ///DESCRIPCIÓN: Lanza reporte en PDF
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Junio/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    protected void Btn_Reporte_PDF_Click(object sender, ImageClickEventArgs Event)
    {
        Generar_Reporte("PDF");
    }
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");            
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Mensaje_Error.Text = "Excepción ['" + Ex.Message + "']";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    protected void Rdb_Tipo_Reporte_Click(object sender, EventArgs e)
    {
        if (Rdb_Tipo_Reporte.SelectedValue == "1")
        {
            Pnl_Vehiculo_Seleccionado.Style.Add("display", "none");
            Pnl_Mecanicos.Style.Add("display", "none");
            Pnl_Unidad_Responsable.Style.Add("display", "inline");
        }
        else if (Rdb_Tipo_Reporte.SelectedValue == "2")
        {
            Pnl_Unidad_Responsable.Style.Add("display", "none");
            Pnl_Mecanicos.Style.Add("display", "none");

            Pnl_Vehiculo_Seleccionado.Style.Add("display", "inline");
        }
        else if (Rdb_Tipo_Reporte.SelectedValue == "3")
        {
            Pnl_Unidad_Responsable.Style.Add("display", "none");
            Pnl_Vehiculo_Seleccionado.Style.Add("display", "none");
            Pnl_Mecanicos.Style.Add("display", "inline");

        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Datos_Vehiculo_Click
    ///DESCRIPCIÓN: Ejecuta la Busqueda de los Vehiculos por datos generales.
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 15/Mayo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Buscar_Datos_Vehiculo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Session["FILTRO_BUSQUEDA"] = "DATOS_GENERALES";
            Llenar_Grid_Listado_Bienes_Vehiculos(0);
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Avanzada_Vehiculo_Click
    ///DESCRIPCIÓN: Lanza la Busqueda del Vehiculo
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Junio/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    protected void Btn_Busqueda_Avanzada_Vehiculo_Click(object sender, ImageClickEventArgs Event)
    {
        MPE_Busqueda_Vehiculo.Show();
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Limpiar_Filtros_Buscar_Datos_Vehiculo_Click
    ///DESCRIPCIÓN          : Hace la limpieza de los campos.
    ///PARAMETROS           : 
    ///CREO                 : Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO           : 15/Mayo/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Btn_Limpiar_Filtros_Buscar_Datos_Vehiculo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Txt_Busqueda_Vehiculo_Numero_Inventario.Text = "";
            Txt_Busqueda_Vehiculo_Numero_Economico.Text = "";
            Txt_Busqueda_Vehiculo_Modelo.Text = "";
            Cmb_Busqueda_Vehiculo_Marca.SelectedIndex = 0;
            Txt_Busqueda_Vehiculo_Anio_Fabricacion.Text = "";
            Cmb_Busqueda_Vehiculo_Color.SelectedIndex = 0;
            Cmb_Busqueda_Vehiculo_Dependencias.SelectedIndex = 0;
            MPE_Busqueda_Vehiculo.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Excepción.";
            Lbl_Mensaje_Error.Text = "Ex:['" + Ex.Message + "']";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias_SelectedIndexChanged
    ///DESCRIPCIÓN          : Maneja el evento del Combo de Dependencias.
    ///PARAMETROS           : 
    ///CREO                 : Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO           : 15/Mayo/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.SelectedIndex > 0)
        {
            Llenar_Combo_Empleados(Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.SelectedItem.Value.Trim(), ref Cmb_Busqueda_Vehiculo_Nombre_Resguardante);
        }
        else
        {
            Llenar_Combo_Empleados(null, ref Cmb_Busqueda_Vehiculo_Nombre_Resguardante);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Resguardante_Vehiculo_Click
    ///DESCRIPCIÓN: Ejecuta la Busqueda de los vehiculos por resguardos.
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 15/Mayo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Buscar_Resguardante_Vehiculo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Session["FILTRO_BUSQUEDA"] = "RESGUARDANTES";
            Llenar_Grid_Listado_Bienes_Vehiculos(0);
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Limpiar_Filtros_Buscar_Resguardante_Vehiculo_Click
    ///DESCRIPCIÓN          : Hace la limpieza de los campos.
    ///PARAMETROS           : 
    ///CREO                 : Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO           : 15/Mayo/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Btn_Limpiar_Filtros_Buscar_Resguardante_Vehiculo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Txt_Busqueda_Vehiculo_RFC_Resguardante.Text = "";
            Txt_Busqueda_Vehiculo_No_Empleado.Text = "";
            Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.SelectedIndex = 0;
            Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias_SelectedIndexChanged(Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias, null);
            Cmb_Busqueda_Vehiculo_Nombre_Resguardante.SelectedIndex = 0;
            MPE_Busqueda_Vehiculo.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Excepción.";
            Lbl_Mensaje_Error.Text = "Ex:['" + Ex.Message + "']";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Productos_PageIndexChanging
    ///DESCRIPCIÓN: Maneja la paginación del GridView de los Vehiculos
    ///PROPIEDADES:     
    ///CREO : Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 15/Mayo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Listado_Busqueda_Vehiculo_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Llenar_Grid_Listado_Bienes_Vehiculos(e.NewPageIndex);
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Busqueda_Vehiculo_SelectedIndexChanged
    ///DESCRIPCIÓN: Maneja el evento de cambio de Seleccion del GridView de Vehiculos del
    ///             Modal de Busqueda.
    ///PROPIEDADES:     
    ///CREO : Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 15/Mayo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Listado_Busqueda_Vehiculo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (Grid_Listado_Busqueda_Vehiculo.SelectedIndex > (-1))
            {
                String Vehiculo_ID = Grid_Listado_Busqueda_Vehiculo.SelectedRow.Cells[1].Text.Trim();
                Cargar_Datos_Vehiculo(Vehiculo_ID, "IDENTIFICADOR");
                Rdb_Tipo_Reporte.SelectedIndex = 1;
            }
            MPE_Busqueda_Vehiculo.Hide();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    #endregion
    #region Reporte

    ///*************************************************************************************************************************
    ///Nombre: Pasar_DataTable_A_Excel
    ///Descripción: Pasa DataTable a Excel. 
    ///Parámetros: Dt_Reporte.- DataTable que se pasara a excel. 
    ///CREO: Salvador Vázquez Camacho.
    ///FECHA_CREO: 14/Junio/2012
    ///Usuario Modifico:
    ///Fecha Modifico:
    ///Causa Modificación:
    ///*************************************************************************************************************************
    public void Pasar_DataTable_A_Excel(System.Data.DataTable Dt_Reporte)
    {
        String Ruta = "Entradas [" + (String.Format("{0:dd_MMM_yyyy}", DateTime.Now)) + "].xls";//Variable que almacenara el nombre del archivo. 

        Dt_Reporte.Columns.RemoveAt(3);
        Dt_Reporte.Columns["FOLIO_SOLICITUD"].ColumnName = "FOLIO";
        Dt_Reporte.Columns["FECHA_SOLICITUD"].ColumnName = "FECHA SOLICITUD";
        Dt_Reporte.Columns["TIPO_SOLICIUD"].ColumnName = "TIPO";
        Dt_Reporte.Columns["DEPENDENCIA"].ColumnName = "UNIDAD RESPONSABLE";
        Dt_Reporte.Columns["NO_INVENTARIO"].ColumnName = "NO. INVENTARIO";
        Dt_Reporte.Columns["NO_ECONOMICO"].ColumnName = "NO. ECONOMICO";
        Dt_Reporte.Columns["DESCRIPCION_SERVICIO"].ColumnName = "DESCRIPCION SERVICIO";
        Dt_Reporte.Columns["DIAGNOSTICO_SERVICIO"].ColumnName = "DIAGNOSTICO";
        Dt_Reporte.Columns["MECANICO"].ColumnName = "MECANICO";
        Dt_Reporte.Columns["NO_ENTRADA"].ColumnName = "NO. ENTRADA";
        Dt_Reporte.Columns["FECHA_RECEPCION"].ColumnName = "FECHA RECEPCIÓN";
        Dt_Reporte.Columns["KILOMETRAJE"].ColumnName = "KM ENTRADA";
        try
        {
            //Creamos el libro de Excel.
            CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();

            Libro.Properties.Title = "Reporte ";
            Libro.Properties.Created = DateTime.Now;
            Libro.Properties.Author = "Taller";

            //Creamos una hoja que tendrá el libro.
            CarlosAg.ExcelXmlWriter.Worksheet Hoja = Libro.Worksheets.Add("Registros");
            //Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon = Hoja.Table.Rows.Add();
            //Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");

            Estilo_Cabecera.Font.FontName = "Tahoma";
            Estilo_Cabecera.Font.Size = 10;
            Estilo_Cabecera.Font.Bold = true;
            Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Cabecera.Font.Color = "#FFFFFF";
            Estilo_Cabecera.Interior.Color = "#193d61";
            Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Alignment.WrapText = true;

            Estilo_Contenido.Font.FontName = "Tahoma";
            Estilo_Contenido.Font.Size = 8;
            Estilo_Contenido.Font.Bold = true;
            Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Contenido.Font.Color = "#000000";
            Estilo_Contenido.Interior.Color = "White";
            Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Alignment.WrapText = true;

            //Agregamos las columnas que tendrá la hoja de excel.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//Movimiento
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(85));//Fecha
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(70));//Cantidad
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Tipo de Bien
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Numero de Inventario.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Caracteristicas.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//Condiciones.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(180));//Dependencia.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(250));//Proveedor.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(250));//Responsable.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(115));//Importe.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Factura.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(250));//Observaciones.

            if (Dt_Reporte is System.Data.DataTable)
            {
                if (Dt_Reporte.Rows.Count > 0)
                {
                    foreach (System.Data.DataColumn COLUMNA in Dt_Reporte.Columns)
                    {
                        if (COLUMNA is System.Data.DataColumn)
                        {
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(COLUMNA.ColumnName, "HeaderStyle"));
                        }
                        Renglon.Height = 65;
                    }

                    foreach (System.Data.DataRow FILA in Dt_Reporte.Rows)
                    {
                        if (FILA is System.Data.DataRow)
                        {
                            Renglon = Hoja.Table.Rows.Add();

                            foreach (System.Data.DataColumn COLUMNA in Dt_Reporte.Columns)
                            {
                                if (COLUMNA is System.Data.DataColumn)
                                {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(FILA[COLUMNA.ColumnName].ToString(), DataType.String, "BodyStyle"));
                                }
                            }
                            Renglon.Height = 35;
                            Renglon.AutoFitHeight = true;
                        }
                    }
                }
            }

            //Abre el archivo de excel
            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Ruta);
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            Libro.Save(Response.OutputStream);
            Response.End();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
    ///DESCRIPCIÓN: caraga el data set fisico con el cual se genera el Reporte especificado
    ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
    ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
    ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
    ///CREO: Susana Trigueros Armenta.
    ///FECHA_CREO: 01/Mayo/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Generar_Reporte(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte)
    {
        ReportDocument Reporte = new ReportDocument();
        String File_Path = Server.MapPath("../Rpt/Servicios_Generales/" + Nombre_Reporte);
        Reporte.Load(File_Path);
        String Nombre_Reporte_Generar = "Rpt_Tal_Refacciones" + Cls_Sessiones.No_Empleado + "_" + Convert.ToString(DateTime.Now.ToString("yyyy'-'MM'-'dd'_t'HH'-'mm'-'ss")) + ".pdf";
        String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
        Ds_Reporte = Data_Set_Consulta_DB;
        Reporte.SetDataSource(Ds_Reporte);
        Reporte.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, Nombre_Reporte_Generar);
        //Reporte.SetParameterValue("No_Total_Reg", Data_Set_Consulta_DB.Tables[0].Rows.Count);
        //ExportOptions Export_Options = new ExportOptions();
        //DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
        //Disk_File_Destination_Options.DiskFileName = Server.MapPath(Ruta);
        //Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
        //Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
        //Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
        //Reporte.Export(Export_Options);
        //Mostrar_Reporte(Nombre_Reporte_Generar, "PDF");
    }
    /// *************************************************************************************
    /// NOMBRE:             Exportar_Reporte_PDF
    /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
    ///                     especificada.
    /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
    ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
    /// USUARIO CREO:       Salvador Hernandez Ramírez.
    /// FECHA CREO:         16/Mayo/2011
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    public void Exportar_Reporte_Excel(ReportDocument Reporte, String Nombre_Reporte_Generar)
    {
        if (Reporte is ReportDocument)
        {
            ExportOptions CrExportOptions = new ExportOptions();

            DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
            CrDiskFileDestinationOptions.DiskFileName = HttpContext.Current.Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
            CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;

            CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
            CrExportOptions.ExportFormatType = ExportFormatType.Excel;
            Reporte.Export(CrExportOptions);
        }
    }
    /// *************************************************************************************
    /// NOMBRE:             Exportar_Reporte_PDF
    /// DESCRIPCIÓN:        Sobrecarga de Método que guarda el reporte generado en archivo XLS en la ruta
    ///                     especificada.
    /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
    ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
    /// USUARIO CREO:       Antonio Salvador Benavides Guardado
    /// FECHA CREO:         21/Noviembre/2011
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    public void Exportar_Reporte_Excel(ReportDocument Reporte, String Ruta_Reporte_Generar, String Nombre_Reporte_Generar)
    {
        if (Reporte is ReportDocument)
        {
            ExportOptions CrExportOptions = new ExportOptions();

            DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
            CrDiskFileDestinationOptions.DiskFileName = HttpContext.Current.Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
            CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;

            CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
            CrExportOptions.ExportFormatType = ExportFormatType.Excel;
            Reporte.Export(CrExportOptions);
        }
    }
    /// *********************************************** **************************************
    /// NOMBRE:             Generar_Reporte
    /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
    ///              
    /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
    ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
    ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
    ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
    /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
    /// FECHA MODIFICO:     16/Mayo/2011
    /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
    ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
    /// *************************************************************************************
    public void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte_Crystal, String Nombre_Reporte_Generar, String Formato)
    {
        ReportDocument Reporte = new ReportDocument(); // Variable de tipo reporte.
        String Ruta = String.Empty;  // Variable que almacenará la ruta del archivo del crystal report. 
        try
        {
            Ruta = @Server.MapPath("../Rpt/Servicios_Generales/" + Ruta_Reporte_Crystal);
            Reporte.Load(Ruta);

            if (Ds_Reporte_Crystal is DataSet)
            {
                if (Ds_Reporte_Crystal.Tables.Count > 0)
                {
                    if (Formato == ".pdf")
                    {
                        Reporte.SetDataSource(Ds_Reporte_Crystal);
                        Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar);
                        Mostrar_Reporte(Nombre_Reporte_Generar, ".pdf");
                    }
                    else if (Formato == ".xls")
                    {
                        Reporte.SetDataSource(Ds_Reporte_Crystal);
                        Exportar_Reporte_Excel(Reporte, Nombre_Reporte_Generar);
                        Mostrar_Reporte(Nombre_Reporte_Generar, ".xls");
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE:             Exportar_Reporte_PDF
    /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
    ///                     especificada.
    /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
    ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    public void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte_Generar)
    {
        ExportOptions Opciones_Exportacion = new ExportOptions();
        DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
        PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

        try
        {
            if (Reporte is ReportDocument)
            {
                Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
                Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
        }
    }


    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
    /// USUARIO CREO:        Juan Alberto Hernández Negrete.
    /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:    Salvador Hernández Ramírez
    /// FECHA MODIFICO:      16-Mayo-2011
    /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
    {
        String Pagina = "../../Reporte/";

        try
        {
            if (Formato == ".pdf")
            {
                Pagina = Pagina + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            else if (Formato == ".xls")
            {
                String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    #endregion
}