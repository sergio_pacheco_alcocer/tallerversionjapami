﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Taller_Mecanico.Catalogo_Periodos_Revista.Negocio;

public partial class paginas_Taller_Mecanico_Frm_Cat_Tal_Periodos_Revista : System.Web.UI.Page
{
    #region Page_Load

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN         : Metodo que se carga cada que ocurre un PostBack de la Página
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 22/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                Configuracion_Formulario(true);
                Llenar_Grid_Listado(0);
            }
            Div_Contenedor_Msj_Error.Visible = false;
        }

    #endregion

    #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
        ///DESCRIPCIÓN         : Carga una configuracion de los controles del Formulario
        ///PARAMETROS          : 1.- Estatus. Estatus en el que se cargara la configuración de los
        ///                          controles.
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 22/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Configuracion_Formulario(Boolean Estatus)
        {
            Btn_Nuevo.Visible = true;
            Btn_Nuevo.AlternateText = "Nuevo";
            Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
            Btn_Modificar.Visible = true;
            Btn_Modificar.AlternateText = "Modificar";
            Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
            Btn_Eliminar.Visible = Estatus;
            Grid_Listado.Enabled = Estatus;
            Grid_Listado.SelectedIndex = (-1);
            Txt_Nombre.Enabled = !Estatus;
            Txt_Periodo.Enabled = !Estatus;
            Cmb_Estatus.Enabled = !Estatus;
            //Configuracion_Acceso("Frm_Cat_Pat_Com_Zonas.aspx");
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Limpiar_Catalogo
        ///DESCRIPCIÓN         : Limpia los controles del Formulario
        ///PARAMETROS          :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 22/Junio/2012 
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Limpiar_Catalogo()
        {
            Hdf_Periodo_ID.Value = "";
            Txt_Busqueda.Text = "";
            Txt_Nombre.Text = "";
            Txt_Periodo.Text = "";
            Cmb_Estatus.SelectedIndex = 0;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Listado
        ///DESCRIPCIÓN         : Llena el Listado con una consulta que puede o no
        ///                      tener Filtros.
        ///PARAMETROS          : 1.- Pagina. Pagina en la cual se mostrará el Grid_VIew
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 22/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Llenar_Grid_Listado(int Pagina)
        {
            try
            {
                Grid_Listado.SelectedIndex = (-1);
                Cls_Cat_Tal_Periodos_Revista_Negocio Negocio = new Cls_Cat_Tal_Periodos_Revista_Negocio();
                Negocio.P_Nombre = Txt_Busqueda.Text.Trim();
                Grid_Listado.Columns[1].Visible = true;
                Grid_Listado.DataSource = Negocio.Consultar_Periodos_Revista();
                Grid_Listado.PageIndex = Pagina;
                Grid_Listado.DataBind();
                Grid_Listado.Columns[1].Visible = false;
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Mostrar_Datos
        ///DESCRIPCIÓN         : Mostrar Datos.
        ///PARAMETROS          : 1.- Periodo_Seleccionado_ID. ID del periodo que se desea consultar sus detalles
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 22/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Mostrar_Datos(String Periodo_Seleccionado_ID)
        {
            Cls_Cat_Tal_Periodos_Revista_Negocio Tarjeta_Negocio = new Cls_Cat_Tal_Periodos_Revista_Negocio();
            Tarjeta_Negocio.P_Periodo_ID = Periodo_Seleccionado_ID;
            Tarjeta_Negocio = Tarjeta_Negocio.Consultar_Detalles_Periodo_Revista();
            Hdf_Periodo_ID.Value = Tarjeta_Negocio.P_Periodo_ID;
            Txt_Nombre.Text = Tarjeta_Negocio.P_Nombre;
            Txt_Periodo.Text = Tarjeta_Negocio.P_Periodo_Mes.ToString();
            Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(Tarjeta_Negocio.P_Estatus));
            //Grid_Listado.SelectedIndex = -1;
        }

        #region Validaciones

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Componentes
            ///DESCRIPCIÓN         : Hace una validacion de que haya datos en los componentes antes de hacer
            ///                      una operación.
            ///PROPIEDADES         :     
            ///CREO                : Salvador Vázquez Camacho.
            ///FECHA_CREO          : 22/Junio/2012
            ///MODIFICO            :
            ///FECHA_MODIFICO      :
            ///CAUSA_MODIFICACIÓN  :
            ///*******************************************************************************
            private Boolean Validar_Componentes()
            {
                Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                String Mensaje_Error = "";
                Boolean Validacion = true;
                //if (Hdf_Periodo_ID.Value.Trim().Length == 0)
                //{
                //    Mensaje_Error = Mensaje_Error + "+ Seleccionar el Periodo.";
                //    Validacion = false;
                //}
                if (Txt_Nombre.Text.Trim().Length == 0)
                {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Introducir el Nombre.";
                    Validacion = false;
                }
                if (Txt_Periodo.Text.Trim().Length == 0)
                {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Introducir el Periodo";
                    Validacion = false;
                }
                if (Cmb_Estatus.SelectedIndex == 0)
                {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar una opcion del Combo de Estatus.";
                    Validacion = false;
                }
                if (!Validacion)
                {
                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
                    Div_Contenedor_Msj_Error.Visible = true;
                }
                return Validacion;
            }

        #endregion

    #endregion

    #region Grids

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_PageIndexChanging
        ///DESCRIPCIÓN         : Maneja la paginación del GridView
        ///PROPIEDADES         :     
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 22/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Grid_Listado_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Grid_Listado.SelectedIndex = (-1);
                Llenar_Grid_Listado(e.NewPageIndex);
                Limpiar_Catalogo();
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_SelectedIndexChanged
        ///DESCRIPCIÓN         : Obtiene los datos de un Listado Seleccionada para mostrarlos a detalle
        ///PARAMETROS          :     
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 22/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Grid_Listado_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Grid_Listado.SelectedIndex > (-1))
                {
                    Limpiar_Catalogo();
                    String Periodo_ID = HttpUtility.HtmlDecode(Grid_Listado.SelectedRow.Cells[1].Text.Trim());
                    Mostrar_Datos(Periodo_ID);
                    System.Threading.Thread.Sleep(500);
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

    #endregion

    #region Eventos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
        ///DESCRIPCIÓN         : Deja los componentes listos para dar de Alta un registro.
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 22/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Btn_Nuevo.AlternateText.Equals("Nuevo"))
                {
                    Configuracion_Formulario(false);
                    Limpiar_Catalogo();
                    Btn_Nuevo.AlternateText = "Dar de Alta";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Salir.AlternateText = "Cancelar";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Modificar.Visible = false;
                    Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue("VIGENTE"));
                }
                else
                {
                    if (Validar_Componentes())
                    {
                        Cls_Cat_Tal_Periodos_Revista_Negocio Negocio = new Cls_Cat_Tal_Periodos_Revista_Negocio();
                        Negocio.P_Nombre = Txt_Nombre.Text;
                        Negocio.P_Periodo_ID = Hdf_Periodo_ID.Value.Trim();
                        Negocio.P_Periodo_Mes = Txt_Periodo.Text.Trim();
                        Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                        Negocio.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                        Negocio.Alta_Periodos_Revista();
                        Configuracion_Formulario(true);
                        Limpiar_Catalogo();
                        Llenar_Grid_Listado(Grid_Listado.PageIndex);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo", "alert('Alta Exitosa');", true);
                        Btn_Nuevo.AlternateText = "Nuevo";
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                        Btn_Salir.AlternateText = "Salir";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    }
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
        ///DESCRIPCIÓN         : Deja los componentes listos para hacer la modificacion.
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 22/Junio/2012 
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Modificar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Btn_Modificar.AlternateText.Equals("Modificar"))
                {
                    if (Grid_Listado.Rows.Count > 0 && Grid_Listado.SelectedIndex > (-1))
                    {
                        Configuracion_Formulario(false);
                        Btn_Modificar.AlternateText = "Actualizar";
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                        Btn_Salir.AlternateText = "Cancelar";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        Btn_Nuevo.Visible = false;
                    }
                    else
                    {
                        Lbl_Ecabezado_Mensaje.Text = "Debe seleccionar el Registro que se desea Modificar.";
                        Lbl_Mensaje_Error.Text = "";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                }
                else
                {
                    if (Validar_Componentes())
                    {
                        Cls_Cat_Tal_Periodos_Revista_Negocio Negocio = new Cls_Cat_Tal_Periodos_Revista_Negocio();
                        Negocio.P_Periodo_ID = Hdf_Periodo_ID.Value;
                        Negocio.P_Nombre = Txt_Nombre.Text.Trim();
                        Negocio.P_Periodo_Mes = Txt_Periodo.Text.Trim();
                        Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                        Negocio.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado;
                        Negocio.Modificar_Periodos_Revista();
                        Configuracion_Formulario(true);
                        Limpiar_Catalogo();
                        Llenar_Grid_Listado(Grid_Listado.PageIndex);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo", "alert('Actualización Exitosa');", true);
                        Btn_Modificar.AlternateText = "Modificar";
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                        Btn_Salir.AlternateText = "Salir";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    }
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Eliminar_Click
        ///DESCRIPCIÓN         : Elimina un registro de la Base de Datos
        ///PROPIEDADES         : 
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 22/Junio/2012 
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Eliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Grid_Listado.Rows.Count > 0 && Grid_Listado.SelectedIndex > (-1))
                {
                    Cls_Cat_Tal_Periodos_Revista_Negocio Negocio = new Cls_Cat_Tal_Periodos_Revista_Negocio();
                    Negocio.P_Periodo_ID = Hdf_Periodo_ID.Value;
                    Negocio.Eliminar_Periodos_Revista();
                    Grid_Listado.SelectedIndex = (-1);
                    Llenar_Grid_Listado(Grid_Listado.PageIndex);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo", "alert('Eliminación Exitosa');", true);
                    Limpiar_Catalogo();
                }
                else
                {
                    Lbl_Ecabezado_Mensaje.Text = "Debe seleccionar el Registro que se desea Eliminar.";
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN         : Cancela la operación que esta en proceso (Alta o Actualizar) o Sale del Formulario.
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 22/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, EventArgs e)
        {
            if (Btn_Salir.AlternateText.Equals("Salir"))
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Configuracion_Formulario(true);
                Limpiar_Catalogo();
                Btn_Salir.AlternateText = "Salir";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Click
        ///DESCRIPCIÓN         : Busca el Vehiculo por el No. Inventario.
        ///PARAMETROS          :     
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 22/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Busqueda_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Txt_Busqueda.Text.Trim().Length == 0)
                {
                    Limpiar_Catalogo();
                    Lbl_Ecabezado_Mensaje.Text = "Es necesario introducir el Nombre para buscar";
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
                Llenar_Grid_Listado(0);
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                Lbl_Mensaje_Error.Text = "Se presento una Excepcion al Buscar [" + Ex.Message + "]";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }


    #endregion

}
