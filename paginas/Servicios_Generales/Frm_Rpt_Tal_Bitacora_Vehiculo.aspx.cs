﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using CarlosAg.ExcelXmlWriter;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Revista_Mecanica.Negocio;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;
using JAPAMI.Control_Patrimonial_Operacion_Vehiculos.Negocio;

public partial class paginas_Taller_Mecanico_Frm_Rpt_Tal_Bitacora_Vehiculo : System.Web.UI.Page
{
    #region Page_Load

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN         : Metodo que se carga cada que ocurre un PostBack de la Página
        ///PARAMETROS          :     
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 03/Julio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                Llenar_Combo_Unidades_Responsables();
            }
            Div_Contenedor_Msj_Error.Visible = false;
        }

    #endregion

    #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario_Vehiculo
        ///DESCRIPCIÓN         : Limpia los campos del Formulario [Seccion de Vehiculos].
        ///PARAMETROS          : 
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 03/Julio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///******************************************************************************* 
        private void Limpiar_Formulario_Vehiculo()
        {
            Hdf_Vehiculo_ID.Value = "";
            Txt_No_Inventario.Text = "";
            Txt_No_Economico.Text = "";
            Txt_Unidad_Responsable.Text = "";
            Txt_Datos_Vehiculo.Text = "";
            Txt_Placas.Text = "";
            Txt_Anio.Text = "";
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Vehiculo
        ///DESCRIPCIÓN         : Se cargan los Datos del Vehiculo Seleccionado.
        ///PARAMETROS          : 
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 03/Julio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///******************************************************************************* 
        private void Cargar_Datos_Vehiculo(String Vehiculo, String Tipo_Busqueda)
        {
            Limpiar_Formulario_Vehiculo();
            Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            switch (Tipo_Busqueda)
            {
                case "NO_INVENTARIO":
                    Consulta_Negocio.P_No_Inventario = Vehiculo;
                    break;
                case "IDENTIFICADOR":
                    Consulta_Negocio.P_Vehiculo_ID = Vehiculo;
                    break;
                default: break;
            }
            DataTable Dt_Vehiculo = Consulta_Negocio.Consultar_Vehiculos();
            if (Dt_Vehiculo.Rows.Count > 0)
            {
                Hdf_Vehiculo_ID.Value = Dt_Vehiculo.Rows[0]["VEHICULO_ID"].ToString().Trim();
                Txt_No_Inventario.Text = Dt_Vehiculo.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                Txt_No_Economico.Text = Dt_Vehiculo.Rows[0]["NO_ECONOMICO"].ToString().Trim();
                Txt_Datos_Vehiculo.Text = Dt_Vehiculo.Rows[0]["VEHICULO_DESCRIPCION"].ToString().Trim();
                Txt_Placas.Text = Dt_Vehiculo.Rows[0]["PLACAS"].ToString().Trim();
                Txt_Anio.Text = Dt_Vehiculo.Rows[0]["ANIO"].ToString().Trim();
                if (!String.IsNullOrEmpty(Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString()))
                {
                    Cls_Cat_Dependencias_Negocio Dep_Negocio = new Cls_Cat_Dependencias_Negocio();
                    Dep_Negocio.P_Dependencia_ID = Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString().Trim();
                    DataTable Dt_Dep = Dep_Negocio.Consulta_Dependencias();
                    if (Dt_Dep.Rows.Count > 0)
                    {
                        Txt_Unidad_Responsable.Text = Dt_Dep.Rows[0]["CLAVE_NOMBRE"].ToString();
                    }
                }
                Grid_Listado_Solicitudes.PageIndex = 0;
                Llenar_Grid_Listado_Solicitudes();
                Grid_Listado_Revista.PageIndex = 0;
                Llenar_Grid_Listado_Revista();
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = "El Vehículo no se encontro.";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Unidades_Responsables
        ///DESCRIPCIÓN         : Se llena el Combo de las Unidades Responsables.
        ///PARAMETROS          : 
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 03/Julio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///******************************************************************************* 
        private void Llenar_Combo_Unidades_Responsables()
        {
            Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            Negocio.P_Estatus = "ACTIVO";
            DataTable Dt_Dependencias = Negocio.Consultar_Unidades_Responsables();
            Cmb_Busqueda_Vehiculo_Dependencias.DataSource = Dt_Dependencias;
            Cmb_Busqueda_Vehiculo_Dependencias.DataTextField = "CLAVE_NOMBRE";
            Cmb_Busqueda_Vehiculo_Dependencias.DataValueField = "DEPENDENCIA_ID";
            Cmb_Busqueda_Vehiculo_Dependencias.DataBind();
            Cmb_Busqueda_Vehiculo_Dependencias.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Busqueda_Vehiculos
        ///DESCRIPCIÓN         : Llena el Grid con los vehiculos que cumplan el filtro
        ///PARAMETROS          : 
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 03/Julio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Llenar_Grid_Busqueda_Vehiculos()
        {
            Grid_Listado_Busqueda_Vehiculo.SelectedIndex = (-1);
            Grid_Listado_Busqueda_Vehiculo.Columns[1].Visible = true;
            Cls_Ope_Tal_Consultas_Generales_Negocio Vehiculo_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            if (Txt_Busqueda_Vehiculo_Numero_Inventario.Text.Trim().Length > 0) { Vehiculo_Negocio.P_No_Inventario = Txt_Busqueda_Vehiculo_Numero_Inventario.Text.Trim(); }
            if (Cmb_Busqueda_Vehiculo_Dependencias.SelectedIndex > 0) { Vehiculo_Negocio.P_Dependencia_ID = Cmb_Busqueda_Vehiculo_Dependencias.SelectedItem.Value; }
            Grid_Listado_Busqueda_Vehiculo.DataSource = Vehiculo_Negocio.Consultar_Vehiculos();
            Grid_Listado_Busqueda_Vehiculo.DataBind();
            Grid_Listado_Busqueda_Vehiculo.Columns[1].Visible = false;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Listado_Solicitudes
        ///DESCRIPCIÓN         : Llena el Grid con las solicitudes de un vehiculo
        ///PARAMETROS          : 
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 03/Julio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Llenar_Grid_Listado_Solicitudes()
        {
            Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitudes_Negocio = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
            Solicitudes_Negocio.P_Bien_ID = Hdf_Vehiculo_ID.Value;
            Solicitudes_Negocio.P_Tipo_Servicio = "SERVICIO_CORRECTIVO', 'SERVICIO_PREVENTIVO";
            DataTable Dt_Solicitudes = Solicitudes_Negocio.Consultar_Listado_Solicitudes_Servicio();
            Dt_Solicitudes.DefaultView.Sort = "NO_SOLICITUD";
            if (Dt_Solicitudes.Rows.Count > 0)
            {
                Grid_Listado_Solicitudes.DataSource = Dt_Solicitudes;
                Grid_Listado_Solicitudes.DataBind();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Listado_Solicitudes
        ///DESCRIPCIÓN         : Llena el Grid con las solicitudes de un vehiculo
        ///PARAMETROS          : 
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 03/Julio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Llenar_Grid_Listado_Revista()
        {
            Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitudes_Negocio = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
            Solicitudes_Negocio.P_Bien_ID = Hdf_Vehiculo_ID.Value;
            Solicitudes_Negocio.P_Tipo_Servicio = "REVISTA_MECANICA";
            DataTable Dt_Solicitudes = Solicitudes_Negocio.Consultar_Listado_Solicitudes_Servicio();
            Dt_Solicitudes.DefaultView.Sort = "NO_SOLICITUD";
            if (Dt_Solicitudes.Rows.Count > 0)
            {
                Grid_Listado_Revista.DataSource = Dt_Solicitudes;
                Grid_Listado_Revista.DataBind();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Pasar_DataSet_A_Excel
        ///DESCRIPCION         : Pasa DataTable a Excel. 
        ///PARÁMETROS          : Dt_Reporte.- DataTable que se pasara a excel. 
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 29/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        public void Pasar_DataSet_A_Excel(DataSet Ds_Reportes)
        {
            String Ruta = "Bitacora de Vehiculo [" + (String.Format("{0:dd_MMM_yyyy}", DateTime.Now)) + "].xls";//Variable que almacenara el nombre del archivo. 

            try
            {
                //Creamos el libro de Excel.
                CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();

                Libro.Properties.Title = "Reporte de Tarjeta de Gasolina";
                Libro.Properties.Created = DateTime.Now;
                Libro.Properties.Author = "Patrimonio";

                //Creamos una hoja que tendrá el libro.
                CarlosAg.ExcelXmlWriter.Worksheet Hoja = Libro.Worksheets.Add("Registros");
                //Agregamos un renglón a la hoja de excel.
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon = Hoja.Table.Rows.Add();
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");
                //Creamos el estilo clave para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Titulo = Libro.Styles.Add("Titulo");

                Estilo_Cabecera.Font.FontName = "Tahoma";
                Estilo_Cabecera.Font.Size = 10;
                Estilo_Cabecera.Font.Bold = true;
                Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Cabecera.Font.Color = "#FFFFFF";
                Estilo_Cabecera.Interior.Color = "#193d61";
                Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Alignment.WrapText = true;

                Estilo_Contenido.Font.FontName = "Tahoma";
                Estilo_Contenido.Font.Size = 8;
                Estilo_Contenido.Font.Bold = true;
                Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Contenido.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Contenido.Font.Color = "#000000";
                Estilo_Contenido.Interior.Color = "White";
                Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Alignment.WrapText = true;

                Estilo_Titulo.Font.FontName = "Tahoma";
                Estilo_Titulo.Font.Size = 10;
                Estilo_Titulo.Font.Bold = true;
                Estilo_Titulo.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Titulo.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Titulo.Font.Color = "#FFFFFF";
                Estilo_Titulo.Interior.Color = "Black";
                Estilo_Titulo.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Titulo.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Titulo.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Titulo.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Titulo.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                Estilo_Titulo.Alignment.WrapText = true;

                //Agregamos las columnas que tendrá la hoja de excel.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));

                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("No. Inventario", "Titulo"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Txt_No_Inventario.Text, "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("No. Economico", "Titulo"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Txt_No_Economico.Text, "BodyStyle"));
                Renglon = Hoja.Table.Rows.Add();

                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Dependencia", "Titulo"));
                WorksheetCell cell = Renglon.Cells.Add(Txt_Unidad_Responsable.Text);
                cell.MergeAcross = 3;            // Merge two cells together
                cell.StyleID = "BodyStyle";
                Renglon = Hoja.Table.Rows.Add();

                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Datos Vehiculo", "Titulo"));
                cell = Renglon.Cells.Add(Txt_Datos_Vehiculo.Text);
                cell.MergeAcross = 3;            // Merge two cells together
                cell.StyleID = "BodyStyle";
                Renglon = Hoja.Table.Rows.Add();

                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Placas", "Titulo"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Txt_Placas.Text, "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("Año", "Titulo"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Txt_Anio.Text, "BodyStyle"));
                Renglon = Hoja.Table.Rows.Add();
                Renglon = Hoja.Table.Rows.Add();

                foreach (DataTable Dt_Reporte in Ds_Reportes.Tables)
                {
                    if (Dt_Reporte is System.Data.DataTable)
                    {
                        if (Dt_Reporte.Rows.Count > 0)
                        {
                            Renglon = Hoja.Table.Rows.Add();
                            cell = Renglon.Cells.Add(Dt_Reporte.TableName.ToString());
                            cell.MergeAcross = 4;            // Merge two cells together
                            cell.StyleID = "Titulo";
                            Renglon = Hoja.Table.Rows.Add();
                            foreach (DataColumn COLUMNA in Dt_Reporte.Columns)
                            {
                                if (COLUMNA is System.Data.DataColumn)
                                {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(COLUMNA.ColumnName, "HeaderStyle"));
                                }
                                Renglon.Height = 23;
                            }

                            foreach (DataRow FILA in Dt_Reporte.Rows)
                            {
                                if (FILA is System.Data.DataRow)
                                {
                                    Renglon = Hoja.Table.Rows.Add();

                                    foreach (DataColumn COLUMNA in Dt_Reporte.Columns)
                                    {
                                        if (COLUMNA is System.Data.DataColumn)
                                        {
                                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(FILA[COLUMNA.ColumnName].ToString(), DataType.String, "BodyStyle"));
                                        }
                                    }
                                    Renglon.Height = 15;
                                    Renglon.AutoFitHeight = true;
                                }
                            }
                            Renglon = Hoja.Table.Rows.Add();
                            Renglon = Hoja.Table.Rows.Add();
                        }
                    }
                }
                //Abre el archivo de excel
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + Ruta);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;
                Libro.Save(Response.OutputStream);
                Response.End();
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
        ///DESCRIPCIÓN         : caraga el data set fisico con el cual se genera el Reporte especificado
        ///PARAMETROS          :  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
        ///                       2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
        ///                       3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 29/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Generar_Reporte(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte)
        {
            ReportDocument Reporte = new ReportDocument();
            String File_Path = Server.MapPath("../Rpt/Servicios_Generales/" + Nombre_Reporte);
            Reporte.Load(File_Path);
            String Nombre_Reporte_Generar = "Rpt_Tal_Bitacota_Vehiculo_" + Cls_Sessiones.No_Empleado + "_" + Convert.ToString(DateTime.Now.ToString("yyyy'-'MM'-'dd'_t'HH'-'mm'-'ss")) + ".pdf";
            String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
            Ds_Reporte = Data_Set_Consulta_DB;
            Reporte.SetDataSource(Ds_Reporte);
            ExportOptions Export_Options = new ExportOptions();
            DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
            Disk_File_Destination_Options.DiskFileName = Server.MapPath(Ruta);
            Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
            Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
            Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
            Reporte.Export(Export_Options);
            Mostrar_Reporte(Nombre_Reporte_Generar, "PDF");            
        }

        /// *************************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Mostrar_Reporte
        ///DESCRIPCIÓN         : Muestra el reporte en pantalla.
        ///PARÁMETROS          : Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
        ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 29/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        /// *************************************************************************************
        protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
        {
            String Pagina = "../../Reporte/";
            try
            {
                Pagina = Pagina + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
        }

    #endregion

    #region Grids

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Vehiculos_PageIndexChanging
        ///DESCRIPCIÓN         : Maneja el evento de cambio de Página del GridView de Busqueda
        ///                      de vehiculos.
        ///PARAMETROS          : 
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 03/Julio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Grid_Busqueda_Vehiculos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Grid_Listado_Busqueda_Vehiculo.PageIndex = e.NewPageIndex;
                Llenar_Grid_Busqueda_Vehiculos();
                //MPE_Listado_Vehiculos.Show();
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Vehiculos_SelectedIndexChanged
        ///DESCRIPCIÓN         : Obtiene los datos de un Listado Seleccionada para mostrarlos a detalle
        ///PARAMETROS          : 
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 03/Julio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Grid_Busqueda_Vehiculos_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Grid_Listado_Busqueda_Vehiculo.SelectedIndex > (-1))
                {
                    Limpiar_Formulario_Vehiculo();
                    String Vehiculo_ID = HttpUtility.HtmlDecode(Grid_Listado_Busqueda_Vehiculo.SelectedRow.Cells[1].Text.Trim());
                    Cargar_Datos_Vehiculo(Vehiculo_ID, "IDENTIFICADOR");
                    System.Threading.Thread.Sleep(500);
                    MPE_Listado_Vehiculos.Hide();
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Solicitudes_PageIndexChanging
        ///DESCRIPCIÓN         :  
        ///PARAMETROS          : 
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 03/Julio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Grid_Listado_Solicitudes_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            Grid_Listado_Solicitudes.PageIndex = e.NewPageIndex;
            Llenar_Grid_Listado_Solicitudes();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Solicitudes_PageIndexChanging
        ///DESCRIPCIÓN         :  
        ///PARAMETROS          : 
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 03/Julio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Grid_Listado_Revista_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Grid_Listado_Revista.PageIndex = e.NewPageIndex;
            Llenar_Grid_Listado_Revista();
        }
        

    #endregion

    #region Eventos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Directa_Vehiculo_Click
        ///DESCRIPCIÓN         : Busca el Vehiculo por el No. Inventario.
        ///PARAMETROS          : 
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 03/Julio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Busqueda_Directa_Vehiculo_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Txt_No_Inventario.Text.Trim().Length > 0)
                {
                    Cargar_Datos_Vehiculo(Txt_No_Inventario.Text.Trim(), "NO_INVENTARIO");
                }
                else
                {
                    Limpiar_Formulario_Vehiculo();
                    Lbl_Ecabezado_Mensaje.Text = "Es necesario introducir el No. de Inventario del Vehículo.";
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                Lbl_Mensaje_Error.Text = "Se presento una Excepcion al Buscar [" + Ex.Message + "]";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Avanzada_Vehiculo_Click
        ///DESCRIPCIÓN         : Busqueda Avanzada del Vehiculo
        ///PARAMETROS          : 
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 03/Julio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Busqueda_Avanzada_Vehiculo_Click(object sender, ImageClickEventArgs e)
        {
            Grid_Listado_Busqueda_Vehiculo.PageIndex = 0;
            Grid_Listado_Busqueda_Vehiculo.SelectedIndex = (-1);
            MPE_Listado_Vehiculos.Show();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Limpiar_Click
        ///DESCRIPCIÓN         : Limpia los componentes del Form
        ///PARAMETROS          : 
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 03/Julio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Limpiar_Click(object sender, ImageClickEventArgs e)
        {
            Limpiar_Formulario_Vehiculo();
            Grid_Listado_Solicitudes.DataSource = new DataTable();
            Grid_Listado_Solicitudes.DataBind();
            Grid_Listado_Revista.DataSource = new DataTable();
            Grid_Listado_Revista.DataBind();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Reporte_Execel_Click
        ///DESCRIPCIÓN         : Genera un reporte de tipo Excel
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 03/Julio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  : 
        ///*******************************************************************************
        protected void Btn_Reporte_Execel_Click(object sender, ImageClickEventArgs e)
        {
            if (Hdf_Vehiculo_ID.Value.ToString().Trim() != "")
            {
                try
                {
                    DataSet Ds_Consultas = new DataSet();
                    Cls_Ope_Tal_Solicitud_Servicio_Negocio Negocio = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                    Negocio.P_Bien_ID = Hdf_Vehiculo_ID.Value;
                    Negocio.P_Tipo_Servicio = "SERVICIO_CORRECTIVO', 'SERVICIO_PREVENTIVO";

                    DataTable Dt_Servicios = Negocio.Consultar_Listado_Solicitudes_Servicio();
                    Dt_Servicios.TableName = "Solicitudes de Servicios";
                    Dt_Servicios.Columns["NO_SOLICITUD"].ColumnName = "No. Solicitud";
                    Dt_Servicios.Columns["DESCRIPCION_SERVICIO"].ColumnName = "Descripcion";
                    Dt_Servicios.Columns["FECHA_ELABORACION"].ColumnName = "Fecha de Elaboracion";
                    Dt_Servicios.Columns["FECHA_RECEPCION_REAL"].ColumnName = "Fecha de Recepcion";
                    Dt_Servicios.Columns["ESTATUS"].ColumnName = "Estatus";
                    Dt_Servicios.Columns.RemoveAt(9);
                    Dt_Servicios.Columns.RemoveAt(7);
                    Dt_Servicios.Columns.RemoveAt(4);
                    Dt_Servicios.Columns.RemoveAt(2);
                    Dt_Servicios.Columns.RemoveAt(1);

                    Negocio.P_Tipo_Servicio = "REVISTA_MECANICA";

                    DataTable Dt_Revista = Negocio.Consultar_Listado_Solicitudes_Servicio();
                    Dt_Revista.TableName = "Solicitudes de Revista";
                    Dt_Revista.Columns["NO_SOLICITUD"].ColumnName = "No. Solicitud";
                    Dt_Revista.Columns["DESCRIPCION_SERVICIO"].ColumnName = "Descripcion";
                    Dt_Revista.Columns["FECHA_ELABORACION"].ColumnName = "Fecha de Elaboracion";
                    Dt_Revista.Columns["FECHA_RECEPCION_REAL"].ColumnName = "Fecha de Recepcion";
                    Dt_Revista.Columns["ESTATUS"].ColumnName = "Estatus";
                    Dt_Revista.Columns.RemoveAt(9);
                    Dt_Revista.Columns.RemoveAt(7);
                    Dt_Revista.Columns.RemoveAt(4);
                    Dt_Revista.Columns.RemoveAt(2);
                    Dt_Revista.Columns.RemoveAt(1);

                    Ds_Consultas.Tables.Add(Dt_Servicios.Copy());
                    Ds_Consultas.Tables.Add(Dt_Revista.Copy());
                    Pasar_DataSet_A_Excel(Ds_Consultas);
                }
                catch (Exception Ex)
                {
                    Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = "Es necesario introducir la clave para generar el reporte.";
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Reporte_PDF_Click
        ///DESCRIPCIÓN         : Genera un reporte de tipo PDF
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 03/Julio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  : 
        ///*******************************************************************************
        protected void Btn_Reporte_PDF_Click(object sender, ImageClickEventArgs e)
        {
            if (Hdf_Vehiculo_ID.Value.ToString().Trim() != "")
            {
                try
                {
                    //Dataset esqueleto del Reporte
                    Ds_Rpt_Tal_Bitacora_Vehiculo Ds_Reporte = new Ds_Rpt_Tal_Bitacora_Vehiculo();

                    Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                    Consulta_Negocio.P_Vehiculo_ID = Hdf_Vehiculo_ID.Value;

                    DataTable Dt_Generales = Consulta_Negocio.Consultar_Vehiculos();
                    if (Dt_Generales.Rows.Count > 0)
                    {
                        Dt_Generales.Columns["VEHICULO_DESCRIPCION"].ColumnName = "DATOS_VEHICULO";
                        if (!String.IsNullOrEmpty(Dt_Generales.Rows[0]["DEPENDENCIA_ID"].ToString()))
                        {
                            Cls_Cat_Dependencias_Negocio Dep_Negocio = new Cls_Cat_Dependencias_Negocio();
                            Dep_Negocio.P_Dependencia_ID = Dt_Generales.Rows[0]["DEPENDENCIA_ID"].ToString().Trim();
                            DataTable Dt_Dep = Dep_Negocio.Consulta_Dependencias();
                            if (Dt_Dep.Rows.Count > 0)
                            {
                                Dt_Generales.Columns.Add(new DataColumn("UNIDAD_RESPONSABLE"));
                                Dt_Generales.Rows[0]["UNIDAD_RESPONSABLE"] = Dt_Dep.Rows[0]["CLAVE_NOMBRE"].ToString();
                            }
                        }
                    }
                    Dt_Generales.TableName = "DT_GENERALES";



                    Cls_Ope_Tal_Solicitud_Servicio_Negocio Negocio = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                    Negocio.P_Bien_ID = Hdf_Vehiculo_ID.Value;
                    Negocio.P_Tipo_Servicio = "SERVICIO_CORRECTIVO', 'SERVICIO_PREVENTIVO";

                    DataTable Dt_Servicios = Negocio.Consultar_Listado_Solicitudes_Servicio();
                    Dt_Servicios.TableName = "DT_SOLICITUDES";
                    Dt_Servicios.DefaultView.Sort = "NO_SOLICITUD";
                    Dt_Servicios.Columns["DESCRIPCION_SERVICIO"].ColumnName = "DESCRIPCION";
                    Dt_Servicios.Columns["FECHA_RECEPCION_REAL"].ColumnName = "FECHA_RECEPCION";
                    Dt_Servicios.Columns.RemoveAt(9);
                    Dt_Servicios.Columns.RemoveAt(7);
                    Dt_Servicios.Columns.RemoveAt(4);
                    Dt_Servicios.Columns.RemoveAt(2);
                    Dt_Servicios.Columns.RemoveAt(1);

                    Negocio.P_Tipo_Servicio = "REVISTA_MECANICA";

                    DataTable Dt_Revista = Negocio.Consultar_Listado_Solicitudes_Servicio();
                    Dt_Revista.TableName = "DT_REVISTA";
                    Dt_Revista.DefaultView.Sort = "NO_SOLICITUD";
                    Dt_Revista.Columns["DESCRIPCION_SERVICIO"].ColumnName = "DESCRIPCION";
                    Dt_Revista.Columns["FECHA_RECEPCION_REAL"].ColumnName = "FECHA_RECEPCION";
                    Dt_Revista.Columns.RemoveAt(9);
                    Dt_Revista.Columns.RemoveAt(7);
                    Dt_Revista.Columns.RemoveAt(4);
                    Dt_Revista.Columns.RemoveAt(2);
                    Dt_Revista.Columns.RemoveAt(1);

                    DataSet Ds_Consulta = new DataSet();
                    Ds_Consulta.Tables.Add(Dt_Generales.Copy());
                    Ds_Consulta.Tables.Add(Dt_Servicios.Copy());
                    Ds_Consulta.Tables.Add(Dt_Revista.Copy());
                    Ds_Consulta.Tables.Add(new Cls_Tal_Parametros_Negocio().Obtener_Tabla_Reporte(this.Server).Copy());

                    //Generar y lanzar el reporte
                    String Ruta_Reporte_Crystal = "Rpt_Tal_Bitacora_Vehiculo.rpt";
                    Generar_Reporte(Ds_Consulta, Ds_Reporte, Ruta_Reporte_Crystal);
                }
                catch (Exception Ex)
                {
                    Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = "Es necesario introducir la clave para generar el reporte.";
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Datos_Vehiculo_Click
        ///DESCRIPCIÓN: Ejecuta la Busqueda de los Vehiculos por datos generales.
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 15/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Buscar_Datos_Vehiculo_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Session["FILTRO_BUSQUEDA"] = "DATOS_GENERALES";
                Llenar_Grid_Listado_Bienes_Vehiculos(0);
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Listado_Vehiculos
        ///DESCRIPCIÓN          : Llena el Grid de los Vehiculos.
        ///PARAMETROS           : 
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 15/Mayo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Llenar_Grid_Listado_Bienes_Vehiculos(Int32 Pagina)
        {
            try
            {
                Grid_Listado_Busqueda_Vehiculo.Columns[1].Visible = true;
                Cls_Ope_Pat_Com_Vehiculos_Negocio Vehiculos = new Cls_Ope_Pat_Com_Vehiculos_Negocio();
                Vehiculos.P_Tipo_DataTable = "VEHICULOS";
                if (Session["FILTRO_BUSQUEDA"] != null)
                {
                    Vehiculos.P_Tipo_Filtro_Busqueda = Session["FILTRO_BUSQUEDA"].ToString();
                    Vehiculos.P_Estatus = "VIGENTE";
                    if (Session["FILTRO_BUSQUEDA"].ToString().Trim().Equals("DATOS_GENERALES"))
                    {
                        if (Txt_Busqueda_Vehiculo_Numero_Inventario.Text.Trim().Length > 0) { Vehiculos.P_Numero_Inventario = Convert.ToInt64(Txt_Busqueda_Vehiculo_Numero_Inventario.Text.Trim()); }
                        if (Txt_Busqueda_Vehiculo_Numero_Economico.Text.Trim().Length > 0) { Vehiculos.P_Numero_Economico_ = Txt_Busqueda_Vehiculo_Numero_Economico.Text.Trim(); }
                        if (Txt_Busqueda_Vehiculo_Anio_Fabricacion.Text.Trim().Length > 0) { Vehiculos.P_Anio_Fabricacion = Convert.ToInt32(Txt_Busqueda_Vehiculo_Anio_Fabricacion.Text.Trim()); }
                        Vehiculos.P_Modelo_ID = Txt_Busqueda_Vehiculo_Modelo.Text.Trim();
                        if (Cmb_Busqueda_Vehiculo_Marca.SelectedIndex > 0)
                        {
                            Vehiculos.P_Marca_ID = Cmb_Busqueda_Vehiculo_Marca.SelectedItem.Value.Trim();
                        }
                        if (Cmb_Busqueda_Vehiculo_Color.SelectedIndex > 0)
                        {
                            Vehiculos.P_Color_ID = Cmb_Busqueda_Vehiculo_Color.SelectedItem.Value.Trim();
                        }
                        if (Cmb_Busqueda_Vehiculo_Dependencias.SelectedIndex > 0)
                        {
                            Vehiculos.P_Dependencia_ID = Cmb_Busqueda_Vehiculo_Dependencias.SelectedItem.Value.Trim();
                        }
                    }
                    else if (Session["FILTRO_BUSQUEDA"].ToString().Trim().Equals("RESGUARDANTES"))
                    {
                        Vehiculos.P_RFC_Resguardante = Txt_Busqueda_Vehiculo_RFC_Resguardante.Text.Trim();
                        Vehiculos.P_No_Empleado = Txt_Busqueda_Vehiculo_No_Empleado.Text.Trim();
                        if (Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.SelectedIndex > 0)
                        {
                            Vehiculos.P_Dependencia_ID = Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.SelectedItem.Value.Trim();
                        }
                        if (Cmb_Busqueda_Vehiculo_Nombre_Resguardante.SelectedIndex > 0)
                        {
                            Vehiculos.P_Resguardante_ID = Cmb_Busqueda_Vehiculo_Nombre_Resguardante.SelectedItem.Value.Trim();
                        }
                    }
                }
                Grid_Listado_Busqueda_Vehiculo.DataSource = Vehiculos.Consultar_DataTable();
                Grid_Listado_Busqueda_Vehiculo.PageIndex = Pagina;
                Grid_Listado_Busqueda_Vehiculo.DataBind();
                Grid_Listado_Busqueda_Vehiculo.Columns[1].Visible = false;
                MPE_Listado_Vehiculos.Show();
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Limpiar_Filtros_Buscar_Datos_Vehiculo_Click
        ///DESCRIPCIÓN          : Hace la limpieza de los campos.
        ///PARAMETROS           : 
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 15/Mayo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Limpiar_Filtros_Buscar_Datos_Vehiculo_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Txt_Busqueda_Vehiculo_Numero_Inventario.Text = "";
                Txt_Busqueda_Vehiculo_Numero_Economico.Text = "";
                Txt_Busqueda_Vehiculo_Modelo.Text = "";
                Cmb_Busqueda_Vehiculo_Marca.SelectedIndex = 0;
                Txt_Busqueda_Vehiculo_Anio_Fabricacion.Text = "";
                Cmb_Busqueda_Vehiculo_Color.SelectedIndex = 0;
                Cmb_Busqueda_Vehiculo_Dependencias.SelectedIndex = 0;
                MPE_Listado_Vehiculos.Show();
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = "Excepción.";
                Lbl_Mensaje_Error.Text = "Ex:['" + Ex.Message + "']";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias_SelectedIndexChanged
        ///DESCRIPCIÓN          : Maneja el evento del Combo de Dependencias.
        ///PARAMETROS           : 
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 15/Mayo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.SelectedIndex > 0)
            {
                Llenar_Combo_Empleados(Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.SelectedItem.Value.Trim(), ref Cmb_Busqueda_Vehiculo_Nombre_Resguardante);
            }
            else
            {
                Llenar_Combo_Empleados(null, ref Cmb_Busqueda_Vehiculo_Nombre_Resguardante);
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Empleados
        ///DESCRIPCIÓN          : Llena el Combo con los empleados de una dependencia.
        ///PARAMETROS           : 
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 15/Mayo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Llenar_Combo_Empleados(String Dependencia_ID, ref DropDownList Combo_Empleados)
        {
            Combo_Empleados.Items.Clear();
            if (Dependencia_ID != null && Dependencia_ID.Trim().Length > 0)
            {
                Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                Negocio.P_Estatus = "ACTIVO";
                Negocio.P_Dependencia_ID = Dependencia_ID.Trim();
                DataTable Dt_Datos = Negocio.Consultar_Empleados();
                Combo_Empleados.DataSource = Dt_Datos;
                Combo_Empleados.DataValueField = "EMPLEADO_ID";
                Combo_Empleados.DataTextField = "NOMBRE";
                Combo_Empleados.DataBind();
            }
            Combo_Empleados.Items.Insert(0, new ListItem("< TODOS >", ""));
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Resguardante_Vehiculo_Click
        ///DESCRIPCIÓN: Ejecuta la Busqueda de los vehiculos por resguardos.
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 15/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Buscar_Resguardante_Vehiculo_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Session["FILTRO_BUSQUEDA"] = "RESGUARDANTES";
                Llenar_Grid_Listado_Bienes_Vehiculos(0);
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Limpiar_Filtros_Buscar_Resguardante_Vehiculo_Click
        ///DESCRIPCIÓN          : Hace la limpieza de los campos.
        ///PARAMETROS           : 
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 15/Mayo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Limpiar_Filtros_Buscar_Resguardante_Vehiculo_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Txt_Busqueda_Vehiculo_RFC_Resguardante.Text = "";
                Txt_Busqueda_Vehiculo_No_Empleado.Text = "";
                Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias.SelectedIndex = 0;
                Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias_SelectedIndexChanged(Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias, null);
                Cmb_Busqueda_Vehiculo_Nombre_Resguardante.SelectedIndex = 0;
                MPE_Listado_Vehiculos.Show();
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = "Excepción.";
                Lbl_Mensaje_Error.Text = "Ex:['" + Ex.Message + "']";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Productos_PageIndexChanging
        ///DESCRIPCIÓN: Maneja la paginación del GridView de los Vehiculos
        ///PROPIEDADES:     
        ///CREO : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO: 15/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Busqueda_Vehiculo_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Llenar_Grid_Listado_Bienes_Vehiculos(e.NewPageIndex);
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Busqueda_Vehiculo_SelectedIndexChanged
        ///DESCRIPCIÓN: Maneja el evento de cambio de Seleccion del GridView de Vehiculos del
        ///             Modal de Busqueda.
        ///PROPIEDADES:     
        ///CREO : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO: 15/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Busqueda_Vehiculo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Grid_Listado_Busqueda_Vehiculo.SelectedIndex > (-1))
                {
                    String Vehiculo_ID = Grid_Listado_Busqueda_Vehiculo.SelectedRow.Cells[1].Text.Trim();
                    Cargar_Datos_Vehiculo(Vehiculo_ID, "IDENTIFICADOR");
                }
                MPE_Listado_Vehiculos.Hide();
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Vehiculos_Click
        ///DESCRIPCIÓN         : Realiza la busqueda avanzada de un vehiculo
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 03/Julio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  : 
        ///*******************************************************************************
        protected void Btn_Busqueda_Vehiculos_Click(object sender, EventArgs e)
        {
            try
            {
                Grid_Listado_Busqueda_Vehiculo.PageIndex = 0;
                Llenar_Grid_Busqueda_Vehiculos();
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                Lbl_Mensaje_Error.Text = "Se presento una Excepcion al Buscar [" + Ex.Message + "]";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

    #endregion
}
