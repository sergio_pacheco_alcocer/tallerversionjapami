﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Taller_Mecanico.Requisiciones.Negocio;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Areas.Negocios;
using JAPAMI.Taller_Mecanico.Administrar_Requisiciones.Negocio;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using JAPAMI.Taller_Mecanico.Impresion_Requisiciones.Negocio;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;
using JAPAMI.SAP_Partidas_Especificas.Negocio;
using JAPAMI.Catalogo_Compras_Proyectos_Programas.Negocio;
using JAPAMI.Catalogo_SAP_Fuente_Financiamiento.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio;
using JAPAMI.Taller_Mecanico.Reporte_Solicitud_Servicio.Negocio;

public partial class paginas_Taller_Mecanico_Frm_Ope_Tal_Elaborar_Requisicion : System.Web.UI.Page {

    #region VARIABLES / CONSTANTES

        //objeto de la clase de negocio de dependencias para acceder a la clase de datos y realizar conexion
        private Cls_Cat_Dependencias_Negocio Dependencia_Negocio;

        //objeto de la clase de negocio de Requisicion para acceder a la clase de datos y realizar conexion
        private Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio;

        //objeto en donde se guarda un id de producto o servicio para siempre tener referencia
        private static String PS_ID = "PS_ID";
        private static String Estatus = "ESTATUS";
        private Cls_Ope_Tal_Administrar_Requisiciones_Negocio Administrar_Requisicion;
        private int Contador_Columna;
        private String Informacion;

        private static String P_Dt_Productos_Servicios = "P_Dt_Productos_Servicios";
        private static String P_Dt_Partidas = "P_Dt_Partidas";
        private static String P_Dt_Productos = "P_Dt_Productos";
        private static String P_Dt_Requisiciones = "P_Dt_Requisiciones";
        private static String P_Dt_Productos_Servicios_Modal = "P_Dt_Productos_Servicios_Modal";
        private static String P_Dt_Programas = "P_Dt_Programas";

        private const String Operacion_Comprometer = "COMPROMETER";
        private const String Operacion_Descomprometer = "DESCOMPROMETER";
        private const String Operacion_Quitar_Renglon = "QUITAR";
        private const String Operacion_Agregar_Renglon_Nuevo = "AGREGAR_NUEVO";
        private const String Operacion_Agregar_Renglon_Copia = "AGREGAR_COPIA";

        private const String SubFijo_Requisicion = "RQ-";
        private const String EST_EN_CONSTRUCCION = "EN CONSTRUCCION";
        private const String EST_GENERADA = "GENERADA";
        private const String EST_CANCELADA = "CANCELADA";
        private const String EST_REVISAR = "REVISAR";
        private const String EST_RECHAZADA = "RECHAZADA";
        private const String EST_AUTORIZADA = "AUTORIZADA";
        private const String EST_FILTRADA = "FILTRADA";
        private const String EST_COTIZADA = "COTIZADA";
        private const String EST_COMPRA = "COMPRA";
        private const String EST_TERMINADA = "TERMINADA";
        private const String EST_COTIZADA_RECHAZADA = "COTIZADA-RECHAZADA";
        private const String EST_ALMACEN = "ALMACEN";

        private const String COLOR_AZUL = "AZUL";
        private const String COLOR_ROJO = "ROJO";

        private static String P_Dt_Presupuestos = "P_Dt_Presupuestos";
        private static String P_Disponible = "P_Disponible";

    #endregion

    #region PAGE LOAD / INIT

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN: Al Cargar la Pagina
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        ///*******************************************************************************
        protected void Page_Load(object sender, EventArgs e) {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            ////Valores de primera vez        
            if (!IsPostBack) {
                Cargar_Ventana_Emergente_Busqueda_Solicitudes();
                Session["Activa"] = true;
                if (Session["Inicio"] == null) {
                    Session["Inicio"] = "Inicio";
                    Construir_DataTables();
                }
                ViewState["SortDirection"] = "DESC";
                DateTime _DateTime = DateTime.Now;
                int dias = _DateTime.Day;
                dias = dias * -1;
                dias++;
                _DateTime = _DateTime.AddDays(dias);
                Txt_Fecha.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
                Txt_Fecha_Inicial.Text = _DateTime.ToString("dd/MMM/yyyy").ToUpper();
                Txt_Fecha_Final.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
                Lbl_Categoria.Text = "Refacción";
                //llenar combo dependencias
                Dependencia_Negocio = new Cls_Cat_Dependencias_Negocio();
                DataTable Dt_Dependencias = Dependencia_Negocio.Consulta_Dependencias();
                Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Dependencia, Dt_Dependencias, "CLAVE_NOMBRE", "DEPENDENCIA_ID");
                Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Dependencia_Panel, Dt_Dependencias, 1, 0);
                Cmb_Dependencia.SelectedIndex = 0;
                //Cmb_Dependencia_Panel.SelectedValue = Cls_Sessiones.Dependencia_ID_Empleado;
                //MOD
                //Cmb_Dependencia_Panel.Enabled = false;
                Llenar_Combos_Busqueda();
                String[] Datos_Combo = 
                    { EST_EN_CONSTRUCCION, EST_GENERADA, EST_CANCELADA, EST_AUTORIZADA, 
                      EST_FILTRADA,EST_RECHAZADA,EST_REVISAR, EST_COTIZADA, EST_COTIZADA_RECHAZADA, EST_COMPRA, EST_ALMACEN, EST_TERMINADA };
                Llenar_Combo(Cmb_Estatus, Datos_Combo);
                Llenar_Grid_Requisiciones();
                Habilitar_Controles("Uno");

                //Verificar si su rol es jefe de dependencia, admin de modulo o admin de sistema
                DataTable Dt_Grupo_Rol = Cls_Util.Consultar_Grupo_Rol_ID(Cls_Sessiones.Rol_ID.ToString());
                if (Dt_Grupo_Rol != null) {
                    String Grupo_Rol = Dt_Grupo_Rol.Rows[0][Apl_Cat_Roles.Campo_Grupo_Roles_ID].ToString();
                    if (Grupo_Rol == "00001" || Grupo_Rol == "00002") {
                        Cmb_Dependencia_Panel.Enabled = true;
                    } else {
                        DataTable Dt_URs = Cls_Util.Consultar_URs_De_Empleado(Cls_Sessiones.Empleado_ID);
                        if (Dt_URs.Rows.Count > 1) {
                            Cmb_Dependencia_Panel.Enabled = true;
                            Cls_Util.Llenar_Combo_Con_DataTable_Generico
                                (Cmb_Dependencia_Panel, Dt_URs, 1, 0);
                            Cmb_Dependencia_Panel.SelectedValue = Cls_Sessiones.Dependencia_ID_Empleado;
                        }
                    }
                }
                Cargar_Datos_Presupuesto();
            }
            Mostrar_Informacion("", false);
            Modal_Busqueda_Prod_Serv.Hide();
        }

    #endregion

    #region METODOS

        #region INICIAL

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
            ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
            ///en la busqueda del Modalpopup
            ///CREO: Gustavo Angeles
            ///FECHA_CREO: 9/Diciembre/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///*******************************************************************************
            private void Construir_DataTables() {
                Session[P_Dt_Productos_Servicios_Modal] = null;
                Session[PS_ID] = null;
                Session[P_Dt_Productos_Servicios] = Construir_Tabla_Detalles_Requisicion();
                Session[P_Dt_Partidas] = Construir_Tabla_Presupuestos();
                Session[P_Dt_Productos] = Construir_Tabla_Productos();
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCIÓN: Habilitar_Controles
            /// DESCRIPCIÓN: Habilita la configuracion de acuerdo a la operacion     
            /// RETORNA: 
            /// CREO: Gustavo Angeles Cruz
            /// FECHA_CREO: 30/Agosto/2010 
            /// MODIFICO: Francisco A. Gallardo Castañeda
            /// FECHA_MODIFICO: Mayo/2012
            /// CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            /// *******************************************************************************/
            private void Habilitar_Controles(String Modo) {
                try {
                    switch (Modo) {
                        case "Uno":
                            Btn_Nuevo.Visible = true;
                            Btn_Modificar.Visible = false;
                            Btn_Eliminar.Visible = false;
                            Btn_Salir.Visible = true;
                            Btn_Imprimir_Req.Visible = false;
                            Btn_Nuevo.ToolTip = "Nuevo";
                            Btn_Modificar.ToolTip = "Modificar";
                            Btn_Eliminar.ToolTip = "Eliminar";
                            Btn_Salir.ToolTip = "Inicio";
                            Btn_Listar_Requisiciones.Visible = false;
                            Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                            Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                            Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar.png";
                            Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                            Txt_Fecha.Enabled = false;
                            Btn_Buscar_Solicitud.Style.Add("display", "none");
                            Txt_Folio.Enabled = false;
                            Cmb_Dependencia.Enabled = false;
                            Cmb_Tipo.Enabled = false;
                            Cmb_Estatus.Enabled = false;
                            Ibtn_Buscar_Producto.Enabled = false;
                            Ibtn_Agregar_Producto.Enabled = false;
                            Grid_Productos_Servicios.Enabled = false; 
                            Txt_Total.Enabled = false;
                            Txt_Producto_Servicio.Enabled = false;
                            Txt_Comentario.Enabled = false;
                            Txt_Cantidad.Enabled = false;
                            Txt_Justificacion.Enabled = false;
                            Txt_Especificaciones.Enabled = false;
                            Chk_Verificar.Enabled = false;
                            break;

                        case "Inicial":
                            Btn_Nuevo.Visible = true;
                            Btn_Modificar.Visible = true;
                            Btn_Eliminar.Visible = false;
                            Btn_Salir.Visible = false;
                            Btn_Imprimir_Req.Visible = true;
                            Btn_Nuevo.ToolTip = "Nuevo";
                            Btn_Modificar.ToolTip = "Modificar";
                            Btn_Eliminar.ToolTip = "Eliminar";
                            Btn_Salir.ToolTip = "Inicio";

                            Btn_Listar_Requisiciones.Visible = true;
                            Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                            Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                            Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar.png";
                            Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                            Btn_Buscar_Solicitud.Style.Add("display", "none");
                            Txt_Fecha.Enabled = false;
                            Txt_Folio.Enabled = false;
                            Cmb_Dependencia.Enabled = false;
                            Cmb_Tipo.Enabled = false;
                            Cmb_Estatus.Enabled = false; 
                            Ibtn_Buscar_Producto.Enabled = false;
                            Ibtn_Agregar_Producto.Enabled = false;
                            Grid_Productos_Servicios.Enabled = false;
                            Txt_Comentario.Enabled = false;
                            Txt_Cantidad.Enabled = false;
                            Txt_Justificacion.Enabled = false;
                            Txt_Especificaciones.Enabled = false;
                            Chk_Verificar.Enabled = false;
                            Lbl_Disponible_Partida.Text = "$ 0.00";
                            Lbl_Disponible_Producto.Text = "Disponible: 0 / Precio aproximado: $ 0.00";
                            break;
                        //Estado de Nuevo
                        case "Nuevo":
                            Btn_Listar_Requisiciones.Visible = false;
                            Btn_Nuevo.ToolTip = "Dar de Alta";
                            Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                            Btn_Modificar.Visible = false;
                            Btn_Eliminar.Visible = false;
                            Btn_Imprimir_Req.Visible = false;
                            Btn_Salir.ToolTip = "Cancelar";
                            Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                            Btn_Salir.Visible = true;
                            Btn_Buscar_Solicitud.Style.Add("display", "inline");
                            Cmb_Dependencia.Enabled = false;
                            Cmb_Tipo.Enabled = true;
                            Cmb_Estatus.Enabled = true;
                            Ibtn_Buscar_Producto.Enabled = true;
                            Ibtn_Agregar_Producto.Enabled = true;
                            Grid_Productos_Servicios.Enabled = true; 
                            Txt_Comentario.Enabled = true;
                            Txt_Comentario.Text = "";
                            Txt_Cantidad.Enabled = true;
                            Txt_Justificacion.Enabled = true;
                            Txt_Especificaciones.Enabled = true;
                            Chk_Verificar.Enabled = true;
                            //Poner la fecha
                            Txt_Fecha.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
                            //Verificar tipo empleado logueado
                            Cmb_Estatus.SelectedValue = "GENERADA";
                            Txt_Folio.Text = "Asigna folio al guardar requisición";
                            Lbl_Disponible_Producto.Text = "Disponible: 0 / Precio aproximado: $ 0.00";
                            Div_Comentarios.Style.Add("display","none");
                            break;
                        //Estado de Modificar
                        case "Modificar":
                            Btn_Listar_Requisiciones.Visible = false;
                            Btn_Nuevo.Visible = false;
                            Btn_Modificar.ToolTip = "Actualizar";
                            Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                            Btn_Eliminar.Visible = false;
                            Btn_Imprimir_Req.Visible = false;
                            Btn_Salir.ToolTip = "Cancelar";
                            Btn_Salir.Visible = true;
                            Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                            Cmb_Dependencia.Enabled = false;
                            Cmb_Tipo.Enabled = false;
                            Cmb_Estatus.Enabled = true; 
                            Ibtn_Buscar_Producto.Enabled = true;
                            Ibtn_Agregar_Producto.Enabled = true;
                            Grid_Productos_Servicios.Enabled = true;
                            Txt_Comentario.Enabled = true;
                            Txt_Comentario.Text = "";
                            Txt_Cantidad.Enabled = true;
                            Txt_Justificacion.Enabled = true;
                            Txt_Especificaciones.Enabled = true;
                            Chk_Verificar.Enabled = true;
                            Div_Comentarios.Style.Add("display","none");
                            Btn_Buscar_Solicitud.Style.Add("display", "none");
                            break;
                        default: break;
                    }
                } catch (Exception ex) {
                    Mostrar_Informacion(ex.ToString(), true);
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Mostrar_Información
            ///DESCRIPCIÓN: Llena las areas de texto con el registro seleccionado del grid
            ///RETORNA: 
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 24/Agosto/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///********************************************************************************/
            private void Mostrar_Informacion(String txt, Boolean mostrar, String Color) {
                if (Color == COLOR_AZUL) {
                    Lbl_Informacion.Style.Add("color", "#0000FF");
                } else {
                    Lbl_Informacion.Style.Add("color", "#990000");
                }
                Lbl_Informacion.Visible = mostrar;
                Img_Warning.Visible = mostrar;
                Lbl_Informacion.Text = txt;
            }
            
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Mostrar_Información
            ///DESCRIPCIÓN: Llena las areas de texto con el registro seleccionado del grid
            ///RETORNA: 
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 24/Agosto/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///********************************************************************************/
            private void Mostrar_Informacion(String txt, Boolean mostrar) {
                Lbl_Informacion.Style.Add("color", "#990000");
                Lbl_Informacion.Visible = mostrar;
                Img_Warning.Visible = mostrar;
                Lbl_Informacion.Text = txt;
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
            /// DESCRIPCIÓN: Limpia las areas de texto y deja los combos en su valor inical
            /// RETORNA: 
            /// CREO: 
            /// FECHA_CREO: 24/Agosto/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///********************************************************************************/
            private void Limpiar_Formulario() {
                Cmb_Tipo.SelectedIndex = 0;
                Cmb_Estatus.SelectedIndex = 0;
                Chk_Verificar.Checked = false;
                Limpiar_Cajas_Texto();
                Limpiar_Grids_Y_DataTables();
                Hdf_Total_Req.Value = null;
                Hdf_No_Economico.Value = "";
                Hdf_Tipo_Bien.Value = "";
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCIÓN: Limpiar_Cajas_Texto
            /// DESCRIPCIÓN: Limpia las areas de texto y deja los combos en su valor inical
            /// RETORNA: 
            /// CREO: 
            /// FECHA_CREO: 24/Agosto/2010 
            /// MODIFICO: Francisco A. Gallardo Castañeda
            /// FECHA_MODIFICO: Mayo/2012
            /// CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///********************************************************************************/
            private void Limpiar_Cajas_Texto() {
                Hdf_Programa.Value = "";
                Txt_Programa.Text = "";
                Txt_Folio.Text = "";
                Txt_Fecha.Text = "";
                Txt_Producto_Servicio.Text = "";
                Txt_Cantidad.Text = "";
                Txt_Comentario.Text = "";
                Txt_Busqueda.Text = "";
                Txt_Subtotal.Text = "0.0";
                Txt_IVA.Text = "0.0";
                Txt_Total.Text = "0.0";
                Txt_Justificacion.Text = "";
                Txt_Especificaciones.Text = "";
                Lbl_Disponible_Partida.Text = "$ 0.00";
                Hdf_No_Solicitud.Value = "";
                Txt_Solicitud.Text = "";
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCIÓN: Limpiar_Grids_Y_DataTables
            /// DESCRIPCIÓN: Limpia las grids y datatable de la página
            /// CREO: 
            /// FECHA_CREO: 24/Agosto/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///********************************************************************************/
            private void Limpiar_Grids_Y_DataTables() {
                Grid_Productos_Servicios.DataSource = null;
                Grid_Productos_Servicios.DataBind();
                Grid_Comentarios.DataSource = null;
                Grid_Comentarios.DataBind();
                Grid_Partidas_Tmp.DataSource = null;
                Grid_Partidas_Tmp.DataBind();
                Grid_Comentarios.DataSource = null;
                Grid_Comentarios.DataBind();
                Grid_Productos_Tmp.DataSource = null;
                Grid_Productos_Tmp.DataBind();

                ((DataTable)Session[P_Dt_Productos_Servicios]).Rows.Clear();
                ((DataTable)Session[P_Dt_Partidas]).Rows.Clear();
                ((DataTable)Session[P_Dt_Productos]).Rows.Clear();
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Construir_Tabla_Detalles_Requisicion
            ///DESCRIPCIÓN: Construir_Tabla_Detalles_Requisicion
            ///CREO: Gustavo Angeles
            ///FECHA_CREO: 9/Diciembre/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///*******************************************************************************
            private DataTable Construir_Tabla_Detalles_Requisicion() {
                DataTable Tabla = new DataTable();
                DataColumn Columna = null;
                DataTable Tabla_Base_Datos = JAPAMI.Taller_Mecanico.Requisiciones.Datos.Cls_Ope_Tal_Requisiciones_Datos.Consultar_Columnas_De_Tabla_BD(Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion);
                foreach (DataRow Renglon in Tabla_Base_Datos.Rows) {
                    Columna = new DataColumn(Renglon["COLUMNA"].ToString(), System.Type.GetType("System.String"));
                    Tabla.Columns.Add(Columna);
                }
                Columna = new DataColumn("UNIDAD", System.Type.GetType("System.String"));
                Tabla.Columns.Add(Columna);
                return Tabla;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Construir_Tabla_Presupuestos
            ///DESCRIPCIÓN: Construir_Tabla_Presupuestos
            ///CREO: Gustavo Angeles
            ///FECHA_CREO: 9/Diciembre/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///*******************************************************************************
            private DataTable Construir_Tabla_Presupuestos() {
                DataTable Tabla = new DataTable();
                DataColumn Columna = null;
                DataTable Tabla_Base_Datos = JAPAMI.Taller_Mecanico.Requisiciones.Datos.Cls_Ope_Tal_Requisiciones_Datos.Consultar_Columnas_De_Tabla_BD(Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto);
                foreach (DataRow Renglon in Tabla_Base_Datos.Rows) {
                    Columna = new DataColumn(Renglon["COLUMNA"].ToString(), System.Type.GetType("System.String"));
                    Tabla.Columns.Add(Columna);
                }
                Columna = new DataColumn("CLAVE", System.Type.GetType("System.String"));
                Tabla.Columns.Add(Columna);
                return Tabla;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Construir_Tabla_Productos
            ///DESCRIPCIÓN: Construir_Tabla_Productos
            ///CREO: Gustavo Angeles
            ///FECHA_CREO: 9/Diciembre/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///*******************************************************************************
            private DataTable Construir_Tabla_Productos() {
                DataTable Tabla = new DataTable();
                DataColumn Columna = null;
                DataTable Tabla_Base_Datos =
                    JAPAMI.Taller_Mecanico.Requisiciones.
                    Datos.Cls_Ope_Tal_Requisiciones_Datos.
                    Consultar_Columnas_De_Tabla_BD(Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones);
                foreach (DataRow Renglon in Tabla_Base_Datos.Rows) {
                    Columna = new DataColumn(Renglon["COLUMNA"].ToString(), System.Type.GetType("System.String"));
                    Tabla.Columns.Add(Columna);
                }
                return Tabla;
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCIÓN: Generar_Tabla_Informacion
            /// DESCRIPCIÓN: Crea una tabla con la informacion que se requiere ingresar al formulario
            /// RETORNA: 
            /// CREO: Gustavo Angeles Cruz
            /// FECHA_CREO: 24/Agosto/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///********************************************************************************/
            private void Generar_Tabla_Informacion() {
                Contador_Columna = Contador_Columna + 1;
                if (Contador_Columna > 2) {
                    Contador_Columna = 0;
                    Informacion += "</tr><tr>";
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Limpiar_Modal_Produtos_Servicios
            ///DESCRIPCIÓN: Limpia el Modal de Busqueda
            ///en la busqueda del Modalpopup
            ///CREO: Gustavo Angeles
            ///FECHA_CREO: 9/Diciembre/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///*******************************************************************************
            private void Limpiar_Modal_Produtos_Servicios() {
                Session[P_Dt_Productos_Servicios_Modal] = null;
                Grid_Productos_Servicios_Modal.DataSource = null;// P_Dt_Productos_Servicios_Modal;
                Grid_Productos_Servicios_Modal.DataBind();
            }
    
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
            ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
            ///en la busqueda del Modalpopup
            ///CREO: Gustavo Angeles
            ///FECHA_CREO: 9/Diciembre/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///*******************************************************************************
            private double Formato_Double(double numero) {
                try {
                    String Str_Numero = String.Format("{0:n}", numero); 
                    numero = Convert.ToDouble(Str_Numero);
                } catch (Exception Ex) {
                    String Str = Ex.ToString();
                    numero = 0;
                }
                return numero;
            }

        #endregion

        #region LLENADO DE COMPONENTES
    
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCIÓN: Llenar_Grid_Requisiciones
            /// DESCRIPCIÓN: Llena el grid principal de requisiciones
            /// RETORNA: 
            /// CREO: Gustavo Angeles Cruz
            /// FECHA_CREO: Diciembre/2010 
            /// MODIFICO: Francisco A. Gallardo Castañeda
            /// FECHA_MODIFICO: Mayo/2012
            /// CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///********************************************************************************/
            public void Llenar_Grid_Requisiciones() {
                Div_Contenido.Style.Add("display","none");
                Div_Listado_Requisiciones.Style.Add("display","inline");
                Requisicion_Negocio = new Cls_Ope_Tal_Requisiciones_Negocio();
                if (Cmb_Dependencia_Panel.SelectedIndex > 0) Requisicion_Negocio.P_Dependencia_ID = Cmb_Dependencia_Panel.SelectedValue;

                Requisicion_Negocio.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicial.Text));

                Requisicion_Negocio.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Final.Text));
                

                if (Txt_Busqueda.Text.Trim().Length > 0) {
                    String No_Requisa = Txt_Busqueda.Text;
                    No_Requisa = No_Requisa.ToUpper();
                    No_Requisa = No_Requisa.Replace("RQ-", "");
                    int Int_No_Requisa = 0;
                    try {
                        Int_No_Requisa = int.Parse(No_Requisa);
                    } catch (Exception Ex) {
                        String Str = Ex.ToString();
                        No_Requisa = "0";
                    }
                    Requisicion_Negocio.P_Requisicion_ID = No_Requisa;
                }
                Requisicion_Negocio.P_Tipo = Cmb_Tipo_Busqueda.SelectedValue.Trim();
                Requisicion_Negocio.P_Estatus = Cmb_Estatus_Busqueda.SelectedValue.Trim();
                Session[P_Dt_Requisiciones] = Requisicion_Negocio.Consultar_Requisiciones();
                if (Session[P_Dt_Requisiciones] != null && ((DataTable)Session[P_Dt_Requisiciones]).Rows.Count > 0) {
                    Div_Contenido.Style.Add("display","none");
                    Grid_Requisiciones.DataSource = Session[P_Dt_Requisiciones] as DataTable;
                    Grid_Requisiciones.DataBind();
                } else { 
                    Session[P_Dt_Requisiciones] = null;
                    Grid_Requisiciones.DataSource = null;
                    Grid_Requisiciones.DataBind();
                }
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCIÓN: Llenar_Combos_Generales()
            /// DESCRIPCIÓN: Llena los combos principales de la interfaz de usuario
            /// RETORNA: 
            /// CREO: Gustavo Angeles Cruz
            /// FECHA_CREO: Diciembre/2010 
            /// MODIFICO: Francisco A. Gallardo Castañeda
            /// FECHA_MODIFICO: Mayo/2012
            /// CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///********************************************************************************/
            public void Llenar_Combos_Generales() { 
                Txt_Subtotal.Text = "0.0";
                Txt_Total.Text = "0.0";
                Txt_IVA.Text = "0.0";
                //Llenar combo de tipo
                Cmb_Tipo.Items.Clear();
                Cmb_Tipo.Items.Add("<<SELECCIONAR>>");
                Cmb_Tipo.Items.Add("STOCK");
                Cmb_Tipo.Items.Add("TRANSITORIA");
                Cmb_Tipo.Items[0].Value = "0";
                Cmb_Tipo.Items[0].Selected = true;
                //Seleccionar combo dependencias
                
                Cargar_Datos_Proyectos_Programas();
            }

            public void Cargar_Datos_Proyectos_Programas()
            {
                Requisicion_Negocio = new Cls_Ope_Tal_Requisiciones_Negocio();
                if (Cmb_Dependencia.SelectedIndex > 0) Requisicion_Negocio.P_Dependencia_ID = Cmb_Dependencia.SelectedValue;
                DataTable Data_Table_Proyectos = Requisicion_Negocio.Consultar_Proyectos_Programas();
                Session[P_Dt_Programas] = Data_Table_Proyectos;

            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCIÓN: Llenar_Combos_Busqueda()
            /// DESCRIPCIÓN: Llena los combos principales de la interfaz de usuario
            /// RETORNA: 
            /// CREO: Gustavo Angeles Cruz
            /// FECHA_CREO: Diciembre/2010 
            /// MODIFICO: Francisco A. Gallardo Castañeda
            /// FECHA_MODIFICO: Mayo/2012
            /// CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///********************************************************************************/
            public void Llenar_Combos_Busqueda() {
                String[] Datos_Combo = 
                        {"CONST ,GENERADA, REVISAR", EST_EN_CONSTRUCCION, EST_GENERADA, EST_CANCELADA, EST_AUTORIZADA, 
                          EST_FILTRADA,EST_RECHAZADA,EST_REVISAR, EST_COTIZADA, EST_COTIZADA_RECHAZADA, EST_COMPRA, EST_ALMACEN, EST_TERMINADA };
                Llenar_Combo(Cmb_Estatus_Busqueda, Datos_Combo);
                Cmb_Estatus_Busqueda.SelectedIndex = 1;
                Cmb_Tipo_Busqueda.Items.Clear();
                Cmb_Tipo_Busqueda.Items.Add("STOCK,TRANSITORIA");
                Cmb_Tipo_Busqueda.Items.Add("STOCK");
                Cmb_Tipo_Busqueda.Items.Add("TRANSITORIA");
                Cmb_Tipo_Busqueda.Items[0].Selected = true;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Refrescar_Grid
            ///DESCRIPCIÓN: refresca el gris con los registros de asuntos mas actuales 
            ///que existen en la base de datos
            ///PARAMETROS: 
            ///CREO: Silvia Morales Portuhondo
            ///FECHA_CREO: 02/Noviembre/2010 
            /// MODIFICO: Francisco A. Gallardo Castañeda
            /// FECHA_MODIFICO: Mayo/2012
            /// CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///*******************************************************************************
            public void Refrescar_Grid() {
                Grid_Productos_Servicios.DataSource = ((DataTable)Session[P_Dt_Productos_Servicios]);
                Grid_Productos_Servicios.DataBind();
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combo
            ///DESCRIPCIÓN:
            ///CREO: Gustavo Angeles
            ///FECHA_CREO: 9/Diciembre/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///*******************************************************************************
            private void Llenar_Combo(DropDownList Combo, String[] Items) {
                Combo.Items.Clear();
                Combo.Items.Add("<<SELECCIONAR>>");
                foreach (String _Item in Items) {
                    Combo.Items.Add(_Item);
                }
                Combo.Items[0].Value = "0";
                Combo.Items[0].Selected = true;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Agregar_Tooltip_Combo
            ///DESCRIPCIÓN:  
            ///CREO: Gustavo Angeles
            ///FECHA_CREO: 9/Diciembre/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///*******************************************************************************
            private void Agregar_Tooltip_Combo(DropDownList Combo) {
                foreach (ListItem Item in Combo.Items) {
                    Item.Attributes.Add("Title", Item.Text);
                }
            }

        #endregion

        #region DATATABLE

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
            ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
            ///en la busqueda del Modalpopup
            ///CREO: Gustavo Angeles
            ///FECHA_CREO: 9/Diciembre/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///*******************************************************************************
            private DataTable Agregar_Quitar_Renglones_A_DataTable(DataTable _DataTable, DataRow _DataRow, String Operacion) {
                if (Operacion == Operacion_Agregar_Renglon_Nuevo) {
                    _DataTable.Rows.Add(_DataRow);
                } else if (Operacion == Operacion_Agregar_Renglon_Copia) {
                    _DataTable.ImportRow(_DataRow);
                    _DataTable.AcceptChanges();
                } else if (Operacion == Operacion_Quitar_Renglon) {
                    ((DataTable)Session[P_Dt_Productos_Servicios]).Rows.Remove(_DataRow);
                }
                return _DataTable;
            }
            
        #endregion
    
        #region VALIDACIONES

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
            ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
            ///en la busqueda del Modalpopup
            ///PARAMETROS:   
            ///CREO: Gustavo Angeles
            ///FECHA_CREO: 9/Diciembre/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///*******************************************************************************
            public bool Verificar_Fecha() {
                bool Respuesta = false;
                //Variables que serviran para hacer la convecion a datetime las fechas y poder validarlas 
                DateTime Date1 = new DateTime();
                DateTime Date2 = new DateTime();
                try {
                    //Convertimos el Texto de los TextBox fecha a dateTime
                    Date1 = DateTime.Parse(Txt_Fecha_Inicial.Text);
                    Date2 = DateTime.Parse(Txt_Fecha_Final.Text);
                    if (Date1 <= Date2) {
                        Respuesta = true;
                    }
                } catch (Exception e) {
                    String str = e.ToString();
                    Respuesta = false;
                }
                return Respuesta;
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCIÓN: Busca_Productos_Servicios_Duplicados
            /// DESCRIPCIÓN: Habilita la configuracion de acuerdo a la operacion     
            /// RETORNA: 
            /// CREO: Gustavo Angeles Cruz
            /// FECHA_CREO: 30/Agosto/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            /// *******************************************************************************/
            private bool Busca_Productos_Servicios_Duplicados(String Prod_Serv_ID) {
                bool Respuesta = false;
                if (Session[P_Dt_Productos_Servicios] != null && ((DataTable)Session[P_Dt_Productos_Servicios]).Rows.Count != 0) {
                    DataRow[] _DataRow = ((DataTable)Session[P_Dt_Productos_Servicios]).Select(Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Prod_Serv_ID + "'");
                    if (_DataRow != null && _DataRow.Length > 0) {
                        Respuesta = true;
                    }
                }
                return Respuesta;
            }
    
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Longitud
            ///DESCRIPCIÓN: Valida la longitud del texto que se recibe en un TextBox
            ///PARAMETROS:             
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 20/Diciembre/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///*******************************************************************************
            public Boolean Validar_Longitud(TextBox Txt_Control, int Int_Tamaño) {
                Boolean Bln_Bandera;
                Bln_Bandera = false;
                if (Txt_Control.Text.Length >= Int_Tamaño)
                    Bln_Bandera = true;
                return Bln_Bandera;
            }
    
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCIÓN: Validaciones
            /// DESCRIPCIÓN: Genera el String con la informacion que falta y ejecuta la 
            /// operacion solicitada si las validaciones son positivas
            /// RETORNA: 
            /// CREO: Gustavo Angeles Cruz
            /// FECHA_CREO: 24/Agosto/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///********************************************************************************/
            private Boolean Validaciones(bool Validar_Completo) {
                Boolean Bln_Bandera;
                Contador_Columna = 0;
                Bln_Bandera = true;
                Informacion += "<table style='width: 100%;font-size:9px;' >" +
                    "<tr colspan='3'>Es necesario:</tr>" +
                    "<tr>";
                //Verifica que campos esten seleccionados o tengan valor valor
                if (Cmb_Tipo.SelectedIndex == 0) {
                    Informacion += "<td>+ Seleccionar Tipo. </td>";
                    Bln_Bandera = false;
                    Generar_Tabla_Informacion();
                }
                if (Cmb_Estatus.SelectedIndex == 0) {
                    Informacion += "<td>+ Seleccionar Estatus.</td>";
                    Bln_Bandera = false;
                    Generar_Tabla_Informacion();
                }
                if (Cmb_Dependencia.SelectedIndex == 0) {
                    Informacion += "<td>+ Seleccionar Dependencia.</td>";
                    Bln_Bandera = false;
                    Generar_Tabla_Informacion();
                }
                if (Hdf_No_Solicitud.Value.Trim().Length == 0)
                {
                    Informacion += "<td>+ Seleccionar La Solicitud a la cual se le cargarán las Refacciones.</td>";
                    Bln_Bandera = false;
                    Generar_Tabla_Informacion();
                }
                if (Validar_Completo) {
                    if (Session[P_Dt_Productos_Servicios] != null) {
                        if (((DataTable)Session[P_Dt_Productos_Servicios]).Rows.Count == 0) {
                            Informacion += "<td>+ Agregar Producto.</td>";
                            Bln_Bandera = false;
                            Generar_Tabla_Informacion();
                        }
                    }
                    if (Txt_Justificacion.Text.Trim().Length != 0) {
                        if (Validar_Longitud(Txt_Comentario, 250)) {
                            Informacion += "<td>+ Campo comentario excede la longitud permitida.</td>";
                            Bln_Bandera = false;
                            Generar_Tabla_Informacion();
                        }
                    }
                }
                Informacion += "</tr></table>";
                return Bln_Bandera;
            }
    
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
            ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
            ///en la busqueda del Modalpopup
            ///CREO: Gustavo Angeles
            ///FECHA_CREO: 9/Diciembre/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///*******************************************************************************
            private DataTable Verifica_Disponible_De_Un_Producto_En_BD(String Producto_ID) {
                Requisicion_Negocio = new Cls_Ope_Tal_Requisiciones_Negocio();
                Requisicion_Negocio.P_Producto_ID = Producto_ID;
                DataTable Producto = Requisicion_Negocio.Consultar_Poducto_Por_ID();
                try {
                    if (Producto == null || Producto.Rows.Count <= 0) {
                        Producto = null;
                    }
                } catch (Exception Ex) {
                    String Str = Ex.ToString();
                    Mostrar_Informacion(Str, true);
                    return null;
                }
                return Producto;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
            ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
            ///en la busqueda del Modalpopup
            ///CREO: Gustavo Angeles
            ///FECHA_CREO: 9/Diciembre/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///*******************************************************************************
            private int Verifica_Disponible_De_Un_Producto_De_Stock_En_Dt_Productos(String Producto_ID, DataTable Productos) {
                int Disponible = 0;
                try {
                    DataRow[] Renglones = Productos.Select("Refaccion_ID = '" + Producto_ID + "'");
                    //Si encuentra el producto
                    if (Renglones.Length > 0) {
                        Disponible = int.Parse(Renglones[0]["DISPONIBLE"].ToString());
                    }//si no encuentra el producto consulta en la base de datos
                    else {
                        DataTable _DataTable = Verifica_Disponible_De_Un_Producto_En_BD(Producto_ID);
                        if (_DataTable != null) {
                            Disponible = int.Parse(_DataTable.Rows[0]["DISPONIBLE"].ToString());
                            //Agrego los productos consultados al DataTable de Productos
                            ((DataTable)Session[P_Dt_Productos]).ImportRow(_DataTable.Rows[0]);
                            ((DataTable)Session[P_Dt_Productos]).AcceptChanges();
                        }
                    }
                } catch (Exception Ex) {
                    String Str = Ex.ToString();
                    Mostrar_Informacion(Str, true);
                    return -1;
                }
                return Disponible;
            }

            /// *****************************************************************************************
            /// NOMBRE: Consulta_Disponible
            /// DESCRIPCIÓN:  
            /// CREÓ: Gustavo Angeles Cruz
            /// FECHA CREÓ: 11/Junio/2011
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            /// *****************************************************************************************
            private double Consulta_Disponible() {
                double Cantidad = 0;
                try {
                    Cantidad = Convert.ToDouble(Session[P_Disponible].ToString());
                } catch (Exception Ex) {
                    Cantidad = 0;
                }
                return Cantidad;
            }

            /// *****************************************************************************************
            /// NOMBRE: Actualiza_Disponible
            /// DESCRIPCIÓN:  
            /// CREÓ: Gustavo Angeles Cruz
            /// FECHA CREÓ: 11/Junio/2011
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            /// *****************************************************************************************
            private void Actualiza_Disponible(double Importe, String Operacion) {
                double Cantidad = 0;
                try {
                    Cantidad = Convert.ToDouble(Session[P_Disponible].ToString());
                    if (Operacion == Operacion_Comprometer) {
                        Cantidad = Cantidad - Importe;
                    } else if (Operacion == Operacion_Descomprometer) {
                        Cantidad = Cantidad + Importe;
                    }
                    String Mto_Disponible = String.Format("{0:n}", Cantidad);
                    Lbl_Disponible_Partida.Text = " $ " + Mto_Disponible;
                    Session[P_Disponible] = Cantidad;
                } catch (Exception Ex) {
                    throw new Exception(Ex.ToString());
                }
            }

        #endregion

        #region CALCULOS
    
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Calcular_Impuestos
            ///DESCRIPCIÓN: Calcula IVA, IEPS, SUBTOTAL y TOTAL y lo spone en sus cajas de texto
            ///PARAMETROS: 
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 22/Noviembre/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///*******************************************************************************
            private void Calcular_Impuestos() {
                double Subtotal = 0;
                double Total = 0;
                double IVA = 0;
                double IEPS = 0;
                if (Session[P_Dt_Productos_Servicios] != null && ((DataTable)Session[P_Dt_Productos_Servicios]).Rows.Count > 0) {
                    foreach (DataRow Renglon in ((DataTable)Session[P_Dt_Productos_Servicios]).Rows) {
                        Subtotal = Subtotal + double.Parse(Renglon["Monto"].ToString());
                        IVA = IVA + double.Parse(Renglon["Monto_IVA"].ToString());
                        IEPS = IEPS + double.Parse(Renglon["Monto_IEPS"].ToString());
                    }
                }
                Subtotal = Formato_Double(Subtotal);
                IEPS = Formato_Double(IEPS);
                IVA = Formato_Double(IVA);

                Txt_Subtotal.Text = String.Format("{0:n}", Subtotal.ToString());
                Txt_IVA.Text = String.Format("{0:n}", IVA.ToString());
                Total = Subtotal + IVA + IEPS;
                Total = Formato_Double(Total);
                Txt_Total.Text = String.Format("{0:n}", Total.ToString());
            }
            
        #endregion

        #region METODOS EVENTUALES

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Evento_Boton_Agregar_Producto
            ///DESCRIPCIÓN: Evento_Boton_Agregar_Producto
            ///CREO: Gustavo Angeles
            ///FECHA_CREO: 9/Diciembre/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///*******************************************************************************
            private void Evento_Boton_Agregar_Producto() {
                Cmb_Tipo.Enabled = false;

                Requisicion_Negocio = new Cls_Ope_Tal_Requisiciones_Negocio();
                String Impuesto1 = "";
                String Impuesto2 = "";
                String Porcentaje_IVA = "0";
                String Porcentaje_IEPS = "0";
                DataSet Data_Set_Impuestos;
                //AGREGAR UN PRODUCTO A LA TABLA
                Requisicion_Negocio.P_Producto_ID = Session[PS_ID].ToString();
                //DataSet Ds_Detalle_Productos = Requisicion_Negocio.Consultar_Detalle_Productos();
                DataTable Dt_Producto_Consultado = null;
                Dt_Producto_Consultado = Requisicion_Negocio.Consultar_Poducto_Por_ID();

                Impuesto1 = Dt_Producto_Consultado.Rows[0][Cat_Com_Productos.Campo_Impuesto_ID].ToString();
                //VERIFICAR SI HAY IMPUESTOS
                if (Impuesto1 != "") {
                    Requisicion_Negocio.P_Impuesto_ID = Impuesto1;
                    Data_Set_Impuestos = Requisicion_Negocio.Consultar_Impuesto();
                    Impuesto1 = Data_Set_Impuestos.Tables[0].Rows[0].ItemArray[0].ToString();
                    if (Impuesto1 == "IVA") {
                        Porcentaje_IVA = Data_Set_Impuestos.Tables[0].Rows[0].ItemArray[1].ToString();
                    }
                    if (Impuesto1 == "IEPS") {
                        Porcentaje_IEPS = Data_Set_Impuestos.Tables[0].Rows[0].ItemArray[1].ToString();
                    }
                }
                //Si es producto calcula el impuesto 2
                if (Impuesto2 != "") {
                    Requisicion_Negocio.P_Impuesto_ID = Impuesto2;
                    Data_Set_Impuestos = Requisicion_Negocio.Consultar_Impuesto();
                    Impuesto2 = Data_Set_Impuestos.Tables[0].Rows[0].ItemArray[0].ToString();
                    if (Impuesto2 == "IVA") {
                        Porcentaje_IVA = Data_Set_Impuestos.Tables[0].Rows[0].ItemArray[1].ToString();
                    }
                    if (Impuesto2 == "IEPS") {
                        Porcentaje_IEPS = Data_Set_Impuestos.Tables[0].Rows[0].ItemArray[1].ToString();
                    }
                }
                //AGREGAR LOS DATOS AL RENGLON DE MI DATA SET DE PRODUCTOS

                DataRow Renglon_Producto = ((DataTable)Session[P_Dt_Productos_Servicios]).NewRow();
                Renglon_Producto["FUENTE_FINANCIAMIENTO_ID"] = Hdf_Fte_Financiamiento.Value.Trim();
                Renglon_Producto["Tipo"] = "PRODUCTO";
                Renglon_Producto[Cat_Tal_Refacciones.Campo_Refaccion_ID] = Dt_Producto_Consultado.Rows[0][Cat_Tal_Refacciones.Campo_Refaccion_ID].ToString();
                DataTable Dt_Conceptos = Cls_Util.Consultar_Conceptos_De_Partidas("'" + Hdf_Partida.Value + "'");
                Renglon_Producto["Concepto_ID"] = Dt_Conceptos.Rows[0]["CONCEPTO_ID"].ToString();
                Renglon_Producto["Nombre_Concepto"] = Dt_Conceptos.Rows[0]["DESCRIPCION"].ToString(); 
                Renglon_Producto["Clave"] = Dt_Producto_Consultado.Rows[0]["CLAVE"].ToString();
                Renglon_Producto["Partida_ID"] = Hdf_Partida.Value;
                Renglon_Producto["Proyecto_Programa_ID"] = Hdf_Programa.Value;
                Renglon_Producto["Nombre_Producto_Servicio"] = Dt_Producto_Consultado.Rows[0]["NOMBRE_DESCRIPCION"].ToString();
                //Cantidad de productos solicitados
                Renglon_Producto["Cantidad"] = Txt_Cantidad.Text.Trim();
                Renglon_Producto["Unidad"] = Dt_Producto_Consultado.Rows[0]["UNIDAD"].ToString();

                //Costo unitario sin impuiestos del producto solicitado
                String Costo = Dt_Producto_Consultado.Rows[0][Cat_Tal_Refacciones.Campo_Costo_Unitario].ToString();
                Renglon_Producto["Precio_Unitario"] = Costo;
                //Importe, Cantidad * Costo
                double Importe = double.Parse(Txt_Cantidad.Text) * double.Parse(Costo);
                Importe = Formato_Double(Importe);
                double Monto_IEPS = 0.00;
                double Monto_IVA = 0.00;
                //Importe:Monto, Cantidad * Costo
                Renglon_Producto["Monto"] = Importe.ToString();
                if (Impuesto1 == "IEPS" || Impuesto2 == "IEPS") {
                    Monto_IEPS = (Importe * double.Parse(Porcentaje_IEPS)) / 100;
                    Monto_IEPS = Formato_Double(Monto_IEPS);
                } else {
                    Monto_IEPS = 0.0;
                    Porcentaje_IEPS = "0.00";
                }
                if (Impuesto1 == "IVA" || Impuesto2 == "IVA") {
                    Monto_IVA = ((Importe + Monto_IEPS) * double.Parse(Porcentaje_IVA)) / 100;
                    Monto_IVA = Formato_Double(Monto_IVA);
                } else {
                    Monto_IVA = 0.0;
                    Porcentaje_IVA = "0.00";
                }
                double Monto_Total = Importe + Monto_IVA + Monto_IEPS;
                Monto_Total = Formato_Double(Monto_Total);
                Renglon_Producto["Monto_Total"] = Monto_Total.ToString();
                Renglon_Producto["Monto_IVA"] = Monto_IVA.ToString();
                Renglon_Producto["Porcentaje_IVA"] = Porcentaje_IVA;
                Renglon_Producto["Monto_IEPS"] = Monto_IEPS.ToString();
                Renglon_Producto["Porcentaje_IEPS"] = Porcentaje_IEPS;
                //@
                double Disponible_Partida = Consulta_Disponible();

                if (Disponible_Partida >= Monto_Total) {
                    bool Agregar_Producto = true;
                    Double Cantidad_Que_Solicitan = Double.Parse(Txt_Cantidad.Text.Trim());
                    if (Cmb_Tipo.SelectedValue == "STOCK") {
                        int Disponible_Producto = Verifica_Disponible_De_Un_Producto_De_Stock_En_Dt_Productos(Session[PS_ID].ToString(), ((DataTable)Session[P_Dt_Productos]));
                        // bool Producto_Encontrado = false;
                        if (Disponible_Producto >= Cantidad_Que_Solicitan) {
                            //Se comprometen los productos en el Dt_Productos, todo es virtual
                            Comprometer_O_Descomprometer_Disponible_A_Productos_En_Dt_Productos(
                                Session[PS_ID].ToString(), ((DataTable)Session[P_Dt_Productos]), Operacion_Comprometer, Cantidad_Que_Solicitan);
                        } else {
                            Agregar_Producto = false;
                            Mostrar_Informacion("La cantidad de productos no se encuentra disponible: Existencia [" + Disponible_Producto + "]", true);
                        }
                    }
                    if (Agregar_Producto) {
                        //Aqui se agrega el producto al DataSet Ds_Productos_Servicios
                        Session[P_Dt_Productos_Servicios] = Agregar_Quitar_Renglones_A_DataTable(((DataTable)Session[P_Dt_Productos_Servicios]), Renglon_Producto, Operacion_Agregar_Renglon_Nuevo);
                        Refrescar_Grid();
                        Calcular_Impuestos();
                        Actualiza_Disponible(Monto_Total, Operacion_Comprometer);
                    }
                } else {
                    Mostrar_Informacion("Presupuesto insuficiente en la partida: " + Txt_Partida.Text.Trim(), true);
                }
                Txt_Cantidad.Text = "";
                Txt_Producto_Servicio.Text = "";
                Actualizar_Disponible_Productos("Refaccion_ID", Session[PS_ID].ToString(), ((DataTable)Session[P_Dt_Productos]));
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Evento_Grid_Requisiciones_Seleccionar
            ///DESCRIPCIÓN:
            ///CREO: Gustavo Angeles
            ///FECHA_CREO: 9/Diciembre/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///*******************************************************************************
            private void Evento_Grid_Requisiciones_Seleccionar(String Dato) {
                //@
                Session.Remove(P_Disponible);
                DataTable Dt_Comentarios = (DataTable)Grid_Comentarios.DataSource;
                Habilitar_Controles("Inicial");
                Txt_Justificacion.Visible = true;
                Construir_DataTables();
                Llenar_Combos_Generales();
                Div_Listado_Requisiciones.Style.Add("display","none");
                Div_Contenido.Style.Add("display","inline");

                Dependencia_Negocio = new Cls_Cat_Dependencias_Negocio();
                Requisicion_Negocio = new Cls_Ope_Tal_Requisiciones_Negocio();
                String No_Requisicion = Dato; 
                DataRow[] Requisicion = ((DataTable)Session[P_Dt_Requisiciones]).Select("No_Requisicion = '" + No_Requisicion + "'");

                Txt_Folio.Text = Requisicion[0][Ope_Tal_Requisiciones.Campo_Folio].ToString();
                String Fecha = Requisicion[0][Ope_Tal_Requisiciones.Campo_Fecha_Creo].ToString();
                Fecha = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(Fecha));
                Txt_Fecha.Text = Fecha.ToUpper();
                //Seleccionar combo dependencia
                Cmb_Dependencia.SelectedValue =
                    Requisicion[0][Ope_Tal_Requisiciones.Campo_Dependencia_ID].ToString().Trim();
                //Seleccionar Tipo
                Cmb_Tipo.SelectedValue =
                    Requisicion[0][Ope_Tal_Requisiciones.Campo_Tipo].ToString().Trim();
                //Seleccionar estatus
                Cmb_Estatus.SelectedValue =
                    Requisicion[0][Ope_Tal_Requisiciones.Campo_Estatus].ToString().Trim();
                Session[Estatus] = Requisicion[0][Ope_Tal_Requisiciones.Campo_Estatus].ToString().Trim(); 
                //Poner Justificación
                Txt_Justificacion.Text =
                    Requisicion[0][Ope_Tal_Requisiciones.Campo_Justificacion_Compra].ToString();
                //Poner especificaciones
                Txt_Especificaciones.Text =
                    Requisicion[0][Ope_Tal_Requisiciones.Campo_Especificacion_Prod_Serv].ToString();
                Txt_Subtotal.Text =
                    Requisicion[0][Ope_Tal_Requisiciones.Campo_Subtotal].ToString();
                Txt_IVA.Text =
                    Requisicion[0][Ope_Tal_Requisiciones.Campo_IVA].ToString();
                //Total de la requisición
                Txt_Total.Text =
                    Requisicion[0][Ope_Tal_Requisiciones.Campo_Total].ToString();
                Hdf_Total_Req.Value = Txt_Total.Text.Trim();
                Hdf_No_Solicitud.Value = Requisicion[0][Ope_Tal_Requisiciones.Campo_No_Solicitud].ToString();
                if (!String.IsNullOrEmpty(Hdf_No_Solicitud.Value)) {
                    Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                    Solicitud.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                    DataTable Dt_Resultado = Solicitud.Consultar_Listado_Solicitudes_Servicio();
                    if (Dt_Resultado.Rows.Count > 0) {
                        Txt_Solicitud.Text = "Folio: " + Dt_Resultado.Rows[0]["FOLIO"].ToString().Trim();
                        Txt_Solicitud.Text += " / No. Inventario: " + Dt_Resultado.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                        Txt_Solicitud.Text += " / Descripción: " + Dt_Resultado.Rows[0]["DESCRIPCION_SERVICIO"].ToString().Trim();
                        Hdf_Tipo_Bien.Value = Dt_Resultado.Rows[0]["TIPO_BIEN"].ToString();
                        Hdf_No_Economico.Value = Dt_Resultado.Rows[0]["NO_ECONOMICO"].ToString().Trim();
                    } else {
                        Txt_Solicitud.Text = "";
                    }
                } else {
                    Txt_Solicitud.Text = "";
                }

                //LLenar DataTable P_Dt_Productos_Servicios
                Requisicion_Negocio.P_Requisicion_ID = No_Requisicion;
                Session[P_Dt_Productos_Servicios] = Requisicion_Negocio.Consultar_Productos_Servicios_Requisiciones();

                String Str_Partidas_IDs = "";
                String Str_Productos_IDs = "";
                if (Session[P_Dt_Productos_Servicios] != null && ((DataTable)Session[P_Dt_Productos_Servicios]).Rows.Count > 0) {
                    Grid_Productos_Servicios.DataSource = Session[P_Dt_Productos_Servicios] as DataTable;
                    Grid_Productos_Servicios.DataBind();
                    //Recorrer P_Dt_Productos_Servicios para obtener las partidas y los producos
                    foreach (DataRow Row in ((DataTable)Session[P_Dt_Productos_Servicios]).Rows) {
                        Str_Partidas_IDs = Str_Partidas_IDs + Row["PARTIDA_ID"].ToString() + ",";
                        Str_Productos_IDs = Str_Productos_IDs + Row[Cat_Tal_Refacciones.Campo_Refaccion_ID].ToString() + ",";
                    }
                    if (Str_Partidas_IDs.Length > 0) {
                        Str_Partidas_IDs = Str_Partidas_IDs.Substring(0, Str_Partidas_IDs.Length - 1);
                    }
                    if (Str_Productos_IDs.Length > 0) {
                        Str_Productos_IDs = Str_Productos_IDs.Substring(0, Str_Productos_IDs.Length - 1);
                    }
                }
                //Llenar DataTable P_Dt_Productos
                Requisicion_Negocio = new Cls_Ope_Tal_Requisiciones_Negocio();
                Requisicion_Negocio.P_Producto_ID = Str_Productos_IDs;
                Session[P_Dt_Productos] = Requisicion_Negocio.Consultar_Poducto_Por_ID();

                //Llenar DataTable P_Dt_Partidas
                Requisicion_Negocio.P_Fuente_Financiamiento =
                    ((DataTable)Session[P_Dt_Productos_Servicios]).Rows[0]["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();

                Requisicion_Negocio.P_Dependencia_ID =
                    Requisicion[0][Ope_Tal_Requisiciones.Campo_Dependencia_ID].ToString().Trim();

                Requisicion_Negocio.P_Proyecto_Programa_ID =
                    ((DataTable)Session[P_Dt_Productos_Servicios]).Rows[0]["PROYECTO_PROGRAMA_ID"].ToString();

                Cls_Cat_Com_Proyectos_Programas_Negocio Proy_Neg = new Cls_Cat_Com_Proyectos_Programas_Negocio();
                Proy_Neg.P_Proyecto_Programa_ID = Requisicion_Negocio.P_Proyecto_Programa_ID;
                DataTable Dt_Programa = Proy_Neg.Consulta_Programas_Proyectos();
                if (Dt_Programa != null)
                {
                    if (Dt_Programa.Rows.Count > 0)
                    {
                        Hdf_Programa.Value = Requisicion_Negocio.P_Proyecto_Programa_ID;
                        Txt_Programa.Text = Dt_Programa.Rows[0][Cat_Com_Proyectos_Programas.Campo_Clave].ToString().Trim() + " - " + Dt_Programa.Rows[0][Cat_Com_Proyectos_Programas.Campo_Descripcion].ToString().Trim();
                    }
                }

                //Se llena el combo partidas
                //Llenar Combo Partidas con el evento del Combo Programas
                //Consultar las partidas usadas 1
                Requisicion_Negocio.P_Partida_ID = Str_Partidas_IDs;
                Requisicion_Negocio.P_Anio_Presupuesto = DateTime.Now.Year;

                double Disponible_Partida = Cls_Ope_Psp_Manejo_Presupuesto.Consultar_Disponible_Partida(Hdf_Fte_Financiamiento.Value.Trim(), Hdf_Programa.Value.Trim(), Cmb_Dependencia.SelectedValue.Trim(), Hdf_Partida.Value.Trim(), DateTime.Now.Year.ToString(), Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE);
                Session[P_Disponible] = Disponible_Partida;
                Actualiza_Disponible(0, Operacion_Comprometer);
                Cls_Ope_Tal_Administrar_Requisiciones_Negocio Administrar = new Cls_Ope_Tal_Administrar_Requisiciones_Negocio();
                Administrar.P_Requisicion_ID = Txt_Folio.Text.Replace(SubFijo_Requisicion, "");
                DataSet Data_Set = Administrar.Consulta_Observaciones();
                if (Data_Set != null && Data_Set.Tables[0].Rows.Count != 0) {
                    Div_Comentarios.Style.Add("display","inline");
                    Grid_Comentarios.DataSource = Data_Set;
                    Grid_Comentarios.DataBind();
                } else {
                    Div_Comentarios.Style.Add("display","none");
                    Grid_Comentarios.DataSource = null; ;
                    Grid_Comentarios.DataBind();
                }
            }

        #endregion
    
        #region PRESUPUESTOS

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
            ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
            ///en la busqueda del Modalpopup
            ///CREO: Gustavo Angeles
            ///FECHA_CREO: 9/Diciembre/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///*******************************************************************************
            private void Comprometer_O_Descomprometer_Disponible_A_Partida_En_Dt_Partidas(String Partida_ID, DataTable Partidas, String Operacion, double Cantidad) {
                double Disponible = 0;
                double Comprometido = 0;
                try {
                    DataRow[] Renglones = Partidas.Select("Partida_ID = '" + Partida_ID + "'");
                    //Si encuentra el presupuesto
                    if (Renglones.Length > 0) {
                        Disponible = double.Parse(Renglones[0]["MONTO_DISPONIBLE"].ToString());
                        Comprometido = double.Parse(Renglones[0]["MONTO_COMPROMETIDO"].ToString());
                        if (Operacion == Operacion_Comprometer) {
                            Disponible = Disponible - Cantidad;
                            Comprometido = Comprometido + Cantidad;
                        } else if (Operacion == Operacion_Descomprometer) {
                            Disponible = Disponible + Cantidad;
                            Comprometido = Comprometido - Cantidad;
                        }
                        Renglones[0]["MONTO_DISPONIBLE"] = Disponible.ToString();
                        Renglones[0]["MONTO_COMPROMETIDO"] = Comprometido.ToString();
                        Session[P_Dt_Partidas] = Partidas;
                    }//si no encuentra el presupuesto consulta en la base de datos
                } catch (Exception Ex) {
                    String Str = Ex.ToString();
                    Mostrar_Informacion(Str, true);
                }
            }
    
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Presupuesto
            ///DESCRIPCIÓN: Valores Iniciales delos Parametros.
            ///CREO: Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO: 29/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            private void Cargar_Datos_Presupuesto() {
                Cls_Tal_Parametros_Negocio Parametros_Negocio = new Cls_Tal_Parametros_Negocio();
                if (!String.IsNullOrEmpty(Hdf_No_Economico.Value.Trim()) && !String.IsNullOrEmpty(Hdf_Tipo_Bien.Value.Trim()))
                {
                    Parametros_Negocio = Parametros_Negocio.Consulta_Parametros_Presupuesto(Hdf_No_Economico.Value.Trim(), Hdf_Tipo_Bien.Value.Replace(" ","_"));

                    //Fuente de Financiamiento
                    Cls_Cat_SAP_Fuente_Financiamiento_Negocio Fte_Finan = new Cls_Cat_SAP_Fuente_Financiamiento_Negocio();
                    Fte_Finan.P_Fuente_Financiamiento_ID = Parametros_Negocio.P_Fuente_Financiamiento_ID;
                    DataTable Dt_FF = Fte_Finan.Consulta_Datos_Fuente_Financiamiento();
                    if (Dt_FF != null)
                    {
                        if (Dt_FF.Rows.Count > 0)
                        {
                            Hdf_Fte_Financiamiento.Value = Parametros_Negocio.P_Fuente_Financiamiento_ID;
                            Txt_Fte_Financiamiento.Text = Dt_FF.Rows[0][Cat_SAP_Fuente_Financiamiento.Campo_Clave].ToString().Trim() + " - " + Dt_FF.Rows[0][Cat_SAP_Fuente_Financiamiento.Campo_Descripcion].ToString().Trim();
                        }
                    }

                    //Partidas
                    Cls_Cat_SAP_Partidas_Especificas_Negocio Part_Neg = new Cls_Cat_SAP_Partidas_Especificas_Negocio();
                    Part_Neg.P_Partida_ID = Parametros_Negocio.P_Partida_ID;
                    DataTable Dt_Partida = Part_Neg.Consulta_Partida_Especifica();
                    if (Dt_Partida != null)
                    {
                        if (Dt_Partida.Rows.Count > 0)
                        {
                            Hdf_Partida.Value = Parametros_Negocio.P_Partida_ID;
                            Txt_Partida.Text = Dt_Partida.Rows[0][Cat_Sap_Partidas_Especificas.Campo_Clave].ToString().Trim() + " - " + Dt_Partida.Rows[0][Cat_Sap_Partidas_Especificas.Campo_Nombre].ToString().Trim();
                        }
                    }
                }
            }

        #endregion
    
        #region PRODUCTOS

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Actualizar_Disponible_Productos
            ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
            ///en la busqueda del Modalpopup
            ///CREO: Gustavo Angeles
            ///FECHA_CREO: 9/Diciembre/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///*******************************************************************************
            private void Actualizar_Disponible_Productos(String Campo_ID, String ID, DataTable Dt_Tabla) {
                if (Dt_Tabla != null && Dt_Tabla.Rows.Count > 0) {
                    DataRow[] Productos = Dt_Tabla.Select(Campo_ID + " = '" + ID + "'");
                    if (Productos.Length > 0) {
                        Txt_Producto_Servicio.Text = Productos[0]["NOMBRE"].ToString();
                        String Disponible = Productos[0]["DISPONIBLE"].ToString();
                        String Precio_Sin_Impuesto = Productos[0]["COSTO_UNITARIO"].ToString();
                        if (Cmb_Tipo.SelectedValue == "STOCK") {
                            Lbl_Disponible_Producto.Text = "Disponible: " + Disponible +
                                " / Precio aproximado: $ " + Precio_Sin_Impuesto + " + Impuesto";
                        } else {
                            Lbl_Disponible_Producto.Text = "Precio aproximado: $ " + Precio_Sin_Impuesto + " + Impuesto";
                        }
                    }
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
            ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
            ///en la busqueda del Modalpopup
            ///CREO: Gustavo Angeles
            ///FECHA_CREO: 9/Diciembre/2010 
            ///MODIFICO: Francisco A. Gallardo Castañeda
            ///FECHA_MODIFICO: Mayo/2012
            ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
            ///*******************************************************************************
            private void Comprometer_O_Descomprometer_Disponible_A_Productos_En_Dt_Productos(String Producto_ID, DataTable Productos, String Operacion, Double Cantidad) {
                Double Disponible = 0;
                Double Comprometido = 0;
                try {
                    DataRow[] Renglones = Productos.Select("Refaccion_ID = '" + Producto_ID + "'");
                    //Si encuentra el presupuesto
                    if (Renglones.Length > 0) {
                        Disponible = Double.Parse(Renglones[0]["DISPONIBLE"].ToString());
                        Comprometido = Double.Parse(Renglones[0]["COMPROMETIDO"].ToString());
                        if (Operacion == Operacion_Comprometer) {
                            Disponible = Disponible - Cantidad;
                            Comprometido = Comprometido + Cantidad;
                        } else if (Operacion == Operacion_Descomprometer) {
                            Disponible = Disponible + Cantidad;
                            Comprometido = Comprometido - Cantidad;
                        }
                        Renglones[0]["DISPONIBLE"] = Disponible.ToString();
                        Renglones[0]["COMPROMETIDO"] = Comprometido.ToString();
                        Session[P_Dt_Productos] = Productos;
                    }
                } catch (Exception Ex) {
                    String Str = Ex.ToString();
                    Mostrar_Informacion(Str, true);
                }
            }

        #endregion

    #endregion
   
    #region GRIDS

        /// *****************************************************************************************
        /// NOMBRE: Grid_Requisiciones_RowDataBound
        /// DESCRIPCIÓN:  
        /// CREÓ: Gustavo Angeles Cruz
        /// FECHA CREÓ: 11/Junio/2011
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        /// *****************************************************************************************
        protected void Grid_Requisiciones_RowDataBound(object sender, GridViewRowEventArgs e) {
            if (e.Row.RowType == DataControlRowType.DataRow) {
                String Folio = e.Row.Cells[1].Text.Trim();

                DataRow[] Renglon = ((DataTable)Session[P_Dt_Requisiciones]).Select("FOLIO = '" + Folio + "'");
                if (Renglon.Length > 0) {
                    e.Row.Cells[2].Style.Add("FONT color", "red");
                    //Style cs = new Style();
                    //cs.
                    //e.Row.Cells[2].Style.Add("backgolorround-color", "gray");
                    ImageButton Boton = (ImageButton)e.Row.FindControl("Btn_Alerta");
                    String Estatus = Renglon[0]["ESTATUS"].ToString().Trim();
                    //System.Windows.Forms.MessageBox.Show(Renglon[0]["FECHA_CREO"].ToString());
                    if (Estatus == EST_EN_CONSTRUCCION && Renglon[0]["ALERTA"].ToString().Trim() == "AMARILLO" || Estatus == EST_REVISAR)
                    {
                        Boton.ImageUrl = "../imagenes/gridview/circle_yellow.png";
                        Boton.Visible = true;

                        e.Row.Cells[2].Style.Add("foreground", "white");
                    } else {
                        Boton.Visible = false;
                    }
                    char[] chars = { ' ' };
                    //char []diagonal = {'/'};
                    String Fecha = Renglon[0]["FECHA_CREO"].ToString().Substring(0, 10);
                    char[] diagonal = { '/' };
                }
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Productos_Servicios_PageIndexChanging
        ///DESCRIPCIÓN:  
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        ///*******************************************************************************
        protected void Grid_Productos_Servicios_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            Grid_Productos_Servicios.DataSource = ((DataTable)Session[P_Dt_Productos_Servicios]);
            Grid_Productos_Servicios.PageIndex = e.NewPageIndex;
            Grid_Productos_Servicios.DataBind();
        }

        /// ******************************************************************************************
        /// NOMBRE: Grid_Requisiciones_Sorting
        /// DESCRIPCIÓN: Ordena las columnas en orden ascendente o descendente.
        /// CREÓ: Gustavo Angeles Cruz
        /// FECHA CREÓ: 11/Junio/2011
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        /// ******************************************************************************************
        protected void Grid_Requisiciones_Sorting(object sender, GridViewSortEventArgs e) {
            Grid_Sorting(Grid_Requisiciones, ((DataTable)Session[P_Dt_Requisiciones]), e);
        }

        /// *****************************************************************************************
        /// NOMBRE: Grid_Sorting
        /// DESCRIPCIÓN: Ordena las columnas en orden ascendente o descendente.
        /// CREÓ: Gustavo Angeles Cruz
        /// FECHA CREÓ: 11/Junio/2011
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        /// *****************************************************************************************
        private void Grid_Sorting(GridView Grid, DataTable Dt_Table, GridViewSortEventArgs e) {
            if (Dt_Table != null) {
                DataView Dv_Vista = new DataView(Dt_Table);
                String Orden = ViewState["SortDirection"].ToString();
                if (Orden.Equals("ASC")) {
                    Dv_Vista.Sort = e.SortExpression + " DESC";
                    ViewState["SortDirection"] = "DESC";
                } else {
                    Dv_Vista.Sort = e.SortExpression + " ASC";
                    ViewState["SortDirection"] = "ASC";
                }
                Grid.DataSource = Dv_Vista;
                Grid.DataBind();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Requisiciones_PageIndexChanging
        ///DESCRIPCIÓN:  
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        ///******************************************************************************* 
        protected void Grid_Requisiciones_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            Grid_Requisiciones.DataSource = ((DataTable)Session[P_Dt_Requisiciones]);
            Grid_Requisiciones.PageIndex = e.NewPageIndex;
            Grid_Requisiciones.DataBind();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Comentarios_SelectedIndexChanged
        ///DESCRIPCIÓN: 
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        ///*******************************************************************************
        protected void Grid_Comentarios_SelectedIndexChanged(object sender, EventArgs e) {
            GridViewRow Renglon = Grid_Comentarios.SelectedRow;
            Txt_Comentario.Text = Renglon.Cells[1].Text;
        }
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Comentarios_PageIndexChanging
        ///DESCRIPCIÓN:  
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        ///*******************************************************************************
        protected void Grid_Comentarios_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            Administrar_Requisicion = new Cls_Ope_Tal_Administrar_Requisiciones_Negocio();
            Administrar_Requisicion.P_Requisicion_ID = Txt_Folio.Text.Replace(SubFijo_Requisicion, "");
            DataSet Data_Set = Administrar_Requisicion.Consulta_Observaciones();
            Grid_Comentarios.DataSource = Data_Set;
            Grid_Comentarios.PageIndex = e.NewPageIndex;
            Grid_Comentarios.DataBind();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
        ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
        ///en la busqueda del Modalpopup
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        ///*******************************************************************************
        protected void Grid_Productos_Servicios_Modal_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Grid_Productos_Servicios_Modal.DataSource = Session[P_Dt_Productos_Servicios_Modal] as DataTable;
            Grid_Productos_Servicios_Modal.PageIndex = e.NewPageIndex;
            Grid_Productos_Servicios_Modal.DataBind();
            Modal_Busqueda_Prod_Serv.Show();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
        ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
        ///en la busqueda del Modalpopup
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        ///*******************************************************************************
        protected void Grid_Productos_Servicios_RowCommand(object sender, GridViewCommandEventArgs e)
        {

                String Comando = e.CommandName.ToString();
                if (Comando != "Page")
                {
                    int indice = Convert.ToInt32(e.CommandArgument);
                    //GridViewRow Renglon = Grid_Productos_Servicios.Rows[indice];
                    Grid_Productos_Servicios.SelectedIndex = indice;
                    String Tipo = Grid_Productos_Servicios.SelectedDataKey["Tipo"].ToString().Trim();
                    String Producto_Servicio_ID =
                        Grid_Productos_Servicios.SelectedDataKey[Cat_Tal_Refacciones.Campo_Refaccion_ID].ToString().Trim();
                    DataRow[] _DataRow = ((DataTable)Session[P_Dt_Productos_Servicios]).Select(Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Producto_Servicio_ID + "'");


                    if (_DataRow != null && _DataRow.Length > 0)
                    {
                        double Cantidad_Productos = double.Parse(_DataRow[0]["CANTIDAD"].ToString().Trim());
                        double Monto = double.Parse(_DataRow[0]["MONTO"].ToString().Trim());
                        double Monto_Total = double.Parse(_DataRow[0]["MONTO_TOTAL"].ToString().Trim());

                        double Monto_IEPS = double.Parse(_DataRow[0]["MONTO_IEPS"].ToString().Trim());
                        double Monto_IVA = double.Parse(_DataRow[0]["MONTO_IVA"].ToString().Trim());

                        String Partida_ID = _DataRow[0]["Partida_ID"].ToString().Trim();
                        //seleccionar el combo de partidas
                        switch (Comando)
                        {
                            case "Eliminar":
                                if (Cmb_Tipo.SelectedValue.Trim() == "STOCK")
                                {
                                    Comprometer_O_Descomprometer_Disponible_A_Productos_En_Dt_Productos(
                                        Producto_Servicio_ID, ((DataTable)Session[P_Dt_Productos]), Operacion_Descomprometer, Cantidad_Productos);
                                }
                                ((DataTable)Session[P_Dt_Productos_Servicios]).Rows.Remove(_DataRow[0]);
                                ((DataTable)Session[P_Dt_Productos_Servicios]).AcceptChanges();
                                Refrescar_Grid();
                                Calcular_Impuestos();
                                //Se actualizan los grids de partidas y productos disponibles
                                //@
                                //Actualizar_Grid_Partidas_Productos();
                                Actualiza_Disponible(Monto_Total, Operacion_Descomprometer);
                                break;
                            case "Mas":
                                if (Tipo == "PRODUCTO")
                                {
                                    double Monto_IEPS_A_Comprometer = Monto_IEPS / Cantidad_Productos;
                                    double Monto_IVA_A_Comprometer = Monto_IVA / Cantidad_Productos;
                                    double Monto_A_Comprometer = Monto / Cantidad_Productos;
                                    double Monto_Total_A_Comprometer = Monto_Total / Cantidad_Productos;

                                    bool Agregar_Producto = true;
                                    //@
                                    double Disponible_Partida = Consulta_Disponible();

                                    if (Disponible_Partida >= Monto_A_Comprometer)
                                    {
                                        if (Cmb_Tipo.SelectedValue == "STOCK")
                                        {
                                            int Disponible_Producto =
                                                Verifica_Disponible_De_Un_Producto_De_Stock_En_Dt_Productos(
                                                    Producto_Servicio_ID, ((DataTable)Session[P_Dt_Productos]));
                                            if (Disponible_Producto >= 1)
                                            {
                                                //Se comprometen los productos en el Dt_Productos, todo es virtual
                                                Comprometer_O_Descomprometer_Disponible_A_Productos_En_Dt_Productos(
                                                    Producto_Servicio_ID, ((DataTable)Session[P_Dt_Productos]), Operacion_Comprometer, 1);
                                            }
                                            else
                                            {
                                                Agregar_Producto = false;
                                                Mostrar_Informacion(
                                                    "La cantidad de productos no se encuentra disponible: Existencia [" +
                                                    Disponible_Producto + "]", true);
                                            }
                                        }
                                        if (Agregar_Producto)
                                        {
                                            //Se actualizan los productos
                                            _DataRow[0]["CANTIDAD"] = (Cantidad_Productos + 1);
                                            _DataRow[0]["MONTO"] = (Monto + Monto_A_Comprometer);
                                            _DataRow[0]["MONTO_IEPS"] = (Monto_IEPS + Monto_IEPS_A_Comprometer);
                                            _DataRow[0]["MONTO_IVA"] = (Monto_IVA + Monto_IVA_A_Comprometer);
                                            _DataRow[0]["MONTO_TOTAL"] = (Monto_Total + Monto_Total_A_Comprometer);
                                            Refrescar_Grid();
                                            Calcular_Impuestos();
                                            //Comprometer Presupuesto en P_Dt_Partidas
                                            //@
                                            Actualiza_Disponible(Monto_Total_A_Comprometer, Operacion_Comprometer);
                                        }
                                    }
                                    else
                                    {
                                        Mostrar_Informacion(
                                            "Presupuesto insuficiente en la partida: " +
                                            Txt_Partida.Text.Trim(), true);
                                    }
                                }
                                else if (Tipo == "SERVICIO")
                                {
                                    Mostrar_Informacion(
                                        "Los Servicios solo pueden tener en cantidad (1) uno", true);
                                }
                                break;
                            case "Menos":
                                if (Tipo == "PRODUCTO")
                                {
                                    int ct = int.Parse(_DataRow[0]["CANTIDAD"].ToString().Trim());
                                    if (ct > 1)
                                    {
                                        double Monto_IEPS_A_Descomprometer = Monto_IEPS / Cantidad_Productos;
                                        double Monto_IVA_A_Descomprometer = Monto_IVA / Cantidad_Productos;
                                        double Monto_A_Descomprometer = Monto / Cantidad_Productos;
                                        double Monto_Total_A_Descomprometer = Monto_Total / Cantidad_Productos;
                                        //@
                                        double Disponible_Partida = Consulta_Disponible();

                                        if (Cmb_Tipo.SelectedValue == "STOCK")
                                        {
                                            int Disponible_Producto =
                                                Verifica_Disponible_De_Un_Producto_De_Stock_En_Dt_Productos(
                                                    Producto_Servicio_ID, ((DataTable)Session[P_Dt_Productos]));
                                            //Se descomprometen los productos en el Dt_Productos, todo es virtual
                                            Comprometer_O_Descomprometer_Disponible_A_Productos_En_Dt_Productos(
                                                Producto_Servicio_ID, ((DataTable)Session[P_Dt_Productos]), Operacion_Descomprometer, 1);
                                        }
                                        //Se actualizan los productos
                                        _DataRow[0]["CANTIDAD"] = (Cantidad_Productos - 1);
                                        _DataRow[0]["MONTO"] = (Monto - Monto_A_Descomprometer);
                                        _DataRow[0]["MONTO_IEPS"] = (Monto_IEPS - Monto_IEPS_A_Descomprometer);
                                        _DataRow[0]["MONTO_IVA"] = (Monto_IVA - Monto_IVA_A_Descomprometer);
                                        _DataRow[0]["MONTO_TOTAL"] = (Monto_Total - Monto_Total_A_Descomprometer);

                                        //Comprometer Presupuesto en P_Dt_Partidas
                                        //@
                                        Actualiza_Disponible(Monto_Total_A_Descomprometer, Operacion_Descomprometer);
                                    }
                                }
                                else if (Tipo == "SERVICIO")
                                {
                                    //AQUI NO PASA NADA
                                }
                                break;
                        }
                        Refrescar_Grid();
                        Calcular_Impuestos();
                        Actualizar_Disponible_Productos("Refaccion_ID", Producto_Servicio_ID, ((DataTable)Session[P_Dt_Productos]));
                    }
                }
        }

    #endregion

    #region EVENTOS

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Imprimir_Req_Click
        ///DESCRIPCIÓN:
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        ///*******************************************************************************
        protected void Btn_Imprimir_Req_Click(object sender, ImageClickEventArgs e) {
            if (Txt_Folio.Text.Trim().Length > 0) {
                DataSet Ds_Reporte = null;
                Ds_Rpt_Tal_Requisicion Ds_Req = new Ds_Rpt_Tal_Requisicion();
                //DataTable Dt_Requisicion = null;
                Cls_Ope_Tal_Impresion_Requisiciones_Negocio Req_Negocio = new Cls_Ope_Tal_Impresion_Requisiciones_Negocio();
                DataTable Dt_Cabecera = new DataTable();
                DataTable Dt_Detalles = new DataTable();
                DataTable Dt_Solicitud = new DataTable();
                try {
                    String Requisicion_ID = Txt_Folio.Text.Replace("RQ-", "");
                    Req_Negocio.P_Requisicion_ID = Requisicion_ID.Trim();
                    Dt_Cabecera = Req_Negocio.Consultar_Requisiciones();
                    Dt_Detalles = Req_Negocio.Consultar_Requisiciones_Detalles();

                    if (!String.IsNullOrEmpty(Dt_Cabecera.Rows[0]["NO_SOLICITUD"].ToString())) {
                        Cls_Rpt_Tal_Solicitud_Servicio_Negocio Rpt = new Cls_Rpt_Tal_Solicitud_Servicio_Negocio();
                        Rpt.P_No_Solicitud = Convert.ToInt32(Dt_Cabecera.Rows[0]["NO_SOLICITUD"].ToString());
                        Rpt.P_Tipo_Bien = Hdf_Tipo_Bien.Value;
                        Dt_Solicitud = Rpt.Consulta_Datos_Solicitud();
                    } else {
                        Dt_Solicitud = Ds_Req.Tables["DT_SERVICIO"].Clone();
                    }

                    Ds_Reporte = new DataSet();
                    Dt_Cabecera.TableName = "REQUISICION";
                    Dt_Detalles.TableName = "DETALLES";
                    Dt_Solicitud.TableName = "DT_SERVICIO";
                    Ds_Reporte.Tables.Add(Dt_Cabecera.Copy());
                    Ds_Reporte.Tables.Add(Dt_Detalles.Copy());
                    Ds_Reporte.Tables.Add(Dt_Solicitud.Copy());
                    Ds_Reporte.Tables.Add(new Cls_Tal_Parametros_Negocio().Obtener_Tabla_Reporte(this.Server).Copy());
                    Generar_Reporte(ref Ds_Reporte, "Rpt_Tal_Requisiciones.rpt", "Requisicion.pdf");
                } catch (Exception Ex) {
                    Mostrar_Informacion(Ex.Message.ToString(), true);
                }
            } else {
                Mostrar_Informacion("Seleccione una Requisición", true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
        ///DESCRIPCIÓN: 
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        ///*******************************************************************************
        protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e) {
            if (Btn_Nuevo.ToolTip == "Nuevo") {                
                String[] Datos_Combo = { EST_EN_CONSTRUCCION, EST_GENERADA };
                Llenar_Combo(Cmb_Estatus, Datos_Combo);
                Limpiar_Modal_Produtos_Servicios();
                Limpiar_Grids_Y_DataTables();
                Limpiar_Cajas_Texto();
                Llenar_Combos_Generales();
                Habilitar_Controles("Nuevo");
                Div_Listado_Requisiciones.Style.Add("display","none");
                Div_Contenido.Style.Add("display", "inline");
                Modal_Busqueda_Prod_Serv.Hide();
                Construir_DataTables();
                Cmb_Tipo.SelectedValue = "TRANSITORIA";
                //@
                //Actualizar_Grid_Partidas_Productos();
                Session.Remove(P_Disponible);
            } else if (Btn_Nuevo.ToolTip == "Dar de Alta") {
                if (Validaciones(true)) {
                    Cargar_Datos_Proyectos_Programas();
                    //AGREGAR VALORES A CAPA DE NEGOCIO Y LLAMAR INSERTAR
                    Administrar_Requisicion = new Cls_Ope_Tal_Administrar_Requisiciones_Negocio();
                    Administrar_Requisicion.P_Requisicion_ID =
                        JAPAMI.Generar_Requisicion.Datos.
                        Cls_Ope_Com_Requisiciones_Datos.
                        Obtener_Consecutivo(Ope_Tal_Requisiciones.Campo_Requisicion_ID, Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones).ToString();
                    char[] ch = { ' ' };
                    DataRow[] Rows = ((DataTable)Session[P_Dt_Programas]).Select("PROYECTO_PROGRAMA_ID ='" + Hdf_Programa.Value + "'");
                    String PEP = "";//Rows[0]["ELEMENTO_PEP"].ToString();
                    String Codigo = "";
                    Codigo += Txt_Fte_Financiamiento.Text.Split(ch)[0] + "-1.1.1-";//area SE SUSTITUYE EN LA CAPA DE DATOS
                    Codigo += Txt_Programa.Text.Split(ch)[0] + "-";
                    Codigo += Cmb_Dependencia.SelectedItem.Text.Split(ch)[0] + "-";
                    Codigo += Txt_Partida.Text.Split(ch)[0];
                    //
                    Requisicion_Negocio = new Cls_Ope_Tal_Requisiciones_Negocio();
                    Requisicion_Negocio.P_Comentarios = Txt_Comentario.Text;
                    Requisicion_Negocio.P_Folio = Txt_Folio.Text;
                    Requisicion_Negocio.P_Codigo_Programatico = Codigo;
                    Requisicion_Negocio.P_Elemento_PEP = PEP;
                    //Requisicion_Negocio.P_Area_ID = Cmb_Area.SelectedValue;
                    Requisicion_Negocio.P_Dependencia_ID = Cmb_Dependencia.SelectedValue;
                    Requisicion_Negocio.P_Estatus = Cmb_Estatus.SelectedValue;
                    Requisicion_Negocio.P_Tipo = Cmb_Tipo.SelectedValue;
                    Requisicion_Negocio.P_Subtotal = Txt_Subtotal.Text;
                    Requisicion_Negocio.P_Total = Txt_Total.Text;
                    Requisicion_Negocio.P_IVA = Txt_IVA.Text;
                    Requisicion_Negocio.P_IEPS = "0.0";
                    Requisicion_Negocio.P_Dt_Productos_Servicios = Session[P_Dt_Productos_Servicios] as DataTable;
                    Requisicion_Negocio.P_Tipo_Articulo = "PRODUCTO";
                    Requisicion_Negocio.P_Fase = "REQUISICION";
                    Requisicion_Negocio.P_Justificacion_Compra = ((Txt_Justificacion.Text.Trim().Length > 0) ? Txt_Justificacion.Text : "Refacciones para Servicio");
                    Requisicion_Negocio.P_Especificacion_Productos = Txt_Especificaciones.Text;
                    Requisicion_Negocio.P_Verificacion_Entrega = (Chk_Verificar.Checked) ? "SI" : "NO";
                    Requisicion_Negocio.P_Partida_ID = Hdf_Partida.Value;
                    Requisicion_Negocio.P_Dt_Productos_Almacen = ((DataTable)Session[P_Dt_Productos]);
                    Requisicion_Negocio.P_Dt_Partidas = ((DataTable)Session[P_Dt_Partidas]);
                    Requisicion_Negocio.P_Fuente_Financiamiento = Hdf_Fte_Financiamiento.Value;
                    Requisicion_Negocio.P_Proyecto_Programa_ID = Hdf_Programa.Value;
                    Requisicion_Negocio.P_Anio_Presupuesto = DateTime.Now.Year;
                    //
                    Requisicion_Negocio.P_Ramo_33 = "NO";//esto se debe tomar de un combo
                    Requisicion_Negocio.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                    //@
                    String Consecutivo = Requisicion_Negocio.Proceso_Insertar_Requisicion();
                    if (!string.IsNullOrEmpty(Consecutivo)) {
                        Consecutivo = Consecutivo.Replace("EXITO", "");
                        Consecutivo = Consecutivo.Replace("-", "");
                        Txt_Folio.Text = "RQ-" + Consecutivo;
                        ScriptManager.RegisterStartupScript(
                            this, this.GetType(), "Requisiciones", "alert('" + Consecutivo + "');", true);
                        if (Requisicion_Negocio.P_Tipo == "TRANSITORIA") {
                            bool Esta_En_Rango_Caja_Chica = Requisicion_Negocio.Verificar_Rango_Caja_Chica();
                            if (Esta_En_Rango_Caja_Chica) {
                                String Mensaje = "Es posible que su solicitud sea rechazada, debido a que el requerimiento puede ser cubierto por CAJA CHICA";
                                Mostrar_Informacion(Mensaje, true, COLOR_AZUL);
                            }
                        }
                    } else {
                        ScriptManager.RegisterStartupScript(
                            this, this.GetType(), "Requisiciones", "alert('No se pudo registrar la requisición');", true);
                    }
                    Llenar_Grid_Requisiciones();
                    Habilitar_Controles("Uno");
                } else {
                    Mostrar_Informacion(Informacion, true);
                }
            }//fin if Nuevo
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
        ///DESCRIPCIÓN: 
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        ///*******************************************************************************
        protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e) {
            Limpiar_Modal_Produtos_Servicios();
            if (Btn_Modificar.ToolTip == "Modificar") {
                if (Txt_Folio.Text.Trim() != "") {
                    if (Cmb_Estatus.SelectedValue == EST_EN_CONSTRUCCION || Cmb_Estatus.SelectedValue == EST_REVISAR || (Cmb_Estatus.SelectedValue == EST_ALMACEN && Cmb_Tipo.SelectedValue == "STOCK" )) {
                        String Estatus_Tmp = Cmb_Estatus.SelectedValue;
                        String[] Datos_Combo = { EST_EN_CONSTRUCCION, EST_GENERADA, EST_CANCELADA };
                        Cmb_Estatus.SelectedValue = Estatus_Tmp;
                        Llenar_Combo(Cmb_Estatus, Datos_Combo);
                        Habilitar_Controles("Modificar");
                    } else {
                        Mostrar_Informacion("No se puede modificar una requisición con el estatus seleccionado", true);
                    }
                } else {
                    Mostrar_Informacion("Debe seleccionar una requisición", true);
                }
            } else if (Btn_Modificar.ToolTip == "Actualizar") {
                //Proceso para modificar
                if (Validaciones(true)) {
                    Cargar_Datos_Proyectos_Programas();
                    //Cargar datos Negocio
                    Requisicion_Negocio = new Cls_Ope_Tal_Requisiciones_Negocio();
                    //Requisicion_Negocio.P_Area_ID = Cmb_Area.SelectedValue;
                    Requisicion_Negocio.P_Dependencia_ID = Cmb_Dependencia.SelectedValue;
                    Requisicion_Negocio.P_Estatus = Cmb_Estatus.SelectedValue.Trim();
                    Requisicion_Negocio.P_Tipo = Cmb_Tipo.SelectedValue;
                    Requisicion_Negocio.P_Folio = Txt_Folio.Text;
                    Requisicion_Negocio.P_Subtotal = Txt_Subtotal.Text;
                    Requisicion_Negocio.P_IVA = Txt_IVA.Text;
                    Requisicion_Negocio.P_IEPS = "0.0";
                    Requisicion_Negocio.P_Total = Txt_Total.Text;
                    Requisicion_Negocio.P_Comentarios = Txt_Comentario.Text;
                    Requisicion_Negocio.P_Dt_Productos_Servicios = Session[P_Dt_Productos_Servicios] as DataTable;
                    Requisicion_Negocio.P_Requisicion_ID = Txt_Folio.Text.Replace(SubFijo_Requisicion, "");
                    Requisicion_Negocio.P_Justificacion_Compra = ((Txt_Justificacion.Text.Trim().Length > 0) ? Txt_Justificacion.Text : "Refacciones para Servicio");
                    Requisicion_Negocio.P_Especificacion_Productos = Txt_Especificaciones.Text;
                    Requisicion_Negocio.P_Verificacion_Entrega = (Chk_Verificar.Checked) ? "SI" : "NO";
                    Requisicion_Negocio.P_Fuente_Financiamiento = Hdf_Fte_Financiamiento.Value;
                    Requisicion_Negocio.P_Proyecto_Programa_ID = Hdf_Programa.Value;
                    Requisicion_Negocio.P_Anio_Presupuesto = DateTime.Now.Year;

                    if (Requisicion_Negocio.P_Estatus == "CANCELADA") {
                        foreach (DataRow Renglon in ((DataTable)Session[P_Dt_Productos_Servicios]).Rows) {
                            String Producto_Servicio_ID = Renglon[Cat_Tal_Refacciones.Campo_Refaccion_ID].ToString().Trim();
                            double Cantidad_Productos = double.Parse(Renglon["CANTIDAD"].ToString().Trim());
                            double Monto_Total = double.Parse(Renglon["MONTO_TOTAL"].ToString().Trim());
                            String Partida_ID = Renglon["Partida_ID"].ToString().Trim();
                            //Se descompromete los presupuestos en P_Dt_Partidas                                    
                            Comprometer_O_Descomprometer_Disponible_A_Partida_En_Dt_Partidas(Partida_ID, ((DataTable)Session[P_Dt_Partidas]), Operacion_Descomprometer, Monto_Total);
                            //Se descompromete los Productos en P_Dt_Productos
                            if (Requisicion_Negocio.P_Tipo == "STOCK") {
                                Comprometer_O_Descomprometer_Disponible_A_Productos_En_Dt_Productos(
                                    Producto_Servicio_ID, ((DataTable)Session[P_Dt_Productos]), Operacion_Descomprometer, Cantidad_Productos);
                            }
                        }
                    }
                    Requisicion_Negocio.P_Dt_Productos_Almacen = ((DataTable)Session[P_Dt_Productos]);
                    Requisicion_Negocio.P_Dt_Partidas = ((DataTable)Session[P_Dt_Partidas]);
                    if (!String.IsNullOrEmpty(Hdf_Total_Req.Value))
                    {
                        if (Double.Parse(Hdf_Total_Req.Value) < Double.Parse(Txt_Total.Text.Trim()))
                        {
                            Requisicion_Negocio.P_Total_Diferencia = (Double.Parse(Txt_Total.Text.Trim()) - Double.Parse(Hdf_Total_Req.Value)).ToString(); 
                            Requisicion_Negocio.P_Modificar_RQ = true;
                        }
                        else
                        {
                            if (Cmb_Estatus.SelectedItem.Value.Trim().Equals("CANCELADA")) { Requisicion_Negocio.P_Total = (Double.Parse(Txt_Total.Text)).ToString(); }
                            Requisicion_Negocio.P_Modificar_RQ = false;
                        }
                    }
                    else
                    {
                        Requisicion_Negocio.P_Modificar_RQ = false;
                    }
                    Requisicion_Negocio.Proceso_Actualizar_Requisicion();
                    Administrar_Requisicion = new Cls_Ope_Tal_Administrar_Requisiciones_Negocio();
                    Administrar_Requisicion.P_Requisicion_ID = Txt_Folio.Text.Replace(SubFijo_Requisicion, "");
                    DataSet Data_Set = Administrar_Requisicion.Consulta_Observaciones();
                    Grid_Comentarios.DataSource = Data_Set;
                    Grid_Comentarios.DataBind();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Requisiciones", "alert('Requisición modificada');", true);
                    Llenar_Grid_Requisiciones();
                    Habilitar_Controles("Uno");
                } else {
                    Mostrar_Informacion(Informacion, true);
                }
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
        ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
        ///en la busqueda del Modalpopup
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e) {
            if (Btn_Salir.ToolTip == "Cancelar") {
                if (Btn_Nuevo.ToolTip == "Dar de Alta") {
                    Limpiar_Formulario();
                }

                Habilitar_Controles("Inicial");
                String[] Datos_Combo = 
                    { EST_EN_CONSTRUCCION, EST_GENERADA, EST_CANCELADA, EST_AUTORIZADA, 
                      EST_FILTRADA,EST_RECHAZADA,EST_REVISAR, EST_COTIZADA, EST_COTIZADA_RECHAZADA, EST_COMPRA, EST_ALMACEN, EST_TERMINADA, "CONFIRMADA" };
                Llenar_Combo(Cmb_Estatus, Datos_Combo);
                if (Session[Estatus] != null) {
                    Cmb_Estatus.SelectedValue = Session[Estatus].ToString();
                }
                Llenar_Grid_Requisiciones();
                Habilitar_Controles("Uno");
            } else {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
        ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
        ///en la busqueda del Modalpopup
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        ///*******************************************************************************
        protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e) {
            Habilitar_Controles("Inicial");
            Llenar_Grid_Requisiciones();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Ibtn_Buscar_Producto_Click
        ///DESCRIPCIÓN: 
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        ///*******************************************************************************
        protected void Ibtn_Buscar_Producto_Click(object sender, ImageClickEventArgs e) {
            //if (Validaciones(false)) {
                Limpiar_Modal_Produtos_Servicios();
                Modal_Busqueda_Prod_Serv.Show();
            //} else {
                //Mostrar_Informacion(Informacion, true);
            //}
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:IBtn_MDP_Prod_Serv_Buscar_Click
        ///DESCRIPCIÓN:  
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        ///*******************************************************************************
        private void Evento_IBtn_MDP_Prod_Serv_Buscar() {
            Limpiar_Modal_Produtos_Servicios();
            DataTable Dt_Prod_Srv_Tmp = null;
            Requisicion_Negocio = new Cls_Ope_Tal_Requisiciones_Negocio();

            Requisicion_Negocio.P_Tipo = Cmb_Tipo.SelectedValue;
            Requisicion_Negocio.P_Partida_ID = Hdf_Partida.Value;

            if (Cmb_Tipo.SelectedValue == "STOCK") {
                Requisicion_Negocio.P_Tipo = "STOCK";
                Requisicion_Negocio.P_Nombre_Producto_Servicio = Txt_Nombre.Text;
                Grid_Productos_Servicios_Modal.Columns[3].Visible = true;
                Dt_Prod_Srv_Tmp = Requisicion_Negocio.Consultar_Productos();
                Grid_Productos_Servicios_Modal.Columns[4].Visible = true;
                Grid_Productos_Servicios_Modal.Columns[5].Visible = true;
                Grid_Productos_Servicios_Modal.Columns[7].Visible = true;
            } else if (Cmb_Tipo.SelectedValue == "TRANSITORIA") {
                Requisicion_Negocio.P_Tipo = "TRANSITORIO";
                Requisicion_Negocio.P_Nombre_Producto_Servicio = Txt_Nombre.Text;
                Grid_Productos_Servicios_Modal.Columns[3].Visible = false;
                Grid_Productos_Servicios_Modal.Columns[4].Visible = false;
                Grid_Productos_Servicios_Modal.Columns[5].Visible = false;
                Grid_Productos_Servicios_Modal.Columns[7].Visible = false;
                Grid_Productos_Servicios_Modal.Columns[3].Visible = true;
                Dt_Prod_Srv_Tmp = Requisicion_Negocio.Consultar_Productos();
            }

            if (Dt_Prod_Srv_Tmp != null && Dt_Prod_Srv_Tmp.Rows.Count > 0) {
                Session[P_Dt_Productos_Servicios_Modal] = Dt_Prod_Srv_Tmp;
                Grid_Productos_Servicios_Modal.DataSource = Dt_Prod_Srv_Tmp;
                Grid_Productos_Servicios_Modal.DataBind();
                Grid_Productos_Servicios_Modal.Visible = true;
            }
            Txt_Nombre.Text = "";
            Modal_Busqueda_Prod_Serv.Show();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:IBtn_MDP_Prod_Serv_Buscar_Click
        ///DESCRIPCIÓN:  
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        ///*******************************************************************************
        protected void IBtn_MDP_Prod_Serv_Buscar_Click(object sender, ImageClickEventArgs e) {
            Evento_IBtn_MDP_Prod_Serv_Buscar();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
        ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
        ///en la busqueda del Modalpopup
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        ///*******************************************************************************
        protected void IBtn_MDP_Prod_Serv_Cerrar_Click(object sender, ImageClickEventArgs e) {
            Limpiar_Modal_Produtos_Servicios();
            Modal_Busqueda_Prod_Serv.Hide();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
        ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
        ///en la busqueda del Modalpopup
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        ///*******************************************************************************
        protected void Ibtn_Agregar_Producto_Click(object sender, ImageClickEventArgs e) {
            if (Txt_Producto_Servicio.Text.Length > 0 && Txt_Cantidad.Text.Length > 0) {
                if (Busca_Productos_Servicios_Duplicados(Session[PS_ID].ToString())) {
                    Mostrar_Informacion("La Refacción ya se encuentra en la lista", true);
                    return;
                }
                Evento_Boton_Agregar_Producto();
            } else {
                Mostrar_Informacion("Debe buscar una refacción para ser agregado e indicar la cantidad solicitada.", true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Lnk_Observaciones_Click
        ///DESCRIPCIÓN: Lnk_Observaciones_Click
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        ///*******************************************************************************
        protected void Lnk_Observaciones_Click(object sender, EventArgs e) {
            if (Lnk_Observaciones.Text == "Mostrar") {
                Administrar_Requisicion = new Cls_Ope_Tal_Administrar_Requisiciones_Negocio();
                Administrar_Requisicion.P_Requisicion_ID = Txt_Folio.Text.Replace(SubFijo_Requisicion, "");
                DataSet Data_Set = Administrar_Requisicion.Consulta_Observaciones();
                if (Data_Set != null && Data_Set.Tables[0].Rows.Count != 0) {
                    Div_Comentarios.Style.Add("display","inline");
                    Grid_Comentarios.DataSource = Data_Set;
                    Grid_Comentarios.DataBind();
                }
            } 
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Cmb_Tipo_SelectedIndexChanged
        ///DESCRIPCIÓN: 
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        ///*******************************************************************************
        protected void Cmb_Tipo_SelectedIndexChanged(object sender, EventArgs e) {
            if (Cmb_Tipo.SelectedIndex > 0) {
                Consultar_Presuesto();
            } else {
                Lbl_Disponible_Partida.Text = "$ 0.00";
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Cerrar_Click
        ///DESCRIPCIÓN: 
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        ///*******************************************************************************
        protected void Btn_Cerrar_Click(object sender, EventArgs e) {
            Limpiar_Modal_Produtos_Servicios();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Listar_Requisiciones_Click
        ///DESCRIPCIÓN: 
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        ///*******************************************************************************
        protected void Btn_Listar_Requisiciones_Click(object sender, ImageClickEventArgs e) {
            Llenar_Grid_Requisiciones();
            Habilitar_Controles("Uno");
        }
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Presuesto
        ///DESCRIPCIÓN: 
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        ///*******************************************************************************
        protected void Consultar_Presuesto() {
            Lbl_Partida.Text = "";
            Lbl_Clave.Text = "";
            Lbl_Disponible.Text = "";
            Lbl_Fecha_Asignacion.Text = "";
            Lbl_Disponible_Producto.Text = "Disponible: 0 / Precio aproximado: $ 0.00";
            Txt_Producto_Servicio.Text = "";
            Requisicion_Negocio = new Cls_Ope_Tal_Requisiciones_Negocio();
            //Cls_Tal_Parametros_Negocio Parametros_Negocio = new Cls_Tal_Parametros_Negocio();
            //Parametros_Negocio = Parametros_Negocio.Consulta_Parametros();
            Requisicion_Negocio.P_Dependencia_ID = Cmb_Dependencia.SelectedValue.Trim();
            //Requisicion_Negocio.P_Proyecto_Programa_ID = Parametros_Negocio.P_Programa_ID.ToString();
            //Requisicion_Negocio.P_Fuente_Financiamiento = Parametros_Negocio.P_Fuente_Financiamiento_ID.ToString();
            //Requisicion_Negocio.P_Partida_ID = Parametros_Negocio.P_Partida_ID.ToString();
            Requisicion_Negocio.P_Proyecto_Programa_ID = Hdf_Programa.Value.Trim();
            Requisicion_Negocio.P_Fuente_Financiamiento = Hdf_Fte_Financiamiento.Value.Trim();
            Requisicion_Negocio.P_Partida_ID = Hdf_Partida.Value.Trim();
            Requisicion_Negocio.P_Anio_Presupuesto = DateTime.Now.Year;           
            //DataTable Dt_Presupuesto = null;
            double Disponible_Partida = Cls_Ope_Psp_Manejo_Presupuesto.Consultar_Disponible_Partida(Hdf_Fte_Financiamiento.Value.Trim(), Hdf_Programa.Value.Trim(), Cmb_Dependencia.SelectedValue.Trim(), Hdf_Partida.Value.Trim(), DateTime.Now.Year.ToString(), Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE);
            //double Disponible_Partida = Cls_Ope_Psp_Manejo_Presupuesto.Consultar_Importes_Presupuesto_Aprobado (Cmb_Dependencia.SelectedValue.Trim(), Hdf_Fte_Financiamiento.Value.Trim(), "", "", Hdf_Partida.Value.Trim(), DateTime.Now.Year, Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE);

            Session[P_Disponible] = Disponible_Partida;
            if (Disponible_Partida > 0) {
                String Mto_Disponible = String.Format("{0:n}", Disponible_Partida);
                Lbl_Disponible_Partida.Text = " $ " + Mto_Disponible;
            } else {
                Lbl_Disponible_Partida.Text = "Sin presupuesto asignado";
            }
        }

        /// *****************************************************************************************
        /// NOMBRE: Txt_Nombre_TextChanged
        /// DESCRIPCIÓN:  
        /// CREÓ: Gustavo Angeles Cruz
        /// FECHA CREÓ: 11/Junio/2011
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        /// *****************************************************************************************
        protected void Txt_Nombre_TextChanged(object sender, EventArgs e)
        {
            Evento_IBtn_MDP_Prod_Serv_Buscar();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Seleccionar_Requisicion_Click
        ///DESCRIPCIÓN:  
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        ///*******************************************************************************
        protected void Btn_Seleccionar_Requisicion_Click(object sender, ImageClickEventArgs e) {
            String No_Requisicion = ((ImageButton)sender).CommandArgument;
            Evento_Grid_Requisiciones_Seleccionar(No_Requisicion);
            Cargar_Datos_Presupuesto();
            Consultar_Presuesto();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
        ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
        ///en la busqueda del Modalpopup
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        ///*******************************************************************************
        protected void Btn_Seleccionar_Producto_Click(object sender, ImageClickEventArgs e)
        {
            String ID = ((ImageButton)sender).CommandArgument;
            Session[PS_ID] = ID;
            Actualizar_Disponible_Productos("ID", ID, ((DataTable)Session[P_Dt_Productos_Servicios_Modal]));
            Session[P_Dt_Productos_Servicios_Modal] = null;
            Grid_Productos_Servicios_Modal.DataSource = null;
            Grid_Productos_Servicios_Modal.DataBind();
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN : Cargar_Ventana_Emergente_Busqueda_Refacciones
        ///DESCRIPCIÓN          : Establece el evento onclik del control para abrir la ventana emergente
        ///PARAMETROS: 
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 21/Otubre/2011
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        private void Cargar_Ventana_Emergente_Busqueda_Solicitudes()
        {
            String Ventana_Modal = "Abrir_Ventana_Modal('Ventanas_Emergentes/Frm_Busqueda_Solicitudes_Servicio.aspx";
            String Propiedades = ", 'center:yes;resizable:no;status:no;dialogWidth:680px;dialogHide:true;help:no;scroll:no');";
            Btn_Buscar_Solicitud.Attributes.Add("onclick", Ventana_Modal + "?Fecha=False'" + Propiedades);
        }

        protected void Btn_Buscar_Solicitud_Click(object sender, ImageClickEventArgs e) {
            if (Session["SERVICIO_SELECCIONADO"] != null) {
                GridViewRow Servicio = (GridViewRow)Session["SERVICIO_SELECCIONADO"];
                Hdf_No_Solicitud.Value = HttpUtility.HtmlDecode(Servicio.Cells[1].Text).Trim();
                Hdf_No_Economico.Value = HttpUtility.HtmlDecode(Servicio.Cells[4].Text).Trim();
                Hdf_Tipo_Bien.Value = HttpUtility.HtmlDecode(Servicio.Cells[6].Text).Trim();
                Txt_Solicitud.Text = "Folio: " + HttpUtility.HtmlDecode(Servicio.Cells[2].Text).Trim();
                Txt_Solicitud.Text += " / No. Economico: " + Hdf_No_Economico.Value;
                Txt_Solicitud.Text += " / Descripción: " + HttpUtility.HtmlDecode(Servicio.Cells[5].Text).Trim();
                Session.Remove("SERVICIO_SELECCIONADO");
                Txt_Justificacion.Text = "Requisición de Refacciones para Reparación de Servicio con Folio: " + HttpUtility.HtmlDecode(Servicio.Cells[2].Text).Trim() + " / No. Economico: " + Hdf_No_Economico.Value;
                if (!String.IsNullOrEmpty(Hdf_No_Solicitud.Value)) { Cargar_Datos_Solicitud_Servicio(); }
            }
        }

        protected void Cargar_Datos_Solicitud_Servicio() { 
            Cls_Ope_Tal_Solicitud_Servicio_Negocio Sol_Negocio = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
            Sol_Negocio.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
            Sol_Negocio = Sol_Negocio.Consultar_Detalles_Solicitud_Servicio();
            Cmb_Dependencia.SelectedIndex = Cmb_Dependencia.Items.IndexOf(Cmb_Dependencia.Items.FindByValue(Sol_Negocio.P_Dependencia_ID));
            Cls_Cat_Com_Proyectos_Programas_Negocio Proy_Neg = new Cls_Cat_Com_Proyectos_Programas_Negocio();
            Proy_Neg.P_Proyecto_Programa_ID = Sol_Negocio.P_Programa_Proyecto_ID;
            DataTable Dt_Programa = Proy_Neg.Consulta_Programas_Proyectos();
            if (Dt_Programa != null) {
                if (Dt_Programa.Rows.Count > 0) {
                    Hdf_Programa.Value = Sol_Negocio.P_Programa_Proyecto_ID;
                    Txt_Programa.Text = Dt_Programa.Rows[0][Cat_Com_Proyectos_Programas.Campo_Clave].ToString().Trim() + " - " + Dt_Programa.Rows[0][Cat_Com_Proyectos_Programas.Campo_Descripcion].ToString().Trim();
                }
            }
            Cargar_Datos_Presupuesto();
            Consultar_Presuesto();
        }

    #endregion

    #region REPORTES

        /// *****************************************************************************************
        /// NOMBRE: Generar_Reporte
        /// DESCRIPCIÓN: 
        /// CREÓ: Gustavo Angeles Cruz
        /// FECHA CREÓ: 11/Junio/2011
        ///MODIFICO:Jesus Toledo
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        /// *****************************************************************************************
        protected void Generar_Reporte(ref DataSet Ds_Datos, String Nombre_Plantilla_Reporte, String Nombre_Reporte_Generar) {
            ReportDocument Reporte = new ReportDocument();//Variable de tipo reporte.
            String Ruta = String.Empty;//Variable que almacenara la ruta del archivo del crystal report. 
            try {
                Ruta = @Server.MapPath("../Rpt/Servicios_Generales/" + Nombre_Plantilla_Reporte);
                Reporte.Load(Ruta);

                if (Ds_Datos is DataSet) {
                    if (Ds_Datos.Tables.Count > 0) {
                        Reporte.SetDataSource(Ds_Datos);
                        Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar);
                        Mostrar_Reporte(Nombre_Reporte_Generar);
                    }
                }
            } catch (Exception Ex) {
                throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
            }
        }

        /// *****************************************************************************************
        /// NOMBRE: Exportar_Reporte_PDF
        /// DESCRIPCIÓN:  
        /// CREÓ: Gustavo Angeles Cruz
        /// FECHA CREÓ: 11/Junio/2011
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        /// *****************************************************************************************
        protected void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte) {
            ExportOptions Opciones_Exportacion = new ExportOptions();
            DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
            PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

            try {
                if (Reporte is ReportDocument) {
                    Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte);
                    Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                    Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                    Reporte.Export(Opciones_Exportacion);
                }
            }  catch (Exception Ex) {
                throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
            }
        }

        /// *****************************************************************************************
        /// NOMBRE: Mostrar_Reporte
        /// DESCRIPCIÓN:  
        /// CREÓ: Gustavo Angeles Cruz
        /// FECHA CREÓ: 11/Junio/2011
        ///MODIFICO: Francisco A. Gallardo Castañeda
        ///FECHA_MODIFICO: Mayo/2012
        ///CAUSA_MODIFICACIÓN: Adecuación a Taller Mpal.
        /// *****************************************************************************************
        protected void Mostrar_Reporte(String Nombre_Reporte) {
            String Pagina = "../../Reporte/";

            try {
                Pagina = Pagina + Nombre_Reporte;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window",
                    "window.open('" + Pagina + "', 'Requisición','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            } catch (Exception Ex) {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Menos_Click
        ///DESCRIPCIÓN: Btn_Menos_Click
        ///CREO: David Herrera Rincón
        ///FECHA_CREO: 27/Febrero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Menos_Click(object sender, ImageClickEventArgs e)
        {
            //Asignamos el valor del row
            Grid_Productos_Servicios.SelectedIndex = int.Parse(((ImageButton)sender).CommandArgument);
            String Tipo = Grid_Productos_Servicios.SelectedDataKey["Tipo"].ToString().Trim();
            String Producto_Servicio_ID = Grid_Productos_Servicios.SelectedDataKey["REFACCION_ID"].ToString().Trim();
            DataRow[] _DataRow = ((DataTable)Session[P_Dt_Productos_Servicios]).Select("REFACCION_ID = '" + Producto_Servicio_ID + "'");

            try
            {
                if (_DataRow != null && _DataRow.Length > 0)
                {
                    Double Cantidad_Productos = Double.Parse(_DataRow[0]["CANTIDAD"].ToString().Trim());
                    Double Monto = Double.Parse(_DataRow[0]["MONTO"].ToString().Trim());
                    Double Monto_Total = Double.Parse(_DataRow[0]["MONTO_TOTAL"].ToString().Trim());

                    Double Monto_IEPS = Double.Parse(_DataRow[0]["MONTO_IEPS"].ToString().Trim());
                    Double Monto_IVA = Double.Parse(_DataRow[0]["MONTO_IVA"].ToString().Trim());

                    String Partida_ID = _DataRow[0]["Partida_ID"].ToString().Trim();
                    //seleccionar el combo de partidas

                    if (Tipo == "PRODUCTO")
                    {
                        Double ct = Double.Parse(_DataRow[0]["CANTIDAD"].ToString().Trim());
                        if (ct > 1)
                        {
                            Double Monto_IEPS_A_Descomprometer = Monto_IEPS / Cantidad_Productos;
                            Double Monto_IVA_A_Descomprometer = Monto_IVA / Cantidad_Productos;
                            Double Monto_A_Descomprometer = Monto / Cantidad_Productos;
                            Double Monto_Total_A_Descomprometer = Monto_Total / Cantidad_Productos;
                            //@
                            Double Disponible_Partida = Consulta_Disponible();

                            if (Cmb_Tipo.SelectedValue == "STOCK")
                            {
                                Double Disponible_Producto = Verifica_Disponible_De_Un_Producto_De_Stock_En_Dt_Productos(Producto_Servicio_ID, ((DataTable)Session[P_Dt_Productos]));
                                //Se descomprometen los productos en el Dt_Productos, todo es virtual
                                Comprometer_O_Descomprometer_Disponible_A_Productos_En_Dt_Productos(Producto_Servicio_ID, ((DataTable)Session[P_Dt_Productos]), Operacion_Descomprometer, 1);
                            }
                            //Se actualizan los productos
                            _DataRow[0]["CANTIDAD"] = (Cantidad_Productos - 1);
                            _DataRow[0]["MONTO"] = (Monto - Monto_A_Descomprometer);
                            _DataRow[0]["MONTO_IEPS"] = (Monto_IEPS - Monto_IEPS_A_Descomprometer);
                            _DataRow[0]["MONTO_IVA"] = (Monto_IVA - Monto_IVA_A_Descomprometer);
                            _DataRow[0]["MONTO_TOTAL"] = (Monto_Total - Monto_Total_A_Descomprometer);

                            //Comprometer Presupuesto en P_Dt_Partidas
                            //@
                            Actualiza_Disponible(Monto_Total_A_Descomprometer, Operacion_Descomprometer);
                        }
                    }
                    Refrescar_Grid();
                    Calcular_Impuestos();
                    Actualizar_Disponible_Productos("Producto_ID", Producto_Servicio_ID, ((DataTable)Session[P_Dt_Productos]));
                }
            }
            catch (Exception Ex)
            {
                Mostrar_Informacion(Ex.Message.ToString(), true);
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Mas_Click
        ///DESCRIPCIÓN: Btn_Mas_Click
        ///CREO: David Herrera Rincón
        ///FECHA_CREO: 27/Febrero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Eliminar_Click(object sender, ImageClickEventArgs e)
        {
            Grid_Productos_Servicios.SelectedIndex = int.Parse(((ImageButton)sender).CommandArgument);
            String Tipo = Grid_Productos_Servicios.SelectedDataKey["Tipo"].ToString().Trim();
            String Producto_Servicio_ID = Grid_Productos_Servicios.SelectedDataKey["REFACCION_ID"].ToString().Trim();
            DataRow[] _DataRow = ((DataTable)Session[P_Dt_Productos_Servicios]).Select("REFACCION_ID = '" + Producto_Servicio_ID + "'");

            if (_DataRow != null && _DataRow.Length > 0)
            {
                double Cantidad_Productos = double.Parse(_DataRow[0]["CANTIDAD"].ToString().Trim());
                double Monto = double.Parse(_DataRow[0]["MONTO"].ToString().Trim());
                double Monto_Total = double.Parse(_DataRow[0]["MONTO_TOTAL"].ToString().Trim());

                double Monto_IEPS = double.Parse(_DataRow[0]["MONTO_IEPS"].ToString().Trim());
                double Monto_IVA = double.Parse(_DataRow[0]["MONTO_IVA"].ToString().Trim());

                String Partida_ID = _DataRow[0]["Partida_ID"].ToString().Trim();
                if (Cmb_Tipo.SelectedValue.Trim() == "STOCK")
                {
                    Comprometer_O_Descomprometer_Disponible_A_Productos_En_Dt_Productos(
                        Producto_Servicio_ID, ((DataTable)Session[P_Dt_Productos]), Operacion_Descomprometer, Cantidad_Productos);
                }
                ((DataTable)Session[P_Dt_Productos_Servicios]).Rows.Remove(_DataRow[0]);
                ((DataTable)Session[P_Dt_Productos_Servicios]).AcceptChanges();
                //Se actualizan los grids de partidas y productos disponibles
                //@
                //Actualizar_Grid_Partidas_Productos();
                Actualiza_Disponible(Monto_Total, Operacion_Descomprometer);
                Refrescar_Grid();
                Calcular_Impuestos();
                Actualizar_Disponible_Productos("Refaccion_ID", Producto_Servicio_ID, ((DataTable)Session[P_Dt_Productos]));
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Mas_Click
        ///DESCRIPCIÓN: Btn_Mas_Click
        ///CREO: David Herrera Rincón
        ///FECHA_CREO: 27/Febrero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Mas_Click(object sender, ImageClickEventArgs e)
        {
            //Asignamos el valor del row
            Grid_Productos_Servicios.SelectedIndex = int.Parse(((ImageButton)sender).CommandArgument);
            String Tipo = Grid_Productos_Servicios.SelectedDataKey["Tipo"].ToString().Trim();
            String Producto_Servicio_ID = Grid_Productos_Servicios.SelectedDataKey["REFACCION_ID"].ToString().Trim();
            DataRow[] _DataRow = ((DataTable)Session[P_Dt_Productos_Servicios]).Select("REFACCION_ID = '" + Producto_Servicio_ID + "'");

            try
            {
                if (_DataRow != null && _DataRow.Length > 0)
                {
                    Double Cantidad_Productos = Double.Parse(_DataRow[0]["CANTIDAD"].ToString().Trim());
                    Double Monto = Double.Parse(_DataRow[0]["MONTO"].ToString().Trim());
                    Double Monto_Total = Double.Parse(_DataRow[0]["MONTO_TOTAL"].ToString().Trim());

                    Double Monto_IEPS = Double.Parse(_DataRow[0]["MONTO_IEPS"].ToString().Trim());
                    Double Monto_IVA = Double.Parse(_DataRow[0]["MONTO_IVA"].ToString().Trim());

                    String Partida_ID = _DataRow[0]["Partida_ID"].ToString().Trim();
                    //seleccionar el combo de partidas
                    if (Tipo == "PRODUCTO")
                    {
                        Double Monto_IEPS_A_Comprometer = Monto_IEPS / Cantidad_Productos;
                        Double Monto_IVA_A_Comprometer = Monto_IVA / Cantidad_Productos;
                        Double Monto_A_Comprometer = Monto / Cantidad_Productos;
                        Double Monto_Total_A_Comprometer = Monto_Total / Cantidad_Productos;

                        Boolean Agregar_Producto = true;
                        Double Disponible_Partida = Consulta_Disponible();

                        if (Disponible_Partida >= Monto_A_Comprometer)
                        {
                            if (Cmb_Tipo.SelectedValue == "STOCK")
                            {
                                Double Disponible_Producto = Verifica_Disponible_De_Un_Producto_De_Stock_En_Dt_Productos(Producto_Servicio_ID, ((DataTable)Session[P_Dt_Productos]));
                                if (Disponible_Producto >= 1)
                                {
                                    //Se comprometen los productos en el Dt_Productos, todo es virtual
                                    Comprometer_O_Descomprometer_Disponible_A_Productos_En_Dt_Productos(Producto_Servicio_ID, ((DataTable)Session[P_Dt_Productos]), Operacion_Comprometer, 1);
                                }
                                else
                                {
                                    Agregar_Producto = false;
                                    Mostrar_Informacion("La cantidad de productos no se encuentra disponible: Existencia [" + Disponible_Producto + "]", true);
                                }
                            }
                            if (Agregar_Producto)
                            {
                                //Se actualizan los productos
                                _DataRow[0]["CANTIDAD"] = (Cantidad_Productos + 1);
                                _DataRow[0]["MONTO"] = (Monto + Monto_A_Comprometer);
                                _DataRow[0]["MONTO_IEPS"] = (Monto_IEPS + Monto_IEPS_A_Comprometer);
                                _DataRow[0]["MONTO_IVA"] = (Monto_IVA + Monto_IVA_A_Comprometer);
                                _DataRow[0]["MONTO_TOTAL"] = (Monto_Total + Monto_Total_A_Comprometer);
                                Refrescar_Grid();
                                Calcular_Impuestos();
                                Actualiza_Disponible(Monto_Total_A_Comprometer, Operacion_Comprometer);
                            }
                        }
                        else
                        {
                            Mostrar_Informacion("Presupuesto insuficiente en la partida: " + _DataRow[0]["PARTIDA_ID"].ToString(), true);
                        }
                    }
                    else if (Tipo == "SERVICIO")
                    {
                        Mostrar_Informacion("Los Servicios solo pueden tener en cantidad (1) uno", true);
                    }
                    Refrescar_Grid();
                    Calcular_Impuestos();
                    Actualizar_Disponible_Productos("Producto_ID", Producto_Servicio_ID, ((DataTable)Session[P_Dt_Productos]));
                }
            }
            catch (Exception Ex)
            {
                Mostrar_Informacion(Ex.Message.ToString(), true);
            }
        }

    #endregion

    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// 
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
    /// FECHA CREÓ: 23/Mayo/2011 10:43 a.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            Botones.Add(Btn_Nuevo);
            Botones.Add(Btn_Modificar);
            Botones.Add(Btn_Eliminar);
            Botones.Add(Btn_Buscar);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 29/Noviembre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion
    
}