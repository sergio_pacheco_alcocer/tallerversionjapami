﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Entradas_Vehiculos.Negocio;
using JAPAMI.Taller_Mecanico.Catalogo_Partes_Vehiculos.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Control_Patrimonial_Operacion_Vehiculos.Negocio;
using JAPAMI.Empleados.Negocios;
using System.Text.RegularExpressions;
using JAPAMI.Taller_Mecanico.Catalogo_Mecanicos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Revista_Mecanica.Negocio;
using JAPAMI.Taller_Mecanico.Catalogo_Detalles_Rev_Mec.Negocio;

public partial class paginas_Taller_Mecanico_Frm_Ope_Tal_Diagnostico_Mecanico_Rev_Mec : System.Web.UI.Page {
    
    #region Page_Load
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN: Carga la Pagina Inicial
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 17/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e) {
            Div_Contenedor_Msj_Error.Visible = false;
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack) {
                Llenar_Combo_Unidades_Responsables();
                Llenar_Combo_Mecanicos();
                Grid_Listado_Servicios.PageIndex = 0;
                Llenar_Listado_Servicios();
                Configuracion_Formulario("INICIAL");
                Cmb_Unidad_Responsable.Enabled = false;
                Llenar_Grid_Listado_Detalles();
            }
        }

    #endregion

    #region Metodos

        #region Llenado de Campos [Combos, Listados, Vehiculos]

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Unidades_Responsables
            ///DESCRIPCIÓN: Se llena el Combo de las Unidades Responsables.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Combo_Unidades_Responsables() {
                Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                Negocio.P_Estatus = "ACTIVO";
                DataTable Dt_Dependencias = Negocio.Consultar_Unidades_Responsables();
                Cmb_Unidad_Responsable.DataSource = Dt_Dependencias;
                Cmb_Unidad_Responsable.DataTextField = "CLAVE_NOMBRE";
                Cmb_Unidad_Responsable.DataValueField = "DEPENDENCIA_ID";
                Cmb_Unidad_Responsable.DataBind();
                Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Servicios
            ///DESCRIPCIÓN: Se llena el Listado de los Servicios para asignar el Mecanico.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Listado_Servicios() {
                Cls_Ope_Tal_Revista_Mecanica_Negocio Rev_Prev = new Cls_Ope_Tal_Revista_Mecanica_Negocio();
                Rev_Prev.P_Estatus = "PROCESO";
                DataTable Dt_Resultados = Rev_Prev.Consultar_Revistas_Mecanicas();

                Grid_Listado_Servicios.Columns[1].Visible = true;
                Grid_Listado_Servicios.Columns[2].Visible = true;
                Grid_Listado_Servicios.Columns[3].Visible = true;
                Grid_Listado_Servicios.DataSource = Dt_Resultados;
                Grid_Listado_Servicios.DataBind();
                Grid_Listado_Servicios.Columns[1].Visible = false;
                Grid_Listado_Servicios.Columns[2].Visible = false;
                Grid_Listado_Servicios.Columns[3].Visible = false;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Mecanicos
            ///DESCRIPCIÓN: Se llena el Listado de los Mecanicos.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Combo_Mecanicos() {
                Cls_Cat_Tal_Mecanicos_Negocio Negocio = new Cls_Cat_Tal_Mecanicos_Negocio();
                //Negocio.P_Estatus = "VIGENTE";
                DataTable Dt_Resultados = Negocio.Consultar_Mecanicos();
                Cmb_Mecanicos.DataSource = Dt_Resultados;
                Cmb_Mecanicos.DataTextField = "NOMBRE_EMPLEADO";
                Cmb_Mecanicos.DataValueField = "MECANICO_ID";
                Cmb_Mecanicos.DataBind();
                Cmb_Mecanicos.Items.Insert(0, new ListItem("< - SELECCIONE - >", ""));
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Vehiculo
            ///DESCRIPCIÓN: Se cargan los Datos del Vehiculo Seleccionado.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Cargar_Datos_Vehiculo(String Vehiculo, String Tipo_Busqueda) {
                Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                switch (Tipo_Busqueda) { 
                    case "NO_INVENTARIO":
                        Consulta_Negocio.P_No_Inventario = Vehiculo;
                        break;
                    case "IDENTIFICADOR":
                        Consulta_Negocio.P_Vehiculo_ID = Vehiculo;
                        break;
                    default: break;
                }
                if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Consulta_Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value; }
                DataTable Dt_Vehiculo = Consulta_Negocio.Consultar_Vehiculos();
                if (Dt_Vehiculo.Rows.Count > 0) {
                    Hdf_Vehiculo_ID.Value = Dt_Vehiculo.Rows[0]["VEHICULO_ID"].ToString().Trim();
                    Txt_No_Inventario.Text = Dt_Vehiculo.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                    Txt_No_Economico.Text = Dt_Vehiculo.Rows[0]["NO_ECONOMICO"].ToString().Trim();
                    Txt_Datos_Vehiculo.Text = Dt_Vehiculo.Rows[0]["VEHICULO_DESCRIPCION"].ToString().Trim();
                    Txt_Placas.Text = Dt_Vehiculo.Rows[0]["PLACAS"].ToString().Trim();
                    Txt_Anio.Text = Dt_Vehiculo.Rows[0]["ANIO"].ToString().Trim();
                    Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString()));
                } else {
                    Lbl_Ecabezado_Mensaje.Text = "El Vehículo no se encontro.";
                    if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Lbl_Mensaje_Error.Text = "[Puede ser que no este asignado a la Unidad Responsable Seleccionada]."; }
                    else { Lbl_Mensaje_Error.Text = ""; }
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Listado_Detalles
            ///DESCRIPCIÓN: Llena la Tabla con los detalles de las Partes a Revisar
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 09/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Grid_Listado_Detalles() {
                Grid_Listado_Detalles.Columns[0].Visible = true;
                Grid_Listado_Detalles.Columns[1].Visible = true;
                Grid_Listado_Detalles.Columns[2].Visible = true;
                Grid_Listado_Detalles.DataSource = Crear_Tabla_Detalles();
                Grid_Listado_Detalles.DataBind();
                Agrupar_Partes_Listado_Detalles();
                Grid_Listado_Detalles.Columns[0].Visible = false;
                Grid_Listado_Detalles.Columns[1].Visible = false;
                Grid_Listado_Detalles.Columns[2].Visible = false;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Agrupar_Partes_Listado_Detalles
            ///DESCRIPCIÓN: Unifica las Celdas
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 09/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Agrupar_Partes_Listado_Detalles()
            {
                Boolean Primer_Color = true;
                foreach (GridViewRow Fila_Actual in Grid_Listado_Detalles.Rows)
                {
                    if (!String.IsNullOrEmpty(HttpUtility.HtmlDecode(Fila_Actual.Cells[2].Text).Trim()))
                    {
                        Int32 Cant_Unificar = Convert.ToInt32(HttpUtility.HtmlDecode(Fila_Actual.Cells[2].Text).Trim());
                        if (Cant_Unificar > 0)
                        {
                            TableCell Celda = Fila_Actual.Cells[3];
                            Celda.RowSpan = Cant_Unificar;
                            if (Primer_Color) { Celda.BackColor = System.Drawing.Color.White; Primer_Color = false; }
                            else { Celda.BackColor = System.Drawing.Color.FromArgb(230, 230, 230); Primer_Color = true; }
                            Celda.BorderColor = System.Drawing.Color.FromArgb(47, 78, 125);
                            Celda.BorderWidth = Convert.ToInt32(2);
                        }
                        else
                        {
                            TableCell Celda = Fila_Actual.Cells[3];
                            Celda.Visible = false;
                        }
                        Fila_Actual.BorderColor = System.Drawing.Color.FromArgb(47, 78, 125);
                        Fila_Actual.BorderWidth = Convert.ToInt32(1);
                    }
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Crear_Tabla_Detalles
            ///DESCRIPCIÓN: Se crea la Tabla DataTable de los Detalles
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 09/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private DataTable Crear_Tabla_Detalles()
            {
                DataTable Dt_Detalles = new DataTable();
                Dt_Detalles.Columns.Add("DETALLE_ID", Type.GetType("System.String"));
                Dt_Detalles.Columns.Add("SUBDETALLE_ID", Type.GetType("System.String"));
                Dt_Detalles.Columns.Add("CANT_UNIFICAR", Type.GetType("System.Int32"));
                Dt_Detalles.Columns.Add("NOMBRE_DETALLE", Type.GetType("System.String"));
                Dt_Detalles.Columns.Add("NOMBRE_SUBDETALLE", Type.GetType("System.String"));

                Cls_Cat_Tal_Detalles_Rev_Mec_Negocio Negocio = new Cls_Cat_Tal_Detalles_Rev_Mec_Negocio();
                Negocio.P_Estatus = "VIGENTE";
                Negocio.P_Tipo = "PARTE";
                DataTable Dt_Parents = Negocio.Consultar_Detalles_Rev_Mec();
                foreach (DataRow Fila_Parent in Dt_Parents.Rows)
                {
                    String Parte_ID = Fila_Parent["DETALLE_ID"].ToString();
                    String Nombre_Parte = Fila_Parent["NOMBRE"].ToString();
                    Cls_Cat_Tal_Detalles_Rev_Mec_Negocio Tmp = new Cls_Cat_Tal_Detalles_Rev_Mec_Negocio();
                    Tmp.P_Estatus = "VIGENTE";
                    Tmp.P_Tipo = "SUBPARTE";
                    Tmp.P_Parent = Parte_ID;
                    DataTable Dt_Childs = Tmp.Consultar_Detalles_Rev_Mec();
                    Boolean Cant_Unificar = true;
                    foreach (DataRow Fila_Child in Dt_Childs.Rows)
                    {
                        DataRow Fila_Nueva = Dt_Detalles.NewRow();
                        Fila_Nueva["DETALLE_ID"] = Parte_ID;
                        Fila_Nueva["SUBDETALLE_ID"] = Fila_Child["DETALLE_ID"].ToString();
                        if (Cant_Unificar)
                        {
                            Fila_Nueva["CANT_UNIFICAR"] = Dt_Childs.Rows.Count;
                            Cant_Unificar = false;
                        }
                        Fila_Nueva["NOMBRE_DETALLE"] = Nombre_Parte;
                        Fila_Nueva["NOMBRE_SUBDETALLE"] = Fila_Child["NOMBRE"].ToString();
                        Dt_Detalles.Rows.Add(Fila_Nueva);
                    }
                }
                return Dt_Detalles;
            }

        #endregion

        #region Generales [Configuracion, Limpiar]
                
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
            ///DESCRIPCIÓN: Limpia los campos del Formulario.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Limpiar_Formulario() {
                Hdf_No_Entrada.Value = "";
                Hdf_No_Registro.Value = "";
                Hdf_No_Solicitud.Value = "";
                Txt_Folio.Text = "";
                Txt_Fecha_Elaboracion.Text = "";
                Txt_Fecha_Recepcion.Text = "";
                Txt_Kilometraje.Text = "";
                Cmb_Tipo_Servicio.SelectedIndex = 0;
                Cmb_Unidad_Responsable.SelectedIndex = 0;
                Txt_Descripcion_Servicio.Text = "";
                Hdf_Vehiculo_ID.Value = "";
                Txt_No_Inventario.Text = "";
                Txt_No_Economico.Text = "";
                Txt_Datos_Vehiculo.Text = "";
                Txt_Placas.Text = "";
                Txt_Anio.Text = "";
                Cmb_Mecanicos.SelectedIndex = 0;
                Txt_Diagnostico_Mecanico.Text = "";
                Chk_Servicio_Adicional.Checked = false;
                Txt_Descripcion_Servicio_Adicional.Text = "";
                Chk_Servicio_Adicional_CheckedChanged(Chk_Servicio_Adicional, null);
                Limpiar_Grid_Detalles();
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Limpiar_Grid_Detalles
            ///DESCRIPCIÓN: Limpia el Grid de Detalles.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 09/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Limpiar_Grid_Detalles() {
                foreach (GridViewRow Fila_Actual in Grid_Listado_Detalles.Rows) {
                    if (Fila_Actual.FindControl("RBtn_Bien") != null) {
                        RadioButton RBtn_Bien = (RadioButton)Fila_Actual.FindControl("RBtn_Bien");
                        RBtn_Bien.Checked = true;
                    }
                    if (Fila_Actual.FindControl("RBtn_Regular") != null) {
                        RadioButton RBtn_Regular = (RadioButton)Fila_Actual.FindControl("RBtn_Regular");
                        RBtn_Regular.Checked = false;
                    }
                    if (Fila_Actual.FindControl("RBtn_Mal") != null)
                    {
                        RadioButton RBtn_Mal = (RadioButton)Fila_Actual.FindControl("RBtn_Mal");
                        RBtn_Mal.Checked = false;
                    }
                    if (Fila_Actual.FindControl("Txt_Observaciones") != null) {
                        TextBox Txt_Observaciones = (TextBox)Fila_Actual.FindControl("Txt_Observaciones");
                        Txt_Observaciones.Text = "";
                    }
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
            ///DESCRIPCIÓN: Se Habilitan y/o inhabilitan los campos dependendiendo de la Condicion.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Configuracion_Formulario(String Operacion) {
                switch (Operacion) {
                    case "INICIAL":
                        Div_Campos.Visible = false;
                        Div_Listado_Servicios.Visible = true;
                        Btn_Guardar_Diagnostico_Mecanico.Visible = false;
                        Btn_Ver_Resultado_Revista.Visible = false;
                        break;
                    case "OPERACION":
                        Div_Campos.Visible = true;
                        Div_Listado_Servicios.Visible = false;
                        Btn_Guardar_Diagnostico_Mecanico.Visible = true;
                        Btn_Ver_Resultado_Revista.Visible = true;
                        break;
                }
            }

        #endregion

        #region Clase de Negocio de Solicitudes [Registrar y Consulta]

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Mostrar_Registro
            ///DESCRIPCIÓN: Muestra el Registro en los campos.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 09/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Mostrar_Registro() {
                Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                Solicitud.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                Solicitud = Solicitud.Consultar_Detalles_Solicitud_Servicio();
                if (Solicitud.P_No_Solicitud > (-1)) {
                    Txt_Folio.Text = Solicitud.P_Folio_Solicitud.Trim();
                    Txt_Fecha_Elaboracion.Text = String.Format("{0:dd/MMM/yyyy}", Solicitud.P_Fecha_Elaboracion);
                    Cmb_Tipo_Servicio.SelectedIndex = Cmb_Tipo_Servicio.Items.IndexOf(Cmb_Tipo_Servicio.Items.FindByValue(Solicitud.P_Tipo_Servicio));
                    Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Solicitud.P_Dependencia_ID));
                    Hdf_Vehiculo_ID.Value = Solicitud.P_Bien_ID;
                    Txt_Descripcion_Servicio.Text = Solicitud.P_Descripcion_Servicio;
                    Cargar_Datos_Vehiculo(Hdf_Vehiculo_ID.Value, "IDENTIFICADOR");
                }
                if (Hdf_No_Entrada.Value.Trim().Length > 0) { 
                    Cls_Ope_Tal_Entradas_Vehiculos_Negocio Entrada = new Cls_Ope_Tal_Entradas_Vehiculos_Negocio();
                    Entrada.P_No_Entrada = Convert.ToInt32(Hdf_No_Entrada.Value);
                    Entrada = Entrada.Consultar_Detalles_Entrada_Vehiculo();
                    if (Entrada.P_Kilometraje > (-1.0)) {
                        Txt_Kilometraje.Text = String.Format("{0:########0.00}", Entrada.P_Kilometraje);
                        Txt_Fecha_Recepcion.Text = String.Format("{0:dd/MMM/yyyy}", Entrada.P_Fecha_Entrada);
                    }
                }
                Mostrar_Registro_();
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Mostrar_Registro_Servicio
            ///DESCRIPCIÓN: Muestra el Registro en los campos.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 09/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Mostrar_Registro_() {
                Cls_Ope_Tal_Revista_Mecanica_Negocio Rev_Negocio = new Cls_Ope_Tal_Revista_Mecanica_Negocio();
                Rev_Negocio.P_No_Registro = Convert.ToInt32(Hdf_No_Registro.Value.Trim());
                Rev_Negocio = Rev_Negocio.Consultar_Detalles_Revista_Mecanica();
                if (Rev_Negocio.P_No_Registro > (-1)) {
                    Cmb_Mecanicos.SelectedIndex = Cmb_Mecanicos.Items.IndexOf(Cmb_Mecanicos.Items.FindByValue(Rev_Negocio.P_Mecanico_ID));
                }
                
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Diagnosticar_Mecanico_Servicio
            ///DESCRIPCIÓN: Guarda el Diagnostico el Mecanico al Servicio.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Diagnosticar_Mecanico_Servicio() {
                Cls_Ope_Tal_Revista_Mecanica_Negocio Negocio = new Cls_Ope_Tal_Revista_Mecanica_Negocio();
                Negocio.P_No_Registro = Convert.ToInt32(Hdf_No_Registro.Value.Trim());
                Negocio.P_Mecanico_ID = Cmb_Mecanicos.SelectedItem.Value.Trim();
                Negocio.P_Vehiculo_ID = Hdf_Vehiculo_ID.Value;
                Negocio.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                Negocio.P_Estatus = "TERMINADO";
                Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                Negocio.P_Usuario_ID = Cls_Sessiones.Empleado_ID;
                Negocio.P_No_Registro = Convert.ToInt32(Hdf_No_Registro.Value.Trim());
                Negocio.P_Diagnostico = Txt_Diagnostico_Mecanico.Text.Trim();
                Negocio.P_Dt_Detalles_Revista = Obtener_Datos_Detalles();
                Negocio.Asignar_Diagnostico_Revista_Mecanica();
                if (Chk_Servicio_Adicional.Checked) {
                    Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                    Solicitud.P_Folio_Solicitud = "";
                    Solicitud.P_Fecha_Elaboracion = DateTime.Today;
                    Solicitud.P_Tipo_Servicio = "SERVICIO_CORRECTIVO";
                    Solicitud.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value;
                    Solicitud.P_Bien_ID = Hdf_Vehiculo_ID.Value;
                    Solicitud.P_Descripcion_Servicio = Txt_Descripcion_Servicio_Adicional.Text.Trim();
                    Solicitud.P_Kilometraje = Convert.ToDouble(Txt_Kilometraje.Text.Trim());
                    Solicitud.P_Estatus = "GENERADO";
                    Solicitud.P_Procedencia = Hdf_No_Solicitud.Value.Trim();
                    Solicitud.P_Empleado_Solicito_ID = Cls_Sessiones.Empleado_ID;
                    Solicitud.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                    Solicitud.Alta_Solicitud_Servicio();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Obtener_Datos_Detalles
            ///DESCRIPCIÓN: Regresa un DataTable Con los Datos de los Detalles.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 28/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private DataTable Obtener_Datos_Detalles() {
                DataTable Dt_Detalles = new DataTable();
                Dt_Detalles.Columns.Add("PARTE_ID", Type.GetType("System.String"));
                Dt_Detalles.Columns.Add("SUBPARTE_ID", Type.GetType("System.String"));
                Dt_Detalles.Columns.Add("VALOR", Type.GetType("System.String"));
                Dt_Detalles.Columns.Add("OBSERVACIONES", Type.GetType("System.String"));
                foreach (GridViewRow Fila_Actual in Grid_Listado_Detalles.Rows) {
                    DataRow Nueva_Fila = Dt_Detalles.NewRow();
                    Nueva_Fila["PARTE_ID"] = HttpUtility.HtmlDecode(Fila_Actual.Cells[0].Text).Trim();
                    Nueva_Fila["SUBPARTE_ID"] = HttpUtility.HtmlDecode(Fila_Actual.Cells[1].Text).Trim();
                    if (Fila_Actual.FindControl("RBtn_Bien") != null && Fila_Actual.FindControl("RBtn_Regular") != null && Fila_Actual.FindControl("RBtn_Mal") != null) {
                        RadioButton RBtn_Bien = (RadioButton)Fila_Actual.FindControl("RBtn_Bien");
                        RadioButton RBtn_Regular = (RadioButton)Fila_Actual.FindControl("RBtn_Regular");
                        RadioButton RBtn_Mal = (RadioButton)Fila_Actual.FindControl("RBtn_Mal");
                        if (RBtn_Bien.Checked) Nueva_Fila["VALOR"] = "B";
                        else if (RBtn_Regular.Checked) Nueva_Fila["VALOR"] = "R";
                        else if (RBtn_Mal.Checked) Nueva_Fila["VALOR"] = "M";
                    }
                    if (Fila_Actual.FindControl("Txt_Observaciones") != null) {
                        TextBox Txt_Observaciones = (TextBox)Fila_Actual.FindControl("Txt_Observaciones");
                        Nueva_Fila["OBSERVACIONES"] = Txt_Observaciones.Text.Trim();
                    }
                    Dt_Detalles.Rows.Add(Nueva_Fila);
                }
                return Dt_Detalles;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Obtener_Resultados_Revista_Mecanica
            ///DESCRIPCIÓN: Regresa una cadena con los resultados.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 28/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private String Obtener_Resultados_Revista_Mecanica() {
                Int32 Estado_Bien = 0;
                Int32 Estado_Regular = 0;
                Int32 Estado_Mal = 0;
                foreach (GridViewRow Fila_Actual in Grid_Listado_Detalles.Rows) {
                    if (Fila_Actual.FindControl("RBtn_Bien") != null && Fila_Actual.FindControl("RBtn_Regular") != null && Fila_Actual.FindControl("RBtn_Mal") != null) {
                        RadioButton RBtn_Bien = (RadioButton)Fila_Actual.FindControl("RBtn_Bien");
                        RadioButton RBtn_Regular = (RadioButton)Fila_Actual.FindControl("RBtn_Regular");
                        RadioButton RBtn_Mal = (RadioButton)Fila_Actual.FindControl("RBtn_Mal");
                        if (RBtn_Bien.Checked) Estado_Bien += 1;
                        else if (RBtn_Regular.Checked) Estado_Regular += 1;
                        else if (RBtn_Mal.Checked) Estado_Mal += 1;
                    }
                }
                return "De " + Grid_Listado_Detalles.Rows.Count + " elemento" + ((Grid_Listado_Detalles.Rows.Count > 1) ? "s" : "") + " evaluado" + ((Grid_Listado_Detalles.Rows.Count > 1) ? "s" : "") + " el Resultado es:\\n\\nCon Buen Estado: " + Estado_Bien.ToString() + " Elemento" + ((Estado_Bien > 1 || Estado_Bien == 0) ? "s" : "") + ", \\nCon Estado Regular: " + Estado_Regular.ToString() + " Elemento" + ((Estado_Regular > 1 || Estado_Regular == 0) ? "s" : "") + ", \\nCon Mal Estado: " + Estado_Mal.ToString() + " Elemento" + ((Estado_Mal > 1 || Estado_Mal == 0) ? "s" : "") + ".";
            }        

        #endregion

        #region Validaciones

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Asignacion
            ///DESCRIPCIÓN: Valida la Solicitud de Servicio antes de ser Asignada a un mecanico
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private Boolean Validar_Asignacion() { 
                Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                String Mensaje_Error = "";
                Boolean Validacion = true;
                if (Hdf_No_Registro.Value.Trim().Length == 0) { 
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar el Servicio que se realizará.";
                    Mensaje_Error = Mensaje_Error + " <br />";
                    Validacion = false;
                }
                if (Cmb_Mecanicos.SelectedIndex == 0) {
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar el Mecanico que hará el Servicio.";
                    Mensaje_Error = Mensaje_Error + " <br />";
                    Validacion = false;
                }
                if (Txt_Diagnostico_Mecanico.Text.Trim().Length == 0) {
                    Mensaje_Error = Mensaje_Error + "+ Introducir el Diagnostico del Mecanico.";
                    Mensaje_Error = Mensaje_Error + " <br />";
                    Validacion = false;
                }
                if (!Validacion) {
                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
                    Div_Contenedor_Msj_Error.Visible = true;
                } 
                return Validacion;
            }

        #endregion

    #endregion
        
    #region Grids
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Servicios_PageIndexChanging
        ///DESCRIPCIÓN: Maneja la paginación del Listado
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 17/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Servicios_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            try {
                Grid_Listado_Servicios.SelectedIndex = (-1);
                Grid_Listado_Servicios.PageIndex = e.NewPageIndex;
                Llenar_Listado_Servicios();
            } catch(Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Servicios_SelectedIndexChanged
        ///DESCRIPCIÓN: Obtiene los datos de un Servicio 
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 17/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Servicios_SelectedIndexChanged(object sender, EventArgs e) {
            try{
                if (Grid_Listado_Servicios.SelectedIndex > (-1)){
                    Limpiar_Formulario();
                    Hdf_No_Entrada.Value = HttpUtility.HtmlDecode(Grid_Listado_Servicios.SelectedRow.Cells[1].Text.Trim()).Trim();
                    Hdf_No_Registro.Value = HttpUtility.HtmlDecode(Grid_Listado_Servicios.SelectedRow.Cells[2].Text.Trim()).Trim();
                    Hdf_No_Solicitud.Value = HttpUtility.HtmlDecode(Grid_Listado_Servicios.SelectedRow.Cells[3].Text.Trim()).Trim();
                    Mostrar_Registro();
                    Configuracion_Formulario("OPERACION");
                    Grid_Listado_Servicios.SelectedIndex = -1;
                    System.Threading.Thread.Sleep(500);
                }
            } catch(Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }
 
    #endregion

    #region Eventos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Ver_Resultado_Revista_Click
        ///DESCRIPCIÓN: Controla las operaciones del Boton Diagnostico
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 17/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Ver_Resultado_Revista_Click(object sender, ImageClickEventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "GACO", "alert('" + Obtener_Resultados_Revista_Mecanica() + "');", true);
        }
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Guardar_Diagnostico_Mecanico_Click
        ///DESCRIPCIÓN: Controla las operaciones del Boton Diagnostico
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 17/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Guardar_Diagnostico_Mecanico_Click(object sender, ImageClickEventArgs e) {
            if (Validar_Asignacion()) {
                Diagnosticar_Mecanico_Servicio();
                Llenar_Listado_Servicios();
                Configuracion_Formulario("INICIAL");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "GACO", "alert('Operacion Exitosa: Guardar Diagnostico de Mecanico a Servicio.\\nServicio Folio: " + Txt_Folio.Text.Trim() + ".');", true);
                Limpiar_Formulario();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Controla las operaciones del Boton de Salir
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 17/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e) {
            if (Div_Campos.Visible) {
               Limpiar_Formulario();
               Configuracion_Formulario("INICIAL");
            } else {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Actualizar_Listado_Click
        ///DESCRIPCIÓN: Ejecuta el Proceso para Salir.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Actualizar_Listado_Click(object sender, ImageClickEventArgs e) {
            Llenar_Listado_Servicios();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Chk_Servicio_Adicional_CheckedChanged
        ///DESCRIPCIÓN: Se habilita o no el Cuadro de Texto para Servicio Adicional
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 19/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Chk_Servicio_Adicional_CheckedChanged(object sender, EventArgs e) {
            Pnl_Descripcion_Servicio_Adicional.Visible = Chk_Servicio_Adicional.Checked;
        }

    #endregion
}
