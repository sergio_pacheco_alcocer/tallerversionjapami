﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Ope_Tal_Archivos_Servicios.aspx.cs" Inherits="paginas_Taller_Mecanico_Frm_Ope_Tal_Archivos_Servicios"
    Title="Archivos del Servicio" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .Tabla_Comentarios
        {
            border-collapse: collapse;
            margin-left: 25px;
            color: #25406D;
            font-family: Verdana,Geneva,MS Sans Serif;
            font-size: small;
            text-align: left;
        }
        .Tabla_Comentarios, .Tabla_Comentarios th, .Tabla_Comentarios td
        {
            border: 1px solid #999999;
            padding: 2px 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">

    <script type="text/javascript">
        window.onerror = new Function("return true");
        //Abrir una ventana modal
        function Abrir_Ventana_Modal(Url, Propiedades) {
            window.showModalDialog(Url, null, Propiedades);
        }
        function Validar_Longitud_Texto(Text_Box, Max_Longitud) {
            if (Text_Box.value.length > Max_Longitud) {
                Text_Box.value = Text_Box.value.substring(0, Max_Longitud);
            }
        }
    </script>

    <script type="text/javascript" language="javascript">
    //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_sesiones.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion()
        {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para matener la sesión activa
        setInterval('MantenSesion()', <%=(int)(0.9*(Session.Timeout * 60000))%>);
        function formatCurrency(num) {
            var Combo = "";
            var Beneficio;
                num = num.toString().replace(/\$|\,/g,'');
                if(isNaN(num))
                num = "0";
                sign = (num == (num = Math.abs(num)));
                num = Math.floor(num*100+0.50000000001);
                cents = num%100;
                num = Math.floor(num/100).toString();
                if(cents<10)
                    cents = "0" + cents;
                    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
                    num = num.substring(0,num.length-(4*i+3))+','+
                    num.substring(num.length-(4*i+3));
                    return (((sign)?'':'-') + num + '.' + cents);
            }
            
    </script>

    <script type="text/javascript" language="javascript">
        function Limpiar_Ctlr_Busqueda() {
            document.getElementById("<%=Txt_Folio_Solicitud_Busqueda.ClientID%>").value = "";
            document.getElementById("<%=Cmb_Tipo_Solicitud_Busqueda.ClientID%>").value = "";
            document.getElementById("<%=Cmb_Unidad_Responsable_Busqueda.ClientID%>").value = "";
            document.getElementById("<%=Txt_Numero_Inventario_Busqueda.ClientID%>").value = "";
            document.getElementById("<%=Txt_Numero_Economico_Busqueda.ClientID%>").value = "";
            document.getElementById("<%=Txt_Fecha_Recepcion_Inicial.ClientID%>").value = "";
            document.getElementById("<%=Txt_Fecha_Recepcion_Final.ClientID%>").value = "";
            document.getElementById("<%=Cmb_Filtrado_Estatus.ClientID%>").value = "";
            document.getElementById("<%=Cmb_Reparacion_Busqueda.ClientID%>").value = "";
            return false;
        } 
    </script>

    <script type="text/javascript">
        function uploadComplete(sender, args) {
            try {
                var filename = args.get_fileName();

                if (filename != "") {
                    // code to get File Extension..   
                    var arr1 = new Array;
                    arr1 = filename.split("\\");
                    var len = arr1.length;
                    var img1 = arr1[len - 1];
                    var filext = img1.substring(img1.lastIndexOf(".") + 1);


                    if (filext == "jpg" || filext == "JPG" || filext == "jpeg" || filext == "JPEG" || filext == "png" || filext == "PNG" || filext == "gif" || filext == "GIF") {
                        if (args.get_length() > 2621440) {
                            return false;
                        }
                        $get("<%=lblMesg.ClientID%>").style.color = "green";
                        $get("<%=lblMesg.ClientID%>").innerHTML = "Archivo leido... Subiendo";
                        __doPostBack('tctl00$Cph_Area_Trabajo1$Upd_Panel', '');
                        return true;
                    } else {
                        $get("<%=lblMesg.ClientID%>").style.color = "red";
                        $get("<%=lblMesg.ClientID%>").innerHTML = "Tipo de archivo inválido " + filename + "\n\nFormatos Validos [.jpg, .jpeg, .png, .gif]";
                        return false;
                    }
                }
            } catch (e) {

            }

        }

        function uploadError(sender) {

            $get("<%=lblMesg.ClientID%>").style.color = "red";
            $get("<%=lblMesg.ClientID%>").innerHTML = "No se pudo cargar el archivo.";
        }        
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" EnablePartialRendering="true" AsyncPostBackTimeout="9000">
    </cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Loading" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Area_Trabajo" style="background-color: #ffffff; width: 100%; height: 100%;">
                <center>
                    <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                        <tr align="center">
                            <td class="label_titulo" colspan="2">
                                Archivos de Servicios
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div id="Div_Contenedor_Msj_Error" style="width: 98%;" runat="server" visible="false">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td colspan="2" align="left">
                                                <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                    Width="24px" Height="24px" />
                                                <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 10%;">
                                            </td>
                                            <td style="width: 90%; text-align: left;" valign="top">
                                                <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left" style="width: 50%;">
                                <asp:ImageButton ID="Btn_Guardar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_guardar.png"
                                    CssClass="Img_Button" ToolTip="Guardar" OnClick="Btn_Guardar_Click" />
                                <asp:ImageButton ID="Btn_Imprimir_Solicitud_Servicio" runat="server" ImageUrl="~/paginas/imagenes/gridview/grid_print.png"
                                    Width="24px" CssClass="Img_Button" AlternateText="Imprimir" ToolTip="Imprimir Solicitud"
                                    OnClick="Btn_Imprimir_Solicitud_Servicio_Click" />
                                <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                    Width="24px" CssClass="Img_Button" AlternateText="Salir" OnClick="Btn_Salir_Click" />
                            </td>
                            <td style="width: 50%;">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                    <br />
                    <div id="Div_Listado_Solicitudes" runat="server" style="width: 100%;">
                        <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                                <td>
                                    <div id="Div_Filtros_Busqueda" runat="server" style="border-style: outset; width: 98%;">
                                        <table width="100%" border="0" cellspacing="0" class="estilo_fuente">
                                            <tr>
                                                <td colspan="4">
                                                    <asp:Label ID="Lbl_Leyenda_Busqueda" runat="server" Width="100%" Text="FILTROS PARA EL LISTADO"
                                                        ForeColor="White" BackColor="Black" Font-Bold="true" Style="text-align: center;"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Folio_Solicitud_Busqueda" runat="server" Text="Folio"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Folio_Solicitud_Busqueda" runat="server" Width="96%" MaxLength="10"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Folio_Solicitud_Busqueda" runat="server"
                                                        TargetControlID="Txt_Folio_Solicitud_Busqueda" FilterType="Numbers">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 15%;">
                                                    &nbsp;&nbsp;
                                                    <asp:Label ID="Lbl_Tipo_Solicitud_Busqueda" runat="server" Text="Tipo Solicitud"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:DropDownList ID="Cmb_Tipo_Solicitud_Busqueda" runat="server" Width="98%">
                                                        <asp:ListItem Value="">&lt; - - TODOS - - &gt;</asp:ListItem>
                                                        <asp:ListItem Value="REVISTA_MECANICA">REVISTA MECANICA</asp:ListItem>
                                                        <asp:ListItem Value="SERVICIO_CORRECTIVO">SERVICIO CORRECTIVO</asp:ListItem>
                                                        <asp:ListItem Value="SERVICIO_PREVENTIVO">SERVICIO PREVENTIVO</asp:ListItem>
                                                        <asp:ListItem Value="VERIFICACION">VERIFICACI&Oacute;N</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Unidad_Responsable_Busqueda" runat="server" Text="Unidad Responsable"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:DropDownList ID="Cmb_Unidad_Responsable_Busqueda" runat="server" Width="99.2%">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Numero_Inventario_Busqueda" runat="server" Text="No. Inventario"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Numero_Inventario_Busqueda" runat="server" Width="96%"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Numero_Inventario_Busqueda" runat="server"
                                                        TargetControlID="Txt_Numero_Inventario_Busqueda" FilterType="Numbers">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 15%;">
                                                    &nbsp;&nbsp;
                                                    <asp:Label ID="Lbl_Numero_Economico_Busqueda" runat="server" Text="No. Económico"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Numero_Economico_Busqueda" runat="server" Width="96%"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Numero_Economico_Busqueda" runat="server"
                                                        TargetControlID="Txt_Numero_Economico_Busqueda" FilterType="UppercaseLetters, LowercaseLetters, Custom, Numbers"
                                                        ValidChars="ñÑ -_*" InvalidChars="'%">
                                                    </cc1:FilteredTextBoxExtender>
                                                    <cc1:TextBoxWatermarkExtender ID="Tbw_Economico" runat="server" TargetControlID="Txt_Numero_Economico_Busqueda"
                                                        WatermarkCssClass="watermarked" WatermarkText="&lt;U00000&gt;" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <hr />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" style="text-align: center;">
                                                    <asp:Label ID="Lbl_Leyenda_Fecha_Recepcion" runat="server" Text="Rango en Fecha de Recepción"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Fecha_Recepcion_Inicial" runat="server" Text="Fecha Inicial"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Fecha_Recepcion_Inicial" runat="server" Width="85%" Enabled="false"></asp:TextBox>
                                                    <asp:ImageButton ID="Btn_Fecha_Recepcion_Inicial" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                                    <cc1:CalendarExtender ID="CE_Txt_Fecha_Recepcion_Inicial" runat="server" TargetControlID="Txt_Fecha_Recepcion_Inicial"
                                                        PopupButtonID="Btn_Fecha_Recepcion_Inicial" Format="dd/MMM/yyyy">
                                                    </cc1:CalendarExtender>
                                                </td>
                                                <td style="width: 15%;">
                                                    &nbsp;&nbsp;<asp:Label ID="Lbl_Fecha_Recepcion_Final" runat="server" Text="Fecha Final"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Fecha_Recepcion_Final" runat="server" Width="85%" Enabled="false"></asp:TextBox>
                                                    <asp:ImageButton ID="Btn_Fecha_Recepcion_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                                    <cc1:CalendarExtender ID="CE_Txt_Fecha_Recepcion_Final" runat="server" TargetControlID="Txt_Fecha_Recepcion_Final"
                                                        PopupButtonID="Btn_Fecha_Recepcion_Final" Format="dd/MMM/yyyy">
                                                    </cc1:CalendarExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <hr />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Filtrado_Estatus" runat="server" Text="Estatus"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:DropDownList ID="Cmb_Filtrado_Estatus" runat="server" Width="98%">
                                                        <asp:ListItem Value="">&lt; - TODAS - &gt;</asp:ListItem>
                                                        <asp:ListItem Value="PENDIENTE">PENDIENTE</asp:ListItem>
                                                        <asp:ListItem Value="RECHAZADA">RECHAZADA</asp:ListItem>
                                                        <asp:ListItem Value="CANCELADA">CANCELADA</asp:ListItem>
                                                        <asp:ListItem Value="AUTORIZADA">AUTORIZADA</asp:ListItem>
                                                        <asp:ListItem Value="EN_REPARACION">EN REPARACIÓN</asp:ListItem>
                                                        <asp:ListItem Value="ENTREGADO">ENTREGADO</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 15%;">
                                                    &nbsp;&nbsp;Reparación&nbsp;
                                                </td>
                                                <td style="text-align: left; width: 35%;">
                                                    <asp:DropDownList ID="Cmb_Reparacion_Busqueda" runat="server" Width="98%">
                                                        <asp:ListItem Value="">&lt; - - TODOS - - &gt;</asp:ListItem>
                                                        <asp:ListItem Value="INTERNA">INTERNO</asp:ListItem>
                                                        <asp:ListItem Value="EXTERNA">EXTERNO</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="right">
                                                    <asp:ImageButton ID="Btn_Actualizar_Listado" runat="server" ToolTip="Actualizar Listado"
                                                        AlternateText="Actualizar Listado" OnClick="Btn_Actualizar_Listado_Click" ImageUrl="~/paginas/imagenes/paginas/actualizar_detalle.png"
                                                        Width="16px" />
                                                    <asp:ImageButton ID="Btn_Limpiar_Filtros_Listado" runat="server" ToolTip="Limpiar Filtros Listado"
                                                        AlternateText="Limpiar Filtros Listado" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png"
                                                        Width="16px" Height="16px" OnClientClick="javascript:return Limpiar_Ctlr_Busqueda();" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <asp:GridView ID="Grid_Listado_Solicitudes" runat="server" CssClass="GridView_1"
                            AutoGenerateColumns="False" AllowPaging="True" AllowSorting="true" PageSize="20" Width="99%" GridLines="None"
                            EmptyDataText="No se Encontrarón Solicitudes." OnSelectedIndexChanged="Grid_Listado_Solicitudes_SelectedIndexChanged"
                            OnPageIndexChanging="Grid_Listado_Solicitudes_PageIndexChanging" OnSorting="Grid_Listado_Servicios_Sorting" DataKeyNames="NO_SOLICITUD,ESTATUS">
                            <RowStyle CssClass="GridItem" />
                            <Columns>
                                <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                    <ItemStyle Width="30px" />
                                </asp:ButtonField>
                                <asp:BoundField DataField="NO_SOLICITUD" HeaderText="NO_SOLICITUD" SortExpression="NO_SOLICITUD">
                                    <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FOLIO" HeaderText="FOLIO" SortExpression="FOLIO">
                                    <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FECHA_ELABORACION" HeaderText="Fecha Elaboración" SortExpression="FECHA_ELABORACION"
                                    DataFormatString="{0:dd/MMM/yyyy}">
                                    <ItemStyle Width="160px" HorizontalAlign="Center" Font-Size="X-Small" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FECHA_RECEPCION_REAL" HeaderText="Fecha Recepción" SortExpression="FECHA_RECEPCION_REAL"
                                    DataFormatString="{0:dd/MMM/yyyy}">
                                    <ItemStyle Width="160px" HorizontalAlign="Center" Font-Size="X-Small" />
                                </asp:BoundField>
                                <asp:BoundField DataField="TIPO_SERVICIO" HeaderText="Tipo Servicio" SortExpression="TIPO_SERVICIO">
                                    <ItemStyle Width="160px" HorizontalAlign="Center" Font-Size="X-Small" />
                                </asp:BoundField>
                                <asp:BoundField DataField="NO_INVENTARIO" HeaderText="No. Inventario" SortExpression="NO_INVENTARIO">
                                    <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS">
                                    <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small" />
                                </asp:BoundField>
                                <asp:BoundField DataField="NO_ECONOMICO" HeaderText="No. Economico" SortExpression="NO_ECONOMICO">
                                    <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small" />
                                </asp:BoundField>
                            </Columns>
                            <PagerStyle CssClass="GridHeader" />
                            <SelectedRowStyle CssClass="GridSelected" />
                            <HeaderStyle CssClass="GridHeader" />
                            <AlternatingRowStyle CssClass="GridAltItem" />
                        </asp:GridView>
                    </div>
                    <div id="Div_Campos" runat="server" style="width: 100%;">
                        <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                                <td colspan="4">
                                    <asp:HiddenField ID="Hdf_No_Solicitud" runat="server" />
                                    <asp:HiddenField ID="Hdf_Folio_Solicitud" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;&nbsp;&nbsp;
                                </td>
                                <td style="width: 15%;">
                                    &nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="Lbl_Folio_Solicitud" runat="server" Text="Folio Solicitud" Font-Bold="true"
                                        ForeColor="Black"></asp:Label>
                                </td>
                                <td style="width: 35%;">
                                    <asp:TextBox ID="Txt_Folio_Solicitud" runat="server" Width="98%" ReadOnly="true"
                                        BorderStyle="Outset" Style="text-align: center;" Font-Bold="true" ForeColor="Red"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Fecha_Elaboracion" runat="server" Text="Fecha Elaboración"></asp:Label>
                                </td>
                                <td style="width: 35%;">
                                    <asp:TextBox ID="Txt_Fecha_Elaboracion" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                </td>
                                <td style="width: 15%;">
                                    &nbsp;&nbsp;&nbsp;
                                </td>
                                </td>
                                <td style="width: 35%;">
                                    <asp:TextBox ID="Txt_Estatus" runat="server" Width="98%" ReadOnly="true" BorderStyle="Outset"
                                        Style="text-align: center;" Font-Bold="true"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text="Unidad Responsable"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="Cmb_Unidad_Responsable" runat="server" Width="100%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Kilometraje" runat="server" Text="Kilometraje [Km]"></asp:Label>
                                </td>
                                <td style="width: 16%;">
                                    <asp:TextBox ID="Txt_Kilometraje" runat="server" Width="95%"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Kilometraje" runat="server" TargetControlID="Txt_Kilometraje"
                                        ValidChars="." FilterType="Custom, Numbers">
                                    </cc1:FilteredTextBoxExtender>
                                </td>
                                <td style="width: 15%;">
                                    &nbsp;&nbsp;
                                    <asp:Label ID="Lbl_Tipo_Servicio" runat="server" Text="Tipo Servicio"></asp:Label>
                                </td>
                                <td style="width: 35%;">
                                    <asp:DropDownList ID="Cmb_Tipo_Servicio" runat="server" Width="100%">
                                        <asp:ListItem Value="">&lt; - - SELECCIONE - - &gt;</asp:ListItem>
                                        <asp:ListItem Value="REVISTA_MECANICA">REVISTA MECANICA</asp:ListItem>
                                        <asp:ListItem Value="SERVICIO_CORRECTIVO">SERVICIO CORRECTIVO</asp:ListItem>
                                        <asp:ListItem Value="SERVICIO_PREVENTIVO">SERVICIO PREVENTIVO</asp:ListItem>
                                        <asp:ListItem Value="VERIFICACION">VERIFICACI&Oacute;N</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Correo_Electronico" runat="server" Text="Correo Electronico"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="Txt_Correo_Electronico" runat="server" Width="99%">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:Panel ID="Pnl_Vehiculo_Seleccionado" runat="server" Width="99%" GroupingText="Vehículo para el Servicio">
                                        <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:HiddenField ID="Hdf_Vehiculo_ID" runat="server" />
                                                    <asp:HiddenField ID="Hdf_Empleado_Solicito_ID" runat="server" />
                                                    <asp:HiddenField ID="Hdf_Email" runat="server" />
                                                    <asp:HiddenField ID="Hdf_Estatus_Servicio" runat="server" />
                                                    <asp:HiddenField ID="Hdf_Mecanico_ID" runat="server" />
                                                    <asp:HiddenField ID="Hdf_No_Servicio" runat="server" />
                                                    <asp:Label ID="Lbl_No_Inventario" runat="server" Text="No. Inventario"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_No_Inventario" runat="server" Width="98%" MaxLength="7"></asp:TextBox>
                                                </td>
                                                <td style="width: 15%;">
                                                    &nbsp;&nbsp;
                                                    <asp:Label ID="Lbl_No_Economico" runat="server" Text="No. Economico"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_No_Economico" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Datos_Vehiculo" runat="server" Text="Vehículo"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Datos_Vehiculo" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Placas" runat="server" Text="Placas"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Placas" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                                </td>
                                                <td style="width: 15%;">
                                                    &nbsp;&nbsp;
                                                    <asp:Label ID="Lbl_Anio" runat="server" Text="Año"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Anio" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="1">
                                                    Reparación
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Reparacion" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="Pnl_Bien_Mueble_Seleccionado" runat="server" 
                                        GroupingText="Bien Mueble para el Servicio" Width="99%">
                                        <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                                            <tr>
                                                <td style="width:15%;">
                                                    <asp:HiddenField ID="Hdf_Bien_Mueble_ID" runat="server" />
                                                    <asp:HiddenField ID="Hdf_Tipo_Bien" runat="server" />  
                                                    <asp:Label ID="Lbl_No_Inventario_BM" runat="server" Text="No. Inventario"></asp:Label>
                                                </td>
                                                <td style="width:35%;">
                                                    <asp:TextBox ID="Txt_No_Inventario_BM" runat="server" MaxLength="7" Width="98%" Enabled="false"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Inventario_BM" runat="server" 
                                                        FilterType="Numbers" TargetControlID="Txt_No_Inventario_BM">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width:15%;">
                                                    &nbsp;
                                                </td>
                                                <td style="width:35%;">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:15%;">
                                                    <asp:Label ID="Lbl_Descripcion_Bien" runat="server" Text="Descripción Bien"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Descripcion_Bien" runat="server" Enabled="false" Rows="2" 
                                                        TextMode="MultiLine" Width="99%"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:15%;">
                                                    <asp:Label ID="Lbl_Numero_Serie_Bien" runat="server" Text="No. Serie"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Numero_Serie_Bien" runat="server" Enabled="false" 
                                                        Width="99%"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Panel ID="Pnl_Descripcion_Servicio" runat="server" Width="99%" GroupingText="Descripción del Servicio">
                                        <asp:TextBox ID="Txt_Descripcion_Servicio" runat="server" Rows="2" Style="text-transform: uppercase"
                                            TextMode="MultiLine" Width="99%" Height="90px" ReadOnly="True"></asp:TextBox>
                                    </asp:Panel>
                                </td>                            
                                <td colspan="2">
                                    <asp:Panel ID="Pnl_Diagnostico_Servicio" runat="server" Width="99%" GroupingText="Diagnostico del Servicio">
                                        <asp:TextBox ID="Txt_Diagnostico_Servicio" runat="server" Rows="3" Style="text-transform: uppercase"
                                            TextMode="MultiLine" Width="99%" Height="90px" ReadOnly="True"></asp:TextBox>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:Panel ID="Pnl_Grid_Archivos" runat="server" Width="99%" GroupingText="Archivos/Imagenes del Servicio">
                                        <cc1:AsyncFileUpload OnClientUploadError="uploadError" OnClientUploadComplete="uploadComplete"
                                            runat="server" ID="Afu_Archivos_Servicios" Width="98%" CompleteBackColor="White"
                                            UploadingBackColor="#CCFFFF" ThrobberID="Throbber" OnUploadedComplete="FileUploadComplete" />
                                        <asp:Label ID="Throbber" Text="wait" runat="server" Width="30px">                                                                     
                                    <div id="Div1" class="progressBackgroundFilter"></div>
                                    <div  class="processMessage" id="div2">
                                        <img alt="" src="../imagenes/paginas/Updating.gif" />
                                    </div>
                                        </asp:Label>
                                        <br />
                                        <asp:Label ID="lblMesg" runat="server" Text=""></asp:Label>
                                        <asp:GridView ID="Grid_Facturas" runat="server" AllowPaging="False" AutoGenerateColumns="False"
                                            CssClass="GridView_1" GridLines="None" DataKeyNames="NOMBRE_ARCHIVO,IMAGEN_SERVICIO_ID" Width="99%"
                                            EnableModelValidation="True">
                                            <RowStyle CssClass="GridItem" />
                                            <Columns>
                                                <asp:BoundField DataField="IMAGEN_SERVICIO_ID" HeaderText="Identificador" SortExpression="IMAGEN_SERVICIO_ID"
                                                    ItemStyle-HorizontalAlign="Left" Visible="false">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FECHA_ARCHIVO" HeaderText="Fecha" SortExpression="FECHA_ARCHIVO"
                                                    ItemStyle-HorizontalAlign="Left" DataFormatString="{0:dd/MMM/yyyy h:mm:ss}">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS"
                                                    HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EMPLEADO" HeaderText="Empleado" HeaderStyle-HorizontalAlign="Center"
                                                    Visible="true" SortExpression="EMPLEADO">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="30%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="NOMBRE_ARCHIVO" HeaderText="Nombre" SortExpression="NOMBRE_ARCHIVO"
                                                    HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="30%" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Ver" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%"
                                                    Visible="true">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="Btn_Archivo" runat="server" CommandName="Cmd_Archivo" Target="_blank"
                                                            NavigateUrl="<%# Bind('RUTA_ARCHIVO') %>" Text="Ver" Visible='<%# Grid_Archivo %>'
                                                            ForeColor="#2F4E7D"></asp:HyperLink>
                                                        <itemstyle font-size="X-Small" horizontalalign="Center" width="20%" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                
                                                <asp:TemplateField HeaderText="Eliminar" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="10%"
                                                    Visible="true">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="Btn_Eliminar" runat="server" CssClass="Img_Button" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                                                            ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png" OnClick="Btn_Eliminar_Click"
                                                            ToolTip="Quitar" Height="16px" Width="16px" />
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="0px" />
                                                    <ItemStyle Font-Size="X-Small" Width="7px" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle CssClass="GridHeader" />
                                            <SelectedRowStyle CssClass="GridSelected" />
                                            <HeaderStyle CssClass="GridHeader" />
                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                        </asp:GridView>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                        <br />
                    </div>
                </center>
            </div>
            <br />
            <br />
            <br />
            <br />
        </ContentTemplate>        
    </asp:UpdatePanel>
</asp:Content>
