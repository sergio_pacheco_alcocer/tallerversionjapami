﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Operacion_Propuesta_Ganadora.Negocios;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using System.Text;
using System.Reflection;
using CarlosAg.ExcelXmlWriter;
using JAPAMI.Taller_Mecanico.Administrar_Requisiciones.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;

public partial class paginas_Taller_Mecanico_Frm_Ope_Tal_Propuesta_Ganadora : System.Web.UI.Page
{
    ///*******************************************************************************
    ///PAGE_LOAD
    ///*******************************************************************************
    #region Page_Load
    
    protected void Page_Load(object sender, EventArgs e) {
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        Lbl_Mensaje_Error.Text = "";
        Div_Contenedor_Msj_Error.Visible = false;
        if (!IsPostBack) {
            ViewState["SortDirection"] = "ASC";
            Configurar_Formulario("Inicio");
            Llenar_Grid_Requisiciones();
        }
    }

    #endregion


    ///*******************************************************************************
    ///METODOS
    ///*******************************************************************************
    #region Metodos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Configurar_Formulario
    ///DESCRIPCIÓN: Metodo que configura el formulario con respecto al estado de habilitado o visible
    ///´de los componentes de la pagina
    ///PARAMETROS: 1.- String Estatus: Estatus que puede tomar el formulario con respecto a sus componentes, ya sea "Inicio" o "Nuevo"
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 01/JULIO/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Configurar_Formulario(String Estatus)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";

        switch (Estatus)
        {
            case "Inicio":

                Div_Detalle_Requisicion.Visible = false;
                Div_Grid_Requisiciones.Visible = true;

                //Boton Modificar
                Btn_Modificar.Visible = true;
                Btn_Modificar.ToolTip = "Modificar";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                Btn_Modificar.Enabled = true;
                //Boton Salir
                Btn_Salir.Visible = true;
                Btn_Salir.ToolTip = "Inicio";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                //
                Grid_Requisiciones.Visible = true;
                Grid_Requisiciones.Enabled = true;
                Div_Detalle_Requisicion.Visible = false;
                Grid_Productos.Enabled = false;
                Cmb_Estatus.Enabled = false;                
                Cmb_Proveedores.Enabled = false;

                break;
            case "Nuevo":

                Btn_Modificar.Visible = true;
                Btn_Modificar.ToolTip = "Guardar";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                Btn_Modificar.Enabled = true;
                //Boton Salir
                Btn_Salir.Visible = true;
                Btn_Salir.ToolTip = "Cancelar";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                //
                Div_Grid_Requisiciones.Visible = false;
                Div_Detalle_Requisicion.Visible = true;
                Grid_Productos.Enabled = true;
                Cmb_Estatus.Enabled = true;
                Cmb_Estatus.SelectedValue = "COTIZADA";
                Cmb_Proveedores.Enabled = true;
                if (Cmb_Proveedores.Items.Count == 2)
                {
                    Cmb_Proveedores.SelectedIndex = 1;
                    Cmb_Proveedores_SelectedIndexChanged(null, null);
                }

                break;
        }//fin del switch

    }


    public void Limpiar_Componentes()
    {

        Session["Concepto_ID"] = null;
        Session["Dt_Productos"] = null;
        Session["Dt_Requisiciones"] = null;
        Session["No_Requisicion"] = null;
        Session["TIPO_ARTICULO"] = null;
        Session["Concepto_ID"] = null;
        Hdf_Dependencia_ID.Value = "";
        Txt_Dependencia.Text = "";
        Txt_Concepto.Text = "";
        Txt_Folio.Text = "";
        Txt_Concepto.Text = "";
        Txt_Fecha_Generacion.Text = "";
        Chk_Verificacion.Checked = false;
        Txt_Justificacion.Text = "";
        Txt_Especificacion.Text = "";
        Grid_Productos.DataSource = new DataTable();
        Grid_Productos.DataBind();
        Txt_SubTotal_Cotizado_Requisicion.Text = "";
        Txt_Total_Cotizado_Requisicion.Text = "";
        Txt_IVA_Cotizado.Text = "";
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Estatus
    ///DESCRIPCIÓN: Metodo que carga el combo Cmb_Estatus
    ///PARAMETROS:  
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 8/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_Combo_Estatus()
    {
        Cmb_Estatus.Items.Clear();
        Cmb_Estatus.Items.Add("<<SELECCIONAR>>");
        Cmb_Estatus.Items.Add("PROVEEDOR");
        Cmb_Estatus.Items.Add("COTIZADA");
        Cmb_Estatus.Items[0].Value = "0";
        Cmb_Estatus.Items[0].Selected = true;
    }

    public void Calcular_Importe_Total()
    {
        DataTable Dt_Productos = (DataTable)Session["Dt_Productos"];
        double Total_Cotizado = 0;
        double IVA_Cotizado = 0;
        double IEPS_Cotizado = 0;
        double Subtotal_Cotizado = 0;
        if (Dt_Productos.Rows.Count != 0)
        {
            for (int i = 0; i < Dt_Productos.Rows.Count; i++)
            {
                if (Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_IVA_Cotizado].ToString().Trim() != String.Empty)
                {
                    IVA_Cotizado = IVA_Cotizado + double.Parse(Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_IVA_Cotizado].ToString().Trim());
                }
                else
                {
                    IVA_Cotizado = IVA_Cotizado + 0;
                }

                if (Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_IEPS_Cotizado].ToString().Trim() != String.Empty)
                {
                    IEPS_Cotizado = 0;//IEPS_Cotizado + double.Parse(Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_IEPS_Cotizado].ToString().Trim());
                }
                else
                {
                    IEPS_Cotizado = IEPS_Cotizado + 0;
                }
                if (Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Subtota_Cotizado].ToString().Trim() != String.Empty)
                {
                    Subtotal_Cotizado = Subtotal_Cotizado + double.Parse(Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Subtota_Cotizado].ToString().Trim());
                }
                else
                {
                    Subtotal_Cotizado = Subtotal_Cotizado + 0;
                }
                if (Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Total_Cotizado].ToString().Trim() != String.Empty)
                {
                    Total_Cotizado = Subtotal_Cotizado + IEPS_Cotizado + IVA_Cotizado;//Total_Cotizado + double.Parse(Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Total_Cotizado].ToString().Trim());
                }
                else
                {
                    Total_Cotizado = Total_Cotizado + 0 ;
                }
            }
            Txt_Total_Cotizado_Requisicion.Text = Total_Cotizado.ToString();
            Txt_SubTotal_Cotizado_Requisicion.Text = Subtotal_Cotizado.ToString();
            Txt_IVA_Cotizado.Text = IVA_Cotizado.ToString(); 




        }
        else
        {

            Txt_Total_Cotizado_Requisicion.Text = "0.0";
            Txt_SubTotal_Cotizado_Requisicion.Text = "0.0";
            Txt_IVA_Cotizado.Text = "0.0"; 

        }
    }
    #endregion

    ///*******************************************************************************
    ///GRID
    ///*******************************************************************************
    #region Grid

    #region Grid_Requisicion
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:Llenar_Grid_Requisiciones
    ///DESCRIPCIÓN: Metodo que permite llenar el Grid_Requisiciones
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 01/JULIO/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_Grid_Requisiciones()
    {
        Cls_Ope_Tal_Propuesta_Ganadora_Negocio Clase_Negocio = new Cls_Ope_Tal_Propuesta_Ganadora_Negocio();
        DataTable Dt_Requisiciones = Clase_Negocio.Consultar_Requisiciones();
        if (Dt_Requisiciones.Rows.Count != 0)
        {
            Session["Dt_Requisiciones"] = Dt_Requisiciones;
            Grid_Requisiciones.DataSource = Dt_Requisiciones;
            Grid_Requisiciones.DataBind();

        }
        else
        {
            Grid_Requisiciones.DataSource = new DataTable();
            Grid_Requisiciones.DataBind();

        }



    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Requisiciones_SelectedIndexChanged
    ///DESCRIPCIÓN: Evento del Grid_Requisiciones al seleccionar
    ///PARAMETROS:   
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 01/JULIO/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Requisiciones_SelectedIndexChanged(object sender, EventArgs e)
    {

        Cls_Ope_Tal_Propuesta_Ganadora_Negocio Clase_Negocio = new Cls_Ope_Tal_Propuesta_Ganadora_Negocio();

        GridViewRow Row = Grid_Requisiciones.SelectedRow;
        Clase_Negocio.P_No_Requisicion = Grid_Requisiciones.SelectedDataKey["No_Requisicion"].ToString();
        Session["No_Requisicion"] = Clase_Negocio.P_No_Requisicion;
        //llenamos combo estatus
        Llenar_Combo_Estatus();

        //Consultamos los detalles del producto seleccionado 
        DataTable Dt_Detalle_Requisicion = Clase_Negocio.Consultar_Detalle_Requisicion();
        //Mostramos el div de detalle y el grid de Requisiciones
        Div_Grid_Requisiciones.Visible = false;
        Div_Detalle_Requisicion.Visible = true;
        Btn_Salir.ToolTip = "Listado";
        //llenamos la informacion del detalle de la requisicion seleccionada
        Hdf_Dependencia_ID.Value = Dt_Detalle_Requisicion.Rows[0][Cat_Dependencias.Campo_Dependencia_ID].ToString().Trim();
        Txt_Dependencia.Text = Dt_Detalle_Requisicion.Rows[0]["DEPENDENCIA"].ToString().Trim();
        Txt_Folio.Text = Dt_Detalle_Requisicion.Rows[0]["FOLIO"].ToString().Trim();
        Txt_Concepto.Text = Dt_Detalle_Requisicion.Rows[0]["CONCEPTO"].ToString().Trim();
        Txt_Fecha_Generacion.Text = Dt_Detalle_Requisicion.Rows[0]["FECHA_GENERACION"].ToString().Trim();
        Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByText(Dt_Detalle_Requisicion.Rows[0]["ESTATUS"].ToString().Trim()));
        Txt_Justificacion.Text = Dt_Detalle_Requisicion.Rows[0]["JUSTIFICACION_COMPRA"].ToString().Trim();
        Txt_Especificacion.Text = Dt_Detalle_Requisicion.Rows[0]["ESPECIFICACION_PROD_SERV"].ToString().Trim();
        
        Txt_IVA_Cotizado.Text = Dt_Detalle_Requisicion.Rows[0][Ope_Com_Requisiciones.Campo_IVA_Cotizado].ToString().Trim();
        
        Txt_Total_Cotizado_Requisicion.Text = Dt_Detalle_Requisicion.Rows[0][Ope_Com_Requisiciones.Campo_Total_Cotizado].ToString().Trim();
        Txt_SubTotal_Cotizado_Requisicion.Text = Dt_Detalle_Requisicion.Rows[0][Ope_Com_Requisiciones.Campo_Subtotal_Cotizado].ToString().Trim();
        Session["TIPO_ARTICULO"] = "PRODUCTO";
        Session["Concepto_ID"] = Dt_Detalle_Requisicion.Rows[0]["CONCEPTO_ID"].ToString().Trim();
        //VALIDAMOS EL CAMPO DE VERIFICAR CARACTERISTICAS, GARANTIA Y POLIZAS
        if (Dt_Detalle_Requisicion.Rows[0]["VERIFICACION_ENTREGA"].ToString().Trim() == "NO" || Dt_Detalle_Requisicion.Rows[0]["VERIFICACION_ENTREGA"].ToString().Trim() == String.Empty)
        {
            Chk_Verificacion.Checked = false;
        }
        if (Dt_Detalle_Requisicion.Rows[0]["VERIFICACION_ENTREGA"].ToString().Trim() == "SI")
        {
            Chk_Verificacion.Checked = true;
        }
        //Consultamos los productos de esta requisicion
        Clase_Negocio.P_Tipo_Articulo = Session["TIPO_ARTICULO"].ToString().Trim();
        DataTable Dt_Productos = Clase_Negocio.Consultar_Productos_Servicios();
        if (Dt_Productos.Rows.Count != 0)
        {
            Session["Dt_Productos"] = Dt_Productos;
            Grid_Productos.DataSource = Dt_Productos;
            Grid_Productos.DataBind();
            Grid_Productos.Visible = true;
            Grid_Productos.Enabled = false;
            //Llenamos los Text Box con los datos del Dt_Productos
            //Consultamos los proveedores
            Clase_Negocio.P_Concepto_ID = Session["Concepto_ID"].ToString().Trim();
            DataTable Dt_Proveedores = Clase_Negocio.Consultar_Proveedores();
            Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Proveedores, Dt_Proveedores);
            
            //for (int i = 0; i < Grid_Productos.Rows.Count; i++)
            //{

            //    DropDownList Cmb_Temporal_Proveedores = (DropDownList)Grid_Productos.Rows[i].FindControl("Cmb_Proveedor");
            //    Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Temporal_Proveedores, Dt_Proveedores);
                //Cmb_Temporal_Proveedores.SelectedIndex = 0;
                //Llenamos los Combos de acuerdo al proveedor que tiene asignado  && Cmb_Temporal_Proveedores.SelectedItem.Text.Contains(Dt_Productos.Rows[i]["Proveedor_ID"].ToString().Trim())
            if (Dt_Productos.Rows[0]["Proveedor_ID"].ToString().Trim() != String.Empty)
            {
                try
                {
                    Cmb_Proveedores.SelectedIndex = Cmb_Proveedores.Items.IndexOf(Cmb_Proveedores.Items.FindByText(Dt_Productos.Rows[0]["Nombre_Proveedor"].ToString().Trim()));
                }
                catch
                {
                    //En caso de no encontrar el proveedor se selecciona vacio el combo
                    Cmb_Proveedores.SelectedIndex = 0;
                }
            }

            //}//Fin del for
        }
        Div_Grid_Requisiciones.Visible = false;
        Div_Detalle_Requisicion.Visible = true;
        Btn_Salir.ToolTip = "Listado";
        Cmb_Proveedores.Enabled = false;

        Btn_Modificar_Click(Btn_Modificar, null);
    }

     ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:Grid_Requisiciones_Sorting
    ///DESCRIPCIÓN: Evento para ordenar por columna seleccionada en el Grid_Requisiciones
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 01/JULIO/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Requisiciones_Sorting(object sender, GridViewSortEventArgs e)
    {
    }

    #endregion Grid_Requisiciones

    #region Grid_Productos
    public void Llenar_Grid_Productos()
    {
        Cls_Ope_Tal_Propuesta_Ganadora_Negocio Clase_Negocio = new Cls_Ope_Tal_Propuesta_Ganadora_Negocio();
        DataTable Dt_Requisiciones = Clase_Negocio.Consultar_Requisiciones();
        if (Dt_Requisiciones.Rows.Count != 0)
        {
            Session["Dt_Requisiciones"] = Dt_Requisiciones;
            Grid_Requisiciones.DataSource = Dt_Requisiciones;
            Grid_Requisiciones.DataBind();

        }
        else
        {
            Grid_Requisiciones.DataSource = new DataTable();
            Grid_Requisiciones.DataBind();

        }


    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:Grid_Productos_Sorting
    ///DESCRIPCIÓN: Evento para ordenar por columna seleccionada en el Grid_Productos
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 01/JULIO/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Productos_Sorting(object sender, GridViewSortEventArgs e)
    {

    }



    #endregion


    #endregion

    ///*******************************************************************************
    ///EVENTOS
    ///*******************************************************************************
    #region Eventos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Evento del Boton Salir
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 01/JULIO/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {

        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        switch (Btn_Salir.ToolTip)
        {
            case "Inicio":
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                //LIMPIAMOS VARIABLES DE SESSION
                Session["Dt_Requisiciones"] = null;

                Session["No_Requisicion"] = null;

                break;
            case "Cancelar":
                Configurar_Formulario("Inicio");
                Llenar_Grid_Requisiciones();
                Limpiar_Componentes();
                break;
            case "Listado":
                Configurar_Formulario("Inicio");
                Llenar_Grid_Requisiciones();
                Limpiar_Componentes();


                break;
        }
    }

    protected void Btn_Calcular_Precios_Cotizados_Click(object sender, EventArgs e)
    {
        DataTable Dt_Productos = new DataTable();
        Dt_Productos = (DataTable)Session["Dt_Productos"];
        Cls_Ope_Tal_Propuesta_Ganadora_Negocio Clase_Negocio = new Cls_Ope_Tal_Propuesta_Ganadora_Negocio();

        Clase_Negocio.P_No_Requisicion = Session["No_Requisicion"].ToString().Trim();

        for (int i = 0; i < Dt_Productos.Rows.Count; i++)
        {
            Dt_Productos = (DataTable)Session["Dt_Productos"];
            DropDownList Cmb_Temporal_Proveedores = (DropDownList)Grid_Productos.Rows[i].FindControl("Cmb_Proveedor");
            //Asignamos los valores que se cotizaron en la propuesta del cotizador
            Clase_Negocio.P_Producto_ID = Dt_Productos.Rows[i]["PROD_SERV_ID"].ToString().Trim();

            //Asignamos el proveedor seleccionado al grid
            if (Cmb_Temporal_Proveedores.SelectedIndex != 0)
            {
                Clase_Negocio.P_Proveedor_ID = Cmb_Temporal_Proveedores.SelectedValue;
                Dt_Productos.Rows[i]["Proveedor_ID"] = Cmb_Temporal_Proveedores.SelectedValue;
                Dt_Productos.Rows[i]["Nombre_Proveedor"] = Cmb_Temporal_Proveedores.SelectedItem;
                //Consutamos el producto seleccionado para pasar los valores calculados de lo ya cotizado del producto k corresponde a la posicion del for
                DataTable Dt_Producto_Propuesta = Clase_Negocio.Consultar_Productos_Propuesta();
                Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_IEPS_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_IEPS_Cotizado].ToString().Trim();
                Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_IVA_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_IVA_Cotizado].ToString().Trim();
                Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Subtota_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_Subtota_Cotizado].ToString().Trim();
                Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Total_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_Total_Cotizado].ToString().Trim();
                Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_Precio_U_Sin_Imp_Cotizado].ToString().Trim();
                Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_Precio_U_Con_Imp_Cotizado].ToString().Trim();
            }
            Session["Dt_Productos"] = Dt_Productos;

        }
        //llenamos nuevamente el grid de productos
        Grid_Productos.DataSource = Dt_Productos;
        Grid_Productos.DataBind();
        //Cargamos nuevamente el grid con los proveedores y el proveedor seleccionado 
        DataTable Dt_Proveedores = Clase_Negocio.Consultar_Proveedores();
        for (int i = 0; i < Grid_Productos.Rows.Count; i++)
        {

            DropDownList Cmb_Temporal_Proveedores = (DropDownList)Grid_Productos.Rows[i].FindControl("Cmb_Proveedor");
            Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Temporal_Proveedores, Dt_Proveedores);
            Cmb_Temporal_Proveedores.SelectedIndex = 0;
            //Llenamos los Combos de acuerdo al proveedor que tiene asignado  && Cmb_Temporal_Proveedores.SelectedItem.Text.Contains(Dt_Productos.Rows[i]["Proveedor_ID"].ToString().Trim())
            if (Dt_Productos.Rows[i]["Proveedor_ID"].ToString().Trim() != String.Empty)
            {
                try
                {
                    Cmb_Temporal_Proveedores.SelectedIndex = Cmb_Temporal_Proveedores.Items.IndexOf(Cmb_Temporal_Proveedores.Items.FindByText(Dt_Productos.Rows[i]["Nombre_Proveedor"].ToString().Trim()));
                }
                catch
                {
                    //En caso de no encontrar el proveedor se selecciona vacio el combo
                    Cmb_Temporal_Proveedores.SelectedIndex = 0;
                }
            }

        }//Fin del for


        //CAlculamos el total cotizado
        Calcular_Importe_Total();
    }

    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Cls_Ope_Tal_Propuesta_Ganadora_Negocio Clase_Negocio = new Cls_Ope_Tal_Propuesta_Ganadora_Negocio();
        
        switch (Btn_Modificar.ToolTip)
        {
            case "Modificar":
                if (Txt_Folio.Text != String.Empty)
                {
                    Configurar_Formulario("Nuevo");
                    Cmb_Estatus.Enabled = true;
                    Cmb_Proveedores.Enabled = true;
                }
                else
                {
                    Div_Contenedor_Msj_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Es necesario selecciona una Requisicion";
                }
                break;
            case "Guardar":
                String Proyecto_ID = "";                
                //Realizamos las validaciones
                if (Cmb_Estatus.SelectedIndex == 0)
                {
                    Div_Contenedor_Msj_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Es necesario seleccionar un Estatus";
                }//fin del if
                if (Txt_Total_Cotizado_Requisicion.Text.Trim() == String.Empty)
                {
                    Div_Contenedor_Msj_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "</br>Es necesario calcular los precios";
                }
                else
                {
                    if (Convert.ToDouble(Txt_Total_Cotizado_Requisicion.Text.Trim()) <= 0)
                    {
                        Div_Contenedor_Msj_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "</br>Es necesario cotizar los precios";
                    }
                }
                //Validamos en caso de seleccionar el estatus de Cotizada
                if (Cmb_Estatus.SelectedValue.Trim() == "COTIZADA")
                {
                    DataTable Dt_Productos = (DataTable)Session["Dt_Productos"];
                    //Validamos que todos los productos sean cotizados
                    for (int i = 0; i < Dt_Productos.Rows.Count; i++)
                    {
                        if (Dt_Productos.Rows[i]["Precio_U_Sin_Imp_Cotizado"].ToString().Trim() == String.Empty)
                        {
                            Div_Contenedor_Msj_Error.Visible = true;
                            Lbl_Mensaje_Error.Text = "Es necesario asignar precio cotizado a todos los productos";
                            break;
                        }
                    }
                }
                if (Cmb_Estatus.SelectedValue.Trim() == "COTIZADA") { 
                    Cls_Ope_Tal_Administrar_Requisiciones_Negocio Requisicion_Negocio = new Cls_Ope_Tal_Administrar_Requisiciones_Negocio();
                    Requisicion_Negocio.P_Requisicion_ID = Session["No_Requisicion"].ToString().Trim();
                    DataTable Dt_Requisicion = Requisicion_Negocio.Consultar_Codigo_Programatico_RQ();
                    DataTable Dt_Partidas_Reserva = Cls_Ope_Psp_Manejo_Presupuesto.Consultar_Detalles_Reserva(Dt_Requisicion.Rows[0]["NUM_RESERVA"].ToString().Trim());
                    String Partida_ID = Dt_Partidas_Reserva.Rows[0][Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString().Trim();
                    Proyecto_ID = Dt_Partidas_Reserva.Rows[0][Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                    String Dependencia_ID = Hdf_Dependencia_ID.Value.Trim();//Dt_Partidas_Reserva.Rows[0][Ope_Psp_Reservas.Campo_Dependencia_ID].ToString().Trim();
                    String FF = Dt_Partidas_Reserva.Rows[0][Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim();
                    Int32 Anio = DateTime.Today.Year;//Convert.ToInt32(Dt_Partidas_Reserva.Rows[0][Ope_Psp_Reservas.Campo_Anio]);
                    Double Total = ((!String.IsNullOrEmpty(Dt_Requisicion.Rows[0]["TOTAL"].ToString())) ? Convert.ToDouble(Dt_Requisicion.Rows[0]["TOTAL"].ToString().Trim()) : 0.0);
                    Double Disponible = Cls_Ope_Psp_Manejo_Presupuesto.Consultar_Disponible_Partida(FF, Proyecto_ID, Dependencia_ID, Partida_ID, Anio.ToString(), Ope_Psp_Presupuesto_Aprobado.Campo_Disponible);//Double.Parse(Dt_Presupuesto_Actual.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Disponible].ToString().Trim());
                    //DataTable Dt_Presupuesto_Actual = Cls_Ope_Psp_Manejo_Presupuesto.Consultar_Presupuesto_Aprobado(Dependencia_ID, FF, Proyecto_ID, "", Partida_ID, Anio);
                    if (!String.IsNullOrEmpty(Txt_Total_Cotizado_Requisicion.Text.Trim())) {
                        Double Monto_Anterior = Total;
                        Double Monto_Cotizado = Convert.ToDouble(Txt_Total_Cotizado_Requisicion.Text.Trim());
                        if (Monto_Cotizado > Monto_Anterior) {
                            Double Diferencia = Monto_Cotizado - Monto_Anterior;                            
                            if (Disponible < Diferencia)
                            {
                                Div_Contenedor_Msj_Error.Visible = true;
                                Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ No existe presupuesto suficiente </br>";
                            }
                        }
                    } 
                }
                    

                if (Div_Contenedor_Msj_Error.Visible == false)
                {
                    //Cargamos datos al objeto de clase negocio
                    Clase_Negocio.P_No_Requisicion = Session["No_Requisicion"].ToString().Trim();
                    Clase_Negocio.P_Dt_Productos = (DataTable)Session["Dt_Productos"];
                    Clase_Negocio.P_Estatus = Cmb_Estatus.SelectedValue;
                    Clase_Negocio.P_Dependencia_ID = Hdf_Dependencia_ID.Value;
                    
                    Clase_Negocio.P_IEPS_Cotizado = "0";
                    if (Txt_IVA_Cotizado.Text.Trim() != String.Empty)
                        Clase_Negocio.P_IVA_Cotizado = Txt_IVA_Cotizado.Text;
                    else
                        Clase_Negocio.P_IVA_Cotizado = "0";
                    if (Txt_Total_Cotizado_Requisicion.Text.Trim() != String.Empty)
                        Clase_Negocio.P_Total_Cotizado = Txt_Total_Cotizado_Requisicion.Text;
                    else
                        Clase_Negocio.P_Total_Cotizado = "0";
                    if (Txt_SubTotal_Cotizado_Requisicion.Text.Trim() != String.Empty)
                        Clase_Negocio.P_Subtotal_Cotizado = Txt_SubTotal_Cotizado_Requisicion.Text;
                    else
                        Clase_Negocio.P_Subtotal_Cotizado = "0";

                    bool Operacion_Realizada = false;
                    Operacion_Realizada = Clase_Negocio.Agregar_Cotizaciones();

                    if (Operacion_Realizada == true)
                    {
                        Operacion_Realizada = Clase_Negocio.Modificar_Requisicion();
                        if (Operacion_Realizada == true)
                        {
                            if (Cmb_Estatus.SelectedValue.Trim() == "COTIZADA") {
                                Cls_Ope_Tal_Administrar_Requisiciones_Negocio Requisicion_Negocio = new Cls_Ope_Tal_Administrar_Requisiciones_Negocio();
                                Requisicion_Negocio.P_Dependencia_ID = Hdf_Dependencia_ID.Value;
                                Requisicion_Negocio.P_Programa_ID = Proyecto_ID;
                                Requisicion_Negocio.P_Estatus = "CONFIRMADA";
                                Requisicion_Negocio.P_Observacion_ID = Requisicion_Negocio.Generar_ID();
                                Requisicion_Negocio.P_Folio = Txt_Folio.Text;
                                Requisicion_Negocio.P_Tipo = "TRANSITORIA";
                                Requisicion_Negocio.P_Tipo_Articulo = "PRODUCTO";
                                Requisicion_Negocio.P_Estatus_Busqueda = null;
                                Requisicion_Negocio.P_Campo_Busqueda = null;
                                Requisicion_Negocio.P_Fecha_Inicial = null;
                                DataSet Dato_Requisicion_ID = Requisicion_Negocio.Consulta_Requisiciones();
                                Requisicion_Negocio.P_Requisicion_ID = Session["No_Requisicion"].ToString().Trim();
                                Requisicion_Negocio.P_Comentario = "";
                                Requisicion_Negocio.Modificar_Requisicion();
                            }
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Cotizacion Ganadora", "alert('Se Guardo la Cotizacion Ganadora Exitosamente');", true);
                            Configurar_Formulario("Inicio");
                            Limpiar_Componentes();
                            Llenar_Grid_Requisiciones();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Cotizacion Ganadora", "alert('No se Guardo la Cotizacion Ganadora');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Cotizacion Ganadora", "alert('No se Guardo la Cotizacion Ganadora');", true);
                    }                    
                }

                break;

        }//fin del switch
    }

    ///*************************************************************************************************************************
    ///Nombre: Cmb_Proveedores_SelectedIndexChanged
    ///Descripción: Muestra los costos por proveedor
    ///Parámetros: 
    ///CREO: Jesus Toledo Rdz
    ///FECHA_CREO: 17/Dic/2012
    ///Usuario Modifico:
    ///Fecha Modifico:
    ///Causa Modificación:
    ///*************************************************************************************************************************
    protected void Cmb_Proveedores_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable Dt_Productos = new DataTable();
        Dt_Productos = (DataTable)Session["Dt_Productos"];
        Cls_Ope_Tal_Propuesta_Ganadora_Negocio Clase_Negocio = new Cls_Ope_Tal_Propuesta_Ganadora_Negocio();

        Clase_Negocio.P_No_Requisicion = Session["No_Requisicion"].ToString().Trim();

        for (int i = 0; i < Dt_Productos.Rows.Count; i++)
        {
            Dt_Productos = (DataTable)Session["Dt_Productos"];
            //Asignamos los valores que se cotizaron en la propuesta del cotizador
            Clase_Negocio.P_Producto_ID = Dt_Productos.Rows[i]["PROD_SERV_ID"].ToString().Trim();

            //Asignamos el proveedor seleccionado al grid
            if (Cmb_Proveedores.SelectedIndex != 0)
            {
                Clase_Negocio.P_Proveedor_ID = Cmb_Proveedores.SelectedValue;
                Dt_Productos.Rows[i]["Proveedor_ID"] = Cmb_Proveedores.SelectedValue;
                Dt_Productos.Rows[i]["Nombre_Proveedor"] = Cmb_Proveedores.SelectedItem;
                //Consutamos el producto seleccionado para pasar los valores calculados de lo ya cotizado del producto k corresponde a la posicion del for
                DataTable Dt_Producto_Propuesta = Clase_Negocio.Consultar_Productos_Propuesta();
                Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_IEPS_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_IEPS_Cotizado].ToString().Trim();
                Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_IVA_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_IVA_Cotizado].ToString().Trim();
                Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Subtota_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_Subtota_Cotizado].ToString().Trim();
                Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Total_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_Total_Cotizado].ToString().Trim();
                Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_Precio_U_Sin_Imp_Cotizado].ToString().Trim();
                Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_Precio_U_Con_Imp_Cotizado].ToString().Trim();
                if (Dt_Productos.Columns[Ope_Tal_Propuesta_Cotizacion.Campo_Descripcion_Producto] == null) Dt_Productos.Columns.Add(Ope_Tal_Propuesta_Cotizacion.Campo_Descripcion_Producto, Type.GetType("System.String"));
                if (Dt_Productos.Columns[Ope_Tal_Req_Refaccion.Campo_Marca_OC] == null) Dt_Productos.Columns.Add(Ope_Tal_Req_Refaccion.Campo_Marca_OC, Type.GetType("System.String"));
                Dt_Productos.Rows[i][Ope_Tal_Propuesta_Cotizacion.Campo_Descripcion_Producto] = Dt_Producto_Propuesta.Rows[0][Ope_Tal_Propuesta_Cotizacion.Campo_Descripcion_Producto].ToString().Trim();
                Dt_Productos.Rows[i][Ope_Tal_Req_Refaccion.Campo_Marca_OC] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_Marca].ToString().Trim();
            }
            Session["Dt_Productos"] = Dt_Productos;
            //MARCA, DESCRIPCION_PRODUCTO
        }
        //llenamos nuevamente el grid de productos
        Grid_Productos.DataSource = Dt_Productos;
        Grid_Productos.DataBind();
        Calcular_Importe_Total();
    }

    ///*************************************************************************************************************************
    ///Nombre: Btn_Descargar_Cuadro_Comparativo_Click
    ///Descripción: Descarga el cuadro comparivo
    ///Parámetros: 
    ///CREO: Jesus Toledo Rdz
    ///FECHA_CREO: 17/Dic/2012
    ///Usuario Modifico:
    ///Fecha Modifico:
    ///Causa Modificación:
    ///*************************************************************************************************************************
    protected void Btn_Descargar_Cuadro_Comparativo_Click(object sender, EventArgs e) {
        try {
            Generar_Cuadro_Comparativo();
        } catch (Exception Ex) {
            Lbl_Mensaje_Error.Text = "No se puede obtener el Cuadro comparativo.";
            Div_Contenedor_Msj_Error.Visible = true;
                
        }
    }

    ///*************************************************************************************************************************
    ///Nombre: Generar_Cuadro_Comparativo
    ///Descripción: genera cuadro comparativo. 
    ///Parámetros: 
    ///CREO: Jesus Toledo Rdz
    ///FECHA_CREO: 17/Dic/2012
    ///Usuario Modifico:
    ///Fecha Modifico:
    ///Causa Modificación:
    ///*************************************************************************************************************************
    private void Generar_Cuadro_Comparativo() 
    {
        try
        {
            DataTable Dt_Productos_Cotizados = new DataTable();
            DataTable Dt_Productos = new DataTable();
            Dt_Productos = (DataTable)Session["Dt_Productos"];
            Dt_Productos_Cotizados = ((DataTable)Session["Dt_Productos"]).Clone();
            if (Dt_Productos_Cotizados.Columns[Ope_Tal_Propuesta_Cotizacion.Campo_Descripcion_Producto] == null) Dt_Productos_Cotizados.Columns.Add(Ope_Tal_Propuesta_Cotizacion.Campo_Descripcion_Producto, Type.GetType("System.String"));
            if (Dt_Productos_Cotizados.Columns[Ope_Tal_Req_Refaccion.Campo_Marca_OC] == null) Dt_Productos_Cotizados.Columns.Add(Ope_Tal_Req_Refaccion.Campo_Marca_OC, Type.GetType("System.String"));
            Cls_Ope_Tal_Propuesta_Ganadora_Negocio Clase_Negocio = new Cls_Ope_Tal_Propuesta_Ganadora_Negocio();

            Clase_Negocio.P_No_Requisicion = Session["No_Requisicion"].ToString().Trim();
            for (int Combo = 1; Combo < Cmb_Proveedores.Items.Count; Combo++)
            {
                for (int i = 0; i < Dt_Productos.Rows.Count; i++)
                {
                    //Asignamos los valores que se cotizaron en la propuesta del cotizador
                    Clase_Negocio.P_Producto_ID = Dt_Productos.Rows[i]["PROD_SERV_ID"].ToString().Trim();
                    //Asignamos el proveedor seleccionado al grid
                    Clase_Negocio.P_Proveedor_ID = Cmb_Proveedores.Items[Combo].Value;
                    DataRow Renglon_Refaccion = Dt_Productos_Cotizados.NewRow();
                    Renglon_Refaccion["Proveedor_ID"] = Cmb_Proveedores.SelectedValue;
                    Renglon_Refaccion["Nombre_Proveedor"] = Cmb_Proveedores.Items[Combo].ToString();
                    //Consutamos el producto seleccionado para pasar los valores calculados de lo ya cotizado del producto k corresponde a la posicion del for
                    DataTable Dt_Producto_Propuesta = Clase_Negocio.Consultar_Productos_Propuesta();
                    Renglon_Refaccion[Ope_Tal_Req_Refaccion.Campo_Cantidad] = Dt_Producto_Propuesta.Rows[0][Ope_Tal_Propuesta_Cotizacion.Campo_Cantidad].ToString().Trim();
                    Renglon_Refaccion["NOMBRE_PROD_SERV"] = Dt_Producto_Propuesta.Rows[0][Ope_Tal_Propuesta_Cotizacion.Campo_Descripcion_Producto].ToString().Trim();
                    Renglon_Refaccion[Ope_Tal_Req_Refaccion.Campo_IEPS_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Tal_Propuesta_Cotizacion.Campo_IEPS_Cotizado].ToString().Trim();
                    Renglon_Refaccion[Ope_Tal_Req_Refaccion.Campo_IVA_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Tal_Propuesta_Cotizacion.Campo_IVA_Cotizado].ToString().Trim();
                    Renglon_Refaccion[Ope_Tal_Req_Refaccion.Campo_Subtota_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Tal_Propuesta_Cotizacion.Campo_Subtota_Cotizado].ToString().Trim();
                    Renglon_Refaccion[Ope_Tal_Req_Refaccion.Campo_Total_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Tal_Propuesta_Cotizacion.Campo_Total_Cotizado].ToString().Trim();
                    Renglon_Refaccion[Ope_Tal_Req_Refaccion.Campo_Precio_U_Sin_Imp_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Tal_Propuesta_Cotizacion.Campo_Precio_U_Sin_Imp_Cotizado].ToString().Trim();
                    Renglon_Refaccion[Ope_Tal_Req_Refaccion.Campo_Precio_U_Con_Imp_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Tal_Propuesta_Cotizacion.Campo_Precio_U_Con_Imp_Cotizado].ToString().Trim();
                    Renglon_Refaccion[Ope_Tal_Propuesta_Cotizacion.Campo_Descripcion_Producto] = Dt_Producto_Propuesta.Rows[0][Ope_Tal_Propuesta_Cotizacion.Campo_Descripcion_Producto].ToString().Trim();
                    Renglon_Refaccion[Ope_Tal_Req_Refaccion.Campo_Marca_OC] = Dt_Producto_Propuesta.Rows[0][Ope_Tal_Propuesta_Cotizacion.Campo_Marca].ToString().Trim();
                    Dt_Productos_Cotizados.Rows.Add(Renglon_Refaccion);
                }
            }
            Pasar_DataTable_A_Excel(Dt_Productos_Cotizados);
        }
        catch (Exception Ex) { throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]"); }
    }

    ///*************************************************************************************************************************
    ///Nombre: Pasar_DataTable_A_Excel
    ///Descripción: Pasa DataTable a Excel. 
    ///Parámetros: Dt_Reporte.- DataTable que se pasara a excel. 
    ///CREO: Salvador Vázquez Camacho.
    ///FECHA_CREO: 14/Junio/2012
    ///Usuario Modifico:
    ///Fecha Modifico:
    ///Causa Modificación:
    ///*************************************************************************************************************************
    public void Pasar_DataTable_A_Excel(System.Data.DataTable P_Dt_Reporte)
    {
        Int32 No_Proveedores = Cmb_Proveedores.Items.Count - 1;
        String Ruta = "Cuadro_Comparativo_" + Txt_Folio.Text.Trim() + ".xls";
        DataTable Dt_Reporte = new DataTable();
        Dt_Reporte = P_Dt_Reporte.Copy();
        Dt_Reporte.Columns.RemoveAt(11);
        Dt_Reporte.Columns.RemoveAt(9);
        Dt_Reporte.Columns.RemoveAt(8);
        Dt_Reporte.Columns.RemoveAt(6);
        Dt_Reporte.Columns.RemoveAt(3);
        Dt_Reporte.Columns.RemoveAt(1);
        Dt_Reporte.Columns.RemoveAt(0);
        Dt_Reporte.Columns["NOMBRE_PROD_SERV"].ColumnName = "Refacción";
        Dt_Reporte.Columns["DESCRIPCION_PRODUCTO"].ColumnName = "Descripción";
        Dt_Reporte.Columns["CANTIDAD"].ColumnName = "Cantidad";
        Dt_Reporte.Columns["PRECIO_U_SIN_IMP_COTIZADO"].ColumnName = "Precio unitario";
        Dt_Reporte.Columns["IVA_COTIZADO"].ColumnName = "IVA";
        Dt_Reporte.Columns["TOTAL_COTIZADO"].ColumnName = "Total";
        Dt_Reporte.Columns["NOMBRE_PROVEEDOR"].ColumnName = "Proveedor";
        Dt_Reporte.Columns["MARCA_OC"].ColumnName = "Marca";

        try
        {
            //Creamos el libro de Excel.
            CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();

            Libro.Properties.Title = "Reporte de Tarjeta de Gasolina";
            Libro.Properties.Created = DateTime.Now;
            Libro.Properties.Author = "Patrimonio";

            //Creamos una hoja que tendrá el libro.
            CarlosAg.ExcelXmlWriter.Worksheet Hoja = Libro.Worksheets.Add("Registros");
            //Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon = Hoja.Table.Rows.Add();
            //Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");

            Estilo_Cabecera.Font.FontName = "Tahoma";
            Estilo_Cabecera.Font.Size = 10;
            Estilo_Cabecera.Font.Bold = true;
            Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Cabecera.Font.Color = "#FFFFFF";
            Estilo_Cabecera.Interior.Color = "#193d61";
            Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Alignment.WrapText = true;

            Estilo_Contenido.Font.FontName = "Tahoma";
            Estilo_Contenido.Font.Size = 8;
            Estilo_Contenido.Font.Bold = true;
            Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Contenido.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Contenido.Font.Color = "#000000";
            Estilo_Contenido.Interior.Color = "White";
            Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Alignment.WrapText = true;

            //Agregamos las columnas que tendrá la hoja de excel.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(250));//Refaccion
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(60));//Cantidad
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//Precio unitario
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(70));//IVA
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(70));//Total.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(250));//Proveedor
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(250));//Descripcion.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(180));//Marca.

            if (Dt_Reporte is System.Data.DataTable)
            {
                if (Dt_Reporte.Rows.Count > 0)
                {
                    foreach (System.Data.DataColumn COLUMNA in Dt_Reporte.Columns)
                    {
                        if (COLUMNA is System.Data.DataColumn)
                        {
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(COLUMNA.ColumnName, "HeaderStyle"));
                        }
                        Renglon.Height = 20;
                    }

                    foreach (System.Data.DataRow FILA in Dt_Reporte.Rows)
                    {
                        if (FILA is System.Data.DataRow)
                        {
                            Renglon = Hoja.Table.Rows.Add();
                            foreach (System.Data.DataColumn COLUMNA in Dt_Reporte.Columns)
                            {
                                if (COLUMNA is System.Data.DataColumn)
                                {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(FILA[COLUMNA.ColumnName].ToString(), DataType.String, "BodyStyle"));
                                }
                            }
                            Renglon.Height = 20;
                            Renglon.AutoFitHeight = true;
                        }
                    }
                }
            }
                

            //Abre el archivo de excel
            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Ruta);
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            Libro.Save(Response.OutputStream);
            Response.End();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }


    #endregion




}
