﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Common;
using System.Windows.Forms;
using System.Data.OleDb;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Catalogo_Taller_Refacciones.Negocio;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;
using JAPAMI.Taller_Mecanico.Catalogo_Tipos_Refacciones.Negocio;

public partial class paginas_Taller_Mecanico_Frm_Ope_Tal_Migracion_Datos_Taller_Municipal : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Leer_Excel
    ///DESCRIPCIÓN:          Metodo que consulta un archivo EXCEL
    ///PARAMETROS:           String sqlExcel.- string que contiene el select
    ///CREO:                 Susana Trigueros Armenta
    ///FECHA_CREO:           23/Mayo/2011 
    ///MODIFICO:             Salvador Hernández Ramírez
    ///FECHA_MODIFICO:       26/Mayo/2011 
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public DataSet Leer_Excel(String sqlExcel, String Archivo)
    {
        //Para empezar definimos la conexión OleDb a nuestro fichero Excel.
        String Rta = @MapPath("../../Archivos/" + Archivo);
        String sConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                           "Data Source=" + Rta + ";" +
                           "Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";

        //Definimos el DataSet donde insertaremos los datos que leemos del excel
        DataSet DS = new DataSet();

        //Definimos la conexión OleDb al fichero Excel y la abrimos
        OleDbConnection oledbConn = new OleDbConnection(sConnectionString);
        oledbConn.Open();

        //Creamos un comand para ejecutar la sentencia SELECT.
        OleDbCommand oledbCmd = new OleDbCommand(sqlExcel, oledbConn);

        //Creamos un dataAdapter para leer los datos y asocialor al DataSet.
        OleDbDataAdapter da = new OleDbDataAdapter(oledbCmd);
        da.Fill(DS);
        oledbConn.Close();
        return DS;
    }

    protected void Btn_Subir_Refacciones_Click(object sender, EventArgs e) {
        DataSet Ds_Bienes_Muebles = new DataSet();
        String SqlExcel = "Select * From [REFACCIONES$]";
        Ds_Bienes_Muebles = Leer_Excel(SqlExcel, "DATOS_TALLER.xlsx");
        DataTable Bienes_Muebles = Ds_Bienes_Muebles.Tables[0];
        Int32 Filas_Leidas = Bienes_Muebles.Rows.Count;
        for (Int32 Fila_Actual = 1; Fila_Actual <= Filas_Leidas; Fila_Actual++) {
            Cls_Cat_Tal_Refacciones_Negocio Refacciones_Negocio = new Cls_Cat_Tal_Refacciones_Negocio();
            Refacciones_Negocio.P_Clave = Fila_Actual.ToString();
            Refacciones_Negocio.P_Nombre = (Bienes_Muebles.Rows[Fila_Actual]["NOMBRE"].ToString().Trim().ToUpper().Length > 0) ? Bienes_Muebles.Rows[Fila_Actual]["NOMBRE"].ToString().Trim().ToUpper() : "";
            Refacciones_Negocio.P_Descripcion = (Bienes_Muebles.Rows[Fila_Actual]["DESCRIPCION"].ToString().Trim().ToUpper().Length > 0) ? Bienes_Muebles.Rows[Fila_Actual]["DESCRIPCION"].ToString().Trim().ToUpper() : "";
            Refacciones_Negocio.P_Costo_Unitario = (Bienes_Muebles.Rows[Fila_Actual]["COSTO_UNITARIO"].ToString().Trim().ToUpper().Length > 0) ? Bienes_Muebles.Rows[Fila_Actual]["COSTO_UNITARIO"].ToString().Trim().ToUpper() : "0";
            Refacciones_Negocio.P_Ubicacion = "PENDIENTE";
            Refacciones_Negocio.P_Tipo = "TRANSITORIO";
            Refacciones_Negocio.P_Estatus = "VIGENTE";
            Refacciones_Negocio.P_Tipo_Refaccion_ID = Obtener_ID_Tipo_Refaccion(Bienes_Muebles.Rows[Fila_Actual]["TIPO_REFACCION"].ToString().Trim().ToUpper());
            Refacciones_Negocio.P_Unidad_ID = Obtener_ID_Unidad(Bienes_Muebles.Rows[Fila_Actual]["UNIDAD"].ToString().Trim().ToUpper());
            Refacciones_Negocio.Alta_Refaccion();
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "a", "alert('Cargadas');", true);
    }
    private String Obtener_ID_Tipo_Refaccion(String Dato)
    {
        String Color_ID = null;
        String Mi_SQL = null;
        Mi_SQL = "SELECT * FROM " + Cat_Tal_Tipos_Refacciones.Tabla_Cat_Tal_Tipos_Refacciones + " WHERE " + Cat_Tal_Tipos_Refacciones.Campo_Descripcion + "='" + Dato.Trim() + "'";
        DataSet Ds_Aux_Color = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
        if (Ds_Aux_Color != null && Ds_Aux_Color.Tables.Count > 0) {
            DataTable Dt_Aux_Color = Ds_Aux_Color.Tables[0];
            if (Dt_Aux_Color != null && Dt_Aux_Color.Rows.Count > 0) {
                Color_ID = Dt_Aux_Color.Rows[0][Cat_Tal_Tipos_Refacciones.Campo_Tipo_Refaccion_ID].ToString().Trim();
            }
        }
        if (Color_ID == null)
        {
            Cls_Cat_Tal_Tipos_Refacciones_Negocio Color_Negocio = new Cls_Cat_Tal_Tipos_Refacciones_Negocio();
            Color_Negocio.P_Descripcion = Dato.Trim();
            Color_Negocio.P_Estatus = "VIGENTE";
            Color_Negocio.P_Usuario = "INICIAL";
            Color_Negocio.Alta_Nivel();
            Color_ID = Obtener_ID_Tipo_Refaccion(Dato);
        }
        return Color_ID;
    }
    private String Obtener_ID_Unidad(String Dato)
    {
        String Color_ID = null;
        String Mi_SQL = null;
        Mi_SQL = "SELECT * FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + " WHERE " + Cat_Com_Unidades.Campo_Nombre + "='" + Dato.Trim() + "'";
        DataSet Ds_Aux_Color = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
        if (Ds_Aux_Color != null && Ds_Aux_Color.Tables.Count > 0)
        {
            DataTable Dt_Aux_Color = Ds_Aux_Color.Tables[0];
            if (Dt_Aux_Color != null && Dt_Aux_Color.Rows.Count > 0)
            {
                Color_ID = Dt_Aux_Color.Rows[0][Cat_Com_Unidades.Campo_Unidad_ID].ToString().Trim();
            }
        }
        return Color_ID;
    }
}
