﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Tal_Unidades_Responsables_Rol.aspx.cs" Inherits="paginas_Taller_Mecanico_Frm_Ope_Tal_Unidades_Responsables_Rol" Title="Accesos Rol - Taller " %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script type="text/javascript" language="javascript">
        function Limpiar_Ctlr_Busqueda_UR(){
            document.getElementById("<%=Txt_Busqueda_Clave.ClientID%>").value="";
            document.getElementById("<%=Txt_Busqueda_Nombre.ClientID%>").value="";                                
            return false;
        }
        function numbersonly(e) {
            var unicode = e.charCode ? e.charCode : e.keyCode
            if (unicode != 8 && unicode != 48) {
                if (unicode < 48 || unicode > 57) //if not a number
                //disable key press
                {
                    return false;
                }
            }
        }  
    </script>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScptM_Catalogo" runat="server" />  
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>                     
            </asp:UpdateProgress>
            <div id="Div_Requisitos" style="background-color:#ffffff; width:100%; height:100%;">
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo" colspan="2">Accesos Taller Mecanico Municipal [Unidades Responsables]</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                          <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;">              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top">
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                            </table>                   
                          </div>                
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>                        
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" style="width:50%;">
                            <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" ToolTip="Modificar" AlternateText="Modificar" Width="24px" OnClick="Btn_Modificar_Click"/>
                            <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" ToolTip="Salir" AlternateText="Salir" Width="24px" OnClick="Btn_Salir_Click"/> 
                        </td>
                        <td style="width:50%;">&nbsp;</td>                        
                    </tr>
                </table>   
                <br />
                <center>
                    <table width="98%" class="estilo_fuente">
                        <tr>
                            <td colspan="4">                                 
                                <asp:HiddenField ID="Hdf_Dependencia_ID" runat="server" /> 
                            </td>                                                                
                        </tr>
                        <tr>
                            <td style="width:25%;">No. Empleado</td>
                            <td style="width:75%;" colspan="3">
                                <asp:TextBox ID="Txt_No_Empleado" runat="server" onkeypress="return numbersonly(event);"></asp:TextBox>
                                 
                                <asp:ImageButton ID="Btn_Buscar" runat="server" 
                                 ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                 ToolTip="Buscar" onclick="Btn_Buscar_Click"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:25%;">Nombre Empleado</td>
                            <td style="width:75%;" colspan="3">
                                <asp:TextBox ID="Txt_Nombre_Empleado" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:25%;">U.Responsable del Empleado</td>
                            <td style="width:75%;" colspan="3">
                                <asp:DropDownList ID="Cmb_UR_Empleado" runat="server"  Width="98%">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                        <td colspan="4"><hr /></td>
                        </tr>              
                        <tr>                        
                            <td  style="width:15%; font-size:12px;">Clave</td>
                            <td  style="width:35%;" colspan="3">
                                <asp:TextBox ID="Txt_Clave" runat="server" Width="98%" TabIndex="0" Enabled="false"/>                        
                            </td>                                                                 
                        </tr>             
                        <tr>
                            <td  style="width:15%; font-size:12px;">Nombre Unidad Responsable</td>
                            <td  style="width:35%; text-align:left; cursor:default;" colspan="3">
                                <asp:TextBox ID="Txt_Nombre" runat="server" Width="93%" Enabled="false"/>     
                                <asp:ImageButton ID="Btn_Lanzar_Busqueda_UR" runat="server" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png" ToolTip="Buscar Rol" AlternateText="Buscar Rol" Height="16px" Width="16px" OnClick="Btn_Lanzar_Busqueda_UR_Click"/>                       
                            </td>                                                      
                        </tr>     
                        <tr>
                            <td  style="width:15%; vertical-align:top; font-size:12px;"> Comentarios </td>
                            <td  style="width:35%; " colspan="3">
                                <asp:TextBox ID="Txt_Comentarios" runat="server" TextMode="MultiLine" Width="99.5%" Height="50px" Enabled="false"/>
                            </td>                                                      
                        </tr>                                  
                        <tr>
                            <td style="width:100%;text-align:right;" colspan="4">
                                   <asp:Button ID="Btn_Agregar_Dependencia" runat="server"  Text="Agregar" CssClass="button" CausesValidation="false"  Width="200px" BackColor="Black" OnClick="Btn_Agregar_Dependencia_Click" /> 
                                   <asp:Button ID="Btn_Todas_Dependencias" runat="server"  Text="Agregar Todas" CssClass="button" CausesValidation="false"  Width="200px" BackColor="Black" OnClick="Btn_Todas_Dependencias_Click" />
                            </td>                                                     
                        </tr>  
                        <tr>
                            <td colspan="4"> <hr /> </td>                                                                
                        </tr>    
                        <tr>
                            <td colspan="4">
                                  <asp:GridView ID="Grid_UR" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                    ForeColor="#333333" GridLines="None" AllowPaging="false" Width="100%" 
                                    EmptyDataText="No hay datos cargados." >
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:BoundField DataField="DEPENDENCIA_ID" HeaderText="Dependencia ID" SortExpression="DEPENDENCIA_ID">
                                            <ItemStyle Width="3px" Font-Size="X-Small" HorizontalAlign="Center" />
                                            <HeaderStyle Width="3px" Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CLAVE" HeaderText="Clave" SortExpression="CLAVE" >
                                            <ItemStyle Width="120px" Font-Size="X-Small" HorizontalAlign="Center" />
                                            <HeaderStyle Width="120px" Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NOMBRE" HeaderText="Nombre" SortExpression="NOMBRE">
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                            <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Quitar">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="Btn_Quitar_Unidad_Responsable" runat="server" ImageUrl="~/paginas/imagenes/gridview/grid_delete.png" 
                                                        ToolTip="Quitar Unidad Responsable" AlternateText="Quitar Unidad Responsable" Height="16px" Width="16px"                                                        
                                                        CommandArgument='<%# Eval("DEPENDENCIA_ID") %>' OnClick="Btn_Quitar_Unidad_Responsable_Click" />
                                            </ItemTemplate>
                                            <ItemStyle Width="30px"  Font-Size="X-Small" HorizontalAlign="Left" />
                                            <HeaderStyle Width="30px"  Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView> 
                            </td>                                                                
                        </tr>    
                    </table>
                </center>        
            </div>
            <br />
            <br />
            <br />
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
        
    <asp:UpdatePanel ID="UpPnl_Aux_Busqueda_UR" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_MPE_UR" runat="server" Text="" style="display:none;"/>
                <cc1:ModalPopupExtender ID="MPE_UR" runat="server" 
                TargetControlID="Btn_Comodin_MPE_UR" PopupControlID="Pnl_Busqueda_Contenedor" 
                CancelControlID="Btn_Cerrar_Ventana"
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter"/>  
        </ContentTemplate>
    </asp:UpdatePanel> 
    
    <asp:Panel ID="Pnl_Busqueda_Contenedor" runat="server" CssClass="drag"  HorizontalAlign="Center" Width="850px" 
            style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">                         
            <asp:Panel ID="Pnl_Busqueda_Resguardante_Cabecera" runat="server" 
                style="cursor: move;background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
                <table width="99%">
                    <tr>
                        <td style="color:Black;font-size:12;font-weight:bold;">
                           <asp:Image ID="Img_Informatcion_Autorizacion" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                             B&uacute;squeda: Unidades Responsables
                        </td>
                        <td align="right" style="width:10%;">
                           <asp:ImageButton ID="Btn_Cerrar_Ventana" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana"
                                ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png"/>  
                        </td>
                    </tr>
                </table>            
            </asp:Panel>                                                                          
           <div style="color: #5D7B9D">
             <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;" >                                    
                        <asp:UpdatePanel ID="Upnl_Filtros_Busqueda_Prestamos" runat="server">
                            <ContentTemplate>
                            
                                <asp:UpdateProgress ID="Progress_Upnl_Filtros_Busqueda_Prestamos" runat="server" AssociatedUpdatePanelID="Upnl_Filtros_Busqueda_Prestamos" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                        <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress"><img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress> 
                                                             
                                  <table width="100%">
                                   <tr>
                                        <td style="width:100%" colspan="4" align="right">
                                            <asp:ImageButton ID="Btn_Limpiar_Ctlr_Busqueda" runat="server" OnClientClick="javascript:return Limpiar_Ctlr_Busqueda_UR();"
                                                ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de Busqueda"/>                         
                                        </td>
                                    </tr>     
                                   <tr>
                                        <td style="width:100%" colspan="4">
                                            <hr />
                                        </td>
                                    </tr>   
                                    <tr>
                                        <td style="width:20%;text-align:left;font-size:11px;">
                                            Clave
                                        </td>              
                                        <td style="width:30%;text-align:left;">
                                            <asp:TextBox ID="Txt_Busqueda_Clave" runat="server" Width="99.5%" />
                                           <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Clave" runat="server" FilterType="Custom, LowercaseLetters, Numbers, UppercaseLetters"
                                                TargetControlID="Txt_Busqueda_Clave"/>                                                                   
                                        </td>                                         
                                    </tr>    
                                    <tr>
                                        <td style="width:20%;text-align:left;font-size:11px;">
                                            Nombre
                                        </td>              
                                        <td colspan="3">
                                            <asp:TextBox ID="Txt_Busqueda_Nombre" runat="server" Width="99.5%" />
                                           <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_Nombre" runat="server" FilterType="Custom, LowercaseLetters, Numbers, UppercaseLetters"
                                                TargetControlID="Txt_Busqueda_Nombre" ValidChars="áéíóúÁÉÍÓÚ ñÑ"/>
                                            <cc1:TextBoxWatermarkExtender ID="Twm_Nombre" runat="server" 
                                                TargetControlID ="Txt_Busqueda_Nombre" WatermarkText="Busqueda por Nombre" 
                                                WatermarkCssClass="watermarked"/>                                                                                               
                                        </td>                                         
                                    </tr>                                                                  
                                   <tr>
                                        <td style="width:100%" colspan="4">
                                            <hr />
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <td style="width:100%;text-align:left;" colspan="4">
                                            <center>
                                               <asp:Button ID="Btn_Busqueda_UR" runat="server"  Text="Busqueda de UR" CssClass="button" CausesValidation="false"  Width="200px" OnClick="Btn_Busqueda_UR_Click" /> 
                                            </center>
                                        </td>                                                     
                                    </tr>                                                                        
                                  </table>   
                                  <br />
                                  <div id="Div_Resultados_Busqueda_UR" runat="server" style="border-style:outset; width:99%; height: 250px; overflow:auto;">
                                      <asp:GridView ID="Grid_Busqueda_UR" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                            ForeColor="#333333" GridLines="None" AllowPaging="True" Width="100%" 
                                            PageSize="100" EmptyDataText="No se encontrarón resultados para los filtros de la busqueda" 
                                            OnPageIndexChanging="Grid_Busqueda_UR_PageIndexChanging"
                                            OnSelectedIndexChanged="Grid_Busqueda_UR_SelectedIndexChanged"
                                            >
                                            <RowStyle CssClass="GridItem" />
                                            <Columns>
                                                <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                    <ItemStyle Width="30px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                    <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                                </asp:ButtonField>
                                                <asp:BoundField DataField="DEPENDENCIA_ID" HeaderText="Dependencia ID" SortExpression="DEPENDENCIA_ID">
                                                    <ItemStyle Width="3px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                    <HeaderStyle Width="3px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CLAVE" HeaderText="Clave" SortExpression="CLAVE" >
                                                    <ItemStyle Width="120px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                    <HeaderStyle Width="120px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="NOMBRE" HeaderText="Nombre" SortExpression="NOMBRE">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                    <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COMENTARIOS" HeaderText="Comentarios" SortExpression="COMENTARIOS">
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                    <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                            </Columns>
                                            <PagerStyle CssClass="GridHeader" />
                                            <SelectedRowStyle CssClass="GridSelected" />
                                            <HeaderStyle CssClass="GridHeader" />
                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView> 
                                </div>                                                                                                                                                          
                            </ContentTemplate>                                                                   
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>                                                      
                    </td>
                </tr>
             </table>                                                   
           </div>                 
    </asp:Panel>       
</asp:Content>

