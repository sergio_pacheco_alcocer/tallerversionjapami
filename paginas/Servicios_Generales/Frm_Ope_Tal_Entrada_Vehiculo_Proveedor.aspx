﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Tal_Entrada_Vehiculo_Proveedor.aspx.cs" Inherits="paginas_Taller_Mecanico_Frm_Ope_Tal_Entrada_Vehiculo_Proveedor"  Title="Entrada Vehículo Proveedor" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" 
    EnableScriptGlobalization="true"
        EnableScriptLocalization="true"
        EnablePartialRendering="true" 
        AsyncPostBackTimeout="9000">
</cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Loading" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>                     
            </asp:UpdateProgress>
            <div id="Div_Area_Trabajo" style="background-color:#ffffff; width:100%; height:100%;">
                <center>
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo" colspan="2">Entrada de Vehículo de Proveedor</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                          <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;">              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top">
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                            </table>                   
                          </div>                
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>                        
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" style="width:50%;">
                            <asp:ImageButton ID="Btn_Ejecutar_Entrada_Vehiculo" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_aceptarplan.png" Width="24px" CssClass="Img_Button" AlternateText="Ejecutar Entrada" ToolTip="Ejecutar Entrada" OnClick="Btn_Ejecutar_Entrada_Vehiculo_Click" />
                            <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" Width="24px" CssClass="Img_Button" AlternateText="Salir" OnClick="Btn_Salir_Click" />
                        </td>
                        <td style="width:50%;">&nbsp;                                 
                        </td>                        
                    </tr>
                </table>   
                <br />
                <div id="Div_Listado_Servicios" runat="server" style="width:100%;">
                    <br />
                    <asp:GridView ID="Grid_Listado_Servicios" runat="server" CssClass="GridView_1"
                        AutoGenerateColumns="False" AllowPaging="True" PageSize="20" Width="99%"
                        GridLines= "None" EmptyDataText="No se Encontrarón Servicios para hacer Entrada."
                        OnPageIndexChanging="Grid_Listado_Servicios_PageIndexChanging"
                        OnSelectedIndexChanged="Grid_Listado_Servicios_SelectedIndexChanged">
                        <RowStyle CssClass="GridItem" />
                        <Columns>
                            <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png" >
                                <ItemStyle Width="30px" />
                            </asp:ButtonField>
                            <asp:BoundField DataField="NO_ASIGNACION_PROV_SERV" HeaderText="NO_ASIGNACION_PROV_SERV" SortExpression="NO_ASIGNACION_PROV_SERV">
                                <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NO_SERVICIO" HeaderText="NO_SERVICIO" SortExpression="NO_SERVICIO">
                                <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NO_SOLICITUD" HeaderText="NO_SOLICITUD" SortExpression="NO_SOLICITUD">
                                <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FOLIO_SOLICITUD" HeaderText="Folio" SortExpression="FOLIO_SOLICITUD">
                                <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" Font-Bold="true" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FECHA_RECEPCION_REAL" HeaderText="Fecha Recepción" SortExpression="FECHA_RECEPCION_REAL" DataFormatString="{0:dd/MMM/yyyy}">
                                <ItemStyle Width="140px" HorizontalAlign="Center" Font-Size="X-Small"/>
                            </asp:BoundField>
                            <asp:BoundField DataField="TIPO_SERVICIO" HeaderText="Tipo Servicio" SortExpression="TIPO_SERVICIO">
                                <ItemStyle Width="140px" HorizontalAlign="Center" Font-Size="X-Small"/>
                            </asp:BoundField>
                            <asp:BoundField DataField="DESCRIPCION_SERVICIO" HeaderText="Descripción" SortExpression="DESCRIPCION_SERVICIO">
                                <ItemStyle Font-Size="X-Small"/>
                            </asp:BoundField>
                            <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" SortExpression="DEPENDENCIA">
                                <ItemStyle Font-Size="X-Small"/>
                            </asp:BoundField>
                            <asp:BoundField DataField="NO_INVENTARIO" HeaderText="No. Inventario" SortExpression="NO_INVENTARIO">
                                <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small"/>
                            </asp:BoundField>
                            <asp:BoundField DataField="NO_ECONOMICO" HeaderText="No. Economico" SortExpression="NO_ECONOMICO">
                                <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small"/>
                            </asp:BoundField>
                            <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS">
                                <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small"/>
                            </asp:BoundField>
                        </Columns>
                        <PagerStyle CssClass="GridHeader" />
                        <SelectedRowStyle CssClass="GridSelected" />
                        <HeaderStyle CssClass="GridHeader" />                                
                        <AlternatingRowStyle CssClass="GridAltItem" />       
                    </asp:GridView>
                </div>                
                <div id="Div_Campos" runat="server" style="width:100%;"> 
                    <br />              
                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                            <td colspan="4">
                                <asp:HiddenField ID="Hdf_No_Solicitud" runat="server" />
                                <asp:HiddenField ID="Hdf_No_Servicio" runat="server" />
                                <asp:HiddenField ID="Hdf_No_Asignacion" runat="server" />
                                <asp:HiddenField ID="Hdf_No_Registro" runat="server" />
                                <asp:HiddenField ID="Hdf_Estatus_Asignacion_Proveedor" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%;">
                                <asp:Label ID="Lbl_Folio" runat="server" Text="Folio" ForeColor="Black" Font-Bold="true"></asp:Label>
                            </td>
                            <td style="width:35%;">
                                <asp:TextBox ID="Txt_Folio" runat="server" Width="98%" ForeColor="Red" Font-Bold="true" style="text-align:right;"></asp:TextBox>
                            </td>
                            <td colspan="2">
                                &nbsp;&nbsp;
                            </td>  
                        </tr>
                        <tr>
                            <td style="width:15%;">
                                <asp:Label ID="Lbl_Fecha_Elaboracion" runat="server" Text="Fecha Elaboración"></asp:Label>
                            </td>
                            <td style="width:35%;">
                                <asp:TextBox ID="Txt_Fecha_Elaboracion" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                            </td>
                            <td style="width:15%;">
                                &nbsp;&nbsp;  
                                <asp:Label ID="Lbl_Fecha_Recepcion" runat="server" Text="Fecha Recepción"></asp:Label>
                            </td>
                            <td style="width:35%;">
                                <asp:TextBox ID="Txt_Fecha_Recepcion" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%;">
                                <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text="Unidad Responsable"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="Cmb_Unidad_Responsable" runat="server" Width="100%" Enabled="false">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%;">
                                <asp:Label ID="Lbl_Kilometraje" runat="server" Text="Kilometraje" ></asp:Label>
                            </td>
                            <td style="width:16%;">
                                <asp:TextBox ID="Txt_Kilometraje" runat="server" Width="95%" Enabled="false"></asp:TextBox>   
                            </td>
                            <td style="width:15%;">
                                &nbsp;&nbsp;  
                                <asp:Label ID="Lbl_Tipo_Servicio" runat="server" Text="Tipo Servicio"></asp:Label>
                            </td>
                            <td style="width:35%;">
                                <asp:DropDownList ID="Cmb_Tipo_Servicio" runat="server" Width="100%" Enabled="false">
                                    <asp:ListItem Value="">&lt; - - SELECCIONE - - &gt;</asp:ListItem>
                                    <asp:ListItem Value="SERVICIO_CORRECTIVO">SERVICIO CORRECTIVO</asp:ListItem>
                                    <asp:ListItem Value="SERVICIO_PREVENTIVO">SERVICIO PREVENTIVO</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4"> &nbsp;&nbsp; </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Panel ID="Pnl_Vehiculo_Seleccionado" runat="server" Width="99%" GroupingText="Vehículo para el Servicio">
                                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:HiddenField ID="Hdf_Vehiculo_ID" runat="server" />
                                                <asp:Label ID="Lbl_No_Inventario" runat="server" Text="No. Inventario"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_No_Inventario" runat="server" Width="70%" MaxLength="7" Enabled="false"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Inventario" runat="server" TargetControlID="Txt_No_Inventario" FilterType="Numbers">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width:15%;">
                                                &nbsp;&nbsp;  
                                                <asp:Label ID="Lbl_No_Economico" runat="server" Text="No. Economico"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_No_Economico" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:Label ID="Lbl_Datos_Vehiculo" runat="server" Text="Vehículo"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="Txt_Datos_Vehiculo" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:Label ID="Lbl_Placas" runat="server" Text="Placas"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_Placas" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                            </td>
                                            <td style="width:15%;">
                                                &nbsp;&nbsp;  
                                                <asp:Label ID="Lbl_Anio" runat="server" Text="Año"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_Anio" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="Pnl_Bien_Mueble_Seleccionado" runat="server" 
                                        GroupingText="Bien Mueble para el Servicio" Width="99%">
                                        <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                                            <tr>
                                                <td style="width:15%;">
                                                    <asp:HiddenField ID="Hdf_Bien_Mueble_ID" runat="server" />
                                                    <asp:Label ID="Lbl_No_Inventario_BM" runat="server" Text="No. Inventario"></asp:Label>
                                                </td>
                                                <td style="width:35%;">
                                                    <asp:TextBox ID="Txt_No_Inventario_BM" runat="server" MaxLength="7" Width="98%" Enabled="false"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Inventario_BM" runat="server" 
                                                        FilterType="Numbers" TargetControlID="Txt_No_Inventario_BM">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width:15%;">
                                                    &nbsp;
                                                </td>
                                                <td style="width:35%;">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:15%;">
                                                    <asp:Label ID="Lbl_Descripcion_Bien" runat="server" Text="Descripción Bien"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Descripcion_Bien" runat="server" Enabled="false" Rows="2" 
                                                        TextMode="MultiLine" Width="99%"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:15%;">
                                                    <asp:Label ID="Lbl_Numero_Serie_Bien" runat="server" Text="No. Serie"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Numero_Serie_Bien" runat="server" Enabled="false" 
                                                        Width="99%"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                             <td colspan="4">&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Panel ID="Pnl_Descripcion_Servicio" runat="server" Width="99%" GroupingText="Descripción del Servicio">
                                    <asp:TextBox ID="Txt_Descripcion_Servicio" runat="server" Rows="5" TextMode="MultiLine" Width="99%" Enabled="false"></asp:TextBox>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                             <td colspan="4">&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Panel ID="Pnl_Diagnostico_Servicio" runat="server" Width="99%" GroupingText="Diagnostico del Servicio">
                                    <asp:TextBox ID="Txt_Diagnostico_Servicio" runat="server" Rows="5" TextMode="MultiLine" Width="99%" Enabled="false"></asp:TextBox>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                             <td colspan="4">&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:15%;">
                                <asp:HiddenField ID="Hdf_Proveedor_ID" runat="server" />
                                <asp:Label ID="Lbl_Proveedor" runat="server" Text="Proveedor" Font-Bold="true"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="Txt_Nombre_Proveedor" runat="server" Width="98%" Enabled="false" style="float:left"></asp:TextBox>
                            </td>                 
                        </tr>
                    </table>
                    <hr style="width:98%;"/>
                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                            <td colspan="4" style="text-align:center;">
                                <asp:Label ID="Lbl_Leyenda_Datos_Salida" runat="server" Text="DATOS DE LA SALIDA AL PROVEEDOR" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                             <td colspan="4">&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:15%;">
                                <asp:Label ID="Lbl_Kilometraje_Salida" runat="server" Text="Kilometraje [Km]" Font-Bold="true"></asp:Label>
                            </td>
                            <td style="width:35%;">
                                <asp:TextBox ID="Txt_Kilometraje_Salida" runat="server" Width="98%" Enabled="false" ></asp:TextBox>
                            </td>
                            <td style="width:15%;">
                                &nbsp;&nbsp;  
                                <asp:Label ID="Lbl_Fecha_Salida_Proveedor" runat="server" Text="Fecha de Salida" Font-Bold="true"></asp:Label>
                            </td>
                            <td style="width:35%;">
                                <asp:TextBox ID="Txt_Fecha_Salida_Proveedor" runat="server" Width="95%" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%; vertical-align:text-top;">
                                <asp:Label ID="Lbl_Comentarios_Salida" runat="server" Text="Comentarios" Font-Bold="true"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="Txt_Comentarios_Salida" runat="server" Width="98%" Rows="2" TextMode="MultiLine" style="float:left" Enabled="false"></asp:TextBox>
                            </td>                 
                        </tr>                                
                        <tr>
                            <td colspan="4">
                                <center>
                                   <asp:Button ID="Btn_Mostrar_Detalle_Salida" runat="server"  Text="Mostrar Detalle Salida" CssClass="button" CausesValidation="false"  Width="200px" OnClick="Btn_Mostrar_Detalle_Salida_Click" /> 
                                </center>
                            </td>                                                     
                        </tr> 
                    </table>
                    <hr style="width:98%;"/>
                    <br />       
                    <hr style="width:98%;"/>
                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                            <td colspan="4" style="text-align:center;">
                                <asp:Label ID="Lbl_Leyenda_Datos_Entrada" runat="server" Text="DATOS DE LA ENTRADA AL PROVEEDOR" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                             <td colspan="4">&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:15%;">
                                <asp:Label ID="Lbl_Kilometraje_Entrada" runat="server" Text="Kilometraje [Km]" Font-Bold="true"></asp:Label>
                            </td>
                            <td style="width:35%;">
                                <asp:TextBox ID="Txt_Kilometraje_Entrada" runat="server" Width="93%" ></asp:TextBox>
                            </td>
                            <td style="width:15%;">
                                <asp:Label ID="Lbl_Fecha_Entrada_Proveedor" runat="server" Text="Fecha de Entrada" Font-Bold="true"></asp:Label>
                            </td>
                            <td style="width:35%;">
                                <asp:TextBox ID="Txt_Fecha_Entrada_Proveedor" runat="server" Width="80%" Enabled="false"></asp:TextBox>
                                <asp:ImageButton ID="Btn_Fecha_Entrada_Proveedor" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                <cc1:CalendarExtender ID="CE_Txt_Fecha_Entrada_Proveedor" runat="server" TargetControlID="Txt_Fecha_Entrada_Proveedor" PopupButtonID="Btn_Fecha_Entrada_Proveedor" Format="dd/MMM/yyyy">
                                </cc1:CalendarExtender>  
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%; vertical-align:text-top;">
                                <asp:Label ID="Lbl_Comentarios_Entrada" runat="server" Text="Comentarios" Font-Bold="true"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="Txt_Comentarios_Entrada" runat="server" Width="98%" Rows="2" TextMode="MultiLine" style="float:left" ></asp:TextBox>
                            </td>                 
                        </tr>        
                    </table>
                    <hr style="width:98%;"/>
                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                            <td colspan="4">
                                <div runat="server" id="Div_Detalles_Revision" style="width:99%;">
                                    <asp:GridView ID="Grid_Listado_Detalles" runat="server" CssClass="GridView_1"
                                        AutoGenerateColumns="False" Width="99%"  
                                        GridLines= "Horizontal">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:BoundField DataField="PARTE_ID" HeaderText="PARTE_ID" SortExpression="PARTE_ID">
                                                <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="SUBPARTE_ID" HeaderText="SUBPARTE_ID" SortExpression="SUBPARTE_ID">
                                                <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CANT_UNIFICAR" HeaderText="CANT_UNIFICAR" SortExpression="CANT_UNIFICAR" NullDisplayText="0">
                                                <ItemStyle Width="10px" HorizontalAlign="Center" Font-Size="X-Small"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NOMBRE_PARTE" HeaderText="Parte" SortExpression="NOMBRE_PARTE">
                                                <ItemStyle Width="140px" HorizontalAlign="Center" Font-Size="X-Small"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NOMBRE_SUBPARTE" HeaderText="Sub Parte" SortExpression="NOMBRE_SUBPARTE">
                                                <ItemStyle Width="140px" HorizontalAlign="Center" Font-Size="X-Small"/>
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Si">
                                                <ItemTemplate>
                                                    <asp:RadioButton ID="RBtn_SI" runat="server" Text="" GroupName="GRBtn_Opciones" />
                                                </ItemTemplate>
                                                <ItemStyle Width="15px" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="No">
                                                <ItemTemplate>
                                                    <asp:RadioButton ID="RBtn_NO" runat="server" Text="" GroupName="GRBtn_Opciones" />
                                                </ItemTemplate>
                                                <ItemStyle Width="15px" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />                                
                                        <AlternatingRowStyle CssClass="GridAltItem" />       
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                    </table>        
                </div>
                </center>        
            </div>
            <br />
            <br />
            <br />
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:UpdatePanel ID="UpPnl_Aux_Listado_Detalles" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_MPE_Listado_Detalles" runat="server" Text="" style="display:none;"/>
                <cc1:ModalPopupExtender ID="MPE_Listado_Detalles" runat="server" 
                TargetControlID="Btn_Comodin_MPE_Listado_Detalles" PopupControlID="Pnl_Busqueda_Contenedor" 
                CancelControlID="Btn_Cerrar_Ventana" PopupDragHandleControlID="Pnl_Busqueda_Resguardante_Cabecera"
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter"/>  
        </ContentTemplate>
    </asp:UpdatePanel>    
    
    <asp:Panel ID="Pnl_Busqueda_Contenedor" runat="server" CssClass="drag"  HorizontalAlign="Center" Width="850px" 
            style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">                         
            <asp:Panel ID="Pnl_Busqueda_Resguardante_Cabecera" runat="server" 
                style="cursor: move;background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
                <table width="99%">
                    <tr>
                        <td style="color:Black;font-size:12;font-weight:bold;">
                           <asp:Image ID="Img_Informatcion_Autorizacion" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                             Detalles en la Salida del Vehiculo
                        </td>
                        <td align="right" style="width:10%;">
                           <asp:ImageButton ID="Btn_Cerrar_Ventana" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana"
                                ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png"/>  
                        </td>
                    </tr>
                </table>            
            </asp:Panel>                                                                          
           <div style="color: #5D7B9D">
             <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;" >                                    
                        <asp:UpdatePanel ID="Upnl_Listado_Detalles" runat="server">
                            <ContentTemplate>
                            
                                <asp:UpdateProgress ID="Progress_Upnl_Listado_Detalles" runat="server" AssociatedUpdatePanelID="Upnl_Listado_Detalles" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                        <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress"><img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>  
                                  <br />
                                  <div id="Div_Listado_Detalles" runat="server" style="border-style:outset; width:99%; height: 450px; overflow:auto;">
                                      <asp:GridView ID="Grid_Listado_Detalles_Salida" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                            ForeColor="#333333" GridLines="None" AllowPaging="True" Width="100%" 
                                            PageSize="100" EmptyDataText="No hay registros" >
                                            <RowStyle CssClass="GridItem" />
                                                <Columns>
                                                    <asp:BoundField DataField="PARTE_ID" HeaderText="PARTE_ID" SortExpression="PARTE_ID">
                                                        <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="SUBPARTE_ID" HeaderText="SUBPARTE_ID" SortExpression="SUBPARTE_ID">
                                                        <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="CANT_UNIFICAR" HeaderText="CANT_UNIFICAR" SortExpression="CANT_UNIFICAR" NullDisplayText="0">
                                                        <ItemStyle Width="10px" HorizontalAlign="Center" Font-Size="X-Small"/>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="NOMBRE_PARTE" HeaderText="Parte" SortExpression="NOMBRE_PARTE">
                                                        <ItemStyle Width="140px" HorizontalAlign="Center" Font-Size="X-Small"/>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="NOMBRE_SUBPARTE" HeaderText="Sub Parte" SortExpression="NOMBRE_SUBPARTE">
                                                        <ItemStyle Width="140px" HorizontalAlign="Center" Font-Size="X-Small"/>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="VALOR" HeaderText="Contenia" SortExpression="VALOR">
                                                        <ItemStyle Width="140px" HorizontalAlign="Center" Font-Size="X-Small"/>
                                                    </asp:BoundField>
                                                </Columns>
                                            <PagerStyle CssClass="GridHeader" />
                                            <SelectedRowStyle CssClass="GridSelected" />
                                            <HeaderStyle CssClass="GridHeader" />
                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView> 
                                </div>                                                                                                                                                          
                            </ContentTemplate>                                                                   
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>                                                      
                    </td>
                </tr>
             </table>    
             <asp:HiddenField ID="Hdf_Tipo_Bien" runat="server" />                                               
           </div>                 
    </asp:Panel>  
</asp:Content>

