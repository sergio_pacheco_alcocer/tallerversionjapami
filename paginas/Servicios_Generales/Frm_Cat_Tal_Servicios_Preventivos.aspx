﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Cat_Tal_Servicios_Preventivos.aspx.cs" Inherits="paginas_Taller_Municipal_Frm_Cat_Tal_Servicios_Preventivos" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">

    <script type="text/javascript">
        window.onerror = new Function("return true");
        //Abrir una ventana modal
        function Abrir_Ventana_Modal(Url, Propiedades) {
            window.showModalDialog(Url, null, Propiedades);
        }
        function Validar_Longitud_Texto(Text_Box, Max_Longitud) {
            if (Text_Box.value.length > Max_Longitud) {
                Text_Box.value = Text_Box.value.substring(0, Max_Longitud);
            }
        }
    </script>

    <script type="text/javascript" language="javascript">
    //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_sesiones.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion()
        {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para matener la sesión activa
        setInterval('MantenSesion()', <%=(int)(0.9*(Session.Timeout * 60000))%>);

        
    </script>
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" 
    EnableScriptGlobalization="true"
        EnableScriptLocalization="true"
        EnablePartialRendering="true" 
        AsyncPostBackTimeout="9000">
</cc1:ToolkitScriptManager>
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" EnablePartialRendering="true" AsyncPostBackTimeout="9000">
    </asp:ScriptManager>--%>
    <%--Inicio formulario--%>
    <asp:UpdatePanel ID="Upd_Servicios_Preventivos" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--update progrees--%>
            <%--<asp:UpdateProgress ID="Uprg_Progress" runat="server" AssociatedUpdatePanelID="Upd_Servicios_Preventivos" DisplayAfter="0">
            <ProgressTemplate>
            <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
            <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
            </ProgressTemplate>
            </asp:UpdateProgress>--%>
            
            <div id="Div_Servicios" style="background-color:#ffffff; width:100%; height:100%">
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                    <td colspan ="4" class="label_titulo">Catálogo de Tipos de Servicios</td>
                    </tr>
                    <tr>
                    <td colspan="4">
                     <asp:Image ID="Img_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" />
                     <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" CssClass="estilo_fuente_mensaje_error" Text="" /><br />
                     <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                    </td>                        
                    </tr>
                    <tr class="barra_busqueda">
                    <td colspan="2" align="left" style="width:20%">
                    <asp:ImageButton ID="Btn_Nuevo" runat="server" 
                            ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" CssClass="Img_Button" 
                            onclick="Btn_Nuevo_Click"/>
                     <asp:ImageButton ID="Btn_Modificar" runat="server" 
                            ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" CssClass="Img_Button" 
                            onclick="Btn_Modificar_Click"/>
                     <asp:ImageButton ID="Btn_Eliminar" runat="server" 
                            ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png" CssClass="Img_Button" 
                            onclick="Btn_Eliminar_Click"/>
                     <asp:ImageButton ID="Btn_Salir" runat="server" 
                            ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" CssClass="Img_Button" 
                            onclick="Btn_Salir_Click"/>
                     </td>
                     <td colspan="2" align="right" valign="top" style="width:80%">
                        <table style="width: 80%;>
                            <tr>
                                <td style="vertical-align: top; text-align: right; width: 5%">                                    
                                </td>
                                <tr>
                                    <td style="vertical-align: top; text-align: right; width: 90%">
                                        Búsqueda:
                                        <asp:TextBox ID="Txt_Buscar" runat="server" MaxLength="100" TabIndex="5" 
                                            ToolTip="Buscar" Width="280px" />
                                        <cc1:TextBoxWatermarkExtender ID="WTE_Txt_Buscar" runat="server" 
                                            TargetControlID="Txt_Buscar" WatermarkCssClass="watermarked" 
                                            WatermarkText="&lt;- Clave ó Nombre -&gt;" />
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Buscar" runat="server" 
                                            FilterType="UppercaseLetters,LowercaseLetters,Numbers" 
                                            TargetControlID="Txt_Buscar" />
                                    </td>
                                    <td style="vertical-align: top; text-align: right; width: 5%">
                                        <asp:ImageButton ID="Btn_Buscar" runat="server" 
                                            ImageUrl="~/paginas/imagenes/paginas/busqueda.png" OnClick="Btn_Buscar_Click" 
                                            TabIndex="6" ToolTip="Buscar" />
                                    </td>
                        </tr>
                            </tr>
                        </table>
                     </td>
                    </tr>    
                    </table>

                    &nbsp;
                    <br />
                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                    <td style="width:18%">
                                Clave</td>
                            <td style="width:32%">
                                <asp:TextBox ID="Txt_Clave" runat="server" Width="92%" Text="" MaxLength="20" Style="text-transform: uppercase" Enabled="false"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="Txt_Clave_FilteredTextBoxExtender" 
                                    runat="server" Enabled="True" TargetControlID="Txt_Clave" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="width:18%">
                                Estatus</td>
                            <td style="width:32%">
                                <asp:DropDownList ID="Cmb_Estatus" Width="94%" runat="server">
                                    <asp:ListItem Text="<SELECCIONE>" Value="SELECCIONE" />
                                    <asp:ListItem Text="VIGENTE" Value="VIGENTE" />
                                    <asp:ListItem Text="BAJA" Value="BAJA" />
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:18%">
                                Nombre</td>
                            <td style="width:32%">
                                <asp:TextBox ID="Txt_Nombre" runat="server" Width="92%" Text="" MaxLength="50" Style="text-transform: uppercase"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="Txt_Nombre_FilteredTextBoxExtender" 
                                    runat="server" Enabled="True" TargetControlID="Txt_Nombre" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="width:18%">
                                Búsqueda Avanzada de Refacciones</td>
                            <td style="width:18%">
                                <asp:ImageButton ID="Btn_Busqueda_Refacciones" runat="server" Height="22px" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png"
                                    OnClick="Btn_Busqueda_Refacciones_Click" TabIndex="10" ToolTip="Búsqueda Avanzada"
                                    Width="22px" /></td>
                        </tr>
                        <tr>
                            <td style="width:18%">
                                Descripcion</td>
                            <td colspan="3">
                                <asp:TextBox ID="Txt_Descripcion" runat="server" Width="97%" Height="60px" 
                                    TextMode="MultiLine" Style="text-transform: uppercase"></asp:TextBox>
                                    <cc1:TextBoxWatermarkExtender ID="Twe_Txt_Comentarios" runat="server" WatermarkCssClass="watermarked"
                                     WatermarkText="Límite de Caractes 250" TargetControlID="Txt_Descripcion">
                                </cc1:TextBoxWatermarkExtender>
                                <cc1:FilteredTextBoxExtender ID="Txt_Comentarios_FilteredTextBoxExtender" 
                                runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                TargetControlID="Txt_Descripcion" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                            </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <%--<tr>
                        <td colspan="4" style="width:18%">
                                Búsqueda Avanzada de Refacciones <asp:ImageButton ID="Btn_Busqueda_Refacciones" runat="server" Height="22px" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png"
                                    OnClick="Btn_Busqueda_Refacciones_Click" TabIndex="10" ToolTip="Búsqueda Avanzada"
                                    Width="22px" /></td>
                        </tr>--%>
                        <tr>
                            <td style="width:18%">&nbsp;</td>
                            <td style="width:32%">&nbsp;</td>
                            <td style="width:18%">&nbsp;</td>
                            <td style="width:32%">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:18%">&nbsp;</td>
                            <td style="width:32%">&nbsp;</td>
                            <td style="width:18%">&nbsp;</td>
                            <td style="width:32%">&nbsp;</td>
                        </tr>
                        </table>
                         <%--inicio de TabContainer--%>
                        <cc1:TabContainer ID="Tab_Contenedor_Servicios" runat="server" Width="98%" 
                            ActiveTabIndex="1">                        
                        <%--tab para grid de Servicios Preventivos--%>
                        <cc1:TabPanel ID="Tbp_Constancia_Adeudo" runat="server" HeaderText="Tbp_Constancia_Adeudo" Width="100%">
                        <HeaderTemplate>Servicios Preventivos</HeaderTemplate><ContentTemplate><center>
                        <br />
                        <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                            <td align="center" colspan="4">
                                <asp:GridView ID="Grid_Servicios" runat="server" AllowPaging="True" 
                                    AutoGenerateColumns="False" CssClass="GridView_1" 
                                    GridLines="None" 
                                    DataKeyNames="SERVICIO_PREVENTIVO_ID" Style="white-space:normal" Width="96%" 
                                    onpageindexchanging="Grid_Servicios_PageIndexChanging" 
                                    onselectedindexchanged="Grid_Servicios_SelectedIndexChanged" 
                                    EnableModelValidation="True">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                            ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                            <ItemStyle Width="5%" />
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="SERVICIO_PREVENTIVO_ID" HeaderText="Id" 
                                            Visible="False">
                                            <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                            <ItemStyle HorizontalAlign="Left" Width="5%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CLAVE" HeaderText="Clave">
                                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                            <ItemStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NOMBRE" HeaderText="Nombre">
                                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                            <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ESTATUS" HeaderText="Estatus">
                                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                            <ItemStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripción">
                                            <HeaderStyle HorizontalAlign="Left" Width="50%" />
                                            <ItemStyle HorizontalAlign="Left" Width="50%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </td>
                        </tr>                        
                        <tr>
                            <td style="width:18%">&nbsp;</td>
                            <td style="width:32%">&nbsp;</td>
                            <td style="width:18%">&nbsp;</td>
                            <td style="width:32%">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:18%">&nbsp;</td>
                            <td style="width:32%">&nbsp;</td>
                            <td style="width:18%">&nbsp;</td>
                            <td style="width:32%">&nbsp;</td>
                        </tr>
                    </table>     
                    </center></ContentTemplate></cc1:TabPanel>  
                    <%--tab para Busqueda de refacciones--%>
                        <cc1:TabPanel ID="Tbp_Recargos" runat="server" HeaderText="Tbp_Recargos" Width="100%">
                        <HeaderTemplate>Refacciones</HeaderTemplate><ContentTemplate><center>   
                        <br />
                        <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                        <tr>                            
                            <td align="center" colspan="4">
                                <asp:GridView ID="Grid_Refacciones" runat="server" AllowPaging="True" 
                                    AutoGenerateColumns="False" CssClass="GridView_1" 
                                    GridLines="None" 
                                    DataKeyNames="REFACCION_ID" Style="white-space:normal" Width="95%" 
                                    onpageindexchanging="Grid_Refacciones_PageIndexChanging" 
                                    onselectedindexchanged="Grid_Refaccion_SelectedIndexChanged" 
                                    EnableModelValidation="True">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:BoundField DataField="REFACCION_ID" HeaderText="Id" Visible="False">
                                            <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                            <ItemStyle HorizontalAlign="Left" Width="5%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CANTIDAD" HeaderText="Cantidad">
                                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                            <ItemStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CLAVE" HeaderText="Clave">
                                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                            <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small" />
                                        </asp:BoundField>  
                                        <asp:BoundField DataField="NOMBRE" HeaderText="Nombre">
                                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                            <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small" />
                                        </asp:BoundField>                                        
                                        <asp:TemplateField>
                                            <ItemTemplate>                                            
                                                <asp:ImageButton id="Btn_Grid_Eliminar" runat="server" ToolTip="Quitar Refacción" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Right" Width="5%" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:18%">&nbsp;</td>
                            <td style="width:32%">&nbsp;</td>
                            <td style="width:18%">&nbsp;</td>
                            <td style="width:32%">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:18%">&nbsp;</td>
                            <td style="width:32%">&nbsp;</td>
                            <td style="width:18%">&nbsp;</td>
                            <td style="width:32%">&nbsp;</td>
                        </tr>
                        </table>
                        </center></ContentTemplate></cc1:TabPanel>
                    </cc1:TabContainer>             
                </div>
            
            </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
