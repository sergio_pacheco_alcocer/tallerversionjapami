﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Taller_Mecanico.Reporte_Informe_Tramite_Pagos.Negocio;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using CarlosAg.ExcelXmlWriter;
using System.Text;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;

public partial class paginas_Taller_Mecanico_Frm_Rpt_Tal_Informe_Tramite_Pagos : System.Web.UI.Page {

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN: Carga la Pagina Inicial
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Julio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Page_Load(object sender, EventArgs e) {
            Div_Contenedor_Msj_Error.Visible = false;
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack) {
                Txt_Fecha_Recepcion_Inicial.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Today);
                Txt_Fecha_Recepcion_Final.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Today);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Reporte_PDF_Click
        ///DESCRIPCIÓN: Saca Reporte en PDF
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Julio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Btn_Reporte_PDF_Click(object sender, ImageClickEventArgs e) {
            try {
                Generacion_Reporte("PDF");
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                Lbl_Mensaje_Error.Text = "[" + Ex.Message + "]";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Reporte_EXCEL_Click
        ///DESCRIPCIÓN: Saca Reporte en Excel
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 30/Julio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Btn_Reporte_EXCEL_Click(object sender, ImageClickEventArgs e) {
            try {
                Generacion_Reporte("EXCEL");
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                Lbl_Mensaje_Error.Text = "[" + Ex.Message + "]";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        /// *************************************************************************************
        /// NOMBRE:              Generacion_Reporte
        /// DESCRIPCIÓN:         Se ejecuta la Generacion del Reporte.
        /// PARÁMETROS:          Tipo.- Tipo de Formato al Reporte
        /// USUARIO CREO:        Francisco Antonio Gallardo Castañeda.
        /// FECHA CREO:          31/Julio/2012
        /// USUARIO MODIFICO:     
        /// FECHA MODIFICO:      
        /// CAUSA MODIFICACIÓN:   
        /// *************************************************************************************
        private void Generacion_Reporte(String Tipo) {
            Cls_Rpt_Tal_Informe_Tramite_Pagos_Negocio Rpt_Negocio = new Cls_Rpt_Tal_Informe_Tramite_Pagos_Negocio();
            if (Txt_Fecha_Recepcion_Inicial.Text.Trim().Length > 0) Rpt_Negocio.P_Fecha_Inicial = Convert.ToDateTime(Txt_Fecha_Recepcion_Inicial.Text);
            if (Txt_Fecha_Recepcion_Final.Text.Trim().Length > 0) Rpt_Negocio.P_Fecha_Final = Convert.ToDateTime(Txt_Fecha_Recepcion_Final.Text);
            DataTable Dt_Datos = Rpt_Negocio.Consultar_Informe_Tramite_Pagos();
            Dt_Datos.TableName = "Dt_Datos";
            DataTable Dt_Firmas = Obtener_Tabla_Firmas();
            DataSet Ds_Consulta = new DataSet();
            Ds_Consulta.Tables.Add(Dt_Datos.Copy());
            Ds_Consulta.Tables.Add(Dt_Firmas.Copy());

            Ds_Consulta.Tables.Add(new Cls_Tal_Parametros_Negocio().Obtener_Tabla_Reporte(this.Server).Copy());
            Ds_Rpt_Tal_Intorme_Tramite_Pagos Ds_Reporte = new Ds_Rpt_Tal_Intorme_Tramite_Pagos();
            Generar_Reporte(Ds_Consulta, Ds_Reporte, "Rpt_Tal_Intorme_Tramite_Pagos.rpt", Tipo);
        }

        /// *************************************************************************************
        /// NOMBRE:              Obtener_Tabla_Firmas
        /// DESCRIPCIÓN:         Obtiene los Datos de las Firmas del Reporte
        /// PARÁMETROS:          Dt_Datos.- Datos a Sumar
        /// USUARIO CREO:        Francisco Antonio Gallardo Castañeda.
        /// FECHA CREO:          31/Julio/2012
        /// USUARIO MODIFICO:    
        /// FECHA MODIFICO:      
        /// CAUSA MODIFICACIÓN:   
        /// *************************************************************************************
        private DataTable Obtener_Tabla_Firmas() {
            Cls_Tal_Parametros_Negocio Parametros_Neg = new Cls_Tal_Parametros_Negocio();
            Parametros_Neg = Parametros_Neg.Consulta_Parametros();
            DataTable Dt_Firmas = new DataTable("Dt_Firmas");
            Dt_Firmas.Columns.Add("DIRECTOR_DEPENDENCIA", Type.GetType("System.String"));
            Dt_Firmas.Columns.Add("OFICIAL_MAYOR", Type.GetType("System.String"));
            DataRow Fila = Dt_Firmas.NewRow();
            Fila["DIRECTOR_DEPENDENCIA"] = Parametros_Neg.P_Director;
            Fila["OFICIAL_MAYOR"] = Parametros_Neg.P_Oficial_Mayor;
            Dt_Firmas.Rows.Add(Fila);
            return Dt_Firmas;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
        ///DESCRIPCIÓN: caraga el data set fisico con el cual se genera el Reporte especificado
        ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
        ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
        ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
        ///CREO: Susana Trigueros Armenta.
        ///FECHA_CREO: 01/Mayo/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        private void Generar_Reporte(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, String Nombre_Reporte, String Formato) {
            ReportDocument Reporte = new ReportDocument();
            String File_Path = Server.MapPath("../Rpt/Taller_Mecanico/" + Nombre_Reporte);
            Reporte.Load(File_Path);
            String Nombre_Reporte_Generar = "Rpt_Tal_Inf_" + Cls_Sessiones.No_Empleado + "_" + Convert.ToString(DateTime.Now.ToString("yyyy'-'MM'-'dd'_t'HH'-'mm'-'ss")) + ((Formato.Equals("PDF")) ? ".pdf" : ".xls");
            String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
            Ds_Reporte = Data_Set_Consulta_DB;
            Reporte.SetDataSource(Ds_Reporte);
            Reporte.SetParameterValue("NUMERO_REGISTROS", Data_Set_Consulta_DB.Tables["Dt_Datos"].Rows.Count);
            Reporte.SetParameterValue("SUMA_IMPORTE_TOTAL", Calcular_Monto_Total(Data_Set_Consulta_DB.Tables["Dt_Datos"]));
            ExportOptions Export_Options = new ExportOptions();
            DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
            Disk_File_Destination_Options.DiskFileName = Server.MapPath(Ruta);
            Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
            Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
            Export_Options.ExportFormatType = ((Formato.Equals("PDF")) ? ExportFormatType.PortableDocFormat : ExportFormatType.Excel);
            Reporte.Export(Export_Options);
            Mostrar_Reporte(Nombre_Reporte_Generar, Formato);
        }

        /// *************************************************************************************
        /// NOMBRE:              Mostrar_Reporte
        /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
        /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
        ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
        /// USUARIO CREO:        Juan Alberto Hernández Negrete.
        /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
        /// USUARIO MODIFICO:    Salvador Hernández Ramírez
        /// FECHA MODIFICO:      23-Mayo-2011
        /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
        /// *************************************************************************************
        protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato) {
            String Pagina = "../../Reporte/";
            try {
                if (Formato == "PDF") {
                    Pagina = Pagina + Nombre_Reporte_Generar;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt", "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
                } else if (Formato == "EXCEL") {
                    String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
                }
            } catch (Exception Ex) {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
        }

        /// *************************************************************************************
        /// NOMBRE:              Calcular_Monto_Total
        /// DESCRIPCIÓN:         Calcula el Monto Total
        /// PARÁMETROS:          Dt_Datos.- Datos a Sumar
        /// USUARIO CREO:        Francisco Antonio Gallardo Castañeda.
        /// FECHA CREO:          31/Julio/2012
        /// USUARIO MODIFICO:    
        /// FECHA MODIFICO:      
        /// CAUSA MODIFICACIÓN:   
        /// *************************************************************************************
        private Double Calcular_Monto_Total(DataTable Dt_Datos) {
            Double Importe_Total = 0;
            foreach (DataRow Dr_Actual in Dt_Datos.Rows) {
                Importe_Total = Importe_Total + Convert.ToDouble(Dr_Actual["IMPORTE_TOTAL"]);
            }
            return Importe_Total;
        }

}