﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using System.Text.RegularExpressions;
using JAPAMI.Taller_Mecanico.Catalogo_Tarjetas_Gasolina.Negocio;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using JAPAMI.Empleados.Negocios;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;

public partial class paginas_Taller_Mecanico_Frm_Ope_Tal_Mov_Tarj_Gas : System.Web.UI.Page
{
    #region Page_Load

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Page_Load
    ///DESCRIPCIÓN         : Metodo que se carga cada que ocurre un PostBack de la Página
    ///PROPIEDADES         :
    ///CREO                : Salvador Vázquez Camacho.
    ///FECHA_CREO          : 18/Junio/2012
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  : 
    ///******************************************************************************* 
    protected void Page_Load(object sender, EventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        if (!IsPostBack)
        {
            Configuracion_Formulario(true);
            Llenar_Combo_Unidades_Responsables();
        }
    }

    #endregion

    #region Metodos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
    ///DESCRIPCIÓN: Carga una configuracion de los controles del Formulario
    ///PROPIEDADES: 1. Estatus. Estatus en el que se cargara la configuración de los
    ///              controles.
    ///CREO: Salvador Vázquez Camacho.
    ///FECHA_CREO: 18/Junio/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Configuracion_Formulario(Boolean Estatus)
    {
        Btn_Modificar.Visible = true;
        Btn_Modificar.AlternateText = "Modificar";
        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
        Btn_Salir.Enabled = true;

        Txt_Busqueda_Saldo.Enabled = !Estatus;
        Txt_Busqueda_Comentarios.Enabled = !Estatus;

        Txt_Nuemero_Tarjeta.Enabled = false;
        Txt_Asignada_A.Enabled = false;
        Txt_No_Inventario.Enabled = false;
        Txt_No_Economico.Enabled = false;
        Txt_Unidad_Responsable.Enabled = false;
        Txt_Datos_Vehiculo.Enabled = false;
        Txt_Placas.Enabled = false;
        Txt_Anio.Enabled = false;
        Txt_Saldo_inicial.Enabled = false;
        Cmb_Estatus.Enabled = false;
        Grid_Listado.SelectedIndex = (-1);

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Busqueda_Tarjetas_Resguardo
    ///DESCRIPCIÓN: Llena el Grid con los empleados que cumplan el filtro
    ///PROPIEDADES:     
    ///CREO:                 
    ///CREO: Salvador Vázquez Camacho.
    ///FECHA_CREO: 14/Junio/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Grid_Busqueda_Tarjetas_Movimientos()
    {
        Grid_Busqueda_Tarjetas_Movimientos.SelectedIndex = (-1);
        Grid_Busqueda_Tarjetas_Movimientos.Columns[1].Visible = true;

        Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Negocio = new Cls_Cat_Tal_Tarjetas_Gasolina_Negocio();
        if (Txt_Busqueda_No_Economico.Text.Trim().Length > 0) { Negocio.P_Numero_Economico = Txt_Busqueda_No_Economico.Text.Trim(); }
        if (Txt_Busqueda_No_Inventario.Text.Trim().Length > 0) { Negocio.P_Numero_Inventario = Txt_Busqueda_No_Inventario.Text.Trim(); }
        if (Cmb_Busqueda_Dependencia.SelectedIndex > 0) { Negocio.P_Unidad_Responsble = Cmb_Busqueda_Dependencia.SelectedItem.Value; }
        Grid_Busqueda_Tarjetas_Movimientos.DataSource = Negocio.Consultar_Tarjetas_Gasolina();
        Grid_Busqueda_Tarjetas_Movimientos.DataBind();
        Grid_Busqueda_Tarjetas_Movimientos.Columns[1].Visible = false;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Unidades_Responsables
    ///DESCRIPCIÓN: Se llena el Combo de las Unidades Responsables.
    ///PROPIEDADES:     
    ///CREO: Salvador Vázquez Camacho.
    ///FECHA_CREO: 18/Junio/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Llenar_Combo_Unidades_Responsables()
    {
        Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
        Negocio.P_Estatus = "ACTIVO";
        DataTable Dt_Dependencias = Negocio.Consultar_Unidades_Responsables();
        Cmb_Busqueda_Dependencia.DataSource = Dt_Dependencias;
        Cmb_Busqueda_Dependencia.DataTextField = "CLAVE_NOMBRE";
        Cmb_Busqueda_Dependencia.DataValueField = "DEPENDENCIA_ID";
        Cmb_Busqueda_Dependencia.DataBind();
        Cmb_Busqueda_Dependencia.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Vehiculo
    ///DESCRIPCIÓN: Se cargan los Datos del Vehiculo Seleccionado.
    ///PROPIEDADES:     
    ///CREO: Salvador Vázquez Camacho.
    ///FECHA_CREO: 14/Junio/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Cargar_Datos_Vehiculo(String Vehiculo, String Tipo_Busqueda, String Tipo_Bien)
    {
        DataTable Dt_Vehiculo = new DataTable();
        Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
        switch (Tipo_Busqueda)
        {
            case "NO_INVENTARIO":
                Consulta_Negocio.P_No_Inventario = Vehiculo;
                break;
            case "IDENTIFICADOR":
                Consulta_Negocio.P_Vehiculo_ID = Vehiculo;
                Consulta_Negocio.P_Bien_Mueble_ID = Vehiculo;
                break;
            case "NO_ECONOMICO":
                Consulta_Negocio.P_No_Economico = Vehiculo;
                break;
            default: break;
        }
        if (Tipo_Bien == "VEHICULO")
            Dt_Vehiculo = Consulta_Negocio.Consultar_Vehiculos();
        else if (Tipo_Bien == "BIEN_MUEBLE")
            Dt_Vehiculo = Consulta_Negocio.Consultar_Bien_Mueble();
        if (Dt_Vehiculo.Rows.Count > 0)
        {
            if (Tipo_Bien == "VEHICULO")
            {
                Hdf_Vehiculo_ID.Value = Dt_Vehiculo.Rows[0]["VEHICULO_ID"].ToString().Trim();
                Txt_No_Inventario.Text = Dt_Vehiculo.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                Txt_No_Economico.Text = Dt_Vehiculo.Rows[0]["NO_ECONOMICO"].ToString().Trim();
                Txt_Datos_Vehiculo.Text = Dt_Vehiculo.Rows[0]["VEHICULO_DESCRIPCION"].ToString().Trim();
                Txt_Placas.Text = Dt_Vehiculo.Rows[0]["PLACAS"].ToString().Trim();
                Txt_Anio.Text = Dt_Vehiculo.Rows[0]["ANIO"].ToString().Trim();
            }
            else if (Tipo_Bien == "BIEN_MUEBLE")
            {
                Hdf_Vehiculo_ID.Value = Dt_Vehiculo.Rows[0]["BIEN_MUEBLE_ID"].ToString().Trim();
                Txt_No_Inventario.Text = Dt_Vehiculo.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                Txt_No_Economico.Text = "";
                Txt_Datos_Vehiculo.Text = Dt_Vehiculo.Rows[0]["DESCRIPCION_BIEN_MUEBLE"].ToString().Trim();
                Txt_Placas.Text = Dt_Vehiculo.Rows[0]["NUMERO_SERIE"].ToString().Trim();
                Txt_Anio.Text = "";
            }
            if (!String.IsNullOrEmpty(Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString()))
            {
                Cls_Cat_Dependencias_Negocio Dep_Negocio = new Cls_Cat_Dependencias_Negocio();
                Dep_Negocio.P_Dependencia_ID = Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString().Trim();
                Hdf_Dependencia_ID.Value = Dep_Negocio.P_Dependencia_ID;
                DataTable Dt_Dep = Dep_Negocio.Consulta_Dependencias();
                if (Dt_Dep.Rows.Count > 0)
                {
                    Txt_Unidad_Responsable.Text = Dt_Dep.Rows[0]["CLAVE_NOMBRE"].ToString();
                    //Llenar_Combo_Proyectos_Dependecia();
                    //if (Cmb_Programa_Proyecto.Items.Count > 2)
                    //{
                    //    Cmb_Programa_Proyecto.Enabled = true;
                    //    Cmb_Programa_Proyecto.SelectedIndex = 0;
                    //}
                    //else
                    //{
                    //    Cmb_Programa_Proyecto.Enabled = false;
                    //    Cmb_Programa_Proyecto.SelectedIndex = 1;
                    //}
                }

            }
            Cargar_Datos_Resguardante_Vehiculo(Vehiculo, "IDENTIFICADOR", Tipo_Bien);
        }
        else
        {
            Lbl_Ecabezado_Mensaje.Text = "El Vehículo no se encontro.";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Resguardante_Vehiculo
    ///DESCRIPCIÓN: Cargar_Datos_Resguardante_Vehiculo
    ///PROPIEDADES:     
    ///CREO: Salvador Vázquez Camacho.
    ///FECHA_CREO: 18/Junio/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Cargar_Datos_Resguardante_Vehiculo(string Vehiculo, string Tipo_Busqueda, string Tipo_Bien)
    {
        Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
        DataTable Dt_Vehiculo = new DataTable();
        switch (Tipo_Busqueda)
        {
            case "NO_INVENTARIO":
                Consulta_Negocio.P_No_Inventario = Vehiculo;
                break;
            case "IDENTIFICADOR":
                Consulta_Negocio.P_Vehiculo_ID = Vehiculo;
                break;
            default: break;
        }
        Consulta_Negocio.P_Tipo = Tipo_Bien;
        Consulta_Negocio.P_Estatus = "VIGENTE";
        if (Tipo_Bien == "BIEN_MUEBLE")
            Dt_Vehiculo = Consulta_Negocio.Consultar_Resguardantes_Bienes();
        if (Tipo_Bien == "VEHICULO")
            Dt_Vehiculo = Consulta_Negocio.Consultar_Resguardantes_Vehiculos();
        if (Dt_Vehiculo.Rows.Count > 0)
        {
            Pnl_Resguardantes.Visible = true;
            Grid_Resguardante.DataSource = Dt_Vehiculo;
            Grid_Resguardante.DataBind();
        }
        else
        {
            Grid_Resguardante.DataSource = null;
            Grid_Resguardante.DataBind();
            Lbl_Ecabezado_Mensaje.Text = "No existen resguardantes.";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Catalogo
    ///DESCRIPCIÓN: Limpia los controles del Formulario
    ///PROPIEDADES:     
    ///CREO: Salvador Vázquez Camacho.
    ///FECHA_CREO: 18/Junio/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Limpiar_Catalogo()
    {
        Hdf_Empleado_ID.Value = "";
        Hdf_Tarjeta_ID.Value = "";
        Hdf_Vehiculo_ID.Value = "";
        Hdf_Dependencia_ID.Value = "";
        Hdf_Partida_ID.Value = "";
        Hdf_Programa_ID.Value = "";
        Txt_Anio.Text = "";
        Txt_Asignada_A.Text = "";
        Txt_No_Economico.Text = "";
        Txt_No_Inventario.Text = "";
        Txt_Nuemero_Tarjeta.Text = "";
        Txt_Placas.Text = "";
        Txt_Saldo_inicial.Text = "";
        Txt_Unidad_Responsable.Text = "";
        Txt_Datos_Vehiculo.Text = "";
        Txt_Busqueda_Saldo.Text = "";
        Txt_Busqueda_Comentarios.Text = "";
        Cmb_Estatus.SelectedIndex = 0;
        Txt_Saldo_Cargado.Text = "";
        Txt_Saldo_Consumido.Text = "";
        Txt_Saldo_Actual.Text = "";
        Txt_Partida.Text = "";
        Txt_Disponible_Partida.Text = "";
        Grid_Busqueda_Tarjetas_Movimientos.DataSource = new DataTable();
        Grid_Busqueda_Tarjetas_Movimientos.DataBind();
        Grid_Listado.SelectedIndex = -1;
        Grid_Listado.DataSource = new DataTable();
        Grid_Listado.DataBind();
    }

    #region Validaciones

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Componentes
    ///DESCRIPCIÓN         : Hace una validacion de que haya datos en los componentes antes de hacer
    ///                      una operación.
    ///PROPIEDADES         :
    ///CREO                : Salvador Vázquez Camacho.
    ///FECHA_CREO          : 18/Junio/2012
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///*******************************************************************************
    private Boolean Validar_Componentes()
    {
        Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
        String Mensaje_Error = "";
        Boolean Validacion = true;
        if (Hdf_Tarjeta_ID.Value.Trim().Length == 0)
        {
            Mensaje_Error = Mensaje_Error + "+ Seleccionar la Tarjeta.";
            Validacion = false;
        }
        if (String.IsNullOrEmpty(Txt_Disponible_Partida.Text))
        {
            Mensaje_Error = Mensaje_Error + " <br />";
            Mensaje_Error = Mensaje_Error + "+ Seleccionar una partida con fondo disponible.";
            Validacion = false;
        }
        if (Txt_Busqueda_Saldo.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
            Mensaje_Error = Mensaje_Error + "+ Introducir el Saldo del Movimiento [$].";
            Validacion = false;
        }
        else
        {
            if (!Validar_Valores_Decimales(Txt_Busqueda_Saldo.Text.Trim()))
            {
                Mensaje_Error = Mensaje_Error + " <br />";
                Mensaje_Error = Mensaje_Error + "+ El Formato del Saldo del Movimiento no es Correcto [Correcto: '12345', '12353.0' ó '12254.33'].";
                Validacion = false;
            }
            else
            {
                //if ((!String.IsNullOrEmpty(Txt_Disponible_Partida.Text.Trim())) && (!String.IsNullOrEmpty(Txt_Disponible_Partida.Text.Trim())))
                //{
                //    if (Convert.ToDouble(Txt_Disponible_Partida.Text.Replace("$", "")) < Convert.ToDouble(Txt_Busqueda_Saldo.Text.Trim()))
                //    {
                //        Mensaje_Error = Mensaje_Error + " <br />";
                //        Mensaje_Error = Mensaje_Error + "+ La cantidad a cargar es mayor que la disponible.";
                //        Validacion = false;
                //    }
                //}
                //else
                //{
                //    Mensaje_Error = Mensaje_Error + " <br />";
                //    Mensaje_Error = Mensaje_Error + "+ La cantidad a cargar es mayor que la disponible.";
                //    Validacion = false;
                //}
            }
        }
        if (!Validacion)
        {
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
            Div_Contenedor_Msj_Error.Visible = true;
        }
        return Validacion;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Valores_Decimales
    ///DESCRIPCIÓN         : Valida los valores decimales para un Anexo.
    ///PROPIEDADES         :
    ///CREO                : Salvador Vázquez Camacho.
    ///FECHA_CREO          : 18/Junio/2012
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///*******************************************************************************
    private Boolean Validar_Valores_Decimales(String Valor)
    {
        Boolean Validacion = true;
        Regex Expresion_Regular = new Regex(@"^[0-9]{1,50}(\.[0-9]{0,2})?$");
        Validacion = Expresion_Regular.IsMatch(Valor);
        return Validacion;
    }

    #endregion

    #endregion

    #region Grids

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Mostrar_Datos
    ///DESCRIPCIÓN         : Mostrar Datos.
    ///PROPIEDADES         :
    ///CREO                : Salvador Vázquez Camacho.
    ///FECHA_CREO          : 18/Junio/2012
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///*******************************************************************************
    private void Mostrar_Datos(String Tarjeta_Seleccionada_ID)
    {
        double Disponible_Partida = 0;
        String Tipo_Bien = "";
        Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Tarjeta_Negocio = new Cls_Cat_Tal_Tarjetas_Gasolina_Negocio();
        Tarjeta_Negocio.P_Tarjeta_ID = Tarjeta_Seleccionada_ID;
        Hdf_Tarjeta_ID.Value = Tarjeta_Negocio.P_Tarjeta_ID;
        Tarjeta_Negocio = Tarjeta_Negocio.Consultar_Detalles_Tarjetas_Gasolina();
        Txt_Nuemero_Tarjeta.Text = Tarjeta_Negocio.P_Numero_Tarjeta;
        Txt_Saldo_inicial.Text = String.Format("{0:c}", ((Tarjeta_Negocio.P_Saldo_Inicial == (-1)) ? 0 : Tarjeta_Negocio.P_Saldo_Inicial));
        Txt_Saldo_Cargado.Text = String.Format("{0:c}", ((Tarjeta_Negocio.P_Saldo_Cargado == (-1)) ? 0 : Tarjeta_Negocio.P_Saldo_Cargado));
        Txt_Saldo_Consumido.Text = String.Format("{0:c}", ((Tarjeta_Negocio.P_Saldo_Consumido == (-1)) ? 0 : Tarjeta_Negocio.P_Saldo_Consumido));
        Txt_Saldo_Actual.Text = String.Format("{0:c}", ((Tarjeta_Negocio.P_Saldo_Actual == (-1)) ? 0 : Tarjeta_Negocio.P_Saldo_Actual));
        Hdf_Programa_ID.Value = Tarjeta_Negocio.P_Programa_Proyecto_ID;
        Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(Tarjeta_Negocio.P_Estatus));
        Grid_Listado.DataSource = Tarjeta_Negocio.P_Dt_Historial;
        Grid_Listado.DataBind();
        if (!String.IsNullOrEmpty(Tarjeta_Negocio.P_Vehiculo_ID))
        {
            Txt_No_Inventario.Text = Tarjeta_Negocio.P_Vehiculo_ID;
            Tipo_Bien = "VEHICULO";
        }
        else if (!String.IsNullOrEmpty(Tarjeta_Negocio.P_Bien_ID))
        {
            Txt_No_Inventario.Text = Tarjeta_Negocio.P_Bien_ID;
            Tipo_Bien = "BIEN_MUEBLE";
        }
        Cargar_Datos_Vehiculo(Txt_No_Inventario.Text, "IDENTIFICADOR", Tipo_Bien);
        Hdf_Partida_ID.Value = Tarjeta_Negocio.P_Partida_ID;

        if (!String.IsNullOrEmpty(Hdf_Partida_ID.Value) && !String.IsNullOrEmpty(Hdf_Dependencia_ID.Value))
        {
            Cls_Tal_Parametros_Negocio Parametros_Negocio = new Cls_Tal_Parametros_Negocio();
            Parametros_Negocio = Parametros_Negocio.Consulta_Parametros();

            Disponible_Partida = Cls_Ope_Psp_Manejo_Presupuesto.Consultar_Disponible_Partida(Parametros_Negocio.P_Fuente_Financiamiento_ID
                , Hdf_Programa_ID.Value, Hdf_Dependencia_ID.Value,
                Hdf_Partida_ID.Value, DateTime.Now.Year.ToString(), Ope_Psp_Presupuesto_Aprobado.Campo_Disponible);

            Txt_Disponible_Partida.Text = String.Format("{0:c}", Disponible_Partida);
        }

        Cls_Ope_Tal_Consultas_Generales_Negocio Generales = new Cls_Ope_Tal_Consultas_Generales_Negocio();
        Generales.P_Partida_Especifica_ID = Hdf_Partida_ID.Value.ToString().Trim();
        if (!String.IsNullOrEmpty(Generales.P_Partida_Especifica_ID))
        {
            DataTable Dt_Partida = Generales.Consultar_Partidas_Especificas();
            if (Dt_Partida.Rows.Count > 0)
            {
                Txt_Partida.Text = Dt_Partida.Rows[0]["CLAVE"].ToString();
            }
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged
    ///DESCRIPCIÓN         : Maneja el evento de cambio de Selección del GridView de Busqueda
    ///                      de empleados.
    ///PROPIEDADES         :
    ///CREO                : Salvador Vázquez Camacho.
    ///FECHA_CREO          : 18/Junio/2012
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///*******************************************************************************
    protected void Grid_Busqueda_Tarjetas_Movimientos_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (Grid_Busqueda_Tarjetas_Movimientos.SelectedIndex > (-1))
            {
                String Tarjeta_Seleccionada_ID = HttpUtility.HtmlDecode(Grid_Busqueda_Tarjetas_Movimientos.SelectedRow.Cells[1].Text.Trim());
                Mostrar_Datos(Tarjeta_Seleccionada_ID);
                System.Threading.Thread.Sleep(500);
                Grid_Busqueda_Tarjetas_Movimientos.SelectedIndex = (-1);
                MPE_Listado_Tarjetas.Hide();
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_RowDeleting
    ///DESCRIPCIÓN         : Elimina la reserva de la partida.
    ///PROPIEDADES         :
    ///CREO                : Salvador Vázquez Camacho.
    ///FECHA_CREO          : 18/Junio/2012
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///*******************************************************************************
    protected void Grid_Listado_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            if (e.RowIndex > (-1))
            {
                Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Negocio = new Cls_Cat_Tal_Tarjetas_Gasolina_Negocio();
                Negocio.P_Numero_Registro = HttpUtility.HtmlDecode(Grid_Listado.Rows[e.RowIndex].Cells[0].Text.Trim());
                Negocio.P_Tipo_Movimiento = "DESCONTAR";
                Negocio.P_Empleado_Realizo_ID = Cls_Sessiones.Empleado_ID;
                Negocio.P_Tarjeta_ID = Hdf_Tarjeta_ID.Value.Trim();
                Negocio.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado;
                Negocio.P_Partida_ID = Hdf_Partida_ID.Value;
                Negocio.P_Dependencia_ID = Hdf_Dependencia_ID.Value;
                Negocio.Cancelar_Reserva();
                Configuracion_Formulario(true);
                Limpiar_Catalogo();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo", "alert('Actualización Exitosa');", true);
                Mostrar_Datos(Negocio.P_Tarjeta_ID);
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_RowDataBound
    ///DESCRIPCIÓN         : Muestra u oculta el boton de eliminar
    ///PROPIEDADES         :
    ///CREO                : Salvador Vázquez Camacho.
    ///FECHA_CREO          : 18/Junio/2012
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///*******************************************************************************
    protected void Grid_Listado_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[6].Text.ToString().Trim() == "CANCELADO")
            {
                e.Row.Cells[7].Visible = false;
            }
        }
    }

    #endregion

    #region Eventos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
    ///DESCRIPCIÓN         : Deja los componentes listos para hacer la modificacion.
    ///PROPIEDADES         :
    ///CREO                : Salvador Vázquez Camacho.
    ///FECHA_CREO          : 18/Junio/2012
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///*******************************************************************************
    protected void Btn_Modificar_Click(object sender, EventArgs e)
    {
        try
        {
            if (Btn_Modificar.AlternateText.Equals("Modificar"))
            {
                if (Grid_Resguardante.Rows.Count > 0)
                {
                    Configuracion_Formulario(false);
                    Btn_Modificar.AlternateText = "Actualizar";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                    Btn_Salir.AlternateText = "Cancelar";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                }
                else
                {
                    Lbl_Ecabezado_Mensaje.Text = "Debe seleccionar el Registro que se desea Modificar.";
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            else
            {
                if (Validar_Componentes())
                {
                    Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Negocio = new Cls_Cat_Tal_Tarjetas_Gasolina_Negocio();
                    Negocio.P_Tipo_Movimiento = "CARGAR";
                    Negocio.P_Empleado_Realizo_ID = Cls_Sessiones.Empleado_ID;
                    Negocio.P_Comentarios = Txt_Busqueda_Comentarios.Text.ToUpper();
                    Negocio.P_Saldo_Movimiento = Double.Parse(Txt_Busqueda_Saldo.Text.ToString());
                    Negocio.P_Tarjeta_ID = Hdf_Tarjeta_ID.Value.Trim();
                    Negocio.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                    Negocio.P_Numero_Tarjeta = Txt_Nuemero_Tarjeta.Text.Trim();
                    Negocio.P_Partida_ID = Hdf_Partida_ID.Value;
                    Negocio.P_Dependencia_ID = Hdf_Dependencia_ID.Value;
                    Negocio.P_Programa_Proyecto_ID = Hdf_Programa_ID.Value;
                    Negocio.Agregar_Movimiento_Tarjete_Gasolina();
                    Configuracion_Formulario(true);
                    Limpiar_Catalogo();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo", "alert('Actualización Exitosa');", true);
                    Mostrar_Datos(Negocio.P_Tarjeta_ID);
                    Btn_Modificar.AlternateText = "Modificar";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Btn_Salir.AlternateText = "Salir";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                }
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Click
    ///DESCRIPCIÓN         : Llena la Tabla con la opcion buscada
    ///PROPIEDADES         :
    ///CREO                : Salvador Vázquez Camacho.
    ///FECHA_CREO          : 18/Junio/2012
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///*******************************************************************************
    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        MPE_Listado_Tarjetas.Show();
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN         : Cancela la operación que esta en proceso (Alta o Actualizar) o Sale del Formulario.
    ///PROPIEDADES         :
    ///CREO                : Salvador Vázquez Camacho.
    ///FECHA_CREO          : 18/Junio/2012
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, EventArgs e)
    {
        if (Btn_Salir.AlternateText.Equals("Salir"))
        {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
        else
        {
            Configuracion_Formulario(true);
            Limpiar_Catalogo();
            Btn_Salir.AlternateText = "Salir";
            Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Directa_Tarjeta_Click
    ///DESCRIPCIÓN         : Ejecuta la Busqueda rapida de una tarjeta de gasolina.
    ///PARAMETROS          :     
    ///CREO                : Salvador Vázquez Camacho.
    ///FECHA_CREO          : 20/Junio/2012
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///*******************************************************************************
    protected void Btn_Busqueda_Directa_Tarjeta_Click(object sender, ImageClickEventArgs e)
    {
        if (!(Txt_Busqueda.Text.Trim().Length == 0))
        {
            Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Negocio = new Cls_Cat_Tal_Tarjetas_Gasolina_Negocio();
            Negocio.P_Numero_Tarjeta = Txt_Busqueda.Text.Trim();
            DataTable Dt_Tarjeta = Negocio.Consultar_Tarjetas_Gasolina();
            Limpiar_Catalogo();

            if (Dt_Tarjeta.Rows.Count > 0)
            {
                Mostrar_Datos(Dt_Tarjeta.Rows[0]["TARJETA_ID"].ToString());
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = "No se encontro el No. de Tarjeta";
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        else
        {
            Lbl_Ecabezado_Mensaje.Text = "Debe introducir un valor para buscar.";
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
        Txt_Busqueda.Text = "";
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Tarjetas_Click
    ///DESCRIPCIÓN         : Ejecuta la Busqueda Avanzada para el Resguardante.
    ///PARAMETROS          :     
    ///CREO                : Salvador Vázquez Camacho.
    ///FECHA_CREO          : 19/Junio/2012
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///*******************************************************************************
    protected void Btn_Busqueda_Tarjetas_Click(object sender, EventArgs e)
    {
        try
        {
            Grid_Busqueda_Tarjetas_Movimientos.PageIndex = 0;
            Llenar_Grid_Busqueda_Tarjetas_Movimientos();
            MPE_Listado_Tarjetas.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    #endregion

}
