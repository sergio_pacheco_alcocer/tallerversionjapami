﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Tal_Salidas_Express_Refacciones.aspx.cs" Inherits="paginas_Taller_Mecanico_Frm_Ope_Tal_Salidas_Express_Refacciones" Title="Operaciones Vales Express" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" language="javascript">
        function Limpiar_Ctlr_Busqueda_Resguardante(){
            document.getElementById("<%=Txt_Busqueda_No_Empleado.ClientID%>").value="";
            document.getElementById("<%=Txt_Busqueda_RFC.ClientID%>").value="";  
            document.getElementById("<%=Txt_Busqueda_Nombre_Empleado.ClientID%>").value="";     
            document.getElementById("<%=Cmb_Busqueda_Dependencia.ClientID%>").value="";                               
            return false;
        } 
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
 s   <script type="text/javascript">
        window.onerror = new Function("return true");
        //Abrir una ventana modal
        function Abrir_Ventana_Modal(Url, Propiedades) {
            window.showModalDialog(Url, null, Propiedades);
        }
        function Validar_Longitud_Texto(Text_Box, Max_Longitud) {
            if (Text_Box.value.length > Max_Longitud) {
                Text_Box.value = Text_Box.value.substring(0, Max_Longitud);
            }
        }
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">

  
   <cc1:ToolkitScriptManager ID="ScptM_Catalogo" runat="server" 
    EnableScriptGlobalization="true"
        EnableScriptLocalization="true"
        EnablePartialRendering="true" 
        AsyncPostBackTimeout="9000">
</cc1:ToolkitScriptManager>
  
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>         
            <asp:UpdateProgress ID="Uprg_Loading" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>                     
            </asp:UpdateProgress>
            <div id="Div_Area_Trabajo" style="background-color:#ffffff; width:100%; height:100%;">
                <center>
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">Suministro de Refacciones Servicio Express</td>
                    </tr>
                    <tr>
                        <td>
                          <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;">              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top">
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                            </table>                   
                          </div>                
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>                        
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" >
                            <asp:ImageButton ID="Btn_Nuevo" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" 
                                Width="24px" CssClass="Img_Button" AlternateText="Nuevo" 
                                ToolTip="Nueva Salida de Suministro Express" onclick="Btn_Nuevo_Click" />
                            <asp:ImageButton ID="Btn_Cancelar" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png" Width="24px" 
                                CssClass="Img_Button" AlternateText="Cancelar"
                                ToolTip="Cancela Salida de Suministro Express" onclick="Btn_Cancelar_Click"
                                 OnClientClick="return confirm('¿Esta seguro que desea cancelar el Suministro?');" />
                            <asp:ImageButton ID="Btn_Imprimir" runat="server" 
                                ImageUrl="~/paginas/imagenes/gridview/grid_print.png" Width="24px" 
                                CssClass="Img_Button" AlternateText="Imprimir" 
                                ToolTip="Imprimir Reporte" onclick="Btn_Imprimir_Click"  />
                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Salir"
                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" Width="24px" 
                                CssClass="Img_Button" AlternateText="Salir" onclick="Btn_Salir_Click" />
                        </td>                     
                    </tr>
                </table>
                </center>
            </div>
       
            <div id="Div_Campos" runat="server" style="width:100%;">                
                    <table width="97%"  border="0" cellspacing="0" class="estilo_fuente" style="text-align:center;">
                        <tr>
                            <td colspan="2">
                                <asp:HiddenField ID="Hdf_Refaccion_ID" runat="server" />
                                <asp:HiddenField ID="Hdf_Vehiculo_ID" runat="server" />
                                <asp:HiddenField ID="Hdf_Dependencia_ID" runat="server" />
                                <asp:HiddenField ID="Hdf_Usuario_Recibe_ID" runat="server" />
                                <asp:HiddenField ID="Hdf_No_Registro" runat="server" />
                            </td>
                        </tr>
                            <tr>
                                <td style="width:15%;">
                                    <asp:Label ID="Lbl_No_Registro" runat="server" Text="No. Movimiento"></asp:Label>
                                </td>
                                <td style="width:35%;">
                                    <asp:TextBox ID="Txt_No_Registro" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                </td>
                                <td style="width:15%;">
                                    &nbsp;&nbsp;  
                                    <asp:Label ID="Lbl_Estatus" runat="server" Text="Estatus"></asp:Label>
                                </td>
                                <td style="width:35%;">
                                    <asp:TextBox ID="Txt_Estatus" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                    </table>
                    <asp:Panel ID="Pnl_Vehiculo_Seleccionado" runat="server" Width="99%" GroupingText="Vehículo para la Refacción" HorizontalAlign="Center">
                        <table width="98%"  border="0" cellspacing="0" class="estilo_fuente" >
                            <tr>
                                <td style="width:15%;">
                                    <asp:Label ID="Lbl_No_Inventario" runat="server" Text="No. Inventario"></asp:Label>
                                </td>
                                <td style="width:35%;">
                                    <asp:TextBox ID="Txt_No_Inventario" runat="server" Width="70%" MaxLength="7"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Inventario" runat="server" TargetControlID="Txt_No_Inventario" FilterType="Numbers">
                                    </cc1:FilteredTextBoxExtender>
                                    <asp:ImageButton ID="Btn_Busqueda_Vehiculo" runat="server" 
                                        ImageUrl="~/paginas/imagenes/paginas/busqueda.png" AlternateText="Buscar" 
                                        ToolTip="Buscar" onclick="Btn_Busqueda_Vehiculo_Click" />
                                </td>
                                <td>
                                    <asp:Label ID="Lbl_No_Economico" runat="server" Text="No. Economico"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="Txt_No_Economico" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text="Unidad Responsable"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="Txt_Unidad_Responsable" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Lbl_Datos_Vehiculo" runat="server" Text="Datos Vehiculo"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="Txt_Datos_Vehiculo" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:15%;">
                                    <asp:Label ID="Lbl_Placas" runat="server" Text="Placas"></asp:Label>
                                </td>
                                <td style="width:35%;">
                                    <asp:TextBox ID="Txt_Placas" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                </td>
                                <td style="width:15%;">
                                    &nbsp;&nbsp;  
                                    <asp:Label ID="Lbl_Anio" runat="server" Text="Año"></asp:Label>
                                </td>
                                <td style="width:35%;">
                                    <asp:TextBox ID="Txt_Anio" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            
                        </table>
                    </asp:Panel>
                    <table width="99%"  border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                            <td style="width:15%;">
                                <asp:Label ID="Lbl_Usuario_Recibe" runat="server" Text="Usuario Recibe"></asp:Label>
                            </td>
                            <td >
                                <asp:TextBox ID="Txt_Usuario_Recibe" Width="95%" runat="server" Enabled="false"></asp:TextBox>
                                <asp:ImageButton ID="Btn_Busqueda_Usuario" runat="server" 
                                    ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png" Width="16px" 
                                    AlternateText="Busqueda Avanzada" ToolTip="Buscar Usuario" 
                                    onclick="Btn_Busqueda_Usuario_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Lbl_Observaciones" runat="server" Text="Observaciones"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="Txt_Observaciones" Width="95%" runat="server" Enabled="false" 
                                    TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <asp:Label ID="Lbl_Refaccion" runat="server" Text="Refaccion"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="Txt_Refaccion" runat="server" Width="95%" Enabled="false"></asp:TextBox>
                                <asp:ImageButton ID="Btn_Busqueda_Refaccion" runat="server" 
                                    ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png" Width="16px" 
                                    AlternateText="Busqueda Avanzada" ToolTip="Buscar Refacción" 
                                    onclick="Btn_Busqueda_Refaccion_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%;" >
                                <asp:Label ID="Lbl_Cantidad" runat="server" Text="Cantidad"></asp:Label>
                            </td>
                            <td >
                                <asp:TextBox ID="Txt_Cantidad" runat="server" Enabled="false"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Cantidad" runat="server" TargetControlID="Txt_Cantidad" FilterType="Numbers">
                                </cc1:FilteredTextBoxExtender>
                                <asp:Label ID="Lbl_Unidad" runat="server" Text=""></asp:Label>
                                <asp:Button ID="Btn_Agregar_Refaccion" runat="server" Text="Agregar" 
                                    CssClass="button" onclick="Btn_Agregar_Refaccion_Click" />
                            </td>
                        </tr>
                    </table>
                    <%--inicio de TabContainer--%>
                    <cc1:TabContainer ID="Tab_Contenedor_Salidas_Refacciones_Express" 
                        runat="server" Width="98%" ActiveTabIndex="0">                        
                        <%--tab para grid de Movimientos de Salida--%>
                        <cc1:TabPanel ID="Tbp_Salidas_Expess" runat="server" HeaderText="Tbp_Constancia_Adeudo" Width="100%">
                            <HeaderTemplate>Salidas Express</HeaderTemplate>
                            <ContentTemplate>
                            <center>
                                <asp:GridView ID="Grid_Listado" runat="server" CssClass="GridView_1"
                                AutoGenerateColumns="False" AllowPaging="True" Width="99%"
                                GridLines= "None" EmptyDataText="No hay Salidas Registradas." 
                                onselectedindexchanged="Grid_Listado_SelectedIndexChanged" 
                                onpageindexchanging="Grid_Listado_PageIndexChanging" >
                                <RowStyle CssClass="GridItem" />
                                <Columns>
                                    <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png" >
                                        <ItemStyle Width="30px" />
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="NO_REGISTRO" HeaderText="No Mov" SortExpression="NO_REGISTRO" NullDisplayText="-" >
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FECHA_MOVIMIENTO" HeaderText="Fecha" SortExpression="FECHA_MOVIMIENTO" NullDisplayText="-" DataFormatString="{0:dd/MMM/yyyy hh:mm:ss tt}" >
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DEPENDENCIAS_CLAVE_NOMBRE" HeaderText="Unidad Responsable" SortExpression="DEPENDENCIAS_CLAVE_NOMBRE" NullDisplayText="-" >
                                        <ItemStyle Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="VEHICULOS_NO_INVENTARIO" HeaderText="Vehiculo" SortExpression="VEHICULOS_NO_INVENTARIO" NullDisplayText="-" >
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="REALIZO_MOVIMIENTO" HeaderText="Realizo" SortExpression="REALIZO_MOVIMIENTO" NullDisplayText="-" >
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS" NullDisplayText="-" >
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                </Columns>
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridAltItem" />       
                                </asp:GridView>
                            </center>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <%--tab para grid de Refacciones por Salida--%>
                        <cc1:TabPanel ID="Tbp_Refacciones_Por_Salida" runat="server" HeaderText="Tbp_Constancia_Adeudo" Width="100%">
                            <HeaderTemplate>Refacciones por Salida</HeaderTemplate>
                            <ContentTemplate>
                            <center>
                                <asp:GridView ID="Grid_Listado_Refacciones" runat="server" CssClass="GridView_1"
                                AutoGenerateColumns="False" AllowPaging="True" Width="99%"
                                GridLines= "None" EmptyDataText="No hay Refacciones registradas." 
                                    onrowdatabound="Grid_Listado_Refacciones_RowDataBound" >
                                <RowStyle CssClass="GridItem" />
                                <Columns>
                                    <asp:BoundField DataField="CANTIDAD" HeaderText="Cantidad" SortExpression="CANTIDAD" NullDisplayText="-" >
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="REFACCIONES_CLAVE_NOMBRE" HeaderText="Refaccion" SortExpression="REFACCIONES_CLAVE_NOMBRE" NullDisplayText="-" >
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="COSTO_UNITARIO" HeaderText="Costo Unitario" SortExpression="COSTO_UNITARIO" NullDisplayText="-" >
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="COSTO_TOTAL" HeaderText="Costo Total" SortExpression="COSTO_TOTAL" NullDisplayText="-" >
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Quitar">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="Btn_Quitar_Refaccion" runat="server" ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png" 
                                                    ToolTip="Quitar Refacción" AlternateText="Quitar Refaccion" Height="16px" Width="16px"
                                                    OnClientClick = "return confirm('¿Esta seguro que desea quitar la Refacción del Listado?');"
                                                    OnClick="Btn_Quitar_Refaccion_Click" />
                                        </ItemTemplate>
                                        <ItemStyle Width="30px"  Font-Size="X-Small" HorizontalAlign="Left" />
                                        <HeaderStyle Width="30px"  Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridAltItem" />       
                                </asp:GridView>
                            </center>
                            </ContentTemplate>
                        </cc1:TabPanel>
                    </cc1:TabContainer>
                </div>
                </center>
            </div>
        </ContentTemplate>       
    </asp:UpdatePanel>
    
    <asp:UpdatePanel ID="Aux_MPE_Listado_Empleados" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_MPE_Listado_Empleados" runat="server" Text="Button" style="display:none;" />
                <cc1:ModalPopupExtender ID="MPE_Listado_Empleados" runat="server" CancelControlID="Btn_Cerrar_Ventana"
                    TargetControlID="Btn_Comodin_MPE_Listado_Empleados" PopupControlID="Pnl_Busqueda_Contenedor" PopupDragHandleControlID="Pnl_Busqueda_Resguardante_Cabecera"
                    DropShadow="True" BackgroundCssClass="progressBackgroundFilter" >
                </cc1:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:Panel ID="Pnl_Busqueda_Contenedor" runat="server" CssClass="drag"  HorizontalAlign="Center" Width="850px" 
            style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">                         
            <asp:Panel ID="Pnl_Busqueda_Resguardante_Cabecera" runat="server" 
                style="cursor: move;background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
                <table width="99%">
                    <tr>
                        <td style="color:Black;font-size:12;font-weight:bold;">
                           <asp:Image ID="Img_Informatcion_Autorizacion" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                             B&uacute;squeda: Empleados
                        </td>
                        <td align="right" style="width:10%;">
                           <asp:ImageButton ID="Btn_Cerrar_Ventana" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana"
                                ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png"/>  
                        </td>
                    </tr>
                </table>            
            </asp:Panel>                                                                          
           <div style="color: #5D7B9D">
             <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;" >                                    
                        <asp:UpdatePanel ID="Upnl_Filtros_Busqueda_Prestamos" runat="server">
                            <ContentTemplate>
                            
                                <asp:UpdateProgress ID="Progress_Upnl_Filtros_Busqueda_Prestamos" runat="server" AssociatedUpdatePanelID="Upnl_Filtros_Busqueda_Prestamos" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                        <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress"><img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress> 
                                                             
                                <table width="100%">
                                   <tr>
                                        <td style="width:100%" colspan="4" align="right">
                                            <asp:ImageButton ID="Btn_Limpiar_Ctlr_Busqueda" runat="server" OnClientClick="javascript:return Limpiar_Ctlr_Busqueda_Resguardante();"
                                                ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de Busqueda"/>                         
                                        </td>
                                    </tr>     
                                   <tr>
                                        <td style="width:100%" colspan="4">
                                            <hr />
                                        </td>
                                    </tr>   
                                    <tr>
                                        <td style="width:20%;text-align:left;font-size:11px;">
                                           No Empleado 
                                        </td>              
                                        <td style="width:30%;text-align:left;font-size:11px;">
                                           <asp:TextBox ID="Txt_Busqueda_No_Empleado" runat="server" Width="98%" />
                                           <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_No_Empleado" runat="server" FilterType="Numbers" TargetControlID="Txt_Busqueda_No_Empleado"/>
                                            <cc1:TextBoxWatermarkExtender ID="Twm_Txt_Busqueda_No_Empleado" runat="server" TargetControlID ="Txt_Busqueda_No_Empleado" WatermarkText="Busqueda por No Empleado" 
                                                WatermarkCssClass="watermarked"/>                                                                                                                                          
                                        </td> 
                                        <td style="width:20%;text-align:left;font-size:11px;">
                                            RFC
                                        </td>              
                                        <td style="width:30%;text-align:left;font-size:11px;">
                                           <asp:TextBox ID="Txt_Busqueda_RFC" runat="server" Width="98%" />
                                           <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_RFC" runat="server" FilterType="Numbers, UppercaseLetters"
                                                TargetControlID="Txt_Busqueda_RFC"/>  
                                            <cc1:TextBoxWatermarkExtender ID="Twm_Txt_Busqueda_RFC" runat="server" 
                                                TargetControlID ="Txt_Busqueda_RFC" WatermarkText="Busqueda por RFC" 
                                                WatermarkCssClass="watermarked"/>                                                                                                                                     
                                        </td>                               
                                    </tr>
                                    <tr>
                                        <td style="width:20%;text-align:left;font-size:11px;">
                                            Nombre
                                        </td>              
                                        <td style="width:30%;text-align:left;" colspan="3">
                                            <asp:TextBox ID="Txt_Busqueda_Nombre_Empleado" runat="server" Width="99.5%" />
                                           <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_Nombre_Empleado" runat="server" FilterType="Custom, LowercaseLetters, Numbers, UppercaseLetters"
                                                TargetControlID="Txt_Busqueda_Nombre_Empleado" ValidChars="áéíóúÁÉÍÓÚ ñÑ"/>
                                            <cc1:TextBoxWatermarkExtender ID="Twm_Nombre_Empleado" runat="server" 
                                                TargetControlID ="Txt_Busqueda_Nombre_Empleado" WatermarkText="Busqueda por Nombre" 
                                                WatermarkCssClass="watermarked"/>                                                                                               
                                        </td>                                         
                                    </tr>                   
                                    <tr>
                                        <td style="width:20%;text-align:left;font-size:11px;">
                                            Unidad Responsable
                                        </td>              
                                        <td style="width:30%;text-align:left;font-size:11px;" colspan="3">
                                           <asp:DropDownList ID="Cmb_Busqueda_Dependencia" runat="server" Width="100%" />                                          
                                        </td> 
                                    </tr>                                                                
                                   <tr>
                                        <td style="width:100%" colspan="4">
                                            <hr />
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <td style="width:100%;text-align:left;" colspan="4">
                                            <center>
                                               <asp:Button ID="Btn_Busqueda_Empleados" runat="server"  Text="Busqueda de Empleados" CssClass="button" CausesValidation="false" OnClick="Btn_Busqueda_Empleados_Click" Width="200px" /> 
                                            </center>
                                        </td>                                                     
                                    </tr>                                                                        
                                  </table>   
                                  <br />
                                  <div id="Div_Resultados_Busqueda_Resguardantes" runat="server" style="border-style:outset; width:99%; height: 250px; overflow:auto;">
                                      <asp:GridView ID="Grid_Busqueda_Empleados" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                            ForeColor="#333333" GridLines="None" AllowPaging="True" Width="100%" 
                                            PageSize="100" EmptyDataText="No se encontrarón resultados para los filtros de la busqueda" 
                                            OnSelectedIndexChanged="Grid_Busqueda_Empleados_SelectedIndexChanged"
                                            OnPageIndexChanging="Grid_Busqueda_Empleados_PageIndexChanging" >
                                            <RowStyle CssClass="GridItem" />
                                            <Columns>
                                                <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                    <ItemStyle Width="30px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                    <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                                </asp:ButtonField>
                                                <asp:BoundField DataField="EMPLEADO_ID" HeaderText="EMPLEADO_ID" SortExpression="EMPLEADO_ID">
                                                    <ItemStyle Width="3px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                    <HeaderStyle Width="3px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="NO_EMPLEADO" HeaderText="No. Empleado" SortExpression="NO_EMPLEADO" >
                                                    <ItemStyle Width="70px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                    <HeaderStyle Width="70px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="NOMBRE" HeaderText="Nombre" SortExpression="NOMBRE" NullDisplayText="-" >
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                    <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" SortExpression="DEPENDENCIA" NullDisplayText="-" >
                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                    <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                            </Columns>
                                            <PagerStyle CssClass="GridHeader" />
                                            <SelectedRowStyle CssClass="GridSelected" />
                                            <HeaderStyle CssClass="GridHeader" />
                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView> 
                                </div>                                                                                                                                                          
                            </ContentTemplate>                                                                   
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>                                                      
                    </td>
                </tr>
             </table>                                                   
           </div>                 
    </asp:Panel>

</asp:Content>

