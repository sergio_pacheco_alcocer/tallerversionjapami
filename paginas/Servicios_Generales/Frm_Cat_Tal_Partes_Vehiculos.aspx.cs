﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using System.Collections.Generic;
using JAPAMI.Taller_Mecanico.Catalogo_Partes_Vehiculos.Negocio;

public partial class paginas_Taller_Mecanico_Frm_Cat_Tal_Partes_Vehiculos : System.Web.UI.Page{

    #region Page_Load

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN: Metodo que se carga cada que ocurre un PostBack de la Página
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 08/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e){
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack) {
                Configuracion_Formulario(true);
                Llenar_Cmb_Parent();
                Llenar_Grid_Listado(0);
                Limpiar_Catalogo();
            }
            Div_Contenedor_Msj_Error.Visible = false;
        }

    #endregion
    
    #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Cmb_Parent
        ///DESCRIPCIÓN: Llena el Combo de Parent
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 08/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private void Llenar_Cmb_Parent() {
            try{
                Cls_Cat_Tal_Partes_Vehiculos_Negocio Negocio = new Cls_Cat_Tal_Partes_Vehiculos_Negocio();
                Negocio.P_Estatus = "VIGENTE";
                Negocio.P_Tipo = "PARTE";
                Cmb_Parent.DataSource = Negocio.Consultar_Partes();
                Cmb_Parent.DataTextField = "NOMBRE";
                Cmb_Parent.DataValueField = "PARTE_ID";
                Cmb_Parent.DataBind();
                Cmb_Parent.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
            }catch(Exception Ex){
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
        ///DESCRIPCIÓN: Carga una configuracion de los controles del Formulario
        ///PROPIEDADES: 1. Estatus. Estatus en el que se cargara la configuración de los
        ///              controles.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 08/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private void Configuracion_Formulario( Boolean Estatus ) {
            Btn_Nuevo.Visible = true;
            Btn_Nuevo.AlternateText = "Nuevo";
            Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
            Btn_Modificar.Visible = true;
            Btn_Modificar.AlternateText = "Modificar";
            Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
            Btn_Eliminar.Visible = Estatus;
            Txt_Nombre.Enabled = !Estatus;
            Cmb_Estatus.Enabled = !Estatus;
            Cmb_Tipo.Enabled = !Estatus;
            Cmb_Parent.Enabled = !Estatus;
            Grid_Listado.Enabled = Estatus;
            Grid_Listado.SelectedIndex = (-1);
            Deseleccionar_Grids_Internos();
            Btn_Buscar.Enabled = Estatus;
            Txt_Busqueda.Enabled = Estatus;
            Cmb_Tipo_SelectedIndexChanged(Cmb_Tipo, null);
            //Configuracion_Acceso("Frm_Cat_Pat_Com_Zonas.aspx");
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Limpiar_Catalogo
        ///DESCRIPCIÓN: Limpia los controles del Formulario
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 08/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private void Limpiar_Catalogo() {
            Hdf_Parte_ID.Value = "";
            Txt_Parte_ID.Text = "";
            Txt_Nombre.Text = "";
            Cmb_Estatus.SelectedIndex = 0;
            Cmb_Tipo.SelectedIndex = 0;
            Cmb_Parent.SelectedIndex = 0;
        }
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Listado
        ///DESCRIPCIÓN: Llena el Listado con una consulta que puede o no
        ///             tener Filtros.
        ///PROPIEDADES:     
        ///             1. Pagina.  Pagina en la cual se mostrará el Grid_VIew
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 08/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private void Llenar_Grid_Listado(int Pagina) {
            try{
                Cls_Cat_Tal_Partes_Vehiculos_Negocio Negocio = new Cls_Cat_Tal_Partes_Vehiculos_Negocio();
                Negocio.P_Tipo = "SUBPARTE";
                Session["Dt_SubPartes"] = Negocio.Consultar_Partes();
                Negocio = new Cls_Cat_Tal_Partes_Vehiculos_Negocio();
                Negocio.P_Nombre = Txt_Busqueda.Text.Trim();
                Negocio.P_Tipo = "PARTE";
                Grid_Listado.DataSource = Negocio.Consultar_Partes();
                Grid_Listado.PageIndex = Pagina;
                Grid_Listado.DataBind();
                Session.Remove("Dt_SubPartes");
                Deseleccionar_Grids_Internos();
            }catch(Exception Ex){
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Deseleccionar_Grids_Internos
        ///DESCRIPCIÓN         : Deseleccionar_Grids_Internos
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 07/Julio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Deseleccionar_Grids_Internos()
        {
            foreach (GridViewRow Fila_Grid in Grid_Listado.Rows)
            {
                if (Fila_Grid.FindControl("Grid_SubListado") != null)
                {
                    GridView Grid_SubListado = (GridView)Fila_Grid.FindControl("Grid_SubListado");
                    Grid_SubListado.SelectedIndex = (-1);
                }
            }
        }
    
        #region Validaciones

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Componentes
            ///DESCRIPCIÓN: Hace una validacion de que haya datos en los componentes antes de hacer
            ///             una operación.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 08/Mayo/2012 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private Boolean Validar_Componentes() {
                Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                String Mensaje_Error = "";
                Boolean Validacion = true;
                if (Txt_Nombre.Text.Trim().Length == 0) {
                    Mensaje_Error = Mensaje_Error + "+ Introducir el Nombre.";
                    Validacion = false;
                }
                if (Cmb_Estatus.SelectedIndex == 0) {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar una opcion del Combo de Estatus.";
                    Validacion = false;
                }
                if (Cmb_Tipo.SelectedIndex == 0) {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar una opcion del Combo de Tipo.";
                    Validacion = false;
                }
                if (Cmb_Tipo.SelectedItem.Value.Trim().Equals("SUBPARTE")) {
                    if (Cmb_Parent.SelectedIndex == 0) {
                        if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                        Mensaje_Error = Mensaje_Error + "+ Seleccionar una opcion del Combo de Parent.";
                        Validacion = false;
                    }
                    if (Cmb_Parent.SelectedItem.Value.Trim().Equals(Hdf_Parte_ID.Value.Trim())) {
                        if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                        Mensaje_Error = Mensaje_Error + "+ El Valor no puede ser su mismo Parent.";
                        Validacion = false;
                    }
                }
                if (!Validacion) {
                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
                    Div_Contenedor_Msj_Error.Visible = true;
                }
                return Validacion;
            }

        #endregion

    #endregion

    #region Grids

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_PageIndexChanging
        ///DESCRIPCIÓN: Maneja la paginación del GridView
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 08/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            try {
                Grid_Listado.SelectedIndex = (-1);
                Llenar_Grid_Listado(e.NewPageIndex);
                Limpiar_Catalogo();
            } catch(Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_SelectedIndexChanged
        ///DESCRIPCIÓN: Obtiene los datos de un Listado Seleccionada para mostrarlos a detalle
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 08/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_SelectedIndexChanged(object sender, EventArgs e) {
            try{
                if (Grid_Listado.SelectedIndex > (-1)){
                    Limpiar_Catalogo();
                    Cls_Cat_Tal_Partes_Vehiculos_Negocio Negocio = new Cls_Cat_Tal_Partes_Vehiculos_Negocio();
                    Negocio.P_Parte_ID = HttpUtility.HtmlDecode(Grid_Listado.SelectedRow.Cells[2].Text.Trim());
                    Negocio = Negocio.Consultar_Detalles_Parte();
                    Hdf_Parte_ID.Value = Negocio.P_Parte_ID;
                    Txt_Parte_ID.Text = Negocio.P_Parte_ID;
                    Txt_Nombre.Text = Negocio.P_Nombre;
                    Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(Negocio.P_Estatus));
                    Cmb_Tipo.SelectedIndex = Cmb_Tipo.Items.IndexOf(Cmb_Tipo.Items.FindByValue(Negocio.P_Tipo));
                    Cmb_Parent.SelectedIndex = Cmb_Parent.Items.IndexOf(Cmb_Parent.Items.FindByValue(Negocio.P_Parent));
                    Deseleccionar_Grids_Internos();
                    System.Threading.Thread.Sleep(500);
                }
            }catch(Exception Ex){
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_RowDataBound
        ///DESCRIPCIÓN: Ejecuta el DataRow del Listado.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 09/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_RowDataBound(object sender, GridViewRowEventArgs e) {
            if (e.Row.RowType == DataControlRowType.DataRow) {
                String Parte_ID = HttpUtility.HtmlDecode(e.Row.Cells[2].Text);
                if (Session["Dt_SubPartes"] != null) {
                    DataTable Dt_SubPartes = (DataTable)Session["Dt_SubPartes"];
                    DataTable Dt_Filtrado = Dt_SubPartes.Clone();
                    DataRow[] Dr_Filas_Filtradas = Dt_SubPartes.Select("PARENT_ID = '" + Parte_ID + "'");
                    foreach (DataRow Fila_Actual in Dr_Filas_Filtradas) {
                        Dt_Filtrado.ImportRow(Fila_Actual);
                    }
                    if (e.Row.FindControl("Grid_SubListado") != null) {
                        GridView Grid_SubListado = (GridView)e.Row.FindControl("Grid_SubListado");
                        Grid_SubListado.DataSource = Dt_Filtrado;
                        Grid_SubListado.DataBind();
                    }
                }
            }
        }
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_SubListado_SelectedIndexChanged
        ///DESCRIPCIÓN: Obtiene los datos de un Listado Seleccionada para mostrarlos a detalle
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 09/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_SubListado_SelectedIndexChanged(object sender, EventArgs e) {
            try{
                if (sender != null) {
                    GridView Grid_SubListado = (GridView)sender;
                    if (Grid_SubListado.SelectedIndex > (-1)){
                        Limpiar_Catalogo();
                        Cls_Cat_Tal_Partes_Vehiculos_Negocio Negocio = new Cls_Cat_Tal_Partes_Vehiculos_Negocio();
                        Negocio.P_Parte_ID = HttpUtility.HtmlDecode(Grid_SubListado.SelectedRow.Cells[1].Text.Trim());
                        Negocio = Negocio.Consultar_Detalles_Parte();
                        Hdf_Parte_ID.Value = Negocio.P_Parte_ID;
                        Txt_Parte_ID.Text = Negocio.P_Parte_ID;
                        Txt_Nombre.Text = Negocio.P_Nombre;
                        Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(Negocio.P_Estatus));
                        Cmb_Tipo.SelectedIndex = Cmb_Tipo.Items.IndexOf(Cmb_Tipo.Items.FindByValue(Negocio.P_Tipo));
                        Cmb_Parent.SelectedIndex = Cmb_Parent.Items.IndexOf(Cmb_Parent.Items.FindByValue(Negocio.P_Parent));
                        Grid_Listado.SelectedIndex = (-1);
                        System.Threading.Thread.Sleep(500);
                    }
                }

            }catch(Exception Ex){
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }

    #endregion

    #region Eventos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
        ///DESCRIPCIÓN: Deja los componentes listos para dar de Alta un registro.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 08/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Nuevo_Click(object sender, EventArgs e){
            try{
                if (Btn_Nuevo.AlternateText.Equals("Nuevo")){
                    Configuracion_Formulario(false);
                    Limpiar_Catalogo();
                    Btn_Nuevo.AlternateText = "Dar de Alta";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Salir.AlternateText = "Cancelar";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Modificar.Visible = false;
                    Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue("VIGENTE"));
                }else {
                    if (Validar_Componentes()){
                        Cls_Cat_Tal_Partes_Vehiculos_Negocio Negocio = new Cls_Cat_Tal_Partes_Vehiculos_Negocio();
                        Negocio.P_Nombre = Txt_Nombre.Text.Trim();
                        Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                        Negocio.P_Tipo = Cmb_Tipo.SelectedItem.Value;
                        Negocio.P_Parent = Cmb_Parent.SelectedItem.Value;
                        Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                        Negocio.Alta_Parte();
                        Configuracion_Formulario(true);
                        Llenar_Grid_Listado(Grid_Listado.PageIndex);
                        Llenar_Cmb_Parent();
                        Limpiar_Catalogo();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo", "alert('Alta Exitosa');", true);
                        Btn_Nuevo.AlternateText = "Nuevo";
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                        Btn_Salir.AlternateText = "Salir";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    }
                }
            }catch(Exception Ex){
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
        ///DESCRIPCIÓN: Deja los componentes listos para hacer la modificacion.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 08/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Modificar_Click(object sender, EventArgs e){
            try{
                if (Btn_Modificar.AlternateText.Equals("Modificar")){
                    if (!String.IsNullOrEmpty(Hdf_Parte_ID.Value)){
                        Configuracion_Formulario(false);
                        Btn_Modificar.AlternateText = "Actualizar";
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                        Btn_Salir.AlternateText = "Cancelar";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        Btn_Nuevo.Visible = false;
                    }else{
                        Lbl_Ecabezado_Mensaje.Text = "Debe seleccionar el Registro que se desea Modificar.";
                        Lbl_Mensaje_Error.Text = "";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                } else {
                    if (Validar_Componentes()){
                        Cls_Cat_Tal_Partes_Vehiculos_Negocio Negocio = new Cls_Cat_Tal_Partes_Vehiculos_Negocio();
                        Negocio.P_Parte_ID = Hdf_Parte_ID.Value;
                        Negocio.P_Nombre = Txt_Nombre.Text.Trim();
                        Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                        Negocio.P_Tipo = Cmb_Tipo.SelectedItem.Value;
                        Negocio.P_Parent = Cmb_Parent.SelectedItem.Value;
                        Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                        Negocio.Modificar_Parte();
                        Configuracion_Formulario(true);
                        Llenar_Grid_Listado(Grid_Listado.PageIndex);
                        Llenar_Cmb_Parent();
                        Limpiar_Catalogo();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo", "alert('Actualización Exitosa');", true);
                        Btn_Modificar.AlternateText = "Modificar";
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                        Btn_Salir.AlternateText = "Salir";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    }
                }
            }catch(Exception Ex){
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Click
        ///DESCRIPCIÓN: Llena la Tabla con la opcion buscada
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 08/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e) {
            try{
                Limpiar_Catalogo();
                Grid_Listado.SelectedIndex = (-1);
                Grid_Listado.SelectedIndex = (-1);
                Llenar_Grid_Listado(0);
                if (Grid_Listado.Rows.Count == 0 && Txt_Busqueda.Text.Trim().Length > 0) {
                    Lbl_Ecabezado_Mensaje.Text = "Para la Busqueda con el nombre \"" + Txt_Busqueda.Text + "\" no se encontrarón coincidencias";
                    Lbl_Mensaje_Error.Text = "(Se cargarón todos los registros almacenados)";
                    Div_Contenedor_Msj_Error.Visible = true;
                    Txt_Busqueda.Text = "";
                    Llenar_Grid_Listado(0);
                }
            }catch(Exception Ex){
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Eliminar_Click
        ///DESCRIPCIÓN: Elimina un registro de la Base de Datos
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 08/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Eliminar_Click(object sender, EventArgs e){
            try{
                if (!String.IsNullOrEmpty(Hdf_Parte_ID.Value)){
                    Cls_Cat_Tal_Partes_Vehiculos_Negocio Negocio = new Cls_Cat_Tal_Partes_Vehiculos_Negocio();
                    Negocio.P_Parte_ID = Hdf_Parte_ID.Value;
                    Negocio.Eliminar_Parte();
                    Grid_Listado.SelectedIndex = (-1);
                    Llenar_Grid_Listado(Grid_Listado.PageIndex);
                    Llenar_Cmb_Parent();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo", "alert('Eliminación Exitosa');", true);
                    Limpiar_Catalogo();
                }else{
                    Lbl_Ecabezado_Mensaje.Text = "Debe seleccionar el Registro que se desea Eliminar.";
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }catch(Exception Ex){
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Cancela la operación que esta en proceso (Alta o Actualizar) o Sale del Formulario.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 08/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, EventArgs e){
            if (Btn_Salir.AlternateText.Equals("Salir")){
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }else {
                Configuracion_Formulario(true);
                Limpiar_Catalogo();
                Btn_Salir.AlternateText = "Salir";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Cmb_Tipo_SelectedIndexChanged
        ///DESCRIPCIÓN: Habilita ó Inhabilita el Parent.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 08/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Cmb_Tipo_SelectedIndexChanged(object sender, EventArgs e) {
            if (Cmb_Tipo.Enabled) { 
                if (Cmb_Tipo.SelectedItem.Value.Trim().Equals("SUBPARTE")) {
                    Cmb_Parent.Enabled = true;
                } else {
                    Cmb_Parent.SelectedIndex = 0;
                    Cmb_Parent.Enabled = false;
                }
            } else {
                Cmb_Parent.SelectedIndex = 0;
                Cmb_Parent.Enabled = false;
            }
        }

    #endregion

    #region (Control Acceso Pagina)
        /// *****************************************************************************************************************************
        /// NOMBRE: Configuracion_Acceso
        /// 
        /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
        /// 
        /// PARÁMETROS: No Áplica.
        /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
        /// FECHA CREÓ: 23/Mayo/2011 10:43 a.m.
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACIÓN:
        /// *****************************************************************************************************************************
        protected void Configuracion_Acceso(String URL_Pagina)
        {
            List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
            DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

            try
            {
                //Agregamos los botones a la lista de botones de la página.
                Botones.Add(Btn_Nuevo);
                Botones.Add(Btn_Modificar);
                Botones.Add(Btn_Eliminar);
                Botones.Add(Btn_Buscar);

                if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
                {
                    if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                    {
                        //Consultamos el menu de la página.
                        Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                        if (Dr_Menus.Length > 0)
                        {
                            //Validamos que el menu consultado corresponda a la página a validar.
                            if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                            {
                                Cls_Util.Configuracion_Acceso_Sistema_SIAS_AlternateText(Botones, Dr_Menus[0]); // Habilitamos la configuracón de los botones.
                            }
                            else
                            {
                                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                            }
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: IsNumeric
        /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
        /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
        /// CREO        : Juan Alberto Hernandez Negrete
        /// FECHA_CREO  : 29/Noviembre/2010
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private Boolean Es_Numero(String Cadena)
        {
            Boolean Resultado = true;
            Char[] Array = Cadena.ToCharArray();
            try
            {
                for (int index = 0; index < Array.Length; index++)
                {
                    if (!Char.IsDigit(Array[index])) return false;
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al validar si es un dato numerico. Error [" + Ex.Message + "]");
            }
            return Resultado;
        }
    #endregion

}