﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using CarlosAg.ExcelXmlWriter;
using System.Data;
using System.Data.OleDb;
using JAPAMI.Taller_Mecanico.Catalogo_Tarjetas_Gasolina.Negocio;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using System.Security.Cryptography;
using System.IO;
using JAPAMI.Sessiones;

public partial class paginas_Servicios_Generales_Mig_Mov_Gas : System.Web.UI.Page
{
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Page_Load
    ///DESCRIPCIÓN:          Page_Load
    ///PARAMETROS:           
    ///CREO:                 Jesus Toledo
    ///FECHA_CREO:           23/Mayo/2011 
    ///MODIFICO:              
    ///FECHA_MODIFICO:         
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Cls_Sessiones.Nombre_Empleado)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        Liga_Frente.HRef = "../../ARCHIVOS_TALLER_MUNICIPAL/TARJETAS_GASOLINA/layout.xlsx";
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Leer_Excel
    ///DESCRIPCIÓN:          Metodo que consulta un archivo EXCEL
    ///PARAMETROS:           String sqlExcel.- string que contiene el select
    ///CREO:                 Jesus Toledo
    ///FECHA_CREO:           23/Mayo/2011 
    ///MODIFICO:              
    ///FECHA_MODIFICO:        
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public DataSet Leer_Excel(String sqlExcel, String Archivo)
    {
        //Para empezar definimos la conexión OleDb a nuestro fichero Excel.
        String Rta = @MapPath("../../ARCHIVOS_TALLER_MUNICIPAL/TARJETAS_GASOLINA/" + Archivo);
        String sConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                           "Data Source=" + Rta + ";" +
                           "Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";

        //Definimos el DataSet donde insertaremos los datos que leemos del excel
        DataSet DS = new DataSet();

        //Definimos la conexión OleDb al fichero Excel y la abrimos
        OleDbConnection oledbConn = new OleDbConnection(sConnectionString);
        oledbConn.Open();

        //Creamos un comand para ejecutar la sentencia SELECT.
        OleDbCommand oledbCmd = new OleDbCommand(sqlExcel, oledbConn);

        //Creamos un dataAdapter para leer los datos y asocialor al DataSet.
        OleDbDataAdapter da = new OleDbDataAdapter(oledbCmd);
        da.Fill(DS);
        oledbConn.Close();
        return DS;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:Btn_Salir_Click
    ///DESCRIPCIÓN:         Cancela la operación que esta en proceso (Alta o Actualizar) o Sale del Formulario.
    ///PROPIEDADES:     
    ///CREO:                Jesus Toledo
    ///FECHA_CREO:          23/Mayo/2011 
    ///MODIFICO:              
    ///FECHA_MODIFICO:        
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, EventArgs e)
    {
        if (Btn_Salir_Tarjeta_Gasolina.AlternateText.Equals("Salir"))
        {
            Session["NOMBRE_ARCHIVO_TARJETAS"] = null;
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Movimientos
    ///DESCRIPCIÓN:          Llenar_Grid_Movimientos
    ///PROPIEDADES:     
    ///CREO:                 Jesus Toledo
    ///FECHA_CREO:           23/Mayo/2011 
    ///MODIFICO:              
    ///FECHA_MODIFICO:        
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Mostrar_Movimientos_Click(object sender, ImageClickEventArgs e)
    {
        Int64 Max_Movimientos_Migrados = 100;
        DataSet Ds_Movimientos = new DataSet();
        DataTable Dt_Movimientos = new DataTable();
        DataTable Dt_100_Movimientos = new DataTable();
        String SqlExcel = "Select * From [MOVIMIENTOS$]";
        if (Session["NOMBRE_ARCHIVO_TARJETAS"] != null)
        {
            Ds_Movimientos = Leer_Excel(SqlExcel, "Relacion_Mov_Combustible.xlsx");
            Dt_Movimientos = Ds_Movimientos.Tables[0];
            Dt_100_Movimientos = Dt_Movimientos.Clone();
            if (Dt_Movimientos.Rows.Count <= 100)
                Max_Movimientos_Migrados = Dt_Movimientos.Rows.Count - 1;
            for (int Cont_Renglones = 0; Cont_Renglones <= Max_Movimientos_Migrados; Cont_Renglones++)
            {
                if (!String.IsNullOrEmpty(Dt_Movimientos.Rows[Cont_Renglones]["NUMERO_TARJETA"].ToString()))
                {
                    Dt_100_Movimientos.ImportRow(Dt_Movimientos.Rows[Cont_Renglones]);
                    Dt_100_Movimientos.AcceptChanges();
                }
            }
            Grid_Movimientos.DataSource = Dt_100_Movimientos;
            Grid_Movimientos.DataBind();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:Guardar_Movimientos
    ///DESCRIPCIÓN:         Guardar_Movimientos
    ///PROPIEDADES:     
    ///CREO:                Jesus Toledo
    ///FECHA_CREO:          23/Mayo/2011 
    ///MODIFICO:              
    ///FECHA_MODIFICO:        
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Guardar_Movimientos()
    {
        Int64 Cont_Movimientos_Migrados = 0;
        DataSet Ds_Movimientos = new DataSet();
        DataTable Dt_Movimientos = new DataTable();
        DataTable Dt_100_Movimientos = new DataTable();
        String SqlExcel = "Select * From [MOVIMIENTOS$]";
        String Movimientos_No_Migrados = "";
        if (Session["NOMBRE_ARCHIVO_TARJETAS"] != null)
        {
            Ds_Movimientos = Leer_Excel(SqlExcel, "Relacion_Mov_Combustible.xlsx");
            Dt_Movimientos = Ds_Movimientos.Tables[0];
            foreach (DataRow Dr_Renglon_Contador in Dt_Movimientos.Rows)
            {
                try
                {
                    Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Negocio_Movimientos = new Cls_Cat_Tal_Tarjetas_Gasolina_Negocio();
                    Negocio_Movimientos.P_Numero_Tarjeta = Dr_Renglon_Contador["NUMERO_TARJETA"].ToString().Trim();
                    Negocio_Movimientos = Negocio_Movimientos.Consultar_Detalles_Tarjetas_Gasolina();
                    Negocio_Movimientos.P_Saldo_Movimiento = Convert.ToDouble(Dr_Renglon_Contador["MONTO"].ToString().Trim());
                    Negocio_Movimientos.P_Comentarios = Dr_Renglon_Contador["COMENTARIOS"].ToString().Trim();
                    Negocio_Movimientos.P_Tipo_Movimiento = "CARGAR";
                    Negocio_Movimientos.P_Usuario_Creo = "[MIGRACIÓN CONTEL]";
                    if (!String.IsNullOrEmpty(Dr_Renglon_Contador["FECHA_MOVIMIENTO"].ToString().Trim()))
                        Negocio_Movimientos.P_Fecha_Movimiento = Convert.ToDateTime(Dr_Renglon_Contador["FECHA_MOVIMIENTO"].ToString().Trim());
                    Negocio_Movimientos.P_Empleado_Realizo_ID = Cls_Sessiones.Empleado_ID;
                    Negocio_Movimientos.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                    Negocio_Movimientos.P_Ignorar_Reserva = true;
                    Negocio_Movimientos.Agregar_Movimiento_Tarjete_Gasolina();
                    Cont_Movimientos_Migrados++;
                }
                catch { Movimientos_No_Migrados = Movimientos_No_Migrados + Dr_Renglon_Contador["NUMERO_TARJETA"].ToString().Trim(); }
                finally { }
            }
            Lbl_Total.Text = Movimientos_No_Migrados;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "-", "alert('EXITO:" + Cont_Movimientos_Migrados.ToString() + " FILAS REGISTRADAS!');", true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:Obtener_Dato_Consulta
    ///DESCRIPCIÓN:         Obtener_Dato_Consulta
    ///PROPIEDADES:     
    ///CREO:                Jesus Toledo
    ///FECHA_CREO:          23/Mayo/2011 
    ///MODIFICO:              
    ///FECHA_MODIFICO:        
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private String Obtener_Dato_Consulta(String Campo, String Tabla, String Condiciones)
    {
        String Mi_SQL;
        String Dato_Consulta = "";

        try
        {
            Mi_SQL = "SELECT " + Campo;
            if (Tabla != "")
            {
                Mi_SQL += " FROM " + Tabla;
            }
            if (Condiciones != "")
            {
                Mi_SQL += " WHERE " + Condiciones;
            }

            SqlDataReader Dr_Dato = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

            if (Dr_Dato.Read())
            {
                if (Dr_Dato[0] != null)
                {
                    Dato_Consulta = Dr_Dato[0].ToString();
                }
                else
                {
                    Dato_Consulta = "";
                }
                Dr_Dato.Close();
            }
            else
            {
                Dato_Consulta = "";
            }
            if (Dr_Dato != null)
            {
                Dr_Dato.Close();
            }
            Dr_Dato = null;
        }
        catch
        {
        }
        finally
        {
        }

        return Dato_Consulta;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:Btn_Procesar_Click
    ///DESCRIPCIÓN:         Btn_Procesar_Click
    ///PROPIEDADES:     
    ///CREO:                Jesus Toledo
    ///FECHA_CREO:          23/Mayo/2011 
    ///MODIFICO:              
    ///FECHA_MODIFICO:        
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Procesar_Click(object sender, EventArgs e)
    {

    }

    ///*************************************************************************************************************************
    ///Nombre: Pasar_DataTable_A_Excel
    ///Descripción: Pasa DataTable a Excel. 
    ///Parámetros: Dt_Reporte.- DataTable que se pasara a excel. 
    ///CREO:                 Jesus Toledo
    ///FECHA_CREO:           23/Mayo/2011 
    ///MODIFICO:              
    ///FECHA_MODIFICO:        
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Pasar_DataTable_A_Excel(System.Data.DataTable P_Dt_Reporte)
    {
        String Ruta = "Documento.xls";
        DataTable Dt_Reporte = new DataTable();
        Dt_Reporte = P_Dt_Reporte.Copy();
        //Dt_Reporte.Columns["NO"].ColumnName = "Refacción";
        //Dt_Reporte.Columns["DESCRIPCION_PRODUCTO"].ColumnName = "Descripción";
        //Dt_Reporte.Columns["CANTIDAD"].ColumnName = "Cantidad";
        //Dt_Reporte.Columns["PRECIO_U_SIN_IMP_COTIZADO"].ColumnName = "Precio unitario";
        //Dt_Reporte.Columns["IVA_COTIZADO"].ColumnName = "IVA";
        //Dt_Reporte.Columns["TOTAL_COTIZADO"].ColumnName = "Total";
        //Dt_Reporte.Columns["NOMBRE_PROVEEDOR"].ColumnName = "Proveedor";
        //Dt_Reporte.Columns["MARCA_OC"].ColumnName = "Marca";

        try
        {
            //Creamos el libro de Excel.
            CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();

            Libro.Properties.Title = "Reporte de Tarjeta de Gasolina";
            Libro.Properties.Created = DateTime.Now;
            Libro.Properties.Author = "Patrimonio";

            //Creamos una hoja que tendrá el libro.
            CarlosAg.ExcelXmlWriter.Worksheet Hoja = Libro.Worksheets.Add("Registros");
            //Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon = Hoja.Table.Rows.Add();
            //Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");

            Estilo_Cabecera.Font.FontName = "Tahoma";
            Estilo_Cabecera.Font.Size = 10;
            Estilo_Cabecera.Font.Bold = true;
            Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Cabecera.Font.Color = "#FFFFFF";
            Estilo_Cabecera.Interior.Color = "#193d61";
            Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Alignment.WrapText = true;

            Estilo_Contenido.Font.FontName = "Tahoma";
            Estilo_Contenido.Font.Size = 8;
            Estilo_Contenido.Font.Bold = true;
            Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Contenido.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Contenido.Font.Color = "#000000";
            Estilo_Contenido.Interior.Color = "White";
            Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Alignment.WrapText = true;

            //Agregamos las columnas que tendrá la hoja de excel.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(250));//Refaccion
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(60));//Cantidad
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//Precio unitario
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(70));//IVA
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(70));//Total.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(250));//Proveedor
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(250));//Descripcion.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(180));//Marca.

            if (Dt_Reporte is System.Data.DataTable)
            {
                if (Dt_Reporte.Rows.Count > 0)
                {
                    foreach (System.Data.DataColumn COLUMNA in Dt_Reporte.Columns)
                    {
                        if (COLUMNA is System.Data.DataColumn)
                        {
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(COLUMNA.ColumnName, "HeaderStyle"));
                        }
                        Renglon.Height = 20;
                    }

                    foreach (System.Data.DataRow FILA in Dt_Reporte.Rows)
                    {
                        if (FILA is System.Data.DataRow)
                        {
                            Renglon = Hoja.Table.Rows.Add();
                            foreach (System.Data.DataColumn COLUMNA in Dt_Reporte.Columns)
                            {
                                if (COLUMNA is System.Data.DataColumn)
                                {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(FILA[COLUMNA.ColumnName].ToString(), DataType.String, "BodyStyle"));
                                }
                            }
                            Renglon.Height = 20;
                            Renglon.AutoFitHeight = true;
                        }
                    }
                }
            }

            //Abre el archivo de excel
            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Ruta);
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            Libro.Save(Response.OutputStream);
            Response.End();
        }
        catch { };
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:Fup_Frente_UploadedComplete
    ///DESCRIPCIÓN:         Fup_Frente_UploadedComplete
    ///PROPIEDADES:     
    ///CREO:                Jesus Toledo
    ///FECHA_CREO:          23/Mayo/2011 
    ///MODIFICO:              
    ///FECHA_MODIFICO:        
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Fup_Frente_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        HashAlgorithm sha = HashAlgorithm.Create("SHA1");
        String Checksum_Archivo = BitConverter.ToString(sha.ComputeHash(Fup_Frente.FileBytes));       //obtener checksum del archivo
        Dictionary<String, Byte[]> Diccionario_Archivos = Obtener_Diccionario_Archivos();   //obtener diccionario checksum-archivo
        String Extension_Archivo = Path.GetExtension(Fup_Frente.FileName).ToLower();//Aqui obtiene el Nombre del Archivo CAMBIAR POR FUP DE MI FORM
        Session["NOMBRE_ARCHIVO_TARJETAS"] = Fup_Frente.FileName + Extension_Archivo;
        // arreglo con las extensiones de archivo permitidas
        String[] Extensiones_Permitidas = { ".xlsx" };//Esto esta bien
        // si la extension del archivo recibido no es valida, regresar
        String Nombre_Archivo = "Relacion_Mov_Combustible.xlsx";
        Hdf_Nombre_Guardado.Value = Nombre_Archivo;

        if (Array.IndexOf(Extensiones_Permitidas, Extension_Archivo) < 0)
        {
            //Mensaje_Error(" No se permite subir archivos con extensión: " + Extension_Archivo);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Error_Extension_Archivo", "top.$get(\"" +
            Fup_Frente.ClientID + "\").style.background-color = 'red!important';", true);
            return;
        }

        if (Fup_Frente.FileBytes.Length > 2048000) // si la longitud del archivo recibido es mayor que 2MB, mostrar mensaje
        {
            //Mensaje_Error(" El tamaño del archivo excede el limite permitido: " + Fup_Frente.FileName);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Error_Tamanio_Archivo", "top.$get(\"" +
            Fup_Frente.ClientID + "\").style.background-color = 'red!important';", true);
            return;
        }

        if (!Diccionario_Archivos.ContainsKey(Checksum_Archivo)) //si el checksum no esta en el diccionario, agregarlo y guardar en variable de sesion
        {
            Diccionario_Archivos.Add(Checksum_Archivo, Fup_Frente.FileBytes);
            Session["Diccionario_Archivos"] = Diccionario_Archivos;
        }
        Guardar_Archivos(Diccionario_Archivos, Checksum_Archivo, Nombre_Archivo);
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:Guardar_Archivos
    ///DESCRIPCIÓN:         Guardar_Archivos
    ///PROPIEDADES:     
    ///CREO:                Jesus Toledo
    ///FECHA_CREO:          23/Mayo/2011 
    ///MODIFICO:              
    ///FECHA_MODIFICO:        
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Guardar_Archivos(Dictionary<String, Byte[]> P_Dicc, String P_Checksum, String P_Ruta_Archivo)
    {
        DataTable Tabla_Tramites = (DataTable)Session["Tabla_imagenes_Entrada"];
        //Dictionary<String, Byte[]> Diccionario_Archivos = Obtener_Diccionario_Archivos();
        String Nombre_Directorio;
        String Ruta_Archivo;

        try
        {
            Nombre_Directorio = MapPath(Path.GetDirectoryName(P_Ruta_Archivo));
            Ruta_Archivo = MapPath(HttpUtility.HtmlDecode(@"../../ARCHIVOS_TALLER_MUNICIPAL\TARJETAS_GASOLINA\" + P_Ruta_Archivo));
            if (!Directory.Exists(Nombre_Directorio))                       //si el directorio no existe, crearlo
                Directory.CreateDirectory(Nombre_Directorio);
            //crear filestream y binarywriter para guardar archivo
            FileStream Escribir_Archivo = new FileStream(Ruta_Archivo, FileMode.Create, FileAccess.Write);
            BinaryWriter Datos_Archivo = new BinaryWriter(Escribir_Archivo);
            Datos_Archivo.Write(P_Dicc[P_Checksum]);
            // Guardar archivo (escribir datos en el filestream)                            
            //Cerrar Objetos
            Escribir_Archivo.Flush();
            //Escribir_Archivo.Dispose();
            Datos_Archivo.Flush();
            Datos_Archivo.Close();
            Escribir_Archivo.Close();
        }
        catch (Exception Ex)
        {
            throw new Exception("Guardar_Archivos " + Ex.Message.ToString(), Ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:Obtener_Diccionario_Archivos
    ///DESCRIPCIÓN:         Obtener_Diccionario_Archivos
    ///PROPIEDADES:     
    ///CREO:                Jesus Toledo
    ///FECHA_CREO:          23/Mayo/2011 
    ///MODIFICO:              
    ///FECHA_MODIFICO:        
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private Dictionary<String, Byte[]> Obtener_Diccionario_Archivos()
    {
        Dictionary<String, Byte[]> Diccionario_Archivos = new Dictionary<String, Byte[]>();

        // si existe el diccionario en variable de sesion
        if (Session["Diccionario_Archivos"] != null)
        {
            Diccionario_Archivos = (Dictionary<String, Byte[]>)Session["Diccionario_Archivos"];
        }

        return Diccionario_Archivos;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:Btn_Guardar_Click
    ///DESCRIPCIÓN:         Btn_Guardar_Click
    ///PROPIEDADES:     
    ///CREO:                Jesus Toledo
    ///FECHA_CREO:          23/Mayo/2011 
    ///MODIFICO:              
    ///FECHA_MODIFICO:        
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Guardar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Guardar_Movimientos();
        }
        catch (Exception Ex)
        {

        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:Btn_Limpiar_Formulario_Click
    ///DESCRIPCIÓN:         Btn_Limpiar_Formulario_Click
    ///PROPIEDADES:     
    ///CREO:                Jesus Toledo
    ///FECHA_CREO:          23/Mayo/2011 
    ///MODIFICO:              
    ///FECHA_MODIFICO:        
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Limpiar_Formulario_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("Frm_Migr_Mov_Tarjetas.aspx");
    }
}
