﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Catalogo_Taller_Servicios_Preventivos.Negocio;
using JAPAMI.Catalogo_Taller_Refacciones.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Entradas_Vehiculos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Preventivos.Negocio;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Correctivos.Negocio;
using JAPAMI.Taller_Mecanico.Catalogo_Partes_Vehiculos.Negocio;
using System.Text.RegularExpressions;
using JAPAMI.Taller_Mecanico.Operacion_Proveedor_Entrada_Salida.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Asignacion_Proveedor.Negocio;
using JAPAMI.Catalogo_Compras_Proveedores.Negocio;

public partial class paginas_Taller_Mecanico_Frm_Ope_Tal_Entrada_Vehiculo_Proveedor : System.Web.UI.Page {

    #region Page_Load

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN: Carga la Pagina Inicial
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 28/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e) {
            Div_Contenedor_Msj_Error.Visible = false;
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack) {
                Llenar_Combo_Unidades_Responsables();
                Llenar_Listado_Servicios();
                Configuracion_Formulario("INICIAL");
                Div_Listado_Detalles.Visible = false;
                Btn_Mostrar_Detalle_Salida.Visible = false;
                //Llenar_Grid_Listado_Detalles();
                //Llenar_Grid_Listado_Detalles_Salida();
            }
        }

    #endregion

    #region Metodos

        #region Llenado de Campos, Combos y Listados

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Servicios
            ///DESCRIPCIÓN: Se llena el Listado de los Servicios para asignar el Mecanico.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 28/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Listado_Servicios() {
                Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio Cls_Negocio = new Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio();
                DataTable Dt_Resultados = Cls_Negocio.Consultar_Asignaciones_Servicios();
                Grid_Listado_Servicios.Columns[1].Visible = true;
                Grid_Listado_Servicios.Columns[2].Visible = true;
                Grid_Listado_Servicios.Columns[3].Visible = true;
                Grid_Listado_Servicios.DataSource = Dt_Resultados;
                Grid_Listado_Servicios.DataBind();
                Grid_Listado_Servicios.Columns[1].Visible = false;
                Grid_Listado_Servicios.Columns[2].Visible = false;
                Grid_Listado_Servicios.Columns[3].Visible = false;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Unidades_Responsables
            ///DESCRIPCIÓN: Se llena el Combo de las Unidades Responsables.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 28/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Combo_Unidades_Responsables() {
                Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                Negocio.P_Estatus = "ACTIVO";
                DataTable Dt_Dependencias = Negocio.Consultar_Unidades_Responsables();
                Cmb_Unidad_Responsable.DataSource = Dt_Dependencias;
                Cmb_Unidad_Responsable.DataTextField = "CLAVE_NOMBRE";
                Cmb_Unidad_Responsable.DataValueField = "DEPENDENCIA_ID";
                Cmb_Unidad_Responsable.DataBind();
                Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Listado_Detalles
            ///DESCRIPCIÓN: Llena la Tabla con los detalles de las Partes a Revisar
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 28/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Grid_Listado_Detalles() {
                Grid_Listado_Detalles.Columns[0].Visible = true;
                Grid_Listado_Detalles.Columns[1].Visible = true;
                Grid_Listado_Detalles.Columns[2].Visible = true;
                Grid_Listado_Detalles.DataSource = Crear_Tabla_Detalles();
                Grid_Listado_Detalles.DataBind();
                Agrupar_Partes_Listado_Detalles(ref Grid_Listado_Detalles);
                Grid_Listado_Detalles.Columns[0].Visible = false;
                Grid_Listado_Detalles.Columns[1].Visible = false;
                Grid_Listado_Detalles.Columns[2].Visible = false;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Listado_Detalles_Salida
            ///DESCRIPCIÓN: Llena la Tabla con los detalles de las Partes a Revisar
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 01/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Grid_Listado_Detalles_Salida(){
                Grid_Listado_Detalles_Salida.Columns[0].Visible = true;
                Grid_Listado_Detalles_Salida.Columns[1].Visible = true;
                Grid_Listado_Detalles_Salida.Columns[2].Visible = true;
                Grid_Listado_Detalles_Salida.DataSource = Crear_Tabla_Detalles();
                Grid_Listado_Detalles_Salida.DataBind();
                Agrupar_Partes_Listado_Detalles(ref Grid_Listado_Detalles_Salida);
                Grid_Listado_Detalles_Salida.Columns[0].Visible = false;
                Grid_Listado_Detalles_Salida.Columns[1].Visible = false;
                Grid_Listado_Detalles_Salida.Columns[2].Visible = false;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Crear_Tabla_Detalles
            ///DESCRIPCIÓN: Se crea la Tabla DataTable de los Detalles
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 28/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private DataTable Crear_Tabla_Detalles() {
                DataTable Dt_Detalles = new DataTable();
                Dt_Detalles.Columns.Add("PARTE_ID", Type.GetType("System.String"));
                Dt_Detalles.Columns.Add("SUBPARTE_ID", Type.GetType("System.String"));
                Dt_Detalles.Columns.Add("CANT_UNIFICAR", Type.GetType("System.Int32"));
                Dt_Detalles.Columns.Add("NOMBRE_PARTE", Type.GetType("System.String"));
                Dt_Detalles.Columns.Add("NOMBRE_SUBPARTE", Type.GetType("System.String"));
                Dt_Detalles.Columns.Add("VALOR", Type.GetType("System.String"));

                Cls_Cat_Tal_Partes_Vehiculos_Negocio Negocio = new Cls_Cat_Tal_Partes_Vehiculos_Negocio();
                Negocio.P_Estatus = "VIGENTE";
                Negocio.P_Tipo = "PARTE";
                DataTable Dt_Parents = Negocio.Consultar_Partes();
                foreach (DataRow Fila_Parent in Dt_Parents.Rows) {
                    String Parte_ID = Fila_Parent["PARTE_ID"].ToString();
                    String Nombre_Parte = Fila_Parent["NOMBRE"].ToString();
                    Cls_Cat_Tal_Partes_Vehiculos_Negocio Tmp = new Cls_Cat_Tal_Partes_Vehiculos_Negocio();
                    Tmp.P_Estatus = "VIGENTE";
                    Tmp.P_Tipo = "SUBPARTE";
                    Tmp.P_Parent = Parte_ID;
                    DataTable Dt_Childs = Tmp.Consultar_Partes();
                    Boolean Cant_Unificar = true;
                    foreach (DataRow Fila_Child in Dt_Childs.Rows) {
                        DataRow Fila_Nueva = Dt_Detalles.NewRow();
                        Fila_Nueva["PARTE_ID"] = Parte_ID;
                        Fila_Nueva["SUBPARTE_ID"] = Fila_Child["PARTE_ID"].ToString();
                        if (Cant_Unificar) {
                            Fila_Nueva["CANT_UNIFICAR"] = Dt_Childs.Rows.Count;
                            Cant_Unificar = false;
                        }
                        Fila_Nueva["NOMBRE_PARTE"] = Nombre_Parte;
                        Fila_Nueva["NOMBRE_SUBPARTE"] = Fila_Child["NOMBRE"].ToString();
                        Fila_Nueva["VALOR"] = "---";
                        Dt_Detalles.Rows.Add(Fila_Nueva);
                    }
                }
                return Dt_Detalles;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Agrupar_Partes_Listado_Detalles
            ///DESCRIPCIÓN: Unifica las Celdas
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 28/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Agrupar_Partes_Listado_Detalles(ref GridView Grid_Listado) {
                Boolean Primer_Color = true;
                foreach (GridViewRow Fila_Actual in Grid_Listado.Rows) { 
                    if (!String.IsNullOrEmpty(HttpUtility.HtmlDecode(Fila_Actual.Cells[2].Text).Trim())) {
                        Int32 Cant_Unificar = Convert.ToInt32(HttpUtility.HtmlDecode(Fila_Actual.Cells[2].Text).Trim());
                        if (Cant_Unificar > 0) {
                            TableCell Celda = Fila_Actual.Cells[3];
                            Celda.RowSpan = Cant_Unificar;
                            if (Primer_Color) { Celda.BackColor = System.Drawing.Color.White; Primer_Color = false; }
                            else { Celda.BackColor = System.Drawing.Color.FromArgb(230, 230, 230); Primer_Color = true; }
                            Celda.BorderColor = System.Drawing.Color.FromArgb(47, 78, 125);
                            Celda.BorderWidth = Convert.ToInt32(2);
                        } else {
                            TableCell Celda = Fila_Actual.Cells[3];
                            Celda.Visible = false;
                        } 
                        Fila_Actual.BorderColor = System.Drawing.Color.FromArgb(47, 78, 125);
                        Fila_Actual.BorderWidth = Convert.ToInt32(1);
                    }
                }
            }

        #endregion

        #region Generales

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
            ///DESCRIPCIÓN: Se Habilitan y/o inhabilitan los campos dependendiendo de la Condicion.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 28/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Configuracion_Formulario(String Operacion) {
                switch (Operacion) {
                    case "INICIAL":
                        Div_Campos.Visible = false;
                        Div_Listado_Servicios.Visible = true;
                        Btn_Ejecutar_Entrada_Vehiculo.Visible = false;
                        break;
                    case "OPERACION":
                        Div_Campos.Visible = true;
                        Div_Listado_Servicios.Visible = false;
                        Btn_Ejecutar_Entrada_Vehiculo.Visible = true;
                        break;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
            ///DESCRIPCIÓN: Limpia los campos del Formulario.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 28/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Limpiar_Formulario() {
                Hdf_No_Servicio.Value = "";
                Hdf_No_Solicitud.Value = "";
                Hdf_No_Asignacion.Value = "";
                Hdf_Estatus_Asignacion_Proveedor.Value = "";
                Hdf_No_Registro.Value = "";
                Hdf_Proveedor_ID.Value = "";
                Txt_Folio.Text = "";
                Txt_Fecha_Elaboracion.Text = "";
                Txt_Fecha_Recepcion.Text = "";
                Txt_Kilometraje.Text = "";
                Cmb_Tipo_Servicio.SelectedIndex = 0;
                Cmb_Unidad_Responsable.SelectedIndex = 0;
                Txt_Descripcion_Servicio.Text = "";
                Hdf_Vehiculo_ID.Value = "";
                Txt_No_Inventario.Text = "";
                Txt_No_Economico.Text = "";
                Txt_Datos_Vehiculo.Text = "";
                Txt_Placas.Text = "";
                Txt_Anio.Text = "";
                Txt_Nombre_Proveedor.Text = "";
                Txt_Diagnostico_Servicio.Text = "";
                Txt_Kilometraje_Salida.Text = "";
                Txt_Fecha_Salida_Proveedor.Text = "";
                Txt_Comentarios_Salida.Text = "";
                Limpiar_Grid_Detalles();
                Llenar_Grid_Listado_Detalles_Salida();
            }
    
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Limpiar_Grid_Detalles
            ///DESCRIPCIÓN: Limpia el Grid de Detalles.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 28/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Limpiar_Grid_Detalles() {
                foreach (GridViewRow Fila_Actual in Grid_Listado_Detalles.Rows) {
                    if (Fila_Actual.FindControl("RBtn_SI") != null) {
                        RadioButton RBtn_SI = (RadioButton)Fila_Actual.FindControl("RBtn_SI");
                        RBtn_SI.Checked = true;
                    }
                    if (Fila_Actual.FindControl("RBtn_NO") != null) {
                        RadioButton RBtn_NO = (RadioButton)Fila_Actual.FindControl("RBtn_NO");
                        RBtn_NO.Checked = false;
                    }
                }
            }

        #endregion

        #region Interaccion Clase de Negocio

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Mostrar_Registro
            ///DESCRIPCIÓN: Muestra el Registro en los campos.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 28/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Mostrar_Registro() {
                Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                Solicitud.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                Solicitud = Solicitud.Consultar_Detalles_Solicitud_Servicio();
                if (Solicitud.P_No_Solicitud > (-1)) {
                    Txt_Folio.Text = Solicitud.P_Folio_Solicitud.Trim();
                    Txt_Fecha_Elaboracion.Text = String.Format("{0:dd/MMM/yyyy}", Solicitud.P_Fecha_Elaboracion);
                    Txt_Fecha_Recepcion.Text = String.Format("{0:dd/MMM/yyyy}", Solicitud.P_Fecha_Recepcion_Real);
                    Cmb_Tipo_Servicio.SelectedIndex = Cmb_Tipo_Servicio.Items.IndexOf(Cmb_Tipo_Servicio.Items.FindByValue(Solicitud.P_Tipo_Servicio));
                    Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Solicitud.P_Dependencia_ID));
                    Hdf_Vehiculo_ID.Value = Solicitud.P_Bien_ID;
                    Txt_Descripcion_Servicio.Text = Solicitud.P_Descripcion_Servicio.ToUpper();
                    if (Solicitud.P_Tipo_Bien.Equals("BIEN_MUEBLE"))
                    {
                        Pnl_Bien_Mueble_Seleccionado.Visible = true;
                        Pnl_Vehiculo_Seleccionado.Visible = false;
                        Hdf_Bien_Mueble_ID.Value = Solicitud.P_Bien_ID;
                        Hdf_Tipo_Bien.Value = "BIEN_MUEBLE";
                        Cargar_Datos_Bien_Mueble(Hdf_Bien_Mueble_ID.Value, "IDENTIFICADOR");
                    }
                    else if (Solicitud.P_Tipo_Bien.Equals("VEHICULO"))
                    {
                        Pnl_Vehiculo_Seleccionado.Visible = true;
                        Pnl_Bien_Mueble_Seleccionado.Visible = false;
                        Hdf_Vehiculo_ID.Value = Solicitud.P_Bien_ID;
                        Hdf_Tipo_Bien.Value = "VEHICULO";
                        Cargar_Datos_Vehiculo(Hdf_Vehiculo_ID.Value, "IDENTIFICADOR");
                    }
                    if (Solicitud.P_Kilometraje > -1) Txt_Kilometraje.Text = String.Format("{0:######0}", Solicitud.P_Kilometraje);
                }
                Mostrar_Registro_Servicio();
                Mostrar_Proveedor_Seleccionado();
                Mostrar_Datos_Salida_Proveedor();
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Mostrar_Registro_Servicio
            ///DESCRIPCIÓN: Muestra el Registro en los campos.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 28/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Mostrar_Registro_Servicio() {
                if (Cmb_Tipo_Servicio.SelectedItem.Value.Trim().Equals("SERVICIO_PREVENTIVO")) {
                    Cls_Ope_Tal_Servicios_Preventivos_Negocio Serv_Negocio = new Cls_Ope_Tal_Servicios_Preventivos_Negocio();
                    Serv_Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value.Trim());
                    Serv_Negocio = Serv_Negocio.Consultar_Detalles_Servicio_Preventivo();
                    Txt_Diagnostico_Servicio.Text = Serv_Negocio.P_Diagnostico.Trim();
                } else if (Cmb_Tipo_Servicio.SelectedItem.Value.Trim().Equals("SERVICIO_CORRECTIVO")) {
                    Cls_Ope_Tal_Servicios_Correctivos_Negocio Serv_Negocio = new Cls_Ope_Tal_Servicios_Correctivos_Negocio();
                    Serv_Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value.Trim());
                    Serv_Negocio = Serv_Negocio.Consultar_Detalles_Servicio_Correctivo();
                    Txt_Diagnostico_Servicio.Text = Serv_Negocio.P_Diagnostico.Trim();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Vehiculo
            ///DESCRIPCIÓN: Se cargan los Datos del Vehiculo Seleccionado.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Cargar_Datos_Vehiculo(String Vehiculo, String Tipo_Busqueda) {
                Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                switch (Tipo_Busqueda) {
                    case "NO_INVENTARIO":
                        Consulta_Negocio.P_No_Inventario = Vehiculo;
                        break;
                    case "IDENTIFICADOR":
                        Consulta_Negocio.P_Vehiculo_ID = Vehiculo;
                        break;
                    default: break;
                }
                if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Consulta_Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value; }
                DataTable Dt_Vehiculo = Consulta_Negocio.Consultar_Vehiculos();
                if (Dt_Vehiculo.Rows.Count > 0) {
                    Hdf_Vehiculo_ID.Value = Dt_Vehiculo.Rows[0]["VEHICULO_ID"].ToString().Trim();
                    Txt_No_Inventario.Text = Dt_Vehiculo.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                    Txt_No_Economico.Text = Dt_Vehiculo.Rows[0]["NO_ECONOMICO"].ToString().Trim();
                    Txt_Datos_Vehiculo.Text = Dt_Vehiculo.Rows[0]["VEHICULO_DESCRIPCION"].ToString().Trim();
                    Txt_Placas.Text = Dt_Vehiculo.Rows[0]["PLACAS"].ToString().Trim();
                    Txt_Anio.Text = Dt_Vehiculo.Rows[0]["ANIO"].ToString().Trim();
                    Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString()));
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Bien_Mueble
            ///DESCRIPCIÓN: Se cargan los Datos del Bien Mueble Seleccionado.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 04/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Cargar_Datos_Bien_Mueble(String Bien_Mueble, String Tipo_Busqueda)
            {
                Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                switch (Tipo_Busqueda)
                {
                    case "NO_INVENTARIO":
                        Consulta_Negocio.P_No_Inventario = Bien_Mueble;
                        break;
                    case "IDENTIFICADOR":
                        Consulta_Negocio.P_Bien_Mueble_ID = Bien_Mueble;
                        break;
                    default: break;
                }
                DataTable Dt_Bienes_Muebles = Consulta_Negocio.Consultar_Bien_Mueble();
                if (Dt_Bienes_Muebles.Rows.Count > 0)
                {
                    Hdf_Bien_Mueble_ID.Value = Dt_Bienes_Muebles.Rows[0]["BIEN_MUEBLE_ID"].ToString().Trim();
                    Txt_No_Inventario_BM.Text = Dt_Bienes_Muebles.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                    Txt_Numero_Serie_Bien.Text = Dt_Bienes_Muebles.Rows[0]["NUMERO_SERIE"].ToString().Trim();
                    Txt_Descripcion_Bien.Text = Dt_Bienes_Muebles.Rows[0]["DESCRIPCION_BIEN_MUEBLE"].ToString().Trim();
                    if (!String.IsNullOrEmpty(Dt_Bienes_Muebles.Rows[0]["DEPENDENCIA_ID"].ToString()))
                    {
                        Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Bienes_Muebles.Rows[0]["DEPENDENCIA_ID"].ToString()));
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Text = "ERROR [Puede ser que el Bien esté dado de Baja].";
                        Cmb_Unidad_Responsable.SelectedIndex = 0;
                    }
                }
                else
                {
                    Lbl_Ecabezado_Mensaje.Text = "El Bien no se encontro.";
                    if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Lbl_Mensaje_Error.Text = "[Puede ser que no este asignado a la Unidad Responsable Seleccionada]."; }
                    else { Lbl_Mensaje_Error.Text = ""; }
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Mostrar_Proveedor_Seleccionado
            ///DESCRIPCIÓN: Carga el Dato del Proveedor.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 28/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Mostrar_Proveedor_Seleccionado() {
                Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio Clase_Negocio = new Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio();
                Clase_Negocio.P_No_Asignacion_Proveedor = Convert.ToInt32(Hdf_No_Asignacion.Value);
                Clase_Negocio = Clase_Negocio.Consultar_Detalles_Asignacion_Servicio();
                Hdf_Proveedor_ID.Value=Clase_Negocio.P_Proveedor_ID;
                Hdf_Estatus_Asignacion_Proveedor.Value = Clase_Negocio.P_Estatus;
                if (Clase_Negocio.P_No_Asignacion_Proveedor > (-1.0)) {
                    Cls_Cat_Com_Proveedores_Negocio Prov_Negocio = new Cls_Cat_Com_Proveedores_Negocio();
                    Prov_Negocio.P_Proveedor_ID = Clase_Negocio.P_Proveedor_ID;
                    DataTable Dt_Resultados = Prov_Negocio.Consulta_Datos_Proveedores();
                    if (Dt_Resultados != null && Dt_Resultados.Rows.Count > 0) {
                        Txt_Nombre_Proveedor.Text = Dt_Resultados.Rows[0][Cat_Com_Proveedores.Campo_Nombre].ToString().Trim();
                    }
                }
            }
                
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Mostrar_Proveedor_Seleccionado
            ///DESCRIPCIÓN: Carga el Dato del Proveedor.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 28/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Mostrar_Datos_Salida_Proveedor() {
                Cls_Ope_Tal_Proveedor_Entrada_Salida_Negocio Clase_Negocio = new Cls_Ope_Tal_Proveedor_Entrada_Salida_Negocio();
                Clase_Negocio.P_No_Asignacion = Convert.ToInt32(Hdf_No_Asignacion.Value);
                Clase_Negocio = Clase_Negocio.Consultar_Detalles_Proveedor_Entrada_Salida();
                if (Clase_Negocio.P_No_Registro > (-1)) {
                    Hdf_No_Registro.Value = Clase_Negocio.P_No_Registro.ToString();
                    if (Clase_Negocio.P_Kilometraje_Salida > -1) { Txt_Kilometraje_Salida.Text = String.Format("{0:######0}", Clase_Negocio.P_Kilometraje_Salida); Txt_Kilometraje_Entrada.Text = String.Format("{0:######0}", Clase_Negocio.P_Kilometraje_Salida); }
                    Txt_Fecha_Salida_Proveedor.Text = String.Format("{0:dd/MMM/yyyy}", Clase_Negocio.P_Fecha_Salida);
                    Txt_Comentarios_Salida.Text = Clase_Negocio.P_Comentarios_Salida.Trim();
                    Cargar_Grid_Detalles_Entrada(Clase_Negocio.P_Dt_Detalles_Salida);
                    Cargar_Grid_Detalles_Salida(Clase_Negocio.P_Dt_Detalles_Salida);                
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cargar_Grid_Detalles_Entrada
            ///DESCRIPCIÓN: Carga el Grid de los detalles con los predeterminados de Entrada
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 28/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Cargar_Grid_Detalles_Entrada(DataTable Dt_Detalles) {
                foreach (GridViewRow Fila_Grid in Grid_Listado_Detalles.Rows) {
                    RadioButton RBtn_SI = null;
                    if (Fila_Grid.FindControl("RBtn_SI") != null) {
                        RBtn_SI = (RadioButton)Fila_Grid.FindControl("RBtn_SI");
                    }
                    RadioButton RBtn_NO = null;
                    if (Fila_Grid.FindControl("RBtn_NO") != null) {
                        RBtn_NO = (RadioButton)Fila_Grid.FindControl("RBtn_NO");
                    }
                    String SubParte = HttpUtility.HtmlDecode(Fila_Grid.Cells[1].Text.Trim());
                    DataRow[] Filas = Dt_Detalles.Select("SUBPARTE_ID = '" + SubParte.Trim() + "'");
                    if (Filas.Length > 0) {
                        if (Filas[0]["VALOR"].ToString().Trim().Equals("SI")) {
                            RBtn_NO.Checked = false;
                            RBtn_SI.Checked = true;
                        } else if (Filas[0]["VALOR"].ToString().Trim().Equals("NO")) {
                            RBtn_NO.Checked = true;
                            RBtn_SI.Checked = false;
                        }
                    }
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cargar_Grid_Detalles_Salida
            ///DESCRIPCIÓN: Carga el Grid de los detalles con los predeterminados de Entrada
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 01/Junio/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Cargar_Grid_Detalles_Salida(DataTable Dt_Detalles) {
                foreach (GridViewRow Fila_Grid in Grid_Listado_Detalles_Salida.Rows) {
                    String Valor = "---";
                    String SubParte = HttpUtility.HtmlDecode(Fila_Grid.Cells[1].Text.Trim());
                    DataRow[] Filas = Dt_Detalles.Select("SUBPARTE_ID = '" + SubParte.Trim() + "'");
                    if (Filas.Length > 0) {
                        Valor = Filas[0]["VALOR"].ToString().Trim();
                    }
                    Fila_Grid.Cells[5].Text = Valor.Trim();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Alta_Registro_Entrada
            ///DESCRIPCIÓN: Da de alta el registro de la Entrada
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 29/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Alta_Registro_Entrada() {
                Cls_Ope_Tal_Proveedor_Entrada_Salida_Negocio Clase_Negocio = new Cls_Ope_Tal_Proveedor_Entrada_Salida_Negocio();
                Clase_Negocio.P_No_Asignacion = Convert.ToInt32(Hdf_No_Asignacion.Value);
                Clase_Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value);
                Clase_Negocio.P_No_Registro = Convert.ToInt32(Hdf_No_Registro.Value);
                Clase_Negocio.P_Fecha_Entrada = Convert.ToDateTime(Txt_Fecha_Entrada_Proveedor.Text);
                if (Txt_Kilometraje_Entrada.Text.Trim().Length > (0)) Clase_Negocio.P_Kilometraje_Entrada = Convert.ToDouble(Txt_Kilometraje_Entrada.Text);
                Clase_Negocio.P_Comentarios_Entrada = Txt_Comentarios_Entrada.Text.Trim();
                Clase_Negocio.P_Estatus = "CERRADA";
                if (Hdf_Estatus_Asignacion_Proveedor.Value.Trim().Equals("RECHAZADO")) {
                    Clase_Negocio.P_Estatus_Servicio = "REPARACION";
                } else if (Hdf_Estatus_Asignacion_Proveedor.Value.Trim().Equals("ACEPTADO")) {
                    Clase_Negocio.P_Estatus_Servicio = "REPARADO";
                }
                Clase_Negocio.P_Tipo = "ENTRADA";
                Clase_Negocio.P_Tipo_Bien = Hdf_Tipo_Bien.Value.Trim();
                Clase_Negocio.P_Tipo_Servicio = Cmb_Tipo_Servicio.SelectedItem.Value.Trim();
                Clase_Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                Clase_Negocio.Alta_Registro_Entrada();
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Obtener_Datos_Detalles
            ///DESCRIPCIÓN: Regresa un DataTable Con los Datos de los Detalles.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 28/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private DataTable Obtener_Datos_Detalles() {
                DataTable Dt_Detalles = new DataTable();
                Dt_Detalles.Columns.Add("PARTE_ID", Type.GetType("System.String"));
                Dt_Detalles.Columns.Add("SUBPARTE_ID", Type.GetType("System.String"));
                Dt_Detalles.Columns.Add("VALOR", Type.GetType("System.String"));
                foreach (GridViewRow Fila_Actual in Grid_Listado_Detalles.Rows) {
                    DataRow Nueva_Fila = Dt_Detalles.NewRow();
                    Nueva_Fila["PARTE_ID"] = HttpUtility.HtmlDecode(Fila_Actual.Cells[0].Text).Trim();
                    Nueva_Fila["SUBPARTE_ID"] = HttpUtility.HtmlDecode(Fila_Actual.Cells[1].Text).Trim();
                    if (Fila_Actual.FindControl("RBtn_SI") != null && Fila_Actual.FindControl("RBtn_NO") != null) {
                        RadioButton RBtn_SI = (RadioButton)Fila_Actual.FindControl("RBtn_SI");
                        RadioButton RBtn_NO = (RadioButton)Fila_Actual.FindControl("RBtn_NO");
                        if (RBtn_SI.Checked) Nueva_Fila["VALOR"] = "SI";
                        else if (RBtn_NO.Checked) Nueva_Fila["VALOR"] = "NO";
                    }
                    Dt_Detalles.Rows.Add(Nueva_Fila);
                }
                return Dt_Detalles;
            }

        #endregion

        #region Validaciones

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Salida
            ///DESCRIPCIÓN: Valida la Salida del Vehiculo.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 28/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private Boolean Validar_Entrada() {
                String Mensaje_Error_ = "";
                Boolean Validacion = true;
                if (Hdf_No_Servicio.Value.Trim().Length == 0) {
                    Mensaje_Error_ = Mensaje_Error_ + "+ Seleccionar el Servicio que se realizará.";
                    Mensaje_Error_ = Mensaje_Error_ + " <br />";
                    Validacion = false;
                }
                if (Hdf_No_Asignacion.Value.Trim().Length == 0) {
                    Mensaje_Error_ = Mensaje_Error_ + "+ No hay Proveedor Seleccionado.";
                    Mensaje_Error_ = Mensaje_Error_ + " <br />";
                    Validacion = false;
                }
                if (String.IsNullOrEmpty(Txt_Fecha_Entrada_Proveedor.Text.Trim())) {
                    Mensaje_Error_ = Mensaje_Error_ + "+ Seleccionar la Fecha de Salida.";
                    Mensaje_Error_ = Mensaje_Error_ + " <br />";
                    Validacion = false;
                    
                }
                if (!String.IsNullOrEmpty(Txt_Kilometraje_Entrada.Text.Trim())) {
                    if (!Validar_Valores_Decimales(Txt_Kilometraje_Entrada.Text.Trim())) {
                        Mensaje_Error_ = Mensaje_Error_ + "+ El Formato del Kilometraje no es Correcto [Correcto: '12345', '12353.0' ó '12254.33'].";
                        Mensaje_Error_ = Mensaje_Error_ + " <br />";
                        Validacion = false;
                    }
                }
                
                if (!Validacion) {
                    Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error_);
                    Div_Contenedor_Msj_Error.Visible = true;
                }
                //else {
                //    Validacion = Validar_Detalles();
                //}
                return Validacion;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Valores_Decimales
            ///DESCRIPCIÓN: Valida los valores decimales para un Anexo.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO: 28/mayo/2012 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private Boolean Validar_Valores_Decimales(String Valor) {
                Boolean Validacion = true;
                Regex Expresion_Regular = new Regex(@"^[0-9]{1,50}(\.[0-9]{0,2})?$");
                Validacion = Expresion_Regular.IsMatch(Valor);
                return Validacion;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Detalles
            ///DESCRIPCIÓN: Valida los Detalles Completos
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 28/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private Boolean Validar_Detalles() { 
                Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                String Mensaje_Error = "";
                Boolean Validacion = true;
                foreach (GridViewRow Fila_Actual in Grid_Listado_Detalles.Rows) { 
                    if (Fila_Actual.FindControl("RBtn_SI") != null && Fila_Actual.FindControl("RBtn_NO") != null) {
                        RadioButton RBtn_SI = (RadioButton)Fila_Actual.FindControl("RBtn_SI");
                        RadioButton RBtn_NO = (RadioButton)Fila_Actual.FindControl("RBtn_NO");
                        if (!(RBtn_SI.Checked || RBtn_NO.Checked)) {
                            Mensaje_Error = Mensaje_Error + "+ No se ha seleccionado [Si/No] dentro de la Parte " + HttpUtility.HtmlDecode(Fila_Actual.Cells[3].Text).Trim() + " en la SubParte " + HttpUtility.HtmlDecode(Fila_Actual.Cells[4].Text).Trim() + ".";
                            Mensaje_Error = Mensaje_Error + " <br />";
                            Validacion = false;
                            break;
                        }
                    }
                }
                if (!Validacion) {
                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
                    Div_Contenedor_Msj_Error.Visible = true;
                }
                return Validacion;
            }

        #endregion
        
    #endregion

    #region Grids
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Servicios_PageIndexChanging
        ///DESCRIPCIÓN: Maneja la paginación del Listado
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 28/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Servicios_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            try {
                Grid_Listado_Servicios.SelectedIndex = (-1);
                Grid_Listado_Servicios.PageIndex = e.NewPageIndex;
                Llenar_Listado_Servicios();
            } catch(Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Servicios_SelectedIndexChanged
        ///DESCRIPCIÓN: Obtiene los datos de un Servicio 
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 28/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Servicios_SelectedIndexChanged(object sender, EventArgs e) {
            try{
                if (Grid_Listado_Servicios.SelectedIndex > (-1)){
                    Limpiar_Formulario();
                    Hdf_No_Asignacion.Value = HttpUtility.HtmlDecode(Grid_Listado_Servicios.SelectedRow.Cells[1].Text.Trim()).Trim();
                    Hdf_No_Servicio.Value = HttpUtility.HtmlDecode(Grid_Listado_Servicios.SelectedRow.Cells[2].Text.Trim()).Trim();
                    Hdf_No_Solicitud.Value = HttpUtility.HtmlDecode(Grid_Listado_Servicios.SelectedRow.Cells[3].Text.Trim()).Trim();
                    Mostrar_Registro();
                    Txt_Fecha_Entrada_Proveedor.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Today);
                    Configuracion_Formulario("OPERACION");
                    Grid_Listado_Servicios.SelectedIndex = -1;
                    System.Threading.Thread.Sleep(500);
                }
            } catch(Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }
 
    #endregion

    #region Eventos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Ejecutar_Salida_Vehiculo_Click
        ///DESCRIPCIÓN: Controla las operaciones del Boton de Ejecutar Salida
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 29/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Ejecutar_Entrada_Vehiculo_Click(object sender, ImageClickEventArgs e) {
            if (Validar_Entrada()) {
                Alta_Registro_Entrada();
                Llenar_Listado_Servicios();
                Configuracion_Formulario("INICIAL");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "GACO", "alert('Operacion Exitosa: Entrada de Unidad de Proveedor');", true);
                Limpiar_Formulario();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Controla las operaciones del Boton de Salir
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 28/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e) {
            if (Div_Campos.Visible) {
               Limpiar_Formulario();
               Configuracion_Formulario("INICIAL");
            } else {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Mostrar_Detalle_Salida_Click
        ///DESCRIPCIÓN: Muestra los Detalles de la Salida
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 1/Junio/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Mostrar_Detalle_Salida_Click(object sender, EventArgs e) {
            MPE_Listado_Detalles.Show();
        }

    #endregion

}
