<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Ope_Tal_Entrada_Facturas.aspx.cs" Inherits="paginas_Taller_Mecanico_Frm_Ope_Tal_Entrada_Facturas"
    EnableEventValidation="false" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">

    <script type="text/javascript">
        function EnviarPostbackConParametros(args) {
            __doPostBack("IdPostBack", args);
        }
        function fillCell(row, cellNumber, text) {
            var cell = row.insertCell(cellNumber);
            cell.innerHTML = text;
            cell.style.borderBottom = cell.style.borderRight = "solid 1px #aaaaff";
        }
        function addToClientTable(name, text) {
            var table = document.getElementById("<%= clientSide.ClientID %>");
            var row = table.insertRow(0);
            fillCell(row, 0, name + text);
        }

        function uploadError(sender, args) {
            addToClientTable(args.get_fileName(), "<span style='color:red;'>" + args.get_errorMessage() + "</span>");
        }
        function uploadComplete(sender, args) {
            var contentType = args.get_contentType();
            var text = args.get_length() + " bytes";
            if (contentType.length > 0) {
                text += ", '" + contentType + "'";
            }
            addToClientTable(args.get_fileName(), text);
        }
        function recarga() {
            window.opener.location.reload();
        }
        function Abrir_Carga_PopUp() {
            $find('Archivos_Facturas').show();
            return false;
        }
        function Cerrar_Carga_PopUp() {
            $find('Archivos_Facturas').hide();
            return false;
        }
    </script>

    <script type="text/javascript">
        window.onerror = new Function("return true");
        //Abrir una ventana modal
        function Abrir_Ventana_Modal(Url, Propiedades) {
            window.showModalDialog(Url, null, Propiedades);
        }
        function Validar_Longitud_Texto(Text_Box, Max_Longitud) {
            if (Text_Box.value.length > Max_Longitud) {
                Text_Box.value = Text_Box.value.substring(0, Max_Longitud);
            }
        }
        function Validar_Montos_Facturas() {
            var Monto_Total_Factura = 0;
            var Monto_Acumulado = 0;
            var Monto_Diferencia = 0;
            var Monto_Caja = 0;
            Monto_Total_Factura = $("#<%=Txt_Costo_Unitario.ClientID %>").val();
            Monto_Caja = $("#<%=Txt_Monto.ClientID %>").val();
            var table = document.getElementById('<%=Grid_Facturas.ClientID %>');
            if (table != null) {
                for (var i = 1; i < table.rows.length; i++) {
                    var Row = table.rows[i];
                    var CellValue = 0;
                    if (Row.cells[1].children[0].value != "")
                        CellValue = Row.cells[1].children[0].value;
                    Monto_Acumulado += parseFloat(CellValue);
                }
            }
            else {
                Monto_Acumulado += parseFloat(Monto_Caja);
            }
            Monto_Diferencia = parseFloat(Monto_Total_Factura) - parseFloat(Monto_Acumulado);
            $("#<%=Txt_Monto.ClientID %>").val(formatCurrency(Monto_Diferencia));
        }
    </script>

    <script type="text/javascript" language="javascript">
    //El nombre del controlador que mantiene la sesi�n
        var CONTROLADOR = "../../Mantenedor_sesiones.ashx";

        //Ejecuta el script en segundo plano evitando as� que caduque la sesi�n de esta p�gina
        function MantenSesion()
        {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para matener la sesi�n activa
        setInterval('MantenSesion()', <%=(int)(0.9*(Session.Timeout * 60000))%>);
        function formatCurrency(num) {
            var Combo = "";
            var Beneficio;
                num = num.toString().replace(/\$|\,/g,'');
                if(isNaN(num))
                num = "0";
                sign = (num == (num = Math.abs(num)));
                num = Math.floor(num*100+0.50000000001);
                cents = num%100;
                num = Math.floor(num/100).toString();
                if(cents<10)
                    cents = "0" + cents;
                    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
                    num = num.substring(0,num.length-(4*i+3))+','+
                    num.substring(num.length-(4*i+3));
                    return (((sign)?'':'-') + num + '.' + cents);
            }
            
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" EnablePartialRendering="true" AsyncPostBackTimeout="9000" />
    <%--Inicio formulario--%>
    <asp:UpdatePanel ID="Upd_Lista_Refacciones" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--update progrees--%>
            <asp:UpdateProgress ID="Uprg_Progress" runat="server" AssociatedUpdatePanelID="Upd_Lista_Refacciones"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Servicios" style="background-color: #ffffff; width: 100%; height: 100%">
                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td class="label_titulo" colspan="2">
                            Entrada de Facturas (Servicios Externos)
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" />
                            <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" CssClass="estilo_fuente_mensaje_error"
                                Text="" /><br />
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
                    <tr class="barra_busqueda">
                        <td align="left" style="width: 20%">
                            <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                CssClass="Img_Button" OnClick="Btn_Modificar_Click" Visible="false" />
                            <asp:ImageButton ID="Btn_Generar_Solicitud_Pago" ToolTip="Cerrar Facturas" runat="server"
                                ImageUrl="~/paginas/imagenes/paginas/sias_aceptarplan.png" CssClass="Img_Button"
                                OnClick="Btn_Generar_Solicitud_Pago_Click" />
                            <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                CssClass="Img_Button" OnClick="Btn_Salir_Click" />
                                <cc1:ModalPopupExtender ID="MPE_Archivos_Facturas" runat="server" BackgroundCssClass="popUpStyle" BehaviorID="Archivos_Facturas" PopupControlID="Pnl_Seleccionar_Archivo" TargetControlID="Btn_Open" CancelControlID="Btn_Cerrar" DropShadow="true" DynamicServicePath="" Enabled="true"></cc1:ModalPopupExtender>
                                <asp:Button ID="Btn_Open" runat="server" Text="" style="background-color: transparent; border-style:none; visibility:hidden" />
                                <asp:Button Style="background-color: transparent; border-style:none; visibility:hidden" ID="Btn_Cerrar" runat="server" Text="" />
                        </td>
                        <td align="right" valign="top" style="width: 80%">
                            <table style="width: 80%;">
                                <tr>
                                    <td style="vertical-align: top; text-align: right; width: 5%">
                                    </td>
                                    <td style="vertical-align: top; text-align: right; width: 90%">
                                        B�squeda:
                                        <asp:TextBox ID="Txt_Buscar" runat="server" MaxLength="100" TabIndex="5" ToolTip="Buscar"
                                            Width="180px" />
                                        <cc1:TextBoxWatermarkExtender ID="WTE_Txt_Buscar" runat="server" WatermarkCssClass="watermarked"
                                            WatermarkText="<No. Servicio>" TargetControlID="Txt_Buscar" />
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Buscar" runat="server" TargetControlID="Txt_Buscar"
                                            FilterType="Numbers" />
                                    </td>
                                    <td style="vertical-align: top; text-align: right; width: 5%">
                                        <asp:ImageButton ID="Btn_Buscar" runat="server" TabIndex="6" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                            ToolTip="Buscar" OnClick="Btn_Buscar_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            &nbsp;
            <br />
            <div id="Div_Listado_Servicios" runat="server" style="width: 100%;">
                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                </table>
                <br />
                <asp:GridView ID="Grid_Listado_Servicios" runat="server" CssClass="GridView_1" AutoGenerateColumns="False"
                    AllowPaging="True" PageSize="20" Width="99%" DataKeyNames="NO_ENTRADA,NO_SERVICIO,NO_SOLICITUD,NO_RESERVA"
                    GridLines="None" EmptyDataText="No se Encontrar�n Servicios Pendientes por Asignar Proveedor"
                    OnPageIndexChanging="Grid_Listado_Servicios_PageIndexChanging" AllowSorting="true"
                    OnSorting="Grid_Listado_Servicios_Sorting" OnSelectedIndexChanged="Grid_Listado_Servicios_SelectedIndexChanged">
                    <RowStyle CssClass="GridItem" />
                    <Columns>
                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                            <ItemStyle Width="30px" />
                        </asp:ButtonField>
                        <asp:BoundField DataField="NO_ENTRADA" HeaderText="NO_ENTRADA" SortExpression="NO_ENTRADA">
                            <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NO_SERVICIO" HeaderText="NO_SERVICIO" SortExpression="NO_SERVICIO">
                            <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NO_SOLICITUD" HeaderText="NO_SOLICITUD" SortExpression="NO_SOLICITUD">
                            <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FOLIO" HeaderText="Folio" SortExpression="FOLIO">
                            <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" Font-Bold="true" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FECHA_RECEPCION" HeaderText="Fecha Recepci�n" SortExpression="FECHA_RECEPCION"
                            DataFormatString="{0:dd/MMM/yyyy}">
                            <ItemStyle Width="140px" HorizontalAlign="Center" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TIPO_SERVICIO" HeaderText="Tipo Servicio" SortExpression="TIPO_SERVICIO">
                            <ItemStyle Width="140px" HorizontalAlign="Center" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DESCRIPCION_SERVICIO" HeaderText="Descripci�n" SortExpression="DESCRIPCION_SERVICIO">
                            <ItemStyle Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" SortExpression="DEPENDENCIA">
                            <ItemStyle Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NO_INVENTARIO" HeaderText="No. Inventario" SortExpression="NO_INVENTARIO">
                            <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NO_ECONOMICO" HeaderText="No. Economico" SortExpression="NO_ECONOMICO">
                            <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ESTATUS" HeaderText="" SortExpression="ESTATUS">
                            <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small" Font-Bold="true" />
                        </asp:BoundField>
                    </Columns>
                    <PagerStyle CssClass="GridHeader" />
                    <SelectedRowStyle CssClass="GridSelected" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAltItem" />
                </asp:GridView>
            </div>
            <div id="Div_Campos" runat="server" style="width: 100%;">
                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td colspan="4">
                            <asp:HiddenField ID="HiddenField1" runat="server" />
                            <asp:HiddenField ID="HiddenField2" runat="server" />
                            <asp:HiddenField ID="HiddenField3" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Folio" runat="server" Text="Folio" ForeColor="Black" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Folio" runat="server" Width="98%" ForeColor="Red" Font-Bold="true"
                                Style="text-align: right;"></asp:TextBox>
                        </td>
                        <td colspan="2">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Fecha_Elaboracion" runat="server" Text="Fecha Elaboraci�n"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Fecha_Elaboracion" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                        </td>
                        <td style="width: 15%;">
                            &nbsp;&nbsp;
                            <asp:Label ID="Lbl_Fecha_Recepcion" runat="server" Text="Fecha Recepci�n"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Fecha_Recepcion" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text="Unidad Responsable"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="Cmb_Unidad_Responsable" runat="server" Width="100%" Enabled="false">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Kilometraje" runat="server" Text="Kilometraje"></asp:Label>
                        </td>
                        <td style="width: 16%;">
                            <asp:TextBox ID="Txt_Kilometraje" runat="server" Width="95%" Enabled="false"></asp:TextBox>
                        </td>
                        <td style="width: 15%;">
                            &nbsp;&nbsp;
                            <asp:Label ID="Lbl_Tipo_Servicio" runat="server" Text="Tipo Servicio"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:DropDownList ID="Cmb_Tipo_Servicio" runat="server" Width="100%" Enabled="false">
                                <asp:ListItem Value="">&lt; - - SELECCIONE - - &gt;</asp:ListItem>
                                <asp:ListItem Value="SERVICIO_CORRECTIVO">SERVICIO CORRECTIVO</asp:ListItem>
                                <asp:ListItem Value="SERVICIO_PREVENTIVO">SERVICIO PREVENTIVO</asp:ListItem>
                                <asp:ListItem Value="VERIFICACION">VERIFICACI&Oacute;N</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Panel ID="Pnl_Vehiculo_Seleccionado" runat="server" Width="100%" GroupingText="Veh�culo para el Servicio">
                                <table width="100%" border="0" cellspacing="0" class="estilo_fuente">
                                    <tr>
                                        <td style="width: 15%;">
                                            <asp:HiddenField ID="Hdf_Vehiculo_ID" runat="server" />
                                            <asp:Label ID="Lbl_No_Inventario" runat="server" Text="No. Inventario"></asp:Label>
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:TextBox ID="Txt_No_Inventario" runat="server" Width="70%" MaxLength="7" Enabled="false"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Inventario" runat="server" TargetControlID="Txt_No_Inventario"
                                                FilterType="Numbers">
                                            </cc1:FilteredTextBoxExtender>
                                        </td>
                                        <td style="width: 15%;">
                                            &nbsp;&nbsp;
                                            <asp:Label ID="Lbl_No_Economico" runat="server" Text="No. Economico"></asp:Label>
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:TextBox ID="Txt_No_Economico" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">
                                            <asp:Label ID="Lbl_Datos_Vehiculo" runat="server" Text="Veh�culo"></asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="Txt_Datos_Vehiculo" runat="server" Width="99.2%" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">
                                            <asp:Label ID="Lbl_Placas" runat="server" Text="Placas"></asp:Label>
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:TextBox ID="Txt_Placas" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                        </td>
                                        <td style="width: 15%;">
                                            &nbsp;&nbsp;
                                            <asp:Label ID="Lbl_Anio" runat="server" Text="A�o"></asp:Label>
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:TextBox ID="Txt_Anio" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="Pnl_Bien_Mueble_Seleccionado" runat="server" 
                                        GroupingText="Bien Mueble para el Servicio" Width="99%">
                                        <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                                            <tr>
                                                <td style="width:15%;">
                                                    <asp:HiddenField ID="Hdf_Bien_Mueble_ID" runat="server" />
                                                    <asp:HiddenField ID="Hdf_Tipo_Bien" runat="server" />  
                                                    <asp:Label ID="Lbl_No_Inventario_BM" runat="server" Text="No. Inventario"></asp:Label>
                                                </td>
                                                <td style="width:35%;">
                                                    <asp:TextBox ID="Txt_No_Inventario_BM" runat="server" MaxLength="7" Width="98%" Enabled="false"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Inventario_BM" runat="server" 
                                                        FilterType="Numbers" TargetControlID="Txt_No_Inventario_BM">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width:15%;">
                                                    &nbsp;
                                                </td>
                                                <td style="width:35%;">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:15%;">
                                                    <asp:Label ID="Lbl_Descripcion_Bien" runat="server" Text="Descripci�n Bien"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Descripcion_Bien" runat="server" Enabled="false" Rows="2" 
                                                        TextMode="MultiLine" Width="99%"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:15%;">
                                                    <asp:Label ID="Lbl_Numero_Serie_Bien" runat="server" Text="No. Serie"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Numero_Serie_Bien" runat="server" Enabled="false" 
                                                        Width="99%"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Panel ID="Pnl_Descripcion_Servicio" runat="server" Width="100%" GroupingText="Descripci�n del Servicio">
                                <asp:TextBox ID="Txt_Descripcion_Servicio" runat="server" Rows="5" TextMode="MultiLine"
                                    Width="99%" Enabled="false"></asp:TextBox>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                </table>                
                <table width="100%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr style="background-color: #3366CC">
                        <td id="Barra_Generales" style="text-align: left; font-size: 15px; color: #FFFFFF;"
                            colspan="4">
                            Datos Proveedor
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            Proveedor
                        </td>
                        <td colspan="3" style="width: 85%;">
                            <asp:TextBox ID="Txt_Nombre_Proveedor" runat="server" Width="97%" Enabled="false"
                                Style="float: left"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="background-color: #3366CC">
                        <td style="text-align: left; font-size: 15px; color: #FFFFFF;" id="Td2" colspan="4">
                            Diagnostico
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%">
                            Diagnostico
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Descripcion" runat="server" Width="97%" Height="60px" TextMode="MultiLine"
                                Style="text-transform: uppercase" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%">
                            Costo Servicio
                        </td>
                        <td style="width: 85%" colspan="3">
                            <asp:TextBox ID="Txt_Costo_Unitario" runat="server" Width="20%" Text="" MaxLength="50"
                                Enabled="false" onBlur="this.value=formatCurrency(this.value);"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%">
                            &nbsp;
                        </td>
                        <td style="width: 35%">
                            &nbsp;
                        </td>
                        <td style="width: 15%">
                            &nbsp;
                        </td>
                        <td style="width: 35%">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="width: 100%">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr style="background-color: #3366CC">
                        <td id="Td1" style="text-align: left; font-size: 15px; color: #FFFFFF;" colspan="2">
                            Facturas
                        </td>
                        <td id="Td3" style="text-align:right; font-size: 15px; color: #FFFFFF;" colspan="2">
                        Agregar Factura                        
                            <asp:ImageButton ID="Btn_Agregar_Factura" runat="server" ImageUrl="~/paginas/imagenes/paginas/accept.png"
                                Width="18px" Height="18px" CssClass="Img_Button" AlternateText="Agregar Factura"
                                ToolTip="Agregar Factura" OnClick="Btn_Agregar_Factura_Click" />
                                </td>
                    </tr>
                    <tr>
                        <td style="width: 15%">
                            Factura
                        </td>
                        <td style="width: 35%">
                            <asp:TextBox ID="Txt_Factura" runat="server" Width="94%"></asp:TextBox>
                        </td>
                        <td style="width: 15%">
                            Fecha Factura
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Fecha_Factura" runat="server" Width="90%" Enabled="true"></asp:TextBox>
                            <asp:ImageButton ID="Btn_Fecha_Factura" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                            <cc1:CalendarExtender ID="CE_Txt_Fecha_Factura" runat="server" Format="dd/MMM/yyyy"
                                TargetControlID="Txt_Fecha_Factura" PopupButtonID="Btn_Fecha_Factura">
                            </cc1:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            No. Factura
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_No_Factura" runat="server" Width="94%" Enabled="false"></asp:TextBox>
                        </td>
                        <td style="width: 15%;">
                            Monto de Factura
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Monto" runat="server" Width="96%" Enabled="true" onBlur="this.value=formatCurrency(this.value);"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="Txt_Monto_FilteredTextBoxExtender" runat="server"
                                Enabled="true" TargetControlID="Txt_Monto" FilterType="Custom,Numbers" ValidChars=".,">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%">
                            Observaciones
                        </td>
                        <td colspan="3" style="width: 85%">
                            <asp:TextBox ID="Txt_Observaciones" runat="server" Width="98.3%" Height="60px" TextMode="MultiLine"
                                Style="text-transform: uppercase"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="Twe_Txt_Observaciones" runat="server" WatermarkCssClass="watermarked"
                                WatermarkText="L�mite de Caractes 1024" TargetControlID="Txt_Observaciones">
                            </cc1:TextBoxWatermarkExtender>
                            <cc1:FilteredTextBoxExtender ID="Txt_Observaciones_FilteredTextBoxExtender" runat="server"
                                FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" TargetControlID="Txt_Observaciones"
                                ValidChars="/*-+!#$%&/()=?���.,:;()���������� ">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            Subir Factura
                        </td>
                        <td style="width: 85%;" colspan="3">
                        <asp:ImageButton ID="Img_Btn_Cargar" runat="server" CssClass="Img_Button" CausesValidation="false"
                                                    ImageUrl="~/paginas/imagenes/paginas/subir.png" ToolTip="Cargar Archivo" 
                                                    OnClientClick="javascript:return Abrir_Carga_PopUp();" 
                                                    onclick="Img_Btn_Cargar_Click" />
                            
                            <table width="95%" runat="server" id="clientSide" style="display: inline;float:right">
                            </table>            
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Label ID="Lbl_Progress_File" runat="server" Text="">
                                <asp:UpdateProgress ID="Progress_Upnl_Filtros_Busqueda_Prestamos" runat="server"
                                    AssociatedUpdatePanelID="Upd_Lista_Refacciones" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                        </div>
                                        <div class="processMessage" id="div_progress">
                                            <img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </asp:Label>
                        </td>
                    </tr>
                </table>
                <br />
                <br />
                <%--update progrees--%>
                <asp:UpdateProgress ID="Uprg_Progress1" runat="server" AssociatedUpdatePanelID="Upd_Lista_Refacciones"
                                DisplayAfter="0">
                                <ProgressTemplate>
                                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                    </div>
                                    <div class="processMessage" id="div_progress">
                                        <img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                <asp:GridView ID="Grid_Facturas" runat="server" AllowPaging="False" AutoGenerateColumns="False"
                    CssClass="GridView_1" GridLines="None" DataKeyNames="NO_FACTURA_SERVICIO,NUMERO_FACTURA"
                    Width="99%" EnableModelValidation="True" OnSelectedIndexChanged="Grid_Facturas_SelectedIndexChanged"
                    OnRowDeleting="Grid_Facturas_RowDeleting" 
                    onrowdatabound="Grid_Facturas_RowDataBound" 
                    ondatabound="Grid_Facturas_DataBound">
                    <RowStyle CssClass="GridItem" />
                    <Columns>
                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                            <ItemStyle Width="3%" />
                        </asp:ButtonField>
                        <asp:BoundField DataField="NO_FACTURA_SERVICIO" HeaderText="NO_FACTURA_SERVICIO"
                            HeaderStyle-HorizontalAlign="Left" Visible="false" SortExpression="NO_FACTURA_SERVICIO">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NUMERO_FACTURA" HeaderText="No." SortExpression="NO_FACTURA"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="5%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FACTURA" HeaderText="Factura" SortExpression="FACTURA"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="7%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FECHA_FACTURA" HeaderText="Fecha" SortExpression="FECHA_FACTURA"
                            HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:dd/MMM/yyyy}">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MONTO_FACTURA" DataFormatString="{0:c}" HeaderText="Monto"
                            HeaderStyle-HorizontalAlign="Left" SortExpression="MONTO_FACTURA">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="8%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="IVA_FACTURA" DataFormatString="{0:c}" HeaderText="IVA"
                            HeaderStyle-HorizontalAlign="Left" SortExpression="IVA_FACTURA">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="8%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TOTAL_FACTURA" DataFormatString="{0:c}" HeaderText="Total"
                            HeaderStyle-HorizontalAlign="Left" SortExpression="TOTAL_FACTURA">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="8%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NOMBRE_ARCHIVO" HeaderText="Nombre de Archivo" SortExpression="NOMBRE_ARCHIVO"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Ver" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="30%">
                            <ItemTemplate>
                                <asp:HyperLink ID="Btn_Archivo" runat="server" CommandName="Cmd_Archivo" Target="_blank"
                                    NavigateUrl="<%# Bind('RUTA_ARCHIVO') %>" Text="<%# Bind('NOMBRE_ARCHIVO') %>"
                                    ForeColor="#2F4E7D"></asp:HyperLink>
                                <itemstyle font-size="X-Small" horizontalalign="Left" width="20%" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:ButtonField ButtonType="Image" CommandName="Delete" ImageUrl="~/paginas/imagenes/paginas/Eliminar_Incidencia.png">
                            <ItemStyle />
                        </asp:ButtonField>
                    </Columns>
                    <PagerStyle CssClass="GridHeader" />
                    <SelectedRowStyle CssClass="GridSelected" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAltItem" />
                </asp:GridView>
                <asp:HiddenField ID="Hdf_No_Entrada_Factura" runat="server" />
                <asp:HiddenField ID="Hdf_No_Entrada" runat="server" />
                <asp:HiddenField ID="Hdf_No_Servicio" runat="server" />
                <asp:HiddenField ID="Hdf_No_Solicitud" runat="server" />
                <asp:HiddenField ID="Hdf_Proveedor_ID" runat="server" />
                <asp:HiddenField ID="Hdf_No_Asignacion" runat="server" />
                <asp:HiddenField ID="Hdf_No_Reserva" runat="server" />
                <asp:HiddenField ID="Hdf_Suma_Monto_Facturas" runat="server" />
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Btn_Agregar_Factura" />
            <asp:PostBackTrigger ControlID="Btn_Modificar" />
        </Triggers>
    </asp:UpdatePanel>
    <!--Modal para la seleccion del archivo-->
        <asp:Panel ID="Pnl_Seleccionar_Archivo" runat="server" CssClass="drag" HorizontalAlign="Center" Width="650px" style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">
            <asp:Panel ID="Pnl_Cabecera_Seleccionar_Archivo" runat="server">
                <table width="100%">
                    <tr>
                        <td style="color:Black;font-size:12;font-weight:bold;"><asp:Image ID="Image1" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />Seleccionar el Archivo para la Factura</td>
                    </tr>
                </table>
            </asp:Panel>
            <div style="color: #5D7B9D">
                <table width="100%">
                    <tr>
                        <td align="left" style="text-align:left;">
                            <asp:UpdatePanel ID="Upnl_Archivos_Requisicion" runat="server">
                                <ContentTemplate>
                                    <asp:UpdateProgress ID="Progress_Upnl_Archivos_Requisicion" runat="server" AssociatedUpdatePanelID="Upnl_Archivos_Requisicion" DisplayAfter="0">
                                        <ProgressTemplate>
                                            <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                            <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress"><img alt="" src="../Imagenes/paginas/Sias_Roler.gif" /></div>                                        
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <table width="100%">
                                        <tr>
                                            <td colspan="4" style="width:100%;"><hr /></td>
                                        </tr>
                                        <tr>
                                            <td style="width:20%;text-align:left;font-size:11px;">
                                            <cc1:AsyncFileUpload runat="server" OnClientUploadError="uploadError" OnClientUploadComplete="uploadComplete"
                                ID="Fup_Factura_Recibida" Width="350px" UploadingBackColor="#CCFFFF" ThrobberID="Lbl_Throbber"
                                OnUploadedComplete="Fup_Factura_Recibida_UploadedComplete" />                                                
                                                <asp:Label ID="Lbl_Throbber" runat="server" Text="Espere" Width="30px">
                                                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                                    <div  class="processMessage" id="div_progress">
                                                        <center>
                                                            <img alt="" src="../Imagenes/paginas/Sias_Roler.gif" />
                                                            <br /><br />
                                                            <span id="spanUploading" runat="server" style="color:White;font-size:30px;font-weight:bold;font-family:Lucida Calligraphy;font-style:italic;">
                                                                Cargando...
                                                            </span>
                                                        </center>
                                                    </div>                                                
                                                </asp:Label>
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td colspan="4" style="width:100%;text-align:left;">
                                                <center>
                                                    <asp:Button ID="Btn_Archivos_Facturas" runat="server" Text="Aceptar" 
                                                        CssClass="button" CausesValidation="false" Width="200px"  OnClientClick="javascript:return Cerrar_Carga_PopUp();" />
                                                </center>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger EventName="Click" ControlID="Btn_Archivos_Facturas" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>    
    <br />
    <br />
</asp:Content>
