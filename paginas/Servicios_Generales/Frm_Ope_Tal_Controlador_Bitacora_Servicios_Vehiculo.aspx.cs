﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Ayudante_JQuery;
using JAPAMI.Dependencias.Datos;
using JAPAMI.Parametros.EasyUI;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio;

public partial class paginas_Servicios_Generales_Frm_Ope_Tal_Controlador_Bitacora_Servicios_Vehiculo : System.Web.UI.Page
{
    int pagina, renglon;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.controlador(this.Response, this.Request);
    }

    public void controlador(HttpResponse response, HttpRequest request)
    {

        String accion = "";
        String strdatos = "";

        accion = HttpContext.Current.Request["accion"] == null ? string.Empty : HttpContext.Current.Request["accion"].ToString().Trim();

        switch (accion)
        {
            case "consultar_listado_unidades_responsables":
                strdatos = Consultar_Unidades_Responsables();
                break;
            case "Consultar_Solicitudes_Servicios":
                strdatos = Consultar_Solicitudes_Servicios();
                break;
        }

        response.Clear();
        response.ContentType = "application/json";
        response.Flush();
        response.Write(strdatos);
        response.End();

    }

    private String Consultar_Unidades_Responsables()
    {
        String strdatos = "{[]}";
        String Catalogo = String.Empty;
        DataTable Dt_Resultado = new DataTable();
        Cls_Cat_Dependencias_Negocio Dependencia_Negocio = new Cls_Cat_Dependencias_Negocio();
        try
        {
            pagina = ParamsofEasyUI.page;
            renglon = ParamsofEasyUI.rows;
            Catalogo = HttpContext.Current.Request["Catalogo"] == null ? string.Empty : HttpContext.Current.Request["Catalogo"].ToString().Trim();//Para recuperar Query String Nombre Catalogo
            Dt_Resultado = Cls_Cat_Dependencias_Datos.Consulta_Dependencias(Dependencia_Negocio);
            //strdatos = Ayudante_JQuery.dataTableToJSON(Dt_Resultado);
            strdatos = Ayudante_JQuery.dataTableToJSONsintotalrenglones(Dt_Resultado);
            return strdatos;
        }
        catch (Exception Ex) { return Ex.ToString(); };
    }

    private string Consultar_Solicitudes_Servicios()
    {
        String Str_Datos = "{[]}";
        Cls_Ope_Tal_Solicitud_Servicio_Negocio Servicios_Negocio = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
        try
        {
            pagina = ParamsofEasyUI.page;
            renglon = ParamsofEasyUI.rows;
            //Carga de Parametros
            Servicios_Negocio.P_Tipo_Servicio = HttpContext.Current.Request["tipo_solicitud"] == null ? string.Empty : HttpContext.Current.Request["tipo_solicitud"].ToString().Trim();
            Servicios_Negocio.P_Folio_Solicitud = HttpContext.Current.Request["folio"] == null ? string.Empty : HttpContext.Current.Request["folio"].ToString().Trim();
            Servicios_Negocio.P_Dependencia_ID = HttpContext.Current.Request["dependencia_id"] == null ? string.Empty : HttpContext.Current.Request["dependencia_id"].ToString().Trim();
            Servicios_Negocio.P_No_Inventario = HttpContext.Current.Request["no_inventario"] == null ? -1 : Convert.ToInt32(HttpContext.Current.Request["no_inventario"].ToString().Trim());
            Servicios_Negocio.P_No_Economico = HttpContext.Current.Request["no_economico"] == null ? string.Empty : HttpContext.Current.Request["no_economico"].ToString().Trim();
            Servicios_Negocio.P_Busq_Fecha_Recepcion_Inicial = HttpContext.Current.Request["fecha_inicial"] == null ? new DateTime() : HttpContext.Current.Request["fecha_inicial"].ToString().Trim().Length > 0 ? Convert.ToDateTime(HttpContext.Current.Request["fecha_inicial"].ToString().Trim()) : new DateTime();
            Servicios_Negocio.P_Busq_Fecha_Recepcion_Final = HttpContext.Current.Request["fecha_final"] == null ? new DateTime() : HttpContext.Current.Request["fecha_final"].ToString().Trim().Length > 0 ? Convert.ToDateTime(HttpContext.Current.Request["fecha_final"].ToString().Trim()) : new DateTime();
            Servicios_Negocio.P_Estatus = HttpContext.Current.Request["estatus"] == null ? string.Empty : HttpContext.Current.Request["estatus"].ToString().Trim();
            Servicios_Negocio.P_Reparacion = HttpContext.Current.Request["tipo_reparacion"] == null ? string.Empty : HttpContext.Current.Request["tipo_reparacion"].ToString().Trim();
            Servicios_Negocio.P_Ordenar_Listado = "FOLIO DESC";

            //Consulta de Información
            DataTable Dt_Resultado = Servicios_Negocio.Consultar_Listado_Solicitudes_Servicio();
            Str_Datos = Ayudante_JQuery.onDataGrid(Dt_Resultado, pagina, renglon);
        }
        catch (Exception ex)
        {
            Str_Datos = "{[]}";
        }
        return Str_Datos;
    }

}
