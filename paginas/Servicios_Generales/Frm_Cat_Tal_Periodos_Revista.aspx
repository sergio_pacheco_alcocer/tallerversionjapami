﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Cat_Tal_Periodos_Revista.aspx.cs" Inherits="paginas_Taller_Mecanico_Frm_Cat_Tal_Periodos_Revista" Title="Periodos Revista Mecanica" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">

    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" EnablePartialRendering="true" AsyncPostBackTimeout="9000">
    </cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
    <ContentTemplate> 
        <asp:UpdateProgress ID="Uprg_Loading" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
            <ProgressTemplate>
                <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
            </ProgressTemplate>                     
        </asp:UpdateProgress>
    
        <div id="Div_Area_Trabajo" style="background-color:#ffffff; width:100%; height:100%;">
        <center>
            <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                <tr align="center">
                    <td class="label_titulo" colspan="2">Catalogo Periodos Revista Mecanica</td>
                </tr>
                <tr>
                    <td colspan="2">
                      <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                        <table style="width:100%;">
                          <tr>
                            <td colspan="2" align="left">
                              <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                Width="24px" Height="24px"/>
                                <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                            </td>            
                          </tr>
                          <tr>
                            <td style="width:10%;">              
                            </td>          
                            <td style="width:90%;text-align:left;" valign="top">
                              <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                            </td>
                          </tr>          
                        </table>                   
                      </div>                
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>                        
                </tr>
                <tr class="barra_busqueda" align="right">
                    <td align="left" style="width:50%;">
                        <asp:ImageButton ID="Btn_Nuevo" runat="server" 
                            ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" 
                            Width="24px" CssClass="Img_Button" AlternateText="Nuevo" 
                            ToolTip="Nuevo Periodo" onclick="Btn_Nuevo_Click" />
                        <asp:ImageButton ID="Btn_Modificar" runat="server" 
                            ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" Width="24px" 
                            CssClass="Img_Button" AlternateText="Modificar"
                            ToolTip="Modificar Periodo" onclick="Btn_Modificar_Click" />
                        <asp:ImageButton ID="Btn_Eliminar" runat="server" 
                            ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png" Width="24px" 
                            CssClass="Img_Button" AlternateText="Cancelar" ToolTip="Cancelar Periodo" 
                            OnClientClick="return confirm('¿Esta seguro que desea CANCELAR el Periodo?');" 
                            onclick="Btn_Eliminar_Click" />
                        <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Salir"
                            ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" Width="24px" 
                            CssClass="Img_Button" AlternateText="Salir" onclick="Btn_Salir_Click" />
                    </td>
                    <td style="width:50%;">B&uacute;squeda
                        <asp:TextBox ID="Txt_Busqueda" runat="server" Width="130px"></asp:TextBox>
                        <asp:ImageButton ID="Btn_Busqueda" runat="server" 
                            ImageUrl="~/paginas/imagenes/paginas/busqueda.png" AlternateText="Buscar" 
                            ToolTip="Buscar" onclick="Btn_Busqueda_Click" />
                        <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda" runat="server" 
                            WatermarkText="< Nombre >" TargetControlID="Txt_Busqueda" 
                            WatermarkCssClass="watermarked"></cc1:TextBoxWatermarkExtender>    
                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda" runat="server" 
                            TargetControlID="Txt_Busqueda" InvalidChars="<,>,&,',!," 
                            FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                            ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                        </cc1:FilteredTextBoxExtender>                                  
                    </td>
                </tr>
            </table>
        </center>
        </div>
               
        <div id="Div_Campos" runat="server" style="width:100%;">
            <center>
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td colspan="4" >
                            <asp:HiddenField ID="Hdf_Periodo_ID" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width:10%;" >
                            <asp:Label ID="Lbl_Nombre" runat="server" Text="Nombre"></asp:Label>
                        </td>
                        <td colspan="3" >
                            <asp:TextBox ID="Txt_Nombre" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <asp:Label ID="Lbl_Periodo" runat="server" Text="Periodo"></asp:Label>
                        </td>
                        <td style="width:40%;" >
                            <asp:TextBox ID="Txt_Periodo" runat="server" Width="50%" Enabled="false"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Periodo" runat="server" TargetControlID="Txt_Periodo" FilterType="Numbers">
                            </cc1:FilteredTextBoxExtender>
                            <asp:Label ID="Lbl_Meses" runat="server" Text="Meses"></asp:Label>
                        </td>
                        <td style="width:10%;" >
                            <asp:Label ID="Lbl_Estatus" runat="server" Text="Estatus"></asp:Label>
                        </td>
                        <td >
                            <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="100%">
                            <asp:ListItem Value="">&lt; - - SELECCIONE - - &gt;</asp:ListItem>
                            <asp:ListItem Value="VIGENTE">VIGENTE</asp:ListItem>
                            <asp:ListItem Value="BAJA">BAJA</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </center>
            <hr />
            <asp:GridView ID="Grid_Listado" runat="server" CssClass="GridView_1" AutoGenerateColumns="False" 
                AllowPaging="True" PageSize="10" Width="99%" GridLines= "None" 
                EmptyDataText="No se encontrarón movimientos" 
                onselectedindexchanged="Grid_Listado_SelectedIndexChanged">
                <RowStyle CssClass="GridItem" />
                <Columns>
                    <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png" >
                        <ItemStyle Width="30px" />
                    </asp:ButtonField>
                    <asp:BoundField DataField="PERIODO_ID" HeaderText="ID" 
                        SortExpression="PERIODO_ID"  >
                        <ItemStyle Width="50px" HorizontalAlign="Center" Font-Size="X-Small" />
                    </asp:BoundField>
                    <asp:BoundField DataField="NOMBRE" HeaderText="Nombre" 
                        SortExpression="NOMBRE"  >
                        <ItemStyle Width="50px" HorizontalAlign="Center" Font-Size="X-Small" />
                    </asp:BoundField>
                    <asp:BoundField DataField="PERIODO_MES" HeaderText="Periodo en Meses" 
                        SortExpression="PERIODO_MES"  >
                        <ItemStyle Width="50px" HorizontalAlign="Center" Font-Size="X-Small" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" 
                        SortExpression="ESTATUS"  >
                        <ItemStyle Width="50px" HorizontalAlign="Center" Font-Size="X-Small" />
                    </asp:BoundField>
                </Columns>
                <PagerStyle CssClass="GridHeader" />
                <SelectedRowStyle CssClass="GridSelected" />
                <HeaderStyle CssClass="GridHeader" />                                
                <AlternatingRowStyle CssClass="GridAltItem" />       
            </asp:GridView>      
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
