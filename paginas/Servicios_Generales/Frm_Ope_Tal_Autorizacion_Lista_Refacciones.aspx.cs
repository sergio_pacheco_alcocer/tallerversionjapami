﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Catalogo_Taller_Servicios_Preventivos.Negocio;
using JAPAMI.Catalogo_Taller_Refacciones.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Entradas_Vehiculos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Preventivos.Negocio;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Correctivos.Negocio;

public partial class paginas_Taller_Municipal_Frm_Ope_Tal_Autorizacion_Lista_Refacciones : System.Web.UI.Page
{
    #region Variables
    private const int Const_Estado_Inicial = 0;
    private const int Const_Estado_Nuevo = 1;
    private const int Const_Estado_Modificar = 2;
    #endregion

    #region Page Load / Init
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            //if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!Page.IsPostBack)
            {
                Estado_Botones(Const_Estado_Inicial);
                Configurar_Formulario();
                Llenar_Listado_Servicios();
                //Configuracion_Acceso("Frm_Cat_Tal_Servicios_Preventivos.aspx");
                //Cargar_Servicios(0);
            }
            Mensaje_Error();
        }
        catch (Exception Ex)
        {
            //Mensaje_Error(Txt_Pagos.Text.Trim() +" - "+ Ex.Message);
            Estado_Botones(Const_Estado_Inicial);
        }
    }
    #endregion

    #region Metodos

    #region Metodos/Validaciones
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Validar_Campos
    ///DESCRIPCIÓN: valdia que se ingresen los campos obligatorios
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 06/27/2011 11:31:53 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private Boolean Validar_Campos()
    {
        Boolean Resultado = true;
                
        if (String.IsNullOrEmpty(Hdf_No_Entrada.Value))
        {
            Lbl_Ecabezado_Mensaje.Text = "Los siguientes datos son necesarios:";
            Mensaje_Error("- No se encontró numero de entrada");
            Resultado = false;
        }
        if (String.IsNullOrEmpty( Hdf_No_Servicio.Value))
        {
            Lbl_Ecabezado_Mensaje.Text = "Los siguientes datos son necesarios:";
            Mensaje_Error("- Seleccione la Solicitud");
            Resultado = false;
        }
        if (Cmb_Autorizar.SelectedIndex<=0)
        {
            Lbl_Ecabezado_Mensaje.Text = "Los siguientes datos son necesarios:";
            Mensaje_Error("- Seleccione el estatus");
            Resultado = false;
        }
        

        return Resultado;
    }
    #endregion

    #region Grids

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Solicitudes_PageIndexChanging
    ///DESCRIPCIÓN: Maneja la paginación del Listado
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Mayo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Listado_Servicios_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Grid_Listado_Servicios.SelectedIndex = (-1);
            Grid_Listado_Servicios.PageIndex = e.NewPageIndex;
            Llenar_Listado_Servicios();
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Solicitudes_SelectedIndexChanged
    ///DESCRIPCIÓN: Obtiene los datos de una Solicitud 
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Mayo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Listado_Servicios_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (Grid_Listado_Servicios.SelectedIndex > (-1))
            {
                Limpiar_Formulario();
                Hdf_No_Entrada.Value = HttpUtility.HtmlDecode(Grid_Listado_Servicios.SelectedRow.Cells[1].Text.Trim()).Trim();
                Hdf_No_Servicio.Value = HttpUtility.HtmlDecode(Grid_Listado_Servicios.SelectedRow.Cells[2].Text.Trim()).Trim();
                Hdf_No_Solicitud.Value = HttpUtility.HtmlDecode(Grid_Listado_Servicios.SelectedRow.Cells[3].Text.Trim()).Trim();
                Mostrar_Registro();
                Estado_Botones(Const_Estado_Modificar);
                Grid_Listado_Servicios.SelectedIndex = -1;
                System.Threading.Thread.Sleep(500);
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }

    #endregion

    #region Metodos ABC
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Obtener_Dato_Consulta
    ///DESCRIPCIÓN          : Consulta el Campo dado de la Tabla Indicada
    ///PARAMETROS:     
    ///CREO                 : Antonio Salvador Benvides Guardado
    ///FECHA_CREO           : 24/Agosto/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private String Obtener_Dato_Consulta(String Campo, String Tabla, String Condiciones)
    {
        String Mi_SQL;
        String Dato_Consulta = "";

        try
        {
            Mi_SQL = "SELECT " + Campo;
            if (Tabla != "")
            {
                Mi_SQL += " FROM " + Tabla;
            }
            if (Condiciones != "")
            {
                Mi_SQL += " WHERE " + Condiciones;
            }

            SqlDataReader Dr_Dato = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

            if (Dr_Dato.Read())
            {
                if (Dr_Dato[0] != null)
                {
                    Dato_Consulta = Dr_Dato[0].ToString();
                }
                else
                {
                    Dato_Consulta = "";
                }
                Dr_Dato.Close();
            }
            else
            {
                Dato_Consulta = "";
            }
            if (Dr_Dato != null)
            {
                Dr_Dato.Close();
            }
            Dr_Dato = null;
        }
        catch
        {
        }
        finally
        {
        }

        return Dato_Consulta;
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Modificar_Servicios
    ///DESCRIPCIÓN: se obtienen los datos para modificar el servicio
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 06/27/2011 11:12:18 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    private void Modificar_Servicio()
    {
        Cls_Cat_Tal_Servicios_Preventivos_Negocio Servicios_Negocio = new Cls_Cat_Tal_Servicios_Preventivos_Negocio();
        
        DataTable Dt_Refacciones = Formar_Tabla_Refacciones();
        DataTable Dt_Seleccionados = Formar_Tabla_Refacciones();
        String Estatus = "";
        try
        {
            if (Validar_Campos())
            {
                if (Cmb_Autorizar.SelectedValue.Trim().ToUpper().Contains("ACEPTADO"))
                    Estatus = "PROCESO";
                if (Cmb_Autorizar.SelectedValue.Trim().ToUpper().Contains("RECHAZADO"))
                    Estatus = "REPARACION";
                Servicios_Negocio.P_No_Servicio = Hdf_No_Servicio.Value;
                Servicios_Negocio.P_No_Entrada = Convert.ToInt32(Hdf_No_Entrada.Value);
                Servicios_Negocio.P_Estatus = Estatus;
                Servicios_Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                if (Lbl_Tipo_Servicio.Text.Trim().ToUpper().Contains("PREV"))
                {
                    Servicios_Negocio.Modifica_Servicio_Preventivo();
                }
                if (Lbl_Tipo_Servicio.Text.Trim().ToUpper().Contains("CORR"))
                {
                    Servicios_Negocio.Modifica_Servicio_Correctivo();
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Servicios Preventivos", "alert('La modificacion del Servicios fue Exitosa');", true);
                Estado_Botones(Const_Estado_Inicial);
                Limpiar_Formulario();
                Llenar_Listado_Servicios();
            }

        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Alta_Servicio
    ///DESCRIPCIÓN: se obtienen los datos para dar de alta el registro
    ///PARAMETROS: 
    ///CREO: jesus toledo
    ///FECHA_CREO: 05/Mayo/2012 11:12:18 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    private void Alta_Servicio()
    {
        Cls_Cat_Tal_Servicios_Preventivos_Negocio Servicios_Negocio = new Cls_Cat_Tal_Servicios_Preventivos_Negocio();
        DataTable Dt_Refacciones = Formar_Tabla_Refacciones();

        try
        {
            if (Validar_Campos())
            {
                if (Session["Tabla_Refacciones"] != null)
                    Dt_Refacciones = (DataTable)Session["Tabla_Refacciones"];
                Servicios_Negocio.P_No_Servicio = Hdf_No_Solicitud.Value;
                Servicios_Negocio.P_Dt_Refacciones_Servicios = Dt_Refacciones;
                Servicios_Negocio.Alta_Listado();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Servicios Preventivos", "alert('El alta del Servicio fue Exitosa');", true);
                Estado_Botones(Const_Estado_Inicial);
                Limpiar_Formulario();
                Llenar_Listado_Servicios();
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    /////*******************************************************************************
    /////NOMBRE DE LA FUNCIÓN: Llenar_Listado_Solicitudes
    /////DESCRIPCIÓN: Se llena el Listado de las Solicitudes.
    /////PROPIEDADES:     
    /////CREO: Francisco Antonio Gallardo Castañeda.
    /////FECHA_CREO: 04/Mayo/2012
    /////MODIFICO:
    /////FECHA_MODIFICO
    /////CAUSA_MODIFICACIÓN
    /////******************************************************************************* 
    //private void Llenar_Listado_Solicitudes()
    //{
    //    Cls_Cat_Tal_Servicios_Preventivos_Negocio Servicio_Negocio = new Cls_Cat_Tal_Servicios_Preventivos_Negocio();
    //    Servicio_Negocio.P_Filtro = "SELECT * FROM " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos;
    //    Servicio_Negocio.P_Filtro = Servicio_Negocio.P_Filtro + " WHERE " + Ope_Tal_Serv_Preventivos.Campo_Estatus + " = 'PROCESO'";
    //    DataTable Dt_Resultados = Servicio_Negocio.Consulta_General();
    //    Grid_Listado_Solicitudes.DataSource = Dt_Resultados;
    //    Grid_Listado_Solicitudes.DataBind();
    //}
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Servicios
    ///DESCRIPCIÓN: Se llena el Listado de los Servicios para asignar el Mecanico.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Llenar_Listado_Servicios()
    {
        DataTable Dt_Resultados = new DataTable();
        Cls_Ope_Tal_Servicios_Preventivos_Negocio Serv_Prev = new Cls_Ope_Tal_Servicios_Preventivos_Negocio();
        Cls_Ope_Tal_Servicios_Correctivos_Negocio Serv_Corr = new Cls_Ope_Tal_Servicios_Correctivos_Negocio();
        Serv_Prev.P_Estatus = "LISTADO";
        Serv_Prev.P_Reparacion = "INTERNA";
        if (!String.IsNullOrEmpty(Txt_Buscar.Text.Trim()))
            Serv_Prev.P_No_Servicio = Convert.ToInt32( Txt_Buscar.Text.Trim());
        DataTable Dt_Resultados_Preventivos = Serv_Prev.Consultar_Servicios_Preventivos();
        Serv_Corr.P_Estatus = "LISTADO";
        Serv_Corr.P_Reparacion = "INTERNA";
        if (!String.IsNullOrEmpty(Txt_Buscar.Text.Trim()))
            Serv_Corr.P_No_Solicitud = Convert.ToInt32(Txt_Buscar.Text.Trim());
        DataTable Dt_Resultados_Correctivos = Serv_Corr.Consultar_Servicios_Correctivos();

        Dt_Resultados_Preventivos.Merge(Dt_Resultados_Correctivos);
        Dt_Resultados_Preventivos.AcceptChanges();

        Grid_Listado_Servicios.Columns[1].Visible = true;
        Grid_Listado_Servicios.Columns[2].Visible = true;
        Grid_Listado_Servicios.Columns[3].Visible = true;
        Grid_Listado_Servicios.DataSource = Dt_Resultados_Preventivos;
        Grid_Listado_Servicios.DataBind();
        Grid_Listado_Servicios.Columns[1].Visible = false;
        Grid_Listado_Servicios.Columns[2].Visible = false;
        Grid_Listado_Servicios.Columns[3].Visible = false;
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA CLASE: Cargar_Refacciones
    ///DESCRIPCIÓN: consulta y muestra los datos, instancia la clase de negocios para acceder a la consulta
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 15/May/2012 06:24:15 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    private void Cargar_Refacciones(int Page_Index)
    {
        DataTable Dt_Refacciones = new DataTable();
        Cls_Cat_Tal_Refacciones_Negocio Refacciones_Negocio = new Cls_Cat_Tal_Refacciones_Negocio();
        Cls_Cat_Tal_Servicios_Preventivos_Negocio Servicios_Negocio = new Cls_Cat_Tal_Servicios_Preventivos_Negocio();
        if (Session["Tabla_Refacciones_Modificados"] != null)
            Dt_Refacciones = (DataTable)Session["Tabla_Refacciones_Modificados"];

        try
        {
            if (Session["Tabla_Refacciones_Modificados"] != null)
            {
                Grid_Refacciones.PageIndex = Page_Index;
                Grid_Refacciones.DataSource = Dt_Refacciones;
                Grid_Refacciones.DataBind();
            }
            else
            {
                Servicios_Negocio.P_No_Servicio = Hdf_No_Servicio.Value;
                Servicios_Negocio.P_Tipo_Servicio = Lbl_Tipo_Servicio.Text.Trim();
                Dt_Refacciones = Servicios_Negocio.Consultar_Listado();
                Grid_Refacciones.PageIndex = Page_Index;
                Grid_Refacciones.DataSource = Dt_Refacciones;
                Grid_Refacciones.DataBind();
                
                if (Dt_Refacciones.Rows.Count > 0)
                {
                    Session["Tabla_Refacciones"] = Dt_Refacciones;
                    Grid_Refacciones.PageIndex = Page_Index;
                    Grid_Refacciones.DataSource = Dt_Refacciones;
                    Grid_Refacciones.DataBind();
                }
                else
                {
                    Session["Tabla_Refacciones"] = null;
                    Grid_Refacciones.DataSource = null;
                    Grid_Refacciones.DataBind();
                }
            }

        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }

    #endregion
    #region Clase de Negocio de Solicitudes [Registrar y Consulta]

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Mostrar_Registro
    ///DESCRIPCIÓN: Muestra el Registro en los campos.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 09/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Mostrar_Registro()
    {
        Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
        Solicitud.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
        Solicitud = Solicitud.Consultar_Detalles_Solicitud_Servicio();
        if (Solicitud.P_No_Solicitud > (-1))
        {
            //Txt_Folio.Text = Solicitud.P_Folio_Solicitud.Trim();
            //Txt_Fecha_Elaboracion.Text = String.Format("{0:dd/MMM/yyyy}", Solicitud.P_Fecha_Elaboracion);
            Lbl_Tipo_Servicio.Text = Solicitud.P_Tipo_Servicio;
            Lbl_Unidad_Responsable.Text = Obtener_Dato_Consulta(" NOMBRE ", Cat_Dependencias.Tabla_Cat_Dependencias, Cat_Dependencias.Campo_Dependencia_ID + "  = '" + Solicitud.P_Dependencia_ID + "'");
            Hdf_Vehiculo_ID.Value = Solicitud.P_Bien_ID;
            Txt_Descripcion_Servicio.Text = Solicitud.P_Descripcion_Servicio;
            Cargar_Datos_Vehiculo(Hdf_Vehiculo_ID.Value, "IDENTIFICADOR");
        }
        if (Hdf_No_Entrada.Value.Trim().Length > 0)
        {
            Cls_Ope_Tal_Entradas_Vehiculos_Negocio Entrada = new Cls_Ope_Tal_Entradas_Vehiculos_Negocio();
            Entrada.P_No_Entrada = Convert.ToInt32(Hdf_No_Entrada.Value);
            Entrada = Entrada.Consultar_Detalles_Entrada_Vehiculo();
            if (Entrada.P_Kilometraje > (-1.0))
            {
                Lbl_Kilometraje.Text = String.Format("{0:########0.00}", Entrada.P_Kilometraje);
                //Lbl_Fecha_Recepcion.Text = String.Format("{0:dd/MMM/yyyy}", Entrada.P_Fecha_Entrada);
            }
        }
        Mostrar_Registro_Servicio();
        Cargar_Refacciones(0);
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Mostrar_Registro_Servicio
    ///DESCRIPCIÓN: Muestra el Registro en los campos.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 09/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Mostrar_Registro_Servicio()
    {
        
            Cls_Ope_Tal_Servicios_Preventivos_Negocio Serv_Negocio = new Cls_Ope_Tal_Servicios_Preventivos_Negocio();
            Cls_Ope_Tal_Servicios_Correctivos_Negocio Corr_Negocio = new Cls_Ope_Tal_Servicios_Correctivos_Negocio();
            if (Lbl_Tipo_Servicio.Text.Trim().Contains("CORRECTIVO"))
            {
                Corr_Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value.Trim());
                Corr_Negocio = Corr_Negocio.Consultar_Detalles_Servicio_Correctivo();
                if (Corr_Negocio.P_No_Servicio > (-1))
                {

                    Lbl_Mecanicos.Text = Obtener_Dato_Consulta(" NOMBRE ||' '|| APELLIDO_PATERNO ||' '|| APELLIDO_MATERNO as NOMBRE ", Cat_Empleados.Tabla_Cat_Empleados, " EMPLEADO_ID in (SELECT EMPLEADO_ID FROM " + Cat_Tal_Mecanicos.Tabla_Cat_Tal_Mecanicos + " WHERE MECANICO_ID = '" + Corr_Negocio.P_Mecanico_ID + "')");
                    Txt_Diagnostico_Mecanico.Text = Corr_Negocio.P_Diagnostico;
                }

            }
            else
            {
                Serv_Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value.Trim());
                Serv_Negocio = Serv_Negocio.Consultar_Detalles_Servicio_Preventivo();
                if (Serv_Negocio.P_No_Servicio > (-1))
                {

                    Lbl_Mecanicos.Text = Obtener_Dato_Consulta(" NOMBRE ||' '|| APELLIDO_PATERNO ||' '|| APELLIDO_MATERNO as NOMBRE ", Cat_Empleados.Tabla_Cat_Empleados, " EMPLEADO_ID in (SELECT EMPLEADO_ID FROM " + Cat_Tal_Mecanicos.Tabla_Cat_Tal_Mecanicos + " WHERE MECANICO_ID = '" + Serv_Negocio.P_Mecanico_ID + "')");
                    Txt_Diagnostico_Mecanico.Text = Serv_Negocio.P_Diagnostico;
                }
            }
    }    

    #endregion
    #region [Cargar Datos/Solicitud]
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Vehiculo
    ///DESCRIPCIÓN: Se cargan los Datos del Vehiculo Seleccionado.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Cargar_Datos_Vehiculo(String Vehiculo, String Tipo_Busqueda)
    {
        Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
        switch (Tipo_Busqueda)
        {
            case "NO_INVENTARIO":
                Consulta_Negocio.P_No_Inventario = Vehiculo;
                break;
            case "IDENTIFICADOR":
                Consulta_Negocio.P_Vehiculo_ID = Vehiculo;
                break;
            default: break;
        }
        //if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Consulta_Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value; }
        DataTable Dt_Vehiculo = Consulta_Negocio.Consultar_Vehiculos();
        if (Dt_Vehiculo.Rows.Count > 0)
        {
            Hdf_Vehiculo_ID.Value = Dt_Vehiculo.Rows[0]["VEHICULO_ID"].ToString().Trim();
            Lbl_No_Inventario.Text = Dt_Vehiculo.Rows[0]["NO_INVENTARIO"].ToString().Trim();
            Lbl_No_Economico.Text = Dt_Vehiculo.Rows[0]["NO_ECONOMICO"].ToString().Trim();
            Lbl_Datos_Vehiculo.Text = Dt_Vehiculo.Rows[0]["VEHICULO_DESCRIPCION"].ToString().Trim();
            Lbl_Placas.Text = Dt_Vehiculo.Rows[0]["PLACAS"].ToString().Trim();
            Lbl_Anio.Text = Dt_Vehiculo.Rows[0]["ANIO"].ToString().Trim();
            Lbl_Unidad_Responsable.Text = Obtener_Dato_Consulta(" NOMBRE ", Cat_Dependencias.Tabla_Cat_Dependencias, Cat_Dependencias.Campo_Dependencia_ID + "  = '" + Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString() + "'");
            //Lbl_Unidad_Responsable.Text = Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString();
        }
        else
        {
            Mensaje_Error("El Vehículo no se encontro.");
        }
    }
    #endregion

    #region Metodos Generales    
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Configurar_Formulario
    ///DESCRIPCIÓN: Cargar Combos y Datos del formulario
    ///CREO: jtoledo
    ///FECHA_CREO: 17/May/2012
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    private void Configurar_Formulario()
    {
        try
        {
            
        }
        catch (Exception ex) { Mensaje_Error(ex.Message); }

    }
    ///*******************************************************************************
    ///NOMBRE DE LA METODO: LLenar_Combo_Id
    ///        DESCRIPCIÓN: llena todos los combos
    ///         PARAMETROS: 1.- Obj_DropDownList: Combo a llenar
    ///                     2.- Dt_Temporal: DataTable genarada por una consulta a la base de datos
    ///                     3.- Texto: nombre de la columna del dataTable que mostrara el texto en el combo
    ///                     3.- Valor: nombre de la columna del dataTable que mostrara el valor en el combo
    ///                     3.- Seleccion: Id del combo el cual aparecera como seleccionado por default
    ///               CREO: Jesus S. Toledo Rdz.
    ///         FECHA_CREO: 06/9/2010
    ///           MODIFICO:
    ///     FECHA_MODIFICO:
    /// CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_Combo_ID(DropDownList Obj_DropDownList, DataTable Dt_Temporal, String _Texto, String _Valor, String Seleccion)
    {
        String Texto = "";
        try
        {
            Obj_DropDownList.Items.Clear();
            Obj_DropDownList.Items.Add(new ListItem("< SELECCIONAR >", "0"));
            foreach (DataRow row in Dt_Temporal.Rows)
            {
                if (_Texto.Contains("+"))
                {
                    String[] Array_Texto = _Texto.Split('+');

                    foreach (String Campo in Array_Texto)
                    {
                        Texto = Texto + row[Campo].ToString();
                        Texto = Texto + "  ";
                    }
                }
                else
                {
                    Texto = row[_Texto].ToString();
                }
                Obj_DropDownList.Items.Add(new ListItem(Texto, row[_Valor].ToString()));
                Texto = "";
            }
            Obj_DropDownList.SelectedValue = Seleccion;
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }

    }
    public void Llenar_Combo_ID(DropDownList Obj_DropDownList)
    {
        try
        {
            Obj_DropDownList.Items.Clear();
            Obj_DropDownList.Items.Add(new ListItem("< SELECCIONAR >", "0"));
            Obj_DropDownList.SelectedValue = "0";
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }

    }
    ///****************************************************************************************
    ///NOMBRE DE LA FUNCION:Mensaje_Error
    ///DESCRIPCION : Muestra el error
    ///PARAMETROS  : P_Texto: texto de un TextBox
    ///CREO        : Toledo Rodriguez Jesus S.
    ///FECHA_CREO  : 04-Septiembre-2010
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACION:
    ///****************************************************************************************
    private void Mensaje_Error(String P_Mensaje)
    {
        Img_Error.Visible = true;
        Lbl_Mensaje_Error.Text += P_Mensaje + "</br>";
    }
    private void Mensaje_Error()
    {

        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Lbl_Ecabezado_Mensaje.Text = "";
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Estado_Botones
    ///DESCRIPCIÓN: Metodo para establecer el estado de los botones y componentes del formulario
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 02/02/2011 05:49:53 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Estado_Botones(int P_Estado)
    {
        Boolean Estado = false;
        switch (P_Estado)
        {
            case 0: //Estado inicial  
                Btn_Modificar.AlternateText = "Modificar";
                Btn_Salir.AlternateText = "Inicio";

                Btn_Modificar.ToolTip = "Modificar";
                Btn_Salir.ToolTip = "Inicio";

                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";

                Btn_Modificar.Visible = true;
                Btn_Salir.Visible = true;
                Div_Asignar_Refacciones.Style.Value = "display:none;";
                Div_Datos_Solicitud.Style.Value = "display:none;";
                Div_Servicios_Preventivos.Style.Value = "display:inline;";

                Estado = false;
                //Configuracion_Acceso("Frm_Cat_Tal_Servicios_Preventivos.aspx");
                break;

            case 1: //Seleccionado  
                Btn_Modificar.AlternateText = "Modificar";
                Btn_Salir.AlternateText = "Cancelar";

                Btn_Modificar.ToolTip = "Modificar";
                Btn_Salir.ToolTip = "Cancelar";

                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";

                Btn_Modificar.Visible = false;
                Btn_Salir.Visible = true;
                Div_Asignar_Refacciones.Style.Value = "display:none;";
                Div_Servicios_Preventivos.Style.Value = "display:inline;";
                Div_Datos_Solicitud.Style.Value = "display:inline;";
                Estado = true;
                break;

            case 2: //Modificar                    

                Btn_Modificar.AlternateText = "Actualizar";
                Btn_Salir.AlternateText = "Cancelar";

                Btn_Modificar.ToolTip = "Actualizar";
                Btn_Salir.ToolTip = "Cancelar";

                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";

                Div_Asignar_Refacciones.Style.Value = "display:inline;";
                Div_Servicios_Preventivos.Style.Value = "display:none;";
                Div_Datos_Solicitud.Style.Value = "display:inline;";

                Btn_Modificar.Visible = true;
                Btn_Salir.Visible = true;

                Estado = true;
                break;

        }

        //Txt_Descripcion.Enabled = Estado;
        //Txt_Nombre.Enabled = Estado;
        //Cmb_Estatus.Enabled = Estado;

    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
    ///DESCRIPCIÓN: Metodo para establecer el estado de los botones y componentes del formulario
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 02/02/2011 05:49:53 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Limpiar_Formulario()
    {
        Session["Tabla_Servicios"] = null;
        Session["Tabla_Refacciones"] = null;
        Session["Tabla_Refacciones_Seleccionados"] = null;
        Session["Tabla_Refacciones_Modificados"] = null;
        Grid_Refacciones.PageIndex = 0;
        Grid_Refacciones.SelectedIndex = (-1);
        Grid_Refacciones.DataSource = null;
        Grid_Refacciones.DataBind();        
        Hdf_No_Solicitud.Value = null;
        Hdf_No_Entrada.Value = null;
        Hdf_No_Servicio.Value = null;
        Hdf_Vehiculo_ID.Value = null;
        //Grid_Refacciones.PageIndex = 0;
        //Grid_Refacciones.SelectedIndex = (-1);
        //Borrar_Ventana_Emergente_Busqueda_Refacciones();
    }

    #endregion

    #region Metodos Operacion
    ///*******************************************************************************************************
    /// NOMBRE_FUNCIÓN: Formar_Tabla_Refacciones
    /// DESCRIPCIÓN: Crear tabla con columnas para almacenar refacciones seleccionadas
    /// PARÁMETROS:
    /// CREO: Jesus Toledo
    /// FECHA_CREO: 01-may-2012
    /// MODIFICÓ: 
    /// FECHA_MODIFICÓ: 
    /// CAUSA_MODIFICACIÓN: 
    ///*******************************************************************************************************
    private DataTable Formar_Tabla_Refacciones()
    {
        // tabla y columnas
        DataTable Dt_Refacciones = new DataTable();

        // agregar columnas a la tabla        
        Dt_Refacciones.Columns.Add("REFACCION_ID", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("CLAVE", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("CANTIDAD", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("NOMBRE", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("DESCRIPCION", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("SELECCIONADO", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("TIPO", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("ESTATUS", System.Type.GetType("System.String"));
        // regresar tabla
        return Dt_Refacciones;
    }

    #endregion

    #endregion

    #region Eventos
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Salir/Cancelar
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 10/Mayo/2011 12:55:33 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Salir.AlternateText.Equals("Inicio"))
            {
                Limpiar_Formulario();
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Estado_Botones(Const_Estado_Inicial);
                Limpiar_Formulario();
                Llenar_Listado_Servicios();
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
    ///DESCRIPCIÓN: se obtienen los datos para modificar los paramentros
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 06/27/2011 11:10:44 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (!String.IsNullOrEmpty(Hdf_No_Solicitud.Value))
            {
                if (Btn_Modificar.AlternateText == "Modificar")
                {
                    Estado_Botones(Const_Estado_Modificar);
                }
                else if (Btn_Modificar.AlternateText == "Actualizar")
                {
                    Modificar_Servicio();
                }
            }
            
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message.ToString());
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Click
    ///DESCRIPCIÓN: Evento que busca los registros
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 10/Mayo/2011 12:55:33 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        Llenar_Listado_Servicios();
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Evento que agarra la session de las refacciones seleccionadas
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 10/Mayo/2011 12:55:33 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    protected void Btn_Busqueda_Refacciones_Click(object sender, ImageClickEventArgs e)
    {
        Session["Tabla_Refacciones_B"] = null;
        Session["Tabla_Refacciones_Modificados_B"] = null;
        DataTable Dt_Refacciones = Formar_Tabla_Refacciones();
        DataTable Dt_Refacciones_Seleccionados = Formar_Tabla_Refacciones();
        DataRow[] Dr_Buscar_Refaccion = null;
        if (Session["Tabla_Refacciones_Seleccionados"] != null)
        {
            Dt_Refacciones_Seleccionados = (DataTable)Session["Tabla_Refacciones_Seleccionados"];
            if (Session["Tabla_Refacciones_Modificados"] != null)
                Dt_Refacciones = (DataTable)Session["Tabla_Refacciones_Modificados"];
            else if (Session["Tabla_Refacciones"] != null)
                Dt_Refacciones = (DataTable)Session["Tabla_Refacciones"];

            if (Dt_Refacciones.Rows.Count > 0 || Dt_Refacciones_Seleccionados.Rows.Count > 0)
            {
                foreach (DataRow Dr_Sel in Dt_Refacciones_Seleccionados.Rows)
                {
                    Dr_Buscar_Refaccion = Dt_Refacciones.Select(Cat_Tal_Refacciones.Campo_Refaccion_ID + " = " + Dr_Sel[Cat_Tal_Refacciones.Campo_Refaccion_ID].ToString());
                    if (Dr_Buscar_Refaccion.Length <= 0)
                    {
                        Dt_Refacciones.ImportRow(Dr_Sel);
                        Dt_Refacciones.AcceptChanges();
                    }
                }
                Session["Tabla_Refacciones_Modificados"] = Dt_Refacciones.Copy();
            }

            Cargar_Refacciones(0);
            Session["Tabla_Refacciones_Seleccionados"] = null;
        }
        //Session["Tabla_Refacciones"] = null;
    }    
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Txt_Buscar_TextChanged
    ///DESCRIPCIÓN: Busqueda Principal del catalogo
    //////PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 03/Ago/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    protected void Txt_Grid_Cantidad_TextChanged(object sender, EventArgs e)
    {
        Chk_Seleccionar_CheckedChanged(sender, e);
        TextBox Txt_Cantidad = null;
        DataTable Dt_Refacciones = new DataTable();
        if (Session["Tabla_Refacciones_Modificados"] != null)
            Dt_Refacciones = (DataTable)Session["Tabla_Refacciones_Modificados"];
        else if (Session["Tabla_Refacciones"] != null)
            Dt_Refacciones = (DataTable)Session["Tabla_Refacciones"];
        try
        {
            if (Dt_Refacciones.Rows.Count > 0)
            {
                for (Int32 Cont = 0; Cont < Grid_Refacciones.Rows.Count; Cont++)
                {
                    Txt_Cantidad = (TextBox)Grid_Refacciones.Rows[Cont].Cells[1].FindControl("Txt_Grid_Cantidad");

                    Dt_Refacciones.Rows[Cont + (Grid_Refacciones.PageIndex * Grid_Refacciones.PageSize)]["CANTIDAD"] = Txt_Cantidad.Text.Trim();
                }
            }
            if (Dt_Refacciones.Rows.Count > 0)
                Session["Tabla_Refacciones_Modificados"] = Dt_Refacciones;
            Cargar_Refacciones(Grid_Refacciones.PageIndex);
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
        
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Grid_Ordenes_Variacion_PageIndexChanging
    ///DESCRIPCIÓN: Paginar grid de ordenes de variacion
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 01/Dic/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Grid_Refacciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Cargar_Refacciones(e.NewPageIndex);
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Chk_Seleccionar_CheckedChanged
    ///DESCRIPCIÓN: Seleccionar Refaccion
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 26/Abr/2012 03:03:06 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Chk_Seleccionar_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox Chk_Check_Seleccionar = null;
        TextBox Txt_Cantidad = null;
        DataTable Dt_Refacciones = new DataTable();
        if (Session["Tabla_Refacciones_Modificados"] != null)
            Dt_Refacciones = (DataTable)Session["Tabla_Refacciones_Modificados"];
        else if (Session["Tabla_Refacciones"] != null)
            Dt_Refacciones = (DataTable)Session["Tabla_Refacciones"];
        try
        {
            //Chk_Check_Seleccionar = (CheckBox)sender;
            //Refaccion_Seleccionado = Chk_Check_Seleccionar.Comman;
            if (Dt_Refacciones.Rows.Count > 0)
            {
                for (Int32 Cont = 0; Cont < Grid_Refacciones.Rows.Count; Cont++)
                {
                    Chk_Check_Seleccionar = (CheckBox)Grid_Refacciones.Rows[Cont].Cells[0].FindControl("Chk_Seleccionar");
                    Txt_Cantidad = (TextBox)Grid_Refacciones.Rows[Cont].Cells[1].FindControl("Txt_Grid_Cantidad");
                    if (Chk_Check_Seleccionar.Checked)
                    {
                        Dt_Refacciones.Rows[Cont + (Grid_Refacciones.PageIndex * Grid_Refacciones.PageSize)][Cat_Tal_Refacciones.Campo_Refaccion_ID] = Grid_Refacciones.DataKeys[Cont].Values["REFACCION_ID"];
                        Dt_Refacciones.Rows[Cont + (Grid_Refacciones.PageIndex * Grid_Refacciones.PageSize)]["SELECCIONADO"] = "SI";
                        Dt_Refacciones.Rows[Cont + (Grid_Refacciones.PageIndex * Grid_Refacciones.PageSize)]["CANTIDAD"] = Txt_Cantidad.Text.Trim();
                    }
                    else
                    {
                        Dt_Refacciones.Rows[Cont + (Grid_Refacciones.PageIndex * Grid_Refacciones.PageSize)][Cat_Tal_Refacciones.Campo_Refaccion_ID] = Grid_Refacciones.DataKeys[Cont].Values["REFACCION_ID"];
                        Dt_Refacciones.Rows[Cont + (Grid_Refacciones.PageIndex * Grid_Refacciones.PageSize)]["SELECCIONADO"] = "NO";
                        Dt_Refacciones.Rows[Cont + (Grid_Refacciones.PageIndex * Grid_Refacciones.PageSize)]["CANTIDAD"] = "";
                        Txt_Cantidad.Text = "";
                    }

                }
            }
            if (Dt_Refacciones.Rows.Count > 0)
            {
                Session["Tabla_Refacciones_Modificados"] = Dt_Refacciones;
                Cargar_Refacciones(Grid_Refacciones.PageIndex);
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }    
    #endregion
    protected void Btn_Asignacion_Click(object sender, ImageClickEventArgs e)
    {
        Btn_Modificar_Click(sender, e);
    }
}