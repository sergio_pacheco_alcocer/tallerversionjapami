﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Tal_Rpt_Refacciones.aspx.cs" Inherits="paginas_Taller_Mecanico_Frm_Rpt_Refacciones" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <script type="text/javascript">
        window.onerror = new Function("return true");
        //Abrir una ventana modal
        function Abrir_Ventana_Modal(Url, Propiedades) {
            window.showModalDialog(Url, null, Propiedades);
        }
        function Validar_Longitud_Texto(Text_Box, Max_Longitud) {
            if (Text_Box.value.length > Max_Longitud) {
                Text_Box.value = Text_Box.value.substring(0, Max_Longitud);
            }
        }
        function Limpiar_Ctlr_Busqueda_Vehiculo() {
            document.getElementById("<%=Hdf_Vehiculo_ID.ClientID%>").value = "";
            document.getElementById("<%=Txt_No_Inventario.ClientID%>").value = "";
            document.getElementById("<%=Txt_No_Economico.ClientID%>").value = "";
            return false;
        }
        function Limpiar_Ctlr_General() {
            document.getElementById("<%=Cmb_Unidad_Responsable.ClientID%>").value = "";
            document.getElementById("<%=Cmb_Tipo_Servicio.ClientID%>").value = "";
            document.getElementById("<%=Cmb_Tipos_Trabajo.ClientID%>").value = "";
            document.getElementById("<%=Rdb_Tipo_Reporte.ClientID%>").rows[0].cells[0].childNodes[0].checked=true;;
            document.getElementById("<%=Cmb_Lista_Mecanicos.ClientID%>").value = "";
            document.getElementById("<%=Txt_Fecha_Recepcion_Inicial.ClientID%>").value = "";
            document.getElementById("<%=Txt_Fecha_Recepcion_Final.ClientID%>").value = "";
            document.getElementById("<%=Pnl_Mecanicos.ClientID%>").style.display = "none";
            document.getElementById("<%=Pnl_Unidad_Responsable.ClientID%>").style.display = "inline";
            document.getElementById("<%=Pnl_Vehiculo_Seleccionado.ClientID%>").style.display = "none";
            Limpiar_Ctlr_Busqueda_Vehiculo();
            return false;
        }
        function Rdb_Tipo_Reporte_Selected() 
        {
            var rbl = document.getElementById("<%=Rdb_Tipo_Reporte.ClientID%>");
            if (rbl.rows[0].cells[0].childNodes[0].checked) 
            {
                document.getElementById("<%=Pnl_Mecanicos.ClientID%>").style.display = "none";
                document.getElementById("<%=Pnl_Unidad_Responsable.ClientID%>").style.display = "inline";
                document.getElementById("<%=Pnl_Vehiculo_Seleccionado.ClientID%>").style.display = "none";
            }
            if (rbl.rows[1].cells[0].childNodes[0].checked) {
                document.getElementById("<%=Pnl_Mecanicos.ClientID%>").style.display = "none";
                document.getElementById("<%=Pnl_Unidad_Responsable.ClientID%>").style.display = "none";
                document.getElementById("<%=Pnl_Vehiculo_Seleccionado.ClientID%>").style.display = "inline";
            }
            if (rbl.rows[2].cells[0].childNodes[0].checked) {
                document.getElementById("<%=Pnl_Mecanicos.ClientID%>").style.display = "inline";
                document.getElementById("<%=Pnl_Unidad_Responsable.ClientID%>").style.display = "none";
                document.getElementById("<%=Pnl_Vehiculo_Seleccionado.ClientID%>").style.display = "none";
            }
            return true;
        }
    </script>
    <script type="text/javascript" language="javascript">
    //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_sesiones.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion()
        {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para matener la sesión activa
        setInterval('MantenSesion()', <%=(int)(0.9*(Session.Timeout * 60000))%>);
        function formatCurrency(num) {
            var Combo = "";
            var Beneficio;
                num = num.toString().replace(/\$|\,/g,'');
                if(isNaN(num))
                num = "0";
                sign = (num == (num = Math.abs(num)));
                num = Math.floor(num*100+0.50000000001);
                cents = num%100;
                num = Math.floor(num/100).toString();
                if(cents<10)
                    cents = "0" + cents;
                    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
                    num = num.substring(0,num.length-(4*i+3))+','+
                    num.substring(num.length-(4*i+3));
                    return (((sign)?'':'-') + num + '.' + cents);
            }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScptM_Operacion" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Requisitos" style="background-color: #ffffff; width: 100%; height: 100%;">
                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo" colspan="2">
                            Reporte de Refacciones
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div id="Div_Contenedor_Msj_Error" style="width: 98%;" runat="server" visible="false">
                                <table style="width: 100%;">
                                    <tr>
                                        <td colspan="2" align="left">
                                            <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                Width="24px" Height="24px" />
                                            <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%;">
                                        </td>
                                        <td style="width: 90%; text-align: left;" valign="top">
                                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" style="width: 50%;">
                            <asp:ImageButton ID="Btn_Reporte_PDF" runat="server" OnClick="Btn_Reporte_PDF_Click"
                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" Width="24px" CssClass="Img_Button"
                                AlternateText="Modificar" />
                            <asp:ImageButton ID="Btn_Reporte_EXCEL" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png" Width="24px" CssClass="Img_Button"
                                AlternateText="Salir" Visible="false"/>
                        </td>
                        <td style="width: 50%;">
                            &nbsp;&nbsp;
                            <asp:ImageButton ID="Btn_Limpiar_Todo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_Limpiar.png"
                                AlternateText="Limpiar Todo" ToolTip="Limpiar Todo" Width="16px" Height="16px"
                                OnClientClick="javascript:return Limpiar_Ctlr_General();" />
                        </td>
                    </tr>
                </table>
                <div id="Div_Datos_Vehiculo" runat="server" style="width: 100%;">
                    <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                            <td colspan="4">
                                <asp:Panel ID="Pnl_Fecha_Recepcion" runat="server" Width="99%" GroupingText="Fecha de Recepción">
                                    <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td style="width: 15%;">
                                                <asp:Label ID="Lbl_Fecha_Recepcion_Inicial" runat="server" Text="Fecha Inicial"></asp:Label>
                                            </td>
                                            <td style="width: 35%;">
                                                <asp:TextBox ID="Txt_Fecha_Recepcion_Inicial" runat="server" Width="80%"></asp:TextBox>
                                                <asp:ImageButton ID="Btn_Fecha_Recepcion_Inicial" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                                <cc1:CalendarExtender ID="CE_Txt_Fecha_Recepcion_Inicial" runat="server" TargetControlID="Txt_Fecha_Recepcion_Inicial"
                                                    PopupButtonID="Btn_Fecha_Recepcion_Inicial" Format="dd/MMM/yyyy">
                                                </cc1:CalendarExtender>                                                 
                                            </td>
                                            <td style="width: 15%;">
                                                <asp:Label ID="Lbl_Fecha_Recepcion_Final" runat="server" Text="Fecha Final"></asp:Label>
                                            </td>
                                            <td style="width: 35%;">
                                                <asp:TextBox ID="Txt_Fecha_Recepcion_Final" runat="server" Width="80%"></asp:TextBox>
                                                <asp:ImageButton ID="Btn_Fecha_Recepcion_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                                <cc1:CalendarExtender ID="CE_Txt_Fecha_Recepcion_Final" runat="server" TargetControlID="Txt_Fecha_Recepcion_Final"
                                                    PopupButtonID="Btn_Fecha_Recepcion_Final" Format="dd/MMM/yyyy">
                                                </cc1:CalendarExtender>                                                 
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Panel ID="Pnl_Datos_Servicios" runat="server" Width="99%" GroupingText="Servicios">
                                    <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td style="width: 15%;">
                                                <asp:Label ID="Lbl_Tipo_Servicio" runat="server" Text="Tipo de Servicio"></asp:Label>
                                            </td>
                                            <td style="width: 35%;">
                                                <asp:DropDownList ID="Cmb_Tipo_Servicio" runat="server" Width="98%">
                                                    <asp:ListItem Value="">&lt; - - TODOS - - &gt;</asp:ListItem>
                                                    <asp:ListItem Value="SERVICIO_CORRECTIVO">SERVICIO CORRECTIVO</asp:ListItem>
                                                    <asp:ListItem Value="SERVICIO_PREVENTIVO">SERVICIO PREVENTIVO</asp:ListItem>                                                    
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 15%;">
                                                <asp:Label ID="Lbl_Reparacion" runat="server" Text="Reparación"></asp:Label>
                                            </td>
                                            <td style="width: 35%;">
                                                <asp:DropDownList ID="Cmb_Reparacion" runat="server" Width="98%">
                                                    <asp:ListItem Value="">&lt; - - TODAS - - &gt;</asp:ListItem>
                                                    <asp:ListItem Value="INTERNA">INTERNA</asp:ListItem>
                                                    <asp:ListItem Value="EXTERNA">EXTERNA</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Panel ID="Pnl_Tipos_Trabajo" runat="server" Width="99%" GroupingText="Tipos de Trabajo">
                                    <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td style="width: 15%;">
                                                <asp:Label ID="Label1" runat="server" Text="Tipo de Trabajo"></asp:Label>
                                            </td>
                                            <td style="width: 85%;" colspan="3">
                                                <asp:DropDownList ID="Cmb_Tipos_Trabajo" runat="server" Width="99%">                                                    
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;">
                                Filtrar Por
                            </td>
                            <td style="width: 35%;">
                                <asp:RadioButtonList ID="Rdb_Tipo_Reporte" runat="server" AutoPostBack="false">
                                    <asp:ListItem Value="1" OnClick="javascript:return Rdb_Tipo_Reporte_Selected();">U. Reponsable</asp:ListItem>
                                    <asp:ListItem Value="2" OnClick="javascript:return Rdb_Tipo_Reporte_Selected();">No. Inventario</asp:ListItem>
                                    <asp:ListItem Value="3" OnClick="javascript:return Rdb_Tipo_Reporte_Selected();">Mecanico</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td style="width: 15%;">
                            </td>
                            <td style="width: 35%;">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Panel ID="Pnl_Vehiculo_Seleccionado" runat="server" Width="99%" GroupingText="Vehículo">
                                    <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td style="width: 15%;">
                                                <asp:HiddenField ID="Hdf_Vehiculo_ID" runat="server" />
                                                <asp:Label ID="Lbl_No_Inventario" runat="server" Text="No. Inventario" />
                                            </td>
                                            <td style="width: 35%;">
                                                <asp:TextBox ID="Txt_No_Inventario" runat="server" Width="70%" Enabled="false"></asp:TextBox>
                                                <asp:ImageButton ID="Btn_Limpiar_Vehiculo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_Limpiar.png"
                                                    AlternateText="Limpiar" ToolTip="Limpiar" Width="16px" Height="16px" OnClientClick="javascript:return Limpiar_Ctlr_Busqueda_Vehiculo();" />
                                                <asp:ImageButton ID="Btn_Busqueda_Avanzada_Vehiculo" runat="server" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png"
                                                    Width="16px" AlternateText="Busqueda Avanzada" ToolTip="Busqueda Avanzada" OnClick="Btn_Busqueda_Avanzada_Vehiculo_Click" />
                                            </td>
                                            <td style="width: 15%;">
                                                &nbsp;&nbsp;
                                                <asp:Label ID="Lbl_No_Economico" runat="server" Text="No. Economico"></asp:Label>
                                            </td>
                                            <td style="width: 35%;">
                                                <asp:TextBox ID="Txt_No_Economico" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Panel ID="Pnl_Unidad_Responsable" runat="server" Width="99%" GroupingText="Unidad Responsable">
                                    <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td style="width: 15%;">
                                                <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text="U. Responsable" />
                                            </td>
                                            <td colspan="3" style="width: 85%;">
                                                <asp:DropDownList ID="Cmb_Unidad_Responsable" runat="server" Width="99%">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Panel ID="Pnl_Mecanicos" runat="server" Width="99%" GroupingText="Lista de Mecanicos">
                                    <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td style="width: 15%;">
                                                <asp:Label ID="Lbl_Mecanicos" runat="server" Text="Mecánicos" />
                                            </td>
                                            <td colspan="3" style="width: 85%;">
                                                <asp:DropDownList ID="Cmb_Lista_Mecanicos" runat="server" Width="99%">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <br />
            <br />
            <br />
            <br />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Btn_Reporte_PDF" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpPnl_MPE_Busqueda_Vehiculo" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_Busqueda_Vehiculo" runat="server" Text="Button" Style="display: none;" />
            <cc1:ModalPopupExtender ID="MPE_Busqueda_Vehiculo" runat="server" TargetControlID="Btn_Comodin_Busqueda_Vehiculo"
                PopupControlID="Pnl_Busqueda_Vehiculo" CancelControlID="Btn_Cerrar_Busqueda_Vehiculo"
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Pnl_Busqueda_Vehiculo" runat="server" CssClass="drag" HorizontalAlign="Center"
        Style="display: none; border-style: outset; border-color: Silver; width: 760px;">
        <center>
            <asp:Panel ID="Pnl_Busqueda_Vehiculo_Interno" runat="server" CssClass="estilo_fuente"
                Style="cursor: move; background-color: Silver; color: Black; font-size: 12; font-weight: bold;
                border-style: outset;">
                <table class="estilo_fuente" width="100%">
                    <tr>
                        <td style="color: Black; font-size: 12; font-weight: bold; width: 90%">
                            <asp:Image ID="Img_Encabezado_Busqueda_Vehiculos" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                            Busqueda de Vehículos
                        </td>
                        <td align="right">
                            <asp:ImageButton ID="Btn_Cerrar_Busqueda_Vehiculo" CausesValidation="false" runat="server"
                                Style="cursor: pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:UpdatePanel ID="UpPnl_Busqueda_Vehiculo" runat="server">
                <ContentTemplate>
                    <asp:UpdateProgress ID="UpPrg_Busqueda_Vehiculo" runat="server" AssociatedUpdatePanelID="UpPnl_Busqueda_Vehiculo"
                        DisplayAfter="0">
                        <ProgressTemplate>
                            <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                            </div>
                            <div class="processMessage" id="div_progress">
                                <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <br />
                    <cc1:TabContainer ID="Tab_Contenedor_Pestagnas_Busqueda_Vehiculo" runat="server"
                        Width="100%" ActiveTabIndex="1" CssClass="Tab">
                        <cc1:TabPanel runat="server" HeaderText="Tab_Panel_Datos_Generales_Busqueda_Vehiculo"
                            ID="Tab_Panel_Datos_Generales_Busqueda_Vehiculo" Width="100%" Height="400px"
                            BackColor="White">
                            <HeaderTemplate>
                                Generales</HeaderTemplate>
                            <ContentTemplate>
                                <div style="border-style: outset; width: 99.5%; height: 200px; background-color: White;">
                                    <table width="100%">
                                        <tr>
                                            <td style="text-align: left;" colspan="4">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Numero_Inventario" runat="server" Text="Número Inventario"
                                                    CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Busqueda_Vehiculo_Numero_Inventario" runat="server" Width="97%"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_Numero_Inventario" runat="server"
                                                    TargetControlID="Txt_Busqueda_Vehiculo_Numero_Inventario" FilterType="Numbers"
                                                    Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Numero_Economico" runat="server" Text="Número Económico"
                                                    CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Busqueda_Vehiculo_Numero_Economico" runat="server" Width="97%"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_Numero_Economico" runat="server"
                                                    TargetControlID="Txt_Busqueda_Vehiculo_Numero_Economico" InvalidChars="<,>,&,',!,"
                                                    FilterType="Numbers" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ -_" Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Modelo" runat="server" Text="Modelo" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Busqueda_Vehiculo_Modelo" runat="server" Width="97%"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_Modelo" runat="server"
                                                    TargetControlID="Txt_Busqueda_Vehiculo_Modelo" InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ -_" Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Marca" runat="server" Text="Marca" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Marca" runat="server" Width="100%">
                                                    <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Anio_Fabricacion" runat="server" Text="Año Fabricación"
                                                    CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Busqueda_Vehiculo_Anio_Fabricacion" runat="server" Width="97%"
                                                    MaxLength="4"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_Anio_Fabricacion" runat="server"
                                                    TargetControlID="Txt_Busqueda_Vehiculo_Anio_Fabricacion" FilterType="Numbers"
                                                    Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Color" runat="server" Text="Color" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Color" runat="server" Width="100%">
                                                    <asp:ListItem Text="&lt; TODOS &gt;" Value="TODOS"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Dependencias" runat="server" Text="U. Responsable"
                                                    CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 80%; text-align: left;" colspan="3">
                                                <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Dependencias" runat="server" Width="85%">
                                                    <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:ImageButton ID="Btn_Buscar_Datos_Vehiculo" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                                    CausesValidation="False" OnClick="Btn_Buscar_Datos_Vehiculo_Click" ToolTip="Buscar Contrarecibos" />
                                                <asp:ImageButton ID="Btn_Limpiar_Filtros_Buscar_Datos_Vehiculo" runat="server" OnClick="Btn_Limpiar_Filtros_Buscar_Datos_Vehiculo_Click"
                                                    CausesValidation="False" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png"
                                                    ToolTip="Limpiar Filtros" Width="20px" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel runat="server" HeaderText="Tab_Panel_Reguardantes_Busqueda_Vehiculo"
                            ID="Tab_Panel_Resguardantes_Busqueda_Vehiculo" Width="100%" BackColor="White">
                            <HeaderTemplate>
                                Resguardantes</HeaderTemplate>
                            <ContentTemplate>
                                <div style="border-style: outset; width: 99.5%; height: 200px; background-color: White;">
                                    <table width="100%">
                                        <tr>
                                            <td style="text-align: left;" colspan="2">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_RFC_Resguardante" runat="server" Text="RFC Reguardante"
                                                    CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Busqueda_Vehiculo_RFC_Resguardante" runat="server" Width="200px"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_RFC_Resguardante" runat="server"
                                                    TargetControlID="Txt_Busqueda_Vehiculo_RFC_Resguardante" InvalidChars="<,>,&,',!,"
                                                    FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ "
                                                    Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_No_Empleado" runat="server" Text="No. Empleado"
                                                    CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Busqueda_Vehiculo_No_Empleado" runat="server" Width="97%"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_No_Empleado" runat="server"
                                                    TargetControlID="Txt_Busqueda_Vehiculo_No_Empleado" InvalidChars="<,>,&,',!,"
                                                    FilterType="Numbers" Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Resguardantes_Dependencias" runat="server" Text="U. Responsable"
                                                    CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td colspan="3" style="text-align: left;">
                                                <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias" runat="server"
                                                    Width="100%" AutoPostBack="true" OnSelectedIndexChanged="Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias_SelectedIndexChanged">
                                                    <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Nombre_Resguardante" runat="server" Text="Resguardante"
                                                    CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td colspan="3" style="text-align: left;">
                                                <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Nombre_Resguardante" runat="server" Width="100%">
                                                    <asp:ListItem Text="&lt;TODOS&gt;" Value="TODOS"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="text-align: right">
                                                <asp:ImageButton ID="Btn_Buscar_Resguardante_Vehiculo" runat="server" CausesValidation="False"
                                                    ImageUrl="~/paginas/imagenes/paginas/busqueda.png" OnClick="Btn_Buscar_Resguardante_Vehiculo_Click"
                                                    ToolTip="Buscar Listados" />
                                                <asp:ImageButton ID="Btn_Limpiar_Filtros_Buscar_Resguardante_Vehiculo" runat="server"
                                                    CausesValidation="False" OnClick="Btn_Limpiar_Filtros_Buscar_Resguardante_Vehiculo_Click"
                                                    ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png" Width="20px" ToolTip="Limpiar Filtros" />
                                                &nbsp;&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ContentTemplate>
                        </cc1:TabPanel>
                    </cc1:TabContainer>
                    <div style="width: 99%; height: 150px; overflow: auto; border-style: outset; background-color: White;">
                        <center>
                            <caption>
                                <asp:GridView ID="Grid_Listado_Busqueda_Vehiculo" runat="server" AllowPaging="true"
                                    AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" OnPageIndexChanging="Grid_Listado_Busqueda_Vehiculo_PageIndexChanging"
                                    OnSelectedIndexChanged="Grid_Listado_Busqueda_Vehiculo_SelectedIndexChanged"
                                    Width="98%" PageSize="100">
                                    <RowStyle CssClass="GridItem" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                    <Columns>
                                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                            <ItemStyle Width="30px" />
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="VEHICULO_ID" HeaderText="VEHICULO_ID" SortExpression="VEHICULO_ID" />
                                        <asp:BoundField DataField="NUMERO_INVENTARIO" HeaderText="No. Inven." SortExpression="NUMERO_INVENTARIO">
                                            <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NUMERO_ECONOMICO" HeaderText="No. Eco." SortExpression="NUMERO_ECONOMICO">
                                            <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="VEHICULO" HeaderText="Vehículo" SortExpression="VEHICULO">
                                            <ItemStyle Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="MARCA" HeaderText="Marca" SortExpression="MARCA">
                                            <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="MODELO" HeaderText="Modelo" SortExpression="MODELO">
                                            <ItemStyle Width="150px" Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ANIO" HeaderText="Año" SortExpression="ANIO">
                                            <ItemStyle Width="70px" Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS">
                                            <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                    </Columns>
                                    <HeaderStyle CssClass="GridHeader" />
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                </asp:GridView>
                            </caption>
                        </center>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </center>
    </asp:Panel>
</asp:Content>

