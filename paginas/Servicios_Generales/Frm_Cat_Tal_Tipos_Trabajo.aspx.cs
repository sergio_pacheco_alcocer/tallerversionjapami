﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Taller_Mecanico.Catalogo_Tipos_Trabajo.Negocio;
using JAPAMI.Catalogo_Compras_Unidades.Negocio;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using JAPAMI.Catalogo_Compras_Impuestos.Negocio;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;

public partial class paginas_Taller_Municipal_Frm_Cat_Tal_Tipos_Trabajo : System.Web.UI.Page
{

    #region Variables
    private const int Const_Estado_Inicial = 0;
    private const int Const_Estado_Nuevo = 1;
    private const int Const_Estado_Modificar = 2;
    #endregion

    #region Page Load / Init
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!Page.IsPostBack)
            {
                Txt_Descripcion.Attributes.Add("onkeypress", " Validar_Longitud_Texto(this, 250);");
                Txt_Descripcion.Attributes.Add("onkeyup", " Validar_Longitud_Texto(this, 250);");
                Estado_Botones(Const_Estado_Inicial);
                Configurar_Formulario();
                //Configuracion_Acceso("Frm_Cat_Tal_Refacciones.aspx");
                Limpiar_Formulario();
                Cargar_Refacciones(0);
            }
            Mensaje_Error();
        }
        catch (Exception Ex)
        {
            //Mensaje_Error(Txt_Pagos.Text.Trim() +" - "+ Ex.Message);
            Estado_Botones(Const_Estado_Inicial);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Configurar_Formulario
    ///DESCRIPCIÓN: Cargar Combos y Datos del formulario
    ///CREO: jtoledo
    ///FECHA_CREO: 17/May/2012
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    private void Configurar_Formulario()
    {
        try
        {
            Limpiar_Formulario();
        }
        catch { }

    }
    #endregion

    #region Metodos

    #region Metodos Generales
    ///*******************************************************************************
    ///NOMBRE DE LA METODO: LLenar_Combo_Id
    ///        DESCRIPCIÓN: llena todos los combos
    ///         PARAMETROS: 1.- Obj_DropDownList: Combo a llenar
    ///                     2.- Dt_Temporal: DataTable genarada por una consulta a la base de datos
    ///                     3.- Texto: nombre de la columna del dataTable que mostrara el texto en el combo
    ///                     3.- Valor: nombre de la columna del dataTable que mostrara el valor en el combo
    ///                     3.- Seleccion: Id del combo el cual aparecera como seleccionado por default
    ///               CREO: Jesus S. Toledo Rdz.
    ///         FECHA_CREO: 06/9/2010
    ///           MODIFICO:
    ///     FECHA_MODIFICO:
    /// CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_Combo_ID(DropDownList Obj_DropDownList, DataTable Dt_Temporal, String _Texto, String _Valor, String Seleccion)
    {
        String Texto = "";
        try
        {
            Obj_DropDownList.Items.Clear();
            Obj_DropDownList.Items.Add(new ListItem("< SELECCIONAR >", "0"));
            foreach (DataRow row in Dt_Temporal.Rows)
            {
                if (_Texto.Contains("+"))
                {
                    String[] Array_Texto = _Texto.Split('+');

                    foreach (String Campo in Array_Texto)
                    {
                        Texto = Texto + row[Campo].ToString();
                        Texto = Texto + "  ";
                    }
                }
                else
                {
                    Texto = row[_Texto].ToString();
                }
                Obj_DropDownList.Items.Add(new ListItem(Texto, row[_Valor].ToString()));
                Texto = "";
            }
            Obj_DropDownList.SelectedValue = Seleccion;
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }

    }
    public void Llenar_Combo_ID(DropDownList Obj_DropDownList)
    {
        try
        {
            Obj_DropDownList.Items.Clear();
            Obj_DropDownList.Items.Add(new ListItem("< SELECCIONAR >", "0"));
            Obj_DropDownList.SelectedValue = "0";
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }

    }
    ///****************************************************************************************
    ///NOMBRE DE LA FUNCION:Mensaje_Error
    ///DESCRIPCION : Muestra el error
    ///PARAMETROS  : P_Texto: texto de un TextBox
    ///CREO        : Toledo Rodriguez Jesus S.
    ///FECHA_CREO  : 04-Septiembre-2010
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACION:
    ///****************************************************************************************
    private void Mensaje_Error(String P_Mensaje)
    {
        Img_Error.Visible = true;
        Lbl_Mensaje_Error.Text += P_Mensaje + "</br>";
    }
    private void Mensaje_Error()
    {
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Lbl_Ecabezado_Mensaje.Text = "";
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Estado_Botones
    ///DESCRIPCIÓN: Metodo para establecer el estado de los botones y componentes del formulario
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 02/02/2011 05:49:53 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Estado_Botones(int P_Estado)
    {
        Boolean Estado = false;
        switch (P_Estado)
        {
            case 0: //Estado inicial  
                Btn_Nuevo.AlternateText = "Nuevo";
                Btn_Modificar.AlternateText = "Modificar";
                Btn_Eliminar.AlternateText = "Eliminar";
                Btn_Salir.AlternateText = "Inicio";

                Btn_Nuevo.ToolTip = "Nuevo";
                Btn_Modificar.ToolTip = "Modificar";
                Btn_Eliminar.ToolTip = "Eliminar";
                Btn_Salir.ToolTip = "Inicio";

                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar.png";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";

                Btn_Nuevo.Visible = true;
                Btn_Modificar.Visible = true;
                Btn_Eliminar.Visible = true;
                Btn_Salir.Visible = true;
                Txt_Buscar.Enabled = true;
                Btn_Buscar.Visible = true;

                Estado = false;
                //Configuracion_Acceso("Frm_Cat_Tal_Refacciones.aspx");
                break;

            case 1: //Nuevo  
                Btn_Nuevo.AlternateText = "Guardar";
                Btn_Modificar.AlternateText = "Modificar";
                Btn_Eliminar.AlternateText = "Eliminar";
                Btn_Salir.AlternateText = "Cancelar";

                Btn_Nuevo.ToolTip = "Guardar";
                Btn_Modificar.ToolTip = "Modificar";
                Btn_Eliminar.ToolTip = "Eliminar";
                Btn_Salir.ToolTip = "Cancelar";

                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar.png";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";

                Btn_Nuevo.Visible = true;
                Btn_Modificar.Visible = false;
                Btn_Eliminar.Visible = false;
                Btn_Salir.Visible = true;
                Txt_Buscar.Enabled = false;
                Btn_Buscar.Visible = false;

                Estado = true;


                break;

            case 2: //Modificar                    

                Btn_Nuevo.AlternateText = "Nuevo";
                Btn_Modificar.AlternateText = "Actualizar";
                Btn_Eliminar.AlternateText = "Eliminar";
                Btn_Salir.AlternateText = "Cancelar";

                Btn_Nuevo.ToolTip = "Nuevo";
                Btn_Modificar.ToolTip = "Actualizar";
                Btn_Eliminar.ToolTip = "Eliminar";
                Btn_Salir.ToolTip = "Cancelar";

                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar.png";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";

                Btn_Nuevo.Visible = false;
                Btn_Modificar.Visible = true;
                Btn_Eliminar.Visible = false;
                Btn_Salir.Visible = true;
                Txt_Buscar.Enabled = false;
                Btn_Buscar.Visible = false;

                Estado = true;
                
                break;

        }

        Txt_Descripcion.Enabled = Estado;
        Txt_Nombre.Enabled = Estado;
        Cmb_Estatus.Enabled = Estado;

    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
    ///DESCRIPCIÓN: Metodo para establecer el estado de los botones y componentes del formulario
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 02/02/2011 05:49:53 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Limpiar_Formulario()
    {
        Session["Tabla_Trabajos"] = null;
        Txt_Clave.Text = "";
        Txt_Nombre.Text = "";
        Txt_Descripcion.Text = "";
        Cmb_Estatus.SelectedIndex = 0;

        Grid_Trabajos.PageIndex = 0;
        Grid_Trabajos.SelectedIndex = (-1);

    }

    #endregion

    #region Metodos ABC

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Modificar_Refaccion
    ///DESCRIPCIÓN: se obtienen los datos para modificar la refaccion
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 06/27/2011 11:12:18 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    private void Modificar()
    {
        Cls_Cat_Tal_Tipos_Trabajo_Negocio Negocio = new Cls_Cat_Tal_Tipos_Trabajo_Negocio();
        try
        {
            if (Validar_Campos())
            {
                Negocio.P_Tipo_Trabajo_ID = Grid_Trabajos.SelectedDataKey["TIPO_TRABAJO_ID"].ToString();
                
                Negocio.P_Nombre = Txt_Nombre.Text.Trim().ToUpper();
                Negocio.P_Descripcion = (Txt_Descripcion.Text.Trim().ToUpper().Length > 0) ? Txt_Descripcion.Text.Trim().ToUpper() : Txt_Nombre.Text.Trim().ToUpper();
                Negocio.P_Estatus = Cmb_Estatus.SelectedValue.ToString().Trim().ToUpper();
                Negocio.Modificar_Tipos_Trabajo();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Niveles", "alert('La modificacion fue Exitosa');", true);
                Estado_Botones(Const_Estado_Inicial);
                Limpiar_Formulario();
                Cargar_Refacciones(0);
            }

        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Alta
    ///DESCRIPCIÓN: se obtienen los datos para dar de alta la refaccion
    ///PARAMETROS: 
    ///CREO: jesus toledo
    ///FECHA_CREO: 05/Mayo/2012 11:12:18 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    private void Alta()
    {
        Cls_Cat_Tal_Tipos_Trabajo_Negocio Negocio = new Cls_Cat_Tal_Tipos_Trabajo_Negocio();
        try
        {
            if (Validar_Campos())
            {
                Negocio.P_Nombre = Txt_Nombre.Text.Trim().ToUpper();
                Negocio.P_Descripcion = (Txt_Descripcion.Text.Trim().ToUpper().Length > 0) ? Txt_Descripcion.Text.Trim().ToUpper() : Txt_Nombre.Text.Trim().ToUpper();
                Negocio.P_Estatus = Cmb_Estatus.SelectedValue.ToString().Trim().ToUpper();
                Negocio.Alta_Tipos_Trabajo();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Niveles", "alert('El alta fue Exitosa');", true);
                Estado_Botones(Const_Estado_Inicial);
                Limpiar_Formulario();
                Cargar_Refacciones(0);
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Eliminar_Refaccion
    ///DESCRIPCIÓN: se obtienen los datos para dar de alta la refaccion
    ///PARAMETROS: 
    ///CREO: jesus toledo
    ///FECHA_CREO: 05/Mayo/2012 11:12:18 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    private void Eliminar_Refaccion()
    {
        Cls_Cat_Tal_Tipos_Trabajo_Negocio Negocio = new Cls_Cat_Tal_Tipos_Trabajo_Negocio();
        try
        {
            if (Validar_Campos())
            {
                Negocio.P_Tipo_Trabajo_ID = Grid_Trabajos.SelectedDataKey["TIPO_TRABAJO_ID"].ToString();
                Negocio.P_Clave = Txt_Clave.Text.Trim().ToUpper();
                Negocio.P_Nombre = Txt_Nombre.Text.Trim().ToUpper();
                Negocio.P_Descripcion = Txt_Descripcion.Text.Trim().ToUpper();
                Negocio.P_Estatus = "BAJA";
                Negocio.Modificar_Tipos_Trabajo();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Niveles", "alert('Se dio de baja el registro');", true);
                Estado_Botones(Const_Estado_Inicial);
                Limpiar_Formulario();
                Cargar_Refacciones(0);
            }

        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA CLASE: Cargar_Refacciones
    ///DESCRIPCIÓN: consulta y muestra los datos, instancia la clase de negocios para acceder a la consulta
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 15/May/2012 06:24:15 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    private void Cargar_Refacciones(int Page_Index)
    {
        DataTable Dt_Niveles = new DataTable();
        Cls_Cat_Tal_Tipos_Trabajo_Negocio Negocio = new Cls_Cat_Tal_Tipos_Trabajo_Negocio();
        if (Session["Tabla_Trabajos"] != null)
            Dt_Niveles = (DataTable)Session["Tabla_Trabajos"];
        try
        {
            if (Session["Tabla_Trabajos"] != null)
            {
                Grid_Trabajos.PageIndex = Page_Index;
                Grid_Trabajos.DataSource = Dt_Niveles;
                Grid_Trabajos.DataBind();
            }
            else
            {
                Negocio.P_Nombre = Txt_Buscar.Text.Trim().ToUpper();
                Dt_Niveles = Negocio.Consultar_Tipos_Trabajo();

                if (Dt_Niveles.Rows.Count > 0)
                {
                    Session["Tabla_Trabajos"] = Dt_Niveles;
                    Grid_Trabajos.PageIndex = Page_Index;
                    Grid_Trabajos.DataSource = Dt_Niveles;
                    Grid_Trabajos.DataBind();
                }
                else
                {
                    Grid_Trabajos.DataSource = null;
                    Grid_Trabajos.DataBind();
                    Mensaje_Error("No se encontraron refacciones con la clave proporcionada");
                }
            }
            Grid_Trabajos.SelectedIndex = (-1);
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }

    #endregion

    #region Metodos/Validaciones
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Validar_Campos
    ///DESCRIPCIÓN: valdia que se ingresen los campos obligatorios
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 06/27/2011 11:31:53 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private Boolean Validar_Campos()
    {
        Boolean Resultado = true;

        if (Txt_Nombre.Text.Trim() == "")
        {
            Lbl_Ecabezado_Mensaje.Text = "Los siguientes datos son necesarios:";
            Mensaje_Error("- Nombre de la refacción");
            Resultado = false;
        }
        if (Cmb_Estatus.SelectedIndex <= 0)
        {
            Lbl_Ecabezado_Mensaje.Text = "Los siguientes datos son necesarios:";
            Mensaje_Error("- Estatus de la refacción");
            Resultado = false;
        }

        return Resultado;
    }
    #endregion

    #region Metodos Operacion
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Mostrar_Refaccion
    ///DESCRIPCIÓN: Cargar datos de Refaccion
    ///PARAMETROS: 
    ///CREO: Jesus Toledo
    ///FECHA_CREO: 05/May/2012
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************  
    private void Mostrar_Refaccion()
    {
        String Index_Refaccion;
        DataTable Dt_Refacciones;
        DataRow[] Dr_Seleccionado;
        try
        {
            if (Grid_Trabajos.SelectedIndex >= 0)
            {
                if (!String.IsNullOrEmpty(Grid_Trabajos.SelectedDataKey["TIPO_TRABAJO_ID"].ToString()))
                {
                    Index_Refaccion = Grid_Trabajos.SelectedDataKey["TIPO_TRABAJO_ID"].ToString();
                    if (Session["Tabla_Trabajos"] != null)
                    {
                        Dt_Refacciones = (DataTable)Session["Tabla_Trabajos"];
                        Dr_Seleccionado = Dt_Refacciones.Select(Cat_Tal_Tipos_Trabajo.Campo_Tipo_Trabajo_ID + " = " + Index_Refaccion);
                        if (Dr_Seleccionado.Length > 0)
                        {
                            Txt_Clave.Text = Dr_Seleccionado[0][Cat_Tal_Tipos_Trabajo.Campo_Clave].ToString();
                            Txt_Nombre.Text = Dr_Seleccionado[0][Cat_Tal_Tipos_Trabajo.Campo_Nombre].ToString();
                            Txt_Descripcion.Text = Dr_Seleccionado[0][Cat_Tal_Tipos_Trabajo.Campo_Descripcion].ToString();
                            Cmb_Estatus.SelectedValue = Dr_Seleccionado[0][Cat_Tal_Tipos_Trabajo.Campo_Estatus].ToString();
                        }
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    #endregion

    #endregion

    #region Eventos/Botones
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
    ///DESCRIPCIÓN: se obtienen los datos para dar de alta refaccion
    ///PARAMETROS: 
    ///CREO: Jesus Toledo Rodriguez
    ///FECHA_CREO: 05/Mayo/2012
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Nuevo.AlternateText == "Nuevo")
            {
                Limpiar_Formulario();
                Estado_Botones(Const_Estado_Nuevo);
            }
            else if (Btn_Nuevo.AlternateText == "Guardar")
            {
                Alta();
            }

        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message.ToString());
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
    ///DESCRIPCIÓN: se obtienen los datos para modificar los paramentros
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 06/27/2011 11:10:44 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Grid_Trabajos.SelectedIndex >= 0)
            {
                if (Btn_Modificar.AlternateText == "Modificar")
                {
                    Estado_Botones(Const_Estado_Modificar);
                }
                else if (Btn_Modificar.AlternateText == "Actualizar")
                {
                    Modificar();
                }
            }
            else
            {
                Mensaje_Error("Seleccione una refacción");
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message.ToString());
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Eliminar_Click
    ///DESCRIPCIÓN: se obtienen los datos para eliminar la refaccion
    ///PARAMETROS: 
    ///CREO: Jesus Toledo Rdz
    ///FECHA_CREO: 05/Mayo/2012 11:10:44 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Btn_Eliminar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Grid_Trabajos.SelectedIndex >= 0)
            {
                Eliminar_Refaccion();
            }
            else
            {
                Mensaje_Error("Seleccione un registro");
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message.ToString());
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Salir/Cancelar
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 10/Mayo/2011 12:55:33 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Salir.AlternateText.Equals("Inicio"))
            {
                Limpiar_Formulario();
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Estado_Botones(Const_Estado_Inicial);
                Limpiar_Formulario();
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Aceptar_Busqueda_Av_Click
    ///DESCRIPCIÓN: Evento para buscar la constancia
    ///PARAMETROS: 
    ///CREO: jesus toledo
    ///FECHA_CREO: 27/junio/2011 01:38:02 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Estado_Botones(Const_Estado_Inicial);
            Limpiar_Formulario();
            Session["Tabla_Trabajos"] = null;
            Cargar_Refacciones(0);
            Txt_Buscar.Text = "";
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Grid_Ordenes_Variacion_PageIndexChanging
    ///DESCRIPCIÓN: Paginar grid de ordenes de variacion
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 01/Dic/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Grid_Trabajos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Cargar_Refacciones(e.NewPageIndex);
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Grid_Ordenes_Variacion_PageIndexChanging
    ///DESCRIPCIÓN: Paginar grid de ordenes de variacion
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 01/Dic/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Grid_Trabajos_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Grid_Trabajos.SelectedIndex > (-1))
        {
            Int32 Seleccion = Grid_Trabajos.SelectedIndex;            
            Grid_Trabajos.SelectedIndex = Seleccion;
            Mostrar_Refaccion();
        }
    }
    protected void Txt_Buscar_TextChanged(object sender, EventArgs e)
    {
        Btn_Buscar_Click(sender, null);
    }
    #endregion

    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// 
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// 
    /// PARÁMETROS: No Áplica.
    /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
    /// FECHA CREÓ: 23/Mayo/2011 10:43 a.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.            
            Botones.Add(Btn_Modificar);


            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 29/Noviembre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion

}
