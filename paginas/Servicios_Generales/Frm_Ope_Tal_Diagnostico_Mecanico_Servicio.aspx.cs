﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Entradas_Vehiculos.Negocio;
using JAPAMI.Taller_Mecanico.Catalogo_Partes_Vehiculos.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Control_Patrimonial_Operacion_Vehiculos.Negocio;
using JAPAMI.Empleados.Negocios;
using System.Text.RegularExpressions;
using JAPAMI.Taller_Mecanico.Catalogo_Mecanicos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Preventivos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Correctivos.Negocio;
using JAPAMI.Taller_Mecanico.Catalogo_Tipos_Trabajo.Negocio;

public partial class paginas_Taller_Mecanico_Frm_Ope_Tal_Diagnostico_Mecanico_Servicio : System.Web.UI.Page {
    
    #region Page_Load
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN: Carga la Pagina Inicial
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 17/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e) {
            Div_Contenedor_Msj_Error.Visible = false;
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack) {
                Llenar_Combo_Unidades_Responsables();
                Llenar_Combo_Mecanicos();
                Cargar_Combo_Trabajos();
                Grid_Listado_Servicios.PageIndex = 0;
                Llenar_Listado_Servicios();
                Configuracion_Formulario("INICIAL");
                Cmb_Unidad_Responsable.Enabled = false;
            }
        }

    #endregion

    #region Metodos

        #region Llenado de Campos [Combos, Listados, Vehiculos]
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Cmb_Partida_General_SelectedIndexChanged
        ///DESCRIPCIÓN: Accion de Seleccion de Cmb_Partida_General
        ///PARAMETROS: 
        ///CREO: jtoledo
        ///FECHA_CREO: 09/Jul/2012
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///******************************************************************************* 
        private void Cargar_Combo_Trabajos()
        {
            Cls_Cat_Tal_Tipos_Trabajo_Negocio Consultas_Negocio = new Cls_Cat_Tal_Tipos_Trabajo_Negocio();
                Consultas_Negocio.P_Estatus = "VIGENTE";
                DataTable Dt_Trabajos = Consultas_Negocio.Consultar_Tipos_Trabajo();
                Cmb_Tipo_Trabajo.DataSource = Dt_Trabajos;
                Cmb_Tipo_Trabajo.DataValueField = Cat_Tal_Tipos_Trabajo.Campo_Tipo_Trabajo_ID;
                Cmb_Tipo_Trabajo.DataTextField = Cat_Tal_Tipos_Trabajo.Campo_Nombre;
                Cmb_Tipo_Trabajo.DataBind();
                Cmb_Tipo_Trabajo.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));            

        }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Unidades_Responsables
            ///DESCRIPCIÓN: Se llena el Combo de las Unidades Responsables.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Combo_Unidades_Responsables() {
                Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                Negocio.P_Estatus = "ACTIVO";
                DataTable Dt_Dependencias = Negocio.Consultar_Unidades_Responsables();
                Cmb_Unidad_Responsable.DataSource = Dt_Dependencias;
                Cmb_Unidad_Responsable.DataTextField = "CLAVE_NOMBRE";
                Cmb_Unidad_Responsable.DataValueField = "DEPENDENCIA_ID";
                Cmb_Unidad_Responsable.DataBind();
                Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Servicios
            ///DESCRIPCIÓN: Se llena el Listado de los Servicios para asignar el Mecanico.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Listado_Servicios() {
                Cls_Ope_Tal_Servicios_Preventivos_Negocio Serv_Prev = new Cls_Ope_Tal_Servicios_Preventivos_Negocio();
                Serv_Prev.P_Estatus = "PENDIENTE','ASIGNADO_MECANICO";
                DataTable Dt_Resultados_Preventivos = Serv_Prev.Consultar_Servicios_Preventivos();

                Cls_Ope_Tal_Servicios_Correctivos_Negocio Serv_Correc = new Cls_Ope_Tal_Servicios_Correctivos_Negocio();
                Serv_Correc.P_Estatus = "PENDIENTE','ASIGNADO_MECANICO";
                DataTable Dt_Resultados_Correctivos = Serv_Correc.Consultar_Servicios_Correctivos();

                Dt_Resultados_Preventivos.Merge(Dt_Resultados_Correctivos);

                Grid_Listado_Servicios.Columns[1].Visible = true;
                Grid_Listado_Servicios.Columns[2].Visible = true;
                Grid_Listado_Servicios.Columns[3].Visible = true;
                Grid_Listado_Servicios.DataSource = Dt_Resultados_Preventivos;
           Grid_Listado_Servicios.DataBind();
                Grid_Listado_Servicios.Columns[1].Visible = false;
                Grid_Listado_Servicios.Columns[2].Visible = false;
                Grid_Listado_Servicios.Columns[3].Visible = false;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Mecanicos
            ///DESCRIPCIÓN: Se llena el Listado de los Mecanicos.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Llenar_Combo_Mecanicos() {
                Cls_Cat_Tal_Mecanicos_Negocio Negocio = new Cls_Cat_Tal_Mecanicos_Negocio();
                //Negocio.P_Estatus = "VIGENTE";
                DataTable Dt_Resultados = Negocio.Consultar_Mecanicos();
                Cmb_Mecanicos.DataSource = Dt_Resultados;
                Cmb_Mecanicos.DataTextField = "NOMBRE_EMPLEADO";
                Cmb_Mecanicos.DataValueField = "MECANICO_ID";
                Cmb_Mecanicos.DataBind();
                Cmb_Mecanicos.Items.Insert(0, new ListItem("< - SELECCIONE - >", ""));
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Vehiculo
            ///DESCRIPCIÓN: Se cargan los Datos del Vehiculo Seleccionado.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Cargar_Datos_Vehiculo(String Vehiculo, String Tipo_Busqueda) {
                Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
                String Tipo_Bien = "";
                DataTable Dt_Vehiculo = new DataTable();
                switch (Tipo_Busqueda) { 
                    case "NO_INVENTARIO":
                        Consulta_Negocio.P_No_Inventario = Vehiculo;
                        break;
                    case "IDENTIFICADOR":
                        Consulta_Negocio.P_Vehiculo_ID = Vehiculo;
                        break;
                    default: break;
                }
                if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Consulta_Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value; }
                
                Tipo_Bien = Hdf_Tipo_Bien.Value.Trim();

                if (Tipo_Bien == "VEHICULO")
                    Dt_Vehiculo = Consulta_Negocio.Consultar_Vehiculos();
                else if (Tipo_Bien == "BIEN_MUEBLE")
                    Dt_Vehiculo = Consulta_Negocio.Consultar_Bien_Mueble();
                if (Dt_Vehiculo.Rows.Count > 0) {
                    if (Tipo_Bien == "VEHICULO")
                    {
                        Pnl_Bien_Mueble_Seleccionado.Visible = false;
                        Pnl_Vehiculo_Seleccionado.Visible = false;
                        Chk_Servicio_Adicional.Enabled = true;
                        Hdf_Vehiculo_ID.Value = Dt_Vehiculo.Rows[0]["VEHICULO_ID"].ToString().Trim();
                        Txt_No_Inventario.Text = Dt_Vehiculo.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                        Txt_No_Economico.Text = Dt_Vehiculo.Rows[0]["NO_ECONOMICO"].ToString().Trim();
                        Txt_Datos_Vehiculo.Text = Dt_Vehiculo.Rows[0]["VEHICULO_DESCRIPCION"].ToString().Trim();
                        Txt_Placas.Text = Dt_Vehiculo.Rows[0]["PLACAS"].ToString().Trim();
                        Txt_Anio.Text = Dt_Vehiculo.Rows[0]["ANIO"].ToString().Trim();
                    }
                    else if (Tipo_Bien == "BIEN_MUEBLE")
                    {
                        Pnl_Bien_Mueble_Seleccionado.Visible = true;
                        Pnl_Vehiculo_Seleccionado.Visible = false;
                        Chk_Servicio_Adicional.Enabled = false;
                        Hdf_Vehiculo_ID.Value = Dt_Vehiculo.Rows[0]["BIEN_MUEBLE_ID"].ToString().Trim();
                        Txt_No_Inventario_BM.Text = Dt_Vehiculo.Rows[0]["NO_INVENTARIO"].ToString().Trim();
                        Txt_Descripcion_Bien.Text = Dt_Vehiculo.Rows[0]["DESCRIPCION_BIEN_MUEBLE"].ToString().Trim();
                        Txt_Numero_Serie_Bien.Text = Dt_Vehiculo.Rows[0]["NUMERO_SERIE"].ToString().Trim();
                    }
                    Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString()));
                } else {
                    Lbl_Ecabezado_Mensaje.Text = "El Vehículo no se encontro.";
                    if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Lbl_Mensaje_Error.Text = "[Puede ser que no este asignado a la Unidad Responsable Seleccionada]."; }
                    else { Lbl_Mensaje_Error.Text = ""; }
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }

        #endregion

        #region Generales [Configuracion, Limpiar]
                
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
            ///DESCRIPCIÓN: Limpia los campos del Formulario.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Limpiar_Formulario() {
                Hdf_No_Entrada.Value = "";
                Hdf_No_Servicio.Value = "";
                Hdf_No_Solicitud.Value = "";
                Txt_Folio.Text = "";
                Txt_Fecha_Elaboracion.Text = "";
                Txt_Fecha_Recepcion.Text = "";
                Txt_Kilometraje.Text = "";
                Cmb_Tipo_Servicio.SelectedIndex = 0;
                Cmb_Unidad_Responsable.SelectedIndex = 0;
                Txt_Descripcion_Servicio.Text = "";
                Hdf_Vehiculo_ID.Value = "";
                Hdf_Programa_ID.Value = "";
                Txt_No_Inventario.Text = "";
                Txt_No_Economico.Text = "";
                Txt_Datos_Vehiculo.Text = "";
                Txt_Placas.Text = "";
                Txt_Anio.Text = "";
                Cmb_Mecanicos.SelectedIndex = 0;
                Txt_Diagnostico_Mecanico.Text = "";
                Cmb_Reparacion.SelectedIndex = 0;
                Chk_Servicio_Adicional.Checked = false;
                Txt_Descripcion_Servicio_Adicional.Text = "";
                Chk_Servicio_Adicional_CheckedChanged(Chk_Servicio_Adicional, null);
                Cmb_Reparacion_SelectedIndexChanged(Cmb_Reparacion, null);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
            ///DESCRIPCIÓN: Se Habilitan y/o inhabilitan los campos dependendiendo de la Condicion.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Configuracion_Formulario(String Operacion) {
                switch (Operacion) {
                    case "INICIAL":
                        Div_Campos.Visible = false;
                        Div_Listado_Servicios.Visible = true;
                        Btn_Guardar_Diagnostico_Mecanico.Visible = false;
                        break;
                    case "OPERACION":
                        Div_Campos.Visible = true;
                        Div_Listado_Servicios.Visible = false;
                        Btn_Guardar_Diagnostico_Mecanico.Visible = true;
                        break;
                }
            }

        #endregion

        #region Clase de Negocio de Solicitudes [Registrar y Consulta]

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Mostrar_Registro
            ///DESCRIPCIÓN: Muestra el Registro en los campos.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 09/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Mostrar_Registro() {
                Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                Solicitud.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
                Solicitud = Solicitud.Consultar_Detalles_Solicitud_Servicio();
                if (Solicitud.P_No_Solicitud > (-1)) {
                    Txt_Folio.Text = Solicitud.P_Folio_Solicitud.Trim();
                    Txt_Fecha_Elaboracion.Text = String.Format("{0:dd/MMM/yyyy}", Solicitud.P_Fecha_Elaboracion);
                    Cmb_Tipo_Servicio.SelectedIndex = Cmb_Tipo_Servicio.Items.IndexOf(Cmb_Tipo_Servicio.Items.FindByValue(Solicitud.P_Tipo_Servicio));
                    Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Solicitud.P_Dependencia_ID));
                    Hdf_Vehiculo_ID.Value = Solicitud.P_Bien_ID;
                    Hdf_Programa_ID.Value = Solicitud.P_Programa_Proyecto_ID;
                    Txt_Descripcion_Servicio.Text = Solicitud.P_Descripcion_Servicio;
                    Cargar_Datos_Vehiculo(Hdf_Vehiculo_ID.Value, "IDENTIFICADOR");
                }
                if (Hdf_No_Entrada.Value.Trim().Length > 0) { 
                    Cls_Ope_Tal_Entradas_Vehiculos_Negocio Entrada = new Cls_Ope_Tal_Entradas_Vehiculos_Negocio();
                    Entrada.P_No_Entrada = Convert.ToInt32(Hdf_No_Entrada.Value);
                    Entrada = Entrada.Consultar_Detalles_Entrada_Vehiculo();
                    if (Entrada.P_Kilometraje > (-1.0)) {
                        Txt_Kilometraje.Text = String.Format("{0:########0.00}", Entrada.P_Kilometraje);
                        Txt_Fecha_Recepcion.Text = String.Format("{0:dd/MMM/yyyy}", Entrada.P_Fecha_Entrada);
                    }
                }
                Mostrar_Registro_Servicio();
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Mostrar_Registro_Servicio
            ///DESCRIPCIÓN: Muestra el Registro en los campos.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 09/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Mostrar_Registro_Servicio() {
                if (Cmb_Tipo_Servicio.SelectedItem.Value.Trim().Equals("SERVICIO_PREVENTIVO")) {
                    Cls_Ope_Tal_Servicios_Preventivos_Negocio Serv_Negocio = new Cls_Ope_Tal_Servicios_Preventivos_Negocio();
                    Serv_Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value.Trim());
                    Serv_Negocio = Serv_Negocio.Consultar_Detalles_Servicio_Preventivo();
                    if (Serv_Negocio.P_No_Servicio > (-1)) 
                    {
                        if (!String.IsNullOrEmpty(Serv_Negocio.P_Mecanico_ID.Trim()))
                            Cmb_Mecanicos.SelectedIndex = Cmb_Mecanicos.Items.IndexOf(Cmb_Mecanicos.Items.FindByValue(Serv_Negocio.P_Mecanico_ID));
                        else
                            Cmb_Mecanicos.Enabled = true;
                    }
                } else if (Cmb_Tipo_Servicio.SelectedItem.Value.Trim().Equals("SERVICIO_CORRECTIVO")) {
                    Cls_Ope_Tal_Servicios_Correctivos_Negocio Serv_Negocio = new Cls_Ope_Tal_Servicios_Correctivos_Negocio();
                    Serv_Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value.Trim());
                    Serv_Negocio = Serv_Negocio.Consultar_Detalles_Servicio_Correctivo();
                    if (Serv_Negocio.P_No_Servicio > (-1)) 
                    {
                        if (!String.IsNullOrEmpty(Serv_Negocio.P_Mecanico_ID.Trim()))
                            Cmb_Mecanicos.SelectedIndex = Cmb_Mecanicos.Items.IndexOf(Cmb_Mecanicos.Items.FindByValue(Serv_Negocio.P_Mecanico_ID));
                        else
                            Cmb_Mecanicos.Enabled = true;
                    }
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Diagnosticar_Mecanico_Servicio
            ///DESCRIPCIÓN: Guarda el Diagnostico el Mecanico al Servicio.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private void Diagnosticar_Mecanico_Servicio() {
                if (Cmb_Tipo_Servicio.SelectedItem.Value.Trim().Equals("SERVICIO_PREVENTIVO")) {
                    Cls_Ope_Tal_Servicios_Preventivos_Negocio Negocio = new Cls_Ope_Tal_Servicios_Preventivos_Negocio();
                    Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value.Trim());
                    Negocio.P_Mecanico_ID = Cmb_Mecanicos.SelectedItem.Value.Trim();
                    Negocio.P_Tipo_Trabajo_ID = Cmb_Tipo_Trabajo.SelectedItem.Value.Trim();
                    Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                    Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value.Trim());
                    if (Cmb_Reparacion.SelectedItem.Value.Trim().Equals("INTERNA")) {
                        Negocio.P_Fecha_Entrega_Probable = Convert.ToDateTime(Txt_Fecha_Probable_Entrega.Text);
                        Negocio.P_Estatus = "PROCESO";
                    } else {
                        Negocio.P_Estatus = "REPARACION";
                    }
                    Negocio.P_Diagnostico = Txt_Diagnostico_Mecanico.Text.Trim();
                    Negocio.P_Reparacion = Cmb_Reparacion.SelectedItem.Value.Trim();
                    Negocio.Asignar_Diagnostico_Servicio_Preventivo();
                } else if (Cmb_Tipo_Servicio.SelectedItem.Value.Trim().Equals("SERVICIO_CORRECTIVO")) {
                    Cls_Ope_Tal_Servicios_Correctivos_Negocio Negocio = new Cls_Ope_Tal_Servicios_Correctivos_Negocio();
                    Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value.Trim());
                    Negocio.P_Mecanico_ID = Cmb_Mecanicos.SelectedItem.Value.Trim();
                    Negocio.P_Tipo_Trabajo_ID = Cmb_Tipo_Trabajo.SelectedItem.Value.Trim();
                    if (Cmb_Reparacion.SelectedItem.Value.Trim().Equals("INTERNA"))
                    {
                        Negocio.P_Fecha_Entrega_Probable = Convert.ToDateTime(Txt_Fecha_Probable_Entrega.Text);
                        Negocio.P_Estatus = "PROCESO";
                    }
                    else
                    {
                        Negocio.P_Estatus = "REPARACION";
                    }
                    Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                    Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value.Trim());
                    Negocio.P_Diagnostico = Txt_Diagnostico_Mecanico.Text.Trim();
                    Negocio.P_Reparacion = Cmb_Reparacion.SelectedItem.Value.Trim();
                    Negocio.Asignar_Diagnostico_Servicio_Correctivo();
                }
                if (Chk_Servicio_Adicional.Checked) {
                    Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                    Solicitud.P_Folio_Solicitud = "";
                    Solicitud.P_Fecha_Elaboracion = DateTime.Today;
                    Solicitud.P_Tipo_Servicio = "SERVICIO_CORRECTIVO";
                    Solicitud.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value;
                    Solicitud.P_Bien_ID = Hdf_Vehiculo_ID.Value;
                    Solicitud.P_Tipo_Bien = "VEHICULO";
                    Solicitud.P_Programa_Proyecto_ID = Hdf_Programa_ID.Value.Trim();
                    Solicitud.P_Descripcion_Servicio = Txt_Descripcion_Servicio_Adicional.Text.Trim();
                    if (Txt_Kilometraje.Text.Trim().Length > 0) Solicitud.P_Kilometraje = Convert.ToDouble(Txt_Kilometraje.Text.Trim());
                    Solicitud.P_Estatus = "PENDIENTE";
                    Solicitud.P_Procedencia = Hdf_No_Solicitud.Value.Trim();
                    Solicitud.P_Empleado_Solicito_ID = Cls_Sessiones.Empleado_ID;
                    Solicitud.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                    Solicitud.Alta_Solicitud_Servicio();
                }
            }

        #endregion

        #region Validaciones

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Asignacion
            ///DESCRIPCIÓN: Valida la Solicitud de Servicio antes de ser Asignada a un mecanico
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 17/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            private Boolean Validar_Asignacion() { 
                Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                String Mensaje_Error = "";
                Boolean Validacion = true;
                if (Hdf_No_Servicio.Value.Trim().Length == 0) { 
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar el Servicio que se realizará.";
                    Mensaje_Error = Mensaje_Error + " <br />";
                    Validacion = false;
                }
                if (Cmb_Mecanicos.SelectedIndex == 0) {
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar el Mecanico que hará el Servicio.";
                    Mensaje_Error = Mensaje_Error + " <br />";
                    Validacion = false;
                }
                if (Txt_Diagnostico_Mecanico.Text.Trim().Length == 0) {
                    Mensaje_Error = Mensaje_Error + "+ Introducir el Diagnostico del Mecanico.";
                    Mensaje_Error = Mensaje_Error + " <br />";
                    Validacion = false;
                }
                if (Cmb_Reparacion.SelectedIndex == 0) {
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar si el Servicio sera Interno o Externo.";
                    Mensaje_Error = Mensaje_Error + " <br />";
                    Validacion = false;
                }
                if (Cmb_Tipo_Trabajo.SelectedIndex == 0)
                {
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar el Tipo de Trabajador.";
                    Mensaje_Error = Mensaje_Error + " <br />";
                    Validacion = false;
                }
                if (Cmb_Reparacion.SelectedItem.Value.Trim().Equals("INTERNA")) { 
                    if (Txt_Fecha_Probable_Entrega.Text.Trim().Length == 0) {
                        Mensaje_Error = Mensaje_Error + "+ Seleccionar la Fecha Probable de Entrega.";
                        Mensaje_Error = Mensaje_Error + " <br />";
                        Validacion = false;
                    }
                }
                if (!Validacion) {
                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
                    Div_Contenedor_Msj_Error.Visible = true;
                } 
                return Validacion;
            }

        #endregion

    #endregion
        
    #region Grids
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Servicios_PageIndexChanging
        ///DESCRIPCIÓN: Maneja la paginación del Listado
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 17/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Servicios_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            try {
                Grid_Listado_Servicios.SelectedIndex = (-1);
                Grid_Listado_Servicios.PageIndex = e.NewPageIndex;
                Llenar_Listado_Servicios();
            } catch(Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Servicios_SelectedIndexChanged
        ///DESCRIPCIÓN: Obtiene los datos de un Servicio 
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 17/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Servicios_SelectedIndexChanged(object sender, EventArgs e) {
            try{
                if (Grid_Listado_Servicios.SelectedIndex > (-1)){
                    Limpiar_Formulario();
                    Hdf_No_Entrada.Value = HttpUtility.HtmlDecode(Grid_Listado_Servicios.SelectedRow.Cells[1].Text.Trim()).Trim();
                    Hdf_No_Servicio.Value = HttpUtility.HtmlDecode(Grid_Listado_Servicios.SelectedRow.Cells[2].Text.Trim()).Trim();
                    Hdf_No_Solicitud.Value = HttpUtility.HtmlDecode(Grid_Listado_Servicios.SelectedRow.Cells[3].Text.Trim()).Trim();
                    Hdf_Tipo_Bien.Value = Grid_Listado_Servicios.SelectedDataKey["TIPO_BIEN"].ToString();
                    Mostrar_Registro();
                    Configuracion_Formulario("OPERACION");
                    Grid_Listado_Servicios.SelectedIndex = -1;
                    System.Threading.Thread.Sleep(500);
                }
            } catch(Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;                
            }
        }
 
    #endregion

    #region Eventos
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Guardar_Diagnostico_Mecanico_Click
        ///DESCRIPCIÓN: Controla las operaciones del Boton Diagnostico
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 17/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Guardar_Diagnostico_Mecanico_Click(object sender, ImageClickEventArgs e) {
            if (Validar_Asignacion()) {
                Diagnosticar_Mecanico_Servicio();
                Llenar_Listado_Servicios();
                Configuracion_Formulario("INICIAL");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "GACO", "alert('Operacion Exitosa: Guardar Diagnostico de Mecanico a Servicio.\\nServicio Folio: " + Txt_Folio.Text.Trim() + ".');", true);
                Limpiar_Formulario();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Controla las operaciones del Boton de Salir
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 17/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e) {
            if (Div_Campos.Visible) {
               Limpiar_Formulario();
               Configuracion_Formulario("INICIAL");
            } else {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Actualizar_Listado_Click
        ///DESCRIPCIÓN: Ejecuta el Proceso para Salir.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Actualizar_Listado_Click(object sender, ImageClickEventArgs e) {
            Llenar_Listado_Servicios();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Chk_Servicio_Adicional_CheckedChanged
        ///DESCRIPCIÓN: Se habilita o no el Cuadro de Texto para Servicio Adicional
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 19/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Chk_Servicio_Adicional_CheckedChanged(object sender, EventArgs e) {
            Pnl_Descripcion_Servicio_Adicional.Visible = Chk_Servicio_Adicional.Checked;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Cmb_Reparacion_SelectedIndexChanged
        ///DESCRIPCIÓN: Se ejecuta el metodo de cambio de Selección.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 28/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Cmb_Reparacion_SelectedIndexChanged(object sender, EventArgs e) {
            if (Cmb_Reparacion.SelectedItem.Value.Trim().Equals("INTERNA")) {
                Lbl_Fecha_Probable_Entrega.Visible = true;
                Txt_Fecha_Probable_Entrega.Visible = true;
                Btn_Fecha_Probable_Entrega.Visible = true;
                Txt_Fecha_Probable_Entrega.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Today);
            } else {
                Txt_Fecha_Probable_Entrega.Text = "";
                Lbl_Fecha_Probable_Entrega.Visible = false;
                Txt_Fecha_Probable_Entrega.Visible = false;
                Btn_Fecha_Probable_Entrega.Visible = false;
            }
        }

    #endregion

}