﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Ope_Tal_Mov_Tarj_Gas.aspx.cs" Inherits="paginas_Taller_Mecanico_Frm_Ope_Tal_Mov_Tarj_Gas"
    Title="Movimientos Tarjetas Gasolina" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">
        .ocultar
        {
            display:none;
        }

    </style>

    <script type="text/javascript" language="javascript">
        function Limpiar_Ctlr_Busqueda_Resguardante() {
            document.getElementById("<%=Cmb_Busqueda_Dependencia.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_No_Economico.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_No_Inventario.ClientID%>").value = "";
            return false;
        }
        function getKeyPressBusqueda(txt_Busqueda, event) {
            if (event.which == 13) {
                (document.getElementById('<%=Btn_Busqueda_Directa_Tarjeta.ClientID %>')).click();
                return false;
            }
            return true;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">


    <cc1:ToolkitScriptManager ID="ScptM_Catalogo" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" EnablePartialRendering="true" AsyncPostBackTimeout="9000">
    </cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Loading" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Area_Trabajo" style="background-color: #ffffff; width: 100%; height: 100%;">
                <center>
                    <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                        <tr align="center">
                            <td class="label_titulo" colspan="2">
                                Movimientos Tarjetas de Gasolina
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div id="Div_Contenedor_Msj_Error" style="width: 98%;" runat="server" visible="false">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td colspan="2" align="left">
                                                <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                    Width="24px" Height="24px" />
                                                <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 10%;">
                                            </td>
                                            <td style="width: 90%; text-align: left;" valign="top">
                                                <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left" style="width: 50%;">
                                <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                    Width="24px" CssClass="Img_Button" AlternateText="Modificar" ToolTip="Modificar Tarjeta"
                                    OnClick="Btn_Modificar_Click" />
                                <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Salir" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                    Width="24px" CssClass="Img_Button" AlternateText="Salir" OnClick="Btn_Salir_Click" />
                            </td>
                            <td style="width: 50%;">
                                B&uacute;squeda
                                <asp:TextBox ID="Txt_Busqueda" runat="server" Width="200px" onkeypress="return getKeyPressBusqueda(this, event);"></asp:TextBox>
                                <asp:ImageButton ID="Btn_Busqueda_Directa_Tarjeta" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                    AlternateText="Buscar" OnClick="Btn_Busqueda_Directa_Tarjeta_Click" ToolTip="Buscar" />
                                <asp:ImageButton ID="Btn_Buscar" runat="server" CausesValidation="false" Width="24px"
                                    ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png" ToolTip="Buscar" AlternateText="Consultar"
                                    OnClick="Btn_Buscar_Click" />
                                <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda" runat="server" WatermarkText="<No. Tarjeta>"
                                    TargetControlID="Txt_Busqueda" WatermarkCssClass="watermarked">
                                </cc1:TextBoxWatermarkExtender>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda" runat="server" TargetControlID="Txt_Busqueda"
                                    InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                    </table>
                </center>
            </div>
            <div id="Div_Campos" runat="server" style="width: 100%;">
                <table width="99%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td colspan="2">
                            <asp:HiddenField ID="Hdf_Tarjeta_ID" runat="server" />
                            <asp:HiddenField ID="Hdf_Empleado_ID" runat="server" />
                            <asp:HiddenField ID="Hdf_Vehiculo_ID" runat="server" />
                            <asp:HiddenField ID="Hdf_Dependencia_ID" runat="server" />
                            <asp:HiddenField ID="Hdf_Partida_ID" runat="server" />
                            <asp:HiddenField ID="Hdf_Programa_ID" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Numero_Tarjeta" runat="server" Text="Número de Tarjeta"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="Txt_Nuemero_Tarjeta" runat="server" Width="35%" Enabled="false"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nuemero_Tarjeta" runat="server" TargetControlID="Txt_Nuemero_Tarjeta"
                                FilterType="Numbers">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                        <td style="width: 15%;">
                            &nbsp;&nbsp;
                            <asp:Label ID="Lbl_Saldo_Actual" runat="server" Text="Saldo Actual" Font-Bold="true"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Saldo_Actual" runat="server" Width="97%" Enabled="false" Font-Bold="true"
                               ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Asignada_A" runat="server" Text="Asignada a"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="Txt_Asignada_A" runat="server" Width="85%" Enabled="false" Visible="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="Pnl_Resguardantes" runat="server" Width="99%" GroupingText="Resguardantes"
                    Visible="false">
                    <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                            <td>
                                <asp:GridView ID="Grid_Resguardante" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                    CssClass="GridView_Nested" EmptyDataText="No hay Resguardantes para esta Tarjeta."
                                    GridLines="None" PageSize="10" Width="99%">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:BoundField DataField="NO_EMPLEADO" HeaderText="No. Empleado" SortExpression="NO_EMPLEADO">
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="10%" />
                                            <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" Width="10%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="RESGUARDANTE" HeaderText="Resguardante" SortExpression="RESGUARDANTE">
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="65%" />
                                            <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" Width="65%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="TIPO_UNIDAD" HeaderText="Tipo unidad" NullDisplayText="-"
                                            SortExpression="TIPO_UNIDAD">
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="15%" />
                                            <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" Width="15%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NUMERO_INVENTARIO" HeaderText="Inventario" NullDisplayText="-"
                                            SortExpression="NUMERO_INVENTARIO">
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="10%" />
                                            <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" Width="10%" />
                                        </asp:BoundField>
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="Pnl_Vehiculo_Seleccionado" runat="server" Width="99%" GroupingText="Vehículo para la Tarjeta">
                    <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                            <td style="width: 15%;">
                                <asp:Label ID="Lbl_No_Inventario" runat="server" Text="No. Inventario"></asp:Label>
                            </td>
                            <td style="width: 35%;">
                                <asp:TextBox ID="Txt_No_Inventario" runat="server" Width="87%" MaxLength="7"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Inventario" runat="server" TargetControlID="Txt_No_Inventario"
                                    FilterType="Numbers">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td>
                                <asp:Label ID="Lbl_No_Economico" runat="server" Text="No. Economico"></asp:Label>
                            </td>
                            <td align="right">
                                <asp:TextBox ID="Txt_No_Economico" runat="server" Width="94%" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text="Unidad Responsable"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="Txt_Unidad_Responsable" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Lbl_Datos_Vehiculo" runat="server" Text="Datos Vehiculo"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="Txt_Datos_Vehiculo" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;">
                                <asp:Label ID="Lbl_Placas" runat="server" Text="Placas"></asp:Label>
                            </td>
                            <td style="width: 35%;">
                                <asp:TextBox ID="Txt_Placas" runat="server" Width="87%" Enabled="false"></asp:TextBox>
                            </td>
                            <td style="width: 15%;">
                                &nbsp;&nbsp;
                                <asp:Label ID="Lbl_Anio" runat="server" Text="Año"></asp:Label>
                            </td>
                            <td style="width: 35%;" align="right">
                                <asp:TextBox ID="Txt_Anio" runat="server" Width="94%" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <table width="99%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Saldo_Incial" runat="server" Text="Saldo Inicial"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="Txt_Saldo_inicial" runat="server" Width="85%" Enabled="false"></asp:TextBox>
                        </td>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Estatus" runat="server" Text="Estatus"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="93%">
                                <asp:ListItem Value="">&lt; - - SELECCIONE - - &gt;</asp:ListItem>
                                <asp:ListItem Value="VIGENTE">VIGENTE</asp:ListItem>
                                <asp:ListItem Value="BAJA">BAJA</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Saldo_Cargado" runat="server" Text="Saldo Cargado"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Saldo_Cargado" runat="server" Width="85%" Enabled="false"></asp:TextBox>
                        </td>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Saldo_Consumido" runat="server" Text="Saldo Consumido"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Saldo_Consumido" runat="server" Width="91%" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_partida" runat="server" Text="Clave de Partida"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Partida" runat="server" Width="85%" Enabled="false"></asp:TextBox>
                        </td>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Disponible_Partida" Visible="false" runat="server" Text="Disponible"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Disponible_Partida" Visible="false" runat="server" Width="91%" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <hr style="height: 2pt; width: 98%" />
                <table width="99%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Busqueda_Saldo" runat="server" Text="Cantidad [$]"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Busqueda_Saldo" runat="server" Width="88%" Enabled="false"></asp:TextBox>
                        </td>
                        <td style="width: 15%;">&nbsp;</td>
                        <td style="width: 35%;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Busqueda_Comentarios" runat="server" Text="Comentarios"></asp:Label>
                        </td>
                        <td colspan="3" align="left">
                            <asp:TextBox ID="Txt_Busqueda_Comentarios" runat="server" Width="97%" Height="60px"
                                TextMode="MultiLine" Style="text-transform: uppercase"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="Twe_Txt_Busqueda_Comentarios" runat="server" WatermarkCssClass="watermarked"
                                WatermarkText="Límite de Caractes 250" TargetControlID="Txt_Busqueda_Comentarios">
                            </cc1:TextBoxWatermarkExtender>
                            <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_Comentarios" runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                TargetControlID="Txt_Busqueda_Comentarios" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="Grid_Listado" runat="server" CssClass="GridView_1" AutoGenerateColumns="False"
                    AllowPaging="True" PageSize="100" Width="99%" GridLines="None" EmptyDataText="No se encontrarón movimientos"
                    OnRowDataBound="Grid_Listado_RowDataBound" OnRowDeleting="Grid_Listado_RowDeleting">
                    <RowStyle CssClass="GridItem" />
                    <Columns>
                        <asp:BoundField DataField="NUMERO_REGISTRO" HeaderText="No. Registro" HeaderStyle-Width="7%" ItemStyle-CssClass="ocultar" HeaderStyle-CssClass="ocultar" ItemStyle-HorizontalAlign="Right" />
                        <asp:BoundField DataField="NUMERO_MOVIVIENTO" HeaderText="No. Movimiento" SortExpression="NUMERO_MOVIVIENTO">
                            <ItemStyle Width="130px" HorizontalAlign="Center" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="SALDO_MOVIMIENTO" HeaderText="Cantidad" SortExpression="SALDO_MOVIMIENTO"
                            DataFormatString="{0:c}">
                            <ItemStyle Width="130px" HorizontalAlign="Center" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TIPO_MOVIMIENTO" HeaderText="Tipo Mov." SortExpression="TIPO_MOVIMIENTO">
                            <ItemStyle Width="130px" HorizontalAlign="Center" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FECHA_MOVIMIENTO" HeaderText="Fecha" SortExpression="FECHA_MOVIMIENTO">
                            <ItemStyle Width="130px" HorizontalAlign="Center" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="COMENTARIOS" HeaderText="Comentarios" SortExpression="COMENTARIOS">
                            <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                        </asp:BoundField>
                         <asp:BoundField DataField="ESTATUS" HeaderText="ESTATUS" HeaderStyle-Width="7%" ItemStyle-CssClass="ocultar" HeaderStyle-CssClass="ocultar" ItemStyle-HorizontalAlign="Right" />
                        <asp:ButtonField ButtonType="Image" CommandName="Delete" ImageUrl="~/paginas/imagenes/paginas/Aplicar_Incidencia.png">
                            <ItemStyle Width="30px" />
                        </asp:ButtonField>
                    </Columns>
                    <PagerStyle CssClass="GridHeader" />
                    <SelectedRowStyle CssClass="GridSelected" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAltItem" />
                </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="Aux_MPE_Listado_Empleados" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_MPE_Listado_Empleados" runat="server" Text="Button" Style="display: none;" />
            <cc1:ModalPopupExtender ID="MPE_Listado_Tarjetas" runat="server" TargetControlID="Btn_Comodin_MPE_Listado_Empleados"
                CancelControlID="Btn_Cerrar_Ventana" PopupControlID="Pnl_Busqueda_Contenedor"
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter">
            </cc1:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Pnl_Busqueda_Contenedor" runat="server" CssClass="drag" HorizontalAlign="Center"
        Width="850px" Style="display: none; border-style: outset; border-color: Silver;
        background-image: url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG); background-repeat: repeat-y;">
        <asp:Panel ID="Pnl_Busqueda_Resguardante_Cabecera" runat="server" Style="cursor: move;
            background-color: Silver; color: Black; font-size: 12; font-weight: bold; border-style: outset;">
            <table width="99%">
                <tr>
                    <td style="color: Black; font-size: 12; font-weight: bold;">
                        <asp:Image ID="Img_Informatcion_Autorizacion" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                        B&uacute;squeda: Tarjetas
                    </td>
                    <td align="right" style="width: 10%;">
                        <asp:ImageButton ID="Btn_Cerrar_Ventana" CausesValidation="false" runat="server"
                            Style="cursor: pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <div style="color: #5D7B9D">
            <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;">
                        <asp:UpdatePanel ID="Upnl_Filtros_Busqueda_Prestamos" runat="server">
                            <ContentTemplate>
                                <asp:UpdateProgress ID="Progress_Upnl_Filtros_Busqueda_Prestamos" runat="server"
                                    AssociatedUpdatePanelID="Upnl_Filtros_Busqueda_Prestamos" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                        </div>
                                        <div style="background-color: Transparent; position: fixed; top: 50%; left: 47%;
                                            padding: 10px; z-index: 1002;" id="div_progress">
                                            <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:Panel ID="Pnl_Busqueda_Avanzada_Tarjetas" runat="server" DefaultButton="Btn_Busqueda_Tarjetas">
                                    <table width="100%">
                                        <tr>
                                            <td style="width: 100%" colspan="4" align="right">
                                                <asp:ImageButton ID="Btn_Limpiar_Ctlr_Busqueda" runat="server" OnClientClick="javascript:return Limpiar_Ctlr_Busqueda_Resguardante();"
                                                    ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de Busqueda" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%" colspan="4">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left; font-size: 11px;">
                                                No Economico
                                            </td>
                                            <td style="width: 30%; text-align: left; font-size: 11px;">
                                                <asp:TextBox ID="Txt_Busqueda_No_Economico" runat="server" Width="98%" />
                                                <cc1:FilteredTextBoxExtender ID="FTE" runat="server" FilterType="Numbers" TargetControlID="Txt_Busqueda_No_Economico" />
                                                <cc1:TextBoxWatermarkExtender ID="Twm_Txt_Busqueda_No_Economico" runat="server" TargetControlID="Txt_Busqueda_No_Economico"
                                                    WatermarkText="Busqueda por No Economico" WatermarkCssClass="watermarked" />
                                            </td>
                                            <td style="width: 20%; text-align: left; font-size: 11px;">
                                                No Inventario
                                            </td>
                                            <td style="width: 30%; text-align: left; font-size: 11px;">
                                                <asp:TextBox ID="Txt_Busqueda_No_Inventario" runat="server" Width="98%" />
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_No_Inventario" runat="server" FilterType="Numbers"
                                                    TargetControlID="Txt_Busqueda_No_Inventario" />
                                                <cc1:TextBoxWatermarkExtender ID="Twm_Txt_Busqueda_No_Inventario" runat="server"
                                                    TargetControlID="Txt_Busqueda_No_Inventario" WatermarkText="Busqueda por No Inventario"
                                                    WatermarkCssClass="watermarked" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left; font-size: 11px;">
                                                Unidad Responsable
                                            </td>
                                            <td style="width: 30%; text-align: left; font-size: 11px;" colspan="3">
                                                <asp:DropDownList ID="Cmb_Busqueda_Dependencia" runat="server" Width="100%" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%" colspan="4">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%; text-align: left;" colspan="4">
                                                <center>
                                                    <asp:Button ID="Btn_Busqueda_Tarjetas" runat="server" Text="Busqueda de Tarjetas"
                                                        CssClass="button" CausesValidation="false" Width="200px" OnClick="Btn_Busqueda_Tarjetas_Click" />
                                                </center>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <br />
                                <div id="Div_Resultados_Busqueda_Resguardantes" runat="server" style="border-style: outset;
                                    width: 99%; height: 250px; overflow: auto;">
                                    <asp:GridView ID="Grid_Busqueda_Tarjetas_Movimientos" runat="server" AutoGenerateColumns="False"
                                        CellPadding="4" ForeColor="#333333" GridLines="None" AllowPaging="True" Width="100%"
                                        PageSize="100" EmptyDataText="No se encontrarón resultados para los filtros de la busqueda"
                                        OnSelectedIndexChanged="Grid_Busqueda_Tarjetas_Movimientos_SelectedIndexChanged">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                <ItemStyle Width="5%" />
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="TARJETA_ID" HeaderText="ID Tarjeta" SortExpression="TARJETA_ID">
                                                <ItemStyle Width="1%" Font-Size="X-Small" HorizontalAlign="Center" />
                                                <HeaderStyle Width="1%" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Number_TARJETA" HeaderText="No. Tarjeta" SortExpression="NUMERO_TARJETA">
                                                <ItemStyle Width="10%" Font-Size="X-Small" HorizontalAlign="Center" />
                                                <HeaderStyle Width="10%" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NO_INVENTARIO" HeaderText="No. Inventario" SortExpression="NO_INVENTARIO"
                                                NullDisplayText="-">
                                                <ItemStyle Width="10%" Font-Size="X-Small" HorizontalAlign="Center" />
                                                <HeaderStyle Width="10%" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NO_ECONOMICO" HeaderText="No. Económico" SortExpression="NO_ECONOMICO"
                                                NullDisplayText="-">
                                                <ItemStyle Width="10%" Font-Size="X-Small" HorizontalAlign="Center" />
                                                <HeaderStyle Width="10%" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" SortExpression="DEPENDENCIA"
                                                NullDisplayText="-">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS"
                                                NullDisplayText="-">
                                                <ItemStyle Width="10%" Font-Size="X-Small" HorizontalAlign="Center" />
                                                <HeaderStyle Width="10%" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>
