﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Tal_Costo_Mano_Obra.aspx.cs" Inherits="paginas_Taller_Mecanico_Frm_Ope_Tal_Costo_Mano_Obra" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <script type="text/javascript">
        window.onerror = new Function("return true");
        //Abrir una ventana modal
        function Abrir_Ventana_Modal(Url, Propiedades) {
            window.showModalDialog(Url, null, Propiedades);
        }
        function Validar_Longitud_Texto(Text_Box, Max_Longitud) {
            if (Text_Box.value.length > Max_Longitud) {
                Text_Box.value = Text_Box.value.substring(0, Max_Longitud);
            }
        }
    </script>

    <script type="text/javascript" language="javascript">
    //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_sesiones.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion()
        {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para matener la sesión activa
        setInterval('MantenSesion()', <%=(int)(0.9*(Session.Timeout * 60000))%>);
        function formatCurrency(num) {
            var Combo = "";
            var Beneficio;
                num = num.toString().replace(/\$|\,/g,'');
                if(isNaN(num))
                num = "0";
                sign = (num == (num = Math.abs(num)));
                num = Math.floor(num*100+0.50000000001);
                cents = num%100;
                num = Math.floor(num/100).toString();
                if(cents<10)
                    cents = "0" + cents;
                    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
                    num = num.substring(0,num.length-(4*i+3))+','+
                    num.substring(num.length-(4*i+3));
                    return (((sign)?'':'-') + num + '.' + cents);
            }
        function Calcular_Horas(num) {                
                var Total = 0.00;
                var Horas = num;
                var Costo = document.getElementById('<%= Lbl_Costo_Hora.ClientID %>').value;
                Total = Costo * Horas;
                document.getElementById('<%= Txt_Total.ClientID %>').value = formatCurrency(Total);
            }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" EnablePartialRendering="true" AsyncPostBackTimeout="9000" />
    <%--Inicio formulario--%>
    <asp:UpdatePanel ID="Upd_Lista_Refacciones" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--update progrees--%>
            <asp:UpdateProgress ID="Uprg_Progress" runat="server" AssociatedUpdatePanelID="Upd_Lista_Refacciones" DisplayAfter="0">
            <ProgressTemplate>
            <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
            <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
            </ProgressTemplate>
            </asp:UpdateProgress>
            
                <div id="Div_Servicios" style="background-color:#ffffff; width:100%; height:100%">
            <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
            <tr>
                <td class="label_titulo" colspan="4">
                    Reporte de Costo de Mano de Obra</td>
            </tr>
                    
                <tr>
                <td colspan="4">
                    <asp:Image ID="Img_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" />
                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" CssClass="estilo_fuente_mensaje_error" Text="" /><br />
                    <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                </td>                        
                </tr>
                <tr class="barra_busqueda">
                <td colspan="2" align="left" style="width:20%">                    
                    <asp:ImageButton ID="Btn_Modificar" runat="server" 
                        ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" CssClass="Img_Button" OnClick="Btn_Modificar_Click"/>
                    <asp:ImageButton ID="Btn_Salir" runat="server" 
                        ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" CssClass="Img_Button" OnClick="Btn_Salir_Click"/>
                    </td>
                    <td colspan="2" align="right" valign="top" style="width:80%">
                    <table style="width: 80%;">
                        <tr>
                            <td style="vertical-align: top; text-align: right; width: 5%">                                    
                            </td>
                            <td style="vertical-align: top; text-align: right; width: 90%">
                                Búsqueda:
                                <asp:TextBox ID="Txt_Buscar" runat="server" MaxLength="100" TabIndex="5" ToolTip="Buscar"
                                    Width="180px" />
                                <cc1:TextBoxWatermarkExtender ID="WTE_Txt_Buscar" runat="server" WatermarkCssClass="watermarked"
                                    WatermarkText="<No. Servicio>" TargetControlID="Txt_Buscar" />
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Buscar" runat="server" TargetControlID="Txt_Buscar"
                                    FilterType="Numbers" />
                            </td>
                            <td style="vertical-align: top; text-align: right; width: 5%">
                                <asp:ImageButton ID="Btn_Buscar" runat="server" TabIndex="6" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                    OnClick="Btn_Buscar_Click" ToolTip="Buscar"/>
                            </td>
                        </tr>
                    </table>
                    </td>
                </tr>    
                </table>
                </div>
                &nbsp;
                <br />
                <div id="Div_Servicios_Preventivos" runat="server" style="background-color:#ffffff; width:100%; height:100%">
                    <br>
                    <br></br>
                    <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                        <tr>
                            <td align="center" colspan="4">
                                <asp:GridView ID="Grid_Listado_Servicios" runat="server" AllowPaging="True" 
                                    AutoGenerateColumns="False" CssClass="GridView_1" 
                                    EmptyDataText="No se Encontrarón Servicios Pendientes" GridLines="None" 
                                    OnPageIndexChanging="Grid_Listado_Servicios_PageIndexChanging" 
                                    OnSelectedIndexChanged="Grid_Listado_Servicios_SelectedIndexChanged" 
                                    PageSize="20" Width="99%">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                            ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                            <ItemStyle Width="30px" />
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="NO_ENTRADA" HeaderText="NO_ENTRADA" 
                                            SortExpression="NO_ENTRADA">
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="90px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NO_SERVICIO" HeaderText="NO_SERVICIO" 
                                            SortExpression="NO_SERVICIO">
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="90px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NO_SOLICITUD" HeaderText="NO_SOLICITUD" 
                                            SortExpression="NO_SOLICITUD">
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="90px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FOLIO" HeaderText="Folio" SortExpression="FOLIO">
                                            <ItemStyle Font-Bold="true" Font-Size="X-Small" HorizontalAlign="Center" 
                                                Width="90px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FECHA_RECEPCION" DataFormatString="{0:dd/MMM/yyyy}" 
                                            HeaderText="Fecha Recepción" SortExpression="FECHA_RECEPCION">
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="140px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="TIPO_SERVICIO" HeaderText="Tipo Servicio" 
                                            SortExpression="TIPO_SERVICIO">
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="140px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DESCRIPCION_SERVICIO" HeaderText="Descripción" SortExpression="DESCRIPCION_SERVICIO">
                                            <ItemStyle Font-Size="X-Small"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" 
                                            SortExpression="DEPENDENCIA">
                                            <ItemStyle Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NO_INVENTARIO" HeaderText="No. Inventario" 
                                            SortExpression="NO_INVENTARIO">
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="120px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NO_ECONOMICO" HeaderText="No. Economico" SortExpression="NO_ECONOMICO">
                                            <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small"/>
                                        </asp:BoundField>
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                    </br>
                </div>

                <div id="Div_Datos_Solicitud" runat="server">
                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
						<tr>
                            <td style="width:15%;">
                                <b>Unidad Responsable</b>
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="Lbl_Unidad_Responsable" runat="server" Text="" Enabled="false" Width="96%" Style="text-transform: uppercase"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%;">
                                <b>Kilometraje</b>
                            </td>
                            <td style="width:35%;">
                                <asp:TextBox ID="Lbl_Kilometraje" runat="server" Text="" Enabled="false" Width="90%" Style="text-transform: uppercase"></asp:TextBox>
                            </td>
                            <td style="width:15%;">
                                &nbsp;&nbsp;  
                                <b>Tipo Servicio</b>
                            </td>
                            <td style="width:35%;">
                                <asp:TextBox ID="Lbl_Tipo_Servicio" runat="server" Text="" Enabled="false" Width="90%" Style="text-transform: uppercase"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4"> &nbsp;&nbsp; </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Panel ID="Pnl_Vehiculo_Seleccionado" runat="server" Width="99%" GroupingText="Vehículo para el Servicio">
                                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:HiddenField ID="Hdf_Vehiculo_ID" runat="server" />
                                                <b>No. Inventario</b>
                                            </td>
                                            <td style="width:35%;">
												<asp:TextBox ID="Lbl_No_Inventario" runat="server" Text="" Enabled="false" Width="94%" Style="text-transform: uppercase"></asp:TextBox>                                        
                                            </td>
                                            <td style="width:15%;">
                                                &nbsp;&nbsp;  
                                                <b>No. Economico</b>
                                            </td>
                                            <td style="width:35%;">
												<asp:TextBox ID="Lbl_No_Economico" runat="server" Text="" Enabled="false" Width="100%" Style="text-transform: uppercase"></asp:TextBox>                                          
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%;">
                                                <b>Vehículo</b>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="Lbl_Datos_Vehiculo" runat="server" Text="" Enabled="false" Width="100%" Style="text-transform: uppercase"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%;">
                                                <b>Placas</b>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Lbl_Placas" runat="server" Text="" Enabled="false" Width="94%" Style="text-transform: uppercase"></asp:TextBox>
                                            </td>
                                            <td style="width:15%;">
                                                &nbsp;&nbsp;  
                                                <b>Año</b>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Lbl_Anio" runat="server" Text="" Enabled="false" Width="100%" Style="text-transform: uppercase"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                             <td colspan="4">&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Panel ID="Pnl_Descripcion_Servicio" runat="server" Width="99%" GroupingText="Descripción del Servicio">
                                    <asp:TextBox ID="Txt_Descripcion_Servicio" runat="server" Rows="5" TextMode="MultiLine" Width="99%" Enabled="false"></asp:TextBox>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                             <td colspan="4">&nbsp;&nbsp;</td>
                        </tr>
                    </table>
                    <hr style="width:98%;"/>
                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                            <td style="width:35%;">
                                <b>Mecanico Asignado</b>
                            </td>
                            <td colspan="3" style="width:65%;">
                                <asp:TextBox ID="Lbl_Mecanicos" runat="server" Text="Mecanico Asignado" Enabled="false" Width="95%" Style="text-transform: uppercase"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <hr style="width:98%;"/>
                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                             <td colspan="4">&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Panel ID="Pnl_Diagnostico_Mecanico" runat="server" Width="99%" GroupingText="Descripción del Diagnostico del Mecanico">
                                    <asp:TextBox ID="Txt_Diagnostico_Mecanico" runat="server" Rows="6" TextMode="MultiLine" Width="99%" Enabled="false"></asp:TextBox>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:15%;">
                                <b>Costo X hora</b>
                            </td>
                            <td colspan="3" style="width:35%;">
                                <asp:TextBox ID="Lbl_Costo_Hora" runat="server" Text="" Width="100px" enabled="false" style="text-align:right;"></asp:TextBox>
                            </td>                            
                        </tr>
                        <tr>
                            <td style="width: 15%; text-align: left">
                                <b>Cantidad de Horas</b>
                            </td>
                            <td colspan="3" style="width: 35%;">
                                <asp:TextBox ID="Txt_Horas" runat="server" Width="100px" Text="" MaxLength="50" onBlur="this.value=formatCurrency(this.value);"
                                    onkeyPress="Calcular_Horas(this.value);" onkeyUp="Calcular_Horas(this.value);" style="text-align:right;"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="Txt_Horas_FilteredTextBoxExtender" runat="server"
                                    Enabled="True" TargetControlID="Txt_Horas" FilterType="Custom,Numbers" ValidChars=".,">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                        <td style="width:15%;">
                                <b>Total</b>
                            </td>
                            <td colspan="3">
                                    <asp:TextBox ID="Txt_Total" runat="server" Width="100px" Text="" MaxLength="50"
                                     onBlur="this.value=formatCurrency(this.value);" onChange="this.value=formatCurrency(this.value);" style="text-align:right;"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                    Enabled="True" TargetControlID="Txt_Total" FilterType="Custom,Numbers"
                                    ValidChars=".,">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
					</table>
		</div>
            <br>
            <br></br>
            <br>
            <br></br>
            <asp:HiddenField ID="Hdf_No_Entrada" runat="server" />
            <asp:HiddenField ID="Hdf_No_Servicio" runat="server" />
            <asp:HiddenField ID="Hdf_No_Solicitud" runat="server" />
            </br>
        </br>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

