﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Catalogo_Taller_Servicios_Preventivos.Negocio;
using JAPAMI.Catalogo_Taller_Refacciones.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Entradas_Vehiculos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Preventivos.Negocio;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Correctivos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Asignacion_Proveedor.Negocio;

public partial class paginas_Taller_Mecanico_Frm_Ope_Asignacion_Proveedor_Servicio : System.Web.UI.Page
{
    #region Page_Load

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Page_Load
    ///DESCRIPCIÓN: Carga la Pagina Inicial
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");        
        if (!IsPostBack)
        {
            Cargar_Ventana_Emergente_Busqueda_Refacciones();
            //Cmb_Filtrado_Estatus.SelectedIndex = Cmb_Filtrado_Estatus.Items.IndexOf(Cmb_Filtrado_Estatus.Items.FindByValue("PENDIENTE"));
            Llenar_Combo_Unidades_Responsables();
            Llenar_Listado_Servicios();
            Grid_Listado_Servicios.PageIndex = 0;            
            Configuracion_Formulario("INICIAL");
            Cmb_Unidad_Responsable.Enabled = false;
        }
        Mensaje_Error();
    }

    #endregion

    #region [Metodos]
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Mostrar_Registro
    ///DESCRIPCIÓN: Muestra el Registro en los campos.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 09/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Mostrar_Registro()
    {
        Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
        DataTable Dt_Tarjeta_Informativa = Generar_Tabla_Informativa();
        Solicitud.P_No_Solicitud = Convert.ToInt32(Hdf_No_Solicitud.Value);
        Solicitud = Solicitud.Consultar_Detalles_Solicitud_Servicio();
        if (Solicitud.P_No_Solicitud > (-1))
        {
            Txt_Folio.Text = Solicitud.P_Folio_Solicitud.Trim();
            Txt_Fecha_Elaboracion.Text = String.Format("{0:dd/MMM/yyyy}", Solicitud.P_Fecha_Elaboracion);
            Cmb_Tipo_Servicio.SelectedIndex = Cmb_Tipo_Servicio.Items.IndexOf(Cmb_Tipo_Servicio.Items.FindByValue(Solicitud.P_Tipo_Servicio));
            Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Solicitud.P_Dependencia_ID));
            Hdf_Vehiculo_ID.Value = Solicitud.P_Bien_ID;
            Txt_Descripcion_Servicio.Text = Solicitud.P_Descripcion_Servicio.ToUpper();
            //Para Crear la Trajeta informativa con el ultimo cambio se checa si es nula la fecha de cambio de llantas o bateria
            if (Solicitud.P_Fecha_Llantas != new DateTime())
            {
                DataRow Dr_Elemento = Dt_Tarjeta_Informativa.NewRow();
                Dr_Elemento["CAMBIO"] = "Último cambio de Llantas";
                Dr_Elemento["FECHA_CAMBIO"] = String.Format("{0:dd/MMM/yyyy}", Solicitud.P_Fecha_Llantas);
                Dt_Tarjeta_Informativa.Rows.Add(Dr_Elemento);
            }
            else if (Solicitud.P_Fecha_Bateria != new DateTime())
            {
                DataRow Dr_Elemento = Dt_Tarjeta_Informativa.NewRow();
                Dr_Elemento["CAMBIO"] = "Último cambio de Bateria";
                Dr_Elemento["FECHA_CAMBIO"] = String.Format("{0:dd/MMM/yyyy}", Solicitud.P_Fecha_Bateria);
                Dt_Tarjeta_Informativa.Rows.Add(Dr_Elemento);
            }
            Grid_Tarjeta_Informativa.DataSource = Dt_Tarjeta_Informativa;
            Grid_Tarjeta_Informativa.DataBind();
            if (Solicitud.P_Tipo_Bien.Equals("BIEN_MUEBLE"))
            {
                Pnl_Bien_Mueble_Seleccionado.Visible = true;
                Pnl_Vehiculo_Seleccionado.Visible = false;
                Hdf_Bien_Mueble_ID.Value = Solicitud.P_Bien_ID;
                Cargar_Datos_Bien_Mueble(Hdf_Bien_Mueble_ID.Value, "IDENTIFICADOR");
            }
            else if (Solicitud.P_Tipo_Bien.Equals("VEHICULO"))
            {
                Pnl_Vehiculo_Seleccionado.Visible = true;
                Pnl_Bien_Mueble_Seleccionado.Visible = false;
                Hdf_Vehiculo_ID.Value = Solicitud.P_Bien_ID;
                Cargar_Datos_Vehiculo(Hdf_Vehiculo_ID.Value, "IDENTIFICADOR");
            }
        }
        if (Hdf_No_Entrada.Value.Trim().Length > 0)
        {
            Cls_Ope_Tal_Entradas_Vehiculos_Negocio Entrada = new Cls_Ope_Tal_Entradas_Vehiculos_Negocio();
            Entrada.P_No_Entrada = Convert.ToInt32(Hdf_No_Entrada.Value);
            Entrada = Entrada.Consultar_Detalles_Entrada_Vehiculo();
            if (Entrada.P_Kilometraje > (-1.0)) { Txt_Kilometraje.Text = String.Format("{0:########0.00}", Entrada.P_Kilometraje); }
            if (!String.Format("{0:ddMMyyyy}", new DateTime()).Equals(String.Format("{0:ddMMyyyy}", Entrada.P_Fecha_Entrada))) { Txt_Fecha_Recepcion.Text = String.Format("{0:dd/MMM/yyyy}", Entrada.P_Fecha_Entrada); }
        }
        Mostrar_Registro_Servicio();
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Mostrar_Registro_Servicio
    ///DESCRIPCIÓN: Muestra el Registro en los campos.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 09/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Mostrar_Registro_Servicio() {
        if (Cmb_Tipo_Servicio.SelectedItem.Value.Trim().Equals("SERVICIO_PREVENTIVO") || Cmb_Tipo_Servicio.SelectedItem.Value.Trim().Equals("VERIFICACION")) {
            Cls_Ope_Tal_Servicios_Preventivos_Negocio Serv_Negocio = new Cls_Ope_Tal_Servicios_Preventivos_Negocio();
            Serv_Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value.Trim());
            Serv_Negocio = Serv_Negocio.Consultar_Detalles_Servicio_Preventivo();
            Txt_Diagnostico_Servicio.Text = Serv_Negocio.P_Diagnostico.Trim();
        } else if (Cmb_Tipo_Servicio.SelectedItem.Value.Trim().Equals("SERVICIO_CORRECTIVO")) {
            Cls_Ope_Tal_Servicios_Correctivos_Negocio Serv_Negocio = new Cls_Ope_Tal_Servicios_Correctivos_Negocio();
            Serv_Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value.Trim());
            Serv_Negocio = Serv_Negocio.Consultar_Detalles_Servicio_Correctivo();
            Txt_Diagnostico_Servicio.Text = Serv_Negocio.P_Diagnostico.Trim();
        }
    }
    
    #region Metodos Generales
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN : Cargar_Ventana_Emergente_Busqueda_Refacciones
    ///DESCRIPCIÓN          : Establece el evento onclik del control para abrir la ventana emergente
    ///PARAMETROS: 
    ///CREO                 : Jesus Toledo Rodriguez
    ///FECHA_CREO           : 21/Otubre/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Cargar_Ventana_Emergente_Busqueda_Refacciones()
    {
        String Ventana_Modal = "Abrir_Ventana_Modal('Ventanas_Emergentes/Frm_Busqueda_Proveedores.aspx";
        String Propiedades = ", 'center:yes;resizable:no;status:no;dialogWidth:680px;dialogHide:true;help:no;scroll:no');";
        Btn_Busqueda_Proveedores.Attributes.Add("onclick", Ventana_Modal + "?Fecha=False'" + Propiedades);
    }
    private void Borrar_Ventana_Emergente_Busqueda_Refacciones()
    {
        Btn_Busqueda_Proveedores.Attributes.Clear();
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Configurar_Formulario
    ///DESCRIPCIÓN: Cargar Combos y Datos del formulario
    ///CREO: jtoledo
    ///FECHA_CREO: 17/May/2012
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    private void Configurar_Formulario()
    {
        try
        {

        }
        catch (Exception ex) { Mensaje_Error(ex.Message); }

    }
    ///*******************************************************************************
    ///NOMBRE DE LA METODO: LLenar_Combo_Id
    ///        DESCRIPCIÓN: llena todos los combos
    ///         PARAMETROS: 1.- Obj_DropDownList: Combo a llenar
    ///                     2.- Dt_Temporal: DataTable genarada por una consulta a la base de datos
    ///                     3.- Texto: nombre de la columna del dataTable que mostrara el texto en el combo
    ///                     3.- Valor: nombre de la columna del dataTable que mostrara el valor en el combo
    ///                     3.- Seleccion: Id del combo el cual aparecera como seleccionado por default
    ///               CREO: Jesus S. Toledo Rdz.
    ///         FECHA_CREO: 06/9/2010
    ///           MODIFICO:
    ///     FECHA_MODIFICO:
    /// CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_Combo_ID(DropDownList Obj_DropDownList, DataTable Dt_Temporal, String _Texto, String _Valor, String Seleccion)
    {
        String Texto = "";
        try
        {
            Obj_DropDownList.Items.Clear();
            Obj_DropDownList.Items.Add(new ListItem("< SELECCIONAR >", "0"));
            foreach (DataRow row in Dt_Temporal.Rows)
            {
                if (_Texto.Contains("+"))
                {
                    String[] Array_Texto = _Texto.Split('+');

                    foreach (String Campo in Array_Texto)
                    {
                        Texto = Texto + row[Campo].ToString();
                        Texto = Texto + "  ";
                    }
                }
                else
                {
                    Texto = row[_Texto].ToString();
                }
                Obj_DropDownList.Items.Add(new ListItem(Texto, row[_Valor].ToString()));
                Texto = "";
            }
            Obj_DropDownList.SelectedValue = Seleccion;
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }

    }
    public void Llenar_Combo_ID(DropDownList Obj_DropDownList)
    {
        try
        {
            Obj_DropDownList.Items.Clear();
            Obj_DropDownList.Items.Add(new ListItem("< SELECCIONAR >", "0"));
            Obj_DropDownList.SelectedValue = "0";
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }

    }
    ///****************************************************************************************
    ///NOMBRE DE LA FUNCION:Mensaje_Error
    ///DESCRIPCION : Muestra el error
    ///PARAMETROS  : P_Texto: texto de un TextBox
    ///CREO        : Toledo Rodriguez Jesus S.
    ///FECHA_CREO  : 04-Septiembre-2010
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACION:
    ///****************************************************************************************
    private void Mensaje_Error(String P_Mensaje)
    {
        Img_Error.Visible = true;
        Lbl_Mensaje_Error.Text += P_Mensaje + "</br>";
    }
    private void Mensaje_Error()
    {

        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Lbl_Ecabezado_Mensaje.Text = "";
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Estado_Botones
    ///DESCRIPCIÓN: Metodo para establecer el estado de los botones y componentes del formulario
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 02/02/2011 05:49:53 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Estado_Botones(int P_Estado)
    {
        Boolean Estado = false;
        switch (P_Estado)
        {
            case 0: //Estado inicial  
                Btn_Salir.AlternateText = "Inicio";

                Btn_Salir.ToolTip = "Inicio";

                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                Btn_Cancelar_Servicio.Visible = false;
                Btn_Salir.Visible = true;
                Div_Campos.Style.Value = "display:none;";
                Div_Listado_Servicios.Style.Value = "display:none;";

                Estado = false;
                Borrar_Ventana_Emergente_Busqueda_Refacciones();
                //Configuracion_Acceso("Frm_Cat_Tal_Servicios_Preventivos.aspx");
                break;

            case 1: //Seleccionado  
                Btn_Salir.AlternateText = "Cancelar";

                Btn_Salir.ToolTip = "Cancelar";

                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";

                Btn_Salir.Visible = true;
                Btn_Cancelar_Servicio.Visible = true;
                Div_Campos.Style.Value = "display:none;";
                Div_Listado_Servicios.Style.Value = "display:none;";
                Estado = true;
                Cargar_Ventana_Emergente_Busqueda_Refacciones();
                break;

            case 2: //Modificar                    

                Btn_Salir.AlternateText = "Cancelar";

                Btn_Salir.ToolTip = "Cancelar";

                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";

                Div_Campos.Style.Value = "display:none;";
                Div_Listado_Servicios.Style.Value = "display:none;";

                Btn_Salir.Visible = true;
                Btn_Cancelar_Servicio.Visible = true;
                Estado = true;
                Cargar_Ventana_Emergente_Busqueda_Refacciones();
                break;

        }

        //Txt_Descripcion.Enabled = Estado;
        //Txt_Nombre.Enabled = Estado;
        //Cmb_Estatus.Enabled = Estado;

    }

    #endregion

    #region Metodos Operacion
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Asignar_Proveedor_Servicio
    ///DESCRIPCIÓN: Asigna el Mecanico al Servicio.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Asignar_Proveedor_Servicio()
    {
        Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio Negocio = new Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio();            
            Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value.Trim());
            Negocio.P_Proveedor_ID = Hdf_Proveedor_ID.Value.Trim();
            Negocio.P_Estatus = "GENERADO";
            Negocio.P_Estatus_Servicio = "ASIGNADO_PROVEEDOR";
            if (Cmb_Tipo_Servicio.SelectedValue.Trim().Equals("VERIFICACION")) Negocio.P_Estatus_Servicio = "SALIDA_PROVEEDOR";
            Negocio.P_Tipo = "SERVICIO_PREVENTIVO";
            if (Cmb_Tipo_Servicio.SelectedValue.Trim().Equals("SERVICIO_CORRECTIVO")) Negocio.P_Tipo = "SERVICIO_CORRECTIVO";
            Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;
            Negocio.Alta_Proveedor_Servicio();
     
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cancelar_Servicio_Proveedor
    ///DESCRIPCIÓN:Cancela el Servicio para el proveedor asignado.
    ///PROPIEDADES:     
    ///CREO: Jesus Toledo Rodriguez.
    ///FECHA_CREO: 12/Octubre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Cancelar_Servicio_Proveedor()
    {
        if (Cmb_Tipo_Servicio.SelectedItem.Value.Trim().Equals("SERVICIO_PREVENTIVO")) {
                    Cls_Ope_Tal_Servicios_Preventivos_Negocio Negocio = new Cls_Ope_Tal_Servicios_Preventivos_Negocio();
                    Negocio.P_Mecanico_ID = Hdf_Mecanico_ID.Value.Trim();
                    Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value.Trim());
                    Negocio.P_Estatus = "ASIGNADO_MECANICO";
                    Negocio.P_Estatus_Solicitud = null;
                    Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                    Negocio.Modifica_Servicio_Preventivo();
        }
        else if (Cmb_Tipo_Servicio.SelectedItem.Value.Trim().Equals("SERVICIO_CORRECTIVO"))
        {
            Cls_Ope_Tal_Servicios_Correctivos_Negocio Negocio = new Cls_Ope_Tal_Servicios_Correctivos_Negocio();
            Negocio.P_No_Servicio = Convert.ToInt32(Hdf_No_Servicio.Value.Trim());
            Negocio.P_Mecanico_ID = Hdf_Mecanico_ID.Value.Trim();
            Negocio.P_Estatus = "ASIGNADO_MECANICO";
            Negocio.P_Estatus_Solicitud = null;
            Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;
            Negocio.Modifica_Servicio_Correctivo();
        }
    }
    ///*******************************************************************************************************
    /// NOMBRE_FUNCIÓN: Formar_Tabla_Refacciones
    /// DESCRIPCIÓN: Crear tabla con columnas para almacenar refacciones seleccionadas
    /// PARÁMETROS:
    /// CREO: Jesus Toledo
    /// FECHA_CREO: 01-may-2012
    /// MODIFICÓ: 
    /// FECHA_MODIFICÓ: 
    /// CAUSA_MODIFICACIÓN: 
    ///*******************************************************************************************************
    private DataTable Formar_Tabla_Refacciones()
    {
        // tabla y columnas
        DataTable Dt_Refacciones = new DataTable();

        // agregar columnas a la tabla        
        Dt_Refacciones.Columns.Add("REFACCION_ID", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("CLAVE", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("CANTIDAD", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("NOMBRE", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("DESCRIPCION", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("SELECCIONADO", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("TIPO", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("ESTATUS", System.Type.GetType("System.String"));
        // regresar tabla
        return Dt_Refacciones;
    }

    #endregion

    #region Validaciones

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Asignacion
    ///DESCRIPCIÓN: Valida la Solicitud de Servicio antes de ser Asignada a un mecanico
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private Boolean Validar_Asignacion()
    {        
        String Mensaje_Error_ = "";
        Boolean Validacion = true;
        if (Hdf_No_Servicio.Value.Trim().Length == 0)
        {
            Mensaje_Error_ = Mensaje_Error_ + "+ Seleccionar el Servicio que se realizará.";
            Mensaje_Error_ = Mensaje_Error_ + " <br />";
            Validacion = false;
        }
        if (String.IsNullOrEmpty( Hdf_Proveedor_ID.Value ))
        {
            Mensaje_Error_ = Mensaje_Error_ + "+ Seleccionar el Proveedor que hará el Servicio.";
            Mensaje_Error_ = Mensaje_Error_ + " <br />";
            Validacion = false;
        }
        if (!Validacion)
        {
            Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
            Mensaje_Error( Mensaje_Error_);
        }
        return Validacion;
    }

    #endregion

    #region Llenado de Campos [Combos, Listados, Vehiculos]
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Generar_Tabla_Informativa
    /// DESCRIPCION: Genera la tabla para almacenar los datos de las fechas de los cambios de llantas o baterias
    /// PARAMETROS: 
    /// CREO: Jesus Toledo Rodriguez
    /// FECHA_CREO: 10-Oct-2012
    /// MODIFICO:
    /// FECHA_MODIFICO:
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private DataTable Generar_Tabla_Informativa()
    {
        DataTable Tabla_Nueva = new DataTable();
        DataColumn Columna0_Cambio;
        DataColumn Columna1_Fecha_Cambio;

        try
        {
            // ---------- Inicializar columnas
            Columna0_Cambio = new DataColumn();
            Columna0_Cambio.DataType = System.Type.GetType("System.String");
            Columna0_Cambio.ColumnName = "CAMBIO";
            Tabla_Nueva.Columns.Add(Columna0_Cambio);
            Columna1_Fecha_Cambio = new DataColumn();
            Columna1_Fecha_Cambio.DataType = System.Type.GetType("System.String");
            Columna1_Fecha_Cambio.ColumnName = "FECHA_CAMBIO";
            Tabla_Nueva.Columns.Add(Columna1_Fecha_Cambio);

            return Tabla_Nueva;
        }
        catch (Exception ex)
        {
            throw new Exception("Generar_Tabla_Documentos " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Unidades_Responsables
    ///DESCRIPCIÓN: Se llena el Combo de las Unidades Responsables.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Llenar_Combo_Unidades_Responsables()
    {
        Cls_Ope_Tal_Consultas_Generales_Negocio Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
        Negocio.P_Estatus = "ACTIVO";
        DataTable Dt_Dependencias = Negocio.Consultar_Unidades_Responsables();
        Cmb_Unidad_Responsable.DataSource = Dt_Dependencias;
        Cmb_Unidad_Responsable.DataTextField = "CLAVE_NOMBRE";
        Cmb_Unidad_Responsable.DataValueField = "DEPENDENCIA_ID";
        Cmb_Unidad_Responsable.DataBind();
        Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("< - - SELECCIONE - - >", ""));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Servicios
    ///DESCRIPCIÓN: Se llena el Listado de los Servicios para asignar el Mecanico.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Llenar_Listado_Servicios()
    {
        DataTable Dt_Resultados = new DataTable();
        DataView Dv_Resultados = new DataView();
        Cls_Ope_Tal_Servicios_Preventivos_Negocio Serv_Prev = new Cls_Ope_Tal_Servicios_Preventivos_Negocio();
        Cls_Ope_Tal_Servicios_Correctivos_Negocio Serv_Correc = new Cls_Ope_Tal_Servicios_Correctivos_Negocio();
        Serv_Prev.P_Reparacion = "EXTERNA";
        Serv_Prev.P_Estatus = "REPARACION";
        Serv_Prev.P_Tipo_Servicio = "SERVICIO_GENERAL','REVISTA_MECANICA','SERVICIO_PREVENTIVO";
        Serv_Correc.P_Estatus = "REPARACION";
        Serv_Correc.P_Reparacion = "EXTERNA";
        Serv_Correc.P_Tipo_Servicio = "SERVICIO_GENERAL','REVISTA_MECANICA','SERVICIO_CORRECTIVO";
        if (!String.IsNullOrEmpty(Txt_Buscar.Text.Trim()))
        {
            Serv_Correc.P_Folio_Solicitud = Convert.ToInt32(Txt_Buscar.Text.Trim());
            Serv_Prev.P_Folio_Solicitud = Convert.ToInt32( Txt_Buscar.Text.Trim());
        }

        if (Session["Frm_Ope_Tal_Generacion_Vista_Refacciones_Listado"] == null)
        {


            DataTable Dt_Resultados_Correctivos = Serv_Correc.Consultar_Servicios_Correctivos();
            DataTable Dt_Resultados_Preventivos = Serv_Prev.Consultar_Servicios_Preventivos();

            Dt_Resultados_Preventivos.Merge(Dt_Resultados_Correctivos);
            Dt_Resultados_Preventivos.AcceptChanges();
            Dt_Resultados = Dt_Resultados_Preventivos.Copy();
            Grid_Listado_Servicios.Columns[1].Visible = true;
            Grid_Listado_Servicios.Columns[2].Visible = true;
            Grid_Listado_Servicios.Columns[3].Visible = true;
            Grid_Listado_Servicios.DataSource = Dt_Resultados;
            Grid_Listado_Servicios.DataBind();
            Grid_Listado_Servicios.Columns[1].Visible = false;
            Grid_Listado_Servicios.Columns[2].Visible = false;
            Grid_Listado_Servicios.Columns[3].Visible = false;

            Session["Frm_Ope_Tal_Generacion_Lista_Refacciones_Listado"] = Dt_Resultados;
        }
        else
        {
            Dv_Resultados = (DataView)Session["Frm_Ope_Tal_Generacion_Vista_Refacciones_Listado"];
            Grid_Listado_Servicios.Columns[1].Visible = true;
            Grid_Listado_Servicios.Columns[2].Visible = true;
            Grid_Listado_Servicios.Columns[3].Visible = true;
            Grid_Listado_Servicios.DataSource = Dv_Resultados;
            Grid_Listado_Servicios.DataBind();
            Grid_Listado_Servicios.Columns[1].Visible = false;
            Grid_Listado_Servicios.Columns[2].Visible = false;
            Grid_Listado_Servicios.Columns[3].Visible = false;
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Observaciones
    ///DESCRIPCIÓN: Se llena el Listado de los Servicios para asignar el Mecanico.
    ///PROPIEDADES:     
    ///CREO: Jesus Toledo
    ///FECHA_CREO: 30/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Llenar_Listado_Observaciones()
    {
        Pnl_Historico.Style.Value = "display:inline;";
        Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio Negocio = new Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio();
        Negocio.P_No_Servicio = Int32.Parse(Hdf_No_Servicio.Value.Trim());
        Negocio.P_Tipo = "PREV";
        if (Cmb_Tipo_Servicio.SelectedValue.Trim().Equals("SERVICIO_CORRECTIVO")) Negocio.P_Tipo = "CORR";
        DataTable Dt_Resultados = Negocio.Consulta_Proveedor_Servicio();
        
        Grid_Historial_Proveedores.DataSource = Dt_Resultados;
        Grid_Historial_Proveedores.DataBind();

        if (Dt_Resultados.Rows.Count<=0)
        {
            Pnl_Historico.Style.Value = "display:none;";
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Mecanicos
    ///DESCRIPCIÓN: Se llena el Listado de los Mecanicos.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    //private void Llenar_Combo_Mecanicos()
    //{
    //    Cls_Cat_Tal_Mecanicos_Negocio Negocio = new Cls_Cat_Tal_Mecanicos_Negocio();
    //    Negocio.P_Estatus = "VIGENTE";
    //    DataTable Dt_Resultados = Negocio.Consultar_Mecanicos();
    //    Cmb_Mecanicos.DataSource = Dt_Resultados;
    //    Cmb_Mecanicos.DataTextField = "NOMBRE_EMPLEADO";
    //    Cmb_Mecanicos.DataValueField = "MECANICO_ID";
    //    Cmb_Mecanicos.DataBind();
    //    Cmb_Mecanicos.Items.Insert(0, new ListItem("< - SELECCIONE - >", ""));
    //}

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Vehiculo
    ///DESCRIPCIÓN: Se cargan los Datos del Vehiculo Seleccionado.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Cargar_Datos_Vehiculo(String Vehiculo, String Tipo_Busqueda)
    {
        Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
        switch (Tipo_Busqueda)
        {
            case "NO_INVENTARIO":
                Consulta_Negocio.P_No_Inventario = Vehiculo;
                break;
            case "IDENTIFICADOR":
                Consulta_Negocio.P_Vehiculo_ID = Vehiculo;
                break;
            default: break;
        }
        if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Consulta_Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value; }
        DataTable Dt_Vehiculo = Consulta_Negocio.Consultar_Vehiculos();
        if (Dt_Vehiculo.Rows.Count > 0)
        {
            Hdf_Vehiculo_ID.Value = Dt_Vehiculo.Rows[0]["VEHICULO_ID"].ToString().Trim();
            Txt_No_Inventario.Text = Dt_Vehiculo.Rows[0]["NO_INVENTARIO"].ToString().Trim();
            Txt_No_Economico.Text = Dt_Vehiculo.Rows[0]["NO_ECONOMICO"].ToString().Trim();
            Txt_Datos_Vehiculo.Text = Dt_Vehiculo.Rows[0]["VEHICULO_DESCRIPCION"].ToString().Trim();
            Txt_Placas.Text = Dt_Vehiculo.Rows[0]["PLACAS"].ToString().Trim();
            Txt_Anio.Text = Dt_Vehiculo.Rows[0]["ANIO"].ToString().Trim();
            Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Vehiculo.Rows[0]["DEPENDENCIA_ID"].ToString()));
        }
        else
        {
            Lbl_Ecabezado_Mensaje.Text = "El Vehículo no se encontro.";
            if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Mensaje_Error("[Puede ser que no este asignado a la Unidad Responsable Seleccionada]."); }
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cargar_Datos_Bien_Mueble
    ///DESCRIPCIÓN: Se cargan los Datos del Bien Mueble Seleccionado.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Cargar_Datos_Bien_Mueble(String Bien_Mueble, String Tipo_Busqueda)
    {
        Cls_Ope_Tal_Consultas_Generales_Negocio Consulta_Negocio = new Cls_Ope_Tal_Consultas_Generales_Negocio();
        switch (Tipo_Busqueda)
        {
            case "NO_INVENTARIO":
                Consulta_Negocio.P_No_Inventario = Bien_Mueble;
                break;
            case "IDENTIFICADOR":
                Consulta_Negocio.P_Bien_Mueble_ID = Bien_Mueble;
                break;
            default: break;
        }
        DataTable Dt_Bienes_Muebles = Consulta_Negocio.Consultar_Bien_Mueble();
        if (Dt_Bienes_Muebles.Rows.Count > 0)
        {
            Hdf_Bien_Mueble_ID.Value = Dt_Bienes_Muebles.Rows[0]["BIEN_MUEBLE_ID"].ToString().Trim();
            Txt_No_Inventario_BM.Text = Dt_Bienes_Muebles.Rows[0]["NO_INVENTARIO"].ToString().Trim();
            Txt_Numero_Serie_Bien.Text = Dt_Bienes_Muebles.Rows[0]["NUMERO_SERIE"].ToString().Trim();
            Txt_Descripcion_Bien.Text = Dt_Bienes_Muebles.Rows[0]["DESCRIPCION_BIEN_MUEBLE"].ToString().Trim();
            if (!String.IsNullOrEmpty(Dt_Bienes_Muebles.Rows[0]["DEPENDENCIA_ID"].ToString()))
            {
                Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Bienes_Muebles.Rows[0]["DEPENDENCIA_ID"].ToString()));
            }
            else
            {
                Lbl_Mensaje_Error.Text = "ERROR [Puede ser que el Bien esté dado de Baja].";
                Cmb_Unidad_Responsable.SelectedIndex = 0;
            }
        }
        else
        {
            Lbl_Ecabezado_Mensaje.Text = "El Bien no se encontro.";
            if (Cmb_Unidad_Responsable.SelectedIndex > 0) { Lbl_Mensaje_Error.Text = "[Puede ser que no este asignado a la Unidad Responsable Seleccionada]."; }
            else { Lbl_Mensaje_Error.Text = ""; }
        }
    }
    #endregion

    #region Generales [Configuracion, Limpiar]

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
    ///DESCRIPCIÓN: Limpia los campos del Formulario.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Limpiar_Formulario()
    {
        Hdf_No_Entrada.Value = "";
        Hdf_No_Servicio.Value = "";
        Hdf_No_Solicitud.Value = "";
        Hdf_Proveedor_ID.Value = "";
        Hdf_No_Reserva.Value = "";
        Txt_Folio.Text = "";
        Txt_Fecha_Elaboracion.Text = "";
        Txt_Fecha_Recepcion.Text = "";
        Txt_Kilometraje.Text = "";
        Cmb_Tipo_Servicio.SelectedIndex = 0;
        Cmb_Unidad_Responsable.SelectedIndex = 0;
        Txt_Descripcion_Servicio.Text = "";
        Hdf_Vehiculo_ID.Value = "";
        Txt_No_Inventario.Text = "";
        Txt_No_Economico.Text = "";
        Txt_Datos_Vehiculo.Text = "";
        Txt_Placas.Text = "";
        Txt_Anio.Text = "";
        Txt_Nombre_Proveedor.Text = "";
        Txt_Diagnostico_Servicio.Text = "";
        Session["Frm_Ope_Tal_Generacion_Vista_Refacciones_Listado"] = null;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
    ///DESCRIPCIÓN: Se Habilitan y/o inhabilitan los campos dependendiendo de la Condicion.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private void Configuracion_Formulario(String Operacion)
    {
        switch (Operacion)
        {
            case "INICIAL":
                Div_Campos.Visible = false;
                Div_Listado_Servicios.Visible = true;
                Btn_Cancelar_Servicio.Visible = false;
                Btn_Asignacion.Visible = false;
                break;
            case "OPERACION":
                Div_Campos.Visible = true;
                Div_Listado_Servicios.Visible = false;
                Btn_Cancelar_Servicio.Visible = true;
                Btn_Asignacion.Visible = true;
                break;
        }
    }

    #endregion

    #endregion

    #region [Eventos]
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Asignacion_Mecanico_Click
    ///DESCRIPCIÓN: Controla las operaciones del Boton Asignacion
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Asignacion_Click(object sender, ImageClickEventArgs e)
    {
        if (Validar_Asignacion())
        {
            Asignar_Proveedor_Servicio();
            Llenar_Listado_Servicios();
            Configuracion_Formulario("INICIAL");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "GACO", "alert('Operacion Exitosa: Asignación de Proveedor a Servicio.\\nServicio Folio: " + Txt_Folio.Text.Trim() + "\\nProveedor Asignado: " + Txt_Nombre_Proveedor.Text.Trim() + " ');", true);
            Limpiar_Formulario();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Cancelar_Servicio_Click
    ///DESCRIPCIÓN: Controla las operaciones del Boton Cerrar Reparacion
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 4/Junio/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Cancelar_Servicio_Click(object sender, ImageClickEventArgs e)
    {
        Cancelar_Servicio_Proveedor();
        Llenar_Listado_Observaciones();
        Configuracion_Formulario("INICIAL");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "GACO", "alert('Operacion Exitosa: Servicio: " + Hdf_No_Servicio.Value.ToString() + "\n El servicio se pasó a Diagnóstico Mecánico. " + "');", true);        
        Limpiar_Formulario();
        Llenar_Listado_Servicios();
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Controla las operaciones del Boton de Salir
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        if (Div_Campos.Visible)
        {
            Limpiar_Formulario();
            Configuracion_Formulario("INICIAL");
        }
        else
        {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Evento que agarra la session de las refacciones seleccionadas
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 10/Mayo/2011 12:55:33 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    protected void Btn_Busqueda_Proveedores_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["Proveedor_ID"] != null && Session["Nombre_Proveedor"] != null)
        {
            Hdf_Proveedor_ID.Value = Session["Proveedor_ID"].ToString();
            Txt_Nombre_Proveedor.Text = Session["Nombre_Proveedor"].ToString();
        }
        Session["Proveedor_ID"] = null;
        Session["Nombre_Proveedor"] = null;
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Evento que agarra la session de las refacciones seleccionadas
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 10/Mayo/2011 12:55:33 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        Session["Frm_Ope_Tal_Generacion_Vista_Refacciones_Listado"] = null;
        Llenar_Listado_Servicios();
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Servicios_PageIndexChanging
    ///DESCRIPCIÓN: Maneja la paginación del Listado
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Listado_Servicios_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Grid_Listado_Servicios.SelectedIndex = (-1);
            Grid_Listado_Servicios.PageIndex = e.NewPageIndex;
            Llenar_Listado_Servicios();
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Sorting
    ///DESCRIPCIÓN: se obtienen los datos del movimiento
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 07/26/2011 06:22:19 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Grid_Listado_Servicios_Sorting(object sender, GridViewSortEventArgs e)
    {
        String Orden = "";
        //Tabla que contendra la Lista de los servicios
        DataTable Dt_Listado = null;
        if (Session["Frm_Ope_Tal_Generacion_Lista_Refacciones_Listado"] != null)
            Dt_Listado = (DataTable)Session["Frm_Ope_Tal_Generacion_Lista_Refacciones_Listado"];
        //Si hay Datos
        if (Dt_Listado != null)
        {
            //Se realiza el Ordenamiento segun el campo seleccionado en nuestro Grid
            //Se Define la Vista donde quedara el ordenamiento
            DataView Dv_Listado = new DataView(Dt_Listado);
            if (ViewState["SortDirection"] != null)
                Orden = ViewState["SortDirection"].ToString();

            if (Orden.Equals("ASC"))
            {
                Dv_Listado.Sort = e.SortExpression + " " + "DESC";
                ViewState["SortDirection"] = "DESC";
            }
            else
            {
                Dv_Listado.Sort = e.SortExpression + " " + "ASC";
                ViewState["SortDirection"] = "ASC";
            }
            //Asignar Vista al Origen de Datos de nuestro Grid
            Grid_Listado_Servicios.DataSource = Dv_Listado;
            Grid_Listado_Servicios.DataBind();
            Session["Frm_Ope_Tal_Generacion_Vista_Refacciones_Listado"] = Dv_Listado;
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Servicios_SelectedIndexChanged
    ///DESCRIPCIÓN: Obtiene los datos de un Servicio 
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Listado_Servicios_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = Grid_Listado_Servicios.SelectedRow;
            if (Grid_Listado_Servicios.SelectedIndex > (-1)) {
                Limpiar_Formulario();
                Hdf_No_Entrada.Value = Grid_Listado_Servicios.SelectedDataKey["NO_ENTRADA"].ToString(); //HttpUtility.HtmlDecode(Grid_Listado_Servicios.SelectedRow.Cells[1].Text.Trim()).Trim();
                Hdf_No_Servicio.Value = Grid_Listado_Servicios.SelectedDataKey["NO_SERVICIO"].ToString(); //HttpUtility.HtmlDecode(Grid_Listado_Servicios.SelectedRow.Cells[2].Text.Trim()).Trim();
                Hdf_No_Solicitud.Value = Grid_Listado_Servicios.SelectedDataKey["NO_SOLICITUD"].ToString();//HttpUtility.HtmlDecode(Grid_Listado_Servicios.SelectedRow.Cells[3].Text.Trim()).Trim();
                Hdf_Mecanico_ID.Value = Grid_Listado_Servicios.SelectedDataKey["MECANICO_ID"].ToString();
                Mostrar_Registro();
                Llenar_Listado_Observaciones();
                Configuracion_Formulario("OPERACION");
                Grid_Listado_Servicios.SelectedIndex = -1;
                System.Threading.Thread.Sleep(500);
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Servicios_RowCreated
    ///DESCRIPCIÓN: Prueba de metodo para establecer seleccion en todo el renglon del grid
    ///PROPIEDADES:     
    ///CREO: Jesus Toledo Rodriguez
    ///FECHA_CREO: 09/Enero/2013
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Listado_Servicios_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes["onmouseover"] = "this.style.cursor='pointer';this.style.textDecoration='none';";
            e.Row.Attributes["onmouseout"] = "this.style.textDecoration='none';";
            e.Row.ToolTip = "Click para seleccionar renglon";
            e.Row.Attributes["onclick"] = this.Page.ClientScript.GetPostBackClientHyperlink(this.Grid_Listado_Servicios, "Select$" + e.Row.RowIndex);
        }
    }
    #endregion
    
}