﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Catalogo_Taller_Refacciones.Negocio;

public partial class paginas_Taller_Municipal_Ventanas_Emergentes_Frm_Busqueda_Refacciones : System.Web.UI.Page
{
    #region Variables
    private const int Const_Estado_Inicial = 0;
    private const int Const_Estado_Nuevo = 1;
    private const int Const_Estado_Modificar = 2;
    #endregion

    #region Page Load / Init
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {            
            //Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            //if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!Page.IsPostBack)
            {
                Limpiar_Formulario();
                Estado_Botones(Const_Estado_Inicial);
                //Configuracion_Acceso("Frm_Cat_Tal_Refacciones.aspx");
                //Cargar_Refacciones(0);
            }
            Mensaje_Error();
        }
        catch (Exception Ex)
        {
            //Mensaje_Error(Txt_Pagos.Text.Trim() +" - "+ Ex.Message);
            Estado_Botones(Const_Estado_Inicial);
        }
    }
    #endregion

    #region Metodos

    #region Metodos Generales
    ///*******************************************************************************
    ///NOMBRE DE LA METODO: LLenar_Combo_Id
    ///        DESCRIPCIÓN: llena todos los combos
    ///         PARAMETROS: 1.- Obj_DropDownList: Combo a llenar
    ///                     2.- Dt_Temporal: DataTable genarada por una consulta a la base de datos
    ///                     3.- Texto: nombre de la columna del dataTable que mostrara el texto en el combo
    ///                     3.- Valor: nombre de la columna del dataTable que mostrara el valor en el combo
    ///                     3.- Seleccion: Id del combo el cual aparecera como seleccionado por default
    ///               CREO: Jesus S. Toledo Rdz.
    ///         FECHA_CREO: 06/9/2010
    ///           MODIFICO:
    ///     FECHA_MODIFICO:
    /// CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_Combo_ID(DropDownList Obj_DropDownList, DataTable Dt_Temporal, String _Texto, String _Valor, String Seleccion)
    {
        String Texto = "";
        try
        {
            Obj_DropDownList.Items.Clear();
            Obj_DropDownList.Items.Add(new ListItem("< SELECCIONAR >", "0"));
            foreach (DataRow row in Dt_Temporal.Rows)
            {
                if (_Texto.Contains("+"))
                {
                    String[] Array_Texto = _Texto.Split('+');

                    foreach (String Campo in Array_Texto)
                    {
                        Texto = Texto + row[Campo].ToString();
                        Texto = Texto + "  ";
                    }
                }
                else
                {
                    Texto = row[_Texto].ToString();
                }
                Obj_DropDownList.Items.Add(new ListItem(Texto, row[_Valor].ToString()));
                Texto = "";
            }
            Obj_DropDownList.SelectedValue = Seleccion;
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }

    }
    public void Llenar_Combo_ID(DropDownList Obj_DropDownList)
    {
        try
        {
            Obj_DropDownList.Items.Clear();
            Obj_DropDownList.Items.Add(new ListItem("< SELECCIONAR >", "0"));
            Obj_DropDownList.SelectedValue = "0";
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }

    }
    ///****************************************************************************************
    ///NOMBRE DE LA FUNCION:Mensaje_Error
    ///DESCRIPCION : Muestra el error
    ///PARAMETROS  : P_Texto: texto de un TextBox
    ///CREO        : Toledo Rodriguez Jesus S.
    ///FECHA_CREO  : 04-Septiembre-2010
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACION:
    ///****************************************************************************************
    private void Mensaje_Error(String P_Mensaje)
    {
        IBtn_Imagen_Error.Visible = true;
        Lbl_Mensaje_Error.Text += P_Mensaje + "</br>";
    }
    private void Mensaje_Error()
    {
        IBtn_Imagen_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Lbl_Ecabezado_Mensaje.Text = "";
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Estado_Botones
    ///DESCRIPCIÓN: Metodo para establecer el estado de los botones y componentes del formulario
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 02/02/2011 05:49:53 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Estado_Botones(int P_Estado)
    {
        Boolean Estado = false;
        switch (P_Estado)
        {
            case 0: //Estado inicial  
                Btn_Seleccionar.AlternateText = "Seleccionar";
                Btn_Limpiar_Ctlr_Busqueda.AlternateText = "Limpiar";
                Btn_Regresar.AlternateText = "Regresar";
                
                Estado = true;
                //Configuracion_Acceso("Frm_Cat_Tal_Refacciones.aspx");
                break;

            case 1: //Nuevo  
                Estado = true;
                break;

            case 2: //Modificar                    
                Estado = true;
                break;
        }

        Txt_Descripcion.Enabled = Estado;
        Txt_Nombre.Enabled = Estado;
        Cmb_Tipo.Enabled = Estado;
        Txt_Clave.Enabled = Estado;
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
    ///DESCRIPCIÓN: Metodo para establecer el estado de los botones y componentes del formulario
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 02/02/2011 05:49:53 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Limpiar_Formulario()
    {
        Session["Tabla_Refacciones_B"] = null;
        Session["Tabla_Refacciones_Modificados_B"] = null;
        Txt_Clave.Text = "";
        Txt_Nombre.Text = "";
        Txt_Descripcion.Text = "";
        Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue("VIGENTE"));
        Cmb_Tipo.SelectedIndex = 0;
        Grid_Refacciones.PageIndex = 0;
        Grid_Refacciones.SelectedIndex = (-1);
        Grid_Refacciones.DataSource = null;
        Grid_Refacciones.DataBind();
    }

    #endregion

    #region Metodos ABC    
    ///******************************************************************************* 
    ///NOMBRE DE LA CLASE: Cargar_Refacciones
    ///DESCRIPCIÓN: consulta y muestra los datos, instancia la clase de negocios para acceder a la consulta
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 15/May/2012 06:24:15 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    private void Cargar_Refacciones(int Page_Index)
    {
        DataTable Dt_Refacciones = new DataTable();
        Cls_Cat_Tal_Refacciones_Negocio Refacciones_Negocio = new Cls_Cat_Tal_Refacciones_Negocio();
        String Mi_SQL = "";
        if (Session["Tabla_Refacciones_Modificados_B"] != null)
            Dt_Refacciones = (DataTable)Session["Tabla_Refacciones_Modificados_B"];
                
        try
        {
            if (Session["Tabla_Refacciones_Modificados_B"] != null)
            {
                Grid_Refacciones.PageIndex = Page_Index;
                Grid_Refacciones.DataSource = Dt_Refacciones;
                Grid_Refacciones.DataBind();
            }
            else
            {
            if (Session["Tabla_Refacciones_B"] != null)
                {
                    Dt_Refacciones = (DataTable)Session["Tabla_Refacciones_B"];
                }
                else
                {
                    Refacciones_Negocio.P_Clave = Txt_Clave.Text.Trim().ToUpper();
                    Refacciones_Negocio.P_Nombre = Txt_Nombre.Text.Trim().ToUpper();
                    Refacciones_Negocio.P_Descripcion = Txt_Descripcion.Text.Trim().ToUpper();
                    if (Cmb_Estatus.SelectedIndex > 0)
                        Refacciones_Negocio.P_Estatus = Cmb_Estatus.SelectedValue.Trim().ToUpper();
                    if (Cmb_Tipo.SelectedIndex > 0)
                        Refacciones_Negocio.P_Tipo = Cmb_Tipo.SelectedValue.Trim().ToUpper();
                    Refacciones_Negocio.P_Campos_Dinamicos = "";
                    Mi_SQL = Mi_SQL + " " + Cat_Tal_Refacciones.Campo_Refaccion_ID + "";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Clave + "";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Nombre + "";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Estatus + "";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Tipo + "";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Descripcion + "";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Unidad_ID + "";
                    Mi_SQL = Mi_SQL + ",'NO' AS SELECCIONADO ";
                    Mi_SQL = Mi_SQL + ",null AS CANTIDAD ";
                    Refacciones_Negocio.P_Campos_Dinamicos = Mi_SQL;

                    Dt_Refacciones = Refacciones_Negocio.Consulta_Busqueda_Refacciones();
                }
                if (Dt_Refacciones.Rows.Count > 0)
                {
                    Session["Tabla_Refacciones_B"] = Dt_Refacciones;
                    Grid_Refacciones.PageIndex = Page_Index;
                    Grid_Refacciones.DataSource = Dt_Refacciones;
                    Grid_Refacciones.DataBind();
                }
                else
                {
                    Grid_Refacciones.DataSource = null;
                    Grid_Refacciones.DataBind();
                    Mensaje_Error("No se encontraron refacciones con la clave proporcionada");
                }
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }

    #endregion

    #region Metodos/Validaciones
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Validar_Campos
    ///DESCRIPCIÓN: valdia que se ingresen los campos obligatorios
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 06/27/2011 11:31:53 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private Boolean Validar_Campos()
    {
        Boolean Resultado = true;

        if (Txt_Nombre.Text.Trim() == "")
        {
            Lbl_Ecabezado_Mensaje.Text = "Los siguientes datos son necesarios:";
            Mensaje_Error("- Nombre de la refacción");
            Resultado = false;
        }
        if (Txt_Clave.Text.Trim() == "")
        {
            Lbl_Ecabezado_Mensaje.Text = "Los siguientes datos son necesarios:";
            Mensaje_Error("- Clave de la refacción");
            Resultado = false;
        }
        if (Txt_Descripcion.Text.Trim() == "")
        {
            Lbl_Ecabezado_Mensaje.Text = "Los siguientes datos son necesarios:";
            Mensaje_Error("- Descripcion de la refacción");
            Resultado = false;
        }
        if (Cmb_Estatus.SelectedIndex <= 0)
        {
            Lbl_Ecabezado_Mensaje.Text = "Los siguientes datos son necesarios:";
            Mensaje_Error("- Estatus de la refacción");
            Resultado = false;
        }
        if (Cmb_Tipo.SelectedIndex <= 0)
        {
            Lbl_Ecabezado_Mensaje.Text = "Los siguientes datos son necesarios:";
            Mensaje_Error("- Tipo de la refacción");
            Resultado = false;
        }

        return Resultado;
    }
    #endregion

    #region Metodos Operacion
    ///*******************************************************************************************************
    /// NOMBRE_FUNCIÓN: Formar_Tabla_Refacciones
    /// DESCRIPCIÓN: Crear tabla con columnas para almacenar refacciones
    /// PARÁMETROS:
    /// CREO: Jesus Toledo
    /// FECHA_CREO: 01-may-2012
    /// MODIFICÓ: 
    /// FECHA_MODIFICÓ: 
    /// CAUSA_MODIFICACIÓN: 
    ///*******************************************************************************************************
    private DataTable Formar_Tabla_Refacciones()
    {
        // tabla y columnas
        DataTable Dt_Refacciones = new DataTable();

        // agregar columnas a la tabla        
        Dt_Refacciones.Columns.Add("REFACCION_ID", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("SELECCIONADO", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("CANTIDAD", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("CLAVE", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("NOMBRE", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("DESCRIPCION", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("TIPO", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("ESTATUS", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("UNIDAD_ID", System.Type.GetType("System.String"));
        // regresar tabla
        return Dt_Refacciones;
    }
    ///*******************************************************************************************************
    /// NOMBRE_FUNCIÓN: Formar_Tabla_Refacciones
    /// DESCRIPCIÓN: Crear tabla con columnas para almacenar refacciones seleccionadas
    /// PARÁMETROS:
    /// CREO: Jesus Toledo
    /// FECHA_CREO: 01-may-2012
    /// MODIFICÓ: 
    /// FECHA_MODIFICÓ: 
    /// CAUSA_MODIFICACIÓN: 
    ///*******************************************************************************************************
    private DataTable Formar_Tabla_Resultado()
    {
        // tabla y columnas
        DataTable Dt_Refacciones = new DataTable();

        // agregar columnas a la tabla        
        Dt_Refacciones.Columns.Add("REFACCION_ID", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("CANTIDAD", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("CLAVE", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("NOMBRE", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("DESCRIPCION", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("TIPO", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("ESTATUS", System.Type.GetType("System.String"));
        Dt_Refacciones.Columns.Add("UNIDAD_ID", System.Type.GetType("System.String"));
        // regresar tabla
        return Dt_Refacciones;
    }
    #endregion

    #endregion

    #region Eventos/Botones    
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Grid_Refacciones_RowDataBound
    ///DESCRIPCIÓN: se obtienen los datos de refaccion
    ///PARAMETROS: 
    ///CREO: Jesus Toledo Rodriguez
    ///FECHA_CREO: 05/Mayo/2012
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************  
    protected void Grid_Refacciones_DataBound(object sender, EventArgs e)
    {
        DataTable Dt_Datos = new DataTable();
        Dt_Datos = Formar_Tabla_Refacciones();
        
            if (Session["Tabla_Refacciones_Modificado_B"] != null)
            {
                Dt_Datos = (DataTable)Session["Tabla_Refacciones_B"];
            }
            else if (Session["Tabla_Refacciones_B"] != null)
            {
                Dt_Datos = (DataTable)Session["Tabla_Refacciones_B"];
            }
            for (Int32 Contador = 0; Contador < Grid_Refacciones.Rows.Count; Contador++)
            {
                if (Grid_Refacciones.Rows[Contador].Cells[0].FindControl("Chk_Seleccionar") != null)
                {
                    CheckBox Chk_Check_Seleccionar = (CheckBox)Grid_Refacciones.Rows[Contador].Cells[0].FindControl("Chk_Seleccionar");
                    if (!String.IsNullOrEmpty(Dt_Datos.Rows[Contador]["SELECCIONADO"].ToString()))
                    {
                        if (Dt_Datos.Rows[Contador + (Grid_Refacciones.PageIndex * Grid_Refacciones.PageSize)]["SELECCIONADO"].ToString().Trim().Equals("NO"))
                            Chk_Check_Seleccionar.Checked = false;
                        else if (Dt_Datos.Rows[Contador + (Grid_Refacciones.PageIndex * Grid_Refacciones.PageSize)]["SELECCIONADO"].ToString().Trim().Equals("SI"))
                            Chk_Check_Seleccionar.Checked = true;
                    }
                    else
                    {
                        Chk_Check_Seleccionar.Checked = false;
                    }
                }
            }
        
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
    ///DESCRIPCIÓN: se obtienen los datos para dar de alta refaccion
    ///PARAMETROS: 
    ///CREO: Jesus Toledo Rodriguez
    ///FECHA_CREO: 05/Mayo/2012
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************  
    protected void Btn_Busqueda_Click(object sender, EventArgs e)
    {
        try
        {
            Session["Tabla_Refacciones_B"] = null;
            Session["Tabla_Refacciones_Modificados_B"] = null;
            Cargar_Refacciones(0);
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Txt_Buscar_TextChanged
    ///DESCRIPCIÓN: Busqueda Principal del catalogo
    //////PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 03/Ago/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    protected void Txt_Clave_TextChanged(object sender, EventArgs e)
    {
        try
        {
            Session["Tabla_Refacciones_B"] = null;
            Session["Tabla_Refacciones_Modificados_B"] = null;
            Cargar_Refacciones(0);
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Txt_Buscar_TextChanged
    ///DESCRIPCIÓN: Busqueda Principal del catalogo
    //////PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 03/Ago/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    protected void Txt_Grid_Cantidad_TextChanged(object sender, EventArgs e)
    {
        TextBox Txt_Cantidad = null;
        DataTable Dt_Refacciones = new DataTable();
        if (Session["Tabla_Refacciones_Modificados_B"] != null)
            Dt_Refacciones = (DataTable)Session["Tabla_Refacciones_Modificados_B"];
        else if (Session["Tabla_Refacciones_B"] != null)
            Dt_Refacciones = (DataTable)Session["Tabla_Refacciones_B"];
        try
        {
            if (Dt_Refacciones.Rows.Count > 0)
            {
                for (Int32 Cont = 0; Cont < Grid_Refacciones.Rows.Count; Cont++)
                {
                    Txt_Cantidad = (TextBox)Grid_Refacciones.Rows[Cont].Cells[1].FindControl("Txt_Grid_Cantidad");
                    
                        Dt_Refacciones.Rows[Cont + (Grid_Refacciones.PageIndex * Grid_Refacciones.PageSize)]["CANTIDAD"] = Txt_Cantidad.Text.Trim();
                }
            }
            if (Dt_Refacciones.Rows.Count>0)
                Session["Tabla_Refacciones_Modificados_B"] = Dt_Refacciones;
            Cargar_Refacciones(Grid_Refacciones.PageIndex);
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Txt_Buscar_TextChanged
    ///DESCRIPCIÓN: Busqueda Principal del catalogo
    //////PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 03/Ago/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    protected void Txt_Nombre_TextChanged(object sender, EventArgs e)
    {
        try
        {
            Session["Tabla_Refacciones_B"] = null;
            Session["Tabla_Refacciones_Modificados_B"] = null;
            Cargar_Refacciones(0);
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Txt_Buscar_TextChanged
    ///DESCRIPCIÓN: Busqueda Principal del catalogo
    //////PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 03/Ago/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    protected void Txt_Descripcion_TextChanged(object sender, EventArgs e)
    {
        try
        {
            Session["Tabla_Refacciones_B"] = null;
            Session["Tabla_Refacciones_Modificados_B"] = null;
            Cargar_Refacciones(0);
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Regresar_Click
    ///DESCRIPCIÓN          : Evento Click del control Button Regresar
    ///PARAMETROS           : sender y e
    ///CREO                 : Jesus Toledo Rdz
    ///FECHA_CREO           : 02/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************  
    protected void Btn_Regresar_Click(object sender, ImageClickEventArgs e)
    {
        Session.Remove("Tabla_Refacciones_B");
        Session.Remove("Tabla_Refacciones_Modificados_B");
        Grid_Refacciones.DataSource = null;
        Grid_Refacciones.DataBind();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Cerrar", "window.close();", true);
        //ClientScript.RegisterStartupScript(this.GetType(), "Cerrar_Script", Pagina);
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Seleccionar_Click
    ///DESCRIPCIÓN          : Evento Click del control Button Seleccionar
    ///PARAMETROS           : sender y e
    ///CREO                 : Jesus Toledo Rdz
    ///FECHA_CREO           : 02/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************  
    protected void Btn_Seleccionar_Click(object sender, ImageClickEventArgs e)
    {
        CheckBox Chk_Check_Seleccionar = null;
        TextBox Txt_Cantidad = null;
        DataTable Dt_Seleccionados = Formar_Tabla_Resultado();
        DataTable Dt_Refacciones = Formar_Tabla_Refacciones();
        //Agarrar las refacciones seleccionadas y mandarlas a la session
        if (Session["Tabla_Refacciones_Modificados_B"] != null)
        {
            Dt_Refacciones = (DataTable)Session["Tabla_Refacciones_Modificados_B"];

            foreach (DataRow Refaccion in Dt_Refacciones.Rows)
            {
                if (Refaccion["SELECCIONADO"].ToString().Trim() == "SI")
                {
                    DataRow Dr_Seleccinado = Dt_Seleccionados.NewRow();
                    Dr_Seleccinado[Cat_Tal_Refacciones.Campo_Refaccion_ID] = Refaccion[Cat_Tal_Refacciones.Campo_Refaccion_ID];
                    Dr_Seleccinado[Cat_Tal_Refacciones.Campo_Clave] = Refaccion[Cat_Tal_Refacciones.Campo_Clave];
                    Dr_Seleccinado[Cat_Tal_Refacciones.Campo_Nombre] = Refaccion[Cat_Tal_Refacciones.Campo_Nombre];
                    Dr_Seleccinado[Cat_Tal_Refacciones.Campo_Descripcion] = Refaccion[Cat_Tal_Refacciones.Campo_Descripcion];
                    Dr_Seleccinado[Cat_Tal_Refacciones.Campo_Tipo] = Refaccion[Cat_Tal_Refacciones.Campo_Tipo];
                    Dr_Seleccinado[Cat_Tal_Refacciones.Campo_Estatus] = Refaccion[Cat_Tal_Refacciones.Campo_Estatus];
                    Dr_Seleccinado[Ope_Tal_Serv_Prev_Detalles.Campo_Cantidad] = Refaccion[Ope_Tal_Serv_Prev_Detalles.Campo_Cantidad];
                    Dr_Seleccinado[Cat_Tal_Refacciones.Campo_Unidad_ID] = Refaccion[Cat_Tal_Refacciones.Campo_Unidad_ID];
                    if (Refaccion[Ope_Tal_Serv_Prev_Detalles.Campo_Cantidad] != null)
                    {
                        if (!String.IsNullOrEmpty( Refaccion[Ope_Tal_Serv_Prev_Detalles.Campo_Cantidad].ToString()) && Refaccion[Ope_Tal_Serv_Prev_Detalles.Campo_Cantidad].ToString()!="0")
                        Dt_Seleccionados.Rows.Add(Dr_Seleccinado);
                    }
                }
            }            
        }

        Session.Remove("Tabla_Refacciones_B");
        Session.Remove("Tabla_Refacciones_Modificados_B");

        Grid_Refacciones.DataSource = null;
        Grid_Refacciones.DataBind();
        Session["Tabla_Refacciones_Seleccionados"] = Dt_Seleccionados;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Cerrar", "window.close();", true);
        //ClientScript.RegisterStartupScript(this.GetType(), "Cerrar_Script", Pagina);
    }
    
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Grid_Ordenes_Variacion_PageIndexChanging
    ///DESCRIPCIÓN: Paginar grid de ordenes de variacion
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 01/Dic/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************    
    protected void Grid_Refacciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Cargar_Refacciones(e.NewPageIndex);
    }    
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Chk_Seleccionar_CheckedChanged
    ///DESCRIPCIÓN: Seleccionar Refaccion
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 26/Abr/2012 03:03:06 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Chk_Seleccionar_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox Chk_Check_Seleccionar = null;
        TextBox Txt_Cantidad = null;
        DataTable Dt_Refacciones = new DataTable();
        if (Session["Tabla_Refacciones_Modificados_B"] != null)
            Dt_Refacciones = (DataTable)Session["Tabla_Refacciones_Modificados_B"];
        else if (Session["Tabla_Refacciones_B"] != null)
            Dt_Refacciones = (DataTable)Session["Tabla_Refacciones_B"];
        try
        {
            //Chk_Check_Seleccionar = (CheckBox)sender;
            //Refaccion_Seleccionado = Chk_Check_Seleccionar.Comman;
            if (Dt_Refacciones.Rows.Count > 0)
            {
                for (Int32 Cont = 0; Cont < Grid_Refacciones.Rows.Count; Cont++)
                {
                    Chk_Check_Seleccionar = (CheckBox)Grid_Refacciones.Rows[Cont].Cells[0].FindControl("Chk_Seleccionar");
                    Txt_Cantidad = (TextBox)Grid_Refacciones.Rows[Cont].Cells[1].FindControl("Txt_Grid_Cantidad");
                    if (Chk_Check_Seleccionar.Checked)
                    {
                        Dt_Refacciones.Rows[Cont + (Grid_Refacciones.PageIndex * Grid_Refacciones.PageSize)][Cat_Tal_Refacciones.Campo_Refaccion_ID] = Grid_Refacciones.DataKeys[Cont].Values["REFACCION_ID"];
                        Dt_Refacciones.Rows[Cont + (Grid_Refacciones.PageIndex * Grid_Refacciones.PageSize)]["SELECCIONADO"] = "SI";
                        Dt_Refacciones.Rows[Cont + (Grid_Refacciones.PageIndex * Grid_Refacciones.PageSize)]["CANTIDAD"] = Txt_Cantidad.Text.Trim();
                    }
                    else
                    {
                        Dt_Refacciones.Rows[Cont + (Grid_Refacciones.PageIndex * Grid_Refacciones.PageSize)][Cat_Tal_Refacciones.Campo_Refaccion_ID] = Grid_Refacciones.DataKeys[Cont].Values["REFACCION_ID"];
                        Dt_Refacciones.Rows[Cont + (Grid_Refacciones.PageIndex * Grid_Refacciones.PageSize)]["SELECCIONADO"] = "NO";
                        Dt_Refacciones.Rows[Cont + (Grid_Refacciones.PageIndex * Grid_Refacciones.PageSize)]["CANTIDAD"] = 0;
                        Txt_Cantidad.Text = "0";
                    }
                    
                }
            }
            if (Dt_Refacciones.Rows.Count > 0)
            {
                Session["Tabla_Refacciones_Modificados_B"] = Dt_Refacciones;
                Cargar_Refacciones(Grid_Refacciones.PageIndex);
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    #endregion    
}