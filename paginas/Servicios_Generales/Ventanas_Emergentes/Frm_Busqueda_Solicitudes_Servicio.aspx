﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Frm_Busqueda_Solicitudes_Servicio.aspx.cs" Inherits="paginas_Taller_Municipal_Ventanas_Emergentes_Frm_Busqueda_Refacciones_Stock" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Buscador Refacciones</title>
    <link href="../../estilos/estilo_paginas.css" rel="stylesheet" type="text/css" />
    <link href="../../estilos/estilo_masterpage.css" rel="stylesheet" type="text/css" />
    <link href="../../estilos/estilo_paginas.css" rel="stylesheet" type="text/css" />
    <link href="../../estilos/estilo_ajax.css" rel="stylesheet" type="text/css" />

    <base target="_self" />
    <script type="text/javascript">
        window.onerror = new Function("return true");
        function Limpiar_Controles() {
            if (document.getElementById("<%=IBtn_Imagen_Error.ClientID%>") != null) {
                document.getElementById("<%=IBtn_Imagen_Error.ClientID%>").style.visibility = "hidden";
            }
            document.getElementById("<%=Lbl_Ecabezado_Mensaje.ClientID%>").innerHTML = "";
            document.getElementById("<%=Lbl_Mensaje_Error.ClientID%>").innerHTML = "";

            document.getElementById("<%=Txt_No_Inventario.ClientID%>").value = "";
            document.getElementById("<%=Txt_Folio_Solicitud.ClientID%>").value = "";
            return false;
        }
        //Abrir una ventana modal
        function Abrir_Ventana_Modal(Url, Propiedades) {
            window.showModalDialog(Url, null, Propiedades);
        }
        function Validar_Longitud_Texto(Text_Box, Max_Longitud) {
            if (Text_Box.value.length > Max_Longitud) {
                Text_Box.value = Text_Box.value.substring(0, Max_Longitud);
            }
        }
        function formatCurrency(num) {
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
                    num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + num + '.' + cents);
        }
    </script>

    <script type="text/javascript" language="javascript">
    //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_sesiones.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion()
        {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }
        //Temporizador para matener la sesión activa
        setInterval('MantenSesion()', <%=(int)(0.9*(Session.Timeout * 60000))%>);        
    </script>
</head>
<body bgcolor="White">
    <form id="form1" runat="server">
     <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" 
    EnableScriptGlobalization="true"
        EnableScriptLocalization="true"
        EnablePartialRendering="true" 
        AsyncPostBackTimeout="9000">
</cc1:ToolkitScriptManager>
    <%--<asp:ScriptManager ID="ScriptManager1"  runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" EnablePartialRendering="true" AsyncPostBackTimeout="9000">
    </asp:ScriptManager>--%>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../../imagenes/paginas/Sias_Roler.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div id="Div_Contenido" style="background-color:#ffffff; width:100%; height:100%;">
                <table width="99%" cellpadding="2" cellspacing="0">
                    <tr>
                        <td style="color:Black; text-align:center; font-size:12; font-weight:bold;" colspan='4'>Búsqueda: de Solicitudes de Servicio</td>                        
                    </tr>
                    <tr class="barra_busqueda">
                        <td align="left" colspan="3" style="width:90%;"> 
                            <asp:ImageButton ID="Btn_Regresar" runat="server" 
                                AlternateText="Regresar" CssClass="Img_Button" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                onclick="Btn_Regresar_Click" Width="24px" />                            
                        </td>
                        <td colspan="1" style="text-align:left; width:10%"; text-align:right;">
                        <asp:ImageButton ID="Btn_Limpiar_Ctlr_Busqueda" runat="server" OnClientClick="javascript:return Limpiar_Controles();"
                            ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de Busqueda" Style="float:right"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%" colspan="2" align="left">
                            <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" Height="24px" 
                                ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Width="24px" />
                            <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" 
                                CssClass="estilo_fuente_mensaje_error" Text="" />                        
                        </td>
                        <td style="width:50%" colspan="2" align="right">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width:10%;">              
                        </td>          
                        <td style="width:90%;text-align:left;" valign="top" colspan="3">
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                        </td>
                    </tr>
                </table>
            &nbsp;
            <br />
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td style="width:15%;">
                            <asp:Label ID="Lbl_No_Inventario" runat="server" Text="No. Inventario"></asp:Label>
                        </td>
                        <td style="width:35%;">
                            <asp:TextBox ID="Txt_No_Inventario" runat="server" Width="98%" MaxLength="7"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Inventario" runat="server" TargetControlID="Txt_No_Inventario" FilterType="Numbers">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                        <td style="width:15%;">
                            &nbsp;
                            <asp:Label ID="Lbl_Folio_Solicitud" runat="server" Text="Folio Solicitud"></asp:Label>
                        </td>
                        <td style="width:35%;">
                            <asp:TextBox ID="Txt_Folio_Solicitud" runat="server" Width="98%" MaxLength="7"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Folio_Solicitud" runat="server" TargetControlID="Txt_Folio_Solicitud" FilterType="Numbers">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:18%">
                            <asp:Label ID="Label1" runat="server" Text="No. Economico"></asp:Label></td>
                        <td style="width:32%">
                            <asp:TextBox ID="Txt_No_Economico" runat="server" Width="98%" Enabled="true"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="Tbw_Economico" runat="server" 
                                TargetControlID="Txt_No_Economico" WatermarkCssClass="watermarked" 
                                WatermarkText="&lt;U00000&gt;" />
                        </td>
                        <td style="width:18%">
                            &nbsp;</td>
                        <td style="width:32%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width:100%;text-align:left;" colspan="4">
                            <center>
                                <asp:Button ID="Btn_Busqueda" runat="server"  Text="Buscar" CssClass="button"  
                                CausesValidation="false" Width="200px" onclick="Btn_Busqueda_Click"/> 
                            </center>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:18%">
                            &nbsp;</td>
                        <td style="width:32%">
                            &nbsp;</td>
                        <td style="width:18%">
                            &nbsp;</td>
                        <td style="width:32%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:GridView ID="Grid_Listado_Solicitudes" runat="server" AllowPaging="true" 
                                AutoGenerateColumns="False" CssClass="GridView_1" 
                                GridLines="none" DataKeyNames="NO_SOLICITUD"
                                PageSize="5" Style="white-space:normal" Width="96%" 
                                EmptyDataText="No se Encontrarón Solicitudes."
                                onselectedindexchanged="Grid_Listado_Solicitudes_SelectedIndexChanged" 
                                onpageindexchanging="Grid_Listado_Solicitudes_PageIndexChanging"
                                onrowcreated="Grid_Listado_Servicios_RowCreated">
                                <RowStyle CssClass="GridItem" />
                                <Columns>
                                    <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png" >
                                        <ItemStyle Width="10px" />
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="NO_SOLICITUD" HeaderText="NO_SOLICITUD" SortExpression="NO_SOLICITUD">
                                        <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FOLIO" HeaderText="Folio" SortExpression="FOLIO">
                                        <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NO_INVENTARIO" HeaderText="No. Inventario" SortExpression="NO_INVENTARIO">
                                        <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small"/>
                                    </asp:BoundField>
                                     <asp:BoundField DataField="NO_ECONOMICO" HeaderText="No. Economico" SortExpression="NO_ECONOMICO">
                                        <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small"/>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DESCRIPCION_SERVICIO" HeaderText="Descripción del Servicio" SortExpression="DESCRIPCION_SERVICIO">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="TIPO_BIEN" HeaderText="Tipo Bien" SortExpression="TIPO_BIEN">
                                        <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small"/>
                                    </asp:BoundField>
                                </Columns>
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:18%">&nbsp;</td>
                        <td style="width:32%">&nbsp;</td>
                        <td style="width:18%">&nbsp;</td>
                        <td style="width:32%">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width:18%">&nbsp;</td>
                        <td style="width:32%">&nbsp;</td>
                        <td style="width:18%">&nbsp;</td>
                        <td style="width:32%">&nbsp;</td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>    
    </form>
</body>
</html>
