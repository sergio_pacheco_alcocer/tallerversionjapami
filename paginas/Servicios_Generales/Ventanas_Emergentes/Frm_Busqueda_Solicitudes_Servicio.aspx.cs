﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Catalogo_Taller_Refacciones.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Correctivos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Preventivos.Negocio;

public partial class paginas_Taller_Municipal_Ventanas_Emergentes_Frm_Busqueda_Refacciones_Stock : System.Web.UI.Page {

    #region Variables
        private const int Const_Estado_Inicial = 0;
        private const int Const_Estado_Nuevo = 1;
        private const int Const_Estado_Modificar = 2;
    #endregion

    #region Page Load / Init
        protected void Page_Load(object sender, EventArgs e) {
            IBtn_Imagen_Error.Visible = false;  
            try {            
                if (!Page.IsPostBack) {
                    Limpiar_Formulario();
                    Llenar_Listado_Solicitudes();
                }
            } catch (Exception Ex) {
            }
        }
    #endregion

    #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
        ///DESCRIPCIÓN: Se Limpian los filtros de Busqueda.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Limpiar_Formulario() {
            Txt_Folio_Solicitud.Text = "";
            Txt_No_Inventario.Text = "";
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Solicitudes
        ///DESCRIPCIÓN: Se llena el Listado de las Solicitudes.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Llenar_Listado_Solicitudes() {
            Cls_Ope_Tal_Servicios_Preventivos_Negocio Ser_Prev_Negocio = new Cls_Ope_Tal_Servicios_Preventivos_Negocio();
            if (Txt_No_Inventario.Text.Trim().Length > 0) { Ser_Prev_Negocio.P_No_Inventario = Convert.ToInt32(Txt_No_Inventario.Text.Trim()); }
            if (Txt_No_Economico.Text.Trim().Length > 0) { Ser_Prev_Negocio.P_No_Economico = Txt_No_Economico.Text.Trim(); }
            if (Txt_Folio_Solicitud.Text.Trim().Length > 0) { Ser_Prev_Negocio.P_Folio_Solicitud = Convert.ToInt32(Txt_Folio_Solicitud.Text.Trim()); }
            Ser_Prev_Negocio.P_Reparacion = "INTERNA";
            DataTable Dt_Resultados_Preventivos = Ser_Prev_Negocio.Consultar_Servicios_Preventivos();
            Cls_Ope_Tal_Servicios_Correctivos_Negocio Ser_Corr_Negocio = new Cls_Ope_Tal_Servicios_Correctivos_Negocio();
            if (Txt_No_Inventario.Text.Trim().Length > 0) { Ser_Corr_Negocio.P_No_Inventario = Convert.ToInt32(Txt_No_Inventario.Text.Trim()); }
            if (Txt_No_Economico.Text.Trim().Length > 0) { Ser_Corr_Negocio.P_No_Economico = Txt_No_Economico.Text.Trim(); }
            if (Txt_Folio_Solicitud.Text.Trim().Length > 0) { Ser_Corr_Negocio.P_Folio_Solicitud = Convert.ToInt32(Txt_Folio_Solicitud.Text.Trim()); }
            Ser_Corr_Negocio.P_Reparacion = "INTERNA";
            Dt_Resultados_Preventivos.Merge(Ser_Corr_Negocio.Consultar_Servicios_Correctivos());
            Dt_Resultados_Preventivos.DefaultView.Sort = "FOLIO DESC";
            Grid_Listado_Solicitudes.Columns[1].Visible = true;
            Grid_Listado_Solicitudes.DataSource = Dt_Resultados_Preventivos;
            Grid_Listado_Solicitudes.DataBind();
            Grid_Listado_Solicitudes.Columns[1].Visible = false;
            
        }

    #endregion

    #region Grids


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Solicitudes_PageIndexChanging
        ///DESCRIPCIÓN: Maneja la paginación del Listado
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Solicitudes_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            try {
                Grid_Listado_Solicitudes.SelectedIndex = (-1);
                Grid_Listado_Solicitudes.PageIndex = e.NewPageIndex;
                Llenar_Listado_Solicitudes();
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Solicitudes_SelectedIndexChanged
        ///DESCRIPCIÓN: Obtiene los datos de una Solicitud 
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Solicitudes_SelectedIndexChanged(object sender, EventArgs e) {
            try {
                if (Grid_Listado_Solicitudes.SelectedIndex > (-1)) {
                    Limpiar_Formulario();
                    Session["SERVICIO_SELECCIONADO"] = Grid_Listado_Solicitudes.SelectedRow;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Cerrar", "window.close();", true);
                }
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Servicios_RowCreated
        ///DESCRIPCIÓN: Prueba de metodo para establecer seleccion en todo el renglon del grid
        ///PROPIEDADES:     
        ///CREO: Jesus Toledo Rodriguez
        ///FECHA_CREO: 09/Enero/2013
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Servicios_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onmouseover"] = "this.style.cursor='pointer';this.style.textDecoration='none';";
                e.Row.Attributes["onmouseout"] = "this.style.textDecoration='none';";
                e.Row.ToolTip = "Click para seleccionar renglon";
                e.Row.Attributes["onclick"] = this.Page.ClientScript.GetPostBackClientHyperlink(this.Grid_Listado_Solicitudes, "Select$" + e.Row.RowIndex);
            }
        }

    #endregion

    #region Eventos/Botones

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Regresar_Click
        ///DESCRIPCIÓN          : Evento Click del control Button Regresar
        ///PARAMETROS           : sender y e
        ///CREO                 : Jesus Toledo Rdz
        ///FECHA_CREO           : 02/Diciembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************  
        protected void Btn_Regresar_Click(object sender, ImageClickEventArgs e) {
            Session.Remove("SERVICIO_SELECCIONADO");
            Grid_Listado_Solicitudes.DataSource = null;
            Grid_Listado_Solicitudes.DataBind();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Cerrar", "window.close();", true);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Busqueda_Click
        ///DESCRIPCIÓN          : Llena el Grid.
        ///PARAMETROS           : sender y e
        ///CREO                 : Jesus Toledo Rdz
        ///FECHA_CREO           : 02/Diciembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************  
        protected void Btn_Busqueda_Click(object sender, EventArgs e) {
            Llenar_Listado_Solicitudes();
        }

    #endregion

}