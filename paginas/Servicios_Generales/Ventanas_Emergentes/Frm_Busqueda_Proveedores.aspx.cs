﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Catalogo_Compras_Proveedores.Negocio;

public partial class paginas_Taller_Mecanico_Ventanas_Emergentes_Frm_Busqueda_Proveedores : System.Web.UI.Page
{
    #region Variables
    private const int Const_Estado_Inicial = 0;
    private const int Const_Estado_Nuevo = 1;
    private const int Const_Estado_Modificar = 2;
    #endregion

    #region Page Load / Init
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            //if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!Page.IsPostBack)
            {
                Limpiar_Controles_Busqueda_Avanzada();
                Estado_Botones(Const_Estado_Inicial);
                //Configuracion_Acceso("Frm_Cat_Tal_Refacciones.aspx");
                //Cargar_Refacciones(0);
                Cmb_Busqueda_Estatus.SelectedIndex = Cmb_Busqueda_Estatus.Items.IndexOf(Cmb_Busqueda_Estatus.Items.FindByValue("ACTIVO"));
            }
            Mensaje_Error();
        }
        catch (Exception Ex)
        {
            //Mensaje_Error(Txt_Pagos.Text.Trim() +" - "+ Ex.Message);
            Estado_Botones(Const_Estado_Inicial);
        }
    }
    #endregion

    #region Metodos Generales
    ///*******************************************************************************
    ///NOMBRE DE LA METODO: LLenar_Combo_Id
    ///        DESCRIPCIÓN: llena todos los combos
    ///         PARAMETROS: 1.- Obj_DropDownList: Combo a llenar
    ///                     2.- Dt_Temporal: DataTable genarada por una consulta a la base de datos
    ///                     3.- Texto: nombre de la columna del dataTable que mostrara el texto en el combo
    ///                     3.- Valor: nombre de la columna del dataTable que mostrara el valor en el combo
    ///                     3.- Seleccion: Id del combo el cual aparecera como seleccionado por default
    ///               CREO: Jesus S. Toledo Rdz.
    ///         FECHA_CREO: 06/9/2010
    ///           MODIFICO:
    ///     FECHA_MODIFICO:
    /// CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_Combo_ID(DropDownList Obj_DropDownList, DataTable Dt_Temporal, String _Texto, String _Valor, String Seleccion)
    {
        String Texto = "";
        try
        {
            Obj_DropDownList.Items.Clear();
            Obj_DropDownList.Items.Add(new ListItem("< SELECCIONAR >", "0"));
            foreach (DataRow row in Dt_Temporal.Rows)
            {
                if (_Texto.Contains("+"))
                {
                    String[] Array_Texto = _Texto.Split('+');

                    foreach (String Campo in Array_Texto)
                    {
                        Texto = Texto + row[Campo].ToString();
                        Texto = Texto + "  ";
                    }
                }
                else
                {
                    Texto = row[_Texto].ToString();
                }
                Obj_DropDownList.Items.Add(new ListItem(Texto, row[_Valor].ToString()));
                Texto = "";
            }
            Obj_DropDownList.SelectedValue = Seleccion;
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }

    }
    public void Llenar_Combo_ID(DropDownList Obj_DropDownList)
    {
        try
        {
            Obj_DropDownList.Items.Clear();
            Obj_DropDownList.Items.Add(new ListItem("< SELECCIONAR >", "0"));
            Obj_DropDownList.SelectedValue = "0";
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }

    }
    ///****************************************************************************************
    ///NOMBRE DE LA FUNCION:Mensaje_Error
    ///DESCRIPCION : Muestra el error
    ///PARAMETROS  : P_Texto: texto de un TextBox
    ///CREO        : Toledo Rodriguez Jesus S.
    ///FECHA_CREO  : 04-Septiembre-2010
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACION:
    ///****************************************************************************************
    private void Mensaje_Error(String P_Mensaje)
    {
        IBtn_Imagen_Error.Visible = true;
        Lbl_Mensaje_Error.Text += P_Mensaje + "</br>";
    }
    private void Mensaje_Error()
    {
        IBtn_Imagen_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Lbl_Ecabezado_Mensaje.Text = "";
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Estado_Botones
    ///DESCRIPCIÓN: Metodo para establecer el estado de los botones y componentes del formulario
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 02/02/2011 05:49:53 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Estado_Botones(int P_Estado)
    {
        Boolean Estado = false;
        switch (P_Estado)
        {
            case 0: //Estado inicial  
                Btn_Limpiar_Ctlr_Busqueda.AlternateText = "Limpiar";
                Btn_Regresar.AlternateText = "Regresar";

                Estado = true;
                //Configuracion_Acceso("Frm_Cat_Tal_Refacciones.aspx");
                break;

            case 1: //Nuevo  
                Estado = true;
                break;

            case 2: //Modificar                    
                Estado = true;
                break;
        }

        Txt_Busqueda_Padron_Proveedor.Enabled = Estado;
        Txt_Busqueda_Nombre_Comercial.Enabled = Estado;
        Txt_Busqueda_RFC.Enabled = Estado;
        Cmb_Busqueda_Estatus.Enabled = Estado;
        Txt_Busqueda_Razon_Social.Enabled = Estado;
    }   

    #endregion

#region Metodos [Validaciones/Limpiar]

    public void Limpiar_Controles_Busqueda_Avanzada()
    {
        //Limpiamos las Cajas y variable de session del Div de Busqueda avanzada
        Txt_Busqueda_Padron_Proveedor.Text = "";
        Txt_Busqueda_Nombre_Comercial.Text = "";
        Txt_Busqueda_RFC.Text = "";
        Txt_Busqueda_Razon_Social.Text = "";
        Cmb_Busqueda_Estatus.SelectedIndex = Cmb_Busqueda_Estatus.Items.IndexOf(Cmb_Busqueda_Estatus.Items.FindByValue("ACTIVO"));

    }
#endregion
    #region Eventos
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Txt_Busqueda_Padron_Proveedor_TextChanged
    ///DESCRIPCIÓN: Busqueda Principal del catalogo
    //////PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 03/Ago/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    protected void Txt_Busqueda_Padron_Proveedor_TextChanged(object sender, EventArgs e)
    {
        try
        {            
            Btn_Busqueda_Click(sender, e);
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Txt_Busqueda_Nombre_Comercial_TextChanged
    ///DESCRIPCIÓN: Busqueda Principal del catalogo
    //////PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 03/Ago/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    protected void Txt_Busqueda_Nombre_Comercial_TextChanged(object sender, EventArgs e)
    {
        try
        {            
            Btn_Busqueda_Click(sender, e);
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Txt_Busqueda_Razon_Social_TextChanged
    ///DESCRIPCIÓN: Busqueda Principal del catalogo
    //////PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 03/Ago/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    protected void Txt_Busqueda_Razon_Social_TextChanged(object sender, EventArgs e)
    {
        try
        {
            Btn_Busqueda_Click(sender, e);            
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Txt_Busqueda_RFC_TextChanged
    ///DESCRIPCIÓN: Busqueda Principal del catalogo
    //////PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 03/Ago/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************   
    protected void Txt_Busqueda_RFC_TextChanged(object sender, EventArgs e)
    {
        try
        {            
            Btn_Busqueda_Click(sender, e);
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Click
    ///DESCRIPCIÓN: Busqueda Principal del catalogo
    //////PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 03/Ago/2011
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************  
    protected void Btn_Busqueda_Click(object sender, EventArgs e)
    {
        try
        {
            // Consulta_Proveedores(); //Consultar los proveedores que coincidan con el nombre porporcionado por el usuario
            Cls_Cat_Com_Proveedores_Negocio RS_Consulta_Cat_Com_Proveedores = new Cls_Cat_Com_Proveedores_Negocio(); //Variable de conexión hacia la capa de Negocios
            DataTable Dt_Proveedores; //Variable que obtendrá los datos de la consulta 
            if (Txt_Busqueda_Nombre_Comercial.Text != "")
            {
                RS_Consulta_Cat_Com_Proveedores.P_Nombre_Comercial = Txt_Busqueda_Nombre_Comercial.Text;
            }
            if (Txt_Busqueda_Padron_Proveedor.Text.Trim() != String.Empty)
            {
                RS_Consulta_Cat_Com_Proveedores.P_Proveedor_ID = String.Format("{0:0000000000}", Convert.ToInt32(Txt_Busqueda_Padron_Proveedor.Text.Trim()));
            }
            if (Txt_Busqueda_Razon_Social.Text.Trim() != String.Empty)
            {
                RS_Consulta_Cat_Com_Proveedores.P_Razon_Social = Txt_Busqueda_Razon_Social.Text.Trim();
            }
            if (Txt_Busqueda_RFC.Text.Trim() != String.Empty)
            {
                RS_Consulta_Cat_Com_Proveedores.P_RFC = Txt_Busqueda_RFC.Text.Trim();
            }
            if (Cmb_Busqueda_Estatus.SelectedIndex != 0)
            {
                RS_Consulta_Cat_Com_Proveedores.P_Estatus = Cmb_Busqueda_Estatus.SelectedItem.Text.Trim();
            }
            RS_Consulta_Cat_Com_Proveedores.P_tipo = "TALLER";
            //Consulta los Proveedores con sus datos generales
            Dt_Proveedores = RS_Consulta_Cat_Com_Proveedores.Consulta_Avanzada_Proveedor();
            if (Dt_Proveedores.Rows.Count != 0)
            {
                Grid_Proveedores.DataSource = Dt_Proveedores;
                Grid_Proveedores.DataBind();
                Session["Dt_Proveedores"] = Dt_Proveedores;
            }
            else
            {
                Grid_Proveedores.EmptyDataText = "No se han encontrado registros.";
                //Lbl_Mensaje_Error.Text = "+ No se encontraron datos <br />";
                Grid_Proveedores.DataSource = new DataTable();
                Grid_Proveedores.DataBind();
            }


            Limpiar_Controles_Busqueda_Avanzada(); //Limpia los controles de la forma
            //Si no se encontraron Proveedores con un nombre similar al proporcionado por el usuario entonces manda un mensaje al usuario
            if (Grid_Proveedores.Rows.Count <= 0)
            {
                Mensaje_Error("No se encontraron proveedores con el nombre proporcionado <br />");
            }            
        }
        catch (Exception ex)
        {
            Mensaje_Error(ex.Message.ToString());
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Regresar_Click
    ///DESCRIPCIÓN          : Evento Click del control Button Regresar
    ///PARAMETROS           : sender y e
    ///CREO                 : Jesus Toledo Rdz
    ///FECHA_CREO           : 02/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************  
    protected void Btn_Regresar_Click(object sender, ImageClickEventArgs e)
    {
        Session["Proveedor_ID"] = null;
        Session["Nombre_Proveedor"] = null;
        Grid_Proveedores.DataSource = null;
        Grid_Proveedores.DataBind();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Cerrar", "window.close();", true);
        //ClientScript.RegisterStartupScript(this.GetType(), "Cerrar_Script", Pagina);
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Habilitar_Componentes
    ///DESCRIPCIÓN: Metodo que  Habilita o Deshabilita los componentes
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 4/NOV/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Proveedores_SelectedIndexChanged(object sender, EventArgs e)
    {        
        //Consultamos los datos dep Proveedor seleccionado 

        if (Grid_Proveedores.SelectedDataKey["Proveedor_ID"] != null && Grid_Proveedores.SelectedDataKey["Nombre"] != null)
        {
            Session["Proveedor_ID"] = Grid_Proveedores.SelectedDataKey["Proveedor_ID"].ToString().Trim();
            if(!String.IsNullOrEmpty(Grid_Proveedores.SelectedDataKey["Nombre"].ToString().Trim()))
                Session["Nombre_Proveedor"] = Grid_Proveedores.SelectedDataKey["Nombre"].ToString().Trim();
            else
                Session["Nombre_Proveedor"] = Grid_Proveedores.SelectedDataKey["Compania"].ToString().Trim();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Cerrar", "window.close();", true);
        }
    }
    #endregion

}