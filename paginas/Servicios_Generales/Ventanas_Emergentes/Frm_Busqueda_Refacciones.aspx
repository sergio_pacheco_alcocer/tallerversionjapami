﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Frm_Busqueda_Refacciones.aspx.cs" Inherits="paginas_Taller_Municipal_Ventanas_Emergentes_Frm_Busqueda_Refacciones" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Buscador Refacciones</title>
    <link href="../../estilos/estilo_paginas.css" rel="stylesheet" type="text/css" />
    <link href="../../estilos/estilo_masterpage.css" rel="stylesheet" type="text/css" />
    <link href="../../estilos/estilo_paginas.css" rel="stylesheet" type="text/css" />
    <link href="../../estilos/estilo_ajax.css" rel="stylesheet" type="text/css" />

    <base target="_self" />
    <script type="text/javascript">
        window.onerror = new Function("return true");
        function Limpiar_Controles() {
            if (document.getElementById("<%=IBtn_Imagen_Error.ClientID%>") != null) {
                document.getElementById("<%=IBtn_Imagen_Error.ClientID%>").style.visibility = "hidden";
            }
            document.getElementById("<%=Lbl_Ecabezado_Mensaje.ClientID%>").innerHTML = "";
            document.getElementById("<%=Lbl_Mensaje_Error.ClientID%>").innerHTML = "";

            document.getElementById("<%=Txt_Clave.ClientID%>").value = "";
            document.getElementById("<%=Txt_Nombre.ClientID%>").value = "";
            document.getElementById('<%= Cmb_Estatus.ClientID %>').value = 'SELECCIONE';
            document.getElementById('<%= Cmb_Tipo.ClientID %>').value = 'SELECCIONE';
            document.getElementById("<%=Txt_Descripcion.ClientID%>").value = "";
            return false;
        }
        //Abrir una ventana modal
        function Abrir_Ventana_Modal(Url, Propiedades) {
            window.showModalDialog(Url, null, Propiedades);
        }
        function Validar_Longitud_Texto(Text_Box, Max_Longitud) {
            if (Text_Box.value.length > Max_Longitud) {
                Text_Box.value = Text_Box.value.substring(0, Max_Longitud);
            }
        }
        function formatCurrency(num) {
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
                    num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + num + '.' + cents);
        }
    </script>

    <script type="text/javascript" language="javascript">
    //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_sesiones.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion()
        {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }
        //Temporizador para matener la sesión activa
        setInterval('MantenSesion()', <%=(int)(0.9*(Session.Timeout * 60000))%>);        
    </script>
</head>

<body bgcolor="White">
    <form id="form1" runat="server" style="background-color:White">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" 
    EnableScriptGlobalization="true"
        EnableScriptLocalization="true"
        EnablePartialRendering="true" 
        AsyncPostBackTimeout="9000">
</cc1:ToolkitScriptManager>
    <%--<asp:ScriptManager ID="ScriptManager1"  runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" EnablePartialRendering="true" AsyncPostBackTimeout="9000">
    </asp:ScriptManager>--%>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../../imagenes/paginas/Sias_Roler.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div id="Div_Contenido" style="background-color:#ffffff; width:100%; height:100%;">
        <table width="99%" cellpadding="2" cellspacing="0">
                    <tr>
                        <td style="color:Black; text-align:center; font-size:12; font-weight:bold;" colspan='4'>                            
                            B&uacute;squeda: de Refacciones
                        </td>                        
                    </tr>
                    <tr class="barra_busqueda">
                        <td align="left" colspan="3" style="width:90%";"> 
                            <asp:ImageButton ID="Btn_Regresar" runat="server" 
                                AlternateText="Regresar" CssClass="Img_Button" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                onclick="Btn_Regresar_Click" Width="24px" />                            
                        </td>
                        <td colspan="1" style="text-align:left; width:10%"; text-align:right;">
                        <asp:ImageButton ID="Btn_Seleccionar" runat="server" Style="float:left" onclick="Btn_Seleccionar_Click"
                            ImageUrl="~/paginas/imagenes/paginas/accept.png" ToolTip="Seleccionar"/>
                            <asp:ImageButton ID="Btn_Limpiar_Ctlr_Busqueda" runat="server" OnClientClick="javascript:return Limpiar_Controles();"
                            ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de Busqueda" Style="float:right"/>
                        </td>
            </tr>
                    <tr>
                        <td style="width:50%" colspan="2" align="left">
                            <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" Height="24px" 
                                ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Width="24px" />
                            <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" 
                                CssClass="estilo_fuente_mensaje_error" Text="" />                        
                        </td>
                        <td style="width:50%" colspan="2" align="right">
                            &nbsp;</td>
                    </tr>
                    
                    <tr>
                        <td style="width:10%;">              
                        </td>          
                        <td style="width:90%;text-align:left;" valign="top" colspan="3">
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                        </td>
                    </tr>
                    </table>
                    &nbsp;
                    <br />
                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                        
                        </tr>
                    <tr>
                    <td style="width:18%">
                                Clave</td>
                            <td style="width:32%">
                                <asp:TextBox ID="Txt_Clave" runat="server" Width="92%" Text="" MaxLength="20" Style="text-transform: uppercase" AutoPostBack="true" OnTextChanged="Txt_Clave_TextChanged"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="Txt_Clave_FilteredTextBoxExtender" 
                                    runat="server" Enabled="True" TargetControlID="Txt_Clave" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="width:18%">
                                Estatus</td>
                            <td style="width:32%">
                                <asp:DropDownList ID="Cmb_Estatus" Width="94%" runat="server" Enabled="false">
                                    <asp:ListItem Text="<SELECCIONE>" Value="SELECCIONE" />
                                    <asp:ListItem Text="VIGENTE" Value="VIGENTE" />
                                    <asp:ListItem Text="BAJA" Value="BAJA" />
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:18%">
                                Nombre</td>
                            <td style="width:32%">
                                <asp:TextBox ID="Txt_Nombre" runat="server" Width="92%" Text="" MaxLength="50" Style="text-transform: uppercase" AutoPostBack="true" OnTextChanged="Txt_Nombre_TextChanged"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="Txt_Nombre_FilteredTextBoxExtender" 
                                    runat="server" Enabled="True" TargetControlID="Txt_Nombre" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="width:18%">Tipo</td>
                            <td style="width:32%">
                                <asp:DropDownList ID="Cmb_Tipo" Width="94%" runat="server">
                                    <asp:ListItem Text="<SELECCIONE>" Value="SELECCIONE" />
                                    <asp:ListItem Text="STOCK" Value="STOCK" />
                                    <asp:ListItem Text="TRANSITORIO" Value="TRANSITORIO" />
                                </asp:DropDownList>
                            </td>
                        </tr>
                        
                        <tr>
                            <td style="width:18%">
                                Descripcion</td>
                            <td colspan="3">
                                <asp:TextBox ID="Txt_Descripcion" runat="server" AutoPostBack="true" 
                                    Height="60px" OnTextChanged="Txt_Descripcion_TextChanged" 
                                    Style="text-transform: uppercase" TextMode="MultiLine" Width="97%"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="Txt_Comentarios_FilteredTextBoxExtender" 
                                    runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                    TargetControlID="Txt_Descripcion" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <tr>
                                <td style="width:18%">
                                    &nbsp;</td>
                                <td style="width:32%">
                                    &nbsp;</td>
                                <td style="width:18%">
                                    &nbsp;</td>
                                <td style="width:32%">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="4" style="width:100%;text-align:left;">
                                    <center>
                                        <asp:Button ID="Btn_Busqueda" runat="server" CausesValidation="false" 
                                            CssClass="button" onclick="Btn_Busqueda_Click" Text="Buscar" Width="200px" />
                                    </center>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:18%">
                                    &nbsp;</td>
                                <td style="width:32%">
                                    &nbsp;</td>
                                <td style="width:18%">
                                    &nbsp;</td>
                                <td style="width:32%">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4">
                                    <asp:GridView ID="Grid_Refacciones" runat="server" AllowPaging="false" 
                                        AutoGenerateColumns="False" CssClass="GridView_1" 
                                        DataKeyNames="REFACCION_ID,DESCRIPCION" GridLines="none" 
                                        OnDataBound="Grid_Refacciones_DataBound" 
                                        onpageindexchanging="Grid_Refacciones_PageIndexChanging" PageSize="100" 
                                        Style="white-space:normal" Width="96%">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Seleccionar">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="Chk_Seleccionar" runat="server" AutoPostBack="false" 
                                                        OnCheckedChanged="Chk_Seleccionar_CheckedChanged" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Width="17%" />
                                                <ItemStyle HorizontalAlign="Center" Width="17%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Width="12%" HeaderText="Cantidad" 
                                                ItemStyle-HorizontalAlign="Center">
                                                <itemtemplate>
                                                    <asp:TextBox ID="Txt_Grid_Cantidad" runat="server" MaxLength="7" 
                                                        OnTextChanged="Txt_Grid_Cantidad_TextChanged" 
                                                        style="width:30%; text-align:left;" Text='<%# Bind("CANTIDAD", "{0:0.##}") %>'>
                                                </asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="Fte_Txt_Grid_Cantidad" runat="server" 
                                                        FilterType="Numbers,Custom" TargetControlID="Txt_Grid_Cantidad" 
                                                        ValidChars="," />
                                                </itemtemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="REFACCION_ID" HeaderText="Id" Visible="false">
                                                <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="5%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CLAVE" HeaderText="Clave">
                                                <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NOMBRE" HeaderText="Nombre">
                                                <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ESTATUS" HeaderText="Estatus">
                                                <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripcion" 
                                                Visible="false" />
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:18%">
                                    &nbsp;</td>
                                <td style="width:32%">
                                    &nbsp;</td>
                                <td style="width:18%">
                                    &nbsp;</td>
                                <td style="width:32%">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td style="width:18%">
                                    &nbsp;</td>
                                <td style="width:32%">
                                    &nbsp;</td>
                                <td style="width:18%">
                                    &nbsp;</td>
                                <td style="width:32%">
                                    &nbsp;</td>
                            </tr>
                        </tr>
                        </table>
                        </div>
        </ContentTemplate>
    </asp:UpdatePanel>    
    </form>
</body>
</html>
