﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Frm_Busqueda_Proveedores.aspx.cs" Inherits="paginas_Taller_Mecanico_Ventanas_Emergentes_Frm_Busqueda_Proveedores" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Buscador Proveedores</title>
    <link href="../../estilos/estilo_paginas.css" rel="stylesheet" type="text/css" />
    <link href="../../estilos/estilo_masterpage.css" rel="stylesheet" type="text/css" />
    <link href="../../estilos/estilo_paginas.css" rel="stylesheet" type="text/css" />
    <link href="../../estilos/estilo_ajax.css" rel="stylesheet" type="text/css" />

    <base target="_self" />
    <script type="text/javascript">
        window.onerror = new Function("return true");
        function Limpiar_Controles() {
            if (document.getElementById("<%=IBtn_Imagen_Error.ClientID%>") != null) {
                document.getElementById("<%=IBtn_Imagen_Error.ClientID%>").style.visibility = "hidden";
            }
            document.getElementById("<%=Lbl_Ecabezado_Mensaje.ClientID%>").innerHTML = "";
            document.getElementById("<%=Lbl_Mensaje_Error.ClientID%>").innerHTML = "";

            document.getElementById("<%=Txt_Busqueda_Padron_Proveedor.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_Nombre_Comercial.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_RFC.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_Razon_Social.ClientID%>").value = "";
            document.getElementById('<%= Cmb_Busqueda_Estatus.ClientID %>').value = 'SELECCIONE';

            return false;
        }
        //Abrir una ventana modal
        function Abrir_Ventana_Modal(Url, Propiedades) {
            window.showModalDialog(Url, null, Propiedades);
        }
        function Validar_Longitud_Texto(Text_Box, Max_Longitud) {
            if (Text_Box.value.length > Max_Longitud) {
                Text_Box.value = Text_Box.value.substring(0, Max_Longitud);
            }
        }
        function formatCurrency(num) {
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
                    num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + num + '.' + cents);
        }
    </script>

    <script type="text/javascript" language="javascript">
    //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_sesiones.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion()
        {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }
        //Temporizador para matener la sesión activa
        setInterval('MantenSesion()', <%=(int)(0.9*(Session.Timeout * 60000))%>);        
    </script>
</head>
<body bgcolor="White">
    <form id="form1" runat="server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" EnablePartialRendering="true" AsyncPostBackTimeout="9000" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../../imagenes/paginas/Sias_Roler.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div id="Div_Contenido" style="background-color:#ffffff; width:100%; height:100%;">
        <table width="99%" cellpadding="2" cellspacing="0">
                    <tr>
                        <td style="color:Black; text-align:center; font-size:12; font-weight:bold;" colspan='4'>                            
                            B&uacute;squeda: de Proveedores
                        </td>                        
                    </tr>
                    <tr class="barra_busqueda">
                        <td align="left" colspan="3" style="width:90%";"> 
                            <asp:ImageButton ID="Btn_Regresar" runat="server" 
                                AlternateText="Regresar" CssClass="Img_Button" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                onclick="Btn_Regresar_Click" Width="24px" />                            
                        </td>
                        <td colspan="1" style="text-align:right;width:10%";">                        
                            <asp:ImageButton ID="Btn_Limpiar_Ctlr_Busqueda" runat="server" OnClientClick="javascript:return Limpiar_Controles();"
                            ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de Busqueda" Style="float:right"/>
                        </td>
            </tr>
                    <tr>
                        <td style="width:50%" colspan="2" align="left">
                            <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" Height="24px" 
                                ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Width="24px" />
                            <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" 
                                CssClass="estilo_fuente_mensaje_error" Text="" />                        
                        </td>
                        <td style="width:50%" colspan="2" align="right">
                            &nbsp;</td>
                    </tr>
                    
                    <tr>
                        <td style="width:10%;">              
                        </td>          
                        <td style="width:90%;text-align:left;" valign="top" colspan="3">
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                        </td>
                    </tr>
                    </table>
                    &nbsp;
                    <br />
                    <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                                        <td style="text-align:left;width:20%;">Padrón Proveedor</td>
                                        <td style="text-align:left;width:30%;">
                                            <asp:TextBox ID="Txt_Busqueda_Padron_Proveedor" runat="server" Width="100%" TabIndex="3" MaxLength="10" AutoPostBack="true" OnTextChanged="Txt_Busqueda_Padron_Proveedor_TextChanged"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" WatermarkCssClass="watermarked"
                                                WatermarkText="<Ingrese el Padron de Proveedor>" TargetControlID="Txt_Busqueda_Padron_Proveedor" />
                                           <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" 
                                            TargetControlID="Txt_Busqueda_Padron_Proveedor"  
                                            FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,."
                                            Enabled="True" InvalidChars="<,>,&,',!,">   
                                            </cc1:FilteredTextBoxExtender>
                                            </td>
                                        <td style="text-align:right;width:20%;">
                                                Nombre Comercial</td>
                                        <td style="text-align:left;width:30%;">
                                            <asp:TextBox ID="Txt_Busqueda_Nombre_Comercial" runat="server" TabIndex="4" Width="100%" AutoPostBack="true" OnTextChanged="Txt_Busqueda_Nombre_Comercial_TextChanged"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Rol_ID" runat="server" WatermarkCssClass="watermarked"
                                                WatermarkText="<Ingrese nombre>" TargetControlID="Txt_Busqueda_Nombre_Comercial" /></td>
                                        
                                        
                                    </tr>
                                        
                                    <tr>
                                        <td>RFC</td>
                                        <td>
                                            <asp:TextBox ID="Txt_Busqueda_RFC" runat="server" Width="100%" TabIndex="5"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" WatermarkCssClass="watermarked"
                                                WatermarkText="<Ingrese el RFC>" TargetControlID="Txt_Busqueda_RFC" />
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" 
                                                TargetControlID="Txt_Busqueda_RFC"  
                                                FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                                ValidChars="ÑñáéíóúÁÉÍÓÚ"
                                                Enabled="True" InvalidChars="'">   
                                                </cc1:FilteredTextBoxExtender>
                                                </td>
                                        <td align="right"> Razón Social
                                        </td>
                                        <td>
                                            <asp:TextBox ID="Txt_Busqueda_Razon_Social" runat="server" Width="100%" TabIndex="6" AutoPostBack="true" OnTextChanged="Txt_Busqueda_Razon_Social_TextChanged"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" WatermarkCssClass="watermarked"
                                                WatermarkText="<Ingrese Razon Social>" TargetControlID="Txt_Busqueda_Razon_Social" />
                                                 <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" WatermarkCssClass="watermarked"
                                                WatermarkText="<Ingrese el RFC>" TargetControlID="Txt_Busqueda_RFC" />
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td>
                                            
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="Cmb_Busqueda_Estatus" runat="server" Width="98%" style="display:none">
                                                <asp:ListItem Text="<SELECCIONE>" Value="SELECCIONE" />
                                                <asp:ListItem Text="ACTIVO" Value="ACTIVO" />
                                                <asp:ListItem Text="INACTIVO" Value="INACTIVO" />
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            
                                        </td>
                                        <td>
                                            
                                        </td>
                                    </tr>
                        <tr>
                            <td style="width:18%">
                                &nbsp;</td>
                            <td style="width:32%">
                                &nbsp;</td>
                            <td style="width:18%">
                                &nbsp;</td>
                            <td style="width:32%">
                                &nbsp;</td>
                        </tr>
                        <tr>
                                        <td style="width:100%;text-align:left;" colspan="4">
                                            <center>
                                                <asp:Button ID="Btn_Busqueda" runat="server"  Text="Buscar" CssClass="button"  
                                                CausesValidation="false" Width="200px" onclick="Btn_Busqueda_Click"/> 
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                            <td style="width:18%">
                                &nbsp;</td>
                            <td style="width:32%">
                                &nbsp;</td>
                            <td style="width:18%">
                                &nbsp;</td>
                            <td style="width:32%">
                                &nbsp;</td>
                        </tr>
                            <tr>
                            <td align="center" colspan="4">
                                <div ID="Div_Proveedores" runat="server" style="overflow:auto;height:200px;width:99%;vertical-align:top;border-style:outset;border-color:Silver;">
                            <asp:GridView ID="Grid_Proveedores" runat="server" AutoGenerateColumns="False" 
                                CssClass="GridView_1" GridLines="None" AllowSorting="True"
                                Width="100%" DataKeyNames="Proveedor_ID,Nombre,Compania"
                                onselectedindexchanged="Grid_Proveedores_SelectedIndexChanged" >
                                <RowStyle CssClass="GridItem" />
                                <Columns>
                                    <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                        ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                        <ItemStyle Width="5%" />
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="Proveedor_ID" HeaderText="Padron Proveedor" 
                                        Visible="True">
                                        <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                        <ItemStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small"/>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Nombre" HeaderText="Razón Social" Visible="True">
                                        <HeaderStyle HorizontalAlign="Left" Width="25%" />
                                        <ItemStyle HorizontalAlign="Left" Width="25%" Font-Size="X-Small"/>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Compania" HeaderText="Nombre Comercial" Visible="True">
                                        <HeaderStyle HorizontalAlign="Left" Width="40%" />
                                        <ItemStyle HorizontalAlign="Left" Width="40%" Font-Size="X-Small"/>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus" Visible="True">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                        <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small"/>
                                    </asp:BoundField>
                                </Columns>
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                                <HeaderStyle CssClass="GridHeader" />                                
                                <AlternatingRowStyle CssClass="GridAltItem" />                                
                            </asp:GridView>
                            </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:18%">&nbsp;</td>
                            <td style="width:32%">&nbsp;</td>
                            <td style="width:18%">&nbsp;</td>
                            <td style="width:32%">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:18%">&nbsp;</td>
                            <td style="width:32%">&nbsp;</td>
                            <td style="width:18%">&nbsp;</td>
                            <td style="width:32%">&nbsp;</td>
                        </tr>
                        </table>
                        </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
