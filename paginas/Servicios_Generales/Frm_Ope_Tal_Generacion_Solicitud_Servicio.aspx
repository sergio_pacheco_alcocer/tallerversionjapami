﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Ope_Tal_Generacion_Solicitud_Servicio.aspx.cs"
    Inherits="paginas_Taller_Mecanico_Frm_Ope_Tal_Generacion_Solicitud_Servicio"
    Title="Generación de Solicitud de Servicio" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">

    <script type="text/javascript">
        window.onerror = new Function("return true");
        function Limpiar_Filtros() {
            if (document.getElementById("<%=Cmb_Entrada_Gpo_UR.ClientID%>").disabled == false) {
                document.getElementById("<%=Cmb_Entrada_Gpo_UR.ClientID%>").selectedIndex = 0;
                Inicializar_Combo(document.getElementById("<%=Cmb_Entrada_UR.ClientID%>"), "TODAS");
            }
            document.getElementById("<%=Cmb_Filtrado_Estatus.ClientID%>").selectedIndex = 0;
            document.getElementById("<%=Cmb_Entrada_Tipo_Servicio.ClientID%>").selectedIndex = 0;
            return false;
        }
        function Cambio(miControl) {
            alert(miControl.Value);
        }
        function Inicializar_Combo(Combo, Texto) {
            var No_items = Combo.options.length;
            for (i = No_items - 1; i >= 0; i--) {
                Combo.options.remove(i);
            }
            var opt = document.createElement("option");
            Combo.options.add(opt);
            opt.text = "< - - " + Texto + " - ->";
            opt.value = "";
            return false;
        }
        function Tipo_Bien() {
            var Combo = "";
            Combo = document.getElementById('<%= Cmb_Tipo_Bien.ClientID %>');
            Beneficio = Combo.options[Combo.selectedIndex].text.toUpperCase();
            if (Beneficio.indexOf("VEHICULO") != -1) {
                document.getElementById('<%= Txt_No_Economico.ClientID %>').disabled = false;
                document.getElementById('<%= Txt_No_Inventario.ClientID %>').disabled = true;
                document.getElementById('<%= Txt_No_Inventario.ClientID %>').value = "";
                document.getElementById('<%= Txt_No_Economico.ClientID %>').style.display = "inline";
                document.getElementById('<%= Lbl_No_Economico.ClientID %>').style.display = "inline";
                document.getElementById('<%= Txt_No_Serie.ClientID %>').style.display = "none";
                document.getElementById('<%= Lbl_No_Serie.ClientID %>').style.display = "none";
            }
            else if (Beneficio.indexOf("MUEBLE") != -1) {
                document.getElementById('<%= Txt_No_Economico.ClientID %>').value = "";
                document.getElementById('<%= Txt_No_Economico.ClientID %>').disabled = true;
                document.getElementById('<%= Txt_No_Economico.ClientID %>').style.display = "none";
                document.getElementById('<%= Lbl_No_Economico.ClientID %>').style.display = "none";
                document.getElementById('<%= Txt_No_Serie.ClientID %>').style.display = "inline";
                document.getElementById('<%= Lbl_No_Serie.ClientID %>').style.display = "inline";
                document.getElementById('<%= Txt_No_Inventario.ClientID %>').disabled = false;
            }

        }
        function Formato_Unidad() {
            var Str_Economico = $("#<%=Txt_No_Economico.ClientID %>").val();
            var Longitud_Palabra = Str_Economico.length;
            if (!isNaN(Str_Economico[0]))
                Str_Economico = 'U' + Str_Economico;
            else
                Str_Economico = Str_Economico.toUpperCase();
            $("#<%=Txt_No_Economico.ClientID %>").val(Str_Economico);
        }

        function Formato_Ceros() {
            var Str_Economico = $("#<%=Txt_No_Economico.ClientID %>").val();
            var Longitud_Palabra = Str_Economico.length;
            if (Longitud_Palabra > 0) {
                var Str_Economico_Copia = new Array(6);
                Str_Economico_Copia[0] = Str_Economico[0];
                if (Longitud_Palabra == 6) {
                    Str_Economico_Copia[1] = Str_Economico[1];
                    Str_Economico_Copia[2] = Str_Economico[2];
                    Str_Economico_Copia[3] = Str_Economico[3];
                    Str_Economico_Copia[4] = Str_Economico[4];
                    Str_Economico_Copia[5] = Str_Economico[5];
                    Str_Economico_Copia[6] = Str_Economico[6];
                }
                else {
                    Str_Economico_Copia[0] = Str_Economico[0];
                    Str_Economico_Copia[1] = '0';
                    Str_Economico_Copia[2] = '0';
                    Str_Economico_Copia[3] = '0';
                    Str_Economico_Copia[4] = '0';
                    Str_Economico_Copia[5] = '0';
                    Str_Economico_Copia[6] = '0';
                }
                if (Longitud_Palabra < 6) {
                    for (var i = 1; i < Longitud_Palabra; i++) {
                        Str_Economico_Copia[6 - (Longitud_Palabra - i)] = Str_Economico[i];
                    }
                }
                $("#<%=Txt_No_Economico.ClientID %>").val(Str_Economico_Copia[0] + Str_Economico_Copia[1] + Str_Economico_Copia[2] + Str_Economico_Copia[3] + Str_Economico_Copia[4] + Str_Economico_Copia[5]);
            }
        }

        function get_KeyPress(textbox, evento) {
            if (evento.which == 13) {
                Formato_Unidad();
                Formato_Ceros();
                (document.getElementById('<%=Btn_Busqueda_Directa.ClientID %>')).click();
                return false;
            }
            return true;
        }
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScptM_Operacion" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Loading" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Area_Trabajo" style="background-color: #ffffff; width: 100%; height: 100%;">
                <center>
                    <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                        <tr align="center">
                            <td class="label_titulo" colspan="2">
                                Generación de Solicitud de Servicio
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div id="Div_Contenedor_Msj_Error" style="width: 98%;" runat="server" visible="false">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td colspan="2" align="left">
                                                <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                    Width="24px" Height="24px" />
                                                <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 10%;">
                                            </td>
                                            <td style="width: 90%; text-align: left;" valign="top">
                                                <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left" style="width: 50%;">
                                <asp:ImageButton ID="Btn_Nuevo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png"
                                    Width="24px" CssClass="Img_Button" AlternateText="Nuevo" OnClick="Btn_Nuevo_Click" />
                                <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                    Width="24px" CssClass="Img_Button" AlternateText="Modificar" OnClick="Btn_Modificar_Click" />
                                <asp:ImageButton ID="Btn_Imprimir_Solicitud_Servicio" runat="server" ImageUrl="~/paginas/imagenes/gridview/grid_print.png"
                                    Width="24px" CssClass="Img_Button" AlternateText="Imprimir" ToolTip="Imprimir Solicitud"
                                    OnClick="Btn_Imprimir_Solicitud_Servicio_Click" />
                                <asp:ImageButton ID="Btn_Cancelar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png"
                                    Width="24px" CssClass="Img_Button" AlternateText="Cancelar" ToolTip="Cancelar Solicitud"
                                    OnClick="Btn_Cancelar_Click" OnClientClick="return confirm('¿Esta seguro que desea CANCELAR la solicitud?');" />
                                <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                    Width="24px" CssClass="Img_Button" AlternateText="Salir" OnClick="Btn_Salir_Click" />
                            </td>
                            <td style="width: 50%;">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                    <br />
                    <div id="Div_Listado_Solicitudes" runat="server" style="width: 100%;">
                        <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Entrada_Gpo_UR" runat="server" Text="Gerencia"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="Cmb_Entrada_Gpo_UR" runat="server" Width="100%" OnSelectedIndexChanged="Cmb_Entrada_Gpo_UR_SelectedIndexChanged"
                                        AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Entrada_UR" runat="server" Text="Unidad Responsable"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="Cmb_Entrada_UR" runat="server" Width="100%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Entrada_Tipo_Servicio" runat="server" Text="Tipo Servicio"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="Cmb_Entrada_Tipo_Servicio" runat="server" Width="100%">
                                        <asp:ListItem Value="">&lt; - - TODOS - - &gt;</asp:ListItem>
                                        <asp:ListItem Value="REVISTA_MECANICA">REVISTA MECANICA</asp:ListItem>
                                        <asp:ListItem Value="SERVICIO_GENERAL">SERVICIO GENERAL</asp:ListItem>
                                        <asp:ListItem Value="VERIFICACION">VERIFICACIÓN</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Filtrado_Estatus" runat="server" Text="Filtrar [Estatus]"></asp:Label>
                                </td>
                                <td style="width: 35%;">
                                    <asp:DropDownList ID="Cmb_Filtrado_Estatus" runat="server" Width="100%">
                                        <asp:ListItem Value="">&lt; - - TODAS - - &gt;</asp:ListItem>
                                        <asp:ListItem Value="PENDIENTE','AUTORIZADA">PENDIENTE, AUTORIZADA</asp:ListItem>
                                        <asp:ListItem Value="PENDIENTE">PENDIENTE</asp:ListItem>
                                        <asp:ListItem Value="RECHAZADA">RECHAZADA</asp:ListItem>
                                        <asp:ListItem Value="CANCELADA">CANCELADA</asp:ListItem>
                                        <asp:ListItem Value="AUTORIZADA">AUTORIZADA</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 15%;">
                                    &nbsp;
                                    <asp:ImageButton ID="Btn_Actualizar_Listado" runat="server" ToolTip="Actualizar Listado"
                                        AlternateText="Actualizar Listado" OnClick="Btn_Actualizar_Listado_Click" ImageUrl="~/paginas/imagenes/paginas/actualizar_detalle.png"
                                        Width="16px" />
                                    <asp:ImageButton ID="Btn_Limpiar_Ctlr_Busqueda" runat="server" OnClientClick="javascript:return Limpiar_Filtros();"
                                        ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de Busqueda" />
                                </td>
                                <td style="width: 35%;">
                                    &nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>
                        </table>
                        <br />
                        <asp:GridView ID="Grid_Listado_Solicitudes" runat="server" CssClass="GridView_1"
                            AutoGenerateColumns="False" AllowPaging="True" PageSize="10" Width="99%" GridLines="None"
                            EmptyDataText="No se Encontrarón Solicitudes." OnSelectedIndexChanged="Grid_Listado_Solicitudes_SelectedIndexChanged"
                            OnPageIndexChanging="Grid_Listado_Solicitudes_PageIndexChanging" DataKeyNames="NO_SOLICITUD,TIPO_BIEN">
                            <RowStyle CssClass="GridItem" />
                            <Columns>
                                <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                    <ItemStyle Width="5%" />
                                </asp:ButtonField>
                                <asp:BoundField DataField="NO_SOLICITUD" HeaderText="NO_SOLICITUD" SortExpression="NO_SOLICITUD">
                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FECHA_ELABORACION" HeaderText="Fecha" SortExpression="FECHA_ELABORACION"
                                    DataFormatString="{0:dd/MMM/yyyy}">
                                    <ItemStyle Width="10%" HorizontalAlign="Center" Font-Size="X-Small" />
                                    <HeaderStyle HorizontalAlign="Center"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="TIPO_BIEN" HeaderText="Bien" SortExpression="TIPO_BIEN">
                                    <ItemStyle Width="10%" HorizontalAlign="Center" Font-Size="X-Small" />
                                    <HeaderStyle HorizontalAlign="Center"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="TIPO_SERVICIO" HeaderText="Tipo de Servicio" SortExpression="TIPO_SERVICIO">
                                    <ItemStyle Width="15%" HorizontalAlign="Center" Font-Size="X-Small" />
                                    <HeaderStyle HorizontalAlign="Center"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="NO_INVENTARIO" HeaderText="No. Inv." SortExpression="NO_INVENTARIO">
                                    <ItemStyle Width="10%" HorizontalAlign="Center" Font-Size="X-Small" />
                                    <HeaderStyle HorizontalAlign="Center"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="DESCRIPCION_SERVICIO" HeaderText="Descripción" SortExpression="DESCRIPCION_SERVICIO">
                                    <ItemStyle Font-Size="X-Small" />
                                    <HeaderStyle HorizontalAlign="Center"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="NO_ECONOMICO" HeaderText="Ecónomico" SortExpression="NO_ECONOMICO">
                                    <ItemStyle Width="10%" HorizontalAlign="Center" Font-Size="X-Small" />
                                    <HeaderStyle HorizontalAlign="Center"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS">
                                    <ItemStyle Width="10%" HorizontalAlign="Center" Font-Size="X-Small" />
                                    <HeaderStyle HorizontalAlign="Center"/>
                                </asp:BoundField>
                            </Columns>
                            <PagerStyle CssClass="GridHeader" />
                            <SelectedRowStyle CssClass="GridSelected" />
                            <HeaderStyle CssClass="GridHeader" />
                            <AlternatingRowStyle CssClass="GridAltItem" />
                        </asp:GridView>
                    </div>
                    <div id="Div_Campos" runat="server" style="width: 100%;">
                        <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                                <td colspan="4">
                                    <asp:HiddenField ID="Hdf_No_Solicitud" runat="server" />
                                    <asp:HiddenField ID="Hdf_Folio_Solicitud" runat="server" />
                                    <asp:HiddenField ID="Hdf_Tipo_Bien" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Fecha_Elaboracion" runat="server" Text="Fecha Elaboración"></asp:Label>
                                </td>
                                <td style="width: 35%;">
                                    <asp:TextBox ID="Txt_Fecha_Elaboracion" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                </td>
                                <td style="width: 15%;">
                                    &nbsp;
                                </td>
                                <td style="width: 35%;">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    Presupuesto Disponible
                                </td>
                                <td style="width: 35%;">
                                    <asp:TextBox ID="Txt_Presupuesto" runat="server" Width="98%" ReadOnly="true" BorderStyle="Outset"
                                        Style="text-align: center;" Font-Bold="true"></asp:TextBox>
                                </td>
                                <td style="width: 15%;">
                                    &nbsp;&nbsp;
                                </td>
                                <td style="width: 35%;">
                                    <asp:TextBox ID="Txt_Estatus" runat="server" Width="98%" ReadOnly="true" BorderStyle="Outset"
                                        Style="text-align: center;" Font-Bold="true"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Grupo_UR" runat="server" Text="Gerencia"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="Cmb_Grupo_UR" runat="server" Width="100%" Enabled="false">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text="Unidad Responsable"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="Cmb_Unidad_Responsable" runat="server" Width="100%" Enabled="false">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Programa_Proyecto" runat="server" Text="Programa Proyecto"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="Cmb_Programa_Proyecto" runat="server" Width="100%" Enabled="false">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Tipo_Servicio" runat="server" Text="Tipo Servicio"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="Cmb_Tipo_Servicio" runat="server" Width="100%">
                                        <asp:ListItem Value="">&lt; - - SELECCIONE - - &gt;</asp:ListItem>
                                        <asp:ListItem Value="REVISTA_MECANICA">REVISTA MECANICA</asp:ListItem>
                                        <asp:ListItem Value="SERVICIO_GENERAL">SERVICIO GENERAL</asp:ListItem>
                                        <asp:ListItem Value="VERIFICACION">VERIFICACIÓN</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    Tipo Bien
                                </td>
                                <td style="width: 35%;">
                                    <asp:DropDownList ID="Cmb_Tipo_Bien" Width="98%" runat="server" onchange="javascript:Tipo_Bien();">
                                        <asp:ListItem Value="0">&lt;-- SELECCIONE --&gt;</asp:ListItem>
                                        <asp:ListItem Value="BIEN_MUEBLE">BIEN MUEBLE</asp:ListItem>
                                        <asp:ListItem Value="VEHICULO">VEHICULO</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 15%;">
                                </td>
                                <td style="width: 35%;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:HiddenField ID="Hdf_Vehiculo_ID" runat="server" />
                                    <asp:Label ID="Lbl_No_Inventario" runat="server" Text="No. Inventario"></asp:Label>
                                </td>
                                <td style="width: 35%;">
                                    <asp:TextBox ID="Txt_No_Inventario" runat="server" Width="98%" MaxLength="7" Enabled="false"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Inventario" runat="server" TargetControlID="Txt_No_Inventario"
                                        FilterType="Numbers">
                                    </cc1:FilteredTextBoxExtender>
                                </td>
                                <td style="width: 15%;">
                                    &nbsp;&nbsp;
                                    <asp:Label ID="Lbl_No_Economico" runat="server" Text="No. Economico"></asp:Label>
                                    <asp:Label ID="Lbl_No_Serie" runat="server" Text="No. Serie" Style="display: none"></asp:Label>
                                </td>
                                <td style="width: 35%;">
                                    <asp:TextBox ID="Txt_No_Economico" runat="server" Width="70%" Enabled="false" MaxLength="6"
                                        onBlur="javascript:Formato_Ceros();" onkeyup="javascript:Formato_Unidad();" onkeypress="return get_KeyPress(this,event);"></asp:TextBox>
                                    <asp:TextBox ID="Txt_No_Serie" runat="server" Width="70%" Enabled="true" Style="display: none"></asp:TextBox>
                                    <cc1:TextBoxWatermarkExtender ID="Tbw_Economico" runat="server" TargetControlID="Txt_No_Economico"
                                        WatermarkCssClass="watermarked" WatermarkText="&lt;U00000&gt;" />
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="Txt_No_Economico"
                                        ValidChars="UMum" FilterType="Custom, Numbers" />
                                    <asp:ImageButton ID="Btn_Busqueda_Directa" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                        AlternateText="Buscar" ToolTip="Buscar" OnClick="Btn_Busqueda_Directa_Click" />
                                    <asp:ImageButton ID="Btn_Busqueda_Avanzada" runat="server" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png"
                                        Width="24px" AlternateText="Busqueda Avanzada" ToolTip="Busqueda Avanzada" OnClick="Btn_Busqueda_Avanzada_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:Panel ID="Pnl_Vehiculo_Seleccionado" runat="server" Width="99%" GroupingText="Vehículo para el Servicio">
                                        <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Datos_Vehiculo" runat="server" Text="Vehículo"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Datos_Vehiculo" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Placas" runat="server" Text="Placas"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Placas" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                                </td>
                                                <td style="width: 15%;">
                                                    &nbsp;&nbsp;
                                                    <asp:Label ID="Lbl_Anio" runat="server" Text="Año"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Anio" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Kilometraje" runat="server" Text="Kilometraje"></asp:Label>
                                                </td>
                                                <td style="width: 16%;">
                                                    <asp:TextBox ID="Txt_Kilometraje" runat="server" Width="80%"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Kilometraje" runat="server" TargetControlID="Txt_Kilometraje"
                                                        ValidChars="." FilterType="Custom, Numbers">
                                                    </cc1:FilteredTextBoxExtender>
                                                    <asp:Label ID="Lbl_Leyenda_KM" runat="server" Text="Km."></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="Pnl_Bien_Mueble_Seleccionado" runat="server" GroupingText="Bien Mueble para el Servicio"
                                        Width="99%">
                                        <table border="0" cellspacing="0" class="estilo_fuente" width="98%">
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:HiddenField ID="Hdf_Bien_Mueble_ID" runat="server" />
                                                    <asp:Label ID="Lbl_No_Inventario_BM" runat="server" Text="No. Inventario"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_No_Inventario_BM" runat="server" MaxLength="7" Width="98%" Enabled="false"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Inventario_BM" runat="server" FilterType="Numbers"
                                                        TargetControlID="Txt_No_Inventario_BM">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 15%;">
                                                    &nbsp;
                                                </td>
                                                <td style="width: 35%;">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Descripcion_Bien" runat="server" Text="Descripción Bien"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Descripcion_Bien" runat="server" Enabled="false" Rows="2" TextMode="MultiLine"
                                                        Width="99%"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Numero_Serie_Bien" runat="server" Text="No. Serie"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Numero_Serie_Bien" runat="server" Enabled="false" Width="99%"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    &nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:Panel ID="Pnl_Descripcion_Servicio" runat="server" Width="99%" GroupingText="Descripción del Servicio"
                                        Style="text-align: left">
                                        <asp:TextBox ID="Txt_Descripcion_Servicio" runat="server" Rows="5" TextMode="MultiLine"
                                            Width="99%" onkeyup="javascript:this.value=this.value.toUpperCase();"></asp:TextBox>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Correo_Electronico" runat="server" Text="Correo Electronico"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="Txt_Correo_Electronico" runat="server" Width="97%"> </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:Panel ID="Pnl_Motivo_Rechazo" runat="server" Width="99%" GroupingText="Comentarios de la Autorización">
                                        <asp:TextBox ID="Txt_Motivo_Rechazo" runat="server" Rows="3" TextMode="MultiLine"
                                            Width="99%" Enabled="false"></asp:TextBox>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </div>
                </center>
            </div>
            <br />
            <br />
            <br />
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpPnl_MPE_Busqueda_Vehiculo" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_Busqueda_Vehiculo" runat="server" Text="Button" Style="display: none;" />
            <cc1:ModalPopupExtender ID="MPE_Busqueda_Vehiculo" runat="server" TargetControlID="Btn_Comodin_Busqueda_Vehiculo"
                PopupControlID="Pnl_Busqueda_Vehiculo" CancelControlID="Btn_Cerrar_Busqueda_Vehiculo"
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Pnl_Busqueda_Vehiculo" runat="server" CssClass="drag" HorizontalAlign="Center"
        Style="display: none; border-style: outset; border-color: Silver; width: 760px;">
        <center>
            <asp:Panel ID="Pnl_Busqueda_Vehiculo_Interno" runat="server" CssClass="estilo_fuente"
                Style="cursor: move; background-color: Silver; color: Black; font-size: 12; font-weight: bold;
                border-style: outset;">
                <table class="estilo_fuente" width="100%">
                    <tr>
                        <td style="color: Black; font-size: 12; font-weight: bold; width: 90%">
                            <asp:Image ID="Img_Encabezado_Busqueda_Vehiculos" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                            Busqueda de Vehículos
                        </td>
                        <td align="right">
                            <asp:ImageButton ID="Btn_Cerrar_Busqueda_Vehiculo" CausesValidation="false" runat="server"
                                Style="cursor: pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:UpdatePanel ID="UpPnl_Busqueda_Vehiculo" runat="server">
                <ContentTemplate>
                    <asp:UpdateProgress ID="UpPrg_Busqueda_Vehiculo" runat="server" AssociatedUpdatePanelID="UpPnl_Busqueda_Vehiculo"
                        DisplayAfter="0">
                        <ProgressTemplate>
                            <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                            </div>
                            <div id="div_progress" class="processMessage">
                                <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <br />
                    <cc1:TabContainer ID="Tab_Contenedor_Pestagnas_Busqueda_Vehiculo" runat="server"
                        ActiveTabIndex="0" CssClass="Tab" Width="100%">
                        <cc1:TabPanel ID="Tab_Panel_Datos_Generales_Busqueda_Vehiculo" runat="server" BackColor="White"
                            HeaderText="Tab_Panel_Datos_Generales_Busqueda_Vehiculo" Height="400px" Width="100%">
                            <HeaderTemplate>
                                Generales
                            </HeaderTemplate>
                            <ContentTemplate>
                                <div style="border-style: outset; width: 99.5%; height: 200px; background-color: White;">
                                    <table width="100%">
                                        <tr>
                                            <td colspan="4" style="text-align: left;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Numero_Inventario" runat="server" CssClass="estilo_fuente"
                                                    Text="Número Inventario"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Busqueda_Vehiculo_Numero_Inventario" runat="server" Width="97%"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_Numero_Inventario" runat="server"
                                                    Enabled="True" FilterType="Numbers" TargetControlID="Txt_Busqueda_Vehiculo_Numero_Inventario">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Numero_Economico" runat="server" CssClass="estilo_fuente"
                                                    Text="Número Económico"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Busqueda_Vehiculo_Numero_Economico" runat="server" Width="97%"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_Numero_Economico" runat="server"
                                                    Enabled="True" FilterType="Numbers" InvalidChars="&lt;,&gt;,&amp;,',!," TargetControlID="Txt_Busqueda_Vehiculo_Numero_Economico"
                                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ -_">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Modelo" runat="server" CssClass="estilo_fuente"
                                                    Text="Modelo"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Busqueda_Vehiculo_Modelo" runat="server" Width="97%"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_Modelo" runat="server"
                                                    Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                    InvalidChars="&lt;,&gt;,&amp;,',!," TargetControlID="Txt_Busqueda_Vehiculo_Modelo"
                                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ -_">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Marca" runat="server" CssClass="estilo_fuente"
                                                    Text="Marca"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Marca" runat="server" Width="100%">
                                                    <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Anio_Fabricacion" runat="server" CssClass="estilo_fuente"
                                                    Text="Año Fabricación"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Busqueda_Vehiculo_Anio_Fabricacion" runat="server" MaxLength="4"
                                                    Width="97%"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_Anio_Fabricacion" runat="server"
                                                    Enabled="True" FilterType="Numbers" TargetControlID="Txt_Busqueda_Vehiculo_Anio_Fabricacion">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Color" runat="server" CssClass="estilo_fuente"
                                                    Text="Color"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Color" runat="server" Width="100%">
                                                    <asp:ListItem Text="&lt; TODOS &gt;" Value="TODOS"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Dependencias" runat="server" CssClass="estilo_fuente"
                                                    Text="U. Responsable"></asp:Label>
                                            </td>
                                            <td colspan="3" style="width: 80%; text-align: left;">
                                                <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Dependencias" runat="server" Enabled="False"
                                                    Width="85%">
                                                    <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:ImageButton ID="Btn_Buscar_Datos_Vehiculo" runat="server" CausesValidation="False"
                                                    ImageUrl="~/paginas/imagenes/paginas/busqueda.png" OnClick="Btn_Buscar_Datos_Vehiculo_Click"
                                                    ToolTip="Buscar Contrarecibos" />
                                                <asp:ImageButton ID="Btn_Limpiar_Filtros_Buscar_Datos_Vehiculo" runat="server" CausesValidation="False"
                                                    ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png" OnClick="Btn_Limpiar_Filtros_Buscar_Datos_Vehiculo_Click"
                                                    ToolTip="Limpiar Filtros" Width="20px" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="Tab_Panel_Resguardantes_Busqueda_Vehiculo" runat="server" BackColor="White"
                            HeaderText="Tab_Panel_Reguardantes_Busqueda_Vehiculo" Width="100%">
                            <HeaderTemplate>
                                Resguardantes
                            </HeaderTemplate>
                            <ContentTemplate>
                                <div style="border-style: outset; width: 99.5%; height: 200px; background-color: White;">
                                    <table width="100%">
                                        <tr>
                                            <td colspan="2" style="text-align: left;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_RFC_Resguardante" runat="server" CssClass="estilo_fuente"
                                                    Text="RFC Reguardante"></asp:Label>
                                            </td>
                                            <td style="text-align: left; width: 30%;">
                                                <asp:TextBox ID="Txt_Busqueda_Vehiculo_RFC_Resguardante" runat="server" Width="200px"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_RFC_Resguardante" runat="server"
                                                    Enabled="True" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                                    InvalidChars="&lt;,&gt;,&amp;,',!," TargetControlID="Txt_Busqueda_Vehiculo_RFC_Resguardante"
                                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_No_Empleado" runat="server" CssClass="estilo_fuente"
                                                    Text="No. Empleado"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Busqueda_Vehiculo_No_Empleado" runat="server" Width="97%"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_No_Empleado" runat="server"
                                                    Enabled="True" FilterType="Numbers" InvalidChars="&lt;,&gt;,&amp;,',!," TargetControlID="Txt_Busqueda_Vehiculo_No_Empleado">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Resguardantes_Dependencias" runat="server" CssClass="estilo_fuente"
                                                    Text="U. Responsable"></asp:Label>
                                            </td>
                                            <td colspan="3" style="text-align: left;">
                                                <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias" runat="server"
                                                    AutoPostBack="true" Enabled="false" OnSelectedIndexChanged="Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias_SelectedIndexChanged"
                                                    Width="100%">
                                                    <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Nombre_Resguardante" runat="server" CssClass="estilo_fuente"
                                                    Text="Resguardante"></asp:Label>
                                            </td>
                                            <td colspan="3" colspan="3" style="text-align: left;">
                                                <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Nombre_Resguardante" runat="server" Width="100%">
                                                    <asp:ListItem Text="&lt;TODOS&gt;" Value="TODOS"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="text-align: right">
                                                <asp:ImageButton ID="Btn_Buscar_Resguardante_Vehiculo" runat="server" CausesValidation="False"
                                                    ImageUrl="~/paginas/imagenes/paginas/busqueda.png" OnClick="Btn_Buscar_Resguardante_Vehiculo_Click"
                                                    ToolTip="Buscar Listados" />
                                                <asp:ImageButton ID="Btn_Limpiar_Filtros_Buscar_Resguardante_Vehiculo" runat="server"
                                                    CausesValidation="False" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png"
                                                    OnClick="Btn_Limpiar_Filtros_Buscar_Resguardante_Vehiculo_Click" ToolTip="Limpiar Filtros"
                                                    Width="20px" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ContentTemplate>
                        </cc1:TabPanel>
                    </cc1:TabContainer>
                    <div style="width: 99%; height: 150px; overflow: auto; border-style: outset; background-color: White;">
                        <center>
                            <caption>
                                <asp:GridView ID="Grid_Listado_Busqueda_Vehiculo" runat="server" AllowPaging="true"
                                    AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" OnPageIndexChanging="Grid_Listado_Busqueda_Vehiculo_PageIndexChanging"
                                    OnSelectedIndexChanged="Grid_Listado_Busqueda_Vehiculo_SelectedIndexChanged"
                                    PageSize="100" Width="98%">
                                    <RowStyle CssClass="GridItem" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                    <Columns>
                                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                            <ItemStyle Width="30px" />
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="VEHICULO_ID" HeaderText="VEHICULO_ID" SortExpression="VEHICULO_ID" />
                                        <asp:BoundField DataField="NUMERO_INVENTARIO" HeaderText="No. Inven." SortExpression="NUMERO_INVENTARIO">
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="90px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NUMERO_ECONOMICO" HeaderText="No. Eco." SortExpression="NUMERO_ECONOMICO">
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="90px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="VEHICULO" HeaderText="Vehículo" SortExpression="VEHICULO">
                                            <ItemStyle Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="MARCA" HeaderText="Marca" SortExpression="MARCA">
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="90px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="MODELO" HeaderText="Modelo" SortExpression="MODELO">
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="150px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ANIO" HeaderText="Año" SortExpression="ANIO">
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="70px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS">
                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="90px" />
                                        </asp:BoundField>
                                    </Columns>
                                    <HeaderStyle CssClass="GridHeader" />
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                </asp:GridView>
                            </caption>
                        </center>
                        <asp:HiddenField ID="Hdf_Estatus" runat="server" />
                        <asp:HiddenField ID="HiddenField2" runat="server" />
                        <asp:HiddenField ID="HiddenField3" runat="server" />
                        <asp:HiddenField ID="HiddenField4" runat="server" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </center>
    </asp:Panel>
</asp:Content>
