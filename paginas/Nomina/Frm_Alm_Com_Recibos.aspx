﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Cat_Nom_Bancos.aspx.cs" Inherits="paginas_Nomina_Frm_Cat_Nom_Bancos"
    Title="Catálogo Bancos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">
    <%--<script src="../../javascript/Js_Cat_Nom_Bancos.js" type="text/javascript"></script>--%>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="SM_Bancos" runat="server" />
    <asp:UpdatePanel ID="UPnl_Bancos" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="UPnl_Bancos"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Antiguedad_Sindicato" style="background-color: #ffffff; width: 100%;
                height: 100%;">
                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">
                            Cuentas Bancarias
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png"
                                Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error" />
                        </td>
                    </tr>
                </table>
                <table width="98%" border="0" cellspacing="0">
                    <tr align="center">
                        <td colspan="2">
                            <div align="right" class="barra_busqueda">
                                <table style="width: 100%; height: 28px;">
                                    <tr>
                                        <td align="left" style="width: 59%;">
                                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" CssClass="Img_Button"
                                                TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" OnClick="Btn_Nuevo_Click" />
                                            <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" CssClass="Img_Button"
                                                TabIndex="4" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" OnClick="Btn_Modificar_Click" />
                                            <asp:ImageButton ID="Btn_Eliminar" runat="server" ToolTip="Eliminar" CssClass="Img_Button"
                                                TabIndex="5" ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png" OnClick="Btn_Eliminar_Click"
                                                OnClientClick="return confirm('¿Está seguro de eliminar el Banco seleccionado?');" />
                                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button"
                                                TabIndex="6" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" OnClick="Btn_Salir_Click" />
                                        </td>
                                        <td align="right" style="width: 41%;">
                                            <table style="width: 100%; height: 28px;">
                                                <tr>
                                                    <td style="width: 60%; vertical-align: top; text-align: right">
                                                        B&uacute;squeda
                                                        <asp:TextBox ID="Txt_Busqueda_Bancos" runat="server" MaxLength="100" TabIndex="21"
                                                            ToolTip="Busquedad de Bancos" Width="180px" />
                                                        <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda_Bancos" runat="server" WatermarkCssClass="watermarked"
                                                            WatermarkText="<Banco_ID ó Nombre>" TargetControlID="Txt_Busqueda_Bancos" />
                                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Bancos" runat="server" TargetControlID="Txt_Busqueda_Bancos"
                                                            FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ. " />
                                                        <asp:ImageButton ID="Btn_Busqueda_Bancos" runat="server" TabIndex="22" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                                            ToolTip="Consultar" OnClick="Btn_Busqueda_Bancos_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
                <br />
                <table width="98%">
                    <tr>
                        <td style="width: 100%" colspan="4">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; width: 20%; display: none;">
                            Banco ID
                        </td>
                        <td style="text-align: left; width: 30%; display: none;">
                            <asp:TextBox ID="Txt_Banco_ID" runat="server" Width="98%" TabIndex="0" />
                        </td>
                        <td style="text-align: left; width: 20%;">
                        </td>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; width: 20%;">
                            <b>*</b>No Cuenta
                        </td>
                        <td style="text-align: left; width: 30%;">
                            <asp:TextBox ID="Txt_No_Cuenta" runat="server" Width="98%" TabIndex="1" MaxLength="20" />
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Cuenta" runat="server" TargetControlID="Txt_No_Cuenta"
                                FilterType="Numbers" />
                        </td>
                        <td style="text-align: left; width: 20%;">
                            &nbsp;&nbsp;<b>*</b>Tipo
                        </td>
                        <td style="text-align: left; width: 30%;">
                            <asp:DropDownList ID="Cmb_Tipo" runat="server" Width="100%">
                                <asp:ListItem Value="">&lt;-- Seleccione -- &gt;</asp:ListItem>
                                <asp:ListItem Value="NOMINA" Selected="True"> NOMINA</asp:ListItem>
                                <asp:ListItem Value="INGRESOS">INGRESOS</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; width: 20%;">
                            <b>*</b>Nombre
                        </td>
                        <td style="text-align: left; width: 30%;">
                            <asp:TextBox ID="Txt_Nombre_Banco" runat="server" Width="98%" TabIndex="2" MaxLength="100" />
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Banco" runat="server" TargetControlID="Txt_Nombre_Banco"
                                FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ " />
                        </td>
                        <td style="text-align: left; width: 20%;">
                            &nbsp;&nbsp;Clabe
                        </td>
                        <td style="text-align: left; width: 30%;">
                            <asp:TextBox ID="Txt_Clave_Banco" runat="server" Width="98%" TabIndex="2" MaxLength="2" />
                            <cc1:FilteredTextBoxExtender ID="Fte_Clave_Banco" runat="server" TargetControlID="Txt_Clave_Banco"
                                FilterType="Numbers" />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; width: 20%;">
                            <b>*</b>Sucursal
                        </td>
                        <td style="text-align: left; width: 30%;">
                            <asp:TextBox ID="Txt_Sucursal" runat="server" Width="98%" TabIndex="3" MaxLength="100" />
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Sucursal" runat="server" TargetControlID="Txt_Sucursal"
                                FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ " />
                        </td>
                        <td style="text-align: left; width: 20%;">
                            &nbsp;&nbsp;Referencia
                        </td>
                        <td style="text-align: left; width: 30%;">
                            <asp:TextBox ID="Txt_Referencia" runat="server" Width="98%" TabIndex="4" MaxLength="30" />
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Referencia" runat="server" TargetControlID="Txt_Referencia"
                                FilterType="Numbers" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 20%;">
                            *Cuenta Contable
                        </td>
                        <td colspan="3" style="width: 80%;">
                            <asp:DropDownList ID="Cmb_Cuenta_Contable" runat="server" Width="95%" />
                            <asp:ImageButton ID="Btn_Buscar_Cuenta" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                ToolTip="Consultar" OnClick="Cmb_Cuenta_Contable_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; width: 20%; vertical-align: top;">
                            Comentarios
                        </td>
                        <td style="text-align: left; width: 30%;" colspan="3">
                            <asp:TextBox ID="Txt_Comentarios" runat="server" Width="99.5%" MaxLength="100" TextMode="MultiLine"
                                TabIndex="5" Height="45px" Wrap="true" />
                            <cc1:FilteredTextBoxExtender ID="FTxt_Comentarios" runat="server" TargetControlID="Txt_Comentarios"
                                FilterType="Custom, LowercaseLetters, UppercaseLetters, Numbers" ValidChars="áéíóúÁÉÍÓÚ ñÑ" />
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Comentarios" runat="server" TargetControlID="Txt_Comentarios"
                                WatermarkText="Límite de Caractes 250" WatermarkCssClass="watermarked" />
                            <span id="Contador_Caracteres_Comentarios" class="watermarked"></span>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%" colspan="4">
                            <hr />
                        </td>
                    </tr>
                </table>
                <table style="width: 98%;" id="Tbl_Plan_Pagos" runat="server">
                    <tr>
                        <td class="button_autorizar" style="text-align: left; width: 20%; cursor: default;
                            display: none;">
                            Plan Pagos
                        </td>
                        <td class="button_autorizar" style="text-align: left; width: 30%; display: none;">
                            <asp:DropDownList ID="Cmb_Plan_Pagos" runat="server" Width="100%">
                                <asp:ListItem Value="">&lt;-- Seleccione -- &gt;</asp:ListItem>
                                <asp:ListItem Value="NORMAL">NORMAL</asp:ListItem>
                                <asp:ListItem Value="MESES_SIN_INTERESES">MESES SIN INTERESES</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="button_autorizar" style="text-align: left; width: 20%; cursor: default;
                            display: none;">
                            &nbsp;&nbsp;No Meses
                        </td>
                        <td class="button_autorizar" style="text-align: left; width: 30%; display: none;">
                            <asp:DropDownList ID="Cmb_No_Meses" runat="server" Width="100%" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%" colspan="4">
                            <hr />
                        </td>
                    </tr>
                </table>
                <div style="overflow: auto; height: 400px; width: 97%; vertical-align: top; border-style: outset;
                    color: White;">
                    <asp:GridView ID="Grid_Bancos" runat="server" CssClass="GridView_1" Width="100%"
                        AutoGenerateColumns="False" GridLines="None" AllowPaging="true" PageSize="5"
                        OnPageIndexChanging="Grid_Bancos_PageIndexChanging" OnSelectedIndexChanged="Grid_Bancos_SelectedIndexChanged"
                        AllowSorting="True" OnSorting="Grid_Bancos_Sorting" HeaderStyle-CssClass="tblHead">
                        <Columns>
                            <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                <ItemStyle Width="2%" />
                                <HeaderStyle Width="2%" />
                            </asp:ButtonField>
                            <asp:BoundField DataField="BANCO_ID" />
                            <asp:BoundField DataField="NOMBRE" HeaderText="Nombre" SortExpression="NOMBRE">
                                <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small" />
                                <ItemStyle HorizontalAlign="Left" Width="20%" />
                            </asp:BoundField>
                            <asp:BoundField DataField="COMENTARIOS" HeaderText="Comentarios" SortExpression="COMENTARIOS">
                                <HeaderStyle HorizontalAlign="Left" Width="58%" Font-Size="X-Small" />
                                <ItemStyle HorizontalAlign="Left" Width="58%" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TIPO" HeaderText="Tipo" SortExpression="Tipo">
                                <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small" />
                                <ItemStyle HorizontalAlign="Left" Width="20%" />
                            </asp:BoundField>
                        </Columns>
                        <SelectedRowStyle CssClass="GridSelected" />
                        <PagerStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="GridAltItem" />
                    </asp:GridView>
                </div>
                <br />
                <br />
                <br />
                <br />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:UpdatePanel ID="Aux_Mpe_Busqueda_Cuenta" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Open_Busqueda_Cuenta" runat="server" Text="Button" Style="display: none;" />
            <cc1:ModalPopupExtender ID="Mpe_Busqueda_Cuenta" runat="server" BehaviorID="Busqueda_Cuenta" 
                TargetControlID="Btn_Open_Busqueda_Cuenta" PopupControlID="Pnl_Busqueda_Cuenta" 
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter" />
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:Panel ID="Pnl_Busqueda_Cuenta" runat="server" CssClass="drag" HorizontalAlign="Center"
        Width="850px" Style="display: none; border-style: outset; border-color: Silver;
        background-image: url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG); background-repeat: repeat-y;">
        <asp:Panel ID="Pnl_Busqueda_Cuenta_Cabecera" runat="server" Style="cursor: move;
            background-color: Silver; color: Black; font-size: 12; font-weight: bold; border-style: outset;">
            <table width="99%">
                <tr>
                    <td style="color: Black; font-size: 12; font-weight: bold;">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                        B&uacute;squeda: Cuentas Contables
                    </td>
                    <td align="right" style="width: 10%;">
                        <asp:ImageButton ID="Btn_Cerrar_Ventana" CausesValidation="false" runat="server"
                            Style="cursor: pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png"
                            OnClientClick="javascript:return Cerrar_Busqueda_Cuenta();" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <div style="color: #5D7B9D">
            <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;">
                        <asp:UpdatePanel ID="Upnl_Filtros_Busqueda_Cuenta" runat="server">
                            <ContentTemplate>
                                <asp:UpdateProgress ID="Progress_Upnl_Filtros_Busqueda_Cuenta" runat="server" AssociatedUpdatePanelID="Upnl_Filtros_Busqueda_Cuenta"
                                    DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                        </div>
                                        <div style="background-color: Transparent; position: fixed; top: 50%; left: 47%;
                                            padding: 10px; z-index: 1002;" id="div_progress">
                                            <img alt="" src="../Imagenes/paginas/Sias_Roler.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <table width="100%">
                                    <tr>
                                        <td colspan="4">
                                            <table style="width: 80%;">
                                                <tr>
                                                    <td align="left">
                                                        <asp:ImageButton ID="Img_Error_Busqueda_Cuenta" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                            Width="24px" Height="24px" Visible="false" />
                                                        <asp:Label ID="Lbl_Error_Busqueda_Cuenta" runat="server" Text="" CssClass="estilo_fuente_mensaje_error"
                                                            Visible="false" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%" colspan="4" align="right">
                                            <asp:ImageButton ID="Btn_Limpiar_Ctlr" runat="server" OnClientClick="javascript:return Limpiar_Controles_Busqueda();"
                                                ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de B&uacute;squeda" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%" colspan="4">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: left; font-size: 11px;">
                                            Cuenta
                                        </td>
                                        <td colspan="3" style="text-align: left; font-size: 11px;">
                                            <asp:TextBox ID="Txt_Busqueda_Cuenta" runat="server" Width="98%" TabIndex="11" MaxLength="100" />
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Empleado" runat="server" TargetControlID="Txt_Busqueda_Cuenta"
                                                FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ. ">
                                            </cc1:FilteredTextBoxExtender>
                                        </td>
                                        <td colspan="2" style="width: 50%; text-align: left; font-size: 11px;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%" colspan="4">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%; text-align: left;" colspan="4">
                                            <center>
                                                <asp:Button ID="Btn_Busqueda_Cuenta_Contable" runat="server" Text="Busqueda de Cuenta"
                                                    CssClass="button" CausesValidation="false" Width="300px" TabIndex="15" OnClick="Btn_Busqueda_Cuenta_Contable_Click" />
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%" colspan="4">
                                            <div id="Div_Busqueda" runat="server" style="overflow: auto; max-height: 200px; width: 99%">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="Grid_Cuentas" runat="server" CssClass="GridView_1" AutoGenerateColumns="False"
                                                                GridLines="None" Width="99.9%" AllowSorting="True" HeaderStyle-CssClass="tblHead"
                                                                EmptyDataText="No se encontraron Registros">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="">
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="Btn_Seleccionar_Cuenta" runat="server" ImageUrl="~/paginas/imagenes/gridview/blue_button.png"
                                                                                CommandArgument='<%# Eval("Cuenta_Contable_ID") %>' OnClick="Btn_Seleccionar_Cuenta_Click" />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                                        <ItemStyle HorizontalAlign="Center" Width="3%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="Cuenta_Contable_ID" HeaderText="Cuenta_ID">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="CUENTA" HeaderText="No. Cuenta" SortExpression="CUENTA">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                                                        <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripcion" SortExpression="Descripcion">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="60%" />
                                                                        <ItemStyle HorizontalAlign="Left" Width="60%" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <SelectedRowStyle CssClass="GridSelected" />
                                                                <PagerStyle CssClass="GridHeader" />
                                                                <HeaderStyle CssClass="tblHead" />
                                                                <AlternatingRowStyle CssClass="GridAltItem" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <hr />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>
