﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using JAPAMI.Sessiones;
using System.Data.Common;
using System.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using SharpContent.ApplicationBlocks.Data;

public partial class paginas_Nomina_Frm_Layout_Empleados_Actualizar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

      ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Actualizar_Empleado
    ///DESCRIPCIÓN:          Metodo que consulta un archivo EXCEL
    ///PARAMETROS:           String sqlExcel.- string que contiene el select
    ///CREO:                 Susana Trigueros Armenta
    ///FECHA_CREO:           23/Mayo/2011 
    ///MODIFICO:             Salvador Hernández Ramírez
    ///FECHA_MODIFICO:       26/Mayo/2011 
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public DataTable Actualizar_Empleado(DataTable Dt_Empleados)
    {


        if (Dt_Empleados.Rows.Count > 0)
        {
            String Mi_SQL = "";
            int j = 0;
            for (int i = 0; i < Dt_Empleados.Rows.Count; i++)
            {
                if ((Dt_Empleados.Rows[i]["Cuenta_Contable"].ToString().Trim() != "0") || (Dt_Empleados.Rows[i]["Cuenta_Contable"].ToString().Trim() != String.Empty))
                {
                    try
                    {
                        Mi_SQL = "UPDATE " + Cat_Empleados.Tabla_Cat_Empleados;
                        Mi_SQL = Mi_SQL + " SET " + Cat_Empleados.Campo_Cuenta_Contable_ID;
                        Mi_SQL = Mi_SQL + "=(SELECT " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                        Mi_SQL = Mi_SQL + " FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables;
                        Mi_SQL = Mi_SQL + " WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta;
                        Mi_SQL = Mi_SQL + " =" + Dt_Empleados.Rows[i]["Cuenta_Contable"].ToString().Trim().Replace(".", "") + ")";
                        Mi_SQL = Mi_SQL + " WHERE " + Cat_Empleados.Campo_No_Empleado;
                        Mi_SQL = Mi_SQL + " ='" + String.Format("{0:000000}", Convert.ToInt32(Dt_Empleados.Rows[i]["No_Empleado"].ToString().Trim())) + "'";
                        SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    catch(Exception Ex)
                    {
                        int fila = j;
                    }
                }
                j++;
            
            }
            //Grid_Empleados.DataSource = Dt_Empleados;
            //Grid_Empleados.DataBind();
        }

        return Dt_Empleados;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Leer_Excel
    ///DESCRIPCIÓN:          Metodo que consulta un archivo EXCEL
    ///PARAMETROS:           String sqlExcel.- string que contiene el select
    ///CREO:                 Susana Trigueros Armenta
    ///FECHA_CREO:           23/Mayo/2011 
    ///MODIFICO:             Salvador Hernández Ramírez
    ///FECHA_MODIFICO:       26/Mayo/2011 
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public DataSet Leer_Excel(String sqlExcel)
    {
        //Para empezar definimos la conexión OleDb a nuestro fichero Excel.
        //String Rta = @MapPath("../../Archivos/PRESUPUESTO_IRAPUATO.xls");
        String Rta = @MapPath("../../Archivos/Empleados.xls");
        string sConnectionString = "";// @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Rta + ";Extended Properties=Excel 8.0;";



        if (Rta.Contains(".xlsx"))       // Formar la cadena de conexion si el archivo es Exceml xml
        {
            sConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                    "Data Source=" + Rta + ";" +
                    "Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";
        }
        else if (Rta.Contains(".xls"))   // Formar la cadena de conexion si el archivo es Exceml binario
        {
            sConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                    "Data Source=" + Rta + ";" +
                    "Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";
            //sConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;" +
            //        "Data Source=" + Rta + ";" +
            //        "Extended Properties=Excel 8.0;";
        }

        //Definimos el DataSet donde insertaremos los datos que leemos del excel
        DataSet DS = new DataSet();

        //Definimos la conexión OleDb al fichero Excel y la abrimos
        OleDbConnection oledbConn = new OleDbConnection(sConnectionString);
        oledbConn.Open();

        //Creamos un comand para ejecutar la sentencia SELECT.
        OleDbCommand oledbCmd = new OleDbCommand(sqlExcel, oledbConn);

        //Creamos un dataAdapter para leer los datos y asocialor al DataSet.
        OleDbDataAdapter da = new OleDbDataAdapter(oledbCmd);
        da.Fill(DS);
        return DS;
    }


    public DataSet Leer_Excel(String sqlExcel, String Path)
    {
        //Para empezar definimos la conexión OleDb a nuestro fichero Excel.
        //String Rta = @MapPath("../../Archivos/PRESUPUESTO_IRAPUATO.xls");
        String Rta = @MapPath(Path);
        string sConnectionString = "";// @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Rta + ";Extended Properties=Excel 8.0;";

        if (Rta.Contains(".xlsx"))       // Formar la cadena de conexion si el archivo es Exceml xml
        {
            sConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                    "Data Source=" + Rta + ";" +
                    "Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";
        }
        else if (Rta.Contains(".xls"))   // Formar la cadena de conexion si el archivo es Exceml binario
        {
            sConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                    "Data Source=" + Rta + ";" +
                    "Extended Properties=Excel 8.0;";
        }

        //Definimos el DataSet donde insertaremos los datos que leemos del excel
        DataSet DS = new DataSet();

        //Definimos la conexión OleDb al fichero Excel y la abrimos
        OleDbConnection oledbConn = new OleDbConnection(sConnectionString);
        oledbConn.Open();

        //Creamos un comand para ejecutar la sentencia SELECT.
        OleDbCommand oledbCmd = new OleDbCommand(sqlExcel, oledbConn);

        //Creamos un dataAdapter para leer los datos y asocialor al DataSet.
        OleDbDataAdapter da = new OleDbDataAdapter(oledbCmd);
        da.Fill(DS);
        return DS;
    }

    protected void Btn_Actualizar_Empleados_Click(object sender, ImageClickEventArgs e)
    {
        //leemos el archivo 
        DataSet Ds = Leer_Excel("Select * From [Empleados$]");
        Actualizar_Empleado(Ds.Tables[0]);

    }
}
