﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Area_Funcional.Negocio;
using JAPAMI.Catalogo_SAP_Fuente_Financiamiento.Negocio;
using JAPAMI.Catalogo_Compras_Proyectos_Programas.Negocio;
using System.Collections.Generic;
using JAPAMI.Puestos.Negocios;
using JAPAMI.Grupos_Dependencias.Negocio;

public partial class paginas_Nomina_Frm_Cat_Dependencias : System.Web.UI.Page
{
    #region (Load/Init)
    ///************************************************************************************************************************************************
    /// NOMBRE DE LA FUNCION: Page_Load
    /// 
    /// DESCRIPCION : Carga la configuración inicial de los controles de la página.
    /// 
    /// CREO        : Juan Alberto Hernández Negrete
    /// FECHA_CREO  : 08/Marzo/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///************************************************************************************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                ViewState["SortDirection"] = "ASC";
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion

    #region (Metodos)

    #region (Métodos Generales)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Inicializa_Controles
    /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda realizar
    ///               diferentes operaciones
    /// PARAMETROS  : 
    /// CREO        : Juan Alberto Hernández Negrete
    /// FECHA_CREO  : 8/Marzo/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Inicializa_Controles()
    {
        try
        {
            Habilitar_Controles("Inicial"); //Habilita los controles de la forma para que el usuario pueda indica que operación desea realizar
            Limpia_Controles();             //Limpia los controles del forma
            Consulta_Dependencias();        //Consulta todas las dependencias que fueron dadas de alta en la BD
            Consulta_Areas_Funcionales();   //Consulta las áreas funcionales registradas en el sistema.
            Consulta_Fte_Financiamiento();  //Consulta las fuentes de financiamiento registradas en el sistema.
            Consulta_Programas();           //Consulta los programas registrados en el sistema.
            Consultar_Puestos();
            Consulta_Grupos_Dependencia();  //consulta los grupos dependencia
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message.ToString());
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Controles
    /// DESCRIPCION : Limpia los controles que se encuentran en la forma
    /// PARAMETROS  : 
    /// CREO        : Juan Alberto Hernández Negrete
    /// FECHA_CREO  : 8/Marzo/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Limpia_Controles()
    {
        try
        {
            Txt_Dependencia_ID.Text = "";
            Txt_Clave_Dependecia.Text = "";
            Txt_Nombre_Dependencia.Text = "";
            Txt_Comentarios_Dependencia.Text = "";
            Txt_Busqueda_Dependencia.Text = "";
            Cmb_Estatus_Dependencia.SelectedIndex = -1;
            Cmb_Area_Funcional.SelectedIndex = -1;
            Cmb_Grupo_Dependencia.SelectedIndex = -1;

            Cmb_Fuente_Financiamiento.SelectedIndex = -1;
            Grid_Fuentes_Financiamiento.SelectedIndex = -1;
            Grid_Fuentes_Financiamiento.DataSource = new DataTable();
            Grid_Fuentes_Financiamiento.DataBind();

            Cmb_Programa.SelectedIndex = -1;
            Grid_Programas.SelectedIndex = -1;
            Grid_Programas.DataSource = new DataTable();
            Grid_Programas.DataBind();

            Cmb_Puestos.SelectedIndex = -1;
            Grid_Puestos.SelectedIndex = -1;
            Grid_Puestos.DataSource = new DataTable();
            Grid_Puestos.DataBind();

        }
        catch (Exception ex)
        {
            throw new Exception("Limpia_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Habilitar_Controles
    /// DESCRIPCION : Habilita y Deshabilita los controles de la forma para prepara la página
    ///                para a siguiente operación
    /// PARAMETROS  : Operacion: Indica la operación que se desea realizar por parte del usuario
    ///                           si es una alta, modificacion
    ///                           
    /// CREO        : Juan Alberto Hernández Negrete
    /// FECHA_CREO  : 8/Marzo/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Habilitar_Controles(String Operacion)
    {
        Boolean Habilitado; ///Indica si el control de la forma va hacer habilitado para utilización del usuario
        try
        {
            Habilitado = false;
            switch (Operacion)
            {
                case "Inicial":
                    Habilitado = false;
                    Cmb_Estatus_Dependencia.SelectedIndex = 0;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = true;
                    Btn_Eliminar.Visible = true;
                    Btn_Nuevo.CausesValidation = false;
                    Btn_Modificar.CausesValidation = false;
                    Cmb_Estatus_Dependencia.Enabled = false;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";

                    Configuracion_Acceso("Frm_Cat_Dependencias.aspx");
                    break;

                case "Nuevo":
                    Habilitado = true;
                    Cmb_Estatus_Dependencia.SelectedIndex = 0;
                    Btn_Nuevo.ToolTip = "Dar de Alta";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = false;
                    Btn_Eliminar.Visible = false;
                    Btn_Nuevo.CausesValidation = true;
                    Btn_Modificar.CausesValidation = true;
                    Cmb_Estatus_Dependencia.Enabled = false;
                    Cmb_Estatus_Dependencia.SelectedIndex = 1;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";

                    if (Session["Dt_Fte_Financiamiento"] != null) Session.Remove("Dt_Fte_Financiamiento");
                    if (Session["Dt_Programas"] != null) Session.Remove("Dt_Programas");
                    if (Session["PUESTOS_UNIDAD_RESPNSABLE"] != null) Session.Remove("PUESTOS_UNIDAD_RESPNSABLE");
                    break;

                case "Modificar":
                    Habilitado = true;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Nuevo.Visible = false;
                    Btn_Modificar.Visible = true;
                    Btn_Eliminar.Visible = false;
                    Btn_Nuevo.CausesValidation = true;
                    Btn_Modificar.CausesValidation = true;
                    Cmb_Estatus_Dependencia.Enabled = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                    break;
            }
            Txt_Nombre_Dependencia.Enabled = Habilitado;
            Txt_Comentarios_Dependencia.Enabled = Habilitado;
            Cmb_Grupo_Dependencia.Enabled = Habilitado;
            Txt_Busqueda_Dependencia.Enabled = !Habilitado;
            Btn_Buscar_Dependencia.Enabled = !Habilitado;
            Grid_Dependencias.Enabled = !Habilitado;
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;

            Cmb_Area_Funcional.Enabled = Habilitado;
            Txt_Clave_Dependecia.Enabled = Habilitado;

            Cmb_Fuente_Financiamiento.Enabled = Habilitado;
            Btn_Agregar_Fte_Financiamiento.Enabled = Habilitado;
            Grid_Fuentes_Financiamiento.Enabled = Habilitado;

            Cmb_Programa.Enabled = Habilitado;
            Btn_Agregar_Programa.Enabled = Habilitado;
            Grid_Programas.Enabled = Habilitado;

            Cmb_Puestos.Enabled = Habilitado;
            Btn_Agregar_Puestos.Enabled = Habilitado;
            Grid_Puestos.Enabled = Habilitado;
        }
        catch (Exception ex)
        {
            throw new Exception("Habilitar_Controles " + ex.Message.ToString(), ex);
        }
    }
    #endregion

    #region (Métodos Operación)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Alta_Dependencia
    /// DESCRIPCION : Da de Alta la Dependencia con los datos proporcionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 23-Agosto-2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Alta_Dependencia()
    {
        Cls_Cat_Dependencias_Negocio Obj_Cat_Dependencias = new Cls_Cat_Dependencias_Negocio();  //Variable de conexión hacia la capa de Negocios

        try
        {
            Obj_Cat_Dependencias.P_Nombre = Txt_Nombre_Dependencia.Text;
            Obj_Cat_Dependencias.P_Estatus = Cmb_Estatus_Dependencia.SelectedValue;
            Obj_Cat_Dependencias.P_Comentarios = Txt_Comentarios_Dependencia.Text;
            Obj_Cat_Dependencias.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
            Obj_Cat_Dependencias.P_Area_Funcional_ID = Cmb_Area_Funcional.SelectedValue.Trim();
            Obj_Cat_Dependencias.P_Clave = Txt_Clave_Dependecia.Text.Trim();
            Obj_Cat_Dependencias.P_Grupo_Dependencia_ID = Cmb_Grupo_Dependencia.SelectedValue;

            if (Session["Dt_Fte_Financiamiento"] != null)
            {
                Obj_Cat_Dependencias.P_Dt_Fuentes_Financiamiento = (DataTable)Session["Dt_Fte_Financiamiento"];
            }
            if (Session["Dt_Fte_Financiamiento"] != null)
            {
                Session.Remove("Dt_Fte_Financiamiento");
            }

            if (Session["Dt_Programas"] != null)
            {
                Obj_Cat_Dependencias.P_Dt_Programas = (DataTable)Session["Dt_Programas"];
            }
            if (Session["Dt_Programas"] != null)
            {
                Session.Remove("Dt_Programas");
            }

            if (Session["PUESTOS_UNIDAD_RESPNSABLE"] != null)
            {
                Obj_Cat_Dependencias.P_Dt_Puestos = (DataTable)Session["PUESTOS_UNIDAD_RESPNSABLE"];
            }
            if (Session["PUESTOS_UNIDAD_RESPNSABLE"] != null)
            {
                Session.Remove("PUESTOS_UNIDAD_RESPNSABLE");
            }
           

            Obj_Cat_Dependencias.Alta_Dependencia(); //Da de alto los datos de la dependencia en la BD
            Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
            Habilitar_Controles("Inicial");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Dependencias", "alert('El Alta de Dependencia fue Exitosa');", true);
        }
        catch (Exception ex)
        {
            throw new Exception("Alta_Dependencia " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Modificar_Dependencia
    /// DESCRIPCION : Modifica los datos de la dependencia con los proporcionados por el usuario
    ///               en la BD
    /// PARAMETROS  : 
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 23-Agosto-2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Modificar_Dependencia()
    {
        Cls_Cat_Dependencias_Negocio Obj_Cat_Dependecias = new Cls_Cat_Dependencias_Negocio();  //Variable de conexión hacia la capa de Negocios

        try
        {
            Obj_Cat_Dependecias.P_Dependencia_ID = Txt_Dependencia_ID.Text;
            Obj_Cat_Dependecias.P_Nombre = Txt_Nombre_Dependencia.Text;
            Obj_Cat_Dependecias.P_Estatus = Cmb_Estatus_Dependencia.SelectedValue;
            Obj_Cat_Dependecias.P_Comentarios = Txt_Comentarios_Dependencia.Text;
            Obj_Cat_Dependecias.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
            Obj_Cat_Dependecias.P_Area_Funcional_ID = Cmb_Area_Funcional.SelectedValue.Trim();
            Obj_Cat_Dependecias.P_Clave = Txt_Clave_Dependecia.Text.Trim();
            Obj_Cat_Dependecias.P_Grupo_Dependencia_ID = Cmb_Grupo_Dependencia.SelectedValue;

            if (Session["Dt_Fte_Financiamiento"] != null)
            {
                Obj_Cat_Dependecias.P_Dt_Fuentes_Financiamiento = (DataTable)Session["Dt_Fte_Financiamiento"];
            }
            if (Session["Dt_Fte_Financiamiento"] != null)
            {
                Session.Remove("Dt_Fte_Financiamiento");
            }

            if (Session["Dt_Programas"] != null)
            {
                Obj_Cat_Dependecias.P_Dt_Programas = (DataTable)Session["Dt_Programas"];
            }
            if (Session["Dt_Programas"] != null)
            {
                Session.Remove("Dt_Programas");
            }

            if (Session["PUESTOS_UNIDAD_RESPNSABLE"] != null)
            {
                Obj_Cat_Dependecias.P_Dt_Puestos = (DataTable)Session["PUESTOS_UNIDAD_RESPNSABLE"];
            }
            if (Session["PUESTOS_UNIDAD_RESPNSABLE"] != null)
            {
                Session.Remove("PUESTOS_UNIDAD_RESPNSABLE");
            }

            Obj_Cat_Dependecias.Modificar_Dependencia(); //Sustituye los datos de la dependencia que se encuentran en la BD por los que fueron proporcionados por el usuario
            Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones            
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Dependencias", "alert('La Modificación de la Dependencia fue Exitosa');", true);
        }
        catch (Exception ex)
        {
            throw new Exception("Modificar_Dependencia " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Eliminar_Dependencia
    /// DESCRIPCION : Elimina los datos de la dependencia que fue seleccionada por el Usuario
    /// PARAMETROS  : 
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 23-Agosto-2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Eliminar_Dependencia()
    {
        Cls_Cat_Dependencias_Negocio Rs_Eliminar_Cat_Dependencias = new Cls_Cat_Dependencias_Negocio();  //Variable de conexión hacia la capa de Negocios
        try
        {
            Rs_Eliminar_Cat_Dependencias.P_Dependencia_ID = Txt_Dependencia_ID.Text;
            Rs_Eliminar_Cat_Dependencias.Elimina_Dependencia(); //Elimina la dependencia seleccionada por el usuario de la BD

            Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Dependencias", "alert('La Eliminación de la Dependencia fue Exitosa');", true);
        }
        catch (Exception ex)
        {
            throw new Exception("Eliminar_Dependencia " + ex.Message.ToString(), ex);
        }
    }
    #endregion

    #region (Metodos Validacion)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Datos_Dependencias
    /// DESCRIPCION : Validar que se hallan proporcionado todos los datos.
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 9/Marzo/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar_Datos_Dependencia()
    {
        Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
        Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";

        if (string.IsNullOrEmpty(Txt_Nombre_Dependencia.Text.Trim()))
        {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + El Nombre de la dependencia es un dato requerido por el sistema. <br>";
            Datos_Validos = false;
        }

        if (string.IsNullOrEmpty(Txt_Clave_Dependecia.Text.Trim()))
        {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + La Clave es un dato requerido por el sistema. La clave deberá de ser de 9 carácteres alfanumericos. <br>";
            Datos_Validos = false;
        }

        else if (Txt_Clave_Dependecia.Text.Trim().Length != 9)
        {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + La clave deberá de ser de 9 carácteres alfanumericos. <br>";
            Datos_Validos = false;
        }

        if (Cmb_Estatus_Dependencia.SelectedIndex <= 0)
        {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + El Estatus es un dato requerido por el sistema. <br>";
            Datos_Validos = false;
        }

        if (Cmb_Area_Funcional.SelectedIndex <= 0)
        {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + El Área Funcional es un dato requerido por el sistema. <br>";
            Datos_Validos = false;
        }
        if (Cmb_Grupo_Dependencia.SelectedIndex <= 0)
        {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + La depenendica es un dato requerido por el sistema. <br>";
            Datos_Validos = false;
        }

        if (Grid_Fuentes_Financiamiento.Rows.Count <= 0) {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + No se ha agregado ninguna Fuente de Financiamiento a la Dependencia. <br>";
            Datos_Validos = false;
        }

        if (Grid_Programas.Rows.Count <= 0)
        {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + No se ha agregado ningún Programa a la Dependencia. <br>";
            Datos_Validos = false;
        }
        return Datos_Validos;
    }
    #endregion

    #region (Métodos Consulta)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Dependencias
    /// DESCRIPCION : Consulta las dependencias que estan dadas de alta en la BD
    /// PARAMETROS  : 
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 24-Agosto-2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consulta_Dependencias()
    {
        Cls_Cat_Dependencias_Negocio Rs_Consulta_Cat_Dependencias = new Cls_Cat_Dependencias_Negocio(); //Variable de conexión hacia la capa de Negocios
        DataTable Dt_Dependencias; //Variable que obtendra los datos de la consulta 

        try
        {
            if (Txt_Busqueda_Dependencia.Text != "")
            {
                Rs_Consulta_Cat_Dependencias.P_Nombre = Txt_Busqueda_Dependencia.Text;
            }
            Dt_Dependencias = Rs_Consulta_Cat_Dependencias.Consulta_Dependencias(); //Consulta los datos generales de las dependencias dadas de alta en la BD
            Session["Consulta_Dependencias"] = Dt_Dependencias;
            Llena_Grid_Dependencias(); //Agrega las dependencias obtenidas de la consulta anterior
        }
        catch (Exception ex)
        {
            throw new Exception("Consulta_Dependencias " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Areas_Funcionales
    /// DESCRIPCION : Consulta las áreas funcionales que estan dadas de alta en la BD
    /// PARAMETROS  : 
    /// CREO        : Juan Alberto Hernández Negrete
    /// FECHA_CREO  : 8/Marzo/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consulta_Areas_Funcionales()
    {
        Cls_Cat_Dependencias_Negocio Obj_Dependencias = new Cls_Cat_Dependencias_Negocio();//Variable de conexión con la capa de negocios.
        DataTable Dt_Areas_Funcionales = null;                                             //Variable que almacena un listado de areas funcionales registradas actualmente en el sistema.

        try
        {
            Dt_Areas_Funcionales = Obj_Dependencias.Consulta_Area_Funcional();
            Cmb_Area_Funcional.DataSource = Dt_Areas_Funcionales;
            Cmb_Area_Funcional.DataTextField = "Clave_Nombre";
            Cmb_Area_Funcional.DataValueField = Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID;
            Cmb_Area_Funcional.DataBind();

            Cmb_Area_Funcional.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
            Cmb_Area_Funcional.SelectedIndex = -1;
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al consultar las áreas funcionales registradas actualmente en el sistema. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Grupos_Dependencia
    /// DESCRIPCION : Consulta las áreas funcionales que estan dadas de alta en la BD
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 01/Diciembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consulta_Grupos_Dependencia()
    {
        Cls_Cat_Grupos_Dependencias_Negocio Obj_Dependencias = new Cls_Cat_Grupos_Dependencias_Negocio();//Variable de conexión con la capa de negocios.
        DataTable Dt_Grupos_Dependencia = null;                                             //Variable que almacena un listado de areas funcionales registradas actualmente en el sistema.

        try
        {
            Dt_Grupos_Dependencia = Obj_Dependencias.Consultar_Grupos_Dependencias();
            Cmb_Grupo_Dependencia.DataSource = Dt_Grupos_Dependencia;
            //Cmb_Grupo_Dependencia.DataTextField = "Clave_Nombre";
            Cmb_Grupo_Dependencia.DataTextField = "Nombre";
            Cmb_Grupo_Dependencia.DataValueField = Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID;
            Cmb_Grupo_Dependencia.DataBind();
            Cmb_Grupo_Dependencia.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
            Cmb_Grupo_Dependencia.SelectedIndex = -1;
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al consultar las áreas funcionales registradas actualmente en el sistema. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Fte_Financiamiento
    /// DESCRIPCION : Consulta las fuentes de financiamiento registradas en el sistema.
    /// PARAMETROS  : 
    /// CREO        : Juan Alberto Hernández Negrete
    /// FECHA_CREO  : 8/Marzo/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consulta_Fte_Financiamiento()
    {
        Cls_Cat_SAP_Fuente_Financiamiento_Negocio Obj_Fte_Financiamiento = new Cls_Cat_SAP_Fuente_Financiamiento_Negocio();//Variable de conexión con la capa de negocios.
        DataTable Dt_Fte_Financiamiento = null;//Variable que almacenara los resultados de la busqueda realizada.

        try
        {
            Dt_Fte_Financiamiento = Obj_Fte_Financiamiento.Consulta_Datos_Fuente_Financiamiento();
            Cmb_Fuente_Financiamiento.DataSource = Dt_Fte_Financiamiento;
            Cmb_Fuente_Financiamiento.DataTextField = Cat_SAP_Fuente_Financiamiento.Campo_Descripcion;
            Cmb_Fuente_Financiamiento.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
            Cmb_Fuente_Financiamiento.DataBind();

            Cmb_Fuente_Financiamiento.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
            Cmb_Fuente_Financiamiento.SelectedIndex = -1;

        }
        catch (Exception Ex)
        {
            throw new Exception("Error al consultar las fuentes de financiamento registradas actualmente en el sistema. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Areas_Funcionales
    /// DESCRIPCION : Consulta los programas registrados en el sistema.
    /// PARAMETROS  : 
    /// CREO        : Juan Alberto Hernández Negrete
    /// FECHA_CREO  : 8/Marzo/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consulta_Programas()
    {
        Cls_Cat_Com_Proyectos_Programas_Negocio Obj_Programas = new Cls_Cat_Com_Proyectos_Programas_Negocio();//Variable de conexion con la capa de negocios
        DataTable Dt_Programas = null;//Variable que alamacenara los resultados obtenidos de la busqueda realizada.

        try
        {
            Dt_Programas = Obj_Programas.Consulta_Programas_Proyectos();
            Cmb_Programa.DataSource = Dt_Programas;
            Cmb_Programa.DataTextField = Cat_Com_Proyectos_Programas.Campo_Nombre;
            Cmb_Programa.DataValueField = Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID;
            Cmb_Programa.DataBind();

            Cmb_Programa.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
            Cmb_Programa.SelectedIndex = -1;
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al consultar los programas registrados actualmente en el sistema. Error: [" + Ex.Message + "]");
        }
    }
    ///*************************************************************************************
    /// NOMBRE DE LA FUNCION: Es_Clave_Repetida
    /// DESCRIPCION : Consulta las dependencias por clave y si encuentra alguna dependecia
    ///               con esta clave valida que no se trate de una modificacion si es asi
    ///               se permitira que la dependencia mantega la misma clave al modificarse.
    /// PARAMETROS  : 
    /// CREO        : Juan Alberto Hernández Negrete
    /// FECHA_CREO  : 8/Marzo/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*************************************************************************************
    private Boolean Es_Clave_Repetida(String Clave_Ingresada)
    {
        Cls_Cat_Dependencias_Negocio Obj_Dependencias = new Cls_Cat_Dependencias_Negocio();//Variable de conexion a la capa de negocios.
        DataTable Dt_Dependencias = null;   //Variable que almacenara la lista de resultados obtenidos de la búsqueda realizada.
        String Dependecia_ID = "";          //Identificador unico de la dependencia para uso interno del sistema.
        Boolean Permitir = true;

        try
        {
            if (Clave_Ingresada.Trim().Length == 9)
            {
                Obj_Dependencias.P_Clave = Clave_Ingresada;
                Dt_Dependencias = Obj_Dependencias.Consulta_Dependencias();

                if (Dt_Dependencias is DataTable)
                {
                    if (Dt_Dependencias.Rows.Count == 1)
                    {
                        foreach (DataRow Dependencia in Dt_Dependencias.Rows)
                        {
                            if (Dependencia is DataRow)
                            {
                                if (!string.IsNullOrEmpty(Dependencia[Cat_Dependencias.Campo_Dependencia_ID].ToString()))
                                {
                                    Dependecia_ID = Dependencia[Cat_Dependencias.Campo_Dependencia_ID].ToString();

                                    if (Dependecia_ID.Trim().Equals(Txt_Dependencia_ID.Text.Trim()))
                                    {
                                        Permitir = true;
                                    }
                                    else
                                    {
                                        Permitir = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else {
                Permitir = false;
            }
        }   
        catch (Exception Ex)
        {
            throw new Exception("Error al consultar si la nueva clave ingresada ya le pertence alguna dependencia actualmente. Error: [" + Ex.Message + "]");
        }
        return Permitir;
    }
    #endregion

    #region (Métodos Grids)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Llena_Grid_Dependencias
    /// DESCRIPCION : Llena el grid con las dependencias que se encuentran en la base de datos
    /// PARAMETROS  : 
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 23-Agosto-2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Llena_Grid_Dependencias()
    {
        DataTable Dt_Dependencias; //Variable que obtendra los datos de la consulta 
        try
        {
            Grid_Dependencias.DataBind();
            Dt_Dependencias = (DataTable)Session["Consulta_Dependencias"];
            Grid_Dependencias.DataSource = Dt_Dependencias;
            Grid_Dependencias.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception("Llena_Grid_Dependencias " + ex.Message.ToString(), ex);
        }
    }
    #endregion

    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// 
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// 
    /// PARÁMETROS: No Áplica.
    /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
    /// FECHA CREÓ: 23/Mayo/2011 10:43 a.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            Botones.Add(Btn_Nuevo);
            Botones.Add(Btn_Modificar);
            Botones.Add(Btn_Eliminar);
            Botones.Add(Btn_Buscar_Dependencia);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 29/Noviembre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion

    #region (Puestos)
    protected void Consultar_Puestos() {
        Cls_Cat_Puestos_Negocio Obj_Puestos = new Cls_Cat_Puestos_Negocio();//Variable de conexion a al capa de negocios.
        DataTable Dt_Puestos = null;//Variable que almacenara una lista de puestos.

        try
        {
            Dt_Puestos = Obj_Puestos.Consultar_Puestos();
            Cmb_Puestos.DataSource = Dt_Puestos;
            Cmb_Puestos.DataTextField = Cat_Puestos.Campo_Nombre;
            Cmb_Puestos.DataValueField = Cat_Puestos.Campo_Puesto_ID;
            Cmb_Puestos.DataBind();
            Cmb_Puestos.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
            Cmb_Puestos.SelectedIndex = -1;
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al consultar los puestos registrados en sistema. Error: [" + Ex.Message + "]");
        }
    }

    protected void Consultar_Puestos_Unidad_Responsable(String Unidad_Responsable_ID)
    {
        Cls_Cat_Puestos_Negocio Obj_Puestos = new Cls_Cat_Puestos_Negocio();
        DataTable Dt_Puestos_Unidad_Rersponsable = null;

        try
        {
            Obj_Puestos.P_Dependencia_ID = Unidad_Responsable_ID;
            Dt_Puestos_Unidad_Rersponsable = Obj_Puestos.Consultar_Puestos_Disponibles_Dependencia();
            Session["PUESTOS_UNIDAD_RESPNSABLE"] = Dt_Puestos_Unidad_Rersponsable;
            Grid_Puestos.DataSource = (DataTable)Session["PUESTOS_UNIDAD_RESPNSABLE"];
            Grid_Puestos.DataBind();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al consultar los puestos de la unidad responsable registrados en sistema. Error: [" + Ex.Message + "]");
        }
    }

    protected void Agregar_Puesto_Unidad_Responsable()
    {
        ListItem Puesto_Seleccionado = null;

        try
        {
            Puesto_Seleccionado = Cmb_Puestos.SelectedItem;

            if (Session["PUESTOS_UNIDAD_RESPNSABLE"] != null)
            {

                if (!Validar_No_Repetir_Puestos((DataTable)Session["PUESTOS_UNIDAD_RESPNSABLE"], Puesto_Seleccionado))
                    Agregar_Puesto((DataTable)Session["PUESTOS_UNIDAD_RESPNSABLE"], Puesto_Seleccionado);
            }
            else
            {
                DataTable Dt_Puestos = new DataTable();
                Dt_Puestos.Columns.Add(Cat_Puestos.Campo_Puesto_ID, typeof(String));
                Dt_Puestos.Columns.Add(Cat_Puestos.Campo_Nombre, typeof(String));
                Dt_Puestos.Columns.Add(Cat_Puestos.Campo_Salario_Mensual, typeof(String));
                Dt_Puestos.Columns.Add("ESTATUS_PUESTO", typeof(String));
                Dt_Puestos.Columns.Add(Cat_Nom_Dep_Puestos_Det.Campo_Clave, typeof(String));

                if (!Validar_No_Repetir_Puestos(Dt_Puestos, Puesto_Seleccionado))
                    Agregar_Puesto(Dt_Puestos, Puesto_Seleccionado);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al agregar un puesto a la unidad responsable. Error: [" + Ex.Message + "]");
        }
    }

    protected void Agregar_Puesto(DataTable Dt_Puestos, ListItem Puesto_Agregar)
    {
        Cls_Cat_Puestos_Negocio Obj_Puestos = new Cls_Cat_Puestos_Negocio();
        DataTable Dt_Puestos_Consultado = null;
        DataRow Renglon = null;

        try
        {
            Obj_Puestos.P_Puesto_ID = Puesto_Agregar.Value;
            Dt_Puestos_Consultado = Obj_Puestos.Consultar_Puestos();

            if (Dt_Puestos_Consultado is DataTable)
            {
                if (Dt_Puestos_Consultado.Rows.Count > 0)
                {
                    foreach (DataRow PUESTO in Dt_Puestos_Consultado.Rows)
                    {
                        if (PUESTO is DataRow)
                        {
                            Renglon = Dt_Puestos.NewRow();
                            Renglon[Cat_Puestos.Campo_Puesto_ID] = PUESTO[Cat_Puestos.Campo_Puesto_ID].ToString().Trim();
                            Renglon[Cat_Puestos.Campo_Nombre] = PUESTO[Cat_Puestos.Campo_Nombre].ToString().Trim();
                            Renglon[Cat_Puestos.Campo_Salario_Mensual] = PUESTO[Cat_Puestos.Campo_Salario_Mensual].ToString().Trim();
                            Renglon["ESTATUS_PUESTO"] = "DISPONIBLE";
                            Renglon[Cat_Nom_Dep_Puestos_Det.Campo_Clave] = Consultar_Consecutivo(Dt_Puestos);
                            Dt_Puestos.Rows.Add(Renglon);
                        }
                    }
                }
            }
            Session["PUESTOS_UNIDAD_RESPNSABLE"] = Dt_Puestos;
            Grid_Puestos.DataSource = (DataTable)Session["PUESTOS_UNIDAD_RESPNSABLE"];
            Grid_Puestos.DataBind();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al agregar un puesto a la unidad responsable. Error: [" + Ex.Message + "]");
        }
    }

    protected void Eliminar_Puesto(DataTable Dt_Puestos, String Puestos_ID)
    {
        try
        {
            DataRow[] Dr_Puestos = Dt_Puestos.Select(Cat_Puestos.Campo_Puesto_ID + "=" + Puestos_ID);
            DataRow Dr_Puesto = null;

            if (Dr_Puestos.Length > 0)
            {
                Dr_Puesto = Dr_Puestos[0];
                Dt_Puestos.Rows.Remove(Dr_Puesto);
                Session["PUESTOS_UNIDAD_RESPNSABLE"] = Dt_Puestos;
                Grid_Puestos.DataSource = (DataTable)Session["PUESTOS_UNIDAD_RESPNSABLE"];
                Grid_Puestos.DataBind();
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al agregar un puesto a la unidad responsable. Error: [" + Ex.Message + "]");
        }
    }

    protected Boolean Validar_No_Repetir_Puestos(DataTable Dt_Puestos, ListItem Puesto_Validar)
    {
        DataRow[] Dr_Puestos = null;
        Boolean Es_Repetido = true;

        try
        {
            if (Dt_Puestos is DataTable)
            {
                if (Dt_Puestos.Rows.Count > 0)
                {
                    Dr_Puestos = Dt_Puestos.Select(Cat_Puestos.Campo_Puesto_ID + "=" + Puesto_Validar.Value);

                    if (Dr_Puestos.Length <= 0)
                    {
                        Es_Repetido = false;
                    }
                    else
                    {
                        //Si se encontro algun coincidencia entre el grupo a agregar con alguno agregado anteriormente, se avisa
                        //al usuario que elemento ha agregar ya existe en la tabla de grupos.
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "",
                            "alert('No se puede agregar el puesto, ya que esta ya se ha agregado');", true);
                        Cmb_Puestos.SelectedIndex = -1;
                    }
                }
                else Es_Repetido = false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("");
        }
        return Es_Repetido;
    }
    #endregion

    #endregion

    #region (Grid)

    #region (Grid Dependencias)
    ///************************************************************************************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Dependencias_SelectedIndexChanged
    /// 
    /// DESCRIPCION : Consulta los datos de la dependencia que selecciono el usuario
    /// 
    /// CREO        : Juan Alberto Hernández Negrete
    /// FECHA_CREO  : 08/Marzo/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///************************************************************************************************************************************************
    protected void Grid_Dependencias_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Cat_Dependencias_Negocio Rs_Consulta_Cat_Dependencias = new Cls_Cat_Dependencias_Negocio(); //Variable de conexión hacia la capa de Negocios
        DataTable Dt_Dependencias; //Variable que obtendra los datos de la consulta 

        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Rs_Consulta_Cat_Dependencias.P_Dependencia_ID = Grid_Dependencias.SelectedRow.Cells[1].Text;
            Dt_Dependencias = Rs_Consulta_Cat_Dependencias.Consulta_Dependencias(); //Consulta todos los datos de la dependencia que fue seleccionada por el usuario
            if (Dt_Dependencias.Rows.Count > 0)
            {
                //Asigna los valores de los campos obtenidos de la consulta anterior a los controles de la forma
                foreach (DataRow Registro in Dt_Dependencias.Rows)
                {
                    if (!String.IsNullOrEmpty(Registro[Cat_Dependencias.Campo_Dependencia_ID].ToString()))
                    {
                        Txt_Dependencia_ID.Text = Registro[Cat_Dependencias.Campo_Dependencia_ID].ToString();

                        LLenar_Grid_Fte_Financiamiento(0, Rs_Consulta_Cat_Dependencias.Consultar_Sap_Det_Fte_Dependencia());
                        LLenar_Grid_Programas(0, Rs_Consulta_Cat_Dependencias.Consultar_Sap_Det_Prog_Dependencia());
                        Consultar_Puestos_Unidad_Responsable(Grid_Dependencias.SelectedRow.Cells[1].Text.Trim());
                    }

                    if (!String.IsNullOrEmpty(Registro[Cat_Dependencias.Campo_Nombre].ToString()))
                        Txt_Nombre_Dependencia.Text = Registro[Cat_Dependencias.Campo_Nombre].ToString();

                    if (!String.IsNullOrEmpty(Registro[Cat_Dependencias.Campo_Comentarios].ToString()))
                        Txt_Comentarios_Dependencia.Text = Registro[Cat_Dependencias.Campo_Comentarios].ToString();

                    if (!String.IsNullOrEmpty(Registro[Cat_Dependencias.Campo_Estatus].ToString()))
                        Cmb_Estatus_Dependencia.SelectedValue = Registro[Cat_Dependencias.Campo_Estatus].ToString();

                    if (!String.IsNullOrEmpty(Registro[Cat_Dependencias.Campo_Area_Funcional_ID].ToString()))
                        Cmb_Area_Funcional.SelectedIndex = Cmb_Area_Funcional.Items.IndexOf(Cmb_Area_Funcional.Items.FindByValue(Registro[Cat_Dependencias.Campo_Area_Funcional_ID].ToString()));

                    if (!String.IsNullOrEmpty(Registro[Cat_Dependencias.Campo_Clave].ToString()))
                        Txt_Clave_Dependecia.Text = Registro[Cat_Dependencias.Campo_Clave].ToString();

                    if (!String.IsNullOrEmpty(Registro[Cat_Dependencias.Campo_Grupo_Dependencia_ID].ToString()))
                        Cmb_Grupo_Dependencia.SelectedIndex = Cmb_Grupo_Dependencia.Items.IndexOf(Cmb_Grupo_Dependencia.Items.FindByValue(Registro[Cat_Dependencias.Campo_Grupo_Dependencia_ID].ToString()));
                    else
                        Cmb_Grupo_Dependencia.SelectedIndex = 0;
                }
            }
            ScriptManager.RegisterStartupScript(Upd_Panel, typeof(string), "Imagen", "javascript:Inicializar_Eventos_Cat_Dependencias();", true);
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///************************************************************************************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Dependencias_PageIndexChanging
    /// 
    /// DESCRIPCION : Consulta y cambia la página del Grid. 
    /// : 
    /// CREO        : Juan Alberto Hernández Negrete
    /// FECHA_CREO  : 08/Marzo/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///************************************************************************************************************************************************
    protected void Grid_Dependencias_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Limpia_Controles(); //Limpia los controles de la forma
            Grid_Dependencias.PageIndex = e.NewPageIndex; //Asigna la nueva página que selecciono el usuario
            Llena_Grid_Dependencias(); //Muestra las dependencias que estan asignadas en la página seleccionada por el usuario
            Grid_Dependencias.SelectedIndex = -1;
            ScriptManager.RegisterStartupScript(Upd_Panel, typeof(string), "Imagen", "javascript:Inicializar_Eventos_Cat_Dependencias();", true);
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///************************************************************************************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Dependencias_Sorting
    /// 
    /// DESCRIPCION : Reordena la columna del grid, ya sea de forma DESC o ASC
    /// : 
    /// CREO        : Juan Alberto Hernández Negrete
    /// FECHA_CREO  : 08/Marzo/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///************************************************************************************************************************************************
    protected void Grid_Dependencias_Sorting(object sender, GridViewSortEventArgs e)
    {
        //Se consultan las dependencias que actualmente se encuentran registradas en el sistema.
        Consulta_Dependencias();

        DataTable Dt_Dependencias = (Grid_Dependencias.DataSource as DataTable);

        if (Dt_Dependencias != null)
        {
            DataView Dv_Dependencias = new DataView(Dt_Dependencias);
            String Orden = ViewState["SortDirection"].ToString();

            if (Orden.Equals("ASC"))
            {
                Dv_Dependencias.Sort = e.SortExpression + " " + "DESC";
                ViewState["SortDirection"] = "DESC";
            }
            else
            {
                Dv_Dependencias.Sort = e.SortExpression + " " + "ASC";
                ViewState["SortDirection"] = "ASC";
            }

            Grid_Dependencias.DataSource = Dv_Dependencias;
            Grid_Dependencias.DataBind();
        }
        ScriptManager.RegisterStartupScript(Upd_Panel, typeof(string), "Imagen", "javascript:Inicializar_Eventos_Cat_Dependencias();", true);
    }
    #endregion

    #region (Grid Fuentes Financiamiento)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Agregar_Fuente_Financiamiento
    /// DESCRIPCION : Agrega una Fuente Financiamiento
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 8/Marzo/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Agregar_Fuente_Financiamiento(DataTable _DataTable, GridView _GridView, DropDownList _DropDownList)
    {
        DataRow[] Filas;//Variable que almacenara un arreglo de DataRows
        DataTable Dt_Fuente_Financiamiento = (DataTable)Session["Dt_Fte_Financiamiento"];//Variable que almacenara una lista de empleados.
        Cls_Cat_SAP_Fuente_Financiamiento_Negocio Obj_Fuente_Financiamiento = new Cls_Cat_SAP_Fuente_Financiamiento_Negocio();//Variable de conexion con la capa de negocios

        try
        {
            int indice = _DropDownList.SelectedIndex;
            if (indice > 0)
            {
                Filas = _DataTable.Select(Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + "='" + _DropDownList.SelectedValue.Trim() + "'");
                if (Filas.Length > 0)
                {
                    //Si se encontro algun coincidencia entre el grupo a agregar con alguno agregado anteriormente, se avisa
                    //al usuario que elemento ha agregar ya existe en la tabla de grupos.
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "",
                        "alert('No se puede agregar la fuente de financiamiento, ya que esta ya se ha agregada');", true);
                    _DropDownList.SelectedIndex = -1;
                }
                else
                {
                    Obj_Fuente_Financiamiento.P_Fuente_Financiamiento_ID = _DropDownList.SelectedValue.Trim();
                    DataTable Dt_Temporal = Obj_Fuente_Financiamiento.Consulta_Datos_Fuente_Financiamiento();
                    if (!(Dt_Temporal == null))
                    {
                        if (Dt_Temporal.Rows.Count > 0)
                        {
                            DataRow Renglon = Dt_Fuente_Financiamiento.NewRow();
                            Renglon[Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID] = Dt_Temporal.Rows[0][0].ToString();
                            Renglon[Cat_SAP_Fuente_Financiamiento.Campo_Clave] = Dt_Temporal.Rows[0][1].ToString();
                            Renglon[Cat_SAP_Fuente_Financiamiento.Campo_Descripcion] = Dt_Temporal.Rows[0][2].ToString();

                            Dt_Fuente_Financiamiento.Rows.Add(Renglon);
                            Dt_Fuente_Financiamiento.AcceptChanges();
                            Session["Dt_Fte_Financiamiento"] = Dt_Fuente_Financiamiento;
                            _GridView.Columns[0].Visible = true;
                            _GridView.DataSource = (DataTable)Session["Dt_Fte_Financiamiento"];
                            _GridView.DataBind();
                            _GridView.Columns[0].Visible = false;
                            _DropDownList.SelectedIndex = -1;
                        }
                    }
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "",
                    "alert('No se a seleccionado ninguna fuente de financiamiento a agregar');", true);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al agregar las fuentes de financiamiento. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Agregar_Fte_Financiamiento_Click
    /// 
    /// DESCRIPCION : Evento que genera la peticion para agregar una nueva fuente de financiamiento del
    ///               combo de fuentes de financiamiento a la tabla de fuentes de financiamiento.
    ///               
    /// CREO        : Juan Alberto Hernández Negrete
    /// FECHA_CREO  : 8/Marzo/201a
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Agregar_Fte_Financiamiento_Click(object sender, EventArgs e)
    {
        if (Cmb_Fuente_Financiamiento.SelectedIndex > 0)
        {
            if (Session["Dt_Fte_Financiamiento"] != null)
            {
                Agregar_Fuente_Financiamiento((DataTable)Session["Dt_Fte_Financiamiento"], Grid_Fuentes_Financiamiento, Cmb_Fuente_Financiamiento);
            }
            else
            {
                DataTable Dt_Fuentes_Financiamiento = new DataTable();//Variable que almacenara una lista de empleados
                //Definicion de sus columnas.
                Dt_Fuentes_Financiamiento.Columns.Add(Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID, typeof(System.String));
                Dt_Fuentes_Financiamiento.Columns.Add(Cat_SAP_Fuente_Financiamiento.Campo_Clave, typeof(System.String));
                Dt_Fuentes_Financiamiento.Columns.Add(Cat_SAP_Fuente_Financiamiento.Campo_Descripcion, typeof(System.String));

                Session["Dt_Fte_Financiamiento"] = Dt_Fuentes_Financiamiento;
                Grid_Fuentes_Financiamiento.DataSource = (DataTable)Session["Dt_Fte_Financiamiento"];
                Grid_Fuentes_Financiamiento.DataBind();

                Agregar_Fuente_Financiamiento(Dt_Fuentes_Financiamiento, Grid_Fuentes_Financiamiento, Cmb_Fuente_Financiamiento);
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "",
                "alert('No se a seleccionado ninguna percepcion a agregar');", true);
        }
        ScriptManager.RegisterStartupScript(Upd_Panel, typeof(string), "Imagen", "javascript:Inicializar_Eventos_Cat_Dependencias();", true);
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Eliminar_Fte_Financiamiento_Click
    /// 
    /// DESCRIPCION : Evento que genera la peticion para Quitar el la fuente de financiamiento 
    ///               seleccionado de la tabla de fuentes de financiamiento.
    ///               
    /// CREO        : Juan Alberto Hernández Negrete
    /// FECHA_CREO  : 8/Marzo/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Eliminar_Fte_Financiamiento_Click(object sender, EventArgs e)
    {
        DataRow[] Renglones;//Variable que almacena una lista de DataRows del  Grid_Empleados
        DataRow Renglon;//Variable que almacenara un Renglon del Grid_Empleados
        ImageButton Btn_Eliminar_Fte_Financiamiento = (ImageButton)sender;//Variable que almacenra el control Btn_Eliminar_Empleado

        if (Session["Dt_Fte_Financiamiento"] != null)
        {
            Renglones = ((DataTable)Session["Dt_Fte_Financiamiento"]).Select(Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + "='" + Btn_Eliminar_Fte_Financiamiento.CommandArgument + "'");

            if (Renglones.Length > 0)
            {
                Renglon = Renglones[0];
                DataTable Tabla = (DataTable)Session["Dt_Fte_Financiamiento"];
                Tabla.Rows.Remove(Renglon);
                Session["Dt_Fte_Financiamiento"] = Tabla;
                Grid_Fuentes_Financiamiento.SelectedIndex = (-1);
                LLenar_Grid_Fte_Financiamiento(Grid_Fuentes_Financiamiento.PageIndex, Tabla);
            }
        }
        else
        {
            Lbl_Mensaje_Error.Text = "Se debe seleccionar de la tabla el Empleados a quitar";
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
        }
        ScriptManager.RegisterStartupScript(Upd_Panel, typeof(string), "Imagen", "javascript:Inicializar_Eventos_Cat_Dependencias();", true);
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: LLenar_Grid_Fte_Financiamiento
    ///
    ///DESCRIPCIÓN: LLena el grid de fuentes de financiamiento
    ///
    ///CREO: Juan alberto Hernández Negrete
    ///FECHA_CREO: 8/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void LLenar_Grid_Fte_Financiamiento(Int32 Pagina, DataTable Tabla)
    {
        Grid_Fuentes_Financiamiento.Columns[0].Visible = true;
        Grid_Fuentes_Financiamiento.SelectedIndex = (-1);
        Grid_Fuentes_Financiamiento.DataSource = Tabla;
        Grid_Fuentes_Financiamiento.PageIndex = Pagina;
        Grid_Fuentes_Financiamiento.DataBind();
        Grid_Fuentes_Financiamiento.Columns[0].Visible = false;
        Session["Dt_Fte_Financiamiento"] = Tabla;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Fuentes_Financiamiento_PageIndexChanging
    ///DESCRIPCIÓN: Realiza el Cambio de la pagina de la tabla.
    ///CREO: Juan Alberto Hernández Negrete
    ///FECHA_CREO: 8/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Fuentes_Financiamiento_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            if (Session["Dt_Fte_Financiamiento"] != null)
            {
                LLenar_Grid_Fte_Financiamiento(e.NewPageIndex, (DataTable)Session["Dt_Fte_Financiamiento"]);
            }
            ScriptManager.RegisterStartupScript(Upd_Panel, typeof(string), "Imagen", "javascript:Inicializar_Eventos_Cat_Dependencias();", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error cambiar de un de la tabla. Error: [" + Ex.Message + "]");
        }
    }
    #endregion

    #region (Grid Programas)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Agregar_Programas
    /// DESCRIPCION : Agrega un Programa
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 8/Marzo/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Agregar_Programas(DataTable _DataTable, GridView _GridView, DropDownList _DropDownList)
    {
        DataRow[] Filas;//Variable que almacenara un arreglo de DataRows
        DataTable Dt_Programas = (DataTable)Session["Dt_Programas"];//Variable que almacenara una lista de empleados.
        Cls_Cat_Com_Proyectos_Programas_Negocio Obj_Programas = new Cls_Cat_Com_Proyectos_Programas_Negocio();//Variable de conexion con la capa de negocios

        try
        {
            int indice = _DropDownList.SelectedIndex;
            if (indice > 0)
            {
                Filas = _DataTable.Select(Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID + "='" + _DropDownList.SelectedValue.Trim() + "'");
                if (Filas.Length > 0)
                {
                    //Si se encontro algun coincidencia entre el grupo a agregar con alguno agregado anteriormente, se avisa
                    //al usuario que elemento ha agregar ya existe en la tabla de grupos.
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "",
                        "alert('No se puede agregar el Programa, ya que este ya se ha agregado');", true);
                    _DropDownList.SelectedIndex = -1;
                }
                else
                {
                    Obj_Programas.P_Proyecto_Programa_ID = _DropDownList.SelectedValue.Trim();
                    DataTable Dt_Temporal = Obj_Programas.Consulta_Programas_Proyectos();
                    if (!(Dt_Temporal == null))
                    {
                        if (Dt_Temporal.Rows.Count > 0)
                        {
                            DataRow Renglon = Dt_Programas.NewRow();
                            Renglon[Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID] = Dt_Temporal.Rows[0][0].ToString();
                            Renglon[Cat_SAP_Fuente_Financiamiento.Campo_Clave] = Dt_Temporal.Rows[0][5].ToString();
                            Renglon[Cat_SAP_Fuente_Financiamiento.Campo_Descripcion] = Dt_Temporal.Rows[0][3].ToString();

                            Dt_Programas.Rows.Add(Renglon);
                            Dt_Programas.AcceptChanges();
                            Session["Dt_Programas"] = Dt_Programas;
                            _GridView.Columns[0].Visible = true;
                            _GridView.DataSource = (DataTable)Session["Dt_Programas"];
                            _GridView.DataBind();
                            _GridView.Columns[0].Visible = false;
                            _DropDownList.SelectedIndex = -1;
                        }
                    }
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "",
                    "alert('No se a seleccionado ningun programa a agregar');", true);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al agregar las fuentes de financiamiento. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Agregar_Programa_Click
    /// 
    /// DESCRIPCION : Evento que genera la peticion para agregar un nuevo programa del
    ///               combo de programas a la tabla de programas.
    ///               
    /// CREO        : Juan Alberto Hernández Negrete
    /// FECHA_CREO  : 8/Marzo/201a
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Agregar_Programa_Click(object sender, EventArgs e)
    {
        if (Cmb_Programa.SelectedIndex > 0)
        {
            if (Session["Dt_Programas"] != null)
            {
                Agregar_Programas((DataTable)Session["Dt_Programas"], Grid_Programas, Cmb_Programa);
            }
            else
            {
                DataTable Dt_Programas = new DataTable();//Variable que almacenara una lista de empleados
                //Definicion de sus columnas.
                Dt_Programas.Columns.Add(Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID, typeof(System.String));
                Dt_Programas.Columns.Add(Cat_Com_Proyectos_Programas.Campo_Clave, typeof(System.String));
                Dt_Programas.Columns.Add(Cat_Com_Proyectos_Programas.Campo_Descripcion, typeof(System.String));

                Session["Dt_Programas"] = Dt_Programas;
                Grid_Programas.DataSource = (DataTable)Session["Dt_Programas"];
                Grid_Programas.DataBind();

                Agregar_Programas(Dt_Programas, Grid_Programas, Cmb_Programa);
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "",
                "alert('No se a seleccionado ningun programa a agregar');", true);
        }
        ScriptManager.RegisterStartupScript(Upd_Panel, typeof(string), "Imagen", "javascript:Inicializar_Eventos_Cat_Dependencias();", true);
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Eliminar_Programa_Click
    /// 
    /// DESCRIPCION : Evento que genera la peticion para Quitar el programa
    ///               seleccionado de la tabla de programas.
    ///               
    /// CREO        : Juan Alberto Hernández Negrete
    /// FECHA_CREO  : 8/Marzo/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Eliminar_Programa_Click(object sender, EventArgs e)
    {
        DataRow[] Renglones;//Variable que almacena una lista de DataRows del  Grid_Empleados
        DataRow Renglon;//Variable que almacenara un Renglon del Grid_Empleados
        ImageButton Btn_Eliminar_Programa = (ImageButton)sender;//Variable que almacenra el control Btn_Eliminar_Empleado

        if (Session["Dt_Programas"] != null)
        {
            Renglones = ((DataTable)Session["Dt_Programas"]).Select(Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID + "='" + Btn_Eliminar_Programa.CommandArgument + "'");

            if (Renglones.Length > 0)
            {
                Renglon = Renglones[0];
                DataTable Tabla = (DataTable)Session["Dt_Programas"];
                Tabla.Rows.Remove(Renglon);
                Session["Dt_Programas"] = Tabla;
                Grid_Programas.SelectedIndex = (-1);
                LLenar_Grid_Programas(Grid_Fuentes_Financiamiento.PageIndex, Tabla);
            }
        }
        else
        {
            Lbl_Mensaje_Error.Text = "Se debe seleccionar de la tabla el Empleados a quitar";
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
        }
        ScriptManager.RegisterStartupScript(Upd_Panel, typeof(string), "Imagen", "javascript:Inicializar_Eventos_Cat_Dependencias();", true);
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: LLenar_Grid_Programas
    ///
    ///DESCRIPCIÓN: LLena el grid de programas
    ///
    ///CREO: Juan alberto Hernández Negrete
    ///FECHA_CREO: 8/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void LLenar_Grid_Programas(Int32 Pagina, DataTable Tabla)
    {
        Grid_Programas.Columns[0].Visible = true;
        Grid_Programas.SelectedIndex = (-1);
        Grid_Programas.DataSource = Tabla;
        Grid_Programas.PageIndex = Pagina;
        Grid_Programas.DataBind();
        Grid_Programas.Columns[0].Visible = false;
        Session["Dt_Programas"] = Tabla;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Programas_PageIndexChanging
    ///DESCRIPCIÓN: Realiza el Cambio de la pagina de la tabla.
    ///CREO: Juan Alberto Hernández Negrete
    ///FECHA_CREO: 8/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Programas_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            if (Session["Dt_Programas"] != null)
            {
                LLenar_Grid_Programas(e.NewPageIndex, (DataTable)Session["Dt_Programas"]);
            }
            ScriptManager.RegisterStartupScript(Upd_Panel, typeof(string), "Imagen", "javascript:Inicializar_Eventos_Cat_Dependencias();", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error cambiar de un de la tabla. Error: [" + Ex.Message + "]");
        }
    }
    #endregion

    #endregion

    #region (Eventos)

    #region (Eventos Operación)
    ///************************************************************************************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Nuevo_Click
    /// 
    /// DESCRIPCION : Alta de un nuevo registro de dependencia.
    /// 
    /// CREO        : Juan Alberto Hernández Negrete
    /// FECHA_CREO  : 08/Marzo/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///************************************************************************************************************************************************
    protected void Btn_Nuevo_Click(object sender, EventArgs e)
    {
        try
        {
            if (Btn_Nuevo.ToolTip == "Nuevo")
            {
                Limpia_Controles(); //Limpia los controles de la forma para poder introducir nuevos datos
                Habilitar_Controles("Nuevo"); //Habilita los controles para la introducción de datos por parte del usuario
            }
            else
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                //Valida si todos los campos requeridos estan llenos si es así da de alta los datos en la base de datos
                if (Validar_Datos_Dependencia())
                {
                    if (!Es_Clave_Repetida(Txt_Clave_Dependecia.Text.Trim()))
                    {
                        Txt_Clave_Dependecia.Text = "";

                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "La clave ya existe en el sistema.";
                    }
                    else
                    {
                        Alta_Dependencia(); //Da de alta la Dependencia con los datos que proporciono el usuario
                    }
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
            ScriptManager.RegisterStartupScript(Upd_Panel, typeof(string), "Imagen", "javascript:Inicializar_Eventos_Cat_Dependencias();", true);
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///************************************************************************************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Modificar_Click
    /// 
    /// DESCRIPCION : Modifica el registro de dependencia seleccionado.
    /// 
    /// CREO        : Juan Alberto Hernández Negrete
    /// FECHA_CREO  : 08/Marzo/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///************************************************************************************************************************************************
    protected void Btn_Modificar_Click(object sender, EventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (Btn_Modificar.ToolTip == "Modificar")
            {
                //Si el usuario selecciono una dependencia entonces habilita los controles para que pueda modificar la información
                //de la dependencia
                if (Txt_Dependencia_ID.Text != "")
                {
                    Habilitar_Controles("Modificar"); //Habilita los controles para la modificación de los datos
                }
                //Si el usuario no selecciono una dependencia le indica al usuario que la seleccione para poder modificar
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Seleccione la Dependencia que desea modificar sus datos <br>";
                }
            }
            else
            {
                //Si el usuario proporciono todos los datos requeridos entonces modificar los datos de la dependencia en la BD
                if (Validar_Datos_Dependencia())
                {
                    if (!Es_Clave_Repetida(Txt_Clave_Dependecia.Text.Trim()))
                    {
                        Txt_Clave_Dependecia.Text = "";

                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "La clave ya existe en el sistema.";
                    }
                    else
                    {
                        Modificar_Dependencia(); //Modifica los datos de la Dependencia con los datos proporcionados por el usuario
                    }
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
            ScriptManager.RegisterStartupScript(Upd_Panel, typeof(string), "Imagen", "javascript:Inicializar_Eventos_Cat_Dependencias();", true);
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///************************************************************************************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Eliminar_Click
    /// 
    /// DESCRIPCION : Elimina el registro de dependencia seleccionado.
    /// 
    /// CREO        : Juan Alberto Hernández Negrete
    /// FECHA_CREO  : 08/Marzo/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///************************************************************************************************************************************************
    protected void Btn_Eliminar_Click(object sender, EventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            //Si el usuario selecciono una dependencia entonces la elimina de la base de datos
            if (Txt_Dependencia_ID.Text != "")
            {
                Eliminar_Dependencia(); //Elimina la Dependencia que fue seleccionada por el usuario
            }
            //Si el usuario no selecciono alguna dependencia manda un mensaje indicando que es necesario que seleccione alguna para
            //poder eliminar
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Seleccione la Dependencia que desea eliminar <br>";
            }
            ScriptManager.RegisterStartupScript(Upd_Panel, typeof(string), "Imagen", "javascript:Inicializar_Eventos_Cat_Dependencias();", true);
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///************************************************************************************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Salir_Click
    /// 
    /// DESCRIPCION : Cancela y sale de la operación actual.
    /// 
    /// CREO        : Juan Alberto Hernández Negrete
    /// FECHA_CREO  : 08/Marzo/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///************************************************************************************************************************************************
    protected void Btn_Salir_Click(object sender, EventArgs e)
    {
        try
        {
            if (Btn_Salir.ToolTip == "Salir")
            {
                Session.Remove("Consulta_Dependencias");
                Session.Remove("Dt_Fte_Financiamiento");
                Session.Remove("Dt_Programas");
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Inicializa_Controles();//Habilita los controles para la siguiente operación del usuario en el catálogo
            }
            ScriptManager.RegisterStartupScript(Upd_Panel, typeof(string), "Imagen", "javascript:Inicializar_Eventos_Cat_Dependencias();", true);
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///************************************************************************************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Buscar_Dependencia_Click
    /// 
    /// DESCRIPCION : Ejecuta la búsqueda de la dependencia por la descripción ingresada.
    /// 
    /// CREO        : Juan Alberto Hernández Negrete
    /// FECHA_CREO  : 08/Marzo/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///************************************************************************************************************************************************
    protected void Btn_Buscar_Dependencia_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Consulta_Dependencias(); //Consulta las dependencias que coincidan con el nombre porporcionado por el usuario
            //Limpia_Controles();
            //Si no se encontraron dependencias con un nombre similar al proporcionado por el usuario entonces manda un mensaje al usuario
            if (Grid_Dependencias.Rows.Count <= 0)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontraron Dependencias con el nombre proporcionado <br>";
            }
            ScriptManager.RegisterStartupScript(Upd_Panel, typeof(string), "Imagen", "javascript:Inicializar_Eventos_Cat_Dependencias();", true);
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion

    #region (Cajas de Texto)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Txt_Clave_Dependecia_TextChanged
    ///
    ///DESCRIPCIÓN: Valida si la clave que se ingreso no corresponde a una dependencia 
    ///             en el sistema.
    ///             
    ///CREO: Juan alberto Hernández Negrete
    ///FECHA_CREO: 8/Marzo/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Txt_Clave_Dependecia_TextChanged(object sender, EventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (!Es_Clave_Repetida(Txt_Clave_Dependecia.Text.Trim()))
            {
                Txt_Clave_Dependecia.Text = "";

                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "La clave ya existe en el sistema!";
            }
            ScriptManager.RegisterStartupScript(Upd_Panel, typeof(string), "Imagen", "javascript:Inicializar_Eventos_Cat_Dependencias();", true);
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
        }
    }
    #endregion

    #region (Eventos Puestos)
    protected void Btn_Agregar_Puesto_Click(object sender, EventArgs e)
    {
        Agregar_Puesto_Unidad_Responsable();
    }

    protected void Btn_Eliminar_Puesto_Click(object sender, EventArgs e)
    {
        Cls_Cat_Puestos_Negocio Obj_Puestos = new Cls_Cat_Puestos_Negocio();
        DataTable Dt_Puestos = null;

        if (Btn_Nuevo.ToolTip.Trim().ToUpper().Equals("NUEVO"))
        {
            Obj_Puestos.P_Puesto_ID = ((ImageButton)sender).CommandArgument.Trim();
            Obj_Puestos.P_Dependencia_ID = Txt_Dependencia_ID.Text.Trim();
            Obj_Puestos.P_Estatus = "OCUPADO";
            Dt_Puestos = Obj_Puestos.Consultar_Puestos_Disponibles_Dependencia();

            if (Dt_Puestos is DataTable) {
                if (Dt_Puestos.Rows.Count > 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Información", "alert('No es posible eliminar un puesto de la unidad responsable " +
                        "si el puesto actualmente se encuentra ocupado por algun empleado.');", true);
                }
                else Eliminar_Puesto((DataTable)Session["PUESTOS_UNIDAD_RESPNSABLE"], ((ImageButton)sender).CommandArgument.Trim());
            }
            else Eliminar_Puesto((DataTable)Session["PUESTOS_UNIDAD_RESPNSABLE"], ((ImageButton)sender).CommandArgument.Trim());
        }
        else
        {
            Eliminar_Puesto((DataTable)Session["PUESTOS_UNIDAD_RESPNSABLE"], ((ImageButton)sender).CommandArgument.Trim());
        }
    }
    #endregion

    #endregion




    protected String Consultar_Consecutivo(DataTable Dt_Puestos)
    {
        Int32 Consecutivo = 0;
        String Clave = String.Empty;

        if (Dt_Puestos is DataTable)
        {
            if (Dt_Puestos.Rows.Count > 0)
            {
                foreach (DataRow PUESTO in Dt_Puestos.Rows)
                {
                    if (PUESTO is DataRow)
                    {
                        if (!String.IsNullOrEmpty(PUESTO[Cat_Nom_Dep_Puestos_Det.Campo_Clave].ToString().Trim())) {
                            if (Consecutivo <= (Convert.ToInt32(PUESTO[Cat_Nom_Dep_Puestos_Det.Campo_Clave].ToString().Trim())))
                            {
                                Consecutivo = (Convert.ToInt32(PUESTO[Cat_Nom_Dep_Puestos_Det.Campo_Clave].ToString().Trim()) + 1);
                            }
                        }
                    }
                }
            }
            else {
                Consecutivo = 1;
            }
        }
        return Consecutivo.ToString().Trim();
    }

}
