﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls.Adapters;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Roles.Negocio;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Areas.Negocios;
using JAPAMI.Empleados.Negocios;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Requisitos_Empleados.Negocios;
using AjaxControlToolkit;
using System.IO;
using System.Globalization;
using JAPAMI.Programas.Negocios;
using JAPAMI.Tipos_Contratos.Negocios;
using JAPAMI.Puestos.Negocios;
using JAPAMI.Escolaridad.Negocios;
using JAPAMI.Sindicatos.Negocios;
using JAPAMI.Turnos.Negocios;
using JAPAMI.Zona_Economica.Negocios;
using JAPAMI.Tipo_Trabajador.Negocios;
using System.Text.RegularExpressions;
using JAPAMI.Vacaciones_Empleado.Negocio;
using JAPAMI.Tipos_Nominas.Negocios;
using JAPAMI.Utilidades_Nomina;
using JAPAMI.Bancos_Nomina.Negocio;
using JAPAMI.Catalogo_SAP_Fuente_Financiamiento.Negocio;
using JAPAMI.Catalogo_Compras_Proyectos_Programas.Negocio;
using JAPAMI.Catalogo_Compras_Partidas.Negocio;
using JAPAMI.Indemnizacion.Negocio;

public partial class paginas_Nomina_Frm_Cat_Empleados : System.Web.UI.Page
{
    #region (Page Load)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Page_Load
    /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda realizar
    ///               diferentes operaciones
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 10-Septiembre-2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                Session["Activa"] = true;//Variable para mantener la session activa.
                Inicializa_Controles();
                ViewState["SortDirection"] = "ASC";
            }

            Txt_Password_Empleado.Attributes.Add("value", Txt_Password_Empleado.Text);
            Txt_Confirma_Password_Empleado.Attributes.Add("value", Txt_Confirma_Password_Empleado.Text);

            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";

            //Solo es posible consultar
            Btn_Nuevo.Visible = true;
            Btn_Modificar.Visible = true;
            Btn_Eliminar.Visible = true;
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion

    #region (Metodos)

    #region (Metodos Generales)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Inicializa_Controles
    /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda realizar
    ///               diferentes operaciones
    /// PARAMETROS  : 
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 10-Septiembre-2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Inicializa_Controles()
    {
        try
        {
            Limpia_Controles();
            Habilitar_Controles("Inicial"); //Habilita los controles de la forma para que el usuario pueda indica que operación desea realizar
            Consultar_Roles();
            Consultar_SAP_Unidades_Responsables();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message.ToString());
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Controles
    /// DESCRIPCION : Limpia los controles que se encuentran en la forma
    /// PARAMETROS  : 
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 10-Septiembre-2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Limpia_Controles()
    {
        try
        {
            ///Datos Generales del Empleado
            Txt_Empleado_ID.Text = "";
            Txt_No_Empleado.Text = "";
            Cmb_Estatus_Empleado.SelectedIndex = 1;
            Txt_Nombre_Empleado.Text = "";
            Txt_Apellido_Paterno_Empleado.Text = "";
            Txt_Apellido_Materno_Empleado.Text = "";
            Txt_Password_Empleado.Text = "";
            Txt_Confirma_Password_Empleado.Text = "";
            Txt_Password_Empleado.Attributes.Add("value", "");
            Txt_Confirma_Password_Empleado.Attributes.Add("value", "");            
            Txt_Comentarios_Empleado.Text = "";
            Txt_Fecha_Nacimiento_Empleado.Text = "";
            Cmb_Sexo_Empleado.SelectedIndex = 0;
            Txt_RFC_Empleado.Text = "";
            Txt_CURP_Empleado.Text = "";
            Txt_Domicilio_Empleado.Text = "";
            Txt_Colonia_Empleado.Text = "";
            Txt_Codigo_Postal_Empleado.Text = "";
            Txt_Ciudad_Empleado.Text = "";
            Txt_Estado_Empleado.Text = "";
            Txt_Telefono_Casa_Empleado.Text = "";
            Txt_Celular_Empleado.Text = "";
            Txt_Nextel_Empleado.Text = "";
            Txt_Telefono_Oficina_Empleado.Text = "";
            Txt_Extension_Empleado.Text = "";
            Txt_Fax_Empleado.Text = "";
            Txt_Correo_Electronico_Empleado.Text = "";
            Txt_No_Licencia.Text = "";
            Cmb_Tipo_Licencia.SelectedIndex = 0;
            Txt_Fecha_Vencimiento.Text = "";
            if (Cmb_Roles_Empleado.Items.Count > 0) Cmb_Roles_Empleado.SelectedIndex = 0;
            if (Cmb_SAP_Unidad_Responsable.Items.Count > 0) Cmb_SAP_Unidad_Responsable.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw new Exception("Limpiar_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Habilitar_Controles
    /// DESCRIPCION : Habilita y Deshabilita los controles de la forma para prepara la página
    ///               para a siguiente operación
    /// PARAMETROS  : Operacion: Indica la operación que se desea realizar por parte del usuario
    ///                          si es una alta, modificacion
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 10-Septiembre-2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Habilitar_Controles(String Operacion)
    {
        Boolean Habilitado; ///Indica si el control de la forma va hacer habilitado para utilización del usuario

        try
        {
            Habilitado = false;
            switch (Operacion)
            {
                case "Inicial":
                    Habilitado = false;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Salir";
                    Cmb_Estatus_Empleado.Enabled = Habilitado;
                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = true;
                    Btn_Eliminar.Visible = true;
                    Btn_Nuevo.CausesValidation = false;
                    Btn_Modificar.CausesValidation = false;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";

                    Btn_Subir_Foto.Enabled = false;
                    Img_Foto_Empleado.ImageUrl = "~/paginas/imagenes/paginas/Sias_No_Disponible.JPG";
                    Img_Foto_Empleado.DataBind();
                    Txt_Ruta_Foto.Value = "";
                    break;

                case "Nuevo":
                    Habilitado = true;
                    Btn_Nuevo.ToolTip = "Dar de Alta";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = false;
                    Btn_Eliminar.Visible = false;
                    Btn_Nuevo.CausesValidation = true;
                    Btn_Modificar.CausesValidation = true;
                    Cmb_Estatus_Empleado.Enabled = false;
                    Btn_Subir_Foto.Enabled = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";

                    Img_Foto_Empleado.ImageUrl = "~/paginas/imagenes/paginas/Sias_No_Disponible.JPG";
                    Img_Foto_Empleado.DataBind();
                    Txt_Ruta_Foto.Value = "";
                    Cmb_Estatus_Empleado.SelectedIndex = 1;
                    break;
                case "Modificar":
                    Habilitado = true;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Cmb_Estatus_Empleado.Enabled = true;
                    Btn_Nuevo.Visible = false;
                    Btn_Modificar.Visible = true;
                    Btn_Eliminar.Visible = false;
                    Btn_Nuevo.CausesValidation = true;
                    Btn_Modificar.CausesValidation = true;
                    Btn_Subir_Foto.Enabled = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                    break;
            }
            ///Datos Personales
            Txt_No_Empleado.Enabled = Habilitado;
            Txt_Nombre_Empleado.Enabled = Habilitado;
            Txt_Apellido_Paterno_Empleado.Enabled = Habilitado;
            Txt_Apellido_Materno_Empleado.Enabled = Habilitado;
            Txt_Password_Empleado.Enabled = Habilitado;
            Txt_Confirma_Password_Empleado.Enabled = Habilitado;
            Cmb_SAP_Unidad_Responsable.Enabled = Habilitado;
            Txt_Comentarios_Empleado.Enabled = Habilitado;
            Txt_Fecha_Nacimiento_Empleado.Enabled = Habilitado;
            Cmb_Sexo_Empleado.Enabled = Habilitado;
            Txt_RFC_Empleado.Enabled = Habilitado;
            Txt_CURP_Empleado.Enabled = Habilitado;
            Txt_Domicilio_Empleado.Enabled = Habilitado;
            Txt_Colonia_Empleado.Enabled = Habilitado;
            Txt_Codigo_Postal_Empleado.Enabled = Habilitado;
            Txt_Ciudad_Empleado.Enabled = Habilitado;
            Txt_Estado_Empleado.Enabled = Habilitado;
            Txt_Telefono_Casa_Empleado.Enabled = Habilitado;
            Txt_Celular_Empleado.Enabled = Habilitado;
            Txt_Nextel_Empleado.Enabled = Habilitado;
            Txt_Telefono_Oficina_Empleado.Enabled = Habilitado;
            Txt_Extension_Empleado.Enabled = Habilitado;
            Txt_Fax_Empleado.Enabled = Habilitado;
            Txt_Correo_Electronico_Empleado.Enabled = Habilitado;
            Txt_No_Licencia.Enabled = Habilitado;
            Cmb_Tipo_Licencia.Enabled = Habilitado;
            Txt_Fecha_Vencimiento.Enabled = Habilitado;
            Cmb_Roles_Empleado.Enabled = Habilitado;
            Grid_Empleados.Enabled = !Habilitado;
            Async_Foto_Empleado.Enabled = Habilitado;
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Cmb_Estatus_Empleado.Enabled = false;
        }
        catch (Exception ex)
        {
            throw new Exception("Habilitar_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Agregar_Tooltip_Combos
    /// DESCRIPCION : Agregar tooltip al combo que es pasado como parametro.
    /// PARAMETROS  : _DropDownList es el combo al cual se le agregara el tooltip
    /// 
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 26/Octubre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Agregar_Tooltip_Combos(DropDownList Cmb_Combo)
    {
        for (int i = 0; i <= Cmb_Combo.Items.Count - 1; i++)
        {
            Cmb_Combo.Items[i].Attributes.Add("Title", Cmb_Combo.Items[i].Text);
        }
    }
    
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Remover_Sesiones_Control_Carga_Archivos
    /// DESCRIPCION : Remueve la sesion del Ctlr AsyncFileUpload que mantiene al archivo
    /// en memoria.
    /// 
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 27/Octubre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Remover_Sesiones_Control_Carga_Archivos(String Client_ID)
    {
        HttpContext currentContext;
        if (HttpContext.Current != null && HttpContext.Current.Session != null)
        {
            currentContext = HttpContext.Current;
        }
        else
        {
            currentContext = null;
        }

        if (currentContext != null)
        {
            foreach (String key in currentContext.Session.Keys)
            {
                if (key.Contains(Client_ID))
                {
                    currentContext.Session.Remove(key);
                    break;
                }
            }
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Recorre_Grid_Envia_Limpiar_Control_Subir_Archivos
    /// DESCRIPCION : Recorre el GridView que es pasado como parametro y obtiene el ID
    /// del control AsyncFileUpload. Para posteriormente remover la sesion que mantiene al
    /// archivo en memoria.
    /// PARAMETROS: _GridView
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 27/Octubre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Recorre_Grid_Envia_Limpiar_Control_Subir_Archivos(GridView Grid_Generico)
    {
        for (int cont = 0; cont < Grid_Generico.Rows.Count; cont++)
        {
            String Client_ID = ((AsyncFileUpload)Grid_Generico.Rows[cont].Cells[2].FindControl("Async_Requisito_Empleado")).ClientID;
            Remover_Sesiones_Control_Carga_Archivos(Client_ID);
        }
    }
    ///******************************************************************************
    /// NOMBRE DE LA FUNCIÓN: Subir_Foto_Click
    /// DESCRIPCIÓN: Carga la Foto del empleado a dar de alta
    /// CREO: Juan Alberto Hernandez Negrete
    /// FECHA_CREO: 30/Octubre/2010
    /// MODIFICO:
    /// FECHA_MODIFICO
    /// CAUSA_MODIFICACIÓN   
    ///******************************************************************************
    protected void Subir_Foto_Click(object sender, EventArgs e)
    {
        String Ruta_Servidor_Empleados = "";
        String Nombre_Dir_Empleado = "";
        AsyncFileUpload Asy_FileUpload;
        Cls_Cat_Empleados_Negocios Cat_Empleados = new Cls_Cat_Empleados_Negocios();

        try
        {
            if (Async_Foto_Empleado.HasFile)
            {
                //Se obtiene la direccion en donde se va a guardar el archivo. Ej. C:/Dir_Servidor/..
                Ruta_Servidor_Empleados = Server.MapPath("Foto_Empleados");

                //Crear el Directorio Proveedores. Ej. Proveedores
                if (!Directory.Exists(Ruta_Servidor_Empleados))
                {
                    System.IO.Directory.CreateDirectory(Ruta_Servidor_Empleados);
                }

                //Se establece el nombre del directorio Ej. Empleado_00001
                Nombre_Dir_Empleado = "Empleado_" + (Txt_Empleado_ID.Text.Trim().Equals("") ? Cat_Empleados.Consulta_Id_Empleado() : Txt_Empleado_ID.Text.Trim());

                if (Directory.Exists(Ruta_Servidor_Empleados))
                {
                    //Obtenemos el Ctlr AsyncFileUpload del GridView.
                    Asy_FileUpload = Async_Foto_Empleado;

                    //Validamos que el nombre del archivo no se encuentre vacio.
                    if (!Asy_FileUpload.FileName.Equals(""))
                    {
                        //Valida que no exista el directorio, si no existe lo crea [172.16.0.103/Web/Project/Empleado/Empleado_00001]
                        DirectoryInfo Ruta_Completa_Dir_Empleado;
                        if (!Directory.Exists(Ruta_Servidor_Empleados + Nombre_Dir_Empleado))
                        {
                            Ruta_Completa_Dir_Empleado = Directory.CreateDirectory(Ruta_Servidor_Empleados + @"\" + Nombre_Dir_Empleado);
                        }

                        //Se asigna el directorio en donde se va a guardar los documentos. Ej. [Empleado/]
                        String Ruta_Dir_Empleado = Nombre_Dir_Empleado + @"\";

                        //Se establece la ruta completa del archivo . Ej. [172.16.0.103/Web/Project/Empleado/Empleado_00001/File1.txt]
                        String Ruta_Completa_Archivo_A_Cargar = Ruta_Servidor_Empleados + @"\" + Ruta_Dir_Empleado +
                            Nombre_Dir_Empleado + "." + Asy_FileUpload.FileName.Split(new Char[] { '.' })[1];

                        //Se valida que el Ctlr AsyncFileUpload. Contenga el archivo a guardar.
                        if (Asy_FileUpload.HasFile)
                        {
                            DirectoryInfo directory = new DirectoryInfo((Ruta_Servidor_Empleados + @"\" + Ruta_Dir_Empleado));
                            foreach (FileInfo fi in directory.GetFiles())
                            {
                                File.Delete((Ruta_Servidor_Empleados + @"\" + Ruta_Dir_Empleado) + @"\" + fi.Name);
                            }

                            //Se guarda el archivo. En la ruta indicada. Ej.  [172.16.0.103/Web/Project/Empleado/Empleado_00001/File1.txt]
                            Asy_FileUpload.SaveAs(Ruta_Completa_Archivo_A_Cargar);
                            //Guardamos en el campo hidden la ruta de la foto del empelado.
                            Txt_Ruta_Foto.Value = @HttpUtility.HtmlDecode("Foto_Empleados" + @"\" + Ruta_Dir_Empleado + Nombre_Dir_Empleado + "." + Asy_FileUpload.FileName.Split(new Char[] { '.' })[1]);
                            Img_Foto_Empleado.ImageUrl = @HttpUtility.HtmlDecode("Foto_Empleados" + @"\" + Ruta_Dir_Empleado + Nombre_Dir_Empleado + "." + Asy_FileUpload.FileName.Split(new Char[] { '.' })[1]);
                            Img_Foto_Empleado.DataBind();
                            Remover_Sesiones_Control_Carga_Archivos(Async_Foto_Empleado.ClientID);
                        }
                    }
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "", "alert('No se ha seleccionado  ninguna foto a guardar');", true);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error generado al cargar la foto del empleado. ERROR: [" + Ex.Message + "]");
        }
    }
    /// ********************************************************************************
    /// Nombre: Crear_Tabla_Mostrar_Errores_Pagina
    /// Descripción: Crea la tabla que almacenara que datos son requeridos 
    /// por el sistema
    /// Creo: Juan Alberto Hernández Negrete 
    /// Fecha Creo: 20/Octubre/2010
    /// Modifico:
    /// Fecha Modifico:
    /// Causa Modifico:
    /// ********************************************************************************
    private String Crear_Tabla_Mostrar_Errores_Pagina( String Errores ) {
        String Tabla_Inicio = "<table style='width:100%px;font-size:10px;color:red;text-align:left;'>";
        String Tabla_Cierra = "</table>";
        String Fila_Inicia = "<tr>";
        String Fila_Cierra = "</tr>";
        String Celda_Inicia = "<td style='width:25%;text-align:left;vertical-align:top;font-size:10px;' " +
                                "onmouseover=this.style.background='#DFE8F6';this.style.color='#000000'"+
                                " onmouseout=this.style.background='#ffffff';this.style.color='red'>";
        String Celda_Cierra = "</td>";
        char[] Separador = {'+'};
        String[] _Errores_Temp = Errores.Replace("<br>", "").Split(Separador);
        String[] _Errores = new String[(_Errores_Temp.Length - 1)];
        String Tabla;
        String Filas = "";
        String Celdas = "";
        int Contador_Celdas = 1;
        for (int i = 0; i < _Errores.Length; i++) _Errores[i] = _Errores_Temp[i+1];

        Tabla = Tabla_Inicio;
        for (int i = 0; i < _Errores.Length; i++)
        {
            if (Contador_Celdas == 5 )
            {
                Filas += Fila_Inicia;
                Filas += Celdas;
                Filas += Fila_Cierra;
                Celdas = "";
                Contador_Celdas = 0;
                i = i - 1;
            }
            else
            {
                Celdas += Celda_Inicia;
                Celdas += "<b style='font-size:12px;'>+</b>" + _Errores[i];
                Celdas += Celda_Cierra;                
            }
            Contador_Celdas = Contador_Celdas + 1;            
        }
        if (_Errores.Length < 5 || Contador_Celdas > 0) {
            Filas += Fila_Inicia;
            Filas += Celdas;
            Filas += Fila_Cierra;
        }
        Tabla += Filas;
        Tabla += Tabla_Cierra;
        return Tabla;
    }
    #endregion
    
    #region (Operacion [Alta - Modificar - Eliminar])
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Alta_Empleado
    /// DESCRIPCION : Da de Alta el Empleado con los datos proporcionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 10-Septiembre-2010
    /// MODIFICO          :Juan Alberto Hernandez Negrete
    /// FECHA_MODIFICO    :3/Noviembre/2010
    /// CAUSA_MODIFICACION: Completar el Catalogo
    ///*******************************************************************************
    private void Alta_Empleado()
    {
        Cls_Cat_Empleados_Negocios Rs_Alta_Cat_Empleados = new Cls_Cat_Empleados_Negocios(); //Variable de conexión hacia la capa de negocios para envio de los datos a dar de alta
        try
        {
            ///Datos Generales del Empleado
            Rs_Alta_Cat_Empleados.P_Ruta_Foto = Txt_Ruta_Foto.Value;
            Rs_Alta_Cat_Empleados.P_No_Empleado = Convert.ToString(Txt_No_Empleado.Text);
            Rs_Alta_Cat_Empleados.P_Estatus = Cmb_Estatus_Empleado.SelectedValue;
            Rs_Alta_Cat_Empleados.P_Nombre = Convert.ToString(Txt_Nombre_Empleado.Text);
            Rs_Alta_Cat_Empleados.P_Apellido_Paterno = Convert.ToString(Txt_Apellido_Paterno_Empleado.Text);
            Rs_Alta_Cat_Empleados.P_Apelldo_Materno = Convert.ToString(Txt_Apellido_Materno_Empleado.Text);
            if (Cmb_Roles_Empleado.SelectedIndex > 0) Rs_Alta_Cat_Empleados.P_Rol_ID = Cmb_Roles_Empleado.SelectedValue;
            if (Txt_Password_Empleado.Text != "") Rs_Alta_Cat_Empleados.P_Password = Convert.ToString(Txt_Password_Empleado.Text);
            Rs_Alta_Cat_Empleados.P_Comentarios = Convert.ToString(Txt_Comentarios_Empleado.Text);
            Rs_Alta_Cat_Empleados.P_Confronto = Txt_Empleado_Confronto.Text.Trim();

            ///Datos Personales del Empleado
            Rs_Alta_Cat_Empleados.P_Fecha_Nacimiento = Convert.ToDateTime(Txt_Fecha_Nacimiento_Empleado.Text);
            Rs_Alta_Cat_Empleados.P_Sexo = Cmb_Sexo_Empleado.SelectedValue;
            if (Txt_RFC_Empleado.Text != "") Rs_Alta_Cat_Empleados.P_RFC = Convert.ToString(Txt_RFC_Empleado.Text);
            if (Txt_CURP_Empleado.Text != "") Rs_Alta_Cat_Empleados.P_CURP = Convert.ToString(Txt_CURP_Empleado.Text);
            Rs_Alta_Cat_Empleados.P_Calle = Convert.ToString(Txt_Domicilio_Empleado.Text);
            Rs_Alta_Cat_Empleados.P_Colonia = Convert.ToString(Txt_Colonia_Empleado.Text);
            if (Txt_Codigo_Postal_Empleado.Text != "") Rs_Alta_Cat_Empleados.P_Codigo_Postal = Convert.ToInt32(Txt_Codigo_Postal_Empleado.Text.ToString());
            Rs_Alta_Cat_Empleados.P_Ciudad = Convert.ToString(Txt_Ciudad_Empleado.Text);
            Rs_Alta_Cat_Empleados.P_Estado = Convert.ToString(Txt_Estado_Empleado.Text);
            if (Txt_Telefono_Casa_Empleado.Text != "") Rs_Alta_Cat_Empleados.P_Telefono_Casa = Convert.ToString(Txt_Telefono_Casa_Empleado.Text);
            if (Txt_Celular_Empleado.Text != "") Rs_Alta_Cat_Empleados.P_Celular = Convert.ToString(Txt_Celular_Empleado.Text);
            if (Txt_Nextel_Empleado.Text != "") Rs_Alta_Cat_Empleados.P_Nextel = Convert.ToString(Txt_Nextel_Empleado.Text);
            if (Txt_Telefono_Oficina_Empleado.Text != "") Rs_Alta_Cat_Empleados.P_Telefono_Oficina = Convert.ToString(Txt_Telefono_Oficina_Empleado.Text);
            if (Txt_Extension_Empleado.Text != "") Rs_Alta_Cat_Empleados.P_Extension = Convert.ToString(Txt_Extension_Empleado.Text);
            if (Txt_Fax_Empleado.Text != "") Rs_Alta_Cat_Empleados.P_Fax = Convert.ToString(Txt_Fax_Empleado.Text);
            if (Txt_Correo_Electronico_Empleado.Text != "") Rs_Alta_Cat_Empleados.P_Correo_Electronico = Convert.ToString(Txt_Correo_Electronico_Empleado.Text);
            if (Txt_No_Licencia.Text != "") Rs_Alta_Cat_Empleados.P_No_Licencia = Convert.ToString(Txt_No_Licencia.Text);
            if (Cmb_Tipo_Licencia.Text != "") Rs_Alta_Cat_Empleados.P_Tipo_Licencia = Convert.ToString(Cmb_Tipo_Licencia.Text);
            if (Txt_Fecha_Vencimiento.Text != "") Rs_Alta_Cat_Empleados.P_Fecha_Vigencia_Licencia = Convert.ToDateTime(Txt_Fecha_Vencimiento.Text);
            
            ///Usuario creo
            Rs_Alta_Cat_Empleados.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;

            ///Crea el DataTable de los Documentos
            //Guardar_Documentos();
            if (Session["Dt_Requisitos_Empleado"] != null)
            {
                Rs_Alta_Cat_Empleados.P_Documentos_Anexos_Empleado = (DataTable)Session["Dt_Requisitos_Empleado"];
                Session.Remove("Dt_Requisitos_Empleado");
            }

            //------------------------------------  SAP Código Programático  ----------------------------------------------
            Rs_Alta_Cat_Empleados.P_Dependencia_ID = Cmb_SAP_Unidad_Responsable.SelectedValue.Trim();
            //Obtiene el área a la que pertenece la dependencia
            DataSet Ds_Area_Dependecia = Rs_Alta_Cat_Empleados.Consulta_Area_Unidad_Responsable();
            if (Ds_Area_Dependecia.Tables[0].Rows.Count > 0)
            {
                if (Ds_Area_Dependecia.Tables[0].Rows[0]["Area_Funcional_ID"] != null)
                {
                    Rs_Alta_Cat_Empleados.P_Area_ID = Ds_Area_Dependecia.Tables[0].Rows[0]["Area_Funcional_ID"].ToString();
                }
            }

            Rs_Alta_Cat_Empleados.Alta_Empleado(); //Da de alta los datos del Empleado proporcionados por el usuario en la BD
            Txt_Empleado_ID.Text = Rs_Alta_Cat_Empleados.P_Empleado_ID;
            //Consulta_Empleados();//Comentado para no mostrar el grid de empleados.
            Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Empleados", "alert('El Alta del Empleado fue Exitosa');", true);
        }
        catch (Exception ex)
        {
            throw new Exception("Alta_Empleado " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Modificar_Empleado
    /// DESCRIPCION : Modifica los datos del Empleado con los proporcionados por el usuario en la BD
    /// PARAMETROS  : 
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 10-Septiembre-2010
    /// MODIFICO          :Juan Alberto Hernandez Negrete
    /// FECHA_MODIFICO    :3/Noviembre/2010
    /// CAUSA_MODIFICACION: Completar el Catalogo
    ///*******************************************************************************
    private void Modificar_Empleado()
    {
        Cls_Cat_Empleados_Negocios Rs_Modificar_Cat_Empleados = new Cls_Cat_Empleados_Negocios();
        try
        {
            ///Datos Generales del Empleado
            Rs_Modificar_Cat_Empleados.P_Ruta_Foto = Txt_Ruta_Foto.Value;
            Rs_Modificar_Cat_Empleados.P_Empleado_ID = Txt_Empleado_ID.Text;
            Rs_Modificar_Cat_Empleados.P_No_Empleado = Convert.ToString(Txt_No_Empleado.Text);
            Rs_Modificar_Cat_Empleados.P_Estatus = Cmb_Estatus_Empleado.SelectedValue;
            Rs_Modificar_Cat_Empleados.P_Nombre = Convert.ToString(Txt_Nombre_Empleado.Text);
            Rs_Modificar_Cat_Empleados.P_Apellido_Paterno = Convert.ToString(Txt_Apellido_Paterno_Empleado.Text);
            Rs_Modificar_Cat_Empleados.P_Apelldo_Materno = Convert.ToString(Txt_Apellido_Materno_Empleado.Text);
            Rs_Modificar_Cat_Empleados.P_Dependencia_ID = Cmb_SAP_Unidad_Responsable.SelectedValue;
            //Obtiene el área a la que pertenece la dependencia
            //DataSet Ds_Area_Dependecia = Rs_Modificar_Cat_Empleados.Consulta_Area_Unidad_Responsable();
            //if (Ds_Area_Dependecia.Tables[0].Rows.Count > 0)
            //{
            //    if (Ds_Area_Dependecia.Tables[0].Rows[0]["Area_Funcional_ID"] != null)
            //    {
            //        Rs_Modificar_Cat_Empleados.P_Area_ID = Ds_Area_Dependecia.Tables[0].Rows[0]["Area_Funcional_ID"].ToString();
            //    }
            //}
            if (Cmb_Roles_Empleado.SelectedIndex > 0)
            {
                if (Txt_Password_Empleado.Text != "") Rs_Modificar_Cat_Empleados.P_Password = Convert.ToString(Txt_Password_Empleado.Text);
                Rs_Modificar_Cat_Empleados.P_Rol_ID = Cmb_Roles_Empleado.SelectedValue;
            }
            Rs_Modificar_Cat_Empleados.P_Comentarios = Convert.ToString(Txt_Comentarios_Empleado.Text);
            Rs_Modificar_Cat_Empleados.P_Confronto = Txt_Empleado_Confronto.Text.Trim();

            ///Datos Personales del Empleado
            Rs_Modificar_Cat_Empleados.P_Fecha_Nacimiento = Convert.ToDateTime(Txt_Fecha_Nacimiento_Empleado.Text);
            Rs_Modificar_Cat_Empleados.P_Sexo = Cmb_Sexo_Empleado.SelectedValue;
            if (Txt_RFC_Empleado.Text != "") Rs_Modificar_Cat_Empleados.P_RFC = Convert.ToString(Txt_RFC_Empleado.Text);
            if (Txt_CURP_Empleado.Text != "") Rs_Modificar_Cat_Empleados.P_CURP = Convert.ToString(Txt_CURP_Empleado.Text);
            Rs_Modificar_Cat_Empleados.P_Calle = Convert.ToString(Txt_Domicilio_Empleado.Text);
            Rs_Modificar_Cat_Empleados.P_Colonia = Convert.ToString(Txt_Colonia_Empleado.Text);
            if (Txt_Codigo_Postal_Empleado.Text != "") Rs_Modificar_Cat_Empleados.P_Codigo_Postal = Convert.ToInt32(Txt_Codigo_Postal_Empleado.Text.ToString());
            Rs_Modificar_Cat_Empleados.P_Ciudad = Convert.ToString(Txt_Ciudad_Empleado.Text);
            Rs_Modificar_Cat_Empleados.P_Estado = Convert.ToString(Txt_Estado_Empleado.Text);
            if (Txt_Telefono_Casa_Empleado.Text != "") Rs_Modificar_Cat_Empleados.P_Telefono_Casa = Convert.ToString(Txt_Telefono_Casa_Empleado.Text);
            if (Txt_Celular_Empleado.Text != "") Rs_Modificar_Cat_Empleados.P_Celular = Convert.ToString(Txt_Celular_Empleado.Text);
            if (Txt_Nextel_Empleado.Text != "") Rs_Modificar_Cat_Empleados.P_Nextel = Convert.ToString(Txt_Nextel_Empleado.Text);
            if (Txt_Telefono_Oficina_Empleado.Text != "") Rs_Modificar_Cat_Empleados.P_Telefono_Oficina = Convert.ToString(Txt_Telefono_Oficina_Empleado.Text);
            if (Txt_Extension_Empleado.Text != "") Rs_Modificar_Cat_Empleados.P_Extension = Convert.ToString(Txt_Extension_Empleado.Text);
            if (Txt_Fax_Empleado.Text != "") Rs_Modificar_Cat_Empleados.P_Fax = Convert.ToString(Txt_Fax_Empleado.Text);
            if (Txt_Correo_Electronico_Empleado.Text != "") Rs_Modificar_Cat_Empleados.P_Correo_Electronico = Convert.ToString(Txt_Correo_Electronico_Empleado.Text);
            if (Txt_No_Licencia.Text != "") Rs_Modificar_Cat_Empleados.P_No_Licencia = Convert.ToString(Txt_No_Licencia.Text);
            if (Cmb_Tipo_Licencia.Text != "") Rs_Modificar_Cat_Empleados.P_Tipo_Licencia = Convert.ToString(Cmb_Tipo_Licencia.Text);
            if (Txt_Fecha_Vencimiento.Text != "") Rs_Modificar_Cat_Empleados.P_Fecha_Vigencia_Licencia = Convert.ToDateTime(Txt_Fecha_Vencimiento.Text);

            Rs_Modificar_Cat_Empleados.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
            
            Rs_Modificar_Cat_Empleados.Modificar_Empleado(); //Sustituye los datos que se encuentran en la BD por lo que introdujo el usuario
            //Consulta_Empleados();//Comentado para no mostrar el grid de empleados.
            Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Empleados", "alert('La Modificación del Empleado fue Exitosa');", true);
        }
        catch (Exception ex)
        {
            throw new Exception("Modificar_Empleado " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Eliminar_Empleado
    /// DESCRIPCION : Elimina los datos del Empleado que fue seleccionado por el Usuario
    /// PARAMETROS  : 
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 13-Septiembre-2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Eliminar_Empleado()
    {
        Cls_Cat_Empleados_Negocios Rs_Eliminar_Cat_Empleados = new Cls_Cat_Empleados_Negocios(); //Variable de conexión hacia la capa de Negocios para la eliminación de los datos
        try
        {
            Rs_Eliminar_Cat_Empleados.P_Empleado_ID = Txt_Empleado_ID.Text;
            Rs_Eliminar_Cat_Empleados.P_Dependencia_ID = Cmb_SAP_Unidad_Responsable.SelectedValue.Trim();
            Rs_Eliminar_Cat_Empleados.P_Tipo_Movimiento = "BAJA";
            Rs_Eliminar_Cat_Empleados.P_Estatus = "INACTIVO";
            
            Rs_Eliminar_Cat_Empleados.Eliminar_Empleado(); //Elimina el Empleado que selecciono el usuario de la BD
            Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
            Grid_Empleados.DataBind();            
        }
        catch (Exception ex)
        {
            throw new Exception("Eliminar_Empleado " + ex.Message.ToString(), ex);
        }
    }
    #endregion

    #region (Metodos Consulta)
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Empleados
        /// DESCRIPCION : Consulta los Empleados que estan dadas de alta en la BD
        /// PARAMETROS  : 
        /// CREO        : Juan Alberto Hernandez Negrete
        /// FECHA_CREO  : 10-Septiembre-2010
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private void Consulta_Empleados()
        {
            Cls_Cat_Empleados_Negocios Rs_Consulta_Ca_Empleados = new Cls_Cat_Empleados_Negocios(); //Variable de conexión hacia la capa de Negocios
            DataTable Dt_Empleados; //Variable que obtendra los datos de la consulta 

            try
            {
                if (!string.IsNullOrEmpty(Txt_Empleado_ID.Text))
                {
                    Rs_Consulta_Ca_Empleados.P_Empleado_ID = Txt_Empleado_ID.Text;
                }
                Dt_Empleados = Rs_Consulta_Ca_Empleados.Consulta_Empleados(); //Consulta todos los Empleados que coindican con lo proporcionado por el usuario
                Session["Consulta_Empleados"] = Dt_Empleados;
                Llena_Grid_Empleados();
            }
            catch (Exception ex)
            {
                throw new Exception("Consulta_Empleados " + ex.Message.ToString(), ex);
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Roles
        /// DESCRIPCION : Consulta los Roles que estan dadas de alta en la DB
        /// PARAMETROS  : 
        /// CREO        : Juan Alberto Hernandez Negrete
        /// FECHA_CREO  : 25-Agosto-2010
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private void Consultar_Roles()
        {
            DataTable Dt_Roles;
            Cls_Apl_Cat_Roles_Business Rs_Consulta_Apl_Cat_Roles = new Cls_Apl_Cat_Roles_Business();

            try
            {
                Dt_Roles = Rs_Consulta_Apl_Cat_Roles.Llenar_Tbl_Roles();
                Cmb_Roles_Empleado.DataSource = Dt_Roles;
                Cmb_Roles_Empleado.DataValueField = "Rol_ID";
                Cmb_Roles_Empleado.DataTextField = "Nombre";
                Cmb_Roles_Empleado.DataBind();
                Cmb_Roles_Empleado.Items.Insert(0, new ListItem("<- Seleccione ->", ""));
                Cmb_Roles_Empleado.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                throw new Exception("Consultar_Roles " + ex.Message.ToString(), ex);
            }
        }

        private void Consultar_SAP_Unidades_Responsables()
        {
            Cls_Cat_Dependencias_Negocio Obj_Unidades_Responsables = new Cls_Cat_Dependencias_Negocio();//Variable de conexion con la capa de negocios.
            DataTable Dt_Unidades_Responsables = null;//Variable que lista las unidades responsables registrdas en sistema.

            try
            {
                Dt_Unidades_Responsables = Obj_Unidades_Responsables.Consulta_Dependencias();
                Cmb_SAP_Unidad_Responsable.DataSource = Dt_Unidades_Responsables;
                Cmb_SAP_Unidad_Responsable.DataTextField = Cat_Dependencias.Campo_Nombre;
                Cmb_SAP_Unidad_Responsable.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
                Cmb_SAP_Unidad_Responsable.DataBind();
                Cmb_SAP_Unidad_Responsable.Items.Insert(0, new ListItem("<- Seleccione ->", ""));
                Cmb_SAP_Unidad_Responsable.SelectedIndex = -1;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar las unidades responsables registradas en sistema. Error: [" + Ex.Message + "]");
            }
        }
    #endregion

    #region (Metodos Validacion)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Email
    /// DESCRIPCION : Valida el E-mail Ingresado
    /// CREO        : Susana Trigueros Armenta
    /// FECHA_CREO  : 25/Octubre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public Boolean Validar_Email()
    {
        string Patron_Email = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@" +
                                   @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\." +
                                   @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|" +
                                   @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

        if (Txt_Correo_Electronico_Empleado.Text != null) return Regex.IsMatch(Txt_Correo_Electronico_Empleado.Text, Patron_Email);
        else return false;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_RFC
    /// DESCRIPCION : Valida el RFC Ingresado
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 25/Octubre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public Boolean Validar_RFC()
    {
        string Patron_RFC = @"^[a-zA-Z]{3,4}(\d{6})((\D|\d){3})?$";

        if (Txt_RFC_Empleado.Text != null) return Regex.IsMatch(Txt_RFC_Empleado.Text, Patron_RFC);
        else return false;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Codigo_Postal
    /// DESCRIPCION : Valida el Codigo Postal Ingresado
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 25/Octubre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public Boolean Validar_Codigo_Postal()
    {
        string Patron_CP = @"^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$";

        if (Txt_Codigo_Postal_Empleado.Text != null) return Regex.IsMatch(Txt_Codigo_Postal_Empleado.Text, Patron_CP);
        else return false;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Celular_Empleado
    /// DESCRIPCION : Valida el Telefono Ingresado
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 25/Octubre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public Boolean Validar_Celular_Empleado()
    {
        string Patron_Celular = @"^[0-9]{2,3}-? ?[0-9]{7,10}$";

        if (Txt_Celular_Empleado.Text != null) return Regex.IsMatch(Txt_Celular_Empleado.Text, Patron_Celular);
        else return false;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Telefono_Casa_Empleado
    /// DESCRIPCION : Valida el Telefono Ingresado
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 25/Octubre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public Boolean Validar_Telefono_Casa_Empleado()
    {
        string Patron_Tel_Casa = @"^[0-9]{2,3}-? ?[0-9]{5,8}$";

        if (Txt_Telefono_Casa_Empleado.Text != null) return Regex.IsMatch(Txt_Telefono_Casa_Empleado.Text, Patron_Tel_Casa);
        else return false;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Telefono_Oficina_Empleado
    /// DESCRIPCION : Valida el Telefono Ingresado
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 25/Octubre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public Boolean Validar_Telefono_Oficina_Empleado()
    {
        string Patron_Telefono_Oficina = @"^[0-9]{2,3}-? ?[0-9]{6,8}$";

        if (Txt_Telefono_Oficina_Empleado.Text != null) return Regex.IsMatch(Txt_Telefono_Oficina_Empleado.Text, Patron_Telefono_Oficina);
        else return false;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Fax
    /// DESCRIPCION : Valida el Fax Ingresado
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 25/Octubre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public Boolean Validar_Fax()
    {
        string Patron_Fax = @"^[0-9]{2,3}-? ?[0-9]{6,8}$";

        if (Txt_Fax_Empleado.Text != null) return Regex.IsMatch(Txt_Fax_Empleado.Text, Patron_Fax);
        else return false;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_CURP
    /// DESCRIPCION : Valida el Fax Ingresado
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 25/Octubre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public Boolean Validar_CURP()
    {
        string Patron_Curp = @"^[a-zA-Z]{4}(\d{6})([a-zA-Z]{6})(\d{2})?$";

        if (Txt_CURP_Empleado.Text != null) return Regex.IsMatch(Txt_CURP_Empleado.Text, Patron_Curp);
        else return false;
    }
    /// ********************************************************************************
    /// Nombre: Validar_Datos
    /// Descripción: Validar Campos
    /// Creo: Juan Alberto Hernández Negrete 
    /// Fecha Creo: 20/Octubre/2010
    /// Modifico:
    /// Fecha Modifico:
    /// Causa Modifico:
    /// ********************************************************************************
    private Boolean Validar_Datos()
    {
        Boolean Datos_Validos = true;
        Lbl_Mensaje_Error.Text = "";

        ///-------------------------------  Datos Generales  --------------------------------------------
        if (Txt_No_Empleado.Text == "")
        {
            Lbl_Mensaje_Error.Text += "+ El No. de Empleado <br>";
            Datos_Validos = false;
        }
        if (Cmb_Estatus_Empleado.SelectedIndex <= 0)
        {
            Lbl_Mensaje_Error.Text += "+ El Estatus del Empleado <br>";
            Datos_Validos = false;
        }
        if (Txt_Nombre_Empleado.Text == "")
        {
            Lbl_Mensaje_Error.Text += "+ El Nombre del Empleado <br>";
            Datos_Validos = false;
        }
        if (Cmb_Roles_Empleado.SelectedIndex <= 0)
        {
            Lbl_Mensaje_Error.Text += "+ El Rol del Empleado <br>";
            Datos_Validos = false;
        }
        if (Txt_Apellido_Paterno_Empleado.Text == "")
        {
            Lbl_Mensaje_Error.Text += "+ El Apellido Paterno del Empleado <br>";
            Datos_Validos = false;
        }
        if (string.IsNullOrEmpty(Txt_Apellido_Materno_Empleado.Text))
        {
            Lbl_Mensaje_Error.Text += "+ El Apellido Materno del Empleado <br>";
            Datos_Validos = false;
        }
        if (string.IsNullOrEmpty(Txt_Password_Empleado.Text))
        {
            Lbl_Mensaje_Error.Text += "+ Password del Empleado <br>";
            Datos_Validos = false;
        }
        if (string.IsNullOrEmpty(Txt_Confirma_Password_Empleado.Text))
        {
            Lbl_Mensaje_Error.Text += "+ Confirmacion del Password del Empleado <br>";
            Datos_Validos = false;
        }
        if (string.IsNullOrEmpty(Txt_Comentarios_Empleado.Text))
        {
            Lbl_Mensaje_Error.Text += "+ Comentarios <br>";
            Datos_Validos = false;
        }
        if (Txt_Comentarios_Empleado.Text.Length > 250)
        {
            Lbl_Mensaje_Error.Text += "+ Los comentarios proporcionados no deben ser mayor a 250 caracteres <br>";
            Datos_Validos = false;
        }
        ///---------------------------------------  Datos Personales  -----------------------------------------------
        if (string.IsNullOrEmpty(Txt_Fecha_Nacimiento_Empleado.Text) || (Txt_Fecha_Nacimiento_Empleado.Text.Trim().Equals("__/___/____")))
        {
            Lbl_Mensaje_Error.Text += "+ La Fecha de Nacimiento del Empleado <br>";
            Datos_Validos = false;
        }
        else if (!Validar_Formato_Fecha(Txt_Fecha_Nacimiento_Empleado.Text.Trim()))
        {
            Txt_Fecha_Nacimiento_Empleado.Text = "";
            Lbl_Mensaje_Error.Text += "+ Formato de Fecha de Nacimiento Incorrecto <br>";
            Datos_Validos = false;
        }

        if (Cmb_Sexo_Empleado.SelectedIndex == 0)
        {
            Lbl_Mensaje_Error.Text += "+ El Sexo del Empleado <br>";
            Datos_Validos = false;
        }
        if (!string.IsNullOrEmpty(Txt_RFC_Empleado.Text))
        {
            if (!Validar_RFC())
            {
                Lbl_Mensaje_Error.Text += "+ Formato del RFC Incorrecto <br>";
                Datos_Validos = false;
            }
        }
        if (!string.IsNullOrEmpty(Txt_CURP_Empleado.Text))
        {
            if (!Validar_CURP())
            {
                Lbl_Mensaje_Error.Text += "+ Formato del CURP Incorrecto <br>";
                Datos_Validos = false;
            }
        }
        if (Txt_Domicilio_Empleado.Text == "")
        {
            Lbl_Mensaje_Error.Text += "+ El Domicilio del Empleado <br>";
            Datos_Validos = false;
        }
        if (Txt_Colonia_Empleado.Text == "")
        {
            Lbl_Mensaje_Error.Text += "+ La Colonia del Empleado <br>";
            Datos_Validos = false;
        }
        if (!string.IsNullOrEmpty(Txt_Codigo_Postal_Empleado.Text))
        {
            if (!Validar_Codigo_Postal())
            {
                Lbl_Mensaje_Error.Text += "+ Formato del Codigo Postal Incorrecto <br>";
                Datos_Validos = false;
            }
        }
        if (Txt_Ciudad_Empleado.Text == "")
        {
            Lbl_Mensaje_Error.Text += "+ La Ciudad del Empleado <br>";
            Datos_Validos = false;
        }
        if (Txt_Estado_Empleado.Text == "")
        {
            Lbl_Mensaje_Error.Text += "+ El Estado del Empleado <br>";
            Datos_Validos = false;
        }
        if (!string.IsNullOrEmpty(Txt_Telefono_Casa_Empleado.Text))
        {
            if (!Validar_Telefono_Casa_Empleado())
            {
                Lbl_Mensaje_Error.Text += "+ Formato del Telefono de Casa Incorrecto <br>";
                Datos_Validos = false;
            }
        }
        if (!string.IsNullOrEmpty(Txt_Celular_Empleado.Text))
        {
            if (!Validar_Celular_Empleado())
            {
                Lbl_Mensaje_Error.Text += "+ Formato del Celular Incorrecto <br>";
                Datos_Validos = false;
            }
        }
        if (!string.IsNullOrEmpty(Txt_Telefono_Oficina_Empleado.Text))
        {
            if (!Validar_Telefono_Oficina_Empleado())
            {
                Lbl_Mensaje_Error.Text += "+ Formato del Telefono de Oficina Incorrecto <br>";
                Datos_Validos = false;
            }
        }
        if (!string.IsNullOrEmpty(Txt_Fax_Empleado.Text))
        {
            if (!Validar_Fax())
            {
                Lbl_Mensaje_Error.Text += "+ Formato del Fax Incorrecto <br>";
                Datos_Validos = false;
            }
        }
        if (!string.IsNullOrEmpty(Txt_Correo_Electronico_Empleado.Text))
        {
            if (!Validar_Email())
            {
                Lbl_Mensaje_Error.Text += "+ Formato del Correo Electronico Incorrecto <br>";
                Datos_Validos = false;
            }
        }
        
        if (Cmb_SAP_Unidad_Responsable.SelectedIndex <= 0)
        {
            Lbl_Mensaje_Error.Text += "+ Seleccione Unidad Responsable. <br>";
            Datos_Validos = false;
        }

        Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode("Es necesario Introducir: <br>" + Crear_Tabla_Mostrar_Errores_Pagina(Lbl_Mensaje_Error.Text));
        return Datos_Validos;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Formato_Fecha
    /// DESCRIPCION : Valida el formato de las fechas.
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 23/Noviembre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar_Formato_Fecha(String Fecha)
    {
        String Cadena_Fecha = @"^(([0-9])|([0-2][0-9])|([3][0-1]))\/(ene|feb|mar|abr|may|jun|jul|ago|sep|oct|nov|dic)\/\d{4}$";
        if (Fecha != null)
        {
            return Regex.IsMatch(Fecha, Cadena_Fecha);
        }
        else
        {
            return false;
        }
    }
    /// ********************************************************************************
    /// Nombre: Validar_Fechas
    /// Descripción: Valida que la Fecha Inicial no sea mayor que la Final
    /// Creo: Juan Alberto Hernández Negrete 
    /// Fecha Creo: 20/Octubre/2010
    /// Modifico:
    /// Fecha Modifico:
    /// Causa Modifico:
    /// ********************************************************************************
    private Boolean Validar_Fechas(String _Fecha_Inicio, String _Fecha_Fin)
    {
        DateTime Fecha_Inicio = Convert.ToDateTime(_Fecha_Inicio);
        DateTime Fecha_Fin = Convert.ToDateTime(_Fecha_Fin);
        Boolean Fecha_Valida = false;
        if (Fecha_Inicio < Fecha_Fin) Fecha_Valida = true;
        return Fecha_Valida;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 18/Noviembre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean IsNumeric(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion

    #endregion

    #region (Grid)

    #region (Grid Empleados)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Empleados_SelectedIndexChanged
    /// DESCRIPCION : Consulta los datos del Empleado que selecciono el usuario
    /// PARAMETROS  : 
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 12-Septiembre-2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Grid_Empleados_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Cat_Empleados_Negocios Rs_Consulta_Cat_Empleados = new Cls_Cat_Empleados_Negocios(); //Variable de conexión a la capa de Negocios para la consulta de los datos del empleado
        DataTable Dt_Empleados; //Variable que obtendra los datos de la consulta

        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Rs_Consulta_Cat_Empleados.P_Empleado_ID = Grid_Empleados.SelectedRow.Cells[1].Text;
            Dt_Empleados = Rs_Consulta_Cat_Empleados.Consulta_Datos_Empleado(); //Consulta los datos del empleado que fue seleccionado por el usuario
            if (Dt_Empleados.Rows.Count > 0)
            {
                //Agrega los valores de los campos a los controles correspondientes de la forma
                foreach (DataRow Registro in Dt_Empleados.Rows)
                {
                    Img_Foto_Empleado.ImageUrl = (@Registro[Cat_Empleados.Campo_Ruta_Foto].ToString().Equals("")) ? "~/paginas/imagenes/paginas/Sias_No_Disponible.JPG" : @Registro[Cat_Empleados.Campo_Ruta_Foto].ToString();
                    Img_Foto_Empleado.DataBind();
                    Txt_Ruta_Foto.Value = (@Registro[Cat_Empleados.Campo_Ruta_Foto].ToString().Equals("")) ? "~/paginas/imagenes/paginas/Sias_No_Disponible.JPG" : @Registro[Cat_Empleados.Campo_Ruta_Foto].ToString();

                    Txt_Empleado_ID.Text = Registro[Cat_Empleados.Campo_Empleado_ID].ToString();
                    if (Registro[Cat_Empleados.Campo_No_Empleado].ToString().Trim() != String.Empty) Txt_No_Empleado.Text = Registro[Cat_Empleados.Campo_No_Empleado].ToString();
                    if (Registro[Cat_Empleados.Campo_Estatus].ToString().Trim() != String.Empty) Cmb_Estatus_Empleado.SelectedValue = Registro[Cat_Empleados.Campo_Estatus].ToString();
                    if (Registro[Cat_Empleados.Campo_Nombre].ToString().Trim() != String.Empty) Txt_Nombre_Empleado.Text = Registro[Cat_Empleados.Campo_Nombre].ToString();
                    if (Registro[Cat_Empleados.Campo_Apellido_Paterno].ToString().Trim() != String.Empty) Txt_Apellido_Paterno_Empleado.Text = Registro[Cat_Empleados.Campo_Apellido_Paterno].ToString();
                    if (Registro[Cat_Empleados.Campo_Apellido_Materno].ToString().Trim() != String.Empty) Txt_Apellido_Materno_Empleado.Text = Registro[Cat_Empleados.Campo_Apellido_Materno].ToString();
                    //if (!String.IsNullOrEmpty(Registro[Cat_Empleados.Campo_Confronto].ToString())) Txt_Empleado_Confronto.Text = Registro[Cat_Empleados.Campo_Confronto].ToString();

                    if (Registro["Dependencia_ID"].ToString().Trim() != String.Empty) Cmb_SAP_Unidad_Responsable.SelectedValue = Registro["Dependencia_ID"].ToString();
                    if (Registro[Cat_Empleados.Campo_Rol_ID].ToString().Trim() != String.Empty) Cmb_Roles_Empleado.SelectedValue = Registro[Cat_Empleados.Campo_Rol_ID].ToString();
                    if (Registro["Password"].ToString().Trim() != String.Empty) Txt_Password_Empleado.Text = Registro["Password"].ToString();
                    if (Registro["Password"].ToString().Trim() != String.Empty) Txt_Confirma_Password_Empleado.Text = Registro["Password"].ToString();
                    if (Registro[Cat_Empleados.Campo_Fecha_Nacimiento].ToString().Trim() != String.Empty) Txt_Fecha_Nacimiento_Empleado.Text = String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(Registro[Cat_Empleados.Campo_Fecha_Nacimiento].ToString()));
                    if (Registro[Cat_Empleados.Campo_Sexo].ToString().Trim() != String.Empty)
                    {
                        Cmb_Sexo_Empleado.SelectedValue = Registro[Cat_Empleados.Campo_Sexo].ToString();
                    }
                    else
                    {
                        Cmb_Sexo_Empleado.SelectedIndex = 0;
                    }
                    if (Registro[Cat_Empleados.Campo_RFC].ToString().Trim() != String.Empty) Txt_RFC_Empleado.Text = Registro[Cat_Empleados.Campo_RFC].ToString();
                    if (Registro[Cat_Empleados.Campo_CURP].ToString().Trim() != String.Empty) Txt_CURP_Empleado.Text = Registro[Cat_Empleados.Campo_CURP].ToString();
                    if (Registro[Cat_Empleados.Campo_Calle].ToString().Trim() != String.Empty) Txt_Domicilio_Empleado.Text = Registro[Cat_Empleados.Campo_Calle].ToString();
                    if (Registro[Cat_Empleados.Campo_Colonia].ToString().Trim() != String.Empty) Txt_Colonia_Empleado.Text = Registro[Cat_Empleados.Campo_Colonia].ToString();
                    if (Registro[Cat_Empleados.Campo_Codigo_Postal].ToString() != null) Txt_Codigo_Postal_Empleado.Text = Registro[Cat_Empleados.Campo_Codigo_Postal].ToString();
                    if (Registro[Cat_Empleados.Campo_Ciudad].ToString().Trim() != String.Empty) Txt_Ciudad_Empleado.Text = Registro[Cat_Empleados.Campo_Ciudad].ToString();
                    if (Registro[Cat_Empleados.Campo_Estado].ToString().Trim() != String.Empty) Txt_Estado_Empleado.Text = Registro[Cat_Empleados.Campo_Estado].ToString();
                    if (Registro[Cat_Empleados.Campo_Telefono_Casa].ToString().Trim() != String.Empty) Txt_Telefono_Casa_Empleado.Text = Registro[Cat_Empleados.Campo_Telefono_Casa].ToString();
                    if (Registro[Cat_Empleados.Campo_Celular].ToString().Trim() != String.Empty) Txt_Celular_Empleado.Text = Registro[Cat_Empleados.Campo_Celular].ToString();
                    if (Registro[Cat_Empleados.Campo_Nextel].ToString().Trim() != String.Empty) Txt_Nextel_Empleado.Text = Registro[Cat_Empleados.Campo_Nextel].ToString();
                    if (Registro[Cat_Empleados.Campo_Telefono_Oficina].ToString().Trim() != String.Empty) Txt_Telefono_Oficina_Empleado.Text = Registro[Cat_Empleados.Campo_Telefono_Oficina].ToString();
                    if (Registro[Cat_Empleados.Campo_Extension].ToString().Trim() != String.Empty) Txt_Extension_Empleado.Text = Registro[Cat_Empleados.Campo_Extension].ToString();
                    if (Registro[Cat_Empleados.Campo_Fax].ToString().Trim() != String.Empty) Txt_Fax_Empleado.Text = Registro[Cat_Empleados.Campo_Fax].ToString();
                    if (Registro[Cat_Empleados.Campo_Correo_Electronico].ToString().Trim() != String.Empty) Txt_Correo_Electronico_Empleado.Text = Registro[Cat_Empleados.Campo_Correo_Electronico].ToString();
                    if (Registro[Cat_Empleados.Campo_Comentarios].ToString().Trim() != String.Empty) Txt_Comentarios_Empleado.Text = Registro[Cat_Empleados.Campo_Comentarios].ToString();
                    if (Registro["No_Licencia_Manejo"].ToString().Trim() != String.Empty) Txt_No_Licencia.Text = Registro["No_Licencia_Manejo"].ToString();
                    String tipo_lic = Registro["Tipo_Licencia"].ToString().Trim();
                    if (Registro["Tipo_Licencia"].ToString().Trim() != String.Empty)
                    {
                        Cmb_Tipo_Licencia.Text = Registro["Tipo_Licencia"].ToString();
                    }
                    else
                    {
                        Cmb_Tipo_Licencia.SelectedIndex = 0;
                    }
                    
                    if (Registro["Fecha_Vencimiento_Licencia"].ToString().Trim() != String.Empty) Txt_Fecha_Vencimiento.Text = String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(Registro["Fecha_Vencimiento_Licencia"].ToString()));

                    //SAP Código Programático.------------------------------------------------------------------------------------
                    if (!String.IsNullOrEmpty(Registro[Cat_Empleados.Campo_Dependencia_ID].ToString()))
                    {
                        Cmb_SAP_Unidad_Responsable.SelectedIndex = Cmb_SAP_Unidad_Responsable.Items.IndexOf(
                            Cmb_SAP_Unidad_Responsable.Items.FindByValue(Registro[Cat_Empleados.Campo_Dependencia_ID].ToString()));
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Empleados_PageIndexChanging
    /// DESCRIPCION : Cambia la pagina de la tabla de empleados
    ///               
    /// PARAMETROS  : 
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 10-Septiembre-2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Grid_Empleados_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Limpia_Controles();                        //Limpia todos los controles de la forma
            Grid_Empleados.PageIndex = e.NewPageIndex; //Indica la Página a visualizar
            Llena_Grid_Empleados();                    //Carga los Empleados que estan asignados a la página seleccionada

            Grid_Empleados.SelectedIndex = -1;
            Img_Foto_Empleado.ImageUrl = "~/paginas/imagenes/paginas/Sias_No_Disponible.JPG";
            Img_Foto_Empleado.DataBind();
            Txt_Ruta_Foto.Value = "";
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Llena_Grid_Empleados
    /// DESCRIPCION : Llena el grid con los Empleados que fueron obtenidos de la consulta
    ///               Consulta_Empleados
    /// PARAMETROS  : 
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 10-Septiembre-2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Llena_Grid_Empleados()
    {
        DataTable Dt_Empleados; //Variable que obtendra los datos de la consulta 
        try
        {
            Grid_Empleados.Columns[1].Visible = true;
            Grid_Empleados.DataBind();
            Dt_Empleados = (DataTable)Session["Consulta_Empleados"];
            Grid_Empleados.DataSource = Dt_Empleados;
            Grid_Empleados.DataBind();
            Grid_Empleados.Columns[1].Visible = false;
            Grid_Empleados.SelectedIndex = -1;
        }
        catch (Exception ex)
        {
            throw new Exception("Llena_Grid_Empleados " + ex.Message.ToString(), ex);
        }
    }
    /// **************************************************************************************************************************************
    /// NOMBRE: Grid_Empleados_Sorting
    /// 
    /// DESCRIPCIÓN: Ordena las columnas en orden ascendente o descendente.
    /// 
    /// CREÓ:   Juan Alberto Hernández Negrete.
    /// FECHA CREÓ: 18/Febrero/2011 19:04 pm.
    /// MODIFICÓ:
    /// FECHA MODIFICÓ:
    /// CAUSA MODIFICACIÓN:
    /// **************************************************************************************************************************************
    protected void Grid_Empleados_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable Dt_Calendario_Nominas = (Grid_Empleados.DataSource as DataTable);

        if (Dt_Calendario_Nominas != null)
        {
            DataView Dv_Calendario_Nominas = new DataView(Dt_Calendario_Nominas);
            String Orden = ViewState["SortDirection"].ToString();

            if (Orden.Equals("ASC"))
            {
                Dv_Calendario_Nominas.Sort = e.SortExpression + " " + "DESC";
                ViewState["SortDirection"] = "DESC";
            }
            else
            {
                Dv_Calendario_Nominas.Sort = e.SortExpression + " " + "ASC";
                ViewState["SortDirection"] = "ASC";
            }

            Grid_Empleados.DataSource = Dv_Calendario_Nominas;
            Grid_Empleados.DataBind();
        }
    }
    #endregion

    #endregion

    #region (Eventos)

    #region (Botones)

    #region (Operacion [Alta - Modificar - Eliminar - Consultar])
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Nuevo_Click
    /// DESCRIPCION : Alta de Empleado
    /// PARAMETROS  : 
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 25-Agosto-2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Nuevo_Click(object sender, EventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (Btn_Nuevo.ToolTip == "Nuevo")
            {
                Habilitar_Controles("Nuevo"); //Habilita los controles para la introducción de datos por parte del usuario
                Limpia_Controles();           //Limpia los controles de la forma para poder introducir nuevos datos
            }
            else
            {
                //Si todos los campos requeridos fueron proporcionados por el usuario entonces da de alta los mismo en la base de datos
                if (Validar_Datos())
                {
                    if (Txt_Confirma_Password_Empleado.Text != "" || Txt_Password_Empleado.Text != "")
                    {
                        if (Txt_Password_Empleado.Text == "")
                        {
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                            Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
                            Lbl_Mensaje_Error.Text += "+ El password del Empleado";
                            return;
                        }
                        if (Txt_Confirma_Password_Empleado.Text == "")
                        {
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                            Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
                            Lbl_Mensaje_Error.Text += "+ La confirmación del password del Empleado";
                            return;
                        }
                        if (Txt_Password_Empleado.Text != Txt_Confirma_Password_Empleado.Text)
                        {
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                            Lbl_Mensaje_Error.Text = "+ El password y su confirmación deben ser iguales, favor de verificar";
                            return;
                        }
                    }
                    if (Validar_Datos())
                    {
                        Alta_Empleado(); //Da de alta los datos proporcionados por el usuario
                        Limpia_Controles();    //Limpia los controles de la forma
                        Grid_Empleados.DataSource = new DataTable();
                        Grid_Empleados.DataBind();
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                    }
                }
                //Si faltaron campos por capturar envia un mensaje al usuario indicando que campos faltaron de proporcionar
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Modificar_Click
    /// DESCRIPCION : Modificar al Empleado Seleccionado
    /// PARAMETROS  : 
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 25-Agosto-2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Modificar_Click(object sender, EventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (Btn_Modificar.ToolTip == "Modificar")
            {
                if (Txt_Empleado_ID.Text != "")
                {
                    Habilitar_Controles("Modificar"); //Habilita los controles para la modificación de los datos
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Seleccione el Empleado que desea modificar sus datos <br>";
                }
            }
            else
            {
                //Si todos los campos requeridos fueron proporcionados por el usuario entonces modifica estos en la BD
                if (Validar_Datos())
                {
                    if (Txt_Confirma_Password_Empleado.Text != "" || Txt_Password_Empleado.Text != "")
                    {
                        if (Txt_Password_Empleado.Text == "")
                        {
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                            Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
                            Lbl_Mensaje_Error.Text += "+ El password del Empleado";
                            return;
                        }
                        if (Txt_Confirma_Password_Empleado.Text == "")
                        {
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                            Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
                            Lbl_Mensaje_Error.Text += "+ La confirmación del password del Empleado";
                            return;
                        }
                        if (Txt_Password_Empleado.Text != Txt_Confirma_Password_Empleado.Text)
                        {
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                            Lbl_Mensaje_Error.Text = "El password y su confirmación deben ser iguales, favor de verificar";
                            return;
                        }
                    }
                    if (Validar_Datos())
                    {
                        Modificar_Empleado(); //Modifica los datos del Empleado con los datos proporcionados por el usuario   
                        Grid_Empleados.DataSource = new DataTable();
                        Grid_Empleados.DataBind();
                    }
                    else {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                    }
                }
                //Si faltaron campos por capturar envia un mensaje al usuario indicando que campos faltaron de proporcionar
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Eliminar_Click
    /// DESCRIPCION : Eliminar al Empleado Seleccionado
    /// PARAMETROS  : 
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 25-Agosto-2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Eliminar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            //Si el usuario selecciono un Empleado entonces lo elimina de la base de datos
            if (Txt_Empleado_ID.Text != "")
            {
                Eliminar_Empleado(); //Elimina el Empleado que fue seleccionado por el usuario
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Empleados", "alert('El empleado fue puesto como INACTIVO');", true);
                Grid_Empleados.DataSource = new DataTable();
                Grid_Empleados.DataBind();
            }
            //Si el usuario no selecciono algún Empleado manda un mensaje indicando que es necesario que seleccione algun para
            //poder eliminar
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Seleccione el Empleado que desea eliminar <br>";
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Salir_Click
    /// DESCRIPCION : Salir o Cancelar la Operacion Actual
    /// PARAMETROS  : 
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 25-Agosto-2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, EventArgs e)
    {
        try
        {
            if (Btn_Salir.ToolTip == "Salir")
            {
                Session.Remove("Consulta_Empleados");
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Inicializa_Controles();//Habilita los controles para la siguiente operación del usuario en el catálogo
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

        protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
        {
            Cls_Cat_Empleados_Negocios Rs_Consulta_Ca_Empleados = new Cls_Cat_Empleados_Negocios(); //Variable de conexión hacia la capa de Negocios
            DataTable Dt_Empleados; //Variable que obtendra los datos de la consulta 

            try
            {
                if (!string.IsNullOrEmpty(Txt_Busqueda.Text.Trim()))
                {
                    if (Txt_Busqueda.Text.Trim().Length > 5)
                        Rs_Consulta_Ca_Empleados.P_Nombre = Txt_Busqueda.Text.Trim();
                    else
                        Rs_Consulta_Ca_Empleados.P_No_Empleado = Txt_Busqueda.Text.Trim();
                }
                Dt_Empleados = Rs_Consulta_Ca_Empleados.Consulta_Empleados_General(); //Consulta todos los Empleados que coindican con lo proporcionado por el usuario
                Session["Consulta_Empleados"] = Dt_Empleados;
                Llena_Grid_Empleados();
                Txt_Busqueda.Text = "";
            }
            catch (Exception ex)
            {
                throw new Exception("Consulta_Empleados " + ex.Message.ToString(), ex);
            }
        }
    #endregion

    #endregion

    #region (Combos)

        protected void Cmb_SAP_Unidad_Responsable_SelectedIndexChanged(object sender, EventArgs e)
        {
            Cls_Cat_Dependencias_Negocio Obj_Dependencias = new Cls_Cat_Dependencias_Negocio();
            DataTable Dt_Depedencias = null;
            String Clave_Unidad_Responsable = String.Empty;
            String Area_Funcional = String.Empty;

            try
            {
                if (Cmb_SAP_Unidad_Responsable.SelectedIndex > 0)
                {
                    Obj_Dependencias.P_Dependencia_ID = Cmb_SAP_Unidad_Responsable.SelectedValue.Trim();
                    Dt_Depedencias = Obj_Dependencias.Consulta_Dependencias();

                    if (Dt_Depedencias is DataTable)
                    {
                        if (Dt_Depedencias.Rows.Count > 0)
                        {
                            foreach (DataRow DEPENDENCIA in Dt_Depedencias.Rows)
                            {
                                if (DEPENDENCIA is DataRow)
                                {
                                    if (!String.IsNullOrEmpty(DEPENDENCIA[Cat_Dependencias.Campo_Clave].ToString()))
                                        Clave_Unidad_Responsable = DEPENDENCIA[Cat_Dependencias.Campo_Clave].ToString();

                                    if (!String.IsNullOrEmpty(DEPENDENCIA[Cat_Dependencias.Campo_Area_Funcional_ID].ToString()))
                                    {
                                        Area_Funcional = DEPENDENCIA[Cat_Dependencias.Campo_Area_Funcional_ID].ToString();
                                    }
                                }
                            }
                        }
                    }
                }      
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al seleccionar un elemento de la lista de Unidades Responsables. Error: [" + Ex.Message + "]");
            }
        }

    #endregion

    #endregion
}
