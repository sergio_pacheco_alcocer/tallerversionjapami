﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" 
    AutoEventWireup="true" CodeFile="Frm_Cat_Empleados.aspx.cs" Inherits="paginas_Nomina_Frm_Cat_Empleados" 
    Title="Catálogo de Empleados"  Culture="auto" UICulture="auto"%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">

    <script type="text/javascript" language="javascript">
        //Metodo para mantener los calendarios en una capa mas alat.
         function calendarShown(sender, args){
            sender._popupBehavior._element.style.zIndex = 10000005;
        }
        
        function pageLoad(sender, args) {
            $('textarea[id*=Txt_Comentarios_Empleado]').keyup(function() {var Caracteres =  $(this).val().length; if (Caracteres > 250) {this.value = this.value.substring(0, 250); Caracteres =  $(this).val().length;  $(this).css("background-color", "Yellow");$(this).css("color", "Red");}else{$(this).css("background-color", "White");$(this).css("color", "Black");}$('#Contador_Caracteres_Comentarios').text('Carácteres Ingresados [ ' + Caracteres + ' ]/[ 250 ]');});
            
            $('input[id$=Txt_No_Empleado]').bind("blur", function(){
                var Ceros = "";
                if($(this).val() != undefined){
                    for(i=0;    i<(6-$(this).val().length);    i++){
                        Ceros += '0';
                    }
                    $(this).val(Ceros + $(this).val());
                    Ceros = "";
                }
            });
            
            $('input[id$=Txt_Empleado_Confronto]').css("display", "none");
            Generar_CONFRONTO();
        }
        
        function Generar_CONFRONTO(){
            var CONFRONTO = "";
            var FULL_NAME = "";
            var i = 0;

            try{
                FULL_NAME = $('input[id$=Txt_Apellido_Paterno_Empleado]').val() + " " + 
                            $('input[id$=Txt_Apellido_Materno_Empleado]').val() + " " +
                            $('input[id$=Txt_Nombre_Empleado]').val();
                
                if(FULL_NAME.replace(/^\s*|\s*$/g,"") != ""){
                    FULL_NAME = FULL_NAME.toUpperCase().toString().replace(/^\s*|\s*$/g,"");
                    var Palabras = FULL_NAME.split(' ');
                    
                    for(i=0; i<Palabras.length; i++){
                        CONFRONTO += Palabras[i].substring(0,1);
                    }
                    
                    $('input[id$=Txt_Empleado_Confronto]').css("display", "Block");
                    $('input[id$=Txt_Empleado_Confronto]').val(CONFRONTO);
                    $('input[id$=Txt_Empleado_Confronto]').css("color", "#000000");
                    $('input[id$=Txt_Empleado_Confronto]').css("font-size", "15px");
                    $('input[id$=Txt_Empleado_Confronto]').css("font-family", "Elephant");
                    $('input[id$=Txt_Empleado_Confronto]').css("text-align", "Center");
                    $('input[id$=Txt_Empleado_Confronto]').css("background-color", "#f5f5f5");
                    $('input[id$=Txt_Empleado_Confronto]').css('border-style', 'solid');
                    $('input[id$=Txt_Empleado_Confronto]').css('border-color', 'Black');
                    $('input[id$=Txt_Empleado_Confronto]').css('height', 20);
                    
                }else{
                    $('input[id$=Txt_Empleado_Confronto]').val('');
                    $('input[id$=Txt_Empleado_Confronto]').css("display", "none");
                }
            }catch(e){
                alert("Error al generar el CONFRONTO del empleado. Error: " + e + "]");
            }
        }
</script>

   <!--SCRIPT PARA LA VALIDACION QUE NO EXPERE LA SESSION-->  
   <script language="javascript" type="text/javascript">
    <!--
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_Session.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para matener la sesión activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);
        
    //-->
   </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">

    <cc1:ToolkitScriptManager ID="ScriptManager_Empleados" runat="server" EnableScriptGlobalization = "true" EnableScriptLocalization = "True"/>
    <asp:UpdatePanel ID="Upd_Panel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>  
        
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            
            <div id="Div_Empleados" style="background-color:#ffffff; width:100%; height:100%;font-size:10px;" >
            <div id="tooltip"></div>
                <table width="100%" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">
                            Catálogo de Empleados
                        </td>
                    </tr>
                    <tr>
                        <td >&nbsp;
                            <asp:UpdatePanel ID="Upnl_Mensajes_Error" runat="server" UpdateMode="Always" RenderMode="Inline">
                                <ContentTemplate>                         
                                    <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                                    <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"/>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr> 
                </table>

                <table width="98%"  border="0" cellspacing="0">
                         <tr align="center">
                             <td>
                                 <div align="right" class="barra_busqueda">
                                      <table style="width:100%;height:28px;">
                                        <tr>
                                          <td align="left" style="width:59%;"> 
                                            <asp:UpdatePanel ID="Upnl_Botones_Operacion" runat="server" UpdateMode="Conditional" RenderMode="Inline" >
                                                <ContentTemplate> 
                                                    <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" CssClass="Img_Button"
                                                        ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" onclick="Btn_Nuevo_Click" />
                                                    <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" CssClass="Img_Button"
                                                        ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" onclick="Btn_Modificar_Click" />
                                                    <asp:ImageButton ID="Btn_Eliminar" runat="server" ToolTip="Eliminar" CssClass="Img_Button"
                                                        ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png"
                                                        OnClick="Btn_Eliminar_Click"/>
                                                    <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button"
                                                        ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" onclick="Btn_Salir_Click"/>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                          </td>
                                          <td align="right" style="width:41%;">
                                            <table style="width:100%;height:28px;">
                                                <tr>
                                                    <td align = "right">
                                                        Búsqueda:
                                                        <asp:TextBox ID="Txt_Busqueda" runat="server" Width="180"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda" runat="server" WatermarkCssClass="watermarked"
                                                            WatermarkText="<Nombre o No. Empleado>" TargetControlID="Txt_Busqueda" />
                                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda" runat="server" TargetControlID="Txt_Busqueda" 
                                                            FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                                            ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ "/>
                                                        <asp:ImageButton ID="Btn_Buscar" runat="server" ToolTip="Consultar"
                                                            ImageUrl="~/paginas/imagenes/paginas/busqueda.png" onclick="Btn_Buscar_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                           </td>
                                         </tr>
                                      </table>
                                    </div>
                             </td>
                         </tr>
                </table>
              
            <asp:UpdatePanel ID="Upnl_Catalogo_Empleados" runat="server" UpdateMode="Conditional">
                <ContentTemplate>             
                <asp:Panel ID="Pnl_Datos_Generales" runat="server"  GroupingText="Datos Generales" Width="97%">
                    <table>
                        <tr>
                            <td style="width:10%;vertical-align:top;" align="center">
                                <asp:Image ID="Img_Foto_Empleado" runat="server"  Width="70px" Height="85px"
                                    ImageUrl="~/paginas/imagenes/paginas/Sias_No_Disponible.JPG" BorderWidth="1px"/>
                                <asp:HiddenField ID="Txt_Ruta_Foto" runat="server" />         
                                <asp:ImageButton ID="Btn_Subir_Foto" runat="server" ToolTip="Mostrar Foto del Empleado"  OnClick="Subir_Foto_Click" 
                                     ImageUrl="../imagenes/paginas/Sias_Actualizar.png" style="cursor:hand;"/>
                            </td>
                            <td style="width:90%;">
                                <table width="100%" class="estilo_fuente">
                                    <tr>
                                        <td style="width:20%;text-align:left;">
                                            Empleado ID
                                        </td>
                                        <td style="width:30%;text-align:left;">
                                            <asp:TextBox ID="Txt_Empleado_ID" runat="server" ReadOnly="True" Width="98%"/>
                                        </td>
                                        <td style="width:20%;text-align:left;">
                                            
                                        </td>
                                        <td style="width:30%;text-align:left;">
                                            <asp:TextBox ID="Txt_Empleado_Confronto" runat="server" Width="98%" Enabled="false"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:20%;text-align:left;">
                                            *No Empleado
                                        </td>
                                        <td style="width:30%;text-align:left;">
                                            <asp:TextBox ID="Txt_No_Empleado" runat="server" Width="98%" TabIndex="1" MaxLength="6"/> 
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Empleadoo" runat="server"
                                                TargetControlID="Txt_No_Empleado" FilterType="Numbers"/>
                                        </td>
                                        <td style="width:20%;text-align:left;">
                                            &nbsp;&nbsp;Estatus
                                        </td>
                                        <td style="width:30%;text-align:left;">
                                            <asp:DropDownList ID="Cmb_Estatus_Empleado" runat="server" Width="101%" TabIndex="2">
                                                <asp:ListItem>&lt; - Seleccione - &gt;</asp:ListItem>
                                                <asp:ListItem>ACTIVO</asp:ListItem>
                                                <asp:ListItem>INACTIVO</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:20%;text-align:left;">
                                            *Nombre
                                        </td>
                                        <td style="width:30%;text-align:left;" colspan="3">
                                            <asp:TextBox ID="Txt_Nombre_Empleado" runat="server" TabIndex="3" MaxLength="100" Width="99%"
                                                onkeyup="javascript:Generar_CONFRONTO();"/>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Empleado" runat="server" TargetControlID="Txt_Nombre_Empleado" 
                                                FilterType="Custom, UppercaseLetters, LowercaseLetters" ValidChars="ÑñáéíóúÁÉÍÓÚ "/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:20%;text-align:left;">
                                            *Apellido Paterno
                                        </td>
                                        <td style="width:30%;text-align:left;">
                                            <asp:TextBox ID="Txt_Apellido_Paterno_Empleado" runat="server" TabIndex="4" MaxLength="100" Width="98%"
                                                onkeyup="javascript:Generar_CONFRONTO();"/>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Apellido_Paterno_Empleado" runat="server" 
                                                TargetControlID="Txt_Apellido_Paterno_Empleado" FilterType="Custom, UppercaseLetters, LowercaseLetters" 
                                                ValidChars="ÑñáéíóúÁÉÍÓÚ "/>
                                        </td>
                                        <td style="width:20%;text-align:left;">
                                            &nbsp;&nbsp;Apellido Materno
                                        </td>
                                        <td style="width:30%;text-align:left;">
                                            <asp:TextBox ID="Txt_Apellido_Materno_Empleado" runat="server" TabIndex="5" MaxLength="100" Width="98%"
                                                onkeyup="javascript:Generar_CONFRONTO();"/>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Apellido_Materno_Empleado" runat="server" 
                                                TargetControlID="Txt_Apellido_Materno_Empleado" FilterType="Custom, UppercaseLetters, LowercaseLetters" 
                                                ValidChars="ÑñáéíóúÁÉÍÓÚ "/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:20%;text-align:left;">
                                            *Unidad Responsable
                                        </td>
                                        <td style="width:30%;text-align:left;">
                                            <asp:DropDownList ID="Cmb_SAP_Unidad_Responsable" runat="server" TabIndex="6" Width="100%" OnSelectedIndexChanged="Cmb_SAP_Unidad_Responsable_SelectedIndexChanged" />
                                        </td>
                                        <td style="width:20%;text-align:left;">
                                            &nbsp;&nbsp;*Rol
                                        </td>
                                        <td style="width:30%;text-align:left;">
                                            <asp:DropDownList ID="Cmb_Roles_Empleado" runat="server" Width="100%" TabIndex="7"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:20%;text-align:left;">
                                            Password
                                        </td>
                                        <td style="width:30%;text-align:left;">
                                            <asp:TextBox ID="Txt_Password_Empleado" runat="server" TabIndex="8" MaxLength="100" 
                                                Width="98%" TextMode="Password"/>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Password_Empleado" runat="server" 
                                                TargetControlID="Txt_Password_Empleado" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                                ValidChars="ÑñáéíóúÁÉÍÓÚ!¡#$%&/()=¿?.,:;-+* "/>
                                        </td>
                                        <td style="width:20%;text-align:left;">
                                            &nbsp;&nbsp;Confirmar Password
                                        </td>
                                        <td style="width:30%;text-align:left;">
                                            <asp:TextBox ID="Txt_Confirma_Password_Empleado" runat="server" TabIndex="9" MaxLength="100" 
                                                Width="98%" TextMode="Password"/>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Confirma_Password_Empleado" runat="server" 
                                                TargetControlID="Txt_Confirma_Password_Empleado" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                                ValidChars="ÑñáéíóúÁÉÍÓÚ!¡#$%&/()=¿?.,:;-+* " />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:20%;text-align:left;vertical-align:top;">
                                            Comentarios
                                        </td>
                                        <td colspan="3" style="width:80%;text-align:left;">
                                            <asp:TextBox ID="Txt_Comentarios_Empleado" runat="server" TabIndex="10" MaxLength="250"
                                                TextMode="MultiLine" Width="100%" Height="60px" />
                                            <span id="Contador_Caracteres_Comentarios" class="watermarked"></span>
                                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Comentarios_Empleado" runat="server" 
                                                TargetControlID ="Txt_Comentarios_Empleado" WatermarkText="Límite de Caractes 250" 
                                                WatermarkCssClass="watermarked"/>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Comentarios_Empleado" runat="server" 
                                                TargetControlID="Txt_Comentarios_Empleado" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                                ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ "/>
                                        </td>
                                    </tr>    
                                    <tr>                    
                                        <td style="width:20%;text-align:left;">
                                            Buscar Foto
                                        </td>
                                        <td style="width:80%;text-align:left;" colspan="3">
                                            <cc1:AsyncFileUpload ID="Async_Foto_Empleado" runat="server"  Width="550px" />
                                        </td>
                                    </tr>
                                    <caption>
                                        <br />
                                        <tr>
                                            <td style="width:20%;text-align:left;">
                                                *F. Nacimiento
                                            </td>
                                            <td style="width:30%;text-align:left;">
                                                <asp:TextBox ID="Txt_Fecha_Nacimiento_Empleado" runat="server" Height="18px" 
                                                    MaxLength="11" TabIndex="11" Width="84%" />
                                                <cc1:TextBoxWatermarkExtender ID="TBWE_Txt_Fecha_Nacimiento_Empleado" 
                                                    runat="server" Enabled="True" TargetControlID="Txt_Fecha_Nacimiento_Empleado" 
                                                    WatermarkCssClass="watermarked" WatermarkText="Dia/Mes/Año" />
                                                <cc1:CalendarExtender ID="CE_Txt_Fecha_Nacimiento_Empleado" runat="server" 
                                                    Enabled="True" Format="dd/MMM/yyyy" PopupButtonID="Btn_Fecha_Nacimiento" 
                                                    TargetControlID="Txt_Fecha_Nacimiento_Empleado" />
                                                <asp:ImageButton ID="Btn_Fecha_Nacimiento" runat="server" 
                                                    CausesValidation="false" Height="18px" 
                                                    ImageUrl="../imagenes/paginas/SmallCalendar.gif" style="vertical-align:top;" />
                                                <cc1:MaskedEditExtender ID="Mee_Txt_Fecha_Nacimiento_Empleado" runat="server" 
                                                    ClearMaskOnLostFocus="false" Enabled="True" Filtered="/" Mask="99/LLL/9999" 
                                                    MaskType="None" TargetControlID="Txt_Fecha_Nacimiento_Empleado" 
                                                    UserDateFormat="DayMonthYear" UserTimeFormat="None" />
                                                <cc1:MaskedEditValidator ID="Mev_Txt_Fecha_Nacimiento_Empleado" runat="server" 
                                                    ControlExtender="Mee_Txt_Fecha_Nacimiento_Empleado" 
                                                    ControlToValidate="Txt_Fecha_Nacimiento_Empleado" 
                                                    EmptyValueMessage="Fecha Requerida" Enabled="true" 
                                                    InvalidValueMessage="Fecha Nacimiento Invalida" IsValidEmpty="false" 
                                                    style="font-size:10px;background-color:#F0F8FF;color:Black;font-weight:bold;" 
                                                    TooltipMessage="Ingrese o Seleccione la Fecha Nacimiento" />
                                            </td>
                                            <td style="width:20%;text-align:left;">
                                                &nbsp;&nbsp;*Sexo
                                            </td>
                                            <td style="width:30%;text-align:left;">
                                                <asp:DropDownList ID="Cmb_Sexo_Empleado" runat="server" TabIndex="11" 
                                                    Width="100%">
                                                    <asp:ListItem>&lt;- Seleccionar -&gt;</asp:ListItem>
                                                    <asp:ListItem>MASCULINO</asp:ListItem>
                                                    <asp:ListItem>FEMENINO</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:20%;text-align:left;">
                                                RFC
                                            </td>
                                            <td style="width:30%;text-align:left;">
                                                <asp:TextBox ID="Txt_RFC_Empleado" runat="server" MaxLength="13" 
                                                    onblur="$('input[id$=Txt_RFC_Empleado]').filter(function(){if(!this.value.match(/^[a-zA-Z]{3,4}(\d{6})((\D|\d){3})?$/))$(this).val('');});" 
                                                    onkeyup="this.value = this.value.toUpperCase();" TabIndex="12" Width="98%" />
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_RFC_Empleado" runat="server" 
                                                    Enabled="True" FilterType="Numbers, UppercaseLetters, LowercaseLetters" 
                                                    TargetControlID="Txt_RFC_Empleado" ValidChars=" " />
                                            </td>
                                            <td style="width:20%;text-align:left;">
                                                &nbsp;&nbsp;C.U.R.P.
                                            </td>
                                            <td style="width:30%;text-align:left;">
                                                <asp:TextBox ID="Txt_CURP_Empleado" runat="server" MaxLength="18" 
                                                    onblur="$('input[id$=Txt_CURP_Empleado]').filter(function(){if(!this.value.match(/^[a-zA-Z]{4}(\d{6})([a-zA-Z]{6})(\d{2})?$/))$(this).val('');});" 
                                                    onkeyup="this.value = this.value.toUpperCase();" TabIndex="13" Width="98%" />
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_CURP_Empleado" runat="server" 
                                                    Enabled="True" FilterType="Numbers, UppercaseLetters, LowercaseLetters" 
                                                    TargetControlID="Txt_CURP_Empleado" ValidChars=" " />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:20%;text-align:left;">
                                                *Domicilio
                                            </td>
                                            <td colspan="3" style="width:80%;text-align:left;">
                                                <asp:TextBox ID="Txt_Domicilio_Empleado" runat="server" MaxLength="100" 
                                                    TabIndex="14" Width="99.5%" />
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Domicilio_Empleado" runat="server" 
                                                    Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters" 
                                                    TargetControlID="Txt_Domicilio_Empleado" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ# " />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:20%;text-align:left;">
                                                *Colonia
                                            </td>
                                            <td style="width:30%;text-align:left;">
                                                <asp:TextBox ID="Txt_Colonia_Empleado" runat="server" MaxLength="100" 
                                                    TabIndex="15" Width="98%" />
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Colonia_Empleado" runat="server" 
                                                    Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters" 
                                                    TargetControlID="Txt_Colonia_Empleado" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ " />
                                            </td>
                                            <td style="width:20%;text-align:left;">
                                                &nbsp;&nbsp;CP
                                            </td>
                                            <td style="width:30%;text-align:left;">
                                                <asp:TextBox ID="Txt_Codigo_Postal_Empleado" runat="server" MaxLength="5" 
                                                    onblur="$('input[id$=Txt_Codigo_Postal_Empleado]').filter(function(){if(!this.value.match(/^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/))$(this).val('');});" 
                                                    TabIndex="16" Width="98%" />
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Codigo_Postal_Empleado" runat="server" 
                                                    Enabled="True" FilterType="Numbers" 
                                                    TargetControlID="Txt_Codigo_Postal_Empleado" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:20%;text-align:left;">
                                                *Ciudad
                                            </td>
                                            <td style="width:30%;text-align:left;">
                                                <asp:TextBox ID="Txt_Ciudad_Empleado" runat="server" MaxLength="50" 
                                                    TabIndex="17" Width="98%" />
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Ciudad_Empleado" runat="server" 
                                                    Enabled="True" FilterType="UppercaseLetters, LowercaseLetters" 
                                                    TargetControlID="Txt_Ciudad_Empleado" ValidChars=" " />
                                            </td>
                                            <td style="width:20%;text-align:left;">
                                                &nbsp;&nbsp;*Estado
                                            </td>
                                            <td style="width:30%;text-align:left;">
                                                <asp:TextBox ID="Txt_Estado_Empleado" runat="server" MaxLength="50" 
                                                    TabIndex="18" Width="98%" />
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Estado_Empleado" runat="server" 
                                                    Enabled="True" FilterType="UppercaseLetters, LowercaseLetters" 
                                                    TargetControlID="Txt_Estado_Empleado" ValidChars=" " />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:20%;text-align:left;">
                                                Tel. Casa
                                            </td>
                                            <td style="width:30%;text-align:left;">
                                                <asp:TextBox ID="Txt_Telefono_Casa_Empleado" runat="server" MaxLength="20" 
                                                    onblur="$('input[id$=Txt_Telefono_Casa_Empleado]').filter(function(){if(!this.value.match(/^[0-9]{0,3}-? ?[0-9]{6,7}$/))$(this).val('');});" 
                                                    TabIndex="19" Width="98%" />
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Telefono_Casa_Empleado" runat="server" 
                                                    Enabled="True" FilterType="Custom, Numbers" 
                                                    TargetControlID="Txt_Telefono_Casa_Empleado" ValidChars="- " />
                                            </td>
                                            <td style="width:20%;text-align:left;">
                                                &nbsp;&nbsp;Celular
                                            </td>
                                            <td style="width:30%;text-align:left;">
                                                <asp:TextBox ID="Txt_Celular_Empleado" runat="server" MaxLength="20" 
                                                    onblur="$('input[id$=Txt_Celular_Empleado]').filter(function(){if(!this.value.match(/^[0-9]{0,3}-? ?[0-9]{6,7}$/))$(this).val('');});" 
                                                    TabIndex="20" Width="98%" />
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Celular_Empleado" runat="server" 
                                                    Enabled="True" FilterType="Custom, Numbers" 
                                                    TargetControlID="Txt_Celular_Empleado" ValidChars="- " />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:20%;text-align:left;">
                                                Nextel
                                            </td>
                                            <td style="width:30%;text-align:left;">
                                                <asp:TextBox ID="Txt_Nextel_Empleado" runat="server" MaxLength="20" 
                                                    TabIndex="21" Width="98%" />
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nextel_Empleado" runat="server" 
                                                    Enabled="True" FilterType="Custom, Numbers" 
                                                    TargetControlID="Txt_Nextel_Empleado" ValidChars="*- " />
                                            </td>
                                            <td style="width:20%;text-align:left;">
                                                &nbsp;&nbsp;Tel. Oficina
                                            </td>
                                            <td style="width:30%;text-align:left;">
                                                <asp:TextBox ID="Txt_Telefono_Oficina_Empleado" runat="server" MaxLength="20" 
                                                    onblur="$('input[id$=Txt_Telefono_Oficina_Empleado]').filter(function(){if(!this.value.match(/^[0-9]{0,3}-? ?[0-9]{6,7}$/))$(this).val('');});" 
                                                    TabIndex="22" Width="98%" />
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Telefono_Oficina_Empleado" 
                                                    runat="server" Enabled="True" FilterType="Custom, Numbers" 
                                                    TargetControlID="Txt_Telefono_Oficina_Empleado" ValidChars="- " />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:20%;text-align:left;">
                                                Extensión
                                            </td>
                                            <td style="width:20%;text-align:left;">
                                                <asp:TextBox ID="Txt_Extension_Empleado" runat="server" MaxLength="10" 
                                                    TabIndex="23" Width="98%" />
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Extension_Empleado" runat="server" 
                                                    Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters" 
                                                    TargetControlID="Txt_Extension_Empleado" ValidChars="- " />
                                            </td>
                                            <td style="width:20%;text-align:left;">
                                                &nbsp;&nbsp;Fax
                                            </td>
                                            <td style="width:20%;text-align:left;">
                                                <asp:TextBox ID="Txt_Fax_Empleado" runat="server" MaxLength="20" TabIndex="24" 
                                                    Width="98%" />
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Fax_Empleado" runat="server" 
                                                    Enabled="True" FilterType="Numbers" TargetControlID="Txt_Fax_Empleado" 
                                                    ValidChars="- " />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:20%;text-align:left;">
                                                E-Mail
                                            </td>
                                            <td colspan="3" style="width:80%;text-align:left;">
                                                <asp:TextBox ID="Txt_Correo_Electronico_Empleado" runat="server" 
                                                    MaxLength="100" 
                                                    onblur="$('input[id$=Txt_Correo_Electronico_Empleado]').filter(function(){if(!this.value.match(/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/))$(this).val('');});" 
                                                    TabIndex="25" Width="99.5%" />
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Correo_Electronico_Empleado" 
                                                    runat="server" Enabled="True" 
                                                    FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters" 
                                                    TargetControlID="Txt_Correo_Electronico_Empleado" ValidChars=".@_-" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:20%;text-align:left;">
                                                No. Licencia Manejo
                                            </td>
                                            <td style="width:20%;text-align:left;">
                                                <asp:TextBox ID="Txt_No_Licencia" runat="server" MaxLength="20" TabIndex="25" 
                                                    Width="58%" />
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Licencia" runat="server" 
                                                    Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters" 
                                                    TargetControlID="Txt_No_Licencia" ValidChars="- " />
                                                <asp:DropDownList ID="Cmb_Tipo_Licencia" runat="server" TabIndex="25" 
                                                    Width="35%">
                                                    <asp:ListItem>&lt;- Seleccionar -&gt;</asp:ListItem>
                                                    <asp:ListItem>TIPO A</asp:ListItem>
                                                    <asp:ListItem>TIPO B</asp:ListItem>
                                                    <asp:ListItem>TIPO C</asp:ListItem>
                                                    <asp:ListItem>TIPO D</asp:ListItem>
                                                    <asp:ListItem>PERMISO A</asp:ListItem>
                                                    <asp:ListItem>PERMISO D</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width:20%;text-align:left;">
                                                &nbsp;&nbsp;Fecha Vencimiento
                                            </td>
                                            <td style="width:20%;text-align:left;">
                                                <asp:TextBox ID="Txt_Fecha_Vencimiento" runat="server" Height="18px" 
                                                    MaxLength="11" TabIndex="25" Width="84%" />
                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" 
                                                    Enabled="True" TargetControlID="Txt_Fecha_Vencimiento" 
                                                    WatermarkCssClass="watermarked" WatermarkText="Dia/Mes/Año" />
                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" 
                                                    Format="dd/MMM/yyyy" PopupButtonID="Btn_Fecha_Vencimiento" 
                                                    TargetControlID="Txt_Fecha_Vencimiento" />
                                                <asp:ImageButton ID="Btn_Fecha_Vencimiento" runat="server" 
                                                    CausesValidation="false" Height="18px" 
                                                    ImageUrl="../imagenes/paginas/SmallCalendar.gif" style="vertical-align:top;" />
                                            </td>
                                        </tr>
                                    </caption>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                
                <br />
                <hr style="width:97%;"/>
                
                <asp:GridView ID="Grid_Empleados" runat="server" AllowPaging="True" CssClass="GridView_1" 
                    AutoGenerateColumns="False" PageSize="5" GridLines="None" Width="96.5%"
                    onpageindexchanging="Grid_Empleados_PageIndexChanging"  
                    onselectedindexchanged="Grid_Empleados_SelectedIndexChanged"
                    AllowSorting="True" OnSorting="Grid_Empleados_Sorting" HeaderStyle-CssClass="tblHead">
                    <Columns>
                        <asp:ButtonField ButtonType="Image" CommandName="Select" 
                            ImageUrl="~/paginas/imagenes/gridview/blue_button.png" >
                            <ItemStyle Width="7%" />
                        </asp:ButtonField>
                        <asp:BoundField DataField="Empleado_ID" HeaderText="Empleado ID" 
                            Visible="True" SortExpression="Empleado_ID">
                            <HeaderStyle HorizontalAlign="Left" Width="0%" />
                            <ItemStyle HorizontalAlign="Left" Width="0%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="No_Empleado" HeaderText="No Empleado" 
                            Visible="True" SortExpression="No_Empleado">
                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="RFC" HeaderText="RFC" SortExpression="RFC">
                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Empleado" HeaderText="Nombre" 
                            Visible="True" SortExpression="Empleado">
                            <HeaderStyle HorizontalAlign="Left" Width="60%" />
                            <ItemStyle HorizontalAlign="left" Width="60%" />
                        </asp:BoundField>                                    
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus" SortExpression="Estatus">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" Width="10%" />
                        </asp:BoundField>
                    </Columns>
                    <SelectedRowStyle CssClass="GridSelected" />
                    <PagerStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAltItem" />
                </asp:GridView>                               
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Btn_Nuevo" EventName="Click"/>
                <asp:AsyncPostBackTrigger ControlID="Btn_Modificar" EventName="Click"/>
                <asp:AsyncPostBackTrigger ControlID="Btn_Eliminar" EventName="Click"/>
                <asp:AsyncPostBackTrigger ControlID="Btn_Salir" EventName="Click"/>
            </Triggers>
        </asp:UpdatePanel>                                                           
        </ContentTemplate>        
    </asp:UpdatePanel>       
</asp:Content>