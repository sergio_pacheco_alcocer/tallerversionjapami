﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Calendarizar_Presupuesto.Negocio;
using JAPAMI.Limite_Presupuestal.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;
using System.Net.Mail;

public partial class paginas_Presupuestos_Frm_Ope_Psp_Autorizar_Calendario_Presupuesto : System.Web.UI.Page
{
    #region PAGE LOAD

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        if (!IsPostBack)
        {
            Autorizar_Presupuesto_Inicio();
        }
    }

    #endregion

    #region METODOS

    #region (Metodos Generales)
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Autorizar_Presupuesto_Inicio
    ///DESCRIPCIÓN          : Metodo para el inicio de la pagina
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 22/Noviembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Autorizar_Presupuesto_Inicio()
    {
        try
        {
            Mostrar_Ocultar_Error(false);
            Limpiar_Formulario("Todo");
            Estado_Botones("inicial");
            Habilitar_Forma(false);
            Llenar_Grid_Dependencias_Presupuestadas();
            Div_Dependencias_Presupuestadas.Style.Add("display", "block");
            Div_Partidas_Asignadas.Style.Add("display", "none");
            Td_Psp_Anterior.Style.Add("display", "none");
            Grid_Dependencias_Presupuestadas.SelectedIndex = -1;
            Grid_Partida_Asignada.SelectedIndex = -1;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al Calendarizar_Presupuesto_Inicio ERROR[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Habilitar_Forma
    ///DESCRIPCIÓN          : Metodo para habilitar o deshabilitar los controles de la pagina
    ///PROPIEDADES          1 Estatus true o false para habilitar los controles
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 10/Noviembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Habilitar_Forma(Boolean Estatus)
    {
        try
        {
            Txt_Limite_Presupuestal.ReadOnly = true;
            if (Hf_Tipo_Calendario.Value.Trim().Equals("Empleado"))
            {
                Cmb_Unidad_Responsable.Enabled = Estatus;
            }
            else 
            {
                Cmb_Unidad_Responsable.Enabled = false;
            }
            
            Cmb_Programa.Enabled = false;
            Cmb_Capitulos.Enabled = Estatus;
            Cmb_Partida_Especifica.Enabled = false;
            Txt_Justificacion.Enabled = Estatus;
            Txt_Enero.Enabled = Estatus;
            Txt_Febrero.Enabled = Estatus;
            Txt_Marzo.Enabled = Estatus;
            Txt_Abril.Enabled = Estatus;
            Txt_Mayo.Enabled = Estatus;
            Txt_Junio.Enabled = Estatus;
            Txt_Julio.Enabled = Estatus;
            Txt_Agosto.Enabled = Estatus;
            Txt_Septiembre.Enabled = Estatus;
            Txt_Octubre.Enabled = Estatus;
            Txt_Noviembre.Enabled = Estatus;
            Txt_Diciembre.Enabled = Estatus;
            Cmb_Producto.Enabled = false;
            Cmb_Estatus.Enabled = Estatus;
            Cmb_Producto.Enabled = false;
            Txt_Comentarios.Enabled = Estatus;
            Btn_Agregar.Enabled = Estatus;
            Btn_Buscar_Producto.Enabled = Estatus;
            Btn_Borrar.Enabled = Estatus;
            Txt_Porcentaje.Enabled = Estatus;
            Btn_Porcentaje.Enabled = Estatus;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al Habilitar_Forma ERROR[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Limpiar_Formulario
    ///DESCRIPCIÓN          : Metodo para limpiar los controles de la pagina
    ///PROPIEDADES          1 Accion para saber que vamos a limpiar
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 10/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Limpiar_Formulario(String Accion)
    {
        try
        {
            switch (Accion)
            {
                case "Datos_Producto":
                    Txt_Enero.Text = "";
                    Txt_Febrero.Text = "";
                    Txt_Marzo.Text = "";
                    Txt_Abril.Text = "";
                    Txt_Mayo.Text = "";
                    Txt_Junio.Text = "";
                    Txt_Julio.Text = "";
                    Txt_Agosto.Text = "";
                    Txt_Septiembre.Text = "";
                    Txt_Octubre.Text = "";
                    Txt_Noviembre.Text = "";
                    Txt_Diciembre.Text = "";
                    Txt_Total.Text = "";
                    Txt_Justificacion.Text = "";
                    Hf_Producto_ID.Value = "";
                    Hf_Precio.Value = "";
                    Lbl_Cantidad.Text = "";
                    Lbl_Txt_Enero.Text = "";
                    Lbl_Txt_Febrero.Text = "";
                    Lbl_Txt_Marzo.Text = "";
                    Lbl_Txt_Abril.Text = "";
                    Lbl_Txt_Mayo.Text = "";
                    Lbl_Txt_Junio.Text = "";
                    Lbl_Txt_Julio.Text = "";
                    Lbl_Txt_Agosto.Text = "";
                    Lbl_Txt_Septiembre.Text = "";
                    Lbl_Txt_Octubre.Text = "";
                    Lbl_Txt_Noviembre.Text = "";
                    Lbl_Txt_Diciembre.Text = "";
                    Cmb_Stock.SelectedIndex = -1;
                    break;
                case "Datos_Partida":
                    Cmb_Producto.SelectedIndex = -1;
                    Grid_Partida_Asignada.SelectedIndex = -1;
                    Txt_Justificacion.Text = "";
                    Txt_Enero.Text = "";
                    Txt_Febrero.Text = "";
                    Txt_Marzo.Text = "";
                    Txt_Abril.Text = "";
                    Txt_Mayo.Text = "";
                    Txt_Junio.Text = "";
                    Txt_Julio.Text = "";
                    Txt_Agosto.Text = "";
                    Txt_Septiembre.Text = "";
                    Txt_Octubre.Text = "";
                    Txt_Noviembre.Text = "";
                    Txt_Diciembre.Text = "";
                    Txt_Total.Text = "";
                    Hf_Producto_ID.Value = "";
                    Hf_Precio.Value = "";
                    Lbl_Cantidad.Text = "";
                    Lbl_Txt_Enero.Text = "";
                    Lbl_Txt_Febrero.Text = "";
                    Lbl_Txt_Marzo.Text = "";
                    Lbl_Txt_Abril.Text = "";
                    Lbl_Txt_Mayo.Text = "";
                    Lbl_Txt_Junio.Text = "";
                    Lbl_Txt_Julio.Text = "";
                    Lbl_Txt_Agosto.Text = "";
                    Lbl_Txt_Septiembre.Text = "";
                    Lbl_Txt_Octubre.Text = "";
                    Lbl_Txt_Noviembre.Text = "";
                    Lbl_Txt_Diciembre.Text = "";
                    break;
                case "Todo":
                    Txt_Enero.Text = "";
                    Txt_Febrero.Text = "";
                    Txt_Marzo.Text = "";
                    Txt_Abril.Text = "";
                    Txt_Mayo.Text = "";
                    Txt_Junio.Text = "";
                    Txt_Julio.Text = "";
                    Txt_Agosto.Text = "";
                    Txt_Septiembre.Text = "";
                    Txt_Octubre.Text = "";
                    Txt_Noviembre.Text = "";
                    Txt_Diciembre.Text = "";
                    Txt_Total.Text = "";
                    Lbl_Cantidad.Text = "";
                    Lbl_Txt_Enero.Text = "";
                    Lbl_Txt_Febrero.Text = "";
                    Lbl_Txt_Marzo.Text = "";
                    Lbl_Txt_Abril.Text = "";
                    Lbl_Txt_Mayo.Text = "";
                    Lbl_Txt_Junio.Text = "";
                    Lbl_Txt_Julio.Text = "";
                    Lbl_Txt_Agosto.Text = "";
                    Lbl_Txt_Septiembre.Text = "";
                    Lbl_Txt_Octubre.Text = "";
                    Lbl_Txt_Noviembre.Text = "";
                    Lbl_Txt_Diciembre.Text = "";
                    Txt_Comentarios.Text = "";
                    Txt_Justificacion.Text = "";
                    Txt_Limite_Presupuestal.Text = "";
                    Cmb_Programa.SelectedIndex = -1;
                    Txt_Total_Ajuste.Text = "";
                    Cmb_Unidad_Responsable.SelectedIndex = -1;
                    Cmb_Capitulos.SelectedIndex = -1;
                    Cmb_Partida_Especifica.SelectedIndex = -1;
                    Cmb_Producto.SelectedIndex = -1;
                    Cmb_Stock.SelectedIndex = -1;
                    Cmb_Estatus.SelectedIndex = -1;
                    Hf_Anio.Value = "";
                    Hf_Dependencia_ID.Value = "";
                    Hf_Fte_Financiamiento.Value = "";
                    Hf_Precio.Value = "";
                    Hf_Producto_ID.Value = "";
                    Hf_Programa.Value = "";
                    Hf_Empleado_ID.Value = "";
                    Hf_Tipo_Calendario.Value = "";
                    Txt_Porcentaje.Text = "";
                    break;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al Limpiar_Formulario ERROR[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Estado_Botones
    ///DESCRIPCIÓN          : metodo que muestra los botones de acuerdo al estado en el que se encuentre
    ///PARAMETROS:          1.- String Estado: El estado de los botones solo puede tomar 
    ///                        + inicial
    ///                        + nuevo
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 10/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Estado_Botones(String Estado)
    {
        DataTable Dt_Partidas_Asignadas = new DataTable();
        Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas_Autorizacion"];
        try
        {
            switch (Estado)
            {
                case "inicial":
                    //Boton Nuevo
                    Btn_Nuevo.ToolTip = "Modificar";
                    Btn_Nuevo.Visible = true;
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";

                    //Boton Salir
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Salir.Enabled = true;
                    Btn_Salir.Visible = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    break;
                case "nuevo":
                    //Boton Nuevo
                    Btn_Nuevo.ToolTip = "Guardar";
                    Btn_Nuevo.Enabled = true;
                    Btn_Nuevo.Visible = true;
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    //Boton Salir
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Salir.Enabled = true;
                    Btn_Salir.Visible = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Div_Dependencias_Presupuestadas.Style.Add("display", "none");
                    Div_Partidas_Asignadas.Style.Add("display", "block");
                    Td_Psp_Anterior.Style.Add("display", "block");
                    break;
            }//fin del switch
        }
        catch (Exception ex)
        {
            throw new Exception("Error al habilitar el  Estado_Botones ERROR[" + ex.Message + "]");
        }
    }

    //********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Limpiar_Sessiones
    ///DESCRIPCIÓN          : Metodo para limpiar las sessiones del formulario
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 12/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Limpiar_Sessiones()
    {
        Session["Dt_Partidas_Asignadas_Autorizacion"] = null;
        Session.Remove("Dt_Partidas_Asignadas_Autorizacion");
    }

    //********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Validar_Datos
    ///DESCRIPCIÓN          : Metodo para validar los datos del formulario
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 23/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private Boolean Validar_Datos()
    {
        Boolean Datos_Validos = true;
        Lbl_Error.Text = String.Empty;
        DataTable Dt_Partidas_Asignadas = new DataTable();

        try
        {
            if (Cmb_Estatus.SelectedIndex <= 0)
            {
                Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar un estatus. <br />";
                Datos_Validos = false;
            }
            else
            {
                if (Cmb_Estatus.SelectedValue == "RECHAZADO")
                {
                    if (String.IsNullOrEmpty(Txt_Comentarios.Text.Trim()))
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Introducir un comentario. <br />";
                        Datos_Validos = false;
                    }
                }
            }

             Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas_Autorizacion"];
             if (Dt_Partidas_Asignadas == null)
             {
                 Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Introducir partidas al presupuesto. <br />";
                 Datos_Validos = false;
             }
             else {
                 if (Dt_Partidas_Asignadas.Rows.Count < 0)
                 {
                     Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Introducir partidas al presupuesto. <br />";
                     Datos_Validos = false;
                 }
             }
            return Datos_Validos;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de validar los datos Error[" + ex.Message + "]");
        }
    }

    //********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Mostrar_Ocultar_Error
    ///DESCRIPCIÓN          : Metodo para mostrar u ocultar los errores
    ///PROPIEDADES          1 Estatus ocultar o mostrar
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 12/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Mostrar_Ocultar_Error(Boolean Estatus)
    {
        Lbl_Error.Visible = Estatus;
        Lbl_Encanezado_Error.Visible = Estatus;
        Img_Error.Visible = Estatus;
    }

    //********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : LLenar_Presupuesto_Dependencia
    ///DESCRIPCIÓN          : Metodo para mostrar los datos del presupuesto dela dependencia
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 14/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void LLenar_Presupuesto_Dependencia()
    {
        Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
        DataTable Dt_Datos = new DataTable();
        try
        {

            Hf_Fte_Financiamiento.Value = "";
            Cmb_Estatus.SelectedIndex = -1;

            if (Hf_Tipo_Calendario.Value.Trim().Equals("Dependencia"))
            {
                Negocio.P_Empleado_ID = String.Empty;
                Negocio.P_Dependencia_ID = Hf_Dependencia_ID.Value.Trim();
                Dt_Datos = Negocio.Consultar_Presupuesto_Dependencia();
            }
            else 
            {
                Negocio.P_Dependencia_ID = String.Empty;
                Negocio.P_Empleado_ID = Hf_Empleado_ID.Value.Trim();
                Dt_Datos = Negocio.Consultar_Presupuesto_Dependencia();
            }

            if (Dt_Datos != null)
            {
                if (Dt_Datos.Rows.Count > 0)
                {
                    Hf_Fte_Financiamiento.Value = Dt_Datos.Rows[0]["FTE_FINANCIAMIENTO_ID"].ToString().Trim();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de llenar los datos del presupuesto de la dependencia Error[" + ex.Message + "]");
        }
    }

    //********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Consultar_Combo_Stock
    ///DESCRIPCIÓN          : Metodo para consultar si una partida es de stock o no
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 14/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Consultar_Combo_Stock()
    {
        Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
        DataTable Dt_Partida = new DataTable();
        try
        {
            if (Cmb_Partida_Especifica.SelectedIndex > 0)
            {
                Negocio.P_Anio_Presupuesto = Hf_Anio.Value.Trim();
                Negocio.P_Partida_ID = Cmb_Partida_Especifica.SelectedValue.Trim();
                Dt_Partida = Negocio.Consultar_Partida_Stock();
                if (Dt_Partida != null)
                {
                    if (Dt_Partida.Rows.Count > 0)
                    {
                        Cmb_Stock.SelectedValue = "SI";
                    }
                    else
                    {
                        Cmb_Stock.SelectedValue = "NO";
                    }
                }
                else
                {
                    Cmb_Stock.SelectedValue = "NO";
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al calcular el total del presupuesto. Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Partida_Asignada
    ///DESCRIPCIÓN          : Metodo para llenar el grid anidado de las partidas asignadas
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 16/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Llenar_Grid_Partida_Asignada()
    {
        Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
        DataTable Dt_Session = new DataTable();
        DataTable Dt_Partidas_Asignadas = new DataTable();
        Dt_Session = (DataTable)Session["Dt_Partidas_Asignadas_Autorizacion"];
        try
        {
            if (Dt_Session != null)
            {
                if (Dt_Session.Rows.Count > 0)
                {
                    Dt_Partidas_Asignadas = Dt_Session; ;
                }
                else
                {
                    if (!String.IsNullOrEmpty(Hf_Dependencia_ID.Value.Trim()))
                    {
                        Negocio.P_Empleado_ID = String.Empty; 
                        Negocio.P_Dependencia_ID = Hf_Dependencia_ID.Value.Trim();
                        Negocio.P_Estatus = "GENERADO";
                        Dt_Partidas_Asignadas = Negocio.Consultar_Partidas_Asignadas();
                        Session["Dt_Partidas_Asignadas_Autorizacion"] = Dt_Partidas_Asignadas;
                    }
                }
            }
            else
            {
                if (Hf_Tipo_Calendario.Value.Trim().Equals("Dependencia"))
                {
                    if (!String.IsNullOrEmpty(Hf_Dependencia_ID.Value.Trim()))
                    {
                        Negocio.P_Dependencia_ID = Hf_Dependencia_ID.Value.Trim();
                        Dt_Partidas_Asignadas = Negocio.Consultar_Partidas_Asignadas();
                        Session["Dt_Partidas_Asignadas_Autorizacion"] = Dt_Partidas_Asignadas;
                    }
                }
                else
                {
                    Negocio.P_Empleado_ID = Hf_Empleado_ID.Value.Trim();
                    Dt_Partidas_Asignadas = Negocio.Consultar_Partidas_Asignadas();
                    Session["Dt_Partidas_Asignadas_Autorizacion"] = Dt_Partidas_Asignadas;
                }
            }
            if (Dt_Partidas_Asignadas != null)
            {
                if (Dt_Partidas_Asignadas.Rows.Count > 0)
                {
                    Obtener_Consecutivo_ID();
                    Dt_Partidas_Asignadas = Crear_Dt_Partida_Asignada();
                    Grid_Partida_Asignada.Columns[1].Visible = true;
                    Grid_Partida_Asignada.DataSource = Dt_Partidas_Asignadas;
                    Grid_Partida_Asignada.DataBind();
                    Grid_Partida_Asignada.Columns[1].Visible = false;
                }
                else
                {
                    Grid_Partida_Asignada.DataSource = Dt_Partidas_Asignadas;
                    Grid_Partida_Asignada.DataBind();
                }
            }
            else
            {
                Grid_Partida_Asignada.DataSource = Dt_Partidas_Asignadas;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de llenar la tabla de las partidas asignadas Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Partida_Asignada
    ///DESCRIPCIÓN          : Metodo para crear el datatable de las partidas asignadas del grid anidado
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 16/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private DataTable Crear_Dt_Partida_Asignada()
    {
        DataTable Dt_Partida_Asignada = new DataTable();
        DataTable Dt_Session = new DataTable();
        Dt_Session = (DataTable)Session["Dt_Partidas_Asignadas_Autorizacion"];
        String Partida_Id = string.Empty;
        Double Ene = 0.00;
        Double Feb = 0.00;
        Double Mar = 0.00;
        Double Abr = 0.00;
        Double May = 0.00;
        Double Jun = 0.00;
        Double Jul = 0.00;
        Double Ago = 0.00;
        Double Sep = 0.00;
        Double Oct = 0.00;
        Double Nov = 0.00;
        Double Dic = 0.00;
        Double Total = 0.00;
        DataRow Fila;
        String Clave = String.Empty;
        Boolean Iguales;

        try
        {
            if (Dt_Session != null)
            {
                if (Dt_Session.Rows.Count > 0)
                {
                    //creamos las columnas del datatable donde se guardaran los datos
                    Dt_Partida_Asignada.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("CLAVE", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL_ENE", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL_FEB", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL_MAR", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL_ABR", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL_MAY", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL_JUN", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL_JUL", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL_AGO", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL_SEP", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL_OCT", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL_NOV", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL_DIC", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL", System.Type.GetType("System.String"));


                    foreach (DataRow Dr_Sessiones in Dt_Session.Rows)
                    {
                        //Obtenemos la partida id y la clave
                        Partida_Id = Dr_Sessiones["PARTIDA_ID"].ToString().Trim();
                        Clave = Dr_Sessiones["CLAVE_PARTIDA"].ToString().Trim();
                        Iguales = false;
                        //verificamos si la partida no a sido ya agrupada
                        if (Dt_Partida_Asignada.Rows.Count > 0)
                        {
                            foreach (DataRow Dr_Partidas in Dt_Partida_Asignada.Rows)
                            {
                                if (Dr_Partidas["PARTIDA_ID"].ToString().Trim().Equals(Partida_Id))
                                {
                                    Iguales = true;
                                }
                            }
                        }

                        // tomamos los datos del datatable para agrupar las partidas asignadas
                        if (!Iguales)
                        {
                            foreach (DataRow Dr_Session in Dt_Session.Rows)
                            {
                                if (Dr_Session["PARTIDA_ID"].ToString().Trim().Equals(Partida_Id))
                                {
                                    Ene = Ene + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["ENERO"].ToString()) ? "0" : Dr_Session["ENERO"].ToString().Trim());
                                    Feb = Feb + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["FEBRERO"].ToString()) ? "0" : Dr_Session["FEBRERO"].ToString().Trim());
                                    Mar = Mar + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["MARZO"].ToString()) ? "0" : Dr_Session["MARZO"].ToString().Trim());
                                    Abr = Abr + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["ABRIL"].ToString()) ? "0" : Dr_Session["ABRIL"].ToString().Trim());
                                    May = May + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["MAYO"].ToString()) ? "0" : Dr_Session["MAYO"].ToString().Trim());
                                    Jun = Jun + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["JUNIO"].ToString()) ? "0" : Dr_Session["JUNIO"].ToString().Trim());
                                    Jul = Jul + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["JULIO"].ToString()) ? "0" : Dr_Session["JULIO"].ToString().Trim());
                                    Ago = Ago + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["AGOSTO"].ToString()) ? "0" : Dr_Session["AGOSTO"].ToString().Trim());
                                    Sep = Sep + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["SEPTIEMBRE"].ToString()) ? "0" : Dr_Session["SEPTIEMBRE"].ToString().Trim());
                                    Oct = Oct + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["OCTUBRE"].ToString()) ? "0" : Dr_Session["OCTUBRE"].ToString().Trim());
                                    Nov = Nov + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["NOVIEMBRE"].ToString()) ? "0" : Dr_Session["NOVIEMBRE"].ToString().Trim());
                                    Dic = Dic + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["DICIEMBRE"].ToString()) ? "0" : Dr_Session["DICIEMBRE"].ToString().Trim());
                                }
                            }
                            Total = Ene + Feb + Mar + Abr + May + Jun + Jul + Ago + Sep + Oct + Nov + Dic;

                            Fila = Dt_Partida_Asignada.NewRow();
                            Fila["PARTIDA_ID"] = Partida_Id;
                            Fila["CLAVE"] = Clave;
                            Fila["TOTAL_ENE"] = String.Format("{0:##,###,##0.00}", Ene);
                            Fila["TOTAL_FEB"] = String.Format("{0:##,###,##0.00}", Feb);
                            Fila["TOTAL_MAR"] = String.Format("{0:##,###,##0.00}", Mar);
                            Fila["TOTAL_ABR"] = String.Format("{0:##,###,##0.00}", Abr);
                            Fila["TOTAL_MAY"] = String.Format("{0:##,###,##0.00}", May);
                            Fila["TOTAL_JUN"] = String.Format("{0:##,###,##0.00}", Jun);
                            Fila["TOTAL_JUL"] = String.Format("{0:##,###,##0.00}", Jul);
                            Fila["TOTAL_AGO"] = String.Format("{0:##,###,##0.00}", Ago);
                            Fila["TOTAL_SEP"] = String.Format("{0:##,###,##0.00}", Sep);
                            Fila["TOTAL_OCT"] = String.Format("{0:##,###,##0.00}", Oct);
                            Fila["TOTAL_NOV"] = String.Format("{0:##,###,##0.00}", Nov);
                            Fila["TOTAL_DIC"] = String.Format("{0:##,###,##0.00}", Dic);
                            Fila["TOTAL"] = String.Format("{0:##,###,##0.00}", Total);
                            Dt_Partida_Asignada.Rows.Add(Fila);

                            Ene = 0.00;
                            Feb = 0.00;
                            Mar = 0.00;
                            Abr = 0.00;
                            May = 0.00;
                            Jun = 0.00;
                            Jul = 0.00;
                            Ago = 0.00;
                            Sep = 0.00;
                            Oct = 0.00;
                            Nov = 0.00;
                            Dic = 0.00;
                            Total = 0.00;
                        }
                    }
                }
            }
            return Dt_Partida_Asignada;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de crear la tabla de partidas asignadas Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Detalles
    ///DESCRIPCIÓN          : Metodo para crear el datatable de los detalles de las partidas asignadas del grid 
    ///PROPIEDADES          1 Partida_ID del cual obtendremos los detalles
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 16/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private DataTable Crear_Dt_Detalles(String Partida_ID)
    {
        DataTable Dt_Detalle = new DataTable();
        DataTable Dt_Session = new DataTable();
        Dt_Session = (DataTable)Session["Dt_Partidas_Asignadas_Autorizacion"];
        DataRow Fila;
        String Partida = String.Empty;

        try
        {
            if (Dt_Session != null)
            {
                if (Dt_Session.Rows.Count > 0)
                {
                    Dt_Detalle.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("PROYECTO_ID", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("PRODUCTO_ID", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("PRECIO", System.Type.GetType("System.Double"));
                    Dt_Detalle.Columns.Add("JUSTIFICACION", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("CLAVE_PARTIDA", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("CLAVE_PRODUCTO", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("UR", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("ENERO", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("FEBRERO", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("MARZO", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("ABRIL", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("MAYO", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("JUNIO", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("JULIO", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("AGOSTO", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("SEPTIEMBRE", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("OCTUBRE", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("NOVIEMBRE", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("DICIEMBRE", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("IMPORTE_TOTAL", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("ID", System.Type.GetType("System.String"));

                    foreach (DataRow Dr_Session in Dt_Session.Rows)
                    {
                        if (Dr_Session["PARTIDA_ID"].ToString().Trim().Equals(Partida_ID.Trim()))
                        {
                            Fila = Dt_Detalle.NewRow();
                            Fila["DEPENDENCIA_ID"] = Dr_Session["DEPENDENCIA_ID"].ToString().Trim();
                            Fila["PROYECTO_ID"] = Dr_Session["PROYECTO_ID"].ToString().Trim();
                            Fila["CAPITULO_ID"] = Dr_Session["CAPITULO_ID"].ToString().Trim();
                            Fila["PARTIDA_ID"] = Dr_Session["PARTIDA_ID"].ToString().Trim();
                            Fila["PRODUCTO_ID"] = Dr_Session["PRODUCTO_ID"].ToString().Trim();
                            Fila["PRECIO"] = Dr_Session["PRECIO"];
                            Fila["JUSTIFICACION"] = Dr_Session["JUSTIFICACION"].ToString().Trim();
                            Fila["CLAVE_PARTIDA"] = Dr_Session["CLAVE_PARTIDA"].ToString().Trim();

                            //obtenemos la clave de la partida
                            Partida = Dr_Session["CLAVE_PARTIDA"].ToString().Trim().Substring(0, Dr_Session["CLAVE_PARTIDA"].ToString().Trim().IndexOf(" "));

                            if (Partida.Trim().Equals("3551"))
                            {
                                if (String.IsNullOrEmpty(Dr_Session["PRODUCTO_ID"].ToString().Trim()))
                                {
                                    Fila["CLAVE_PRODUCTO"] = "SERVICIO Y MANTENIMIENTO";
                                }
                                else
                                {
                                    Fila["CLAVE_PRODUCTO"] = Dr_Session["CLAVE_PRODUCTO"].ToString().Trim();
                                }


                            }
                            else
                            {
                                Fila["CLAVE_PRODUCTO"] = Dr_Session["CLAVE_PRODUCTO"].ToString().Trim();
                            }

                            Fila["UR"] = Dr_Session["UR"].ToString().Trim();
                            Fila["ENERO"] = String.Format("{0:#,###,##0.00}", Dr_Session["ENERO"]);
                            Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}", Dr_Session["FEBRERO"]);
                            Fila["MARZO"] = String.Format("{0:#,###,##0.00}", Dr_Session["MARZO"]);
                            Fila["ABRIL"] = String.Format("{0:#,###,##0.00}", Dr_Session["ABRIL"]);
                            Fila["MAYO"] = String.Format("{0:#,###,##0.00}", Dr_Session["MAYO"]);
                            Fila["JUNIO"] = String.Format("{0:#,###,##0.00}", Dr_Session["JUNIO"]);
                            Fila["JULIO"] = String.Format("{0:#,###,##0.00}", Dr_Session["JULIO"]);
                            Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}", Dr_Session["AGOSTO"]);
                            Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}", Dr_Session["SEPTIEMBRE"]);
                            Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}", Dr_Session["OCTUBRE"]);
                            Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}", Dr_Session["NOVIEMBRE"]);
                            Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}", Dr_Session["DICIEMBRE"]);
                            Fila["IMPORTE_TOTAL"] = String.Format("{0:#,###,##0.00}", Dr_Session["IMPORTE_TOTAL"]);
                            Fila["ID"] = Dr_Session["ID"].ToString().Trim();
                            Dt_Detalle.Rows.Add(Fila);
                        }
                    }
                }
            }
            return Dt_Detalle;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de crear la tabla de partidas asignadas Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Obtener_Consecutivo_ID
    ///DESCRIPCIÓN          : Metodo para obtener el consecutivo del datatable 
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 16/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Obtener_Consecutivo_ID()
    {
        DataTable Dt_Partidas_Asignadas = new DataTable();
        Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas_Autorizacion"];
        int Contador;
        try
        {
            if (Dt_Partidas_Asignadas != null)
            {
                if (Dt_Partidas_Asignadas.Columns.Count > 0)
                {
                    if (Dt_Partidas_Asignadas.Rows.Count > 0)
                    {
                        Contador = -1;
                        foreach (DataRow Dr in Dt_Partidas_Asignadas.Rows)
                        {
                            Contador++;
                            Dr["ID"] = Contador.ToString().Trim();
                        }
                    }
                }
            }
            Session["Dt_Partidas_Asignadas_Autorizacion"] = Dt_Partidas_Asignadas;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al crear el datatable con consecutivo Erro[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Dependencias_Presupuestadas
    ///DESCRIPCIÓN          : Metodo para llenar el grid de las dependencias presupuestadas
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 23/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Llenar_Grid_Dependencias_Presupuestadas()
    {
        Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
        DataTable Dt_Dependencias = new DataTable();
        try
        {
            Grid_Dependencias_Presupuestadas.DataBind();
            Dt_Dependencias = Negocio.Consultar_Dependencias_Presupuestadas();
            if (Dt_Dependencias != null)
            {
                if (Dt_Dependencias.Rows.Count > 0)
                {
                    Grid_Dependencias_Presupuestadas.Columns[1].Visible = true;
                    Grid_Dependencias_Presupuestadas.Columns[2].Visible = true;
                    Grid_Dependencias_Presupuestadas.Columns[3].Visible = true;
                    Grid_Dependencias_Presupuestadas.Columns[4].Visible = true;
                    Grid_Dependencias_Presupuestadas.Columns[8].Visible = true;
                    Grid_Dependencias_Presupuestadas.Columns[9].Visible = true;
                    Grid_Dependencias_Presupuestadas.Columns[10].Visible = true;
                    Grid_Dependencias_Presupuestadas.DataSource = Dt_Dependencias;
                    Grid_Dependencias_Presupuestadas.DataBind();
                    Grid_Dependencias_Presupuestadas.Columns[1].Visible = false;
                    Grid_Dependencias_Presupuestadas.Columns[2].Visible = false;
                    Grid_Dependencias_Presupuestadas.Columns[3].Visible = false;
                    Grid_Dependencias_Presupuestadas.Columns[4].Visible = false;
                    Grid_Dependencias_Presupuestadas.Columns[8].Visible = false;
                    Grid_Dependencias_Presupuestadas.Columns[9].Visible = false;
                    Grid_Dependencias_Presupuestadas.Columns[10].Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de llenar la tabla de las partidas asignadas Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Agregar_Fila_Dt_Partidas_Asignadas
    ///DESCRIPCIÓN          : Metodo para agregar los datos de las partidas asignadas al datatable
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 11/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Agregar_Fila_Dt_Partidas_Asignadas()
    {
        DataTable Dt_Partidas_Asignadas = new DataTable();
        DataRow Fila;
        Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas_Autorizacion"];
        try
        {
            Fila = Dt_Partidas_Asignadas.NewRow();
            Fila["DEPENDENCIA_ID"] = Cmb_Unidad_Responsable.SelectedItem.Value.Trim();
            Fila["PROYECTO_ID"] = Cmb_Programa.SelectedItem.Value.Trim();
            Fila["CAPITULO_ID"] = Cmb_Capitulos.SelectedItem.Value.Trim();
            Fila["PARTIDA_ID"] = Cmb_Partida_Especifica.SelectedItem.Value.Trim();
            if (!string.IsNullOrEmpty(Hf_Producto_ID.Value.Trim())) //verificamos si se selecciono un producto
            {
                Fila["PRODUCTO_ID"] = Cmb_Producto.SelectedItem.Value.Trim();
                Fila["PRECIO"] = Convert.ToDouble(String.IsNullOrEmpty(Hf_Precio.Value.Trim()) ? "0" : Hf_Precio.Value.Trim());
            }
            else
            {
                Fila["PRODUCTO_ID"] = String.Empty;
                Fila["PRECIO"] = 0;
            }

            Fila["JUSTIFICACION"] = Txt_Justificacion.Text.Trim();

            if (!string.IsNullOrEmpty(Hf_Producto_ID.Value.Trim())) //si existe un producto ponermos su clave y si no la clave de la partida
            {
                //Fila["CLAVE_PARTIDA"] = Cmb_Partida_Especifica.SelectedItem.Text.Substring(0, Cmb_Partida_Especifica.SelectedItem.Text.IndexOf(" ")).Trim();
                //Fila["CLAVE_PRODUCTO"] = Cmb_Producto.SelectedItem.Text.Substring(0, Cmb_Producto.SelectedItem.Text.IndexOf(" ")).Trim();
                Fila["CLAVE_PARTIDA"] = Cmb_Partida_Especifica.SelectedItem.Text.Trim();
                Fila["CLAVE_PRODUCTO"] = Cmb_Producto.SelectedItem.Text.Trim();
            }
            else
            {
                //Fila["CLAVE_PARTIDA"] = Cmb_Partida_Especifica.SelectedItem.Text.Substring(0, Cmb_Partida_Especifica.SelectedItem.Text.IndexOf(" ")).Trim();
                Fila["CLAVE_PARTIDA"] = Cmb_Partida_Especifica.SelectedItem.Text.Trim();
                Fila["CLAVE_PRODUCTO"] = String.Empty;
            }
            Fila["UR"] = Cmb_Unidad_Responsable.SelectedItem.Text.Substring(Cmb_Unidad_Responsable.SelectedItem.Text.IndexOf(" ")).Trim();
            Fila["ENERO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Enero.Text.Trim()) ? "0" : Txt_Enero.Text.Trim()));
            Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Febrero.Text.Trim()) ? "0" : Txt_Febrero.Text.Trim()));
            Fila["MARZO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Marzo.Text.Trim()) ? "0" : Txt_Marzo.Text.Trim()));
            Fila["ABRIL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Abril.Text.Trim()) ? "0" : Txt_Abril.Text.Trim()));
            Fila["MAYO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Mayo.Text.Trim()) ? "0" : Txt_Mayo.Text.Trim()));
            Fila["JUNIO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Junio.Text.Trim()) ? "0" : Txt_Junio.Text.Trim()));
            Fila["JULIO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Julio.Text.Trim()) ? "0" : Txt_Julio.Text.Trim()));
            Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Agosto.Text.Trim()) ? "0" : Txt_Agosto.Text.Trim()));
            Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Septiembre.Text.Trim()) ? "0" : Txt_Septiembre.Text.Trim()));
            Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Octubre.Text.Trim()) ? "0" : Txt_Octubre.Text.Trim()));
            Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Noviembre.Text.Trim()) ? "0" : Txt_Noviembre.Text.Trim()));
            Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Diciembre.Text.Trim()) ? "0" : Txt_Diciembre.Text.Trim()));
            String Total = Txt_Total.Text.Replace("$", "");
            Fila["IMPORTE_TOTAL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Total.Trim()) ? "0" : Total.Trim()));
            Fila["ID"] = "";

            Dt_Partidas_Asignadas.Rows.Add(Fila);

            Session["Dt_Partidas_Asignadas_Autorizacion"] = (DataTable)Dt_Partidas_Asignadas;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de crear las columnas de la tabla Error[" + ex.Message + "]");
        }
    }

    //********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Validar_Datos_Productos
    ///DESCRIPCIÓN          : Metodo para validar los datos de los productos
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 12/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private Boolean Validar_Datos_Productos()
    {
        Boolean Datos_Validos = true;
        Lbl_Error.Text = String.Empty;

        try
        {
            if (Cmb_Unidad_Responsable.SelectedIndex <= 0)
            {
                Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar una unidad responsable. <br />";
                Datos_Validos = false;
            }
            if (Cmb_Programa .SelectedIndex <= 0)
            {
                Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar un programa. <br />";
                Datos_Validos = false;
            }
            if (Cmb_Capitulos.SelectedIndex <= 0)
            {
                Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar un capítulo. <br />";
                Datos_Validos = false;
            }
            if (Cmb_Partida_Especifica.SelectedIndex <= 0)
            {
                Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar una partida. <br />";
                Datos_Validos = false;
            }
            
            if (Tr_Productos.Visible)
            {
                //if (String.IsNullOrEmpty(Hf_Producto_ID.Value.Trim()))
                //{
                //    Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar un producto. <br />";
                //    Datos_Validos = false;
                //}
            }

            if (String.IsNullOrEmpty(Txt_Justificacion.Text.Trim()))
            {
                Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Introducir una justificación. <br />";
                Datos_Validos = false;
            }

            if (String.IsNullOrEmpty(Txt_Total.Text.Trim()) || Txt_Total.Text.Trim() == "0.00")
            {
                Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Introducir la cantidad presupuestal para los meses. <br />";
                Datos_Validos = false;
            }
            return Datos_Validos;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de validar los datos Error[" + ex.Message + "]");
        }
    }

    //********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Calcular_Total_Presupuestado
    ///DESCRIPCIÓN          : Metodo para calcular el total que se lleva presupuestodo
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 14/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Calcular_Total_Presupuestado()
    {
        DataTable Dt_Partidas_Asignadas = new DataTable();
        Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas_Autorizacion"];
        Double Total = 0.00;
        Double Presupuesto = 0.00;

        try
        {
            if (Dt_Partidas_Asignadas != null)
            {
                if (Dt_Partidas_Asignadas.Rows.Count > 0)
                {
                    foreach (DataRow Dr in Dt_Partidas_Asignadas.Rows)
                    {
                        Total = Total + Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_TOTAL"].ToString().Trim()) ? "0" : Dr["IMPORTE_TOTAL"].ToString().Trim());
                    }
                }
            }
            Txt_Total_Ajuste.Text = "";
            Txt_Total_Ajuste.Text = String.Format("{0:c}", Total);

            //VALIDAMOS SI EL PRESUPUESTO NO ES MAYOR AL QUE SE OTORGO
            Presupuesto = Convert.ToDouble(string.IsNullOrEmpty(Txt_Limite_Presupuestal.Text.Trim()) ? "0" : Txt_Limite_Presupuestal.Text.Trim().Replace("$", ""));
            Txt_Presupuesto_Restante.Text = "";
            Txt_Presupuesto_Restante.Text = String.Format("{0:c}", (Presupuesto - Total));
            if (Total > Presupuesto)
            {
                Lbl_Error.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Verificar pues revazo el limite presupuestal otorgado";
                Mostrar_Ocultar_Error(true);
                Btn_Agregar.Enabled = false;
                Btn_Nuevo.Enabled = false;
            }
            else
            {
                Btn_Agregar.Enabled = true;
                Btn_Nuevo.Enabled = true;
                Mostrar_Ocultar_Error(false);
                Lbl_Error.Text = "";
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al calcular el total del presupuesto. Error[" + ex.Message + "]");
        }

    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Partidas_Asignadas
    ///DESCRIPCIÓN          : Metodo para crear las columnas del datatable
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 11/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Crear_Dt_Partidas_Asignadas()
    {
        DataTable Dt_Partidas_Asignadas = new DataTable();
        try
        {
            Dt_Partidas_Asignadas.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
            Dt_Partidas_Asignadas.Columns.Add("PROYECTO_ID", System.Type.GetType("System.String"));
            Dt_Partidas_Asignadas.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
            Dt_Partidas_Asignadas.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
            Dt_Partidas_Asignadas.Columns.Add("PRODUCTO_ID", System.Type.GetType("System.String"));
            Dt_Partidas_Asignadas.Columns.Add("PRECIO", System.Type.GetType("System.Double"));
            Dt_Partidas_Asignadas.Columns.Add("JUSTIFICACION", System.Type.GetType("System.String"));
            Dt_Partidas_Asignadas.Columns.Add("CLAVE_PARTIDA", System.Type.GetType("System.String"));
            Dt_Partidas_Asignadas.Columns.Add("CLAVE_PRODUCTO", System.Type.GetType("System.String"));
            Dt_Partidas_Asignadas.Columns.Add("UR", System.Type.GetType("System.String"));
            Dt_Partidas_Asignadas.Columns.Add("ENERO", System.Type.GetType("System.String"));
            Dt_Partidas_Asignadas.Columns.Add("FEBRERO", System.Type.GetType("System.String"));
            Dt_Partidas_Asignadas.Columns.Add("MARZO", System.Type.GetType("System.String"));
            Dt_Partidas_Asignadas.Columns.Add("ABRIL", System.Type.GetType("System.String"));
            Dt_Partidas_Asignadas.Columns.Add("MAYO", System.Type.GetType("System.String"));
            Dt_Partidas_Asignadas.Columns.Add("JUNIO", System.Type.GetType("System.String"));
            Dt_Partidas_Asignadas.Columns.Add("JULIO", System.Type.GetType("System.String"));
            Dt_Partidas_Asignadas.Columns.Add("AGOSTO", System.Type.GetType("System.String"));
            Dt_Partidas_Asignadas.Columns.Add("SEPTIEMBRE", System.Type.GetType("System.String"));
            Dt_Partidas_Asignadas.Columns.Add("OCTUBRE", System.Type.GetType("System.String"));
            Dt_Partidas_Asignadas.Columns.Add("NOVIEMBRE", System.Type.GetType("System.String"));
            Dt_Partidas_Asignadas.Columns.Add("DICIEMBRE", System.Type.GetType("System.String"));
            Dt_Partidas_Asignadas.Columns.Add("IMPORTE_TOTAL", System.Type.GetType("System.String"));
            Dt_Partidas_Asignadas.Columns.Add("ID", System.Type.GetType("System.String"));

            Session["Dt_Partidas_Asignadas_Autorizacion"] = (DataTable)Dt_Partidas_Asignadas;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de crear las columnas de la tabla Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Calcular_Restante
    ///DESCRIPCIÓN          : Metodo para calcular el restante del presupuesto
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 01/Marzo/2013 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Calcular_Restante()
    {
        Double Total = 0.00;
        Double Limite_Presupuestal = 0.00;
        Double Restante = 0.00;
        Double Total_Presupuestado = 0.00;

        Total = Convert.ToDouble(String.IsNullOrEmpty(Txt_Total.Text.Trim()) ? "0" : Txt_Total.Text.Trim().Replace(",", "").Replace("$", ""));
        Limite_Presupuestal = Convert.ToDouble(String.IsNullOrEmpty(Txt_Limite_Presupuestal.Text.Trim()) ? "0" : Txt_Limite_Presupuestal.Text.Trim().Replace(",", "").Replace("$", ""));
        Total_Presupuestado = Convert.ToDouble(String.IsNullOrEmpty(Txt_Total_Ajuste.Text.Trim()) ? "0" : Txt_Total_Ajuste.Text.Trim().Replace(",", "").Replace("$", ""));

        Restante = Limite_Presupuestal - Total_Presupuestado - Total;

        Txt_Presupuesto_Restante.Text = String.Format("{0:n}", Restante);
    }
    #endregion

    #region (Metodos Combos)
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Capitulos
    ///DESCRIPCIÓN          : Metodo para llenar el combo de los capitulos
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 10/Noviembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Llenar_Combo_Capitulos()
    {
        Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Calendarizar_Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
        DataTable Dt_Capitulos = new DataTable();
        try
        {
            if (!String.IsNullOrEmpty(Hf_Dependencia_ID.Value.Trim()))
            {
                Calendarizar_Negocio.P_Dependencia_ID = Hf_Dependencia_ID.Value.Trim();
            }
            if (!String.IsNullOrEmpty(Hf_Empleado_ID.Value.Trim()))
            {
                Calendarizar_Negocio.P_Empleado_ID = Hf_Empleado_ID.Value.Trim();
            }

            Dt_Capitulos = Calendarizar_Negocio.Consultar_Capitulos();
            if (Dt_Capitulos != null)
            {
                if (Dt_Capitulos.Rows.Count > 0)
                {
                    Cmb_Capitulos.Items.Clear();
                    Cmb_Capitulos.DataValueField = Cat_SAP_Capitulos.Campo_Capitulo_ID;
                    Cmb_Capitulos.DataTextField = "NOMBRE";
                    Cmb_Capitulos.DataSource = Dt_Capitulos;
                    Cmb_Capitulos.DataBind();
                }
            }
            Cmb_Capitulos.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        }
        catch (Exception ex)
        {
            throw new Exception("Error al Llenar_Combo_Capitulos ERROR[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Partidas
    ///DESCRIPCIÓN          : Metodo para llenar el combo de las partidas de un capitulo
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 10/Noviembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Llenar_Combo_Partidas()
    {
        Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Calendarizar_Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
        DataTable Dt_Partidas = new DataTable();
        try
        {
            if (Cmb_Capitulos.SelectedIndex > 0)
            {
                Calendarizar_Negocio.P_Capitulo_ID = Cmb_Capitulos.SelectedItem.Value.Trim();
                Dt_Partidas = Calendarizar_Negocio.Consultar_Partidas();
                if (Dt_Partidas != null)
                {
                    if (Dt_Partidas.Rows.Count > 0)
                    {
                        Cmb_Partida_Especifica.Items.Clear();
                        Cmb_Partida_Especifica.DataValueField = Cat_Com_Partidas.Campo_Partida_ID;
                        Cmb_Partida_Especifica.DataTextField = "NOMBRE";
                        Cmb_Partida_Especifica.DataSource = Dt_Partidas;
                        Cmb_Partida_Especifica.DataBind();
                        Cmb_Partida_Especifica.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al Llenar_Combo_Partidas ERROR[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Partidas
    ///DESCRIPCIÓN          : Metodo para llenar el combo de las partidas de un capitulo
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 11/Noviembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Llenar_Combo_Productos()
    {
        Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Calendarizar_Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
        DataTable Dt_Productos = new DataTable();
        String Partida = String.Empty;

        try
        {
            if (Cmb_Partida_Especifica.SelectedIndex > 0)
            {
                Calendarizar_Negocio.P_Partida_ID = Cmb_Partida_Especifica.SelectedItem.Value.Trim();
                Dt_Productos = Calendarizar_Negocio.Consultar_Productos();
                if (Dt_Productos != null)
                {
                    if (Dt_Productos.Rows.Count > 0)
                    {
                        Cmb_Producto.Items.Clear();
                        Cmb_Producto.DataValueField = Cat_Com_Productos.Campo_Producto_ID;
                        Cmb_Producto.DataTextField = "CLAVE_PRODUCTO";
                        Cmb_Producto.DataSource = Dt_Productos;
                        Cmb_Producto.DataBind();

                        //obtenemos la clave de la partida
                        Partida = Cmb_Partida_Especifica.SelectedItem.Text.Trim().Substring(0, Cmb_Partida_Especifica.SelectedItem.Text.Trim().IndexOf(" "));

                        if (Partida.Trim().Equals("3551"))
                        {
                            Cmb_Producto.Items.Insert(0, new ListItem("SERVICIO Y MANTENIMIENTO", ""));
                        }
                        else
                        {
                            Cmb_Producto.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                        }

                        Tr_Productos.Visible = true;
                    }
                    else
                    {
                        Tr_Productos.Visible = false;
                    }
                }
                else
                {
                    Tr_Productos.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al Llenar_Combo_Productos ERROR[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Estatus
    ///DESCRIPCIÓN          : Metodo para llenar el combo del estatus del presupuesto
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 22/Noviembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Llenar_Combo_Estatus()
    {
        try
        {
            Cmb_Estatus.Items.Clear();
            Cmb_Estatus.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
            Cmb_Estatus.Items.Insert(1, new ListItem("AUTORIZADO", "AUTORIZADO"));
            Cmb_Estatus.Items.Insert(2, new ListItem("GENERADO", "GENERADO"));
            Cmb_Estatus.Items.Insert(3, new ListItem("RECHAZADO", "RECHAZADO"));
        }
        catch (Exception ex)
        {
            throw new Exception("Error al Llenar_Combo_Estatus ERROR[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Unidad_Responsable
    ///DESCRIPCIÓN          : Metodo para llenar el combo de la unidad responsable
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 10/Noviembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Llenar_Combo_Unidad_Responsable()
    {
        Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Calendarizar_Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
        Cls_Ope_Psp_Limite_Presupuestal_Negocio Limite_Negocio = new Cls_Ope_Psp_Limite_Presupuestal_Negocio();
        DataTable Dt_Dependencias = new DataTable();
        String Dependencia_Empleado = string.Empty;
        try
        {
            if (Hf_Tipo_Calendario.Value.Equals("Dependencia"))
            {
                Dt_Dependencias = Calendarizar_Negocio.Consultar_Unidad_Responsable();
            }
            else
            {
                Limite_Negocio.P_Accion = String.Empty;
                Dt_Dependencias = Limite_Negocio.Consultar_Unidades_Responsables_Sin_Asignar();
            }


            if (Dt_Dependencias != null)
            {
                if (Dt_Dependencias.Rows.Count > 0)
                {
                    Cmb_Unidad_Responsable.Items.Clear();
                    Cmb_Unidad_Responsable.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
                    Cmb_Unidad_Responsable.DataTextField = "NOMBRE";
                    Cmb_Unidad_Responsable.DataSource = Dt_Dependencias;
                    Cmb_Unidad_Responsable.DataBind();
                    Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                }
            }

            if (Hf_Tipo_Calendario.Value.Equals("Dependencia"))
            {

                Dependencia_Empleado = Cls_Sessiones.Dependencia_ID_Empleado; //obtenemos la dependencia del usuario logueado
                if (!string.IsNullOrEmpty(Hf_Dependencia_ID.Value.Trim()))
                {
                    Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dependencia_Empleado));
                }
                Cmb_Unidad_Responsable.Enabled = true;
                Llenar_Combo_Programas();
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al Llenar_Combo_Unidad_Responsable ERROR[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Programas
    ///DESCRIPCIÓN          : Llena el combo de programas
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 14/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Combo_Programas()
    {
        Cls_Ope_Psp_Limite_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Limite_Presupuestal_Negocio();//Instancia con la clase de negocios

        Cmb_Programa.Items.Clear(); //limpiamos el combo
        if (Cmb_Unidad_Responsable.SelectedIndex > 0)
        {
            
           
            if (Hf_Tipo_Calendario.Value.Trim().Equals("Dependencia"))
            {
                Negocio.P_Dependencia_ID = Hf_Dependencia_ID.Value.Trim();
            }
            else {
                Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value.Trim();
            }

            Cmb_Programa.DataSource = Negocio.Consultar_Programa_Unidades_Responsables();
            Cmb_Programa.DataTextField = "NOMBRE";
            Cmb_Programa.DataValueField = Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id;
            Cmb_Programa.DataBind();
            Cmb_Programa.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));

            Cmb_Programa.SelectedIndex = Cmb_Programa.Items.IndexOf(Cmb_Programa.Items.FindByValue(Hf_Programa.Value.Trim()));
        }
    }
    #endregion

    #region (Metodo Enviar Correo)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Datos_Correo
        ///DESCRIPCIÓN          : Envia un correo a un usuario
        ///PROPIEDADES          : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 14/Febrero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static void Obtener_Datos_Correo(DataTable Dt, String CA, String Estatus, String Comentario)
        {
            String Email = String.Empty;
            String Empleado = String.Empty;
            String Email_PSP = String.Empty;
            String Empleado_PSP = String.Empty;
            String Texto_Correo = String.Empty;
            String Asunto_Correo = String.Empty;

            try
            {
                //OBTENEMOS LOS DATOS DEL CORREO
                if (Dt != null)
                {
                    if (Dt.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt.Rows)
                        {
                            if (Dr["TIPO"].ToString().Trim().Equals("COORDINADOR_PSP"))
                            {
                                Empleado_PSP = Dr["NOMBRE"].ToString().Trim();
                                Email_PSP = Dr["EMAIL"].ToString().Trim();
                            }
                            else
                            {
                                Empleado = Dr["NOMBRE"].ToString().Trim();
                                Email = Dr["EMAIL"].ToString().Trim();
                            }
                        }


                        //ENVIAMOS EL CORREO CORRESPONDIENTE AVISANDO DE LA AUTORIZACION O RECHAZO DEL CALENDARIO DE PRESUPUESTOS

                        if (Estatus.Trim().Equals("AUTORIZADO"))
                        {
                            Texto_Correo = "<html>" +
                                 "<body> ESTIMADO(A): <b>" + Empleado + "</b><br /><br />" +
                                       "POR MEDIO DEL PRESENTE RECIBA UN CORDIAL SALUDO, A LA VEZ QUE LE INFORMO QUE <br />" +
                                       "LA CALENDARIZACIÓN DE PRESUPUESTO CORRESPONDIENTE A SU ÁREA: <b>" + CA + "</b> <br />" +
                                       "HA SIDO <b>AUTORIZADO</b>.<br /><br /><br /><br />" +
                                       "SIN MÁS POR EL MOMENTO, ME DESPIDO AGRADECIENDO SU ATENCIÓN. <br />" +
                                   "</body>" +
                             "</html>";
                        }
                        else
                        {
                            Texto_Correo = "<html>" +
                                 "<body> ESTIMADO(A): <b>" + Empleado + "</b><br /><br />" +
                                       "POR MEDIO DEL PRESENTE RECIBA UN CORDIAL SALUDO, A LA VEZ QUE LE INFORMO QUE <br />" +
                                       "LA CALENDARIZACIÓN DE PRESUPUESTO CORRESPONDIENTE A SU ÁREA: <b>" + CA + "</b> <br />" +
                                       "HA SIDO <b>RECHAZADO</b>, RECIBIENDO LOS SIGUIENTES COMENTARIOS POR PARTE DEL REVISOR DEL PRESUPUESTO; <br /><br />" +
                                       "<b>COMENTARIOS:</b> " + Comentario + "<br /><br />" +
                                       "FAVOR DE HACER LOS CAMBIOS CORRESPONDIENTES A LA CALENDARIZACIÓN DE PRESUPUESTOS PARA QUE PUEDA SER AUTORIZADO. <br /><br /><br /><br />" +
                                       "SIN MÁS POR EL MOMENTO, ME DESPIDO AGRADECIENDO SU ATENCIÓN <br />" +
                                   "</body>" +
                             "</html>";
                        }

                        Asunto_Correo = "Estatus calendarización de presupuesto.";

                        Enviar_Correo(Email, Email_PSP, Asunto_Correo, Texto_Correo);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Envio de Correo " + ex.Message, ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Enviar_Correo
        ///DESCRIPCIÓN          : Envia un correo a un usuario
        ///PROPIEDADES          : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 14/Febrero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static void Enviar_Correo(String Para, String Copia_Para, String Asunto, String Texto_Correo)
        {
            MailMessage Correo = new MailMessage(); //obtenemos el objeto del correo
            //variables para guardar los datos de los parametros del correo
            String Puerto = String.Empty;
            String Usuario_Correo = String.Empty;
            String De = String.Empty;
            String Servidor = String.Empty;
            String Contraseña = String.Empty;

            try
            {
                Puerto = ConfigurationManager.AppSettings["Puerto_Correo"];
                Usuario_Correo = ConfigurationManager.AppSettings["Usuario_Correo"];
                De = ConfigurationManager.AppSettings["Correo_Saliente"];
                Servidor = ConfigurationManager.AppSettings["Servidor_Correo"];
                Contraseña = ConfigurationManager.AppSettings["Password_Correo"];

                if (!String.IsNullOrEmpty(Para) && !String.IsNullOrEmpty(Puerto)
                    && !String.IsNullOrEmpty(Usuario_Correo) && !String.IsNullOrEmpty(De)
                    && !String.IsNullOrEmpty(Servidor) && !String.IsNullOrEmpty(Contraseña))
                {
                    Correo.To.Clear();
                    Correo.To.Add(Para);
                    //validamos que no venga vacio el correo al que mandaremos copia
                    if (!String.IsNullOrEmpty(Copia_Para))
                    {
                        Correo.CC.Add(Copia_Para);
                    }

                    Correo.From = new MailAddress(De, Usuario_Correo.Trim());
                    Correo.Subject = Asunto.Trim();
                    Correo.SubjectEncoding = System.Text.Encoding.UTF8;

                    if ((!Correo.From.Equals("") || Correo.From != null) && (!Correo.To.Equals("") || Correo.To != null))
                    {
                        Correo.Body = Texto_Correo;
                        Correo.BodyEncoding = System.Text.Encoding.UTF8;
                        Correo.IsBodyHtml = true;

                        SmtpClient cliente_correo = new SmtpClient();
                        cliente_correo.Port = int.Parse(Puerto);
                        cliente_correo.UseDefaultCredentials = true;
                        cliente_correo.Credentials = new System.Net.NetworkCredential(De, Contraseña);
                        cliente_correo.Host = Servidor;
                        cliente_correo.Send(Correo);
                        Correo = null;
                    }
                    else
                    {
                        throw new Exception("No se tiene configurada una cuenta de correo, favor de notificar");
                    }
                }
            }
            catch (SmtpException ex)
            {
                throw new Exception("Envio de Correo " + ex.Message, ex);
            }
        }
    #endregion
    #endregion

    #region EVENTOS

    #region EVENTOS GENERALES
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
    ///DESCRIPCIÓN          : Evento del boton de salir
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 10/Noviembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Salir_Click(object sender, EventArgs e)
    {
        switch (Btn_Salir.ToolTip)
        {
            case "Cancelar":
                Limpiar_Sessiones();
                Autorizar_Presupuesto_Inicio();
                break;
            case "Salir":
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "", "window.close();", true);
                break;
            case "Regresar":
                Limpiar_Sessiones();
                Autorizar_Presupuesto_Inicio();
                break;
        }//fin del switch
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Nuevo_Click
    ///DESCRIPCIÓN          : Evento del boton nuevo
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 10/Noviembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Nuevo_Click(object sender, EventArgs e)
    {
        Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
        Mostrar_Ocultar_Error(false);
        DataTable Dt_Partidas_Asignadas = new DataTable();
        DataTable Dt_Correo_Empleado = new DataTable();
        String Id_Empleado = String.Empty;
        String Comentario = String.Empty;
        String Estatus = String.Empty;
        String CA = String.Empty;
        String Tipo_Calendario = String.Empty;

        try
        {
            switch (Btn_Nuevo.ToolTip)
            {
                case "Modificar":
                    if (!String.IsNullOrEmpty(Hf_Dependencia_ID.Value.Trim()) || !String.IsNullOrEmpty(Hf_Empleado_ID.Value.Trim()))
                    {
                        Estado_Botones("nuevo");
                        Limpiar_Formulario("Datos_Producto");
                        Habilitar_Forma(true);
                        Grid_Partida_Asignada.SelectedIndex = -1;
                        Llenar_Grid_Partida_Asignada();
                    }
                    else
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar un registro de la tabla. <br />";
                        Mostrar_Ocultar_Error(true);
                    }
                    break;
                case "Guardar":
                    if (Validar_Datos())
                    {
                        Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas_Autorizacion"];

                        Negocio.P_Anio_Presupuesto = Hf_Anio.Value.Trim();
                        Negocio.P_Fuente_Financiamiento_ID = Hf_Fte_Financiamiento.Value.Trim();
                        Negocio.P_Dependencia_ID = Hf_Dependencia_ID.Value.Trim();
                        Negocio.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                        Negocio.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado;
                        Negocio.P_Estatus = Cmb_Estatus.SelectedValue.Trim();
                        Negocio.P_Comentario = Txt_Comentarios.Text.Trim();
                        Negocio.P_Dt_Datos = Dt_Partidas_Asignadas;
                        Negocio.P_Total = Txt_Total_Ajuste.Text.Trim().Replace("$", "");
                        Negocio.Usuario_ID_Creo = Hf_Correo_Empleado.Value.Trim();

                        if (Hf_Tipo_Calendario.Value.Trim().Equals("Empleado"))
                        {
                            Negocio.P_Empleado_ID = Hf_Empleado_ID.Value.Trim();
                            Negocio.P_Dependencia_ID = string.Empty;
                        }
                        else
                        {
                            Negocio.P_Empleado_ID = string.Empty;
                        }


                        if (Negocio.Guardar_Historial_Calendario())
                        {
                            Limpiar_Sessiones();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alta", "alert('Alta Exitosa');", true);
                            CA = Cmb_Unidad_Responsable.SelectedItem.Text.Trim();
                            Estatus = Cmb_Estatus.SelectedItem.Value.Trim();
                            Comentario = Txt_Comentarios.Text.Trim();
                            Id_Empleado = Hf_Correo_Empleado.Value.Trim();
                            Tipo_Calendario = Hf_Tipo_Calendario.Value.Trim();
                            Autorizar_Presupuesto_Inicio();

                            //validamos si el presupuesto es aceptado o rechazado para mandar correo
                            if (Estatus.Trim().Equals("RECHAZADO") || Estatus.Trim().Equals("AUTORIZADO"))
                            {
                                if (!String.IsNullOrEmpty(Id_Empleado.Trim()))
                                {
                                    if (Tipo_Calendario.Trim().Equals("Empleado"))
                                    {
                                        CA = String.Empty;
                                    }

                                    Negocio.P_Empleado_ID = Hf_Correo_Empleado.Value.Trim();
                                    Dt_Correo_Empleado = Negocio.Consultar_Email_Empleado_Creo();

                                    Obtener_Datos_Correo(Dt_Correo_Empleado, CA.Trim(), Estatus.Trim(), Comentario.Trim());
                                }
                            }
                        }
                    }
                    else
                    {
                        Mostrar_Ocultar_Error(true);
                    }
                    break;
            }//fin del swirch
        }
        catch (Exception ex)
        {
            Lbl_Encanezado_Error.Text = "Error al mandar el correo ERROR[" + ex.Message + "]";
            Mostrar_Ocultar_Error(true);
        }
        finally 
        {
            //Autorizar_Presupuesto_Inicio();
        }
    }//fin del boton Nuevo

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Agregar_Click
    ///DESCRIPCIÓN          : Evento del boton de agregar un nuevo presupuesto de un producto
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 11/Noviembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Agregar_Click(object sender, EventArgs e)
    {
        DataTable Dt_Partidas_Asignadas = new DataTable();
        Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas_Autorizacion"];
        Mostrar_Ocultar_Error(false);
        try
        {
            if (Validar_Datos_Productos())
            {
                if (Dt_Partidas_Asignadas == null)
                {
                    Crear_Dt_Partidas_Asignadas();
                    Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas_Autorizacion"];
                }

                Agregar_Fila_Dt_Partidas_Asignadas();
                Limpiar_Formulario("Datos_Partida");
                Llenar_Grid_Partida_Asignada();
                Calcular_Total_Presupuestado();
            }
            else
            {
                Mostrar_Ocultar_Error(true);
            }
            Calcular_Restante();
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de agregar un registro a la tabla. Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Eliminar_Click
    ///DESCRIPCIÓN          : Evento del boton de eliminar un registro de un presupuesto de un producto
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 14/Noviembre/2011 
    ///MODIFICO             : Jennyfer Ivonne Ceja Lemus
    ///FECHA_MODIFICO       : 22/Agosto/2012
    ///CAUSA_MODIFICACIÓN...: Debido a que se agrego el campo Subnivel Presupuestal a la calendarizacion
    ///*********************************************************************************************************
    protected void Btn_Eliminar_Click(object sender, EventArgs e)
    {
        DataTable Dt_Partidas_Asignadas = new DataTable();
        ImageButton Btn_Eliminar = (ImageButton)sender;
        Int32 No_Fila = -1;
        String Id = String.Empty;
        Double Ene = 0.00;
        Double Feb = 0.00;
        Double Mar = 0.00;
        Double Abr = 0.00;
        Double May = 0.00;
        Double Jun = 0.00;
        Double Jul = 0.00;
        Double Ago = 0.00;
        Double Sep = 0.00;
        Double Oct = 0.00;
        Double Nov = 0.00;
        Double Dic = 0.00;
        Double Precio = 0.00;

        try
        {
            Id = Btn_Eliminar.CommandArgument.ToString().Trim();
            Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas_Autorizacion"];
            if (Dt_Partidas_Asignadas != null)
            {
                if (Dt_Partidas_Asignadas.Rows.Count > 0)
                {
                    foreach (DataRow Dr in Dt_Partidas_Asignadas.Rows)
                    {
                        No_Fila++;
                        if (Dr["ID"].ToString().Trim().Equals(Id))
                        {
                            if (Hf_Tipo_Calendario.Value.Trim().Equals("Empleado"))
                            {
                                Llenar_Combo_Unidad_Responsable();
                                Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Partidas_Asignadas.Rows[No_Fila]["DEPENDENCIA_ID"].ToString().Trim()));
                                Llenar_Combo_Programas();
                                Cmb_Programa.SelectedIndex = Cmb_Programa.Items.IndexOf(Cmb_Programa.Items.FindByValue(Dt_Partidas_Asignadas.Rows[No_Fila]["PROYECTO_ID"].ToString().Trim()));
                            }
                            Llenar_Combo_Capitulos();
                            LLenar_Presupuesto_Dependencia();
                            Cmb_Capitulos.SelectedIndex = Cmb_Capitulos.Items.IndexOf(Cmb_Capitulos.Items.FindByValue(Dt_Partidas_Asignadas.Rows[No_Fila]["CAPITULO_ID"].ToString().Trim()));
                            Llenar_Combo_Partidas();
                            Cmb_Partida_Especifica.SelectedIndex = Cmb_Partida_Especifica.Items.IndexOf(Cmb_Partida_Especifica.Items.FindByValue(Dt_Partidas_Asignadas.Rows[No_Fila]["PARTIDA_ID"].ToString().Trim()));
                            Cmb_Partida_Especifica.Enabled = true;
                            Llenar_Combo_Productos();
                            Consultar_Combo_Stock();
                            Txt_Justificacion.Text = Dt_Partidas_Asignadas.Rows[No_Fila]["JUSTIFICACION"].ToString().Trim();
                            if (!string.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["PRODUCTO_ID"].ToString().Trim()))
                            {
                                Cmb_Producto.SelectedIndex = Cmb_Producto.Items.IndexOf(Cmb_Producto.Items.FindByValue(Dt_Partidas_Asignadas.Rows[No_Fila]["PRODUCTO_ID"].ToString().Trim()));
                                Precio = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["PRECIO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["PRECIO"].ToString().Trim());
                                Hf_Precio.Value = Precio.ToString();
                                Hf_Producto_ID.Value = Dt_Partidas_Asignadas.Rows[No_Fila]["PRODUCTO_ID"].ToString().Trim();
                                Cmb_Producto.Enabled = true;

                                if (Precio > 0)
                                {
                                    Ene = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["ENERO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["ENERO"].ToString().Trim());
                                    Feb = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["FEBRERO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["FEBRERO"].ToString().Trim());
                                    Mar = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["MARZO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["MARZO"].ToString().Trim());
                                    Abr = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["ABRIL"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["ABRIL"].ToString().Trim());
                                    May = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["MAYO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["MAYO"].ToString().Trim());
                                    Jun = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["JUNIO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["JUNIO"].ToString().Trim());
                                    Jul = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["JULIO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["JULIO"].ToString().Trim());
                                    Ago = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["AGOSTO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["AGOSTO"].ToString().Trim());
                                    Sep = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["SEPTIEMBRE"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["SEPTIEMBRE"].ToString().Trim());
                                    Oct = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["OCTUBRE"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["OCTUBRE"].ToString().Trim());
                                    Nov = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["NOVIEMBRE"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["NOVIEMBRE"].ToString().Trim());
                                    Dic = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["DICIEMBRE"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["DICIEMBRE"].ToString().Trim());

                                    Lbl_Cantidad.Text = "Cantidad";
                                    Lbl_Txt_Enero.Text = Convert.ToString(Ene / Precio);
                                    Lbl_Txt_Febrero.Text = Convert.ToString(Feb / Precio);
                                    Lbl_Txt_Marzo.Text = Convert.ToString(Mar / Precio);
                                    Lbl_Txt_Abril.Text = Convert.ToString(Abr / Precio);
                                    Lbl_Txt_Mayo.Text = Convert.ToString(May / Precio);
                                    Lbl_Txt_Junio.Text = Convert.ToString(Jun / Precio);
                                    Lbl_Txt_Julio.Text = Convert.ToString(Jul / Precio);
                                    Lbl_Txt_Agosto.Text = Convert.ToString(Ago / Precio);
                                    Lbl_Txt_Septiembre.Text = Convert.ToString(Sep / Precio);
                                    Lbl_Txt_Octubre.Text = Convert.ToString(Oct / Precio);
                                    Lbl_Txt_Noviembre.Text = Convert.ToString(Nov / Precio);
                                    Lbl_Txt_Diciembre.Text = Convert.ToString(Dic / Precio);
                                }
                            }
                            else 
                            {
                                if (Tr_Productos.Visible)
                                {
                                    Cmb_Producto.Enabled = true;
                                }
                            }

                            Txt_Enero.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["ENERO"]);
                            Txt_Febrero.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["FEBRERO"]);
                            Txt_Marzo.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["MARZO"]);
                            Txt_Abril.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["ABRIL"]);
                            Txt_Mayo.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["MAYO"]);
                            Txt_Junio.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["JUNIO"]);
                            Txt_Julio.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["JULIO"]);
                            Txt_Agosto.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["AGOSTO"]);
                            Txt_Septiembre.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["SEPTIEMBRE"]);
                            Txt_Octubre.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["OCTUBRE"]);
                            Txt_Noviembre.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["NOVIEMBRE"]);
                            Txt_Diciembre.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["DICIEMBRE"]);
                            Txt_Total.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["IMPORTE_TOTAL"]);

                            Dt_Partidas_Asignadas.Rows.RemoveAt(No_Fila);
                            Session["Dt_Partidas_Asignadas_Autorizacion"] = Dt_Partidas_Asignadas;
                            //Llenar_Grid_Partida_Asignada();
                            Obtener_Consecutivo_ID();
                            Dt_Partidas_Asignadas = Crear_Dt_Partida_Asignada();
                            Grid_Partida_Asignada.Columns[1].Visible = true;
                            Grid_Partida_Asignada.DataSource = Dt_Partidas_Asignadas;
                            Grid_Partida_Asignada.DataBind();
                            Grid_Partida_Asignada.Columns[1].Visible = false;

                            Calcular_Total_Presupuestado();
                            break;
                        }
                    }
                }
            }
            Calcular_Restante();
        }
        catch (Exception ex)
        {
            throw new Exception("Error al eliminar el registro el grid. Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_ALimpiar_Click
    ///DESCRIPCIÓN          : Evento del boton de limpiar los campos del presupuesto
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 28/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Limpiar_Click(object sender, EventArgs e)
    {
        Mostrar_Ocultar_Error(false);
        try
        {
            Limpiar_Formulario("Datos_Partida");
            Calcular_Total_Presupuestado();
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de a limpiar los datos del formulario. Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Porcentaje_Click
    ///DESCRIPCIÓN          : Evento del boton de eliminar un registro de un presupuesto de un producto
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 17/Mayo/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Porcentaje_Click(object sender, EventArgs e)
    {
        DataTable Dt_Partidas_Asignadas = new DataTable();
        Double Ene = 0.00;
        Double Feb = 0.00;
        Double Mar = 0.00;
        Double Abr = 0.00;
        Double May = 0.00;
        Double Jun = 0.00;
        Double Jul = 0.00;
        Double Ago = 0.00;
        Double Sep = 0.00;
        Double Oct = 0.00;
        Double Nov = 0.00;
        Double Dic = 0.00;
        Double Total = 0.00;
        Double Porcentaje;
        try
        {
            if (!String.IsNullOrEmpty(Txt_Porcentaje.Text.Trim()))
            {
                Porcentaje = Convert.ToDouble(String.IsNullOrEmpty(Txt_Porcentaje.Text.Trim()) ? "0" : Txt_Porcentaje.Text.Trim()) / 100;
                if (Porcentaje > 0 || Porcentaje < 0)
                {
                    Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas_Autorizacion"];
                    if (Dt_Partidas_Asignadas != null)
                    {
                        if (Dt_Partidas_Asignadas.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Partidas_Asignadas.Rows)
                            {
                                Ene = Convert.ToDouble(String.IsNullOrEmpty(Dr["ENERO"].ToString()) ? "0" : Dr["ENERO"].ToString().Trim());
                                Feb = Convert.ToDouble(String.IsNullOrEmpty(Dr["FEBRERO"].ToString()) ? "0" : Dr["FEBRERO"].ToString().Trim());
                                Mar = Convert.ToDouble(String.IsNullOrEmpty(Dr["MARZO"].ToString()) ? "0" : Dr["MARZO"].ToString().Trim());
                                Abr = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABRIL"].ToString()) ? "0" : Dr["ABRIL"].ToString().Trim());
                                May = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAYO"].ToString()) ? "0" : Dr["MAYO"].ToString().Trim());
                                Jun = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUNIO"].ToString()) ? "0" : Dr["JUNIO"].ToString().Trim());
                                Jul = Convert.ToDouble(String.IsNullOrEmpty(Dr["JULIO"].ToString()) ? "0" : Dr["JULIO"].ToString().Trim());
                                Ago = Convert.ToDouble(String.IsNullOrEmpty(Dr["AGOSTO"].ToString()) ? "0" : Dr["AGOSTO"].ToString().Trim());
                                Sep = Convert.ToDouble(String.IsNullOrEmpty(Dr["SEPTIEMBRE"].ToString()) ? "0" : Dr["SEPTIEMBRE"].ToString().Trim());
                                Oct = Convert.ToDouble(String.IsNullOrEmpty(Dr["OCTUBRE"].ToString()) ? "0" : Dr["OCTUBRE"].ToString().Trim());
                                Nov = Convert.ToDouble(String.IsNullOrEmpty(Dr["NOVIEMBRE"].ToString()) ? "0" : Dr["NOVIEMBRE"].ToString().Trim());
                                Dic = Convert.ToDouble(String.IsNullOrEmpty(Dr["DICIEMBRE"].ToString()) ? "0" : Dr["DICIEMBRE"].ToString().Trim());
                                Total = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_TOTAL"].ToString()) ? "0" : Dr["IMPORTE_TOTAL"].ToString().Trim());

                                Ene = Ene + (Ene * Porcentaje);
                                Feb = Feb + (Feb * Porcentaje);
                                Mar = Mar + (Mar * Porcentaje);
                                Abr = Abr + (Abr * Porcentaje);
                                May = May + (May * Porcentaje);
                                Jun = Jun + (Jun * Porcentaje);
                                Jul = Jul + (Jul * Porcentaje);
                                Ago = Ago + (Ago * Porcentaje);
                                Sep = Sep + (Sep * Porcentaje);
                                Oct = Oct + (Oct * Porcentaje);
                                Nov = Nov + (Nov * Porcentaje);
                                Dic = Dic + (Dic * Porcentaje);
                                Total = Total + (Total * Porcentaje);

                                Dr["ENERO"] = String.Format("{0:n}", Ene);
                                Dr["FEBRERO"] = String.Format("{0:n}", Feb);
                                Dr["MARZO"] = String.Format("{0:n}", Mar);
                                Dr["ABRIL"] = String.Format("{0:n}", Abr);
                                Dr["MAYO"] = String.Format("{0:n}", May);
                                Dr["JUNIO"] = String.Format("{0:n}", Jun);
                                Dr["JULIO"] = String.Format("{0:n}", Jul);
                                Dr["AGOSTO"] = String.Format("{0:n}", Ago);
                                Dr["SEPTIEMBRE"] = String.Format("{0:n}", Sep);
                                Dr["OCTUBRE"] = String.Format("{0:n}", Oct);
                                Dr["NOVIEMBRE"] = String.Format("{0:n}", Nov);
                                Dr["DICIEMBRE"] = String.Format("{0:n}", Dic);
                                Dr["IMPORTE_TOTAL"] = String.Format("{0:n}", Total);
                            }

                            Session["Dt_Partidas_Asignadas_Autorizacion"] = Dt_Partidas_Asignadas;
                            Llenar_Grid_Partida_Asignada();
                            Calcular_Total_Presupuestado();
                            Llenar_Combo_Estatus();
                            Txt_Porcentaje.Text = "";
                        }
                    }
                }
            }
            else
            {
                Lbl_Encanezado_Error.Text = "Favor de introducir el porcentaje para aplicar al presupuesto.";
                Lbl_Error.Text = "";
                Mostrar_Ocultar_Error(true);
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al aumentar el porcentaje del presupuesto. Error[" + ex.Message + "]");
        }
    }

    #endregion

    #region EVENTOS GRID
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Partidas_Asignadas_RowDataBound
    ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 16/Noviembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Grid_Partida_Asignada_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        DataTable Dt_Partidas_Asignadas = new DataTable();
        Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas_Autorizacion"];

        try
        {
            if (Dt_Partidas_Asignadas != null)
            {
                if (Dt_Partidas_Asignadas.Rows.Count > 0)
                {
                    GridView Grid_Partidas_Detalle = (GridView)e.Row.Cells[4].FindControl("Grid_Partidas_Asignadas_Detalle");
                    DataTable Dt_Detalles = new DataTable();
                    String Partida_ID = String.Empty;

                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        Partida_ID = e.Row.Cells[1].Text.Trim();
                        Dt_Detalles = Crear_Dt_Detalles(Partida_ID);

                        Grid_Partidas_Detalle.Columns[0].Visible = true;
                        Grid_Partidas_Detalle.Columns[1].Visible = true;
                        Grid_Partidas_Detalle.Columns[2].Visible = true;
                        Grid_Partidas_Detalle.Columns[3].Visible = true;
                        Grid_Partidas_Detalle.Columns[4].Visible = true;
                        Grid_Partidas_Detalle.Columns[5].Visible = true;
                        Grid_Partidas_Detalle.Columns[6].Visible = true;
                        Grid_Partidas_Detalle.Columns[7].Visible = true;
                        Grid_Partidas_Detalle.Columns[8].Visible = true;
                        Grid_Partidas_Detalle.Columns[9].Visible = true;
                        Grid_Partidas_Detalle.Columns[10].Visible = true;
                        Grid_Partidas_Detalle.Columns[25].Visible = true;
                        Grid_Partidas_Detalle.Columns[26].Visible = true;
                        Grid_Partidas_Detalle.DataSource = Dt_Detalles;
                        Grid_Partidas_Detalle.DataBind();
                        Grid_Partidas_Detalle.Columns[1].Visible = false;
                        Grid_Partidas_Detalle.Columns[2].Visible = false;
                        Grid_Partidas_Detalle.Columns[3].Visible = false;
                        Grid_Partidas_Detalle.Columns[4].Visible = false;
                        Grid_Partidas_Detalle.Columns[5].Visible = false;
                        Grid_Partidas_Detalle.Columns[6].Visible = false;
                        Grid_Partidas_Detalle.Columns[7].Visible = false;
                        Grid_Partidas_Detalle.Columns[8].Visible = false;
                        Grid_Partidas_Detalle.Columns[26].Visible = false;

                        if (Hf_Tipo_Calendario.Value.Trim().Equals("Dependencia"))
                        {
                            Grid_Partidas_Detalle.Columns[9].Visible = false;
                        }
                        else
                        {
                            Grid_Partidas_Detalle.Columns[10].Visible = false;
                        }

                        if (Btn_Nuevo.ToolTip == "Guardar")
                        {
                            Grid_Partidas_Detalle.Columns[0].Visible = false;
                        }
                        else
                        {
                            Grid_Partidas_Detalle.Columns[25].Visible = false;
                        }
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error:[" + Ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Partidas_Asignadas_Detalle_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 22/Noviembre/2011 
    ///MODIFICO             : Jennyfer Ivonne Ceja Lemus
    ///FECHA_MODIFICO       : 22/Agosto/2012
    ///CAUSA_MODIFICACIÓN...: Porque se agrego un campo llamado subnivel presupuestal a la calendarizacion presupuestal
    ///*********************************************************************************************************
    protected void Grid_Partidas_Asignadas_Detalle_SelectedIndexChanged(object sender, EventArgs e)
    {
        Double Ene = 0.00;
        Double Feb = 0.00;
        Double Mar = 0.00;
        Double Abr = 0.00;
        Double May = 0.00;
        Double Jun = 0.00;
        Double Jul = 0.00;
        Double Ago = 0.00;
        Double Sep = 0.00;
        Double Oct = 0.00;
        Double Nov = 0.00;
        Double Dic = 0.00;
        Double Precio = 0.00;
        DataTable Dt_Partidas_Asignadas = new DataTable();
        Int32 No_Fila = -1;
        String Id = String.Empty;
        try
        {
            Limpiar_Formulario("Datos_Producto");

            Id = ((GridView)sender).SelectedRow.Cells[26].Text;
            Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas_Autorizacion"];
            if (Dt_Partidas_Asignadas != null)
            {
                if (Dt_Partidas_Asignadas.Rows.Count > 0)
                {
                    foreach (DataRow Dr in Dt_Partidas_Asignadas.Rows)
                    {
                        No_Fila++;
                        if (Dr["ID"].ToString().Trim().Equals(Id))
                        {
                            if(Hf_Tipo_Calendario.Value.Trim().Equals("Empleado"))
                            {
                                Llenar_Combo_Unidad_Responsable();
                                Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Partidas_Asignadas.Rows[No_Fila]["DEPENDENCIA_ID"].ToString().Trim()));
                                Llenar_Combo_Programas();
                                Cmb_Programa.SelectedIndex = Cmb_Programa.Items.IndexOf(Cmb_Programa.Items.FindByValue(Dt_Partidas_Asignadas.Rows[No_Fila]["PROYECTO_ID"].ToString().Trim()));
                            }
                            
                            Llenar_Combo_Capitulos();
                            LLenar_Presupuesto_Dependencia();
                            Cmb_Capitulos.SelectedIndex = Cmb_Capitulos.Items.IndexOf(Cmb_Capitulos.Items.FindByValue(Dt_Partidas_Asignadas.Rows[No_Fila]["CAPITULO_ID"].ToString().Trim()));
                            Llenar_Combo_Partidas();
                            Cmb_Partida_Especifica.SelectedIndex = Cmb_Partida_Especifica.Items.IndexOf(Cmb_Partida_Especifica.Items.FindByValue(Dt_Partidas_Asignadas.Rows[No_Fila]["PARTIDA_ID"].ToString().Trim()));
                            Llenar_Combo_Productos();
                            Consultar_Combo_Stock();
                            Txt_Justificacion.Text = Dt_Partidas_Asignadas.Rows[No_Fila]["JUSTIFICACION"].ToString().Trim();
                            if (!string.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["PRODUCTO_ID"].ToString().Trim()))
                            {
                                Cmb_Producto.SelectedIndex = Cmb_Producto.Items.IndexOf(Cmb_Producto.Items.FindByValue(Dt_Partidas_Asignadas.Rows[No_Fila]["PRODUCTO_ID"].ToString().Trim()));
                                Precio = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["PRECIO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["PRECIO"].ToString().Trim());

                                if (Precio > 0)
                                {
                                    Ene = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["ENERO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["ENERO"].ToString().Trim());
                                    Feb = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["FEBRERO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["FEBRERO"].ToString().Trim());
                                    Mar = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["MARZO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["MARZO"].ToString().Trim());
                                    Abr = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["ABRIL"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["ABRIL"].ToString().Trim());
                                    May = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["MAYO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["MAYO"].ToString().Trim());
                                    Jun = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["JUNIO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["JUNIO"].ToString().Trim());
                                    Jul = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["JULIO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["JULIO"].ToString().Trim());
                                    Ago = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["AGOSTO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["AGOSTO"].ToString().Trim());
                                    Sep = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["SEPTIEMBRE"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["SEPTIEMBRE"].ToString().Trim());
                                    Oct = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["OCTUBRE"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["OCTUBRE"].ToString().Trim());
                                    Nov = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["NOVIEMBRE"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["NOVIEMBRE"].ToString().Trim());
                                    Dic = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["DICIEMBRE"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["DICIEMBRE"].ToString().Trim());

                                    Lbl_Cantidad.Text = "Cantidad";
                                    Lbl_Txt_Enero.Text = Convert.ToString(Ene / Precio);
                                    Lbl_Txt_Febrero.Text = Convert.ToString(Feb / Precio);
                                    Lbl_Txt_Marzo.Text = Convert.ToString(Mar / Precio);
                                    Lbl_Txt_Abril.Text = Convert.ToString(Abr / Precio);
                                    Lbl_Txt_Mayo.Text = Convert.ToString(May / Precio);
                                    Lbl_Txt_Junio.Text = Convert.ToString(Jun / Precio);
                                    Lbl_Txt_Julio.Text = Convert.ToString(Jul / Precio);
                                    Lbl_Txt_Agosto.Text = Convert.ToString(Ago / Precio);
                                    Lbl_Txt_Septiembre.Text = Convert.ToString(Sep / Precio);
                                    Lbl_Txt_Octubre.Text = Convert.ToString(Oct / Precio);
                                    Lbl_Txt_Noviembre.Text = Convert.ToString(Nov / Precio);
                                    Lbl_Txt_Diciembre.Text = Convert.ToString(Dic / Precio);
                                }
                            }

                            Txt_Enero.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["ENERO"]);
                            Txt_Febrero.Text = String.Format("{0:#,###,##0.00}",Dt_Partidas_Asignadas.Rows[No_Fila]["FEBRERO"]);
                            Txt_Marzo.Text = String.Format("{0:#,###,##0.00}",Dt_Partidas_Asignadas.Rows[No_Fila]["MARZO"]);
                            Txt_Abril.Text = String.Format("{0:#,###,##0.00}",Dt_Partidas_Asignadas.Rows[No_Fila]["ABRIL"]);
                            Txt_Mayo.Text = String.Format("{0:#,###,##0.00}",Dt_Partidas_Asignadas.Rows[No_Fila]["MAYO"]);
                            Txt_Junio.Text = String.Format("{0:#,###,##0.00}",Dt_Partidas_Asignadas.Rows[No_Fila]["JUNIO"]);
                            Txt_Julio.Text = String.Format("{0:#,###,##0.00}",Dt_Partidas_Asignadas.Rows[No_Fila]["JULIO"]);
                            Txt_Agosto.Text = String.Format("{0:#,###,##0.00}",Dt_Partidas_Asignadas.Rows[No_Fila]["AGOSTO"]);
                            Txt_Septiembre.Text = String.Format("{0:#,###,##0.00}",Dt_Partidas_Asignadas.Rows[No_Fila]["SEPTIEMBRE"]);
                            Txt_Octubre.Text = String.Format("{0:#,###,##0.00}",Dt_Partidas_Asignadas.Rows[No_Fila]["OCTUBRE"]);
                            Txt_Noviembre.Text = String.Format("{0:#,###,##0.00}",Dt_Partidas_Asignadas.Rows[No_Fila]["NOVIEMBRE"]);
                            Txt_Diciembre.Text = String.Format("{0:#,###,##0.00}",Dt_Partidas_Asignadas.Rows[No_Fila]["DICIEMBRE"]);
                            Txt_Total.Text = String.Format("{0:#,###,##0.00}",Dt_Partidas_Asignadas.Rows[No_Fila]["IMPORTE_TOTAL"]);
                            break;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al seleccionar el registro el grid. Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Partidas_Asignadas_Detalle_RowCreated
    ///DESCRIPCIÓN          : Evento del grid del movimiento del cursor sobre los registros
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 22/Noviembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Grid_Partidas_Asignadas_Detalle_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#507CD1';this.style.color='#FFFFFF'");
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;this.style.color=this.originalstyle;");
        }
    }

    //*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Dependencias_Presupuestadas_SelectedIndexChanged
    ///DESCRIPCIÓN          : Ejecuta la operacion de cambio de seleccion en listado 
    ///                       de unidades responsables.
    ///PARAMETROS           : 
    ///CREO                 : Leslie Gonzalez Vázquez
    ///FECHA_CREO           : 23/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Grid_Dependencias_Presupuestadas_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio(); //instancia con la clase de negocio
        String Dependencia_ID = String.Empty; ;
        DataTable Dt_Capitulos = new DataTable();
        Limpiar_Sessiones();
        try
        {
            if (Grid_Dependencias_Presupuestadas.SelectedIndex > (-1))
            {
                Div_Dependencias_Presupuestadas.Style.Add("display", "none");
                Div_Partidas_Asignadas.Style.Add("display", "block");
                Td_Psp_Anterior.Style.Add("display", "block");
                Mostrar_Ocultar_Error(false);
                Limpiar_Formulario("Todo");
                
                Hf_Tipo_Calendario.Value = HttpUtility.HtmlDecode(Grid_Dependencias_Presupuestadas.SelectedRow.Cells[9].Text).Trim();

                if (Hf_Tipo_Calendario.Value.Trim().Equals("Empleado"))
                {
                    Hf_Dependencia_ID.Value = String.Empty;
                    Hf_Empleado_ID.Value = HttpUtility.HtmlDecode(Grid_Dependencias_Presupuestadas.SelectedRow.Cells[8].Text).Trim();
                    Hf_Programa.Value = String.Empty;
                }else
                {
                    Hf_Dependencia_ID.Value = HttpUtility.HtmlDecode(Grid_Dependencias_Presupuestadas.SelectedRow.Cells[1].Text).Trim();
                    Hf_Empleado_ID.Value = String.Empty;
                    Hf_Programa.Value = HttpUtility.HtmlDecode(Grid_Dependencias_Presupuestadas.SelectedRow.Cells[2].Text).Trim();
                }

                Llenar_Combo_Unidad_Responsable();

                Llenar_Combo_Capitulos();
                Llenar_Combo_Estatus();
                LLenar_Presupuesto_Dependencia();
                Estado_Botones("inicial");
                Llenar_Grid_Partida_Asignada();
                Habilitar_Forma(false);
                Tr_Productos.Visible = false;
                Estado_Botones("inicial");
                Btn_Salir.ToolTip = "Regresar";

                Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(Grid_Dependencias_Presupuestadas.SelectedRow.Cells[4].Text.Trim()));
                Txt_Limite_Presupuestal.Text = String.Format("{0:c}", Convert.ToDouble(String.IsNullOrEmpty(Grid_Dependencias_Presupuestadas.SelectedRow.Cells[3].Text.Trim()) ? "0" : Grid_Dependencias_Presupuestadas.SelectedRow.Cells[3].Text.Trim()));
                Txt_Total_Ajuste.Text = String.Format("{0:c}", Convert.ToDouble(String.IsNullOrEmpty(Grid_Dependencias_Presupuestadas.SelectedRow.Cells[7].Text.Trim()) ? "0" : Grid_Dependencias_Presupuestadas.SelectedRow.Cells[7].Text.Replace("$", "").Trim()));
                Hf_Anio.Value = HttpUtility.HtmlDecode(Grid_Dependencias_Presupuestadas.SelectedRow.Cells[6].Text).Trim();
                Hf_Correo_Empleado.Value = HttpUtility.HtmlDecode(Grid_Dependencias_Presupuestadas.SelectedRow.Cells[10].Text).Trim();
                Calcular_Total_Presupuestado();
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al querer seleccionar un registro del grid de unidades Responsables Error[" + ex.Message + "]");
        }
    }
    #endregion

    #region EVENTOS COMBOS
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Unidad_Responsable_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento del combo de unidad responsable
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 10/Noviembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///******************************************************************************* 
    protected void Cmb_Unidad_Responsable_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Mostrar_Ocultar_Error(false);
            if (Hf_Tipo_Calendario.Value.Trim().Equals("Dependencia"))
            {
                Limpiar_Sessiones();
                Llenar_Combo_Capitulos();
                Cmb_Capitulos.Enabled = true;
                Cmb_Partida_Especifica.SelectedIndex = -1;
                Cmb_Partida_Especifica.Enabled = false;
                Cmb_Producto.SelectedIndex = -1;
                Tr_Productos.Visible = false;
                Limpiar_Formulario("Datos_Partida");
                Llenar_Combo_Programas();
                Cmb_Programa.Enabled = true;
            }
            else
            {
                Llenar_Combo_Programas();
                Cmb_Programa.Enabled = true;
                Cmb_Capitulos.Enabled = true;
                Cmb_Partida_Especifica.Enabled = true;
                Cmb_Producto.SelectedIndex = -1;
                Tr_Productos.Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error en el evento del combo de unidad responsable Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Capitulo_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento del combo de Capitulos
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 10/Noviembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///******************************************************************************* 
    protected void Cmb_Capitulo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Mostrar_Ocultar_Error(false);
            Llenar_Combo_Partidas();
            Cmb_Partida_Especifica.Enabled = true;
            Cmb_Producto.SelectedIndex = -1;
            Tr_Productos.Visible = false;
            Limpiar_Formulario("Datos_Partida");
        }
        catch (Exception ex)
        {
            throw new Exception("Error en el evento del combo de capitulos Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Partidas_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento del combo de partidas
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 11/Noviembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///******************************************************************************* 
    protected void Cmb_Partidas_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Mostrar_Ocultar_Error(false);
            Llenar_Combo_Productos();
            Consultar_Combo_Stock();
            Cmb_Producto.Enabled = true;
            Limpiar_Formulario("Datos_Partida");
        }
        catch (Exception ex)
        {
            throw new Exception("Error en el evento del combo de partidas Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Productos_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento del combo de productos
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 12/Noviembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///******************************************************************************* 
    protected void Cmb_Productos_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
        DataTable Dt_Productos = new DataTable();
        try
        {
            Mostrar_Ocultar_Error(false);
            Limpiar_Formulario("Datos_Producto");
            Grid_Partida_Asignada.SelectedIndex = -1;
            Negocio.P_Partida_ID = Cmb_Partida_Especifica.SelectedItem.Value.Trim();
            Negocio.P_Producto_ID = Cmb_Producto.SelectedItem.Value.Trim();
            Dt_Productos = Negocio.Consultar_Productos();

            if (Dt_Productos != null)
            {
                if (Dt_Productos.Rows.Count > 0)
                {
                    Hf_Precio.Value = Dt_Productos.Rows[0]["COSTO"].ToString().Trim();
                    Hf_Producto_ID.Value = Dt_Productos.Rows[0]["PRODUCTO_ID"].ToString().Trim();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error en el evento del combo de productos Error[" + ex.Message + "]");
        }
    }
    #endregion

    #endregion

    #region MODAL POPUP
    //*******************************************************************************
    //NOMBRE DE LA FUNCIÓN : Grid_Productos_SelectedIndexChanged
    //DESCRIPCIÓN          : Evento de seleccion de un registro del del grid
    //PARAMETROS           :   
    //CREO                 : Leslie González Vázquez
    //FECHA_CREO           : 15/Noviembre/2011 
    //MODIFICO             :
    //FECHA_MODIFICO       :
    //CAUSA_MODIFICACIÓN   :
    //*******************************************************************************
    protected void Grid_Productos_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Mostrar_Ocultar_Error(false);
            Hf_Producto_ID.Value = HttpUtility.HtmlDecode(Grid_Productos.SelectedRow.Cells[1].Text).Trim();
            Hf_Precio.Value = HttpUtility.HtmlDecode(Grid_Productos.SelectedRow.Cells[4].Text).Trim();
            Cmb_Producto.SelectedIndex = Cmb_Producto.Items.IndexOf(Cmb_Producto.Items.FindByValue(Hf_Producto_ID.Value.Trim()));
            Mpe_Busqueda_Productos.Hide();
            Lbl_Error_Busqueda.Text = "";
            Lbl_Error_Busqueda.Style.Add("display", "none");
            Img_Error_Busqueda.Style.Add("display", "none");
            Txt_Busqueda_Nombre_Producto.Text = "";
            Txt_Busqueda_Clave.Text = "";
            Lbl_Numero_Registros.Text = "";
            Grid_Productos.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception("Error en el evento de seleccionar un registro de la tabla de productos Error[" + ex.Message + "]");
        }
    }

    //*******************************************************************************
    //NOMBRE DE LA FUNCIÓN : Btn_Busqueda_Productos_Click
    //DESCRIPCIÓN          : Evento del boton de busquedas de productos
    //PARAMETROS           :   
    //CREO                 : Leslie González Vázquez
    //FECHA_CREO           : 15/Noviembre/2011 
    //MODIFICO             :
    //FECHA_MODIFICO       :
    //CAUSA_MODIFICACIÓN   :
    //*******************************************************************************
    protected void Btn_Busqueda_Productos_Click(object sender, EventArgs e)
    {
        Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio(); //Variable de conexión hacia la capa de Negocios
        DataTable Dt_Productos; //Variable que obtendra los datos de la consulta 
        try
        {

            if (Cmb_Partida_Especifica.SelectedIndex > 0)
            {
                if (!String.IsNullOrEmpty(Txt_Busqueda_Clave.Text.Trim()))
                {
                    Negocio.P_Clave_Producto = Txt_Busqueda_Clave.Text.Trim();
                }
                if (!String.IsNullOrEmpty(Txt_Busqueda_Nombre_Producto.Text.Trim()))
                {
                    Negocio.P_Nombre_Producto = Txt_Busqueda_Nombre_Producto.Text.Trim();
                }

                Negocio.P_Partida_ID = Cmb_Partida_Especifica.SelectedValue.Trim();
                Dt_Productos = Negocio.Consultar_Productos();
                Llenar_Grid_Productos(Dt_Productos);
                Mpe_Busqueda_Productos.Show();
                Lbl_Error_Busqueda.Style.Add("display", "none");
                Img_Error_Busqueda.Style.Add("display", "none");

                if (Dt_Productos is DataTable)
                    Lbl_Numero_Registros.Text = "Registros Encontrados: [" + Dt_Productos.Rows.Count + "]";
                else
                    Lbl_Numero_Registros.Text = "Registros Encontrados: [0]";
            }
            else
            {
                Lbl_Error_Busqueda.Text = "Favor de seleccionar una partida";
                Lbl_Error_Busqueda.Style.Add("display", "block");
                Img_Error_Busqueda.Style.Add("display", "block");
            }

        }
        catch (Exception ex)
        {
            throw new Exception("Consulta_Productos " + ex.Message.ToString(), ex);
        }
    }

    //*******************************************************************************
    //NOMBRE DE LA FUNCIÓN : Llenar_Grid_Productos
    //DESCRIPCIÓN          : Metodo para llenar el grid del modal popup de productos
    //PARAMETROS           1 Dt_Productos datatable que contendra los productos que mostraremos
    //CREO                 : Leslie González Vázquez
    //FECHA_CREO           : 15/Noviembre/2011 
    //MODIFICO             :
    //FECHA_MODIFICO       :
    //CAUSA_MODIFICACIÓN   :
    //*******************************************************************************
    protected void Llenar_Grid_Productos(DataTable Dt_Productos)
    {
        try
        {
            Grid_Productos.DataBind();
            if (Dt_Productos != null)
            {
                if (Dt_Productos.Rows.Count > 0)
                {
                    Grid_Productos.Columns[1].Visible = true;
                    Grid_Productos.Columns[4].Visible = true;
                    Grid_Productos.DataSource = Dt_Productos;
                    Grid_Productos.DataBind();
                    Grid_Productos.Columns[1].Visible = false;
                    Grid_Productos.Columns[4].Visible = false;
                }
            }

        }
        catch (Exception ex)
        {
            throw new Exception("Error al llenar el grid de productos Error[" + ex.Message + "]");
        }
    }
    #endregion
}