﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Frm_Ope_Psp_Vista_Presupuesto.aspx.cs" 
Inherits="paginas_Presupuestos_Frm_Ope_Psp_Vista_Presupuesto" Title="SIAC Sistema Integral Administrativo y Comercial" %>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Vista Previa Presupuestos</title>
    <link href="../estilos/estilo_paginas.css" rel="stylesheet" type="text/css" />
    <link href="../estilos/estilo_masterpage.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:Button ID="Btn_Actualizar_Presupuesto" runat="server" 
        Text="Actualizar Presupuesto"  Width="95%" 
        onclick="Btn_Actualizar_Presupuesto_Click"/>
        </br>
    <div style="background-color:White;width:95%" >
    <asp:label runat="server" id="Lbl_Errror" text="" ForeColor="Red"></asp:label>
    </div>
    <div style="overflow:auto;height:97%;width:97%;vertical-align:top;border-style:outset;border-color: Silver;">
    <table width="100%">
        <tr>
            <td colspan="4">
                
            </td>
        </tr>
        <tr>
            <td style="width: 99%" align="center" colspan="4">
                <div style="overflow:auto;height:600px;width:97%;vertical-align:top;border-style:outset;border-color: Silver;" >
                 <asp:GridView ID="Grid_Presupuestos" runat="server" 
                        AutoGenerateColumns="False" CssClass="GridView_1" GridLines="Both" 
                        HeaderStyle-Height="0%" Width="100%" OnRowDataBound="Grid_Presupuestos_RowDataBound">
                        <RowStyle CssClass="GridItem" />
                        <Columns>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                            <asp:ImageButton ID="Btn_Alerta" runat="server" ImageUrl="~/paginas/imagenes/gridview/circle_grey.png"/>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="5%" />
                            <ItemStyle HorizontalAlign="Center" Width="5%" />
                        </asp:TemplateField> 
                        <asp:BoundField DataField="FF" HeaderText="FF" Visible="true">
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Wrap="true" Width="9%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="AF" HeaderText="AF" 
                                Visible="true">
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Wrap="true" Width="9%" />
                        </asp:BoundField>
                         <asp:BoundField DataField="PROGRAMA" HeaderText="Programa" Visible="true">
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Wrap="true" Width="9%" />
                        </asp:BoundField>
                        
                        <asp:BoundField DataField="UR" HeaderText="UR" Visible="true">
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Wrap="true" Width="9%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="PARTIDA" HeaderText="Programa" Visible="true">
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Wrap="true" Width="9%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ENERO" HeaderText="ENERO" Visible="true">
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Wrap="true" Width="9%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FEBRERO" HeaderText="FEBRERO" Visible="true">
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Wrap="true" Width="9%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MARZO" HeaderText="MARZO" Visible="true">
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Wrap="true" Width="9%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ABRIL" HeaderText="ABRIL" Visible="true">
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Wrap="true" Width="9%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MAYO" HeaderText="MAYO" Visible="true">
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Wrap="true" Width="9%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="JUNIO" HeaderText="JUNIO" Visible="true">
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Wrap="true" Width="9%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="JULIO" HeaderText="JULIO" Visible="true">
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Wrap="true" Width="9%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="AGOSTO" HeaderText="AGOSTO" Visible="true">
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Wrap="true" Width="9%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="SEPTIEMBRE" HeaderText="SEPTIEMBRE" Visible="true">
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Wrap="true" Width="9%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="OCTUBRE" HeaderText="OCTUBRE" Visible="true">
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Wrap="true" Width="9%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NOVIEMBRE" HeaderText="NOVIEMBRE" Visible="true">
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Wrap="true" Width="9%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DICIEMBRE" HeaderText="DICIEMBRE" Visible="true">
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Wrap="true" Width="9%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TOTAL" HeaderText="TOTAL" Visible="true">
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Wrap="true" Width="9%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CORRECTO" HeaderText="CORRECTO" Visible="false">
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Wrap="true" Width="9%" />
                        </asp:BoundField>
                        
                        
                        </Columns>
                        <PagerStyle CssClass="GridHeader" />
                        <SelectedRowStyle CssClass="GridSelected" />
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="GridAltItem" />
                    </asp:GridView>
                </div>
            </td>
        </tr>
        </table>
    
    </div>
    
    </form>
</body>

</html>