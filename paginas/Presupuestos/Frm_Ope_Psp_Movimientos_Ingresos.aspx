﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" 
CodeFile="Frm_Ope_Psp_Movimientos_Ingresos.aspx.cs" Inherits="paginas_Presupuestos_Frm_Ope_Psp_Movimientos_Ingresos" 
Title="SIAC Sistema Integral Administrativo y Comercial" %>
<%@ Register Assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <link href="../../easyui/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../../easyui/themes/icon.css" rel="stylesheet" type="text/css" />
    <script src="../../easyui/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../../easyui/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="../../javascript/validacion/jquery.timers.js" type="text/javascript"></script>
    <script src="../../easyui/jquery.formatCurrency-1.4.0.min.js" type="text/javascript"></script>
    <script src="../../javascript/Js_Ope_Psp_Movimiento_Presupuesto_Ingresos.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        //<--
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_Session.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para matener la sesión activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);
        //-->
        
        (function ($) {
            $.formatCurrency.regions['es-MX'] = {
                symbol: '',
                positiveFormat: '%s%n',
                negativeFormat: '-%s%n',
                decimalSymbol: '.',
                digitGroupSymbol: ',',
                groupDigits: true
            };
        })(jQuery);

        function Validar_Caracteres() {
            $('textarea[id$=Txt_Justificacion]').keyup(function() {
                var Caracteres =  $(this).val().length;
                if (Caracteres > 250) {
                    this.value = this.value.substring(0, 250);
                }
            });
        }
        
        function Rechazado(Chk)
        {
            var Rechazado= $('[id$=Hf_No_Rechazado]').val();
            if($(Chk).attr('checked'))
            {
                var id=$(Chk).parent().attr('class');
                $.messager.prompt('Rechazado', 'Comentario', function(Aceptar){
				        if (Aceptar){
				            Rechazado += id+";"+ Aceptar+"-";
				             $('[id$=Hf_No_Rechazado]').val(Rechazado);
				        }
				        else{
				            $(Chk).attr("checked", false);
				        }
			    });
            }
        }
        
       function Autorizacion_Masiva(Chk)
        {
            var $chkBox = $("input:checkbox[id$=Chk_Autorizado]");
            var $chkBoxPre = $("input:checkbox[id$=Chk_PreAutorizado]");
            if($(Chk).attr('checked')){
                $chkBox.attr("checked", true);
                 $chkBoxPre.attr("checked", true);
            }
            else{
                $chkBox.attr("checked", false);
                $chkBoxPre.attr("checked", false);
            }
        }
        
        function Aceptacion_Masiva(Chk)
        {
            var $chkBox = $("input:checkbox[id$=Chk_Aceptar]");
            if($(Chk).attr('checked')){
                $chkBox.attr("checked", true);
            }
            else{
                $chkBox.attr("checked", false);
            }
        }
        
        function StartUpload(sender, args) {
            try {
                var filename = args.get_fileName();

                if (filename != "") {
                    // code to get File Extension..   
                    var arr1 = new Array;
                    arr1 = filename.split("\\");
                    var len = arr1.length;
                    var img1 = arr1[len - 1];
                    var filext = img1.substring(img1.lastIndexOf(".") + 1);


                    if (filext == "txt" || filext == "doc" || filext == "pdf" || filext == "docx" || filext == "jpg" || filext == "JPG" 
                        || filext == "jpeg" || filext == "JPEG" || filext == "png" || filext == "PNG" || filext == "gif" 
                        || filext == "GIF" || filext == "xlsx" || filext == "rar" || filext == "zip") {
                        if (args.get_length() > 2621440) {
                            var mensaje = "\n\nTabla de Documentos Anexos se limpiara completamente. Favor de volver a seleccionar los\narchivos a subir.";
                            alert("El Archivo " + filename + ". Excedio el Tamaño Permitido:\n\nTamaño del Archivo: [" + args.get_length() + " Bytes]\nTamaño Permitido: [2621440 Bytes o 2.5 Mb]" + mensaje);
                            return false;

                        }
                        return true;
                    } else {
                        var mensaje = "\n\nTabla de Documentos Anexos se limpiara completamente. Favor de volver a seleccionar los\narchivos a subir.";
                        alert("Tipo de archivo inválido " + filename + "\n\nFormatos Validos [.txt, .doc, .docx, .pdf, .jpg, .jpeg, .png, .gif, .xlsx, .xls, .rar, .zip]" + mensaje);
                        return false;
                    }
                }
            } catch (e) {

            }
        }

        function uploadError(sender, args) {
            try {
                var mensaje = "\n\nLa Tabla de Documentos Anexos se limpiara completamente. Favor de volver a seleccionar los\narchivos a subir.";
                alert("Error al Intentar cargar el archivo. [ " + args.get_fileName() + " ]\n\nTamaño Válido de los Archivos:\n + El Archivo debé ser mayor a 1Kb.\n + El Archivo debé ser Menor a 2.5 Mb" + mensaje);
                //refreshGridView();
            } catch (e) {

            }
        }
    </script>
    <style type="text/css">
         .button_autorizar2{
            margin:0 7px 0 0;
            background-color:#f5f5f5;
            border:1px solid #dedede;
            border-top:1px solid #eee;
            border-left:1px solid #eee;

            font-family:"Lucida Grande", Tahoma, Arial, Verdana, sans-serif;
            font-size:100%;    
            line-height:130%;
            text-decoration:none;
            font-weight:bold;
            color:#565656;
            cursor:pointer;
            padding:5px 10px 6px 7px; /* Links */
            width:99%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server"  EnableScriptGlobalization="true" EnableScriptLocalization = "true"/>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                    DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                        </div>
                        <div id="div_progress" class="processMessage" >
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
             
                
             <div id="Div_Fuentes_Financiamiento" style="background-color:#ffffff; width:100%; height:100%;">
                    
                <table width="100%" class="estilo_fuente">
                    <tr align="center">
                        <td colspan="4" class="label_titulo">
                           Movimiento de Pronóstico de Ingresos
                        </td>
                    </tr>
                    <tr>
                        <td runat="server" id="Td_Error" colspan="4">&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" />&nbsp;
                            <asp:Label ID="Lbl_Encabezado_Error" runat="server"   CssClass="estilo_fuente_mensaje_error"/>
                            <br />
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server"  CssClass="estilo_fuente_mensaje_error"/>
                        </td>
                    </tr>
               </table>             
            
               <table width="100%"  border="0" cellspacing="0">
                 <tr align="center">
                     <td colspan="2">                
                         <div  align="right" style="width:99%; background-color: #2F4E7D; color: #FFFFFF; font-weight: bold; font-style: normal; font-variant: normal; font-family: fantasy; height:32px"  >                        
                              <table style="width:100%;height:28px;">
                                <tr>
                                  <td align="left" style="width:59%;">  
                                        <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" CssClass="Img_Button" TabIndex="1"
                                            ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" onclick="Btn_Nuevo_Click" />
                                        <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                            ToolTip="Modificar" OnClick="Btn_Modificar_Click" CssClass="Img_Button"/>
                                        <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button" TabIndex="4"
                                            ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" onclick="Btn_Salir_Click"/>
                                  </td>
                                  <td align="right" style="width:41%;">
                                    <table style="width:100%;height:28px;">
                                        
                                    </table>
                                   </td>
                                 </tr>
                                 <tr>
                                <td colspan = "2"> 
                                    <asp:HiddenField id="Hf_Clase_ID" runat="server"/>
                                    <asp:HiddenField id="Hf_Concepto_ID" runat="server"/>
                                    <asp:HiddenField id="Hf_SubConcepto_ID" runat="server"/>
                                    <asp:HiddenField id="Hf_Tipo_ID" runat="server"/>
                                    <asp:HiddenField id="Hf_Rubro_ID" runat="server"/>
                                    <asp:HiddenField id="Hf_Modificado" runat="server"/>
                                    <asp:HiddenField id="Hf_Ampliacion" runat = "server" />
                                    <asp:HiddenField id="Hf_Reduccion" runat = "server" />
                                    <asp:HiddenField id="Hf_Tipo_Concepto" runat="server"/>
                                    <asp:HiddenField id="Hf_Movimiento_ID" runat="server" />
                                    <asp:HiddenField id="Hf_Clave_Nom_Concepto" runat="server" />
                                    <asp:HiddenField id="Hf_Estimado" runat="server" />
                                    <asp:HiddenField ID="Hf_No_Rechazado" runat="server" />
                                    <asp:HiddenField ID="Hf_Estatus" runat="server" />
                                    <asp:HiddenField id="Hf_Clave_Nom_Programa" runat="server" />
                                    <asp:HiddenField id="Hf_Programa_ID" runat="server" />
                                    <asp:HiddenField id="Hf_Importe_Programa" runat="server" />
                                    <asp:HiddenField id="Hf_Ruta_Doc" runat="server" />
                                    <asp:HiddenField id="Hf_Nombre_Doc" runat="server" />
                                </td>
                            </tr>
                              </table>
                        </div>
                     </td>
                 </tr>
             </table> 
            <center>
             <div id="Div_Grid_Movimientos" runat="server" style="overflow:auto;height:300px;width:98%;vertical-align:top;border-style:solid;border-color: Silver;">
                    <table width="100%">
                        <tr>
                            <td colspan="4">
                                <asp:GridView ID="Grid_Movimiento" runat="server"  CssClass="GridView_1" Width="100%" 
                                        AutoGenerateColumns="False"  GridLines="None" AllowPaging="false" 
                                        AllowSorting="True" HeaderStyle-CssClass="tblHead" 
                                        EmptyDataText="No se encuentra ningun Movimiento"
                                        OnSelectedIndexChanged="Grid_Movimiento_SelectedIndexChanged"
                                        OnSorting="Grid_Movimiento_Sorting">
                                        <Columns>
                                            <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                <ItemStyle Width="3%" />
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="NO_MOVIMIENTO_ING" HeaderText="Modificacion"  SortExpression="NO_MOVIMIENTO_ING">
                                                <HeaderStyle HorizontalAlign="Left" Width="7%" />
                                                <ItemStyle HorizontalAlign="Left" Width="7%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ANIO" HeaderText="Año"  SortExpression="ANIO" >
                                                <HeaderStyle HorizontalAlign="Center" Width="7%" />
                                                <ItemStyle HorizontalAlign="Center" Width="7%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FECHA_CREO" HeaderText="Fecha" SortExpression="FECHA_CREO" DataFormatString="{0:dd/MMM/yyyy}">
                                                <HeaderStyle HorizontalAlign="Left" Width="18%" />
                                                <ItemStyle HorizontalAlign="Left" Width="18%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ESTATUS" HeaderText="Estatus"  SortExpression="ESTATUS" >
                                                <HeaderStyle HorizontalAlign="Center" Width="9%" />
                                                <ItemStyle HorizontalAlign="Center" Width="9%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TOTAL_MODIFICADO" HeaderText="Total" SortExpression="TOTAL_MODIFICADO" DataFormatString="{0:n}">
                                                <HeaderStyle HorizontalAlign="Right" Width="14%" />
                                                <ItemStyle HorizontalAlign="Right" Width="14%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="USUARIO_CREO" />
                                            <asp:TemplateField>
                                                <ItemTemplate >
                                                    <asp:ImageButton ID="Btn_Pdf" runat="server" 
                                                        ImageUrl="~/paginas/imagenes/paginas/adobe_acrobat_professional.png" 
                                                        onclick="Btn_Pdf_Click" CommandArgument='<%#Eval("NO_MOVIMIENTO_ING")%>'
                                                        CssClass="Img_Button"/>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign ="Right" Font-Size="7pt" Width="5%" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <PagerStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </td>
                        </tr>
                    </table >
                </div>
             <div id="Div_Datos" runat="server">
                <br />
                <div id="Div_Datos_Generales" runat="server" style="width:97%;vertical-align:top;" >
                    <asp:Panel ID="Panel1" GroupingText="Datos Generales" runat="server">
                        <table id="Table_Datos_Genrerales" width="100%"   border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                                <td style="width:14%; text-align:left;"> 
                                    <asp:Label ID="Lbl_Operacion" runat="server" Text="* Operación" Width="98%"  ></asp:Label>
                                </td>
                                <td style="width:20%; text-align:left;">
                                    <asp:DropDownList ID="Cmb_Operacion" runat="server" Width="98%" AutoPostBack="true" 
                                    OnSelectedIndexChanged ="Cmb_Operacion_SelectedIndexChanged">
                                        <asp:ListItem Value="AMPLIACION">AMPLIACION</asp:ListItem>
                                        <asp:ListItem Value="REDUCCION">REDUCCION</asp:ListItem>
                                        <asp:ListItem Value="SUPLEMENTO">SUPLEMENTO</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="width:15%; text-align:left">
                                     <asp:Label ID="Lbl_Importe" runat="server" Text="&nbsp;* Importe" Width="98%"  ></asp:Label>
                                </td>
                                <td style="width:21%; text-align:left;">
                                    <asp:TextBox ID="Txt_Importe" runat="server" Width="90%" style="text-align:right;"
                                        onClick="$('input[id$=Txt_Importe]').select();" MaxLength="10"
                                        onBlur="$('input[id$=Txt_Importe]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(0);" >
                                    </asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="Txt_Importe_FilteredTextBoxExtender1" 
                                        runat="server" FilterType="Custom ,Numbers" 
                                        ValidChars=",."
                                        TargetControlID="Txt_Importe" Enabled="True" >
                                    </cc1:FilteredTextBoxExtender>
                                </td>
                                 <td  style="width:13%; text-align:left;">
                                     <asp:Label ID="Lbl_Estatus" runat="server" Text="Estatus " Width="98%"  ></asp:Label>
                                </td>
                                <td style="width:20%; text-align:left;">
                                    <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="98%" ></asp:DropDownList>
                                </td>
                            </tr>
                            <tr> 
                                <%--<td >
                                     <asp:Label ID="Lbl_Tipo_Concepto" runat="server" Text="* Tipo Concepto" Width="98%"  ></asp:Label>
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="Rbl_Tipos_Concepto" runat="server" AutoPostBack= "true" OnSelectedIndexChanged="Rbl_Tipos_Concepto_CheckedChanged">
                                        <asp:ListItem Text="Existente" Value="0" Selected></asp:ListItem>
                                        <asp:ListItem Text="Nueva" Value="1"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>--%>
                                <td>
                                     <asp:Label ID="Lbl_Justificacion" runat="server" Text="&nbsp;* Justificación " Width="85%"  ></asp:Label>
                                </td>
                                 <td style=" text-align:left;" colspan="5">
                                    <asp:TextBox ID="Txt_Justificacion" runat="server" TextMode="MultiLine" Width="97%" onkeyup="Validar_Caracteres()"></asp:TextBox>
                                    <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Justificacion" 
                                         runat="server" WatermarkCssClass="watermarked"
                                        TargetControlID ="Txt_Justificacion" 
                                         WatermarkText="Límite de Caractes 250" Enabled="True">
                                    </cc1:TextBoxWatermarkExtender>
                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Justificacion" runat="server" 
                                        TargetControlID="Txt_Justificacion" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters" 
                                        ValidChars="Ññ.:;()áéíóúÁÉÍÓÚ-/[]{} " Enabled="True">
                                    </cc1:FilteredTextBoxExtender> 
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
                <div style="height:0.2em;">&nbsp;</div>
                <div ID="Div_CRI" runat="server"  style="width:97%;vertical-align:top;" >
                     <asp:Panel ID="Panel2" runat="server" GroupingText="Conceptos">
                        <table width="100%"   border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                                <td style="width:20%; text-align:left;" > 
                                    <asp:Label ID="Lbl_FF" runat="server" Text="* Fuente Financiamiento" Width="100%"></asp:Label>
                                </td>
                                <td style="width:80%; text-align:left;">
                                    <asp:DropDownList ID="Cmb_FF" runat="server" Width="99%" AutoPostBack="true" 
                                    OnSelectedIndexChanged="Cmb_FF_SelectedIndexChanged"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="Tr_Programa" runat = "server">
                                <td style=" text-align:left;" > 
                                    <asp:Label ID="Lbl_Programa" runat="server" Text="* Programa" Width="100%"></asp:Label>
                                </td>
                                <td style=" text-align:left;">
                                    <asp:DropDownList ID="Cmb_Programa" runat="server" Width="80%" AutoPostBack="true" 
                                    OnSelectedIndexChanged="Cmb_Programa_SelectedIndexChanged"></asp:DropDownList>
                                    <asp:TextBox ID="Txt_Busqueda_Programa" runat="server" Style="width: 13%; text-align: left;
                                        font-size: x-small;" MaxLength="50" TabIndex="7" 
                                        onClick="$('input[id$=Txt_Busqueda_Programa]').select();"
                                        ontextchanged="Txt_Busueda_Programa_TextChanged" AutoPostBack ="true"/>
                                    <asp:ImageButton ID="Btn_Busqueda_Programa" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                        ToolTip="Buscar Programa"  OnClick="Btn_Busqueda_Programa_Click" TabIndex="19" style="width:20px; height:18px;"/>
                                </td>
                            </tr>
                             <tr>
                                <td> 
                                    <asp:Label ID="Lbl_Rubro" runat="server" Text="Rubro"   Width="100%"></asp:Label>
                                </td>
                                <td >
                                    <asp:DropDownList ID="Cmb_Rubro" runat="server" Width="99%" AutoPostBack="True" 
                                     OnSelectedIndexChanged="Cmb_Rubro_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td> 
                                    <asp:Label ID="Lbl_Concepto" runat="server" Text="Concepto"   Width="100%"></asp:Label>
                                </td>
                                <td >
                                    <asp:Label ID="Lbl_Nombre_Concepto" runat="server" Text="Concepto" style="color:Black;"  Width="100%"></asp:Label>
                                </td>
                            </tr>
                            <tr runat="server" id="Tr_Tipo">
                                <td> 
                                    <asp:Label ID="Lbl_Tipo" runat="server" Text="Tipo"   Width="100%"></asp:Label>
                                </td>
                                <td >
                                    <asp:DropDownList ID="Cmb_Tipo" runat="server" Width="99%" AutoPostBack="True" 
                                     OnSelectedIndexChanged="Cmb_Tipo_SelectedIndexChanged"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr runat="server" id="Tr_Clase">
                                <td> 
                                    <asp:Label ID="Lbl_Clase" runat="server" Text="Clase"   Width="100%"></asp:Label>
                                </td>
                                <td >
                                    <asp:DropDownList ID="Cmb_Clase" runat="server" Width="99%" AutoPostBack="True" 
                                     OnSelectedIndexChanged="Cmb_Clase_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr><td style="height:0.5em;" colspan="2"></td></tr>
                             <tr runat="server" id= "Tabla_Meses">
                                <td colspan = "2"> 
                                   <table width="99%" style=" " border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td style="width:10%; text-align:left; font-size:8pt; cursor:default;"  class="button_autorizar2">Mes</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Enero </td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Febrero</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Marzo</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Abril</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Mayo</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Junio</td>
                                        </tr>
                                        <tr runat="server" id="Tr_Estimado">
                                             <td style="text-align:left; font-size:8pt; cursor:default; width:10% " class="button_autorizar2">Estimado </td>
                                            <td style="text-align:left; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Ene" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Feb" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Mar" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Abr" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_May" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Jun" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:10%; text-align:left; cursor:default;"  class="button_autorizar2">&nbsp;</td>
                                            <td  style="text-align:right; width:15%; cursor:default; font-size:8pt;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Enero" runat="server" Width="100%"  style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Enero]').formatCurrency({colorize:true, region: 'es-MX'});  Validar(1);"
                                                onClick="$('input[id$=Txt_Enero]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Enero" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Febrero" runat="server" Width="100px" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Febrero]').formatCurrency({colorize:true, region: 'es-MX'});  Validar(2);"
                                                onClick="$('input[id$=Txt_Febrero]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Febrero" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Marzo" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Marzo]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(3);"
                                                onClick="$('input[id$=Txt_Marzo]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Marzo" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                               <asp:TextBox ID="Txt_Abril" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Abril]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(4);"
                                                onClick="$('input[id$=Txt_Abril]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Abril" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Mayo" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Mayo]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(5);"
                                                onClick="$('input[id$=Txt_Mayo]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Mayo" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Junio" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                               onBlur="$('input[id$=Txt_Junio]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(6);"
                                                onClick="$('input[id$=Txt_Junio]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Junio" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:10%; text-align:left; font-size:8pt; cursor:default;"  class="button_autorizar2">Mes</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Julio</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Agosto</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Septiembre </td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Octubre</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Noviembre</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Diciembre</td>
                                        </tr>
                                        <tr runat="server" id="Tr_Estimado1">
                                            <td style="width:10%; text-align:left; font-size:8pt; cursor:default;" class="button_autorizar2">Estimado</td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Jul" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Ago" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Sep" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Oct" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Nov" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Dic" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:10%;  text-align:left; font-size:8pt; cursor:default;" class="button_autorizar2">&nbsp;</td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:100px;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Julio" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Julio]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(7);"
                                                onClick="$('input[id$=Txt_Julio]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Julio" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                               <asp:TextBox ID="Txt_Agosto" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                               onBlur="$('input[id$=Txt_Agosto]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(8);"
                                                onClick="$('input[id$=Txt_Agosto]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Agosto" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Septiembre" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Septiembre]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(9);"
                                                onClick="$('input[id$=Txt_Septiembre]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Septiembre" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Octubre" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                               onBlur="$('input[id$=Txt_Octubre]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(10);"
                                                onClick="$('input[id$=Txt_Octubre]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Octubre" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Noviembre" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Noviembre]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(11);"
                                                onClick="$('input[id$=Txt_Noviembre]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Noviembre" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Diciembre" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Diciembre]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(12);"
                                                onClick="$('input[id$=Txt_Diciembre]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Diciembre" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style=" cursor:default;" class="button_autorizar2" colspan="5">
                                                <asp:Label ID="Lbl_Validacion" runat="server" Width="100%" style="text-align:left; font-size:8pt; color:Red;"></asp:Label>
                                            </td>
                                            <td style="height:1em; width:15%; cursor:default;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Total" runat="server" Width="100%" Text="Total" style="text-align:center; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="height:1em; width:15%; cursor:default;" class="button_autorizar2" >
                                                <asp:TextBox ID="Txt_Total" runat="server" Width="100%" style="text-align:right; font-size:8pt; border-color:Navy" ReadOnly="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr><td style="height:0.1em;">&nbsp;</td></tr>
                                   </table>
                                </td>
                            </tr>
                            <tr><td style="height:0.5em;" colspan="2"></td></tr>
                            <tr>
                                <td colspan="2" >
                                    <table cellpadding="0" cellspacing="0" class="tblHead" style="width:96%; ">
                                        <tr>
                                            <td style="width:15%; font-size:7pt!important; text-align:center;">CONCEPTO</td>
                                            <%--<td style="width:20%; font-size:7pt!important; text-align:right;">PROGRAMA</td>--%>
                                            <td style="width:9%; font-size:7pt!important; text-align:right;">ESTIMADO</td>
                                            <td style="width:9%; font-size:7pt!important; text-align:right;">MODIFICADO</td>
                                            <td  style="width:9%; font-size:7pt!important; text-align:right;"> RECAUDADO</td>
                                            <td  style="width:10%; font-size:7pt!important; text-align:center;"> POR RECAUDAR</td>
                                        </tr>
                                     </table>
                                    <div ID="Div_Conceptos" runat="server" style="width:99%; height:auto; max-height:125px; overflow:auto;">
                                        <asp:GridView ID="Grid_Conceptos" runat="server"  CssClass="GridView_1" Width="97%" 
                                            AutoGenerateColumns="False"  GridLines="Both" 
                                            HeaderStyle-CssClass="tblHead" ShowHeader="false"
                                            EmptyDataText="No se encontraron conceptos"
                                            OnSelectedIndexChanged = "Grid_Conceptos_SelectedIndexChanged"
                                            >
                                            <Columns>
                                                <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                    ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                    <ItemStyle Width="3%" />
                                                </asp:ButtonField>
                                                <asp:BoundField DataField="CLAVE_NOM_CONCEPTO" HeaderText="Concepto" >
                                                    <HeaderStyle HorizontalAlign="Left" Width="25%" Font-Size="7pt"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="25%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CLAVE_NOM_PROGRAMA" HeaderText="Programa" >
                                                    <HeaderStyle HorizontalAlign="Left" Width="25%" Font-Size="7pt"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="25%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ESTIMADO" HeaderText="Aprobado" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="12%" Font-Size="7pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="12%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="MODIFICADO" HeaderText="Modificado" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="12%" Font-Size="7pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="12%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="RECAUDADO" HeaderText="Recaudado" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="12%" Font-Size="7pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="12%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="POR_RECAUDAR" HeaderText="Por Recaudar" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="12%" Font-Size="7pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="12%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="RUBRO_ID" />
                                                <asp:BoundField DataField="TIPO_ID" />
                                                <asp:BoundField DataField="CLASE_ING_ID" />
                                                <asp:BoundField DataField="CONCEPTO_ING_ID" />
                                                <asp:BoundField DataField="SUBCONCEPTO_ING_ID" />
                                                <asp:BoundField DataField="FUENTE_FINANCIAMIENTO_ID" />
                                                <asp:BoundField DataField="AMPLIACION" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="REDUCCION" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMP_ENE" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMP_FEB" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMP_MAR" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMP_ABR" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMP_MAY" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMP_JUN" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMP_JUL" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMP_AGO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMP_SEP" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMP_OCT" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMP_NOV" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMP_DIC" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="PROYECTO_PROGRAMA_ID" />
                                            </Columns>
                                            <SelectedRowStyle CssClass="GridSelected" />
                                            <PagerStyle CssClass="GridHeader" />
                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                            <tr><td style="height:0.5em;" colspan="2"></td></tr>
                            <tr>
                                <td style="text-align:right;" colspan="2">
                                    <table style="width: 100%; text-align: center;">
                                        <tr>
                                            <td style="text-align: left; width: 10%;">
                                                 <asp:Label ID="Lbl_Anexo" runat="server" Text="Documento" class="estilo_fuente"></asp:Label>
                                            </td>
                                            <td  style="text-align: left; width: 80%; vertical-align:middle;">
                                                <div id="Div_Anexos" runat="server" style="width:97%;vertical-align:top;">
                                                    <asp:Label ID="Throbber" Text="wait" runat="server"  Width="80%">
                                                        <div id="Div5" class="progressBackgroundFilter"></div>
                                                        <div  class="processMessage" id="div6">
                                                             <img alt="" src="../Imagenes/paginas/Updating.gif" />
                                                        </div>
                                                    </asp:Label>
                                                    <cc1:AsyncFileUpload ID="AFU_Archivo" runat="server" CompleteBackColor="LightBlue" ErrorBackColor="Red" 
                                                         ThrobberID="Throbber"  UploadingBackColor="LightGray" Width="600px"
                                                         OnClientUploadComplete="StartUpload"  OnClientUploadError="uploadError"
                                                         OnUploadedComplete="Asy_Cargar_Archivo_Complete" FailedValidation="False" />
                                                </div>
                                            </td>
                                            <td style="text-align: right; width: 10%;">
                                                <asp:ImageButton id="Btn_Agregar" runat="server" ImageUrl="~/paginas/imagenes/gridview/add_grid.png"
                                                ToolTip="Agregar" OnClick="Btn_Agregar_Click"/> &nbsp;&nbsp;
                                                <asp:ImageButton id="Btn_Limpiar" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_clear.png"
                                                ToolTip="Limpiar" OnClick="Btn_Limpiar_Click"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                     </asp:Panel>
                </div> 
                <div style="height:0.2em;">&nbsp;</div>
                <div ID="Div_Movimiento_Conceptos" runat="server"  style="width:97%;vertical-align:top;" >
                     <asp:Panel ID="Pnl_Movimiento_Conceptos" runat="server" GroupingText="Movimientos">
                        <table width="100%"   border="0" cellspacing="0" class="estilo_fuente">
                            <tr id="Tr_Chk_Autorizar" runat="server">
                                <td style="text-align:left; width:50%;">
                                    <asp:Label id="Lbl_Tipo_Solicitud" runat="server" Text="Filtro: Tipo Operación &nbsp;"></asp:Label> 
                                   <asp:DropDownList ID="Cmb_Tipo_Solicitud" runat="server" Width="40%" AutoPostBack="true" 
                                    OnSelectedIndexChanged ="Cmb_Tipo_Solicitud_SelectedIndexChanged"></asp:DropDownList>
                                </td>
                                <td style="text-align:right; width:40%;">
                                    <asp:CheckBox ID="Chk_Autorizar" runat="server" OnClick="javascript:Autorizacion_Masiva(this);"/>
                                    <asp:Label id="Lbl_Autorizacion" runat="server" Text="Autorizar Todo"></asp:Label> 
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:CheckBox ID="Chk_Aceptacion" runat="server" OnClick="javascript:Aceptacion_Masiva(this);"/>
                                    <asp:Label id="Lbl_Aceptar" runat="server" Text="Aceptar Todo"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style=" clear:both; display:compact;" colspan = "2">
                                    <div ID="Div_Mov_Conceptos" runat="server" style="width:100%; height:auto; max-height:300px; overflow:auto; vertical-align:top; clear:both;">
                                       <asp:GridView ID="Grid_Mov_Conceptos" runat="server"  CssClass="GridView_1" Width="97%" 
                                        AutoGenerateColumns="False"  GridLines="Both" AllowPaging="false" 
                                        HeaderStyle-CssClass="tblHead" DataKeyNames ="MOVIMIENTO_ING_ID"
                                        EmptyDataText="No se encuentra ningun Movimiento"
                                        OnRowDataBound="Grid_Mov_Conceptos_RowDataBound">
                                        <Columns>
                                            <%--<asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                <ItemStyle Width="3%" />
                                            </asp:ButtonField>--%>
                                            <asp:TemplateField HeaderText="Link" HeaderStyle-Font-Size = "7pt">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="Hyp_Lnk_Ruta" ForeColor="Blue" runat="server" Font-Size="7pt" >Archivo</asp:HyperLink></ItemTemplate><HeaderStyle HorizontalAlign ="Left" width ="3%" />
                                                <ItemStyle HorizontalAlign="Left" Width="6%" Font-Size="7pt"/>
                                            </asp:TemplateField> 
                                            <asp:BoundField DataField="MOVIMIENTO_ING_ID" HeaderText="No." >
                                                <HeaderStyle HorizontalAlign="Left" Width="3%" Font-Size="7pt" />
                                                <ItemStyle HorizontalAlign="Left" Width="3%" Font-Size="7pt"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CLAVE_NOM_CONCEPTO" HeaderText="Concepto" >
                                                <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="7pt"/>
                                                <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="7pt"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CLAVE_NOM_PROGRAMA" HeaderText="Programa" >
                                                    <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="7pt"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                            <asp:BoundField DataField="TIPO_MOVIMIENTO" HeaderText="Operacion" >
                                                <HeaderStyle HorizontalAlign="Left" Width="6%" Font-Size="7pt"/>
                                                <ItemStyle HorizontalAlign="Left" Width="6%" Font-Size="7pt"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="IMP_TOTAL" HeaderText="Importe"  DataFormatString="{0:n}">
                                                <HeaderStyle HorizontalAlign="Right" Width="10%" Font-Size="7pt"/>
                                                <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="7pt"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ESTIMADO" HeaderText="Aprobado"  DataFormatString="{0:n}">
                                                <HeaderStyle HorizontalAlign="Right" Width="10%" Font-Size="7pt"/>
                                                <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="7pt"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="AMPLIACION" HeaderText="Ampliación" DataFormatString="{0:n}">
                                                <HeaderStyle HorizontalAlign="Right" Width="10%" Font-Size="7pt"/>
                                                <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="7pt"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="REDUCCION" HeaderText="Reducción"  DataFormatString="{0:n}">
                                                <HeaderStyle HorizontalAlign="Right" Width="10%" Font-Size="7pt"/>
                                                <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="7pt"/>
                                            </asp:BoundField>
                                             <asp:BoundField DataField="MODIFICADO" HeaderText="Modificado" DataFormatString="{0:n}">
                                                <HeaderStyle HorizontalAlign="Right" Width="10%" Font-Size="7pt"/>
                                                <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="7pt"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ESTATUS" />
                                            <asp:BoundField DataField="RUBRO_ID" />
                                            <asp:BoundField DataField="TIPO_ID" />
                                            <asp:BoundField DataField="CLASE_ING_ID" />
                                            <asp:BoundField DataField="CONCEPTO_ING_ID" />
                                            <asp:BoundField DataField="SUBCONCEPTO_ING_ID" />
                                            <asp:BoundField DataField="FUENTE_FINANCIAMIENTO_ID" />
                                            <asp:BoundField DataField="IMP_ENE" DataFormatString="{0:#,###,##0.00}"/>
                                            <asp:BoundField DataField="IMP_FEB" DataFormatString="{0:#,###,##0.00}"/>
                                            <asp:BoundField DataField="IMP_MAR" DataFormatString="{0:#,###,##0.00}"/>
                                            <asp:BoundField DataField="IMP_ABR" DataFormatString="{0:#,###,##0.00}"/>
                                            <asp:BoundField DataField="IMP_MAY" DataFormatString="{0:#,###,##0.00}"/>
                                            <asp:BoundField DataField="IMP_JUN" DataFormatString="{0:#,###,##0.00}"/>
                                            <asp:BoundField DataField="IMP_JUL" DataFormatString="{0:#,###,##0.00}"/>
                                            <asp:BoundField DataField="IMP_AGO" DataFormatString="{0:#,###,##0.00}"/>
                                            <asp:BoundField DataField="IMP_SEP" DataFormatString="{0:#,###,##0.00}"/>
                                            <asp:BoundField DataField="IMP_OCT" DataFormatString="{0:#,###,##0.00}"/>
                                            <asp:BoundField DataField="IMP_NOV" DataFormatString="{0:#,###,##0.00}"/>
                                            <asp:BoundField DataField="IMP_DIC" DataFormatString="{0:#,###,##0.00}"/>
                                            <asp:BoundField DataField="JUSTIFICACION" />
                                            <asp:BoundField DataField="COMENTARIO" />
                                            <asp:BoundField DataField="FECHA_MODIFICO" />
                                            <asp:BoundField DataField="USUARIO_MODIFICO" />
                                            <asp:BoundField DataField="TIPO_CONCEPTO" />
                                            <asp:BoundField DataField="PROYECTO_PROGRAMA_ID" />
                                            <asp:TemplateField>
                                                <ItemTemplate >
                                                    <asp:ImageButton ID="Btn_Eliminar" runat="server" 
                                                        ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png" 
                                                        onclick="Btn_Eliminar_Click" CommandArgument='<%#Eval("MOVIMIENTO_ING_ID")%>'
                                                        />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign ="Center" Font-Size="7pt" Width="1%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText ="PreAutorizar"> 
                                            <ItemTemplate>
                                                <asp:CheckBox ID="Chk_PreAutorizado" runat="server" CssClass ='<%# Eval("MOVIMIENTO_ING_ID") %>' />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="8%" Font-Size="8pt"/>
                                            <ItemStyle HorizontalAlign ="Center" Font-Size="7pt" Width="8%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText ="Aceptar"> 
                                            <ItemTemplate>
                                                <asp:CheckBox ID="Chk_Aceptar" runat="server" CssClass ='<%# Eval("MOVIMIENTO_ING_ID") %>' />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="8%" Font-Size="8pt"/>
                                            <ItemStyle HorizontalAlign ="Center" Font-Size="7pt" Width="8%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText ="Cancelar">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="Chk_Cancelado" runat="server" OnClick="javascript:Rechazado(this);" CssClass='<%# Eval("MOVIMIENTO_ING_ID") %>' />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="8%" Font-Size="8pt"/>
                                            <ItemStyle HorizontalAlign ="Center" Font-Size="7pt" Width="8%"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText ="Autorizar">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="Chk_Autorizado" runat="server" CssClass='<%# Eval("MOVIMIENTO_ING_ID") %>' />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="8%" Font-Size="8pt"/>
                                            <ItemStyle HorizontalAlign ="Center" Font-Size="7pt" Width="8%"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText ="Rechazar">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="Chk_Rechazado" runat="server" OnClick="javascript:Rechazado(this);" CssClass='<%# Eval("MOVIMIENTO_ING_ID") %>' />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="8%" Font-Size="8pt"/>
                                            <ItemStyle HorizontalAlign ="Center" Font-Size="7pt" Width="8%"  />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="NOMBRE_DOC" />
                                        <asp:BoundField DataField="RUTA_DOC" />
                                        <asp:BoundField DataField="EXTENSION_DOC" />
                                        </Columns>
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <PagerStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:right;" colspan="2">
                                    <asp:Label ID="Lbl_Total_Modificado" runat="server" Text="Total Modificado"></asp:Label>
                                    <asp:TextBox ID="Txt_Total_Modificado" runat="server" Width="20%" style="text-align:right; font-size:8pt; border-color:Navy" ReadOnly="true"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                     </asp:Panel>
                </div>   
                <br />
                <div id="Div_Grid_Comentarios" runat="server" style="width:97%;vertical-align:top;">
                   <asp:Panel ID="Panel4" runat="server" GroupingText="Comentarios">
                    <div id="Div1" runat="server" style="overflow:auto;height:100px;width:97%;vertical-align:top;">
                         <table width="99%" border="0" cellspacing="0" class="estilo_fuente">
                           <tr>
                                <td style="width:12%; text-align:left;" >
                                    <asp:Label ID="Lbl_Comentario" runat="server" Text="Comentario:" Width="100%" style="text-align:left;"></asp:Label>
                                </td>
                                <td colspan = "3" style="width:88%; text-align:left;">
                                    <asp:TextBox ID="Txt_Comentario" runat="server" TextMode="MultiLine" Width="98%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:12%; text-align:left;">
                                    <asp:Label ID="Lbl_Autorizo" runat="server" Text="Autorizo:" Width="100%" style="text-align:left;"></asp:Label>
                                </td>
                                <td style="width:50%; text-align:left;">
                                    <asp:TextBox ID="Txt_Autorizo" runat="server" Width="98%"></asp:TextBox>
                                </td>
                                <td style="width:12%; text-align:left;">
                                    <asp:Label ID="Lbl_Fecha_Autorizo" runat="server" Text="Fecha:" Width="100%" style="text-align:left;"></asp:Label>
                                </td>
                                <td style="width:26%; text-align:left;">
                                    <asp:TextBox ID="Txt_Fecha_Autorizo" runat="server" Width="93%"></asp:TextBox>
                                </td>
                            </tr>
                          </table>
                       </div>
                   </asp:Panel>
                </div>
             </div>
            </center>
         </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

