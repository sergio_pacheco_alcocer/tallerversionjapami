﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" 
CodeFile="Frm_Ope_Movimientos_Presupuesto.aspx.cs" Inherits="paginas_Presupuestos_Frm_Ope_Movimientos_Presupuesto" 
Title="SIAC Sistema Integral Administrativo y Comercial" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta http-equiv="X-UA-Compatible" content="IE=8.0" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <style type="text/css">
         .button_autorizar2{
            margin:0 7px 0 0;
            background-color:#f5f5f5;
            border:1px solid #dedede;
            border-top:1px solid #eee;
            border-left:1px solid #eee;

            font-family:"Lucida Grande", Tahoma, Arial, Verdana, sans-serif;
            font-size:100%;    
            line-height:130%;
            text-decoration:none;
            font-weight:bold;
            color:#565656;
            cursor:pointer;
            padding:5px 10px 6px 7px; /* Links */
            width:99%;
        }
    </style>
    <script src="../../javascript/Js_Ope_Psp_Movimiento_Presupuestal.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        //<--
            //El nombre del controlador que mantiene la sesión
            var CONTROLADOR = "../../Mantenedor_Session.ashx";

            //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
            function MantenSesion() {
                var head = document.getElementsByTagName('head').item(0);
                script = document.createElement('script');
                script.src = CONTROLADOR;
                script.setAttribute('type', 'text/javascript');
                script.defer = true;
                head.appendChild(script);
            }

            //Temporizador para matener la sesión activa
            setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);
        //-->
        (function ($) {
            $.formatCurrency.regions['es-MX'] = {
                symbol: '',
                positiveFormat: '%s%n',
                negativeFormat: '-%s%n',
                decimalSymbol: '.',
                digitGroupSymbol: ',',
                groupDigits: true
            };
        })(jQuery);


        function Validar_Cantidad_Caracteres(Obj) 
        {
            var limit = 250;
            var len = Obj.value.length; //cuenta los caracteres conforme se van introduciendo
            
            if (len > limit) 
            {
                Obj.value = Obj.value.substring(0, limit); //elimina los caracteres de mas
            }
        }
        
        function StartUpload(sender, args) {
            try {
                var filename = args.get_fileName();

                if (filename != "") {
                    // code to get File Extension..   
                    var arr1 = new Array;
                    arr1 = filename.split("\\");
                    var len = arr1.length;
                    var img1 = arr1[len - 1];
                    var filext = img1.substring(img1.lastIndexOf(".") + 1);


                    if (filext == "txt" || filext == "doc" || filext == "pdf" || filext == "docx" || filext == "jpg" || filext == "JPG" 
                        || filext == "jpeg" || filext == "JPEG" || filext == "png" || filext == "PNG" || filext == "gif" 
                        || filext == "GIF" || filext == "xlsx" || filext == "rar" || filext == "zip" || filext == "xls") {
                        if (args.get_length() > 2621440) {
                            var mensaje = "\n\nTabla de Documentos Anexos se limpiara completamente. Favor de volver a seleccionar los\narchivos a subir.";
                            alert("El Archivo " + filename + ". Excedio el Tamaño Permitido:\n\nTamaño del Archivo: [" + args.get_length() + " Bytes]\nTamaño Permitido: [2621440 Bytes o 2.5 Mb]" + mensaje);
                            return false;

                        }
                        return true;
                    } else {
                        var mensaje = "\n\nTabla de Documentos Anexos se limpiara completamente. Favor de volver a seleccionar los\narchivos a subir.";
                        alert("Tipo de archivo inválido " + filename + "\n\nFormatos Validos [.txt, .doc, .docx, .pdf, .jpg, .jpeg, .png, .gif, .xlsx, .xls, .rar, .zip]" + mensaje);
                        return false;
                    }
                }
            } catch (e) {

            }
        }

        function uploadError(sender, args) {
            try {
            var filename = args.get_fileName();
             if (filename != "") {
                var mensaje = "\n\nLa Tabla de Documentos Anexos se limpiara completamente. Favor de volver a seleccionar los\narchivos a subir.";
                alert("Error al Intentar cargar el archivo. [ " + args.get_fileName() + " ]\n\nTamaño Válido de los Archivos:\n + El Archivo debé ser mayor a 1Kb.\n + El Archivo debé ser Menor a 2.5 Mb" + mensaje);
               }
            } catch (e) {

            }
        }
        
        function Abrir_Modal_Popup() {
            $find('Datos_Solicitud').show();
            return false;
        }
        function Cerrar_Modal_Popup() {
            $find('Datos_Solicitud').hide();
            return false;
        } 
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server"  EnableScriptGlobalization="true" EnableScriptLocalization = "true"/>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                    DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                        </div>
                        <div id="div_progress" class="processMessage" >
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
             
                
             <div id="Div_Fuentes_Financiamiento" style="background-color:#ffffff; width:100%; height:100%;">
                    
                <table width="100%" class="estilo_fuente">
                    <tr align="center">
                        <td colspan="4" class="label_titulo">
                           Movimiento de Presupuesto
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
               </table>             
            
               <table width="100%"  border="0" cellspacing="0">
                 <tr align="center">
                     <td colspan="2">                
                         <div  align="right" style="width:99%; background-color: #2F4E7D; color: #FFFFFF; font-weight: bold; font-style: normal; font-variant: normal; font-family: fantasy; height:32px"  >                        
                              <table style="width:100%;height:28px;">
                                <tr>
                                  <td align="left" style="width:59%;">  
                                        <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" CssClass="Img_Button" TabIndex="1"
                                            ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" onclick="Btn_Nuevo_Click" />
                                        <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                            CssClass="Img_Button" AlternateText="Modificar" ToolTip="Modificar" OnClick="Btn_Modificar_Click" />
                                        <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button" TabIndex="4"
                                            ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" onclick="Btn_Salir_Click"/>
                                  </td>
                                  <td align="right" style="width:41%;">
                                    <table style="width:100%;height:28px;">
                                        
                                    </table>
                                   </td>
                                 </tr>
                              </table>
                        </div>
                     </td>
                 </tr>
             </table> 
            <center>
             <div id="Div_Grid_Movimientos" runat="server">
                    <table width="100%">
                       <tr>
                            <td style="width: 15%; text-align:left;"> 
                                <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text="Unidad Responsable" ></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="Cmb_Unidad_Responsable" runat="server" Width="75%"  >
                                </asp:DropDownList>
                                <asp:TextBox ID="Txt_Busqueda_UR" runat="server" Width="15%" MaxLength="50"></asp:TextBox>
                                <asp:ImageButton ID="Btn_Busqueda_UR" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                        ToolTip="Buscar Unidad Responsable"  OnClick="Btn_Busqueda_UR_Click" TabIndex="19" style="width:20px; height:18px;"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:left; width:15%">
                                <asp:Label ID="Lbl_Fecha" runat="server" Text="Fecha"   Width="100%"></asp:Label>
                            </td>
                             <td style="width:35%;">
                                <asp:TextBox ID="Txt_Fecha_Inicial" runat="server" Width="100px" Enabled="false"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="Txt_Fecha_Inicial_FilteredTextBoxExtender" runat="server" TargetControlID="Txt_Fecha_Inicial" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters"
                                    ValidChars="/_" />
                                <cc1:CalendarExtender ID="Txt_Fecha_Inicial_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Inicial" PopupButtonID="Btn_Fecha_Inicial" Format="dd/MMM/yyyy" />
                                <asp:ImageButton ID="Btn_Fecha_Inicial" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Inicial" />
                                :&nbsp;&nbsp;
                                <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="100px" Enabled="false"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="Txt_Fecha_Final" PopupButtonID="Btn_Fecha_Final" Format="dd/MMM/yyyy" />
                                <asp:ImageButton ID="Btn_Fecha_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Final" />
                            </td>
                            <td style="vertical-align:middle;text-align:right;width:20%;">B&uacute;squeda:</td>
                            <td style="width:35%;">
                                <asp:TextBox ID="Txt_Busqueda" runat="server" MaxLength="100" TabIndex="5"  ToolTip = "Buscar por Nombre" Width="180px"/>
                                <asp:ImageButton ID="Btn_Buscar" runat="server" TabIndex="6"
                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" ToolTip="Consultar"
                                onclick="Btn_Buscar_Click" />
                                <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda" runat="server" WatermarkCssClass="watermarked"
                                WatermarkText="<Ingrese Folio>" TargetControlID="Txt_Busqueda" />
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda" 
                                    runat="server" TargetControlID="Txt_Busqueda" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ,/. "/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div style="overflow:auto;height:300px;width:98%;vertical-align:top;border-style:solid;border-color: Silver;">
                                <asp:GridView ID="Grid_Movimiento" runat="server"  CssClass="GridView_1" Width="100%" 
                                        AutoGenerateColumns="False"  GridLines="None" AllowPaging="false" 
                                        AllowSorting="True" HeaderStyle-CssClass="tblHead" 
                                        EmptyDataText="No se encuentra ningun Movimiento"
                                        OnSelectedIndexChanged="Grid_Movimiento_SelectedIndexChanged"
                                        OnSorting="Grid_Movimiento_Sorting">
                                        <Columns>
                                            <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                <ItemStyle Width="3%" />
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="SOLICITUD_ID" HeaderText="Solicitud" SortExpression="SOLICITUD_ID">
                                                <HeaderStyle HorizontalAlign="Left" Width="8%" />
                                                <ItemStyle HorizontalAlign="Left" Width="8%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TIPO_OPERACION" HeaderText="Operación" SortExpression="TIPO_OPERACION" >
                                                <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                <ItemStyle HorizontalAlign="Left" Width="10%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="IMPORTE" HeaderText="Importe"  SortExpression="IMPORTE" DataFormatString="{0:n}">
                                                <HeaderStyle HorizontalAlign="Right" Width="15%" />
                                                <ItemStyle HorizontalAlign="Right" Width="15%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS" >
                                                <HeaderStyle HorizontalAlign="Center" Width="15%" />
                                                <ItemStyle HorizontalAlign="Center" Width="15%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FECHA_CREO" HeaderText="Fecha" SortExpression="FECHA_CREO" DataFormatString="{0:dd/MMM/yyyy HH:mm:ss}">
                                                <HeaderStyle HorizontalAlign="Center" Width="12%" />
                                                <ItemStyle HorizontalAlign="Center" Width="12%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="USUARIO_CREO" HeaderText="Usuario" SortExpression="USUARIO_CREO">
                                                <HeaderStyle HorizontalAlign="Center" Width="40%" />
                                                <ItemStyle HorizontalAlign="Center" Width="40%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="JUSTIFICACION" />
                                            <asp:BoundField DataField="COMENTARIO" />
                                            <asp:BoundField DataField="FECHA_MODIFICO" DataFormatString="{0:dd/MMM/yyyy}"/>
                                            <asp:BoundField DataField="USUARIO_MODIFICO" />
                                            <asp:BoundField DataField="NO_MOVIMIENTO_EGR" />
                                            <asp:TemplateField>
                                                <ItemTemplate >
                                                    <asp:ImageButton ID="Btn_Pdf" runat="server" 
                                                        ImageUrl="~/paginas/imagenes/paginas/adobe_acrobat_professional.png" 
                                                        onclick="Btn_Pdf_Click" CommandArgument='<%#Eval("NO_MOVIMIENTO_EGR")%>'
                                                        ToolTip='<%#Eval("SOLICITUD_ID")%>' CssClass="Img_Button" CommandName='<%#Eval("TIPO_OPERACION")%>'/>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign ="Center" Font-Size="7pt" Width="1%" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <PagerStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                    </div>
                                </td>
                        </tr>
                    </table >
                </div>
             <div id="Div_Datos" runat="server">
                <br />
                <div id="Div_Datos_Generales" runat="server" style="width:97%;vertical-align:top;" >
                    <asp:Panel ID="Panel1" GroupingText="Datos Generales" runat="server">
                        <table id="Table_Datos_Genrerales" width="100%"   border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                                <td style="width:15%; text-align:left;"> 
                                    <asp:Label ID="Lbl_Operacion" runat="server" Text="Operación" Width="98%"  ></asp:Label>
                                </td>
                                <td style="width:18%; text-align:left;">
                                    <asp:DropDownList ID="Cmb_Operacion" runat="server" Width="98%" AutoPostBack="true" OnSelectedIndexChanged="Cmb_Operacion_SelectedIndexChanged" >
                                    </asp:DropDownList>
                                </td>
                                <td style="width:14%; text-align:left;">
                                     <asp:Label ID="Lbl_Importe" runat="server" Text="* Importe" Width="98%"  ></asp:Label>
                                </td>
                                <td style="width:18%; text-align:left;">
                                    <asp:TextBox ID="Txt_Importe" runat="server" Width="90%"
                                        onClick="$('input[id$=Txt_Importe]').select();" MaxLength="10"
                                        onBlur="$('input[id$=Txt_Importe]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(0);" >
                                    </asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="Txt_Importe_FilteredTextBoxExtender1" 
                                        runat="server" FilterType="Custom ,Numbers" 
                                        ValidChars=",."
                                        TargetControlID="Txt_Importe" Enabled="True" >
                                    </cc1:FilteredTextBoxExtender>
                                </td>
                                 <td style="width:14%; text-align:left;">
                                     <asp:Label ID="Lbl_Estatus" runat="server" Text="Estatus " Width="98%"  ></asp:Label>
                                </td>
                                <td style="width:18%; text-align:left;">
                                    <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="98%" >
                                        <asp:ListItem Value = "GENERADO">GENERADO</asp:ListItem>
                                        <asp:ListItem Value = "RECIBIDO">RECIBIDO</asp:ListItem>
                                        <asp:ListItem Value = "ACEPTADO">ACEPTADO</asp:ListItem>
                                        <asp:ListItem Value = "CACELADO">CANCELADO</asp:ListItem>
                                        <asp:ListItem Value = "PREAUTORIZADO">PREAUTORIZADO</asp:ListItem>
                                        <asp:ListItem Value = "AUTORIZADO">AUTORIZADO</asp:ListItem>
                                        <asp:ListItem Value = "RECHAZADO">RECHAZADO</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr runat="server" id="Tr_Tipo_Partida"> 

                                <td>
                                     <asp:Label ID="Lbl_Justificacion" runat="server" Text="* Justificación" Width="98%"></asp:Label>
                                </td>
                                <td colspan="5">
                                     <asp:TextBox ID="Txt_Justificacion" runat="server" TextMode="MultiLine" Width="98%" onKeyUp="javascript:Validar_Cantidad_Caracteres(this);"></asp:TextBox>
                                     <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Justificacion"  runat="server" WatermarkCssClass="watermarked"
                                       TargetControlID ="Txt_Justificacion"  WatermarkText="Límite de Caractes 250" Enabled="True">
                                     </cc1:TextBoxWatermarkExtender>
                                     <cc1:FilteredTextBoxExtender ID="FTE_Txt_Justificacion" runat="server" 
                                        TargetControlID="Txt_Justificacion" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters" 
                                        ValidChars="Ññ.:;()áéíóúÁÉÍÓÚ-/ ">
                                     </cc1:FilteredTextBoxExtender> 
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
                <br />
                <div id="Div_Anexos" runat="server" style="width:97%;vertical-align:top;">
                    <asp:Panel ID="Pnl_Anexos" runat="server" GroupingText="Anexos" Width="100%">
                        <table style="width: 100%; text-align: center;">
                            <tr>
                                <td style="text-align: left; width: 15%;">
                                     <asp:Label ID="Lbl_Anexo" runat="server" Text="Documento"></asp:Label>
                                </td>
                                <td  style="text-align: left; width: 81%; vertical-align:middle;">
                                    <asp:Label ID="Throbber" Text="wait" runat="server"  Width="80%">
                                        <div id="Div5" class="progressBackgroundFilter"></div>
                                        <div  class="processMessage" id="div6">
                                             <img alt="" src="../Imagenes/paginas/Updating.gif" />
                                        </div>
                                    </asp:Label>
                                    <cc1:AsyncFileUpload ID="AFU_Archivo" runat="server" CompleteBackColor="LightBlue" ErrorBackColor="Red" 
                                         ThrobberID="Throbber"  UploadingBackColor="LightGray" Width="600px"
                                         OnClientUploadComplete="StartUpload"  OnClientUploadError="uploadError"
                                         OnUploadedComplete="Asy_Cargar_Archivo_Complete" FailedValidation="False" />
                                </td>
                                <td style="text-align: left; width: 4%;">
                                     <asp:ImageButton id="Btn_Agregar_Doc" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_add.png"
                                    ToolTip="Agregar Documento" OnClick="Btn_Agregar_Doc_Click" style="height:16px; width:16px;"/>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center;" colspan="3">
                                    <asp:GridView ID="Grid_Anexos" runat="server"  CssClass="GridView_Nested" Width="99%" 
                                        AutoGenerateColumns="False"  GridLines="None" AllowPaging="false" 
                                        AllowSorting="True" HeaderStyle-CssClass="GridHeader_Nested"
                                        EmptyDataText="No existen Anexos"
                                        OnRowDataBound = "Grid_Anexos_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Link" HeaderStyle-Font-Size = "8pt">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="Hyp_Lnk_Ruta" ForeColor="Blue" runat="server" Font-Size="8pt" >Archivo</asp:HyperLink></ItemTemplate><HeaderStyle HorizontalAlign ="Left" width ="3%" />
                                                <ItemStyle HorizontalAlign="Left" Width="7%" Font-Size="8pt"/>
                                            </asp:TemplateField> 
                                            <asp:BoundField DataField="NOMBRE" HeaderText="Nombre"  SortExpression="NO_MOVIMIENTO_ING">
                                                <HeaderStyle HorizontalAlign="Left" Width="90%" Font-Size="8pt" />
                                                <ItemStyle HorizontalAlign="Left" Width="90%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ANEXO_ID" />
                                            <asp:BoundField DataField="RUTA_DOCUMENTO" />
                                            <asp:BoundField DataField="EXTENSION" />
                                            <asp:TemplateField>
                                                <ItemTemplate >
                                                    <asp:ImageButton ID="Btn_Eliminar_Doc" runat="server" 
                                                        ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png" 
                                                        onclick="Btn_Eliminar_Doc_Click" CommandArgument='<%#Eval("ANEXO_ID")%>'
                                                        />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign ="Center" Font-Size="8pt" Width="3%" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <SelectedRowStyle CssClass="GridSelected_Nested" />
                                        <PagerStyle CssClass="GridHeader_Nested" />
                                        <AlternatingRowStyle CssClass="GridAltItem_Nested" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
                <br />
                <div ID="Div_Partida_Origen" runat="server"  style="width:97%;vertical-align:top;" >
                     <asp:Panel ID="Panel2" runat="server" GroupingText="Partida Origen">
                        <table width="100%"   border="0" cellspacing="0" class="estilo_fuente">
                             <tr runat="server" id="Tr_Programa_Origen">
                                <td> 
                                    <asp:Label ID="Lbl_Programa" runat="server" Text="Programa"   Width="100%"></asp:Label>
                                </td>
                                <td >
                                    <asp:DropDownList ID="Cmb_Programa_Origen" runat="server" Width="99%" 
                                    AutoPostBack = "true" OnSelectedIndexChanged = "Cmb_Programa_Origen_SelectedIndexChanged"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td > 
                                    <asp:Label ID="lbl_Unidad_Responsable_Origen" runat="server" Text="Unidad Responsable" Width="100%"  
                                    ></asp:Label>
                                </td>
                                <td >
                                    <asp:DropDownList ID="Cmb_Unidad_Responsable_Origen" runat="server" Width="75%" AutoPostBack="true" 
                                    OnSelectedIndexChanged="Cmb_Unidad_Responsable_Origen_SelectedIndexChanged"></asp:DropDownList>
                                    <asp:TextBox ID="Txt_Busqueda_URO" runat="server" Width="15%" MaxLength="50"></asp:TextBox>
                                <asp:ImageButton ID="Btn_Busqueda_URO" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                        ToolTip="Buscar Unidad Responsable"  OnClick="Btn_Busqueda_URO_Click" TabIndex="19" style="width:20px; height:18px;"/>
                                </td>
                            </tr>
                             <tr>
                                <td> 
                                    <asp:Label ID="Lbl_Fuente_Financiamiento_Origen" runat="server" Text="* Fuente Financiamiento"   Width="100%"></asp:Label>
                                </td>
                                <td >
                                    <asp:DropDownList ID="Cmb_Fuente_Financiamiento_Origen" runat="server" Width="99%" AutoPostBack="True" 
                                     OnSelectedIndexChanged="Cmb_Fuente_Financiamiento_Origen_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr runat="server" id="Tr_Capitulo_Origen">
                                <td> 
                                    <asp:Label ID="Lbl_Capitulo" runat="server" Text="Capitulo"   Width="100%"></asp:Label>
                                </td>
                                <td >
                                    <asp:DropDownList ID="Cmb_Capitulo_Origen" runat="server" Width="99%" AutoPostBack="True" 
                                     OnSelectedIndexChanged="Cmb_Capitulo_Origen_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td> 
                                    <asp:Label ID="Lbl_Partida_Origen" runat="server" Text="* Partida"   Width="100%"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat= "server" ID="Lbl_Nombre_Partida_Origen" Width="98%"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan = "2"> 
                                    <asp:HiddenField id="Hf_Area_Funcional_Origen_ID" runat="server"/>
                                    <asp:HiddenField id="Hf_Tipo_Usuario" runat="server"/>
                                    <asp:HiddenField id="Hf_Programa_Origen_ID" runat="server"/>
                                    <asp:HiddenField id="Hf_Partida_Origen_ID" runat="server"/>
                                    <asp:HiddenField id="Hf_Programa_Origen" runat="server"/>
                                    <asp:HiddenField id="Hf_Partida_Origen" runat="server"/>
                                    <asp:HiddenField id="Hf_Capitulo_Origen_ID" runat="server"/>
                                    <asp:HiddenField id="Hf_Disponible" runat="server"/>
                                    <asp:HiddenField id="Hf_Ampliacion_Origen" runat="server"/>
                                    <asp:HiddenField id="Hf_Reduccion_Origen" runat="server"/>
                                    <asp:HiddenField id="Hf_Modificado_Origen" runat="server"/>
                                    <asp:HiddenField id="Hf_Aprobado_Origen" runat="server"/>
                                    <asp:HiddenField id="Hf_Mes_Actual" runat="server"/>
                                    <asp:HiddenField id="Hf_Tipo_Presupuesto" runat="server"/>
                                    <asp:HiddenField id="Hf_Importe_Programa" runat="server"/>
                                    <asp:HiddenField id="Hf_No_Solicitud" runat="server"/>
                                    <asp:HiddenField id="Hf_No_Modificacion" runat="server"/>
                                    <asp:HiddenField id="Hf_Ruta_Doc" runat="server" />
                                    <asp:HiddenField id="Hf_Nombre_Doc" runat="server" />
                                    
                                    <%--BOTON ACEPTAR--%>
                                <asp:HiddenField id="Hf_Estatus_Btn_Aceptar" runat="server"/>
                                
                                <cc1:ModalPopupExtender ID="Mpe_Datos_Solicitud" runat="server" BackgroundCssClass="popUpStyle"  BehaviorID="Datos_Solicitud"
                                    PopupControlID="Pnl_Datos_Solicitud_Contenedor" TargetControlID="Btn_Comodin_Open" PopupDragHandleControlID="Pnl_Datos_Solicitud_Cabecera" 
                                    CancelControlID="Btn_Comodin_Close" DropShadow="True" DynamicServicePath="" Enabled="True"/> 
                                <asp:Button Style="background-color: transparent; border-style:none;" ID="Btn_Comodin_Close" runat="server" Text="" />
                                <asp:Button  Style="background-color: transparent; border-style:none;" ID="Btn_Comodin_Open" runat="server" Text="" />
                                </td>
                            </tr>
                            <tr><td style="height:0.2em;" colspan="2"></td></tr>
                             <tr runat="server" id= "Tabla_Meses">
                                <td colspan = "2"> 
                                   <table width="99%" style=" " border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td style="width:10%; text-align:left; font-size:8pt; cursor:default;"  class="button_autorizar2">Mes</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Enero </td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Febrero</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Marzo</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Abril</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Mayo</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Junio</td>
                                        </tr>
                                        <tr>
                                             <td style="text-align:left; font-size:8pt; cursor:default; width:10% " class="button_autorizar2">Disponible </td>
                                            <td style="text-align:left; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Ene" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Feb" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Mar" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Abr" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_May" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Jun" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:10%; text-align:left; cursor:default;"  class="button_autorizar2">&nbsp;</td>
                                            <td  style="text-align:right; width:15%; cursor:default; font-size:8pt;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Enero" runat="server" Width="100%"  style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Enero]').formatCurrency({colorize:true, region: 'es-MX'});  Validar(1);"
                                                onClick="$('input[id$=Txt_Enero]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Enero" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Febrero" runat="server" Width="100px" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Febrero]').formatCurrency({colorize:true, region: 'es-MX'});  Validar(2);"
                                                onClick="$('input[id$=Txt_Febrero]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Febrero" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Marzo" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Marzo]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(3);"
                                                onClick="$('input[id$=Txt_Marzo]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Marzo" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                               <asp:TextBox ID="Txt_Abril" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Abril]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(4);"
                                                onClick="$('input[id$=Txt_Abril]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Abril" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Mayo" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Mayo]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(5);"
                                                onClick="$('input[id$=Txt_Mayo]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Mayo" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Junio" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                               onBlur="$('input[id$=Txt_Junio]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(6);"
                                                onClick="$('input[id$=Txt_Junio]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Junio" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:10%; text-align:left; font-size:8pt; cursor:default;"  class="button_autorizar2">Mes</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Julio</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Agosto</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Septiembre </td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Octubre</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Noviembre</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Diciembre</td>
                                        </tr>
                                        <tr id="Tr_Disponible_Origen" runat="server">
                                            <td style="width:10%; text-align:left; font-size:8pt; cursor:default;" class="button_autorizar2">Disponible</td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Jul" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Ago" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Sep" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Oct" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Nov" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Dic" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:10%;  text-align:left; font-size:8pt; cursor:default;" class="button_autorizar2">&nbsp;</td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:100px;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Julio" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Julio]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(7);"
                                                onClick="$('input[id$=Txt_Julio]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Julio" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                               <asp:TextBox ID="Txt_Agosto" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                               onBlur="$('input[id$=Txt_Agosto]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(8);"
                                                onClick="$('input[id$=Txt_Agosto]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Agosto" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Septiembre" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Septiembre]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(9);"
                                                onClick="$('input[id$=Txt_Septiembre]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Septiembre" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Octubre" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                               onBlur="$('input[id$=Txt_Octubre]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(10);"
                                                onClick="$('input[id$=Txt_Octubre]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Octubre" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Noviembre" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Noviembre]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(11);"
                                                onClick="$('input[id$=Txt_Noviembre]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Noviembre" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Diciembre" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Diciembre]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(12);"
                                                onClick="$('input[id$=Txt_Diciembre]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Diciembre" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height:1em; cursor:default;" class="button_autorizar2" colspan="5">
                                                <asp:Label ID="Lbl_Validacion" runat="server" Width="100%" style="text-align:left; font-size:8pt; color:Red;"></asp:Label>
                                            </td>
                                            <td style="height:1em; width:15%; cursor:default;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Total" runat="server" Width="100%" Text="Total" style="text-align:center; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="height:1em; width:15%; cursor:default;" class="button_autorizar2" >
                                                <asp:TextBox ID="Txt_Total" runat="server" Width="100%" style="text-align:right; font-size:8pt; border-color:Navy" ReadOnly="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                   </table>
                                </td>
                            </tr>
                            <tr><td style="height:0.2em;" colspan="2"></td></tr>
                            <tr>
                                <td colspan="2">
                                    <table id="Tabla_Encabezado_Origen" runat="server" cellpadding="0" cellspacing="0" class="tblHead" style="width:96%; ">
                                        <tr>
                                            <td style="width:39%; font-size:8pt!important; text-align:center;">PARTIDA</td>
                                            <td style="width:32%; font-size:8pt!important; text-align:center;">PROGRAMA</td>
                                            <td style="width:13%; font-size:8pt!important; text-align:right;">MODIFICADO</td>
                                            <td  style="width:13%; font-size:8pt!important; text-align:right;"> DISPONIBLE</td>
                                        </tr>
                                     </table>
                                    <div ID="Div_Partidas" runat="server" style="width:99%; height:auto; max-height:160px; overflow:auto;">
                                        <asp:GridView ID="Grid_Partidas" runat="server"  CssClass="GridView_1" Width="97%" 
                                            AutoGenerateColumns="False"  GridLines="None" 
                                            HeaderStyle-CssClass="tblHead" ShowHeader="false"
                                            EmptyDataText="No se encuentraron partidas"
                                            OnSelectedIndexChanged = "Grid_Partidas_SelectedIndexChanged"
                                            >
                                            <Columns>
                                                <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                    ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                    <ItemStyle Width="3%" />
                                                </asp:ButtonField>
                                                <asp:BoundField DataField="nombre" HeaderText="Partida" >
                                                    <HeaderStyle HorizontalAlign="Left" Width="39%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="39%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PROGRAMA" HeaderText="Programa" >
                                                    <HeaderStyle HorizontalAlign="Left" Width="32%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="32%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="MODIFICADO" HeaderText="Modificado" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="DISPONIBLE" HeaderText="Disponible" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AMPLIACION"  DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="REDUCCION"  DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="APROBADO"  DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_ENERO"  DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_FEBRERO" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_MARZO" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_ABRIL" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_MAYO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_JUNIO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_JULIO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_AGOSTO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_SEPTIEMBRE" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_OCTUBRE" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_NOVIEMBRE" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_DICIEMBRE" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="Partida_id" />
                                                <asp:BoundField DataField="CAPITULO_ID" />
                                                <asp:BoundField DataField="PROGRAMA_ID" />
                                                <asp:BoundField DataField="FF_ID" />
                                                <asp:BoundField DataField="AF_ID" />
                                                <asp:BoundField DataField="UR_ID" />
                                            </Columns>
                                            <SelectedRowStyle CssClass="GridSelected" />
                                            <PagerStyle CssClass="GridHeader" />
                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:right;" colspan="2">
                                    <asp:ImageButton id="Btn_Agregar_Origen" runat="server" ImageUrl="~/paginas/imagenes/gridview/add_grid.png"
                                    ToolTip="Agregar" OnClick="Btn_Agregar_Origen_Click"/> &nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr><td style="height:0.2em;" colspan="2"></td></tr>
                            <tr>
                                <td colspan="2">
                                    <div ID="Div_Movimientos_Origen" runat="server" style="width:99%; height:auto; max-height:200px; overflow:auto; vertical-align:top;">
                                        <asp:GridView ID="Grid_Mov_Origen" runat="server"  CssClass="GridView_1" Width="98%" 
                                            AutoGenerateColumns="False"  GridLines="None"  HeaderStyle-CssClass="tblHead" 
                                            OnSelectedIndexChanged = "Grid_Mov_Origen_SelectedIndexChanged">
                                            <Columns>
                                                <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                    ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                    <ItemStyle Width="3%" />
                                                </asp:ButtonField>
                                                <asp:BoundField DataField="CLAVE_NOM_PARTIDA" HeaderText="Partida" >
                                                    <HeaderStyle HorizontalAlign="Left" Width="25%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="25%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CLAVE_NOM_PROGRAMA" HeaderText="Programa" >
                                                    <HeaderStyle HorizontalAlign="Left" Width="25%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="25%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="IMPORTE" HeaderText="Importe" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="10%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="APROBADO" HeaderText="Aprobado" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="10%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AMPLIACION" HeaderText="Ampliación" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="10%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="REDUCCION" HeaderText="Reducción" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="10%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="MODIFICADO" HeaderText="Modificado" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="10%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="DISPONIBLE"  DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_ENERO"  DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_FEBRERO" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_MARZO" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_ABRIL" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_MAYO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_JUNIO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_JULIO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_AGOSTO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_SEPTIEMBRE" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_OCTUBRE" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_NOVIEMBRE" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_DICIEMBRE" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_TOTAL" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="ESTATUS" />
                                                <asp:BoundField DataField="TIPO_OPERACION" />
                                                <asp:BoundField DataField="TIPO_PARTIDA" />
                                                <asp:BoundField DataField="TIPO_EGRESO" />
                                                <asp:BoundField DataField="TIPO_MOVIMIENTO" />
                                                <asp:BoundField DataField="PARTIDA_ID" />
                                                <asp:BoundField DataField="PROGRAMA_ID" />
                                                <asp:BoundField DataField="FF_ID" />
                                                <asp:BoundField DataField="AF_ID" />
                                                <asp:BoundField DataField="UR_ID" />
                                                <asp:BoundField DataField="MOVIMIENTO_ID" />
                                                <asp:TemplateField>
                                                    <ItemTemplate >
                                                        <asp:ImageButton ID="Btn_Eliminar" runat="server" 
                                                            ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png" 
                                                            onclick="Btn_Eliminar_Click" CommandArgument='<%#Eval("MOVIMIENTO_ID")%>'
                                                            />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign ="Center" Font-Size="7pt" Width="1%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="CAPITULO_ID" />
                                                <asp:BoundField DataField="JUSTIFICACION" />
                                            </Columns>
                                            <SelectedRowStyle CssClass="GridSelected" />
                                            <PagerStyle CssClass="GridHeader" />
                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:right;" colspan="2">
                                    <asp:Label ID="Lbl_Total_Mov_Origen" runat="server" Text="Total Origen"></asp:Label>
                                    <asp:TextBox ID="Txt_Total_Mov_Origen" runat="server" Width="20%" style="text-align:right; font-size:8pt; border-color:Navy" ReadOnly="true"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                     </asp:Panel>
                </div>   
                <br />
                <div id="Div_Partida_Destino" runat="server" style="width:97%;vertical-align:top;" >
                    <asp:Panel ID="Panel3" runat="server" GroupingText="Partida Destino">
                        <table width="100%"   border="0" cellspacing="0" class="estilo_fuente">
                            <tr runat="server" id="Tr_Programa_Destino">
                                <td> 
                                    <asp:Label ID="Lbl_Programa_Destino" runat="server" Text="Programa"   Width="100%"></asp:Label>
                                </td>
                                <td >
                                    <asp:DropDownList ID="Cmb_Programa_Destino" runat="server" Width="97%" 
                                    AutoPostBack="true" OnSelectedIndexChanged ="Cmb_Programa_Destino_SelectedIndexChanged"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td > 
                                    <asp:Label ID="Lbl_Unidad_Responsable_Destino" runat="server" Text="Unidad Responsable" Width="98%"  ></asp:Label>
                                </td>
                                <td >
                                    <asp:DropDownList ID="Cmb_Unidad_Responsable_Destino" runat="server" Width="75%" AutoPostBack ="true" 
                                    OnSelectedIndexChanged="Cmb_Unidad_Responsable_Destino_SelectedIndexChanged" >
                                    </asp:DropDownList>
                                    <asp:TextBox ID="Txt_Busqueda_URD" runat="server" Width="15%" MaxLength="50"></asp:TextBox>
                                <asp:ImageButton ID="Btn_Busqueda_URD" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                        ToolTip="Buscar Unidad Responsable"  OnClick="Btn_Busqueda_URD_Click" TabIndex="19" style="width:20px; height:18px;"/>
                                </td>
                            </tr>
                            <tr>
                                <td> 
                                    <asp:Label ID="Lbl_Fuente_Financiamiento_Destino" runat="server" Text="* Fuente Financiamiento"   Width="100%"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="Cmb_Fuente_Financiamiento_Destino" runat="server" Width="97%" AutoPostBack="True" 
                                     OnSelectedIndexChanged="Cmb_Fuente_Financiamiento_Destino_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr runat="server" id="Tr_Capitulo_Destino">
                                <td> 
                                    <asp:Label ID="Lbl_Capitulo_Destino" runat="server" Text=" Capitulo"   Width="100%"></asp:Label>
                                </td>
                                <td >
                                    <asp:DropDownList ID="Cmb_Capitulo_Destino" runat="server" Width="97%" AutoPostBack="True" 
                                     OnSelectedIndexChanged="Cmb_Capitulo_Destino_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td> 
                                    <asp:Label ID="Lbl_Partida_Destino" runat="server" Text="* Partida"   Width="100%"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat= "server" ID="Lbl_Nombre_Partida_Destino" Width="98%"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:HiddenField id="Hf_Area_Funcional_Destino_ID" runat="server"/>
                                    <asp:HiddenField id="Hf_Capitulo_Destino_ID" runat="server" />
                                    <asp:HiddenField id="Hf_Programa_Destino" runat="server"/>
                                    <asp:HiddenField id="Hf_Programa_Destino_ID" runat="server"/>
                                    <asp:HiddenField id="Hf_Partida_Destino" runat="server"/>
                                    <asp:HiddenField id="Hf_Partida_Destino_ID" runat="server"/>
                                    <asp:HiddenField id="Hf_Ampliacion_Destino" runat="server"/>
                                    <asp:HiddenField id="Hf_Reduccion_Destino" runat="server"/>
                                    <asp:HiddenField id="Hf_Modificado_Destino" runat="server"/>
                                    <asp:HiddenField id="Hf_Aprobado_Destino" runat="server"/>
                                </td>
                            </tr>
                            <tr><td style="height:0.5em;" colspan="2"></td></tr>
                            <tr id="Tr_Tabla_Meses_Destino" runat="server">
                                <td colspan="2">
                                     <table width="99%" style=" " border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td style="width:10%; text-align:left; font-size:8pt; cursor:default;"  class="button_autorizar2">Mes</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Enero </td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Febrero</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Marzo</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Abril</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Mayo</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Junio</td>
                                        </tr>
                                        <tr>
                                             <td style="text-align:left; font-size:8pt; cursor:default; width:10% " class="button_autorizar2">Disponible </td>
                                            <td style="text-align:left; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Ene_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Feb_Destino" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Mar_Destino" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Abr_Destino" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_May_Destino" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Jun_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:10%; text-align:left; cursor:default;"  class="button_autorizar2">&nbsp;</td>
                                            <td  style="text-align:right; width:15%; cursor:default; font-size:8pt;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Enero_Destino" runat="server" Width="100%"  style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Enero_Destino]').formatCurrency({colorize:true, region: 'es-MX'});  Validacion();"
                                                onClick="$('input[id$=Txt_Enero_Destino]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Enero_Destino" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Febrero_Destino" runat="server" Width="100px" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Febrero_Destino]').formatCurrency({colorize:true, region: 'es-MX'});  Validacion();"
                                                onClick="$('input[id$=Txt_Febrero_Destino]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Febrero_Destino" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Marzo_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Marzo_Destino]').formatCurrency({colorize:true, region: 'es-MX'}); Validacion();"
                                                onClick="$('input[id$=Txt_Marzo_Destino]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Marzo_Destino" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                               <asp:TextBox ID="Txt_Abril_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Abril_Destino]').formatCurrency({colorize:true, region: 'es-MX'}); Validacion();"
                                                onClick="$('input[id$=Txt_Abril_Destino]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Abril_Destino" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Mayo_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Mayo_Destino]').formatCurrency({colorize:true, region: 'es-MX'}); Validacion();"
                                                onClick="$('input[id$=Txt_Mayo_Destino]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Mayo_Destino" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Junio_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                               onBlur="$('input[id$=Txt_Junio_Destino]').formatCurrency({colorize:true, region: 'es-MX'}); Validacion();"
                                                onClick="$('input[id$=Txt_Junio_Destino]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Junio_Destino" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:10%; text-align:left; font-size:8pt; cursor:default;"  class="button_autorizar2">Mes</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Julio</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Agosto</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Septiembre </td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Octubre</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Noviembre</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar2">Diciembre</td>
                                        </tr>
                                        <tr id="Tr_Disponible_Destino" runat="server">
                                            <td style="width:10%; text-align:left; font-size:8pt; cursor:default;" class="button_autorizar2">Disponible</td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Jul_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Ago_Destino" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Sep_Destino" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Oct_Destino" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Nov_Destino" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Disp_Dic_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:10%;  text-align:left; font-size:8pt; cursor:default;" class="button_autorizar2">&nbsp;</td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:100px;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Julio_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Julio_Destino]').formatCurrency({colorize:true, region: 'es-MX'}); Validacion();"
                                                onClick="$('input[id$=Txt_Julio_Destino]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Julio_Destino" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                               <asp:TextBox ID="Txt_Agosto_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                               onBlur="$('input[id$=Txt_Agosto_Destino]').formatCurrency({colorize:true, region: 'es-MX'}); Validacion();"
                                                onClick="$('input[id$=Txt_Agosto_Destino]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Agosto_Destino" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Septiembre_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Septiembre_Destino]').formatCurrency({colorize:true, region: 'es-MX'}); Validacion();"
                                                onClick="$('input[id$=Txt_Septiembre_Destino]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Septiembre_Destino" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Octubre_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                               onBlur="$('input[id$=Txt_Octubre_Destino]').formatCurrency({colorize:true, region: 'es-MX'}); Validacion();"
                                                onClick="$('input[id$=Txt_Octubre_Destino]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Octubre_Destino" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Noviembre_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Noviembre_Destino]').formatCurrency({colorize:true, region: 'es-MX'}); Validacion();"
                                                onClick="$('input[id$=Txt_Noviembre_Destino]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Noviembre_Destino" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar2">
                                                <asp:TextBox ID="Txt_Diciembre_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Diciembre_Destino]').formatCurrency({colorize:true, region: 'es-MX'}); Validacion();"
                                                onClick="$('input[id$=Txt_Diciembre_Destino]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Diciembre_Destino" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height:1em; cursor:default;" class="button_autorizar2" colspan="5">
                                                <asp:Label ID="Lbl_Mensaje_Validacion" runat="server" Width="100%" style="text-align:left; font-size:8pt; color:Red;"></asp:Label>
                                            </td>
                                            <td style="height:1em; width:15%; cursor:default;" class="button_autorizar2">
                                                <asp:Label ID="Lbl_Total_Des" runat="server" Width="100%" Text="Total" style="text-align:center; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="height:1em; width:15%; cursor:default;" class="button_autorizar2" >
                                                <asp:TextBox ID="Txt_Total_Des" runat="server" Width="100%" style="text-align:right; font-size:8pt; border-color:Navy" ReadOnly="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                   </table>
                                </td>
                            </tr>
                            <tr><td style="height:0.5em;" colspan="2"></td></tr>
                            <tr>
                                <td colspan="2">
                                    <table id="Tabla_Encabezado_Destino" runat="server" cellpadding="0" cellspacing="0" class="GridHeader_Nested" style="width:96%; ">
                                        <tr>
                                            <td style="width:39%; font-size:8pt!important; text-align:center;">PARTIDA</td>
                                            <td style="width:32%; font-size:8pt!important; text-align:center;">PROGRAMA</td>
                                            <td style="width:13%; font-size:8pt!important; text-align:right;">MODIFICADO</td>
                                            <td  style="width:13%; font-size:8pt!important; text-align:right;"> DISPONIBLE</td>
                                        </tr>
                                     </table>
                                    <div ID="Div2" runat="server" style="width:99%; height:auto;  max-height:160px; overflow:auto; ">
                                        <asp:GridView ID="Grid_Partidas_Destino" runat="server"  CssClass="GridView_Nested" Width="97%" 
                                            AutoGenerateColumns="False"  GridLines="None" ShowHeader="false"
                                            EmptyDataText="No se encuentraron partidas" HeaderStyle-CssClass="tblHead"
                                            OnSelectedIndexChanged = "Grid_Partidas_Destino_SelectedIndexChanged"
                                            >
                                            <Columns>
                                                <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                    ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                    <ItemStyle Width="3%" />
                                                </asp:ButtonField>
                                                <asp:BoundField DataField="nombre" HeaderText="Partida" >
                                                    <HeaderStyle HorizontalAlign="Left" Width="39%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="39%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PROGRAMA" HeaderText="Programa" >
                                                    <HeaderStyle HorizontalAlign="Left" Width="32%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="32%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="MODIFICADO" HeaderText="Modificado" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="DISPONIBLE" HeaderText="Disponible" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AMPLIACION"  DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="REDUCCION"  DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="APROBADO"  DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_ENERO"  DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_FEBRERO" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_MARZO" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_ABRIL" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_MAYO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_JUNIO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_JULIO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_AGOSTO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_SEPTIEMBRE" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_OCTUBRE" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_NOVIEMBRE" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_DICIEMBRE" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="Partida_id" />
                                                <asp:BoundField DataField="CAPITULO_ID" />
                                                <asp:BoundField DataField="PROGRAMA_ID" />
                                                <asp:BoundField DataField="FF_ID" />
                                                <asp:BoundField DataField="AF_ID" />
                                                <asp:BoundField DataField="UR_ID" />
                                            </Columns>
                                            <SelectedRowStyle CssClass="GridSelected_Nested" />
                                            <PagerStyle CssClass="GridHeader_Nested" />
                                            <HeaderStyle CssClass="GridHeader_Nested" />
                                            <AlternatingRowStyle CssClass="GridAltItem_Nested" />
                                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:right;" colspan="2">
                                    <asp:ImageButton id="Btn_Agregar_Destino" runat="server" ImageUrl="~/paginas/imagenes/gridview/add_grid.png"
                                    ToolTip="Agregar" OnClick="Btn_Agregar_Destino_Click"/> &nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr><td style="height:0.2em;" colspan="2"></td></tr>
                            <tr>
                                <td colspan="2">
                                    <div ID="Div_Mov_Destino" runat="server" style="width:99%; height:auto; max-height:200px; overflow:auto; vertical-align:top;">
                                        <asp:GridView ID="Grid_Mov_Destino" runat="server"  CssClass="GridView_Nested" Width="97%" 
                                            AutoGenerateColumns="False"  GridLines="None"  HeaderStyle-CssClass="tblHead" 
                                            OnSelectedIndexChanged = "Grid_Mov_Destino_SelectedIndexChanged">
                                            <Columns>
                                                <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                    ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                    <ItemStyle Width="3%" />
                                                </asp:ButtonField>
                                                <asp:BoundField DataField="CLAVE_NOM_PARTIDA" HeaderText="Partida" >
                                                    <HeaderStyle HorizontalAlign="Left" Width="25%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="25%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CLAVE_NOM_PROGRAMA" HeaderText="Programa" >
                                                    <HeaderStyle HorizontalAlign="Left" Width="25%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="25%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="IMPORTE" HeaderText="Importe" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="10%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="APROBADO" HeaderText="Aprobado" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="10%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AMPLIACION" HeaderText="Ampliación" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="10%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="REDUCCION" HeaderText="Reducción" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="10%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="MODIFICADO" HeaderText="Modificado" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="10%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="DISPONIBLE"  DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_ENERO"  DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_FEBRERO" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_MARZO" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_ABRIL" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_MAYO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_JUNIO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_JULIO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_AGOSTO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_SEPTIEMBRE" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_OCTUBRE" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_NOVIEMBRE" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_DICIEMBRE" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_TOTAL" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="ESTATUS" />
                                                <asp:BoundField DataField="TIPO_OPERACION" />
                                                <asp:BoundField DataField="TIPO_PARTIDA" />
                                                <asp:BoundField DataField="TIPO_EGRESO" />
                                                <asp:BoundField DataField="TIPO_MOVIMIENTO" />
                                                <asp:BoundField DataField="PARTIDA_ID" />
                                                <asp:BoundField DataField="PROGRAMA_ID" />
                                                <asp:BoundField DataField="FF_ID" />
                                                <asp:BoundField DataField="AF_ID" />
                                                <asp:BoundField DataField="UR_ID" />
                                                <asp:BoundField DataField="MOVIMIENTO_ID" />
                                                <asp:TemplateField>
                                                    <ItemTemplate >
                                                        <asp:ImageButton ID="Btn_Eliminar" runat="server" 
                                                            ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png" 
                                                            onclick="Btn_Eliminar_Click" CommandArgument='<%#Eval("MOVIMIENTO_ID")%>'
                                                            />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign ="Center" Font-Size="7pt" Width="1%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="CAPITULO_ID" />
                                                <asp:BoundField DataField="JUSTIFICACION" />
                                            </Columns>
                                            <SelectedRowStyle CssClass="GridSelected_Nested" />
                                            <PagerStyle CssClass="GridHeader_Nested" />
                                            <HeaderStyle CssClass="GridHeader_Nested" />
                                            <AlternatingRowStyle CssClass="GridAltItem_Nested" />
                                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:right;" colspan="2">
                                    <asp:Label ID="Lbl_Total_Mov_Destino" runat="server" Text="Total Destino"></asp:Label>
                                    <asp:TextBox ID="Txt_Total_Mov_Destino" runat="server" Width="20%" style="text-align:right; font-size:8pt; border-color:Navy" ReadOnly="true"></asp:TextBox>
                                </td>
                            </tr>
                            <tr id="Tr_Restante_Destino" runat = "server">
                                <td style="text-align:right;" colspan="2">
                                    <asp:Label ID="Lbl_Restante" runat="server"  Text="Restante por Asignar"></asp:Label>
                                    <asp:TextBox ID="Txt_Restante_Destino" runat="server" Width="20%" style="text-align:right; font-size:8pt; border-color:Red" ReadOnly="true"></asp:TextBox>
                                </td>
                            </tr>
                    </table> 
                    </asp:Panel>
            </div> 
                <br />
                <div id="Div_Grid_Comentarios" runat="server" style="width:97%;vertical-align:top;">
                   <asp:Panel ID="Panel4" runat="server" GroupingText="Comentarios">
                    <div id="Div1" runat="server" style="overflow:auto;height:auto;width:97%;vertical-align:top;">
                         <table width="99%" border="0" cellspacing="0" class="estilo_fuente">
                           <tr>
                                <td style="width:12%; text-align:left;" >
                                    <asp:Label ID="Lbl_Comentario" runat="server" Text="Comentario:" Width="100%" style="text-align:left;"></asp:Label>
                                </td>
                                <td colspan = "3" style="width:88%; text-align:left;">
                                    <asp:TextBox ID="Txt_Comentario" runat="server" TextMode="MultiLine" Width="98%" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:12%; text-align:left;">
                                    <asp:Label ID="Lbl_Autorizo" runat="server" Text="Reviso:" Width="100%" style="text-align:left;"></asp:Label>
                                </td>
                                <td style="width:50%; text-align:left;">
                                    <asp:TextBox ID="Txt_Autorizo" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                </td>
                                <td style="width:12%; text-align:left;">
                                    <asp:Label ID="Lbl_Fecha_Autorizo" runat="server" Text="Fecha:" Width="100%" style="text-align:left;"></asp:Label>
                                </td>
                                <td style="width:26%; text-align:left;">
                                    <asp:TextBox ID="Txt_Fecha_Autorizo" runat="server" Width="93%" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                          </table>
                       </div>
                   </asp:Panel>
                </div>
             </div>
             <div style="width:97%;vertical-align:top;">
                <asp:Panel ID="Pnl_Datos_Solicitud_Contenedor" runat="server" CssClass="drag"  HorizontalAlign="Center" Width="650px"  Height ="550px"
                style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">
               <div style="color: #5D7B9D">
                 <table width="100%">
                    <tr>
                        <td align="left" style="text-align: left;" >                                    
                            <asp:UpdatePanel ID="Upnl_Modal_Datos_Solicitud" runat="server">
                                <ContentTemplate>
                                    <asp:UpdateProgress ID="Progress_Modal_Datos_Solicitud" runat="server" AssociatedUpdatePanelID="Upnl_Modal_Datos_Solicitud" DisplayAfter="0">
                                        <ProgressTemplate>
                                            <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                            <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress"><img alt="" src="../Imagenes/paginas/Sias_Roler.gif" /></div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress> 
                                    
                                      <table width="100%">
                                       <tr>
                                            <td colspan="2">
                                                <table style="width:95%;">
                                                  <tr>
                                                    <td align="left">
                                                      <asp:ImageButton ID="Img_Error_Busqueda" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                                        Width="24px" Height="24px" style="display:none" />
                                                        <asp:Label ID="Lbl_Error_Busqueda" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" style="display:none"/>
                                                    </td>            
                                                  </tr>         
                                                </table>  
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" colspan="2">
                                               <asp:ImageButton ID="Btn_Cerrar_Ventana" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana"
                                                    ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" OnClientClick="javascript:return Cerrar_Modal_Popup();"/>  
                                            </td>
                                        </tr>
                                       <tr>
                                            <td style="width:100%" colspan="2">
                                                <hr />
                                            </td>
                                        </tr>   
                                        <tr>
                                            <td style="width:100%" colspan="2">
                                                Los siguientes datos son para generar la solicitud de movimientos de presupuesto, por eso es necesario que verifiques los siguientes datos, y si alguno no es válido lo modifiques y posteriormente repórtalo al administrador del sistema para que configure los datos adecuadamente. Por tu atención, Gracias. 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:100%" colspan="2">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:25%;text-align:left;font-size:11px;">
                                               Nombre Remitente
                                            </td>              
                                            <td style="width:75%;text-align:left;font-size:11px;">
                                               <asp:TextBox ID="Txt_Nombre_Solicitante" runat="server" Width="98%" />
                                            </td> 
                                            
                                        </tr>
                                        <tr>
                                            <td style="text-align:left;font-size:11px;">
                                                Puesto Remitente
                                            </td>
                                            <td style="text-align:left;font-size:11px;">
                                                <asp:TextBox ID="Txt_Puesto_Solicitante" runat="server" Width="98%" />
                                            </td>
                                        </tr>
                                        <tr><td colspan="2">&nbsp;</td></tr>
                                        <tr>
                                            <td style="width:25%;text-align:left;font-size:11px;">
                                               Nombre Destinatario
                                            </td>              
                                            <td style="width:75%;text-align:left;font-size:11px;">
                                               <asp:TextBox ID="Txt_Nombre_Dr" runat="server" Width="98%" />
                                            </td> 
                                            
                                        </tr>
                                        <tr>
                                            <td style="text-align:left;font-size:11px;">
                                                Puesto Destinatario
                                            </td>
                                            <td style="text-align:left;font-size:11px;">
                                                <asp:TextBox ID="Txt_Puesto_Dr" runat="server" Width="98%" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:left;font-size:11px;">
                                                Email Destinatario
                                            </td>
                                            <td style="text-align:left;font-size:11px;">
                                                <asp:TextBox ID="Txt_Email_Dr" runat="server" Width="98%" 
                                                  onblur="this.value = (this.value.match(/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/))? this.value : '';"/>
                                            </td>
                                        </tr>
                                        <tr><td colspan="2">&nbsp;</td></tr>
                                        <tr>
                                            <td style="width:25%;text-align:left;font-size:11px;">
                                               Nombre Revisor Presupuestos
                                            </td>              
                                            <td style="width:75%;text-align:left;font-size:11px;">
                                               <asp:TextBox ID="Txt_Nombre_Psp" runat="server" Width="98%" />
                                            </td> 
                                        </tr>
                                        <tr>
                                            <td style="text-align:left;font-size:11px;">
                                                Puesto Revisor Presupuestos
                                            </td>
                                            <td style="text-align:left;font-size:11px;">
                                                <asp:TextBox ID="Txt_Puesto_Psp" runat="server" Width="98%" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:left;font-size:11px;">
                                                Email Revisor Presupuestos
                                            </td>
                                            <td style="text-align:left;font-size:11px;">
                                                <asp:TextBox ID="Txt_Email_Psp" runat="server" Width="98%" 
                                                onblur="this.value = (this.value.match(/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/))? this.value : '';"/>
                                            </td>
                                        </tr>
                                        <tr><td colspan="2">&nbsp;</td></tr>
                                        <tr>
                                            <td colspan="2" style="text-align:left; font-size:11pt; font-weight:bold;">&nbsp;
                                                Enviar Correo 
                                                &nbsp;
                                                <asp:CheckBox ID="Chk_Enviar_correo" runat = "server"  Checked ="false" />
                                            </td>
                                        </tr>
                                       <tr>
                                            <td style="width:100%" colspan="2">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:100%;text-align:left;" colspan="2">
                                                <center>
                                                   <asp:Button ID="Btn_Aceptar" runat="server"  Text="Aceptar" CssClass="button"  
                                                    CausesValidation="false"  Width="200px" OnClick="Btn_Aceptar_Click"/> 
                                                </center>
                                            </td>
                                        </tr> 
                                      </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="Btn_Aceptar" EventName="Click"/>
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                 </table>
               </div> 
             </asp:Panel>
             </div>
            </center>
         </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>