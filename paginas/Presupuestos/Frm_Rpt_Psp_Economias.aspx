﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" 
CodeFile="Frm_Rpt_Psp_Economias.aspx.cs" Inherits="paginas_Presupuestos_Frm_Rpt_Psp_Economias" Title="SIAC Sistema Integral Administrativo y Comercial" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <style type="text/css">
         .button_autorizar2{
            margin:0 7px 0 0;
            background-color:#f5f5f5;
            border:1px solid #dedede;
            border-top:1px solid #eee;
            border-left:1px solid #eee;

            font-family:"Lucida Grande", Tahoma, Arial, Verdana, sans-serif;
            font-size:100%;    
            line-height:130%;
            text-decoration:none;
            font-weight:bold;
            color:#565656;
            cursor:pointer;
            padding:5px 10px 6px 7px; /* Links */
            width:99%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <asp:ScriptManager ID="ScptM_Rpt_Psp" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>                     
            </asp:UpdateProgress>
            <div style="width:100%;">
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr><td colspan="2" style="height:0.5em;"></td></tr>
                    <tr align="center">
                        <td class="label_titulo" colspan="2">Reporte de Economias</td>
                    </tr>
                    <tr>
                        <td colspan ="2">
                          <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px" Enabled="false"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;">              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top">
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                            </table>                   
                          </div>                
                        </td>
                    </tr>
                    <tr><td colspan="2" style="height:0.5em;"></td></tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" style="width:50%">
                            &nbsp;
                            <asp:ImageButton ID="IBtn_Generar" runat="server" style="cursor:pointer;"
                                ImageUrl="~/paginas/imagenes/paginas/microsoft_office2003_excel.png" Width="25px" Height="25px" 
                                CssClass="Img_Button" ToolTip="Generar Reporte" 
                                OnClick="Btn_Generar_Click"/>
                            <asp:ImageButton ID="Btn_Salir" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" Width="24px" 
                                CssClass="Img_Button" ToolTip="Salir" 
                                OnClick="Btn_Salir_Click"/>
                        </td>
                        <td align="right" style="width:50%">
                             <asp:Button id="Btn_Consultar" runat="server" Text = "Consultar" CssClass="button_agregar"
                               OnClick="Btn_Consultar_Click" />
                        </td>
                    </tr>
                </table>   
                <br />
                <center>
                  <asp:Panel ID="Panel1" runat="server" GroupingText="Filtros de reporte" Width="96%">
                    <center>
                        <table width="98%">
                            <tr><td colspan="6" style="height:0.3em;"></td></tr>
                            <tr>
                                <td style="width:23%; text-align:left; cursor:default;" class="button_autorizar2">
                                    <asp:Label ID="Lbl_Anio" runat="server" Text="* Año"></asp:Label>
                                </td>
                                <td style="width:27%; text-align:left; cursor:default;" class="button_autorizar2">
                                    <asp:DropDownList id= "Cmb_Anio" runat = "server" Width="100%"></asp:DropDownList>
                                </td>
                                 <td colspan="2" style=" text-align:left; cursor:default;" class="button_autorizar2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="width:15%; text-align:left; cursor:default;" class="button_autorizar2">
                                    <asp:Label ID="Lbl_FF" runat="server" Text="Fuente Financiamiento"></asp:Label>
                                </td>
                                <td style=" text-align:left; cursor:default; width:85%;"  colspan="3" class="button_autorizar2">
                                    <asp:DropDownList ID="Cmb_FF" runat="server" Width="100%"
                                    OnSelectedIndexChanged="Cmb_FF_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:15%; text-align:left; cursor:default;" class="button_autorizar2">
                                    <asp:Label ID="Lbl_Programa" runat="server" Text="Programa"></asp:Label>
                                </td>
                                <td style=" text-align:left; cursor:default; width:85%;"  colspan="3" class="button_autorizar2">
                                    <asp:DropDownList ID="Cmb_Programa" runat="server" Width="100%"
                                    OnSelectedIndexChanged="Cmb_Programa_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:15%; text-align:left; cursor:default;" class="button_autorizar2">
                                    <asp:Label ID="Lbl_UR" runat="server" Text="Unidad Responsable"></asp:Label>
                                </td>
                                <td style=" text-align:left; cursor:default; width:85%;"  colspan="3" class="button_autorizar2">
                                    <asp:DropDownList ID="Cmb_UR" runat="server" Width="100%"
                                    OnSelectedIndexChanged="Cmb_UR_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:15%; text-align:left; cursor:default;" class="button_autorizar2">
                                    <asp:Label ID="Lbl_Partida" runat="server" Text="Partida"></asp:Label>
                                </td>
                                <td style=" text-align:left; cursor:default; width:90%;"  colspan="3" class="button_autorizar2">
                                    <asp:DropDownList ID="Cmb_Partida" runat="server" Width="100%"></asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                        <table width="98%" class="estilo_fuente" style="text-align:center;">
                        <tr>
                            <td>
                            <div style="overflow:auto;height:auto; max-height:320px;width:85%;vertical-align:top;" >
                                <asp:GridView ID="Grid_Registros" runat="server"  CssClass="GridView_1" Width="155%" 
                                    AutoGenerateColumns="False"  GridLines="None" AllowPaging="false" 
                                    AllowSorting="True" HeaderStyle-CssClass="tblHead" 
                                    EmptyDataText="No se encuentra ningun registro" 
                                    OnRowDataBound="Grid_Registros_RowDataBound" >
                                    <Columns>
                                        <asp:BoundField DataField="CLASIFICACION POR OBJETO" HeaderText="Clasificacion por Objeto">
                                    <HeaderStyle HorizontalAlign="Left" Width="30%" />
                                    <ItemStyle HorizontalAlign="Left" Width="30%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="ENERO" HeaderText="Enero">
                                     <HeaderStyle HorizontalAlign="Center" Width="8%" />
                                    <ItemStyle HorizontalAlign="Right" Width="8%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="FEBRERO" HeaderText="Febrero" >
                                     <HeaderStyle HorizontalAlign="Center" Width="8%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="8%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="MARZO" HeaderText="Marzo">
                                     <HeaderStyle HorizontalAlign="Center" Width="8%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="8%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="ABRIL" HeaderText="Abril">
                                     <HeaderStyle HorizontalAlign="Center" Width="8%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="8%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="MAYO" HeaderText="Mayo" >
                                     <HeaderStyle HorizontalAlign="Center" Width="8%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="8%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="JUNIO" HeaderText="Junio">
                                     <HeaderStyle HorizontalAlign="Center" Width="8%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="8%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="JULIO" HeaderText="Julio" >
                                     <HeaderStyle HorizontalAlign="Center" Width="8%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="8%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="AGOSTO" HeaderText="Agosto" >
                                     <HeaderStyle HorizontalAlign="Center" Width="8%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="8%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="SEPTIEMBRE" HeaderText="Septiembre" >
                                     <HeaderStyle HorizontalAlign="Center" Width="9%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="9%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="OCTUBRE" HeaderText="Octubre" >
                                     <HeaderStyle HorizontalAlign="Center" Width="9%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="9%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="NOVIEMBRE" HeaderText="Noviembre" >
                                     <HeaderStyle HorizontalAlign="Center" Width="9%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="9%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="DICIEMBRE" HeaderText="Diciembre" >
                                     <HeaderStyle HorizontalAlign="Center" Width="9%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="9%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="Tipo" />
                                    </Columns>
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <PagerStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                                </div>
                            </td>
                        </tr>
                    </table>
                    </center> 
                  </asp:Panel>
                </center>
            </div>
          </ContentTemplate>
          <Triggers>
            <asp:PostBackTrigger ControlID="IBtn_Generar" />
          </Triggers>
    </asp:UpdatePanel>
</asp:Content>

