﻿using System;
using System.Linq;
using JAPAMI.Sessiones;
using System.Data;
using JAPAMI.Reporte_Presupuesto_Egresos.Negocio;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using JAPAMI.Constantes;
using System.Text;
using System.Web.UI;
using JAPAMI.Reporte_Justificacion_Movimientos_Egresos.Negocio;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using JAPAMI.Rpt_Psp_Movimeitnos_Presupuesto_Ingresos.Negocio;

public partial class paginas_Presupuestos_Frm_Rpt_Psp_Reporte_Justificacion_Movimientos_Egresos : System.Web.UI.Page
{
    #region(Load)
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Page_Load
        /// DESCRIPCION         : Inicio de la pagina
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 31/Julio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*******************************************************************************
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Valida que existe un usuario logueado en el sistema
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

                if (!IsPostBack)
                {
                    Inicializa_Controles();
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Inicializa_Controles " + Ex.Message.ToString());
            }

        }
    #endregion

    #region(Metodos)
        #region(Metodos Generales)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Inicializa_Controles
            /// DESCRIPCION         : Prepara los controles en la forma para que el usuario pueda
            ///                       realizar diferentes operaciones
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 31/Julio/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*******************************************************************************
            private void Inicializa_Controles()
            {
                try
                {
                    Limpiar_Controles(); //Limpia los controles del formulario
                    Cargar_Combo_Año();
                    Llenar_Combo_Tipo_Reporte();
                }
                catch (Exception ex)
                {
                    throw new Exception("Inicializa_Controles " + ex.Message.ToString());
                }
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Limpiar_Controles
            /// DESCRIPCION         : Limpia los controles que se encuentran en la forma
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 31/Julio/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*******************************************************************************
            private void Limpiar_Controles()
            {
                try
                {
                    DateTime Tiempo_Ahora = DateTime.Now;
                    String Año = "" + Tiempo_Ahora.Year;
                    //Cmb_Anio.SelectedIndex=Cmb_Anio.Items.IndexOf(Cmb_Anio.Items.FindByValue(Año));
                    Cmb_Anio.SelectedIndex = -1;
                    Cmb_Tipo_Reporte.SelectedIndex = -1;
                    Lbl_Mensaje_Error.Text = "";
                    Lbl_Mensaje_Error.Visible = false;
                    Img_Error.Visible = false;
                }
                catch (Exception ex)
                {
                    throw new Exception("Limpia_Controles " + ex.Message.ToString());
                }
            }
    #endregion

        #region(Reporte)
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Dt_Analitico
            ///DESCRIPCIÓN          : metodo para agrupar los datos de las dependencias
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 31/Julio/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public DataTable Obtener_Dt_UR(DataTable Dt)
            {
                DataTable Dt_Datos = new DataTable();
                Double Mod = 0.00;
                Double Tot_Mod = 0.00;
                String Ur = String.Empty;
                String Nom_Ur = String.Empty;
                DataRow Fila;

                try
                {
                    if (Dt != null && Dt.Rows.Count > 0)
                    {
                        Dt_Datos.Columns.Add("CODIGO", System.Type.GetType("System.String"));
                        Dt_Datos.Columns.Add("DEPENDENCIA", System.Type.GetType("System.String"));
                        Dt_Datos.Columns.Add("MODIFICADO", System.Type.GetType("System.Double"));
                        Dt_Datos.Columns.Add("TIPO", System.Type.GetType("System.String"));

                        Ur = Dt.Rows[0]["UR"].ToString().Trim();
                        Nom_Ur = Dt.Rows[0]["NOMBRE_UR"].ToString().Trim();

                        foreach (DataRow Dr in Dt.Rows)
                        {
                            Tot_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"]);

                            if (Ur.Trim().Equals(Dr["UR"].ToString().Trim()))
                            {
                                //SUMAMOS EL TOTAL DE LA UR
                                Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"]);
                            }
                            else
                            {
                                //INSERTAMOS LA FILA DEL TOTAL DEL UR
                                Fila = Dt_Datos.NewRow();
                                Fila["CODIGO"] = Ur.Trim();
                                Fila["DEPENDENCIA"] = Nom_Ur.Trim();
                                Fila["MODIFICADO"] = Mod;
                                Fila["TIPO"] = "CONCEPTO";
                                Dt_Datos.Rows.Add(Fila);
                                Mod = 0.00;

                                //SUMAMOS EL TOTAL DE LA UR
                                Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"]);

                                //GUARDAMOS LOS NUEVOS DATOS DEL UR Y EL PROGRAMA
                                Ur = Dr["UR"].ToString().Trim();
                                Nom_Ur = Dr["NOMBRE_UR"].ToString().Trim();
                            }
                        }

                        Fila = Dt_Datos.NewRow();
                        Fila["CODIGO"] = Ur.Trim();
                        Fila["DEPENDENCIA"] = Nom_Ur.Trim();
                        Fila["MODIFICADO"] = Mod;
                        Fila["TIPO"] = "CONCEPTO";
                        Dt_Datos.Rows.Add(Fila);
                        Mod = 0.00;

                        Fila = Dt_Datos.NewRow();
                        Fila["CODIGO"] = String.Empty;
                        Fila["DEPENDENCIA"] = "TOTAL_PRESUPUESTO";
                        Fila["MODIFICADO"] = Tot_Mod;
                        Fila["TIPO"] = "TOTAL";
                        Dt_Datos.Rows.Add(Fila);
                        Mod = 0.00;
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Error al generar el reporte. Error[" + ex.Message.ToString() + "]";
                }
                return Dt_Datos;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Abrir_Archivo
            ///DESCRIPCIÓN          : Abre el archivo de excel
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 31/Julio/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///******************************************************************************* 
            public void Abrir_Archivo(String Ruta_Archivo)
            {
                try
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + Ruta_Archivo);
                    Response.Charset = "UTF-8";
                    Response.ContentEncoding = Encoding.Default;

                    Response.End();
                }
                catch (Exception Ex)
                {

                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = Ex.Message.ToString();
                    //throw new Exception("Error al generar el reporte de las Dependencias. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Generar_Reporte
            ///DESCRIPCIÓN          : Metodo para generar el reporte
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 31/Julio/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///******************************************************************************* 
            public void Generar_Reporte_Egresos(String No_Movimiento, String Anio, String Tipo_Reporte) 
            {
                Cls_Rpt_Psp_Reporte_Justificacion_Movimientos_Egresos_Negocio Negocio = new Cls_Rpt_Psp_Reporte_Justificacion_Movimientos_Egresos_Negocio();
                DataTable Dt_Registros = new DataTable();
                DataSet Ds_Registros = new DataSet();

                try
                {
                    Negocio.Anio = Anio;
                    Negocio.No_Movimiento = No_Movimiento;
                    Dt_Registros = Negocio.Reporte_Mov_Egresos();

                    if (Dt_Registros != null)
                    {
                        if (Dt_Registros.Rows.Count > 0) 
                        {
                            Dt_Registros.TableName = "Dt_Mov_Egresos";
                            Ds_Registros.Tables.Add(Dt_Registros.Copy());

                            if (Tipo_Reporte.Trim().Equals("PDF"))
                            {
                                Exportar_Reporte(Ds_Registros, "Cr_Rpt_Psp_Reporte_Justificacion_Mov_Egresos.rpt",
                                    "Rpt_Justificacion_Movimientos_Egresos_" + Session.SessionID, "pdf",
                                       ExportFormatType.PortableDocFormat, "Presupuestos");

                                Mostrar_Reporte("Rpt_Justificacion_Movimientos_Egresos_" + Session.SessionID + ".pdf");
                            }
                            else if (Tipo_Reporte.Trim().Equals("Excel"))
                            {
                                Exportar_Reporte(Ds_Registros, "Cr_Rpt_Psp_Reporte_Justificacion_Mov_Egresos.rpt",
                                        "Rpt_Justificacion_Movimientos_Egresos_" + Session.SessionID, "xls",
                                           ExportFormatType.Excel, "Presupuestos");

                                Mostrar_Reporte("Rpt_Justificacion_Movimientos_Egresos_" + Session.SessionID + ".xls");
                            }
                            else if (Tipo_Reporte.Trim().Equals("Word"))
                            {
                                Exportar_Reporte(Ds_Registros, "Cr_Rpt_Psp_Reporte_Justificacion_Mov_Egresos.rpt",
                                    "Rpt_Justificacion_Movimientos_Egresos_" + Session.SessionID, "doc",
                                       ExportFormatType.WordForWindows, "Presupuestos");

                                Mostrar_Reporte("Rpt_Justificacion_Movimientos_Egresos_" + Session.SessionID + ".doc");
                            }
                        }
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al generar el reporte. Error[" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Generar_Reporte_Ingresos
            ///DESCRIPCIÓN          : Metodo para generar el reporte
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 01/Agosto/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///******************************************************************************* 
            public void Generar_Reporte_Ingresos(String No_Movimiento, String Anio, String Tipo_Reporte)
            {
                Cls_Rpt_Psp_Reporte_Justificacion_Movimientos_Egresos_Negocio Negocio = new Cls_Rpt_Psp_Reporte_Justificacion_Movimientos_Egresos_Negocio();
                DataTable Dt_Registros = new DataTable();
                DataSet Ds_Registros = new DataSet();

                try
                {
                    Negocio.Anio = Anio;
                    Negocio.No_Movimiento = No_Movimiento;
                    Dt_Registros = Negocio.Reporte_Mov_Ingresos();

                    if (Dt_Registros != null)
                    {
                        if (Dt_Registros.Rows.Count > 0)
                        {
                            Dt_Registros.TableName = "Dt_Mov_Ingresos";
                            Ds_Registros.Tables.Add(Dt_Registros.Copy());

                            if (Tipo_Reporte.Trim().Equals("PDF"))
                            {
                                Exportar_Reporte(Ds_Registros, "Cr_Rpt_Psp_Reporte_Justificacion_Mov_Ingresos.rpt",
                                    "Rpt_Justificacion_Movimientos_Ingresos_" + Session.SessionID, "pdf",
                                       ExportFormatType.PortableDocFormat, "Presupuestos");

                                Mostrar_Reporte("Rpt_Justificacion_Movimientos_Ingresos_" + Session.SessionID + ".pdf");
                            }
                            else if (Tipo_Reporte.Trim().Equals("Excel"))
                            {
                                Exportar_Reporte(Ds_Registros, "Cr_Rpt_Psp_Reporte_Justificacion_Mov_Ingresos.rpt",
                                        "Rpt_Justificacion_Movimientos_Ingresos_" + Session.SessionID, "xls",
                                           ExportFormatType.Excel, "Presupuestos");

                                Mostrar_Reporte("Rpt_Justificacion_Movimientos_Ingresos_" + Session.SessionID + ".xls");
                            }
                            else if (Tipo_Reporte.Trim().Equals("Word"))
                            {
                                Exportar_Reporte(Ds_Registros, "Cr_Rpt_Psp_Reporte_Justificacion_Mov_Ingresos.rpt",
                                    "Rpt_Justificacion_Movimientos_Ingresos_" + Session.SessionID, "doc",
                                       ExportFormatType.WordForWindows, "Presupuestos");

                                Mostrar_Reporte("Rpt_Justificacion_Movimientos_Ingresos_" + Session.SessionID + ".doc");
                            }
                        }
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al generar el reporte. Error[" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************************************
            /// NOMBRE_FUNCIÓN: Exportar_Reporte
            /// DESCRIPCIÓN: Genera el reporte de Crystal con los datos proporcionados en el DataTable 
            /// PARÁMETROS:
            /// 		1. Ds_Reporte: Dataset con datos a imprimir
            /// 		2. Nombre_Reporte: Nombre del archivo de reporte .rpt
            /// 		3. Nombre_Archivo: Nombre del archivo a generar
            /// CREO: Roberto González Oseguera
            /// FECHA_CREO: 04-sep-2011
            /// MODIFICÓ: 
            /// FECHA_MODIFICÓ: 
            /// CAUSA_MODIFICACIÓN: 
            ///*******************************************************************************************************
            private void Exportar_Reporte(DataSet Ds_Reporte, String Nombre_Reporte, String Nombre_Archivo,
                String Extension_Archivo, ExportFormatType Formato, String Nombre_Carpeta)
            {
                ReportDocument Reporte = new ReportDocument();
                String Ruta = Server.MapPath("../Rpt/" + Nombre_Carpeta + "/" + Nombre_Reporte);

                try
                {
                    Reporte.Load(Ruta);
                    Reporte.SetDataSource(Ds_Reporte);
                }
                catch
                {
                    //Lbl_Mensaje_Error.Visible = true;
                    //Lbl_Mensaje_Error.Text = "No se pudo cargar el reporte";
                }

                String Archivo_Reporte = Nombre_Archivo + "." + Extension_Archivo;  // formar el nombre del archivo a generar 
                try
                {
                    ExportOptions Export_Options_Calculo = new ExportOptions();
                    DiskFileDestinationOptions Disk_File_Destination_Options_Calculo = new DiskFileDestinationOptions();
                    Disk_File_Destination_Options_Calculo.DiskFileName = Server.MapPath("../../Reporte/" + Archivo_Reporte);
                    Export_Options_Calculo.ExportDestinationOptions = Disk_File_Destination_Options_Calculo;
                    Export_Options_Calculo.ExportDestinationType = ExportDestinationType.DiskFile;
                    Export_Options_Calculo.ExportFormatType = Formato;
                    Reporte.Export(Export_Options_Calculo);
                }
                catch (Exception Ex)
                {
                    //Lbl_Mensaje_Error.Visible = true;
                    //Lbl_Mensaje_Error.Text = Ex.Message.ToString();
                }
            }

            /// *************************************************************************************
            /// NOMBRE: Mostrar_Reporte
            /// 
            /// DESCRIPCIÓN: Muestra el reporte en pantalla.
            ///              
            /// PARÁMETROS: Nombre_Reporte.- Nombre que tiene el reporte que se mostrara en pantalla.
            /// 
            /// USUARIO CREO: Juan Alberto Hernández Negrete.
            /// FECHA CREO: 3/Mayo/2011 18:20 p.m.
            /// USUARIO MODIFICO:
            /// FECHA MODIFICO:
            /// CAUSA MODIFICACIÓN:
            /// *************************************************************************************
            protected void Mostrar_Reporte(String Nombre_Reporte)
            {
                String Pagina = "../../Reporte/";
                try
                {
                    Pagina = Pagina + Nombre_Reporte;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt_Nominas_Negativas",
                        "window.open('" + Pagina + "', 'Busqueda_Empleados','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
                }
            }
        #endregion

        #region (Metodos Combos)
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Cargar_Combo_Año
            ///DESCRIPCIÓN          : Cargara los años de los registros que se encuentran en el presupuesto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 31/Julio/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///******************************************************************************* 
            protected void Cargar_Combo_Año()
            {
                Cls_Rpt_Presupuesto_Egresos_Negocio Rs_Año = new Cls_Rpt_Presupuesto_Egresos_Negocio();

                try
                {
                    System.Data.DataTable Dt_Año = Rs_Año.Consultar_Presupuesto_Año();
                    Cmb_Anio.Items.Clear();
                    Cmb_Anio.DataSource = Dt_Año;
                    Cmb_Anio.DataValueField = Ope_Psp_Presupuesto_Aprobado.Campo_Anio;
                    Cmb_Anio.DataTextField = Ope_Psp_Presupuesto_Aprobado.Campo_Anio;
                    Cmb_Anio.DataBind();
                    Cmb_Anio.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                    Cmb_Anio.SelectedIndex = 0;

                    Cmb_Anio.SelectedIndex = Cmb_Anio.Items.IndexOf(Cmb_Anio.Items.FindByValue(String.Format("{0:yyyy}", DateTime.Now)));
                }
                catch (Exception Ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = Ex.Message.ToString();
                    //throw new Exception("Error al generar el reporte de las Dependencias. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Tipo_Reporte
            ///DESCRIPCIÓN          : Llena el combo de tipo de reporte
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 31/Julio/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Llenar_Combo_Tipo_Reporte()
            {
                Cls_Rpt_Presupuesto_Egresos_Negocio Negocios = new Cls_Rpt_Presupuesto_Egresos_Negocio(); //conexion con la capa de negocios
                DataTable Dt_Datos = new DataTable();

                try
                {
                    if (!String.IsNullOrEmpty(Cmb_Anio.SelectedItem.Value.Trim()))
                    {
                        Negocios.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                        Dt_Datos = Negocios.Consultar_Tipo_Reporte();

                        //para destino
                        Cmb_Tipo_Reporte.Items.Clear();
                        Cmb_Tipo_Reporte.DataSource = Dt_Datos;
                        Cmb_Tipo_Reporte.DataValueField = Ope_Psp_Presupuesto_Egr_Esp.Campo_No_Modificacion;
                        Cmb_Tipo_Reporte.DataTextField = "MODIFICACION";
                        Cmb_Tipo_Reporte.DataBind();
                        Cmb_Tipo_Reporte.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }

            ///*****************************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Anios
            ///DESCRIPCIÓN          : Metodo para llenar el combo de los años
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 05/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*****************************************************************************************************************
            private void Llenar_Combo_Anios_Ingresos()
            {
                Cls_Rpt_Psp_Movimeitnos_Presupuesto_Ingresos_Negocio Obj_Ingresos = new Cls_Rpt_Psp_Movimeitnos_Presupuesto_Ingresos_Negocio();
                DataTable Dt_Anios = new DataTable(); //Para almacenar los datos de los tipos de nominas

                try
                {
                    Cmb_Anio.Items.Clear();

                    Dt_Anios = Obj_Ingresos.Consultar_Anios();

                    Cmb_Anio.DataValueField = Ope_Psp_Movimiento_Ing.Campo_Anio;
                    Cmb_Anio.DataTextField = Ope_Psp_Movimiento_Ing.Campo_Anio;
                    Cmb_Anio.DataSource = Dt_Anios;
                    Cmb_Anio.DataBind();

                    Cmb_Anio.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));

                    if(Dt_Anios != null)
                    {
                        if (Dt_Anios.Rows.Count > 0)
                        {
                            Cmb_Anio.SelectedIndex = Cmb_Anio.Items.IndexOf(Cmb_Anio.Items.FindByValue(DateTime.Now.Year.ToString()));
                        }
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al llenar el combo de anios: Error[" + Ex.Message + "]");
                }
            }

            ///*****************************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_No_Modificacion
            ///DESCRIPCIÓN          : Metodo para llenar el combo de las modificaciones del año
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 05/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*****************************************************************************************************************
            private void Llenar_Combo_No_Modificacion_Ingresos()
            {
                Cls_Rpt_Psp_Movimeitnos_Presupuesto_Ingresos_Negocio Obj_Ingresos = new Cls_Rpt_Psp_Movimeitnos_Presupuesto_Ingresos_Negocio();
                DataTable Dt = new DataTable(); //Para almacenar los datos de los tipos de nominas

                try
                {
                    Cmb_Tipo_Reporte.Items.Clear();

                    if (!String.IsNullOrEmpty(Cmb_Anio.SelectedItem.Value.Trim()))
                    {
                        Obj_Ingresos.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();

                        Dt = Obj_Ingresos.Consultar_No_Modificacion();

                        Cmb_Tipo_Reporte.DataValueField = Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing;
                        Cmb_Tipo_Reporte.DataTextField = Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing;
                        Cmb_Tipo_Reporte.DataSource = Dt;
                        Cmb_Tipo_Reporte.DataBind();
                    }

                    Cmb_Tipo_Reporte.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al llenar el combo de número de modificaciones: Error[" + Ex.Message + "]");
                }
            }
        #endregion
    #endregion

    #region(Eventos)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
        ///DESCRIPCIÓN          : Salir de la pantalla
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 31/Julio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Reporte_Excel_Click
        ///DESCRIPCIÓN          : Manda llamar los datos para cargarlos en el reporte de excel
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 31/Julio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Btn_Reporte_Excel_Click(object sender, ImageClickEventArgs e)
        {
            Lbl_Mensaje_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            String Tipo_Reporte = String.Empty;
            String Dependencia = String.Empty;
            String Gpo_Dependencia = String.Empty;

            try
            {
                if (!String.IsNullOrEmpty(Cmb_Tipo_Reporte.SelectedItem.Value.Trim()))
                {
                    if (Cmb_Anio.SelectedIndex > 0)
                    {
                        Lbl_Mensaje_Error.Visible = false;
                        Lbl_Mensaje_Error.Text = "";
                        Tipo_Reporte = Cmb_Tipo_Reporte.SelectedItem.Value.Trim();

                        if (Rbl_Presupuesto_ID.Text.Equals("Ingresos"))
                        {
                            Generar_Reporte_Ingresos(Tipo_Reporte, Cmb_Anio.SelectedItem.Value.Trim(), "Excel");
                        }
                        else
                        {
                            Generar_Reporte_Egresos(Tipo_Reporte, Cmb_Anio.SelectedItem.Value.Trim(), "Excel");
                        }
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "Seleccione el Año";
                    }
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Seleccione un número de modificación";
                }
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = Ex.Message.ToString();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Reporte_Pdf_Click
        ///DESCRIPCIÓN          : Manda llamar los datos para cargarlos en el reporte de pdf
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 31/Julio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Btn_Reporte_Pdf_Click(object sender, ImageClickEventArgs e)
        {
            Lbl_Mensaje_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            String Tipo_Reporte = String.Empty;
            String Dependencia = String.Empty;
            String Gpo_Dependencia = String.Empty;

            try
            {
                if (!String.IsNullOrEmpty(Cmb_Tipo_Reporte.SelectedItem.Value.Trim()))
                {
                    if (Cmb_Anio.SelectedIndex > 0)
                    {

                        Lbl_Mensaje_Error.Visible = false;
                        Lbl_Mensaje_Error.Text = "";
                        Tipo_Reporte = Cmb_Tipo_Reporte.SelectedItem.Value.Trim();

                        if (Rbl_Presupuesto_ID.Text.Equals("Ingresos"))
                        {
                            Generar_Reporte_Ingresos(Tipo_Reporte, Cmb_Anio.SelectedItem.Value.Trim(), "PDF");
                        }
                        else
                        {
                            Generar_Reporte_Egresos(Tipo_Reporte, Cmb_Anio.SelectedItem.Value.Trim(), "PDF");
                        }
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "Seleccione el Año";
                    }
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Seleccione un número de modificación";
                }
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = Ex.Message.ToString();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Reporte_Word_Click
        ///DESCRIPCIÓN          : Manda llamar los datos para cargarlos en el reporte de word
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 31/Julio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Btn_Reporte_Word_Click(object sender, ImageClickEventArgs e)
        {
            Lbl_Mensaje_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            String Tipo_Reporte = String.Empty;
            String Dependencia = String.Empty;
            String Gpo_Dependencia = String.Empty;

            try
            {
                if (!String.IsNullOrEmpty(Cmb_Tipo_Reporte.SelectedItem.Value.Trim()))
                {
                    if (Cmb_Anio.SelectedIndex > 0)
                    {

                        Lbl_Mensaje_Error.Visible = false;
                        Lbl_Mensaje_Error.Text = "";

                        Tipo_Reporte = Cmb_Tipo_Reporte.SelectedItem.Value.Trim();

                        if (Rbl_Presupuesto_ID.Text.Equals("Ingresos"))
                        {
                            Generar_Reporte_Ingresos(Tipo_Reporte, Cmb_Anio.SelectedItem.Value.Trim(), "Word");
                        }
                        else
                        {
                            Generar_Reporte_Egresos(Tipo_Reporte, Cmb_Anio.SelectedItem.Value.Trim(), "Word");
                        }
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "Seleccione el Año";
                    }
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Seleccione un número de modificación";
                }
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = Ex.Message.ToString();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Anio_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de anios
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 31/Julio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_Anio_SelectedIndexChanged(object sender, EventArgs e)
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = String.Empty;

            try
            {
                if (Rbl_Presupuesto_ID.Text.Equals("Ingresos"))
                {
                    Llenar_Combo_No_Modificacion_Ingresos();
                }
                else
                {
                    Llenar_Combo_Tipo_Reporte();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del combo de fuente de financiamiento Error[" + ex.Message + "]");
            }
        }

        //*******************************************************************************
        //NOMBRE DE LA FUNCIÓN : Btn_Busqueda_Empleados_Click
        //DESCRIPCIÓN          : Evento del boton de busquedas de empleados
        //PARAMETROS           :   
        //CREO                 : Leslie González Vázquez
        //FECHA_CREO           : 26/octubre/2011 
        //MODIFICO             :
        //FECHA_MODIFICO       :
        //CAUSA_MODIFICACIÓN   :
        //*******************************************************************************
        public void Rbl_Presupuesto_ID_CheckedChanged(object sender, System.EventArgs e)
        {
            try
            {
                if (Rbl_Presupuesto_ID.Text.Equals("Ingresos"))
                {
                    Llenar_Combo_Anios_Ingresos();
                    Llenar_Combo_No_Modificacion_Ingresos();
                }
                else
                {
                    Llenar_Combo_Tipo_Reporte();
                    Cargar_Combo_Año();
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error en el evento del tipo de presupuesto" + Ex.Message);
            }

        }
    #endregion
}
