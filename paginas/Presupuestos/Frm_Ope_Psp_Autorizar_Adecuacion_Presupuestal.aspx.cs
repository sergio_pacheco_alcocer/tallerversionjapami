﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Movimiento_Presupuestal.Negocio;
using JAPAMI.Autorizar_Traspaso_Presupuestal.Negocio;
using JAPAMI.Clasificacion_Gasto.Negocio;
using JAPAMI.Limite_Presupuestal.Negocio;
using JAPAMI.Calendarizar_Presupuesto.Negocio;
using JAPAMI.Paramentros_Presupuestos.Negocio;
using JAPAMI.Movimiento_Presupuestal_Ingresos.Negocio;
using JAPAMI.Constantes;

public partial class paginas_presupuestos_Frm_Ope_Psp_Autorizar_Adecuacion_Presupuestal : System.Web.UI.Page
{
    #region(Load)
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Valida que existe un usuario logueado en el sistema
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

                if (!IsPostBack)
                {
                    Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                    ViewState["SortDirection"] = "ASC";
                    Chk_Autorizar.Checked = false;
                    Txt_Busqueda.Text = String.Empty;
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
    #endregion

    #region(Metodos)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Inicializa_controles
        ///DESCRIPCIÓN          : metodo para inicializar los controles
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Inicializa_Controles()
        {
            try
            {
                Habilitar_Controles("Nuevo");//Inicializa todos los controles
                Obtener_Datos_Usuario();
                Llenar_Grid_Movimientos();
                Div_Grid_Movimientos.Visible = true;
                Chk_Autorizar.Checked = false;
                Hf_No_Movimiento_Egr.Value = String.Empty;
                Hf_No_Rechazado.Value = String.Empty;
                Hf_Estatus.Value = String.Empty;
            }
            catch (Exception ex)
            {
                throw new Exception("Inicializa_Controles " + ex.Message.ToString());
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Habilitar_Controles
        ///DESCRIPCIÓN          : metodo para habililitar los controles
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Habilitar_Controles(String Operacion)
        {
            try
            {
                switch (Operacion)
                {
                    case "Inicial":
                        Btn_Nuevo.ToolTip = "Nuevo";
                        Btn_Salir.ToolTip = "Salir";
                        Btn_Nuevo.Visible = true;
                        Btn_Nuevo.CausesValidation = false;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";

                        break;

                    case "Nuevo":
                        Btn_Nuevo.ToolTip = "Actualizar";
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Nuevo.Visible = true;
                        Btn_Nuevo.CausesValidation = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                        break;

                }
            }
            catch (Exception ex)
            {
                throw new Exception("Limpia_Controles " + ex.Message.ToString());
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Movimientos
        ///DESCRIPCIÓN          : Metodo para llenar el grid de Movimientos
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Llenar_Grid_Movimientos()
        {
            Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Consulta = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
            DataTable Dt_Movimiento; //Variable que obtendra los datos de la consulta 
            DataTable Dt_Tipo_Mod = new DataTable();
            DataRow Fila;
            Double Total = 0.00;
            DataTable Dt_Mov = new DataTable();

            try
            {
                Grid_Tipo_Modificacion.DataBind();
                if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
                {
                    Consulta.P_Tipo_Egreso = "RAMO33";
                    Consulta.P_Estatus = "'GENERADO'";
                }
                else
                {
                    Consulta.P_Tipo_Egreso = "MUNICIPAL";
                    Consulta.P_Estatus = "'RECIBIDO'";
                }

                Consulta.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();
                Consulta.P_No_Solicitud = String.Empty;
                Dt_Movimiento = Consulta.Consultar_Datos_Movimientos();

                if (Dt_Movimiento != null)
                {
                    if (Dt_Movimiento.Rows.Count > 0)
                    {
                        if (!String.IsNullOrEmpty(Txt_Busqueda.Text.Trim()))
                        {
                            Dt_Mov = (from Fila_Tot in Dt_Movimiento.AsEnumerable()
                                      where Fila_Tot.Field<string>("TIPO_OPERACION") == "ADECUACION"
                                      && (Convert.ToString(Fila_Tot.Field<String>("NO_SOLICITUD")).Contains(Txt_Busqueda.Text.Trim().ToUpper())
                                        || Convert.ToString(Fila_Tot.Field<Decimal>("IMPORTE")).Contains(Txt_Busqueda.Text.Trim())
                                        || Convert.ToString(Fila_Tot.Field<String>("FECHA_CREO")).Contains(Txt_Busqueda.Text.Trim().ToUpper())
                                        || Convert.ToString(Fila_Tot.Field<String>("USUARIO_CREO")).Contains(Txt_Busqueda.Text.Trim().ToUpper()))
                                      select Fila_Tot).AsDataView().ToTable();

                            Dt_Movimiento = Dt_Mov;
                        }
                        else 
                        {
                            //mostramos solo las adecuaciones
                            Dt_Mov = (from Fila_Mov in Dt_Movimiento.AsEnumerable()
                                      where Fila_Mov.Field<string>("TIPO_OPERACION") == "ADECUACION"
                                      select Fila_Mov).AsDataView().ToTable();

                            Dt_Movimiento = Dt_Mov;
                        }


                        Dt_Tipo_Mod.Columns.Add("TIPO_OPERACION", System.Type.GetType("System.String"));

                        foreach (DataRow Dr in Dt_Movimiento.Rows)
                        {
                            Total = Total + Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE"].ToString().Trim()) ? "0" : Dr["IMPORTE"].ToString().Trim());
                        }

                        Fila = Dt_Tipo_Mod.NewRow();
                        Fila["TIPO_OPERACION"] = "ADECUACION";
                        Dt_Tipo_Mod.Rows.Add(Fila);

                        Grid_Tipo_Modificacion.DataSource = Dt_Tipo_Mod;
                        Grid_Tipo_Modificacion.DataBind();
                    }
                }
                Txt_Total_Modificado.Text = "";
                Txt_Total_Modificado.Text = String.Format("{0:c}", Total);
            }
            catch (Exception ex)
            {
                throw new Exception("Movimiento Presupuestal" + ex.Message.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Datos_Usuario
        ///DESCRIPCIÓN          : Metodo para llenar los datos el usuario
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Mayo/2012
        ///MODIFICO             :Jennyfer Ivonne Ceja Lemus 
        ///FECHA_MODIFICO       :13/Agosto/2012
        ///CAUSA_MODIFICACIÓN   :Porque cuando la consulta que nos dice  si el usuario es del ramo33 
        ///                      trae una fila con campos nulls no entraba a ninguna condicion
        ///*******************************************************************************
        protected void Obtener_Datos_Usuario()
        {
            Cls_Ope_Psp_Clasificacion_Gasto_Negocio Negocio = new Cls_Ope_Psp_Clasificacion_Gasto_Negocio(); //conexion con la capa de negocios
            Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Mov_Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
            Boolean Administrador;
            DataTable Dt_Ramo33 = new DataTable();

            Hf_Tipo_Usuario.Value = "";

            try
            {
                //verificar si el empleado que esta logueado tiene el rol de administrador del presupuesto municipal y estatal
                Negocio.P_Rol_Id = Cls_Sessiones.Rol_ID.Trim();
                Administrador = Negocio.Consultar_Rol();

                if (Administrador)
                {
                    Hf_Tipo_Usuario.Value = "Administrador";
                }
                else
                {
                    //verificamos si el usuario logueado es el administrador de ramo33
                    Dt_Ramo33 = Mov_Negocio.Consultar_Usuario_Ramo33();
                    if (Dt_Ramo33 != null && Dt_Ramo33.Rows.Count > 0)
                    {
                        if (Dt_Ramo33.Rows[0][Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv].ToString().Trim().Equals(Cls_Sessiones.Empleado_ID.Trim())
                            || Dt_Ramo33.Rows[0][Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv_2].ToString().Trim().Equals(Cls_Sessiones.Empleado_ID.Trim()))
                        {
                            Hf_Tipo_Usuario.Value = "Ramo33";
                        }
                        else //SE AGREGO ESTE ELSE
                        {
                            Hf_Tipo_Usuario.Value = "Usuario";
                        }
                    }
                    else
                    {
                        Hf_Tipo_Usuario.Value = "Usuario";
                    }
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }

        //********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Atualizar_Datos
        ///DESCRIPCIÓN          : Metodo para actualizar los datos de autorizado o rechazado
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Actualizar_Datos()
        {
            Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
            Int32 Indice = 0;
            Boolean Autorizado;
            Boolean Rechazado;
            String Autorizados = String.Empty;
            Boolean Datos_Autorizar = false;

            try
            {
                foreach (GridViewRow Dr in Grid_Tipo_Modificacion.Rows)
                {
                    GridView Grid_Movimientos = (GridView)Dr.FindControl("Grid_Movimientos");

                    foreach (GridViewRow Renglon_Grid in Grid_Movimientos.Rows)
                    {
                        Indice++;
                        Grid_Movimientos.SelectedIndex = Indice;

                        Negocio.P_Estatus = "AUTORIZADO";
                        Autorizado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Autorizado")).Checked;
                        if (!Autorizado)
                        {
                            Rechazado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Rechazado")).Checked;
                            if (Rechazado)
                            {
                                Datos_Autorizar = true;
                            }
                        }
                        else
                        {
                            Datos_Autorizar = true;
                            Rechazado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Rechazado")).Checked;
                            if (Rechazado)
                            {
                                Datos_Autorizar = false;
                                Lbl_Mensaje_Error.Text = "Favor de solo Autorizar o Rechazar los movimientos, ya que existen movimientos que se seleccionaron ambas opciones";
                                Img_Error.Visible = true;
                                Lbl_Mensaje_Error.Visible = true;
                                return;
                            }
                        }
                    }
                }

                if (Datos_Autorizar)
                {
                    Negocio.P_Dt_Mov = Crear_Dt_Autorizados();
                    Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                    Negocio.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado.Trim();
                    Negocio.P_No_Solicitud = Hf_No_Movimiento_Egr.Value.Trim();
                    Negocio.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();
                    if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
                    {
                        Negocio.P_Tipo_Egreso = "RAMO33";
                    }
                    else
                    {
                        Negocio.P_Tipo_Egreso = "MUNICIPAL";
                    }

                    Autorizados = Negocio.Autorizacion_Adecuaciones();
                    if (Autorizados.Trim().Equals("SI"))
                    {
                        Inicializa_Controles();
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alta", "alert('Operacion Completa');", true);
                    }
                    else
                    {
                        Inicializa_Controles();
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alta", "alert('" + Autorizados + "');", true);
                    }
                }
                else 
                {
                    Lbl_Mensaje_Error.Text = "Favor de solo Autorizar o Rechazar los movimientos";
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al actualizar los datos. Error[" + ex.Message + "]");
            }
        }

        //********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Autorizados
        ///DESCRIPCIÓN          : Metodo para crear el datatable con los datos de la actualizacion
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private DataTable Crear_Dt_Autorizados()
        {
            DataTable Dt_Autorizados = new DataTable();
            Int32 Indice = 0;
            Boolean Autorizado;
            Boolean Rechazado;
            DataRow Fila;
            DataTable Dt_Comentarios = new DataTable();

            try
            {
                Dt_Comentarios = Obtener_Comentarios();
                Dt_Autorizados.Columns.Add("SOLICITUD_ID");
                Dt_Autorizados.Columns.Add("ESTATUS");
                Dt_Autorizados.Columns.Add("COMENTARIO");

                foreach (GridViewRow Dr in Grid_Tipo_Modificacion.Rows)
                {
                    GridView Grid_Movimientos = (GridView)Dr.FindControl("Grid_Movimientos");
                    foreach (GridViewRow Renglon_Grid in Grid_Movimientos.Rows)
                    {
                        Indice++;
                        Grid_Movimientos.SelectedIndex = Indice;

                        Autorizado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Autorizado")).Checked;
                        if (Autorizado)
                        {
                            Fila = Dt_Autorizados.NewRow();
                            Fila["SOLICITUD_ID"] = Renglon_Grid.Cells[1].Text.Trim();
                            Fila["ESTATUS"] = "AUTORIZADO";
                            Fila["COMENTARIO"] = String.Empty;
                            Dt_Autorizados.Rows.Add(Fila);
                        }
                        else
                        {
                            Rechazado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Rechazado")).Checked;
                            if (Rechazado)
                            {
                                Fila = Dt_Autorizados.NewRow();
                                Fila["SOLICITUD_ID"] = Renglon_Grid.Cells[1].Text.Trim();
                                Fila["ESTATUS"] = "RECHAZADO";
                                if (Dt_Comentarios != null && Dt_Comentarios.Rows.Count > 0)
                                {
                                    foreach (DataRow Dr_Com in Dt_Comentarios.Rows)
                                    {
                                        if (Dr_Com["SOLICITUD_ID"].ToString().Trim().Equals(Renglon_Grid.Cells[1].Text.Trim()))
                                        {
                                            Fila["COMENTARIO"] = Dr_Com["COMENTARIO"].ToString().Trim();
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    Fila["COMENTARIO"] = String.Empty;
                                }
                                Dt_Autorizados.Rows.Add(Fila);
                            }
                        }
                    }
                }
                return Dt_Autorizados;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al actualizar los datos. Error[" + ex.Message + "]");
            }
        }

        //********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Comentarios
        ///DESCRIPCIÓN          : Metodo para crear el datatable con los comentarios
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private DataTable Obtener_Comentarios()
        {
            String Comentarios = String.Empty;
            DataTable Dt_Comentarios = new DataTable();
            DataRow Fila;
            String[] Coment;
            String[] Comenta;

            try
            {
                Comentarios = Hf_No_Rechazado.Value.Trim();
                Dt_Comentarios.Columns.Add("SOLICITUD_ID");
                Dt_Comentarios.Columns.Add("COMENTARIO");

                if (!String.IsNullOrEmpty(Comentarios))
                {
                    Coment = Comentarios.Split('-');

                    for (int i = 0; i <= Coment.Length - 2; i++)
                    {
                        Comenta = Coment[i].Split(';');
                        Fila = Dt_Comentarios.NewRow();
                        Fila["SOLICITUD_ID"] = Comenta[0].ToString().Trim();
                        Fila["COMENTARIO"] = Comenta[1].ToString().Trim();
                        Dt_Comentarios.Rows.Add(Fila);
                    }
                }

                return Dt_Comentarios;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al actualizar los datos. Error[" + ex.Message + "]");
            }
        }
    #endregion

    #region(Eventos)
        #region(Botones)
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Nuevo_Click
            ///DESCRIPCIÓN          : Evento del boton de Actualizar
            ///PARAMETROS           :    
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
            {
                Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
                DataTable Dt_Mov = new DataTable();

                try
                {
                    Lbl_Mensaje_Error.Visible = false;
                    Img_Error.Visible = false;
                    Lbl_Mensaje_Error.Text = "";

                    if (Btn_Nuevo.ToolTip == "Actualizar")
                    {
                        Actualizar_Datos();
                    }
                }

                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }

            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Nuevo_Click
            ///DESCRIPCIÓN          : Evento del boton de salir
            ///PARAMETROS           :    
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
            {
                try
                {
                    if (Btn_Salir.ToolTip == "Cancelar")
                    {
                        Inicializa_Controles(); //Habilita los controles para la siguiente operación del usuario en el catálogo
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Buscar_Click
            ///DESCRIPCIÓN          : Evento del boton Buscar 
            ///PARAMETROS           :    
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Febrero/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
            {
                try
                {
                    Llenar_Grid_Movimientos();
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error en la busqueda de movimientos. Error[" + Ex.Message + "]");
                }
            }
        #endregion

        #region (Grid)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Movimientos_RowDataBound
            ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Grid_Movimientos_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Consulta = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
                DataTable Dt_Movimientos_Detalles = new DataTable();
                DataTable Dt_Movimientos = new DataTable();

                try
                {
                    GridView Grid_Movimientos_Datos = (GridView)e.Row.Cells[0].FindControl("Grid_Movimientos_Datos");
                    String Solicitud_ID = String.Empty;

                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        Solicitud_ID = e.Row.Cells[1].Text.Trim();
                        Consulta.P_No_Solicitud = Solicitud_ID;
                        Dt_Movimientos_Detalles = Consulta.Consultar_Datos_Movimientos_Det();

                        if (Dt_Movimientos_Detalles != null)
                        {
                            if (Dt_Movimientos_Detalles.Rows.Count > 0)
                            {
                                Dt_Movimientos = (from Fila in Dt_Movimientos_Detalles.AsEnumerable()
                                                  where Fila.Field<String>("TIPO_PARTIDA").Trim() == "Origen"
                                                  select Fila).AsDataView().ToTable();

                                Dt_Movimientos_Detalles = Dt_Movimientos;
                            }
                        }

                        Grid_Movimientos_Datos.Columns[0].Visible = true;
                        Grid_Movimientos_Datos.DataSource = Dt_Movimientos_Detalles;
                        Grid_Movimientos_Datos.DataBind();
                        Grid_Movimientos_Datos.Columns[0].Visible = false;
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error:[" + Ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Movimientos_RowDataBound
            ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Grid_Movimientos_Datos_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Consulta = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
                DataTable Dt_Movimientos = new DataTable();
                DataTable Dt_Movimientos_Detalles = new DataTable();
                String Solicitud_ID = String.Empty;
                String FF = String.Empty;
                String PP = String.Empty;
                String UR = String.Empty;
                String P = String.Empty;

                try
                {
                    GridView Grid_Movimientos_Detalle = (GridView)e.Row.Cells[0].FindControl("Grid_Movimientos_Detalle");
                    

                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        Solicitud_ID = e.Row.Cells[0].Text.Trim();
                        Consulta.P_No_Solicitud = Solicitud_ID;
                        Dt_Movimientos_Detalles = Consulta.Consultar_Datos_Movimientos_Det();

                        //obtenemos solo los correspondientes a la primera agrupacion
                        FF = e.Row.Cells[1].Text.Trim();
                        PP = e.Row.Cells[2].Text.Trim();
                        UR = HttpUtility.HtmlDecode(e.Row.Cells[3].Text.Trim());
                        P = HttpUtility.HtmlDecode(e.Row.Cells[4].Text.Trim());

                        if (Dt_Movimientos_Detalles != null)
                        {
                            if (Dt_Movimientos_Detalles.Rows.Count > 0)
                            {
                                Dt_Movimientos = (from Fila in Dt_Movimientos_Detalles.AsEnumerable()
                                                  where Fila.Field<String>("FF").Trim() == FF.Trim()
                                                  && Fila.Field<String>("PP").Trim() == PP.Trim()
                                                  && Fila.Field<String>("UR").Trim() == UR.Trim()
                                                  && Fila.Field<String>("PARTIDA").Trim() == P.Trim()
                                                  select Fila).AsDataView().ToTable();

                                Dt_Movimientos_Detalles = Dt_Movimientos;
                            }
                        }

                        Grid_Movimientos_Detalle.Columns[0].Visible = true;
                        Grid_Movimientos_Detalle.Columns[14].Visible = true;
                        Grid_Movimientos_Detalle.DataSource = Dt_Movimientos_Detalles;
                        Grid_Movimientos_Detalle.DataBind();
                        Grid_Movimientos_Detalle.Columns[0].Visible = false;
                        Grid_Movimientos_Detalle.Columns[14].Visible = false;

                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error:[" + Ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Movimientos_RowDataBound
            ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Grid_Movimientos_Detalles_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                Double Ene = 0.00;
                Double Feb = 0.00;
                Double Mar = 0.00;
                Double Abr = 0.00;
                Double May = 0.00;
                Double Jun = 0.00;
                Double Jul = 0.00;
                Double Ago = 0.00;
                Double Sep = 0.00;
                Double Oct = 0.00;
                Double Nov = 0.00;
                Double Dic = 0.00;

                try
                {
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        Ene = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[1].Text.Trim()) ? "0" : e.Row.Cells[1].Text.Trim());
                        Feb = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[2].Text.Trim()) ? "0" : e.Row.Cells[2].Text.Trim());
                        Mar = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[3].Text.Trim()) ? "0" : e.Row.Cells[3].Text.Trim());
                        Abr = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[4].Text.Trim()) ? "0" : e.Row.Cells[4].Text.Trim());
                        May = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[5].Text.Trim()) ? "0" : e.Row.Cells[5].Text.Trim());
                        Jun = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[6].Text.Trim()) ? "0" : e.Row.Cells[6].Text.Trim());
                        Jul = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[7].Text.Trim()) ? "0" : e.Row.Cells[7].Text.Trim());
                        Ago = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[8].Text.Trim()) ? "0" : e.Row.Cells[8].Text.Trim());
                        Sep = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[9].Text.Trim()) ? "0" : e.Row.Cells[9].Text.Trim());
                        Oct = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[10].Text.Trim()) ? "0" : e.Row.Cells[10].Text.Trim());
                        Nov = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[11].Text.Trim()) ? "0" : e.Row.Cells[11].Text.Trim());
                        Dic = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[12].Text.Trim()) ? "0" : e.Row.Cells[12].Text.Trim());

                        if (Ene > 0)
                        {
                            e.Row.Cells[1].Style.Add("font-weight", "bold");
                        }

                        if (Feb > 0)
                        {
                            e.Row.Cells[2].Style.Add("font-weight", "bold");
                        }

                        if (Mar > 0)
                        {
                            e.Row.Cells[3].Style.Add("font-weight", "bold");
                        }

                        if (Abr > 0)
                        {
                            e.Row.Cells[4].Style.Add("font-weight", "bold");
                        }

                        if (May > 0)
                        {
                            e.Row.Cells[5].Style.Add("font-weight", "bold");
                        }

                        if (Jun > 0)
                        {
                            e.Row.Cells[6].Style.Add("font-weight", "bold");
                        }

                        if (Jul > 0)
                        {
                            e.Row.Cells[7].Style.Add("font-weight", "bold");
                        }

                        if (Ago > 0)
                        {
                            e.Row.Cells[8].Style.Add("font-weight", "bold");
                        }

                        if (Sep > 0)
                        {
                            e.Row.Cells[9].Style.Add("font-weight", "bold");
                        }

                        if (Oct > 0)
                        {
                            e.Row.Cells[10].Style.Add("font-weight", "bold");
                        }

                        if (Nov > 0)
                        {
                            e.Row.Cells[11].Style.Add("font-weight", "bold");
                        }

                        if (Dic > 0)
                        {
                            e.Row.Cells[12].Style.Add("font-weight", "bold");
                        }
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error:[" + Ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Movimientos_RowCreated
            ///DESCRIPCIÓN          : Evento del grid del movimiento del cursor sobre los registros
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Grid_Movimientos_RowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#507CD1';this.style.color='#FFFFFF'");
                    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;this.style.color=this.originalstyle;");
                }
            }
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Tipo_Modificacion_RowDataBound
            ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Grid_Tipo_Modificacion_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Consulta = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
                DataTable Dt_Mov = new DataTable();
                DataTable Dt_Movimiento = new DataTable();

                try
                {
                    GridView Grid_Movimientos = (GridView)e.Row.Cells[0].FindControl("Grid_Movimientos");
                    String Tipo_Operacion = String.Empty;

                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {

                        if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
                        {
                            Consulta.P_Tipo_Egreso = "RAMO33";
                            Consulta.P_Estatus = "'GENERADO'";
                        }
                        else
                        {
                            Consulta.P_Tipo_Egreso = "MUNICIPAL";
                            Consulta.P_Estatus = "'RECIBIDO'";
                        }
                        Consulta.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();
                        Consulta.P_No_Solicitud = String.Empty;
                        Tipo_Operacion = e.Row.Cells[0].Text.Trim();
                        Consulta.P_Tipo_Solicitud = Tipo_Operacion;
                        Dt_Movimiento = Consulta.Consultar_Datos_Movimientos_Tipo();

                        if (!String.IsNullOrEmpty(Txt_Busqueda.Text.Trim()))
                        {
                            if (Dt_Movimiento != null)
                            {
                                if (Dt_Movimiento.Rows.Count > 0)
                                {
                                    Dt_Mov = (from Fila in Dt_Movimiento.AsEnumerable()
                                              where (
                                                Convert.ToString(Fila.Field<String>("NO_SOLICITUD")).Contains(Txt_Busqueda.Text.Trim().ToUpper())
                                                || Convert.ToString(Fila.Field<Decimal>("IMPORTE")).Contains(Txt_Busqueda.Text.Trim())
                                                || Convert.ToString(Fila.Field<String>("TIPO_OPERACION")).Contains(Txt_Busqueda.Text.Trim().ToUpper())
                                                || Convert.ToString(Fila.Field<String>("FECHA_CREO")).Contains(Txt_Busqueda.Text.Trim().ToUpper())
                                                || Convert.ToString(Fila.Field<String>("ESTATUS")).Contains(Txt_Busqueda.Text.Trim().ToUpper())
                                                || Convert.ToString(Fila.Field<String>("USUARIO_CREO")).Contains(Txt_Busqueda.Text.Trim().ToUpper()))
                                              select Fila).AsDataView().ToTable();

                                    Dt_Movimiento = Dt_Mov;
                                }
                            }
                        }

                        Grid_Movimientos.Columns[1].Visible = true;
                        Grid_Movimientos.Columns[3].Visible = true;
                        Grid_Movimientos.Columns[5].Visible = true;
                        Grid_Movimientos.DataSource = Dt_Movimiento;
                        Grid_Movimientos.DataBind();
                        Grid_Movimientos.Columns[1].Visible = false;
                        Grid_Movimientos.Columns[3].Visible = false;
                        Grid_Movimientos.Columns[5].Visible = false;

                        Chk_Autorizar.Visible = true;
                        Lbl_Autorizacion.Visible = true;
                        Chk_Autorizar.Checked = false;
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error:[" + Ex.Message + "]");
                }
            }
        #endregion
    #endregion
}
