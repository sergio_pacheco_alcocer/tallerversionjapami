﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" 
CodeFile="Frm_Rpt_Psp_Reporte_Justificacion_Movimientos_Egresos.aspx.cs" 
Inherits="paginas_Presupuestos_Frm_Rpt_Psp_Reporte_Justificacion_Movimientos_Egresos" Title="SIAC " %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <style type="text/css">
         .button_autorizar2{
            margin:0 7px 0 0;
            background-color:#f5f5f5;
            border:1px solid #dedede;
            border-top:1px solid #eee;
            border-left:1px solid #eee;

            font-family:"Lucida Grande", Tahoma, Arial, Verdana, sans-serif;
            font-size:100%;    
            line-height:130%;
            text-decoration:none;
            font-weight:bold;
            color:#565656;
            cursor:pointer;
            padding:5px 10px 6px 7px; /* Links */
            width:99%;
        }
    </style>
    <script type="text/javascript">
        //<--
            //El nombre del controlador que mantiene la sesión
            var CONTROLADOR = "../../Mantenedor_Session.ashx";

            //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
            function MantenSesion() {
                var head = document.getElementsByTagName('head').item(0);
                script = document.createElement('script');
                script.src = CONTROLADOR;
                script.setAttribute('type', 'text/javascript');
                script.defer = true;
                head.appendChild(script);
            }

            //Temporizador para matener la sesión activa
            setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);
        //-->
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server"  >
        <ContentTemplate>  
            
            <asp:UpdateProgress ID="Uprg_Reloj_Checador" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            
            <div id="Div_Reporte_Dependencia" style="background-color:#ffffff; width:100%; height:100%;">    
                 <table width="100%" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">Reporte Movimientos</td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="98%"  border="0" cellspacing="0">
                    <tr align="center">
                        <td colspan="2">                
                            <div align="right" class="barra_busqueda">
                                <table style="width:100%;height:28px;">
                                    <tr>
                                        <td align="left" style="width:59%;">
                                            &nbsp;
                                           <asp:ImageButton ID="Btn_Reporte_Excel" runat="server" onclick="Btn_Reporte_Excel_Click"
                                                ToolTip="Generar Reporte Excel"  Width = "25px" Height = "25px"
                                                ImageUrl="~/paginas/imagenes/paginas/microsoft_office2003_excel.png"/>
                                            &nbsp;
                                            <asp:ImageButton ID="Btn_Reporte_Pdf" runat="server" onclick="Btn_Reporte_Pdf_Click"
                                                ToolTip="Generar Reporte Pdf"  Width = "25px" Height = "25px"
                                                ImageUrl="~/paginas/imagenes/paginas/adobe_acrobat_professional.png"/>
                                            &nbsp;
                                            <asp:ImageButton ID="Btn_Reporte_Word" runat="server" onclick="Btn_Reporte_Word_Click"
                                                ToolTip="Generar Reporte Word"  Width = "25px" Height = "25px"
                                                ImageUrl="~/paginas/imagenes/paginas/microsoft_office2003_word.png"/>
                                            &nbsp;
                                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" 
                                                CssClass="Img_Button" TabIndex="2"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                                onclick="Btn_Salir_Click"/>
                                        </td>
                                      <td align="right" style="width:41%;">
                                            &nbsp;
                                      </td>       
                                    </tr>         
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>  
                <br /> 
                <table width="98%" class="estilo_fuente">
                    <tr>
                        <td style="width:15%;text-align:left;cursor:default;" class="button_autorizar2"> 
                            Presupuesto
                        </td>
                        <td colspan = "3" style="text-align:left;cursor:default;" class="button_autorizar2">
                            <asp:RadioButtonList runat = "server" ID="Rbl_Presupuesto_ID" AutoPostBack= "true" 
                                OnSelectedIndexChanged = "Rbl_Presupuesto_ID_CheckedChanged" >
                                <asp:ListItem Text="Egresos" Value="Egresos" Selected></asp:ListItem>
                                <asp:ListItem Text="Ingresos" Value="Ingresos"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:15%;text-align:left;cursor:default;" class="button_autorizar2"> 
                            Año
                        </td>
                        <td style="width:25%;text-align:left;cursor:default;" class="button_autorizar2">
                            <asp:DropDownList ID="Cmb_Anio" runat="server" Width="98%" TabIndex="2"
                            AutoPostBack="true" OnSelectedIndexChanged="Cmb_Anio_SelectedIndexChanged" ></asp:DropDownList>
                        </td>
                        <td style="width:25%;text-align:left; cursor:default;" class="button_autorizar2"> 
                            No. Modificación
                        </td>
                        <td style="width:35%;text-align:left;cursor:default;" class="button_autorizar2">
                            <asp:DropDownList ID="Cmb_Tipo_Reporte" runat="server" Width="98%" TabIndex="1"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="Btn_Reporte_Excel" />
            <asp:PostBackTrigger ControlID="Btn_Reporte_Word" />--%>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
