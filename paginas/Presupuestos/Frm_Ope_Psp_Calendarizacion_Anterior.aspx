﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Frm_Ope_Psp_Calendarizacion_Anterior.aspx.cs" 
Inherits="paginas_Presupuestos_Frm_Ope_Psp_Calendarizacion_Anterior" Title="SIAC Sistema Integral Administrativo y Comercial"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Presupuesto Anterior</title>
    <link href="../estilos/estilo_paginas.css" rel="stylesheet" type="text/css" />
    <link href="../estilos/estilo_masterpage.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" >
        function Grid_Anidado(Control, Fila)
        {
            var div = document.getElementById(Control); 
            var img = document.getElementById('img' + Control);
            
            if (div.style.display == "none") 
            {
                div.style.display = "inline";
                if (Fila == 'alt') {
                    img.src = "../imagenes/paginas/stocks_indicator_down.png";
                }
                else {
                    img.src = "../imagenes/paginas/stocks_indicator_down.png";
                }
                img.alt = "Contraer Registros";
            }
            else 
            {
                div.style.display = "none";
                if (Fila == 'alt') {
                    img.src = "../imagenes/paginas/add_up.png";
                }
                else {
                    img.src = "../imagenes/paginas/add_up.png";
                }
                img.alt = "Expandir Registros";
            }
        }
    </script>
</head>
<body>
    <form id="Form" runat="server">
        <div style="min-height:580px; height:auto; width:99%;vertical-align:top;border-style:outset;border-color: Silver; background-color:White;">
            <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
                EnableScriptLocalization="true">
            </asp:ScriptManager>    
            <div>
                <asp:UpdatePanel ID="Upl_Contenedor" runat="server">
                    <ContentTemplate>
                         <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upl_Contenedor"
                            DisplayAfter="0">
                            <ProgressTemplate>
                                <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                </div>
                                <div class="processMessage" id="div_progress">
                                    <img alt="" src="../Imagenes/paginas/Updating.gif" />
                                </div>
                            </ProgressTemplate>                    
                        </asp:UpdateProgress>
                        <center>
                        <asp:HiddenField ID="Hf_Tipo_Calendario" runat="server" />
                        <asp:HiddenField ID="Hf_Anio" runat="server" />
                        <asp:HiddenField ID="Hf_Dependencia_ID" runat="server" />
                        <asp:HiddenField ID="Hf_Empleado_ID" runat="server" />
                        <table style="width:100%;">
                           <tr>
                                <td>
                                    <asp:Panel ID="Pnl_Partidas_Asignadas" runat="server" GroupingText="Presupuesto Anterior" Width="99%">
                                        <div style="width:100%; ">
                                            <table style="width:100%;">
                                                <tr>
                                                    <td style="width:100%;">
                                                      <asp:GridView ID="Grid_Partida_Asignada" runat="server" style="white-space:normal;"
                                                        AutoGenerateColumns="False" GridLines="None" 
                                                        Width="100%"  EmptyDataText="No se encontraron partidas aignadas" 
                                                        CssClass="GridView_1" HeaderStyle-CssClass="tblHead"
                                                        DataKeyNames="PARTIDA_ID"   OnRowDataBound="Grid_Partida_Asignada_RowDataBound"
                                                        OnRowCreated="Grid_Partidas_Asignadas_Detalle_RowCreated">
                                                        <Columns>
                                                            <asp:TemplateField> 
                                                              <ItemTemplate> 
                                                                    <a href="javascript:Grid_Anidado('div<%# Eval("PARTIDA_ID") %>', 'one');"> 
                                                                         <img id="imgdiv<%# Eval("PARTIDA_ID") %>" alt="Click expander/contraer registros" border="0" src="../imagenes/paginas/stocks_indicator_down.png" /> 
                                                                    </a> 
                                                              </ItemTemplate> 
                                                              <AlternatingItemTemplate> 
                                                                   <a href="javascript:Grid_Anidado('div<%# Eval("PARTIDA_ID") %>', 'alt');"> 
                                                                        <img id="imgdiv<%# Eval("PARTIDA_ID") %>" alt="Click expander/contraer registros" border="0" src="../imagenes/paginas/stocks_indicator_down.png" /> 
                                                                   </a> 
                                                              </AlternatingItemTemplate> 
                                                              <ItemStyle HorizontalAlign ="Center" Font-Size="X-Small" Width="3%" />
                                                            </asp:TemplateField>      
                                                            <asp:BoundField DataField="PARTIDA_ID" />
                                                            <asp:BoundField DataField="CLAVE" HeaderText="Partida" >
                                                                <HeaderStyle HorizontalAlign="Left"/>
                                                                <ItemStyle HorizontalAlign="Left" Width="6%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TOTAL_ENE" HeaderText="Enero" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Center"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="XX-Small" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TOTAL_FEB" HeaderText="Febrero" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Center"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TOTAL_MAR" HeaderText="Marzo" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Center"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TOTAL_ABR" HeaderText="Abril" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Center"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TOTAL_MAY" HeaderText="Mayo" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Center"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TOTAL_JUN" HeaderText="Junio" DataFormatString="{0:#,###,##0.00}">
                                                               <HeaderStyle HorizontalAlign="Center"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TOTAL_JUL" HeaderText="Julio" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Center"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TOTAL_AGO" HeaderText="Agosto" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Center"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TOTAL_SEP" HeaderText="Septiembre" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Center"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TOTAL_OCT" HeaderText="Octubre" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Center"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>                 
                                                            <asp:BoundField DataField="TOTAL_NOV" HeaderText="Noviembre" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Center"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>                   
                                                            <asp:BoundField DataField="TOTAL_DIC" HeaderText="Diciembre" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Center"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>                
                                                            <asp:BoundField DataField="TOTAL" HeaderText="Total" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Center"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="8%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>
                                                            <asp:TemplateField>
                                                               <ItemTemplate>
                                                                 </td>
                                                                 </tr> 
                                                                 <tr>
                                                                  <td colspan="100%">
                                                                   <div id="div<%# Eval("PARTIDA_ID") %>" style="display:inline;position:relative;left:20px;" >                             
                                                                       <asp:GridView ID="Grid_Partidas_Asignadas_Detalle" runat="server" style="white-space:normal;"
                                                                           CssClass="GridView_Nested" HeaderStyle-CssClass="tblHead"
                                                                           AutoGenerateColumns="false" GridLines="None" Width="98%"
                                                                           OnRowCreated="Grid_Partidas_Asignadas_Detalle_RowCreated">
                                                                           <Columns>
                                                                                <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                                                    ImageUrl="~/paginas/imagenes/paginas/Select_Grid_Inner.png">
                                                                                    <ItemStyle Width="3%" />
                                                                                </asp:ButtonField> 
                                                                                <asp:BoundField DataField="DEPENDENCIA_ID"  />
                                                                                <asp:BoundField DataField="PROYECTO_ID"  />
                                                                                <asp:BoundField DataField="CAPITULO_ID"  />
                                                                                <asp:BoundField DataField="PARTIDA_ID"  />
                                                                                <asp:BoundField DataField="PRODUCTO_ID"  />
                                                                                <asp:BoundField DataField="PRECIO"  />
                                                                                <asp:BoundField DataField="JUSTIFICACION"  />
                                                                                <asp:BoundField DataField="CLAVE_PARTIDA" HeaderText="Partida" />
                                                                                <asp:BoundField DataField="UR" HeaderText="Unidad Responsable" >
                                                                                    <HeaderStyle HorizontalAlign="Left" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Left" Width="14%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField> 
                                                                                <asp:BoundField DataField="CLAVE_PRODUCTO" HeaderText="Clave Producto" >
                                                                                    <HeaderStyle HorizontalAlign="Left" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Left" Width="9%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>                                                                          
                                                                                <asp:BoundField DataField="ENERO" HeaderText="Enero" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="FEBRERO" HeaderText="Febrero" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="MARZO" HeaderText="Marzo" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="ABRIL" HeaderText="Abril" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="MAYO" HeaderText="Mayo" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="JUNIO" HeaderText="Junio" DataFormatString="{0:#,###,##0.00}">
                                                                                   <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="JULIO" HeaderText="Julio" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="AGOSTO" HeaderText="Agosto" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="SEPTIEMBRE" HeaderText="Septiembre" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="OCTUBRE" HeaderText="Octubre" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>                 
                                                                                <asp:BoundField DataField="NOVIEMBRE" HeaderText="Noviembre" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>                   
                                                                                <asp:BoundField DataField="DICIEMBRE" HeaderText="Diciembre" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>                
                                                                                <asp:BoundField DataField="IMPORTE_TOTAL" HeaderText="Total" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="8%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <%--<asp:ImageButton ID="Btn_Eliminar" runat="server" 
                                                                                            ImageUrl="~/paginas/imagenes/gridview/tecnico.png" 
                                                                                            onclick="Btn_Eliminar_Click" CommandArgument='<%#Eval("ID")%>'/>--%>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle HorizontalAlign ="Center" Font-Size="X-Small" Width="3%" />
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="ID" />
                                                                           </Columns>
                                                                           <SelectedRowStyle CssClass="GridSelected_Nested" />
                                                                           <PagerStyle CssClass="GridHeader_Nested" />
                                                                           <HeaderStyle CssClass="GridHeader_Nested" />
                                                                           <AlternatingRowStyle CssClass="GridAltItem_Nested" /> 
                                                                       </asp:GridView>
                                                                   </div>
                                                                  </td>
                                                                 </tr>
                                                               </ItemTemplate>
                                                            </asp:TemplateField>
                                                         </Columns>
                                                         <SelectedRowStyle CssClass="GridSelected" />
                                                            <PagerStyle CssClass="GridHeader" />
                                                            <HeaderStyle CssClass="GridHeader" />
                                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                                      </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                        <table style="width:100%;">
                            <tr>
                                <td class="barra_busqueda" style="height:1px;">                            
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:right;">
                                    <asp:Label ID="Lbl_Total_Ajuste" runat="server" Text="Total" style="font-size:small; font-weight:bold;"></asp:Label>
                                    &nbsp;<asp:TextBox ID="Txt_Total_Ajuste" runat="server" style="text-align:right; border-color:Navy;" ReadOnly="true"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                       </center>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </form>
</body>
</html>
