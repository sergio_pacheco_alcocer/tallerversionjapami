﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" 
CodeFile="Frm_Psp_Ayudante_Rol_Empleado.aspx.cs" Inherits="paginas_Presupuestos_Frm_Psp_Ayudante_Rol_Empleado" 
Title="SIAC Sistema Integral Administrativo y Comercial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server"  EnableScriptGlobalization="true" EnableScriptLocalization = "true"/>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                    DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                        </div>
                        <div id="div_progress" class="processMessage" >
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
             
                
             <div id="Div_Fuentes_Financiamiento" style="background-color:#ffffff; width:100%; height:100%;">
                    
                <table width="100%" class="estilo_fuente">
                    <tr align="center">
                        <td colspan="4" class="label_titulo">
                           ROL EMPLEADO
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
               </table>             
            
               <table width="100%"  border="0" cellspacing="0">
                 <tr align="center">
                     <td colspan="2">                
                         <div  align="right" style="width:99%; background-color: #2F4E7D; color: #FFFFFF; font-weight: bold; font-style: normal; font-variant: normal; font-family: fantasy; height:32px"  >                        
                             
                        </div>
                     </td>
                 </tr>
             </table> 
            <center>
             <div id="Div_Datos" runat="server">
                <br />
                <div ID="Div_Partida_Origen" runat="server"  style="width:97%;vertical-align:top;" >
                     <asp:Panel ID="Panel2" runat="server" GroupingText="">
                        <table width="100%"   border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                                <td style="width:15%"> 
                                    <asp:Label ID="lbl_Unidad_Responsable_Origen" runat="server" Text="Unidad Responsable" Width="100%"  
                                    ></asp:Label>
                                </td>
                                <td style="width:85%">
                                    <asp:DropDownList ID="Cmb_Unidad_Responsable" runat="server" Width="99%" AutoPostBack="true" 
                                    OnSelectedIndexChanged="Cmb_Unidad_Responsable_SelectedIndexChanged"></asp:DropDownList>
                                    <asp:HiddenField id="Empleado_id" runat="server"/>
                                </td>
                            </tr>
                            <tr>
                                <td > 
                                    <asp:Label ID="Label1" runat="server" Text="Rol_ID" Width="100%"  
                                    ></asp:Label>
                                </td>
                                <td >
                                    <asp:TextBox ID="Txt_Rol_id" runat="server" Width="50%"></asp:TextBox>
                                    <asp:HiddenField  ID = "Hf_Empleado_Id" runat="server"/>
                                    <asp:Button ID="Btn_Psp_Modificado_2012" runat="server" Text="Modificar" Width="30%" 
                                        onclick="Btn_Modificar_Click"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div ID="Div_Partidas" runat="server" style="width:99%; height:400px; overflow:auto; vertical-align:top;">
                                        <asp:GridView ID="Grid_Empleado" runat="server"  CssClass="GridView_1" Width="97%" 
                                            AutoGenerateColumns="False"  GridLines="None" 
                                            HeaderStyle-CssClass="tblHead" 
                                            EmptyDataText="No se encuentraron registros"
                                            OnSelectedIndexChanged = "Grid_Empleados_SelectedIndexChanged"
                                            >
                                            <Columns>
                                                <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                    ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                    <ItemStyle Width="3%" />
                                                </asp:ButtonField>
                                                <asp:BoundField DataField="Empleado" HeaderText="Nombre" >
                                                    <HeaderStyle HorizontalAlign="Left" Width="39%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="39%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="NO_EMPLEADO" HeaderText="No_Empleado" >
                                                    <HeaderStyle HorizontalAlign="Left" Width="13%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="13%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PASSWORD" HeaderText="Password" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ROL_ID" HeaderText="Rol_id" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EMPLEADO_ID"/>
                                            </Columns>
                                            <SelectedRowStyle CssClass="GridSelected" />
                                            <PagerStyle CssClass="GridHeader" />
                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                        </table>
                     </asp:Panel>
                </div>   
             </div>
            </center>
         </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

