﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" 
CodeFile="Frm_Ope_Autorizar_Traspaso_Presupuestal.aspx.cs" Inherits="paginas_presupuestos_Frm_Ope_Autorizar_Traspaso_Presupuestal" 
Title="SIAC Sistema Integral Administrativo y Comercial"%>
<%@ Register Assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <link href="../../easyui/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../../easyui/themes/icon.css" rel="stylesheet" type="text/css" />
    <script src="../../easyui/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../../easyui/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="../../javascript/validacion/jquery.timers.js" type="text/javascript"></script>
    
    <script type="text/javascript" >
        function Grid_Anidado(Control, Fila)
        {
            var div = document.getElementById(Control); 
            var img = document.getElementById('img' + Control);
            
            if (div.style.display == "none") 
            {
                div.style.display = "inline";
                if (Fila == 'alt') {
                    img.src = "../imagenes/paginas/stocks_indicator_down.png";
                }
                else {
                    img.src = "../imagenes/paginas/stocks_indicator_down.png";
                }
                img.alt = "Contraer Registros";
            }
            else 
            {
                div.style.display = "none";
                if (Fila == 'alt') {
                    img.src = "../imagenes/paginas/add_up.png";
                }
                else {
                    img.src = "../imagenes/paginas/add_up.png";
                }
                img.alt = "Expandir Registros";
            }
        }
        
        function Rechazado(Chk)
        {
            var Rechazado= $('[id$=Hf_No_Rechazado]').val();
            if($(Chk).attr('checked'))
            {
                var id=$(Chk).parent().attr('class');
                $.messager.prompt('Rechazado', 'Comentario', function(Aceptar){
				        if (Aceptar){
				            Rechazado += id+";"+ Aceptar+"-";
				             $('[id$=Hf_No_Rechazado]').val(Rechazado);
				        }
				        else{
				            $(Chk).attr("checked", false);
				        }
			    });
            }
        }
        
        function Aceptacion_Masiva(Chk)
        {
            var $chkBox = $("input:checkbox[id$=Chk_Aceptar]");
            if($(Chk).attr('checked')){
                $chkBox.attr("checked", true);
            }
            else{
                $chkBox.attr("checked", false);
            }
        }
        
         function Autorizacion_Masiva(Chk)
        {
            var $chkBox = $("input:checkbox[id$=Chk_Autorizado]");
            var $chkBoxPre = $("input:checkbox[id$=Chk_PreAutorizado]");
            if($(Chk).attr('checked')){
                $chkBox.attr("checked", true);
                 $chkBoxPre.attr("checked", true);
            }
            else{
                $chkBox.attr("checked", false);
                $chkBoxPre.attr("checked", false);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager1" runat="server"  EnableScriptGlobalization="true" EnableScriptLocalization = "true"/>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                    DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                        </div>
                        <div id="div_progress" class="processMessage" >
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
             
                
             <div id="Div_Fuentes_Financiamiento" style="background-color:#ffffff; width:100%; height:100%;">
                    
                <table width="100%" class="estilo_fuente">
                    <tr align="center">
                        <td colspan="4" class="label_titulo">
                           Autorización  de Movimiento de Presupuesto
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
               </table>             
            
               <table width="100%"  border="0" cellspacing="0">
                 <tr align="center">
                     <td colspan="2">                
                         <div  align="right" style="width:99%; background-color: #2F4E7D; color: #FFFFFF; font-weight: bold; font-style: normal; font-variant: normal; font-family: fantasy; height:32px"  >                        
                              <table style="width:100%;height:28px;">
                                <tr>
                                  <td align="left" style="width:59%;">  
                                        <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" CssClass="Img_Button" TabIndex="1"
                                            ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" onclick="Btn_Nuevo_Click" />
                                        <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button" TabIndex="4"
                                            ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" onclick="Btn_Salir_Click"/>
                                  </td>
                                  <td align="right" style="width:41%;">
                                    <table style="width: 100%; height: 28px;" runat="server" id="Tabla_Busqueda">
                                        <tr>
                                            <td style="vertical-align: middle; text-align: right; width: 20%;">
                                                B&uacute;squeda:
                                            </td>
                                            <td style="width: 55%;">
                                                <asp:TextBox ID="Txt_Busqueda" runat="server" Width="200px" MaxLength="100"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Rol_ID" runat="server" WatermarkCssClass="watermarked"
                                                    WatermarkText="<Ingrese Filtro>" TargetControlID="Txt_Busqueda" />
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda" runat="server" TargetControlID="Txt_Busqueda"
                                                    FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ./ " />
                                            </td>
                                            <td style="vertical-align: middle; width: 5%;">
                                                <asp:ImageButton ID="Btn_Buscar" runat="server" ToolTip="Consultar" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                                    OnClick="Btn_Buscar_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                   </td>
                                 </tr>
                              </table>
                        </div>
                     </td>
                 </tr>
             </table> 
             <br />
            <center>
             <div id="Div_Grid_Modificaciones" runat = "server" style="width:98%;">
                <asp:HiddenField id="Hf_Tipo_Usuario" runat="server"/>
                <asp:HiddenField id="Hf_Estatus" runat="server"/>
                <asp:HiddenField id="Hf_No_Movimiento_Egr" runat="server"/>
                <asp:HiddenField id="Hf_No_Rechazado" runat = "server"/>
                
                <asp:GridView ID="Grid_Modificaciones" runat="server"  CssClass="GridView_1" Width="100%" 
                    AutoGenerateColumns="False"  GridLines="None" AllowPaging="false" 
                    AllowSorting="True" HeaderStyle-CssClass="tblHead" 
                    EmptyDataText="No se encuentra ningun registro"
                    OnSelectedIndexChanged="Grid_Modificaciones_SelectedIndexChanged"
                    OnSorting="Grid_Modificaciones_Sorting">
                    <Columns>
                        <asp:ButtonField ButtonType="Image" CommandName="Select" 
                            ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                            <ItemStyle Width="3%" />
                        </asp:ButtonField>
                        <asp:BoundField DataField="NO_MOVIMIENTO_EGR" HeaderText="No. Modificación" SortExpression="NO_MOVIMIENTO_EGR">
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" Width="10%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ANIO" HeaderText="Año" SortExpression="ANIO" >
                            <HeaderStyle HorizontalAlign="Left" Width="8%" />
                            <ItemStyle HorizontalAlign="Left" Width="8%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TOTAL_MODIFICADO" HeaderText="Importe Modificado"  SortExpression="TOTAL_MODIFICADO" DataFormatString="{0:n}">
                            <HeaderStyle HorizontalAlign="Right" Width="15%" />
                            <ItemStyle HorizontalAlign="Right" Width="15%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS" >
                            <HeaderStyle HorizontalAlign="Center" Width="15%" />
                            <ItemStyle HorizontalAlign="Center" Width="15%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FECHA_CREO" HeaderText="Fecha" SortExpression="FECHA_CREO" DataFormatString="{0:dd/MMM/yyyy HH:mm:ss}">
                            <HeaderStyle HorizontalAlign="Center" Width="20%" />
                            <ItemStyle HorizontalAlign="Center" Width="20%" />
                        </asp:BoundField>
                    </Columns>
                    <SelectedRowStyle CssClass="GridSelected" />
                    <PagerStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAltItem" />
                </asp:GridView>
             </div>
             <div id="Div_Grid_Movimientos" runat="server">
             <table width="99%">
                <tr id="Tr_Chk_Autorizar" runat="server">
                    <td style="text-align:right;">
                        <asp:CheckBox ID="Chk_Reabrir" runat="server" />
                        <asp:Label id="Lbl_Reabrir" runat="server" Text="Abrir Modificación"></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:CheckBox ID="Chk_Autorizar" runat="server" OnClick="javascript:Autorizacion_Masiva(this);"/>
                        <asp:Label id="Lbl_Autorizacion" runat="server" Text="Autorizar Todo"></asp:Label> 
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:CheckBox ID="Chk_Aceptacion" runat="server" OnClick="javascript:Aceptacion_Masiva(this);"/>
                        <asp:Label id="Lbl_Aceptar" runat="server" Text="Aceptar Todo"></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
             </table>
             <div id="Div_Grid_Mov" runat="server" style="overflow:auto;max-height:500px;width:98%;vertical-align:top;">
                    <table width="100%">
                        <tr>
                            <td >
                                <asp:GridView ID="Grid_Tipo_Modificacion" runat="server" style="white-space:normal;"
                                AutoGenerateColumns="False" GridLines="Horizontal" 
                                Width="99%"  EmptyDataText="No se encontraron registros" 
                                CssClass="GridView_1" HeaderStyle-CssClass="tblHead" ShowHeader="false"
                                DataKeyNames="TIPO_OPERACION"   OnRowDataBound="Grid_Tipo_Modificacion_RowDataBound" >
                                <Columns>
                                    <asp:BoundField DataField="TIPO_OPERACION" HeaderText="Operación" SortExpression="TIPO_OPERACION" >
                                        <ItemStyle HorizontalAlign="Left" Width="815px" Font-Size="10pt" BorderColor="Navy" Font-Bold="true"/>
                                    </asp:BoundField>
                                    <asp:TemplateField>
                                       <ItemTemplate>
                                         </td>
                                         </tr> 
                                         <tr>
                                          <td colspan="100%">
                                           <div id="div<%# Eval("TIPO_OPERACION") %>" style="display:inline;position:relative;left:10px;" >                             
                                               <asp:GridView ID="Grid_Movimientos" runat="server" style="white-space:normal;"
                                                AutoGenerateColumns="False" GridLines="None" 
                                                Width="97%"  EmptyDataText="No se encontraron registros" 
                                                CssClass="GridView_1" HeaderStyle-CssClass="tblHead"
                                                DataKeyNames="SOLICITUD_ID"   OnRowDataBound="Grid_Movimientos_RowDataBound"
                                                OnRowCreated="Grid_Movimientos_RowCreated">
                                                <Columns>
                                                    <asp:TemplateField> 
                                                      <ItemTemplate> 
                                                            <a href="javascript:Grid_Anidado('div<%# Eval("SOLICITUD_ID") %>', 'one');"> 
                                                                 <img id="imgdiv<%# Eval("SOLICITUD_ID") %>" alt="Click expander/contraer registros" border="0" src="../imagenes/paginas/stocks_indicator_down.png" /> 
                                                            </a> 
                                                      </ItemTemplate> 
                                                      <AlternatingItemTemplate> 
                                                           <a href="javascript:Grid_Anidado('div<%# Eval("SOLICITUD_ID") %>', 'alt');"> 
                                                                <img id="imgdiv<%# Eval("SOLICITUD_ID") %>" alt="Click expander/contraer registros" border="0" src="../imagenes/paginas/stocks_indicator_down.png" /> 
                                                           </a> 
                                                      </AlternatingItemTemplate> 
                                                      <ItemStyle HorizontalAlign ="Center" Font-Size="X-Small" Width="3%" />
                                                        </asp:TemplateField>      
                                                        <asp:BoundField DataField="SOLICITUD_ID" HeaderText="Solicitud" SortExpression="SOLICITUD_ID">
                                                            <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="8pt" />
                                                            <ItemStyle HorizontalAlign="Left" Width="10%" Font-Size="8pt"  Font-Bold="true" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="NO_SOLICITUD" HeaderText="No. Solicitud" SortExpression="NO_SOLICITUD" >
                                                            <HeaderStyle HorizontalAlign="Left" Width="12%" Font-Size="8pt" />
                                                            <ItemStyle HorizontalAlign="Left" Width="12%" Font-Size="8pt" Font-Bold="true" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="FF" /> 
                                                        <asp:BoundField DataField="PP" />
                                                        <asp:BoundField DataField="UR" />
                                                        <asp:BoundField DataField="TIPO_OPERACION" HeaderText="Operación" SortExpression="TIPO_OPERACION" >
                                                            <HeaderStyle HorizontalAlign="Left" Width="8%" Font-Size="8pt" />
                                                            <ItemStyle HorizontalAlign="Left" Width="8%" Font-Size="8pt" Font-Bold="true" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="IMPORTE" HeaderText="Importe"  SortExpression="IMPORTE" DataFormatString="{0:n}">
                                                            <HeaderStyle HorizontalAlign="Right" Width="12%" Font-Size="8pt"  />
                                                            <ItemStyle HorizontalAlign="Right" Width="12%" Font-Size="8pt" Font-Bold="true" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS" >
                                                            <HeaderStyle HorizontalAlign="Center" Width="12%" Font-Size="8pt" />
                                                            <ItemStyle HorizontalAlign="Center" Width="12%" Font-Size="8pt" Font-Bold="true" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="FECHA_CREO" HeaderText="Fecha" SortExpression="FECHA_CREO" DataFormatString="{0:dd/MMM/yyyy HH:mm:ss}">
                                                            <HeaderStyle HorizontalAlign="Center" Width="15%" Font-Size="8pt" />
                                                            <ItemStyle HorizontalAlign="Center" Width="15%" Font-Size="8pt" Font-Bold="true"/>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="USUARIO_CREO" HeaderText="Usuario" SortExpression="USUARIO_CREO">
                                                            <HeaderStyle HorizontalAlign="Center" Width="35%" Font-Size="8pt" />
                                                            <ItemStyle HorizontalAlign="Center" Width="35%" Font-Size="8pt" Font-Bold="true"/>
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText ="PreAutorizar"> 
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="Chk_PreAutorizado" runat="server" CssClass ='<%# Eval("SOLICITUD_ID") %>' />
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" Width="10%" Font-Size="8pt"/>
                                                            <ItemStyle HorizontalAlign ="Center" Font-Size="8pt" Width="10%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText ="Aceptar"> 
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="Chk_Aceptar" runat="server" CssClass ='<%# Eval("SOLICITUD_ID") %>' />
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" Width="10%" Font-Size="8pt"/>
                                                            <ItemStyle HorizontalAlign ="Center" Font-Size="8pt" Width="10%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText ="Cancelar">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="Chk_Cancelado" runat="server" OnClick="javascript:Rechazado(this);" CssClass='<%# Eval("SOLICITUD_ID") %>' />
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" Width="10%" Font-Size="8pt"/>
                                                            <ItemStyle HorizontalAlign ="Center" Font-Size="8pt" Width="10%"  />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText ="Autorizar">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="Chk_Autorizado" runat="server" CssClass='<%# Eval("SOLICITUD_ID") %>' />
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" Width="10%" Font-Size="8pt"/>
                                                            <ItemStyle HorizontalAlign ="Center" Font-Size="8pt" Width="10%"  />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText ="Rechazar">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="Chk_Rechazado" runat="server" OnClick="javascript:Rechazado(this);" CssClass='<%# Eval("SOLICITUD_ID") %>' />
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" Width="10%" Font-Size="8pt"/>
                                                            <ItemStyle HorizontalAlign ="Center" Font-Size="8pt" Width="10%"  />
                                                        </asp:TemplateField>
                                                    <asp:TemplateField>
                                                       <ItemTemplate>
                                                         </td>
                                                         </tr> 
                                                         <tr>
                                                          <td colspan="100%">
                                                           <div id="div<%# Eval("SOLICITUD_ID") %>" style="display:inline;position:relative;left:20px;" >                             
                                                               <asp:GridView ID="Grid_Movimientos_Detalle" runat="server" style="white-space:normal;"
                                                                   CssClass="GridView_Nested" HeaderStyle-CssClass="tblHead"
                                                                   AutoGenerateColumns="false" GridLines="Both" Width="97%"
                                                                   OnRowCreated="Grid_Movimientos_RowCreated">
                                                                   <Columns> 
                                                                        <asp:BoundField DataField="SOLICITUD_ID"  />
                                                                        <asp:BoundField DataField="NO_MOVIMIENTO"  />
                                                                        <asp:BoundField DataField="TIPO_EGRESO"  />
                                                                        <asp:BoundField DataField="FF" HeaderText="Fuente." >
                                                                            <HeaderStyle HorizontalAlign="Left" Font-Size="7.5pt"/>
                                                                            <ItemStyle HorizontalAlign="Left" Width="5%" Font-Size="7pt"/>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="PP" HeaderText="Programa">
                                                                            <HeaderStyle HorizontalAlign="Left" Font-Size="7.5pt"/>
                                                                            <ItemStyle HorizontalAlign="Left"  Width="5%" Font-Size="7pt" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="UR" HeaderText="U. Responsable">
                                                                            <HeaderStyle HorizontalAlign="Left" Font-Size="7.5pt"/>
                                                                            <ItemStyle HorizontalAlign="Left"  Width="15%" Font-Size="7pt" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="PARTIDA" HeaderText="Partida">
                                                                            <HeaderStyle HorizontalAlign="Left" Font-Size="7.5pt"/>
                                                                            <ItemStyle HorizontalAlign="Left"  Width="25%" Font-Size="7pt" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="IMPORTE" HeaderText="Importe" DataFormatString="{0:#,###,##0.00}">
                                                                            <HeaderStyle HorizontalAlign="Right" Font-Size="7.5pt"/>
                                                                            <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="7pt"/>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="APROBADO" HeaderText="Aprobado" DataFormatString="{0:#,###,##0.00}">
                                                                            <HeaderStyle HorizontalAlign="Right" Font-Size="7.5pt"/>
                                                                            <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="7pt"/>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="AMPLIACION" HeaderText="Ampliación" DataFormatString="{0:#,###,##0.00}">
                                                                            <HeaderStyle HorizontalAlign="Right" Font-Size="7.5pt"/>
                                                                            <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="7pt"/>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="REDUCCION" HeaderText="Reducción" DataFormatString="{0:#,###,##0.00}">
                                                                            <HeaderStyle HorizontalAlign="Right" Font-Size="7.5pt"/>
                                                                            <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="7pt"/>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="MODIFICADO" HeaderText="Modificado" DataFormatString="{0:#,###,##0.00}">
                                                                            <HeaderStyle HorizontalAlign="Right" Font-Size="7.5pt"/>
                                                                            <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="7pt"/>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="ENERO" HeaderText="Enero" DataFormatString="{0:#,###,##0.00}">
                                                                            <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                            <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="7pt"/>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="FEBRERO" HeaderText="Febrero" DataFormatString="{0:#,###,##0.00}">
                                                                            <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                            <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="7pt"/>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="MARZO" HeaderText="Marzo" DataFormatString="{0:#,###,##0.00}">
                                                                            <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                            <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="7pt"/>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="ABRIL" HeaderText="Abril" DataFormatString="{0:#,###,##0.00}">
                                                                            <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                            <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="7pt"/>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="MAYO" HeaderText="Mayo" DataFormatString="{0:#,###,##0.00}">
                                                                            <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                            <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="7pt"/>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="JUNIO" HeaderText="Junio" DataFormatString="{0:#,###,##0.00}">
                                                                           <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                            <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="7pt"/>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="JULIO" HeaderText="Julio" DataFormatString="{0:#,###,##0.00}">
                                                                            <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                            <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="7pt"/>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="AGOSTO" HeaderText="Agosto" DataFormatString="{0:#,###,##0.00}">
                                                                            <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                            <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="7pt"/>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="SEPTIEMBRE" HeaderText="Septiembre" DataFormatString="{0:#,###,##0.00}">
                                                                            <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                            <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="7pt"/>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="OCTUBRE" HeaderText="Octubre" DataFormatString="{0:#,###,##0.00}">
                                                                            <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                            <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="7pt"/>
                                                                        </asp:BoundField>                 
                                                                        <asp:BoundField DataField="NOVIEMBRE" HeaderText="Noviembre" DataFormatString="{0:#,###,##0.00}">
                                                                            <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                            <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="7pt"/>
                                                                        </asp:BoundField>                   
                                                                        <asp:BoundField DataField="DICIEMBRE" HeaderText="Diciembre" DataFormatString="{0:#,###,##0.00}">
                                                                            <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                            <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="7pt"/>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="TIPO_PARTIDA" HeaderText="Tipo">
                                                                            <HeaderStyle HorizontalAlign="Center" Font-Size="7pt"/>
                                                                            <ItemStyle HorizontalAlign="Center"  Width="5%" Font-Size="7pt"/>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="JUSTIFICACION" />
                                                                   </Columns>
                                                                   <SelectedRowStyle CssClass="GridSelected_Nested" />
                                                                   <PagerStyle CssClass="GridHeader_Nested" />
                                                                   <HeaderStyle CssClass="GridHeader_Nested" />
                                                                   <AlternatingRowStyle CssClass="GridAltItem_Nested" /> 
                                                               </asp:GridView>
                                                           </div>
                                                          </td>
                                                         </tr>
                                                       </ItemTemplate>
                                                    </asp:TemplateField>
                                                 </Columns>
                                                 <SelectedRowStyle CssClass="GridSelected" />
                                                    <PagerStyle CssClass="GridHeader" />
                                                    <HeaderStyle CssClass="GridHeader" />
                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                              </asp:GridView>
                                               
                                           </div>
                                          </td>
                                         </tr>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                 </Columns>
                                 <SelectedRowStyle CssClass="GridSelected" />
                                    <PagerStyle CssClass="GridHeader" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                              </asp:GridView>
                            </td>
                        </tr>
                    </table >
                </div>
              <table width="99%">
                    <tr>
                        <td style="text-align:right;">
                            <asp:Label ID="Lbl_Total_Modificado" runat="server" Text="Total Modificado"></asp:Label>
                            <asp:TextBox ID="Txt_Total_Modificado" runat="server" Width="20%" style="text-align:right; font-size:8pt; border-color:Navy" ReadOnly="true"></asp:TextBox>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
              </table>
              </div>
            </center>
         </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>  