﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" 
CodeFile="Frm_Rpt_Psp_Reservas.aspx.cs" Inherits="paginas_Presupuestos_Frm_Rpt_Psp_Reservas" Title="SIAC Sistema Integral Administrativo y Comercial" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" >
        function Grid_Anidado(Control, Fila) {
            var div = document.getElementById(Control);
            var img = document.getElementById('img' + Control);

            if (div.style.display == "none") {
                div.style.display = "inline";
                if (Fila == 'alt') {
                    img.src = "../imagenes/paginas/stocks_indicator_down.png";
                }
                else {
                    img.src = "../imagenes/paginas/stocks_indicator_down.png";
                }
                img.alt = "Contraer Registros";
            }
            else {
                div.style.display = "none";
                if (Fila == 'alt') {
                    img.src = "../imagenes/paginas/add_up.png";
                }
                else {
                    img.src = "../imagenes/paginas/add_up.png";
                }
                img.alt = "Expandir Registros";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"  AsyncPostBackTimeout="36000" EnableScriptLocalization="true" EnableScriptGlobalization="true"/> 
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
    <ContentTemplate>
                <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                        <ProgressTemplate>
                            <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                            <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                        </ProgressTemplate>                     
                </asp:UpdateProgress>
                <div id="Div_Requisitos" style="background-color:#ffffff; width:100%; height:100%;">
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">Reporte de Reservas</td>
                    </tr>
                    <tr>
                        <td>
                          <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;">              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top" >
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text=""  CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                            </table>                   
                          </div>                          
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>                        
                    </tr>
                    <tr class="barra_busqueda" align="left">
                        <td>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/paginas/imagenes/paginas/Listado.png" CausesValidation="False" 
                                OnClick="Btn_Generar_Listado_Click" AlternateText="Generar Listado" ToolTip="Generar Listado" Width="24px"/>
                                &nbsp;
                            <asp:ImageButton ID="Btn_Reporte_PDF" runat="server" ToolTip="Generar Reporte (PDF)" ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" Width="24px" CssClass="Img_Button" Visible = "false"
                                AlternateText="Generar Reporte (PDF)" onclick="Btn_Generar_Reporte_PDF_Click" /> 
                            <asp:ImageButton ID="Btn_Reporte_Excel" runat="server" ToolTip="Generar Reporte (Excel)" ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png" Width="24px" CssClass="Img_Button" Visible = "false" 
                                AlternateText="Generar Reporte (Excel)" onclick="Btn_Generar_Reporte_Excel_Click" /> 
                            <asp:ImageButton ID="ImageButton4" runat="server" ToolTip="Limpiar Filtros" CausesValidation="False" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png" 
                                AlternateText="Limpiar Filtros" onclick="Btn_Limpiar_Filtros_Click" Width="25px"/>       
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;
                            <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" Width="24px" CssClass="Img_Button"
                                        ToolTip="Salir"     AlternateText="Salir" OnClick="Btn_Salir_Click"/>
                        </td>                        
                    </tr>
                </table>
                <br />
                <center>
                    <div style="border-width:medium; width:95%; border-bottom-style:solid;" >
                    <table width="100%">                                                                            
                        <tr>
                            <td style="width:20%; text-align:left; ">
                                <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text="Unidad Responsable" CssClass="estilo_fuente"></asp:Label>
                            </td>
                            <td style="width:80%; text-align:left;" colspan="3">
                                <asp:DropDownList ID="Cmb_Unidad_Responsable" runat="server" Width="90%" 
                                    onselectedindexchanged="Cmb_Unidad_Responsable_SelectedIndexChanged" AutoPostBack="true" >
                                    <%--<asp:ListItem Text="&lt;-- SELECCIONA UNIDAD RESPONSABLE --&gt;" Value="SELECCIONE"></asp:ListItem>--%>
                                </asp:DropDownList> 
                            </td>
                        </tr>
                        <tr>
                            <td style="width:20%; text-align:left; ">
                                <asp:Label ID="Lbl_Partida" runat="server" Text="Partida" CssClass="estilo_fuente"></asp:Label>
                            </td>
                            <td style="width:80%; text-align:left;" colspan="3">
                                <asp:DropDownList ID="Cmb_Partida" runat="server" Width="90%" >
                                    <%--<asp:ListItem Text="&lt;-- SELECCIONA PARTIDA --&gt;" Value="SELECCIONE"></asp:ListItem>--%>
                                </asp:DropDownList> 
                            </td>
                        </tr>  
                        <tr>
                            <td style="width:20%; text-align:left; ">
                                <asp:Label ID="Lbl_Anio" runat="server" Text=" * Año " CssClass="estilo_fuente"></asp:Label>
                            </td>
                            <td colspan="3" style="text-align:left;">
                                <asp:DropDownList ID="Cmb_Anio" runat="server" Width="150" AutoPostBack="true" OnSelectedIndexChanged="Cmb_Anio_SelectedIndexChanged">
                                    <%--<asp:ListItem Text="&lt;-- TODAS --&gt;" Value="TODAS"></asp:ListItem>   --%>
                                </asp:DropDownList>                                   
                            </td>
                        </tr>                                          
                        <tr>
                            <td style="text-align:left; ">
                                <asp:Label ID="Label1" runat="server" Text=" Del : " CssClass="estilo_fuente"></asp:Label>
                            </td>
                            <td width = "20%" style="text-align:left; ">
                            <asp:TextBox ID="Txt_Fecha_Inicial" runat="server" Width="70%" Enabled="false"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="Txt_Fecha_Inicial_FilteredTextBoxExtender" runat="server" TargetControlID=                   "Txt_Fecha_Inicial" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters"
                            ValidChars="/_" />
                        <cc1:CalendarExtender ID="Txt_Fecha_Inicial_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Inicial" PopupButtonID="Btn_Fecha_Inicial" Format="dd/MMM/yyyy" />
                        <asp:ImageButton ID="Btn_Fecha_Inicial" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Inicial" />
                            </td>
                            
                            <td style="width:20%; text-align:left; " colspan="2">
                                <asp:Label ID="Label2" runat="server" Text=" Al : " CssClass="estilo_fuente"></asp:Label>
                            
                            <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="70%" Enabled="false"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="Txt_Fecha_Final_FilteredTextBoxExtender" runat="server" TargetControlID="Txt_Fecha_Final" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters"
                            ValidChars="/_" />
                        <cc1:CalendarExtender ID="Txt_Fecha_Final_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Final" PopupButtonID="Btn_Fecha_Final" Format="dd/MMM/yyyy" />
                        <asp:ImageButton ID="Btn_Fecha_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Final" />
                            </td>
                        </tr>                                                                                                                     
                    </table>
                </div>
                </center>
                <div style="overflow:auto;height:320px;width:99%;vertical-align:top;
                                   border-style:outset;border-color: Silver;" > 
               <table style="width:100%;">
                        <tr>
                            <td colspan="100%">
                              
                                <asp:GridView ID="Grid_Reservas" runat="server" AutoGenerateColumns="False" 
                                      CssClass="GridView_1" GridLines="None" style="white-space:normal;"
                                    AllowSorting="true" HeaderStyle-CssClass="tblHead" 
                                    EmptyDataText="No se encontraron reservas" OnRowDataBound="Grid_Reservas_RowDataBound"
                                    >
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <%--<asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Image ID="Img_Btn_Expandir" runat="server" 
                                                    ImageUrl="~/paginas/imagenes/paginas/stocks_indicator_down.png" />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="2%" />
                                            <ItemStyle HorizontalAlign="Center" Width="2%" />
                                        </asp:TemplateField> --%>
                                      <asp:TemplateField> 
                                          <ItemTemplate> 
                                              <a href="javascript:Grid_Anidado('div<%# Eval("NO_RESERVA") %>', 'one');"> 
                                              <img id="imgdiv<%# Eval("NO_RESERVA") %>" alt="Click expander/contraer registros" border="0" src="../imagenes/paginas/stocks_indicator_down.png" /> 
                                              </a> 
                                            </ItemTemplate> 
                                              <AlternatingItemTemplate> 
                                                   <a href="javascript:Grid_Anidado('div<%# Eval("NO_RESERVA") %>', 'alt');"> 
                                                        <img id="imgdiv<%# Eval("NO_RESERVA") %>" alt="Click expander/contraer registros" border="0" src="../imagenes/paginas/stocks_indicator_down.png" /> 
                                                   </a> 
                                              </AlternatingItemTemplate> 
                                            <ItemStyle HorizontalAlign ="Center" Font-Size="X-Small" Width="3%" />
                                        </asp:TemplateField>  
                                                         
                                        <asp:BoundField DataField="NO_RESERVA" HeaderText="Folio" Visible="True" >
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Fecha_Creo" HeaderText="Fecha" 
                                            DataFormatString="{0:dd/MMM/yyyy}" Visible="True" >
                                            <HeaderStyle HorizontalAlign="Left" Wrap="true" />
                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Fecha_Creo" HeaderText="Hora" 
                                            DataFormatString="{0:hh:mm:ss tt}" Visible="True" >
                                            <HeaderStyle HorizontalAlign="Left" Wrap="true" />
                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                        </asp:BoundField>                                        
                                       <asp:BoundField DataField="CONCEPTO" HeaderText="Concepto" 
                                            Visible="True" >
                                            <HeaderStyle HorizontalAlign="Left" Wrap="true" />
                                            <ItemStyle HorizontalAlign="Left" Width="50%" />
                                        </asp:BoundField>                                          
                                       <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" 
                                            Visible="True" >
                                            <HeaderStyle HorizontalAlign="Left" Wrap="true" />
                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                        </asp:BoundField>  
                                        <asp:TemplateField>
                                        <ItemTemplate>
                                        <tr>
                                        <td colspan="100%">
                                        <div id="div<%# Eval("NO_RESERVA") %>" style="display:inline;position:relative;left:20px;" >
                                           
                                                <%-- <asp:Label ID="Lbl_Movimientos" runat="server" Text='<%# Bind("IDENTIFICADOR_TIPO") %>' Visible="false"></asp:Label>
                                                <asp:Literal ID="Ltr_Inicio" runat="server" Text="&lt;/td&gt;&lt;tr id='Renglon_Grid' style='display:block;position:static'&gt;&lt;td colspan='3'; align='right'&gt;" /> --%>      
                                                
                                                <asp:GridView ID="Grid_Reservas_Detalles" runat="server" 
                                                    AutoGenerateColumns="False" CssClass="GridView_1"  
                                                    GridLines="None" Width="100%"                                     
                                                    style="white-space:normal;" EmptyDataText="No hay historial para mostrar">
                                                    <RowStyle CssClass="GridItem" />
                                                    <Columns>
                                                    <asp:BoundField DataField="No_Reserva" HeaderText="No_Reserva" Visible="False">
                                                            <FooterStyle HorizontalAlign="Left" />
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left"  Font-Size="X-Small"/>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Fecha" HeaderText="Fecha" Visible="True" DataFormatString="{0:dd/MMM/yyyy}">
                                                        <FooterStyle HorizontalAlign="Left" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" Width="11%" Font-Size="X-Small"/>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Fecha" HeaderText="Hora" Visible="True" DataFormatString="{0:hh:mm:ss tt}">
                                                        <FooterStyle HorizontalAlign="Left" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" Width="11%" Font-Size="X-Small"/>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Usuario_Creo" HeaderText="Modificó" Visible="True">
                                                        <FooterStyle HorizontalAlign="Left" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left"  Font-Size="X-Small"/>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Cargo" HeaderText="Cargo" Visible="True">
                                                        <FooterStyle HorizontalAlign="Left" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                                    </asp:BoundField>                                        
                                                    <asp:BoundField DataField="Abono" HeaderText="Abono" Visible="True">
                                                        <FooterStyle HorizontalAlign="Left" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                                    </asp:BoundField>                                        
                                                    <asp:BoundField DataField="Importe" HeaderText="Importe" Visible="True" DataFormatString="{0:C}">
                                                        <FooterStyle HorizontalAlign="Right" />
                                                        <HeaderStyle HorizontalAlign="Right" />
                                                        <ItemStyle HorizontalAlign="Right" Font-Size="X-Small" />
                                                    </asp:BoundField>                                        
                                                    <asp:BoundField DataField="No_Poliza" HeaderText="Póliza" Visible="True" >
                                                        <FooterStyle HorizontalAlign="Center" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                                                    </asp:BoundField> 
                                                    </Columns>
                                                    <PagerStyle CssClass="GridHeader" />
                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                    <HeaderStyle CssClass="GridHeader" />
                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                </asp:GridView>                                                                                       
                                                <asp:Literal ID="Ltr_Fin" runat="server" Text="" />
                                                </div>
                                          </td>
                                        </tr>
                                      </ItemTemplate>
                                     </asp:TemplateField>
                                     
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>                                             
            </div>
            <br />
            <br />
            <br />
            <br />
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>