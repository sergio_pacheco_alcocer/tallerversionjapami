﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Rpt_Psp_Presupuesto_Ingresos.aspx.cs" Inherits="paginas_Presupuestos_Frm_Rpt_Psp_Presupuesto_Ingresos" Title="SIAC Sistema Integral Administrativo y Comercial" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <asp:ScriptManager ID="ScptM_Rpt_Psp" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>                     
            </asp:UpdateProgress>
            <div style="width:100%;">
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr><td colspan="2" style="height:0.5em;"></td></tr>
                    <tr align="center">
                        <td class="label_titulo" colspan="2">Reporte de Presupuesto de Ingresos</td>
                    </tr>
                    <tr>
                        <td colspan ="2">
                          <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px" Enabled="false"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;">              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top">
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                            </table>                   
                          </div>                
                        </td>
                    </tr>
                    <tr><td colspan="2" style="height:0.5em;"></td></tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" style="width:50%">
                            <asp:ImageButton ID="Btn_Salir" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" Width="24px" 
                                CssClass="Img_Button" ToolTip="Salir" 
                                OnClick="Btn_Salir_Click"/>
                        </td>
                        <td style="width:50%; text-align:right;">
                            <asp:Button id="Btn_Consultar" runat="server" Text = "Consultar" CssClass="button_agregar"
                             OnClick="Btn_Consultar_Click" />
                        </td>
                    </tr>
                </table>   
                <br />
                <center>
                  <asp:Panel ID="Panel1" runat="server" GroupingText="Filtros de reporte" Width="96%">
                    <center>
                        <table width="98%">
                            <tr><td colspan="6" style="height:0.3em;"></td></tr>
                            <tr>
                                <td style="width:23%; text-align:left; cursor:default;" class="button_autorizar">
                                    <asp:Label ID="Lbl_Anio" runat="server" Text="* Año"></asp:Label>
                                </td>
                                <td style="width:27%; text-align:left; cursor:default;" class="button_autorizar">
                                    <asp:DropDownList id= "Cmb_Anio" runat = "server" Width="100%"></asp:DropDownList>
                                </td>
                                 <td colspan="2" cursor:default;" class="button_autorizar">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="width:15%; text-align:left; cursor:default;" class="button_autorizar" rowspan="2">
                                    <asp:Label ID="Lbl_FF" runat="server" Text="Fuente Financiamiento"></asp:Label>
                                </td>
                                <td style=" text-align:left; cursor:default; width:85%;"  colspan="3" class="button_autorizar">
                                    <asp:DropDownList ID="Cmb_FF" runat="server" Width="100%"></asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </center> 
                  </asp:Panel>
                </center>
                <center>
                    <div style="overflow:auto; height:auto; max-height:320px;width:99%;vertical-align:top; width:96%;" >
                        <asp:GridView ID="Grid_Registros" runat="server" AutoGenerateColumns="false" Width="130%" style="white-space:normal"
                                 CssClass="GridView_1" GridLines="Both" OnSorting="Grid_Registros_Sorting"
                                 EmptyDataText="No se encuentro ningun registro" AllowSorting="true"
                                 OnRowDataBound="Grid_Registros_RowDataBound" >
                            <Columns>
                                <asp:BoundField DataField="CONCEPTO" HeaderText="Concepto"  SortExpression="CONCEPTO">
                                    <HeaderStyle HorizontalAlign="Left" Width="25%" />
                                    <ItemStyle HorizontalAlign="Left" Width="25%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="TOTAL" HeaderText="Estimado" SortExpression="TOTAL">
                                     <HeaderStyle HorizontalAlign="Center" Width="7%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="7%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="ACUMULADO EN EL AÑO" HeaderText="Recaudado" SortExpression="ACUMULADO EN EL AÑO">
                                     <HeaderStyle HorizontalAlign="Center" Width="7%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="7%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="PRESUPUESTO POR RECAUDAR" HeaderText="Por Redaudar" SortExpression="PRESUPUESTO POR RECAUDAR">
                                     <HeaderStyle HorizontalAlign="Center" Width="7%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="7%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="ENERO" HeaderText="Ene" SortExpression="ENERO">
                                     <HeaderStyle HorizontalAlign="Center" Width="7%" />
                                    <ItemStyle HorizontalAlign="Right" Width="7%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="FEBRERO" HeaderText="Feb" SortExpression="FEBRERO">
                                     <HeaderStyle HorizontalAlign="Center" Width="7%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="7%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="MARZO" HeaderText="Mar" SortExpression="MARZO">
                                     <HeaderStyle HorizontalAlign="Center" Width="7%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="7%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="ABRIL" HeaderText="Abr" SortExpression="ABRIL">
                                     <HeaderStyle HorizontalAlign="Center" Width="7%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="7%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="MAYO" HeaderText="May" SortExpression="MAYO">
                                     <HeaderStyle HorizontalAlign="Center" Width="7%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="7%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="JUNIO" HeaderText="Jun" SortExpression="JUNIO">
                                     <HeaderStyle HorizontalAlign="Center" Width="7%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="7%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="JULIO" HeaderText="Jul" SortExpression="JULIO">
                                     <HeaderStyle HorizontalAlign="Center" Width="7%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="7%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="AGOSTO" HeaderText="Ago" SortExpression="AGOSTO">
                                     <HeaderStyle HorizontalAlign="Center" Width="7%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="7%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="SEPTIEMBRE" HeaderText="Sep" SortExpression="SEPTIEMBRE">
                                     <HeaderStyle HorizontalAlign="Center" Width="7%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="7%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="OCTUBRE" HeaderText="Oct" SortExpression="OCTUBRE">
                                     <HeaderStyle HorizontalAlign="Center" Width="7%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="7%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="NOVIEMBRE" HeaderText="Nov" SortExpression="NOVIEMBRE">
                                     <HeaderStyle HorizontalAlign="Center" Width="7%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="7%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="DICIEMBRE" HeaderText="Dic" SortExpression="DICIEMBRE">
                                     <HeaderStyle HorizontalAlign="Center" Width="7%"/>
                                    <ItemStyle HorizontalAlign="Right" Width="7%" Font-Size="7pt"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="Tipo" />
                            </Columns>
                            <RowStyle CssClass="GridItem" />
                            <PagerStyle CssClass="GridHeader" />
                            <SelectedRowStyle CssClass="GridSelected" />
                            <HeaderStyle CssClass="GridHeader" /> 
                            <AlternatingRowStyle CssClass="GridAltItem" />    
                        </asp:GridView>
                    </div>
                </center>
            </div>
          </ContentTemplate>
          <Triggers>
            <asp:AsyncPostBackTrigger ControlID="Btn_Generar_Reporte" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="Btn_Consultar" EventName="Click" />
            <asp:PostBackTrigger  ControlID="IBtn_Generar"/>
        </Triggers>
    </asp:UpdatePanel>
    <div style="width:100%;">
        <center>
          <table width="96%">
            <tr>
                <td style="cursor:default; width:98%; text-align:right;" class="button_autorizar">
                    <asp:UpdatePanel ID="Upnl_Export_PDF" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                        <ContentTemplate>
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="Upnl_Export_PDF" DisplayAfter="0">
                                <ProgressTemplate>
                                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                    <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>  
                           <asp:ImageButton ID="Btn_Generar_Reporte" runat="server"  ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" 
                            CausesValidation="false" OnClick="Btn_Generar_Reporte_Click" Width="32px" Height="32px"  style="cursor:hand;"
                            ToolTip="Generar Reporte en PDF"/> 
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    
                    <asp:UpdatePanel ID="Upnl_Export_EXCEL" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                        <ContentTemplate>
                            <asp:ImageButton ID="IBtn_Generar" runat="server" ToolTip="Generar Reporte" 
                            ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png" 
                            Width="32px" Height="32px"  style="cursor:hand;" OnClick="Btn_Generar_Click"/>
                         </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
          </table>
        </center>
    </div>
</asp:Content>


