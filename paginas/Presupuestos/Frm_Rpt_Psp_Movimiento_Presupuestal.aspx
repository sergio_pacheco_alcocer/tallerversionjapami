﻿<%@ Page Title="SIAC Sistema Integral Administrativo y Comercial" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" 
CodeFile="Frm_Rpt_Psp_Movimiento_Presupuestal.aspx.cs" Inherits="paginas_Presupuestos_Frm_Rpt_Psp_Movimiento_Presupuestal" %>
<%@ Register Assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                    DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                        </div>
                        <div id="div_progress" class="processMessage" >
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />                           
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
        
            <div id="Div_Contenido" style="width: 97%; height: 700px;">
                <table width="97%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td colspan="5" class="label_titulo">Reporte de Movimiento Presupuestal</td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <!--Bloque del mensaje de error-->
                            <div id="Div_Contenedor_Msj_Error" style="width:95%;font-size:9px;" runat="server" visible="false">
                                <table style="width:100%;">
                                    <tr>
                                        <td align="left" style="font-size:12px;color:Red;font-family:Tahoma;text-align:left;">
                                            <asp:Image ID="Img_Warning" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                            Width="24px" Height="24px" />
                                        </td>            
                                        <td style="font-size:9px;width:90%;text-align:left;" valign="top">
                                            <asp:Label ID="Lbl_Informacion" runat="server" ForeColor="Red" />
                                        </td>
                                    </tr> 
                                </table>                   
                            </div>
                        </td>
                    </tr>
                    <tr class="barra_busqueda">
                        <td colspan="5" style="width:20%;">
                            <!--Bloque de la busqueda-->
                            <asp:ImageButton ID="Btn_Consultar" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_consultar.png" Width="24px" 
                                CssClass="Img_Button" AlternateText="CONSULTAR" ToolTip="Consultar" 
                                onclick="Btn_Consultar_Click" />
                            <asp:ImageButton ID="Btn_Imprimir" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" Width="24px" CssClass="Img_Button" 
                                AlternateText="NUEVO" ToolTip="Exportar PDF" 
                                onclick="Btn_Imprimir_Click" />  
                            <asp:ImageButton ID="Btn_Imprimir_Excel" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png" Width="24px" CssClass="Img_Button" 
                                AlternateText="Imprimir Excel" ToolTip="Exportar Excel" 
                                onclick="Btn_Imprimir_Excel_Click" />  
                            <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" ToolTip="Salir" 
                                AlternateText="Salir" onclick="Btn_Salir_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">Operaci&oacute;n </td>
                        <td align="left">
                            <asp:DropDownList ID="Cmb_Tipo_Operacion" runat="server" Width="100%"></asp:DropDownList>
                        </td>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left">Unidad&rarr;</td>
                        <td align="left">Origen</td>
                        <td align="left"><asp:DropDownList ID="Cmb_Unidad_Responsable_Origen" runat="server" Width="100%"></asp:DropDownList></td>
                        <td align="left">Destino</td>
                        <td align="left"><asp:DropDownList ID="Cmb_Unidad_Responsable_Destino" runat="server" Width="100%"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">Financiamiento&rarr;</td>
                        <td align="left">Origen</td>
                        <td align="left"><asp:DropDownList ID="Cmb_Fuente_Financiamiento_Origen" runat="server" Width="100%"></asp:DropDownList></td>
                        <td align="left">Destino</td>
                        <td align="left"><asp:DropDownList ID="Cmb_Fuente_Financiamiento_Destino" runat="server" Width="100%"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">Programa&rarr;</td>
                        <td align="left">Origen</td>
                        <td align="left"><asp:DropDownList ID="Cmb_Programa_Origen" runat="server" Width="100%"></asp:DropDownList></td>
                        <td align="left">Destino</td>
                        <td align="left"><asp:DropDownList ID="Cmb_Programa_Destino" runat="server" Width="100%"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">Cap&iacute;tulo&rarr;</td>
                        <td align="left">Origen</td>
                        <td align="left"><asp:DropDownList ID="Cmb_Capitulo_Origen" runat="server" Width="100%"></asp:DropDownList></td>
                        <td align="left">Destino</td>
                        <td align="left"><asp:DropDownList ID="Cmb_Capitulo_Destino" runat="server" Width="100%"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">Partida&rarr;</td>
                        <td align="left">Origen</td>
                        <td align="left"><asp:DropDownList ID="Cmb_Partida_Origen" runat="server" Width="100%"></asp:DropDownList></td>
                        <td align="left">Destino</td>
                        <td align="left"><asp:DropDownList ID="Cmb_Partida_Destino" runat="server" Width="100%"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td align="left">Fecha Inicial</td>
                        <td align="left">
                            <asp:TextBox ID="Txt_Fecha_Inicial" runat="server" Width="150px" Enabled="false"></asp:TextBox>
                            <cc1:filteredtextboxextender id="Txt_Fecha_Inicial_FilteredTextBoxExtender" runat="server"
                                targetcontrolid="Txt_Fecha_Inicial" filtertype="Custom, Numbers, LowercaseLetters, UppercaseLetters"
                                validchars="/_" />
                            <cc1:calendarextender id="Txt_Fecha_Inicial_CalendarExtender" runat="server" targetcontrolid="Txt_Fecha_Inicial"
                                popupbuttonid="Btn_Fecha_Inicial" format="dd/MMM/yyyy" />
                            <asp:ImageButton ID="Btn_Fecha_Inicial" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                    ToolTip="Seleccione la Fecha Inicial" />
                        </td>                       
                        <td align="left">Fecha Final</td>
                        <td align="left">                                
                            <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="150px" Enabled="false"></asp:TextBox>
                            <cc1:calendarextender id="CalendarExtender3" runat="server" targetcontrolid="Txt_Fecha_Final"
                                popupbuttonid="Btn_Fecha_Final" format="dd/MMM/yyyy" />
                            <asp:ImageButton ID="Btn_Fecha_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                ToolTip="Seleccione la Fecha Final" />
                        </td>                    
                    </tr>
                    <tr>
                        <td colspan="5">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" colspan="5">
                            <div id="Div_Grid_Movimiento_Presupuestal" style="width:800px;">
                                <asp:GridView ID="Grid_Movimiento_Presupuestal" runat="server" style="white-space:normal;" 
                                    AutoGenerateColumns="False" CellPadding="1" CssClass="GridView_1" 
                                    GridLines="None" PageSize="5" Width="100%" AllowPaging="true">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:BoundField DataField="No_Solicitud" HeaderText="No" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="Importe" HeaderText="Importe" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:c}" />
                                        <asp:BoundField DataField="Justificacion" HeaderText="Justificaci&oacute;n" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="Justificacion_Solicitud" HeaderText="Justificaci&oacute;n<br />Solicitud" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HtmlEncode="false" />
                                        <asp:BoundField DataField="Estatus" HeaderText="Estatus" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="Tipo_Operacion" HeaderText="Operaci&oacute;n" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="Codigo1" HeaderText="C&oacute;digo<br />Origen" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HtmlEncode="false" />
                                        <asp:BoundField DataField="Area_Funcional_Origen" HeaderText="Area<br />Origen" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HtmlEncode="false" />
                                        <asp:BoundField DataField="Programa_Origen" HeaderText="Programa<br />Origen" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HtmlEncode="false" />
                                        <asp:BoundField DataField="Partida_Origen" HeaderText="Partida<br />Origen" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HtmlEncode="false" />
                                        <asp:BoundField DataField="Unidad_Responsable_Origen" HeaderText="U. Responsable<br />Origen" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HtmlEncode="false" />
                                        <asp:BoundField DataField="Financiamiento_Origen" HeaderText="Financiamiento<br />Origen" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HtmlEncode="false" />
                                        <asp:BoundField DataField="Capitulo_Origen" HeaderText="Cap&iacute;tulo<br />Origen" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HtmlEncode="false" />
                                        <asp:BoundField DataField="Codigo2" HeaderText="C&oacute;digo<br />Destino" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HtmlEncode="false" />
                                        <asp:BoundField DataField="Area_Funcional_Destino" HeaderText="Area<br />Destino" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HtmlEncode="false" />
                                        <asp:BoundField DataField="Programa_Destino" HeaderText="Programa<br />Destino" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HtmlEncode="false" />
                                        <asp:BoundField DataField="Partida_Destino" HeaderText="Partida<br />Destino" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HtmlEncode="false" />
                                        <asp:BoundField DataField="Unidad_Responsable_Destino" HeaderText="U. Responsable<br />Destino" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HtmlEncode="false" />
                                        <asp:BoundField DataField="Financiamiento_Destino" HeaderText="Financiamiento<br />Destino" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HtmlEncode="false" />
                                        <asp:BoundField DataField="Capitulo_Destino" HeaderText="Cap&iacute;tulo<br />Destino" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HtmlEncode="false" />
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

