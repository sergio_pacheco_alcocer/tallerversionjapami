﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Movimiento_Presupuestal_Ingresos.Negocio;
using JAPAMI.Catalogo_SAP_Fuente_Financiamiento.Negocio;
using JAPAMI.Area_Funcional.Negocio;
using JAPAMI.Sap_Partida_Generica.Negocio;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Autorizar_Traspaso_Presupuestal_Inversiones.Negocio;
using JAPAMI.Catalogo_Compras_Presupuesto_Dependencias.Negocio;
using JAPAMI.SAP_Operacion_Departamento_Presupuesto.Negocio;
using JAPAMI.Catalogo_Compras_Partidas.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Empleados.Negocios;

public partial class paginas_Presupuestos_Frm_Ope_Psp_Autorizar_Movimiento_Presupueso_Inversiones : System.Web.UI.Page
{
    #region (Page Load)
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //test();
            //Refresca la sesion del usuario logeado en el sistema
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            //Valida que existe un usuario logueado en el sistema
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

            if (!IsPostBack)
            {
                Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                ViewState["SortDirection"] = "ASC";
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion

    #region(Metodos)

    #region (Metodos Generales)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Inicializa_Controles
    /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda
    ///               realizar diferentes operaciones
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramirez Aguilera
    /// FECHA_CREO  : 21-Octubre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Inicializa_Controles()
    {
        try
        {
            Limpiar_Controles(); //Limpia los controles del forma
            Habilitar_Controles("Inicial");//Inicializa todos los controles
            Consulta_Movimiento_Presupuestal();//busca toda la informacion de las operaciones en la basae de datos
            Tr_Datos_Movimientos.Visible = false;
            Tr_Grid_Movimientos.Visible = true;
        }
        catch (Exception ex)
        {
            throw new Exception("Inicializa_Controles " + ex.Message.ToString());
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Controles
    /// DESCRIPCION : Limpia los controles que se encuentran en la forma
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramirez Aguilera
    /// FECHA_CREO  : 21-Octubre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Limpiar_Controles()
    {
        try
        {
            Txt_Unidad_Responsable_Origen.Text = "";
            Txt_Fuente_Financiamiento_Origen.Text = "";
            Txt_Programa_Origen.Text = "";
            Txt_Partida_Origen.Text = "";

            Txt_Enero.Text = "";
            Txt_Febrero.Text = "";
            Txt_Marzo.Text = "";
            Txt_Abril.Text = "";
            Txt_Mayo.Text = "";
            Txt_Junio.Text = "";
            Txt_Julio.Text = "";
            Txt_Agosto.Text = "";
            Txt_Septiembre.Text = "";
            Txt_Octubre.Text = "";
            Txt_Noviembre.Text = "";
            Txt_Diciembre.Text = "";

            Txt_Unidad_Responsable_Destino.Text = "";
            Txt_Fuente_Financiamiento_Destino.Text = "";
            Txt_Programa_Destino.Text = "";
            Txt_Partida_Destino.Text = "";

            //Lbl_Disp_Ene_Destino.Text = "";
            //Lbl_Disp_Feb_Destino.Text = "";
            //Lbl_Disp_Mar_Destino.Text = "";
            //Lbl_Disp_Abr_Destino.Text = "";
            //Lbl_Disp_May_Destino.Text = "";
            //Lbl_Disp_Jun_Destino.Text = "";
            //Lbl_Disp_Jul_Destino.Text = "";
            //Lbl_Disp_Ago_Destino.Text = "";
            //Lbl_Disp_Sep_Destino.Text = "";
            //Lbl_Disp_Oct_Destino.Text = "";
            //Lbl_Disp_Nov_Destino.Text = "";
            //Lbl_Disp_Dic_Destino.Text = "";

            Txt_Enero_Destino.Text = "";
            Txt_Febrero_Destino.Text = "";
            Txt_Marzo_Destino.Text = "";
            Txt_Abril_Destino.Text = "";
            Txt_Mayo_Destino.Text = "";
            Txt_Junio_Destino.Text = "";
            Txt_Julio_Destino.Text = "";
            Txt_Agosto_Destino.Text = "";
            Txt_Septiembre_Destino.Text = "";
            Txt_Octubre_Destino.Text = "";
            Txt_Noviembre_Destino.Text = "";
            Txt_Diciembre_Destino.Text = "";

            Txt_Importe.Text = "";
            Txt_No_Solicitud.Text = "";
            Txt_Justidicacion_Actual.Text = "";
            Txt_Justificacion_Solicitud.Text = "";
            Cmb_Tipo_Estatus.SelectedIndex = 0;
            Cmb_Operacion.SelectedIndex = -1;
            Txt_Busqueda_Movimiento_Presupuestal.Text = "";
        }
        catch (Exception ex)
        {
            throw new Exception("Limpia_Controles " + ex.Message.ToString());
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Habilitar_Controles
    /// DESCRIPCION : Habilita y Deshabilita los controles de la forma para prepara la página
    ///               para a siguiente operación
    /// PARAMETROS  : 1.- Operacion: Indica la operación que se desea realizar por parte del usuario
    ///               si es una alta, modificacion
    ///                           
    /// CREO        : Hugo Enrique Ramirez Aguilera
    /// FECHA_CREO  : 21-Octubre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Habilitar_Controles(String Operacion)
    {
        Boolean Habilitado; ///Indica si el control de la forma va hacer habilitado para utilización del usuario
        try
        {
            Habilitado = false;
            switch (Operacion)
            {
                case "Inicial":
                    Habilitado = false;
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Salir";

                    Btn_Modificar.CausesValidation = false;

                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";

                    Configuracion_Acceso("Frm_Ope_Psp_Autorizar_Movimiento_Presupueso_Inversiones.aspx");
                    break;

                case "Modificar":
                    Habilitado = true;

                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Salir.ToolTip = "Cancelar";

                    Btn_Modificar.Visible = true;

                    Btn_Modificar.CausesValidation = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                    break;
            }

            Txt_Justificacion_Solicitud.Enabled = Habilitado;
            Cmb_Tipo_Estatus.Enabled = Habilitado;
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Btn_Buscar_Movimiento_Presupuestal.Enabled = !Habilitado;
            Txt_Busqueda_Movimiento_Presupuestal.Enabled = !Habilitado;
        }
        catch (Exception ex)
        {
            throw new Exception("Habilitar_Controles " + ex.Message.ToString(), ex);
        }
    }
    #endregion

    #region(Control Acceso Pagina)
    /// ******************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// PARÁMETROS  :
    /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
    /// FECHA CREÓ  : 12/Octubre/2011 
    /// USUARIO MODIFICO  :
    /// FECHA MODIFICO    :
    /// CAUSA MODIFICACIÓN:
    /// ******************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            Botones.Add(Btn_Modificar);

            Botones.Add(Btn_Buscar_Movimiento_Presupuestal);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Es_Numero
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS  : Cadena.- El dato a evaluar si es numerico.
    /// CREO        : Hugo Enrique Ramirez Aguilera
    /// FECHA_CREO  : 24/Octubre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }

    #endregion

    #region (Método Consulta)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Movimiento_Presupuestal
    /// DESCRIPCION : Consulta los movimientos que estan dadas de alta en la BD
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramirez Aguilera
    /// FECHA_CREO  : 21-Octubre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consulta_Movimiento_Presupuestal()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Rs_Consulta_Movimiento_Presupuestal = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();
        DataTable Dt_Movimiento_Presupuestal; //Variable que obtendra los datos de la consulta 

        try
        {
            Rs_Consulta_Movimiento_Presupuestal.P_Estatus = "GENERADO";
            Dt_Movimiento_Presupuestal = Rs_Consulta_Movimiento_Presupuestal.Consulta_Movimiento();
            Session["Consulta_Movimiento_Presupuestal"] = Dt_Movimiento_Presupuestal;
            Grid_Movimiento_Presupuestal.DataSource = (DataTable)Session["Consulta_Movimiento_Presupuestal"];
            Grid_Movimiento_Presupuestal.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception("Movimiento Presupuestal" + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consultar_Grid_Movimientos_Estatus
    /// DESCRIPCION : realiza una consulta para obtener los estatus a los que pertenecen
    /// PARAMETROS  : 1.- String Estatus:  Muestra el estatus al que pertenece(Generada o autorizada)
    /// CREO        : Hugo Enrique Ramirez Aguilera
    /// FECHA_CREO  : 21-Octubre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consultar_Grid_Movimientos_Estatus(String Estatus)
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Cls_Consulta_Estatus = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();//Variable de conexión hacia la capa de Negocios
        DataTable Dt_Movimiento_Presupuestal = null; //Variable que obtendra los datos de la consulta 
        try
        {
            Cls_Consulta_Estatus.P_Estatus = Estatus;
            Dt_Movimiento_Presupuestal = Cls_Consulta_Estatus.Consulta_Movimiento();
            Session["Consulta_Movimiento_Presupuestal"] = Dt_Movimiento_Presupuestal;
            Grid_Movimiento_Presupuestal.DataSource = (DataTable)Session["Consulta_Movimiento_Presupuestal"];
            Grid_Movimiento_Presupuestal.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception("Consultar Movimientos " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consultar_Grid_Movimientos
    /// DESCRIPCION : Realiza una consulta para obtener todos las registros de la tabla
    ///               que pertenescan al estatus de generada
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramirez Aguilera
    /// FECHA_CREO  : 21-Octubre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consultar_Grid_Movimientos()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Cls_Consultar_Traspazo_Presupuestal = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();//Variable de conexión hacia la capa de Negocios
        DataTable Dt_Movimiento_Presupuestal = null; //Variable que obtendra los datos de la consulta 
        try
        {

            Dt_Movimiento_Presupuestal = Cls_Consultar_Traspazo_Presupuestal.Consulta_Movimiento();
            Session["Consulta_Movimiento_Presupuestal"] = Dt_Movimiento_Presupuestal;
            Grid_Movimiento_Presupuestal.DataSource = (DataTable)Session["Consulta_Movimiento_Presupuestal"];
            Grid_Movimiento_Presupuestal.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception("Consultar Movimientos " + ex.Message.ToString(), ex);
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Movimiento
    /// DESCRIPCION : Consulta los movimientos que estan dadas de alta en la BD
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramirez Aguilera
    /// FECHA_CREO  : 17-Noviembre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Cargar_Grid_Comentario(String Indice)
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Consulta = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();
        DataTable Dt_Consulta; //Variable que obtendra los datos de la consulta 

        try
        {
            Consulta.P_No_Solicitud = Indice;
            Dt_Consulta = Consulta.Consulta_Datos_Comentarios();
            Session["Consulta_Comentarios"] = Dt_Consulta;
            Grid_Comentarios.DataSource = (DataTable)Session["Consulta_Comentarios"];
            Grid_Comentarios.DataBind();


            if (Dt_Consulta != null)
            {
                if (Dt_Consulta.Rows.Count > 0)
                {
                    Div_Grid_Comentarios.Visible = true;
                }
                else
                {
                    Div_Grid_Comentarios.Visible = false;
                }
            }
            else
            {
                Div_Grid_Comentarios.Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Movimiento Presupuestal" + ex.Message.ToString(), ex);
        }
    }

    #endregion

    #region(Validacion)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Movimiento_Presupuestal
    /// DESCRIPCION : Validar que se hallan proporcionado todos los datos
    /// CREO        : Hugo Enrique Ramirez Aguilera
    /// FECHA_CREO  : 21-Octubre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar_Movimiento_Presupuestal()
    {
        String Espacios_Blanco;
        Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
        Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
        Espacios_Blanco = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        if (Cmb_Tipo_Estatus.SelectedValue == "< SELECCIONE >")
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + " +El estatus es un dato requerido por el sistema. <br>";
            Datos_Validos = false;
        }

        if ((Cmb_Tipo_Estatus.SelectedValue == "RECHAZADO") && (Txt_Justificacion_Solicitud.Text == ""))
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + " +El comentario correspondiente. <br>";
            Datos_Validos = false;
        }
        return Datos_Validos;
    }

    #endregion

    #endregion

    #region (Eventos)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Tipo_Estatus_Movimiento_SelectedIndexChanged
    ///DESCRIPCIÓN: el escoger algun estatus este realizara una busqueda y lo cargara en 
    ///             el datagrid
    ///PARAMETROS: 
    ///CREO: Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO: 24/Octubre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************        
    protected void Cmb_Tipo_Estatus_Movimiento_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Psp_Autorizar_Solicitud_Traspaso_Presupuestal_Inversiones_Negocio Cls_Estatus = new Cls_Ope_Psp_Autorizar_Solicitud_Traspaso_Presupuestal_Inversiones_Negocio();
        DataTable Dt_Estatus = new DataTable();
        try
        {
            String Valor_Selecionado;
            Valor_Selecionado = Cmb_Tipo_Estatus.SelectedValue;
            if ((Valor_Selecionado == "AUTORIZADO") || (Valor_Selecionado == "< SELECCIONE ESTATUS >"))
            {
                Txt_Justificacion_Solicitud.ReadOnly = true;
            }
            if (Valor_Selecionado == "RECHAZADO")
            {
                Txt_Justificacion_Solicitud.Enabled = true;
                Txt_Justificacion_Solicitud.Text = "";
                Txt_Justificacion_Solicitud.ReadOnly = false;
                this.SetFocus(Txt_Justificacion_Solicitud);
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
    ///DESCRIPCIÓN: Habilita las cajas de texto necesarias para crear un Nuevo Movimiento
    ///             se convierte en dar alta cuando oprimimos Nuevo y dar alta  Crea un registro  
    ///                de un movimiento presupuestal en la base de datos
    ///PARAMETROS: 
    ///CREO: Hugo Enrique Ramirez Aguilera
    ///FECHA_CREO: 25/Octubre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {

        try
        {

        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
    ///DESCRIPCIÓN: Habilita las cajas de texto necesarias para poder Modificar la informacion,
    ///             se convierte en actualizar cuando oprimimos Modificar y se actualiza el registro 
    ///             en la base de datos
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  17/Noviembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        Cls_Ope_Psp_Autorizar_Solicitud_Traspaso_Presupuestal_Inversiones_Negocio Rs_Alta_Traspaso = new Cls_Ope_Psp_Autorizar_Solicitud_Traspaso_Presupuestal_Inversiones_Negocio();
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Rs_Alta_Comentario = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();
        Cls_Ope_Psp_Manejo_Presupuesto Rs_Traspaso_Presupuestal = new Cls_Ope_Psp_Manejo_Presupuesto();
        DataTable Dt_Consultar = new DataTable();
        DataTable Dt_Movimiento = new DataTable();
        DataTable Dt_Partidas = new DataTable();
        Double Disponible = 0.00;
        Double Importe = 0.00;
        Boolean Valido = true;
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            if (Btn_Modificar.ToolTip == "Modificar")
            {

                if (Grid_Movimiento_Presupuestal.SelectedIndex > -1)
                {
                    Habilitar_Controles("Modificar"); //Habilita los controles para la introducción de datos por parte del usuario
                }
                else
                {
                    Lbl_Mensaje_Error.Text = "Seleccione un movimiento de la tabla!";
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
            else
            {
                //Valida si todos los campos requeridos estan llenos si es así da de alta los datos en la base de datos
                if (Validar_Movimiento_Presupuestal())
                {
                    Rs_Alta_Traspaso.P_Estatus = Cmb_Tipo_Estatus.SelectedValue;

                    if (Rs_Alta_Traspaso.P_Estatus.Trim().Equals("AUTORIZADO"))
                    {
                        if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("TRASPASO") || Cmb_Operacion.SelectedItem.Value.Trim().Equals("REDUCIR"))
                        {
                            Rs_Alta_Comentario.P_No_Solicitud = Txt_No_Solicitud.Text.Trim();
                            Dt_Movimiento = Rs_Alta_Comentario.Consulta_Movimiento();


                            Rs_Alta_Comentario.P_Dependencia_ID_Busqueda = Dt_Movimiento.Rows[0][Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Dependencia_Id].ToString().Trim();
                            Rs_Alta_Comentario.P_Area_Funcional_ID = Dt_Movimiento.Rows[0][Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Area_Funcional_Id].ToString().Trim();
                            Rs_Alta_Comentario.P_Programa_ID = Dt_Movimiento.Rows[0][Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Programa_Id].ToString().Trim();
                            Rs_Alta_Comentario.P_Partida_Especifica_ID = Dt_Movimiento.Rows[0][Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Partida_Id].ToString().Trim();
                            Rs_Alta_Comentario.P_Fuente_Financiamiento_ID = Dt_Movimiento.Rows[0][Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Fuente_Financiamiento_Id].ToString().Trim();
                            
                            Rs_Alta_Comentario.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                            Rs_Alta_Comentario.P_Tipo_Usuario = "Administrador";
                            Dt_Partidas = Rs_Alta_Comentario.Consultar_Partidas_Especificas();
                            if (Dt_Partidas != null && Dt_Partidas.Rows.Count > 0)
                            {
                                Disponible = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas.Rows[0]["DISPONIBLE"].ToString().Trim()) ? "0" : Dt_Partidas.Rows[0]["DISPONIBLE"]);
                                Importe = Convert.ToDouble(String.IsNullOrEmpty(Txt_Importe.Text.Trim()) ? "0" : Txt_Importe.Text.Trim());

                                if (Disponible < Importe)
                                {
                                    Valido = false;
                                }
                            }
                        }
                    }

                    if (Valido)
                    {
                        Rs_Alta_Traspaso.P_Justificacion = Txt_Justificacion_Solicitud.Text.Trim();
                        Rs_Alta_Traspaso.P_Numero_Solicitud = Txt_No_Solicitud.Text.Trim();
                        Rs_Alta_Traspaso.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                        Rs_Alta_Traspaso.P_Comentario = Txt_Justificacion_Solicitud.Text.Trim();

                        Rs_Alta_Traspaso.Modificar_Autorizacion_Traspaso();

                        Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "AUTORIZAR TRASPASO", "alert('La modificacion fue exitosa');", true);
                        Tr_Datos_Movimientos.Visible = false;
                        Tr_Grid_Movimientos.Visible = true;
                        Grid_Movimiento_Presupuestal.Visible = true;
                        Div_Grid.Visible = true;
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "AUTORIZAR TRASPASO", "alert('Esta operación no puede ser autorizada pues ya no se cuenta con el disponible suficiente para realizar la operación');", true);
                    }
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Movimiento_Presupuestal_Click
    ///DESCRIPCIÓN: busca el movimiento y lo carga en el grid
    ///PARAMETROS: 
    ///CREO: Hugo Enrique Ramirez Aguilera
    ///FECHA_CREO: 21/Octubre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_Movimiento_Presupuestal_Click(object sender, ImageClickEventArgs e)
    {
        Cls_Ope_Psp_Autorizar_Solicitud_Traspaso_Presupuestal_Inversiones_Negocio Cls_Buscar_Traspaso = new Cls_Ope_Psp_Autorizar_Solicitud_Traspaso_Presupuestal_Inversiones_Negocio();
        DataTable Dt_Busqueda = new DataTable();
        Boolean Resultado_Numerico = false;//guardara si el resultado es numero o no

        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            Resultado_Numerico = Es_Numero(Txt_Busqueda_Movimiento_Presupuestal.Text.Trim());//para saber si es un numero
            if (!string.IsNullOrEmpty(Txt_Busqueda_Movimiento_Presupuestal.Text.Trim()))
            {
                if (Resultado_Numerico == true)
                {
                    Cls_Buscar_Traspaso.P_Numero_Solicitud = Txt_Busqueda_Movimiento_Presupuestal.Text.ToUpper().Trim();
                }
                else
                {
                    Cls_Buscar_Traspaso.P_Estatus = Txt_Busqueda_Movimiento_Presupuestal.Text.ToUpper().Trim();
                }
                Cls_Buscar_Traspaso.P_Estatus = "GENERADA";
                Dt_Busqueda = Cls_Buscar_Traspaso.Consulta_Autorizacion_Traspaso();
                Cargar_Grid_Movimiento(Dt_Busqueda);
                Grid_Movimiento_Presupuestal.Visible = true;
                Div_Grid.Visible = true;
                if (Grid_Movimiento_Presupuestal.Rows.Count == 0 && !string.IsNullOrEmpty(Txt_Busqueda_Movimiento_Presupuestal.Text))
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "No se encontraron registros sobre el movimiento realizado pruebe otra vez";
                }
                Limpiar_Controles();

            }
            else
            {
                Inicializa_Controles();
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN:  Cancela la operacion actual qye se este realizando
    ///PARAMETROS: 
    ///CREO: Hugo Enrique Ramirez Aguilera
    ///FECHA_CREO: 21/Octubre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if ((Btn_Salir.ToolTip == "Salir") && (Grid_Movimiento_Presupuestal.Visible == true))
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else if (Grid_Movimiento_Presupuestal.Visible == false)
            {
                Inicializa_Controles();
                Grid_Movimiento_Presupuestal.SelectedIndex = -1;
            }
            else
            {   //Habilita los controles para la siguiente operación del usuario en el catálogo
                Inicializa_Controles();
                Grid_Movimiento_Presupuestal.SelectedIndex = -1;
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion

    #region(Grid)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Movimiento_Presupuestal_PageIndexChanging
    ///DESCRIPCIÓN:Cambiar de Pagina de la tabla de las operaciones de solicitud de transferencia
    ///CREO: Hugo Enrique Ramirez Aguilera
    ///FECHA_CREO: 21/Octubre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Movimiento_Presupuestal_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Grid_Movimiento_Presupuestal.PageIndex = e.NewPageIndex; //Asigna la nueva página que selecciono el usuario
            Consultar_Grid_Movimientos();
            Grid_Movimiento_Presupuestal.SelectedIndex = -1;
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Movimiento_Presupuestal_SelectedIndexChanged
    /// DESCRIPCION : Consulta los datos de los movimientos seleccionada por el usuario
    /// CREO        : Hugo Enrique Ramirez Aguilera
    /// FECHA_CREO  : 21-Octubre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Grid_Movimiento_Presupuestal_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Rs_Consulta = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();//Variable de conexión hacia la capa de Negocios
        Cls_Ope_Psp_Autorizar_Solicitud_Traspaso_Presupuestal_Inversiones_Negocio Negocio = new Cls_Ope_Psp_Autorizar_Solicitud_Traspaso_Presupuestal_Inversiones_Negocio();
        DataTable Dt_Partidas = new DataTable();
        DataTable Dt_Partidas_Detalles = new DataTable();
        DataTable Dt_Movimiento;//Variable que obtendra los datos de la consulta 
        DataTable Dt_Datos_Movimientos = new DataTable();
        String Tipo_Operacion;

        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Limpiar_Controles(); //Limpia los controles del la forma para poder agregar los valores del registro seleccionado

            Rs_Consulta.P_No_Solicitud = Grid_Movimiento_Presupuestal.SelectedRow.Cells[1].Text;
            Dt_Movimiento = Rs_Consulta.Consulta_Movimiento();//Consulta todos los datos de los movimientos que fue seleccionada por el usuario

            Tr_Grid_Movimientos.Visible = false;
            Tr_Datos_Movimientos.Visible = true;
            Btn_Buscar_Movimiento_Presupuestal.Enabled = false;
            Txt_Busqueda_Movimiento_Presupuestal.Enabled = false;

            if (Dt_Movimiento.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Movimiento.Rows)
                {
                    if (!String.IsNullOrEmpty(Registro[Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud].ToString()))
                    {
                        Txt_No_Solicitud.Text = (Registro[Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud].ToString());

                        Cargar_Grid_Comentario(Txt_No_Solicitud.Text);
                    }
                    if (!String.IsNullOrEmpty(Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Codigo1].ToString()))
                    {
                        Txt_Codigo1.Text = (Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Codigo1].ToString());

                        Negocio.P_Numero_Solicitud = Txt_No_Solicitud.Text.Trim();
                        Dt_Datos_Movimientos = Negocio.Consultar_Autorizacion_Movimiento();

                        if (Dt_Datos_Movimientos != null)
                        {
                            if (Dt_Datos_Movimientos.Rows.Count > 0)
                            {

                                if (!String.IsNullOrEmpty(Dt_Datos_Movimientos.Rows[0]["DEPENDENCIA_ORIGEN"].ToString().Trim()))
                                {
                                    Txt_Unidad_Responsable_Origen.Text = Dt_Datos_Movimientos.Rows[0]["DEPENDENCIA_ORIGEN"].ToString().Trim();
                                }

                                if (!String.IsNullOrEmpty(Dt_Datos_Movimientos.Rows[0]["FTE_FINANCIAMIENTO_ORIGEN"].ToString().Trim()))
                                {
                                    Txt_Fuente_Financiamiento_Origen.Text = Dt_Datos_Movimientos.Rows[0]["FTE_FINANCIAMIENTO_ORIGEN"].ToString().Trim();
                                }

                                if (!String.IsNullOrEmpty(Dt_Datos_Movimientos.Rows[0]["PROGRAMA_ORIGEN"].ToString().Trim()))
                                {
                                    Txt_Programa_Origen.Text = Dt_Datos_Movimientos.Rows[0]["PROGRAMA_ORIGEN"].ToString().Trim();
                                }

                                if (!String.IsNullOrEmpty(Dt_Datos_Movimientos.Rows[0]["PARTIDA_ORIGEN"].ToString().Trim()))
                                {
                                    Txt_Partida_Origen.Text = Dt_Datos_Movimientos.Rows[0]["PARTIDA_ORIGEN"].ToString().Trim();
                                }

                                if (!String.IsNullOrEmpty(Dt_Datos_Movimientos.Rows[0]["DEPENDENCIA_DESTINO"].ToString().Trim()))
                                {
                                    Txt_Unidad_Responsable_Destino.Text = Dt_Datos_Movimientos.Rows[0]["DEPENDENCIA_DESTINO"].ToString().Trim();
                                }

                                if (!String.IsNullOrEmpty(Dt_Datos_Movimientos.Rows[0]["FTE_FINANCIAMIENTO_DESTINO"].ToString().Trim()))
                                {
                                    Txt_Fuente_Financiamiento_Destino.Text = Dt_Datos_Movimientos.Rows[0]["FTE_FINANCIAMIENTO_DESTINO"].ToString().Trim();
                                }

                                if (!String.IsNullOrEmpty(Dt_Datos_Movimientos.Rows[0]["PROGRAMA_DESTINO"].ToString().Trim()))
                                {
                                    Txt_Programa_Destino.Text = Dt_Datos_Movimientos.Rows[0]["PROGRAMA_DESTINO"].ToString().Trim();
                                }

                                if (!String.IsNullOrEmpty(Dt_Datos_Movimientos.Rows[0]["PARTIDA_DESTINO"].ToString().Trim()))
                                {
                                    Txt_Partida_Destino.Text = Dt_Datos_Movimientos.Rows[0]["PARTIDA_DESTINO"].ToString().Trim();
                                }
                            }
                        }

                        Rs_Consulta.P_No_Solicitud = Txt_No_Solicitud.Text.Trim();
                        Dt_Partidas_Detalles = Rs_Consulta.Consulta_Detalles_Importes();

                        if (Dt_Partidas_Detalles != null)
                        {
                            if (Dt_Partidas_Detalles.Rows.Count > 0)
                            {
                                foreach (DataRow Dr in Dt_Partidas_Detalles.Rows)
                                {
                                    if (Dr["TIPO"].ToString().Trim().Equals("Partida_Origen") || Dr["TIPO"].ToString().Trim().Equals("Partida_Origen_Nueva"))
                                    {
                                        Txt_Enero.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_ENERO"]);
                                        Txt_Febrero.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_FEBRERO"]);
                                        Txt_Marzo.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_MARZO"]);
                                        Txt_Abril.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_ABRIL"]);
                                        Txt_Mayo.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_MAYO"]);
                                        Txt_Junio.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_JUNIO"]);
                                        Txt_Julio.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_JULIO"]);
                                        Txt_Agosto.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_AGOSTO"]);
                                        Txt_Septiembre.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_SEPTIEMBRE"]);
                                        Txt_Octubre.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_OCTUBRE"]);
                                        Txt_Noviembre.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_NOVIEMBRE"]);
                                        Txt_Diciembre.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_DICIEMBRE"]);
                                    }
                                    else if (Dr["TIPO"].ToString().Trim().Equals("Partida_Destino") || Dr["TIPO"].ToString().Trim().Equals("Partida_Destino_Nueva"))
                                    {
                                        Txt_Enero_Destino.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_ENERO"]);
                                        Txt_Febrero_Destino.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_FEBRERO"]);
                                        Txt_Marzo_Destino.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_MARZO"]);
                                        Txt_Abril_Destino.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_ABRIL"]);
                                        Txt_Mayo_Destino.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_MAYO"]);
                                        Txt_Junio_Destino.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_JUNIO"]);
                                        Txt_Julio_Destino.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_JULIO"]);
                                        Txt_Agosto_Destino.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_AGOSTO"]);
                                        Txt_Septiembre_Destino.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_SEPTIEMBRE"]);
                                        Txt_Octubre_Destino.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_OCTUBRE"]);
                                        Txt_Noviembre_Destino.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_NOVIEMBRE"]);
                                        Txt_Diciembre_Destino.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_DICIEMBRE"]);
                                    }
                                }
                            }
                        }
                    }

                    if (!String.IsNullOrEmpty(Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Importe].ToString()))
                    {
                        Txt_Importe.Text = String.Format("{0:#,###,##0.00}", Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Importe]);
                    }

                    if (!String.IsNullOrEmpty(Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Justificacion].ToString()))
                    {
                        Txt_Justidicacion_Actual.Text = (Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Justificacion].ToString());
                    }
                    if (!String.IsNullOrEmpty(Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Tipo_Operacion].ToString()))
                    {
                        Tipo_Operacion = (Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Tipo_Operacion].ToString());
                        Cmb_Operacion.SelectedIndex = Cmb_Operacion.Items.IndexOf(Cmb_Operacion.Items.FindByValue(Tipo_Operacion));

                        if (Tipo_Operacion.Equals("TRASPASO"))
                        {
                            Div_Partida_Destino.Visible = true;
                        }
                        else
                        {
                            Div_Partida_Destino.Visible = false;
                        }
                    }

                    if (!String.IsNullOrEmpty(Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Codigo2].ToString()))
                    {
                        Div_Partida_Destino.Visible = true;
                        Txt_Codigo2.Text = (Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Codigo2].ToString());
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    /// ********************************************************************************
    /// NOMBRE: Grid_Movimiento_Presupuestal_Sorting
    /// 
    /// DESCRIPCIÓN: Ordena las columnas en orden ascendente o descendente.
    /// 
    /// CREÓ:   Hugo Enrique Ramirez Aguilera
    /// FECHA CREÓ: 21-Octubre-2011
    /// MODIFICÓ:
    /// FECHA MODIFICÓ:
    /// CAUSA MODIFICACIÓN:
    /// **********************************************************************************
    protected void Grid_Movimiento_Presupuestal_Sorting(object sender, GridViewSortEventArgs e)
    {
        //Se consultan los movimientos que actualmente se encuentran registradas en el sistema.
        Consultar_Grid_Movimientos();
        DataTable Dt_Movimiento_Presupuestal = (Grid_Movimiento_Presupuestal.DataSource as DataTable);

        if (Dt_Movimiento_Presupuestal != null)
        {
            DataView Dv_Movimiento_Presupuestal = new DataView(Dt_Movimiento_Presupuestal);
            String Orden = ViewState["SortDirection"].ToString();

            if (Orden.Equals("ASC"))
            {
                Dv_Movimiento_Presupuestal.Sort = e.SortExpression + " " + "DESC";
                ViewState["SortDirection"] = "DESC";
            }
            else
            {
                Dv_Movimiento_Presupuestal.Sort = e.SortExpression + " " + "ASC";
                ViewState["SortDirection"] = "ASC";
            }

            Grid_Movimiento_Presupuestal.DataSource = Dv_Movimiento_Presupuestal;
            Grid_Movimiento_Presupuestal.DataBind();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cargar_Grid_Bancos
    /// DESCRIPCION : Carga los Movimientos registrados en el sistema. 
    ///               registradas en el sistema.
    /// CREO        : Hugo Enrique Ramirez Aguilera
    /// FECHA_CREO  : 21-Octubre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Cargar_Grid_Movimiento(DataTable Dt_Busqueda_Movimiento)
    {
        try
        {
            Grid_Movimiento_Presupuestal.DataSource = Dt_Busqueda_Movimiento;
            Grid_Movimiento_Presupuestal.DataBind();
            Grid_Movimiento_Presupuestal.SelectedIndex = -1;
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al cargar los movimientos en la tabla que los listara. Error: [" + Ex.Message + "]");
        }
    }

    #endregion
}
