﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using CarlosAg.ExcelXmlWriter;
using System.Text;
using JAPAMI.Rpt_Psp_Estado_Analitico_Ingresos.Negocio;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using JAPAMI.Rpt_Psp_Movimeitnos_Presupuesto_Ingresos.Negocio;
using JAPAMI.Reporte_Presupuesto_Egresos.Negocio;

public partial class paginas_Presupuestos_Frm_Rpt_Psp_Reporte_Estado_Analitico_Ingresos : System.Web.UI.Page
{
    #region (Page Load)

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Page_Load
        ///DESCRIPCIÓN          : Inicio de la pagina
        ///PARAMETROS           : 
        ///CREO                 : Ramón Baeza Yépez
        ///FECHA_CREO           : 11/Junio/2013
        ///*****************************************************************************************************************
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
                if (!IsPostBack)
                {
                    Reporte_Inicio();
                    ViewState["SortDirection"] = "DESC";
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error en el inicio del reporte analitico de ingresos. Error [" + Ex.Message + "]");
            }
        }
    #endregion

    #region METODOS
        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Reporte_Inicio
        ///DESCRIPCIÓN          : Metodo de inicio de la página
        ///PARAMETROS           : 
        ///CREO                 : Ramón Baeza Yépez
        ///FECHA_CREO           : 11/Junio/2013
        ///*****************************************************************************************************************
        private void Reporte_Inicio()
        {
            try
            {
                Div_Contenedor_Msj_Error.Visible = false;
                Limpiar_Controles("Todo");
                Cmb_Anio.SelectedIndex = -1;
                Llenar_Combo_Anios();           
            }
            catch (Exception Ex)
            {
                throw new Exception("Error en el inicio del reporte analitico de ingresos. Error [" + Ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Limpiar_Controles
        ///DESCRIPCIÓN          : Metodo para limpiar los controles del formulario
        ///PARAMETROS           1 Accion: para indicar que parte del codigo limpiara 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 02/Abril/2012
        ///*****************************************************************************************************************
        private void Limpiar_Controles(String Accion)
        {
            try
            {
                switch (Accion)
                {
                    case "Todo":
                        Lbl_Ecabezado_Mensaje.Text = "";
                        Lbl_Mensaje_Error.Text = "";
                        Cmb_Anio.SelectedIndex = -1;
                        break;
                    case "Error":
                        Lbl_Ecabezado_Mensaje.Text = "";
                        Lbl_Mensaje_Error.Text = "";
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al limpiar los controles Error [" + Ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Anios
        ///DESCRIPCIÓN          : Metodo para llenar el combo de los años
        ///PARAMETROS           : 
        ///CREO                 : Ramón Baeza Yépez
        ///FECHA_CREO           : 11/Junio/2013
        ///*****************************************************************************************************************
        private void Llenar_Combo_Anios()
        {
            Cls_Rpt_Psp_Estado_Analitico_Ingresos_Negocio Obj_Ingresos = new Cls_Rpt_Psp_Estado_Analitico_Ingresos_Negocio();
            DataTable Dt_Anios = new DataTable(); //Para almacenar los datos de los tipos de nominas

            try
            {
                Cmb_Anio.Items.Clear();

                Dt_Anios = Obj_Ingresos.Consultar_Anios();

                Cmb_Anio.DataValueField = Ope_Psp_Pronostico_Ingresos.Campo_Anio;
                Cmb_Anio.DataTextField = Ope_Psp_Pronostico_Ingresos.Campo_Anio;
                Cmb_Anio.DataSource = Dt_Anios;
                Cmb_Anio.DataBind();

                Cmb_Anio.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));

                Cmb_Anio.SelectedIndex = Cmb_Anio.Items.IndexOf(Cmb_Anio.Items.FindByValue(String.Format("{0:yyyy}", DateTime.Now)));
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al llenar el combo de anios: Error[" + Ex.Message + "]");
            }
        }
        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Validar_Campos
        ///DESCRIPCIÓN          : Metodo para validar los campos del formulario
        ///PARAMETROS           : 
        ///CREO                 : Ramón Baeza Yépez
        ///FECHA_CREO           : 02/Abril/2012
        ///*****************************************************************************************************************
        private Boolean Validar_Campos()
        {
            Boolean Datos_Validos = true;
            Limpiar_Controles("Error");
            Lbl_Ecabezado_Mensaje.Text = "Favor de:";
            try
            {
                if (Cmb_Anio.SelectedIndex <= 0)
                {
                    Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Seleccionar un año <br />";
                    Datos_Validos = false;
                }
                return Datos_Validos;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al valodar los campos del formulario Error [" + Ex.Message + "]");
            }
        }
        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Agrupar_Rubro
        ///DESCRIPCIÓN          : Agrupamos los datos de los rubro
        ///PARAMETROS           : 
        ///CREO                 : Ramón Baeza Yépez
        ///FECHA_CREO           : 02/Junio/2012
        ///*****************************************************************************************************************
        private DataTable Agrupar_Rubro(DataTable Dt_Datos)
        {
            DataRow Nueva_Linea;
            DataRow Linea;
            int Cont_Rubro = 0;
            String Rubro = String.Empty;
            String Clave_Rubro = String.Empty;
            Double LEY_INGRESOS_ESTIMADA = 0;
            Double MODIFICADO_A = 0;
            Double DEVENGADO = 0;
            Double RECAUDADO_B = 0;
            Double AVANCE_DE_RECAUDACION_B_A = 0;
            try
            {
                for (int Cont_Datos = 0; Cont_Datos < Dt_Datos.Rows.Count;Cont_Datos++ )
                {
                    Linea = Dt_Datos.Rows[Cont_Datos];
                    //inicializamos la clve
                    if (String.IsNullOrEmpty(Clave_Rubro))
                    {
                        Clave_Rubro = Linea["Clave_Rubro"].ToString();
                        Rubro = Linea["Rubro"].ToString();
                        Cont_Rubro = Cont_Datos;
                    }
                    //Cuando sea un nuevo rubro insertmaos los datos del rubro anterior al inicio de sus desgloses
                    if (!Linea["Clave_Rubro"].ToString().Equals(Clave_Rubro))
                    {
                        Nueva_Linea = Dt_Datos.NewRow();
                        Nueva_Linea["Clave_Rubro"] = Clave_Rubro;
                        Nueva_Linea["Rubro"] = Rubro;
                        if (LEY_INGRESOS_ESTIMADA > 0 || MODIFICADO_A > 0 || DEVENGADO > 0 || RECAUDADO_B > 0 || AVANCE_DE_RECAUDACION_B_A > 0)
                        {
                            Nueva_Linea["lEY_INGRESOS_ESTIMADA"] = LEY_INGRESOS_ESTIMADA;
                            Nueva_Linea["MODIFICADO_A"] = MODIFICADO_A;
                            Nueva_Linea["DEVENGADO"] = DEVENGADO;
                            Nueva_Linea["RECAUDADO_B"] = RECAUDADO_B;
                            Nueva_Linea["AVANCE_DE_RECAUDACION_B_A"] = AVANCE_DE_RECAUDACION_B_A;
                        }
                        Dt_Datos.Rows.InsertAt(Nueva_Linea, Cont_Rubro);
                        Cont_Datos++; 
                        Linea = Dt_Datos.Rows[Cont_Datos];
                        Clave_Rubro = Linea["Clave_Rubro"].ToString();
                        Rubro = Linea["Rubro"].ToString();
                        Cont_Rubro = Cont_Datos;
                        LEY_INGRESOS_ESTIMADA = 0;
                        MODIFICADO_A = 0;
                        DEVENGADO = 0;
                        RECAUDADO_B = 0;
                        AVANCE_DE_RECAUDACION_B_A = 0;
                    }
                    if (!String.IsNullOrEmpty(Linea["lEY_INGRESOS_ESTIMADA"].ToString()))
                        LEY_INGRESOS_ESTIMADA += Convert.ToDouble(Linea["lEY_INGRESOS_ESTIMADA"]);
                    if (!String.IsNullOrEmpty(Linea["MODIFICADO_A"].ToString()))
                        MODIFICADO_A += Convert.ToDouble(Linea["MODIFICADO_A"]);
                    if (!String.IsNullOrEmpty(Linea["DEVENGADO"].ToString()))
                        DEVENGADO += Convert.ToDouble(Linea["DEVENGADO"]);
                    if (!String.IsNullOrEmpty(Linea["RECAUDADO_B"].ToString()))
                        RECAUDADO_B += Convert.ToDouble(Linea["RECAUDADO_B"]);
                    if (!String.IsNullOrEmpty(Linea["AVANCE_DE_RECAUDACION_B_A"].ToString()))
                        AVANCE_DE_RECAUDACION_B_A += Convert.ToDouble(Linea["AVANCE_DE_RECAUDACION_B_A"]);
                }
                Nueva_Linea = Dt_Datos.NewRow();
                Nueva_Linea["Clave_Rubro"] = Clave_Rubro;
                Nueva_Linea["Rubro"] = Rubro;
                if (LEY_INGRESOS_ESTIMADA > 0 || MODIFICADO_A > 0 || DEVENGADO > 0 || RECAUDADO_B > 0 || AVANCE_DE_RECAUDACION_B_A > 0)
                {
                    Nueva_Linea["lEY_INGRESOS_ESTIMADA"] = LEY_INGRESOS_ESTIMADA;
                    Nueva_Linea["MODIFICADO_A"] = MODIFICADO_A;
                    Nueva_Linea["DEVENGADO"] = DEVENGADO;
                    Nueva_Linea["RECAUDADO_B"] = RECAUDADO_B;
                    Nueva_Linea["AVANCE_DE_RECAUDACION_B_A"] = AVANCE_DE_RECAUDACION_B_A;
                }
                Dt_Datos.Rows.InsertAt(Nueva_Linea, Cont_Rubro);
                //Obtenemos los totales de los rubros
                LEY_INGRESOS_ESTIMADA = 0;
                MODIFICADO_A = 0;
                DEVENGADO = 0;
                RECAUDADO_B = 0;
                AVANCE_DE_RECAUDACION_B_A = 0;
                foreach (DataRow Concepto in Dt_Datos.Rows)
                {
                    if(String.IsNullOrEmpty(Concepto["Clave_Tipo"].ToString()))
                    {
                        if (!String.IsNullOrEmpty(Concepto["lEY_INGRESOS_ESTIMADA"].ToString()))
                            LEY_INGRESOS_ESTIMADA += Convert.ToDouble(Concepto["lEY_INGRESOS_ESTIMADA"]);
                        if (!String.IsNullOrEmpty(Concepto["MODIFICADO_A"].ToString()))
                            MODIFICADO_A += Convert.ToDouble(Concepto["MODIFICADO_A"]);
                        if (!String.IsNullOrEmpty(Concepto["DEVENGADO"].ToString()))
                            DEVENGADO += Convert.ToDouble(Concepto["DEVENGADO"]);
                        if (!String.IsNullOrEmpty(Concepto["RECAUDADO_B"].ToString()))
                            RECAUDADO_B += Convert.ToDouble(Concepto["RECAUDADO_B"]);
                        if (!String.IsNullOrEmpty(Concepto["AVANCE_DE_RECAUDACION_B_A"].ToString()))
                            AVANCE_DE_RECAUDACION_B_A += Convert.ToDouble(Concepto["AVANCE_DE_RECAUDACION_B_A"]);
                    }
                }
                //insertamos la los totales en una linea de la tabla
                Nueva_Linea = Dt_Datos.NewRow();
                Nueva_Linea["Rubro"] = "PRESUPUESTO DE INGRESOS";
                Nueva_Linea["lEY_INGRESOS_ESTIMADA"] = LEY_INGRESOS_ESTIMADA;
                Nueva_Linea["MODIFICADO_A"] = MODIFICADO_A;
                Nueva_Linea["DEVENGADO"] = DEVENGADO;
                Nueva_Linea["RECAUDADO_B"] = RECAUDADO_B;
                Nueva_Linea["AVANCE_DE_RECAUDACION_B_A"] = AVANCE_DE_RECAUDACION_B_A;
                Dt_Datos.Rows.InsertAt(Nueva_Linea, 0);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al Agrupar_Rubro Error [" + Ex.Message + "]");
            }
            return Dt_Datos;
        }
    #endregion

    #region EVENTOS

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
        ///DESCRIPCIÓN          : Ejecuta la operacion de click en el boton de salir
        ///PARAMETROS           : 
        ///CREO                 : Ramón Baeza Yépez
        ///FECHA_CREO           : 11/Junio/2013
        ///*****************************************************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            if (Btn_Salir.ToolTip.Trim().Equals("Salir"))
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Generar_Reporte_Click
        ///DESCRIPCIÓN          : Ejecuta la operacion de click en el boton de generar
        ///PARAMETROS           : 
        ///CREO                 : Ramón Baeza Yépez
        ///FECHA_CREO           : 11/Junio/2013
        ///*****************************************************************************************************************
        protected void Btn_Generar_Reporte_Click(object sender, EventArgs e)
        {
            Cls_Rpt_Psp_Estado_Analitico_Ingresos_Negocio Negocio = new Cls_Rpt_Psp_Estado_Analitico_Ingresos_Negocio();
            Div_Contenedor_Msj_Error.Visible = false;
            Limpiar_Controles("Error");

            try
            {
                if (Validar_Campos())
                {
                    Negocio.P_Anio = Cmb_Anio.SelectedItem.Text.Trim();
                    Pasar_Datos_A_Excel(Agrupar_Rubro(Negocio.Consultar_Estado()));
                }
                else
                {
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del boton de generar el reporte. Error[" + ex.Message + "]");
            }
        }
    #endregion
    
    #region (Reportes)
        /// *************************************************************************************************************************
        /// Nombre: Pasar_DataTable_A_Excel
        /// Descripción: Pasa DataTable a Excel.
        /// Parámetros: Dt_Reporte.- DataTable que se pasara a excel.
        /// Usuario Creo: Ramón Baeza Yepez.
        /// Fecha Creó: 11/Junio/2013.
        /// *************************************************************************************************************************
        public void Pasar_Datos_A_Excel(DataTable Dt_Datos)
        {
            String Nombre_Archivo = "Estado Analitico de Ingresos_" + DateTime.Now.ToString("dd DE MMMMM DE yyyy").ToUpper() + ".xls";
            DiskFileDestinationOptions m_crDiskFileDestinationOptions = new DiskFileDestinationOptions();
            ReportDocument Reporte = new ReportDocument();
            try
            {
                //Creamos el libro de Excel.
                CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();

                Libro.Properties.Title = "Plantilla de Puestos";
                Libro.Properties.Created = DateTime.Now;
                Libro.Properties.Author = "Plantilla de Puestos - " + Cls_Sessiones.Nombre_Empleado;

                //Creamos una hoja que tendrá el libro.
                CarlosAg.ExcelXmlWriter.Worksheet Hoja;
                //Agregamos un renglón a la hoja de excel.
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon;

                //Creamos el estilo titulo para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Top = Libro.Styles.Add("Top");
                Estilo_Top.Font.FontName = "Arial";
                Estilo_Top.Font.Size = 8;
                Estilo_Top.Font.Bold = true;
                Estilo_Top.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Top.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Top.Font.Color = "#000000";
                Estilo_Top.Interior.Color = "#FF7A3F";
                Estilo_Top.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Top.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                Estilo_Top.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Top.Alignment.WrapText = true;

                //Creamos el estilo titulo para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Header= Libro.Styles.Add("Header");
                Estilo_Header.Font.FontName = "Arial";
                Estilo_Header.Font.Size = 8;
                Estilo_Header.Font.Bold = true;
                Estilo_Header.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Header.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Header.Font.Color = "#000000";
                Estilo_Header.Interior.Color = "#FF7A3F";
                Estilo_Header.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Header.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Header.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Header.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                Estilo_Header.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Header.Alignment.WrapText = true;

                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_1 = Libro.Styles.Add("BodyStyle_1");
                Estilo_Contenido_1.Font.FontName = "Arial";
                Estilo_Contenido_1.Font.Size = 8;
                Estilo_Contenido_1.Font.Bold = true;
                Estilo_Contenido_1.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Contenido_1.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Contenido_1.Font.Color = "#000000";
                Estilo_Contenido_1.Interior.Color = "White";
                Estilo_Contenido_1.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Contenido_1.Alignment.WrapText = true;

                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_2 = Libro.Styles.Add("BodyStyle_2");
                Estilo_Contenido_2.Font.FontName = "Arial";
                Estilo_Contenido_2.Font.Size = 8;
                Estilo_Contenido_2.Font.Bold = false;
                Estilo_Contenido_2.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Estilo_Contenido_2.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Contenido_2.Font.Color = "#000000";
                Estilo_Contenido_2.Interior.Color = "White";
                Estilo_Contenido_2.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Contenido_2.Alignment.WrapText = true;

                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_3 = Libro.Styles.Add("BodyStyle_3");
                Estilo_Contenido_3.Font.FontName = "Arial";
                Estilo_Contenido_3.Font.Size = 8;
                Estilo_Contenido_3.Font.Bold = false;
                Estilo_Contenido_3.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Contenido_3.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Contenido_3.Font.Color = "#000000";
                Estilo_Contenido_3.Interior.Color = "White";
                Estilo_Contenido_3.NumberFormat = "#,###,##0.00";
                Estilo_Contenido_3.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Contenido_3.Alignment.WrapText = true;
                
                for (int Indice_Hojas = 0; Indice_Hojas < 2; Indice_Hojas++)
                {
                    if (Indice_Hojas < 1)
                        Hoja = Libro.Worksheets.Add("EAI");
                    else
                        Hoja = Libro.Worksheets.Add("EAI_Global_impreso");
                    for (Int32 Contador = 0; Contador < 6; Contador++)
                    {
                        if (Contador != 1)
                            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));
                        else
                            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));
                    }

                    Renglon = Hoja.Table.Rows.Add();
                    //SE CARGA LA CABECERA PRINCIPAL DEL ARCHIVO
                    WorksheetCell cell = Renglon.Cells.Add();
                    Renglon.Cells.Clear();
                    cell = Renglon.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.", DataType.String, "Top");
                    cell.MergeAcross = 6;            // Merge two cells together
                    Renglon.AutoFitHeight = true;
                    Renglon = Hoja.Table.Rows.Add();
                    cell = Renglon.Cells.Add("ESTADO ANALÍTICO DE INGRESOS", DataType.String, "Top");
                    cell.MergeAcross = 6;            // Merge two cells together
                    Renglon.AutoFitHeight = true;
                    Renglon = Hoja.Table.Rows.Add();
                    cell = Renglon.Cells.Add("ESTADO ANALÍTICO DE INGRESOS " + DateTime.Now.ToString("dd DE MMMMM DE yyyy").ToUpper(), DataType.String, "Top");
                    cell.MergeAcross = 6;            // Merge two cells together
                    Renglon.AutoFitHeight = true;
                    Renglon = Hoja.Table.Rows.Add();
                    cell = Renglon.Cells.Add("ESTADO ANALÍTICO DE INGRESOS " + DateTime.Now.ToString("dd DE MMMMM DE yyyy").ToUpper(), DataType.String, "Top");
                    cell.MergeAcross = 6;            // Merge two cells together
                    Renglon.AutoFitHeight = true;
                    Hoja.Table.Rows.RemoveAt(Hoja.Table.Rows.Count - 1);
                    Renglon = Hoja.Table.Rows.Add();
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("RUBRO", DataType.String, "Header"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CONCEPTO", DataType.String, "Header"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("LEY DE INGRESOS ESTIMADA", DataType.String, "Header"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MODIFICADO (A)", DataType.String, "Header"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("DEVENGADO", DataType.String, "Header"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("RECAUDADO (B)", DataType.String, "Header"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("AVANCE DE RECAUDACIÓN(B/A)", DataType.String, "Header"));
                    Renglon = Hoja.Table.Rows.Add();

                    //SE CARGAN LOS DATOS...
                    foreach (System.Data.DataRow Fila in Dt_Datos.Rows)
                    {
                        if (String.IsNullOrEmpty(Fila["Tipo"].ToString()) || Fila["Rubro"].ToString().Equals("TOTALES"))
                        {
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                Fila["Clave_Rubro"].ToString(), DataType.String, "BodyStyle_1"));
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(
                                Fila["Rubro"].ToString(), DataType.String, "BodyStyle_2"));
                            if (Indice_Hojas > 0 || Fila["Rubro"].ToString().Equals("PRESUPUESTO DE INGRESOS"))
                            {
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}",Fila["lEY_INGRESOS_ESTIMADA"]),
                                    DataType.Number, "BodyStyle_3"));
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}",Fila["MODIFICADO_A"]),
                                    DataType.Number, "BodyStyle_3"));
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}",Fila["DEVENGADO"]),
                                    DataType.Number, cell.StyleID = "BodyStyle_3"));		
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}",Fila["RECAUDADO_B"]),
                                    DataType.Number, "BodyStyle_3"));
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}",Fila["AVANCE_DE_RECAUDACION_B_A"]),
                                    DataType.Number, "BodyStyle_3"));
                            }
                            Renglon.AutoFitHeight = true;
                            Renglon = Hoja.Table.Rows.Add();
                        }
                        else if (Indice_Hojas < 1)
                        {
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Fila["Clave_Tipo"].ToString(),
                                DataType.String, "BodyStyle_1"));
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Fila["Tipo"].ToString(),
                                DataType.String, "BodyStyle_2"));

                            if (String.IsNullOrEmpty(Fila["lEY_INGRESOS_ESTIMADA"].ToString()))
                            {
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["lEY_INGRESOS_ESTIMADA"]),
                                    DataType.String, "BodyStyle_3"));
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["MODIFICADO_A"]),
                                    DataType.String, "BodyStyle_3"));
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["DEVENGADO"]),
                                    DataType.String, "BodyStyle_3"));
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["RECAUDADO_B"]),
                                    DataType.String, "BodyStyle_3"));
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["AVANCE_DE_RECAUDACION_B_A"]),
                                    DataType.String, "BodyStyle_3"));
                            }
                            else 
                            {
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["lEY_INGRESOS_ESTIMADA"]),
                                    DataType.Number, "BodyStyle_3"));
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["MODIFICADO_A"]),
                                    DataType.Number, "BodyStyle_3"));
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["DEVENGADO"]),
                                    DataType.Number, "BodyStyle_3"));
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["RECAUDADO_B"]),
                                    DataType.Number, "BodyStyle_3"));
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["AVANCE_DE_RECAUDACION_B_A"]),
                                    DataType.Number, "BodyStyle_3"));
                            }

                            Renglon.AutoFitHeight = true;
                            Renglon = Hoja.Table.Rows.Add();
                        }
                    }
                    Renglon = Hoja.Table.Rows.Add();
                }
                //Guardamos el archivo
                String Ruta_Archivo = @Server.MapPath("../../Reporte/");
                Libro.Save(Ruta_Archivo + Nombre_Archivo);
                Ruta_Archivo = "../../Reporte/" + Nombre_Archivo;
                Mostrar_Excel(Server.MapPath("../../Reporte/" + Nombre_Archivo), "application/vnd.ms-excel");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
            }
        }
        /// *************************************************************************************
        /// NOMBRE: Mostrar_Excel
        /// DESCRIPCIÓN: Muestra el reporte en excel.
        /// PARÁMETROS: No Aplica
        /// USUARIO CREO: Ramón Baeza Yépez
        /// *************************************************************************************
        private void Mostrar_Excel(string Ruta_Archivo, string Contenido)
        {
            try
            {
                System.IO.FileInfo ArchivoExcel = new System.IO.FileInfo(Ruta_Archivo);
                if (ArchivoExcel.Exists)
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ContentType = Contenido;
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + ArchivoExcel.Name);
                    Response.Charset = "UTF-8";
                    Response.ContentEncoding = Encoding.Default;
                    Response.WriteFile(ArchivoExcel.FullName);
                    Response.End();
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al mostrar el reporte en excel. Error: [" + Ex.Message + "]");
            }
        }
    #endregion
}
