﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Catalogo_Psp_Conceptos.Negocio;
using System.Data;
using JAPAMI.Cls_Cat_Psp_SubConcepto.Negocio;

public partial class paginas_Presupuestos_Frm_Cat_Psp_Conceptos : System.Web.UI.Page
{

    #region Variables
    private const int Const_Estado_Inicial = 0;
    private const int Const_Estado_Nuevo = 1;
    private const int Const_Estado_Modificar = 2;
    private const int Const_Estado_Buscar = 3;
    private const int Const_Estado_Deshabilitado = 4;
    //private static DataTable Dt_SAP_Conceptos = new DataTable();
    private static String Dt_Conceptos = "Dt_Psp_Conceptos";
    private static string M_Busqueda = "";
    #endregion

    #region Page Load / Init
    protected void Page_Load(object sender, EventArgs e)
    {
        
        try
        {            
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!Page.IsPostBack)
            {
                Estado_Botones(Const_Estado_Inicial);
                Cargar_Combos();
                Cargar_Grid(0);
                Llenar_Combo_Cuentas_Contables();
                Llenar_Combo_Banco();
                Llenar_Combo_FF();
                //Llenar_Combo_UR();
                ViewState["SortDirection"] = "DESC";
            }
            Mensaje_Error();
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
            Estado_Botones(Const_Estado_Deshabilitado);

        }
    }
    #endregion

    #region Metodos
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cargar_Combos
    ///DESCRIPCIÓN: metodo usado para cargar la informacion de todos los combos del formulario con la respectiva consulta
    ///PARAMETROS: 
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 20/marzo/2012
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Cargar_Combos()
    {
        try
        {
            Cls_Cat_Psp_Conceptos_Negocio Concepto = new Cls_Cat_Psp_Conceptos_Negocio();
            Llenar_Combo_ID(Cmb_Clase, Concepto.Consulta_Clases(), "clave_descripicion", Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID, "0");
            Cmb_Estatus.Items.Add(new ListItem("<SELECCIONE>", "0"));
            Cmb_Estatus.Items.Add(new ListItem("ACTIVO", "ACTIVO"));
            Cmb_Estatus.Items.Add(new ListItem("INACTIVO", "INACTIVO"));
        }
        catch (Exception Ex)
        {
            throw new Exception ("No se pudieron cargar los datos necesarios para dar de alta un Concepto SAP" + "</br>" + Ex.Message);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cargar_Grid
    ///DESCRIPCIÓN: Realizar la consulta y llenar el grid con estos datos
    ///PARAMETROS: 
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 20/marzo/2012
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Cargar_Grid(int Page_Index)
    {
        try
        {
            Cls_Cat_Psp_Conceptos_Negocio Concepto = new Cls_Cat_Psp_Conceptos_Negocio();            
            Concepto.P_Clave = M_Busqueda;
            Session[Dt_Conceptos] = Concepto.Consulta_Concepto();
            Grid_Psp_Conceptos.PageIndex = Page_Index;
            Grid_Psp_Conceptos.Columns[1].Visible = true;
            Grid_Psp_Conceptos.Columns[6].Visible = true;
            Grid_Psp_Conceptos.Columns[7].Visible = true;
            Grid_Psp_Conceptos.Columns[8].Visible = true;
            Grid_Psp_Conceptos.Columns[10].Visible = true;
            Grid_Psp_Conceptos.DataSource = (DataTable)Session[Dt_Conceptos];
            Grid_Psp_Conceptos.DataBind();
            Grid_Psp_Conceptos.Columns[6].Visible = false;
            Grid_Psp_Conceptos.Columns[1].Visible = false;
            Grid_Psp_Conceptos.Columns[7].Visible = false;
            Grid_Psp_Conceptos.Columns[8].Visible = false;
            Grid_Psp_Conceptos.Columns[10].Visible = false;
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA METODO: LLenar_Combo_Id
    ///        DESCRIPCIÓN: llena todos los combos
    ///         PARAMETROS: 1.- Obj_DropDownList: Combo a llenar
    ///                     2.- Dt_Temporal: DataTable genarada por una consulta a la base de datos
    ///                     3.- Texto: nombre de la columna del dataTable que mostrara el texto en el combo
    ///                     3.- Valor: nombre de la columna del dataTable que mostrara el valor en el combo
    ///                     3.- Seleccion: Id del combo el cual aparecera como seleccionado por default
    ///               CREO: Jesus S. Toledo Rdz.
    ///         FECHA_CREO: 06/9/2010
    ///           MODIFICO:
    ///     FECHA_MODIFICO:
    /// CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_Combo_ID(DropDownList Obj_DropDownList, DataTable Dt_Temporal, String Texto, String Valor, String Seleccion)
    {
        try
        {
            Obj_DropDownList.Items.Clear();
            Obj_DropDownList.Items.Add(new ListItem("<SELECCIONE>", "0"));
            foreach (DataRow row in Dt_Temporal.Rows)
            {
                Obj_DropDownList.Items.Add(new ListItem(row[Texto].ToString(), row[Valor].ToString()));
                
            }
            Obj_DropDownList.SelectedValue = Seleccion;
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }

    public void Llenar_Combo_ID(DropDownList Obj_DropDownList)
    {
        try
        {
            Obj_DropDownList.Items.Clear();
            Obj_DropDownList.Items.Add(new ListItem("<SELECCIONE>", "0"));
            Obj_DropDownList.SelectedValue = "0";
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }

    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cargar_Datos
    ///DESCRIPCIÓN: metodo que recibe el DataRow seleccionado de la grilla y carga los datos en los componetes de l formulario
    ///PARAMETROS: 
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 20/marzo/2012
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Cargar_Datos(DataRow Dr_Conceptos)
    {
        try
        {
            Txt_Comentarios.Text = Dr_Conceptos[Cat_Psp_Concepto_Ing.Campo_Descripcion].ToString();
            Txt_ID.Text = Dr_Conceptos[Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID].ToString();
            Txt_Clave.Text = Dr_Conceptos[Cat_Psp_Concepto_Ing.Campo_Clave].ToString();
            Cmb_Clase.SelectedValue = Dr_Conceptos[Cat_Psp_Concepto_Ing.Campo_Clase_Ing_ID].ToString();
            Cmb_Estatus.SelectedValue = Dr_Conceptos[Cat_Psp_Concepto_Ing.Campo_Estatus].ToString();
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }

    ///****************************************************************************************
    ///NOMBRE DE LA FUNCION:Mensaje_Error
    ///DESCRIPCION : Muestra el error
    ///PARAMETROS  : P_Texto: texto de un TextBox
    ///CREO        : Toledo Rodriguez Jesus S.
    ///FECHA_CREO  : 04-Septiembre-2010
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACION:
    ///****************************************************************************************
    private void Mensaje_Error(String P_Mensaje)
    {
        Img_Error.Visible = true;
        Lbl_Error.Text += P_Mensaje + "</br>";
    }
    private void Mensaje_Error()
    {
        Img_Error.Visible = false;
        Lbl_Error.Text = "";
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Estado_Botones
    ///DESCRIPCIÓN: Metodo para establecer el estado de los botones y componentes del formulario
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 02/02/2011 05:49:53 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Estado_Botones(int P_Estado)
    {
        switch (P_Estado)
        {
            case 0: //Estado inicial
                Session["Dt_Fuentes"] = null;
                Mensaje_Error();
                Txt_Busqueda.Text = String.Empty;                
                Txt_Comentarios.Text = String.Empty;
                Txt_ID.Text = String.Empty;
                Txt_Clave.Text = String.Empty;
                Txt_Clave_Con.Text = String.Empty;
                Txt_Anio.Text = String.Empty;
                Cmb_Cuenta_Contable.SelectedIndex = -1;
                Cmb_Banco.SelectedIndex = -1;
                if (Cmb_Fuente_Financiamiento.Items.Count > 0)
                {
                    Cmb_Fuente_Financiamiento.SelectedIndex = -1;
                }
                Txt_Comentarios.Enabled = false;
                Txt_ID.Enabled = false;
                Txt_Clave.Enabled = false;
                Txt_Anio.Enabled = false;

                Cmb_Estatus.SelectedIndex = 0;
                Cmb_Clase.SelectedIndex = 0;
                Cmb_Fuente_Financiamiento.Enabled = false;
                Cmb_Estatus.Enabled = false;
                Cmb_Clase.Enabled = false;
                Cmb_Cuenta_Contable.Enabled = false;
                Cmb_Banco.Enabled = false;
                //Cmb_UR.Enabled = false;
                Grid_Psp_Conceptos.Enabled = true;
                Grid_Psp_Conceptos.SelectedIndex = (-1);
                Grid_Fuente_Financiamiento.DataSource = null;
                Grid_Fuente_Financiamiento.DataBind();

                Btn_Agregar_Documento.Enabled = false;
                Btn_Busqueda.Visible = true;
                Btn_Eliminar.Visible = true;
                Btn_Modificar.Visible = true;
                Btn_Nuevo.Visible = true;
                Btn_Salir.Visible = true;

                Btn_Busqueda.AlternateText = "Buscar";
                Btn_Eliminar.AlternateText = "Eliminar";
                Btn_Modificar.AlternateText = "Modificar";
                Btn_Nuevo.AlternateText = "Nuevo";
                Btn_Salir.AlternateText = "Salir";

                Btn_Busqueda.ToolTip = "Consultar";
                Btn_Eliminar.ToolTip = "Eliminar";
                Btn_Modificar.ToolTip = "Modificar";
                Btn_Nuevo.ToolTip = "Nuevo";
                Btn_Salir.ToolTip = "Salir";

                Btn_Busqueda.ImageUrl = "~/paginas/imagenes/paginas/busqueda.png";
                Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar.png";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";

                Configuracion_Acceso("Frm_Cat_Psp_Conceptos.aspx");
                break;

            case 1: //Nuevo
                Mensaje_Error();
                Session["Dt_Fuentes"] = null;
                Txt_Comentarios.Text = String.Empty;
                Txt_Clave.Text = String.Empty;
                Txt_ID.Text = String.Empty;
                Txt_Clave_Con.Text = String.Empty;
                Txt_Anio.Text = String.Empty;

                Grid_Fuente_Financiamiento.DataSource = null;
                Grid_Fuente_Financiamiento.DataBind();
                Cmb_Estatus.SelectedIndex = 0;
                Cmb_Clase.SelectedIndex = 0;
                Cmb_Fuente_Financiamiento.SelectedIndex = -1;
                //Cmb_UR.SelectedIndex = -1;
                Txt_Comentarios.Enabled = true;
                Txt_Clave.Enabled = true;
                Txt_Anio.Enabled = true;

                Cmb_Estatus.Enabled = false;
                Cmb_Estatus.SelectedIndex  = 1;
                Cmb_Banco.SelectedIndex = -1;
                Cmb_Clase.Enabled = true;
                //Cmb_UR.Enabled = true;
                Cmb_Cuenta_Contable.Enabled = true;
                Cmb_Banco.Enabled = true;
                Cmb_Estatus.Enabled = true;
                Cmb_Fuente_Financiamiento.Enabled = true;
                Btn_Eliminar.Visible = false;
                Btn_Modificar.Visible = false;
                Btn_Nuevo.Visible = true;
                Btn_Salir.Visible = true;

                Grid_Psp_Conceptos.SelectedIndex = (-1);
                Grid_Psp_Conceptos.Enabled = false;
                Btn_Agregar_Documento.Enabled = true;
                Btn_Eliminar.AlternateText = "Eliminar";
                Btn_Modificar.AlternateText = "Modificar";
                Btn_Nuevo.AlternateText = "Guardar";
                Btn_Salir.AlternateText = "Cancelar";

                Btn_Eliminar.ToolTip = "Eliminar";
                Btn_Modificar.ToolTip = "Modificar";
                Btn_Nuevo.ToolTip = "Guardar";
                Btn_Salir.ToolTip = "Cancelar";

                Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar_deshabilitado.png";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar_deshabilitado.png";

                break;

            case 2: //Modificar
                Mensaje_Error();
                Txt_Comentarios.Enabled = true;
                Txt_Clave.Enabled = true;
                Txt_Anio.Enabled = true;

                Cmb_Estatus.Enabled = true;
                Cmb_Clase.Enabled = true;
                Cmb_Cuenta_Contable.Enabled = true;
                Cmb_Banco.Enabled = true;
                //Cmb_UR.Enabled = true;
                Cmb_Fuente_Financiamiento.Enabled = true;
                Btn_Eliminar.Visible = false;
                Btn_Modificar.Visible = true;
                Btn_Nuevo.Visible = false;
                Btn_Salir.Visible = true;
                Btn_Agregar_Documento.Enabled = true;
                Btn_Eliminar.AlternateText = "Eliminar";
                Btn_Modificar.AlternateText = "Guardar";
                Btn_Nuevo.AlternateText = "Nuevo";
                Btn_Salir.AlternateText = "Cancelar";

                Btn_Eliminar.ToolTip = "Eliminar";
                Btn_Modificar.ToolTip = "Guardar";
                Btn_Nuevo.ToolTip = "Nuevo";
                Btn_Salir.ToolTip = "Cancelar";

                Grid_Psp_Conceptos.Enabled = false;

                Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar_deshabilitado.png";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo_deshabilitado.png";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";

                break;

            case 3: //Buscar
                Mensaje_Error();

                Txt_Comentarios.Text = String.Empty;
                Txt_ID.Text = String.Empty;
                Txt_Clave.Text = String.Empty;
                Txt_Clave_Con.Text = String.Empty;
                Txt_Anio.Text = String.Empty;

                Cmb_Fuente_Financiamiento.SelectedIndex = -1;
                Cmb_Estatus.SelectedIndex = 0;
                Cmb_Banco.SelectedIndex = -1;
                Cmb_Clase.SelectedIndex = 0;
                Cmb_Cuenta_Contable.SelectedIndex = -1;
                //Cmb_UR.SelectedIndex = -1;
                break;

            case 4: //Desabilitar

                Txt_Busqueda.Text = String.Empty;
                Txt_Comentarios.Text = String.Empty;
                Txt_ID.Text = String.Empty;
                Txt_Clave.Text = String.Empty;
                Txt_Clave_Con.Text = String.Empty;
                Txt_Anio.Text = String.Empty;

                Txt_Comentarios.Enabled = false;
                Txt_ID.Enabled = false;
                Txt_Clave.Enabled = false;
                Txt_Anio.Enabled = false;   

                Cmb_Estatus.Enabled = false;
                Cmb_Banco.Enabled = false;
                Cmb_Clase.Enabled = false;
                Cmb_Cuenta_Contable.Enabled = false;
                //Cmb_UR.Enabled = false;

                Grid_Psp_Conceptos.Enabled = true;
                Grid_Psp_Conceptos.SelectedIndex = (-1);

                Btn_Agregar_Documento.Enabled = false;
                Btn_Busqueda.Visible = false;
                Btn_Eliminar.Visible = false;
                Btn_Modificar.Visible = false;
                Btn_Nuevo.Visible = false;
                Btn_Salir.Visible = false;

                Btn_Busqueda.AlternateText = "Buscar";
                Btn_Eliminar.AlternateText = "Eliminar";
                Btn_Modificar.AlternateText = "Modificar";
                Btn_Nuevo.AlternateText = "Nuevo";
                Btn_Salir.AlternateText = "Salir";

                Btn_Busqueda.ToolTip = "Buscar";
                Btn_Eliminar.ToolTip = "Eliminar";
                Btn_Modificar.ToolTip = "Modificar";
                Btn_Nuevo.ToolTip = "Nuevo";
                Btn_Salir.ToolTip = "Salir";

                Btn_Busqueda.ImageUrl = "~/paginas/imagenes/paginas/busqueda.png";
                Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar.png";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";

                break;
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Validar_Datos
    ///DESCRIPCIÓN: Guardar datos para dar de alta un nuevo registro de un servicio
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 02/03/2011 10:45:17 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private Boolean Validar_Datos()
    {
        Boolean Resultado = true;
        try
        {

            if (Txt_Clave.Text.Trim() == "")
            {
                Resultado = false;
                Mensaje_Error("Favor de ingresar la Clave del Concepto");
            }
            if (Txt_Anio.Text.Trim() == "")
            {
                Resultado = false;
                Mensaje_Error("Favor de ingresar el Año del Concepto");
            }
            if (Cmb_Clase.SelectedIndex <= 0)
            {
                Resultado = false;
                Mensaje_Error("Favor de especificar la clase relacionado con el Concepto");
            }
            if (Cmb_Estatus.SelectedIndex <= 0)
            {
                Resultado = false;
                Mensaje_Error("Favor de especificar el Estatus del Concepto");
            }
            if (Cmb_Cuenta_Contable.SelectedIndex <= 0)
            {
                Resultado = false;
                Mensaje_Error("Favor de especificar la cuenta contable");
            }
            if (!Txt_Comentarios.Text.Trim().Equals(""))
            {
                if (Txt_Comentarios.Text.Trim().Length >= 250)
                {
                    Txt_Comentarios.Text = Txt_Comentarios.Text.Trim().Substring(0, 250);
                }
            }
            else
            {
                Resultado = false;
                Mensaje_Error("Favor de introducir una Descripción");
            }
            if(Grid_Fuente_Financiamiento.Rows.Count<=0){
                Resultado = false;
                Mensaje_Error("Favor de agregar una fuente de financiamiento minimo");
            }

        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
        return Resultado;
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Alta_Concepto
    ///DESCRIPCIÓN: Realiza el alta de un nuevo registro de un concepto
    ///PARAMETROS: 
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 20/marzo/2012
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Alta_Concepto()
    {
        DataTable Dt_Validar = new DataTable();
        DataTable Dt_Fuentes = new DataTable();
        try
        {
            if (Validar_Datos())
            {
                Cls_Cat_Psp_Conceptos_Negocio Validar = new Cls_Cat_Psp_Conceptos_Negocio();
                Validar.P_Clave = Txt_Clave_Con.Text.Trim();
                Dt_Validar=Validar.Consulta_Concepto();
                if (Dt_Validar.Rows.Count > 0)
                {
                    if (Dt_Validar.Rows[0]["estatus"].ToString() == "ACTIVO")
                    {
                        Mensaje_Error("La clave que vas a ingresar ya existe favor de corregirlo");
                    }
                }
                else
                {
                    Dt_Fuentes = (DataTable)Session["Dt_Fuentes"];
                    Cls_Cat_Psp_Conceptos_Negocio Concepto = new Cls_Cat_Psp_Conceptos_Negocio();
                    if (!Txt_Comentarios.Text.Equals(""))
                    {
                        Concepto.P_Descripcion = Txt_Comentarios.Text.Trim();
                    }
                    Concepto.P_Estatus = Cmb_Estatus.SelectedValue;
                    Concepto.P_Banco_ID = Cmb_Banco.SelectedValue;
                    Concepto.P_Clase_ID = Cmb_Clase.SelectedValue;
                    //Concepto.P_Dependencia_ID = Cmb_UR.SelectedValue;
                    Concepto.P_Cuenta_Contable_ID = Cmb_Cuenta_Contable.SelectedValue;
                    Concepto.P_Clave = Txt_Clave_Con.Text.Trim();
                    Concepto.P_Anio = Txt_Anio.Text.Trim();
                    Concepto.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                    Concepto.P_Dt_Fuentes_Financiamiento = Dt_Fuentes;
                    Concepto.Alta_Concepto();
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alta", "alert('Operacion Completa');", true);
                    Estado_Botones(Const_Estado_Inicial);
                    Txt_Busqueda.Text = "";
                    Cargar_Grid(0);
                }                
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message, Ex);
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Baja_Concepto
    ///DESCRIPCIÓN: dar de baja un registro de la base de datos
    ///PARAMETROS: 
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 20/marzo/2012
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Baja_Concepto()
    {
        try
        {
            Cls_Cat_Psp_Conceptos_Negocio Concepto = new Cls_Cat_Psp_Conceptos_Negocio();
            Concepto.P_Concepto_ID = HttpUtility.HtmlDecode(Grid_Psp_Conceptos.SelectedRow.Cells[1].Text.Trim());
            Concepto.Baja_Concepto();
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Conceptos", "alert('La Baja del Concepto SAP fue Exitosa');", true);
            Estado_Botones(Const_Estado_Inicial);
            Txt_Busqueda.Text = "";
            Cargar_Grid(0);
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message, Ex);
        }

    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Modificar_Concepto
    ///DESCRIPCIÓN: Modifica un registro y lo guarda en la base de datos
    ///PARAMETROS: 
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 20/marzo/2012
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Modificar_Concepto()
    {
        try
        {
            if (Validar_Datos())
            {
                Cls_Cat_Psp_Conceptos_Negocio Concepto = new Cls_Cat_Psp_Conceptos_Negocio();
                if (!Txt_Comentarios.Text.Equals(""))
                {
                    Concepto.P_Descripcion = Txt_Comentarios.Text.Trim();
                }
                Concepto.P_Concepto_ID = HttpUtility.HtmlDecode(Grid_Psp_Conceptos.SelectedRow.Cells[1].Text.Trim());
                Concepto.P_Estatus = Cmb_Estatus.SelectedValue;
                Concepto.P_Clase_ID = Cmb_Clase.SelectedValue;
                Concepto.P_Banco_ID = Cmb_Banco.SelectedValue;
                Concepto.P_Clave = Txt_Clave_Con.Text.Trim();
                Concepto.P_Cuenta_Contable_ID = Cmb_Cuenta_Contable.SelectedValue;
                //Concepto.P_Dependencia_ID = Cmb_UR.SelectedValue;
                Concepto.P_Anio = Txt_Anio.Text.Trim();
                Concepto.P_Dt_Fuentes_Financiamiento = (DataTable)Session["Dt_Fuentes"];
                Concepto.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                Concepto.Cambio_Concepto();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Catálogo de Conceptos", "alert('La modificación del Concepto fue Exitosa');", true);
                Estado_Botones(Const_Estado_Inicial);
                Txt_Busqueda.Text = "";
                Cargar_Grid(0);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message, Ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Cuentas_Contables
    ///DESCRIPCIÓN          : Metodo para llenar el combo de las cuentas contables
    ///PARAMETROS           :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 19/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Combo_Cuentas_Contables()
    {
        Cls_Cat_Psp_SubConceptos_Negocio Negocio = new Cls_Cat_Psp_SubConceptos_Negocio(); //conexion con la capa de negocios
        DataTable Dt_Cuentas = new DataTable();

        try
        {
            Cmb_Cuenta_Contable.Items.Clear();
            Negocio.P_Estatus = "ACTIVO";
            Negocio.P_Descripcion = String.Empty;
            Dt_Cuentas = Negocio.Consultar_Cuentas_Contables();
            if (Dt_Cuentas != null)
            {
                Cmb_Cuenta_Contable.DataSource = Dt_Cuentas;
                Cmb_Cuenta_Contable.DataTextField = "CLAVE_NOMBRE";
                Cmb_Cuenta_Contable.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                Cmb_Cuenta_Contable.DataBind();
                Cmb_Cuenta_Contable.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
                Cmb_Cuenta_Contable.SelectedIndex = -1;
            }
            else
            {
                Cmb_Cuenta_Contable.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error( "Error al cargar el combo de cuentas contables. Error[" + Ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Banco
    ///DESCRIPCIÓN          : Metodo para llenar el combo de los bancos
    ///PARAMETROS           :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 16/Junio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Combo_Banco()
    {
        Cls_Cat_Psp_Conceptos_Negocio Negocio = new Cls_Cat_Psp_Conceptos_Negocio(); //conexion con la capa de negocios
        DataTable Dt = new DataTable();

        try
        {
            Cmb_Banco.Items.Clear();
            Dt = Negocio.Consultar_Banco();
            if (Dt != null)
            {
                Cmb_Banco.DataSource = Dt;
                Cmb_Banco.DataTextField = "NOMBRE";
                Cmb_Banco.DataValueField = Cat_Nom_Bancos.Campo_Banco_ID;
                Cmb_Banco.DataBind();
                Cmb_Banco.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
                Cmb_Banco.SelectedIndex = -1;
            }
            else
            {
                Cmb_Banco.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error("Error al cargar el combo de BANCOS. Error[" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_FF
    ///DESCRIPCIÓN          : Metodo para llenar el combo de los bancos
    ///PARAMETROS           :
    ///CREO                 : Sergio Manuel Gallardo Andrade
    ///FECHA_CREO           : 22/Junio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Combo_FF()
    {
        Cls_Cat_Psp_Conceptos_Negocio Negocio = new Cls_Cat_Psp_Conceptos_Negocio(); //conexion con la capa de negocios
        DataTable Dt = new DataTable();

        try
        {
            Cmb_Fuente_Financiamiento.Items.Clear();
            Dt = Negocio.Consultar_Fuentes_Financiamiento();
            if (Dt != null)
            {
                Cmb_Fuente_Financiamiento.DataSource = Dt;
                Cmb_Fuente_Financiamiento.DataTextField = "DESCRIPCION";
                Cmb_Fuente_Financiamiento.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
                Cmb_Fuente_Financiamiento.DataBind();
                Cmb_Fuente_Financiamiento.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
                Cmb_Fuente_Financiamiento.SelectedIndex = -1;
            }
            else
            {
                Cmb_Fuente_Financiamiento.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error("Error al cargar el combo de FUENTES DE FINANCIAMIENTO. Error[" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_UR
    ///DESCRIPCIÓN          : Metodo para llenar el combo de las unidades responsables
    ///PARAMETROS           :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 13/Junio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    //private void Llenar_Combo_UR()
    //{
    //    Cls_Cat_Psp_Conceptos_Negocio Negocio = new Cls_Cat_Psp_Conceptos_Negocio(); //conexion con la capa de negocios
    //    DataTable Dt = new DataTable();

    //    try
    //    {
    //        Cmb_UR.Items.Clear();
    //        Negocio.P_Estatus = "ACTIVO";
    //        Negocio.P_Descripcion = String.Empty;
    //        Dt = Negocio.Consultar_Dependencias();
    //        if (Dt != null)
    //        {
    //            Cmb_UR.DataSource = Dt;
    //            Cmb_UR.DataTextField = "CLAVE_NOMBRE";
    //            Cmb_UR.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
    //            Cmb_UR.DataBind();
    //            Cmb_UR.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
    //            Cmb_UR.SelectedIndex = -1;
    //        }
    //        else
    //        {
    //            Cmb_UR.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
    //        }
    //    }
    //    catch (Exception Ex)
    //    {
    //        Mensaje_Error("Error al cargar el combo de cuentas contables. Error[" + Ex.Message + "]");
    //    }
    //}
    #endregion

    #region Eventos
    
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Nuevo.AlternateText.Equals("Nuevo"))
            {
                Estado_Botones(Const_Estado_Nuevo);
                Txt_Anio.Text = String.Format("{0:yyyy}", DateTime.Now);
            }
            else
            {
                Alta_Concepto();
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Grid_Psp_Conceptos.SelectedIndex > (-1))
            {
                if (Btn_Modificar.AlternateText.Equals("Modificar"))
                {
                    Estado_Botones(Const_Estado_Modificar);
                    Grid_Fuente_Financiamiento.Columns[2].Visible = true;
                }
                else
                {
                    Modificar_Concepto();
                }
            }
            else
            {
                Mensaje_Error("Favor de seleccionar el Concepto a modificar");
            }

        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);

        }
    }
    protected void Btn_Eliminar_Click(object sender, ImageClickEventArgs e)
    {
        
        try
        {
            if (Grid_Psp_Conceptos.SelectedIndex > (-1))
            {
                Baja_Concepto();
            }
            else
            {
                Mensaje_Error("Favor de seleccionar el Concepto a eliminar");
            }
        }
        catch (Exception Ex)
        {
            String Msg = Ex.ToString();
            Mensaje_Error("Es posible que existan registros secuendarios del registro que desea eliminar");
            //Mensaje_Error(Ex.Message);
        }
    }
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Salir.AlternateText.Equals("Salir"))
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Estado_Botones(Const_Estado_Inicial);
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    protected void Btn_Busqueda_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Estado_Botones(Const_Estado_Buscar);
            Grid_Psp_Conceptos.SelectedIndex = (-1);
            M_Busqueda = Txt_Busqueda.Text.Trim();
            Cargar_Grid(0);

        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    protected void Btn_Agregar_Fuente_Click(object sender, ImageClickEventArgs e)
    {
        DataTable Dt_Consulta = new DataTable();
        Boolean Agregar = true;
        //Valida que todos los datos requeridos los haya proporcionado el usuario
        if (Cmb_Fuente_Financiamiento.SelectedIndex>0)
        {
                if (Session["Dt_Fuentes"] == null)
                {
                    //Agrega los campos que va a contener el DataTable
                    Dt_Consulta.Columns.Add("Fuente_Financiamiento_ID", typeof(System.String));
                    Dt_Consulta.Columns.Add("Descripcion", typeof(System.String));
                    
                }
                else
                {
                    Dt_Consulta = (DataTable)Session["Dt_Fuentes"];
                    Session.Remove("Dt_Fuentes");
                }
                Agregar = true;
                foreach(DataRow Fila in Dt_Consulta.Rows)
                {
                    if (Cmb_Fuente_Financiamiento.SelectedValue == Fila["Fuente_Financiamiento_ID"].ToString())
                    {
                        Agregar = false;
                    }
                }
                if (Agregar)
                {
                    DataRow row = Dt_Consulta.NewRow(); //Crea un nuevo registro a la tabla
                    //Asigna los valores al nuevo registro creado a la tabla
                    row["Fuente_Financiamiento_ID"] = Cmb_Fuente_Financiamiento.SelectedValue;
                    row["Descripcion"] = Cmb_Fuente_Financiamiento.SelectedItem.Text.Trim();
                    Dt_Consulta.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Consulta.AcceptChanges();
                    Session["Dt_Fuentes"] = Dt_Consulta;//Agrega los valores del registro a la sesión
                    Grid_Fuente_Financiamiento.Columns[0].Visible = true;
                    Grid_Fuente_Financiamiento.DataSource = Dt_Consulta; //Agrega los valores de todas las partidas que se tienen al grid
                    Grid_Fuente_Financiamiento.DataBind();
                    Grid_Fuente_Financiamiento.Columns[0].Visible = false;
                    Cmb_Fuente_Financiamiento.SelectedIndex = -1;
                }
                else
                {
                    Mensaje_Error("Es necesario Seleccionar: <br> + Otra fuente de financiamiento  <br>");
                    Session["Dt_Fuentes"] = Dt_Consulta;
                }
            }
            else
            {
                Mensaje_Error("Es necesario Seleccionar: <br> +  una fuente de financiamiento <br>");
                Session["Dt_Fuentes"] = Dt_Consulta;
            }
    }
    protected void Btn_Eliminar_Partida(object sender, EventArgs e)
    {

        ImageButton Btn_Eliminar_Partida = (ImageButton)sender;
        DataTable Dt_Partidas = (DataTable)Session["Dt_Fuentes"];
        DataRow[] Filas = Dt_Partidas.Select("Fuente_Financiamiento_ID" +
                "='" + Btn_Eliminar_Partida.CommandArgument + "'");

        if (!(Filas == null))
        {
            if (Filas.Length >= 0)
            {
                Dt_Partidas.Rows.Remove(Filas[0]);
                Session["Dt_Fuentes"] = Dt_Partidas;
                Grid_Fuente_Financiamiento.Columns[0].Visible = true;
                Grid_Fuente_Financiamiento.DataSource = Session["Dt_Fuentes"]; ; //Agrega los valores de todas las partidas que se tienen al grid
                Grid_Fuente_Financiamiento.DataBind();
                Grid_Fuente_Financiamiento.Columns[0].Visible = false;
            }
        }
    }
    #endregion

    #region Eventos Grid   
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Cuentas_RowDataBound
    /// DESCRIPCION : Agrega un identificador al boton de cancelar de la tabla
    ///               para identicar la fila seleccionada de tabla.
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 11/Julio/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Grid_Fuentes_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                ((ImageButton)e.Row.Cells[2].FindControl("Btn_Eliminar")).CommandArgument = e.Row.Cells[0].Text.Trim();
                ((ImageButton)e.Row.Cells[2].FindControl("Btn_Eliminar")).ToolTip = "Quitar el Documento " + e.Row.Cells[0].Text;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    protected void Grid_Psp_Conceptos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Grid_Psp_Conceptos.SelectedIndex = (-1);
            Cargar_Grid(e.NewPageIndex);
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    protected void Grid_Psp_Conceptos_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Cat_Psp_Conceptos_Negocio Psp_Concepto = new Cls_Cat_Psp_Conceptos_Negocio();
        DataTable Dt_Clases = new DataTable();
        DataTable Dt_Fuentes_Financiamiento = new DataTable();
        try
        {
            if (Grid_Psp_Conceptos.SelectedIndex > (-1))
            {
                GridViewRow selectedRow = Grid_Psp_Conceptos.Rows[Grid_Psp_Conceptos.SelectedIndex];
                String Clave = HttpUtility.HtmlDecode(selectedRow.Cells[2].Text).ToString();
                String Estatus = HttpUtility.HtmlDecode(selectedRow.Cells[5].Text).ToString();
                String Cuenta = HttpUtility.HtmlDecode(selectedRow.Cells[7].Text).ToString();
                String Clase = HttpUtility.HtmlDecode(selectedRow.Cells[6].Text).ToString();
                String Descripcion = HttpUtility.HtmlDecode(selectedRow.Cells[3].Text).ToString();
                String UR = HttpUtility.HtmlDecode(selectedRow.Cells[8].Text).ToString();
                String Anio = HttpUtility.HtmlDecode(selectedRow.Cells[9].Text).ToString();
                String Banco = HttpUtility.HtmlDecode(selectedRow.Cells[10].Text).ToString();

                //Dt_Clases = Psp_Concepto.Consulta_Clases();
                //foreach (DataRow Registro in Dt_Clases.Rows) 
                //{
                //    Cmb_Clase.Items.Add(Registro["clave_descripicion"].ToString());
                //}

                Txt_Clave.Text = Clave.Substring(4,2);
                Txt_Clave_Con.Text = Clave;
                Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(Estatus));
                Cmb_Cuenta_Contable.SelectedIndex = Cmb_Cuenta_Contable.Items.IndexOf(Cmb_Cuenta_Contable.Items.FindByValue(Cuenta));
                Cmb_Clase.SelectedIndex = Cmb_Clase.Items.IndexOf(Cmb_Clase.Items.FindByValue(Clase));
                Cmb_Banco.SelectedIndex = Cmb_Banco.Items.IndexOf(Cmb_Banco.Items.FindByValue(Banco));
                Txt_Comentarios.Text = Descripcion;
                Txt_ID.Text = HttpUtility.HtmlDecode(selectedRow.Cells[1].Text).ToString();
               // Cmb_UR.SelectedIndex = Cmb_UR.Items.IndexOf(Cmb_UR.Items.FindByValue(UR));
                Txt_Anio.Text = Anio;
                //Consulta las fuentes de financiamiento
                Psp_Concepto.P_Concepto_ID=HttpUtility.HtmlDecode(selectedRow.Cells[1].Text).ToString();
                Dt_Fuentes_Financiamiento = Psp_Concepto.Consultar_Fuentes_Por_Concepto();
                if (Dt_Fuentes_Financiamiento.Rows.Count > 0)
                {
                    Session["Dt_Fuentes"] = Dt_Fuentes_Financiamiento;
                    Grid_Fuente_Financiamiento.Columns[0].Visible=true;
                    Grid_Fuente_Financiamiento.DataSource = Dt_Fuentes_Financiamiento;
                    Grid_Fuente_Financiamiento.DataBind();
                    Grid_Fuente_Financiamiento.Columns[0].Visible = false;
                    Grid_Fuente_Financiamiento.Columns[2].Visible = false;
                }

            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    //protected void custPager_PageChanged(object sender, CustomPageChangeArgs e)
    //{
    //    Grid_SAP_Conceptos.DataSource = Dt_SAP_Conceptos;
    //    Grid_SAP_Conceptos.PageSize = e.CurrentPageSize;
    //    Grid_SAP_Conceptos.PageIndex = e.CurrentPageNumber;
        
    //    //Consulta_Percepciones_Deducciones();
    //}
    #endregion

    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// 
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// 
    /// PARÁMETROS: No Áplica.
    /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
    /// FECHA CREÓ: 23/Mayo/2011 10:43 a.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            Botones.Add(Btn_Nuevo);
            Botones.Add(Btn_Modificar);
            Botones.Add(Btn_Eliminar);
            Botones.Add(Btn_Busqueda);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 29/Noviembre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion

    #region ORDENAR GRIDS
    /// ******************************************************************************************
    /// NOMBRE: Grid_Requisiciones_Sorting
    /// DESCRIPCIÓN: Ordena las columnas en orden ascendente o descendente.
    /// CREÓ: Gustavo Angeles Cruz
    /// FECHA CREÓ: 11/Junio/2011
    /// MODIFICÓ:
    /// FECHA MODIFICÓ:
    /// CAUSA MODIFICACIÓN:
    /// ******************************************************************************************
    protected void Grid_Psp_Conceptos_Sorting(object sender, GridViewSortEventArgs e)
    {
        Grid_Sorting(Grid_Psp_Conceptos, ((DataTable)Session[Dt_Conceptos]), e);
        
    }
    /// *****************************************************************************************
    /// NOMBRE: Grid_Sorting
    /// DESCRIPCIÓN: Ordena las columnas en orden ascendente o descendente.
    /// CREÓ: Gustavo Angeles Cruz
    /// FECHA CREÓ: 11/Junio/2011
    /// MODIFICÓ:
    /// FECHA MODIFICÓ:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************
    private void Grid_Sorting(GridView Grid, DataTable Dt_Table, GridViewSortEventArgs e)
    {
        if (Dt_Table != null)
        {
            DataView Dv_Vista = new DataView(Dt_Table);
            String Orden = ViewState["SortDirection"].ToString();
            if (Orden.Equals("ASC"))
            {
                Dv_Vista.Sort = e.SortExpression + " DESC";
                ViewState["SortDirection"] = "DESC";
            }
            else
            {
                Dv_Vista.Sort = e.SortExpression + " ASC";
                ViewState["SortDirection"] = "ASC";
            }

            Grid.Columns[1].Visible = true;
            Grid.Columns[6].Visible = true;
            Grid.Columns[7].Visible = true;
            Grid.Columns[8].Visible = true;
            Grid.DataSource = Dv_Vista;
            Grid.DataBind();
            Grid.Columns[1].Visible = false;
            Grid.Columns[6].Visible = false;
            Grid.Columns[7].Visible = false;
            Grid.Columns[8].Visible = true;
        }
    }
    #endregion

}

