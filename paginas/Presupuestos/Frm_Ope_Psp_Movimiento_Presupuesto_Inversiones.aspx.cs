﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Movimiento_Presupuestal_Ingresos.Negocio;
using JAPAMI.Autorizar_Traspaso_Presupuestal.Negocio;
using JAPAMI.Clasificacion_Gasto.Negocio;
using JAPAMI.Tipo_Presupuesto.Negocio;
using JAPAMI.Limite_Presupuestal.Negocio;
using JAPAMI.Calendarizar_Presupuesto.Negocio;
using JAPAMI.Paramentros_Presupuestos.Negocio;

public partial class paginas_Presupuestos_Frm_Ope_Psp_Movimiento_Presupuesto_Inversiones : System.Web.UI.Page
{
    #region(Load)
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Refresca la sesion del usuario logeado en el sistema
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            //Valida que existe un usuario logueado en el sistema
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

            if (!IsPostBack)
            {
                Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                ViewState["SortDirection"] = "ASC";
                Session["Session_Movimientos_Presupuesto"] = null;

            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion

    #region(Metodos)
    #region(Metodos Generales)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Inicializa_Controles
    /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda
    ///               realizar diferentes operaciones
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 07/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Inicializa_Controles()
    {
        try
        {
            Limpiar_Controles(); //Limpia los controles del forma

            Habilitar_Controles("Inicial");//Inicializa todos los controles
            Cargar_Combo_Responsable();
            Consulta_Movimiento();
            Div_Grid_Movimientos.Visible = true;
            Div_Datos.Visible = false;
            Llenar_Combo_Mes();
        }
        catch (Exception ex)
        {
            throw new Exception("Inicializa_Controles " + ex.Message.ToString());
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Controles
    /// DESCRIPCION : Limpia los controles que se encuentran en la forma
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 07/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Limpiar_Controles()
    {
        try
        {
            //para busqueta
            Txt_Busqueda.Text = "";

            //para datos generales
            Cmb_Operacion.SelectedIndex = 0;
            Txt_Importe.Text = "";
            Txt_No_Solicitud.Text = "";
            Cmb_Estatus.SelectedIndex = 0;

            //partida Origen
            Txt_Codigo1.Text = "";
            Cmb_Fuente_Financiamiento_Origen.Items.Clear();
            Cmb_Capitulo_Origen.SelectedIndex = -1;
            Cmb_Programa_Origen.SelectedIndex = -1;

            //partida Destino
            Txt_Codigo2.Text = "";
            Cmb_Fuente_Financiamiento_Destino.Items.Clear();
            Txt_Justificacion.Text = "";
            Cmb_Capitulo_Destino.SelectedIndex = -1;
            Cmb_Programa_Destino.SelectedIndex = -1;
        }
        catch (Exception ex)
        {
            throw new Exception("Limpia_Controles " + ex.Message.ToString());
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Habilitar_Controles
    /// DESCRIPCION : Habilita y Deshabilita los controles de la forma para prepara la página
    ///               para a siguiente operación
    /// PARAMETROS  : 1.- Operacion: Indica la operación que se desea realizar por parte del usuario
    ///               si es una alta, modificacion
    ///                           
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 07/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Habilitar_Controles(String Operacion)
    {
        Boolean Habilitado; ///Indica si el control de la forma va hacer habilitado para utilización del usuario
        try
        {
            Habilitado = false;
            switch (Operacion)
            {
                case "Inicial":
                    Habilitado = false;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Nuevo.Visible = true;
                    Btn_Nuevo.CausesValidation = false;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";

                    Configuracion_Acceso("Frm_Ope_Psp_Movimiento_Presupuesto_Inversiones.aspx");
                    break;

                case "Nuevo":
                    Habilitado = true;
                    Btn_Nuevo.ToolTip = "Dar de Alta";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Nuevo.Visible = true;
                    Btn_Nuevo.CausesValidation = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";


                    break;

            }
            Grid_Partidas.Visible = true;
            //para datos generales
            Cmb_Operacion.Enabled = Habilitado;
            Txt_Importe.Enabled = Habilitado;
            Cmb_Estatus.Enabled = Habilitado;

            //partida Origen
            Txt_Codigo1.Enabled = Habilitado;
            Cmb_Fuente_Financiamiento_Origen.Enabled = Habilitado;

            //partida Destino
            Txt_Codigo2.Enabled = Habilitado;
            Cmb_Fuente_Financiamiento_Destino.Enabled = Habilitado;
            Txt_Justificacion.Enabled = Habilitado;
            //mensajes de error
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
        }

        catch (Exception ex)
        {
            throw new Exception("Limpia_Controles " + ex.Message.ToString());
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Consultar_Partida_Especifica
    ///DESCRIPCIÓN          : consulta para obtener los datos de las partidas especificas
    ///PARAMETROS           : 
    ///CREO                 : Leslie Gonzalez Vázquez
    ///FECHA_CREO           : 30/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Combo_Mes()
    {
        int Mes = DateTime.Today.Month;
        Hf_Mes_Actual.Value = "";
        try
        {
            switch (Mes)
            {
                case 1:
                    Txt_Enero.Enabled = true;
                    Txt_Febrero.Enabled = true;
                    Txt_Marzo.Enabled = true;
                    Txt_Abril.Enabled = true;
                    Txt_Mayo.Enabled = true;
                    Txt_Junio.Enabled = true;
                    Txt_Julio.Enabled = true;
                    Txt_Agosto.Enabled = true;
                    Txt_Septiembre.Enabled = true;
                    Txt_Octubre.Enabled = true;
                    Txt_Noviembre.Enabled = true;
                    Txt_Diciembre.Enabled = true;

                    Txt_Enero_Destino.Enabled = true;
                    Txt_Febrero_Destino.Enabled = true;
                    Txt_Marzo_Destino.Enabled = true;
                    Txt_Abril_Destino.Enabled = true;
                    Txt_Mayo_Destino.Enabled = true;
                    Txt_Junio_Destino.Enabled = true;
                    Txt_Julio_Destino.Enabled = true;
                    Txt_Agosto_Destino.Enabled = true;
                    Txt_Septiembre_Destino.Enabled = true;
                    Txt_Octubre_Destino.Enabled = true;
                    Txt_Noviembre_Destino.Enabled = true;
                    Txt_Diciembre_Destino.Enabled = true;

                    Hf_Mes_Actual.Value = "ENERO";
                    break;
                case 2:
                    Txt_Enero.Enabled = false;
                    Txt_Febrero.Enabled = true;
                    Txt_Marzo.Enabled = true;
                    Txt_Abril.Enabled = true;
                    Txt_Mayo.Enabled = true;
                    Txt_Junio.Enabled = true;
                    Txt_Julio.Enabled = true;
                    Txt_Agosto.Enabled = true;
                    Txt_Septiembre.Enabled = true;
                    Txt_Octubre.Enabled = true;
                    Txt_Noviembre.Enabled = true;
                    Txt_Diciembre.Enabled = true;

                    Txt_Enero_Destino.Enabled = false;
                    Txt_Febrero_Destino.Enabled = true;
                    Txt_Marzo_Destino.Enabled = true;
                    Txt_Abril_Destino.Enabled = true;
                    Txt_Mayo_Destino.Enabled = true;
                    Txt_Junio_Destino.Enabled = true;
                    Txt_Julio_Destino.Enabled = true;
                    Txt_Agosto_Destino.Enabled = true;
                    Txt_Septiembre_Destino.Enabled = true;
                    Txt_Octubre_Destino.Enabled = true;
                    Txt_Noviembre_Destino.Enabled = true;
                    Txt_Diciembre_Destino.Enabled = true;

                    Hf_Mes_Actual.Value = "FEBRERO";
                    break;
                case 3:
                    Txt_Enero.Enabled = false;
                    Txt_Febrero.Enabled = false;
                    Txt_Marzo.Enabled = true;
                    Txt_Abril.Enabled = true;
                    Txt_Mayo.Enabled = true;
                    Txt_Junio.Enabled = true;
                    Txt_Julio.Enabled = true;
                    Txt_Agosto.Enabled = true;
                    Txt_Septiembre.Enabled = true;
                    Txt_Octubre.Enabled = true;
                    Txt_Noviembre.Enabled = true;
                    Txt_Diciembre.Enabled = true;

                    Txt_Enero_Destino.Enabled = false;
                    Txt_Febrero_Destino.Enabled = false;
                    Txt_Marzo_Destino.Enabled = true;
                    Txt_Abril_Destino.Enabled = true;
                    Txt_Mayo_Destino.Enabled = true;
                    Txt_Junio_Destino.Enabled = true;
                    Txt_Julio_Destino.Enabled = true;
                    Txt_Agosto_Destino.Enabled = true;
                    Txt_Septiembre_Destino.Enabled = true;
                    Txt_Octubre_Destino.Enabled = true;
                    Txt_Noviembre_Destino.Enabled = true;
                    Txt_Diciembre_Destino.Enabled = true;

                    Hf_Mes_Actual.Value = "MARZO";
                    break;
                case 4:
                    Txt_Enero.Enabled = false;
                    Txt_Febrero.Enabled = false;
                    Txt_Marzo.Enabled = false;
                    Txt_Abril.Enabled = true;
                    Txt_Mayo.Enabled = true;
                    Txt_Junio.Enabled = true;
                    Txt_Julio.Enabled = true;
                    Txt_Agosto.Enabled = true;
                    Txt_Septiembre.Enabled = true;
                    Txt_Octubre.Enabled = true;
                    Txt_Noviembre.Enabled = true;
                    Txt_Diciembre.Enabled = true;

                    Txt_Enero_Destino.Enabled = false;
                    Txt_Febrero_Destino.Enabled = false;
                    Txt_Marzo_Destino.Enabled = false;
                    Txt_Abril_Destino.Enabled = true;
                    Txt_Mayo_Destino.Enabled = true;
                    Txt_Junio_Destino.Enabled = true;
                    Txt_Julio_Destino.Enabled = true;
                    Txt_Agosto_Destino.Enabled = true;
                    Txt_Septiembre_Destino.Enabled = true;
                    Txt_Octubre_Destino.Enabled = true;
                    Txt_Noviembre_Destino.Enabled = true;
                    Txt_Diciembre_Destino.Enabled = true;

                    Hf_Mes_Actual.Value = "ABRIL";
                    break;
                case 5:
                    Txt_Enero.Enabled = false;
                    Txt_Febrero.Enabled = false;
                    Txt_Marzo.Enabled = false;
                    Txt_Abril.Enabled = false;
                    Txt_Mayo.Enabled = true;
                    Txt_Junio.Enabled = true;
                    Txt_Julio.Enabled = true;
                    Txt_Agosto.Enabled = true;
                    Txt_Septiembre.Enabled = true;
                    Txt_Octubre.Enabled = true;
                    Txt_Noviembre.Enabled = true;
                    Txt_Diciembre.Enabled = true;

                    Txt_Enero_Destino.Enabled = false;
                    Txt_Febrero_Destino.Enabled = false;
                    Txt_Marzo_Destino.Enabled = false;
                    Txt_Abril_Destino.Enabled = false;
                    Txt_Mayo_Destino.Enabled = true;
                    Txt_Junio_Destino.Enabled = true;
                    Txt_Julio_Destino.Enabled = true;
                    Txt_Agosto_Destino.Enabled = true;
                    Txt_Septiembre_Destino.Enabled = true;
                    Txt_Octubre_Destino.Enabled = true;
                    Txt_Noviembre_Destino.Enabled = true;
                    Txt_Diciembre_Destino.Enabled = true;

                    Hf_Mes_Actual.Value = "MAYO";
                    break;
                case 6:
                    Txt_Enero.Enabled = false;
                    Txt_Febrero.Enabled = false;
                    Txt_Marzo.Enabled = false;
                    Txt_Abril.Enabled = false;
                    Txt_Mayo.Enabled = false;
                    Txt_Junio.Enabled = true;
                    Txt_Julio.Enabled = true;
                    Txt_Agosto.Enabled = true;
                    Txt_Septiembre.Enabled = true;
                    Txt_Octubre.Enabled = true;
                    Txt_Noviembre.Enabled = true;
                    Txt_Diciembre.Enabled = true;

                    Txt_Enero_Destino.Enabled = false;
                    Txt_Febrero_Destino.Enabled = false;
                    Txt_Marzo_Destino.Enabled = false;
                    Txt_Abril_Destino.Enabled = false;
                    Txt_Mayo_Destino.Enabled = false;
                    Txt_Junio_Destino.Enabled = true;
                    Txt_Julio_Destino.Enabled = true;
                    Txt_Agosto_Destino.Enabled = true;
                    Txt_Septiembre_Destino.Enabled = true;
                    Txt_Octubre_Destino.Enabled = true;
                    Txt_Noviembre_Destino.Enabled = true;
                    Txt_Diciembre_Destino.Enabled = true;

                    Hf_Mes_Actual.Value = "JUNIO";
                    break;
                case 7:
                    Txt_Enero.Enabled = false;
                    Txt_Febrero.Enabled = false;
                    Txt_Marzo.Enabled = false;
                    Txt_Abril.Enabled = false;
                    Txt_Mayo.Enabled = false;
                    Txt_Junio.Enabled = false;
                    Txt_Julio.Enabled = true;
                    Txt_Agosto.Enabled = true;
                    Txt_Septiembre.Enabled = true;
                    Txt_Octubre.Enabled = true;
                    Txt_Noviembre.Enabled = true;
                    Txt_Diciembre.Enabled = true;

                    Txt_Enero_Destino.Enabled = false;
                    Txt_Febrero_Destino.Enabled = false;
                    Txt_Marzo_Destino.Enabled = false;
                    Txt_Abril_Destino.Enabled = false;
                    Txt_Mayo_Destino.Enabled = false;
                    Txt_Junio_Destino.Enabled = false;
                    Txt_Julio_Destino.Enabled = true;
                    Txt_Agosto_Destino.Enabled = true;
                    Txt_Septiembre_Destino.Enabled = true;
                    Txt_Octubre_Destino.Enabled = true;
                    Txt_Noviembre_Destino.Enabled = true;
                    Txt_Diciembre_Destino.Enabled = true;

                    Hf_Mes_Actual.Value = "JULIO";
                    break;
                case 8:
                    Txt_Enero.Enabled = false;
                    Txt_Febrero.Enabled = false;
                    Txt_Marzo.Enabled = false;
                    Txt_Abril.Enabled = false;
                    Txt_Mayo.Enabled = false;
                    Txt_Junio.Enabled = false;
                    Txt_Julio.Enabled = false;
                    Txt_Agosto.Enabled = true;
                    Txt_Septiembre.Enabled = true;
                    Txt_Octubre.Enabled = true;
                    Txt_Noviembre.Enabled = true;
                    Txt_Diciembre.Enabled = true;

                    Txt_Enero_Destino.Enabled = false;
                    Txt_Febrero_Destino.Enabled = false;
                    Txt_Marzo_Destino.Enabled = false;
                    Txt_Abril_Destino.Enabled = false;
                    Txt_Mayo_Destino.Enabled = false;
                    Txt_Junio_Destino.Enabled = false;
                    Txt_Julio_Destino.Enabled = false;
                    Txt_Agosto_Destino.Enabled = true;
                    Txt_Septiembre_Destino.Enabled = true;
                    Txt_Octubre_Destino.Enabled = true;
                    Txt_Noviembre_Destino.Enabled = true;
                    Txt_Diciembre_Destino.Enabled = true;

                    Hf_Mes_Actual.Value = "AGOSTO";
                    break;
                case 9:
                    Txt_Enero.Enabled = false;
                    Txt_Febrero.Enabled = false;
                    Txt_Marzo.Enabled = false;
                    Txt_Abril.Enabled = false;
                    Txt_Mayo.Enabled = false;
                    Txt_Junio.Enabled = false;
                    Txt_Julio.Enabled = false;
                    Txt_Agosto.Enabled = false;
                    Txt_Septiembre.Enabled = true;
                    Txt_Octubre.Enabled = true;
                    Txt_Noviembre.Enabled = true;
                    Txt_Diciembre.Enabled = true;

                    Txt_Enero_Destino.Enabled = false;
                    Txt_Febrero_Destino.Enabled = false;
                    Txt_Marzo_Destino.Enabled = false;
                    Txt_Abril_Destino.Enabled = false;
                    Txt_Mayo_Destino.Enabled = false;
                    Txt_Junio_Destino.Enabled = false;
                    Txt_Julio_Destino.Enabled = false;
                    Txt_Agosto_Destino.Enabled = false;
                    Txt_Septiembre_Destino.Enabled = true;
                    Txt_Octubre_Destino.Enabled = true;
                    Txt_Noviembre_Destino.Enabled = true;
                    Txt_Diciembre_Destino.Enabled = true;

                    Hf_Mes_Actual.Value = "SEPTIEMBRE";
                    break;
                case 10:
                    Txt_Enero.Enabled = false;
                    Txt_Febrero.Enabled = false;
                    Txt_Marzo.Enabled = false;
                    Txt_Abril.Enabled = false;
                    Txt_Mayo.Enabled = false;
                    Txt_Junio.Enabled = false;
                    Txt_Julio.Enabled = false;
                    Txt_Agosto.Enabled = false;
                    Txt_Septiembre.Enabled = false;
                    Txt_Octubre.Enabled = true;
                    Txt_Noviembre.Enabled = true;
                    Txt_Diciembre.Enabled = true;

                    Txt_Enero_Destino.Enabled = false;
                    Txt_Febrero_Destino.Enabled = false;
                    Txt_Marzo_Destino.Enabled = false;
                    Txt_Abril_Destino.Enabled = false;
                    Txt_Mayo_Destino.Enabled = false;
                    Txt_Junio_Destino.Enabled = false;
                    Txt_Julio_Destino.Enabled = false;
                    Txt_Agosto_Destino.Enabled = false;
                    Txt_Septiembre_Destino.Enabled = false;
                    Txt_Octubre_Destino.Enabled = true;
                    Txt_Noviembre_Destino.Enabled = true;
                    Txt_Diciembre_Destino.Enabled = true;

                    Hf_Mes_Actual.Value = "OCTUBRE";
                    break;
                case 11:
                    Txt_Enero.Enabled = false;
                    Txt_Febrero.Enabled = false;
                    Txt_Marzo.Enabled = false;
                    Txt_Abril.Enabled = false;
                    Txt_Mayo.Enabled = false;
                    Txt_Junio.Enabled = false;
                    Txt_Julio.Enabled = false;
                    Txt_Agosto.Enabled = false;
                    Txt_Septiembre.Enabled = false;
                    Txt_Octubre.Enabled = false;
                    Txt_Noviembre.Enabled = true;
                    Txt_Diciembre.Enabled = true;

                    Txt_Enero_Destino.Enabled = false;
                    Txt_Febrero_Destino.Enabled = false;
                    Txt_Marzo_Destino.Enabled = false;
                    Txt_Abril_Destino.Enabled = false;
                    Txt_Mayo_Destino.Enabled = false;
                    Txt_Junio_Destino.Enabled = false;
                    Txt_Julio_Destino.Enabled = false;
                    Txt_Agosto_Destino.Enabled = false;
                    Txt_Septiembre_Destino.Enabled = false;
                    Txt_Octubre_Destino.Enabled = false;
                    Txt_Noviembre_Destino.Enabled = true;
                    Txt_Diciembre_Destino.Enabled = true;

                    Hf_Mes_Actual.Value = "NOVIEMBRE";
                    break;
                case 12:
                    Txt_Enero.Enabled = false;
                    Txt_Febrero.Enabled = false;
                    Txt_Marzo.Enabled = false;
                    Txt_Abril.Enabled = false;
                    Txt_Mayo.Enabled = false;
                    Txt_Junio.Enabled = false;
                    Txt_Julio.Enabled = false;
                    Txt_Agosto.Enabled = false;
                    Txt_Septiembre.Enabled = false;
                    Txt_Octubre.Enabled = false;
                    Txt_Noviembre.Enabled = false;
                    Txt_Diciembre.Enabled = true;

                    Txt_Enero_Destino.Enabled = false;
                    Txt_Febrero_Destino.Enabled = false;
                    Txt_Marzo_Destino.Enabled = false;
                    Txt_Abril_Destino.Enabled = false;
                    Txt_Mayo_Destino.Enabled = false;
                    Txt_Junio_Destino.Enabled = false;
                    Txt_Julio_Destino.Enabled = false;
                    Txt_Agosto_Destino.Enabled = false;
                    Txt_Septiembre_Destino.Enabled = false;
                    Txt_Octubre_Destino.Enabled = false;
                    Txt_Noviembre_Destino.Enabled = false;
                    Txt_Diciembre_Destino.Enabled = true;

                    Hf_Mes_Actual.Value = "DICIEMBRE";
                    break;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al llenar el combo de meses. Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_grid_Partidas
    ///DESCRIPCIÓN          : consulta para obtener los datos de las partidas especificas
    ///PARAMETROS           : 
    ///CREO                 : Leslie Gonzalez Vázquez
    ///FECHA_CREO           : 05/marzo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_grid_Partidas()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();
        DataTable Dt_Partidas = new DataTable();

        try
        {
            Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
            Negocio.P_Dependencia_ID_Busqueda = Cmb_Unidad_Responsable_Origen.SelectedItem.Value.Trim();
            Negocio.P_Area_Funcional_ID = Hf_Area_Funcional_Origen_ID.Value.Trim();

            if (!String.IsNullOrEmpty(Hf_Partida_Origen.Value.Trim()))
            {
                Negocio.P_Partida_Especifica_ID = Hf_Partida_Origen.Value.Trim();
            }
            else
            {
                Negocio.P_Partida_Especifica_ID = String.Empty;
            }

            if (Cmb_Fuente_Financiamiento_Origen.SelectedIndex > 0)
            {
                Negocio.P_Fuente_Financiamiento_ID = Cmb_Fuente_Financiamiento_Origen.SelectedItem.Value.Trim();
            }
            else
            {
                Negocio.P_Fuente_Financiamiento_ID = String.Empty;
            }

            Dt_Partidas = Negocio.Consultar_Partidas_Especificas();

            if (Dt_Partidas is DataTable)
            {
                Grid_Partidas.DataBind();

                Grid_Partidas.Columns[0].Visible = true;
                Grid_Partidas.Columns[5].Visible = true;
                Grid_Partidas.Columns[6].Visible = true;
                Grid_Partidas.Columns[7].Visible = true;
                Grid_Partidas.Columns[8].Visible = true;
                Grid_Partidas.Columns[9].Visible = true;
                Grid_Partidas.Columns[10].Visible = true;
                Grid_Partidas.Columns[11].Visible = true;
                Grid_Partidas.Columns[12].Visible = true;
                Grid_Partidas.Columns[13].Visible = true;
                Grid_Partidas.Columns[14].Visible = true;
                Grid_Partidas.Columns[15].Visible = true;
                Grid_Partidas.Columns[16].Visible = true;
                Grid_Partidas.Columns[17].Visible = true;
                Grid_Partidas.Columns[18].Visible = true;
                Grid_Partidas.Columns[19].Visible = true;
                Grid_Partidas.Columns[20].Visible = true;
                Grid_Partidas.Columns[21].Visible = true;
                Grid_Partidas.DataSource = Dt_Partidas;
                Grid_Partidas.DataBind();
                Grid_Partidas.Columns[5].Visible = false;
                Grid_Partidas.Columns[6].Visible = false;
                Grid_Partidas.Columns[7].Visible = false;
                Grid_Partidas.Columns[8].Visible = false;
                Grid_Partidas.Columns[9].Visible = false;
                Grid_Partidas.Columns[10].Visible = false;
                Grid_Partidas.Columns[11].Visible = false;
                Grid_Partidas.Columns[12].Visible = false;
                Grid_Partidas.Columns[13].Visible = false;
                Grid_Partidas.Columns[14].Visible = false;
                Grid_Partidas.Columns[15].Visible = false;
                Grid_Partidas.Columns[16].Visible = false;
                Grid_Partidas.Columns[17].Visible = false;
                Grid_Partidas.Columns[18].Visible = false;
                Grid_Partidas.Columns[19].Visible = false;
                Grid_Partidas.Columns[20].Visible = false;
                Grid_Partidas.Columns[21].Visible = false;
                Div_Partidas.Visible = true;
            }
            Grid_Partidas.SelectedIndex = -1;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al llenar el grid de las partidas. Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Crear_Tabla_Meses
    ///DESCRIPCIÓN          : metodo para obtener los importes de los meses
    ///PARAMETROS           : 
    ///CREO                 : Leslie Gonzalez Vázquez
    ///FECHA_CREO           : 08/Marzo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private DataTable Crear_Tabla_Meses()
    {
        DataTable Dt_Meses = new DataTable();
        DataRow Fila;

        try
        {
            Dt_Meses.Columns.Add("ENERO");
            Dt_Meses.Columns.Add("FEBRERO");
            Dt_Meses.Columns.Add("MARZO");
            Dt_Meses.Columns.Add("ABRIL");
            Dt_Meses.Columns.Add("MAYO");
            Dt_Meses.Columns.Add("JUNIO");
            Dt_Meses.Columns.Add("JULIO");
            Dt_Meses.Columns.Add("AGOSTO");
            Dt_Meses.Columns.Add("SEPTIEMBRE");
            Dt_Meses.Columns.Add("OCTUBRE");
            Dt_Meses.Columns.Add("NOVIEMBRE");
            Dt_Meses.Columns.Add("DICIEMBRE");
            Dt_Meses.Columns.Add("TIPO");

            Fila = Dt_Meses.NewRow();

            if (String.IsNullOrEmpty(Txt_Enero.Text.Trim()))
            {
                Fila["ENERO"] = "0.00";
            }
            else
            {
                Fila["ENERO"] = Txt_Enero.Text.Trim().Replace(",", "");
            }

            if (String.IsNullOrEmpty(Txt_Febrero.Text.Trim()))
            {
                Fila["FEBRERO"] = "0.00";
            }
            else
            {
                Fila["FEBRERO"] = Txt_Febrero.Text.Trim().Replace(",", "");
            }

            if (String.IsNullOrEmpty(Txt_Marzo.Text.Trim()))
            {
                Fila["MARZO"] = "0.00";
            }
            else
            {
                Fila["MARZO"] = Txt_Marzo.Text.Trim().Replace(",", "");
            }

            if (String.IsNullOrEmpty(Txt_Abril.Text.Trim()))
            {
                Fila["ABRIL"] = "0.00";
            }
            else
            {
                Fila["ABRIL"] = Txt_Abril.Text.Trim().Replace(",", "");
            }

            if (String.IsNullOrEmpty(Txt_Mayo.Text.Trim()))
            {
                Fila["MAYO"] = "0.00";
            }
            else
            {
                Fila["MAYO"] = Txt_Mayo.Text.Trim().Replace(",", "");
            }

            if (String.IsNullOrEmpty(Txt_Junio.Text.Trim()))
            {
                Fila["JUNIO"] = "0.00";
            }
            else
            {
                Fila["JUNIO"] = Txt_Junio.Text.Trim().Replace(",", "");
            }

            if (String.IsNullOrEmpty(Txt_Julio.Text.Trim()))
            {
                Fila["JULIO"] = "0.00";
            }
            else
            {
                Fila["JULIO"] = Txt_Julio.Text.Trim().Replace(",", "");
            }

            if (String.IsNullOrEmpty(Txt_Agosto.Text.Trim()))
            {
                Fila["AGOSTO"] = "0.00";
            }
            else
            {
                Fila["AGOSTO"] = Txt_Agosto.Text.Trim().Replace(",", "");
            }

            if (String.IsNullOrEmpty(Txt_Septiembre.Text.Trim()))
            {
                Fila["SEPTIEMBRE"] = "0.00";
            }
            else
            {
                Fila["SEPTIEMBRE"] = Txt_Septiembre.Text.Trim().Replace(",", "");
            }

            if (String.IsNullOrEmpty(Txt_Octubre.Text.Trim()))
            {
                Fila["OCTUBRE"] = "0.00";
            }
            else
            {
                Fila["OCTUBRE"] = Txt_Octubre.Text.Trim().Replace(",", "");
            }

            if (String.IsNullOrEmpty(Txt_Noviembre.Text.Trim()))
            {
                Fila["NOVIEMBRE"] = "0.00";
            }
            else
            {
                Fila["NOVIEMBRE"] = Txt_Noviembre.Text.Trim().Replace(",", "");
            }

            if (String.IsNullOrEmpty(Txt_Diciembre.Text.Trim()))
            {
                Fila["DICIEMBRE"] = "0.00";
            }
            else
            {
                Fila["DICIEMBRE"] = Txt_Diciembre.Text.Trim().Replace(",", "");
            }

            if (Rbl_Tipos_Paritda.Text.Trim().Equals("1"))
            {
                Fila["TIPO"] = "Partida_Origen_Nueva";
            }
            else
            {
                Fila["TIPO"] = "Partida_Origen";
            }

            Dt_Meses.Rows.Add(Fila);

            if (Cmb_Operacion.SelectedItem.Value.Equals("TRASPASO"))
            {
                Fila = Dt_Meses.NewRow();

                if (String.IsNullOrEmpty(Txt_Enero_Destino.Text.Trim()))
                {
                    Fila["ENERO"] = "0.00";
                }
                else
                {
                    Fila["ENERO"] = Txt_Enero_Destino.Text.Trim().Replace(",", "");
                }

                if (String.IsNullOrEmpty(Txt_Febrero_Destino.Text.Trim()))
                {
                    Fila["FEBRERO"] = "0.00";
                }
                else
                {
                    Fila["FEBRERO"] = Txt_Febrero_Destino.Text.Trim().Replace(",", "");
                }

                if (String.IsNullOrEmpty(Txt_Marzo_Destino.Text.Trim()))
                {
                    Fila["MARZO"] = "0.00";
                }
                else
                {
                    Fila["MARZO"] = Txt_Marzo_Destino.Text.Trim().Replace(",", "");
                }

                if (String.IsNullOrEmpty(Txt_Abril_Destino.Text.Trim()))
                {
                    Fila["ABRIL"] = "0.00";
                }
                else
                {
                    Fila["ABRIL"] = Txt_Abril_Destino.Text.Trim().Replace(",", "");
                }

                if (String.IsNullOrEmpty(Txt_Mayo_Destino.Text.Trim()))
                {
                    Fila["MAYO"] = "0.00";
                }
                else
                {
                    Fila["MAYO"] = Txt_Mayo_Destino.Text.Trim().Replace(",", "");
                }

                if (String.IsNullOrEmpty(Txt_Junio_Destino.Text.Trim()))
                {
                    Fila["JUNIO"] = "0.00";
                }
                else
                {
                    Fila["JUNIO"] = Txt_Junio_Destino.Text.Trim().Replace(",", "");
                }

                if (String.IsNullOrEmpty(Txt_Julio_Destino.Text.Trim()))
                {
                    Fila["JULIO"] = "0.00";
                }
                else
                {
                    Fila["JULIO"] = Txt_Julio_Destino.Text.Trim().Replace(",", "");
                }

                if (String.IsNullOrEmpty(Txt_Agosto_Destino.Text.Trim()))
                {
                    Fila["AGOSTO"] = "0.00";
                }
                else
                {
                    Fila["AGOSTO"] = Txt_Agosto_Destino.Text.Trim().Replace(",", "");
                }

                if (String.IsNullOrEmpty(Txt_Septiembre_Destino.Text.Trim()))
                {
                    Fila["SEPTIEMBRE"] = "0.00";
                }
                else
                {
                    Fila["SEPTIEMBRE"] = Txt_Septiembre_Destino.Text.Trim().Replace(",", "");
                }

                if (String.IsNullOrEmpty(Txt_Octubre_Destino.Text.Trim()))
                {
                    Fila["OCTUBRE"] = "0.00";
                }
                else
                {
                    Fila["OCTUBRE"] = Txt_Octubre_Destino.Text.Trim().Replace(",", "");
                }

                if (String.IsNullOrEmpty(Txt_Noviembre_Destino.Text.Trim()))
                {
                    Fila["NOVIEMBRE"] = "0.00";
                }
                else
                {
                    Fila["NOVIEMBRE"] = Txt_Noviembre_Destino.Text.Trim().Replace(",", "");
                }

                if (String.IsNullOrEmpty(Txt_Diciembre_Destino.Text.Trim()))
                {
                    Fila["DICIEMBRE"] = "0.00";
                }
                else
                {
                    Fila["DICIEMBRE"] = Txt_Diciembre_Destino.Text.Trim().Replace(",", "");
                }

                if (Rbl_Tipos_Paritda.Text.Trim().Equals("1"))
                {
                    Fila["TIPO"] = "Partida_Destino_Nueva";
                }
                else
                {
                    Fila["TIPO"] = "Partida_Destino";
                }

                Dt_Meses.Rows.Add(Fila);
            }

        }
        catch (Exception ex)
        {
            throw new Exception("Error al crear la tabla de los importes de los meses. Error[" + ex.Message + "]");
        }
        return Dt_Meses;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_grid_Partidas_Destino
    ///DESCRIPCIÓN          : consulta para obtener los datos de las partidas especificas
    ///PARAMETROS           : 
    ///CREO                 : Leslie Gonzalez Vázquez
    ///FECHA_CREO           : 08/Marzo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_grid_Partidas_Destino()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();
        DataTable Dt_Partidas = new DataTable();

        try
        {
            Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
            Negocio.P_Dependencia_ID_Busqueda = Cmb_Unidad_Responsable_Destino.SelectedItem.Value.Trim();
            Negocio.P_Area_Funcional_ID = Hf_Area_Funcional_Destino_ID.Value.Trim();

            if (!String.IsNullOrEmpty(Hf_Partida_Destino.Value.Trim()))
            {
                Negocio.P_Partida_Especifica_ID = Hf_Partida_Destino.Value.Trim();
            }
            else
            {
                Negocio.P_Partida_Especifica_ID = String.Empty;
            }

            if (Cmb_Fuente_Financiamiento_Destino.SelectedIndex > 0)
            {
                Negocio.P_Fuente_Financiamiento_ID = Cmb_Fuente_Financiamiento_Destino.SelectedItem.Value.Trim();
            }
            else
            {
                Negocio.P_Fuente_Financiamiento_ID = String.Empty;
            }

            Dt_Partidas = Negocio.Consultar_Partidas_Especificas();

            if (Dt_Partidas is DataTable)
            {
                Grid_Partidas_Destino.DataBind();

                Grid_Partidas_Destino.Columns[0].Visible = true;
                Grid_Partidas_Destino.Columns[5].Visible = true;
                Grid_Partidas_Destino.Columns[6].Visible = true;
                Grid_Partidas_Destino.Columns[7].Visible = true;
                Grid_Partidas_Destino.Columns[8].Visible = true;
                Grid_Partidas_Destino.Columns[9].Visible = true;
                Grid_Partidas_Destino.Columns[10].Visible = true;
                Grid_Partidas_Destino.Columns[11].Visible = true;
                Grid_Partidas_Destino.Columns[12].Visible = true;
                Grid_Partidas_Destino.Columns[13].Visible = true;
                Grid_Partidas_Destino.Columns[14].Visible = true;
                Grid_Partidas_Destino.Columns[15].Visible = true;
                Grid_Partidas_Destino.Columns[16].Visible = true;
                Grid_Partidas_Destino.Columns[17].Visible = true;
                Grid_Partidas_Destino.Columns[18].Visible = true;
                Grid_Partidas_Destino.Columns[19].Visible = true;
                Grid_Partidas_Destino.Columns[20].Visible = true;
                Grid_Partidas_Destino.Columns[21].Visible = true;
                Grid_Partidas_Destino.DataSource = Dt_Partidas;
                Grid_Partidas_Destino.DataBind();
                Grid_Partidas_Destino.Columns[5].Visible = false;
                Grid_Partidas_Destino.Columns[6].Visible = false;
                Grid_Partidas_Destino.Columns[7].Visible = false;
                Grid_Partidas_Destino.Columns[8].Visible = false;
                Grid_Partidas_Destino.Columns[9].Visible = false;
                Grid_Partidas_Destino.Columns[10].Visible = false;
                Grid_Partidas_Destino.Columns[11].Visible = false;
                Grid_Partidas_Destino.Columns[12].Visible = false;
                Grid_Partidas_Destino.Columns[13].Visible = false;
                Grid_Partidas_Destino.Columns[14].Visible = false;
                Grid_Partidas_Destino.Columns[15].Visible = false;
                Grid_Partidas_Destino.Columns[16].Visible = false;
                Grid_Partidas_Destino.Columns[17].Visible = false;
                Grid_Partidas_Destino.Columns[18].Visible = false;
                Grid_Partidas_Destino.Columns[19].Visible = false;
                Grid_Partidas_Destino.Columns[20].Visible = false;
                Grid_Partidas_Destino.Columns[21].Visible = false;
                Div_Partidas.Visible = true;
            }
            Grid_Partidas_Destino.SelectedIndex = -1;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al llenar el grid de las partidas. Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Sumar_Total_Origen
    ///DESCRIPCIÓN          : Metodo para sumar el total de la partida origen
    ///PARAMETROS           : 
    ///CREO                 : Leslie Gonzalez Vázquez
    ///FECHA_CREO           : 09/Marzo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Sumar_Total_Origen()
    {
        Double Total = 0.00;

        try
        {
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Enero.Text.Trim()) ? "0" : Txt_Enero.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Febrero.Text.Trim()) ? "0" : Txt_Febrero.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Marzo.Text.Trim()) ? "0" : Txt_Marzo.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Abril.Text.Trim()) ? "0" : Txt_Abril.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Mayo.Text.Trim()) ? "0" : Txt_Mayo.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Junio.Text.Trim()) ? "0" : Txt_Junio.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Julio.Text.Trim()) ? "0" : Txt_Julio.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Agosto.Text.Trim()) ? "0" : Txt_Agosto.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Septiembre.Text.Trim()) ? "0" : Txt_Septiembre.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Octubre.Text.Trim()) ? "0" : Txt_Octubre.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Noviembre.Text.Trim()) ? "0" : Txt_Noviembre.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Diciembre.Text.Trim()) ? "0" : Txt_Diciembre.Text.Trim());

            Txt_Total.Text = String.Format("{0:#,###,##0.00}", Total);
        }
        catch (Exception ex)
        {
            throw new Exception("Error al sumar el total de la partida origen. Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Sumar_Total_Destino
    ///DESCRIPCIÓN          : Metodo para sumar el total de la partida destino
    ///PARAMETROS           : 
    ///CREO                 : Leslie Gonzalez Vázquez
    ///FECHA_CREO           : 09/Marzo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Sumar_Total_Destino()
    {
        Double Total = 0.00;

        try
        {
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Enero_Destino.Text.Trim()) ? "0" : Txt_Enero_Destino.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Febrero_Destino.Text.Trim()) ? "0" : Txt_Febrero_Destino.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Marzo_Destino.Text.Trim()) ? "0" : Txt_Marzo_Destino.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Abril_Destino.Text.Trim()) ? "0" : Txt_Abril_Destino.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Mayo_Destino.Text.Trim()) ? "0" : Txt_Mayo_Destino.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Junio_Destino.Text.Trim()) ? "0" : Txt_Junio_Destino.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Julio_Destino.Text.Trim()) ? "0" : Txt_Julio_Destino.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Agosto_Destino.Text.Trim()) ? "0" : Txt_Agosto_Destino.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Septiembre_Destino.Text.Trim()) ? "0" : Txt_Septiembre_Destino.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Octubre_Destino.Text.Trim()) ? "0" : Txt_Octubre_Destino.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Noviembre_Destino.Text.Trim()) ? "0" : Txt_Noviembre_Destino.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Diciembre_Destino.Text.Trim()) ? "0" : Txt_Diciembre_Destino.Text.Trim());

            Txt_Total_Des.Text = String.Format("{0:#,###,##0.00}", Total);
        }
        catch (Exception ex)
        {
            throw new Exception("Error al sumar el total de la partida  destino. Error[" + ex.Message + "]");
        }
    }
    #endregion

    #region(Control Acceso Pagina)
    /// ******************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// PARÁMETROS  :
    /// USUARIO CREÓ: Hugo Enrique Ramírez Aguilera
    /// FECHA CREÓ  : 07/Noviembre/2011 
    /// USUARIO MODIFICO  :
    /// FECHA MODIFICO    :
    /// CAUSA MODIFICACIÓN:
    /// ******************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            Botones.Add(Btn_Nuevo);
            Botones.Add(Btn_Buscar);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Es_Numero
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS  : Cadena.- El dato a evaluar si es numerico.
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 07/Noviembre/2011 
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion

    #region(Validacion)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Datos
    /// DESCRIPCION : Validar que se hallan proporcionado todos los datos
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 07/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar_Datos()
    {
        String Espacios_Blanco;
        DataTable Dt_Partidas = new DataTable();
        String Mes = String.Empty;
        Double Importe = 0.00;
        Double Total_Origen = 0.00;
        Double Total_Destino = 0.00;
        Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
        Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
        Espacios_Blanco = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        Sumar_Total_Destino();
        Sumar_Total_Origen();


        Total_Origen = Convert.ToDouble(String.IsNullOrEmpty(Txt_Total.Text.Trim()) ? "0" : Txt_Total.Text.Trim());
        Total_Destino = Convert.ToDouble(String.IsNullOrEmpty(Txt_Total_Des.Text.Trim()) ? "0" : Txt_Total_Des.Text.Trim());
        Importe = Convert.ToDouble(String.IsNullOrEmpty(Txt_Importe.Text.Trim()) ? "0" : Txt_Importe.Text.Trim());

        //para la seccion de datos generales
        if (Cmb_Operacion.SelectedIndex < 0)
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Seleccione la operacion deseada. <br>";
            Datos_Validos = false;
        }
        else
        {
            if (Cmb_Operacion.SelectedItem.Text.Trim().Equals("TRASPASO"))
            {
                if (Cmb_Unidad_Responsable_Destino.SelectedIndex <= 0)
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Seleccione algun Unidad responsable de Destino.<br>";
                    Datos_Validos = false;
                }
                if (Cmb_Fuente_Financiamiento_Destino.SelectedIndex <= 0)
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Seleccione algun Fuente de financiamiento de Destino.<br>";
                    Datos_Validos = false;
                }
                else 
                {
                    if (!Cmb_Fuente_Financiamiento_Origen.SelectedItem.Value.Trim().Equals(Cmb_Fuente_Financiamiento_Destino.SelectedItem.Value.Trim()))
                    {
                        Lbl_Mensaje_Error.Text += Espacios_Blanco + "*El traspaso solo se puede realizar entre fuentes de financiamiento iguales.<br>";
                        Datos_Validos = false;
                    }
                }

                if (!String.IsNullOrEmpty(Hf_Programa_Origen.Value.Trim()))
                {
                    if (Rbl_Tipos_Paritda.Text.Trim().Equals("1"))
                    {
                        if (!Hf_Programa_Origen.Value.Trim().Equals(Cmb_Programa_Destino.SelectedItem.Value.Trim()))
                        {
                            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*El traspaso solo se puede realizar entre programas iguales.<br>";
                            Datos_Validos = false;
                        }
                    }
                    else
                    {
                        if (!Hf_Programa_Origen.Value.Trim().Equals(Hf_Programa_Destino.Value.Trim()))
                        {
                            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*El traspaso solo se puede realizar entre programas iguales.<br>";
                            Datos_Validos = false;
                        }
                    }
                }

                if (Txt_Total_Des.Text == "")
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese el importe de los meses de la partida destino.<br>";
                    Datos_Validos = false;
                }
                else
                {
                    if (Total_Destino <= 0)
                    {
                        Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese el importe de los meses de la partida destino.<br>";
                        Datos_Validos = false;
                    }
                }
                if (Importe < Total_Destino || Total_Destino < Importe)
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Favor de verificar los datos de la operacion porque el impote no coincide con el total de la partida destino.<br>";
                    Datos_Validos = false;
                }
                if (Rbl_Tipos_Paritda.Text.Trim().Equals("1"))
                {
                    if (Cmb_Programa_Destino.SelectedIndex <= 0)
                    {
                        Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Seleccione un programa.<br>";
                        Datos_Validos = false;
                    }
                }
            }
        }
        if (Txt_Importe.Text == "")
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese el Importe deseado.<br>";
            Datos_Validos = false;
        }
        if (Cmb_Unidad_Responsable_Origen.SelectedIndex <= 0)
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Seleccione algun Unidad responsable de Origen.<br>";
            Datos_Validos = false;
        }
        if (Cmb_Fuente_Financiamiento_Origen.SelectedIndex <= 0)
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Seleccione algun Fuente de financiamiento de Origen.<br>";
            Datos_Validos = false;
        }
        if (Txt_Justificacion.Text == "")
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese la justificación.<br>";
            Datos_Validos = false;
        }


        if (Txt_Total.Text == "")
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese el importe de los meses de la partida origen.<br>";
            Datos_Validos = false;
        }
        else
        {
            if (Total_Origen <= 0)
            {
                Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese el importe de los meses de la partida origen.<br>";
                Datos_Validos = false;
            }
        }

        if (Importe < Total_Origen || Total_Origen < Importe)
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Favor de verificar los datos de la operacion porque el impote no coincide con el total de la partida origen.<br>";
            Datos_Validos = false;
        }

        if (Rbl_Tipos_Paritda.Text.Trim().Equals("1"))
        {
            if (Cmb_Operacion.SelectedItem.Text.Trim().Equals("AMPLIACION"))
            {

                if (Cmb_Programa_Origen.SelectedIndex <= 0)
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Seleccione un programa.<br>";
                    Datos_Validos = false;
                }
            }
        }

        return Datos_Validos;
    }
    #endregion

    #region(metodos De consulta)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Movimiento
    /// DESCRIPCION : Consulta los movimientos que estan dadas de alta en la BD
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramirez Aguilera
    /// FECHA_CREO  : 17-Noviembre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consulta_Movimiento()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Consulta = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();
        DataTable Dt_Movimiento; //Variable que obtendra los datos de la consulta 

        try
        {
            if (Cmb_Unidad_Responsable.SelectedIndex > 0)
            {
                Consulta.P_Responsable = Cmb_Unidad_Responsable.SelectedValue.Trim();
            }
            else
            {
                Consulta.P_Responsable = String.Empty;
            }

            Dt_Movimiento = Consulta.Consultar_Like_Movimiento();
            Session["Consulta_Movimiento_Presupuestal"] = Dt_Movimiento;
            Grid_Movimiento.DataSource = (DataTable)Session["Consulta_Movimiento_Presupuestal"];
            Grid_Movimiento.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception("Movimiento Presupuestal" + ex.Message.ToString(), ex);
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Movimiento
    /// DESCRIPCION : Consulta los movimientos que estan dadas de alta en la BD
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramirez Aguilera
    /// FECHA_CREO  : 17-Noviembre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Cargar_Grid_Comentario(String Indice)
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Consulta  = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();
        DataTable Dt_Consulta; //Variable que obtendra los datos de la consulta 

        try
        {
            Consulta.P_No_Solicitud = Indice;
            Dt_Consulta = Consulta.Consulta_Datos_Comentarios();
            Session["Consulta_Comentarios"] = Dt_Consulta;
            Grid_Comentarios.DataSource = (DataTable)Session["Consulta_Comentarios"];
            Grid_Comentarios.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception("Movimiento Presupuestal" + ex.Message.ToString(), ex);
        }
    }

    #endregion
    #endregion

    #region(Eventos)
    #region(Botones)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
    ///DESCRIPCIÓN: Habilita las cajas de texto necesarias para crear un Nuevo Movimiento
    ///             se convierte en dar alta cuando oprimimos Nuevo y dar alta  Crea un registro  
    ///                de un movimiento presupuestal en la base de datos
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  07/Noviembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Rs_Alta_Movimiento = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();
        DataTable Dt_Meses = new DataTable();

        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            if (Btn_Nuevo.ToolTip == "Nuevo")
            {
                Limpiar_Controles();
                Hf_Area_Funcional_Destino.Value = "";
                Hf_Area_Funcional_Destino_ID.Value = "";
                Hf_Area_Funcional_Origen.Value = "";
                Hf_Area_Funcional_Origen_ID.Value = "";
                Hf_Programa_Origen.Value = "";
                Hf_Programa_Destino.Value = "";
                Hf_Partida_Origen.Value = "";
                Hf_Partida_Destino.Value = "";
                Hf_Capitulo_Destino.Value = "";
                Hf_Capitulo_Origen.Value = "";
                Hf_Disponible.Value = "0.00";
                Habilitar_Controles("Nuevo"); //Habilita los controles para la introducción de datos por parte del usuario
                Cargar_Combo_Responsable();
                Cargar_Combo_Financiamiento(1);
                Cargar_Combo_Financiamiento(2);
                Llenar_grid_Partidas();
                Llenar_grid_Partidas_Destino();
                Div_Grid_Movimientos.Visible = false;
                Div_Datos.Visible = true;
                Tabla_Meses.Visible = false;
                Tr_Tabla_Meses_Destino.Visible = false;
                Grid_Comentarios.DataSource = null;
                Grid_Comentarios.DataBind();
                Cmb_Estatus.SelectedIndex = 1;
                Cmb_Estatus.Enabled = false;
                Div_Grid_Comentarios.Visible = false;
                Div_Partida_Destino.Visible = true;

                Tr_Capitulo_Destino.Visible = false;
                Tr_Capitulo_Origen.Visible = false;
                Tr_Programa_Destino.Visible = false;
                Tr_Programa_Origen.Visible = false;
                Tr_Tipo_Partida.Visible = true;
                Hf_Tipo_Presupuesto.Value = "Existente";
                Rbl_Tipos_Paritda.SelectedIndex = 0;
            }
            else
            {
                //se validaran los datos para saber si las sumas son iguales
                if (Validar_Datos())
                {
                    Dt_Meses = Crear_Tabla_Meses();

                    Rs_Alta_Movimiento.P_Monto = Txt_Importe.Text.Replace(",", "");
                    Rs_Alta_Movimiento.P_Justificacion = Txt_Justificacion.Text;
                    Rs_Alta_Movimiento.P_Estatus = Cmb_Estatus.SelectedValue;
                    Rs_Alta_Movimiento.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                    Rs_Alta_Movimiento.P_Tipo_Operacion = Cmb_Operacion.SelectedValue.Trim();

                    Rs_Alta_Movimiento.P_Codigo_Programatico_De = Txt_Codigo1.Text.ToUpper();//informacion del origen
                    Rs_Alta_Movimiento.P_Origen_Fuente_Financiamiento_Id = Cmb_Fuente_Financiamiento_Origen.SelectedItem.Value.Trim();
                    Rs_Alta_Movimiento.P_Origen_Area_Funcional_Id = Hf_Area_Funcional_Origen_ID.Value.Trim();
                    Rs_Alta_Movimiento.P_Origen_Programa_Id = Hf_Programa_Origen.Value.Trim();
                    Rs_Alta_Movimiento.P_Origen_Dependencia_Id = Cmb_Unidad_Responsable_Origen.SelectedValue.Trim();
                    Rs_Alta_Movimiento.P_Origen_Partida_Id = Hf_Partida_Origen.Value.Trim();
                    Rs_Alta_Movimiento.P_Meses = Dt_Meses;
                    Rs_Alta_Movimiento.P_Mes_Actual = Hf_Mes_Actual.Value.Trim();
                    Rs_Alta_Movimiento.P_Anio = String.Format("{0:yyyy}", DateTime.Now);

                    if (Cmb_Operacion.SelectedValue.Trim().Equals("TRASPASO"))
                    {
                        Rs_Alta_Movimiento.P_Codigo_Programatico_Al = Txt_Codigo2.Text.ToUpper();//informacion del destino
                        Rs_Alta_Movimiento.P_Destino_Fuente_Financiamiento_Id = Cmb_Fuente_Financiamiento_Destino.SelectedValue.Trim();
                        Rs_Alta_Movimiento.P_Destino_Area_Funcional_Id = Hf_Area_Funcional_Destino_ID.Value.Trim();
                        Rs_Alta_Movimiento.P_Destino_Programa_Id = Hf_Programa_Destino.Value.Trim();
                        Rs_Alta_Movimiento.P_Destino_Dependencia_Id = Cmb_Unidad_Responsable_Destino.SelectedValue.Trim();
                        Rs_Alta_Movimiento.P_Destino_Partida_Id = Hf_Partida_Destino.Value.Trim();

                        if (Rbl_Tipos_Paritda.Text.Trim().Equals("1"))
                        {
                            Rs_Alta_Movimiento.P_Capitulo_ID = Cmb_Capitulo_Destino.SelectedItem.Value.Trim();
                            Rs_Alta_Movimiento.P_Tipo_Partida = "Nueva";
                            Rs_Alta_Movimiento.P_Destino_Programa_Id = Cmb_Programa_Destino.SelectedItem.Value.Trim();
                        }
                        else
                        {
                            Rs_Alta_Movimiento.P_Capitulo_ID = String.Empty;
                            Rs_Alta_Movimiento.P_Tipo_Partida = "Existente";
                            Rs_Alta_Movimiento.P_Destino_Programa_Id = Hf_Programa_Destino.Value.Trim();
                        }
                    }
                    else
                    {
                        Rs_Alta_Movimiento.P_Codigo_Programatico_Al = String.Empty;
                        Rs_Alta_Movimiento.P_Destino_Fuente_Financiamiento_Id = String.Empty;
                        Rs_Alta_Movimiento.P_Destino_Area_Funcional_Id = String.Empty;
                        Rs_Alta_Movimiento.P_Destino_Programa_Id = String.Empty;
                        Rs_Alta_Movimiento.P_Destino_Dependencia_Id = String.Empty;
                        Rs_Alta_Movimiento.P_Destino_Partida_Id = String.Empty;

                        if (Rbl_Tipos_Paritda.Text.Trim().Equals("1"))
                        {
                            Rs_Alta_Movimiento.P_Capitulo_ID = Cmb_Capitulo_Origen.SelectedItem.Value.Trim();
                            Rs_Alta_Movimiento.P_Tipo_Partida = "Nueva";
                            Rs_Alta_Movimiento.P_Origen_Programa_Id = Cmb_Programa_Origen.SelectedItem.Value.Trim();
                        }
                        else
                        {
                            Rs_Alta_Movimiento.P_Capitulo_ID = String.Empty;
                            Rs_Alta_Movimiento.P_Tipo_Partida = "Existente";
                            Rs_Alta_Movimiento.P_Origen_Programa_Id = Hf_Programa_Origen.Value.Trim();
                        }
                    }

                    Rs_Alta_Movimiento.Alta_Movimiento();
                    Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Movimiento Presupuestal", "alert('El Alta del Movimiento Presupuestal fue Exitosa');", true);
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }//else de validar_Datos
            }
        }

        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
    ///DESCRIPCIÓN: Habilita las cajas de texto necesarias para poder Modificar la informacion,
    ///             se convierte en actualizar cuando oprimimos Modificar y se actualiza el registro 
    ///             en la base de datos
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  07/Noviembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Rs_Modificar = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            if (Btn_Modificar.ToolTip == "Modificar")
            {
                if (Txt_No_Solicitud.Text == "")
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "No se encuentra ningun registro seleccionado.<br> *Pruebe seleccionando algun Movimiento de la tabla.";
                }
                else
                {
                    Habilitar_Controles("Modificar"); //Habilita los controles para la modificación de los datos
                    if (Cmb_Estatus.SelectedValue == "AUTORIZADA")
                    {
                        Cmb_Estatus.Enabled = false;
                    }
                    else
                    {
                        if (Cmb_Estatus.Items.Count == 4)
                        {
                            Cmb_Estatus.Items.RemoveAt(3);
                        }
                    }
                }
            }
            else
            {
                if (Validar_Datos())
                {
                    if (Cmb_Estatus.SelectedValue == "AUTORIZADA")
                    {
                        Lbl_Mensaje_Error.Text = "No se puede modificar este registro porque contiene Estatus de AUTORIZADA";
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                    }
                    else
                    {
                        //pasan los datos a la capa de negocio
                        Rs_Modificar.P_No_Solicitud = Txt_No_Solicitud.Text;
                        Rs_Modificar.P_Codigo_Programatico_Al = Txt_Codigo1.Text;
                        Rs_Modificar.P_Codigo_Programatico_De = Txt_Codigo2.Text;
                        Rs_Modificar.P_Monto = Txt_Importe.Text.Replace(",", "");
                        Rs_Modificar.P_Tipo_Operacion = Cmb_Operacion.SelectedValue;
                        Rs_Modificar.P_Justificacion = Txt_Justificacion.Text;
                        Rs_Modificar.P_Estatus = Cmb_Estatus.SelectedValue;
                        Rs_Modificar.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                        //Rs_Modificar.P_Origen_Fuente_Financiamiento_Id = Consulta_Id_Fuente_Financiamiento(Cmb_Fuente_Financiamiento_Origen.SelectedValue);
                        ////Rs_Modificar.P_Origen_Area_Funcional_Id = Consulta_Id_Area_Funcional(Txt_Area_Origen.Text);
                        //Rs_Modificar.P_Origen_Programa_Id = Consulta_Id_Programa(Cmb_Programa_Origen.SelectedValue);
                        //Rs_Modificar.P_Origen_Dependencia_Id = Consulta_Id_Dependencia(Cmb_Unidad_Responsable_Origen.SelectedValue);
                        //Rs_Modificar.P_Origen_Partida_Id = Consulta_Id_Partida(Cmb_Partida_Origen.SelectedValue);
                        //Rs_Modificar.P_Destino_Fuente_Financiamiento_Id = Consulta_Id_Fuente_Financiamiento(Cmb_Fuente_Financiamiento_Destino.SelectedValue);
                        ////Rs_Modificar.P_Destino_Area_Funcional_Id = Consulta_Id_Area_Funcional(Txt_Area_Destino.Text);
                        //Rs_Modificar.P_Destino_Programa_Id = Consulta_Id_Programa(Cmb_Programa_Destino.SelectedValue);
                        //Rs_Modificar.P_Destino_Dependencia_Id = Consulta_Id_Dependencia(Cmb_Unidad_Responsable_Destino.SelectedValue);
                        //Rs_Modificar.P_Destino_Partida_Id = Consulta_Id_Partida(Cmb_Partida_Destino.SelectedValue);
                        Rs_Modificar.Modificar_Movimiento();
                        Cmb_Estatus.Items.RemoveAt(2);
                        Cmb_Estatus.Items.Insert(2, "RECHAZADA");
                        Inicializa_Controles();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Movimiento Presupuestal", "alert('La Modificacion del Movimiento Presupuestal fue Exitosa');", true);
                    }
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }//else de validar_Datos
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Eliminar_Click
    ///DESCRIPCIÓN: cambiara el estatus a cancelada
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  07/Noviembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Eliminar_Click(object sender, ImageClickEventArgs e)
    {
        Cls_Ope_Psp_Autorizar_Solicitud_Traspaso_Presupuestal_Negocio Rs_Modificar_Comentario = new Cls_Ope_Psp_Autorizar_Solicitud_Traspaso_Presupuestal_Negocio();
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Rs_Eliminar = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();
        DataTable Dt_Estatus = new DataTable();
        try
        {

            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";

            if (!string.IsNullOrEmpty(Txt_No_Solicitud.Text.Trim()))
            {
                if (Cmb_Estatus.SelectedValue == "AUTORIZADA")
                {
                    Lbl_Mensaje_Error.Text = "No se puede modificar este registro porque contiene Estatus de AUTORIZADA";
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
                else
                {
                    //pasan los datos a la capa de negocio
                    Rs_Eliminar.P_No_Solicitud = Txt_No_Solicitud.Text;
                    Rs_Eliminar.P_Codigo_Programatico_Al = Txt_Codigo1.Text;
                    Rs_Eliminar.P_Codigo_Programatico_De = Txt_Codigo2.Text;
                    Rs_Eliminar.P_Monto = Txt_Importe.Text.Replace(",", "");
                    Rs_Eliminar.P_Tipo_Operacion = Cmb_Operacion.SelectedValue;
                    Rs_Eliminar.P_Justificacion = Txt_Justificacion.Text;
                    Rs_Eliminar.P_Estatus = "CANCELADA";
                    Rs_Eliminar.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                    //Rs_Eliminar.P_Origen_Fuente_Financiamiento_Id = Consulta_Id_Fuente_Financiamiento(Cmb_Fuente_Financiamiento_Origen.SelectedValue);
                    ////Rs_Eliminar.P_Origen_Area_Funcional_Id = Consulta_Id_Area_Funcional(Txt_Area_Origen.Text);
                    //Rs_Eliminar.P_Origen_Programa_Id = Consulta_Id_Programa(Cmb_Programa_Origen.SelectedValue);
                    //Rs_Eliminar.P_Origen_Dependencia_Id = Consulta_Id_Dependencia(Cmb_Unidad_Responsable_Origen.SelectedValue);
                    //Rs_Eliminar.P_Origen_Partida_Id = Consulta_Id_Partida(Cmb_Partida_Origen.SelectedValue);
                    //Rs_Eliminar.P_Destino_Fuente_Financiamiento_Id = Consulta_Id_Fuente_Financiamiento(Cmb_Fuente_Financiamiento_Destino.SelectedValue);
                    ////Rs_Eliminar.P_Destino_Area_Funcional_Id = Consulta_Id_Area_Funcional(Txt_Area_Destino.Text);
                    //Rs_Eliminar.P_Destino_Programa_Id = Consulta_Id_Programa(Cmb_Programa_Destino.SelectedValue);
                    //Rs_Eliminar.P_Destino_Dependencia_Id = Consulta_Id_Dependencia(Cmb_Unidad_Responsable_Destino.SelectedValue);
                    //Rs_Eliminar.P_Destino_Partida_Id = Consulta_Id_Partida(Cmb_Partida_Destino.SelectedValue);
                    Rs_Eliminar.Modificar_Movimiento();


                    Inicializa_Controles();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Movimiento Presupuestal", "alert('La cancelación fue Exitosa');", true);
                }
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Seleccione algun movimiento de la tabla";
            }//else de validar_Datos
            /*if (!string.IsNullOrEmpty(Txt_No_Solicitud.Text.Trim()))
            {
                Eliminar.P_No_Solicitud = Txt_No_Solicitud.Text;
                Dt_Estatus = Eliminar.Consulta_Movimiento();
                foreach (DataRow Registro in Dt_Estatus.Rows)
                {
                    Estatus = (Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Estatus].ToString());
                    if (Estatus == "AUTORIZADA")
                    {
                        Lbl_Mensaje_Error.Text = "No se puede Eliminar este registro porque contiene Estatus de AUTORIZADO";
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                    }
                    else
                    {
                        Eliminar.P_No_Solicitud = Txt_No_Solicitud.Text;
                        Eliminar.Eliminar_Movimiento(); //Elimina el movimiento presupuestal que fue seleccionada por el usuario
                        Inicializa_Controles();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Movimientos Presupuestal", "alert('Se Elimino correctamente la información');", true);
                                
                    }
                }*/

        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Movimiento_Presupuestal_Click
    ///DESCRIPCIÓN: Busca el movimiento y lo carga en el grid
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  15/Noviembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        Cls_Ope_Psp_Autorizar_Solicitud_Traspaso_Presupuestal_Negocio Rs_Buscar_Traspaso = new Cls_Ope_Psp_Autorizar_Solicitud_Traspaso_Presupuestal_Negocio();
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Rs_Buscar_Fecha = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();
        DataTable Dt_Busqueda = new DataTable();
        Boolean Resultado_Numerico = false;//guardara si el resultado es numero o no
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            Habilitar_Controles("");
            //Cargar_Combo_Responsable();

            if (!string.IsNullOrEmpty(Txt_Busqueda.Text.Trim()))
            {
                Resultado_Numerico = Es_Numero(Txt_Busqueda.Text.Trim());
                if (Resultado_Numerico == true)
                {
                    Rs_Buscar_Fecha.P_No_Solicitud = Txt_Busqueda.Text.ToUpper().Trim();

                    if (Cmb_Unidad_Responsable.SelectedIndex > 0)
                    {
                        Rs_Buscar_Fecha.P_Responsable = Cmb_Unidad_Responsable.SelectedValue;
                    }
                    else
                    {
                        Rs_Buscar_Fecha.P_Responsable = String.Empty;
                    }


                    Dt_Busqueda = Rs_Buscar_Fecha.Consulta_Movimiento_Btn_Busqueda();
                    Grid_Movimiento.DataSource = Dt_Busqueda;
                    Grid_Movimiento.DataBind();
                    Grid_Movimiento.SelectedIndex = -1;
                }
                //Consulta_Movimiento_Fecha

            }
            else
            {
                if (Cmb_Unidad_Responsable.SelectedIndex > 0)
                {
                    Rs_Buscar_Fecha.P_Responsable = Cmb_Unidad_Responsable.SelectedValue;
                }
                else
                {
                    Rs_Buscar_Fecha.P_Responsable = String.Empty;
                }

                if (Txt_Fecha_Inicial.Text == Txt_Fecha_Final.Text)
                {
                    Rs_Buscar_Fecha.P_Fecha_Inicio = String.Format("{0:dd-MMM-yyyy}", Txt_Fecha_Inicial.Text);
                }
                else
                {
                    Rs_Buscar_Fecha.P_Fecha_Inicio = String.Format("{0:dd-MMM-yyyy}", Txt_Fecha_Inicial.Text);
                    Rs_Buscar_Fecha.P_Fecha_Final = String.Format("{0:dd-MMM-yyyy}", Txt_Fecha_Final.Text);
                }
                Dt_Busqueda = Rs_Buscar_Fecha.Consulta_Movimiento_Fecha();
                Grid_Movimiento.DataSource = Dt_Busqueda;
                Grid_Movimiento.DataBind();
                Grid_Movimiento.SelectedIndex = -1;
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Cancela la operacion actual qye se este realizando
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  07/Noviembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if ((Btn_Salir.ToolTip == "Salir") && (Div_Grid_Movimientos.Visible == true))
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else if (Btn_Salir.ToolTip == "Cancelar")
            {
                Inicializa_Controles(); //Habilita los controles para la siguiente operación del usuario en el catálogo
            }
            else
            {
                Inicializa_Controles(); //Habilita los controles para la siguiente operación del usuario en el catálogo
                Grid_Movimiento.SelectedIndex = -1;
            }
            Hf_Area_Funcional_Destino.Value = "";
            Hf_Area_Funcional_Destino_ID.Value = "";
            Hf_Area_Funcional_Origen.Value = "";
            Hf_Area_Funcional_Origen_ID.Value = "";
            Hf_Programa_Origen.Value = "";
            Hf_Programa_Destino.Value = "";
            Hf_Partida_Origen.Value = "";
            Hf_Partida_Destino.Value = "";
            Hf_Capitulo_Destino.Value = "";
            Hf_Capitulo_Origen.Value = "";
            Hf_Disponible.Value = "0.00";
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion

    #region(Combos)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cargar_Combo_Responsable
    ///DESCRIPCIÓN: Cargara todos los responsables dentro del combo 
    ///             (Proviene del metodo Inicializa_Controles())
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  14/Noviembre/2011
    ///MODIFICO:    Leslie Gonzalez Vazquez
    ///FECHA_MODIFICO: 02/Marzo/2012
    ///CAUSA_MODIFICACIÓN: para adecuarlo al presupuesto aprobado
    ///*******************************************************************************
    protected void Cargar_Combo_Responsable()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocios = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos(); //conexion con la capa de negocios
        Cls_Ope_Psp_Clasificacion_Gasto_Negocio Negocio = new Cls_Ope_Psp_Clasificacion_Gasto_Negocio();
        DataTable Dt_Responsable = new DataTable();
        DataTable Dt_Area_Funcional = new DataTable();
        Boolean Administrador;

        Hf_Area_Funcional_Destino.Value = "";
        Hf_Area_Funcional_Destino_ID.Value = "";
        Hf_Area_Funcional_Origen.Value = "";
        Hf_Area_Funcional_Origen_ID.Value = "";
        Hf_Programa_Origen.Value = "";
        Hf_Programa_Destino.Value = "";
        Hf_Partida_Origen.Value = "";
        Hf_Partida_Destino.Value = "";
        Hf_Capitulo_Destino.Value = "";
        Hf_Capitulo_Origen.Value = "";
        Hf_Disponible.Value = "0.00";

        try
        {

            Negocios.P_Tipo_Usuario = "Administrador";
            Dt_Responsable = Negocios.Consultar_URs();

            Cmb_Unidad_Responsable_Origen.Items.Clear();
            Cmb_Unidad_Responsable_Origen.DataSource = Dt_Responsable;
            Cmb_Unidad_Responsable_Origen.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
            Cmb_Unidad_Responsable_Origen.DataTextField = "Nombre";
            Cmb_Unidad_Responsable_Origen.DataBind();
            Cmb_Unidad_Responsable_Origen.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));

            //para destino
            Cmb_Unidad_Responsable_Destino.Items.Clear();
            Cmb_Unidad_Responsable_Destino.DataSource = Dt_Responsable;
            Cmb_Unidad_Responsable_Destino.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
            Cmb_Unidad_Responsable_Destino.DataTextField = "Nombre";
            Cmb_Unidad_Responsable_Destino.DataBind();
            Cmb_Unidad_Responsable_Destino.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));

            //para la inicial
            Cmb_Unidad_Responsable.Items.Clear();
            Cmb_Unidad_Responsable.DataSource = Dt_Responsable;
            Cmb_Unidad_Responsable.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
            Cmb_Unidad_Responsable.DataTextField = "Nombre";
            Cmb_Unidad_Responsable.DataBind();
            Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));

            Cmb_Unidad_Responsable_Origen.Enabled = true;
            Cmb_Unidad_Responsable_Destino.Enabled = true;
            Cmb_Unidad_Responsable.Enabled = true;
            Cmb_Unidad_Responsable.SelectedIndex = -1;
            Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Cls_Sessiones.Dependencia_ID_Empleado.Trim()));
        
            Cmb_Unidad_Responsable_Origen.SelectedIndex = Cmb_Unidad_Responsable_Origen.Items.IndexOf(Cmb_Unidad_Responsable_Origen.Items.FindByValue(Cls_Sessiones.Dependencia_ID_Empleado.Trim()));
            Cmb_Unidad_Responsable_Destino.SelectedIndex = Cmb_Unidad_Responsable_Destino.Items.IndexOf(Cmb_Unidad_Responsable_Destino.Items.FindByValue(Cls_Sessiones.Dependencia_ID_Empleado.Trim()));

            if (Cmb_Unidad_Responsable_Origen.SelectedIndex > 0)
            {
                Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable_Origen.SelectedItem.Value.Trim();
                Dt_Area_Funcional = Negocio.Obtener_Area_Funcional();
                if (Dt_Area_Funcional != null)
                {
                    if (Dt_Area_Funcional.Rows.Count > 0)
                    {
                        Hf_Area_Funcional_Origen.Value = Dt_Area_Funcional.Rows[0]["CLAVE"].ToString().Trim();
                        Hf_Area_Funcional_Origen_ID.Value = Dt_Area_Funcional.Rows[0]["AREA_FUNCIONAL_ID"].ToString().Trim();
                    }
                }

                Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable_Destino.SelectedItem.Value.Trim();
                Dt_Area_Funcional = Negocio.Obtener_Area_Funcional();
                if (Dt_Area_Funcional != null)
                {
                    if (Dt_Area_Funcional.Rows.Count > 0)
                    {
                        Hf_Area_Funcional_Destino.Value = Dt_Area_Funcional.Rows[0]["CLAVE"].ToString().Trim();
                        Hf_Area_Funcional_Destino_ID.Value = Dt_Area_Funcional.Rows[0]["AREA_FUNCIONAL_ID"].ToString().Trim();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cargar_Combo_Financiamiento
    ///DESCRIPCIÓN: Cargara todos las fuentes de financiamiento dentro del combo 
    ///PARAMETROS: int Parametros.- Sirve para saber cual es el combo que va a cargar
    ///             si el de origen o destino
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  07/Noviembre/2011
    ///MODIFICO:    Leslie Gonzalez Vazquez
    ///FECHA_MODIFICO:  02/Marzo/2012
    ///CAUSA_MODIFICACIÓN: Para filtar las fuente de financiamiento de acuerdo a la dependencia
    ///*******************************************************************************
    protected void Cargar_Combo_Financiamiento(int Operacion)
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();
        DataTable Dt_Financinamiento = new DataTable();
        try
        {
            switch (Operacion)
            {
                case 1:
                    Cmb_Fuente_Financiamiento_Origen.Items.Clear();
                    if (Cmb_Unidad_Responsable_Origen.SelectedIndex > 0)
                    {
                        Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                        Negocio.P_Dependencia_ID_Busqueda = Cmb_Unidad_Responsable_Origen.SelectedItem.Value.Trim();
                        Dt_Financinamiento = Negocio.Consultar_Fuente_Financiamiento();

                        Cmb_Fuente_Financiamiento_Origen.DataSource = Dt_Financinamiento;
                        Cmb_Fuente_Financiamiento_Origen.DataValueField = "id";
                        Cmb_Fuente_Financiamiento_Origen.DataTextField = "nombre";
                        Cmb_Fuente_Financiamiento_Origen.DataBind();
                        Cmb_Fuente_Financiamiento_Origen.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                    }
                    if (Dt_Financinamiento.Rows.Count > 0)
                    {
                        Cmb_Fuente_Financiamiento_Origen.SelectedIndex = 1;
                        Cmb_Fuente_Financiamiento_Origen.Enabled = true;
                        Llenar_grid_Partidas();
                    }


                    break;

                case 2:
                    Cmb_Fuente_Financiamiento_Destino.Items.Clear();
                    if (Cmb_Unidad_Responsable_Destino.SelectedIndex > 0)
                    {
                        Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                        Negocio.P_Dependencia_ID_Busqueda = Cmb_Unidad_Responsable_Destino.SelectedItem.Value.Trim();

                        Dt_Financinamiento = Negocio.Consultar_Fuente_Financiamiento();

                        Cmb_Fuente_Financiamiento_Destino.DataSource = Dt_Financinamiento;
                        Cmb_Fuente_Financiamiento_Destino.DataValueField = "id";
                        Cmb_Fuente_Financiamiento_Destino.DataTextField = "nombre";
                        Cmb_Fuente_Financiamiento_Destino.DataBind();
                        Cmb_Fuente_Financiamiento_Destino.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                    }
                    if (Dt_Financinamiento.Rows.Count > 0)
                    {
                        Cmb_Fuente_Financiamiento_Destino.SelectedIndex = 1;
                        Cmb_Fuente_Financiamiento_Destino.Enabled = true;
                        Llenar_grid_Partidas_Destino();
                    }

                    break;
            }

        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Fuente_Financiamiento_Origen_SelectedIndexChanged
    ///DESCRIPCIÓN: habilita el siguiente combo y pasa la informacion de la clave
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  08/Noviembre/2011
    ///MODIFICO:    Leslie Gonzalez Vazquez
    ///FECHA_MODIFICO: 02/Marzo/2012
    ///CAUSA_MODIFICACIÓN: para hacer la funcionalidad correcta del combo
    ///*******************************************************************************
    protected void Cmb_Fuente_Financiamiento_Origen_SelectedIndexChanged(object sender, EventArgs e)
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";

        try
        {
            Hf_Programa_Origen.Value = "";
            Hf_Partida_Origen.Value = "";
            Hf_Capitulo_Origen.Value = "";
            Hf_Disponible.Value = "0.00";

            if (Cmb_Unidad_Responsable_Origen.SelectedIndex > 0)
            {
                if (Rbl_Tipos_Paritda.Text.Trim().Equals("0") || Cmb_Operacion.SelectedItem.Value.Trim().Equals("TRASPASO"))
                {

                    Llenar_grid_Partidas();
                    Tabla_Meses.Visible = false;
                    Hf_Disponible.Value = "0.00";
                    if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("TRASPASO"))
                    {
                        Sumar_Total_Destino();
                    }
                }
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Selecciona una Unidad Responsable";
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Fuente_Financiamiento_Destino_SelectedIndexChanged
    ///DESCRIPCIÓN: habilita el siguiente combo y pasa la informacion de la clave
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  08/Noviembre/2011
    ///MODIFICO:    Leslie Gonzalez Vazquez
    ///FECHA_MODIFICO: 02/Marzo/2012
    ///CAUSA_MODIFICACIÓN: para hacer la funcionalidad correcta del combo
    ///*******************************************************************************
    protected void Cmb_Fuente_Financiamiento_Destino_SelectedIndexChanged(object sender, EventArgs e)
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";

        try
        {
            if (Cmb_Unidad_Responsable_Destino.SelectedIndex > 0)
            {
                if (Rbl_Tipos_Paritda.Text.Trim().Equals("0"))
                {
                    Hf_Programa_Destino.Value = "";
                    Hf_Partida_Destino.Value = "";
                    Hf_Capitulo_Destino.Value = "";

                    Llenar_grid_Partidas_Destino();
                    Tr_Tabla_Meses_Destino.Visible = false;
                    Sumar_Total_Origen();
                }
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Selecciona una Unidad Responsable Destino";
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Unidad_Responsable_Origen_SelectedIndexChanged
    ///DESCRIPCIÓN: 
    ///PARAMETROS: 
    ///CREO:        Leslie Gonzalez Vazquez
    ///FECHA_CREO:  05/Marzo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Cmb_Unidad_Responsable_Origen_SelectedIndexChanged(object sender, EventArgs e)
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";

        Cls_Ope_Psp_Clasificacion_Gasto_Negocio Negocio = new Cls_Ope_Psp_Clasificacion_Gasto_Negocio(); //conexion con la capa de negocios
        DataTable Dt_Area_Funcional = new DataTable();
        try
        {
            Hf_Area_Funcional_Origen.Value = "";
            Hf_Area_Funcional_Origen_ID.Value = "";
            Hf_Programa_Origen.Value = "";
            Hf_Partida_Origen.Value = "";
            Hf_Capitulo_Origen.Value = "";
            Hf_Disponible.Value = "0.00";

            Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable_Origen.SelectedItem.Value.Trim();
            Dt_Area_Funcional = Negocio.Obtener_Area_Funcional();
            if (Dt_Area_Funcional != null)
            {
                if (Dt_Area_Funcional.Rows.Count > 0)
                {
                    Hf_Area_Funcional_Origen.Value = Dt_Area_Funcional.Rows[0]["CLAVE"].ToString().Trim();
                    Hf_Area_Funcional_Origen_ID.Value = Dt_Area_Funcional.Rows[0]["AREA_FUNCIONAL_ID"].ToString().Trim();
                }
            }

            Cargar_Combo_Financiamiento(1);

            if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("TRASPASO"))
            {
                Sumar_Total_Destino();
            }
            else
            {
                if (Rbl_Tipos_Paritda.Text.Trim().Equals("1"))
                {
                    Llenar_Combo_Programas_Origen();
                    Llenar_Combo_Fuente_Financiamiento_Origen();
                }
                else
                {
                    Cargar_Combo_Financiamiento(1);
                    Llenar_grid_Partidas();
                    Tabla_Meses.Visible = false;
                    Hf_Disponible.Value = "0.00";
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Unidad_Responsable_Origen_SelectedIndexChanged
    ///DESCRIPCIÓN: 
    ///PARAMETROS: 
    ///CREO:        Leslie Gonzalez Vazquez
    ///FECHA_CREO:  05/Marzo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Cmb_Unidad_Responsable_Destino_SelectedIndexChanged(object sender, EventArgs e)
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";

        Cls_Ope_Psp_Clasificacion_Gasto_Negocio Negocio = new Cls_Ope_Psp_Clasificacion_Gasto_Negocio(); //conexion con la capa de negocios
        DataTable Dt_Area_Funcional = new DataTable();
        

        try
        {
            if (Rbl_Tipos_Paritda.Text.Trim().Equals("1"))
            {
                Hf_Area_Funcional_Destino.Value = "";
                Hf_Area_Funcional_Destino_ID.Value = "";
                Hf_Programa_Destino.Value = "";
            }
            else {
                Hf_Area_Funcional_Destino.Value = "";
                Hf_Area_Funcional_Destino_ID.Value = "";
                Hf_Programa_Destino.Value = "";
                Hf_Partida_Destino.Value = "";
                Hf_Capitulo_Destino.Value = "";
            }

            Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable_Destino.SelectedItem.Value.Trim();
            Dt_Area_Funcional = Negocio.Obtener_Area_Funcional();
            if (Dt_Area_Funcional != null)
            {
                if (Dt_Area_Funcional.Rows.Count > 0)
                {
                    Hf_Area_Funcional_Destino.Value = Dt_Area_Funcional.Rows[0]["CLAVE"].ToString().Trim();
                    Hf_Area_Funcional_Destino_ID.Value = Dt_Area_Funcional.Rows[0]["AREA_FUNCIONAL_ID"].ToString().Trim();
                }
            }

            Sumar_Total_Origen();

            if (Rbl_Tipos_Paritda.Text.Trim().Equals("1"))
            {
                Llenar_Combo_Programas_Destino();
                Llenar_Combo_Fuente_Financiamiento_Destino();
            }
            else
            {
                Cargar_Combo_Financiamiento(2);
                Llenar_grid_Partidas_Destino();
                Tr_Tabla_Meses_Destino.Visible = false;
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Operacion_SelectedIndexChanged
    ///DESCRIPCIÓN: evento para mostrar u ocultar los datos de las partidas destino
    ///PARAMETROS: 
    ///CREO:        Leslie Gonzalez Vazquez
    ///FECHA_CREO:  08/Marzo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Cmb_Operacion_SelectedIndexChanged(object sender, EventArgs e)
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";

        try
        {
            if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("TRASPASO"))
            {
                Div_Partida_Destino.Visible = true;
                Tr_Tipo_Partida.Visible = true;
                Sumar_Total_Origen();
                Sumar_Total_Destino();
                Llenar_Combo_Capitulos_Destino();
                Llenar_Combo_Programas_Destino();
                Llenar_Combo_Fuente_Financiamiento_Destino();
                Grid_Partidas_Destino.DataSource = new DataTable();
                Grid_Partidas_Destino.DataBind();

                if (Rbl_Tipos_Paritda.Text.Trim().Equals("1"))
                {
                    Tr_Capitulo_Destino.Visible = true;
                    Tr_Programa_Destino.Visible = true;
                    Tr_Capitulo_Origen.Visible = false;
                    Tr_Programa_Origen.Visible = false;
                    Txt_Codigo1.Text = "";
                    Txt_Codigo2.Text = "";
                    Tr_Tabla_Meses_Destino.Visible = false;
                    Cargar_Combo_Financiamiento(1);
                    Llenar_grid_Partidas();
                }
            }
            else if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("REDUCIR"))
            {
                Div_Partida_Destino.Visible = false;
                Tr_Tipo_Partida.Visible = false;
                Tr_Capitulo_Destino.Visible = false;
                Tr_Programa_Destino.Visible = false;
                Tr_Capitulo_Origen.Visible = false;
                Tr_Programa_Origen.Visible = false;
                Rbl_Tipos_Paritda.SelectedIndex = 0;
                Sumar_Total_Origen();
            }
            else if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("AMPLIAR"))
            {
                Div_Partida_Destino.Visible = false;
                Tr_Tipo_Partida.Visible = true;
                Sumar_Total_Origen();
                Llenar_Combo_Capitulos_Origen();
                Llenar_Combo_Programas_Origen();
                Llenar_Combo_Fuente_Financiamiento_Origen();
                Grid_Partidas.DataSource = new DataTable();
                Grid_Partidas.DataBind();

                if (Rbl_Tipos_Paritda.Text.Trim().Equals("1"))
                {
                    Tr_Capitulo_Destino.Visible = false;
                    Tr_Programa_Destino.Visible = false;
                    Tr_Capitulo_Origen.Visible = true;
                    Tr_Programa_Origen.Visible = true;
                    Txt_Codigo1.Text = "";
                    Txt_Codigo2.Text = "";
                    Tabla_Meses.Visible = false;
                    Tr_Tabla_Meses_Destino.Visible = false;
                }
            }

            if (Rbl_Tipos_Paritda.Text.Trim().Equals("0"))
            {
                Tr_Capitulo_Destino.Visible = false;
                Tr_Programa_Destino.Visible = false;
                Tr_Capitulo_Origen.Visible = false;
                Tr_Programa_Origen.Visible = false;
                Cargar_Combo_Financiamiento(1);
                Cargar_Combo_Financiamiento(2);
                Llenar_grid_Partidas_Destino();
                Llenar_grid_Partidas();
                Tr_Tabla_Meses_Destino.Visible = false;
                Txt_Codigo1.Text = "";
                Txt_Codigo2.Text = "";
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    //*******************************************************************************
    //NOMBRE DE LA FUNCIÓN : Rbl_Tipos_Paritda_CheckedChanged
    //DESCRIPCIÓN          : Evento del radiobuttonlist
    //PARAMETROS           :   
    //CREO                 : Leslie González Vázquez
    //FECHA_CREO           : 21/Marzo/2012 
    //MODIFICO             :
    //FECHA_MODIFICO       :
    //CAUSA_MODIFICACIÓN   :
    //*******************************************************************************
    public void Rbl_Tipos_Paritda_CheckedChanged(object sender, System.EventArgs e)
    {
        try
        {
            if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("TRASPASO"))
            {
                Div_Partida_Destino.Visible = true;
                Tr_Tipo_Partida.Visible = true;
                Sumar_Total_Origen();
                Sumar_Total_Destino();
                Llenar_Combo_Capitulos_Destino();
                Llenar_Combo_Programas_Destino();
                Llenar_Combo_UR_Destino();
                Llenar_Combo_Fuente_Financiamiento_Destino();
                Grid_Partidas_Destino.DataSource = new DataTable();
                Grid_Partidas_Destino.DataBind();
                Tr_Tabla_Meses_Destino.Visible = false;
                if (Rbl_Tipos_Paritda.Text.Trim().Equals("1"))
                {
                    Tr_Capitulo_Destino.Visible = true;
                    Tr_Programa_Destino.Visible = true;
                    Tr_Capitulo_Origen.Visible = false;
                    Tr_Programa_Origen.Visible = false;
                    Txt_Codigo2.Text = "";
                    Hf_Tipo_Presupuesto.Value = "Nuevo";
                }
            }
            else if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("REDUCIR"))
            {
                Div_Partida_Destino.Visible = false;
                Tr_Tipo_Partida.Visible = false;
                Tr_Capitulo_Destino.Visible = false;
                Tr_Programa_Destino.Visible = false;
                Tr_Capitulo_Origen.Visible = false;
                Tr_Programa_Origen.Visible = false;
                Tabla_Meses.Visible = false;
                Tr_Tabla_Meses_Destino.Visible = false;
                Rbl_Tipos_Paritda.SelectedIndex = 0;
                Sumar_Total_Origen();
                Hf_Tipo_Presupuesto.Value = "Existente";
            }
            else if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("AMPLIAR"))
            {
                Div_Partida_Destino.Visible = false;
                Tr_Tipo_Partida.Visible = true;
                Sumar_Total_Origen();
                Llenar_Combo_UR_Origen();
                Llenar_Combo_Capitulos_Origen();
                Llenar_Combo_Programas_Origen();
                Llenar_Combo_Fuente_Financiamiento_Origen();
                Grid_Partidas.DataSource = new DataTable();
                Grid_Partidas.DataBind();
                Tabla_Meses.Visible = false;
                Tr_Tabla_Meses_Destino.Visible = false;
                if (Rbl_Tipos_Paritda.Text.Trim().Equals("1"))
                {
                    Tr_Capitulo_Destino.Visible = false;
                    Tr_Programa_Destino.Visible = false;
                    Tr_Capitulo_Origen.Visible = true;
                    Tr_Programa_Origen.Visible = true;
                    Txt_Codigo1.Text = "";
                    Hf_Tipo_Presupuesto.Value = "Nuevo";

                }
            }

            if (Rbl_Tipos_Paritda.Text.Trim().Equals("0"))
            {
                Tr_Capitulo_Destino.Visible = false;
                Tr_Programa_Destino.Visible = false;
                Tr_Capitulo_Origen.Visible = false;
                Tr_Programa_Origen.Visible = false;
                Cargar_Combo_Responsable();
                Cargar_Combo_Financiamiento(1);
                Cargar_Combo_Financiamiento(2);
                Llenar_grid_Partidas_Destino();
                Llenar_grid_Partidas();
                Tr_Tabla_Meses_Destino.Visible = false;
                Txt_Codigo1.Text = "";
                Txt_Codigo2.Text = "";
                Hf_Tipo_Presupuesto.Value = "Existente";
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "Error en el evento del tipo de partida" + Ex.Message;
        }

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Programas_Origen
    ///DESCRIPCIÓN          : Llena el combo de programas de origen
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Marzo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Combo_Programas_Origen()
    {
        Cls_Ope_Psp_Limite_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Limite_Presupuestal_Negocio();//Instancia con la clase de negocios

        Cmb_Programa_Origen.Items.Clear(); //limpiamos el combo
        if (Cmb_Unidad_Responsable_Origen.SelectedIndex > 0)
        {
            Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable_Origen.SelectedItem.Value.Trim();
            Cmb_Programa_Origen.DataSource = Negocio.Consultar_Programa_Unidades_Responsables();
            Cmb_Programa_Origen.DataTextField = "NOMBRE";
            Cmb_Programa_Origen.DataValueField = Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id;
            Cmb_Programa_Origen.DataBind();
            Cmb_Programa_Origen.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
            Cmb_Programa_Origen.SelectedIndex = 1;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Programas_Destino
    ///DESCRIPCIÓN          : Llena el combo de programas de destino
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Marzo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Combo_Programas_Destino()
    {
        Cls_Ope_Psp_Limite_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Limite_Presupuestal_Negocio();//Instancia con la clase de negocios

        Cmb_Programa_Origen.Items.Clear(); //limpiamos el combo
        if (Cmb_Unidad_Responsable_Destino.SelectedIndex > 0)
        {
            Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable_Destino.SelectedItem.Value.Trim();
            Cmb_Programa_Destino.DataSource = Negocio.Consultar_Programa_Unidades_Responsables();
            Cmb_Programa_Destino.DataTextField = "NOMBRE";
            Cmb_Programa_Destino.DataValueField = Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id;
            Cmb_Programa_Destino.DataBind();
            Cmb_Programa_Destino.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
            Cmb_Programa_Destino.SelectedIndex = 1;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Capitulos_Origen
    ///DESCRIPCIÓN          : Llena el combo de capitulos de origen
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Marzo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Combo_Capitulos_Origen()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();

        Cmb_Capitulo_Origen.Items.Clear(); //limpiamos el combo
        Cmb_Capitulo_Origen.DataSource = Negocio.Obtener_Capitulos();
        Cmb_Capitulo_Origen.DataTextField = "CLAVE_NOMBRE";
        Cmb_Capitulo_Origen.DataValueField = Cat_SAP_Capitulos.Campo_Capitulo_ID;
        Cmb_Capitulo_Origen.DataBind();
        Cmb_Capitulo_Origen.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Capitulos_Destino
    ///DESCRIPCIÓN          : Llena el combo de capitulos de Destino
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Marzo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Combo_Capitulos_Destino()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();

        Cmb_Capitulo_Destino.Items.Clear(); //limpiamos el combo
        Cmb_Capitulo_Destino.DataSource = Negocio.Obtener_Capitulos();
        Cmb_Capitulo_Destino.DataTextField = "CLAVE_NOMBRE";
        Cmb_Capitulo_Destino.DataValueField = Cat_SAP_Capitulos.Campo_Capitulo_ID;
        Cmb_Capitulo_Destino.DataBind();
        Cmb_Capitulo_Destino.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Capitulo_Origen_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento del combo de Capitulos origen
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Marzo/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///******************************************************************************* 
    protected void Cmb_Capitulo_Origen_SelectedIndexChanged(object sender, EventArgs e)
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";

        Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Calendarizar_Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
        DataTable Dt_Partidas = new DataTable();
        DataTable Dt_Partidas_Origen = new DataTable();
        DataRow Fila;
        try
        {
            Dt_Partidas_Origen.Columns.Add("nombre");
            Dt_Partidas_Origen.Columns.Add("Partida_id");
            Dt_Partidas_Origen.Columns.Add("CLAVE_PARTIDA");
            Dt_Partidas_Origen.Columns.Add("CAPITULO_ID");
            Dt_Partidas_Origen.Columns.Add("PROGRAMA");
            Dt_Partidas_Origen.Columns.Add("PROGRAMA_ID");
            Dt_Partidas_Origen.Columns.Add("CLAVE_PROGRAMA");
            Dt_Partidas_Origen.Columns.Add("APROBADO");
            Dt_Partidas_Origen.Columns.Add("DISPONIBLE");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_ENERO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_FEBRERO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_MARZO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_ABRIL");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_MAYO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_JUNIO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_JULIO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_AGOSTO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_SEPTIEMBRE");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_OCTUBRE");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_NOVIEMBRE");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_DICIEMBRE");

            Grid_Partidas.DataSource = new DataTable();

            if (Cmb_Capitulo_Origen.SelectedIndex > 0)
            {
                Calendarizar_Negocio.P_Capitulo_ID = Cmb_Capitulo_Origen.SelectedItem.Value.Trim();
                Dt_Partidas = Calendarizar_Negocio.Consultar_Partidas();
                if (Dt_Partidas != null)
                {
                    if (Dt_Partidas.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Partidas.Rows)
                        {
                            Fila = Dt_Partidas_Origen.NewRow();
                            Fila["nombre"] = Dr["NOMBRE"].ToString().Trim();
                            Fila["Partida_id"] = Dr["PARTIDA_ID"].ToString().Trim();
                            Fila["CLAVE_PARTIDA"] = Dr["NOMBRE"].ToString().Trim().Substring(0, Dr["NOMBRE"].ToString().Trim().IndexOf(" "));
                            Fila["CAPITULO_ID"] = Cmb_Capitulo_Origen.SelectedItem.Value.Trim();
                            Fila["PROGRAMA"] = "";
                            Fila["PROGRAMA_ID"] = "";
                            Fila["CLAVE_PROGRAMA"] = "";
                            Fila["APROBADO"] = "0.00";
                            Fila["DISPONIBLE"] = "0.00";
                            Fila["IMPORTE_ENERO"] = "0.00";
                            Fila["IMPORTE_FEBRERO"] = "0.00";
                            Fila["IMPORTE_MARZO"] = "0.00";
                            Fila["IMPORTE_ABRIL"] = "0.00";
                            Fila["IMPORTE_MAYO"] = "0.00";
                            Fila["IMPORTE_JUNIO"] = "0.00";
                            Fila["IMPORTE_JULIO"] = "0.00";
                            Fila["IMPORTE_AGOSTO"] = "0.00";
                            Fila["IMPORTE_SEPTIEMBRE"] = "0.00";
                            Fila["IMPORTE_OCTUBRE"] = "0.00";
                            Fila["IMPORTE_NOVIEMBRE"] = "0.00";
                            Fila["IMPORTE_DICIEMBRE"] = "0.00";
                            Dt_Partidas_Origen.Rows.Add(Fila);
                        }

                        Grid_Partidas.DataBind();
                        Grid_Partidas.Columns[0].Visible = true;
                        Grid_Partidas.Columns[5].Visible = true;
                        Grid_Partidas.Columns[6].Visible = true;
                        Grid_Partidas.Columns[7].Visible = true;
                        Grid_Partidas.Columns[8].Visible = true;
                        Grid_Partidas.Columns[9].Visible = true;
                        Grid_Partidas.Columns[10].Visible = true;
                        Grid_Partidas.Columns[11].Visible = true;
                        Grid_Partidas.Columns[12].Visible = true;
                        Grid_Partidas.Columns[13].Visible = true;
                        Grid_Partidas.Columns[14].Visible = true;
                        Grid_Partidas.Columns[15].Visible = true;
                        Grid_Partidas.Columns[16].Visible = true;
                        Grid_Partidas.Columns[17].Visible = true;
                        Grid_Partidas.Columns[18].Visible = true;
                        Grid_Partidas.Columns[19].Visible = true;
                        Grid_Partidas.Columns[20].Visible = true;
                        Grid_Partidas.Columns[21].Visible = true;
                        Grid_Partidas.DataSource = Dt_Partidas_Origen;
                        Grid_Partidas.DataBind();
                        Grid_Partidas.Columns[5].Visible = false;
                        Grid_Partidas.Columns[6].Visible = false;
                        Grid_Partidas.Columns[7].Visible = false;
                        Grid_Partidas.Columns[8].Visible = false;
                        Grid_Partidas.Columns[9].Visible = false;
                        Grid_Partidas.Columns[10].Visible = false;
                        Grid_Partidas.Columns[11].Visible = false;
                        Grid_Partidas.Columns[12].Visible = false;
                        Grid_Partidas.Columns[13].Visible = false;
                        Grid_Partidas.Columns[14].Visible = false;
                        Grid_Partidas.Columns[15].Visible = false;
                        Grid_Partidas.Columns[16].Visible = false;
                        Grid_Partidas.Columns[17].Visible = false;
                        Grid_Partidas.Columns[18].Visible = false;
                        Grid_Partidas.Columns[19].Visible = false;
                        Grid_Partidas.Columns[20].Visible = false;
                        Grid_Partidas.Columns[21].Visible = false;
                        Div_Partidas.Visible = true;
                        Grid_Partidas.SelectedIndex = -1;

                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error en el evento del combo de capitulos Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Capitulo_Destinon_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento del combo de Capitulos destino
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Marzo/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///******************************************************************************* 
    protected void Cmb_Capitulo_Destino_SelectedIndexChanged(object sender, EventArgs e)
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";

        Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Calendarizar_Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
        DataTable Dt_Partidas = new DataTable();
        DataTable Dt_Partidas_Origen = new DataTable();
        DataRow Fila;

        try
        {
            Dt_Partidas_Origen.Columns.Add("nombre");
            Dt_Partidas_Origen.Columns.Add("Partida_id");
            Dt_Partidas_Origen.Columns.Add("CLAVE_PARTIDA");
            Dt_Partidas_Origen.Columns.Add("CAPITULO_ID");
            Dt_Partidas_Origen.Columns.Add("PROGRAMA");
            Dt_Partidas_Origen.Columns.Add("PROGRAMA_ID");
            Dt_Partidas_Origen.Columns.Add("CLAVE_PROGRAMA");
            Dt_Partidas_Origen.Columns.Add("APROBADO");
            Dt_Partidas_Origen.Columns.Add("DISPONIBLE");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_ENERO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_FEBRERO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_MARZO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_ABRIL");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_MAYO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_JUNIO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_JULIO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_AGOSTO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_SEPTIEMBRE");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_OCTUBRE");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_NOVIEMBRE");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_DICIEMBRE");

            Grid_Partidas_Destino.DataSource = new DataTable();

            if (Cmb_Capitulo_Destino.SelectedIndex > 0)
            {

                Calendarizar_Negocio.P_Capitulo_ID = Cmb_Capitulo_Destino.SelectedItem.Value.Trim();
                Dt_Partidas = Calendarizar_Negocio.Consultar_Partidas();
                if (Dt_Partidas != null)
                {
                    if (Dt_Partidas.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Partidas.Rows)
                        {
                            Fila = Dt_Partidas_Origen.NewRow();
                            Fila["nombre"] = Dr["NOMBRE"].ToString().Trim();
                            Fila["Partida_id"] = Dr["PARTIDA_ID"].ToString().Trim();
                            Fila["CLAVE_PARTIDA"] = Dr["NOMBRE"].ToString().Trim().Substring(0, Dr["NOMBRE"].ToString().Trim().IndexOf(" "));
                            Fila["CAPITULO_ID"] = Cmb_Capitulo_Destino.SelectedItem.Value.Trim();
                            Fila["PROGRAMA"] = "";
                            Fila["PROGRAMA_ID"] = "";
                            Fila["CLAVE_PROGRAMA"] = "";
                            Fila["APROBADO"] = "0.00";
                            Fila["DISPONIBLE"] = "0.00";
                            Fila["IMPORTE_ENERO"] = "0.00";
                            Fila["IMPORTE_FEBRERO"] = "0.00";
                            Fila["IMPORTE_MARZO"] = "0.00";
                            Fila["IMPORTE_ABRIL"] = "0.00";
                            Fila["IMPORTE_MAYO"] = "0.00";
                            Fila["IMPORTE_JUNIO"] = "0.00";
                            Fila["IMPORTE_JULIO"] = "0.00";
                            Fila["IMPORTE_AGOSTO"] = "0.00";
                            Fila["IMPORTE_SEPTIEMBRE"] = "0.00";
                            Fila["IMPORTE_OCTUBRE"] = "0.00";
                            Fila["IMPORTE_NOVIEMBRE"] = "0.00";
                            Fila["IMPORTE_DICIEMBRE"] = "0.00";
                            Dt_Partidas_Origen.Rows.Add(Fila);
                        }

                        Grid_Partidas_Destino.DataBind();
                        Grid_Partidas_Destino.Columns[0].Visible = true;
                        Grid_Partidas_Destino.Columns[5].Visible = true;
                        Grid_Partidas_Destino.Columns[6].Visible = true;
                        Grid_Partidas_Destino.Columns[7].Visible = true;
                        Grid_Partidas_Destino.Columns[8].Visible = true;
                        Grid_Partidas_Destino.Columns[9].Visible = true;
                        Grid_Partidas_Destino.Columns[10].Visible = true;
                        Grid_Partidas_Destino.Columns[11].Visible = true;
                        Grid_Partidas_Destino.Columns[12].Visible = true;
                        Grid_Partidas_Destino.Columns[13].Visible = true;
                        Grid_Partidas_Destino.Columns[14].Visible = true;
                        Grid_Partidas_Destino.Columns[15].Visible = true;
                        Grid_Partidas_Destino.Columns[16].Visible = true;
                        Grid_Partidas_Destino.Columns[17].Visible = true;
                        Grid_Partidas_Destino.Columns[18].Visible = true;
                        Grid_Partidas_Destino.Columns[19].Visible = true;
                        Grid_Partidas_Destino.Columns[20].Visible = true;
                        Grid_Partidas_Destino.Columns[21].Visible = true;
                        Grid_Partidas_Destino.DataSource = Dt_Partidas_Origen;
                        Grid_Partidas_Destino.DataBind();
                        Grid_Partidas_Destino.Columns[5].Visible = false;
                        Grid_Partidas_Destino.Columns[6].Visible = false;
                        Grid_Partidas_Destino.Columns[7].Visible = false;
                        Grid_Partidas_Destino.Columns[8].Visible = false;
                        Grid_Partidas_Destino.Columns[9].Visible = false;
                        Grid_Partidas_Destino.Columns[10].Visible = false;
                        Grid_Partidas_Destino.Columns[11].Visible = false;
                        Grid_Partidas_Destino.Columns[12].Visible = false;
                        Grid_Partidas_Destino.Columns[13].Visible = false;
                        Grid_Partidas_Destino.Columns[14].Visible = false;
                        Grid_Partidas_Destino.Columns[15].Visible = false;
                        Grid_Partidas_Destino.Columns[16].Visible = false;
                        Grid_Partidas_Destino.Columns[17].Visible = false;
                        Grid_Partidas_Destino.Columns[18].Visible = false;
                        Grid_Partidas_Destino.Columns[19].Visible = false;
                        Grid_Partidas_Destino.Columns[20].Visible = false;
                        Grid_Partidas_Destino.Columns[21].Visible = false;
                        Div_Partidas.Visible = true;
                        Grid_Partidas_Destino.SelectedIndex = -1;

                    }
                }
            }
            Sumar_Total_Origen();
            Sumar_Total_Destino();
        }
        catch (Exception ex)
        {
            throw new Exception("Error en el evento del combo de capitulos Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Fuente_Financiamiento
    ///DESCRIPCIÓN          : Llena el combo de fuente de financiamiento
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 11/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Combo_Fuente_Financiamiento_Origen()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Parametros_Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();//Instancia con la clase de negocios
        Cmb_Fuente_Financiamiento_Origen.Items.Clear(); //limpiamos el combo
        Cmb_Fuente_Financiamiento_Origen.DataSource = Parametros_Negocio.Consultar_Fuente_Financiamiento_Ramo33();
        Cmb_Fuente_Financiamiento_Origen.DataTextField = "NOMBRE";
        Cmb_Fuente_Financiamiento_Origen.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
        Cmb_Fuente_Financiamiento_Origen.DataBind();
        Cmb_Fuente_Financiamiento_Origen.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Fuente_Financiamiento
    ///DESCRIPCIÓN          : Llena el combo de fuente de financiamiento
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 11/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Combo_Fuente_Financiamiento_Destino()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Parametros_Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();//Instancia con la clase de negocios
        Cmb_Fuente_Financiamiento_Destino.Items.Clear(); //limpiamos el combo
        Cmb_Fuente_Financiamiento_Destino.DataSource = Parametros_Negocio.Consultar_Fuente_Financiamiento_Ramo33();
        Cmb_Fuente_Financiamiento_Destino.DataTextField = "NOMBRE";
        Cmb_Fuente_Financiamiento_Destino.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
        Cmb_Fuente_Financiamiento_Destino.DataBind();
        Cmb_Fuente_Financiamiento_Destino.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Fuente_Financiamiento
    ///DESCRIPCIÓN          : Llena el combo de fuente de financiamiento
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 11/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Llenar_Combo_UR_Destino()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocios = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos(); //conexion con la capa de negocios
        Cls_Ope_Psp_Clasificacion_Gasto_Negocio Negocio = new Cls_Ope_Psp_Clasificacion_Gasto_Negocio();
        DataTable Dt_Responsable = new DataTable();
        DataTable Dt_Area_Funcional = new DataTable();

        Hf_Area_Funcional_Destino.Value = "";
        Hf_Area_Funcional_Destino_ID.Value = "";
        try
        {

            Negocios.P_Tipo_Usuario = "Todos";
            Dt_Responsable = Negocios.Consultar_URs();

            //para destino
            Cmb_Unidad_Responsable_Destino.Items.Clear();
            Cmb_Unidad_Responsable_Destino.DataSource = Dt_Responsable;
            Cmb_Unidad_Responsable_Destino.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
            Cmb_Unidad_Responsable_Destino.DataTextField = "Nombre";
            Cmb_Unidad_Responsable_Destino.DataBind();
            Cmb_Unidad_Responsable_Destino.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Fuente_Financiamiento
    ///DESCRIPCIÓN          : Llena el combo de fuente de financiamiento
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 11/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Llenar_Combo_UR_Origen()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocios = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos(); //conexion con la capa de negocios
        Cls_Ope_Psp_Clasificacion_Gasto_Negocio Negocio = new Cls_Ope_Psp_Clasificacion_Gasto_Negocio();
        DataTable Dt_Responsable = new DataTable();
        DataTable Dt_Area_Funcional = new DataTable();

        Hf_Area_Funcional_Origen.Value = "";
        Hf_Area_Funcional_Origen_ID.Value = "";
        try
        {

            Negocios.P_Tipo_Usuario = "Todos";
            Dt_Responsable = Negocios.Consultar_URs();

            //para destino
            Cmb_Unidad_Responsable_Origen.Items.Clear();
            Cmb_Unidad_Responsable_Origen.DataSource = Dt_Responsable;
            Cmb_Unidad_Responsable_Origen.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
            Cmb_Unidad_Responsable_Origen.DataTextField = "Nombre";
            Cmb_Unidad_Responsable_Origen.DataBind();
            Cmb_Unidad_Responsable_Origen.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion

    #region (Grid)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Movimiento_SelectedIndexChanged
    /// DESCRIPCION : Consulta los datos de los movimientos seleccionada por el usuario
    /// CREO        : Hugo Enrique Ramirez Aguilera
    /// FECHA_CREO  : 21-Octubre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Grid_Movimiento_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Rs_Consulta = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();//Variable de conexión hacia la capa de Negocios
        DataTable Dt_Partidas = new DataTable();
        DataTable Dt_Partidas_Detalles = new DataTable();
        DataTable Dt_Movimiento;//Variable que obtendra los datos de la consulta 
        int Operacion;
        String Tipo_Operacion;
        String Estatus = "";
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Limpiar_Controles(); //Limpia los controles del la forma para poder agregar los valores del registro seleccionado

            Rs_Consulta.P_No_Solicitud = Grid_Movimiento.SelectedRow.Cells[1].Text;
            Dt_Movimiento = Rs_Consulta.Consulta_Movimiento();//Consulta todos los datos de los movimientos que fue seleccionada por el usuario

            Div_Grid_Movimientos.Visible = false;
            Div_Datos.Visible = true;

            if (Dt_Movimiento.Rows.Count > 0)
            {
                foreach (DataRow Registro in Dt_Movimiento.Rows)
                {
                    if (!String.IsNullOrEmpty(Registro[Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud].ToString()))
                    {
                        Txt_No_Solicitud.Text = (Registro[Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud].ToString());

                        Cargar_Grid_Comentario(Txt_No_Solicitud.Text);
                        Div_Grid_Comentarios.Visible = true;
                    }
                    if (!String.IsNullOrEmpty(Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Codigo1].ToString()))
                    {
                        Div_Partida_Destino.Visible = false;
                        Txt_Codigo1.Text = (Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Codigo1].ToString());
                        Operacion = 1;

                        Cargar_Combo_Responsable();
                        Cmb_Unidad_Responsable_Origen.SelectedIndex = Cmb_Unidad_Responsable_Origen.Items.IndexOf(Cmb_Unidad_Responsable_Origen.Items.FindByValue(Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Dependencia_Id].ToString()));

                        Hf_Area_Funcional_Origen_ID.Value = Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Area_Funcional_Id].ToString();
                        Llenar_Combo_Fuente_Financiamiento_Destino(); Llenar_Combo_Fuente_Financiamiento_Origen();
                        //Cargar_Combo_Financiamiento(Operacion);
                        Cmb_Fuente_Financiamiento_Origen.SelectedIndex = Cmb_Fuente_Financiamiento_Origen.Items.IndexOf(Cmb_Fuente_Financiamiento_Origen.Items.FindByValue(Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Fuente_Financiamiento_Id].ToString()));

                        Hf_Partida_Origen.Value = Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Partida_Id].ToString().Trim();
                        Hf_Programa_Origen.Value = Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Programa_Id].ToString().Trim();
                        Llenar_grid_Partidas();
                        Grid_Partidas.Columns[0].Visible = false;

                        Dt_Partidas = (DataTable)Grid_Partidas.DataSource;
                        if (Dt_Partidas != null)
                        {
                            if (Dt_Partidas.Rows.Count > 0)
                            {
                                Lbl_Disp_Ene.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas.Rows[0]["IMPORTE_ENERO"]);
                                Lbl_Disp_Feb.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas.Rows[0]["IMPORTE_FEBRERO"]);
                                Lbl_Disp_Mar.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas.Rows[0]["IMPORTE_MARZO"]);
                                Lbl_Disp_Abr.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas.Rows[0]["IMPORTE_ABRIL"]);
                                Lbl_Disp_May.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas.Rows[0]["IMPORTE_MAYO"]);
                                Lbl_Disp_Jun.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas.Rows[0]["IMPORTE_JUNIO"]);
                                Lbl_Disp_Jul.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas.Rows[0]["IMPORTE_JULIO"]);
                                Lbl_Disp_Ago.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas.Rows[0]["IMPORTE_AGOSTO"]);
                                Lbl_Disp_Sep.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas.Rows[0]["IMPORTE_SEPTIEMBRE"]);
                                Lbl_Disp_Oct.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas.Rows[0]["IMPORTE_OCTUBRE"]);
                                Lbl_Disp_Nov.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas.Rows[0]["IMPORTE_NOVIEMBRE"]);
                                Lbl_Disp_Dic.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas.Rows[0]["IMPORTE_DICIEMBRE"]);
                            }
                        }

                        Rs_Consulta.P_No_Solicitud = Txt_No_Solicitud.Text.Trim();
                        Dt_Partidas_Detalles = Rs_Consulta.Consulta_Detalles_Importes();

                        if (Dt_Partidas_Detalles != null)
                        {
                            if (Dt_Partidas_Detalles.Rows.Count > 0)
                            {
                                foreach (DataRow Dr in Dt_Partidas_Detalles.Rows)
                                {
                                    if (Dr["TIPO"].ToString().Trim().Equals("Partida_Origen") || Dr["TIPO"].ToString().Trim().Equals("Partida_Origen_Nueva"))
                                    {
                                        Txt_Enero.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_ENERO"]);
                                        Txt_Febrero.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_FEBRERO"]);
                                        Txt_Marzo.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_MARZO"]);
                                        Txt_Abril.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_ABRIL"]);
                                        Txt_Mayo.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_MAYO"]);
                                        Txt_Junio.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_JUNIO"]);
                                        Txt_Julio.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_JULIO"]);
                                        Txt_Agosto.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_AGOSTO"]);
                                        Txt_Septiembre.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_SEPTIEMBRE"]);
                                        Txt_Octubre.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_OCTUBRE"]);
                                        Txt_Noviembre.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_NOVIEMBRE"]);
                                        Txt_Diciembre.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_DICIEMBRE"]);

                                        Sumar_Total_Origen();
                                    }
                                    else if (Dr["TIPO"].ToString().Trim().Equals("Partida_Destino") || Dr["TIPO"].ToString().Trim().Equals("Partida_Destino_Nueva"))
                                    {
                                        Txt_Enero_Destino.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_ENERO"]);
                                        Txt_Febrero_Destino.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_FEBRERO"]);
                                        Txt_Marzo_Destino.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_MARZO"]);
                                        Txt_Abril_Destino.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_ABRIL"]);
                                        Txt_Mayo_Destino.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_MAYO"]);
                                        Txt_Junio_Destino.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_JUNIO"]);
                                        Txt_Julio_Destino.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_JULIO"]);
                                        Txt_Agosto_Destino.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_AGOSTO"]);
                                        Txt_Septiembre_Destino.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_SEPTIEMBRE"]);
                                        Txt_Octubre_Destino.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_OCTUBRE"]);
                                        Txt_Noviembre_Destino.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_NOVIEMBRE"]);
                                        Txt_Diciembre_Destino.Text = String.Format("{0:#,###,##0.00}", Dr["IMPORTE_DICIEMBRE"]);

                                        Sumar_Total_Destino();
                                    }
                                }
                            }
                        }
                    }

                    if (!String.IsNullOrEmpty(Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Importe].ToString()))
                    {
                        Txt_Importe.Text = String.Format("{0:#,###,##0.00}", Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Importe]);
                    }

                    if (!String.IsNullOrEmpty(Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Estatus].ToString()))
                    {
                        Estatus = (Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Estatus].ToString());

                        if (Estatus == "AUTORIZADA")
                        {
                            Cmb_Estatus.Items.Insert(3, "AUTORIZADA");
                            Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(Estatus));
                            Cmb_Estatus.Enabled = false;
                        }
                        else if (Estatus == "CANCELADA")
                        {
                            Cmb_Estatus.Items.RemoveAt(2);
                            Cmb_Estatus.Items.Insert(2, "CANCELADA");
                            Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(Estatus));
                            Cmb_Estatus.Enabled = false;
                        }
                        else
                        {
                            Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(Estatus));
                        }
                    }

                    if (!String.IsNullOrEmpty(Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Justificacion].ToString()))
                    {
                        Txt_Justificacion.Text = (Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Justificacion].ToString());
                    }
                    if (!String.IsNullOrEmpty(Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Tipo_Operacion].ToString()))
                    {
                        Tipo_Operacion = (Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Tipo_Operacion].ToString());
                        Cmb_Operacion.SelectedIndex = Cmb_Operacion.Items.IndexOf(Cmb_Operacion.Items.FindByValue(Tipo_Operacion));
                    }

                    if (!String.IsNullOrEmpty(Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Codigo2].ToString()))
                    {
                        Div_Partida_Destino.Visible = true;
                        Txt_Codigo2.Text = (Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Codigo2].ToString());
                        Operacion = 2;

                        Cmb_Unidad_Responsable_Destino.SelectedIndex = Cmb_Unidad_Responsable_Destino.Items.IndexOf(Cmb_Unidad_Responsable_Destino.Items.FindByValue(Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Dependencia_Id].ToString()));

                        Hf_Area_Funcional_Destino_ID.Value = Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Area_Funcional_Id].ToString();

                        Cargar_Combo_Financiamiento(Operacion);
                        Cmb_Fuente_Financiamiento_Destino.SelectedIndex = Cmb_Fuente_Financiamiento_Destino.Items.IndexOf(Cmb_Fuente_Financiamiento_Destino.Items.FindByValue(Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Fuente_Financiamiento_Id].ToString()));
                        Hf_Partida_Destino.Value = Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Partida_Id].ToString().Trim();
                        Hf_Programa_Destino.Value = Registro[Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Programa_Id].ToString().Trim();

                        Dt_Partidas = new DataTable();
                        Llenar_grid_Partidas_Destino();
                        Grid_Partidas_Destino.Columns[0].Visible = false;
                        Dt_Partidas = (DataTable)Grid_Partidas_Destino.DataSource;

                        if (Dt_Partidas != null)
                        {
                            if (Dt_Partidas.Rows.Count > 0)
                            {
                                Tr_Tabla_Meses_Destino.Visible = true;
                                Lbl_Disp_Ene_Destino.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas.Rows[0]["IMPORTE_ENERO"]);
                                Lbl_Disp_Feb_Destino.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas.Rows[0]["IMPORTE_FEBRERO"]);
                                Lbl_Disp_Mar_Destino.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas.Rows[0]["IMPORTE_MARZO"]);
                                Lbl_Disp_Abr_Destino.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas.Rows[0]["IMPORTE_ABRIL"]);
                                Lbl_Disp_May_Destino.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas.Rows[0]["IMPORTE_MAYO"]);
                                Lbl_Disp_Jun_Destino.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas.Rows[0]["IMPORTE_JUNIO"]);
                                Lbl_Disp_Jul_Destino.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas.Rows[0]["IMPORTE_JULIO"]);
                                Lbl_Disp_Ago_Destino.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas.Rows[0]["IMPORTE_AGOSTO"]);
                                Lbl_Disp_Sep_Destino.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas.Rows[0]["IMPORTE_SEPTIEMBRE"]);
                                Lbl_Disp_Oct_Destino.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas.Rows[0]["IMPORTE_OCTUBRE"]);
                                Lbl_Disp_Nov_Destino.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas.Rows[0]["IMPORTE_NOVIEMBRE"]);
                                Lbl_Disp_Dic_Destino.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas.Rows[0]["IMPORTE_DICIEMBRE"]);
                                Tr_Tabla_Meses_Destino.Visible = true;
                            }
                        }

                    }
                }

                Cmb_Operacion.Enabled = false;
                Txt_Importe.Enabled = false;
                Cmb_Estatus.Enabled = false;
                Txt_Justificacion.Enabled = false;
                Div_Grid_Comentarios.Visible = true;


                //partida Origen
                Txt_Codigo1.Enabled = false;
                Cmb_Fuente_Financiamiento_Origen.Enabled = false;
                Cmb_Unidad_Responsable_Origen.Enabled = false;
                Txt_Enero.Enabled = false;
                Txt_Febrero.Enabled = false;
                Txt_Marzo.Enabled = false;
                Txt_Abril.Enabled = false;
                Txt_Mayo.Enabled = false;
                Txt_Junio.Enabled = false;
                Txt_Julio.Enabled = false;
                Txt_Agosto.Enabled = false;
                Txt_Septiembre.Enabled = false;
                Txt_Octubre.Enabled = false;
                Txt_Noviembre.Enabled = false;
                Txt_Diciembre.Enabled = false;

                Tr_Tipo_Partida.Visible = false;
                Tr_Capitulo_Destino.Visible = false;
                Tr_Programa_Destino.Visible = false;
                Tr_Capitulo_Origen.Visible = false;
                Tr_Programa_Origen.Visible = false;

                //partida Destino
                Txt_Codigo2.Enabled = false;
                Cmb_Fuente_Financiamiento_Destino.Enabled = false;
                Cmb_Unidad_Responsable_Destino.Enabled = false;
                Txt_Enero_Destino.Enabled = false;
                Txt_Febrero_Destino.Enabled = false;
                Txt_Marzo_Destino.Enabled = false;
                Txt_Abril_Destino.Enabled = false;
                Txt_Mayo_Destino.Enabled = false;
                Txt_Junio_Destino.Enabled = false;
                Txt_Julio_Destino.Enabled = false;
                Txt_Agosto_Destino.Enabled = false;
                Txt_Septiembre_Destino.Enabled = false;
                Txt_Octubre_Destino.Enabled = false;
                Txt_Noviembre_Destino.Enabled = false;
                Txt_Diciembre_Destino.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    /// ********************************************************************************
    /// NOMBRE: Grid_Movimiento_Sorting
    /// 
    /// DESCRIPCIÓN: Ordena las columnas en orden ascendente o descendente.
    /// 
    /// CREÓ:   Hugo Enrique Ramirez Aguilera
    /// FECHA CREÓ: 21-Octubre-2011
    /// MODIFICÓ:
    /// FECHA MODIFICÓ:
    /// CAUSA MODIFICACIÓN:
    /// **********************************************************************************
    protected void Grid_Movimiento_Sorting(object sender, GridViewSortEventArgs e)
    {
        /*//Se consultan los movimientos que actualmente se encuentran registradas en el sistema.
        Consultar_Grid_Movimientos();
        DataTable Dt_Movimiento_Presupuestal = (Grid_Movimiento_Presupuestal.DataSource as DataTable);

        if (Dt_Movimiento_Presupuestal != null)
        {
            DataView Dv_Movimiento_Presupuestal = new DataView(Dt_Movimiento_Presupuestal);
            String Orden = ViewState["SortDirection"].ToString();

            if (Orden.Equals("ASC"))
            {
                Dv_Movimiento_Presupuestal.Sort = e.SortExpression + " " + "DESC";
                ViewState["SortDirection"] = "DESC";
            }
            else
            {
                Dv_Movimiento_Presupuestal.Sort = e.SortExpression + " " + "ASC";
                ViewState["SortDirection"] = "ASC";
            }

            Grid_Movimiento_Presupuestal.DataSource = Dv_Movimiento_Presupuestal;
            Grid_Movimiento_Presupuestal.DataBind();
        }*/
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION : Grid_Partidas_SelectedIndexChanged
    /// DESCRIPCION          : 
    /// CREO                 : Leslie Gonzalez
    /// FECHA_CREO           : 05/marzo/2012
    /// MODIFICO             :
    /// FECHA_MODIFICO       : 
    /// CAUSA_MODIFICACION   :
    ///*******************************************************************************
    protected void Grid_Partidas_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();//conexion con la capa de negocios
        String Codigo_Presupuesto = String.Empty;
        String Mes = String.Empty;
        DataTable Dt_Partidas = new DataTable();
        Boolean Repetido = false;
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";

        Hf_Programa_Origen.Value = String.Empty;
        Hf_Capitulo_Origen.Value = String.Empty;
        Hf_Partida_Origen.Value = String.Empty;
        Hf_Disponible.Value = "0.00";
        Txt_Enero.Text = "0.00";
        Txt_Febrero.Text = "0.00";
        Txt_Marzo.Text = "0.00";
        Txt_Abril.Text = "0.00";
        Txt_Mayo.Text = "0.00";
        Txt_Junio.Text = "0.00";
        Txt_Julio.Text = "0.00";
        Txt_Agosto.Text = "0.00";
        Txt_Septiembre.Text = "0.00";
        Txt_Octubre.Text = "0.00";
        Txt_Noviembre.Text = "0.00";
        Txt_Diciembre.Text = "0.00";
        Txt_Total.Text = "0.00";


        try
        {
            if (Grid_Partidas.SelectedIndex > -1)
            {
                if (Rbl_Tipos_Paritda.Text.Trim().Equals("1"))
                {
                    if (Cmb_Operacion.SelectedItem.Text.Trim().Equals("AMPLIAR"))
                    {
                        if (Cmb_Programa_Origen.SelectedIndex <= 0)
                        {
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                            Lbl_Mensaje_Error.Text = "Favor de seleccionar un programa";
                            Grid_Partidas.SelectedIndex = -1;
                            return;
                        }
                        if (Cmb_Fuente_Financiamiento_Origen.SelectedIndex <= 0)
                        {
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                            Lbl_Mensaje_Error.Text = "Favor de seleccionar una fuente de financiamiento";
                            Grid_Partidas.SelectedIndex = -1;
                            return;
                        }
                        if (Cmb_Unidad_Responsable_Origen.SelectedIndex <= 0)
                        {
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                            Lbl_Mensaje_Error.Text = "Favor de seleccionar una unidad responsable";
                            Grid_Partidas.SelectedIndex = -1;
                            return;
                        }
                        Negocio.P_Area_Funcional_ID = Hf_Area_Funcional_Origen_ID.Value.Trim();
                        Negocio.P_Dependencia_ID_Busqueda = Cmb_Unidad_Responsable_Origen.SelectedItem.Value.Trim();
                        Negocio.P_Fuente_Financiamiento_ID = Cmb_Fuente_Financiamiento_Origen.SelectedItem.Value.Trim();
                        Negocio.P_Programa_ID = Cmb_Programa_Origen.SelectedItem.Value.Trim();
                        Negocio.P_Partida_Especifica_ID = Grid_Partidas.SelectedRow.Cells[17].Text.Trim();
                        Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);

                        Dt_Partidas = Negocio.Consultar_Partidas_Especificas();
                        if (Dt_Partidas != null)
                        {
                            if (Dt_Partidas.Rows.Count > 0)
                            {
                                Repetido = true;
                            }
                        }
                    }
                }

                if (!Repetido)
                {
                    Tabla_Meses.Visible = true;

                    Llenar_Combo_Mes();

                    Hf_Partida_Origen.Value = Grid_Partidas.SelectedRow.Cells[17].Text.Trim();
                    Hf_Capitulo_Origen.Value = Grid_Partidas.SelectedRow.Cells[18].Text.Trim();
                    Hf_Programa_Origen.Value = HttpUtility.HtmlDecode(Grid_Partidas.SelectedRow.Cells[19].Text.Trim());

                    ////cargar el codigo programatico
                    Codigo_Presupuesto = Cmb_Fuente_Financiamiento_Origen.SelectedItem.Text.Trim().Substring(0, Cmb_Fuente_Financiamiento_Origen.SelectedItem.Text.IndexOf(" "));
                    Codigo_Presupuesto += "-" + Hf_Area_Funcional_Origen.Value.Trim();
                    if (Rbl_Tipos_Paritda.Text.Trim().Equals("1"))
                    {
                        if (Cmb_Operacion.SelectedItem.Text.Trim().Equals("AMPLIAR"))
                        {
                            Codigo_Presupuesto += "-" + Cmb_Programa_Origen.SelectedItem.Text.Trim().Substring(0, Cmb_Programa_Origen.SelectedItem.Text.Trim().IndexOf(" "));
                        }
                        else
                        {
                            Codigo_Presupuesto += "-" + Grid_Partidas.SelectedRow.Cells[21].Text.Trim();
                        }
                    }
                    else
                    {
                        Codigo_Presupuesto += "-" + Grid_Partidas.SelectedRow.Cells[21].Text.Trim();
                    }
                    Codigo_Presupuesto += "-" + Cmb_Unidad_Responsable_Origen.SelectedItem.Text.Trim().Substring(0, Cmb_Unidad_Responsable_Origen.SelectedItem.Text.IndexOf(" "));
                    Codigo_Presupuesto += "-" + Grid_Partidas.SelectedRow.Cells[20].Text.Trim();
                    Txt_Codigo1.Text = Codigo_Presupuesto;
                    Lbl_Disp_Ene.Text = Grid_Partidas.SelectedRow.Cells[5].Text.Trim();
                    Lbl_Disp_Feb.Text = Grid_Partidas.SelectedRow.Cells[6].Text.Trim();
                    Lbl_Disp_Mar.Text = Grid_Partidas.SelectedRow.Cells[7].Text.Trim();
                    Lbl_Disp_Abr.Text = Grid_Partidas.SelectedRow.Cells[8].Text.Trim();
                    Lbl_Disp_May.Text = Grid_Partidas.SelectedRow.Cells[9].Text.Trim();
                    Lbl_Disp_Jun.Text = Grid_Partidas.SelectedRow.Cells[10].Text.Trim();
                    Lbl_Disp_Jul.Text = Grid_Partidas.SelectedRow.Cells[11].Text.Trim();
                    Lbl_Disp_Ago.Text = Grid_Partidas.SelectedRow.Cells[12].Text.Trim();
                    Lbl_Disp_Sep.Text = Grid_Partidas.SelectedRow.Cells[13].Text.Trim();
                    Lbl_Disp_Oct.Text = Grid_Partidas.SelectedRow.Cells[14].Text.Trim();
                    Lbl_Disp_Nov.Text = Grid_Partidas.SelectedRow.Cells[15].Text.Trim();
                    Lbl_Disp_Dic.Text = Grid_Partidas.SelectedRow.Cells[16].Text.Trim();

                    Hf_Disponible.Value = Grid_Partidas.SelectedRow.Cells[4].Text.Trim();

                    Sumar_Total_Destino();
                    Sumar_Total_Origen();
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Favor de seleccionar otra partida pues esta si existe en el presupuesto";
                    Grid_Partidas.SelectedIndex = -1;
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION : Grid_Partidas_Destino_SelectedIndexChanged
    /// DESCRIPCION          : 
    /// CREO                 : Leslie Gonzalez
    /// FECHA_CREO           : 08/marzo/2012
    /// MODIFICO             :
    /// FECHA_MODIFICO       : 
    /// CAUSA_MODIFICACION   :
    ///*******************************************************************************
    protected void Grid_Partidas_Destino_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();//conexion con la capa de negocios
        String Codigo_Presupuesto = String.Empty;
        String Mes = String.Empty;
        DataTable Dt_Partidas = new DataTable();
        Boolean Repetido = false;
        Hf_Programa_Destino.Value = String.Empty;
        Hf_Capitulo_Destino.Value = String.Empty;
        Hf_Partida_Destino.Value = String.Empty;
        //Hf_Disponible.Value = "0.00";
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";

        Txt_Enero_Destino.Text = "0.00";
        Txt_Febrero_Destino.Text = "0.00";
        Txt_Marzo_Destino.Text = "0.00";
        Txt_Abril_Destino.Text = "0.00";
        Txt_Mayo_Destino.Text = "0.00";
        Txt_Junio_Destino.Text = "0.00";
        Txt_Julio_Destino.Text = "0.00";
        Txt_Agosto_Destino.Text = "0.00";
        Txt_Septiembre_Destino.Text = "0.00";
        Txt_Octubre_Destino.Text = "0.00";
        Txt_Noviembre_Destino.Text = "0.00";
        Txt_Diciembre_Destino.Text = "0.00";
        Txt_Total_Des.Text = "0.00";

        try
        {
            if (Grid_Partidas_Destino.SelectedIndex > -1)
            {
                if (Rbl_Tipos_Paritda.Text.Trim().Equals("1"))
                {
                    if (Cmb_Programa_Destino.SelectedIndex <= 0)
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "Favor de seleccionar un programa";
                        Grid_Partidas_Destino.SelectedIndex = -1;
                        return;
                    }
                    if (Cmb_Fuente_Financiamiento_Destino.SelectedIndex <= 0)
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "Favor de seleccionar una fuente de financiamiento";
                        Grid_Partidas_Destino.SelectedIndex = -1;
                        return;
                    }
                    if (Cmb_Unidad_Responsable_Destino.SelectedIndex <= 0)
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "Favor de seleccionar una unidad responsable";
                        Grid_Partidas_Destino.SelectedIndex = -1;
                        return;
                    }

                    Negocio.P_Area_Funcional_ID = Hf_Area_Funcional_Destino_ID.Value.Trim();
                    Negocio.P_Dependencia_ID_Busqueda = Cmb_Unidad_Responsable_Destino.SelectedItem.Value.Trim();
                    Negocio.P_Fuente_Financiamiento_ID = Cmb_Fuente_Financiamiento_Destino.SelectedItem.Value.Trim();
                    Negocio.P_Programa_ID = Cmb_Programa_Destino.SelectedItem.Value.Trim();
                    Negocio.P_Partida_Especifica_ID = Grid_Partidas_Destino.SelectedRow.Cells[17].Text.Trim();
                    Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);

                    Dt_Partidas = Negocio.Consultar_Partidas_Especificas();
                    if (Dt_Partidas != null)
                    {
                        if (Dt_Partidas.Rows.Count > 0)
                        {
                            Repetido = true;
                        }
                    }
                }

                if (!Repetido)
                {
                    Tr_Tabla_Meses_Destino.Visible = true;

                    //partida Destino
                    Txt_Codigo2.Enabled = false;
                    Llenar_Combo_Mes();

                    Hf_Partida_Destino.Value = Grid_Partidas_Destino.SelectedRow.Cells[17].Text.Trim();
                    Hf_Capitulo_Destino.Value = Grid_Partidas_Destino.SelectedRow.Cells[18].Text.Trim();
                    Hf_Programa_Destino.Value = HttpUtility.HtmlDecode(Grid_Partidas_Destino.SelectedRow.Cells[19].Text.Trim());

                    ////cargar el codigo programatico
                    Codigo_Presupuesto = Cmb_Fuente_Financiamiento_Destino.SelectedItem.Text.Trim().Substring(0, Cmb_Fuente_Financiamiento_Destino.SelectedItem.Text.IndexOf(" "));
                    Codigo_Presupuesto += "-" + Hf_Area_Funcional_Destino.Value.Trim();
                    if (Rbl_Tipos_Paritda.Text.Trim().Equals("1"))
                    {
                        Codigo_Presupuesto += "-" + Cmb_Programa_Destino.SelectedItem.Text.Trim().Substring(0, Cmb_Programa_Destino.SelectedItem.Text.Trim().IndexOf(" "));
                    }
                    else
                    {
                        Codigo_Presupuesto += "-" + Grid_Partidas_Destino.SelectedRow.Cells[21].Text.Trim();
                    }
                    Codigo_Presupuesto += "-" + Cmb_Unidad_Responsable_Destino.SelectedItem.Text.Trim().Substring(0, Cmb_Unidad_Responsable_Destino.SelectedItem.Text.IndexOf(" "));
                    Codigo_Presupuesto += "-" + Grid_Partidas_Destino.SelectedRow.Cells[20].Text.Trim();
                    Txt_Codigo2.Text = Codigo_Presupuesto;
                    Lbl_Disp_Ene_Destino.Text = Grid_Partidas_Destino.SelectedRow.Cells[5].Text.Trim();
                    Lbl_Disp_Feb_Destino.Text = Grid_Partidas_Destino.SelectedRow.Cells[6].Text.Trim();
                    Lbl_Disp_Mar_Destino.Text = Grid_Partidas_Destino.SelectedRow.Cells[7].Text.Trim();
                    Lbl_Disp_Abr_Destino.Text = Grid_Partidas_Destino.SelectedRow.Cells[8].Text.Trim();
                    Lbl_Disp_May_Destino.Text = Grid_Partidas_Destino.SelectedRow.Cells[9].Text.Trim();
                    Lbl_Disp_Jun_Destino.Text = Grid_Partidas_Destino.SelectedRow.Cells[10].Text.Trim();
                    Lbl_Disp_Jul_Destino.Text = Grid_Partidas_Destino.SelectedRow.Cells[11].Text.Trim();
                    Lbl_Disp_Ago_Destino.Text = Grid_Partidas_Destino.SelectedRow.Cells[12].Text.Trim();
                    Lbl_Disp_Sep_Destino.Text = Grid_Partidas_Destino.SelectedRow.Cells[13].Text.Trim();
                    Lbl_Disp_Oct_Destino.Text = Grid_Partidas_Destino.SelectedRow.Cells[14].Text.Trim();
                    Lbl_Disp_Nov_Destino.Text = Grid_Partidas_Destino.SelectedRow.Cells[15].Text.Trim();
                    Lbl_Disp_Dic_Destino.Text = Grid_Partidas_Destino.SelectedRow.Cells[16].Text.Trim();

                    //Hf_Disponible.Value = Grid_Partidas_Destino.SelectedRow.Cells[4].Text.Trim();
                    Sumar_Total_Destino();
                    Sumar_Total_Origen();
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Favor de seleccionar otra partida pues esta si existe en el presupuesto";
                    Grid_Partidas_Destino.SelectedIndex = -1;
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion
    #endregion
}