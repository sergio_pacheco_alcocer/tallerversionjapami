﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Actualizar_Presupuesto.Negocio;


public partial class paginas_Presupuestos_Frm_Ope_Psp_Vista_Presupuesto : System.Web.UI.Page
{
    ///*******************************************************************************
    ///PAGE_LOAD
    ///*******************************************************************************
    #region Page_Load
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if ((DataTable)Session["Dt_Presupuestos"] != null)
            {
                Cls_Ope_Psp_Actualizar_Presupuesto_Negocio Clase_Negocio = new Cls_Ope_Psp_Actualizar_Presupuesto_Negocio();
                //Consultamos Las Fuentes de Financiamiento
                DataTable Dt_Fuentes_Financiamiento = Clase_Negocio.Consultar_Fte_Financiamiento();
                Session["Dt_Fuentes_Financiamiento"] = Dt_Fuentes_Financiamiento;
                //Consultamos las Areas Funcionales 
                DataTable Dt_Area_Funcional = Clase_Negocio.Consultar_Areas_Funcionales();
                Session["Dt_Area_Funcional"] = Dt_Area_Funcional;
                //Consultamos los programas
                DataTable Dt_Programas = Clase_Negocio.Consultar_Programas();
                Session["Dt_Programas"] = Dt_Programas;
                //Consultamos las Unidades Responsables
                DataTable Dt_Unidad_Resp = Clase_Negocio.Consultar_Unidad_Responsable();
                Session["Dt_Unidad_Resp"] = Dt_Unidad_Resp;
                //Consultamos las Partidas Especificas
                DataTable Dt_Partidas_Especificas = Clase_Negocio.Consultar_Partidas();
                Session["Dt_Partidas_Especificas"] = Dt_Partidas_Especificas;

                Grid_Presupuestos.DataSource = (DataTable)Session["Dt_Presupuestos"];
                Grid_Presupuestos.DataBind();
            }

        }
    }
    #endregion

    ///*******************************************************************************
    ///METODOS
    ///*******************************************************************************
    #region Metodos

    #endregion
    ///*******************************************************************************
    ///GRID
    ///*******************************************************************************
    #region Grid
    
    protected void Grid_Presupuestos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            String Fuente_F = e.Row.Cells[1].Text.Trim();
            String Area_F = e.Row.Cells[2].Text.Trim();
            String Programa = e.Row.Cells[3].Text.Trim();
            String U_Responsable = e.Row.Cells[4].Text.Trim();
            String Partida = e.Row.Cells[5].Text.Trim();
            DataRow[] ValidarRenglon;
            ImageButton Boton = (ImageButton)e.Row.FindControl("Btn_Alerta");
            Boton.Visible = false;
               
            ValidarRenglon = ((DataTable)Session["Dt_Fuentes_Financiamiento"]).Select("CLAVE = '" + Fuente_F + "'");
            if (ValidarRenglon.Length <= 0)
            {
                Boton.ImageUrl = "../imagenes/gridview/circle_red.png";
                Boton.Visible = true;
                Boton.ToolTip += "FUENTE DE FINANCIAMIENTO no encontrada."; 
            }
            ValidarRenglon = null;
            ValidarRenglon = ((DataTable)Session["Dt_Area_Funcional"]).Select("CLAVE = '" + Area_F + "'");
            if (ValidarRenglon.Length <= 0)
            {
                Boton.ImageUrl = "../imagenes/gridview/circle_red.png";
                Boton.Visible = true;
                Boton.ToolTip += " AREA FUNCIONAL no encontrada."; 
            }
            ValidarRenglon = null;
            ValidarRenglon = ((DataTable)Session["Dt_Programas"]).Select("CLAVE = '" + Programa + "'");
            if (ValidarRenglon.Length <= 0)
            {
                Boton.ImageUrl = "../imagenes/gridview/circle_red.png";
                Boton.Visible = true;
                Boton.ToolTip += " PROGRAMA no encontrado."; 
            }

            ValidarRenglon = null;
            ValidarRenglon = ((DataTable)Session["Dt_Unidad_Resp"]).Select("CLAVE = '" + U_Responsable + "'");
            if (ValidarRenglon.Length <= 0)
            {
                Boton.ImageUrl = "../imagenes/gridview/circle_red.png";
                Boton.Visible = true;
                Boton.ToolTip += " UNIDAD RESPONSABLE no encontrada."; 
            }
            ValidarRenglon = null;
            DataTable dt = (DataTable)Session["Dt_Partidas_Especificas"];
            ValidarRenglon = ((DataTable)Session["Dt_Partidas_Especificas"]).Select("CLAVE = '" + Partida + "'");
            if (ValidarRenglon.Length <= 0)
            {
                Boton.ImageUrl = "../imagenes/gridview/circle_red.png";
                Boton.Visible = true;
                Boton.ToolTip += " PARTIDA no encontrada."; 
            }
        }
    }
    #endregion

    ///*******************************************************************************
    ///EVENTOS
    ///*******************************************************************************
    #region Eventos
    #endregion

    protected void Btn_Actualizar_Presupuesto_Click(object sender, EventArgs e)
    {
        Cls_Ope_Psp_Actualizar_Presupuesto_Negocio Clase_Negocio = new Cls_Ope_Psp_Actualizar_Presupuesto_Negocio();
        Clase_Negocio.P_Dt_Presupuesto = (DataTable)Session["Dt_Presupuestos"];
        String msj = Clase_Negocio.Actualizar_Presupuesto();
    }
}
