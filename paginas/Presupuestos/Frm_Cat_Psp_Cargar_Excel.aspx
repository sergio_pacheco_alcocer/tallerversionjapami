﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true"
 CodeFile="Frm_Cat_Psp_Cargar_Excel.aspx.cs" Inherits="paginas_Presupuestos_Frm_Cat_Psp_Cargar_Excel" Title="SIAC Sistema Integral Administrativo y Comercial" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <script type="text/javascript" language="javascript">
        function calendarShown(sender, args){
            sender._popupBehavior._element.style.zIndex = 10000005;
        }
    </script> 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptLocalization = "True" AsyncPostBackTimeout="720000" ScriptMode="Release" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
     <ContentTemplate>
        <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0"></asp:UpdateProgress>
        <table border="0" cellspacing="0" class="estilo_fuente" width="100%">
                <tr>
                    <td class="label_titulo">Cargar Datos a BD</td>
                </tr>
                <tr>
                    <td >
                        <div id="Div_Contenedor_Msj_Error" style="width:95%;font-size:9px;" runat="server" visible="false">
                        <table style="width:100%;">
                            <tr>
                                <td align="left" style="font-size:12px;color:Red;font-family:Tahoma;text-align:left;">
                                    <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px"/>
                                </td>                        
                                <td style="font-size:9px;width:90%;text-align:left;" valign="top">
                                    <asp:Label ID="Lbl_Mensaje_Error" runat="server" ForeColor="Red" />
                                </td>
                            </tr> 
                        </table>                   
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="Btn_Psp_2012" runat="server" Text="Psp 2012" Width="30%"
                        OnClientClick="return confirm('¿Está seguro?');" 
                            onclick="Btn_Psp_2012_UR_Click"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="Btn_Psp_Modificado_2012" runat="server" Text="Psp 2012 Modificado" Width="30%"
                        OnClientClick="return confirm('¿Está seguro?');" 
                            onclick="Btn_Psp_Modificado_2012_Click"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="Btn_Cargar_Rubros" runat="server" Text="Rubros_Ing" Width="30%"
                        OnClientClick="return confirm('¿Está seguro?');" 
                            onclick="Btn_Cargar_Rubros_Click"/>
                    </td>
                </tr>
                 <tr>
                    <td>
                        <asp:Button ID="Btn_Cargar_Tipos" runat="server" Text="Tipos Ing" Width="30%"
                        OnClientClick="return confirm('¿Está seguro?');" 
                            onclick="Btn_Cargar_Tipos_Click"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="Btn_Cargar_Clases" runat="server" Text="Clases_Ing" Width="30%"
                        OnClientClick="return confirm('¿Está seguro?');" 
                            onclick="Btn_Cargar_Clases_Click"/>
                    </td>
                </tr>  
                <tr>
                    <td>
                        <asp:Button ID="Btn_Conceptos_Ing" runat="server" Text="Conceptos_Ing" Width="30%"
                        OnClientClick="return confirm('¿Está seguro?');" 
                            onclick="Btn_Cargar_Conceptos_Click"/>
                    </td>
                </tr> 
                <tr>
                    <td>
                        <asp:Button ID="Btn_SubConceptos_Ing" runat="server" Text="SubConceptos_Ing" Width="30%"
                        OnClientClick="return confirm('¿Está seguro?');" 
                            onclick="Btn_Cargar_SubConceptos_Click"/>
                    </td>
                </tr>  
                <tr>
                    <td>
                        <asp:Button ID="Btn_Cargar_Pronostico_Ingresos" runat="server" Text="Pronostico_Ingresos" Width="30%"
                        OnClientClick="return confirm('¿Está seguro?');" 
                            onclick="Btn_Cargar_Pronostico_Ingresos_Click"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="Btn_Cargar_Psp_Ingresos" runat="server" Text="Psp_Ingresos" Width="30%"
                        OnClientClick="return confirm('¿Está seguro?');" 
                            onclick="Btn_Cargar_Psp_Ingresos2_Click"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="Btn_Cargar_Fuentes" runat="server" Text="Fuente Financiamiento" Width="30%"
                        OnClientClick="return confirm('¿Está seguro?');" 
                            onclick="Btn_Cargar_Fuentes_Click"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="Btn_Programas" runat="server" Text="Programas" Width="30%"
                        OnClientClick="return confirm('¿Está seguro?');" 
                            onclick="Btn_Cargar_Programas_Click"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="Btn_Concepto_FF" runat="server" Text="Conceptos_FF" Width="30%"
                        OnClientClick="return confirm('¿Está seguro?');" 
                            onclick="Btn_Concepto_FF_Click"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="Btn_PP_FF_Concepto" runat="server" Text="Conceptos_FF_PP" Width="30%"
                        OnClientClick="return confirm('¿Está seguro?');" 
                            onclick="Btn_PP_FF_Concepto_Click"/>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <asp:Button ID="Btn_2a_Modificacion_2013" runat="server" Text="2a_Modificacion_2013" Width="30%"
                        OnClientClick="return confirm('¿Está seguro?');" 
                            onclick="Btn_2a_Modificacion_2013_Click"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="Btn_2a_Modificacion_2013_Economias" runat="server" Text="2a_Modificacion_2013_economias" Width="30%"
                        OnClientClick="return confirm('¿Está seguro?');" 
                            onclick="Btn_2a_Modificacion_2013_Economias_Click"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="Btn_Reg_Mov" runat="server" Text="Registro Movimiento" Width="30%"
                        OnClientClick="return confirm('¿Está seguro?');" 
                            onclick="Btn_Reg_Mov_Click"/>
                    </td>
                </tr>
         </table>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

