﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using CarlosAg.ExcelXmlWriter;
using System.Text;
using JAPAMI.Rpt_Psp_Movimeitnos_Presupuesto_Ingresos.Negocio;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using JAPAMI.Reporte_Presupuesto_Egresos.Negocio;

public partial class paginas_Presupuestos_Frm_Rpt_Psp_Movimientos_Presupuesto_Ingresos : System.Web.UI.Page
{
    #region (Page Load)
        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Page_Load
        ///DESCRIPCIÓN          : Inicio de la pagina
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 05/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
                if (!IsPostBack)
                {
                    Reporte_Inicio();
                    ViewState["SortDirection"] = "DESC";
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error en el inicio del reporte de movimiento de ingresos. Error [" + Ex.Message + "]");
            }
        }
    #endregion

    #region METODOS
        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Reporte_Sueldos_Inicio
        ///DESCRIPCIÓN          : Metodo de inicio de la página
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 05/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        private void Reporte_Inicio()
        {
            try
            {
                Div_Contenedor_Msj_Error.Visible = false;
                Limpiar_Controles("Todo");
                Llenar_Combo_Anios();
                Cmb_Anio.SelectedIndex = -1;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error en el inicio del reporte analitico de ingresos. Error [" + Ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Limpiar_Controles
        ///DESCRIPCIÓN          : Metodo para limpiar los controles del formulario
        ///PARAMETROS           1 Accion: para indicar que parte del codigo limpiara 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 05/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        private void Limpiar_Controles(String Accion)
        {
            try
            {
                switch (Accion)
                {
                    case "Todo":
                        Lbl_Ecabezado_Mensaje.Text = "";
                        Lbl_Mensaje_Error.Text = "";
                        Cmb_Anio.SelectedIndex = -1;
                        Cmb_No_Modificacion.SelectedIndex = -1;
                        break;
                    case "Error":
                        Lbl_Ecabezado_Mensaje.Text = "";
                        Lbl_Mensaje_Error.Text = "";
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al limpiar los controles Error [" + Ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Anios
        ///DESCRIPCIÓN          : Metodo para llenar el combo de los años
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 05/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        private void Llenar_Combo_Anios()
        {
            Cls_Rpt_Psp_Movimeitnos_Presupuesto_Ingresos_Negocio Obj_Ingresos = new Cls_Rpt_Psp_Movimeitnos_Presupuesto_Ingresos_Negocio();
            DataTable Dt_Anios = new DataTable(); //Para almacenar los datos de los tipos de nominas

            try
            {
                Cmb_Anio.Items.Clear();

                Dt_Anios = Obj_Ingresos.Consultar_Anios();

                Cmb_Anio.DataValueField = Ope_Psp_Movimiento_Ing.Campo_Anio;
                Cmb_Anio.DataTextField = Ope_Psp_Movimiento_Ing.Campo_Anio;
                Cmb_Anio.DataSource = Dt_Anios;
                Cmb_Anio.DataBind();

                Cmb_Anio.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al llenar el combo de anios: Error[" + Ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_No_Modificacion
        ///DESCRIPCIÓN          : Metodo para llenar el combo de las modificaciones del año
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 05/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        private void Llenar_Combo_No_Modificacion()
        {
            Cls_Rpt_Psp_Movimeitnos_Presupuesto_Ingresos_Negocio Obj_Ingresos = new Cls_Rpt_Psp_Movimeitnos_Presupuesto_Ingresos_Negocio();
            DataTable Dt = new DataTable(); //Para almacenar los datos de los tipos de nominas

            try
            {
                Cmb_No_Modificacion.Items.Clear();


                if (!String.IsNullOrEmpty(Cmb_Anio.SelectedItem.Text.Trim()))
                {
                    Obj_Ingresos.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                }
                else 
                {
                    Obj_Ingresos.P_Anio = String.Empty;
                }
                Dt = Obj_Ingresos.Consultar_No_Modificacion();
                Cmb_No_Modificacion.DataValueField = Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing;
                Cmb_No_Modificacion.DataTextField = Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing;
                Cmb_No_Modificacion.DataSource = Dt;
                Cmb_No_Modificacion.DataBind();

                Cmb_No_Modificacion.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al llenar el combo de número de modificaciones: Error[" + Ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Generar_Rpt_Ingresos
        ///DESCRIPCIÓN          : Metodo generar el reporte analitico de ingresos
        ///PARAMETROS           1 Dt_Datos: datos del pronostico de ingresos del reporte que se pasaran en excel  
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 02/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        public void Generar_Rpt_Ingresos(DataTable Dt_Datos, String Gpo_Dep, String Dep)
        {
            String Ruta_Archivo = "Reporte_Presupuesto_Ingresos_" + Cmb_Anio.SelectedItem.Value.Trim() + "_" + Cmb_No_Modificacion.SelectedItem.Value.Trim() + "Modificacion.xls";
            WorksheetCell Celda = new WorksheetCell();
            Double Cantidad = 0.00;
            try
            {
                CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
                Libro.Properties.Title = "Reporte_Presupuesto_Ingresos_" + Cmb_Anio.SelectedItem.Value.Trim() + "_" + Cmb_No_Modificacion.SelectedItem.Value.Trim() + "Modificacion";
                Libro.Properties.Created = DateTime.Now;
                Libro.Properties.Author = "JAPAMI";

                //******************************************** Hojas del libro ******************************************************//

                #region (Estilos)
                //******************************************** Estilos del libro ******************************************************//
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_Negritas = Libro.Styles.Add("BodyStyleBold");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cantidad = Libro.Styles.Add("BodyStyleCant");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cantidad_Negritas = Libro.Styles.Add("BodyStyleCantBold");
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Titulo = Libro.Styles.Add("HeaderStyleTitulo");
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Titulo1 = Libro.Styles.Add("HeaderStyleTitulo1");
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Titulo2 = Libro.Styles.Add("HeaderStyleTitulo2");
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Titulo3 = Libro.Styles.Add("HeaderStyleTitulo3");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total_Negritas = Libro.Styles.Add("BodyStyleTotBold");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total = Libro.Styles.Add("BodyTotalStyle");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total_Negritas_Negativo = Libro.Styles.Add("BodyStyleTotBold_Neg");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cantidad_Negativo = Libro.Styles.Add("BodyStyleCant_Neg");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cantidad_Negritas__Negativo = Libro.Styles.Add("BodyStyleCantBold_Neg");

                Estilo_Cabecera.Font.FontName = "Tahoma";
                Estilo_Cabecera.Font.Size = 10;
                Estilo_Cabecera.Font.Bold = true;
                Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Cabecera.Font.Color = "#000000";
                Estilo_Cabecera.Interior.Color = "lightgray";
                Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Titulo.Font.FontName = "Tahoma";
                Estilo_Titulo.Font.Size = 12;
                Estilo_Titulo.Font.Bold = true;
                Estilo_Titulo.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Titulo.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Titulo.Font.Color = "blue";
                Estilo_Titulo.Interior.Color = "white";
                Estilo_Titulo.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Titulo.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 2, "Black");
                Estilo_Titulo.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 2, "Black");
                Estilo_Titulo.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 2, "Black");

                Estilo_Titulo1.Font.FontName = "Tahoma";
                Estilo_Titulo1.Font.Size = 12;
                Estilo_Titulo1.Font.Bold = true;
                Estilo_Titulo1.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Titulo1.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Titulo1.Font.Color = "#000000";
                Estilo_Titulo1.Interior.Color = "white";
                Estilo_Titulo1.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Titulo1.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 2, "Black");
                Estilo_Titulo1.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 2, "Black");

                Estilo_Titulo2.Font.FontName = "Tahoma";
                Estilo_Titulo2.Font.Size = 12;
                Estilo_Titulo2.Font.Bold = true;
                Estilo_Titulo2.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Titulo2.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Titulo2.Font.Color = "#000000";
                Estilo_Titulo2.Interior.Color = "white";
                Estilo_Titulo2.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Titulo2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 2, "Black");
                Estilo_Titulo2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 2, "Black");
                Estilo_Titulo2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 2, "Black");

                Estilo_Titulo3.Font.FontName = "Tahoma";
                Estilo_Titulo3.Font.Size = 10;
                Estilo_Titulo3.Font.Bold = true;
                Estilo_Titulo3.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Titulo3.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Titulo3.Font.Color = "#000000";
                Estilo_Titulo3.Interior.Color = "white";
                Estilo_Titulo3.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Titulo3.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 2, "Black");
                Estilo_Titulo3.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 2, "Black");
                Estilo_Titulo3.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 2, "Black");
                Estilo_Titulo3.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 2, "Black");

                Estilo_Contenido.Font.FontName = "Tahoma";
                Estilo_Contenido.Font.Size = 9;
                Estilo_Contenido.Font.Bold = false;
                Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Estilo_Contenido.Font.Color = "#000000";
                Estilo_Contenido.Interior.Color = "White";
                Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Contenido_Negritas.Font.FontName = "Tahoma";
                Estilo_Contenido_Negritas.Font.Size = 9;
                Estilo_Contenido_Negritas.Font.Bold = true;
                Estilo_Contenido_Negritas.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Estilo_Contenido_Negritas.Font.Color = "#000000";
                Estilo_Contenido_Negritas.Interior.Color = "White";
                Estilo_Contenido_Negritas.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Contenido_Negritas.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido_Negritas.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido_Negritas.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido_Negritas.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Cantidad.Font.FontName = "Tahoma";
                Estilo_Cantidad.Font.Size = 9;
                Estilo_Cantidad.Font.Bold = false;
                Estilo_Cantidad.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Cantidad.Font.Color = "#000000";
                Estilo_Cantidad.Interior.Color = "White";
                Estilo_Cantidad.NumberFormat = "#,###,##0.00";
                Estilo_Cantidad.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cantidad.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Cantidad_Negritas.Font.FontName = "Tahoma";
                Estilo_Cantidad_Negritas.Font.Size = 9;
                Estilo_Cantidad_Negritas.Font.Bold = true;
                Estilo_Cantidad_Negritas.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Cantidad_Negritas.Font.Color = "#000000";
                Estilo_Cantidad_Negritas.Interior.Color = "White";
                Estilo_Cantidad_Negritas.NumberFormat = "#,###,##0.00";
                Estilo_Cantidad_Negritas.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cantidad_Negritas.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad_Negritas.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad_Negritas.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad_Negritas.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Total_Negritas.Font.FontName = "Tahoma";
                Estilo_Total_Negritas.Font.Size = 9;
                Estilo_Total_Negritas.Font.Bold = true;
                Estilo_Total_Negritas.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Total_Negritas.Font.Color = "#000000";
                Estilo_Total_Negritas.Interior.Color = "Yellow";
                Estilo_Total_Negritas.NumberFormat = "#,###,##0.00";
                Estilo_Total_Negritas.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Total_Negritas.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Negritas.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Negritas.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Negritas.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Total.Font.FontName = "Tahoma";
                Estilo_Total.Font.Size = 9;
                Estilo_Total.Font.Bold = true;
                Estilo_Total.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Estilo_Total.Font.Color = "#000000";
                Estilo_Total.Interior.Color = "Yellow";
                Estilo_Total.NumberFormat = "#,###,##0.00";
                Estilo_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Cantidad_Negativo.Font.FontName = "Tahoma";
                Estilo_Cantidad_Negativo.Font.Size = 9;
                Estilo_Cantidad_Negativo.Font.Bold = false;
                Estilo_Cantidad_Negativo.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Cantidad_Negativo.Font.Color = "Red";
                Estilo_Cantidad_Negativo.Interior.Color = "White";
                Estilo_Cantidad_Negativo.NumberFormat = "#,###,##0.00";
                Estilo_Cantidad_Negativo.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cantidad_Negativo.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad_Negativo.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad_Negativo.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad_Negativo.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Cantidad_Negritas__Negativo.Font.FontName = "Tahoma";
                Estilo_Cantidad_Negritas__Negativo.Font.Size = 9;
                Estilo_Cantidad_Negritas__Negativo.Font.Bold = true;
                Estilo_Cantidad_Negritas__Negativo.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Cantidad_Negritas__Negativo.Font.Color = "Red";
                Estilo_Cantidad_Negritas__Negativo.Interior.Color = "White";
                Estilo_Cantidad_Negritas__Negativo.NumberFormat = "#,###,##0.00";
                Estilo_Cantidad_Negritas__Negativo.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cantidad_Negritas__Negativo.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad_Negritas__Negativo.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad_Negritas__Negativo.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad_Negritas__Negativo.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Total_Negritas_Negativo.Font.FontName = "Tahoma";
                Estilo_Total_Negritas_Negativo.Font.Size = 9;
                Estilo_Total_Negritas_Negativo.Font.Bold = true;
                Estilo_Total_Negritas_Negativo.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Total_Negritas_Negativo.Font.Color = "Red";
                Estilo_Total_Negritas_Negativo.Interior.Color = "Yellow";
                Estilo_Total_Negritas_Negativo.NumberFormat = "#,###,##0.00";
                Estilo_Total_Negritas_Negativo.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Total_Negritas_Negativo.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Negritas_Negativo.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Negritas_Negativo.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Negritas_Negativo.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                #endregion

                #region (Hoja1 - Ingresos)
                //Creamos una hoja que tendrá el libro.
                CarlosAg.ExcelXmlWriter.Worksheet Hoja1 = Libro.Worksheets.Add("Ingresos");
                //Agregamos un renglón a la hoja de excel.
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon_1;
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon1;

                //Agregamos las columnas que tendrá la hoja de excel.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//Concepto.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//ingresos.
                //se llena el encabezado principal de la hoja uno
                Renglon_1 = Hoja1.Table.Rows.Add();
                Celda = Renglon_1.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                Celda.MergeAcross = 1; // Merge 3 cells together
                Celda.StyleID = "HeaderStyleTitulo";
                Renglon_1 = Hoja1.Table.Rows.Add();
                Celda = Renglon_1.Cells.Add(Gpo_Dep.ToUpper());
                Celda.MergeAcross = 1; // Merge 3 cells together
                Celda.StyleID = "HeaderStyleTitulo1";
                Renglon_1 = Hoja1.Table.Rows.Add();
                Celda = Renglon_1.Cells.Add(Dep.ToUpper());
                Celda.MergeAcross = 1; // Merge 3 cells together
                Celda.StyleID = "HeaderStyleTitulo2";
                Renglon1 = Hoja1.Table.Rows.Add();
                Renglon_1 = Hoja1.Table.Rows.Add();
                Celda = Renglon_1.Cells.Add(Cmb_No_Modificacion.SelectedItem.Value.Trim() + "a. MODIFICACIÓN AL PRONOSTICO DE INGRESOS EJERCICIO " + Cmb_Anio.SelectedItem.Value.Trim());
                Celda.MergeAcross = 1; // Merge 3 cells together
                Celda.StyleID = "HeaderStyleTitulo3";
                Renglon1 = Hoja1.Table.Rows.Add();

                Renglon1 = Hoja1.Table.Rows.Add();
                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CONCEPTO", "HeaderStyle"));
                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRONOSTICO INGRESOS " + Cmb_Anio.SelectedItem.Value.Trim() + " MODIFICADO", "HeaderStyle"));
                #endregion

                #region (Hoja2 -  analitico)
                //Creamos una hoja que tendrá el libro.
                CarlosAg.ExcelXmlWriter.Worksheet Hoja2 = Libro.Worksheets.Add("Analitico");
                //Agregamos un renglón a la hoja de excel.
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon_2;
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon2;

                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(280));//Concepto.
                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//aprobado.
                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//incremento.
                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//disminucion.
                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//modificado.

                //Agregamos las columnas que tendrá la hoja de excel.
                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//Concepto.
                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//ingresos.
                //se llena el encabezado principal de la hoja uno
                Renglon_2 = Hoja2.Table.Rows.Add();
                Celda = Renglon_2.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                Celda.MergeAcross = 4; // Merge 3 cells together
                Celda.StyleID = "HeaderStyleTitulo";
                Renglon_2 = Hoja2.Table.Rows.Add();
                Celda = Renglon_2.Cells.Add(Gpo_Dep.ToUpper());
                Celda.MergeAcross = 4; // Merge 3 cells together
                Celda.StyleID = "HeaderStyleTitulo1";
                Renglon_2 = Hoja2.Table.Rows.Add();
                Celda = Renglon_2.Cells.Add(Dep.ToUpper());
                Celda.MergeAcross = 4; // Merge 3 cells together
                Celda.StyleID = "HeaderStyleTitulo2";
                Renglon2 = Hoja2.Table.Rows.Add();
                Renglon_2 = Hoja2.Table.Rows.Add();
                Celda = Renglon_2.Cells.Add(Cmb_No_Modificacion.SelectedItem.Value.Trim() + "a. MODIFICACIÓN AL PRONOSTICO DE INGRESOS EJERCICIO " + Cmb_Anio.SelectedItem.Value.Trim());
                Celda.MergeAcross = 4; // Merge 3 cells together
                Celda.StyleID = "HeaderStyleTitulo3";
                Renglon2 = Hoja2.Table.Rows.Add();

                Renglon2 = Hoja2.Table.Rows.Add();
                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CONCEPTO", "HeaderStyle"));
                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRONOSTICO INGRESOS " + Cmb_Anio.SelectedItem.Value.Trim() + " AUTORIZADO", "HeaderStyle"));
                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("INCREMENTO", "HeaderStyle"));
                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("DISMINUCIÓN", "HeaderStyle"));
                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRONOSTICO INGRESOS " + Cmb_Anio.SelectedItem.Value.Trim() + " MODIFICADO", "HeaderStyle"));
                #endregion

                //**************************************** Agregamos los datos al reporte ***********************************************//
                if (Dt_Datos is DataTable)
                {
                    if (Dt_Datos.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Datos.Rows)
                        {
                            Renglon1 = Hoja1.Table.Rows.Add();
                            Renglon2 = Hoja2.Table.Rows.Add();

                            if (Dr["TIPO"].ToString().Trim().Equals("RUBRO") || Dr["TIPO"].ToString().Trim().Equals("TIPO") ||
                                Dr["TIPO"].ToString().Trim().Equals("CLASE") || Dr["TIPO"].ToString().Trim().Equals("CONCEPTO"))
                            {
                                foreach (DataColumn Columna in Dt_Datos.Columns)
                                {
                                    if (Columna is DataColumn)
                                    {
                                        if (Columna.ColumnName.Equals("CONCEPTO"))
                                        {
                                            if (Dr["TIPO"].ToString().Trim().Equals("RUBRO")) 
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("**** " + Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyleBold"));
                                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("**** " + Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyleBold"));
                                            }
                                            else if (Dr["TIPO"].ToString().Trim().Equals("TIPO"))
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("*** " + Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyleBold"));
                                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("*** " + Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyleBold"));
                                            }
                                            else if (Dr["TIPO"].ToString().Trim().Equals("CLASE"))
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("** " + Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyleBold"));
                                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("** " + Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyleBold"));
                                            }
                                            else if (Dr["TIPO"].ToString().Trim().Equals("CONCEPTO"))
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("* " + Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyleBold"));
                                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("* " + Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyleBold"));
                                            }
                                        }
                                        else if (Columna.ColumnName.Equals("MODIFICADO"))
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "BodyStyleCantBold"));
                                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "BodyStyleCantBold"));
                                            }
                                            else
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "BodyStyleCantBold_Neg"));
                                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "BodyStyleCantBold_Neg"));
                                            }
                                        }
                                        else if (Columna.ColumnName.Equals("INCREMENTO") || Columna.ColumnName.Equals("DISMINUCION") || Columna.ColumnName.Equals("APROBADO"))
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "BodyStyleCantBold"));
                                            else
                                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "BodyStyleCantBold_Neg"));
                                        }
                                    }
                                }
                            }
                            else if (Dr["TIPO"].ToString().Trim().Equals("TOTAL"))
                            {
                                foreach (DataColumn Columna in Dt_Datos.Columns)
                                {
                                    if (Columna is DataColumn)
                                    {
                                        if (Columna.ColumnName.Equals("CONCEPTO"))
                                        {
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyTotalStyle"));
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyTotalStyle"));
                                        }
                                        else if (Columna.ColumnName.Equals("MODIFICADO"))
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "BodyStyleTotBold"));
                                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "BodyStyleTotBold"));
                                            }
                                            else
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "BodyStyleTotBold_Neg"));
                                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "BodyStyleTotBold_Neg"));
                                            }
                                        }
                                        else if (Columna.ColumnName.Equals("INCREMENTO") || Columna.ColumnName.Equals("DISMINUCION") || Columna.ColumnName.Equals("APROBADO"))
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "BodyStyleTotBold"));
                                            else
                                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "BodyStyleTotBold_Neg"));
                                        }
                                    }
                                }
                            }
                            else
                            {
                                foreach (DataColumn Columna in Dt_Datos.Columns)
                                {
                                    if (Columna is DataColumn)
                                    {
                                        if (Columna is DataColumn)
                                        {
                                            if (Columna.ColumnName.Equals("CONCEPTO"))
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyle"));
                                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyle"));
                                            }
                                            else if (Columna.ColumnName.Equals("MODIFICADO"))
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "BodyStyleCant"));
                                                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "BodyStyleCant"));
                                                }
                                                else
                                                {
                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "BodyStyleCant_Neg"));
                                                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "BodyStyleCant_Neg"));
                                                }
                                            }
                                            else if (Columna.ColumnName.Equals("INCREMENTO") || Columna.ColumnName.Equals("DISMINUCION") || Columna.ColumnName.Equals("APROBADO"))
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "BodyStyleCant"));
                                                else
                                                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "BodyStyleCant_Neg"));
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        Renglon_1 = Hoja1.Table.Rows.Add();
                        Renglon_1 = Hoja1.Table.Rows.Add();
                        Celda = Renglon_1.Cells.Add("ELABORO: " + Dt_Datos.Rows[0]["ELABORO"].ToString().Trim());
                        Celda.MergeAcross = 1; // Merge 3 cells together
                        Celda.StyleID = "BodyStyle";
                        //se llena el encabezado principal de la hoja dos
                        Renglon_2 = Hoja2.Table.Rows.Add();
                        Renglon_2 = Hoja2.Table.Rows.Add();
                        Celda = Renglon_2.Cells.Add("ELABORO: " + Dt_Datos.Rows[0]["ELABORO"].ToString().Trim());
                        Celda.MergeAcross = 4; // Merge 3 cells together
                        Celda.StyleID = "BodyStyle";
                    }
                }

                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + Ruta_Archivo);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;
                Libro.Save(Response.OutputStream);
                Response.End();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al generar el reporte en excel Error[" + ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Validar_Campos
        ///DESCRIPCIÓN          : Metodo para validar los campos del formulario
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 05/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        private Boolean Validar_Campos()
        {
            Boolean Datos_Validos = true;
            Limpiar_Controles("Error");
            Lbl_Ecabezado_Mensaje.Text = "Favor de:";
            try
            {
                if (Cmb_Anio.SelectedIndex <= 0)
                {
                    Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Seleccionar un año <br />";
                    Datos_Validos = false;
                }
                if (Cmb_No_Modificacion.SelectedIndex <= 0)
                {
                    Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Seleccionar un número de modificación <br />";
                    Datos_Validos = false;
                }

                return Datos_Validos;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al valodar los campos del formulario Error [" + Ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Registros
        ///DESCRIPCIÓN          : Metodo para llenar el grid de los registros
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 21/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        private void Llenar_Grid_Registros(DataTable Dt_Registros)
        {
            try
            {
                if (Dt_Registros != null)
                {
                    Grid_Registros.Columns[5].Visible = true;
                    Grid_Registros.DataSource = Dt_Registros;
                    Grid_Registros.DataBind();
                    Grid_Registros.Columns[5].Visible = false;
                }
                else
                {
                    Grid_Registros.DataSource = new DataTable();
                    Grid_Registros.DataBind();
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al llenar el grid de los registros: Error[" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Gpo_Dependencia
        ///DESCRIPCIÓN          : metodo para obtener el grupo dependencia para el reporte
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 18/Febrero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private String Obtener_Gpo_Dependencia(String Tipo)
        {
            Cls_Rpt_Presupuesto_Egresos_Negocio Negocios = new Cls_Rpt_Presupuesto_Egresos_Negocio();
            String Dato = String.Empty;
            DataTable Dt_Datos = new DataTable();

            try
            {
                Negocios.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado.Trim();
                Dt_Datos = Negocios.Consultar_Gpo_Dependencia();

                if (Dt_Datos != null)
                {
                    if (Dt_Datos.Rows.Count > 0)
                    {
                        if (Tipo.Trim().Equals("UR"))
                        {
                            Dato = Dt_Datos.Rows[0]["UR"].ToString().Trim();
                        }
                        else
                        {
                            Dato = Dt_Datos.Rows[0]["GPO_DEP"].ToString().Trim();
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al obtener el grupo dependencia. Error[" + Ex.Message + "]");
            }
            return Dato;
        }
    #endregion

    #region EVENTOS
        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
        ///DESCRIPCIÓN          : Ejecuta la operacion de click en el boton de salir
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 05/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            if (Btn_Salir.ToolTip.Trim().Equals("Salir"))
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Generar_Click
        ///DESCRIPCIÓN          : Ejecuta la operacion de click en el boton de generar
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 05/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        protected void Btn_Generar_Click(object sender, EventArgs e)
        {
            Cls_Rpt_Psp_Movimeitnos_Presupuesto_Ingresos_Negocio Obj_Ingresos = new Cls_Rpt_Psp_Movimeitnos_Presupuesto_Ingresos_Negocio(); //Conexion con la capa de negocios
            DataTable Dt = new DataTable(); //Para almacenar los datos de las ordenes de compras
            Div_Contenedor_Msj_Error.Visible = false;
            Limpiar_Controles("Error");
            String Complemento = String.Empty;
            String Dependencia = String.Empty;
            String Gpo_Dependencia = String.Empty;

            try
            {
                if (Validar_Campos())
                {
                   
                    Obj_Ingresos.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                    Obj_Ingresos.P_No_Movimiento_Ing = Cmb_No_Modificacion.SelectedItem.Value.Trim();
                    Dt = Obj_Ingresos.Consultar_Movimientos_Autorizado();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            //obtenemos los datos de la dependencia y del grupo dependencia de la persona que obtendra  el reporte
                            Dependencia = Obtener_Gpo_Dependencia("UR");
                            Gpo_Dependencia = Obtener_Gpo_Dependencia("GPO_DEP");

                            Llenar_Grid_Registros(Dt);
                            Generar_Rpt_Ingresos(Dt, Gpo_Dependencia, Dependencia);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                    }
                }
                else
                {
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del boton de generar el reporte. Error[" + ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Generar_Reporte_Click
        ///DESCRIPCIÓN          : Ejecuta la operacion de click en el boton de generar
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 05/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        protected void Btn_Generar_Reporte_Click(object sender, EventArgs e)
        {
            Cls_Rpt_Psp_Movimeitnos_Presupuesto_Ingresos_Negocio Obj_Ingresos = new Cls_Rpt_Psp_Movimeitnos_Presupuesto_Ingresos_Negocio(); //Conexion con la capa de negocios
            DataTable Dt = new DataTable(); //Para almacenar los datos de las ordenes de compras
            Div_Contenedor_Msj_Error.Visible = false;
            Limpiar_Controles("Error");
            String Complemento = String.Empty;
            DataSet Ds_Registros = null;

            try
            {
                if (Validar_Campos())
                {
                    Obj_Ingresos.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                    Obj_Ingresos.P_No_Movimiento_Ing = Cmb_No_Modificacion.SelectedItem.Value.Trim();
                    Dt = Obj_Ingresos.Consultar_Movimientos_Autorizado();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            Llenar_Grid_Registros(Dt);

                            Ds_Registros = new DataSet();
                            Dt.TableName = "Dt_Mov";
                            Ds_Registros.Tables.Add(Dt.Copy());
                            Generar_Reporte(ref Ds_Registros, "Cr_Rpt_Psp_Movimientos_Ingresos.rpt",
                                "Rpt_Movimientos_Ingresos_" + Cmb_No_Modificacion.SelectedItem.Value.Trim() + "a_Modificación_" + Cmb_Anio.SelectedItem.Value.Trim()+ ".pdf");
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                    }
                }
                else
                {
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del boton de generar el reporte. Error[" + ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Consultar_Click
        ///DESCRIPCIÓN          : Ejecuta la operacion de click en el boton de generar
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 05/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        protected void Btn_Consultar_Click(object sender, EventArgs e)
        {
            Cls_Rpt_Psp_Movimeitnos_Presupuesto_Ingresos_Negocio Obj_Ingresos = new Cls_Rpt_Psp_Movimeitnos_Presupuesto_Ingresos_Negocio(); //Conexion con la capa de negocios
            DataTable Dt = new DataTable(); //Para almacenar los datos de las ordenes de compras
            Div_Contenedor_Msj_Error.Visible = false;
            Limpiar_Controles("Error");
            String Complemento = String.Empty;

            try
            {
                if (Validar_Campos())
                {
                    Obj_Ingresos.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                    Obj_Ingresos.P_No_Movimiento_Ing = Cmb_No_Modificacion.SelectedItem.Value.Trim();
                    Dt = Obj_Ingresos.Consultar_Movimientos_Autorizado();
                    

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            Llenar_Grid_Registros(Dt);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                    }
                }
                else
                {
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del boton de generar el reporte. Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Grid_Registros_Sorting
        ///DESCRIPCIÓN          : Evento de ordenar las columnas de los grids
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 05/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Grid_Registros_Sorting(object sender, GridViewSortEventArgs e)
        {
            Cls_Rpt_Psp_Movimeitnos_Presupuesto_Ingresos_Negocio Obj_Ingresos = new Cls_Rpt_Psp_Movimeitnos_Presupuesto_Ingresos_Negocio(); //Conexion con la capa de negocios
            DataTable Grid_Registros_Sorting = new DataTable();
            DataTable Dt_Pronostico = new DataTable();

            try
            {
                Obj_Ingresos.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                Obj_Ingresos.P_No_Movimiento_Ing = Cmb_No_Modificacion.SelectedItem.Value.Trim();
                Dt_Pronostico = Obj_Ingresos.Consultar_Movimientos_Autorizado();

                Grid_Registros_Sorting = (DataTable)Grid_Registros.DataSource;
                if (Grid_Registros_Sorting != null)
                {
                    DataView Dv_Vista = new DataView(Grid_Registros_Sorting);
                    String Orden = ViewState["SortDirection"].ToString();
                    if (Orden.Equals("ASC"))
                    {
                        Dv_Vista.Sort = e.SortExpression + " DESC";
                        ViewState["SortDirection"] = "DESC";
                    }
                    else
                    {
                        Dv_Vista.Sort = e.SortExpression + " ASC";
                        ViewState["SortDirection"] = "ASC";
                    }

                    Grid_Registros.Columns[5].Visible = true;
                    Grid_Registros.DataSource = Dv_Vista;
                    Grid_Registros.DataBind();
                    Grid_Registros.Columns[5].Visible = false;
                }
            }
            catch (Exception Ex)
            {
                Div_Contenedor_Msj_Error.Visible = true;
                Lbl_Ecabezado_Mensaje.Text = "Error al ordenar la tabla de clases. Error[" + Ex.Message + "]";
                Lbl_Mensaje_Error.Text = String.Empty;
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Grid_Registros_RowDataBound
        ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 05/Junio/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Grid_Registros_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Double Tot = 0.00;

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Tot = Convert.ToDouble(!String.IsNullOrEmpty(e.Row.Cells[4].Text.Trim()) ? "0" : e.Row.Cells[4].Text.Trim());

                    if (e.Row.Cells[5].Text.Trim().Equals("RUBRO"))
                    {
                        e.Row.Style.Add("background-color", "#FFFF99");
                        e.Row.Style.Add("color", "black");
                        e.Row.Cells[0].Text = "**** " + e.Row.Cells[0].Text;
                    }
                    else if (e.Row.Cells[5].Text.Trim().Equals("TIPO"))
                    {
                        e.Row.Style.Add("background-color", "#FFFF99");
                        e.Row.Style.Add("color", "black");
                        e.Row.Cells[0].Text = "*** " + e.Row.Cells[0].Text;
                    }
                    else if (e.Row.Cells[5].Text.Trim().Equals("CLASE"))
                    {
                        e.Row.Style.Add("background-color", "#FFFF99");
                        e.Row.Style.Add("color", "black");
                        e.Row.Cells[0].Text = "** " + e.Row.Cells[0].Text;
                    }
                    else if (e.Row.Cells[5].Text.Trim().Equals("CONCEPTO"))
                    {
                        e.Row.Style.Add("background-color", "#FFFF99");
                        e.Row.Style.Add("color", "black");
                        e.Row.Cells[0].Text = "* " + e.Row.Cells[0].Text;
                    }

                    if (e.Row.Cells[5].Text.Trim().Equals("TOTAL"))
                    {
                        e.Row.Style.Add("background-color", "#FFCC66");
                        e.Row.Style.Add("color", "black");
                    }
                    if (Tot < 0)
                        e.Row.Cells[4].Style.Add("color", "red");
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error:[" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Anio_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de años
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 05/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_Anio_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Llenar_Combo_No_Modificacion();
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del combo de años Error[" + ex.Message + "]");
            }
        }
    #endregion

    #region (Reportes)
        /// *************************************************************************************
        /// NOMBRE: Generar_Reporte
        /// 
        /// DESCRIPCIÓN: Método que invoca la generación del reporte.
        ///              
        /// PARÁMETROS: Nombre_Plantilla_Reporte.- Nombre del archivo del Crystal Report.
        ///             Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
        /// 
        /// USUARIO CREO: Juan Alberto Hernández Negrete.
        /// FECHA CREO: 3/Mayo/2011 18:15 p.m.
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACIÓN:
        /// *************************************************************************************
        protected void Generar_Reporte(ref DataSet Ds_Datos, String Nombre_Plantilla_Reporte, String Nombre_Reporte_Generar)
        {
            ReportDocument Reporte = new ReportDocument();//Variable de tipo reporte.
            String Ruta = String.Empty;//Variable que almacenara la ruta del archivo del crystal report. 

            try
            {
                Ruta = @Server.MapPath("../Rpt/Presupuestos/" + Nombre_Plantilla_Reporte);
                Reporte.Load(Ruta);

                if (Ds_Datos is DataSet)
                {
                    if (Ds_Datos.Tables.Count > 0)
                    {
                        Reporte.SetDataSource(Ds_Datos);
                        Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar);
                        Mostrar_Reporte(Nombre_Reporte_Generar);
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
            }
        }

        /// *************************************************************************************
        /// NOMBRE: Exportar_Reporte_PDF
        /// 
        /// DESCRIPCIÓN: Método que guarda el reporte generado en formato PDF en la ruta
        ///              especificada.
        ///              
        /// PARÁMETROS: Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
        ///             Nombre_Reporte.- Nombre que se le dará al reporte.
        /// 
        /// USUARIO CREO: Juan Alberto Hernández Negrete.
        /// FECHA CREO: 3/Mayo/2011 18:19 p.m.
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACIÓN:
        /// *************************************************************************************
        protected void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte)
        {
            ExportOptions Opciones_Exportacion = new ExportOptions();
            DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
            PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

            try
            {
                if (Reporte is ReportDocument)
                {
                    Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte);
                    Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                    Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                    Reporte.Export(Opciones_Exportacion);
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
            }
        }

        /// *************************************************************************************
        /// NOMBRE: Mostrar_Reporte
        /// 
        /// DESCRIPCIÓN: Muestra el reporte en pantalla.
        ///              
        /// PARÁMETROS: Nombre_Reporte.- Nombre que tiene el reporte que se mostrara en pantalla.
        /// 
        /// USUARIO CREO: Juan Alberto Hernández Negrete.
        /// FECHA CREO: 3/Mayo/2011 18:20 p.m.
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACIÓN:
        /// *************************************************************************************
        protected void Mostrar_Reporte(String Nombre_Reporte)
        {
            //String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
            String Pagina = "../../Reporte/";

            try
            {
                Pagina = Pagina + Nombre_Reporte;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt_Nominas_Negativas",
                    "window.open('" + Pagina + "', 'Busqueda_Empleados','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
        }
    #endregion
}
