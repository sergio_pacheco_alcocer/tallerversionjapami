﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Psp_Presupuesto.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Ayudante_JQuery;
using JAPAMI.Nodo_Atributos;
using JAPAMI.Nodo_Arbol;
using Newtonsoft.Json;
using System.Collections.Generic;
using JAPAMI.Movimiento_Presupuestal.Negocio;
using JAPAMI.Clasificacion_Gasto.Negocio;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using System.Text;
using JAPAMI.Polizas.Negocios;
using JAPAMI.Ope_Psp_Movimientos_Ingresos.Negocio;

public partial class paginas_Presupuestos_Frm_Ope_Psp_Controlador_Presupuesto : System.Web.UI.Page
{
    #region PAGE LOAD
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Page_Load
    ///DESCRIPCIÓN          : Metodo para el inicio de la pagina
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 25/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        if (!IsPostBack)
        {
            //validamos si es una petición para obtener el tipo de usuario que acceso a la pagina o si es una petición
            //para llenar los controles de la pagina
            String Accion = String.Empty;
            if (this.Request.QueryString["Accion"] != null)
            {
                if (!String.IsNullOrEmpty(this.Request.QueryString["Accion"].ToString().Trim()))
                {
                    Accion = this.Request.QueryString["Accion"].ToString().Trim();
                    if (Accion.Trim().Equals("Usuarios"))
                    {
                        Response.Clear();
                        Response.ContentType = "text/html";
                        String Json_Cadena = Obtener_Tipo_Usuario();
                        Response.Write(Json_Cadena);
                        Response.Flush();
                        Response.Close();
                    }
                    else 
                    {
                        Obtener_Parametros();
                    }
                }
            }
        }
    }

    #endregion

    #region METODOS

    #region (Metodos Generales)
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Controlador_Inicio
    ///DESCRIPCIÓN          : Metodo para el inicio de la pagina
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 25/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Controlador_Inicio(String Busqueda, String Anio, String Tipo_Psp, String Mes, String Fecha_Ini, String Fecha_Fin,
        String FF_ID, String AF_ID, String UR_ID, String UR_F_ID, String PP_ID, String PP_F_ID, String PP_Ing_ID, String PP_IngF_ID,
        String Capitulo_ID, String Concepto_ID, String Partida_Gen_ID, String Partida_ID, String CapituloF_ID, String ConceptoF_ID,
        String Partida_GenF_ID, String PartidaF_ID, String Rubro_ID, String Tipo_ID, String Clase_ID, String Concepto_Ing_ID,
        String SubConcepto_ID, String FF_Ing_ID, String Rubro_F_ID, String Tipo_F_ID, String Clase_F_ID, String Concepto_F_Ing_ID,
        String SubConcepto_F_ID, String Estado, String Tot, String Tipo_Det, String Reporte,
        String No_Poliza, String Tipo_Poliza, String Mes_Anio, String No_Modificacion, String No_Solicitud)
    {
        String Accion = String.Empty;
        String Json_Cadena = String.Empty;
        Boolean Exixte_Reporte = false;
        Response.Clear();

        if (this.Request.QueryString["Accion"] != null)
        {
            if (!String.IsNullOrEmpty(this.Request.QueryString["Accion"].ToString().Trim()))
            {
                Accion = this.Request.QueryString["Accion"].ToString().Trim();
                switch (Accion)
                {
                    #region(Generales)
                    case "Anios":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Anios(Busqueda, Tipo_Psp);
                        break;
                    #endregion

                    #region(Combos Ingresos)
                    case "FF_Ing": //obtenermos los datos de las fuentes de financiamiento de ingresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_FF(Busqueda, Anio, "ING", FF_Ing_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID);
                        break;
                    case "PP_Ing": //obtenermos los datos de los proyecto programas de ingresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Programas(Busqueda, Anio, "ING", FF_Ing_ID, AF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID);
                        break;
                    case "R"://obtenermos los datos de los rubros de ingresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Rubros(Busqueda, Anio, Tipo_Psp, FF_Ing_ID, PP_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID);
                        break;
                    case "T"://obtenermos los datos de los tipos de ingresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Tipos(Busqueda, Anio, Tipo_Psp, FF_Ing_ID, PP_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID);
                        break;
                    case "CL"://obtenermos los datos de las clases de ingresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Clases(Busqueda, Anio, Tipo_Psp, FF_Ing_ID, PP_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID);
                        break;
                    case "CON_ING"://obtenermos los datos de los conceptos de ingresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Conceptos_Ing(Busqueda, Anio, Tipo_Psp, FF_Ing_ID, PP_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID);
                        break;
                    case "SUBCON"://obtenermos los datos de los subconceptos de ingresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_SubConceptos(Busqueda, Anio, Tipo_Psp, FF_Ing_ID, PP_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID);
                        break;
                    #endregion

                    #region(Combos_Egresos)
                    case "FF": //obtenermos los datos de las fuentes de financiamiento de egresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_FF(Busqueda, Anio, "EGR", FF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID);
                        break;
                    case "AF": //obtenermos los datos de las áreas funcionales
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_AF(Busqueda, Anio, Tipo_Psp, FF_ID, AF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID);
                        break;
                    case "UR": //obtenermos los datos de las dependencias
                        Exixte_Reporte = false; 
                        Json_Cadena = Obtener_UR(Busqueda, Anio, Tipo_Psp, FF_ID, AF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID);
                        break;
                    case "PP": //obtenermos los datos de los proyectos programas de egresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Programas(Busqueda, Anio, "EGR", FF_ID, AF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID);
                        break;
                    case "CAP": //obtenermos los datos de los capitulos de egresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Capitulos(Busqueda, Anio, Tipo_Psp, FF_ID, AF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID);
                        break;
                    case "CON": //obtenermos los datos de los conceptos de egresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Conceptos(Busqueda, Anio, Tipo_Psp, FF_ID, AF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID);
                        break;
                    case "PG": //obtenermos los datos de las partidas genericas de egresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Partidas_Genericas(Busqueda, Anio, Tipo_Psp, FF_ID, AF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID);
                        break;
                    case "P": //obtenermos los datos de las partidas especificas de egresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Partidas(Busqueda, Anio, Tipo_Psp, FF_ID, AF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID);
                        break;
                    #endregion

                    #region(Nivel Ingresos)
                    case "Nivel_FF_Ing": //obtenemos los datos del nivel de las fuentes de financiamientos de ingresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_FF_Ing(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID,
                                                          PP_Ing_ID, PP_IngF_ID, Rubro_F_ID, Tipo_F_ID, Clase_F_ID, Concepto_F_Ing_ID, SubConcepto_F_ID, Estado, Tot);
                        break;
                    case "Nivel_PP_Ing": //obtenemos los datos del nivel de los proyectos programas de ingresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_PP_Ing(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID,
                                                          PP_Ing_ID, PP_IngF_ID, Rubro_F_ID, Tipo_F_ID, Clase_F_ID, Concepto_F_Ing_ID, SubConcepto_F_ID, Estado, Tot);
                        break;
                    case "Nivel_Rubro":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_Rubro(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID,
                                                          PP_Ing_ID, PP_IngF_ID, Rubro_F_ID, Tipo_F_ID, Clase_F_ID, Concepto_F_Ing_ID, SubConcepto_F_ID, Estado, Tot);
                        break;
                    case "Nivel_Tipo":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_Tipos(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID,
                                                            PP_Ing_ID, PP_IngF_ID, Rubro_F_ID, Tipo_F_ID, Clase_F_ID, Concepto_F_Ing_ID, SubConcepto_F_ID, Estado, Tot);
                        break;
                    case "Nivel_Clase":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_Clases(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID,
                                                            PP_Ing_ID, PP_IngF_ID, Rubro_F_ID, Tipo_F_ID, Clase_F_ID, Concepto_F_Ing_ID, SubConcepto_F_ID, Estado, Tot);
                        break;
                    case "Nivel_Concepto_Ing":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_Conceptos_Ing(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID,
                                                            PP_Ing_ID, PP_IngF_ID, Rubro_F_ID, Tipo_F_ID, Clase_F_ID, Concepto_F_Ing_ID, SubConcepto_F_ID, Estado, Tot);
                        break;
                    case "Nivel_SubConcepto":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_SubConceptos(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID,
                                                            PP_Ing_ID, PP_IngF_ID, Rubro_F_ID, Tipo_F_ID, Clase_F_ID, Concepto_F_Ing_ID, SubConcepto_F_ID, Estado, Tot);
                        break;
                    #endregion

                    #region(Nivel Egresos)
                    case "Nivel_FF": //obtenemos los datos del nivel de las fuentes de financiamientos de egresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_FF(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID,
                            UR_F_ID, PP_F_ID, CapituloF_ID, ConceptoF_ID, Partida_GenF_ID, PartidaF_ID, AF_ID, Estado, Tot);
                        break;
                    case "Nivel_AF": //obtenemos los datos del nivel de las areas funcionales pertenecientes al presupuesto de egresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_AF(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID,
                            UR_F_ID, PP_F_ID, CapituloF_ID, ConceptoF_ID, Partida_GenF_ID, PartidaF_ID, AF_ID, Estado, Tot);
                        break;
                    case "Nivel_PP": //obtenemos los datos del nivel de los proyectos programas al presupuesto de egresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_PP(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID,
                            UR_F_ID, PP_F_ID, CapituloF_ID, ConceptoF_ID, Partida_GenF_ID, PartidaF_ID, AF_ID, Estado, Tot);
                        break;
                    case "Nivel_UR": //obtenemos los datos del nivel de las unidades responsables pertenecientes al presupuesto de egresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_UR(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID,
                            UR_F_ID, PP_F_ID, CapituloF_ID, ConceptoF_ID, Partida_GenF_ID, PartidaF_ID, AF_ID, Estado, Tot);
                        break;
                    case "Nivel_Capitulos":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_Capitulos(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID,
                            UR_F_ID, PP_F_ID, CapituloF_ID, ConceptoF_ID, Partida_GenF_ID, PartidaF_ID, AF_ID, Estado, Tot);
                        break;
                    case "Nivel_Concepto":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_Conceptos(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID,
                            UR_F_ID, PP_F_ID, CapituloF_ID, ConceptoF_ID, Partida_GenF_ID, PartidaF_ID, AF_ID, Estado, Tot);
                        break;
                    case "Nivel_Partida_Gen":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_Partida_Generica(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID,
                            UR_F_ID, PP_F_ID, CapituloF_ID, ConceptoF_ID, Partida_GenF_ID, PartidaF_ID, AF_ID, Estado, Tot);
                        break;
                    case "Nivel_Partida":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_Partida(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID,
                            UR_F_ID, PP_F_ID, CapituloF_ID, ConceptoF_ID, Partida_GenF_ID, PartidaF_ID, AF_ID, Estado, Tot);
                        break;
                    #endregion

                    #region(Detalles Ingresos)
                    case "Det_Mov_Ingresos":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Det_Mov_Ing(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID, PP_Ing_ID, Tipo_Det);
                        break;
                    case "Det_Recaudado_Ing":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Det_Recaudado_Ing(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID, PP_Ing_ID, Tipo_Det);
                        break;
                    #endregion

                    #region(Detalles Egresos)
                    case "Det_Mov_Egresos":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Det_Mov_Egr(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_ID, AF_ID, PP_ID, UR_ID,Capitulo_ID,Concepto_ID, Partida_Gen_ID, Partida_ID, Tipo_Det);
                        break;
                    case "Det_Mov_Con_Egr":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Det_Mov_Con_Egr(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_ID, AF_ID, PP_ID, UR_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID, Tipo_Det);
                        break;
                    #endregion

                    #region(Reporte)
                    case "Reporte":
                        Exixte_Reporte = true;
                        Json_Cadena = Obtener_Reporte(Anio, FF_ID, FF_Ing_ID, PP_ID, PP_F_ID, PP_Ing_ID, PP_IngF_ID, UR_ID, UR_F_ID,
                            AF_ID, Rubro_ID, Rubro_F_ID, Tipo_ID, Tipo_F_ID, Clase_ID, Clase_F_ID, Concepto_Ing_ID, Concepto_F_Ing_ID,
                            SubConcepto_ID, SubConcepto_F_ID, Capitulo_ID, CapituloF_ID, Concepto_ID, ConceptoF_ID, Partida_Gen_ID,
                            Partida_GenF_ID, Partida_ID, PartidaF_ID, Reporte, Tipo_Psp);
                        break;

                    case "Generar_Poliza":
                        Exixte_Reporte = true;
                        Json_Cadena = Obtener_Poliza(No_Poliza, Tipo_Poliza, Mes_Anio);
                        break;

                    case "Eliminar_Archivo":
                        Exixte_Reporte = true;

                        if (this.Request.QueryString["Ruta"] != null)
                        {
                            if (!String.IsNullOrEmpty(this.Request.QueryString["Ruta"].ToString().Trim()))
                            {
                                Json_Cadena = Eliminar_Archivo(this.Request.QueryString["Ruta"].ToString().Trim());
                            }
                        }
                        break;

                    case "Generar_Solicitud_Egr":
                        Exixte_Reporte = true;
                        Json_Cadena = Crear_Solicitud_Movimientos_Egresos(No_Modificacion, No_Solicitud, Tipo_Det, Anio);
                        break;
                    case "Generar_Solicitud_Ing":
                        Exixte_Reporte = true;
                        Json_Cadena = Crear_Solicitud_Movimientos_Ingresos(Anio, No_Modificacion, No_Solicitud);
                        break;
                    #endregion
                }
            }
        }
        if (!Exixte_Reporte)
        {
            Response.ContentType = "application/json";
            Response.Write(Json_Cadena);
            Response.Flush();
            Response.Close();
        }
        else 
        {
            Response.ContentType = "text/html";
            Response.Write(HttpUtility.HtmlEncode(Json_Cadena));
            Response.Flush();
            Response.Close();
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Obtener_Parametros
    ///DESCRIPCIÓN          : Metodo para obtener los parametros
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 25/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Obtener_Parametros() 
    {
        //Parametros de los combos
        String Anio = String.Empty;
        String Mes = String.Empty;
        String Fecha_Ini = String.Empty;
        String Fecha_Fin = String.Empty;
        String FF_ID = String.Empty;
        String FF_Ing_ID = String.Empty;
        String AF_ID = String.Empty;
        String UR_ID = String.Empty;
        String PP_ID = String.Empty;
        String PP_Ing_ID = String.Empty;
        String Capitulo_ID = String.Empty;
        String Concepto_ID = String.Empty;
        String Partida_Gen_ID = String.Empty;
        String Partida_ID = String.Empty;
        String Tipo_Psp = String.Empty;
        String Rubro_ID = String.Empty;
        String Tipo_ID = String.Empty;
        String Clase_ID = String.Empty;
        String Concepto_Ing_ID = String.Empty;
        String SubConcepto_ID = String.Empty;

        String UR_F_ID = String.Empty;
        String PP_F_ID = String.Empty;
        String PP_Ing_F_ID = String.Empty;
        String Capitulo_F_ID = String.Empty;
        String Concepto_F_ID = String.Empty;
        String Partida_Gen_F_ID = String.Empty;
        String Partida_F_ID = String.Empty;
        String Rubro_F_ID = String.Empty;
        String Tipo_F_ID = String.Empty;
        String Clase_F_ID = String.Empty;
        String Concepto_Ing_F_ID = String.Empty;
        String SubConcepto_F_ID = String.Empty;
        String Estado = String.Empty;
        String Tot = String.Empty;
        String Tipo_Det = String.Empty;

        String Busqueda = String.Empty;
        String Reporte = String.Empty;

        String No_Poliza = String.Empty;
        String Tipo_Poliza = String.Empty;
        String Mes_Anio = String.Empty;

        String No_Modificacion = String.Empty;
        String No_Solicitud = String.Empty;

        //obtenemos los parametros e los combos
        if (this.Request.QueryString["Tipo_Psp"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Tipo_Psp"].ToString().Trim()))
        {
            Tipo_Psp = this.Request.QueryString["Tipo_Psp"].ToString().Trim();
        }
        if (this.Request.QueryString["Anio"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Anio"].ToString().Trim()))
        {
            Anio = this.Request.QueryString["Anio"].ToString().Trim();
        }
        if (this.Request.QueryString["Mes"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Mes"].ToString().Trim()))
        {
            Mes = this.Request.QueryString["Mes"].ToString().Trim();
        }
        if (this.Request.QueryString["Fecha_Ini"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Fecha_Ini"].ToString().Trim()))
        {
            Fecha_Ini = this.Request.QueryString["Fecha_Ini"].ToString().Trim();
        }
        if (this.Request.QueryString["Fecha_Fin"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Fecha_Fin"].ToString().Trim()))
        {
            Fecha_Fin = this.Request.QueryString["Fecha_Fin"].ToString().Trim();
        }
        if (this.Request.QueryString["FF"] != null && !String.IsNullOrEmpty(this.Request.QueryString["FF"].ToString().Trim()))
        {
            FF_ID = this.Request.QueryString["FF"].ToString().Trim();
        }
        if (this.Request.QueryString["FF_Ing"] != null && !String.IsNullOrEmpty(this.Request.QueryString["FF_Ing"].ToString().Trim()))
        {
            FF_Ing_ID = this.Request.QueryString["FF_Ing"].ToString().Trim();
        }
        if (this.Request.QueryString["AF"] != null && !String.IsNullOrEmpty(this.Request.QueryString["AF"].ToString().Trim()))
        {
            AF_ID = this.Request.QueryString["AF"].ToString().Trim();
        }
        if (this.Request.QueryString["UR"] != null && !String.IsNullOrEmpty(this.Request.QueryString["UR"].ToString().Trim()))
        {
            UR_ID = this.Request.QueryString["UR"].ToString().Trim();
        }
        if (this.Request.QueryString["PP"] != null && !String.IsNullOrEmpty(this.Request.QueryString["PP"].ToString().Trim()))
        {
            PP_ID = this.Request.QueryString["PP"].ToString().Trim();
        }
        if (this.Request.QueryString["PP_Ing"] != null && !String.IsNullOrEmpty(this.Request.QueryString["PP_Ing"].ToString().Trim()))
        {
            PP_Ing_ID = this.Request.QueryString["PP_Ing"].ToString().Trim();
        }
        if (this.Request.QueryString["Cap"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Cap"].ToString().Trim()))
        {
            Capitulo_ID = this.Request.QueryString["Cap"].ToString().Trim();
        }
        if (this.Request.QueryString["Con"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Con"].ToString().Trim()))
        {
            Concepto_ID = this.Request.QueryString["Con"].ToString().Trim();
        }
        if (this.Request.QueryString["PG"] != null && !String.IsNullOrEmpty(this.Request.QueryString["PG"].ToString().Trim()))
        {
            Partida_Gen_ID = this.Request.QueryString["PG"].ToString().Trim();
        }
        if (this.Request.QueryString["P"] != null && !String.IsNullOrEmpty(this.Request.QueryString["P"].ToString().Trim()))
        {
            Partida_ID = this.Request.QueryString["P"].ToString().Trim();
        }
        if (this.Request.QueryString["R"] != null  && !String.IsNullOrEmpty(this.Request.QueryString["R"].ToString().Trim()))
        {
            Rubro_ID = this.Request.QueryString["R"].ToString().Trim();
        }
        if (this.Request.QueryString["T"] != null  && !String.IsNullOrEmpty(this.Request.QueryString["T"].ToString().Trim()))
        {
            Tipo_ID = this.Request.QueryString["T"].ToString().Trim();
        }
        if (this.Request.QueryString["Cl"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Cl"].ToString().Trim()))
        {
            Clase_ID = this.Request.QueryString["Cl"].ToString().Trim();
        }
        if (this.Request.QueryString["Con_Ing"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Con_Ing"].ToString().Trim()))
        {
            Concepto_Ing_ID = this.Request.QueryString["Con_Ing"].ToString().Trim();
        }
        if (this.Request.QueryString["SubCon"] != null && !String.IsNullOrEmpty(this.Request.QueryString["SubCon"].ToString().Trim()))
        {
            SubConcepto_ID = this.Request.QueryString["SubCon"].ToString().Trim();
        }
        if (this.Request["q"] != null  && !String.IsNullOrEmpty(this.Request["q"].ToString().Trim()))
        {
            Busqueda = this.Request["q"].ToString().Trim();
        }

   //obtenemos los parametros de los combos finales para obtener los rangos

        if (this.Request.QueryString["UR_F"] != null && !String.IsNullOrEmpty(this.Request.QueryString["UR_F"].ToString().Trim()))
        {
            UR_F_ID = this.Request.QueryString["UR_F"].ToString().Trim();
        }
        if (this.Request.QueryString["PP_F"] != null && !String.IsNullOrEmpty(this.Request.QueryString["PP_F"].ToString().Trim()))
        {
            PP_F_ID = this.Request.QueryString["PP_F"].ToString().Trim();
        }
        if (this.Request.QueryString["PP_Ing_F"] != null && !String.IsNullOrEmpty(this.Request.QueryString["PP_Ing_F"].ToString().Trim()))
        {
            PP_Ing_F_ID = this.Request.QueryString["PP_Ing_F"].ToString().Trim();
        }
        if (this.Request.QueryString["Cap_F"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Cap_F"].ToString().Trim()))
        {
            Capitulo_F_ID = this.Request.QueryString["Cap_F"].ToString().Trim();
        }
        if (this.Request.QueryString["Con_F"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Con_F"].ToString().Trim()))
        {
            Concepto_F_ID = this.Request.QueryString["Con_F"].ToString().Trim();
        }
        if (this.Request.QueryString["PG_F"] != null && !String.IsNullOrEmpty(this.Request.QueryString["PG_F"].ToString().Trim()))
        {
            Partida_Gen_F_ID = this.Request.QueryString["PG_F"].ToString().Trim();
        }
        if (this.Request.QueryString["P_F"] != null && !String.IsNullOrEmpty(this.Request.QueryString["P_F"].ToString().Trim()))
        {
            Partida_F_ID = this.Request.QueryString["P_F"].ToString().Trim();
        }
        if (this.Request.QueryString["R_F"] != null && !String.IsNullOrEmpty(this.Request.QueryString["R_F"].ToString().Trim()))
        {
            Rubro_F_ID = this.Request.QueryString["R_F"].ToString().Trim();
        }
        if (this.Request.QueryString["T_F"] != null && !String.IsNullOrEmpty(this.Request.QueryString["T_F"].ToString().Trim()))
        {
            Tipo_F_ID = this.Request.QueryString["T_F"].ToString().Trim();
        }
        if (this.Request.QueryString["Cl_F"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Cl_F"].ToString().Trim()))
        {
            Clase_F_ID = this.Request.QueryString["Cl_F"].ToString().Trim();
        }
        if (this.Request.QueryString["Con_Ing_F"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Con_Ing_F"].ToString().Trim()))
        {
            Concepto_Ing_F_ID = this.Request.QueryString["Con_Ing_F"].ToString().Trim();
        }
        if (this.Request.QueryString["SubCon_F"] != null && !String.IsNullOrEmpty(this.Request.QueryString["SubCon_F"].ToString().Trim()))
        {
            SubConcepto_F_ID = this.Request.QueryString["SubCon_F"].ToString().Trim();
        }


    //obtenemos los parametros del treegrid
        if (this.Request.QueryString["Param_FF_Ing"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_FF_Ing"].ToString().Trim()))
        {
            FF_Ing_ID = this.Request.QueryString["Param_FF_Ing"].ToString().Trim();
        }
        if (this.Request.QueryString["Param_PP_Ing"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_PP_Ing"].ToString().Trim()))
        {
            PP_Ing_ID = this.Request.QueryString["Param_PP_Ing"].ToString().Trim();
        }
        if (this.Request.QueryString["Param_Rubro"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_Rubro"].ToString().Trim()))
        {
            Rubro_ID = this.Request.QueryString["Param_Rubro"].ToString().Trim();
            Rubro_F_ID = String.Empty;
        }
        if (this.Request.QueryString["Param_Tipo"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_Tipo"].ToString().Trim()))
        {
            Tipo_ID= this.Request.QueryString["Param_Tipo"].ToString().Trim();
            Tipo_F_ID = String.Empty;
        }
        if (this.Request.QueryString["Param_Clase"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_Clase"].ToString().Trim()))
        {
            Clase_ID = this.Request.QueryString["Param_Clase"].ToString().Trim();
            Clase_F_ID = String.Empty;
        }
        if (this.Request.QueryString["Param_Concepto_Ing"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_Concepto_Ing"].ToString().Trim()))
        {
            Concepto_Ing_ID = this.Request.QueryString["Param_Concepto_Ing"].ToString().Trim();
            Concepto_Ing_F_ID = String.Empty;
        }
        if (this.Request.QueryString["Param_SubConcepto"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_SubConcepto"].ToString().Trim()))
        {
            SubConcepto_ID = this.Request.QueryString["Param_SubConcepto"].ToString().Trim();
            SubConcepto_F_ID = String.Empty;
        }

        //** EGRESOS**//
        if (this.Request.QueryString["Param_FF"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_FF"].ToString().Trim()))
        {
            FF_ID = this.Request.QueryString["Param_FF"].ToString().Trim();
        }
        if (this.Request.QueryString["Param_AF"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_AF"].ToString().Trim()))
        {
            AF_ID = this.Request.QueryString["Param_AF"].ToString().Trim();
        }
        if (this.Request.QueryString["Param_UR"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_UR"].ToString().Trim()))
        {
            UR_ID = this.Request.QueryString["Param_UR"].ToString().Trim();
            UR_F_ID = String.Empty;
        }
        if (this.Request.QueryString["Param_Progrm"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_Progrm"].ToString().Trim()))
        {
            PP_ID = this.Request.QueryString["Param_Progrm"].ToString().Trim();
            PP_F_ID = String.Empty;
        }
        if (this.Request.QueryString["Param_Cap"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_Cap"].ToString().Trim()))
        {
            Capitulo_ID = this.Request.QueryString["Param_Cap"].ToString().Trim();
            Capitulo_F_ID = String.Empty;
        }
        if (this.Request.QueryString["Param_Con"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_Con"].ToString().Trim()))
        {
            Concepto_ID = this.Request.QueryString["Param_Con"].ToString().Trim();
            Concepto_F_ID = String.Empty;
        }
        if (this.Request.QueryString["Param_Par_Gen"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_Par_Gen"].ToString().Trim()))
        {
            Partida_Gen_ID = this.Request.QueryString["Param_Par_Gen"].ToString().Trim();
            Partida_Gen_F_ID = String.Empty;
        }
        if (this.Request.QueryString["Param_Par"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_Par"].ToString().Trim()))
        {
            Partida_ID = this.Request.QueryString["Param_Par"].ToString().Trim();
            Partida_F_ID = String.Empty;
        }
        if (this.Request.QueryString["Estado"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Estado"].ToString().Trim()))
        {
            Estado = this.Request.QueryString["Estado"].ToString().Trim();
        }
        if (this.Request.QueryString["Tot"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Tot"].ToString().Trim()))
        {
            Tot = this.Request.QueryString["Tot"].ToString().Trim();
        }
        if (this.Request.QueryString["Tipo_Det"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Tipo_Det"].ToString().Trim()))
        {
            Tipo_Det = this.Request.QueryString["Tipo_Det"].ToString().Trim();
        }
        if (this.Request.QueryString["Reporte"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Reporte"].ToString().Trim()))
        {
            Reporte = this.Request.QueryString["Reporte"].ToString().Trim();
        }

        //datos de las polizas
        if (this.Request.QueryString["No_Poliza"] != null && !String.IsNullOrEmpty(this.Request.QueryString["No_Poliza"].ToString().Trim()))
        {
            No_Poliza = this.Request.QueryString["No_Poliza"].ToString().Trim();
        }
        if (this.Request.QueryString["Tipo_Poliza"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Tipo_Poliza"].ToString().Trim()))
        {
            Tipo_Poliza = this.Request.QueryString["Tipo_Poliza"].ToString().Trim();
        }
        if (this.Request.QueryString["Mes_Anio"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Mes_Anio"].ToString().Trim()))
        {
            Mes_Anio = this.Request.QueryString["Mes_Anio"].ToString().Trim();
        }

        if (this.Request.QueryString["No_Modificacion"] != null && !String.IsNullOrEmpty(this.Request.QueryString["No_Modificacion"].ToString().Trim()))
        {
            No_Modificacion = this.Request.QueryString["No_Modificacion"].ToString().Trim();
        }
        if (this.Request.QueryString["No_Solicitud"] != null && !String.IsNullOrEmpty(this.Request.QueryString["No_Solicitud"].ToString().Trim()))
        {
            No_Solicitud = this.Request.QueryString["No_Solicitud"].ToString().Trim();
        }

        Controlador_Inicio(Busqueda, Anio, Tipo_Psp, Mes, Fecha_Ini, Fecha_Fin,
            FF_ID, AF_ID, UR_ID, UR_F_ID, PP_ID, PP_F_ID, PP_Ing_ID, PP_Ing_F_ID,
            Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID,
            Capitulo_F_ID, Concepto_F_ID, Partida_Gen_F_ID, Partida_F_ID,
            Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID, FF_Ing_ID,
            Rubro_F_ID, Tipo_F_ID, Clase_F_ID, Concepto_Ing_F_ID, SubConcepto_F_ID,
            Estado, Tot, Tipo_Det, Reporte, No_Poliza, Tipo_Poliza, Mes_Anio, No_Modificacion, No_Solicitud);
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Obtener_Tipo_Usuario
    ///DESCRIPCIÓN          : Metodo para obtener el tipo de usuario
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 17/Agostp/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private String Obtener_Tipo_Usuario()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Mov_Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
        Cls_Ope_Psp_Presupuesto_Negocio Negocio_PSP = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
        String Administrador = String.Empty;
        DataTable Dt_Ramo33 = new DataTable();
        String Tipo_Usuario = String.Empty;
        String Gpo_Dep = "00000";
        DataTable Dt_Gpo_Dep = new DataTable();

        try
        {
            //verificar si el empleado que esta logueado tiene el rol de administrador del presupuesto municipal y estatal
            Negocio_PSP.P_Rol_ID = Cls_Sessiones.Rol_ID.Trim();
            Administrador = Negocio_PSP.Consultar_Rol();

            if (!String.IsNullOrEmpty(Administrador.Trim()))
            {
                if (Administrador.Trim().Equals("Presupuestos"))
                {
                    Tipo_Usuario = "Administrador";
                }
                else if (Administrador.Trim().Equals("Nomina")) 
                {
                    Tipo_Usuario = "Nomina";
                    //Tipo_Usuario = "Administrador";
                }
            }
            else
            {
                //verificamos si el usuario logueado es el administrador de ramo33
                Dt_Ramo33 = Mov_Negocio.Consultar_Usuario_Ramo33();
                if (Dt_Ramo33 != null && Dt_Ramo33.Rows.Count > 0)
                {
                    if (Dt_Ramo33.Rows[0][Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv].ToString().Trim().Equals(Cls_Sessiones.Empleado_ID.Trim())
                        || Dt_Ramo33.Rows[0][Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv_2].ToString().Trim().Equals(Cls_Sessiones.Empleado_ID.Trim()))
                    {
                        Tipo_Usuario = "Inversiones";
                    }
                    else
                    {
                        Tipo_Usuario = "Usuario";
                    }
                }
                else
                {
                    Tipo_Usuario = "Usuario";
                }

                if (Tipo_Usuario.Trim().Equals("Usuario"))
                {
                    //verificamos si no es algun coordinador administrativo de direccion
                    Negocio_PSP.P_Empleado_ID = Cls_Sessiones.Empleado_ID.Trim();
                    Dt_Gpo_Dep = Negocio_PSP.Obtener_Gpo_Dependencia();

                    if (Dt_Gpo_Dep != null && Dt_Gpo_Dep.Rows.Count > 0)
                    {
                        Gpo_Dep = Dt_Gpo_Dep.Rows[0][Cat_Dependencias.Campo_Grupo_Dependencia_ID].ToString().Trim();
                        Tipo_Usuario = "Coordinador";
                    }
                }
            }

            Tipo_Usuario = Tipo_Usuario.Trim() + "/" + Cls_Sessiones.Dependencia_ID_Empleado.Trim() + "/" + Gpo_Dep.Trim();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al obtener el tipo de usuario. Error[" + Ex.Message + "]");
        }
        return Tipo_Usuario;
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Obtener_Periodo
    ///DESCRIPCIÓN          : Metodo para obtener la fecha inicial y final de un mes
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 24/Agosto/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private String Obtener_Periodo(String Mes, String Anio, String Tipo)
    {
        String Fecha = String.Empty;
        int Dias = 1;
        try
        {
            if (Tipo.Trim().Equals("Inicial"))
            {
                Fecha = Mes + "/01/" + Anio; 
            }
            else 
            {
                Dias = DateTime.DaysInMonth(Convert.ToInt32(Anio), Convert.ToInt32(Mes));
                Fecha =  Mes + "/"+  Dias.ToString() + "/" + Anio; 
            }

            Fecha = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Fecha.Trim()));
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al obtener el tipo de usuario. Error[" + Ex.Message + "]");
        }
        return Fecha;
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Obtener_Det_Mov_Ing
    ///DESCRIPCIÓN          : Metodo para obtener el pie de pagina de un grid
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 25/Agosto/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private String Obtener_Pie_Pagina(String Json_Datos, DataTable Dt)
    {
        String Json = String.Empty;
        String Json_Total = String.Empty;

        try
        {
           Json_Total =  Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al obtener pie de pagina del grid. Error[" + Ex.Message + "]");
        }
        return Json;
    }

    #endregion

    #region METODOS COMBOS JSON
        #region (GENERALES)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Anios
            ///DESCRIPCIÓN          : Metodo para obtener los años presupuestados aprobados
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Anios(String Busqueda, String Tipo_Psp)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json_Anios = string.Empty; //variable donde almacenaremos el string con los datos
                DataTable Dt_Anios = new DataTable(); //tabla para guardar los datos de los años
                String Seleccione = "[{\"ANIO\":\"SELECCIONE\",\"ANIO_ID\":\"\", \"selected\":true}]"; //cadena inicial del combo
                Json_Anios = Seleccione; //asignamos el inicio de la cadena

                try
                {
                    Negocio.P_Busqueda = Busqueda; //parametro por si el combo trae una busqueda
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp; //parametro del tipo de presupuesto
                    Dt_Anios = Negocio.Consultar_Anios(); //consultamos los años de los presupuestos
                    Dt_Anios.TableName = "anios"; //asignamos el nombre a la tabla
                    if (Dt_Anios != null) //validamos que no venga vacia la tabla
                    {
                        if (Dt_Anios.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim())) //si la busqueda es vacia agregamos el seleccione al combo
                            {
                                //obtenemos la cadena Json y agregamos el texto de seleccione al combo
                                Json_Anios = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt_Anios);
                                Json_Anios = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json_Anios.Substring(1);
                            }
                            else
                            {
                                //solo regresamos el valor de la busqueda
                                Json_Anios = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt_Anios);
                            }
                        }
                    }
                    return Json_Anios; //retornamos la cadena Json de los años
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Anios Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_FF
            ///DESCRIPCIÓN          : Metodo para obtener las fuentes de financiamiento
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_FF(String Busqueda, String Anio, String Tipo_Psp, String FF, String UR, String PP,
                 String Cap, String Con, String PG, String P, String R, String T, String Cl, String Con_Ing, String SubCon)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json_FF = string.Empty;
                DataTable Dt_FF = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"FUENTE_FINANCIAMIENTO_ID\":\"\", \"selected\":true}]";
                Json_FF = Seleccione;
                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        Negocio.P_Dependencia_ID = Usuario[1].Trim();
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = "'PF12','PF13', 'PF11'";
                    }

                    Dt_FF = Negocio.Consultar_FF();

                    if (Dt_FF != null)
                    {
                        if (Dt_FF.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json_FF = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt_FF);
                                Json_FF = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json_FF.Substring(1);
                            }
                            else
                            {
                                Json_FF = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt_FF);
                            }
                        }
                    }
                    return Json_FF;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Fuentes de financiamiento Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Programas
            ///DESCRIPCIÓN          : Metodo para obtener los proyectos programas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Programas(String Busqueda, String Anio, String Tipo_Psp, String FF,
                String AF, String UR, String PP, String Cap, String Con, String PG, String P)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json = string.Empty;
                DataTable Dt = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"PROYECTO_PROGRAMA_ID\":\"\", \"selected\":true}]";
                Json = Seleccione;

                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Area_Funcional_ID = AF;
                    Negocio.P_Fte_Financiamiento_Ing = FF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        Negocio.P_Dependencia_ID = Usuario[1].Trim();
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = "'PF12','PF13', 'PF11'";
                    }

                    Dt = Negocio.Consultar_Programa();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                                Json = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json.Substring(1);
                            }
                            else
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener los proyectos programas. Error[" + ex.Message + "]");
                }
            }
        #endregion

        #region (INGRESOS)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_FF
            ///DESCRIPCIÓN          : Metodo para obtener las fuentes de financiamiento
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Rubros(String Busqueda, String Anio, String Tipo_Psp, String FF,
                String PP, String R, String T, String Cl, String Con_Ing, String SubCon)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json = string.Empty;
                DataTable Dt = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"RUBRO_ID\":\"\", \"selected\":true}]";
                Json = Seleccione;

                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento_Ing = FF;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;

                    Dt = Negocio.Consultar_Rubros();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                                Json = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json.Substring(1);
                            }
                            else
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener los rubros Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Tipos
            ///DESCRIPCIÓN          : Metodo para obtener los tipos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Tipos(String Busqueda, String Anio, String Tipo_Psp, String FF,
                String PP, String R, String T, String Cl, String Con_Ing, String SubCon)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json = string.Empty;
                DataTable Dt = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"TIPO_ID\":\"\", \"selected\":true}]";
                Json = Seleccione;

                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento_Ing = FF;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;

                    Dt = Negocio.Consultar_Tipos();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                                Json = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json.Substring(1);
                            }
                            else
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener los tipos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Clases
            ///DESCRIPCIÓN          : Metodo para obtener las Clases
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Clases(String Busqueda, String Anio, String Tipo_Psp, String FF,
                String PP, String R, String T, String Cl, String Con_Ing, String SubCon)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json = string.Empty;
                DataTable Dt = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"CLASE_IN_ID\":\"\", \"selected\":true}]";
                Json = Seleccione;

                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento_Ing = FF;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;

                    Dt = Negocio.Consultar_Clases();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                                Json = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json.Substring(1);
                            }
                            else
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener las clases Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Conceptos_Ing
            ///DESCRIPCIÓN          : Metodo para obtener los conceptos de ingresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Conceptos_Ing(String Busqueda, String Anio, String Tipo_Psp, String FF,
                String PP, String R, String T, String Cl, String Con_Ing, String SubCon)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json = string.Empty;
                DataTable Dt = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"CONCEPTO_ING_ID\":\"\", \"selected\":true}]";
                Json = Seleccione;

                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento_Ing = FF;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;

                    Dt = Negocio.Consultar_Conceptos_Ing();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                                Json = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json.Substring(1);
                            }
                            else
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener los datos de los conceptos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_SubConceptos
            ///DESCRIPCIÓN          : Metodo para obtener las subconceptos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_SubConceptos(String Busqueda, String Anio, String Tipo_Psp, String FF,
                String PP, String R, String T, String Cl, String Con_Ing, String SubCon)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json = string.Empty;
                DataTable Dt = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"SUBCONCEPTO_ING_ID\":\"\", \"selected\":true}]";
                Json = Seleccione;
                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento_Ing = FF;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;

                    Dt = Negocio.Consultar_SubConceptos();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                                Json = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json.Substring(1);
                            }
                            else
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener los datos de los subconcepts Error[" + ex.Message + "]");
                }
            }
        #endregion

        #region (EGRESOS)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_AF
            ///DESCRIPCIÓN          : Metodo para obtener las areas funcionales
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 17/Agosto/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_AF(String Busqueda, String Anio, String Tipo_Psp, String FF, String AF,
                String UR, String PP, String Cap, String Con, String PG, String P)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json_AF = string.Empty;
                DataTable Dt_AF = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"AREA_FUNCIONAL_ID\":\"\", \"selected\":true}]";
                Json_AF = Seleccione;

                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Area_Funcional_ID = AF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        Negocio.P_Dependencia_ID = Usuario[1].Trim();
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = "'PF12','PF13', 'PF11'";
                    }

                    Dt_AF = Negocio.Consultar_AF();

                    if (Dt_AF != null)
                    {
                        if (Dt_AF.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json_AF = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt_AF);
                                Json_AF = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json_AF.Substring(1);
                            }
                            else
                            {
                                Json_AF = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt_AF);
                            }
                        }
                    }
                    return Json_AF;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al obtener las areas funcionales Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_UR
            ///DESCRIPCIÓN          : Metodo para obtener las dependencias
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_UR(String Busqueda, String Anio, String Tipo_Psp, String FF,
                String AF, String UR, String PP, String Cap, String Con, String PG, String P)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json = string.Empty;
                DataTable Dt = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"DEPENDENCIA_ID\":\"\", \"selected\":true}]";
                Json = Seleccione;

                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Area_Funcional_ID = AF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        Negocio.P_Dependencia_ID = Usuario[1].Trim();
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = "'PF12','PF13', 'PF11'";
                    }

                    Dt = Negocio.Consultar_UR();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                                Json = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json.Substring(1);
                            }
                            else
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener las unidades responsables Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Capitulos
            ///DESCRIPCIÓN          : Metodo para obtener los capitulos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Capitulos(String Busqueda, String Anio, String Tipo_Psp, String FF,
                String AF, String UR, String PP, String Cap, String Con, String PG, String P)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json = string.Empty;
                DataTable Dt = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"CAPITULO_ID\":\"\", \"selected\":true}]";
                Json = Seleccione;

                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Area_Funcional_ID = AF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        Negocio.P_Dependencia_ID = Usuario[1].Trim();
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = "'PF12','PF13', 'PF11'";
                    }

                    Dt = Negocio.Consultar_Capitulos();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                                Json = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json.Substring(1);
                            }
                            else
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener los capitulos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Conceptos
            ///DESCRIPCIÓN          : Metodo para obtener los conceptos de egresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Conceptos(String Busqueda, String Anio, String Tipo_Psp, String FF,
                String AF, String UR, String PP, String Cap, String Con, String PG, String P)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json = string.Empty;
                DataTable Dt = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"CONCEPTO_ID\":\"\", \"selected\":true}]";
                Json = Seleccione;

                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Area_Funcional_ID = AF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        Negocio.P_Dependencia_ID = Usuario[1].Trim();
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = "'PF12','PF13', 'PF11'";
                    }

                    Dt = Negocio.Consultar_Conceptos();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                                Json = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json.Substring(1);
                            }
                            else
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener los conceptos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Partidas_Genericas
            ///DESCRIPCIÓN          : Metodo para obtener las partidas genericas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Partidas_Genericas(String Busqueda, String Anio, String Tipo_Psp, String FF,
                String AF, String UR, String PP, String Cap, String Con, String PG, String P)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json = string.Empty;
                DataTable Dt = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"PARTIDA_GENERICA_ID\":\"\", \"selected\":true}]";
                Json = Seleccione;

                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Area_Funcional_ID = AF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        Negocio.P_Dependencia_ID = Usuario[1].Trim();
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = "'PF12','PF13', 'PF11'";
                    }

                    Dt = Negocio.Consultar_Partidas_Gen();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                                Json = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json.Substring(1);
                            }
                            else
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener las partidas genericas Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Partidas
            ///DESCRIPCIÓN          : Metodo para obtener las partidas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Partidas(String Busqueda, String Anio, String Tipo_Psp, String FF,
                String AF, String UR, String PP, String Cap, String Con, String PG, String P)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json = string.Empty;
                DataTable Dt = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"PARTIDA_ID\":\"\", \"selected\":true}]";
                Json = Seleccione;

                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Area_Funcional_ID = AF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        Negocio.P_Dependencia_ID = Usuario[1].Trim();
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = "'PF12','PF13', 'PF11'";
                    }

                    Dt = Negocio.Consultar_Partidas();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                                Json = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json.Substring(1);
                            }
                            else
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener las partidas Error[" + ex.Message + "]");
                }
            }
        #endregion
    #endregion

    #region METODOS NIVELES JSON
        #region (INGRESOS)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_FF_Ing
            ///DESCRIPCIÓN          : Metodo para obtener las fuentes de financiamiento
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 22/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_FF_Ing(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
                String R, String T, String Cl, String Con_Ing, String SubCon, String PP, String PP_F,
                String R_F, String T_F, String Cl_F, String Con_Ing_F, String SubCon_F, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                String Json_Total = String.Empty;//variable para manejar la cadena json del total
                String Json_Tot = String.Empty; //variable para manejar la cadena json del total
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>(); //creamos una lista del objeto del nodo arbol
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid(); //Objeto de la clase de atributos
                // creamos un objeto de tipo JsonSerializerSettings para
                // serializar los datos que mostraremos en el nodo del árbol
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings(); 
                //controla cómo los valores nulos en. NET se manejan durante la serialización  
                //.aqui le estamos indicando que los pase por alto durante la serialización
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore; 
                
                try
                {   //obtenemos los parametros
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_ProgramaF_Ing_ID = PP_F;
                    Negocio.P_RubroF_ID = R_F;
                    Negocio.P_TipoF_ID = T_F;
                    Negocio.P_ClaseF_Ing_ID = Cl_F;
                    Negocio.P_ConceptoF_Ing_ID = Con_Ing_F;
                    Negocio.P_SubConceptoF_ID = SubCon_F;

                    Dt_Datos = Negocio.Consultar_Nivel_FF_Ing(); //obtenemos los datos del presupuesto de ingresos por fuente de financiamiento
                    if (!String.IsNullOrEmpty(Tot))
                    {   //obtenemos el total del presupuesto, para mostrarlo en el pie de pagina del treegrid
                        Json_Total = Obtener_Total_Ing(Dt_Datos);
                    }

                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {   //PASAMOS LOS DATOS CORRESPONDIENTES AL OBJETO DEL NODO ARBOL
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["ID"].ToString().Trim(); //ID DEL NODO
                                Nodo_Arbol.texto = Dr["CLAVE_NOMBRE"].ToString().Trim(); //NOMBRE DEL NODO
                                Nodo_Arbol.state = Estado; //ESTADO DEL NODO
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["ESTIMADO"]); //COLUMNAS DEL NODO
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["RECAUDADO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["DEV_REC"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["POR_RECAUDAR"]);
                                Nodo_Arbol.descripcion10 = String.Format("{0:n}", Dr["FF"]);
                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid(); //ATRIBUTOS DEL NODO PARA IDENTIFICARLO
                                Atributos.valor1 = "Nivel_FF_Ing";
                                Atributos.valor2 = "";
                                Atributos.valor3 = "";
                                Atributos.valor4 = "";
                                Atributos.valor5 = "";
                                Atributos.valor6 = "";
                                Atributos.valor7 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor8 = "";

                                Nodo_Arbol.attributes = Atributos; //AGREGAMOS LOS ATRIBUTOS AL OBJETO DEL NODO ARBOL
                                Lista_Nodo_Arbol.Add(Nodo_Arbol); //AGREGAMOS A LA LISTA DEL NODO, EL OBJETO DEL NODO ARBOL
                            }

                            //UNA VEZ DE OBTENER LA LISTA CON TODOS LOS NODOS DEL ARBOL, PASAMOS A SERIALIZAR LA LISTA
                            //PARA GENERAR LA CADENA JSON CON LA QUE LLENAREMOS EL TREEGRID
                            //Formatting.Indented ES PARA INDICAR QUE QUEREMOS DEJAR UNA SANGRIA ENTRE NODOS HIJOS
                            Json_Datos = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            //ACOMODAMOS LA CADENA OBTENIDA PARA AGREGAR EL PIE DE PAGINA DEL TREEGRID CON EL TOTAL
                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt_Datos.Rows.Count + ", \"rows\":";
                                Json_Tot += Json_Datos;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json_Datos = Json_Tot;
                            }
                        }
                    }

                    return Json_Datos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Nivel_Fuente_Financiamiento_Ingresos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_PP_Ing
            ///DESCRIPCIÓN          : Metodo para obtener los proyectos programas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 22/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_PP_Ing(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
                String R, String T, String Cl, String Con_Ing, String SubCon, String PP, String PP_F,
                String R_F, String T_F, String Cl_F, String Con_Ing_F, String SubCon_F, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                String Json_Total = String.Empty;
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_ProgramaF_Ing_ID = PP_F;
                    Negocio.P_RubroF_ID = R_F;
                    Negocio.P_TipoF_ID = T_F;
                    Negocio.P_ClaseF_Ing_ID = Cl_F;
                    Negocio.P_ConceptoF_Ing_ID = Con_Ing_F;
                    Negocio.P_SubConceptoF_ID = SubCon_F;

                    Dt_Datos = Negocio.Consultar_Nivel_PP_Ing();
                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Ing(Dt_Datos);
                    }

                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["ID"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["CLAVE_NOMBRE"].ToString().Trim();
                                Nodo_Arbol.state = Estado;
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["ESTIMADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["RECAUDADO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["DEV_REC"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["POR_RECAUDAR"]);
                                Nodo_Arbol.descripcion10 = String.Format("{0:n}", Dr["FF"]);
                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_PP_Ing";
                                Atributos.valor2 = "";
                                Atributos.valor3 = "";
                                Atributos.valor4 = "";
                                Atributos.valor5 = "";
                                Atributos.valor6 = "";
                                Atributos.valor7 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor8 = Dr["PP_ID"].ToString().Trim();

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Datos = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt_Datos.Rows.Count + ", \"rows\":";
                                Json_Tot += Json_Datos;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json_Datos = Json_Tot;
                            }
                        }
                    }

                    return Json_Datos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Nivel_Programas_Ingresos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_Rubro
            ///DESCRIPCIÓN          : Metodo para obtener los rubros
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 27/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_Rubro(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
                String R, String T, String Cl, String Con_Ing, String SubCon, String PP, String PP_F,
                String R_F, String T_F, String Cl_F, String Con_Ing_F, String SubCon_F, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                String Json_Total = String.Empty;
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_ProgramaF_Ing_ID = PP_F;
                    Negocio.P_RubroF_ID = R_F;
                    Negocio.P_TipoF_ID = T_F;
                    Negocio.P_ClaseF_Ing_ID = Cl_F;
                    Negocio.P_ConceptoF_Ing_ID = Con_Ing_F;
                    Negocio.P_SubConceptoF_ID = SubCon_F;

                    Dt_Datos = Negocio.Consultar_Nivel_Rubros();
                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Ing(Dt_Datos);
                    }

                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["ID"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["CLAVE_NOMBRE"].ToString().Trim();
                                Nodo_Arbol.state = Estado;
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["ESTIMADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["RECAUDADO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["DEV_REC"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["POR_RECAUDAR"]);
                                Nodo_Arbol.descripcion10 = String.Format("{0:n}", Dr["FF"]);
                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_Rubro";
                                Atributos.valor2 = Dr["RUBRO_ID"].ToString().Trim();
                                Atributos.valor3 = "";
                                Atributos.valor4 = "";
                                Atributos.valor5 = "";
                                Atributos.valor6 = "";
                                Atributos.valor7 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor8 = Dr["PP_ID"].ToString().Trim();

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Datos = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt_Datos.Rows.Count + ", \"rows\":";
                                Json_Tot += Json_Datos;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json_Datos = Json_Tot;
                            }
                        }
                    }

                    return Json_Datos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Nivel_Rubros Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_Tipos
            ///DESCRIPCIÓN          : Metodo para obtener los tipos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 27/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_Tipos(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
                String R, String T, String Cl, String Con_Ing, String SubCon, String PP, String PP_F,
                String R_F, String T_F, String Cl_F, String Con_Ing_F, String SubCon_F, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                String Json_Total = String.Empty;
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_ProgramaF_Ing_ID = PP_F;
                    Negocio.P_RubroF_ID = R_F;
                    Negocio.P_TipoF_ID = T_F;
                    Negocio.P_ClaseF_Ing_ID = Cl_F;
                    Negocio.P_ConceptoF_Ing_ID = Con_Ing_F;
                    Negocio.P_SubConceptoF_ID = SubCon_F;
                    Dt_Datos = Negocio.Consultar_Nivel_Tipos();
                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Ing(Dt_Datos);
                    }

                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["ID"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["CLAVE_NOMBRE"].ToString().Trim();
                                Nodo_Arbol.state = Estado;
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["ESTIMADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["RECAUDADO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["DEV_REC"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["POR_RECAUDAR"]);
                                Nodo_Arbol.descripcion10 = String.Format("{0:n}", Dr["FF"]);

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_Tipo";
                                Atributos.valor2 = Dr["RUBRO_ID"].ToString().Trim();
                                Atributos.valor3 = Dr["TIPO_ID"].ToString().Trim();
                                Atributos.valor4 = "";
                                Atributos.valor5 = "";
                                Atributos.valor6 = "";
                                Atributos.valor7 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor8 = Dr["PP_ID"].ToString().Trim();

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Datos = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt_Datos.Rows.Count + ", \"rows\":";
                                Json_Tot += Json_Datos;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json_Datos = Json_Tot;
                            }
                        }
                    }

                    return Json_Datos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Nivel_Tipo Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_Clases
            ///DESCRIPCIÓN          : Metodo para obtener las clases
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 27/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_Clases(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
                String R, String T, String Cl, String Con_Ing, String SubCon, String PP, String PP_F,
                String R_F, String T_F, String Cl_F, String Con_Ing_F, String SubCon_F, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                String Json_Total = String.Empty;
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_ProgramaF_Ing_ID = PP_F;
                    Negocio.P_RubroF_ID = R_F;
                    Negocio.P_TipoF_ID = T_F;
                    Negocio.P_ClaseF_Ing_ID = Cl_F;
                    Negocio.P_ConceptoF_Ing_ID = Con_Ing_F;
                    Negocio.P_SubConceptoF_ID = SubCon_F;
                    Dt_Datos = Negocio.Consultar_Nivel_Clases();

                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Ing(Dt_Datos);
                    }

                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["ID"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["CLAVE_NOMBRE"].ToString().Trim();
                                Nodo_Arbol.state = Estado;
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["ESTIMADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["RECAUDADO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["DEV_REC"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["POR_RECAUDAR"]);
                                Nodo_Arbol.descripcion10 = String.Format("{0:n}", Dr["FF"]);

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_Clase";
                                Atributos.valor2 = Dr["RUBRO_ID"].ToString().Trim();
                                Atributos.valor3 = Dr["TIPO_ID"].ToString().Trim();
                                Atributos.valor4 = Dr["CLASE_ING_ID"].ToString().Trim();
                                Atributos.valor5 = "";
                                Atributos.valor6 = "";
                                Atributos.valor7 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor8 = Dr["PP_ID"].ToString().Trim();

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Datos = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt_Datos.Rows.Count + ", \"rows\":";
                                Json_Tot += Json_Datos;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json_Datos = Json_Tot;
                            }
                        }
                    }

                    return Json_Datos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Nivel_Clases Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_Conceptos_Ing
            ///DESCRIPCIÓN          : Metodo para obtener los conceptos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 27/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_Conceptos_Ing(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
                String R, String T, String Cl, String Con_Ing, String SubCon, String PP, String PP_F,
                String R_F, String T_F, String Cl_F, String Con_Ing_F, String SubCon_F, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                String Json_Total = String.Empty;
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                DataTable Dt_Det = new DataTable();
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_ProgramaF_Ing_ID = PP_F;
                    Negocio.P_RubroF_ID = R_F;
                    Negocio.P_TipoF_ID = T_F;
                    Negocio.P_ClaseF_Ing_ID = Cl_F;
                    Negocio.P_ConceptoF_Ing_ID = Con_Ing_F;
                    Negocio.P_SubConceptoF_ID = SubCon_F;

                    Dt_Datos = Negocio.Consultar_Nivel_Conceptos_Ing();
                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Ing(Dt_Datos);
                    }

                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {
                                //obtenemos si el concepto tiene subconceptos
                                Negocio.P_Busqueda = String.Empty;
                                Negocio.P_Anio = Anio;
                                Negocio.P_Tipo_Presupuesto = "ING";
                                Negocio.P_Periodo_Inicial = String.Empty;
                                Negocio.P_Fecha_Inicial = String.Empty;
                                Negocio.P_Fecha_Final = String.Empty;
                                Negocio.P_Fte_Financiamiento_Ing = FF;
                                Negocio.P_Programa_Ing_ID = PP;
                                Negocio.P_Rubro_ID = R;
                                Negocio.P_Tipo_ID = T;
                                Negocio.P_Clase_Ing_ID = Cl;
                                Negocio.P_Concepto_Ing_ID = Dr["CONCEPTO_ING_ID"].ToString().Trim();
                                Negocio.P_SubConcepto_ID = String.Empty;

                                Dt_Det = Negocio.Consultar_SubConceptos();

                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["ID"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["CLAVE_NOMBRE"].ToString().Trim();
                                if (Dt_Det != null && Dt_Det.Rows.Count > 0)
                                {
                                    Nodo_Arbol.state = Estado;
                                }
                                else
                                {
                                    Nodo_Arbol.state = "opened";
                                }
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["ESTIMADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["RECAUDADO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["DEV_REC"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["POR_RECAUDAR"]);
                                Nodo_Arbol.descripcion10 = String.Format("{0:n}", Dr["FF"]);

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_Concepto_Ing";
                                Atributos.valor2 = Dr["RUBRO_ID"].ToString().Trim();
                                Atributos.valor3 = Dr["TIPO_ID"].ToString().Trim();
                                Atributos.valor4 = Dr["CLASE_ING_ID"].ToString().Trim();
                                Atributos.valor5 = Dr["CONCEPTO_ING_ID"].ToString().Trim();
                                Atributos.valor6 = "";
                                Atributos.valor7 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor8 = Dr["PP_ID"].ToString().Trim();

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Datos = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt_Datos.Rows.Count + ", \"rows\":";
                                Json_Tot += Json_Datos;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json_Datos = Json_Tot;
                            }
                        }
                    }

                    return Json_Datos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Nivel_Conceptos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_Conceptos
            ///DESCRIPCIÓN          : Metodo para obtener los conceptos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 27/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_SubConceptos(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
                String R, String T, String Cl, String Con_Ing, String SubCon, String PP, String PP_F,
                String R_F, String T_F, String Cl_F, String Con_Ing_F, String SubCon_F, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                String Json_Total = String.Empty;
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_ProgramaF_Ing_ID = PP_F;
                    Negocio.P_RubroF_ID = R_F;
                    Negocio.P_TipoF_ID = T_F;
                    Negocio.P_ClaseF_Ing_ID = Cl_F;
                    Negocio.P_ConceptoF_Ing_ID = Con_Ing_F;
                    Negocio.P_SubConceptoF_ID = SubCon_F;
                    Dt_Datos = Negocio.Consultar_Nivel_SubConceptos();

                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Ing(Dt_Datos);
                    }

                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["ID"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["CLAVE_NOMBRE"].ToString().Trim();
                                Nodo_Arbol.state = "opened";
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["ESTIMADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["RECAUDADO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["DEV_REC"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["POR_RECAUDAR"]);
                                Nodo_Arbol.descripcion10 = Dr["FF"].ToString().Trim();

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_SubConcepto";
                                Atributos.valor2 = Dr["RUBRO_ID"].ToString().Trim();
                                Atributos.valor3 = Dr["TIPO_ID"].ToString().Trim();
                                Atributos.valor4 = Dr["CLASE_ING_ID"].ToString().Trim();
                                Atributos.valor5 = Dr["CONCEPTO_ING_ID"].ToString().Trim();
                                Atributos.valor6 = Dr["SUBCONCEPTO_ING_ID"].ToString().Trim();
                                Atributos.valor7 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor8 = Dr["PP_ID"].ToString().Trim();

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Datos = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt_Datos.Rows.Count + ", \"rows\":";
                                Json_Tot += Json_Datos;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json_Datos = Json_Tot;
                            }
                        }
                    }

                    return Json_Datos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Nivel_Conceptos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Total_Egr
            ///DESCRIPCIÓN          : Metodo para obtener el total
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Total_Ing(DataTable Dt_Datos)
            {
                Double Aprobado = 0.00;
                Double Ampliacion = 0.00;
                Double Reduccion = 0.00;
                Double Modificado = 0.00;
                Double Devengado = 0.00;
                Double Pagado = 0.00;
                Double Dev_Pag = 0.00;
                Double Disponible = 0.00;
                Double Comprometido = 0.00;
                String Json_Total = string.Empty;
                Json_Total = "{[]}";
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                //String Json = String.Empty;

                try
                {
                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {
                                Aprobado += Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                Ampliacion += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                Reduccion += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                Modificado += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                Devengado += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                Pagado += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                Dev_Pag += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());
                                Comprometido += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                Disponible += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                            }

                            Nodo_Arbol = new Cls_Nodo_Arbol();

                            Nodo_Arbol.id = "";
                            Nodo_Arbol.texto = "Total";
                            Nodo_Arbol.descripcion1 = String.Format("{0:n}", Aprobado);
                            Nodo_Arbol.descripcion2 = String.Format("{0:n}", Ampliacion);
                            Nodo_Arbol.descripcion3 = String.Format("{0:n}", Reduccion);
                            Nodo_Arbol.descripcion4 = String.Format("{0:n}", Modificado);
                            Nodo_Arbol.descripcion5 = String.Format("{0:n}", Devengado);
                            Nodo_Arbol.descripcion6 = String.Format("{0:n}", Pagado);
                            Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dev_Pag);
                            Nodo_Arbol.descripcion8 = String.Format("{0:n}", Comprometido);
                            Nodo_Arbol.descripcion9 = String.Format("{0:n}", Disponible);
                            Nodo_Arbol.descripcion10 = "";

                            //Agregamos los atributos
                            Atributos = new Cls_Atributos_TreeGrid();
                            Atributos.valor1 = "Total";
                            Atributos.valor2 = "";
                            Atributos.valor3 = "";
                            Atributos.valor4 = "";
                            Atributos.valor5 = "";
                            Atributos.valor6 = "";
                            Atributos.valor7 = "";
                            Atributos.valor8 = "";

                            Nodo_Arbol.attributes = Atributos;
                            Lista_Nodo_Arbol.Add(Nodo_Arbol);


                            Json_Total = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);
                        }
                    }
                    return Json_Total;
                }
                catch (Exception Ex)
                {
                    throw new Exception(" Error al tratar de Obtener_Total Error[" + Ex.Message + "]");
                }
            }
        #endregion

        #region (EGRESOS)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_FF
            ///DESCRIPCIÓN          : Metodo para obtener las fuentes de financiamiento de egresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_FF(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF, String UR, String PP,
                String Cap, String Con, String PG, String P, String UR_F, String PP_F, String Cap_F, String Con_F, String PG_F, String P_F,
                String AF, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json = "{[]}"; //aki almacenaremos el json de los datos
                DataTable Dt = new DataTable(); // dt donde guardaremos los datos
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Total = String.Empty;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Periodo_Inicial = Mes;
                    Negocio.P_Fecha_Inicial = Fecha_Ini;
                    Negocio.P_Fecha_Final = Fecha_Fin;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_DependenciaF_ID = UR_F;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_ProgramaF_ID = PP_F;
                    Negocio.P_CapituloF_ID = Cap_F;
                    Negocio.P_ConceptoF_ID = Con_F;
                    Negocio.P_Partida_GenericaF_ID = PG_F;
                    Negocio.P_PartidaF_ID = P_F;
                    Negocio.P_Area_Funcional_ID = AF;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        Negocio.P_Dependencia_ID = Usuario[1].Trim();
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = "'PF12','PF13', 'PF11'";
                    }

                    Dt = Negocio.Consultar_Nivel_FF();

                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Egr(Dt);
                    }

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["id"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["nombre"].ToString().Trim();
                                Nodo_Arbol.state = Estado;
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["APROBADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["PAGADO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["DEV_PAG"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["DISPONIBLE"]);
                                Nodo_Arbol.descripcion10 = Dr["FF"].ToString().Trim();
                                Nodo_Arbol.descripcion11 = Dr["UR"].ToString().Trim();
                                Nodo_Arbol.descripcion12 = Dr["PP"].ToString().Trim();

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_FF";
                                Atributos.valor2 = "";
                                Atributos.valor3 = "";
                                Atributos.valor4 = "";
                                Atributos.valor5 = "";
                                Atributos.valor6 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor7 = "";
                                Atributos.valor8 = "";
                                Atributos.valor9 = "";

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt.Rows.Count + ", \"rows\":";
                                Json_Tot += Json;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json = Json_Tot;
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_fuente de financiamiento Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_AF
            ///DESCRIPCIÓN          : Metodo para obtener las areas funcionales de egresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_AF(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF, String UR, String PP,
                String Cap, String Con, String PG, String P, String UR_F, String PP_F, String Cap_F, String Con_F, String PG_F, String P_F,
                String AF, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json = "{[]}"; //aki almacenaremos el json de los datos
                DataTable Dt = new DataTable(); // dt donde guardaremos los datos
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Total = String.Empty;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Periodo_Inicial = Mes;
                    Negocio.P_Fecha_Inicial = Fecha_Ini;
                    Negocio.P_Fecha_Final = Fecha_Fin;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_DependenciaF_ID = UR_F;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_ProgramaF_ID = PP_F;
                    Negocio.P_CapituloF_ID = Cap_F;
                    Negocio.P_ConceptoF_ID = Con_F;
                    Negocio.P_Partida_GenericaF_ID = PG_F;
                    Negocio.P_PartidaF_ID = P_F;
                    Negocio.P_Area_Funcional_ID = AF;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        Negocio.P_Dependencia_ID = Usuario[1].Trim();
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = "'PF12','PF13', 'PF11'";
                    }

                    Dt = Negocio.Consultar_Nivel_AF();

                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Egr(Dt);
                    }

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["id"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["nombre"].ToString().Trim();
                                Nodo_Arbol.state = Estado;
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["APROBADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["PAGADO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["DEV_PAG"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["DISPONIBLE"]);
                                Nodo_Arbol.descripcion10 = Dr["FF"].ToString().Trim();
                                Nodo_Arbol.descripcion11 = Dr["UR"].ToString().Trim();
                                Nodo_Arbol.descripcion12 = Dr["PP"].ToString().Trim();

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_AF";
                                Atributos.valor2 = "";
                                Atributos.valor3 = "";
                                Atributos.valor4 = "";
                                Atributos.valor5 = "";
                                Atributos.valor6 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor7 = Dr["AF_ID"].ToString().Trim();
                                Atributos.valor8 = "";
                                Atributos.valor9 = "";

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt.Rows.Count + ", \"rows\":";
                                Json_Tot += Json;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json = Json_Tot;
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener las areas funcionales Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_PP
            ///DESCRIPCIÓN          : Metodo para obtener los progamas de egresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_PP(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF, String UR, String PP,
                String Cap, String Con, String PG, String P, String UR_F, String PP_F, String Cap_F, String Con_F, String PG_F, String P_F,
                String AF, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json = "{[]}"; //aki almacenaremos el json de los datos
                DataTable Dt = new DataTable(); // dt donde guardaremos los datos
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Total = String.Empty;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Periodo_Inicial = Mes;
                    Negocio.P_Fecha_Inicial = Fecha_Ini;
                    Negocio.P_Fecha_Final = Fecha_Fin;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_DependenciaF_ID = UR_F;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_ProgramaF_ID = PP_F;
                    Negocio.P_CapituloF_ID = Cap_F;
                    Negocio.P_ConceptoF_ID = Con_F;
                    Negocio.P_Partida_GenericaF_ID = PG_F;
                    Negocio.P_PartidaF_ID = P_F;
                    Negocio.P_Area_Funcional_ID = AF;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        Negocio.P_Dependencia_ID = Usuario[1].Trim();
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = "'PF12','PF13', 'PF11'";
                    }

                    Dt = Negocio.Consultar_Nivel_PP();

                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Egr(Dt);
                    }

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["id"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["nombre"].ToString().Trim();
                                Nodo_Arbol.state = Estado;
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["APROBADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["PAGADO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["DEV_PAG"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["DISPONIBLE"]);
                                Nodo_Arbol.descripcion10 = Dr["FF"].ToString().Trim();
                                Nodo_Arbol.descripcion11 = Dr["UR"].ToString().Trim();
                                Nodo_Arbol.descripcion12 = Dr["PP"].ToString().Trim();

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_PP";
                                Atributos.valor2 = "";
                                Atributos.valor3 = "";
                                Atributos.valor4 = "";
                                Atributos.valor5 = "";
                                Atributos.valor6 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor7 = Dr["AF_ID"].ToString().Trim();
                                Atributos.valor8 = Dr["PP_ID"].ToString().Trim();
                                Atributos.valor9 = "";

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt.Rows.Count + ", \"rows\":";
                                Json_Tot += Json;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json = Json_Tot;
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener los programas Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_UR
            ///DESCRIPCIÓN          : Metodo para obtener las unidades responsables de egresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_UR(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF, String UR, String PP,
                String Cap, String Con, String PG, String P, String UR_F, String PP_F, String Cap_F, String Con_F, String PG_F, String P_F,
                String AF, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json = "{[]}"; //aki almacenaremos el json de los datos
                DataTable Dt = new DataTable(); // dt donde guardaremos los datos
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Total = String.Empty;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Periodo_Inicial = Mes;
                    Negocio.P_Fecha_Inicial = Fecha_Ini;
                    Negocio.P_Fecha_Final = Fecha_Fin;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_DependenciaF_ID = UR_F;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_ProgramaF_ID = PP_F;
                    Negocio.P_CapituloF_ID = Cap_F;
                    Negocio.P_ConceptoF_ID = Con_F;
                    Negocio.P_Partida_GenericaF_ID = PG_F;
                    Negocio.P_PartidaF_ID = P_F;
                    Negocio.P_Area_Funcional_ID = AF;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        Negocio.P_Dependencia_ID = Usuario[1].Trim();
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = "'PF12','PF13', 'PF11'";
                    }

                    Dt = Negocio.Consultar_Nivel_UR();

                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Egr(Dt);
                    }

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["id"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["nombre"].ToString().Trim();
                                Nodo_Arbol.state = Estado;
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["APROBADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["PAGADO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["DEV_PAG"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["DISPONIBLE"]);
                                Nodo_Arbol.descripcion10 = Dr["FF"].ToString().Trim();
                                Nodo_Arbol.descripcion11 = Dr["UR"].ToString().Trim();
                                Nodo_Arbol.descripcion12 = Dr["PP"].ToString().Trim();

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_UR";
                                Atributos.valor2 = "";
                                Atributos.valor3 = "";
                                Atributos.valor4 = "";
                                Atributos.valor5 = "";
                                Atributos.valor6 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor7 = Dr["AF_ID"].ToString().Trim();
                                Atributos.valor8 = Dr["PP_ID"].ToString().Trim();
                                Atributos.valor9 = Dr["UR_ID"].ToString().Trim();

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt.Rows.Count + ", \"rows\":";
                                Json_Tot += Json;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json = Json_Tot;
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener  las unidades responsables Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_Capitulos
            ///DESCRIPCIÓN          : Metodo para obtener los capitulos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_Capitulos(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF, String UR, String PP,
                String Cap, String Con, String PG, String P, String UR_F, String PP_F, String Cap_F, String Con_F, String PG_F, String P_F,
                String AF, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json_Capitulos = "{[]}"; //aki almacenaremos el json de los datos
                DataTable Dt_Capitulos = new DataTable(); // dt donde guardaremos los datos
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Total = String.Empty;
                String Json = string.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Periodo_Inicial = Mes;
                    Negocio.P_Fecha_Inicial = Fecha_Ini;
                    Negocio.P_Fecha_Final = Fecha_Fin;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_DependenciaF_ID = UR_F;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_ProgramaF_ID = PP_F;
                    Negocio.P_CapituloF_ID = Cap_F;
                    Negocio.P_ConceptoF_ID = Con_F;
                    Negocio.P_Partida_GenericaF_ID = PG_F;
                    Negocio.P_PartidaF_ID = P_F;

                    Negocio.P_Area_Funcional_ID = AF;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        Negocio.P_Dependencia_ID = Usuario[1].Trim();
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = "'PF12','PF13', 'PF11'";
                    }

                    Dt_Capitulos = Negocio.Consultar_Nivel_Capitulo();

                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Egr(Dt_Capitulos);
                    }

                    if (Dt_Capitulos != null)
                    {
                        if (Dt_Capitulos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Capitulos.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["id"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["nombre"].ToString().Trim();
                                Nodo_Arbol.state = Estado;
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["APROBADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["PAGADO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["DEV_PAG"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["DISPONIBLE"]);
                                Nodo_Arbol.descripcion10 = Dr["FF"].ToString().Trim();
                                Nodo_Arbol.descripcion11 = Dr["UR"].ToString().Trim();
                                Nodo_Arbol.descripcion12 = Dr["PP"].ToString().Trim();

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_Capitulos";
                                Atributos.valor2 = Dr["Capitulo_id"].ToString().Trim();
                                Atributos.valor3 = "";
                                Atributos.valor4 = "";
                                Atributos.valor5 = "";
                                Atributos.valor6 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor7 = Dr["AF_ID"].ToString().Trim();
                                Atributos.valor8 = Dr["PP_ID"].ToString().Trim();
                                Atributos.valor9 = Dr["UR_ID"].ToString().Trim();

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Capitulos = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json = "{\"total\": " + Dt_Capitulos.Rows.Count + ", \"rows\":";
                                Json += Json_Capitulos;
                                Json += ", \"footer\": " + Json_Total + "}";
                            }
                            else
                            {
                                Json = Json_Capitulos;
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Capitulos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_Conceptos
            ///DESCRIPCIÓN          : Metodo para obtener los conceptos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_Conceptos(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF, String UR, String PP,
                String Cap, String Con, String PG, String P, String UR_F, String PP_F, String Cap_F, String Con_F, String PG_F, String P_F,
                String AF, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json_Conceptos = "{[]}"; //aki almacenaremos el json de los datos
                DataTable Dt_Conceptos = new DataTable(); // dt donde guardaremos los datos
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Total = String.Empty;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Periodo_Inicial = Mes;
                    Negocio.P_Fecha_Inicial = Fecha_Ini;
                    Negocio.P_Fecha_Final = Fecha_Fin;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_DependenciaF_ID = UR_F;
                    Negocio.P_ProgramaF_ID = PP_F;
                    Negocio.P_CapituloF_ID = Cap_F;
                    Negocio.P_ConceptoF_ID = Con_F;
                    Negocio.P_Partida_GenericaF_ID = PG_F;
                    Negocio.P_PartidaF_ID = P_F;
                    Negocio.P_Area_Funcional_ID = AF;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        Negocio.P_Dependencia_ID = Usuario[1].Trim();
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = "'PF12','PF13', 'PF11'";
                    }

                    Dt_Conceptos = Negocio.Consultar_Nivel_Concepto();

                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Egr(Dt_Conceptos);
                    }

                    if (Dt_Conceptos != null)
                    {
                        if (Dt_Conceptos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Conceptos.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["id"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["nombre"].ToString().Trim();
                                Nodo_Arbol.state = Estado;
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["APROBADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["PAGADO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["DEV_PAG"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["DISPONIBLE"]);
                                Nodo_Arbol.descripcion10 = Dr["FF"].ToString().Trim();
                                Nodo_Arbol.descripcion11 = Dr["UR"].ToString().Trim();
                                Nodo_Arbol.descripcion12 = Dr["PP"].ToString().Trim();

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_Concepto";
                                Atributos.valor2 = Dr["Capitulo_id"].ToString().Trim();
                                Atributos.valor3 = Dr["Concepto_id"].ToString().Trim();
                                Atributos.valor4 = "";
                                Atributos.valor5 = "";
                                Atributos.valor6 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor7 = Dr["AF_ID"].ToString().Trim();
                                Atributos.valor8 = Dr["PP_ID"].ToString().Trim();
                                Atributos.valor9 = Dr["UR_ID"].ToString().Trim();

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Conceptos = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);


                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt_Conceptos.Rows.Count + ", \"rows\":";
                                Json_Tot += Json_Conceptos;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json_Conceptos = Json_Tot;
                            }
                        }
                    }
                    return Json_Conceptos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Conceptos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_Partida_Generica
            ///DESCRIPCIÓN          : Metodo para obtener las partidas genericas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_Partida_Generica(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF, String UR,
                String PP, String Cap, String Con, String PG, String P, String UR_F, String PP_F, String Cap_F, String Con_F, String PG_F,
                String P_F, String AF, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json_Partida_Generica = "{[]}"; //aki almacenaremos el json de las Partidas Genericas
                DataTable Dt_Partida_Generica = new DataTable(); // dt donde guardaremos las Partidas Genericas
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Total = String.Empty;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Periodo_Inicial = Mes;
                    Negocio.P_Fecha_Inicial = Fecha_Ini;
                    Negocio.P_Fecha_Final = Fecha_Fin;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_DependenciaF_ID = UR_F;
                    Negocio.P_ProgramaF_ID = PP_F;
                    Negocio.P_CapituloF_ID = Cap_F;
                    Negocio.P_ConceptoF_ID = Con_F;
                    Negocio.P_Partida_GenericaF_ID = PG_F;
                    Negocio.P_PartidaF_ID = P_F;
                    Negocio.P_Area_Funcional_ID = AF;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        Negocio.P_Dependencia_ID = Usuario[1].Trim();
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = "'PF12','PF13', 'PF11'";
                    }

                    Dt_Partida_Generica = Negocio.Consultar_Nivel_Partida_Gen();

                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Egr(Dt_Partida_Generica);
                    }

                    if (Dt_Partida_Generica != null)
                    {
                        if (Dt_Partida_Generica.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Partida_Generica.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["id"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["nombre"].ToString().Trim();
                                Nodo_Arbol.state = Estado;
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["APROBADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["PAGADO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["DEV_PAG"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["DISPONIBLE"]);
                                Nodo_Arbol.descripcion10 = Dr["FF"].ToString().Trim();
                                Nodo_Arbol.descripcion11 = Dr["UR"].ToString().Trim();
                                Nodo_Arbol.descripcion12 = Dr["PP"].ToString().Trim();

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_Partida_Gen";
                                Atributos.valor2 = Dr["Capitulo_id"].ToString().Trim();
                                Atributos.valor3 = Dr["Concepto_id"].ToString().Trim();
                                Atributos.valor4 = Dr["Partida_Generica_id"].ToString().Trim();
                                Atributos.valor5 = "";
                                Atributos.valor6 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor7 = Dr["AF_ID"].ToString().Trim();
                                Atributos.valor8 = Dr["PP_ID"].ToString().Trim();
                                Atributos.valor9 = Dr["UR_ID"].ToString().Trim();

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Partida_Generica = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt_Partida_Generica.Rows.Count + ", \"rows\":";
                                Json_Tot += Json_Partida_Generica;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json_Partida_Generica = Json_Tot;
                            }
                        }
                    }
                    return Json_Partida_Generica;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Partida_Generica Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Partida
            ///DESCRIPCIÓN          : Metodo para obtener las partidas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_Partida(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF, String UR, String PP,
                String Cap, String Con, String PG, String P, String UR_F, String PP_F, String Cap_F, String Con_F, String PG_F, String P_F,
                String AF, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json_Partida = "{[]}"; //aki almacenaremos el json de las partidas
                DataTable Dt_Partida = new DataTable(); // dt donde guardaremos las partidas
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Total = String.Empty;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Periodo_Inicial = Mes;
                    Negocio.P_Fecha_Inicial = Fecha_Ini;
                    Negocio.P_Fecha_Final = Fecha_Fin;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_DependenciaF_ID = UR_F;
                    Negocio.P_ProgramaF_ID = PP_F;
                    Negocio.P_CapituloF_ID = Cap_F;
                    Negocio.P_ConceptoF_ID = Con_F;
                    Negocio.P_Partida_GenericaF_ID = PG_F;
                    Negocio.P_PartidaF_ID = P_F;
                    Negocio.P_Area_Funcional_ID = AF;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        Negocio.P_Dependencia_ID = Usuario[1].Trim();
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = "'PF12','PF13', 'PF11'";
                    }

                    Dt_Partida = Negocio.Consultar_Nivel_Partidas();
                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Egr(Dt_Partida);
                    }

                    if (Dt_Partida != null)
                    {
                        if (Dt_Partida.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Partida.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["id"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["nombre"].ToString().Trim();
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["APROBADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["PAGADO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["DEV_PAG"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["DISPONIBLE"]);
                                Nodo_Arbol.descripcion10 = Dr["FF"].ToString().Trim();
                                Nodo_Arbol.descripcion11 = Dr["UR"].ToString().Trim();
                                Nodo_Arbol.descripcion12 = Dr["PP"].ToString().Trim();

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_Partida";
                                Atributos.valor2 = Dr["Capitulo_id"].ToString().Trim();
                                Atributos.valor3 = Dr["Concepto_id"].ToString().Trim();
                                Atributos.valor4 = Dr["Partida_Generica_id"].ToString().Trim();
                                Atributos.valor5 = Dr["Partida_id"].ToString().Trim();
                                Atributos.valor6 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor7 = Dr["AF_ID"].ToString().Trim();
                                Atributos.valor8 = Dr["PP_ID"].ToString().Trim();
                                Atributos.valor9 = Dr["UR_ID"].ToString().Trim();

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Partida = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt_Partida.Rows.Count + ", \"rows\":";
                                Json_Tot += Json_Partida;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json_Partida = Json_Tot;
                            }
                        }
                    }
                    return Json_Partida;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Partida Error[" + ex.Message + "]");
                }
            }

            //********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Total_Egr
            ///DESCRIPCIÓN          : Metodo para obtener el total
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Total_Egr(DataTable Dt_Datos)
            {

                Double Aprobado = 0.00;
                Double Ampliacion = 0.00;
                Double Reduccion = 0.00;
                Double Modificado = 0.00;
                Double Devengado = 0.00;
                Double Pagado = 0.00;
                Double Dev_Pag = 0.00;
                Double Disponible = 0.00;
                Double Comprometido = 0.00;
                String Json_Total = string.Empty;
                Json_Total = "{[]}";
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;

                try
                {
                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            
                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {
                                Aprobado += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                Ampliacion += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                Reduccion += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                Modificado += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                Devengado += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                Pagado += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                Dev_Pag += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                Disponible += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                Comprometido += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                            }

                            Nodo_Arbol = new Cls_Nodo_Arbol();

                            Nodo_Arbol.id = "";
                            Nodo_Arbol.texto = "Total";
                            Nodo_Arbol.descripcion1 = String.Format("{0:n}", Aprobado);
                            Nodo_Arbol.descripcion2 = String.Format("{0:n}", Ampliacion);
                            Nodo_Arbol.descripcion3 = String.Format("{0:n}", Reduccion);
                            Nodo_Arbol.descripcion4 = String.Format("{0:n}", Modificado);
                            Nodo_Arbol.descripcion5 = String.Format("{0:n}", Devengado);
                            Nodo_Arbol.descripcion6 = String.Format("{0:n}", Pagado);
                            Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dev_Pag);
                            Nodo_Arbol.descripcion8 = String.Format("{0:n}", Comprometido);
                            Nodo_Arbol.descripcion9 = String.Format("{0:n}", Disponible);
                            Nodo_Arbol.descripcion10 = "";
                            Nodo_Arbol.descripcion11 = "";
                            Nodo_Arbol.descripcion12 = "";

                            //Agregamos los atributos
                            Atributos = new Cls_Atributos_TreeGrid();
                            Atributos.valor1 = "Total";
                            Atributos.valor2 = "";
                            Atributos.valor3 = "";
                            Atributos.valor4 = "";
                            Atributos.valor5 = "";
                            Atributos.valor6 = "";
                            Atributos.valor7 = "";
                            Atributos.valor8 = "";
                            Atributos.valor9 = "";

                            Nodo_Arbol.attributes = Atributos;
                            Lista_Nodo_Arbol.Add(Nodo_Arbol);

                            Json_Total = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);
                        }
                    }
                    return Json_Total;
                }
                catch (Exception Ex)
                {
                    throw new Exception(" Error al tratar de Obtener_Total Error[" + Ex.Message + "]");
                }
            }
        #endregion
    #endregion

    #region METODOS DETALLES
        #region (INGRESOS)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Det_Mov_Ing
            ///DESCRIPCIÓN          : Metodo para obtener los detalles de los movimientos de ingresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 24/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Det_Mov_Ing(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
                String R, String T, String Cl, String Con_Ing, String SubCon, String PP, String Tipo_Det)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                String Json = ""; //aki almacenaremos el json de las fuentes de financiamiento
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                Double Importe = 0.00;
                DataTable Dt = new DataTable();
                DataRow Fila;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    if (!String.IsNullOrEmpty(Fecha_Ini.Trim()))
                    {
                        Negocio.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Fecha_Ini.Trim()));
                    }
                    else 
                    {
                        Negocio.P_Fecha_Inicial = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Fecha_Fin.Trim()))
                    {
                        Negocio.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Fecha_Fin.Trim()));
                    }
                    else 
                    {
                        Negocio.P_Fecha_Final = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Mes.Trim()))
                    {
                        Negocio.P_Periodo_Inicial = Obtener_Periodo(Mes, Anio, "Inicial");
                        Negocio.P_Periodo_Final = Obtener_Periodo(Mes, Anio, "Final");
                    }
                    else 
                    {
                        Negocio.P_Periodo_Inicial = String.Empty;
                        Negocio.P_Periodo_Final = String.Empty;
                    }
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_Tipo_Detalle = Tipo_Det;

                    Dt_Datos = Negocio.Consultar_Det_Modificacion_Ing();


                    if (Dt_Datos != null && Dt_Datos.Rows.Count > 0)
                    {
                        Dt_Datos.Columns.Add("NO_SOLICITUD_ING");
                        Dt_Datos.Columns.Add("IMPORTE");
                        Dt_Datos.Columns.Add("FECHA_INICIO");
                        Dt_Datos.Columns.Add("FECHA_AUTORIZO");
                        foreach (DataRow Dr in Dt_Datos.Rows)
                        {
                            Dr["NO_SOLICITUD_ING"] = "T" + String.Format("{0:00}", Dr["NO_MODIFICACION"]) + "-" + Dr["NO_MOVIMIENTO"].ToString().Trim() + "-" +Dr["ANIO"].ToString().Trim();
                            Dr["IMPORTE"] = String.Format("{0:n}", Dr["IMPORTE_TOTAL"]);
                            Dr["FECHA_INICIO"] = String.Format("{0:dd/MMM/yyyy}", Dr["FECHA_CREO"]);
                            Dr["FECHA_AUTORIZO"] = String.Format("{0:dd/MMM/yyyy}", Dr["FECHA_MODIFICO"]);
                            Importe += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_TOTAL"].ToString().Trim()) ? "0" : Dr["IMPORTE_TOTAL"]);
                        }
                        Dt_Datos.TableName = "rows";
                        Json_Datos = Ayudante_JQuery.Crear_Tabla_Formato_JSON_DataGrid(Dt_Datos, Dt_Datos.Rows.Count);

                        //creamos el pie de pagina del  grid
                        Dt.Columns.Add("NO_MOVIMIENTO");
                        Dt.Columns.Add("TIPO_MOVIMIENTO");
                        Dt.Columns.Add("NO_SOLICITUD_ING");
                        Dt.Columns.Add("JUSTIFICACION");
                        Dt.Columns.Add("FECHA_INICIO");
                        Dt.Columns.Add("USUARIO_CREO");
                        Dt.Columns.Add("FECHA_AUTORIZO");
                        Dt.Columns.Add("USUARIO_MODIFICO");
                        Dt.Columns.Add("CONCEPTO");
                        Dt.Columns.Add("IMPORTE");
                        Dt.Columns.Add("NO_MODIFICACION");
                        Dt.Columns.Add("ANIO");

                        Fila = Dt.NewRow();
                        Fila["NO_MOVIMIENTO"] = 0;
                        Fila["TIPO_MOVIMIENTO"] = " ";
                        Fila["NO_SOLICITUD_ING"] = " ";
                        Fila["JUSTIFICACION"] = " ";
                        Fila["FECHA_INICIO"] = " ";
                        Fila["USUARIO_CREO"] = " ";
                        Fila["FECHA_AUTORIZO"] = " ";
                        Fila["USUARIO_MODIFICO"] = " ";
                        Fila["CONCEPTO"] = "Total";
                        Fila["IMPORTE"] = String.Format("{0:n}", Importe);
                        Fila["NO_MODIFICACION"] = 0;
                        Fila["ANIO"] = 0;
                        Dt.Rows.Add(Fila);

                        Json_Tot = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);

                        //juntamos las cadenas para formar el footer
                        if (!String.IsNullOrEmpty(Json_Datos) && !String.IsNullOrEmpty(Json_Tot))
                        {
                            Json = Json_Datos.Substring(0, Json_Datos.Length - 1);
                            Json += ", \"footer\": " + Json_Tot + "}";
                        }
                        else 
                        {
                            Json = Json_Datos;
                        }
                    }

                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al obtener los detalles de ingresos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Det_Recaudado_Ing
            ///DESCRIPCIÓN          : Metodo para obtener los detalles de los movimientos de ingresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 28/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Det_Recaudado_Ing(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
                String R, String T, String Cl, String Con_Ing, String SubCon, String PP, String Tipo_Det)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                String Json = ""; //aki almacenaremos el json de las fuentes de financiamiento
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                Double Importe = 0.00;
                DataTable Dt = new DataTable();
                DataRow Fila;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    if (!String.IsNullOrEmpty(Fecha_Ini.Trim()))
                    {
                        Negocio.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Fecha_Ini.Trim()));
                    }
                    else
                    {
                        Negocio.P_Fecha_Inicial = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Fecha_Fin.Trim()))
                    {
                        Negocio.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Fecha_Fin.Trim()));
                    }
                    else
                    {
                        Negocio.P_Fecha_Final = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Mes.Trim()))
                    {
                        Negocio.P_Periodo_Inicial = Obtener_Periodo(Mes, Anio, "Inicial");
                        Negocio.P_Periodo_Final = Obtener_Periodo(Mes, Anio, "Final");
                    }
                    else
                    {
                        Negocio.P_Periodo_Inicial = String.Empty;
                        Negocio.P_Periodo_Final = String.Empty;
                    }
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_Tipo_Detalle = Tipo_Det;

                    Dt_Datos = Negocio.Consultar_Det_Recaudado_Ing();


                    if (Dt_Datos != null && Dt_Datos.Rows.Count > 0)
                    {
                        Dt_Datos.Columns.Add("IMPORTE_TOTAL");
                        Dt_Datos.Columns.Add("FECHA_INICIO");
                        foreach (DataRow Dr in Dt_Datos.Rows)
                        {
                            Dr["IMPORTE_TOTAL"] = String.Format("{0:n}", Dr["IMPORTE"]);
                            Dr["FECHA_INICIO"] = String.Format("{0:dd/MMM/yyyy}", Dr["FECHA"]);
                            Importe += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE"].ToString().Trim()) ? "0" : Dr["IMPORTE"]);
                        }
                        Dt_Datos.TableName = "rows";
                        Json_Datos = Ayudante_JQuery.Crear_Tabla_Formato_JSON_DataGrid(Dt_Datos, Dt_Datos.Rows.Count);

                        //creamos el pie de pagina del  grid
                        Dt.Columns.Add("NO_MOVIMIENTO");
                        Dt.Columns.Add("NO_POLIZA");
                        Dt.Columns.Add("TIPO_POLIZA_ID");
                        Dt.Columns.Add("MES_ANO");
                        Dt.Columns.Add("FECHA_INICIO");
                        Dt.Columns.Add("DESCRIPCION");
                        Dt.Columns.Add("REFERENCIA");
                        Dt.Columns.Add("IMPORTE_TOTAL");
                        Dt.Columns.Add("CONCEPTO");
                        Dt.Columns.Add("USUARIO_CREO");

                        Fila = Dt.NewRow();
                        Fila["NO_MOVIMIENTO"] = 0;
                        Fila["NO_POLIZA"] = " ";
                        Fila["TIPO_POLIZA_ID"] = " ";
                        Fila["MES_ANO"] = " ";
                        Fila["FECHA_INICIO"] = " ";
                        Fila["DESCRIPCION"] = " ";
                        Fila["REFERENCIA"] = " ";
                        Fila["IMPORTE_TOTAL"] = String.Format("{0:n}", Importe);
                        Fila["CONCEPTO"] = "Total";
                        Fila["USUARIO_CREO"] = " ";
                       
                        Dt.Rows.Add(Fila);

                        Json_Tot = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);

                        //juntamos las cadenas para formar el footer
                        if (!String.IsNullOrEmpty(Json_Datos) && !String.IsNullOrEmpty(Json_Tot))
                        {
                            Json = Json_Datos.Substring(0, Json_Datos.Length - 1);
                            Json += ", \"footer\": " + Json_Tot + "}";
                        }
                        else
                        {
                            Json = Json_Datos;
                        }
                    }

                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al obtener los detalles de ingresos Error[" + ex.Message + "]");
                }
            }
        #endregion

        #region (EGRESOS)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Det_Mov_Ing
            ///DESCRIPCIÓN          : Metodo para obtener los detalles de los movimientos de egresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 27/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Det_Mov_Egr(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
                String AF, String PP, String UR, String Cap, String Con, String PG, String P, String Tipo_Det)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                String Json = ""; //aki almacenaremos el json de las fuentes de financiamiento
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                Double Importe = 0.00;
                DataTable Dt = new DataTable();
                DataRow Fila;
                String Json_Tot = String.Empty;
                DataTable Dt_Temp = new DataTable();

                try
                {
                    Negocio.P_Anio = Anio;
                    if (!String.IsNullOrEmpty(Fecha_Ini.Trim()))
                    {
                        Negocio.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Fecha_Ini.Trim()));
                    }
                    else
                    {
                        Negocio.P_Fecha_Inicial = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Fecha_Fin.Trim()))
                    {
                        Negocio.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Fecha_Fin.Trim()));
                    }
                    else
                    {
                        Negocio.P_Fecha_Final = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Mes.Trim()))
                    {
                        Negocio.P_Periodo_Inicial = Obtener_Periodo(Mes, Anio, "Inicial");
                        Negocio.P_Periodo_Final = Obtener_Periodo(Mes, Anio, "Final");
                    }
                    else
                    {
                        Negocio.P_Periodo_Inicial = String.Empty;
                        Negocio.P_Periodo_Final = String.Empty;
                    }
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Area_Funcional_ID = AF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_Tipo_Detalle = Tipo_Det;

                    Dt_Datos = Negocio.Consultar_Det_Modificacion_Egr();

                    if (Dt_Datos != null && Dt_Datos.Rows.Count > 0)
                    {
                        if (!String.IsNullOrEmpty(Tipo_Det.Trim()) && Tipo_Det.Trim().Equals("AMPLIACION"))
                        {
                            Dt_Temp.Columns.Add("NO_MODIFICACION");
                            Dt_Temp.Columns.Add("NO_MOVIMIENTO");
                            Dt_Temp.Columns.Add("ANIO");
                            Dt_Temp.Columns.Add("IMPORTE_TOTAL");
                            Dt_Temp.Columns.Add("TIPO_OPERACION");
                            Dt_Temp.Columns.Add("JUSTIFICACION");
                            Dt_Temp.Columns.Add("FECHA_CREO");
                            Dt_Temp.Columns.Add("USUARIO_CREO");
                            Dt_Temp.Columns.Add("FECHA_MODIFICO");
                            Dt_Temp.Columns.Add("USUARIO_MODIFICO");
                            Dt_Temp.Columns.Add("PARTIDA");
                            Dt_Temp.Columns.Add("NO_SOLICITUD_EGR");
                            Dt_Temp.Columns.Add("IMPORTE");
                            Dt_Temp.Columns.Add("FECHA_INICIO");
                            Dt_Temp.Columns.Add("FECHA_AUTORIZO");
                        }
                        else 
                        {
                            Dt_Datos.Columns.Add("NO_SOLICITUD_EGR");
                            Dt_Datos.Columns.Add("IMPORTE");
                            Dt_Datos.Columns.Add("FECHA_INICIO");
                            Dt_Datos.Columns.Add("FECHA_AUTORIZO");
                        }

                        foreach (DataRow Dr in Dt_Datos.Rows)
                        {
                            if (!String.IsNullOrEmpty(Tipo_Det.Trim()) && Tipo_Det.Trim().Equals("AMPLIACION"))
                            {
                                if ((Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Origen") && Dr["TIPO_OPERACION"].ToString().Trim().Equals("AMPLIACION"))
                                    || (Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Destino") && Dr["TIPO_OPERACION"].ToString().Trim().Equals("TRASPASO"))) 
                                {
                                    Fila = Dt_Temp.NewRow();
                                    Fila["NO_MODIFICACION"] = Dr["NO_MODIFICACION"].ToString().Trim();
                                    Fila["NO_MOVIMIENTO"] = Dr["NO_MOVIMIENTO"].ToString().Trim();
                                    Fila["ANIO"] = Dr["ANIO"].ToString().Trim();
                                    Fila["IMPORTE_TOTAL"] = Dr["IMPORTE_TOTAL"].ToString().Trim();
                                    Fila["TIPO_OPERACION"] = Dr["TIPO_OPERACION"].ToString().Trim();
                                    Fila["JUSTIFICACION"] = Dr["JUSTIFICACION"].ToString().Trim();
                                    Fila["FECHA_CREO"] = Dr["FECHA_CREO"].ToString().Trim();
                                    Fila["USUARIO_CREO"] = Dr["USUARIO_CREO"].ToString().Trim();
                                    Fila["FECHA_MODIFICO"] = Dr["FECHA_MODIFICO"].ToString().Trim();
                                    Fila["USUARIO_MODIFICO"] = Dr["USUARIO_MODIFICO"].ToString().Trim();
                                    Fila["PARTIDA"] = Dr["PARTIDA"].ToString().Trim();
                                    Fila["NO_SOLICITUD_EGR"] = "T" + String.Format("{0:00}", Dr["NO_MODIFICACION"]) + "-" + Dr["NO_MOVIMIENTO"].ToString().Trim() + "-" + Dr["ANIO"].ToString().Trim();
                                    Fila["IMPORTE"] = String.Format("{0:n}", Dr["IMPORTE_TOTAL"]);
                                    Fila["FECHA_INICIO"] = String.Format("{0:dd/MMM/yyyy}", Dr["FECHA_CREO"]);
                                    Fila["FECHA_AUTORIZO"] = String.Format("{0:dd/MMM/yyyy}", Dr["FECHA_MODIFICO"]);
                                    Dt_Temp.Rows.Add(Fila);

                                    Importe += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_TOTAL"].ToString().Trim()) ? "0" : Dr["IMPORTE_TOTAL"]);
                                }
                            }
                            else 
                            {
                                Dr["NO_SOLICITUD_EGR"] = "T" + String.Format("{0:00}", Dr["NO_MODIFICACION"]) + "-" + Dr["NO_MOVIMIENTO"].ToString().Trim() + "-" + Dr["ANIO"].ToString().Trim();
                                Dr["IMPORTE"] = String.Format("{0:n}", Dr["IMPORTE_TOTAL"]);
                                Dr["FECHA_INICIO"] = String.Format("{0:dd/MMM/yyyy}", Dr["FECHA_CREO"]);
                                Dr["FECHA_AUTORIZO"] = String.Format("{0:dd/MMM/yyyy}", Dr["FECHA_MODIFICO"]);
                                Importe += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_TOTAL"].ToString().Trim()) ? "0" : Dr["IMPORTE_TOTAL"]);
                            }
                        }

                        if (!String.IsNullOrEmpty(Tipo_Det.Trim()))
                        {
                            if (Tipo_Det.Trim() != "REDUCCION")
                            {
                                Dt_Datos = Dt_Temp; 
                            }
                        }

                        Dt_Datos.TableName = "rows";
                        Json_Datos = Ayudante_JQuery.Crear_Tabla_Formato_JSON_DataGrid(Dt_Datos, Dt_Datos.Rows.Count);

                        //creamos el pie de pagina del  grid
                        Dt.Columns.Add("NO_MOVIMIENTO");
                        Dt.Columns.Add("TIPO_OPERACION");
                        Dt.Columns.Add("NO_SOLICITUD_EGR");
                        Dt.Columns.Add("JUSTIFICACION");
                        Dt.Columns.Add("FECHA_INICIO");
                        Dt.Columns.Add("USUARIO_CREO");
                        Dt.Columns.Add("FECHA_AUTORIZO");
                        Dt.Columns.Add("USUARIO_MODIFICO");
                        Dt.Columns.Add("PARTIDA");
                        Dt.Columns.Add("IMPORTE");
                        Dt.Columns.Add("NO_MODIFICACION");
                        Dt.Columns.Add("ANIO");

                        Fila = Dt.NewRow();
                        Fila["NO_MOVIMIENTO"] = 0;
                        Fila["TIPO_OPERACION"] = " ";
                        Fila["NO_SOLICITUD_EGR"] = " ";
                        Fila["JUSTIFICACION"] = " ";
                        Fila["FECHA_INICIO"] = " ";
                        Fila["USUARIO_CREO"] = " ";
                        Fila["FECHA_AUTORIZO"] = " ";
                        Fila["USUARIO_MODIFICO"] = " ";
                        Fila["PARTIDA"] = "Total";
                        Fila["IMPORTE"] = String.Format("{0:n}", Importe);
                        Fila["NO_MODIFICACION"] = 0;
                        Fila["ANIO"] = 0;
                        Dt.Rows.Add(Fila);

                        Json_Tot = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);

                        //juntamos las cadenas para formar el footer
                        if (!String.IsNullOrEmpty(Json_Datos) && !String.IsNullOrEmpty(Json_Tot))
                        {
                            Json = Json_Datos.Substring(0, Json_Datos.Length - 1);
                            Json += ", \"footer\": " + Json_Tot + "}";
                        }
                        else
                        {
                            Json = Json_Datos;
                        }
                    }

                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al obtener los detalles de egresos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Det_Mov_Con_Egr
            ///DESCRIPCIÓN          : Metodo para obtener los detalles del devengado, pagado y comprometido de egresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 29/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Det_Mov_Con_Egr(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
                String AF, String PP, String UR, String Cap, String Con, String PG, String P, String Tipo_Det)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                String Json = ""; //aki almacenaremos el json de las fuentes de financiamiento
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                Double Importe = 0.00;
                DataTable Dt = new DataTable();
                DataRow Fila;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    if (!String.IsNullOrEmpty(Fecha_Ini.Trim()))
                    {
                        Negocio.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Fecha_Ini.Trim()));
                    }
                    else
                    {
                        Negocio.P_Fecha_Inicial = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Fecha_Fin.Trim()))
                    {
                        Negocio.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Fecha_Fin.Trim()));
                    }
                    else
                    {
                        Negocio.P_Fecha_Final = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Mes.Trim()))
                    {
                        Negocio.P_Periodo_Inicial = Obtener_Periodo(Mes, Anio, "Inicial");
                        Negocio.P_Periodo_Final = Obtener_Periodo(Mes, Anio, "Final");
                    }
                    else
                    {
                        Negocio.P_Periodo_Inicial = String.Empty;
                        Negocio.P_Periodo_Final = String.Empty;
                    }
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Area_Funcional_ID = AF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_Tipo_Detalle = Tipo_Det;

                    Dt_Datos = Negocio.Consultar_Det_Movimiento_Contable_Egr();

                    if (Dt_Datos != null && Dt_Datos.Rows.Count > 0)
                    {
                        Dt_Datos.Columns.Add("IMPORTE");
                        Dt_Datos.Columns.Add("FECHA");
                        foreach (DataRow Dr in Dt_Datos.Rows)
                        {
                            Dr["IMPORTE"] = String.Format("{0:n}", Dr["IMPORTE_MOV"]);
                            Dr["FECHA"] = String.Format("{0:dd/MMM/yyyy}", Dr["FECHA_MOV"]);
                            Importe += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_MOV"].ToString().Trim()) ? "0" : Dr["IMPORTE_MOV"]);
                        }
                        Dt_Datos.TableName = "rows";
                        Json_Datos = Ayudante_JQuery.Crear_Tabla_Formato_JSON_DataGrid(Dt_Datos, Dt_Datos.Rows.Count);

                        //creamos el pie de pagina del  grid
                        Dt.Columns.Add("NO_RESERVA");
                        Dt.Columns.Add("NO_POLIZA");
                        Dt.Columns.Add("TIPO_POLIZA_ID");
                        Dt.Columns.Add("MES_ANO");
                        Dt.Columns.Add("IMPORTE");
                        Dt.Columns.Add("FECHA");
                        Dt.Columns.Add("USUARIO_MOV");
                        Dt.Columns.Add("ANIO");
                        Dt.Columns.Add("CONCEPTO");
                        Dt.Columns.Add("BENEFICIARIO");
                        Dt.Columns.Add("TIPO_SOLICITUD");
                        Dt.Columns.Add("DEPENDENCIA");
                        Dt.Columns.Add("PROGRAMA");
                        Dt.Columns.Add("FUENTE");
                        Dt.Columns.Add("PARTIDA");


                        Fila = Dt.NewRow();
                        Fila["NO_RESERVA"] = " ";
                        Fila["NO_POLIZA"] = " ";
                        Fila["TIPO_POLIZA_ID"] = " ";
                        Fila["MES_ANO"] = " ";
                        Fila["IMPORTE"] = String.Format("{0:n}", Importe);
                        Fila["FECHA"] = " ";
                        Fila["USUARIO_MOV"] = " ";
                        Fila["ANIO"] = " ";
                        Fila["CONCEPTO"] = " ";
                        Fila["BENEFICIARIO"] = " ";
                        Fila["TIPO_SOLICITUD"] = " ";
                        Fila["DEPENDENCIA"] = " ";
                        Fila["PROGRAMA"] = " ";
                        Fila["FUENTE"] = " ";
                        Fila["PARTIDA"] = "Total";
                        
                        Dt.Rows.Add(Fila);

                        Json_Tot = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);

                        //juntamos las cadenas para formar el footer
                        if (!String.IsNullOrEmpty(Json_Datos) && !String.IsNullOrEmpty(Json_Tot))
                        {
                            Json = Json_Datos.Substring(0, Json_Datos.Length - 1);
                            Json += ", \"footer\": " + Json_Tot + "}";
                        }
                        else
                        {
                            Json = Json_Datos;
                        }
                    }

                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al obtener los detalles de egresos Error[" + ex.Message + "]");
                }
            }
        #endregion
    #endregion

    #region METODOS REPORTE
        #region (Reportes Psp Egresos Ingresos)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Reporte
            ///DESCRIPCIÓN          : Metodo para obtener los datos para los reportes
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 30/Agosto/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Reporte(String Anio, String FF, String FF_Ing, String PP, String PP_F, String PP_Ing, String PP_Ing_F,
                String UR, String UR_F, String AF, String R, String R_F, String T, String T_F, String Cl, String Cl_F, String Con_Ing,
                String Con_Ing_F, String SubCon, String SubCon_F, String Cap, String Cap_F, String Con, String Con_F, String PG, String PG_F,
                String P, String P_F, String Reporte, String Tipo_PSP)
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                String Json_Datos = "{[]}";
                DataTable Dt_Ing = new DataTable();
                DataTable Dt_Egr = new DataTable();
                DataTable Dt_Ing_Completo = new DataTable();
                DataTable Dt_Egr_Completo = new DataTable();
                String Json_Tot = String.Empty;

                try
                {
                    //parametros egresos
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Area_Funcional_ID = AF;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_ProgramaF_ID = PP_F;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_DependenciaF_ID = UR_F;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_CapituloF_ID = Cap_F;
                    Negocio.P_ConceptoF_ID = Con;
                    Negocio.P_Concepto_ID = Con_F;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_GenericaF_ID = PG_F;
                    Negocio.P_PartidaF_ID = P;
                    Negocio.P_Partida_ID = P_F;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        Negocio.P_Dependencia_ID = Usuario[1].Trim();
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = "'PF12','PF13', 'PF11'";
                    }

                    //parametros ingresos
                    Negocio.P_Fte_Financiamiento_Ing = FF_Ing;
                    Negocio.P_Programa_Ing_ID = PP_Ing;
                    Negocio.P_ProgramaF_Ing_ID = PP_Ing_F;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_RubroF_ID = R_F;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_TipoF_ID = T_F;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_ClaseF_Ing_ID = Cl_F;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_ConceptoF_Ing_ID = Con_Ing_F;
                    Negocio.P_SubConcepto_ID = SubCon;
                    Negocio.P_SubConceptoF_ID = SubCon_F;

                    if (Tipo_PSP.Trim().Equals("ING"))
                    {
                        Dt_Ing = Negocio.Consultar_Psp_Ing();
                    }
                    else if (Tipo_PSP.Trim().Equals("EGR"))
                    {
                        Dt_Egr = Negocio.Consultar_Psp_Egr();
                    }
                    else
                    {
                        Dt_Ing = Negocio.Consultar_Psp_Ing();
                        Dt_Egr = Negocio.Consultar_Psp_Egr();
                    }

                    if (Dt_Egr != null && Dt_Egr.Rows.Count > 0)
                    {
                        Dt_Egr_Completo = Generar_Dt_Psp_Egr(Dt_Egr);
                    }

                    if (Dt_Ing != null && Dt_Ing.Rows.Count > 0)
                    {
                        Dt_Ing_Completo = Generar_Dt_Psp_Ing(Dt_Ing);
                    }

                    Json_Datos = Obtener_Param_Niveles(Dt_Egr_Completo, Dt_Ing_Completo, Anio, Reporte);

                    return Json_Datos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Nivel_Fuente_Financiamiento_Ingresos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Param_Niveles
            ///DESCRIPCIÓN          : Metodo para obtener los parametros de los niveles del reporte
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 30/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Param_Niveles(DataTable Dt_Egr, DataTable Dt_Ing, String Anio, String Tipo_Reporte)
            {
                String Nom_Archivo = String.Empty;
                Cls_Ope_Psp_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                //Parametros de los combos
                String Niv_FF = String.Empty;
                String Niv_PP = String.Empty;
                String Niv_AF = String.Empty;
                String Niv_UR = String.Empty;
                String Niv_Cap = String.Empty;
                String Niv_Con = String.Empty;
                String Niv_Par_Gen = String.Empty;
                String Niv_Par = String.Empty;

                String Niv_FF_Ing = String.Empty;
                String Niv_PP_Ing = String.Empty;
                String Niv_Rub = String.Empty;
                String Niv_Tip = String.Empty;
                String Niv_Cla = String.Empty;
                String Niv_Con_Ing = String.Empty;
                String Niv_SubCon = String.Empty;

                DataTable Dt_Niv_Psp_Ing = new DataTable();
                DataTable Dt_Niv_Psp_Egr = new DataTable();
                DataRow Fila;
                Boolean Insertar = false;
                DataSet Ds_Registros_Ing = null;
                DataTable Dt_UR = new DataTable();
                String Dependencia = String.Empty;
                String Gpo_Dep = String.Empty;

                try
                {
                    #region (Parametros)
                    //obtenemos los parametros e los combos
                    if (this.Request.QueryString["Niv_FF"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_FF"].ToString().Trim()))
                    {
                        Niv_FF = this.Request.QueryString["Niv_FF"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_PP"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_PP"].ToString().Trim()))
                    {
                        Niv_PP = this.Request.QueryString["Niv_PP"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_AF"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_AF"].ToString().Trim()))
                    {
                        Niv_AF = this.Request.QueryString["Niv_AF"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_UR"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_UR"].ToString().Trim()))
                    {
                        Niv_UR = this.Request.QueryString["Niv_UR"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_Cap"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_Cap"].ToString().Trim()))
                    {
                        Niv_Cap = this.Request.QueryString["Niv_Cap"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_Con"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_Con"].ToString().Trim()))
                    {
                        Niv_Con = this.Request.QueryString["Niv_Con"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_Par_Gen"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_Par_Gen"].ToString().Trim()))
                    {
                        Niv_Par_Gen = this.Request.QueryString["Niv_Par_Gen"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_Par"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_Par"].ToString().Trim()))
                    {
                        Niv_Par = this.Request.QueryString["Niv_Par"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_FF_Ing"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_FF_Ing"].ToString().Trim()))
                    {
                        Niv_FF_Ing = this.Request.QueryString["Niv_FF_Ing"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_PP_Ing"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_PP_Ing"].ToString().Trim()))
                    {
                        Niv_PP_Ing = this.Request.QueryString["Niv_PP_Ing"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_Rub"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_Rub"].ToString().Trim()))
                    {
                        Niv_Rub = this.Request.QueryString["Niv_Rub"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_Tip"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_Tip"].ToString().Trim()))
                    {
                        Niv_Tip = this.Request.QueryString["Niv_Tip"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_Cla"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_Cla"].ToString().Trim()))
                    {
                        Niv_Cla = this.Request.QueryString["Niv_Cla"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_Con_Ing"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_Con_Ing"].ToString().Trim()))
                    {
                        Niv_Con_Ing = this.Request.QueryString["Niv_Con_Ing"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_SubCon"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_SubCon"].ToString().Trim()))
                    {
                        Niv_SubCon = this.Request.QueryString["Niv_SubCon"].ToString().Trim();
                    }
                    #endregion

                    //obtenemos la dependencia del usuario y el grupo dependencia
                    Negocio.P_Busqueda = String.Empty;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = String.Empty;
                    Negocio.P_Area_Funcional_ID = String.Empty;
                    Negocio.P_Tipo_Usuario = "Usuario";
                    Negocio.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado.Trim();

                    Dt_UR = Negocio.Consultar_UR();

                    if (Dt_UR != null && Dt_UR.Rows.Count > 0)
                    {
                        Dependencia = Dt_UR.Rows[0]["NOM_DEP"].ToString().Trim();
                        Gpo_Dep = Dt_UR.Rows[0]["NOM_GPO_DEP"].ToString().Trim();
                    }

                    if (Dt_Egr != null && Dt_Egr.Rows.Count > 0)
                    {
                        Dt_Niv_Psp_Egr.Columns.Add("Concepto", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Aprobado", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Ampliacion", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Reduccion", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Modificado", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Devengado", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Recaudado", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Dev_Rec", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Comprometido", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Por_Recaudar", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Tipo", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Elaboro", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Dependencia", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Anio", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Gpo_Dependencia", System.Type.GetType("System.String"));

                        foreach (DataRow Dr in Dt_Egr.Rows)
                        {
                            Insertar = false;

                            if (Dr["Tipo"].ToString().Trim().Equals("Tot"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("FF") && Niv_FF.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("AF") && Niv_AF.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("PP") && Niv_PP.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("UR") && Niv_UR.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("Cap") && Niv_Cap.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("Con") && Niv_Con.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("PG") && Niv_Par_Gen.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("P") && Niv_Par.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else
                            {
                                Insertar = false;
                            }

                            if (Insertar)
                            {
                                Fila = Dt_Niv_Psp_Egr.NewRow();
                                Fila["Concepto"] = Dr["Concepto"].ToString().Trim();
                                Fila["Aprobado"] = Dr["Aprobado"].ToString().Trim();
                                Fila["Ampliacion"] = Dr["Ampliacion"].ToString().Trim();
                                Fila["Reduccion"] = Dr["Reduccion"].ToString().Trim();
                                Fila["Modificado"] = Dr["Modificado"].ToString().Trim();
                                Fila["Devengado"] = Dr["Devengado"].ToString().Trim();
                                Fila["Recaudado"] = Dr["Recaudado"].ToString().Trim();
                                Fila["Dev_Rec"] = Dr["Dev_Rec"].ToString().Trim();
                                Fila["Comprometido"] = Dr["Comprometido"].ToString().Trim();
                                Fila["Por_Recaudar"] = Dr["Por_Recaudar"].ToString().Trim();
                                Fila["Tipo"] = Dr["Tipo"].ToString().Trim();
                                Fila["Elaboro"] = Cls_Sessiones.Nombre_Empleado.Trim();
                                Fila["Dependencia"] = Dependencia.Trim();
                                Fila["Anio"] = "PRESUPUESTO DE EGRESOS EJERCICIO " + Anio.Trim();
                                Fila["Gpo_Dependencia"] = Gpo_Dep.Trim();
                                Dt_Niv_Psp_Egr.Rows.Add(Fila);
                            }
                        }

                        if (Tipo_Reporte.Trim().Equals("PDF"))
                        {
                            Ds_Registros_Ing = new DataSet();
                            Dt_Niv_Psp_Egr.TableName = "Dt_Psp_Ing";
                            Ds_Registros_Ing.Tables.Add(Dt_Niv_Psp_Egr.Copy());
                            Exportar_Reporte(Ds_Registros_Ing, "Cr_Rpt_Psp_Presupuesto_Ing.rpt", "Rpt_Presupuesto_Egresos_" + Session.SessionID, "pdf",
                                ExportFormatType.PortableDocFormat, "Presupuestos");
                            Nom_Archivo = "../../Reporte/Rpt_Presupuesto_Egresos_" + Session.SessionID + ".pdf     ";
                        }
                        else if (Tipo_Reporte.Trim().Equals("Excel"))
                        {
                            Ds_Registros_Ing = new DataSet();
                            Dt_Niv_Psp_Egr.TableName = "Dt_Psp_Ing";
                            Ds_Registros_Ing.Tables.Add(Dt_Niv_Psp_Egr.Copy());
                            Exportar_Reporte(Ds_Registros_Ing, "Cr_Rpt_Psp_Presupuesto_Ing.rpt", "Rpt_Presupuesto_Egresos_" + Session.SessionID,
                                "xls", ExportFormatType.Excel, "Presupuestos");
                            Nom_Archivo = "../../Reporte/Rpt_Presupuesto_Egresos_" + Session.SessionID + ".xls     ";
                        }
                        else if (Tipo_Reporte.Trim().Equals("Word"))
                        {
                            Ds_Registros_Ing = new DataSet();
                            Dt_Niv_Psp_Egr.TableName = "Dt_Psp_Ing";
                            Ds_Registros_Ing.Tables.Add(Dt_Niv_Psp_Egr.Copy());
                            Exportar_Reporte(Ds_Registros_Ing, "Cr_Rpt_Psp_Presupuesto_Ing.rpt", "Rpt_Presupuesto_Egresos_" + Session.SessionID,
                                "doc", ExportFormatType.WordForWindows, "Presupuestos");
                            Nom_Archivo = "../../Reporte/Rpt_Presupuesto_Egresos_" + Session.SessionID + ".doc     ";
                        }
                    }

                    if (Dt_Ing != null && Dt_Ing.Rows.Count > 0)
                    {
                        Dt_Niv_Psp_Ing.Columns.Add("Concepto", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Aprobado", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Ampliacion", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Reduccion", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Modificado", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Devengado", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Recaudado", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Dev_Rec", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Comprometido", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Por_Recaudar", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Tipo", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Elaboro", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Dependencia", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Anio", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Gpo_Dependencia", System.Type.GetType("System.String"));

                        foreach (DataRow Dr in Dt_Ing.Rows)
                        {
                            Insertar = false;

                            if (Dr["Tipo"].ToString().Trim().Equals("Tot"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("FF") && Niv_FF_Ing.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("PP") && Niv_PP_Ing.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("R") && Niv_Rub.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("T") && Niv_Tip.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("CL") && Niv_Cla.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("C") && Niv_Con_Ing.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("SC") && Niv_SubCon.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else
                            {
                                Insertar = false;
                            }

                            if (Insertar)
                            {
                                Fila = Dt_Niv_Psp_Ing.NewRow();
                                Fila["Concepto"] = Dr["Concepto"].ToString().Trim();
                                Fila["Aprobado"] = Dr["Aprobado"].ToString().Trim();
                                Fila["Ampliacion"] = Dr["Ampliacion"].ToString().Trim();
                                Fila["Reduccion"] = Dr["Reduccion"].ToString().Trim();
                                Fila["Modificado"] = Dr["Modificado"].ToString().Trim();
                                Fila["Devengado"] = Dr["Devengado"].ToString().Trim();
                                Fila["Recaudado"] = Dr["Recaudado"].ToString().Trim();
                                Fila["Dev_Rec"] = Dr["Dev_Rec"].ToString().Trim();
                                Fila["Comprometido"] = Dr["Comprometido"].ToString().Trim();
                                Fila["Por_Recaudar"] = Dr["Por_Recaudar"].ToString().Trim();
                                Fila["Tipo"] = Dr["Tipo"].ToString().Trim();
                                Fila["Elaboro"] = Cls_Sessiones.Nombre_Empleado.Trim();
                                Fila["Dependencia"] = Dependencia.Trim();
                                Fila["Anio"] = "PRESUPUESTO DE INGRESOS EJERCICIO " + Anio.Trim();
                                Fila["Gpo_Dependencia"] = Gpo_Dep.Trim();
                                Dt_Niv_Psp_Ing.Rows.Add(Fila);
                            }
                        }

                        if (Tipo_Reporte.Trim().Equals("PDF"))
                        {
                            Ds_Registros_Ing = new DataSet();
                            Dt_Niv_Psp_Ing.TableName = "Dt_Psp_Ing";
                            Ds_Registros_Ing.Tables.Add(Dt_Niv_Psp_Ing.Copy());
                            //Generar_Reporte(ref Ds_Registros_Ing, "Cr_Rpt_Psp_Presupuesto_Ing.rpt", "Rpt_Presupuesto_Ingresos" + Session.SessionID + ".pdf");
                            Exportar_Reporte(Ds_Registros_Ing, "Cr_Rpt_Psp_Presupuesto_Ing.rpt", "Rpt_Presupuesto_Ingresos_" + Session.SessionID,
                                "pdf", ExportFormatType.PortableDocFormat, "Presupuestos");
                            Nom_Archivo = "../../Reporte/Rpt_Presupuesto_Ingresos_" + Session.SessionID + ".pdf     ";
                        }
                        else if (Tipo_Reporte.Trim().Equals("Excel"))
                        {
                            Ds_Registros_Ing = new DataSet();
                            Dt_Niv_Psp_Ing.TableName = "Dt_Psp_Ing";
                            Ds_Registros_Ing.Tables.Add(Dt_Niv_Psp_Ing.Copy());
                            Exportar_Reporte(Ds_Registros_Ing, "Cr_Rpt_Psp_Presupuesto_Ing.rpt", "Rpt_Presupuesto_Ingresos_" + Session.SessionID,
                                "xls", ExportFormatType.Excel, "Presupuestos");
                            Nom_Archivo = "../../Reporte/Rpt_Presupuesto_Ingresos_" + Session.SessionID + ".xls    ";
                        }
                        else if (Tipo_Reporte.Trim().Equals("Word"))
                        {
                            Ds_Registros_Ing = new DataSet();
                            Dt_Niv_Psp_Ing.TableName = "Dt_Psp_Ing";
                            Ds_Registros_Ing.Tables.Add(Dt_Niv_Psp_Ing.Copy());
                            Exportar_Reporte(Ds_Registros_Ing, "Cr_Rpt_Psp_Presupuesto_Ing.rpt", "Rpt_Presupuesto_Ingresos_" + Session.SessionID,
                                "doc", ExportFormatType.WordForWindows, "Presupuestos");
                            Nom_Archivo = "../../Reporte/Rpt_Presupuesto_Ingresos_" + Session.SessionID + ".doc     ";
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al obtener los parametros. Error[" + ex.Message + "]");
                }
                return Nom_Archivo;
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Generar_Dt_Psp_Egr
            ///DESCRIPCIÓN          : Metodo para generar la tabla del presupuesto de egresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 30/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private DataTable Generar_Dt_Psp_Egr(DataTable Dt_Egr)
            {
                DataTable Dt_Psp = new DataTable();
                DataRow Fila;

                #region (variables)
                Double Tot_Est = 0.00;
                Double Tot_Amp = 0.00;
                Double Tot_Red = 0.00;
                Double Tot_Mod = 0.00;
                Double Tot_Dev = 0.00;
                Double Tot_Pag = 0.00;
                Double Tot_Dev_Rec = 0.00;
                Double Tot_Com = 0.00;
                Double Tot_xEje = 0.00;

                Double FF_Est = 0.00;
                Double FF_Amp = 0.00;
                Double FF_Red = 0.00;
                Double FF_Mod = 0.00;
                Double FF_Dev = 0.00;
                Double FF_Pag = 0.00;
                Double FF_Dev_Rec = 0.00;
                Double FF_Com = 0.00;
                Double FF_xEje = 0.00;

                Double AF_Est = 0.00;
                Double AF_Amp = 0.00;
                Double AF_Red = 0.00;
                Double AF_Mod = 0.00;
                Double AF_Dev = 0.00;
                Double AF_Pag = 0.00;
                Double AF_Dev_Rec = 0.00;
                Double AF_Com = 0.00;
                Double AF_xEje = 0.00;

                Double PP_Est = 0.00;
                Double PP_Amp = 0.00;
                Double PP_Red = 0.00;
                Double PP_Mod = 0.00;
                Double PP_Dev = 0.00;
                Double PP_Pag = 0.00;
                Double PP_Dev_Rec = 0.00;
                Double PP_Com = 0.00;
                Double PP_xEje = 0.00;

                Double UR_Est = 0.00;
                Double UR_Amp = 0.00;
                Double UR_Red = 0.00;
                Double UR_Mod = 0.00;
                Double UR_Dev = 0.00;
                Double UR_Pag = 0.00;
                Double UR_Dev_Rec = 0.00;
                Double UR_Com = 0.00;
                Double UR_xEje = 0.00;

                Double Cap_Est = 0.00;
                Double Cap_Amp = 0.00;
                Double Cap_Red = 0.00;
                Double Cap_Mod = 0.00;
                Double Cap_Dev = 0.00;
                Double Cap_Pag = 0.00;
                Double Cap_Dev_Rec = 0.00;
                Double Cap_Com = 0.00;
                Double Cap_xEje = 0.00;

                Double Con_Est = 0.00;
                Double Con_Amp = 0.00;
                Double Con_Red = 0.00;
                Double Con_Mod = 0.00;
                Double Con_Dev = 0.00;
                Double Con_Pag = 0.00;
                Double Con_Dev_Rec = 0.00;
                Double Con_Com = 0.00;
                Double Con_xEje = 0.00;

                Double PG_Est = 0.00;
                Double PG_Amp = 0.00;
                Double PG_Red = 0.00;
                Double PG_Mod = 0.00;
                Double PG_Dev = 0.00;
                Double PG_Pag = 0.00;
                Double PG_Dev_Rec = 0.00;
                Double PG_Com = 0.00;
                Double PG_xEje = 0.00;
                #endregion

                try
                {
                    #region(Tabla)
                    String FF_ID = Dt_Egr.Rows[0]["FF_ID"].ToString().Trim();
                    String AF_ID = Dt_Egr.Rows[0]["AF_ID"].ToString().Trim();
                    String PP_ID = Dt_Egr.Rows[0]["PP_ID"].ToString().Trim();
                    String UR_ID = Dt_Egr.Rows[0]["UR_ID"].ToString().Trim();
                    String Cap_ID = Dt_Egr.Rows[0]["CA_ID"].ToString().Trim();
                    String Con_ID = Dt_Egr.Rows[0]["CO_ID"].ToString().Trim();
                    String PG_ID = Dt_Egr.Rows[0]["PG_ID"].ToString().Trim();

                    String FF = "******* " + Dt_Egr.Rows[0]["CLAVE_NOM_FF"].ToString().Trim();
                    String AF = "****** " + Dt_Egr.Rows[0]["CLAVE_NOM_AF"].ToString().Trim();
                    String PP = "***** " + Dt_Egr.Rows[0]["CLAVE_NOM_PP"].ToString().Trim();
                    String UR = "**** " + Dt_Egr.Rows[0]["CLAVE_NOM_UR"].ToString().Trim();
                    String Cap = "*** " + Dt_Egr.Rows[0]["CLAVE_NOM_CAPITULO"].ToString().Trim();
                    String Con = "** " + Dt_Egr.Rows[0]["CLAVE_NOM_CONCEPTO"].ToString().Trim();
                    String PG = "* " + Dt_Egr.Rows[0]["CLAVE_NOM_PARTIDA_GEN"].ToString().Trim();

                    Dt_Psp.Columns.Add("Concepto", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Aprobado", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Ampliacion", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Reduccion", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Modificado", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Devengado", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Recaudado", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Dev_Rec", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Comprometido", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Por_Recaudar", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Tipo", System.Type.GetType("System.String"));

                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = "******** TOTAL";
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "Tot";
                    Dt_Psp.Rows.Add(Fila);

                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = FF.Trim();
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "FF_Tot";
                    Dt_Psp.Rows.Add(Fila);

                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = AF.Trim();
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "AF_Tot";
                    Dt_Psp.Rows.Add(Fila);


                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = PP.Trim();
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "PP_Tot";
                    Dt_Psp.Rows.Add(Fila);


                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = UR.Trim();
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "UR_Tot";
                    Dt_Psp.Rows.Add(Fila);

                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = Cap.Trim();
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "Cap_Tot";
                    Dt_Psp.Rows.Add(Fila);

                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = Con.Trim();
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "Con_Tot";
                    Dt_Psp.Rows.Add(Fila);

                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = PG.Trim();
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "PG_Tot";
                    Dt_Psp.Rows.Add(Fila);

                    #endregion

                    foreach (DataRow Dr in Dt_Egr.Rows)
                    {
                        Tot_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                        Tot_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                        Tot_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                        Tot_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                        Tot_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                        Tot_Pag += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                        Tot_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                        Tot_Com += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                        Tot_xEje += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                        if (FF_ID.Trim().Equals(Dr["FF_ID"].ToString().Trim()))
                        {
                            //sumamos 
                            FF_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                            FF_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            FF_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            FF_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            FF_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            FF_Pag += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                            FF_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                            FF_Com += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                            FF_xEje += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                            if (AF_ID.Trim().Equals(Dr["AF_ID"].ToString().Trim()))
                            {
                                //sumamos 
                                AF_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                AF_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                AF_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                AF_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                AF_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                AF_Pag += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                AF_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                AF_Com += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                AF_xEje += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                if (PP_ID.Trim().Equals(Dr["PP_ID"].ToString().Trim()))
                                {
                                    //sumamos 
                                    PP_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                    PP_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                    PP_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                    PP_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                    PP_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                    PP_Pag += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                    PP_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                    PP_Com += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                    PP_xEje += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                    if (UR_ID.Trim().Equals(Dr["UR_ID"].ToString().Trim()))
                                    {
                                        //sumamos 
                                        UR_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                        UR_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                        UR_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                        UR_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                        UR_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                        UR_Pag += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                        UR_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                        UR_Com += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                        UR_xEje += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                        if (Cap_ID.Trim().Equals(Dr["CA_ID"].ToString().Trim()))
                                        {
                                            //sumamos 
                                            Cap_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                            Cap_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                            Cap_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                            Cap_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                            Cap_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                            Cap_Pag += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                            Cap_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                            Cap_Com += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                            Cap_xEje += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                            if (Con_ID.Trim().Equals(Dr["CO_ID"].ToString().Trim()))
                                            {
                                                Con_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                                Con_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                                Con_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                                Con_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                                Con_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                                Con_Pag += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                                Con_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                                Con_Com += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                                Con_xEje += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                                if (PG_ID.Trim().Equals(Dr["PG_ID"].ToString().Trim()))
                                                {
                                                    PG_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                                    PG_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                                    PG_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                                    PG_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                                    PG_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                                    PG_Pag += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                                    PG_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                                    PG_Com += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                                    PG_xEje += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                                    Fila = Dt_Psp.NewRow();
                                                    Fila["Concepto"] = Dr["CLAVE_NOM_PARTIDA"].ToString().Trim();
                                                    Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                                                    Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                                    Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                                    Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                                    Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                                    Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim()));
                                                    Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim()));
                                                    Fila["Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                                                    Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                                                    Fila["Tipo"] = "P";
                                                    Dt_Psp.Rows.Add(Fila);
                                                }
                                                else
                                                {
                                                    #region (else PG)
                                                    foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                                                    {
                                                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("PG_Tot"))
                                                        {
                                                            Dr_Psp["Aprobado"] = String.Format("{0:n}", PG_Est);
                                                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", PG_Amp);
                                                            Dr_Psp["Reduccion"] = String.Format("{0:n}", PG_Red);
                                                            Dr_Psp["Modificado"] = String.Format("{0:n}", PG_Mod);
                                                            Dr_Psp["Devengado"] = String.Format("{0:n}", PG_Dev);
                                                            Dr_Psp["Recaudado"] = String.Format("{0:n}", PG_Pag);
                                                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PG_Dev_Rec);
                                                            Dr_Psp["Comprometido"] = String.Format("{0:n}", PG_Com);
                                                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PG_xEje);
                                                            Dr_Psp["Tipo"] = "PG";
                                                        }
                                                    }//fin foreach

                                                    //limpiamos las variables
                                                    PG_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                                    PG_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                                    PG_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                                    PG_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                                    PG_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                                    PG_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                                    PG_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                                    PG_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                                    PG_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                                    PG_ID = Dr["PG_ID"].ToString().Trim();
                                                    PG = "* " + Dr["CLAVE_NOM_PARTIDA_GEN"].ToString().Trim();

                                                    Fila = Dt_Psp.NewRow();
                                                    Fila["Concepto"] = PG.Trim();
                                                    Fila["Aprobado"] = String.Empty;
                                                    Fila["Ampliacion"] = String.Empty;
                                                    Fila["Reduccion"] = String.Empty;
                                                    Fila["Modificado"] = String.Empty;
                                                    Fila["Devengado"] = String.Empty;
                                                    Fila["Recaudado"] = String.Empty;
                                                    Fila["Dev_Rec"] = String.Empty;
                                                    Fila["Comprometido"] = String.Empty;
                                                    Fila["Por_Recaudar"] = String.Empty;
                                                    Fila["Tipo"] = "PG_Tot";
                                                    Dt_Psp.Rows.Add(Fila);

                                                    Fila = Dt_Psp.NewRow();
                                                    Fila["Concepto"] = Dr["CLAVE_NOM_PARTIDA"].ToString().Trim();
                                                    Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                                                    Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                                    Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                                    Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                                    Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                                    Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim()));
                                                    Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim()));
                                                    Fila["Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                                                    Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                                                    Fila["Tipo"] = "P";
                                                    Dt_Psp.Rows.Add(Fila);
                                                    #endregion
                                                }
                                            }
                                            else
                                            {
                                                #region (else Con)
                                                foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                                                {
                                                    if (Dr_Psp["Tipo"].ToString().Trim().Equals("Con_Tot"))
                                                    {
                                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", Con_Est);
                                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", Con_Amp);
                                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", Con_Red);
                                                        Dr_Psp["Modificado"] = String.Format("{0:n}", Con_Mod);
                                                        Dr_Psp["Devengado"] = String.Format("{0:n}", Con_Dev);
                                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", Con_Pag);
                                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Con_Dev_Rec);
                                                        Dr_Psp["Comprometido"] = String.Format("{0:n}", Con_Com);
                                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Con_xEje);
                                                        Dr_Psp["Tipo"] = "Con";
                                                    }
                                                    else if (Dr_Psp["Tipo"].ToString().Trim().Equals("PG_Tot"))
                                                    {
                                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", PG_Est);
                                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", PG_Amp);
                                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", PG_Red);
                                                        Dr_Psp["Modificado"] = String.Format("{0:n}", PG_Mod);
                                                        Dr_Psp["Devengado"] = String.Format("{0:n}", PG_Dev);
                                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", PG_Pag);
                                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PG_Dev_Rec);
                                                        Dr_Psp["Comprometido"] = String.Format("{0:n}", PG_Com);
                                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PG_xEje);
                                                        Dr_Psp["Tipo"] = "PG";
                                                    }
                                                }//fin foreach

                                                //limpiamos las variables
                                                Con_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                                Con_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                                Con_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                                Con_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                                Con_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                                Con_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                                Con_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                                Con_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                                Con_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                                PG_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                                PG_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                                PG_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                                PG_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                                PG_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                                PG_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                                PG_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                                PG_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                                PG_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                                Con_ID = Dr["CO_ID"].ToString().Trim();
                                                Con = "** " + Dr["CLAVE_NOM_CONCEPTO"].ToString().Trim();
                                                PG_ID = Dr["PG_ID"].ToString().Trim();
                                                PG = "* " + Dr["CLAVE_NOM_PARTIDA_GEN"].ToString().Trim();

                                                Fila = Dt_Psp.NewRow();
                                                Fila["Concepto"] = Con.Trim();
                                                Fila["Aprobado"] = String.Empty;
                                                Fila["Ampliacion"] = String.Empty;
                                                Fila["Reduccion"] = String.Empty;
                                                Fila["Modificado"] = String.Empty;
                                                Fila["Devengado"] = String.Empty;
                                                Fila["Recaudado"] = String.Empty;
                                                Fila["Dev_Rec"] = String.Empty;
                                                Fila["Comprometido"] = String.Empty;
                                                Fila["Por_Recaudar"] = String.Empty;
                                                Fila["Tipo"] = "Con_Tot";
                                                Dt_Psp.Rows.Add(Fila);

                                                Fila = Dt_Psp.NewRow();
                                                Fila["Concepto"] = PG.Trim();
                                                Fila["Aprobado"] = String.Empty;
                                                Fila["Ampliacion"] = String.Empty;
                                                Fila["Reduccion"] = String.Empty;
                                                Fila["Modificado"] = String.Empty;
                                                Fila["Devengado"] = String.Empty;
                                                Fila["Recaudado"] = String.Empty;
                                                Fila["Dev_Rec"] = String.Empty;
                                                Fila["Comprometido"] = String.Empty;
                                                Fila["Por_Recaudar"] = String.Empty;
                                                Fila["Tipo"] = "PG_Tot";
                                                Dt_Psp.Rows.Add(Fila);

                                                Fila = Dt_Psp.NewRow();
                                                Fila["Concepto"] = Dr["CLAVE_NOM_PARTIDA"].ToString().Trim();
                                                Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                                                Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                                Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                                Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                                Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                                Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim()));
                                                Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim()));
                                                Fila["Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                                                Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                                                Fila["Tipo"] = "P";
                                                Dt_Psp.Rows.Add(Fila);
                                                #endregion
                                            }
                                        }
                                        else
                                        {
                                            #region (else Cap)
                                            foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                                            {
                                                if (Dr_Psp["Tipo"].ToString().Trim().Equals("Cap_Tot"))
                                                {
                                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", Cap_Est);
                                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", Cap_Amp);
                                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", Cap_Red);
                                                    Dr_Psp["Modificado"] = String.Format("{0:n}", Cap_Mod);
                                                    Dr_Psp["Devengado"] = String.Format("{0:n}", Cap_Dev);
                                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", Cap_Pag);
                                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Cap_Dev_Rec);
                                                    Dr_Psp["Comprometido"] = String.Format("{0:n}", Cap_Com);
                                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Cap_xEje);
                                                    Dr_Psp["Tipo"] = "Cap";
                                                }
                                                else if (Dr_Psp["Tipo"].ToString().Trim().Equals("Con_Tot"))
                                                {
                                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", Con_Est);
                                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", Con_Amp);
                                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", Con_Red);
                                                    Dr_Psp["Modificado"] = String.Format("{0:n}", Con_Mod);
                                                    Dr_Psp["Devengado"] = String.Format("{0:n}", Con_Dev);
                                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", Con_Pag);
                                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Con_Dev_Rec);
                                                    Dr_Psp["Comprometido"] = String.Format("{0:n}", Con_Com);
                                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Con_xEje);
                                                    Dr_Psp["Tipo"] = "Con";
                                                }
                                                else if (Dr_Psp["Tipo"].ToString().Trim().Equals("PG_Tot"))
                                                {
                                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", PG_Est);
                                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", PG_Amp);
                                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", PG_Red);
                                                    Dr_Psp["Modificado"] = String.Format("{0:n}", PG_Mod);
                                                    Dr_Psp["Devengado"] = String.Format("{0:n}", PG_Dev);
                                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", PG_Pag);
                                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PG_Dev_Rec);
                                                    Dr_Psp["Comprometido"] = String.Format("{0:n}", PG_Com);
                                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PG_xEje);
                                                    Dr_Psp["Tipo"] = "PG";
                                                }

                                            }//fin foreach

                                            //limpiamos las variables
                                            Cap_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                            Cap_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                            Cap_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                            Cap_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                            Cap_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                            Cap_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                            Cap_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                            Cap_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                            Cap_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                            Con_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                            Con_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                            Con_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                            Con_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                            Con_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                            Con_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                            Con_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                            Con_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                            Con_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                            PG_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                            PG_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                            PG_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                            PG_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                            PG_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                            PG_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                            PG_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                            PG_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                            PG_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                            Cap_ID = Dr["CA_ID"].ToString().Trim();
                                            Cap = "*** " + Dr["CLAVE_NOM_CAPITULO"].ToString().Trim();
                                            Con_ID = Dr["CO_ID"].ToString().Trim();
                                            Con = "** " + Dr["CLAVE_NOM_CONCEPTO"].ToString().Trim();
                                            PG_ID = Dr["PG_ID"].ToString().Trim();
                                            PG = "* " + Dr["CLAVE_NOM_PARTIDA_GEN"].ToString().Trim();

                                            Fila = Dt_Psp.NewRow();
                                            Fila["Concepto"] = Cap.Trim();
                                            Fila["Aprobado"] = String.Empty;
                                            Fila["Ampliacion"] = String.Empty;
                                            Fila["Reduccion"] = String.Empty;
                                            Fila["Modificado"] = String.Empty;
                                            Fila["Devengado"] = String.Empty;
                                            Fila["Recaudado"] = String.Empty;
                                            Fila["Dev_Rec"] = String.Empty;
                                            Fila["Comprometido"] = String.Empty;
                                            Fila["Por_Recaudar"] = String.Empty;
                                            Fila["Tipo"] = "Cap_Tot";
                                            Dt_Psp.Rows.Add(Fila);

                                            Fila = Dt_Psp.NewRow();
                                            Fila["Concepto"] = Con.Trim();
                                            Fila["Aprobado"] = String.Empty;
                                            Fila["Ampliacion"] = String.Empty;
                                            Fila["Reduccion"] = String.Empty;
                                            Fila["Modificado"] = String.Empty;
                                            Fila["Devengado"] = String.Empty;
                                            Fila["Recaudado"] = String.Empty;
                                            Fila["Dev_Rec"] = String.Empty;
                                            Fila["Comprometido"] = String.Empty;
                                            Fila["Por_Recaudar"] = String.Empty;
                                            Fila["Tipo"] = "Con_Tot";
                                            Dt_Psp.Rows.Add(Fila);

                                            Fila = Dt_Psp.NewRow();
                                            Fila["Concepto"] = PG.Trim();
                                            Fila["Aprobado"] = String.Empty;
                                            Fila["Ampliacion"] = String.Empty;
                                            Fila["Reduccion"] = String.Empty;
                                            Fila["Modificado"] = String.Empty;
                                            Fila["Devengado"] = String.Empty;
                                            Fila["Recaudado"] = String.Empty;
                                            Fila["Dev_Rec"] = String.Empty;
                                            Fila["Comprometido"] = String.Empty;
                                            Fila["Por_Recaudar"] = String.Empty;
                                            Fila["Tipo"] = "PG_Tot";
                                            Dt_Psp.Rows.Add(Fila);

                                            Fila = Dt_Psp.NewRow();
                                            Fila["Concepto"] = Dr["CLAVE_NOM_PARTIDA"].ToString().Trim();
                                            Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                                            Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                            Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                            Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                            Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                            Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim()));
                                            Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim()));
                                            Fila["Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                                            Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                                            Fila["Tipo"] = "P";
                                            Dt_Psp.Rows.Add(Fila);
                                            #endregion
                                        }
                                    }
                                    else
                                    {
                                        #region (else ur)
                                        foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                                        {
                                            if (Dr_Psp["Tipo"].ToString().Trim().Equals("UR_Tot"))
                                            {
                                                Dr_Psp["Aprobado"] = String.Format("{0:n}", UR_Est);
                                                Dr_Psp["Ampliacion"] = String.Format("{0:n}", UR_Amp);
                                                Dr_Psp["Reduccion"] = String.Format("{0:n}", UR_Red);
                                                Dr_Psp["Modificado"] = String.Format("{0:n}", UR_Mod);
                                                Dr_Psp["Devengado"] = String.Format("{0:n}", UR_Dev);
                                                Dr_Psp["Recaudado"] = String.Format("{0:n}", UR_Pag);
                                                Dr_Psp["Dev_Rec"] = String.Format("{0:n}", UR_Dev_Rec);
                                                Dr_Psp["Comprometido"] = String.Format("{0:n}", UR_Com);
                                                Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", UR_xEje);
                                                Dr_Psp["Tipo"] = "UR";
                                            }
                                            else if (Dr_Psp["Tipo"].ToString().Trim().Equals("Cap_Tot"))
                                            {
                                                Dr_Psp["Aprobado"] = String.Format("{0:n}", Cap_Est);
                                                Dr_Psp["Ampliacion"] = String.Format("{0:n}", Cap_Amp);
                                                Dr_Psp["Reduccion"] = String.Format("{0:n}", Cap_Red);
                                                Dr_Psp["Modificado"] = String.Format("{0:n}", Cap_Mod);
                                                Dr_Psp["Devengado"] = String.Format("{0:n}", Cap_Dev);
                                                Dr_Psp["Recaudado"] = String.Format("{0:n}", Cap_Pag);
                                                Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Cap_Dev_Rec);
                                                Dr_Psp["Comprometido"] = String.Format("{0:n}", Cap_Com);
                                                Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Cap_xEje);
                                                Dr_Psp["Tipo"] = "Cap";
                                            }
                                            else if (Dr_Psp["Tipo"].ToString().Trim().Equals("Con_Tot"))
                                            {
                                                Dr_Psp["Aprobado"] = String.Format("{0:n}", Con_Est);
                                                Dr_Psp["Ampliacion"] = String.Format("{0:n}", Con_Amp);
                                                Dr_Psp["Reduccion"] = String.Format("{0:n}", Con_Red);
                                                Dr_Psp["Modificado"] = String.Format("{0:n}", Con_Mod);
                                                Dr_Psp["Devengado"] = String.Format("{0:n}", Con_Dev);
                                                Dr_Psp["Recaudado"] = String.Format("{0:n}", Con_Pag);
                                                Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Con_Dev_Rec);
                                                Dr_Psp["Comprometido"] = String.Format("{0:n}", Con_Com);
                                                Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Con_xEje);
                                                Dr_Psp["Tipo"] = "Con";
                                            }
                                            else if (Dr_Psp["Tipo"].ToString().Trim().Equals("PG_Tot"))
                                            {
                                                Dr_Psp["Aprobado"] = String.Format("{0:n}", PG_Est);
                                                Dr_Psp["Ampliacion"] = String.Format("{0:n}", PG_Amp);
                                                Dr_Psp["Reduccion"] = String.Format("{0:n}", PG_Red);
                                                Dr_Psp["Modificado"] = String.Format("{0:n}", PG_Mod);
                                                Dr_Psp["Devengado"] = String.Format("{0:n}", PG_Dev);
                                                Dr_Psp["Recaudado"] = String.Format("{0:n}", PG_Pag);
                                                Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PG_Dev_Rec);
                                                Dr_Psp["Comprometido"] = String.Format("{0:n}", PG_Com);
                                                Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PG_xEje);
                                                Dr_Psp["Tipo"] = "PG";
                                            }

                                        }//fin foreach

                                        //limpiamos las variables
                                        UR_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                        UR_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                        UR_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                        UR_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                        UR_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                        UR_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                        UR_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                        UR_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                        UR_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                        Cap_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                        Cap_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                        Cap_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                        Cap_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                        Cap_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                        Cap_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                        Cap_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                        Cap_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                        Cap_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                        Con_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                        Con_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                        Con_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                        Con_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                        Con_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                        Con_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                        Con_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                        Con_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                        Con_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                        PG_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                        PG_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                        PG_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                        PG_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                        PG_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                        PG_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                        PG_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                        PG_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                        PG_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                        UR_ID = Dr["UR_ID"].ToString().Trim();
                                        UR = "**** " + Dr["CLAVE_NOM_UR"].ToString().Trim();
                                        Cap_ID = Dr["CA_ID"].ToString().Trim();
                                        Cap = "*** " + Dr["CLAVE_NOM_CAPITULO"].ToString().Trim();
                                        Con_ID = Dr["CO_ID"].ToString().Trim();
                                        Con = "** " + Dr["CLAVE_NOM_CONCEPTO"].ToString().Trim();
                                        PG_ID = Dr["PG_ID"].ToString().Trim();
                                        PG = "* " + Dr["CLAVE_NOM_PARTIDA_GEN"].ToString().Trim();

                                        Fila = Dt_Psp.NewRow();
                                        Fila["Concepto"] = UR.Trim();
                                        Fila["Aprobado"] = String.Empty;
                                        Fila["Ampliacion"] = String.Empty;
                                        Fila["Reduccion"] = String.Empty;
                                        Fila["Modificado"] = String.Empty;
                                        Fila["Devengado"] = String.Empty;
                                        Fila["Recaudado"] = String.Empty;
                                        Fila["Dev_Rec"] = String.Empty;
                                        Fila["Comprometido"] = String.Empty;
                                        Fila["Por_Recaudar"] = String.Empty;
                                        Fila["Tipo"] = "UR_Tot";
                                        Dt_Psp.Rows.Add(Fila);

                                        Fila = Dt_Psp.NewRow();
                                        Fila["Concepto"] = Cap.Trim();
                                        Fila["Aprobado"] = String.Empty;
                                        Fila["Ampliacion"] = String.Empty;
                                        Fila["Reduccion"] = String.Empty;
                                        Fila["Modificado"] = String.Empty;
                                        Fila["Devengado"] = String.Empty;
                                        Fila["Recaudado"] = String.Empty;
                                        Fila["Dev_Rec"] = String.Empty;
                                        Fila["Comprometido"] = String.Empty;
                                        Fila["Por_Recaudar"] = String.Empty;
                                        Fila["Tipo"] = "Cap_Tot";
                                        Dt_Psp.Rows.Add(Fila);

                                        Fila = Dt_Psp.NewRow();
                                        Fila["Concepto"] = Con.Trim();
                                        Fila["Aprobado"] = String.Empty;
                                        Fila["Ampliacion"] = String.Empty;
                                        Fila["Reduccion"] = String.Empty;
                                        Fila["Modificado"] = String.Empty;
                                        Fila["Devengado"] = String.Empty;
                                        Fila["Recaudado"] = String.Empty;
                                        Fila["Dev_Rec"] = String.Empty;
                                        Fila["Comprometido"] = String.Empty;
                                        Fila["Por_Recaudar"] = String.Empty;
                                        Fila["Tipo"] = "Con_Tot";
                                        Dt_Psp.Rows.Add(Fila);

                                        Fila = Dt_Psp.NewRow();
                                        Fila["Concepto"] = PG.Trim();
                                        Fila["Aprobado"] = String.Empty;
                                        Fila["Ampliacion"] = String.Empty;
                                        Fila["Reduccion"] = String.Empty;
                                        Fila["Modificado"] = String.Empty;
                                        Fila["Devengado"] = String.Empty;
                                        Fila["Recaudado"] = String.Empty;
                                        Fila["Dev_Rec"] = String.Empty;
                                        Fila["Comprometido"] = String.Empty;
                                        Fila["Por_Recaudar"] = String.Empty;
                                        Fila["Tipo"] = "PG_Tot";
                                        Dt_Psp.Rows.Add(Fila);

                                        Fila = Dt_Psp.NewRow();
                                        Fila["Concepto"] = Dr["CLAVE_NOM_PARTIDA"].ToString().Trim();
                                        Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                                        Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                        Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                        Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                        Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                        Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim()));
                                        Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim()));
                                        Fila["Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                                        Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                                        Fila["Tipo"] = "P";
                                        Dt_Psp.Rows.Add(Fila);
                                        #endregion
                                    }
                                }
                                else
                                {
                                    #region (else pp)
                                    foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                                    {
                                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("PP_Tot"))
                                        {
                                            Dr_Psp["Aprobado"] = String.Format("{0:n}", PP_Est);
                                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", PP_Amp);
                                            Dr_Psp["Reduccion"] = String.Format("{0:n}", PP_Red);
                                            Dr_Psp["Modificado"] = String.Format("{0:n}", PP_Mod);
                                            Dr_Psp["Devengado"] = String.Format("{0:n}", PP_Dev);
                                            Dr_Psp["Recaudado"] = String.Format("{0:n}", PP_Pag);
                                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PP_Dev_Rec);
                                            Dr_Psp["Comprometido"] = String.Format("{0:n}", PP_Com);
                                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PP_xEje);
                                            Dr_Psp["Tipo"] = "PP";
                                        }
                                        else if (Dr_Psp["Tipo"].ToString().Trim().Equals("UR_Tot"))
                                        {
                                            Dr_Psp["Aprobado"] = String.Format("{0:n}", UR_Est);
                                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", UR_Amp);
                                            Dr_Psp["Reduccion"] = String.Format("{0:n}", UR_Red);
                                            Dr_Psp["Modificado"] = String.Format("{0:n}", UR_Mod);
                                            Dr_Psp["Devengado"] = String.Format("{0:n}", UR_Dev);
                                            Dr_Psp["Recaudado"] = String.Format("{0:n}", UR_Pag);
                                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", UR_Dev_Rec);
                                            Dr_Psp["Comprometido"] = String.Format("{0:n}", UR_Com);
                                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", UR_xEje);
                                            Dr_Psp["Tipo"] = "UR";
                                        }
                                        else if (Dr_Psp["Tipo"].ToString().Trim().Equals("Cap_Tot"))
                                        {
                                            Dr_Psp["Aprobado"] = String.Format("{0:n}", Cap_Est);
                                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", Cap_Amp);
                                            Dr_Psp["Reduccion"] = String.Format("{0:n}", Cap_Red);
                                            Dr_Psp["Modificado"] = String.Format("{0:n}", Cap_Mod);
                                            Dr_Psp["Devengado"] = String.Format("{0:n}", Cap_Dev);
                                            Dr_Psp["Recaudado"] = String.Format("{0:n}", Cap_Pag);
                                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Cap_Dev_Rec);
                                            Dr_Psp["Comprometido"] = String.Format("{0:n}", Cap_Com);
                                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Cap_xEje);
                                            Dr_Psp["Tipo"] = "Cap";
                                        }
                                        else if (Dr_Psp["Tipo"].ToString().Trim().Equals("Con_Tot"))
                                        {
                                            Dr_Psp["Aprobado"] = String.Format("{0:n}", Con_Est);
                                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", Con_Amp);
                                            Dr_Psp["Reduccion"] = String.Format("{0:n}", Con_Red);
                                            Dr_Psp["Modificado"] = String.Format("{0:n}", Con_Mod);
                                            Dr_Psp["Devengado"] = String.Format("{0:n}", Con_Dev);
                                            Dr_Psp["Recaudado"] = String.Format("{0:n}", Con_Pag);
                                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Con_Dev_Rec);
                                            Dr_Psp["Comprometido"] = String.Format("{0:n}", Con_Com);
                                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Con_xEje);
                                            Dr_Psp["Tipo"] = "Con";
                                        }
                                        else if (Dr_Psp["Tipo"].ToString().Trim().Equals("PG_Tot"))
                                        {
                                            Dr_Psp["Aprobado"] = String.Format("{0:n}", PG_Est);
                                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", PG_Amp);
                                            Dr_Psp["Reduccion"] = String.Format("{0:n}", PG_Red);
                                            Dr_Psp["Modificado"] = String.Format("{0:n}", PG_Mod);
                                            Dr_Psp["Devengado"] = String.Format("{0:n}", PG_Dev);
                                            Dr_Psp["Recaudado"] = String.Format("{0:n}", PG_Pag);
                                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PG_Dev_Rec);
                                            Dr_Psp["Comprometido"] = String.Format("{0:n}", PG_Com);
                                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PG_xEje);
                                            Dr_Psp["Tipo"] = "PG";
                                        }

                                    }//fin foreach

                                    //limpiamos las variables
                                    PP_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                    PP_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                    PP_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                    PP_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                    PP_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                    PP_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                    PP_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                    PP_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                    PP_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                    UR_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                    UR_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                    UR_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                    UR_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                    UR_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                    UR_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                    UR_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                    UR_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                    UR_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                    Cap_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                    Cap_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                    Cap_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                    Cap_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                    Cap_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                    Cap_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                    Cap_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                    Cap_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                    Cap_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                    Con_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                    Con_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                    Con_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                    Con_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                    Con_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                    Con_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                    Con_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                    Con_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                    Con_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                    PG_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                    PG_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                    PG_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                    PG_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                    PG_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                    PG_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                    PG_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                    PG_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                    PG_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                    PP_ID = Dr["PP_ID"].ToString().Trim();
                                    PP = "***** " + Dr["CLAVE_NOM_PP"].ToString().Trim();
                                    UR_ID = Dr["UR_ID"].ToString().Trim();
                                    UR = "**** " + Dr["CLAVE_NOM_UR"].ToString().Trim();
                                    Cap_ID = Dr["CA_ID"].ToString().Trim();
                                    Cap = "*** " + Dr["CLAVE_NOM_CAPITULO"].ToString().Trim();
                                    Con_ID = Dr["CO_ID"].ToString().Trim();
                                    Con = "** " + Dr["CLAVE_NOM_CONCEPTO"].ToString().Trim();
                                    PG_ID = Dr["PG_ID"].ToString().Trim();
                                    PG = "* " + Dr["CLAVE_NOM_PARTIDA_GEN"].ToString().Trim();

                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = PP.Trim();
                                    Fila["Aprobado"] = String.Empty;
                                    Fila["Ampliacion"] = String.Empty;
                                    Fila["Reduccion"] = String.Empty;
                                    Fila["Modificado"] = String.Empty;
                                    Fila["Devengado"] = String.Empty;
                                    Fila["Recaudado"] = String.Empty;
                                    Fila["Dev_Rec"] = String.Empty;
                                    Fila["Comprometido"] = String.Empty;
                                    Fila["Por_Recaudar"] = String.Empty;
                                    Fila["Tipo"] = "PP_Tot";
                                    Dt_Psp.Rows.Add(Fila);

                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = UR.Trim();
                                    Fila["Aprobado"] = String.Empty;
                                    Fila["Ampliacion"] = String.Empty;
                                    Fila["Reduccion"] = String.Empty;
                                    Fila["Modificado"] = String.Empty;
                                    Fila["Devengado"] = String.Empty;
                                    Fila["Recaudado"] = String.Empty;
                                    Fila["Dev_Rec"] = String.Empty;
                                    Fila["Comprometido"] = String.Empty;
                                    Fila["Por_Recaudar"] = String.Empty;
                                    Fila["Tipo"] = "UR_Tot";
                                    Dt_Psp.Rows.Add(Fila);

                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = Cap.Trim();
                                    Fila["Aprobado"] = String.Empty;
                                    Fila["Ampliacion"] = String.Empty;
                                    Fila["Reduccion"] = String.Empty;
                                    Fila["Modificado"] = String.Empty;
                                    Fila["Devengado"] = String.Empty;
                                    Fila["Recaudado"] = String.Empty;
                                    Fila["Dev_Rec"] = String.Empty;
                                    Fila["Comprometido"] = String.Empty;
                                    Fila["Por_Recaudar"] = String.Empty;
                                    Fila["Tipo"] = "Cap_Tot";
                                    Dt_Psp.Rows.Add(Fila);

                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = Con.Trim();
                                    Fila["Aprobado"] = String.Empty;
                                    Fila["Ampliacion"] = String.Empty;
                                    Fila["Reduccion"] = String.Empty;
                                    Fila["Modificado"] = String.Empty;
                                    Fila["Devengado"] = String.Empty;
                                    Fila["Recaudado"] = String.Empty;
                                    Fila["Dev_Rec"] = String.Empty;
                                    Fila["Comprometido"] = String.Empty;
                                    Fila["Por_Recaudar"] = String.Empty;
                                    Fila["Tipo"] = "Con_Tot";
                                    Dt_Psp.Rows.Add(Fila);

                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = PG.Trim();
                                    Fila["Aprobado"] = String.Empty;
                                    Fila["Ampliacion"] = String.Empty;
                                    Fila["Reduccion"] = String.Empty;
                                    Fila["Modificado"] = String.Empty;
                                    Fila["Devengado"] = String.Empty;
                                    Fila["Recaudado"] = String.Empty;
                                    Fila["Dev_Rec"] = String.Empty;
                                    Fila["Comprometido"] = String.Empty;
                                    Fila["Por_Recaudar"] = String.Empty;
                                    Fila["Tipo"] = "PG_Tot";
                                    Dt_Psp.Rows.Add(Fila);

                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = Dr["CLAVE_NOM_PARTIDA"].ToString().Trim();
                                    Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                                    Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                    Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                    Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                    Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                    Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim()));
                                    Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim()));
                                    Fila["Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                                    Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                                    Fila["Tipo"] = "P";
                                    Dt_Psp.Rows.Add(Fila);
                                    #endregion
                                }
                            }
                            else
                            {
                                #region (else AF)
                                foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                                {
                                    if (Dr_Psp["Tipo"].ToString().Trim().Equals("AF_Tot"))
                                    {
                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", AF_Est);
                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", AF_Amp);
                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", AF_Red);
                                        Dr_Psp["Modificado"] = String.Format("{0:n}", AF_Mod);
                                        Dr_Psp["Devengado"] = String.Format("{0:n}", AF_Dev);
                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", AF_Pag);
                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", AF_Dev_Rec);
                                        Dr_Psp["Comprometido"] = String.Format("{0:n}", AF_Com);
                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", AF_xEje);
                                        Dr_Psp["Tipo"] = "AF";
                                    }
                                    else if (Dr_Psp["Tipo"].ToString().Trim().Equals("PP_Tot"))
                                    {
                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", PP_Est);
                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", PP_Amp);
                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", PP_Red);
                                        Dr_Psp["Modificado"] = String.Format("{0:n}", PP_Mod);
                                        Dr_Psp["Devengado"] = String.Format("{0:n}", PP_Dev);
                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", PP_Pag);
                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PP_Dev_Rec);
                                        Dr_Psp["Comprometido"] = String.Format("{0:n}", PP_Com);
                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PP_xEje);
                                        Dr_Psp["Tipo"] = "PP";
                                    }
                                    else if (Dr_Psp["Tipo"].ToString().Trim().Equals("UR_Tot"))
                                    {
                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", UR_Est);
                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", UR_Amp);
                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", UR_Red);
                                        Dr_Psp["Modificado"] = String.Format("{0:n}", UR_Mod);
                                        Dr_Psp["Devengado"] = String.Format("{0:n}", UR_Dev);
                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", UR_Pag);
                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", UR_Dev_Rec);
                                        Dr_Psp["Comprometido"] = String.Format("{0:n}", UR_Com);
                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", UR_xEje);
                                        Dr_Psp["Tipo"] = "UR";
                                    }
                                    else if (Dr_Psp["Tipo"].ToString().Trim().Equals("Cap_Tot"))
                                    {
                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", Cap_Est);
                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", Cap_Amp);
                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", Cap_Red);
                                        Dr_Psp["Modificado"] = String.Format("{0:n}", Cap_Mod);
                                        Dr_Psp["Devengado"] = String.Format("{0:n}", Cap_Dev);
                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", Cap_Pag);
                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Cap_Dev_Rec);
                                        Dr_Psp["Comprometido"] = String.Format("{0:n}", Cap_Com);
                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Cap_xEje);
                                        Dr_Psp["Tipo"] = "Cap";
                                    }
                                    else if (Dr_Psp["Tipo"].ToString().Trim().Equals("Con_Tot"))
                                    {
                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", Con_Est);
                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", Con_Amp);
                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", Con_Red);
                                        Dr_Psp["Modificado"] = String.Format("{0:n}", Con_Mod);
                                        Dr_Psp["Devengado"] = String.Format("{0:n}", Con_Dev);
                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", Con_Pag);
                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Con_Dev_Rec);
                                        Dr_Psp["Comprometido"] = String.Format("{0:n}", Con_Com);
                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Con_xEje);
                                        Dr_Psp["Tipo"] = "Con";
                                    }
                                    else if (Dr_Psp["Tipo"].ToString().Trim().Equals("PG_Tot"))
                                    {
                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", PG_Est);
                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", PG_Amp);
                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", PG_Red);
                                        Dr_Psp["Modificado"] = String.Format("{0:n}", PG_Mod);
                                        Dr_Psp["Devengado"] = String.Format("{0:n}", PG_Dev);
                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", PG_Pag);
                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PG_Dev_Rec);
                                        Dr_Psp["Comprometido"] = String.Format("{0:n}", PG_Com);
                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PG_xEje);
                                        Dr_Psp["Tipo"] = "PG";
                                    }

                                }//fin foreach

                                //limpiamos las variables
                                AF_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                AF_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                AF_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                AF_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                AF_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                AF_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                AF_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                AF_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                AF_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                PP_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                PP_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                PP_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                PP_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                PP_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                PP_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                PP_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                PP_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                PP_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                UR_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                UR_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                UR_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                UR_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                UR_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                UR_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                UR_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                UR_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                UR_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                Cap_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                Cap_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                Cap_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                Cap_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                Cap_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                Cap_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                Cap_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                Cap_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                Cap_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                Con_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                Con_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                Con_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                Con_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                Con_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                Con_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                Con_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                Con_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                Con_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                PG_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                PG_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                PG_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                PG_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                PG_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                PG_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                PG_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                PG_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                PG_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                                AF_ID = Dr["AF_ID"].ToString().Trim();
                                AF = "****** " + Dr["CLAVE_NOM_AF"].ToString().Trim();
                                PP_ID = Dr["PP_ID"].ToString().Trim();
                                PP = "***** " + Dr["CLAVE_NOM_PP"].ToString().Trim();
                                UR_ID = Dr["UR_ID"].ToString().Trim();
                                UR = "**** " + Dr["CLAVE_NOM_UR"].ToString().Trim();
                                Cap_ID = Dr["CA_ID"].ToString().Trim();
                                Cap = "*** " + Dr["CLAVE_NOM_CAPITULO"].ToString().Trim();
                                Con_ID = Dr["CO_ID"].ToString().Trim();
                                Con = "** " + Dr["CLAVE_NOM_CONCEPTO"].ToString().Trim();
                                PG_ID = Dr["PG_ID"].ToString().Trim();
                                PG = "* " + Dr["CLAVE_NOM_PARTIDA_GEN"].ToString().Trim();

                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = AF.Trim();
                                Fila["Aprobado"] = String.Empty;
                                Fila["Ampliacion"] = String.Empty;
                                Fila["Reduccion"] = String.Empty;
                                Fila["Modificado"] = String.Empty;
                                Fila["Devengado"] = String.Empty;
                                Fila["Recaudado"] = String.Empty;
                                Fila["Dev_Rec"] = String.Empty;
                                Fila["Comprometido"] = String.Empty;
                                Fila["Por_Recaudar"] = String.Empty;
                                Fila["Tipo"] = "AF_Tot";
                                Dt_Psp.Rows.Add(Fila);

                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = PP.Trim();
                                Fila["Aprobado"] = String.Empty;
                                Fila["Ampliacion"] = String.Empty;
                                Fila["Reduccion"] = String.Empty;
                                Fila["Modificado"] = String.Empty;
                                Fila["Devengado"] = String.Empty;
                                Fila["Recaudado"] = String.Empty;
                                Fila["Dev_Rec"] = String.Empty;
                                Fila["Comprometido"] = String.Empty;
                                Fila["Por_Recaudar"] = String.Empty;
                                Fila["Tipo"] = "PP_Tot";
                                Dt_Psp.Rows.Add(Fila);

                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = UR.Trim();
                                Fila["Aprobado"] = String.Empty;
                                Fila["Ampliacion"] = String.Empty;
                                Fila["Reduccion"] = String.Empty;
                                Fila["Modificado"] = String.Empty;
                                Fila["Devengado"] = String.Empty;
                                Fila["Recaudado"] = String.Empty;
                                Fila["Dev_Rec"] = String.Empty;
                                Fila["Comprometido"] = String.Empty;
                                Fila["Por_Recaudar"] = String.Empty;
                                Fila["Tipo"] = "UR_Tot";
                                Dt_Psp.Rows.Add(Fila);

                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = Cap.Trim();
                                Fila["Aprobado"] = String.Empty;
                                Fila["Ampliacion"] = String.Empty;
                                Fila["Reduccion"] = String.Empty;
                                Fila["Modificado"] = String.Empty;
                                Fila["Devengado"] = String.Empty;
                                Fila["Recaudado"] = String.Empty;
                                Fila["Dev_Rec"] = String.Empty;
                                Fila["Comprometido"] = String.Empty;
                                Fila["Por_Recaudar"] = String.Empty;
                                Fila["Tipo"] = "Cap_Tot";
                                Dt_Psp.Rows.Add(Fila);

                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = Con.Trim();
                                Fila["Aprobado"] = String.Empty;
                                Fila["Ampliacion"] = String.Empty;
                                Fila["Reduccion"] = String.Empty;
                                Fila["Modificado"] = String.Empty;
                                Fila["Devengado"] = String.Empty;
                                Fila["Recaudado"] = String.Empty;
                                Fila["Dev_Rec"] = String.Empty;
                                Fila["Comprometido"] = String.Empty;
                                Fila["Por_Recaudar"] = String.Empty;
                                Fila["Tipo"] = "Con_Tot";
                                Dt_Psp.Rows.Add(Fila);

                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = PG.Trim();
                                Fila["Aprobado"] = String.Empty;
                                Fila["Ampliacion"] = String.Empty;
                                Fila["Reduccion"] = String.Empty;
                                Fila["Modificado"] = String.Empty;
                                Fila["Devengado"] = String.Empty;
                                Fila["Recaudado"] = String.Empty;
                                Fila["Dev_Rec"] = String.Empty;
                                Fila["Comprometido"] = String.Empty;
                                Fila["Por_Recaudar"] = String.Empty;
                                Fila["Tipo"] = "PG_Tot";
                                Dt_Psp.Rows.Add(Fila);

                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = Dr["CLAVE_NOM_PARTIDA"].ToString().Trim();
                                Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                                Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim()));
                                Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim()));
                                Fila["Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                                Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                                Fila["Tipo"] = "P";
                                Dt_Psp.Rows.Add(Fila);
                                #endregion
                            }
                        }
                        else
                        {
                            #region (else FF)
                            foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                            {
                                if (Dr_Psp["Tipo"].ToString().Trim().Equals("FF_Tot"))
                                {
                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", FF_Est);
                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", FF_Amp);
                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", FF_Red);
                                    Dr_Psp["Modificado"] = String.Format("{0:n}", FF_Mod);
                                    Dr_Psp["Devengado"] = String.Format("{0:n}", FF_Dev);
                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", FF_Pag);
                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", FF_Dev_Rec);
                                    Dr_Psp["Comprometido"] = String.Format("{0:n}", FF_Com);
                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", FF_xEje);
                                    Dr_Psp["Tipo"] = "FF";
                                }
                                else if (Dr_Psp["Tipo"].ToString().Trim().Equals("AF_Tot"))
                                {
                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", AF_Est);
                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", AF_Amp);
                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", AF_Red);
                                    Dr_Psp["Modificado"] = String.Format("{0:n}", AF_Mod);
                                    Dr_Psp["Devengado"] = String.Format("{0:n}", AF_Dev);
                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", AF_Pag);
                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", AF_Dev_Rec);
                                    Dr_Psp["Comprometido"] = String.Format("{0:n}", AF_Com);
                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", AF_xEje);
                                    Dr_Psp["Tipo"] = "AF";
                                }
                                else if (Dr_Psp["Tipo"].ToString().Trim().Equals("PP_Tot"))
                                {
                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", PP_Est);
                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", PP_Amp);
                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", PP_Red);
                                    Dr_Psp["Modificado"] = String.Format("{0:n}", PP_Mod);
                                    Dr_Psp["Devengado"] = String.Format("{0:n}", PP_Dev);
                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", PP_Pag);
                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PP_Dev_Rec);
                                    Dr_Psp["Comprometido"] = String.Format("{0:n}", PP_Com);
                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PP_xEje);
                                    Dr_Psp["Tipo"] = "PP";
                                }
                                else if (Dr_Psp["Tipo"].ToString().Trim().Equals("UR_Tot"))
                                {
                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", UR_Est);
                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", UR_Amp);
                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", UR_Red);
                                    Dr_Psp["Modificado"] = String.Format("{0:n}", UR_Mod);
                                    Dr_Psp["Devengado"] = String.Format("{0:n}", UR_Dev);
                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", UR_Pag);
                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", UR_Dev_Rec);
                                    Dr_Psp["Comprometido"] = String.Format("{0:n}", UR_Com);
                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", UR_xEje);
                                    Dr_Psp["Tipo"] = "UR";
                                }
                                else if (Dr_Psp["Tipo"].ToString().Trim().Equals("Cap_Tot"))
                                {
                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", Cap_Est);
                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", Cap_Amp);
                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", Cap_Red);
                                    Dr_Psp["Modificado"] = String.Format("{0:n}", Cap_Mod);
                                    Dr_Psp["Devengado"] = String.Format("{0:n}", Cap_Dev);
                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", Cap_Pag);
                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Cap_Dev_Rec);
                                    Dr_Psp["Comprometido"] = String.Format("{0:n}", Cap_Com);
                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Cap_xEje);
                                    Dr_Psp["Tipo"] = "Cap";
                                }
                                else if (Dr_Psp["Tipo"].ToString().Trim().Equals("Con_Tot"))
                                {
                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", Con_Est);
                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", Con_Amp);
                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", Con_Red);
                                    Dr_Psp["Modificado"] = String.Format("{0:n}", Con_Mod);
                                    Dr_Psp["Devengado"] = String.Format("{0:n}", Con_Dev);
                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", Con_Pag);
                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Con_Dev_Rec);
                                    Dr_Psp["Comprometido"] = String.Format("{0:n}", Con_Com);
                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Con_xEje);
                                    Dr_Psp["Tipo"] = "Con";
                                }
                                else if (Dr_Psp["Tipo"].ToString().Trim().Equals("PG_Tot"))
                                {
                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", PG_Est);
                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", PG_Amp);
                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", PG_Red);
                                    Dr_Psp["Modificado"] = String.Format("{0:n}", PG_Mod);
                                    Dr_Psp["Devengado"] = String.Format("{0:n}", PG_Dev);
                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", PG_Pag);
                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PG_Dev_Rec);
                                    Dr_Psp["Comprometido"] = String.Format("{0:n}", PG_Com);
                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PG_xEje);
                                    Dr_Psp["Tipo"] = "PG";
                                }

                            }//fin foreach

                            //limpiamos las variables
                            FF_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                            FF_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            FF_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            FF_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            FF_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            FF_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                            FF_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                            FF_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                            FF_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                            AF_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                            AF_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            AF_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            AF_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            AF_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            AF_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                            AF_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                            AF_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                            AF_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                            PP_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                            PP_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            PP_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            PP_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            PP_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            PP_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                            PP_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                            PP_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                            PP_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                            UR_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                            UR_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            UR_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            UR_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            UR_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            UR_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                            UR_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                            UR_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                            UR_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                            Cap_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                            Cap_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            Cap_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            Cap_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            Cap_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            Cap_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                            Cap_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                            Cap_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                            Cap_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                            Con_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                            Con_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            Con_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            Con_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            Con_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            Con_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                            Con_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                            Con_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                            Con_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                            PG_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                            PG_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            PG_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            PG_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            PG_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            PG_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                            PG_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                            PG_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                            PG_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());

                            FF_ID = Dr["FF_ID"].ToString().Trim();
                            FF = "******* " + Dr["CLAVE_NOM_FF"].ToString().Trim();
                            AF_ID = Dr["AF_ID"].ToString().Trim();
                            AF = "****** " + Dr["CLAVE_NOM_AF"].ToString().Trim();
                            PP_ID = Dr["PP_ID"].ToString().Trim();
                            PP = "***** " + Dr["CLAVE_NOM_PP"].ToString().Trim();
                            UR_ID = Dr["UR_ID"].ToString().Trim();
                            UR = "**** " + Dr["CLAVE_NOM_UR"].ToString().Trim();
                            Cap_ID = Dr["CA_ID"].ToString().Trim();
                            Cap = "*** " + Dr["CLAVE_NOM_CAPITULO"].ToString().Trim();
                            Con_ID = Dr["CO_ID"].ToString().Trim();
                            Con = "** " + Dr["CLAVE_NOM_CONCEPTO"].ToString().Trim();
                            PG_ID = Dr["PG_ID"].ToString().Trim();
                            PG = "* " + Dr["CLAVE_NOM_PARTIDA_GEN"].ToString().Trim();

                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = FF.Trim();
                            Fila["Aprobado"] = String.Empty;
                            Fila["Ampliacion"] = String.Empty;
                            Fila["Reduccion"] = String.Empty;
                            Fila["Modificado"] = String.Empty;
                            Fila["Devengado"] = String.Empty;
                            Fila["Recaudado"] = String.Empty;
                            Fila["Dev_Rec"] = String.Empty;
                            Fila["Comprometido"] = String.Empty;
                            Fila["Por_Recaudar"] = String.Empty;
                            Fila["Tipo"] = "FF_Tot";
                            Dt_Psp.Rows.Add(Fila);

                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = AF.Trim();
                            Fila["Aprobado"] = String.Empty;
                            Fila["Ampliacion"] = String.Empty;
                            Fila["Reduccion"] = String.Empty;
                            Fila["Modificado"] = String.Empty;
                            Fila["Devengado"] = String.Empty;
                            Fila["Recaudado"] = String.Empty;
                            Fila["Dev_Rec"] = String.Empty;
                            Fila["Comprometido"] = String.Empty;
                            Fila["Por_Recaudar"] = String.Empty;
                            Fila["Tipo"] = "AF_Tot";
                            Dt_Psp.Rows.Add(Fila);

                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = PP.Trim();
                            Fila["Aprobado"] = String.Empty;
                            Fila["Ampliacion"] = String.Empty;
                            Fila["Reduccion"] = String.Empty;
                            Fila["Modificado"] = String.Empty;
                            Fila["Devengado"] = String.Empty;
                            Fila["Recaudado"] = String.Empty;
                            Fila["Dev_Rec"] = String.Empty;
                            Fila["Comprometido"] = String.Empty;
                            Fila["Por_Recaudar"] = String.Empty;
                            Fila["Tipo"] = "PP_Tot";
                            Dt_Psp.Rows.Add(Fila);

                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = UR.Trim();
                            Fila["Aprobado"] = String.Empty;
                            Fila["Ampliacion"] = String.Empty;
                            Fila["Reduccion"] = String.Empty;
                            Fila["Modificado"] = String.Empty;
                            Fila["Devengado"] = String.Empty;
                            Fila["Recaudado"] = String.Empty;
                            Fila["Dev_Rec"] = String.Empty;
                            Fila["Comprometido"] = String.Empty;
                            Fila["Por_Recaudar"] = String.Empty;
                            Fila["Tipo"] = "UR_Tot";
                            Dt_Psp.Rows.Add(Fila);

                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = Cap.Trim();
                            Fila["Aprobado"] = String.Empty;
                            Fila["Ampliacion"] = String.Empty;
                            Fila["Reduccion"] = String.Empty;
                            Fila["Modificado"] = String.Empty;
                            Fila["Devengado"] = String.Empty;
                            Fila["Recaudado"] = String.Empty;
                            Fila["Dev_Rec"] = String.Empty;
                            Fila["Comprometido"] = String.Empty;
                            Fila["Por_Recaudar"] = String.Empty;
                            Fila["Tipo"] = "Cap_Tot";
                            Dt_Psp.Rows.Add(Fila);

                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = Con.Trim();
                            Fila["Aprobado"] = String.Empty;
                            Fila["Ampliacion"] = String.Empty;
                            Fila["Reduccion"] = String.Empty;
                            Fila["Modificado"] = String.Empty;
                            Fila["Devengado"] = String.Empty;
                            Fila["Recaudado"] = String.Empty;
                            Fila["Dev_Rec"] = String.Empty;
                            Fila["Comprometido"] = String.Empty;
                            Fila["Por_Recaudar"] = String.Empty;
                            Fila["Tipo"] = "Con_Tot";
                            Dt_Psp.Rows.Add(Fila);

                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = PG.Trim();
                            Fila["Aprobado"] = String.Empty;
                            Fila["Ampliacion"] = String.Empty;
                            Fila["Reduccion"] = String.Empty;
                            Fila["Modificado"] = String.Empty;
                            Fila["Devengado"] = String.Empty;
                            Fila["Recaudado"] = String.Empty;
                            Fila["Dev_Rec"] = String.Empty;
                            Fila["Comprometido"] = String.Empty;
                            Fila["Por_Recaudar"] = String.Empty;
                            Fila["Tipo"] = "PG_Tot";
                            Dt_Psp.Rows.Add(Fila);

                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = Dr["CLAVE_NOM_PARTIDA"].ToString().Trim();
                            Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                            Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                            Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                            Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                            Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                            Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim()));
                            Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim()));
                            Fila["Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                            Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                            Fila["Tipo"] = "P";
                            Dt_Psp.Rows.Add(Fila);
                            #endregion
                        }
                    }

                    #region (Fin Dt Psp)
                    foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                    {
                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", Tot_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", Tot_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", Tot_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", Tot_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", Tot_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", Tot_Pag);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Tot_Dev_Rec);
                            Dr_Psp["Comprometido"] = String.Format("{0:n}", Tot_Com);
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Tot_xEje);
                            Dr_Psp["Tipo"] = "Tot";
                        }
                        else if (Dr_Psp["Tipo"].ToString().Trim().Equals("FF_Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", FF_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", FF_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", FF_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", FF_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", FF_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", FF_Pag);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", FF_Dev_Rec);
                            Dr_Psp["Comprometido"] = String.Format("{0:n}", FF_Com);
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", FF_xEje);
                            Dr_Psp["Tipo"] = "FF";
                        }
                        else if (Dr_Psp["Tipo"].ToString().Trim().Equals("AF_Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", AF_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", AF_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", AF_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", AF_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", AF_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", AF_Pag);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", AF_Dev_Rec);
                            Dr_Psp["Comprometido"] = String.Format("{0:n}", AF_Com);
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", AF_xEje);
                            Dr_Psp["Tipo"] = "AF";
                        }
                        else if (Dr_Psp["Tipo"].ToString().Trim().Equals("PP_Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", PP_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", PP_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", PP_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", PP_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", PP_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", PP_Pag);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PP_Dev_Rec);
                            Dr_Psp["Comprometido"] = String.Format("{0:n}", PP_Com);
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PP_xEje);
                            Dr_Psp["Tipo"] = "PP";
                        }
                        else if (Dr_Psp["Tipo"].ToString().Trim().Equals("UR_Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", UR_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", UR_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", UR_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", UR_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", UR_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", UR_Pag);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", UR_Dev_Rec);
                            Dr_Psp["Comprometido"] = String.Format("{0:n}", UR_Com);
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", UR_xEje);
                            Dr_Psp["Tipo"] = "UR";
                        }
                        else if (Dr_Psp["Tipo"].ToString().Trim().Equals("Cap_Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", Cap_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", Cap_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", Cap_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", Cap_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", Cap_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", Cap_Pag);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Cap_Dev_Rec);
                            Dr_Psp["Comprometido"] = String.Format("{0:n}", Cap_Com);
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Cap_xEje);
                            Dr_Psp["Tipo"] = "Cap";
                        }
                        else if (Dr_Psp["Tipo"].ToString().Trim().Equals("Con_Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", Con_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", Con_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", Con_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", Con_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", Con_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", Con_Pag);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Con_Dev_Rec);
                            Dr_Psp["Comprometido"] = String.Format("{0:n}", Con_Com);
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Con_xEje);
                            Dr_Psp["Tipo"] = "Con";
                        }
                        else if (Dr_Psp["Tipo"].ToString().Trim().Equals("PG_Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", PG_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", PG_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", PG_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", PG_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", PG_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", PG_Pag);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PG_Dev_Rec);
                            Dr_Psp["Comprometido"] = String.Format("{0:n}", PG_Com);
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PG_xEje);
                            Dr_Psp["Tipo"] = "PG";
                        }

                    }//fin foreach
                    #endregion
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al generar el presupuesto de egresos. Error[" + ex.Message + "]");
                }
                return Dt_Psp;
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Generar_Dt_Psp_Igr
            ///DESCRIPCIÓN          : Metodo para generar la tabla del presupuesto de ingresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 30/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private DataTable Generar_Dt_Psp_Ing(DataTable Dt_Ing)
            {
                DataTable Dt_Psp = new DataTable();
                DataRow Fila;
                Boolean SubConcepto = false;
                Boolean Programa = false;

                #region (variables)
                Double Tot_Est = 0.00;
                Double Tot_Amp = 0.00;
                Double Tot_Red = 0.00;
                Double Tot_Mod = 0.00;
                Double Tot_Dev = 0.00;
                Double Tot_Dev_Rec = 0.00;
                Double Tot_Rec = 0.00;
                Double Tot_xRec = 0.00;

                Double FF_Est = 0.00;
                Double PP_Est = 0.00;
                Double R_Est = 0.00;
                Double T_Est = 0.00;
                Double CL_Est = 0.00;
                Double C_Est = 0.00;

                Double FF_Amp = 0.00;
                Double PP_Amp = 0.00;
                Double R_Amp = 0.00;
                Double T_Amp = 0.00;
                Double CL_Amp = 0.00;
                Double C_Amp = 0.00;

                Double FF_Red = 0.00;
                Double PP_Red = 0.00;
                Double R_Red = 0.00;
                Double T_Red = 0.00;
                Double CL_Red = 0.00;
                Double C_Red = 0.00;

                Double FF_Mod = 0.00;
                Double PP_Mod = 0.00;
                Double R_Mod = 0.00;
                Double T_Mod = 0.00;
                Double CL_Mod = 0.00;
                Double C_Mod = 0.00;

                Double FF_Dev = 0.00;
                Double PP_Dev = 0.00;
                Double R_Dev = 0.00;
                Double T_Dev = 0.00;
                Double CL_Dev = 0.00;
                Double C_Dev = 0.00;

                Double FF_Dev_Rec = 0.00;
                Double PP_Dev_Rec = 0.00;
                Double R_Dev_Rec = 0.00;
                Double T_Dev_Rec = 0.00;
                Double CL_Dev_Rec = 0.00;
                Double C_Dev_Rec = 0.00;


                Double FF_Rec = 0.00;
                Double PP_Rec = 0.00;
                Double R_Rec = 0.00;
                Double T_Rec = 0.00;
                Double CL_Rec = 0.00;
                Double C_Rec = 0.00;

                Double FF_xRec = 0.00;
                Double PP_xRec = 0.00;
                Double R_xRec = 0.00;
                Double T_xRec = 0.00;
                Double CL_xRec = 0.00;
                Double C_xRec = 0.00;
                #endregion

                try
                {
                    #region(Tabla)
                    String FF_ID = Dt_Ing.Rows[0]["FF_ID"].ToString().Trim();
                    String PP_ID = Dt_Ing.Rows[0]["PP_ID"].ToString().Trim();
                    String R_ID = Dt_Ing.Rows[0]["R_ID"].ToString().Trim();
                    String T_ID = Dt_Ing.Rows[0]["T_ID"].ToString().Trim();
                    String CL_ID = Dt_Ing.Rows[0]["CL_ID"].ToString().Trim();
                    String C_ID = Dt_Ing.Rows[0]["C_ID"].ToString().Trim();
                    String SC_ID = Dt_Ing.Rows[0]["SC_ID"].ToString().Trim();

                    String FF = "****** " + Dt_Ing.Rows[0]["CLAVE_NOM_FF"].ToString().Trim();
                    String PP = "***** " + Dt_Ing.Rows[0]["CLAVE_NOM_PP"].ToString().Trim();
                    String R = "**** " + Dt_Ing.Rows[0]["CLAVE_NOM_RUBRO"].ToString().Trim();
                    String T = "*** " + Dt_Ing.Rows[0]["CLAVE_NOM_TIPO"].ToString().Trim();
                    String CL = "** " + Dt_Ing.Rows[0]["CLAVE_NOM_CLASE"].ToString().Trim();
                    String C = "* " + Dt_Ing.Rows[0]["CLAVE_NOM_CON"].ToString().Trim();
                    String SC = Dt_Ing.Rows[0]["CLAVE_NOM_SUBCON"].ToString().Trim();

                    if (!String.IsNullOrEmpty(PP))
                    {
                        Programa = true;
                    }
                    if (!String.IsNullOrEmpty(SC))
                    {
                        SubConcepto = true;
                    }
                    else
                    {
                        C_ID = String.Empty;
                    }

                    Dt_Psp.Columns.Add("Concepto", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Aprobado", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Ampliacion", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Reduccion", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Modificado", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Devengado", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Recaudado", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Dev_Rec", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Comprometido", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Por_Recaudar", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Tipo", System.Type.GetType("System.String"));

                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = "******* TOTAL";
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "Tot";
                    Dt_Psp.Rows.Add(Fila);

                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = FF.Trim();
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "FF_Tot";
                    Dt_Psp.Rows.Add(Fila);

                    if (Programa)
                    {
                        Fila = Dt_Psp.NewRow();
                        Fila["Concepto"] = PP.Trim();
                        Fila["Aprobado"] = String.Empty;
                        Fila["Ampliacion"] = String.Empty;
                        Fila["Reduccion"] = String.Empty;
                        Fila["Modificado"] = String.Empty;
                        Fila["Devengado"] = String.Empty;
                        Fila["Recaudado"] = String.Empty;
                        Fila["Dev_Rec"] = String.Empty;
                        Fila["Comprometido"] = String.Empty;
                        Fila["Por_Recaudar"] = String.Empty;
                        Fila["Tipo"] = "PP_Tot";
                        Dt_Psp.Rows.Add(Fila);
                    }

                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = R.Trim();
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "R_Tot";
                    Dt_Psp.Rows.Add(Fila);

                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = T.Trim();
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "T_Tot";
                    Dt_Psp.Rows.Add(Fila);

                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = CL.Trim();
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "CL_Tot";
                    Dt_Psp.Rows.Add(Fila);

                    if (SubConcepto)
                    {
                        Fila = Dt_Psp.NewRow();
                        Fila["Concepto"] = C.Trim();
                        Fila["Aprobado"] = String.Empty;
                        Fila["Ampliacion"] = String.Empty;
                        Fila["Reduccion"] = String.Empty;
                        Fila["Modificado"] = String.Empty;
                        Fila["Devengado"] = String.Empty;
                        Fila["Recaudado"] = String.Empty;
                        Fila["Dev_Rec"] = String.Empty;
                        Fila["Comprometido"] = String.Empty;
                        Fila["Por_Recaudar"] = String.Empty;
                        Fila["Tipo"] = "C_Tot";
                        Dt_Psp.Rows.Add(Fila);
                    }

                    #endregion

                    foreach (DataRow Dr in Dt_Ing.Rows)
                    {
                        Tot_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                        Tot_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                        Tot_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                        Tot_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                        Tot_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                        Tot_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());
                        Tot_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                        Tot_xRec += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());

                        if (String.IsNullOrEmpty(Dr["CLAVE_NOM_PP"].ToString().Trim()))
                        { Programa = false; }
                        else { Programa = true; }
                        if (String.IsNullOrEmpty(Dr["CLAVE_NOM_SUBCON"].ToString().Trim()))
                        { SubConcepto = false; }
                        else { SubConcepto = true; }

                        if (FF_ID.Trim().Equals(Dr["FF_ID"].ToString().Trim()))
                        {
                            //sumamos 
                            FF_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                            FF_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            FF_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            FF_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            FF_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            FF_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());
                            FF_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                            FF_xRec += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());

                            if (PP_ID.Trim().Equals(Dr["PP_ID"].ToString().Trim()))
                            {
                                //sumamos 
                                PP_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                PP_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                PP_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                PP_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                PP_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                PP_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());
                                PP_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                PP_xRec += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());

                                if (R_ID.Trim().Equals(Dr["R_ID"].ToString().Trim()))
                                {
                                    //sumamos 
                                    R_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                    R_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                    R_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                    R_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                    R_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                    R_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());
                                    R_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                    R_xRec += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());

                                    if (T_ID.Trim().Equals(Dr["T_ID"].ToString().Trim()))
                                    {
                                        //sumamos 
                                        T_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                        T_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                        T_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                        T_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                        T_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                        T_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());
                                        T_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                        T_xRec += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());

                                        if (CL_ID.Trim().Equals(Dr["CL_ID"].ToString().Trim()))
                                        {
                                            //sumamos 
                                            CL_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                            CL_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                            CL_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                            CL_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                            CL_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                            CL_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());
                                            CL_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                            CL_xRec += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());

                                            if (C_ID.Trim().Equals(Dr["C_ID"].ToString().Trim()))
                                            {
                                                C_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                                C_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                                C_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                                C_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                                C_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                                C_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());
                                                C_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                                C_xRec += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());

                                                if (SubConcepto)
                                                {
                                                    //sumamos 
                                                    Fila = Dt_Psp.NewRow();
                                                    Fila["Concepto"] = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();
                                                    Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                                    Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                                    Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                                    Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                                    Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                                    Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                                    Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                                    Fila["Comprometido"] = "0.00";
                                                    Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                                    Fila["Tipo"] = "SC";
                                                    Dt_Psp.Rows.Add(Fila);

                                                }
                                            }
                                            else
                                            {
                                                #region (else C)
                                                foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                                                {
                                                    if (SubConcepto)
                                                    {
                                                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("C_Tot"))
                                                        {
                                                            Dr_Psp["Aprobado"] = String.Format("{0:n}", C_Est);
                                                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", C_Amp);
                                                            Dr_Psp["Reduccion"] = String.Format("{0:n}", C_Red);
                                                            Dr_Psp["Modificado"] = String.Format("{0:n}", C_Mod);
                                                            Dr_Psp["Devengado"] = String.Format("{0:n}", C_Dev);
                                                            Dr_Psp["Recaudado"] = String.Format("{0:n}", C_Rec);
                                                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", C_Dev_Rec);
                                                            Dr_Psp["Comprometido"] = "0.00";
                                                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", C_xRec);
                                                            Dr_Psp["Tipo"] = "C";
                                                        }
                                                    }
                                                }//fin foreach

                                                //limpiamos las variables
                                                C_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                                C_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                                C_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                                C_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                                C_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                                C_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                                C_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                                C_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                                C_ID = Dr["C_ID"].ToString().Trim();
                                                SC_ID = Dr["SC_ID"].ToString().Trim();

                                                C = "* " + Dr["CLAVE_NOM_CON"].ToString().Trim();
                                                SC = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();

                                                if (!String.IsNullOrEmpty(SC))
                                                { SubConcepto = true; }
                                                else { SubConcepto = false; }

                                                if (SubConcepto)
                                                {
                                                    Fila = Dt_Psp.NewRow();
                                                    Fila["Concepto"] = C.Trim();
                                                    Fila["Aprobado"] = String.Empty;
                                                    Fila["Ampliacion"] = String.Empty;
                                                    Fila["Reduccion"] = String.Empty;
                                                    Fila["Modificado"] = String.Empty;
                                                    Fila["Devengado"] = String.Empty;
                                                    Fila["Recaudado"] = String.Empty;
                                                    Fila["Dev_Rec"] = String.Empty;
                                                    Fila["Comprometido"] = String.Empty;
                                                    Fila["Por_Recaudar"] = String.Empty;
                                                    Fila["Tipo"] = "C_Tot";
                                                    Dt_Psp.Rows.Add(Fila);
                                                }

                                                if (SubConcepto)
                                                {
                                                    Fila = Dt_Psp.NewRow();
                                                    Fila["Concepto"] = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();
                                                    Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                                    Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                                    Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                                    Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                                    Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                                    Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                                    Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                                    Fila["Comprometido"] = "0.00";
                                                    Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                                    Fila["Tipo"] = "SC";
                                                    Dt_Psp.Rows.Add(Fila);
                                                }
                                                else
                                                {
                                                    Fila = Dt_Psp.NewRow();
                                                    Fila["Concepto"] = Dr["CLAVE_NOM_CON"].ToString().Trim();
                                                    Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                                    Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                                    Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                                    Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                                    Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                                    Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                                    Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                                    Fila["Comprometido"] = "0.00";
                                                    Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                                    Fila["Tipo"] = "C";
                                                    Dt_Psp.Rows.Add(Fila);
                                                }
                                                #endregion
                                            }
                                        }
                                        else
                                        {
                                            #region (else CL)
                                            foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                                            {
                                                if (Dr_Psp["Tipo"].ToString().Trim().Equals("CL_Tot"))
                                                {
                                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", CL_Est);
                                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", CL_Amp);
                                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", CL_Red);
                                                    Dr_Psp["Modificado"] = String.Format("{0:n}", CL_Mod);
                                                    Dr_Psp["Devengado"] = String.Format("{0:n}", CL_Dev);
                                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", CL_Rec);
                                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", CL_Dev_Rec);
                                                    Dr_Psp["Comprometido"] = "0.00";
                                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", CL_xRec);
                                                    Dr_Psp["Tipo"] = "CL";
                                                }

                                                if (SubConcepto)
                                                {
                                                    if (Dr_Psp["Tipo"].ToString().Trim().Equals("C_Tot"))
                                                    {
                                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", C_Est);
                                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", C_Amp);
                                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", C_Red);
                                                        Dr_Psp["Modificado"] = String.Format("{0:n}", C_Mod);
                                                        Dr_Psp["Devengado"] = String.Format("{0:n}", CL_Dev);
                                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", C_Rec);
                                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", CL_Dev_Rec);
                                                        Dr_Psp["Comprometido"] = "0.00";
                                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", C_xRec);
                                                        Dr_Psp["Tipo"] = "C";
                                                    }
                                                }
                                            }//fin foreach

                                            //limpiamos las variables
                                            CL_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                            CL_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                            CL_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                            CL_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                            CL_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                            CL_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                            CL_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                            CL_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                            C_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                            C_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                            C_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                            C_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                            C_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                            C_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                            C_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                            C_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                            CL_ID = Dr["CL_ID"].ToString().Trim();
                                            C_ID = Dr["C_ID"].ToString().Trim();
                                            SC_ID = Dr["SC_ID"].ToString().Trim();

                                            CL = "** " + Dr["CLAVE_NOM_CLASE"].ToString().Trim();
                                            C = "* " + Dr["CLAVE_NOM_CON"].ToString().Trim();
                                            SC = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();

                                            if (!String.IsNullOrEmpty(SC))
                                            { SubConcepto = true; }
                                            else { SubConcepto = false; }

                                            Fila = Dt_Psp.NewRow();
                                            Fila["Concepto"] = CL.Trim();
                                            Fila["Aprobado"] = String.Empty;
                                            Fila["Ampliacion"] = String.Empty;
                                            Fila["Reduccion"] = String.Empty;
                                            Fila["Modificado"] = String.Empty;
                                            Fila["Devengado"] = String.Empty;
                                            Fila["Recaudado"] = String.Empty;
                                            Fila["Dev_Rec"] = String.Empty;
                                            Fila["Comprometido"] = String.Empty;
                                            Fila["Por_Recaudar"] = String.Empty;
                                            Fila["Tipo"] = "CL_Tot";
                                            Dt_Psp.Rows.Add(Fila);

                                            if (SubConcepto)
                                            {
                                                Fila = Dt_Psp.NewRow();
                                                Fila["Concepto"] = C.Trim();
                                                Fila["Aprobado"] = String.Empty;
                                                Fila["Ampliacion"] = String.Empty;
                                                Fila["Reduccion"] = String.Empty;
                                                Fila["Modificado"] = String.Empty;
                                                Fila["Devengado"] = String.Empty;
                                                Fila["Recaudado"] = String.Empty;
                                                Fila["Dev_Rec"] = String.Empty;
                                                Fila["Comprometido"] = String.Empty;
                                                Fila["Por_Recaudar"] = String.Empty;
                                                Fila["Tipo"] = "C_Tot";
                                                Dt_Psp.Rows.Add(Fila);
                                            }

                                            if (SubConcepto)
                                            {
                                                Fila = Dt_Psp.NewRow();
                                                Fila["Concepto"] = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();
                                                Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                                Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                                Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                                Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                                Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                                Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                                Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                                Fila["Comprometido"] = "0.00";
                                                Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                                Fila["Tipo"] = "SC";
                                                Dt_Psp.Rows.Add(Fila);
                                            }
                                            else
                                            {
                                                Fila = Dt_Psp.NewRow();
                                                Fila["Concepto"] = Dr["CLAVE_NOM_CON"].ToString().Trim();
                                                Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                                Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                                Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                                Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                                Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                                Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                                Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                                Fila["Comprometido"] = "0.00";
                                                Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                                Fila["Tipo"] = "C";
                                                Dt_Psp.Rows.Add(Fila);
                                            }
                                            #endregion
                                        }
                                    }
                                    else
                                    {
                                        #region (else T)
                                        foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                                        {
                                            if (Dr_Psp["Tipo"].ToString().Trim().Equals("T_Tot"))
                                            {
                                                Dr_Psp["Aprobado"] = String.Format("{0:n}", T_Est);
                                                Dr_Psp["Ampliacion"] = String.Format("{0:n}", T_Amp);
                                                Dr_Psp["Reduccion"] = String.Format("{0:n}", T_Red);
                                                Dr_Psp["Modificado"] = String.Format("{0:n}", T_Mod);
                                                Dr_Psp["Devengado"] = String.Format("{0:n}", T_Dev);
                                                Dr_Psp["Recaudado"] = String.Format("{0:n}", T_Rec);
                                                Dr_Psp["Dev_Rec"] = String.Format("{0:n}", T_Dev_Rec);
                                                Dr_Psp["Comprometido"] = "0.00";
                                                Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", T_xRec);
                                                Dr_Psp["Tipo"] = "T";
                                            }
                                            if (Dr_Psp["Tipo"].ToString().Trim().Equals("CL_Tot"))
                                            {
                                                Dr_Psp["Aprobado"] = String.Format("{0:n}", CL_Est);
                                                Dr_Psp["Ampliacion"] = String.Format("{0:n}", CL_Amp);
                                                Dr_Psp["Reduccion"] = String.Format("{0:n}", CL_Red);
                                                Dr_Psp["Modificado"] = String.Format("{0:n}", CL_Mod);
                                                Dr_Psp["Devengado"] = String.Format("{0:n}", CL_Dev);
                                                Dr_Psp["Recaudado"] = String.Format("{0:n}", CL_Rec);
                                                Dr_Psp["Dev_Rec"] = String.Format("{0:n}", CL_Dev_Rec);
                                                Dr_Psp["Comprometido"] = "0.00";
                                                Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", CL_xRec);
                                                Dr_Psp["Tipo"] = "CL";
                                            }

                                            if (SubConcepto)
                                            {
                                                if (Dr_Psp["Tipo"].ToString().Trim().Equals("C_Tot"))
                                                {
                                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", C_Est);
                                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", C_Amp);
                                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", C_Red);
                                                    Dr_Psp["Modificado"] = String.Format("{0:n}", C_Mod);
                                                    Dr_Psp["Devengado"] = String.Format("{0:n}", C_Dev);
                                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", C_Rec);
                                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", C_Dev_Rec);
                                                    Dr_Psp["Comprometido"] = "0.00";
                                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", C_xRec);
                                                    Dr_Psp["Tipo"] = "C";
                                                }
                                            }
                                        }//fin foreach

                                        //limpiamos las variables
                                        T_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                        T_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                        T_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                        T_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                        T_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                        T_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                        T_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                        T_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                        CL_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                        CL_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                        CL_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                        CL_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                        CL_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                        CL_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                        CL_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                        CL_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                        C_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                        C_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                        C_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                        C_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                        C_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                        C_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                        C_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                        C_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());


                                        T_ID = Dr["T_ID"].ToString().Trim();
                                        CL_ID = Dr["CL_ID"].ToString().Trim();
                                        C_ID = Dr["C_ID"].ToString().Trim();
                                        SC_ID = Dr["SC_ID"].ToString().Trim();

                                        T = "*** " + Dr["CLAVE_NOM_TIPO"].ToString().Trim();
                                        CL = "** " + Dr["CLAVE_NOM_CLASE"].ToString().Trim();
                                        C = "* " + Dr["CLAVE_NOM_CON"].ToString().Trim();
                                        SC = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();

                                        if (!String.IsNullOrEmpty(SC))
                                        { SubConcepto = true; }
                                        else { SubConcepto = false; }

                                        Fila = Dt_Psp.NewRow();
                                        Fila["Concepto"] = T.Trim();
                                        Fila["Aprobado"] = String.Empty;
                                        Fila["Ampliacion"] = String.Empty;
                                        Fila["Reduccion"] = String.Empty;
                                        Fila["Modificado"] = String.Empty;
                                        Fila["Devengado"] = String.Empty;
                                        Fila["Recaudado"] = String.Empty;
                                        Fila["Dev_Rec"] = String.Empty;
                                        Fila["Comprometido"] = String.Empty;
                                        Fila["Por_Recaudar"] = String.Empty;
                                        Fila["Tipo"] = "T_Tot";
                                        Dt_Psp.Rows.Add(Fila);

                                        Fila = Dt_Psp.NewRow();
                                        Fila["Concepto"] = CL.Trim();
                                        Fila["Aprobado"] = String.Empty;
                                        Fila["Ampliacion"] = String.Empty;
                                        Fila["Reduccion"] = String.Empty;
                                        Fila["Modificado"] = String.Empty;
                                        Fila["Devengado"] = String.Empty;
                                        Fila["Recaudado"] = String.Empty;
                                        Fila["Dev_Rec"] = String.Empty;
                                        Fila["Comprometido"] = String.Empty;
                                        Fila["Por_Recaudar"] = String.Empty;
                                        Fila["Tipo"] = "CL_Tot";
                                        Dt_Psp.Rows.Add(Fila);

                                        if (SubConcepto)
                                        {
                                            Fila = Dt_Psp.NewRow();
                                            Fila["Concepto"] = C.Trim();
                                            Fila["Aprobado"] = String.Empty;
                                            Fila["Ampliacion"] = String.Empty;
                                            Fila["Reduccion"] = String.Empty;
                                            Fila["Modificado"] = String.Empty;
                                            Fila["Devengado"] = String.Empty;
                                            Fila["Recaudado"] = String.Empty;
                                            Fila["Dev_Rec"] = String.Empty;
                                            Fila["Comprometido"] = String.Empty;
                                            Fila["Por_Recaudar"] = String.Empty;
                                            Fila["Tipo"] = "C_Tot";
                                            Dt_Psp.Rows.Add(Fila);
                                        }

                                        if (SubConcepto)
                                        {
                                            Fila = Dt_Psp.NewRow();
                                            Fila["Concepto"] = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();
                                            Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                            Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                            Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                            Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                            Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                            Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                            Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                            Fila["Comprometido"] = "0.00";
                                            Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                            Fila["Tipo"] = "SC";
                                            Dt_Psp.Rows.Add(Fila);
                                        }
                                        else
                                        {
                                            Fila = Dt_Psp.NewRow();
                                            Fila["Concepto"] = Dr["CLAVE_NOM_CON"].ToString().Trim();
                                            Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                            Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                            Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                            Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                            Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                            Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                            Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                            Fila["Comprometido"] = "0.00";
                                            Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                            Fila["Tipo"] = "C";
                                            Dt_Psp.Rows.Add(Fila);
                                        }
                                        #endregion
                                    }
                                }
                                else
                                {
                                    #region (else R)
                                    foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                                    {
                                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("R_Tot"))
                                        {
                                            Dr_Psp["Aprobado"] = String.Format("{0:n}", R_Est);
                                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", R_Amp);
                                            Dr_Psp["Reduccion"] = String.Format("{0:n}", R_Red);
                                            Dr_Psp["Modificado"] = String.Format("{0:n}", R_Mod);
                                            Dr_Psp["Devengado"] = String.Format("{0:n}", R_Dev);
                                            Dr_Psp["Recaudado"] = String.Format("{0:n}", R_Rec);
                                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", R_Dev_Rec);
                                            Dr_Psp["Comprometido"] = "0.00";
                                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", R_xRec);
                                            Dr_Psp["Tipo"] = "R";
                                        }
                                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("T_Tot"))
                                        {
                                            Dr_Psp["Aprobado"] = String.Format("{0:n}", T_Est);
                                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", T_Amp);
                                            Dr_Psp["Reduccion"] = String.Format("{0:n}", T_Red);
                                            Dr_Psp["Modificado"] = String.Format("{0:n}", T_Mod);
                                            Dr_Psp["Devengado"] = String.Format("{0:n}", T_Dev);
                                            Dr_Psp["Recaudado"] = String.Format("{0:n}", T_Rec);
                                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", T_Dev_Rec);
                                            Dr_Psp["Comprometido"] = "0.00";
                                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", T_xRec);
                                            Dr_Psp["Tipo"] = "T";
                                        }
                                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("CL_Tot"))
                                        {
                                            Dr_Psp["Aprobado"] = String.Format("{0:n}", CL_Est);
                                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", CL_Amp);
                                            Dr_Psp["Reduccion"] = String.Format("{0:n}", CL_Red);
                                            Dr_Psp["Modificado"] = String.Format("{0:n}", CL_Mod);
                                            Dr_Psp["Devengado"] = String.Format("{0:n}", CL_Dev);
                                            Dr_Psp["Recaudado"] = String.Format("{0:n}", CL_Rec);
                                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", CL_Dev_Rec);
                                            Dr_Psp["Comprometido"] = "0.00";
                                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", CL_xRec);
                                            Dr_Psp["Tipo"] = "CL";
                                        }

                                        if (SubConcepto)
                                        {
                                            if (Dr_Psp["Tipo"].ToString().Trim().Equals("C_Tot"))
                                            {
                                                Dr_Psp["Aprobado"] = String.Format("{0:n}", C_Est);
                                                Dr_Psp["Ampliacion"] = String.Format("{0:n}", C_Amp);
                                                Dr_Psp["Reduccion"] = String.Format("{0:n}", C_Red);
                                                Dr_Psp["Modificado"] = String.Format("{0:n}", C_Mod);
                                                Dr_Psp["Devengado"] = String.Format("{0:n}", CL_Dev);
                                                Dr_Psp["Recaudado"] = String.Format("{0:n}", C_Rec);
                                                Dr_Psp["Dev_Rec"] = String.Format("{0:n}", CL_Dev_Rec);
                                                Dr_Psp["Comprometido"] = "0.00";
                                                Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", C_xRec);
                                                Dr_Psp["Tipo"] = "C";
                                            }
                                        }
                                    }//fin foreach

                                    //limpiamos las variables
                                    R_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                    R_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                    R_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                    R_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                    R_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                    R_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                    R_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                    R_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                    //limpiamos las variables
                                    T_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                    T_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                    T_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                    T_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                    T_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                    T_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                    T_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                    T_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                    CL_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                    CL_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                    CL_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                    CL_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                    CL_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                    CL_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                    CL_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                    CL_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                    C_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                    C_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                    C_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                    C_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                    C_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                    C_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                    C_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                    C_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                    R_ID = Dr["R_ID"].ToString().Trim();
                                    T_ID = Dr["T_ID"].ToString().Trim();
                                    CL_ID = Dr["CL_ID"].ToString().Trim();
                                    C_ID = Dr["C_ID"].ToString().Trim();
                                    SC_ID = Dr["SC_ID"].ToString().Trim();

                                    R = "**** " + Dr["CLAVE_NOM_RUBRO"].ToString().Trim();
                                    T = "*** " + Dr["CLAVE_NOM_TIPO"].ToString().Trim();
                                    CL = "** " + Dr["CLAVE_NOM_CLASE"].ToString().Trim();
                                    C = "* " + Dr["CLAVE_NOM_CON"].ToString().Trim();
                                    SC = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();

                                    if (!String.IsNullOrEmpty(SC))
                                    { SubConcepto = true; }
                                    else { SubConcepto = false; }


                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = R.Trim();
                                    Fila["Aprobado"] = String.Empty;
                                    Fila["Ampliacion"] = String.Empty;
                                    Fila["Reduccion"] = String.Empty;
                                    Fila["Modificado"] = String.Empty;
                                    Fila["Devengado"] = String.Empty;
                                    Fila["Recaudado"] = String.Empty;
                                    Fila["Dev_Rec"] = String.Empty;
                                    Fila["Comprometido"] = String.Empty;
                                    Fila["Por_Recaudar"] = String.Empty;
                                    Fila["Tipo"] = "R_Tot";
                                    Dt_Psp.Rows.Add(Fila);

                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = T.Trim();
                                    Fila["Aprobado"] = String.Empty;
                                    Fila["Ampliacion"] = String.Empty;
                                    Fila["Reduccion"] = String.Empty;
                                    Fila["Modificado"] = String.Empty;
                                    Fila["Devengado"] = String.Empty;
                                    Fila["Recaudado"] = String.Empty;
                                    Fila["Dev_Rec"] = String.Empty;
                                    Fila["Comprometido"] = String.Empty;
                                    Fila["Por_Recaudar"] = String.Empty;
                                    Fila["Tipo"] = "T_Tot";
                                    Dt_Psp.Rows.Add(Fila);

                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = CL.Trim();
                                    Fila["Aprobado"] = String.Empty;
                                    Fila["Ampliacion"] = String.Empty;
                                    Fila["Reduccion"] = String.Empty;
                                    Fila["Modificado"] = String.Empty;
                                    Fila["Devengado"] = String.Empty;
                                    Fila["Recaudado"] = String.Empty;
                                    Fila["Dev_Rec"] = String.Empty;
                                    Fila["Comprometido"] = String.Empty;
                                    Fila["Por_Recaudar"] = String.Empty;
                                    Fila["Tipo"] = "CL_Tot";
                                    Dt_Psp.Rows.Add(Fila);

                                    if (SubConcepto)
                                    {
                                        Fila = Dt_Psp.NewRow();
                                        Fila["Concepto"] = C.Trim();
                                        Fila["Aprobado"] = String.Empty;
                                        Fila["Ampliacion"] = String.Empty;
                                        Fila["Reduccion"] = String.Empty;
                                        Fila["Modificado"] = String.Empty;
                                        Fila["Devengado"] = String.Empty;
                                        Fila["Recaudado"] = String.Empty;
                                        Fila["Dev_Rec"] = String.Empty;
                                        Fila["Comprometido"] = String.Empty;
                                        Fila["Por_Recaudar"] = String.Empty;
                                        Fila["Tipo"] = "C_Tot";
                                        Dt_Psp.Rows.Add(Fila);
                                    }


                                    if (SubConcepto)
                                    {
                                        Fila = Dt_Psp.NewRow();
                                        Fila["Concepto"] = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();
                                        Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                        Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                        Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                        Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                        Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                        Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                        Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                        Fila["Comprometido"] = "0.00";
                                        Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                        Fila["Tipo"] = "SC";
                                        Dt_Psp.Rows.Add(Fila);
                                    }
                                    else
                                    {
                                        Fila = Dt_Psp.NewRow();
                                        Fila["Concepto"] = Dr["CLAVE_NOM_CON"].ToString().Trim();
                                        Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                        Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                        Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                        Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                        Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                        Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                        Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                        Fila["Comprometido"] = "0.00";
                                        Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                        Fila["Tipo"] = "C";
                                        Dt_Psp.Rows.Add(Fila);
                                    }
                                    #endregion
                                }
                            }
                            else
                            {
                                #region (else PP)
                                foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                                {
                                    if (Programa)
                                    {
                                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("PP_Tot"))
                                        {
                                            Dr_Psp["Aprobado"] = String.Format("{0:n}", PP_Est);
                                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", PP_Amp);
                                            Dr_Psp["Reduccion"] = String.Format("{0:n}", PP_Red);
                                            Dr_Psp["Modificado"] = String.Format("{0:n}", PP_Mod);
                                            Dr_Psp["Devengado"] = String.Format("{0:n}", PP_Dev);
                                            Dr_Psp["Recaudado"] = String.Format("{0:n}", PP_Rec);
                                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PP_Dev_Rec);
                                            Dr_Psp["Comprometido"] = "0.00";
                                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PP_xRec);
                                            Dr_Psp["Tipo"] = "PP";
                                        }
                                    }

                                    if (Dr_Psp["Tipo"].ToString().Trim().Equals("R_Tot"))
                                    {
                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", R_Est);
                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", R_Amp);
                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", R_Red);
                                        Dr_Psp["Modificado"] = String.Format("{0:n}", R_Mod);
                                        Dr_Psp["Devengado"] = String.Format("{0:n}", R_Dev);
                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", R_Rec);
                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", R_Dev_Rec);
                                        Dr_Psp["Comprometido"] = "0.00";
                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", R_xRec);
                                        Dr_Psp["Tipo"] = "R";
                                    }
                                    if (Dr_Psp["Tipo"].ToString().Trim().Equals("T_Tot"))
                                    {
                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", T_Est);
                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", T_Amp);
                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", T_Red);
                                        Dr_Psp["Modificado"] = String.Format("{0:n}", T_Mod);
                                        Dr_Psp["Devengado"] = String.Format("{0:n}", T_Dev);
                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", T_Rec);
                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", T_Dev_Rec);
                                        Dr_Psp["Comprometido"] = "0.00";
                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", T_xRec);
                                        Dr_Psp["Tipo"] = "T";
                                    }
                                    if (Dr_Psp["Tipo"].ToString().Trim().Equals("CL_Tot"))
                                    {
                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", CL_Est);
                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", CL_Amp);
                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", CL_Red);
                                        Dr_Psp["Modificado"] = String.Format("{0:n}", CL_Mod);
                                        Dr_Psp["Devengado"] = String.Format("{0:n}", CL_Dev);
                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", CL_Rec);
                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", CL_Dev_Rec);
                                        Dr_Psp["Comprometido"] = "0.00";
                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", CL_xRec);
                                        Dr_Psp["Tipo"] = "CL";
                                    }

                                    if (SubConcepto)
                                    {
                                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("C_Tot"))
                                        {
                                            Dr_Psp["Aprobado"] = String.Format("{0:n}", C_Est);
                                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", C_Amp);
                                            Dr_Psp["Reduccion"] = String.Format("{0:n}", C_Red);
                                            Dr_Psp["Modificado"] = String.Format("{0:n}", C_Mod);
                                            Dr_Psp["Devengado"] = String.Format("{0:n}", C_Dev);
                                            Dr_Psp["Recaudado"] = String.Format("{0:n}", C_Rec);
                                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", C_Dev_Rec);
                                            Dr_Psp["Comprometido"] = "0.00";
                                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", C_xRec);
                                            Dr_Psp["Tipo"] = "C";
                                        }
                                    }
                                }//fin foreach

                                //limpiamos las variables
                                PP_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                PP_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                PP_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                PP_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                PP_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                PP_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                PP_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                PP_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                //limpiamos las variables
                                R_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                R_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                R_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                R_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                R_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                R_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                R_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                R_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                //limpiamos las variables
                                T_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                T_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                T_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                T_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                T_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                T_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                T_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                T_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                CL_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                CL_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                CL_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                CL_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                CL_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                CL_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                CL_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                CL_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                C_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                C_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                C_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                C_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                C_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                C_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                C_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                C_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                PP_ID = Dr["PP_ID"].ToString().Trim();
                                R_ID = Dr["R_ID"].ToString().Trim();
                                T_ID = Dr["T_ID"].ToString().Trim();
                                CL_ID = Dr["CL_ID"].ToString().Trim();
                                C_ID = Dr["C_ID"].ToString().Trim();
                                SC_ID = Dr["SC_ID"].ToString().Trim();

                                PP = "***** " + Dr["CLAVE_NOM_PP"].ToString().Trim();
                                R = "**** " + Dr["CLAVE_NOM_RUBRO"].ToString().Trim();
                                T = "*** " + Dr["CLAVE_NOM_TIPO"].ToString().Trim();
                                CL = "** " + Dr["CLAVE_NOM_CLASE"].ToString().Trim();
                                C = "* " + Dr["CLAVE_NOM_CON"].ToString().Trim();
                                SC = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();

                                if (!String.IsNullOrEmpty(PP))
                                { Programa = true; }
                                else { Programa = false; }
                                if (!String.IsNullOrEmpty(SC))
                                { SubConcepto = true; }
                                else { SubConcepto = false; }

                                if (Programa)
                                {
                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = PP.Trim();
                                    Fila["Aprobado"] = String.Empty;
                                    Fila["Ampliacion"] = String.Empty;
                                    Fila["Reduccion"] = String.Empty;
                                    Fila["Modificado"] = String.Empty;
                                    Fila["Devengado"] = String.Empty;
                                    Fila["Recaudado"] = String.Empty;
                                    Fila["Dev_Rec"] = String.Empty;
                                    Fila["Comprometido"] = String.Empty;
                                    Fila["Por_Recaudar"] = String.Empty;
                                    Fila["Tipo"] = "PP_Tot";
                                    Dt_Psp.Rows.Add(Fila);
                                }

                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = R.Trim();
                                Fila["Aprobado"] = String.Empty;
                                Fila["Ampliacion"] = String.Empty;
                                Fila["Reduccion"] = String.Empty;
                                Fila["Modificado"] = String.Empty;
                                Fila["Devengado"] = String.Empty;
                                Fila["Recaudado"] = String.Empty;
                                Fila["Dev_Rec"] = String.Empty;
                                Fila["Comprometido"] = String.Empty;
                                Fila["Por_Recaudar"] = String.Empty;
                                Fila["Tipo"] = "R_Tot";
                                Dt_Psp.Rows.Add(Fila);

                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = T.Trim();
                                Fila["Aprobado"] = String.Empty;
                                Fila["Ampliacion"] = String.Empty;
                                Fila["Reduccion"] = String.Empty;
                                Fila["Modificado"] = String.Empty;
                                Fila["Devengado"] = String.Empty;
                                Fila["Recaudado"] = String.Empty;
                                Fila["Dev_Rec"] = String.Empty;
                                Fila["Comprometido"] = String.Empty;
                                Fila["Por_Recaudar"] = String.Empty;
                                Fila["Tipo"] = "T_Tot";
                                Dt_Psp.Rows.Add(Fila);

                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = CL.Trim();
                                Fila["Aprobado"] = String.Empty;
                                Fila["Ampliacion"] = String.Empty;
                                Fila["Reduccion"] = String.Empty;
                                Fila["Modificado"] = String.Empty;
                                Fila["Devengado"] = String.Empty;
                                Fila["Recaudado"] = String.Empty;
                                Fila["Dev_Rec"] = String.Empty;
                                Fila["Comprometido"] = String.Empty;
                                Fila["Por_Recaudar"] = String.Empty;
                                Fila["Tipo"] = "CL_Tot";
                                Dt_Psp.Rows.Add(Fila);

                                if (SubConcepto)
                                {
                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = C.Trim();
                                    Fila["Aprobado"] = String.Empty;
                                    Fila["Ampliacion"] = String.Empty;
                                    Fila["Reduccion"] = String.Empty;
                                    Fila["Modificado"] = String.Empty;
                                    Fila["Devengado"] = String.Empty;
                                    Fila["Recaudado"] = String.Empty;
                                    Fila["Dev_Rec"] = String.Empty;
                                    Fila["Comprometido"] = String.Empty;
                                    Fila["Por_Recaudar"] = String.Empty;
                                    Fila["Tipo"] = "C_Tot";
                                    Dt_Psp.Rows.Add(Fila);
                                }


                                if (SubConcepto)
                                {
                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();
                                    Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                    Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                    Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                    Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                    Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                    Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                    Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                    Fila["Comprometido"] = "0.00";
                                    Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                    Fila["Tipo"] = "SC";
                                    Dt_Psp.Rows.Add(Fila);
                                }
                                else
                                {
                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = Dr["CLAVE_NOM_CON"].ToString().Trim();
                                    Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                    Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                    Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                    Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                    Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                    Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                    Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                    Fila["Comprometido"] = "0.00";
                                    Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                    Fila["Tipo"] = "C";
                                    Dt_Psp.Rows.Add(Fila);
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            #region (else FF)
                            foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                            {
                                if (Dr_Psp["Tipo"].ToString().Trim().Equals("FF_Tot"))
                                {
                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", FF_Est);
                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", FF_Amp);
                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", FF_Red);
                                    Dr_Psp["Modificado"] = String.Format("{0:n}", FF_Mod);
                                    Dr_Psp["Devengado"] = String.Format("{0:n}", FF_Dev);
                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", FF_Rec);
                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", FF_Dev_Rec);
                                    Dr_Psp["Comprometido"] = "0.00";
                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", FF_xRec);
                                    Dr_Psp["Tipo"] = "FF";
                                }

                                if (Programa)
                                {
                                    if (Dr_Psp["Tipo"].ToString().Trim().Equals("PP_Tot"))
                                    {
                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", PP_Est);
                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", PP_Amp);
                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", PP_Red);
                                        Dr_Psp["Modificado"] = String.Format("{0:n}", PP_Mod);
                                        Dr_Psp["Devengado"] = String.Format("{0:n}", PP_Dev);
                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", PP_Rec);
                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PP_Dev_Rec);
                                        Dr_Psp["Comprometido"] = "0.00";
                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PP_xRec);
                                        Dr_Psp["Tipo"] = "PP";
                                    }
                                }

                                if (Dr_Psp["Tipo"].ToString().Trim().Equals("R_Tot"))
                                {
                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", R_Est);
                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", R_Amp);
                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", R_Red);
                                    Dr_Psp["Modificado"] = String.Format("{0:n}", R_Mod);
                                    Dr_Psp["Devengado"] = String.Format("{0:n}", R_Dev);
                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", R_Rec);
                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", R_Dev_Rec);
                                    Dr_Psp["Comprometido"] = "0.00";
                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", R_xRec);
                                    Dr_Psp["Tipo"] = "R";
                                }
                                if (Dr_Psp["Tipo"].ToString().Trim().Equals("T_Tot"))
                                {
                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", T_Est);
                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", T_Amp);
                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", T_Red);
                                    Dr_Psp["Modificado"] = String.Format("{0:n}", T_Mod);
                                    Dr_Psp["Devengado"] = String.Format("{0:n}", T_Dev);
                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", T_Rec);
                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", T_Dev_Rec);
                                    Dr_Psp["Comprometido"] = "0.00";
                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", T_xRec);
                                    Dr_Psp["Tipo"] = "T";
                                }
                                if (Dr_Psp["Tipo"].ToString().Trim().Equals("CL_Tot"))
                                {
                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", CL_Est);
                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", CL_Amp);
                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", CL_Red);
                                    Dr_Psp["Modificado"] = String.Format("{0:n}", CL_Mod);
                                    Dr_Psp["Devengado"] = String.Format("{0:n}", CL_Dev);
                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", CL_Rec);
                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", CL_Dev_Rec);
                                    Dr_Psp["Comprometido"] = "0.00";
                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", CL_xRec);
                                    Dr_Psp["Tipo"] = "CL";
                                }

                                if (SubConcepto)
                                {
                                    if (Dr_Psp["Tipo"].ToString().Trim().Equals("C_Tot"))
                                    {
                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", C_Est);
                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", C_Amp);
                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", C_Red);
                                        Dr_Psp["Modificado"] = String.Format("{0:n}", C_Mod);
                                        Dr_Psp["Devengado"] = String.Format("{0:n}", C_Dev);
                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", C_Rec);
                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", C_Dev_Rec);
                                        Dr_Psp["Comprometido"] = "0.00";
                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", C_xRec);
                                        Dr_Psp["Tipo"] = "C";
                                    }
                                }
                            }//fin foreach

                            //limpiamos las variables
                            FF_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                            FF_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            FF_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            FF_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            FF_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                            FF_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                            FF_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            FF_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                            //limpiamos las variables
                            PP_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                            PP_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            PP_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            PP_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            PP_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                            PP_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                            PP_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            PP_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                            //limpiamos las variables
                            R_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                            R_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            R_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            R_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            R_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                            R_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                            R_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            R_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                            //limpiamos las variables
                            T_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                            T_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            T_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            T_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            T_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                            T_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                            T_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            T_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                            CL_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                            CL_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            CL_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            CL_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            CL_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                            CL_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                            CL_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            CL_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                            C_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                            C_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            C_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            C_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            C_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                            C_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                            C_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            C_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                            FF_ID = Dr["FF_ID"].ToString().Trim();
                            PP_ID = Dr["PP_ID"].ToString().Trim();
                            R_ID = Dr["R_ID"].ToString().Trim();
                            T_ID = Dr["T_ID"].ToString().Trim();
                            CL_ID = Dr["CL_ID"].ToString().Trim();
                            C_ID = Dr["C_ID"].ToString().Trim();
                            SC_ID = Dr["SC_ID"].ToString().Trim();

                            FF = "****** " + Dr["CLAVE_NOM_FF"].ToString().Trim();
                            PP = "***** " + Dr["CLAVE_NOM_PP"].ToString().Trim();
                            R = "**** " + Dr["CLAVE_NOM_RUBRO"].ToString().Trim();
                            T = "*** " + Dr["CLAVE_NOM_TIPO"].ToString().Trim();
                            CL = "** " + Dr["CLAVE_NOM_CLASE"].ToString().Trim();
                            C = "* " + Dr["CLAVE_NOM_CON"].ToString().Trim();
                            SC = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();

                            if (!String.IsNullOrEmpty(PP))
                            { Programa = true; }
                            else { Programa = false; }
                            if (!String.IsNullOrEmpty(SC))
                            { SubConcepto = true; }
                            else { SubConcepto = false; }

                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = FF.Trim();
                            Fila["Aprobado"] = String.Empty;
                            Fila["Ampliacion"] = String.Empty;
                            Fila["Reduccion"] = String.Empty;
                            Fila["Modificado"] = String.Empty;
                            Fila["Devengado"] = String.Empty;
                            Fila["Recaudado"] = String.Empty;
                            Fila["Dev_Rec"] = String.Empty;
                            Fila["Comprometido"] = String.Empty;
                            Fila["Por_Recaudar"] = String.Empty;
                            Fila["Tipo"] = "FF_Tot";
                            Dt_Psp.Rows.Add(Fila);

                            if (Programa)
                            {
                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = PP.Trim();
                                Fila["Aprobado"] = String.Empty;
                                Fila["Ampliacion"] = String.Empty;
                                Fila["Reduccion"] = String.Empty;
                                Fila["Modificado"] = String.Empty;
                                Fila["Devengado"] = String.Empty;
                                Fila["Recaudado"] = String.Empty;
                                Fila["Dev_Rec"] = String.Empty;
                                Fila["Comprometido"] = String.Empty;
                                Fila["Por_Recaudar"] = String.Empty;
                                Fila["Tipo"] = "PP_Tot";
                                Dt_Psp.Rows.Add(Fila);
                            }

                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = R.Trim();
                            Fila["Aprobado"] = String.Empty;
                            Fila["Ampliacion"] = String.Empty;
                            Fila["Reduccion"] = String.Empty;
                            Fila["Modificado"] = String.Empty;
                            Fila["Devengado"] = String.Empty;
                            Fila["Recaudado"] = String.Empty;
                            Fila["Dev_Rec"] = String.Empty;
                            Fila["Comprometido"] = String.Empty;
                            Fila["Por_Recaudar"] = String.Empty;
                            Fila["Tipo"] = "R_Tot";
                            Dt_Psp.Rows.Add(Fila);

                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = T.Trim();
                            Fila["Aprobado"] = String.Empty;
                            Fila["Ampliacion"] = String.Empty;
                            Fila["Reduccion"] = String.Empty;
                            Fila["Modificado"] = String.Empty;
                            Fila["Devengado"] = String.Empty;
                            Fila["Recaudado"] = String.Empty;
                            Fila["Dev_Rec"] = String.Empty;
                            Fila["Comprometido"] = String.Empty;
                            Fila["Por_Recaudar"] = String.Empty;
                            Fila["Tipo"] = "T_Tot";
                            Dt_Psp.Rows.Add(Fila);

                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = CL.Trim();
                            Fila["Aprobado"] = String.Empty;
                            Fila["Ampliacion"] = String.Empty;
                            Fila["Reduccion"] = String.Empty;
                            Fila["Modificado"] = String.Empty;
                            Fila["Devengado"] = String.Empty;
                            Fila["Recaudado"] = String.Empty;
                            Fila["Dev_Rec"] = String.Empty;
                            Fila["Comprometido"] = String.Empty;
                            Fila["Por_Recaudar"] = String.Empty;
                            Fila["Tipo"] = "CL_Tot";
                            Dt_Psp.Rows.Add(Fila);

                            if (SubConcepto)
                            {
                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = C.Trim();
                                Fila["Aprobado"] = String.Empty;
                                Fila["Ampliacion"] = String.Empty;
                                Fila["Reduccion"] = String.Empty;
                                Fila["Modificado"] = String.Empty;
                                Fila["Devengado"] = String.Empty;
                                Fila["Recaudado"] = String.Empty;
                                Fila["Dev_Rec"] = String.Empty;
                                Fila["Comprometido"] = String.Empty;
                                Fila["Por_Recaudar"] = String.Empty;
                                Fila["Tipo"] = "C_Tot";
                                Dt_Psp.Rows.Add(Fila);
                            }

                            if (SubConcepto)
                            {
                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();
                                Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                Fila["Comprometido"] = "0.00";
                                Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                Fila["Tipo"] = "SC";
                                Dt_Psp.Rows.Add(Fila);
                            }
                            else
                            {
                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = Dr["CLAVE_NOM_CON"].ToString().Trim();
                                Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                Fila["Comprometido"] = "0.00";
                                Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                Fila["Tipo"] = "C";
                                Dt_Psp.Rows.Add(Fila);
                            }

                            #endregion
                        }
                    }

                    #region (Fin Dt Psp)
                    foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                    {
                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", Tot_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", Tot_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", Tot_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", Tot_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", Tot_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", Tot_Rec);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Tot_Dev_Rec);
                            Dr_Psp["Comprometido"] = "0.00";
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Tot_xRec);
                            Dr_Psp["Tipo"] = "Tot";
                        }

                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("FF_Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", FF_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", FF_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", FF_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", FF_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", FF_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", FF_Rec);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", FF_Dev_Rec);
                            Dr_Psp["Comprometido"] = "0.00";
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", FF_xRec);
                            Dr_Psp["Tipo"] = "FF";
                        }

                        if (Programa)
                        {
                            if (Dr_Psp["Tipo"].ToString().Trim().Equals("PP_Tot"))
                            {
                                Dr_Psp["Aprobado"] = String.Format("{0:n}", PP_Est);
                                Dr_Psp["Ampliacion"] = String.Format("{0:n}", PP_Amp);
                                Dr_Psp["Reduccion"] = String.Format("{0:n}", PP_Red);
                                Dr_Psp["Modificado"] = String.Format("{0:n}", PP_Mod);
                                Dr_Psp["Devengado"] = String.Format("{0:n}", PP_Dev);
                                Dr_Psp["Recaudado"] = String.Format("{0:n}", PP_Rec);
                                Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PP_Dev_Rec);
                                Dr_Psp["Comprometido"] = "0.00";
                                Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PP_xRec);
                                Dr_Psp["Tipo"] = "PP";
                            }
                        }

                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("R_Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", R_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", R_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", R_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", R_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", R_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", R_Rec);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", R_Dev_Rec);
                            Dr_Psp["Comprometido"] = "0.00";
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", R_xRec);
                            Dr_Psp["Tipo"] = "R";
                        }
                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("T_Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", T_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", T_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", T_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", T_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", T_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", T_Rec);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", T_Dev_Rec);
                            Dr_Psp["Comprometido"] = "0.00";
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", T_xRec);
                            Dr_Psp["Tipo"] = "T";
                        }
                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("CL_Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", CL_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", CL_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", CL_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", CL_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", CL_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", CL_Rec);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", CL_Dev_Rec);
                            Dr_Psp["Comprometido"] = "0.00";
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", CL_xRec);
                            Dr_Psp["Tipo"] = "CL";
                        }

                        if (SubConcepto)
                        {
                            if (Dr_Psp["Tipo"].ToString().Trim().Equals("C_Tot"))
                            {
                                Dr_Psp["Aprobado"] = String.Format("{0:n}", C_Est);
                                Dr_Psp["Ampliacion"] = String.Format("{0:n}", C_Amp);
                                Dr_Psp["Reduccion"] = String.Format("{0:n}", C_Red);
                                Dr_Psp["Modificado"] = String.Format("{0:n}", C_Mod);
                                Dr_Psp["Devengado"] = String.Format("{0:n}", C_Dev);
                                Dr_Psp["Recaudado"] = String.Format("{0:n}", C_Rec);
                                Dr_Psp["Dev_Rec"] = String.Format("{0:n}", C_Dev_Rec);
                                Dr_Psp["Comprometido"] = "0.00";
                                Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", C_xRec);
                                Dr_Psp["Tipo"] = "C";
                            }
                        }
                    }//fin foreach
                    #endregion
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al generar el presupuesto de ingresos. Error[" + ex.Message + "]");
                }
                return Dt_Psp;
            }
        #endregion

        #region (Generar Poliza)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Poliza
            ///DESCRIPCIÓN          : Metodo para obtener los datos para generar la poliza
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 04/Diciembre/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Poliza(String No_Poliza, String Tipo_Poliza, String Mes_Anio)
            {
                Cls_Ope_Con_Polizas_Negocio Negocio = new Cls_Ope_Con_Polizas_Negocio(); //conexion con la capa de negocios
                DataSet Ds_Polizas = new DataSet();
                DataTable Dt_Polizas = new DataTable();
                String Nom_Archivo = "";

                try
                {
                    if (!String.IsNullOrEmpty(No_Poliza.Trim()) && !String.IsNullOrEmpty(Tipo_Poliza.Trim())
                        && !String.IsNullOrEmpty(Mes_Anio.Trim()))
                    {
                        //Negocio.P_No_Poliza = No_Poliza.Trim();
                        //Negocio.P_Tipo_Poliza_ID = Tipo_Poliza.Trim();
                        //Negocio.P_Mes_Ano = Mes_Anio.Trim();
                        Negocio.P_No_Poliza = "0000000005";
                        Negocio.P_Tipo_Poliza_ID = "00004";
                        Negocio.P_Mes_Ano = "0413";
                        Dt_Polizas = Negocio.Consulta_Detalle_Poliza();

                        if (Dt_Polizas != null)
                        {
                            if (Dt_Polizas.Rows.Count > 0)
                            {
                                Dt_Polizas.TableName = "Dt_Datos_Poliza";
                                Ds_Polizas.Tables.Add(Dt_Polizas.Copy());
                                Exportar_Reporte(Ds_Polizas, "Rpt_Con_Poliza.rpt", "Rpt_Poliza_" + Session.SessionID, "pdf",
                                    ExportFormatType.PortableDocFormat, "Contabilidad");
                                Nom_Archivo = "../../Reporte/Rpt_Poliza_" + Session.SessionID + ".pdf     ";
                            }
                        }
                    }

                    return Nom_Archivo;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Poliza Error[" + ex.Message + "]");
                }
            }
        #endregion

        #region (Generar Reporte Movimientos Egresos Ingresos)
            ///*****************************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Crear_Solicitud_Movimientos_Egresos
            ///DESCRIPCIÓN          : Metodo para crear el reporte de movimientos de egresos
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 01/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*****************************************************************************************************************
            public String Crear_Solicitud_Movimientos_Egresos(String No_Movimiento, String No_Solicitud, String Tipo_Movimiento, String Anio)
            {
                DataSet Ds_Registros = null;
                DataTable Dt_Encabezado = new DataTable();
                DataTable Dt_Mov_Justificacion = new DataTable();
                DataTable Dt_Mov_Origen = new DataTable();
                DataTable Dt_Mov_Destino = new DataTable();
                DataTable Dt_Mov_Origen_Det = new DataTable();
                DataTable Dt_Mov_Destino_Det = new DataTable();
                String Nom_Archivo = String.Empty;

                try
                {

                    Ds_Registros = new DataSet();
                    Dt_Encabezado = Crear_Dt_Encabezado(No_Movimiento, No_Solicitud);
                    Dt_Mov_Origen = Crear_Dt_Movimientos("Origen", "Normal", No_Solicitud);
                    Dt_Mov_Destino = Crear_Dt_Movimientos("Destino", "Normal", No_Solicitud);
                    Dt_Mov_Origen_Det = Crear_Dt_Movimientos("Origen", "Det", No_Solicitud);
                    Dt_Mov_Destino_Det = Crear_Dt_Movimientos("Destino", "Det", No_Solicitud);
                    Dt_Mov_Justificacion = Crear_Dt_Movimientos("", "Jus", No_Solicitud);
                    Dt_Encabezado.TableName = "Dt_Encabezado";
                    Dt_Mov_Origen.TableName = "Dt_Mov_Origen";
                    Dt_Mov_Destino.TableName = "Dt_Mov_Destino";
                    Dt_Mov_Origen_Det.TableName = "Dt_Mov_Origen_Det";
                    Dt_Mov_Destino_Det.TableName = "Dt_Mov_Destino_Det";
                    Dt_Mov_Justificacion.TableName = "Dt_Mov_Justificacion";

                    Ds_Registros.Tables.Add(Dt_Encabezado.Copy());
                    Ds_Registros.Tables.Add(Dt_Mov_Origen.Copy());
                    Ds_Registros.Tables.Add(Dt_Mov_Destino.Copy());
                    Ds_Registros.Tables.Add(Dt_Mov_Origen_Det.Copy());
                    Ds_Registros.Tables.Add(Dt_Mov_Destino_Det.Copy());
                    Ds_Registros.Tables.Add(Dt_Mov_Justificacion.Copy());

                    if (Tipo_Movimiento.Trim().Equals("TRASPASO"))
                    {
                        Exportar_Reporte(Ds_Registros, "Cr_Ope_Psp_Movimiento_Presupuestal.rpt", "Rpt_Movimientos_" + Session.SessionID,
                            "pdf", ExportFormatType.PortableDocFormat, "Presupuestos");
                    }
                    else
                    {
                        Exportar_Reporte(Ds_Registros, "Cr_Rpt_Psp_Movimiento_Presupuestal.rpt", "Rpt_Movimientos_" + Session.SessionID,
                            "pdf", ExportFormatType.PortableDocFormat, "Presupuestos");
                    }
                    Nom_Archivo = "../../Reporte/Rpt_Movimientos_" + Session.SessionID + ".pdf     ";
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al generar el reporte. Error[" + ex.Message + "]");
                }
                return Nom_Archivo;
            }

            ///*****************************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Encabezado
            ///DESCRIPCIÓN          : METODO PARA CREAR EL REPORTE DE MOVIMIENTOS
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 01/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*****************************************************************************************************************
            public DataTable Crear_Dt_Encabezado(String No_Mov, String No_Solicitud)
            {
                Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
                DataTable Dt_Encabezado = new DataTable();
                DataRow Fila;
                String Texto = String.Empty;
                String UR = String.Empty;
                String Coordinador = String.Empty;
                String Puesto_Coordinador = String.Empty;
                String Puesto_Solicito = String.Empty;
                String Solicito = String.Empty;
                DataTable Dt = new DataTable();

                try
                {
                    Negocio.P_No_Solicitud = No_Solicitud;
                    Dt = Negocio.Consultar_Datos_Movimientos_Det();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            Solicito = Dt.Rows[0][Ope_Psp_Movimiento_Egr_Det.Campo_Nombre_Solicitante].ToString().Trim().ToUpper();
                            Puesto_Solicito = Dt.Rows[0][Ope_Psp_Movimiento_Egr_Det.Campo_Puesto_Solicitante].ToString().Trim().ToUpper();
                            Coordinador = Dt.Rows[0][Ope_Psp_Movimiento_Egr_Det.Campo_Nombre_Director].ToString().Trim().ToUpper();
                            Puesto_Coordinador = Dt.Rows[0][Ope_Psp_Movimiento_Egr_Det.Campo_Puesto_Director].ToString().Trim().ToUpper();
                            UR = Dt.Rows[0]["UR"].ToString().Trim().ToUpper();
                        }
                    }

                    Dt_Encabezado.Columns.Add("Nombre_UR");
                    Dt_Encabezado.Columns.Add("No_Modificacion");
                    Dt_Encabezado.Columns.Add("No_Solicitud");
                    Dt_Encabezado.Columns.Add("Anio");
                    Dt_Encabezado.Columns.Add("Fecha");
                    Dt_Encabezado.Columns.Add("Solicitante");
                    Dt_Encabezado.Columns.Add("Texto");
                    Dt_Encabezado.Columns.Add("Puesto_Solicitante");
                    Dt_Encabezado.Columns.Add("Dirigido");
                    Dt_Encabezado.Columns.Add("Puesto_Dirigido");

                    Texto = "Por medio del presente reciba un cordial saludo, a la vez que solicito su valioso apoyo";
                    Texto += " para que sean consideradas las modificaciones al presupuesto de la " + UR;
                    Texto += " de acuerdo al archivo anexo.";

                    Fila = Dt_Encabezado.NewRow();
                    Fila["Nombre_UR"] = UR.Trim();
                    Fila["No_Modificacion"] = No_Mov.Trim() + "a. Modificación - No. Solicitud " + No_Solicitud.Trim()
                         + " - " + String.Format("{0:yyyy}", DateTime.Now);
                    Fila["No_Solicitud"] = No_Solicitud.Trim();
                    Fila["Anio"] = String.Format("{0:yyyy}", DateTime.Now);
                    Fila["Fecha"] = Crear_Fecha(String.Format("{0:MM/dd/yyyy}", DateTime.Now));
                    Fila["Solicitante"] = Solicito.Trim();
                    Fila["Texto"] = Texto;
                    Fila["Puesto_Solicitante"] = Puesto_Solicito.Trim();
                    Fila["Dirigido"] = Coordinador.Trim();
                    Fila["Puesto_Dirigido"] = Puesto_Coordinador.Trim();
                    Dt_Encabezado.Rows.Add(Fila);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al generar el reporte. Error[" + ex.Message + "]");
                }
                return Dt_Encabezado;
            }

            ///*****************************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Mov_Origen
            ///DESCRIPCIÓN          : METODO PARA CREAR EL REPORTE DE MOVIMIENTOS
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 01/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*****************************************************************************************************************
            public DataTable Crear_Dt_Movimientos(String Tipo_Partida, String Tipo_Dt, String No_Solicitud)
            {
                Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
                DataTable Dt_Mov = new DataTable();
                DataRow Fila;
                DataTable Dt = new DataTable();
                Negocio.P_No_Solicitud = No_Solicitud;
                Dt = Negocio.Consultar_Datos_Movimientos_Det();
                String Codigo_Programatico = String.Empty;

                try
                {

                    if (Tipo_Dt.Trim().Equals("Det"))
                    {
                        Dt_Mov.Columns.Add("Anio", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("FF", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("AF", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("PP", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("UR", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("P", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("Imp_Ene", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Feb", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Mar", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Abr", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_May", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Jun", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Jul", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Ago", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Sep", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Oct", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Nov", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Dic", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Tot", System.Type.GetType("System.Double"));

                        foreach (DataRow Dr in Dt.Rows)
                        {
                            if (Dr["TIPO_PARTIDA"].ToString().Trim().Equals(Tipo_Partida.Trim()))
                            {
                                Codigo_Programatico = Dr["FF"].ToString().Trim() + "-" + Dr["AF"].ToString().Trim() + "-" + Dr["PP"].ToString().Trim() +
                                     "-" + Dr["UR"].ToString().Trim().Substring(0, Dr["UR"].ToString().Trim().IndexOf(" ")) +
                                     "-" + Dr["PARTIDA"].ToString().Trim().Substring(0, Dr["PARTIDA"].ToString().Trim().IndexOf(" "));

                                Fila = Dt_Mov.NewRow();
                                Fila["Anio"] = String.Format("{0:yyyy}", DateTime.Now);
                                Fila["FF"] = Codigo_Programatico.Trim();
                                Fila["AF"] = Dr["AF"].ToString().Trim();
                                Fila["PP"] = Dr["PP"].ToString().Trim();
                                Fila["UR"] = Dr["UR"].ToString().Trim().Substring(0, Dr["UR"].ToString().Trim().IndexOf(" "));
                                Fila["P"] = Dr["PARTIDA"].ToString().Trim().Substring(0, Dr["PARTIDA"].ToString().Trim().IndexOf(" "));
                                Fila["Imp_Ene"] = Dr["ENERO"];
                                Fila["Imp_Feb"] = Dr["FEBRERO"];
                                Fila["Imp_Mar"] = Dr["MARZO"];
                                Fila["Imp_Abr"] = Dr["ABRIL"];
                                Fila["Imp_May"] = Dr["MAYO"];
                                Fila["Imp_Jun"] = Dr["JUNIO"];
                                Fila["Imp_Jul"] = Dr["JULIO"];
                                Fila["Imp_Ago"] = Dr["AGOSTO"];
                                Fila["Imp_Sep"] = Dr["SEPTIEMBRE"];
                                Fila["Imp_Oct"] = Dr["OCTUBRE"];
                                Fila["Imp_Nov"] = Dr["NOVIEMBRE"];
                                Fila["Imp_Dic"] = Dr["DICIEMBRE"];
                                Fila["Imp_Tot"] = Dr["IMPORTE"];
                                Dt_Mov.Rows.Add(Fila);
                            }
                        }
                    }
                    else if (Tipo_Dt.Trim().Equals("Jus"))
                    {
                        Dt_Mov.Columns.Add("Anio", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("FF", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("AF", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("PP", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("UR", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("P", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("Partida", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("Justificacion", System.Type.GetType("System.String"));

                        foreach (DataRow Dr in Dt.Rows)
                        {
                            Codigo_Programatico = Dr["FF"].ToString().Trim() + "-" + Dr["AF"].ToString().Trim() + "-" + Dr["PP"].ToString().Trim() +
                                     "-" + Dr["UR"].ToString().Trim().Substring(0, Dr["UR"].ToString().Trim().IndexOf(" ")) +
                                     "-" + Dr["PARTIDA"].ToString().Trim().Substring(0, Dr["PARTIDA"].ToString().Trim().IndexOf(" "));

                            Fila = Dt_Mov.NewRow();
                            Fila["Anio"] = String.Format("{0:yyyy}", DateTime.Now);
                            Fila["FF"] = Codigo_Programatico.Trim();
                            Fila["AF"] = Dr["AF"].ToString().Trim();
                            Fila["PP"] = Dr["PP"].ToString().Trim();
                            Fila["UR"] = Dr["UR"].ToString().Trim().Substring(0, Dr["UR"].ToString().Trim().IndexOf(" "));
                            Fila["P"] = Dr["PARTIDA"].ToString().Trim().Substring(0, Dr["PARTIDA"].ToString().Trim().IndexOf(" "));
                            Fila["Partida"] = Dr["PARTIDA"].ToString().Trim().Substring(Dr["PARTIDA"].ToString().Trim().IndexOf(" "));
                            Fila["Justificacion"] = Dr["JUSTIFICACION"].ToString().Trim();
                            Dt_Mov.Rows.Add(Fila);
                        }
                    }
                    else
                    {
                        Dt_Mov.Columns.Add("Anio", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("FF", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("AF", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("PP", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("UR", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("P", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("Partida", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("Presupuesto_Aprobado", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Importe", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Presupuesto_Modificado", System.Type.GetType("System.Double"));

                        foreach (DataRow Dr in Dt.Rows)
                        {
                            if (Dr["TIPO_PARTIDA"].ToString().Trim().Equals(Tipo_Partida.Trim()))
                            {
                                Codigo_Programatico = Dr["FF"].ToString().Trim() + "-" + Dr["AF"].ToString().Trim() + "-" + Dr["PP"].ToString().Trim() +
                                     "-" + Dr["UR"].ToString().Trim().Substring(0, Dr["UR"].ToString().Trim().IndexOf(" ")) +
                                     "-" + Dr["PARTIDA"].ToString().Trim().Substring(0, Dr["PARTIDA"].ToString().Trim().IndexOf(" "));

                                Fila = Dt_Mov.NewRow();
                                Fila["Anio"] = String.Format("{0:yyyy}", DateTime.Now);
                                Fila["FF"] = Codigo_Programatico.Trim();
                                Fila["AF"] = Dr["AF"].ToString().Trim();
                                Fila["PP"] = Dr["PP"].ToString().Trim();
                                Fila["UR"] = Dr["UR"].ToString().Trim().Substring(0, Dr["UR"].ToString().Trim().IndexOf(" "));
                                Fila["P"] = Dr["PARTIDA"].ToString().Trim().Substring(0, Dr["PARTIDA"].ToString().Trim().IndexOf(" "));
                                Fila["Partida"] = Dr["PARTIDA"].ToString().Trim().Substring(Dr["PARTIDA"].ToString().Trim().IndexOf(" "));
                                Fila["Presupuesto_Aprobado"] = Dr["APROBADO"];
                                Fila["Importe"] = Dr["IMPORTE"];
                                Fila["Presupuesto_Modificado"] = Dr["MODIFICADO"];
                                Dt_Mov.Rows.Add(Fila);
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al generar el reporte. Error[" + ex.Message + "]");
                }
                return Dt_Mov;
            }

            //****************************************************************************************************
            //NOMBRE DE LA FUNCIÓN : Crear_Fecha
            //DESCRIPCIÓN          : Metodo para obtener la fecha en letras
            //PARAMETROS           1 Fecha: fecha a la cual le daremos formato
            //CREO                 : Leslie González Vázquez
            //FECHA_CREO           : 13/Diciembre/2011 
            //MODIFICO             :
            //FECHA_MODIFICO       :
            //CAUSA_MODIFICACIÓN   :
            //****************************************************************************************************
            internal static String Crear_Fecha(String Fecha)
            {
                String Fecha_Formateada = String.Empty;
                String Mes = String.Empty;
                String[] Fechas;

                try
                {
                    Fechas = Fecha.Split('/');
                    Mes = Fechas[0].ToString();
                    switch (Mes)
                    {
                        case "01":
                            Mes = "Enero";
                            break;
                        case "02":
                            Mes = "Febrero";
                            break;
                        case "03":
                            Mes = "Marzo";
                            break;
                        case "04":
                            Mes = "Abril";
                            break;
                        case "05":
                            Mes = "Mayo";
                            break;
                        case "06":
                            Mes = "Junio";
                            break;
                        case "07":
                            Mes = "Julio";
                            break;
                        case "08":
                            Mes = "Agosto";
                            break;
                        case "09":
                            Mes = "Septiembre";
                            break;
                        case "10":
                            Mes = "Octubre";
                            break;
                        case "11":
                            Mes = "Noviembre";
                            break;
                        default:
                            Mes = "Diciembre";
                            break;
                    }
                    Fecha_Formateada = "Irapuato, Guanajuato a " + Fechas[1].ToString() + " de " + Mes + " de " + Fechas[2].ToString();
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al crear el formato de fecha. Error: [" + Ex.Message + "]");
                }
                return Fecha_Formateada;
            }

            ///*****************************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Crear_Solicitud_Movimientos_Ingresos
            ///DESCRIPCIÓN          : Metodo para crear el reporte de movimientos de ingresos
            ///PARAMETROS           :  
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Noviembre/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*****************************************************************************************************************
            public String Crear_Solicitud_Movimientos_Ingresos(String Anio, String No_Modificacion, String Solicitud_ID)
            {
                Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio(); //conexion con la capa de negocios
                DataSet Ds_Registros = null;
                String Fecha = String.Empty;
                DataTable Dt_Movimientos = new DataTable();
                DataTable Dt_Mov = new DataTable();
                String Nom_Archivo = String.Empty;
                String No_Solicitud = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio.Trim();
                    Negocio.P_No_Movimiento_Ing = No_Modificacion.Trim();
                    Negocio.P_Estatus = String.Empty;
                    Dt_Movimientos = Negocio.Consultar_Movimientos();

                    if (Dt_Movimientos != null)
                    {
                        if (Dt_Movimientos.Rows.Count > 0)
                        {
                            //obtenemos los movimientos de la solicitud
                            Dt_Mov = (from Fila in Dt_Movimientos.AsEnumerable()
                                      where Fila.Field<decimal>(Ope_Psp_Movimiento_Ing_Det.Campo_Movimiento_Ing_ID) == Convert.ToDecimal(Solicitud_ID.Trim())
                                      select Fila).AsDataView().ToTable();


                            //agregamos las filas de los datos complementarios para el reporte
                            Dt_Mov.Columns.Add("NO_SOLICITUD", System.Type.GetType("System.String"));
                            Dt_Mov.Columns.Add("FECHA", System.Type.GetType("System.String"));
                            Dt_Mov.Columns.Add("SOLICITANTE", System.Type.GetType("System.String"));
                            Dt_Mov.Columns.Add("PUESTO_SOLICITANTE", System.Type.GetType("System.String"));

                            Fecha = Crear_Fecha(String.Format("{0:MM/dd/yyyy}", DateTime.Now));

                            foreach (DataRow Dr in Dt_Mov.Rows)
                            {
                                No_Solicitud = String.Empty;
                                No_Solicitud = "MODIFICACIÓN AL PRESUPUESTO DE INGRESOS    T";
                                No_Solicitud += String.Format("{0:00}", Convert.ToInt32(Dr[Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing].ToString().Trim()));
                                No_Solicitud += " - " + Dr[Ope_Psp_Movimiento_Ing_Det.Campo_Anio].ToString().Trim();

                                Dr["NO_SOLICITUD"] = No_Solicitud;
                                Dr["FECHA"] = Fecha.Trim();
                                Dr["SOLICITANTE"] = "";
                                Dr["PUESTO_SOLICITANTE"] = "";

                                if (Dr[Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Movimiento].ToString().Trim().Equals("REDUCCION"))
                                {
                                    Dr["IMP_TOTAL"] = "-" + Dr["IMP_TOTAL"];
                                }
                            }

                            Ds_Registros = new DataSet();
                            Dt_Mov.TableName = "Dt_Movimientos_Ing";
                            Ds_Registros.Tables.Add(Dt_Mov.Copy());

                            Exportar_Reporte(Ds_Registros, "Cr_Ope_Psp_Movimiento_Presupuestal_Ingresos.rpt", "Rpt_Movimientos_" + Session.SessionID,
                            "pdf", ExportFormatType.PortableDocFormat, "Presupuestos");
                            Nom_Archivo = "../../Reporte/Rpt_Movimientos_" + Session.SessionID + ".pdf     ";
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al generar el reporte. Error[" + ex.Message + "]");
                }
                return Nom_Archivo;
            }
        #endregion

        ///*******************************************************************************************************
        /// NOMBRE_FUNCIÓN: Exportar_Reporte
        /// DESCRIPCIÓN: Genera el reporte de Crystal con los datos proporcionados en el DataTable 
        /// PARÁMETROS:
        /// 		1. Ds_Reporte: Dataset con datos a imprimir
        /// 		2. Nombre_Reporte: Nombre del archivo de reporte .rpt
        /// 		3. Nombre_Archivo: Nombre del archivo a generar
        /// CREO: Roberto González Oseguera
        /// FECHA_CREO: 04-sep-2011
        /// MODIFICÓ: 
        /// FECHA_MODIFICÓ: 
        /// CAUSA_MODIFICACIÓN: 
        ///*******************************************************************************************************
        private void Exportar_Reporte(DataSet Ds_Reporte, String Nombre_Reporte, String Nombre_Archivo,
            String Extension_Archivo, ExportFormatType Formato, String Nombre_Carpeta)
        {
            ReportDocument Reporte = new ReportDocument();
            String Ruta = Server.MapPath("../Rpt/" + Nombre_Carpeta + "/" + Nombre_Reporte);

            try
            {
                Reporte.Load(Ruta);
                Reporte.SetDataSource(Ds_Reporte);
            }
            catch
            {
                //Lbl_Mensaje_Error.Visible = true;
                //Lbl_Mensaje_Error.Text = "No se pudo cargar el reporte";
            }

            String Archivo_Reporte = Nombre_Archivo + "." + Extension_Archivo;  // formar el nombre del archivo a generar 
            try
            {
                ExportOptions Export_Options_Calculo = new ExportOptions();
                DiskFileDestinationOptions Disk_File_Destination_Options_Calculo = new DiskFileDestinationOptions();
                Disk_File_Destination_Options_Calculo.DiskFileName = Server.MapPath("../../Reporte/" + Archivo_Reporte);
                Export_Options_Calculo.ExportDestinationOptions = Disk_File_Destination_Options_Calculo;
                Export_Options_Calculo.ExportDestinationType = ExportDestinationType.DiskFile;
                Export_Options_Calculo.ExportFormatType = Formato;
                Reporte.Export(Export_Options_Calculo);
            }
            catch (Exception Ex)
            {
                //Lbl_Mensaje_Error.Visible = true;
                //Lbl_Mensaje_Error.Text = Ex.Message.ToString();
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Eliminar_Archivo
        ///DESCRIPCIÓN          : Metodo para eliminar un archivo
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 04/Diciembre/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private String Eliminar_Archivo(String Ruta)
        {
            String Hecho = "NO";
            try
            {
                if (System.IO.File.Exists(Server.MapPath(Ruta)))
                {
                    System.IO.File.Delete(Server.MapPath(Ruta));
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al Eliminar_Archivo Error[" + Ex.Message + "]");
            }

            return Hecho;
        }
    #endregion
    #endregion
}
