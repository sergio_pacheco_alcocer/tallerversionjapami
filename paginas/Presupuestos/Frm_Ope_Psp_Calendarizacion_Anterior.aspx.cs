﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Globalization;
using System.Text.RegularExpressions;
using AjaxControlToolkit;
using System.Collections.Generic;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Calendarizar_Presupuesto.Negocio;
using JAPAMI.Limite_Presupuestal.Negocio;
using JAPAMI.Polizas.Negocios;
using JAPAMI.SAP_Partidas_Especificas.Negocio;

public partial class paginas_Presupuestos_Frm_Ope_Psp_Calendarizacion_Anterior : System.Web.UI.Page
{
    #region PAGE LOAD

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        if (!IsPostBack)
        {
            if (this.Request.QueryString["A"] != null && this.Request.QueryString["UR"] != null && this.Request.QueryString["E"] != null && this.Request.QueryString["PSP"] != null) 
            {
                Hf_Anio.Value = this.Request.QueryString["A"].ToString().Trim();
                Hf_Dependencia_ID.Value = this.Request.QueryString["UR"].ToString().Trim();
                Hf_Empleado_ID.Value = this.Request.QueryString["E"].ToString().Trim();
                Hf_Tipo_Calendario.Value = this.Request.QueryString["PSP"].ToString().Trim();
            }

            Calendarizar_Presupuesto_Inicio();
        }
    }

    #endregion

    #region (Metodos Generales)
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Calendarizar_Presupuesto_Inicio
    ///DESCRIPCIÓN          : Metodo para el inicio de la pagina
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 10/Noviembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Calendarizar_Presupuesto_Inicio()
    {
        try
        {
            Llenar_Grid_Partida_Asignada();
            Calcular_Total_Presupuestado();
        }
        catch (Exception ex)
        {
            throw new Exception("Error al Calendarizar_Presupuesto_Inicio ERROR[" + ex.Message + "]");
        }
    }

    //********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Limpiar_Sessiones
    ///DESCRIPCIÓN          : Metodo para limpiar las sessiones del formulario
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 12/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Limpiar_Sessiones()
    {
        Session["Dt_Partidas_Asignadas_Anterior"] = null;
        Session["Dt_Partidas"] = null;

        Session.Remove("Dt_Partidas_Asignadas_Anterior");
        Session.Remove("Dt_Partidas");
    }

    //********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Calcular_Total_Presupuestado
    ///DESCRIPCIÓN          : Metodo para calcular el total que se lleva presupuestodo
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 14/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Calcular_Total_Presupuestado()
    {
        DataTable Dt_Partidas_Asignadas = new DataTable();
        Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas_Anterior"];
        Double Total = 0.00;

        try
        {
            if (Dt_Partidas_Asignadas != null)
            {
                if (Dt_Partidas_Asignadas.Rows.Count > 0)
                {
                    foreach (DataRow Dr in Dt_Partidas_Asignadas.Rows)
                    {
                        Total = Total + Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_TOTAL"].ToString().Trim()) ? "0" : Dr["IMPORTE_TOTAL"].ToString().Trim());
                    }
                }
            }
            Txt_Total_Ajuste.Text = "";
            Txt_Total_Ajuste.Text = String.Format("{0:c}", Total);
        }
        catch (Exception ex)
        {
            throw new Exception("Error al calcular el total del presupuesto. Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Partida_Asignada
    ///DESCRIPCIÓN          : Metodo para llenar el grid anidado de las partidas asignadas
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 16/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Llenar_Grid_Partida_Asignada()
    {
        Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
        DataTable Dt_Partidas_Asignadas = new DataTable();
        DataTable Dt_Partidas = new DataTable();

        try
        {
            if (Hf_Tipo_Calendario.Value.Trim().Equals("Dependencia"))
            {
                if (!String.IsNullOrEmpty(Hf_Dependencia_ID.Value.Trim()))
                {
                    Negocio.P_Dependencia_ID = Hf_Dependencia_ID.Value.Trim();
                    Negocio.P_Empleado_ID = String.Empty;
                    Dt_Partidas_Asignadas = Negocio.Consultar_Partidas_Asignadas_Anteriores();
                    Dt_Partidas = Negocio.Consultar_Partidas_Asignadas();
                    Session["Dt_Partidas_Asignadas_Anterior"] = Dt_Partidas_Asignadas;
                    Session["Dt_Partidas"] = Dt_Partidas;
                }
            }
            else
            {
                Negocio.P_Empleado_ID = Hf_Empleado_ID.Value.Trim();
                Negocio.P_Dependencia_ID = String.Empty;
                Dt_Partidas_Asignadas = Negocio.Consultar_Partidas_Asignadas_Anteriores();
                Dt_Partidas = Negocio.Consultar_Partidas_Asignadas();
                Session["Dt_Partidas_Asignadas_Anterior"] = Dt_Partidas_Asignadas;
                Session["Dt_Partidas"] = Dt_Partidas;
            }

            if (Dt_Partidas_Asignadas != null)
            {
                if (Dt_Partidas_Asignadas.Rows.Count > 0)
                {
                    Obtener_Consecutivo_ID();
                    Dt_Partidas_Asignadas = Crear_Dt_Partida_Asignada();
                    Grid_Partida_Asignada.Columns[1].Visible = true;
                    Grid_Partida_Asignada.DataSource = Dt_Partidas_Asignadas;
                    Grid_Partida_Asignada.DataBind();
                    Grid_Partida_Asignada.Columns[1].Visible = false;
                }
                else
                {
                    Grid_Partida_Asignada.DataSource = Dt_Partidas_Asignadas;
                    Grid_Partida_Asignada.DataBind();
                }
            }
            else
            {
                Grid_Partida_Asignada.DataSource = Dt_Partidas_Asignadas;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de llenar la tabla de las partidas asignadas Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Partida_Asignada
    ///DESCRIPCIÓN          : Metodo para crear el datatable de las partidas asignadas del grid anidado
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 16/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private DataTable Crear_Dt_Partida_Asignada()
    {
        DataTable Dt_Partida_Asignada = new DataTable();
        DataTable Dt_Session = new DataTable();
        Dt_Session = (DataTable)Session["Dt_Partidas_Asignadas_Anterior"];
        String Partida_Id = string.Empty;
        Double Ene = 0.00;
        Double Feb = 0.00;
        Double Mar = 0.00;
        Double Abr = 0.00;
        Double May = 0.00;
        Double Jun = 0.00;
        Double Jul = 0.00;
        Double Ago = 0.00;
        Double Sep = 0.00;
        Double Oct = 0.00;
        Double Nov = 0.00;
        Double Dic = 0.00;
        Double Total = 0.00;
        DataRow Fila;
        String Clave = String.Empty;
        Boolean Iguales;

        try
        {
            if (Dt_Session != null)
            {
                if (Dt_Session.Rows.Count > 0)
                {
                    //creamos las columnas del datatable donde se guardaran los datos
                    Dt_Partida_Asignada.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("CLAVE", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL_ENE", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL_FEB", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL_MAR", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL_ABR", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL_MAY", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL_JUN", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL_JUL", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL_AGO", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL_SEP", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL_OCT", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL_NOV", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL_DIC", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("TOTAL", System.Type.GetType("System.String"));


                    foreach (DataRow Dr_Sessiones in Dt_Session.Rows)
                    {
                        //Obtenemos la partida id y la clave
                        Partida_Id = Dr_Sessiones["PARTIDA_ID"].ToString().Trim();
                        Clave = Dr_Sessiones["CLAVE_PARTIDA"].ToString().Trim();
                        Iguales = false;
                        //verificamos si la partida no a sido ya agrupada
                        if (Dt_Partida_Asignada.Rows.Count > 0)
                        {
                            foreach (DataRow Dr_Partidas in Dt_Partida_Asignada.Rows)
                            {
                                if (Dr_Partidas["PARTIDA_ID"].ToString().Trim().Equals(Partida_Id))
                                {
                                    Iguales = true;
                                }
                            }
                        }

                        // tomamos los datos del datatable para agrupar las partidas asignadas
                        if (!Iguales)
                        {
                            foreach (DataRow Dr_Session in Dt_Session.Rows)
                            {
                                if (Dr_Session["PARTIDA_ID"].ToString().Trim().Equals(Partida_Id))
                                {
                                    Ene = Ene + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["ENERO"].ToString()) ? "0" : Dr_Session["ENERO"].ToString().Trim());
                                    Feb = Feb + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["FEBRERO"].ToString()) ? "0" : Dr_Session["FEBRERO"].ToString().Trim());
                                    Mar = Mar + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["MARZO"].ToString()) ? "0" : Dr_Session["MARZO"].ToString().Trim());
                                    Abr = Abr + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["ABRIL"].ToString()) ? "0" : Dr_Session["ABRIL"].ToString().Trim());
                                    May = May + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["MAYO"].ToString()) ? "0" : Dr_Session["MAYO"].ToString().Trim());
                                    Jun = Jun + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["JUNIO"].ToString()) ? "0" : Dr_Session["JUNIO"].ToString().Trim());
                                    Jul = Jul + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["JULIO"].ToString()) ? "0" : Dr_Session["JULIO"].ToString().Trim());
                                    Ago = Ago + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["AGOSTO"].ToString()) ? "0" : Dr_Session["AGOSTO"].ToString().Trim());
                                    Sep = Sep + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["SEPTIEMBRE"].ToString()) ? "0" : Dr_Session["SEPTIEMBRE"].ToString().Trim());
                                    Oct = Oct + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["OCTUBRE"].ToString()) ? "0" : Dr_Session["OCTUBRE"].ToString().Trim());
                                    Nov = Nov + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["NOVIEMBRE"].ToString()) ? "0" : Dr_Session["NOVIEMBRE"].ToString().Trim());
                                    Dic = Dic + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["DICIEMBRE"].ToString()) ? "0" : Dr_Session["DICIEMBRE"].ToString().Trim());
                                }
                            }
                            Total = Ene + Feb + Mar + Abr + May + Jun + Jul + Ago + Sep + Oct + Nov + Dic;

                            Fila = Dt_Partida_Asignada.NewRow();
                            Fila["PARTIDA_ID"] = Partida_Id;
                            Fila["CLAVE"] = Clave;
                            Fila["TOTAL_ENE"] = String.Format("{0:##,###,##0.00}", Ene);
                            Fila["TOTAL_FEB"] = String.Format("{0:##,###,##0.00}", Feb);
                            Fila["TOTAL_MAR"] = String.Format("{0:##,###,##0.00}", Mar);
                            Fila["TOTAL_ABR"] = String.Format("{0:##,###,##0.00}", Abr);
                            Fila["TOTAL_MAY"] = String.Format("{0:##,###,##0.00}", May);
                            Fila["TOTAL_JUN"] = String.Format("{0:##,###,##0.00}", Jun);
                            Fila["TOTAL_JUL"] = String.Format("{0:##,###,##0.00}", Jul);
                            Fila["TOTAL_AGO"] = String.Format("{0:##,###,##0.00}", Ago);
                            Fila["TOTAL_SEP"] = String.Format("{0:##,###,##0.00}", Sep);
                            Fila["TOTAL_OCT"] = String.Format("{0:##,###,##0.00}", Oct);
                            Fila["TOTAL_NOV"] = String.Format("{0:##,###,##0.00}", Nov);
                            Fila["TOTAL_DIC"] = String.Format("{0:##,###,##0.00}", Dic);
                            Fila["TOTAL"] = String.Format("{0:##,###,##0.00}", Total);
                            Dt_Partida_Asignada.Rows.Add(Fila);

                            Ene = 0.00;
                            Feb = 0.00;
                            Mar = 0.00;
                            Abr = 0.00;
                            May = 0.00;
                            Jun = 0.00;
                            Jul = 0.00;
                            Ago = 0.00;
                            Sep = 0.00;
                            Oct = 0.00;
                            Nov = 0.00;
                            Dic = 0.00;
                            Total = 0.00;
                        }
                    }
                }
            }
            return Dt_Partida_Asignada;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de crear la tabla de partidas asignadas Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Detalles
    ///DESCRIPCIÓN          : Metodo para crear el datatable de los detalles de las partidas asignadas del grid 
    ///PROPIEDADES          1 Partida_ID del cual obtendremos los detalles
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 16/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private DataTable Crear_Dt_Detalles(String Partida_ID)
    {
        DataTable Dt_Detalle = new DataTable();
        DataTable Dt_Session = new DataTable();
        Dt_Session = (DataTable)Session["Dt_Partidas_Asignadas_Anterior"];
        DataRow Fila;

        try
        {
            if (Dt_Session != null)
            {
                if (Dt_Session.Rows.Count > 0)
                {
                    Dt_Detalle.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("PROYECTO_ID", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("PRODUCTO_ID", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("PRECIO", System.Type.GetType("System.Double"));
                    Dt_Detalle.Columns.Add("JUSTIFICACION", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("CLAVE_PARTIDA", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("CLAVE_PRODUCTO", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("UR", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("ENERO", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("FEBRERO", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("MARZO", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("ABRIL", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("MAYO", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("JUNIO", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("JULIO", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("AGOSTO", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("SEPTIEMBRE", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("OCTUBRE", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("NOVIEMBRE", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("DICIEMBRE", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("IMPORTE_TOTAL", System.Type.GetType("System.String"));
                    Dt_Detalle.Columns.Add("ID", System.Type.GetType("System.String"));

                    foreach (DataRow Dr_Session in Dt_Session.Rows)
                    {
                        if (Dr_Session["PARTIDA_ID"].ToString().Trim().Equals(Partida_ID.Trim()))
                        {
                            Fila = Dt_Detalle.NewRow();
                            Fila["DEPENDENCIA_ID"] = Dr_Session["DEPENDENCIA_ID"].ToString().Trim();
                            Fila["PROYECTO_ID"] = Dr_Session["PROYECTO_ID"].ToString().Trim();
                            Fila["CAPITULO_ID"] = Dr_Session["CAPITULO_ID"].ToString().Trim();
                            Fila["PARTIDA_ID"] = Dr_Session["PARTIDA_ID"].ToString().Trim();
                            Fila["PRODUCTO_ID"] = Dr_Session["PRODUCTO_ID"].ToString().Trim();
                            Fila["PRECIO"] = Dr_Session["PRECIO"];
                            Fila["JUSTIFICACION"] = Dr_Session["JUSTIFICACION"].ToString().Trim();
                            Fila["CLAVE_PARTIDA"] = Dr_Session["CLAVE_PARTIDA"].ToString().Trim();
                            Fila["CLAVE_PRODUCTO"] = Dr_Session["CLAVE_PRODUCTO"].ToString().Trim();
                            Fila["UR"] = Dr_Session["UR"].ToString().Trim();
                            Fila["ENERO"] = String.Format("{0:#,###,##0.00}", Dr_Session["ENERO"]);
                            Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}", Dr_Session["FEBRERO"]);
                            Fila["MARZO"] = String.Format("{0:#,###,##0.00}", Dr_Session["MARZO"]);
                            Fila["ABRIL"] = String.Format("{0:#,###,##0.00}", Dr_Session["ABRIL"]);
                            Fila["MAYO"] = String.Format("{0:#,###,##0.00}", Dr_Session["MAYO"]);
                            Fila["JUNIO"] = String.Format("{0:#,###,##0.00}", Dr_Session["JUNIO"]);
                            Fila["JULIO"] = String.Format("{0:#,###,##0.00}", Dr_Session["JULIO"]);
                            Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}", Dr_Session["AGOSTO"]);
                            Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}", Dr_Session["SEPTIEMBRE"]);
                            Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}", Dr_Session["OCTUBRE"]);
                            Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}", Dr_Session["NOVIEMBRE"]);
                            Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}", Dr_Session["DICIEMBRE"]);
                            Fila["IMPORTE_TOTAL"] = String.Format("{0:#,###,##0.00}", Dr_Session["IMPORTE_TOTAL"]);
                            Fila["ID"] = Dr_Session["ID"].ToString().Trim();
                            Dt_Detalle.Rows.Add(Fila);
                        }
                    }
                }
            }
            return Dt_Detalle;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de crear la tabla de partidas asignadas Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Obtener_Consecutivo_ID
    ///DESCRIPCIÓN          : Metodo para obtener el consecutivo del datatable 
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 16/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Obtener_Consecutivo_ID()
    {
        DataTable Dt_Partidas_Asignadas = new DataTable();
        Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas_Anterior"];
        int Contador;
        try
        {
            if (Dt_Partidas_Asignadas != null)
            {
                if (Dt_Partidas_Asignadas.Columns.Count > 0)
                {
                    if (Dt_Partidas_Asignadas.Rows.Count > 0)
                    {
                        Contador = -1;
                        foreach (DataRow Dr in Dt_Partidas_Asignadas.Rows)
                        {
                            Contador++;
                            Dr["ID"] = Contador.ToString().Trim();
                        }
                    }
                }
            }
            Session["Dt_Partidas_Asignadas_Anterior"] = Dt_Partidas_Asignadas;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al crear el datatable con consecutivo Erro[" + ex.Message + "]");
        }
    }
    #endregion

    #region EVENTOS GRID
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Partidas_Asignadas_RowDataBound
    ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 16/Noviembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Grid_Partida_Asignada_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        DataTable Dt_Partidas_Asignadas = new DataTable();
        Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas_Anterior"];

        try
        {
            if (Dt_Partidas_Asignadas != null)
            {
                if (Dt_Partidas_Asignadas.Rows.Count > 0)
                {
                    GridView Grid_Partidas_Detalle = (GridView)e.Row.Cells[4].FindControl("Grid_Partidas_Asignadas_Detalle");
                    DataTable Dt_Detalles = new DataTable();
                    String Partida_ID = String.Empty;

                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        Partida_ID = e.Row.Cells[1].Text.Trim();
                        Dt_Detalles = Crear_Dt_Detalles(Partida_ID);

                        Grid_Partidas_Detalle.Columns[0].Visible = true;
                        Grid_Partidas_Detalle.Columns[1].Visible = true;
                        Grid_Partidas_Detalle.Columns[2].Visible = true;
                        Grid_Partidas_Detalle.Columns[3].Visible = true;
                        Grid_Partidas_Detalle.Columns[4].Visible = true;
                        Grid_Partidas_Detalle.Columns[5].Visible = true;
                        Grid_Partidas_Detalle.Columns[6].Visible = true;
                        Grid_Partidas_Detalle.Columns[7].Visible = true;
                        Grid_Partidas_Detalle.Columns[8].Visible = true;
                        Grid_Partidas_Detalle.Columns[9].Visible = true;
                        Grid_Partidas_Detalle.Columns[10].Visible = true;
                        Grid_Partidas_Detalle.Columns[24].Visible = true;
                        Grid_Partidas_Detalle.Columns[25].Visible = true;
                        Grid_Partidas_Detalle.DataSource = Dt_Detalles;
                        Grid_Partidas_Detalle.DataBind();
                        Grid_Partidas_Detalle.Columns[1].Visible = false;
                        Grid_Partidas_Detalle.Columns[2].Visible = false;
                        Grid_Partidas_Detalle.Columns[3].Visible = false;
                        Grid_Partidas_Detalle.Columns[4].Visible = false;
                        Grid_Partidas_Detalle.Columns[5].Visible = false;
                        Grid_Partidas_Detalle.Columns[6].Visible = false;
                        Grid_Partidas_Detalle.Columns[7].Visible = false;
                        Grid_Partidas_Detalle.Columns[8].Visible = false;
                        Grid_Partidas_Detalle.Columns[25].Visible = false;
                        Grid_Partidas_Detalle.Columns[24].Visible = false;
                        Grid_Partidas_Detalle.Columns[0].Visible = false;

                        if (Hf_Tipo_Calendario.Value.Trim().Equals("Dependencia"))
                        {
                            Grid_Partidas_Detalle.Columns[9].Visible = false;
                        }
                        else
                        {
                            Grid_Partidas_Detalle.Columns[10].Visible = false;
                        }
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error:[" + Ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Partidas_Asignadas_Detalle_RowCreated
    ///DESCRIPCIÓN          : Evento del grid del movimiento del cursor sobre los registros
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 22/Noviembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Grid_Partidas_Asignadas_Detalle_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#507CD1';this.style.color='#FFFFFF'");
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;this.style.color=this.originalstyle;");
        }
    }
    #endregion
}
