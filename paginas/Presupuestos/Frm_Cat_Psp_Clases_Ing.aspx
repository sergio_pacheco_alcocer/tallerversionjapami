﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Cat_Psp_Clases_Ing.aspx.cs" Inherits="paginas_Presupuestos_Frm_Cat_Psp_Clases_Ing"
    Title="SIAC Sistema Integral Administrativo y Comercial" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">

    <script src="../../javascript/Js_Psp_Validaciones_Pronosticos_Ingresos.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
      <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../Imagenes/paginas/Updating.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Requisitos" style="background-color: #ffffff; width: 99%; height: 100%;">
                <table width="100%" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">
                            Clases
                        </td>
                    </tr>
                    <tr>
                        <td runat="server" id="Td_Error">
                            &nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" />&nbsp;
                            <asp:Label ID="Lbl_Encabezado_Error" runat="server" CssClass="estilo_fuente_mensaje_error" />
                            <br />
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" CssClass="estilo_fuente_mensaje_error" />
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0">
                    <tr align="center">
                        <td>
                            <div style="width: 99%; background-color: #2F4E7D; color: #FFFFFF; font-weight: bold;
                                font-style: normal; font-variant: normal; font-family: fantasy; height: 32px">
                                <table style="width: 100%; height: 28px;">
                                    <tr>
                                        <td align="left" style="width: 59%;">
                                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" CssClass="Img_Button"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" OnClick="Btn_Nuevo_Click" />
                                            <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" CssClass="Img_Button"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" OnClick="Btn_Modificar_Click" />
                                            <asp:ImageButton ID="Btn_Eliminar" runat="server" ToolTip="Eliminar" CssClass="Img_Button"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png" OnClick="Btn_Eliminar_Click"
                                                OnClientClick="return confirm('El estatus del registro cambiara a Inactivo. ¿Desea continuar?');" />
                                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" OnClick="Btn_Salir_Click" />
                                        </td>
                                        <td align="right" style="width: 41%;">
                                            <table style="width: 100%; height: 28px;">
                                                <tr>
                                                    <td style="vertical-align: middle; text-align: right; width: 20%;">
                                                        B&uacute;squeda:
                                                    </td>
                                                    <td style="width: 55%;">
                                                        <asp:TextBox ID="Txt_Busqueda" runat="server" Width="200px" MaxLength="100"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Rol_ID" runat="server" WatermarkCssClass="watermarked"
                                                            WatermarkText="<Ingrese Busqueda>" TargetControlID="Txt_Busqueda" />
                                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda" runat="server" TargetControlID="Txt_Busqueda"
                                                            FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ. " />
                                                    </td>
                                                    <td style="vertical-align: middle; width: 5%;">
                                                        <asp:ImageButton ID="Btn_Buscar" runat="server" ToolTip="Consultar" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                                            OnClick="Btn_Buscar_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
                <center>
                    <table width="97%">
                        <tr>
                            <td colspan="4">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 10%; text-align: left;">
                                *Clave Clase</td>
                            <td style="width: 30%; text-align: left;">
                                <asp:TextBox ID="Txt_Clave" runat="server" MaxLength="2" Width="40%" onBlur="javascript:Completar_Clave_Clase();"></asp:TextBox>
                                <asp:TextBox ID="Txt_Clave_Clase" runat="server" Width="40%" Enabled="false"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="Txt_Clave_FilteredTextBoxExtender1" runat="server"
                                    FilterType="Numbers" TargetControlID="Txt_Clave" Enabled="True">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="text-align: left;" colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 10%; text-align: left;">
                                *Año
                            </td>
                            <td style="width: 30%; text-align: left;">
                                <asp:TextBox ID="Txt_Anio" runat="server" Width="83%" MaxLength="4"></asp:TextBox>
                            </td>
                            <td style="width: 10%; text-align: left;">
                                &nbsp;&nbsp;*Estatus
                            </td>
                            <td style="width: 30%; text-align: left;">
                                <asp:DropDownList ID="Cmb_Estatus" runat="server" Enabled="False" Width="100%" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 10%; text-align: left;">
                                *Tipo
                            </td>
                            <td style="text-align: left;" colspan="3">
                                <asp:DropDownList ID="Cmb_Tipo" runat="server" Enabled="False" Width="100%" onChange="javascript:Completar_Clave_Clase();" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 10%; text-align: left;">
                                *Descripción
                            </td>
                            <td style="width: 90%; text-align: left;" colspan="3">
                                <asp:TextBox ID="Txt_Descripcion" runat="server" Width="99%" MaxLength="250" TextMode="MultiLine"
                                    onKeyUp="javascript:Validar_Cantidad_Caracteres(this);"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="Txt_Descripcion_TextBoxWatermarkExtender" runat="server"
                                    TargetControlID="Txt_Descripcion" WatermarkCssClass="watermarked" WatermarkText="&lt;Límite de Caracteres 250&gt;" />
                                <cc1:FilteredTextBoxExtender ID="Txt_Descripcion_FilteredTextBoxExtender" runat="server"
                                    FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" TargetControlID="Txt_Descripcion"
                                    ValidChars="Ññ.,:;/()áéíóúÁÉÍÓÚ ">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:HiddenField ID="Hf_Clave" runat="server" />
                                <asp:HiddenField ID="Hf_Clase_Id" runat="server" />
                                <asp:HiddenField ID="Hf_Tipo_Id" runat="server" />
                                <asp:HiddenField ID="Hf_Anio" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <hr />
                            </td>
                        </tr>
                    </table>
                    <table width="97%">
                        <tr>
                            <td align="center" style="width: 100%;">
                                <div style="overflow: auto; height: 320px; width: 99%; vertical-align: top; width: 100%;">
                                    <asp:GridView ID="Grid_Clases" runat="server" AutoGenerateColumns="false" Width="98%"
                                        OnSelectedIndexChanged="Grid_Clases_SelectedIndexChanged" Style="white-space: normal"
                                        CssClass="GridView_1" GridLines="None" OnSorting="Grid_Clases_Sorting" EmptyDataText="No se encuentro ningun registro"
                                        AllowSorting="true">
                                        <Columns>
                                            <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                <ItemStyle Width="3%" />
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="CLAVE_CLASE" HeaderText="Clave" SortExpression="CLAVE_CLASE">
                                                <HeaderStyle HorizontalAlign="Left" Width="8%" />
                                                <ItemStyle HorizontalAlign="Left" Width="8%" Font-Size="8pt" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DESCRIPCION_CLASE" HeaderText="Descripción" SortExpression="DESCRIPCION_CLASE">
                                                <HeaderStyle HorizontalAlign="Left" Width="40%" />
                                                <ItemStyle HorizontalAlign="Left" Width="40%" Font-Size="8pt" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CLAVE_NOMBRE_TIPO" HeaderText="Tipo" SortExpression="CLAVE_NOMBRE_TIPO">
                                                <HeaderStyle HorizontalAlign="Left" Width="40%" />
                                                <ItemStyle HorizontalAlign="Left" Width="40%" Font-Size="8pt" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ESTATUS_CLASE" HeaderText="Estatus" SortExpression="ESTATUS_CLASE">
                                                <HeaderStyle HorizontalAlign="Left" Width="7%" />
                                                <ItemStyle HorizontalAlign="Left" Width="7%" Font-Size="8pt" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CLASE_ING_ID" />
                                            <asp:BoundField DataField="TIPO_ID" />
                                            <asp:BoundField DataField="ANIO_CLASE" HeaderText="Año" SortExpression="ANIO_CLASE">
                                                <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                                <ItemStyle HorizontalAlign="Left" Width="5%" Font-Size="8pt" />
                                            </asp:BoundField>
                                        </Columns>
                                        <RowStyle CssClass="GridItem" />
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                    </table>
                </center>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
