﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.IO;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using AjaxControlToolkit;
using JAPAMI.Constantes;
using System.Data.OleDb;
using JAPAMI.Layout_Cuentas_Contables.Negocios;
using CarlosAg.ExcelXmlWriter;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.ReportSource;
using JAPAMI.Sessiones;


public partial class paginas_Presupuestos_Frm_Ope_Psp_Presupuesto_Aprovado : System.Web.UI.Page
{
    #region (Page_Load)
    /// ***************************************************************************************************
    /// Nombre: Page_Load
    /// Descripción: Carga la configuración inicial de la página.
    /// Parámetros: No Aplica.
    /// Usuario creo: Ramón Baeza Yépez
    /// Fecha Creó: 02/junio/2011
    /// ***************************************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (String.IsNullOrEmpty(Cls_Sessiones.Empleado_ID))
                Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

            if (!IsPostBack)
            {
                Session["Activa"] = true;//Variable para mantener la session activa.
                Configuracion_Inicial();//Habilita la configuracion inicial de los controles de la pagina.
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
        }
        Lbl_Mensaje_Error.Text = "";
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
    }
    #endregion

    #region (Metodos Generales)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Configuracion_Inicial
    ///DESCRIPCIÓN: Configuracion Inicial de los controles del formulario.
    ///CREO: Ramón Baeza Yépez
    /// Fecha Creó: 02/junio/2011
    ///*******************************************************************************
    private void Configuracion_Inicial()
    {
        Limpiar_Controles();
        Cargar_Años();
        Habilitar_Controles("Inicial");
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cargar_Años
    /// DESCRIPCION : Carga el combo de los Años.
    /// CREO        : Ramón Baeza Yépez
    /// Fecha Creó: 02/junio/2011
    ///*******************************************************************************
    private void Cargar_Años()
    {
        Cls_Ope_Psp_Layout_Cuentas_Contables_Negocio Negocio = new Cls_Ope_Psp_Layout_Cuentas_Contables_Negocio();
        DataTable Dt_Anios = new DataTable();

        try
        {
            Dt_Anios = Negocio.Obtener_Anio_Presupuestado();

            DateTime Fecha = DateTime.Now.AddYears(-5);
            Cmb_Anio.Items.Clear();
            Cmb_Anio.DataTextField = Ope_Psp_Presupuesto_Aprobado.Campo_Anio;
            Cmb_Anio.DataValueField = Ope_Psp_Presupuesto_Aprobado.Campo_Anio;
            Cmb_Anio.DataSource = Dt_Anios;
            Cmb_Anio.DataBind();
            Cmb_Anio.Items.Insert(0, new ListItem("<-- Seleccione -->", "0"));
            //for (int Cont_Años = 1; Cont_Años < 9; Cont_Años++)
            //{
            //    Cmb_Anio.Items.Insert(Cont_Años, new ListItem(Fecha.AddYears(Cont_Años).Year.ToString(), Fecha.AddYears(Cont_Años).Year.ToString()));
            //}

        }
        catch (Exception Ex)
        {
            throw new Exception("Error al cargar los años. Error: [" + Ex.Message.ToString() + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Ctlr
    /// DESCRIPCION : Limpia los Controles de la pagina.
    /// CREO        : Ramón Baeza Yépez
    /// Fecha Creó: 02/junio/2011
    ///*******************************************************************************
    private void Limpiar_Controles()
    {
        try
        {
            Grid_Presupuesto.DataSource = new DataTable();
            Grid_Presupuesto.DataBind();
            Cmb_Anio.SelectedIndex = -1;
            if (!Lbl_Mensaje_Error.Text.Contains("Los siguientes datos no fueron actualizados"))
            {
                Lbl_Mensaje_Error.Text = "";
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al limpiar los controles del formulario. Error: [" + Ex.Message.ToString() + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Habilitar_Controles
    /// DESCRIPCION : Habilita y Deshabilita los controles de la forma para prepara la página
    ///               para a siguiente operación
    /// PARAMETROS  : Operacion: Indica la operación que se desea realizar por parte del usuario
    ///                          si es una alta, modificacion
    /// CREO        : Ramón Baeza Yépez
    /// Fecha Creó: 02/junio/2011
    ///*******************************************************************************
    private void Habilitar_Controles(String Operacion)
    {
        Boolean Habilitado;

        try
        {
            Habilitado = false;
            switch (Operacion)
            {
                case "Inicial":
                    Habilitado = false;
                    Btn_Salir.ToolTip = "Inicio";
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Nuevo.Visible = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Subir.Enabled = false;
                    //AFU_Cargar_Archivo.Enabled = false;
                    break;
                case "Nuevo":
                    Habilitado = true;
                    Btn_Nuevo.ToolTip = "Dar de Alta";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Nuevo.Visible = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Subir.Enabled = true;
                    //AFU_Cargar_Archivo.Enabled = true;
                    break;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al Habilitar los Controles del formulario. Error:[" + ex.Message.ToString() + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Datos
    /// DESCRIPCION : Validar que se hallan proporcionado todos los datos.
    /// CREO        : Ramón Baeza Yépez
    /// Fecha Creó  : 02/junio/2012
    ///*******************************************************************************
    private Boolean Validar_Datos()
    {
        Boolean Datos_Validos = true;
        DataRow Linea;
        String Datos_Faltantes = string.Empty;
        DataTable Dt_Presupuesto = (DataTable)Session["PRESUPUESTO"];
        if (Cmb_Anio.SelectedIndex < 1)
        {
            Lbl_Mensaje_Error.Text = "Seleccione el año <br>";
            Datos_Validos = false;
        }
        else
        {
            for (int Cont_Lineas = 0; Cont_Lineas < Dt_Presupuesto.Rows.Count; Cont_Lineas++)
            {
                Linea = Dt_Presupuesto.Rows[Cont_Lineas];
                if (Cmb_Anio.SelectedIndex > 0)
                    Linea["Anio"] = Cmb_Anio.SelectedValue.Trim();
                if (String.IsNullOrEmpty(Linea["Fuente_Financiamiento_ID"].ToString().Trim())
                        || String.IsNullOrEmpty(Linea["Area_Funcional_ID"].ToString().Trim())
                        || String.IsNullOrEmpty(Linea["Programa_ID"].ToString().Trim())
                        || String.IsNullOrEmpty(Linea["Dependencia_ID"].ToString().Trim())
                        || String.IsNullOrEmpty(Linea["Partida_ID"].ToString().Trim())
                        || String.IsNullOrEmpty(Linea["Cuenta_Contable_ID"].ToString())
                        || String.IsNullOrEmpty(Linea["Anio"].ToString().Trim())
                        ) 
                {
                    Datos_Faltantes += "["+ Linea["FUENTE_FINANCIAMIENTO"].ToString().Trim() + "-" + Linea["AREA_FUNCIONAL"].ToString() + "-" +
                        Linea["PROGRAMA"].ToString().Trim() + "-" + Linea["DEPENDENCIA"].ToString().Trim() + "-" +
                        Linea["PARTIDA"].ToString().Trim() + "-   " + Linea["CUENTA_CONTABLE"].ToString() + " -    " +
                        Linea["ANIO"].ToString().Trim() + "]<br>";
                    Dt_Presupuesto.Rows.RemoveAt(Cont_Lineas);
                    Cont_Lineas--;
                }
            }
            if (!String.IsNullOrEmpty(Datos_Faltantes))
            {
                Lbl_Mensaje_Error.Text = "Los siguientes datos no fueron actualizados <br>" + Datos_Faltantes;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Visible = true;
            }
            Session["PRESUPUESTO"] = Dt_Presupuesto;

        }
        return Datos_Validos;
    }///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Copiar_Archivo_Servidor
    /// DESCRIPCION : Almacena el archivo en el servidor para su uso
    /// CREO        : Ramón Baeza Yépez
    /// FECHA_CREO  : 29/Octubre/2012
    ///*******************************************************************************
    private String Copiar_Archivo_Servidor(AsyncFileUpload Ctlr_AFU_Archivo)
    {
        String Full_Path = String.Empty;
        String Ruta_Archivo_Servidor = "";
        AsyncFileUpload Asy_FileUpload;
        try
        {
            if (Ctlr_AFU_Archivo.HasFile)
            {
                //Se obtiene la direccion en donde se va a guardar el archivo. Ej. C:/Dir_Servidor/..
                Ruta_Archivo_Servidor = Server.MapPath("Presupuesto");

                //Crear el Directorio Proveedores. Ej. Proveedores
                if (!Directory.Exists(Ruta_Archivo_Servidor))
                {
                    System.IO.Directory.CreateDirectory(Ruta_Archivo_Servidor);
                }

                if (Directory.Exists(Ruta_Archivo_Servidor))
                {
                    //Obtenemos el Ctlr AsyncFileUpload del GridView.
                    Asy_FileUpload = Ctlr_AFU_Archivo;

                    //Validamos que el nombre del archivo no se encuentre vacio.
                    if (!Asy_FileUpload.FileName.Equals(""))
                    {
                        //Valida que no exista el directorio, si no existe lo crea [172.16.0.103/Web/Project/Empleado/Empleado_00001]
                        DirectoryInfo Ruta_Completa_Dir_Empleado;
                        if (!Directory.Exists(Ruta_Archivo_Servidor))
                        {
                            Ruta_Completa_Dir_Empleado = Directory.CreateDirectory(Ruta_Archivo_Servidor);
                        }


                        //Se establece la ruta completa del archivo . Ej. [172.16.0.103/Web/Project/Empleado/Empleado_00001/File1.txt]
                        String Ruta_Completa_Archivo_A_Cargar = Ruta_Archivo_Servidor + @"\" + Asy_FileUpload.FileName;

                        //Se valida que el Ctlr AsyncFileUpload. Contenga el archivo a guardar.
                        if (Asy_FileUpload.HasFile)
                        {
                            //Se guarda el archivo. En la ruta indicada. Ej.  [172.16.0.103/Web/Project/Empleado/Empleado_00001/File1.txt]
                            Asy_FileUpload.SaveAs(Ruta_Completa_Archivo_A_Cargar);
                            //Guardamos en el campo hidden la ruta de la foto del empelado.
                            Full_Path = Ruta_Completa_Archivo_A_Cargar;
                        }
                    }
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "", "alert('No se ha seleccionado  ninguna foto a guardar');", true);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error generado al cargar la foto del empleado. ERROR: [" + Ex.Message + "]");
        }
        return Full_Path;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Leer_Tabla_Excel
    /// DESCRIPCION : Metodo que lee la abla del excel
    /// Parametros  : Path, ruta del archivo.
    ///             : Tabla, tabla que sera leida
    /// CREO        : Ramón Baeza Yépez
    /// FECHA_CREO  : 03/junio/2013
    ///*******************************************************************************
    public static DataTable Leer_Tabla_Excel(String Path, String Tabla)
    {
        OleDbConnection Conexion = new OleDbConnection();
        OleDbCommand Comando = new OleDbCommand();
        OleDbDataAdapter Adaptador = new OleDbDataAdapter();
        DataSet Ds_Informacion = new DataSet();
        String Query = String.Empty;

        try
        {
            Conexion.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                "Data Source=" + Path + ";" +
                "Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";

            Conexion.Open();
            Comando.CommandText = "Select * From [" + Tabla + "$]";
            Comando.Connection = Conexion;
            Adaptador.SelectCommand = Comando;
            Adaptador.Fill(Ds_Informacion);
            Conexion.Close();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al leer un archivo de excel. Error: " + Ex.Message + "]");
        }
        return Ds_Informacion.Tables[0];
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Obtener_Codigo_Programatico
    /// DESCRIPCION : Obtiene los identificadores del codigo programatico
    /// Parametros  : Dt_Presupuesto tabla con las claves del codigo
    /// CREO        : Ramón Baeza Yépez
    /// FECHA_CREO  : 03/junio/2013
    ///*******************************************************************************
    protected DataTable Obtener_Codigo_Programatico(DataTable Dt_Presupuesto)
    {
        Cls_Ope_Psp_Layout_Cuentas_Contables_Negocio Negocio = new Cls_Ope_Psp_Layout_Cuentas_Contables_Negocio();
        DataTable Dt_Fuente_Financiamiento;
        DataTable Dt_Area_Funcional;
        DataTable Dt_Proyectos_Programas;
        DataTable Dt_Dependencias;
        DataTable Dt_Partidas;
        DataTable Dt_Cuentas;
        try
        {
            Dt_Presupuesto.Columns.Add("Fuente_Financiamiento_ID", typeof(System.String));
            Dt_Presupuesto.Columns.Add("Area_Funcional_ID", typeof(System.String));
            Dt_Presupuesto.Columns.Add("Programa_ID", typeof(System.String));
            Dt_Presupuesto.Columns.Add("Dependencia_ID", typeof(System.String));
            Dt_Presupuesto.Columns.Add("Partida_ID", typeof(System.String));
            Dt_Presupuesto.Columns.Add("Cuenta_Contable_ID", typeof(System.String));
            Dt_Presupuesto.Columns.Add("Anio", typeof(System.String));
            Dt_Fuente_Financiamiento = Negocio.Consultar_Fuente_Financiamiento();
            Dt_Area_Funcional = Negocio.Consultar_Area_Funcional();
            Dt_Proyectos_Programas = Negocio.Consultar_Programas();
            Dt_Dependencias = Negocio.Consultar_Dependencias();
            Dt_Partidas = Negocio.Consultar_Partidas();
            Dt_Cuentas = Negocio.Consultar_Cuentas_Contables();
            //Obtenemos la fuente de financiamiento
            foreach (DataRow Linea in Dt_Fuente_Financiamiento.Rows)
            {
                foreach (DataRow Row in Dt_Presupuesto.Rows)
                {
                    if (Linea[Cat_SAP_Fuente_Financiamiento.Campo_Clave].ToString().Trim().Equals(Row["FUENTE_FINANCIAMIENTO"].ToString().Trim()))
                        Row["Fuente_Financiamiento_ID"] = Linea[Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID].ToString().Trim();
                }
            }
            //Obtenemos el Area funcional
            foreach (DataRow Linea in Dt_Area_Funcional.Rows)
            {
                foreach (DataRow Row in Dt_Presupuesto.Rows)
                {
                    if (Linea[Cat_SAP_Area_Funcional.Campo_Clave].ToString().Equals(Row["AREA_FUNCIONAL"].ToString()))
                        Row["Area_Funcional_ID"] = Linea[Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID].ToString().Trim();
                }
            }
            //Obtenemos el programa
            foreach (DataRow Linea in Dt_Proyectos_Programas.Rows)
            {
                foreach (DataRow Row in Dt_Presupuesto.Rows)
                {
                    if (Linea[Cat_Sap_Proyectos_Programas.Campo_Clave].ToString().Equals(Row["PROGRAMA"].ToString()))
                        Row["Programa_ID"] = Linea[Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id].ToString().Trim();
                }
            }
            //Obtenemos la dependencia id
            foreach (DataRow Linea in Dt_Dependencias.Rows)
            {
                foreach (DataRow Row in Dt_Presupuesto.Rows)
                {
                    if (Linea[Cat_Dependencias.Campo_Clave].ToString().Equals(Row["DEPENDENCIA"].ToString()))
                        Row["Dependencia_ID"] = Linea[Cat_Dependencias.Campo_Dependencia_ID].ToString().Trim();
                }
            }
            //Obtenemos la dependencia id
            foreach (DataRow Linea in Dt_Partidas.Rows)
            {
                foreach (DataRow Row in Dt_Presupuesto.Rows)
                {
                    if (Linea[Cat_Sap_Partidas_Especificas.Campo_Clave].ToString().Equals(Row["PARTIDA"].ToString()))
                        Row["Partida_ID"] = Linea[Cat_Sap_Partidas_Especificas.Campo_Partida_ID].ToString().Trim();
                }
            }
            //Obtenemos la dependencia id
            foreach (DataRow Linea in Dt_Cuentas.Rows)
            {
                foreach (DataRow Row in Dt_Presupuesto.Rows)
                {
                    if (Linea[Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString().Trim().Equals(Row["CUENTA_CONTABLE"].ToString().Trim()))
                        Row["Cuenta_Contable_ID"] = Linea[Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString().Trim();
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Obtener el Obtener_Codigo_Programatico. Error: " + Ex.Message + "]");
        }
        return Dt_Presupuesto;
    }
        /// *************************************************************************************************************************
        /// Nombre: Generar_Excel
        /// Descripción: Metodo que devuelve un libro de excel.
        /// Parámetros: Dt_Reporte.- Información que se mostrara en el reporte.
        /// Usuario Creo: Ramon Baeza Yepez
        /// Fecha Creó: 03/Julio/2013
        /// *************************************************************************************************************************
        public static CarlosAg.ExcelXmlWriter.Workbook Generar_Excel(DataTable Dt_Reporte)
        {
            //Creamos el libro de Excel.
            CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();

            try
            {
                Libro.Properties.Title = "Reporte";
                Libro.Properties.Created = DateTime.Now;
                Libro.Properties.Author = "JAPAMI";

                //Creamos una hoja que tendrá el libro.
                CarlosAg.ExcelXmlWriter.Worksheet Hoja = Libro.Worksheets.Add("PRESUPUESTO");
                //Agregamos un renglón a la hoja de excel.
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon = Hoja.Table.Rows.Add();
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");

                Estilo_Cabecera.Font.FontName = "Tahoma";
                Estilo_Cabecera.Font.Size = 10;
                Estilo_Cabecera.Font.Bold = true;
                Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera.Font.Color = "#FFFFFF";
                Estilo_Cabecera.Interior.Color = "#193d61";
                Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Contenido.Font.FontName = "Tahoma";
                Estilo_Contenido.Font.Size = 9;
                Estilo_Contenido.Font.Bold = true;
                Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Estilo_Contenido.Font.Color = "#000000";
                Estilo_Contenido.Interior.Color = "White";
                Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");


                if (Dt_Reporte is System.Data.DataTable)
                {
                    if (Dt_Reporte.Rows.Count > 0)
                    {
                        foreach (System.Data.DataColumn COLUMNA in Dt_Reporte.Columns)
                        {
                            if (COLUMNA is System.Data.DataColumn)
                            {
                                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(COLUMNA.ColumnName, "HeaderStyle"));
                            }
                        }

                        foreach (System.Data.DataRow FILA in Dt_Reporte.Rows)
                        {
                            if (FILA is System.Data.DataRow)
                            {
                                Renglon = Hoja.Table.Rows.Add();

                                foreach (System.Data.DataColumn COLUMNA in Dt_Reporte.Columns)
                                {
                                    if (COLUMNA is System.Data.DataColumn)
                                    {
                                        if (COLUMNA.DataType.FullName.Equals("System.DateTime"))
                                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(string.Format("{0:dd/MM/yyyy}", FILA[COLUMNA.ColumnName]), DataType.String, "BodyStyle"));
                                        else
                                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(FILA[COLUMNA.ColumnName].ToString(), DataType.String, "BodyStyle"));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar el reporte de Empleados. Error: [" + Ex.Message + "]");
            }
            return Libro;
        }
    
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Alta
    /// DESCRIPCION : Realiza el alta de los datos del archivo en la base de datos
    /// Parametros  : 
    /// CREO        : Ramón Baeza Yépez
    /// FECHA_CREO  : 03/junio/2013
    ///*******************************************************************************
    protected void Alta()
    {
        Cls_Ope_Psp_Layout_Cuentas_Contables_Negocio Negocio =  new Cls_Ope_Psp_Layout_Cuentas_Contables_Negocio();
        try
        {
            DataTable Dt_Presupuesto = (DataTable)Session["PRESUPUESTO"];
            Negocio.P_Anio = Cmb_Anio.SelectedValue.Trim();
            Negocio.P_Dt_Presupuesto = Dt_Presupuesto;
            Negocio.Alta();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error en el alta. Error: " + Ex.Message + "]");
        }
    }
    #endregion

    #region (Eventos)

    #region (Eventos Botones)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
    ///DESCRIPCIÓN: Alta de un Asignacion de una Deduccion Variable
    ///CREO: Ramon Baeza Yepez
    ///FECHA_CREO: 03/junio/2013
    ///*******************************************************************************
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Nuevo.ToolTip.Equals("Nuevo"))
            {
                Limpiar_Controles();//limpia los controles de la pagina.
                Habilitar_Controles("Nuevo");//Habilita la configuracion de para ejecutar el alta.              
            }
            else
            {
                if (Validar_Datos())
                {
                    Alta();                    
                    Configuracion_Inicial();
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Información", "alert('Operación Completa');", true);
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Salir de la Operacion Actual
    ///CREO: Ramon Baeza Yepez
    ///FECHA_CREO: 03/junio/2013
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Salir.ToolTip == "Inicio")
            {
                Session.Remove("Dt_Empleados");
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Configuracion_Inicial();//Habilita los controles para la siguiente operación del usuario en el catálogo
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Guardar_Archivo
    /// DESCRIPCION : Carga el archivo en el grid de la pantalla.
    /// CREO        : Ramon Baeza Yepez
    ///FECHA_CREO: 03/junio/2013
    ///*******************************************************************************
    protected void Guardar_Archivo(object sender, EventArgs e)
    {
        OleDbConnection Conexion = new OleDbConnection();
        OleDbCommand Comando = new OleDbCommand();
        OleDbDataAdapter Adaptador = new OleDbDataAdapter();
        DataSet Ds = new DataSet();
        String Nombre_Archivo = AFU_Cargar_Archivo.FileName;
        try
        {
            if (AFU_Cargar_Archivo.HasFile)
            {
                String Full_Path = Copiar_Archivo_Servidor(AFU_Cargar_Archivo);
                Session["Ruta"] = Full_Path;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al leer el documento de excel. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Ver_Contenido
    /// DESCRIPCION : Carga el archivo en el grid de la pantalla.
    /// CREO        : Ramon Baeza Yepez
    ///FECHA_CREO: 03/junio/2013
    ///*******************************************************************************
    protected void Ver_Contenido(object sender, EventArgs e)
    {
        OleDbConnection Conexion = new OleDbConnection();
        OleDbCommand Comando = new OleDbCommand();
        OleDbDataAdapter Adaptador = new OleDbDataAdapter();
        DataSet Ds = new DataSet();
        String Nombre_Archivo = AFU_Cargar_Archivo.FileName;
        DataTable Dt_Datos;
        try
        {
            if (!String.IsNullOrEmpty((String)Session["Ruta"]))
            {
                Dt_Datos = Leer_Tabla_Excel((String)Session["Ruta"], "PRESUPUESTO");
                Obtener_Codigo_Programatico(Dt_Datos);
                Session["PRESUPUESTO"] = Dt_Datos;
                Grid_Presupuesto.Columns[6].Visible = true;
                Grid_Presupuesto.DataSource = Dt_Datos;
                Grid_Presupuesto.DataBind();
                Grid_Presupuesto.Columns[6].Visible = false;
                Conexion.Close();
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al leer el documento de excel. Error: [" + Ex.Message + "]");
        }
    }

    /// *************************************************************************************
    /// NOMBRE: Mostrar_Excel
    /// DESCRIPCIÓN: Muestra el reporte en excel.
    /// PARÁMETROS: No Aplica
    /// USUARIO CREO: Ramon Baeza Yépez
    /// FECHA CREO: 03/Julio/2013
    /// *************************************************************************************
    private void Mostrar_Excel(string Ruta_Archivo, string Contenido)
    {
        try
        {
            System.IO.FileInfo ArchivoExcel = new System.IO.FileInfo(Ruta_Archivo);
            if (ArchivoExcel.Exists)
            {
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = Contenido;
                Response.AddHeader("Content-Disposition", "attachment;filename=" + ArchivoExcel.Name);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;
                Response.WriteFile(ArchivoExcel.FullName);
                Response.End();
            }
        }
        catch (Exception Ex)
        {

            throw new Exception("Error al mostrar el reporte en excel. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Descargar_Formato_Click
    ///DESCRIPCIÓN: descarga el formato 
    ///CREO: Ramon Baeza Yepez
    ///FECHA_CREO: 03/junio/2013
    ///*******************************************************************************
    protected void Btn_Descargar_Click(object sender, EventArgs e)
    {
        Cls_Ope_Psp_Layout_Cuentas_Contables_Negocio Negocio = new Cls_Ope_Psp_Layout_Cuentas_Contables_Negocio();
        String Nombre_Archivo = "Reporte de Presupuestos " + JAPAMI.Sessiones.Cls_Sessiones.Empleado_ID + ".xls";
        DiskFileDestinationOptions m_crDiskFileDestinationOptions = new DiskFileDestinationOptions();
        ReportDocument Reporte = new ReportDocument();
        DataTable Dt_Presupuesto;
        try
        {
            if (Cmb_Anio.SelectedIndex > 0)
            {
                Negocio.P_Anio = Cmb_Anio.SelectedValue;
                Dt_Presupuesto = Negocio.Consultar_Anio();
                CarlosAg.ExcelXmlWriter.Workbook Libro = Generar_Excel(Dt_Presupuesto);
                //Guardamos el archivo
                String Ruta_Archivo = @Server.MapPath("../../Reporte/");
                Libro.Save(Ruta_Archivo + Nombre_Archivo);
                Ruta_Archivo = "../../Reporte/" + Nombre_Archivo;
                Mostrar_Excel(Server.MapPath("../../Reporte/" + Nombre_Archivo), "application/vnd.ms-excel");
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion

    #endregion
}
