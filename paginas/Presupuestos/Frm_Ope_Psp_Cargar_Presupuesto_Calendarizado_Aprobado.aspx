﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" 
CodeFile="Frm_Ope_Psp_Cargar_Presupuesto_Calendarizado_Aprobado.aspx.cs" 
Inherits="paginas_Presupuestos_Frm_Ope_Psp_Cargar_Presupuesto_Calendarizado_Aprobado" Title="SIAC Sistema Integral Administrativo y Comercial" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" >
        function Grid_Anidado(Control, Fila) {
            var div = document.getElementById(Control);
            var img = document.getElementById('img' + Control);

            if (div.style.display == "none") {
                div.style.display = "inline";
                if (Fila == 'alt') {
                    img.src = "../imagenes/paginas/stocks_indicator_down.png";
                }
                else {
                    img.src = "../imagenes/paginas/stocks_indicator_down.png";
                }
                img.alt = "Contraer Registros";
            }
            else {
                div.style.display = "none";
                if (Fila == 'alt') {
                    img.src = "../imagenes/paginas/add_up.png";
                }
                else {
                    img.src = "../imagenes/paginas/add_up.png";
                }
                img.alt = "Expandir Registros";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <div id="Div_General" style="width: 98%;" visible="true" runat="server">
        <asp:UpdatePanel ID="Upl_Contenedor" runat="server">
            <ContentTemplate>
                <%--               <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upl_Contenedor"
                    DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                        </div>
                        <div class="processMessage" id="div_progress">
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>                    
                </asp:UpdateProgress>--%>
                <%--Div Encabezado--%>
                <div id="Div_Encabezado" runat="server">
                    <table style="width: 100%;" border="0" cellspacing="0">
                        <tr align="center">
                            <td colspan="4" class="label_titulo">
                               Cargar presupuesto calendarizado aprobado
                            </td>
                        </tr>
                        <tr align="left">
                            <td colspan="4">
                                <asp:Image ID="Img_Warning" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png"
                                    Visible="false" />
                                <asp:Label ID="Lbl_Informacion" runat="server" ForeColor="#990000"></asp:Label>
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left" valign="middle" colspan="2">
                                <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                    ToolTip="Inicio" OnClick="Btn_Salir_Click"  />
                            </td>
                            <td colspan="2">
                            </td>
                        </tr>
                    </table>
                </div>
                <%--Div listado de requisiciones--%>
                <div id="Div_Contenido" runat="server">
                    <table style="width: 100%; text-align: left;" border="1">
                        <caption>
                            <br />
                            <tr>
                                <td style="width: 20%">
                                    Año de presupuesto
                                </td>
                                <td style="width: 20%; text-align: left">
                                    <asp:DropDownList ID="Cmb_Anio" runat="server" OnSelectedIndexChanged="Cmb_Anio_SelectedIndexChanged"
                                        AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                                <td rowspan="2" align="center">
                                    <asp:Button ID="Btn_Cargar_Presupuesto_Calendarizad" runat="server" 
                                        Text="Cargar Presupuesto Calendarizado" CssClass="button" Width="240px"
                                        Height="45px" Style="text-align:center;" onclick="Btn_Cargar_Presupuesto_Calendarizad_Click" 
                                         />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label1" runat="server" Text="Total Aprobado"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Lbl_Aprobado" runat="server" Text="$0.00" ForeColor="Blue"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 20%">
                                    Texto Correo:
                                </td>
                                <td style=" text-align: left" colspan = "2">
                                    <asp:TextBox runat =  "server" ID="Txt_Texto_Correo" TextMode="MultiLine"
                                    Width = "98%" ></asp:TextBox>
                                </td>
                            </tr>
                        </caption>
                    </table>
                </div>
                <div style="width:100%; ">
                    <table style="width:100%;">
                        <tr>
                            <td style="width:100%;">
                                <asp:GridView ID="Grid_Partida_Asignada" runat="server" style="white-space:normal;"
                                AutoGenerateColumns="False" GridLines="None" 
                                Width="100%"  EmptyDataText="No se encontraron partidas aignadas" 
                                CssClass="GridView_1" HeaderStyle-CssClass="tblHead" OnRowDataBound="Grid_Partida_Asignada_RowDataBound"
                                > <%--DataKeyNames="PARTIDA_ID"--%> <%--
                                OnRowCreated="Grid_Partidas_Asignadas_Detalle_RowCreated"--%>
                                    <Columns>
                                     <asp:TemplateField> 
                                      <ItemTemplate> 
                                      <a href="javascript:Grid_Anidado('div<%# Eval("DEPENDENCIA_ID") %>', 'one');"> 
                                      <img id="imgdiv<%# Eval("DEPENDENCIA_ID") %>" alt="Click expander/contraer registros" border="0" src="../imagenes/paginas/stocks_indicator_down.png" /> 
                                      </a> 
                                                              </ItemTemplate> 
                                                              <AlternatingItemTemplate> 
                                                                   <a href="javascript:Grid_Anidado('div<%# Eval("DEPENDENCIA_ID") %>', 'alt');"> 
                                                                        <img id="imgdiv<%# Eval("DEPENDENCIA_ID") %>" alt="Click expander/contraer registros" border="0" src="../imagenes/paginas/stocks_indicator_down.png" /> 
                                                                   </a> 
                                                              </AlternatingItemTemplate> 
                                                              <ItemStyle HorizontalAlign ="Center" Font-Size="X-Small" Width="3%" />
                                                            </asp:TemplateField>      
                                                            <asp:BoundField DataField="DEPENDENCIA_ID" />
                                                            <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" >
                                                                <HeaderStyle HorizontalAlign="Left"/>
                                                                <ItemStyle HorizontalAlign="Left" Width="35%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="MONTO_TOTAL" HeaderText="TOTAL" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Center"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="20%" Font-Size="XX-Small" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField>
                                                               <ItemTemplate>
                                                                 </td>
                                                                 </tr> 
                                                                 <tr>
                                                                  <td colspan="100%">
                                                                   <div id="div<%# Eval("DEPENDENCIA_ID") %>" style="display:inline;position:relative;left:20px;" >                             
                                                                       <asp:GridView ID="Grid_Partidas_Asignadas_Detalle" runat="server" style="white-space:normal;"
                                                                           CssClass="GridView_Nested" HeaderStyle-CssClass="tblHead"
                                                                           AutoGenerateColumns="false" GridLines="Both" Width="98%">
                                                                           <Columns>
                                                                                <asp:BoundField DataField="DEPENDENCIA_ID"  />
                                                                                <asp:BoundField DataField="CODIGO_PROGRAMATICO" HeaderText="Código Programatico" >
                                                                                    <HeaderStyle HorizontalAlign="Left" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Left" Width="15%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>                                                                          
                                                                                <asp:BoundField DataField="ENERO" HeaderText="Enero" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="FEBRERO" HeaderText="Febrero" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="MARZO" HeaderText="Marzo" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="ABRIL" HeaderText="Abril" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="MAYO" HeaderText="Mayo" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="JUNIO" HeaderText="Junio" DataFormatString="{0:#,###,##0.00}">
                                                                                   <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="JULIO" HeaderText="Julio" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="AGOSTO" HeaderText="Agosto" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="SEPTIEMBRE" HeaderText="Septiembre" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="OCTUBRE" HeaderText="Octubre" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>                 
                                                                                <asp:BoundField DataField="NOVIEMBRE" HeaderText="Noviembre" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>                   
                                                                                <asp:BoundField DataField="DICIEMBRE" HeaderText="Diciembre" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>                
                                                                                <asp:BoundField DataField="IMPORTE_TOTAL" HeaderText="Total" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="8%" Font-Size="XX-Small"/>
                                                                                </asp:BoundField>
                                                                           </Columns>
                                                                           <SelectedRowStyle CssClass="GridSelected_Nested" />
                                                                           <PagerStyle CssClass="GridHeader_Nested" />
                                                                           <HeaderStyle CssClass="GridHeader_Nested" />
                                                                           <AlternatingRowStyle CssClass="GridAltItem_Nested" /> 
                                                                       </asp:GridView>
                                                                   </div>
                                                                  </td>
                                                                 </tr>
                                                               </ItemTemplate>
                                                            </asp:TemplateField>
                                                         </Columns>
                                                         <SelectedRowStyle CssClass="GridSelected" />
                                                            <PagerStyle CssClass="GridHeader" />
                                                            <HeaderStyle CssClass="GridHeader" />
                                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                                      </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
