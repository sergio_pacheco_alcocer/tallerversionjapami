﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" 
CodeFile="Frm_Ope_Psp_Asignar_Tipo_Presupuesto.aspx.cs" Inherits="paginas_Presupuestos_Frm_Ope_Psp_Asignar_Tipo_Presupuesto" 
Title="SIAC Sistema Integral Administrativo y Comercial" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
<asp:ScriptManager ID="ScptM_Parametros" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />  
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>                     
            </asp:UpdateProgress>
            <div id="Div_Requisitos" style="background-color:#ffffff; width:100%; height:100%;">
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo" colspan="2">Tipo de Presupuesto</td>
                    </tr>
                    <tr>
                        <td colspan ="2">
                          <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;">              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top">
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                            </table>                   
                          </div>                
                        </td>
                    </tr>
                    <tr>
                        <td colspan ="2">&nbsp;</td>                        
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" style="width:50%">
                            <asp:ImageButton ID="Btn_Nuevo" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" Width="24px" 
                                CssClass="Img_Button" AlternateText="Nuevo" ToolTip="Nuevo" 
                                onclick="Btn_Nuevo_Click" />
                            <asp:ImageButton ID="Btn_Salir" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" Width="24px" 
                                CssClass="Img_Button" AlternateText="Salir" ToolTip="Salir" 
                                onclick="Btn_Salir_Click"/>
                        </td>
                        <td style="width:50%">
                             &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:center;">
                            <table width="100%">
                                <tr>
                                    <td style="width:10%; text-align:left; cursor:default;" class="button_autorizar">
                                       <asp:Label ID="Lbl_Anio" runat="server" Text="* Año"></asp:Label>
                                    </td>
                                    <td style="width:30%; text-align:left; cursor:default;" class="button_autorizar">
                                       <asp:DropDownList id="Cmb_Anio" runat="server" Width="99%" > </asp:DropDownList>
                                    </td>
                                    <td style="width:20%; text-align:left; cursor:default;" class="button_autorizar">
                                       <asp:Label ID="Lbl_Tipo_Presupuesto" runat="server" Text="* Tipo Presupuesto"></asp:Label>
                                    </td>
                                    <td style="width:40%; text-align:left; cursor:default;" class="button_autorizar">
                                        <asp:RadioButtonList ID="Rbl_Tipos_Presupuestos" runat="server" >
                                            <asp:ListItem Text="Anual" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Mensual" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Acumulado Mensual" Value="2"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr><td colspan="2" style="height:0.3em;">&nbsp;</td></tr>
                    <tr>
                        <td colspan="2" style="text-align:center;">
                            <div style="overflow:auto; height:100px; max-height:250px; text-align:center; width:60%;">
                                <asp:GridView ID="Grid_Parametros" runat="server"  CssClass="GridView_1" Width="100%" 
                                    AutoGenerateColumns="False" GridLines="None"
                                    EmptyDataText="&quot;No se encuentra ningun registros&quot;">
                                    <Columns>
                                        <asp:BoundField DataField="ANIO" HeaderText="Año" 
                                            SortExpression="ANIO">
                                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                            <ItemStyle HorizontalAlign="Left" Width="20%" />
                                        </asp:BoundField>
                                       <asp:BoundField DataField="TIPO_DE_CONSULTA" HeaderText="Tipo de Presupuesto" 
                                            SortExpression="TIPO_DE_CONSULTA" >
                                            <HeaderStyle HorizontalAlign="Left" Width="40%" />
                                            <ItemStyle HorizontalAlign="Left" Width="40%" />
                                        </asp:BoundField> 
                                    </Columns>
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="tblHead" />
                                    <PagerStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>   
                <br />
                
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

