﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Cls_Cat_Psp_SubConcepto.Negocio;
using JAPAMI.Cls_Cat_Ing_Procesos.Negocio;

public partial class paginas_Presupuestos_Frm_Cat_Psp_SubConceptos_Ing : System.Web.UI.Page
{
    #region (Page_Load)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Page_Load
        ///DESCRIPCIÓN          : Metodo de inicio de la pagina
        ///PARAMETROS           :
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 08/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

            try
            {
                if (!IsPostBack)
                {
                    Mostar_Limpiar_Error(String.Empty, String.Empty, false);
                    SubConceptos_Inicio();
                    ViewState["SortDirection"] = "DESC";
                }
            }
            catch (Exception Ex)
            {
                Mostar_Limpiar_Error(String.Empty, "Error al cargar la pagina de clases. Error[" + Ex.Message + "]", true);
            }
        }
    #endregion

    #region (Metodos)
        #region (Generales)
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : SubConceptos_Inicio
            ///DESCRIPCIÓN          : Metodo para iniciar los controles de la pagina
            ///PARAMETROS           :
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 08/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void SubConceptos_Inicio()
            {
                try
                {
                    Mostar_Limpiar_Error(String.Empty, String.Empty, false);
                    Limpiar_Forma();
                    Habilitar_Forma(false);
                    Estado_Botones("Inicial");
                    Llenar_Combo_Estatus();
                    Llenar_Combos();
                    Llenar_Grid_SubConceptos();
                    Llenar_Combo_Conceptos();
                    Llenar_Combo_Cuentas_Contables();
                    Txt_Clave_Sub.Visible = true;
                    Llenar_Combo_Impuestos();
                    Llenar_Combo_Categorias();
                }
                catch (Exception Ex)
                {
                    Mostar_Limpiar_Error(String.Empty, "Error al cargar la pagina. Error[" + Ex.Message + "]", true);
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Habilitar_Forma
            ///DESCRIPCIÓN          : Metodo para habilitar o deshabilitar los controles
            ///PARAMETROS           1: Estatus: true o false para habilitar los controles
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 08/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Habilitar_Forma(Boolean Estatus)
            {
                Txt_Clave.Enabled = Estatus;
                Txt_Descripcion.Enabled = Estatus;
                Cmb_Estatus.Enabled = Estatus;
                Cmb_Concepto.Enabled = Estatus;
                Grid_SubConcepto.Enabled = !Estatus;
                Txt_Busqueda.Enabled = !Estatus;
                Btn_Buscar.Enabled = !Estatus;
                Txt_Clave_Sub.Enabled = false;
                Cmb_Cuenta_Contable.Enabled = Estatus;
                Txt_Anio.Enabled = Estatus;
                Txt_Importe.Enabled = Estatus;
                Cmb_Aplica_IVA.Enabled = Estatus;
                Cmb_Tipo_Impuesto.Enabled = Estatus;
                Txt_Tipo_Calculo.Enabled = Estatus;
                Txt_Cantidad_Calculo.Enabled = Estatus;
                Cmb_Categoria.Enabled = Estatus;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Limpiar_Forma
            ///DESCRIPCIÓN          : Metodo para limpiar los controles
            ///PARAMETROS           :
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 08/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Limpiar_Forma()
            {
                Txt_Clave.Text = String.Empty;
                Txt_Descripcion.Text = String.Empty;
                Cmb_Estatus.SelectedIndex = -1;
                Cmb_Concepto.SelectedIndex = -1;
                Txt_Busqueda.Text = String.Empty;
                Hf_Clave.Value = String.Empty;
                Hf_Subconcepto_Id.Value = String.Empty;
                Hf_Concepto_Id.Value = String.Empty;
                Cmb_Cuenta_Contable.SelectedIndex = -1;
                Txt_Clave_Sub.Text = String.Empty;
                Txt_Anio.Text = String.Empty;
                Txt_Importe.Text = String.Empty;
                Hf_Anio.Value = String.Empty;

                Cmb_Aplica_IVA.SelectedIndex = -1;
                Cmb_Tipo_Impuesto.SelectedIndex = -1;
                Txt_Tipo_Calculo.Text = String.Empty;
                Txt_Cantidad_Calculo.Text = String.Empty;
                Cmb_Categoria.SelectedIndex = -1;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Limpiar_Forma
            ///DESCRIPCIÓN          : Metodo para limpiar los controles
            ///PARAMETROS           :
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 16/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Mostar_Limpiar_Error(String Encabezado_Error, String Mensaje_Error, Boolean Mostrar)
            {
                Lbl_Encabezado_Error.Text = Encabezado_Error;
                Lbl_Mensaje_Error.Text = Mensaje_Error;
                Lbl_Mensaje_Error.Visible = Mostrar;
                Td_Error.Visible = Mostrar;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Estado_Botones
            ///DESCRIPCIÓN          : metodo que muestra los botones de acuerdo al estado en el que se encuentre
            ///PARAMETROS           1: String Estado: El estado de los botones solo puede tomar 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public void Estado_Botones(String Estado)
            {
                switch (Estado)
                {
                    case "Inicial":
                        //Boton Nuevo
                        Btn_Nuevo.ToolTip = "Nuevo";
                        Btn_Nuevo.Enabled = true;
                        Btn_Nuevo.Visible = true;
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                        //Boton Modificar
                        Btn_Modificar.ToolTip = "Modificar";
                        Btn_Modificar.Enabled = true;
                        Btn_Modificar.Visible = true;
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                        //Boton Eliminar
                        Btn_Eliminar.Enabled = true;
                        Btn_Eliminar.Visible = true;
                        //Boton Salir
                        Btn_Salir.ToolTip = "Inicio";
                        Btn_Salir.Enabled = true;
                        Btn_Salir.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                        break;
                    case "Nuevo":
                        //Boton Nuevo
                        Btn_Nuevo.ToolTip = "Guardar";
                        Btn_Nuevo.Enabled = true;
                        Btn_Nuevo.Visible = true;
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                        //Boton Modificar
                        Btn_Modificar.Visible = false;
                        //Boton Eliminar
                        Btn_Eliminar.Visible = false;
                        //Boton Salir
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Salir.Enabled = true;
                        Btn_Salir.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        break;
                    case "Modificar":
                        //Boton Nuevo
                        Btn_Nuevo.Visible = false;
                        //Boton Modificar
                        Btn_Modificar.ToolTip = "Actualizar";
                        Btn_Modificar.Enabled = true;
                        Btn_Modificar.Visible = true;
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                        //Boton Eliminar
                        Btn_Eliminar.Visible = false;
                        //Boton Salir
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Salir.Enabled = true;
                        Btn_Salir.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        break;
                }//fin del switch
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Validar_Datos
            ///DESCRIPCIÓN          : metodo para validar los datos del formulario
            ///PARAMETROS           :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 08/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public Boolean Validar_Datos()
            {
                Cls_Cat_Psp_SubConceptos_Negocio Negocio = new Cls_Cat_Psp_SubConceptos_Negocio();//conexion con la capa de negocios
                DataTable Dt_SubConceptos = new DataTable();
                String Mensaje_Encabezado = "Es necesario: ";
                String Mensaje_Error = String.Empty;
                Boolean Datos_Validos = true;

                try
                {
                    Mostar_Limpiar_Error(String.Empty, String.Empty, false);

                    if (String.IsNullOrEmpty(Txt_Clave.Text.Trim()))
                    {
                        Mensaje_Error += "&nbsp;&nbsp;* Instroducir una clave. <br />";
                        Datos_Validos = false;
                    }
                    else
                    {
                        if (Btn_Modificar.ToolTip.Trim().Equals("Actualizar"))
                        {
                            if (!Txt_Clave_Sub.Text.Trim().Equals(Hf_Clave.Value.Trim()) || !Cmb_Concepto.SelectedItem.Value.Trim().Equals(Hf_Concepto_Id.Value.Trim()) || !Txt_Anio.Text.Trim().Equals(Hf_Anio.Value.Trim()))
                            {
                                Negocio.P_Clave = Txt_Clave_Sub.Text.Trim();
                                Negocio.P_Concepto_Ing_ID= Cmb_Concepto.SelectedItem.Value.Trim();
                                if (String.IsNullOrEmpty(Txt_Anio.Text.Trim()))
                                {
                                    Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                                    Txt_Anio.Text = String.Format("{0:yyyy}", DateTime.Now);
                                }
                                else
                                {
                                    Negocio.P_Anio = Txt_Anio.Text.Trim();
                                }
                                Dt_SubConceptos = Negocio.Consultar_SubConcepto_Ing();
                                if (Dt_SubConceptos != null)
                                {
                                    if (Dt_SubConceptos.Rows.Count > 0)
                                    {
                                        Mensaje_Error += "&nbsp;&nbsp;* Instroducir una clave diferente o seleccionar otro Concepto. <br />";
                                        Datos_Validos = false;
                                    }
                                }
                            }
                        }
                        else if (Btn_Nuevo.ToolTip.Trim().Equals("Guardar"))
                        {
                            Negocio.P_Clave = Txt_Clave_Sub.Text.Trim();
                            Negocio.P_Concepto_Ing_ID = Cmb_Concepto.SelectedItem.Value.Trim();
                            if (String.IsNullOrEmpty(Txt_Anio.Text.Trim()))
                            {
                                Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                                Txt_Anio.Text = String.Format("{0:yyyy}", DateTime.Now);
                            }
                            else
                            {
                                Negocio.P_Anio = Txt_Anio.Text.Trim();
                            }
                            Dt_SubConceptos = Negocio.Consultar_SubConcepto_Ing();
                            if (Dt_SubConceptos != null)
                            {
                                if (Dt_SubConceptos.Rows.Count > 0)
                                {
                                    Mensaje_Error += "&nbsp;&nbsp;* Instroducir una clave diferente o seleccionar otro Concepto. <br />";
                                    Datos_Validos = false;
                                }
                            }
                        }
                    }
                    if (Cmb_Estatus.SelectedIndex <= 0)
                    {
                        Mensaje_Error += "&nbsp;&nbsp;* Seleccionar un estatus. <br />";
                        Datos_Validos = false;
                    }
                    if (Cmb_Concepto.SelectedIndex <= 0)
                    {
                        Mensaje_Error += "&nbsp;&nbsp;* Seleccionar un Concepto. <br />";
                        Datos_Validos = false;
                    }
                    if (String.IsNullOrEmpty(Txt_Descripcion.Text.Trim()))
                    {
                        Mensaje_Error += "&nbsp;&nbsp;* Instroducir una descripción. <br />";
                        Datos_Validos = false;
                    }

                    if (!Datos_Validos)
                    {
                        Mostar_Limpiar_Error(Mensaje_Encabezado, Mensaje_Error, true);
                    }
                }
                catch (Exception Ex)
                {
                    Mostar_Limpiar_Error(String.Empty, "Error al validar los datos. Error[" + Ex.Message + "]", true);
                }
                return Datos_Validos;
            }
        #endregion

        #region (Combos / Grid)
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Estatus
            ///DESCRIPCIÓN          : Metodo para llenar el combo de estatus
            ///PARAMETROS           :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Llenar_Combo_Estatus()
            {
                Cmb_Estatus.Items.Clear();
                Cmb_Estatus.Items.Insert(0, new ListItem("<--Seleccione-->", ""));
                Cmb_Estatus.Items.Insert(1, new ListItem("ACTIVO", "ACTIVO"));
                Cmb_Estatus.Items.Insert(2, new ListItem("INACTIVO", "INACTIVO"));
                Cmb_Estatus.DataBind();
                Cmb_Estatus.SelectedIndex = -1;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combos
            ///DESCRIPCIÓN          : Metodo para llenar los combos de requiere folio y el de mostrar tipo de pago
            ///PARAMETROS           :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 21/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Llenar_Combos()
            {
                Cmb_Aplica_IVA.Items.Clear();
                Cmb_Aplica_IVA.Items.Insert(0, new ListItem("<--Seleccione-->", ""));
                Cmb_Aplica_IVA.Items.Insert(1, new ListItem("SI", "SI"));
                Cmb_Aplica_IVA.Items.Insert(2, new ListItem("NO", "NO"));
                Cmb_Aplica_IVA.DataBind();
                Cmb_Aplica_IVA.SelectedIndex = -1;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_SubConceptos
            ///DESCRIPCIÓN          : Metodo para llenar el grid de los subconceptos
            ///PARAMETROS           :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 08/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Llenar_Grid_SubConceptos()
            {
                Cls_Cat_Psp_SubConceptos_Negocio Negocio = new Cls_Cat_Psp_SubConceptos_Negocio(); //conexion con la capa de negocios
                DataTable Dt_Subconceptos = new DataTable();

                try
                {
                    Negocio.P_Descripcion = String.Empty;
                    Negocio.P_Anio = String.Empty;
                    Dt_Subconceptos = Negocio.Consultar_SubConcepto_Concepto_Ing();
                    if (Dt_Subconceptos != null)
                    {
                        Grid_SubConcepto.Columns[6].Visible = true;
                        Grid_SubConcepto.Columns[5].Visible = true;
                        Grid_SubConcepto.Columns[7].Visible = true;
                        Grid_SubConcepto.Columns[8].Visible = true;
                        Grid_SubConcepto.Columns[10].Visible = true;
                        Grid_SubConcepto.Columns[11].Visible = true;
                        Grid_SubConcepto.Columns[12].Visible = true;
                        Grid_SubConcepto.Columns[13].Visible = true;
                        Grid_SubConcepto.Columns[14].Visible = true;
                        Grid_SubConcepto.DataSource = Dt_Subconceptos;
                        Grid_SubConcepto.DataBind();
                        Grid_SubConcepto.Columns[5].Visible = false;
                        Grid_SubConcepto.Columns[6].Visible = false;
                        Grid_SubConcepto.Columns[7].Visible = false;
                        Grid_SubConcepto.Columns[8].Visible = false;
                        Grid_SubConcepto.Columns[10].Visible = false;
                        Grid_SubConcepto.Columns[11].Visible = false;
                        Grid_SubConcepto.Columns[12].Visible = false;
                        Grid_SubConcepto.Columns[13].Visible = false;
                        Grid_SubConcepto.Columns[14].Visible = false;
                    }
                    else
                    {
                        Grid_SubConcepto.DataSource = new DataTable();
                    }
                }
                catch (Exception Ex)
                {
                    Mostar_Limpiar_Error("Error al cargar la tabla de SubConceptos. Error[" + Ex.Message + "]", String.Empty, true);
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Conceptos
            ///DESCRIPCIÓN          : Metodo para llenar el combo de los conceptos
            ///PARAMETROS           :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 08/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Llenar_Combo_Conceptos()
            {
                Cls_Cat_Psp_SubConceptos_Negocio Negocio = new Cls_Cat_Psp_SubConceptos_Negocio(); //conexion con la capa de negocios
                DataTable Dt_Conceptos = new DataTable();

                try
                {
                    Cmb_Concepto.Items.Clear();
                    Negocio.P_Estatus = "ACTIVO";
                    Negocio.P_Descripcion = String.Empty;
                    if (!String.IsNullOrEmpty(Txt_Anio.Text.Trim())) 
                    {
                        Negocio.P_Anio = Txt_Anio.Text.Trim();
                    }
                    else
                    {
                        Negocio.P_Anio = String.Empty;
                    }
                    Dt_Conceptos = Negocio.Consultar_Concepto_Ing();
                    if (Dt_Conceptos != null)
                    {
                        Cmb_Concepto.DataSource = Dt_Conceptos;
                        Cmb_Concepto.DataTextField = "CLAVE_NOMBRE";
                        Cmb_Concepto.DataValueField = Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID;
                        Cmb_Concepto.DataBind();
                        Cmb_Concepto.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                        Cmb_Concepto.SelectedIndex = -1;
                    }
                    else
                    {
                        Cmb_Concepto.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                    }
                }
                catch (Exception Ex)
                {
                    Mostar_Limpiar_Error(String.Empty, "Error al cargar el combo de Conceptos. Error[" + Ex.Message + "]", true);
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Cuentas_Contables
            ///DESCRIPCIÓN          : Metodo para llenar el combo de las cuentas contables
            ///PARAMETROS           :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 19/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Llenar_Combo_Cuentas_Contables()
            {
                Cls_Cat_Psp_SubConceptos_Negocio Negocio = new Cls_Cat_Psp_SubConceptos_Negocio(); //conexion con la capa de negocios
                DataTable Dt_Cuentas = new DataTable();

                try
                {
                    Cmb_Cuenta_Contable.Items.Clear();
                    Negocio.P_Estatus = "ACTIVO";
                    Negocio.P_Descripcion = String.Empty;
                    Dt_Cuentas = Negocio.Consultar_Cuentas_Contables();
                    if (Dt_Cuentas != null)
                    {
                        Cmb_Cuenta_Contable.DataSource = Dt_Cuentas;
                        Cmb_Cuenta_Contable.DataTextField = "CLAVE_NOMBRE";
                        Cmb_Cuenta_Contable.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                        Cmb_Cuenta_Contable.DataBind();
                        Cmb_Cuenta_Contable.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                        Cmb_Cuenta_Contable.SelectedIndex = -1;
                    }
                    else
                    {
                        Cmb_Cuenta_Contable.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                    }
                }
                catch (Exception Ex)
                {
                    Mostar_Limpiar_Error(String.Empty, "Error al cargar el combo de cuentas contables. Error[" + Ex.Message + "]", true);
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Buscar_SubConceptos
            ///DESCRIPCIÓN          : Metodo para buscar 
            ///PARAMETROS           1 Texto_Buscar: texto que buscaremos en el grid
            ///                     2 Dt_Datos: tabla de donde buscaremos los datos
            ///                     3 Tbl_Datos: Grid donde realizaremos la busqueda
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 08/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static void Buscar_SubConceptos(String Texto_Buscar, DataTable Dt_Datos, GridView Tbl_Datos)
            {
                DataView Dv_Datos = null;//Variable que almacena una vista que obtendra a partir de la búsqueda.
                String Expresion_Busqueda = String.Empty;//Variable que almacenara la expresion de búsqueda.

                try
                {
                    Dv_Datos = new DataView(Dt_Datos);//Creamos el objeto que almacenara una vista de la tabla de roles.

                    Expresion_Busqueda = String.Format("{0} '%{1}%'", Tbl_Datos.SortExpression, Texto_Buscar);
                    Dv_Datos.RowFilter = "CLAVE_SUBCONCEPTO like " + Expresion_Busqueda;
                    Dv_Datos.RowFilter += " Or DESCRIPCION_SUBCONCEPTO like " + Expresion_Busqueda;
                    Dv_Datos.RowFilter += " Or ESTATUS_SUBCONCEPTO like " + Expresion_Busqueda;
                    Dv_Datos.RowFilter += " Or CLAVE_NOMBRE_CONCEPTO like " + Expresion_Busqueda;

                    Tbl_Datos.Columns[5].Visible = true;
                    Tbl_Datos.Columns[6].Visible = true;
                    Tbl_Datos.Columns[7].Visible = true;
                    Tbl_Datos.Columns[8].Visible = true;
                    Tbl_Datos.Columns[10].Visible = true;
                    Tbl_Datos.Columns[11].Visible = true;
                    Tbl_Datos.Columns[12].Visible = true;
                    Tbl_Datos.Columns[13].Visible = true;
                    Tbl_Datos.Columns[14].Visible = true;
                    Tbl_Datos.DataSource = Dv_Datos;
                    Tbl_Datos.DataBind();
                    Tbl_Datos.Columns[5].Visible = false;
                    Tbl_Datos.Columns[6].Visible = false;
                    Tbl_Datos.Columns[7].Visible = false;
                    Tbl_Datos.Columns[8].Visible = false;
                    Tbl_Datos.Columns[10].Visible = false;
                    Tbl_Datos.Columns[11].Visible = false;
                    Tbl_Datos.Columns[12].Visible = false;
                    Tbl_Datos.Columns[13].Visible = false;
                    Tbl_Datos.Columns[14].Visible = false;
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al ejecutar la busqueda de subconceptos. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Categorias
            ///DESCRIPCIÓN          : Metodo para llenar el combo de las categorias
            ///PARAMETROS           :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Llenar_Combo_Categorias()
            {
                Cls_Cat_Psp_SubConceptos_Negocio Negocio = new Cls_Cat_Psp_SubConceptos_Negocio(); //conexion con la capa de negocios
                DataTable Dt_Cat = new DataTable();

                try
                {
                    Dt_Cat = Negocio.Consultar_Categorias();

                    Cmb_Categoria.Items.Clear();
                    Cmb_Categoria.DataSource = Dt_Cat;
                    Cmb_Categoria.DataTextField = Cat_Cor_Categoria_Conceptos_Cobros.Campo_Nombre_Categoria;
                    Cmb_Categoria.DataValueField = Cat_Cor_Categoria_Conceptos_Cobros.Campo_Concepto_Categoria_ID;
                    Cmb_Categoria.DataBind();
                    Cmb_Categoria.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                    Cmb_Categoria.SelectedIndex = -1;
                }
                catch (Exception Ex)
                {
                    Mostar_Limpiar_Error(String.Empty, "Error al cargar el combo de Categorias. Error[" + Ex.Message + "]", true);
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Impuestos
            ///DESCRIPCIÓN          : Metodo para llenar el combo de los impuestos
            ///PARAMETROS           :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Llenar_Combo_Impuestos()
            {
                Cls_Cat_Psp_SubConceptos_Negocio Negocio = new Cls_Cat_Psp_SubConceptos_Negocio(); //conexion con la capa de negocios
                DataTable Dt_Impuestos = new DataTable();

                try
                {
                    Dt_Impuestos = Negocio.Consultar_Impuestos();

                    Cmb_Tipo_Impuesto.Items.Clear();
                    Cmb_Tipo_Impuesto.DataSource = Dt_Impuestos;
                    Cmb_Tipo_Impuesto.DataTextField = Cat_Com_Impuestos.Campo_Nombre;
                    Cmb_Tipo_Impuesto.DataValueField = Cat_Com_Impuestos.Campo_Impuesto_ID;
                    Cmb_Tipo_Impuesto.DataBind();
                    Cmb_Tipo_Impuesto.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                    Cmb_Tipo_Impuesto.SelectedIndex = -1;
                }
                catch (Exception Ex)
                {
                    Mostar_Limpiar_Error(String.Empty, "Error al cargar el combo de impuestos. Error[" + Ex.Message + "]", true);
                }
            }
        #endregion
    #endregion

    #region (Eventos)
        #region (Botones)
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Buscar_Click
            ///DESCRIPCIÓN          : Evento del boton Buscar 
            ///PARAMETROS           :    
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 08/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
            {
                Cls_Cat_Psp_SubConceptos_Negocio Negocio = new Cls_Cat_Psp_SubConceptos_Negocio(); //Conexion a la capa de negocios
                DataTable Dt_SubConceptos = new DataTable();
                Mostar_Limpiar_Error(String.Empty, String.Empty, false);

                try
                {
                    if (!String.IsNullOrEmpty(Txt_Busqueda.Text.Trim()))
                    {
                        Negocio.P_Descripcion = String.Empty;
                        Negocio.P_Anio = String.Empty;
                        Dt_SubConceptos = Negocio.Consultar_SubConcepto_Concepto_Ing();
                        if (Dt_SubConceptos != null)
                        {
                            if (Dt_SubConceptos.Rows.Count > 0)
                            {
                                Buscar_SubConceptos(Txt_Busqueda.Text.Trim(), Dt_SubConceptos, Grid_SubConcepto);
                            }
                        }
                    }
                    else
                    {
                        Llenar_Grid_SubConceptos();
                    }
                }
                catch (Exception Ex)
                {
                    Mostar_Limpiar_Error(String.Empty, "Error en la busqueda de subConceptos. Error[" + Ex.Message + "]", true);
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
            ///DESCRIPCIÓN          : Evento del boton Salir 
            ///PARAMETROS           :    
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 08/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Btn_Salir_Click(object sender, EventArgs e)
            {
                Mostar_Limpiar_Error(String.Empty, String.Empty, false);
                switch (Btn_Salir.ToolTip)
                {
                    case "Cancelar":
                        SubConceptos_Inicio();
                        Grid_SubConcepto.SelectedIndex = -1;
                        break;

                    case "Inicio":
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        break;
                }//fin del switch
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Eliminar_Click
            ///DESCRIPCIÓN          : Evento del boton Eliminar 
            ///PARAMETROS           :    
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 08/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Btn_Eliminar_Click(object sender, EventArgs e)
            {
                Mostar_Limpiar_Error(String.Empty, String.Empty, false);
                Cls_Cat_Psp_SubConceptos_Negocio Negocio = new Cls_Cat_Psp_SubConceptos_Negocio();//Variable de conexion con la capa de negocios.
                try
                {
                    if (Grid_SubConcepto.SelectedIndex > (-1))
                    {
                        Negocio.P_SubConcepto_Ing_ID = Hf_Subconcepto_Id.Value.Trim();

                        if (Negocio.Eliminar_SubConcepto_Ing())
                        {
                            SubConceptos_Inicio();
                            Grid_SubConcepto.SelectedIndex = -1;

                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Eliminar", "alert('Operacion Completa: El estatus cambio a INACTIVO');", true);
                        }
                    }
                    else
                    {
                        Mostar_Limpiar_Error("Favor de seleccionar un registro de la tabla", String.Empty, true);
                    }
                }
                catch (Exception Ex)
                {
                    Mostar_Limpiar_Error(String.Empty, "Error al eliminar el SubConcepto. Error[" + Ex.Message + "]", true);
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Nuevo_Click
            ///DESCRIPCIÓN          : Evento del boton Guardar
            ///PARAMETROS           :    
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 08/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Btn_Nuevo_Click(object sender, EventArgs e)
            {
                Mostar_Limpiar_Error(String.Empty, String.Empty, false);
                Cls_Cat_Psp_SubConceptos_Negocio Negocio = new Cls_Cat_Psp_SubConceptos_Negocio();//Variable de conexion con la capa de negocios.
                try
                {
                    switch (Btn_Nuevo.ToolTip)
                    {
                        case "Nuevo":
                            Estado_Botones("Nuevo");
                            Limpiar_Forma();
                            Habilitar_Forma(true);
                            Cmb_Estatus.Enabled = false;
                            Cmb_Estatus.SelectedIndex = 1;
                            Txt_Anio.Text = String.Format("{0:yyyy}", DateTime.Now);
                            Cmb_Aplica_IVA.SelectedIndex = 1;
                            break;
                        case "Guardar":
                            if (Validar_Datos())
                            {
                                Negocio.P_Clave = Txt_Clave_Sub.Text.Trim();
                                Negocio.P_Anio = Txt_Anio.Text.Trim();
                                Negocio.P_Descripcion = Txt_Descripcion.Text.Trim().ToUpper();
                                Negocio.P_Impuesto = Cmb_Tipo_Impuesto.SelectedItem.Value.Trim();
                                Negocio.P_Cuenta_Contable_ID = Cmb_Cuenta_Contable.SelectedItem.Value.Trim();
                                Negocio.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado.Trim();
                                Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value.Trim();
                                Negocio.P_Concepto_Ing_ID = Cmb_Concepto.SelectedItem.Value.Trim();
                                Negocio.P_Requiere_IVA = Cmb_Aplica_IVA.SelectedItem.Value.Trim();
                                Negocio.P_Categoria = Cmb_Categoria.SelectedItem.Value.Trim();
                                Negocio.P_Tipo_Calculo = Txt_Tipo_Calculo.Text.Trim();
                                if (!String.IsNullOrEmpty(Txt_Cantidad_Calculo.Text.Trim()))
                                {
                                    Negocio.P_Cantidad_Calculo = Txt_Cantidad_Calculo.Text.Trim().Replace(",", "").Replace("$", "");
                                }
                                else 
                                {
                                    Negocio.P_Cantidad_Calculo = "0";
                                }

                                if (!String.IsNullOrEmpty(Txt_Importe.Text.Trim()))
                                {
                                    Negocio.P_Importe = Txt_Importe.Text.Trim().Replace(",", "").Replace("$", "");
                                }
                                else
                                {
                                    Negocio.P_Importe = "0";
                                }

                                if (Negocio.Guardar_SubConcepto_Ing())
                                {
                                    SubConceptos_Inicio();
                                    Grid_SubConcepto.SelectedIndex = -1;

                                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alta", "alert('Operacion Completa');", true);
                                }
                            }
                            break;
                    }//fin del swirch
                }
                catch (Exception Ex)
                {
                    Mostar_Limpiar_Error(String.Empty, "Error al guardar los subconceptos. Error[" + Ex.Message + "]", true);
                }
            }//fin del boton Nuevo

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Modificar_Click
            ///DESCRIPCIÓN          : Evento del boton Modificar
            ///PARAMETROS           :    
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 08/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Btn_Modificar_Click(object sender, EventArgs e)
            {
                Mostar_Limpiar_Error(String.Empty, String.Empty, false);
                Cls_Cat_Psp_SubConceptos_Negocio Negocio = new Cls_Cat_Psp_SubConceptos_Negocio();//Variable de conexion con la capa de negocios.
                try
                {
                    if (Grid_SubConcepto.SelectedIndex > (-1))
                    {
                        switch (Btn_Modificar.ToolTip)
                        {
                            //Validacion para actualizar un registro y para habilitar los controles que se requieran
                            case "Modificar":
                                Estado_Botones("Modificar");
                                Habilitar_Forma(true);
                                break;
                            case "Actualizar":
                                if (Validar_Datos())
                                {
                                    Negocio.P_Clave = Txt_Clave_Sub.Text.Trim();
                                    Negocio.P_Anio = Txt_Anio.Text.Trim();
                                    Negocio.P_Descripcion = Txt_Descripcion.Text.Trim().ToUpper();
                                    Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value.Trim();
                                    Negocio.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.Trim();
                                    Negocio.P_Concepto_Ing_ID = Cmb_Concepto.SelectedItem.Value.Trim();
                                    Negocio.P_SubConcepto_Ing_ID = Hf_Subconcepto_Id.Value.Trim();
                                    Negocio.P_Cuenta_Contable_ID = Cmb_Cuenta_Contable.SelectedItem.Value.Trim();
                                    Negocio.P_Concepto_Ing_ID = Cmb_Concepto.SelectedItem.Value.Trim();
                                    Negocio.P_Requiere_IVA = Cmb_Aplica_IVA.SelectedItem.Value.Trim();
                                    Negocio.P_Categoria = Cmb_Categoria.SelectedItem.Value.Trim();
                                    Negocio.P_Tipo_Calculo = Txt_Tipo_Calculo.Text.Trim();
                                    if (!String.IsNullOrEmpty(Txt_Cantidad_Calculo.Text.Trim()))
                                    {
                                        Negocio.P_Cantidad_Calculo = Txt_Cantidad_Calculo.Text.Trim().Replace(",", "").Replace("$", "");
                                    }
                                    else
                                    {
                                        Negocio.P_Cantidad_Calculo = "0";
                                    }

                                    if (!String.IsNullOrEmpty(Txt_Importe.Text.Trim()))
                                    {
                                        Negocio.P_Importe = Txt_Importe.Text.Trim().Replace(",", "").Replace("$", "");
                                    }
                                    else
                                    {
                                        Negocio.P_Importe = "0";
                                    }

                                    if (Negocio.Modificar_SubConcepto_Ing())
                                    {
                                        SubConceptos_Inicio();
                                        Grid_SubConcepto.SelectedIndex = -1;
                                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Actualizar", "alert('Operacion Completa');", true);
                                    }
                                }
                                break;
                        }//fin del switch
                    }
                    else
                    {
                        Mostar_Limpiar_Error("Favor de seleccionar un registro de la tabla", String.Empty, true);
                    }
                }
                catch (Exception Ex)
                {
                    Mostar_Limpiar_Error(String.Empty, "Error al modificar los subconceptos. Error[" + Ex.Message + "]", true);
                }
            }//fin de Modificar
        #endregion

        #region (Grid)
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Clases_SelectedIndexChanged
            ///DESCRIPCIÓN          : Evento de seleccion del grid
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 08/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Grid_SubConcepto_SelectedIndexChanged(object sender, EventArgs e)
            {
                Mostar_Limpiar_Error(String.Empty, String.Empty, false);
                String Clave = String.Empty;
                
                try
                {
                    if (Grid_SubConcepto.SelectedIndex > (-1))
                    {
                        Limpiar_Forma();
                        Llenar_Combo_Categorias();
                        Llenar_Combo_Impuestos();
                        Clave = HttpUtility.HtmlDecode(Grid_SubConcepto.SelectedRow.Cells[1].Text).ToString().Trim(); 
                        Txt_Clave.Text = Clave.Trim().Substring(6);
                        Txt_Clave_Sub.Text = HttpUtility.HtmlDecode(Grid_SubConcepto.SelectedRow.Cells[1].Text).ToString();

                        Txt_Descripcion.Text = HttpUtility.HtmlDecode(Grid_SubConcepto.SelectedRow.Cells[2].Text).ToString(); ;
                        Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(HttpUtility.HtmlDecode(Grid_SubConcepto.SelectedRow.Cells[4].Text.Trim()).ToString()));
                        Cmb_Concepto.SelectedIndex = Cmb_Concepto.Items.IndexOf(Cmb_Concepto.Items.FindByValue(HttpUtility.HtmlDecode(Grid_SubConcepto.SelectedRow.Cells[6].Text.Trim()).ToString()));
                        
                        Cmb_Cuenta_Contable.SelectedIndex = Cmb_Cuenta_Contable.Items.IndexOf(Cmb_Cuenta_Contable.Items.FindByValue(HttpUtility.HtmlDecode(Grid_SubConcepto.SelectedRow.Cells[7].Text.Trim()).ToString()));
                        
                        Txt_Anio.Text = HttpUtility.HtmlDecode(Grid_SubConcepto.SelectedRow.Cells[9].Text).ToString();
                        Txt_Importe.Text = HttpUtility.HtmlDecode(Grid_SubConcepto.SelectedRow.Cells[14].Text).ToString();
                        
                        Txt_Cantidad_Calculo.Text = HttpUtility.HtmlDecode(Grid_SubConcepto.SelectedRow.Cells[13].Text.Trim()).ToString();
                        Cmb_Tipo_Impuesto.SelectedIndex = Cmb_Tipo_Impuesto.Items.IndexOf(Cmb_Tipo_Impuesto.Items.FindByValue(HttpUtility.HtmlDecode(Grid_SubConcepto.SelectedRow.Cells[12].Text.Trim()).ToString()));
                        Cmb_Aplica_IVA.SelectedIndex = Cmb_Aplica_IVA.Items.IndexOf(Cmb_Aplica_IVA.Items.FindByValue(HttpUtility.HtmlDecode(Grid_SubConcepto.SelectedRow.Cells[10].Text.Trim()).ToString()));
                        Cmb_Categoria.SelectedIndex = Cmb_Categoria.Items.IndexOf(Cmb_Categoria.Items.FindByValue(HttpUtility.HtmlDecode(Grid_SubConcepto.SelectedRow.Cells[11].Text.Trim()).ToString()));
                        Txt_Tipo_Calculo.Text = HttpUtility.HtmlDecode(Grid_SubConcepto.SelectedRow.Cells[8].Text).ToString(); ;

                        Hf_Clave.Value = HttpUtility.HtmlDecode(Grid_SubConcepto.SelectedRow.Cells[1].Text).ToString();
                        Hf_Anio.Value = HttpUtility.HtmlDecode(Grid_SubConcepto.SelectedRow.Cells[9].Text).ToString();
                        Hf_Subconcepto_Id.Value = HttpUtility.HtmlDecode(Grid_SubConcepto.SelectedRow.Cells[5].Text).ToString();
                        Hf_Concepto_Id.Value = HttpUtility.HtmlDecode(Grid_SubConcepto.SelectedRow.Cells[6].Text).ToString();

                        Estado_Botones("Inicial");
                        Btn_Eliminar.Enabled = true;
                        Btn_Modificar.Enabled = true;
                        Habilitar_Forma(false);
                    }
                }
                catch (Exception Ex)
                {
                    Mostar_Limpiar_Error(String.Empty, "Error en la seleccion de un registro de la tabla de los subconceptos. Error[" + Ex.Message + "]", true);
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Clases_Sorting
            ///DESCRIPCIÓN          : Evento de ordenar las columnas de los grids
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 08/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Grid_SubConcepto_Sorting(object sender, GridViewSortEventArgs e)
            {
                Mostar_Limpiar_Error(String.Empty, String.Empty, false);
                DataTable Grid_Tipos_Sorting = new DataTable();

                try
                {
                    Llenar_Grid_SubConceptos();
                    Grid_Tipos_Sorting = (DataTable)Grid_SubConcepto.DataSource;
                    if (Grid_Tipos_Sorting != null)
                    {
                        DataView Dv_Vista = new DataView(Grid_Tipos_Sorting);
                        String Orden = ViewState["SortDirection"].ToString();
                        if (Orden.Equals("ASC"))
                        {
                            Dv_Vista.Sort = e.SortExpression + " DESC";
                            ViewState["SortDirection"] = "DESC";
                        }
                        else
                        {
                            Dv_Vista.Sort = e.SortExpression + " ASC";
                            ViewState["SortDirection"] = "ASC";
                        }

                        Grid_SubConcepto.Columns[5].Visible = true;
                        Grid_SubConcepto.Columns[6].Visible = true;
                        Grid_SubConcepto.Columns[7].Visible = true;
                        Grid_SubConcepto.Columns[8].Visible = true;
                        Grid_SubConcepto.Columns[10].Visible = true;
                        Grid_SubConcepto.Columns[11].Visible = true;
                        Grid_SubConcepto.Columns[12].Visible = true;
                        Grid_SubConcepto.Columns[13].Visible = true;
                        Grid_SubConcepto.Columns[14].Visible = true;
                        Grid_SubConcepto.DataSource = Dv_Vista;
                        Grid_SubConcepto.DataBind();
                        Grid_SubConcepto.Columns[5].Visible = false;
                        Grid_SubConcepto.Columns[6].Visible = false;
                        Grid_SubConcepto.Columns[7].Visible = false;
                        Grid_SubConcepto.Columns[8].Visible = false;
                        Grid_SubConcepto.Columns[10].Visible = false;
                        Grid_SubConcepto.Columns[11].Visible = false;
                        Grid_SubConcepto.Columns[12].Visible = false;
                        Grid_SubConcepto.Columns[13].Visible = false;
                        Grid_SubConcepto.Columns[14].Visible = false;
                    }
                }
                catch (Exception Ex)
                {
                    Mostar_Limpiar_Error("Error al ordenar la tabla de clases. Error[" + Ex.Message + "]", String.Empty, true);
                }
            }
        #endregion
    #endregion
}
