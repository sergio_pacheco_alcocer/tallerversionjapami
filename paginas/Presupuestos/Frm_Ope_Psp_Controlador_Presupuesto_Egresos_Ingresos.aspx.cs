﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Psp_Presupuesto_Egresos_Ingresos.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Ayudante_JQuery;
using JAPAMI.Nodo_Atributos;
using JAPAMI.Nodo_Arbol;
using Newtonsoft.Json;
using System.Collections.Generic;
using JAPAMI.Movimiento_Presupuestal.Negocio;
using JAPAMI.Clasificacion_Gasto.Negocio;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using System.Text;
using JAPAMI.Polizas.Negocios;
using JAPAMI.Ope_Psp_Movimientos_Ingresos.Negocio;
using CarlosAg.ExcelXmlWriter;
using Excel = Microsoft.Office.Interop.Excel;

public partial class paginas_Presupuestos_Frm_Ope_Psp_Controlador_Presupuesto_Egresos_Ingresos : System.Web.UI.Page
{
    const String FF_Nomina = "'RP13'";

    #region PAGE LOAD
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Page_Load
    ///DESCRIPCIÓN          : Metodo para el inicio de la pagina
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 25/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        if (!IsPostBack)
        {
            //validamos si es una petición para obtener el tipo de usuario que acceso a la pagina o si es una petición
            //para llenar los controles de la pagina
            String Accion = String.Empty;
            if (this.Request.QueryString["Accion"] != null)
            {
                if (!String.IsNullOrEmpty(this.Request.QueryString["Accion"].ToString().Trim()))
                {
                    Accion = this.Request.QueryString["Accion"].ToString().Trim();
                    if (Accion.Trim().Equals("Usuarios"))
                    {
                        Response.Clear();
                        Response.ContentType = "text/html";
                        String Json_Cadena = Obtener_Tipo_Usuario();
                        Response.Write(Json_Cadena);
                        Response.Flush();
                        Response.End();
                    }
                    else 
                    {
                        Obtener_Parametros();
                    }
                }
            }
        }
    }

    #endregion

    #region METODOS

    #region (Metodos Generales)
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Controlador_Inicio
    ///DESCRIPCIÓN          : Metodo para el inicio de la pagina
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 25/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Controlador_Inicio(String Busqueda, String Anio, String Tipo_Psp, String Mes, String Fecha_Ini, String Fecha_Fin,
        String FF_ID, String AF_ID, String UR_ID, String UR_F_ID, String PP_ID, String PP_F_ID, String PP_Ing_ID, String PP_IngF_ID,
        String Capitulo_ID, String Concepto_ID, String Partida_Gen_ID, String Partida_ID, String CapituloF_ID, String ConceptoF_ID,
        String Partida_GenF_ID, String PartidaF_ID, String Rubro_ID, String Tipo_ID, String Clase_ID, String Concepto_Ing_ID,
        String SubConcepto_ID, String FF_Ing_ID, String Rubro_F_ID, String Tipo_F_ID, String Clase_F_ID, String Concepto_F_Ing_ID,
        String SubConcepto_F_ID, String Estado, String Tot, String Tipo_Det, String Reporte,
        String No_Poliza, String Tipo_Poliza, String Mes_Anio, String No_Modificacion, String No_Solicitud)
    {
        String Accion = String.Empty;
        String Json_Cadena = String.Empty;
        Boolean Exixte_Reporte = false;

        Response.Clear();

        if (this.Request.QueryString["Accion"] != null)
        {
            if (!String.IsNullOrEmpty(this.Request.QueryString["Accion"].ToString().Trim()))
            {
                Accion = this.Request.QueryString["Accion"].ToString().Trim();
                switch (Accion)
                {
                    #region(Generales)
                    case "Anios":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Anios(Busqueda, Tipo_Psp);
                        break;
                    #endregion

                    #region(Combos Ingresos)
                    case "FF_Ing": //obtenermos los datos de las fuentes de financiamiento de ingresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_FF(Busqueda, Anio, "ING", FF_Ing_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID);
                        break;
                    case "PP_Ing": //obtenermos los datos de los proyecto programas de ingresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Programas(Busqueda, Anio, "ING", FF_Ing_ID, AF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID);
                        break;
                    case "R"://obtenermos los datos de los rubros de ingresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Rubros(Busqueda, Anio, Tipo_Psp, FF_Ing_ID, PP_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID);
                        break;
                    case "T"://obtenermos los datos de los tipos de ingresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Tipos(Busqueda, Anio, Tipo_Psp, FF_Ing_ID, PP_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID);
                        break;
                    case "CL"://obtenermos los datos de las clases de ingresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Clases(Busqueda, Anio, Tipo_Psp, FF_Ing_ID, PP_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID);
                        break;
                    case "CON_ING"://obtenermos los datos de los conceptos de ingresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Conceptos_Ing(Busqueda, Anio, Tipo_Psp, FF_Ing_ID, PP_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID);
                        break;
                    case "SUBCON"://obtenermos los datos de los subconceptos de ingresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_SubConceptos(Busqueda, Anio, Tipo_Psp, FF_Ing_ID, PP_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID);
                        break;
                    #endregion

                    #region(Combos_Egresos)
                    case "FF": //obtenermos los datos de las fuentes de financiamiento de egresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_FF(Busqueda, Anio, "EGR", FF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID);
                        break;
                    case "AF": //obtenermos los datos de las áreas funcionales
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_AF(Busqueda, Anio, Tipo_Psp, FF_ID, AF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID);
                        break;
                    case "UR": //obtenermos los datos de las dependencias
                        Exixte_Reporte = false; 
                        Json_Cadena = Obtener_UR(Busqueda, Anio, Tipo_Psp, FF_ID, AF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID);
                        break;
                    case "PP": //obtenermos los datos de los proyectos programas de egresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Programas(Busqueda, Anio, "EGR", FF_ID, AF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID);
                        break;
                    case "CAP": //obtenermos los datos de los capitulos de egresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Capitulos(Busqueda, Anio, Tipo_Psp, FF_ID, AF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID);
                        break;
                    case "CON": //obtenermos los datos de los conceptos de egresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Conceptos(Busqueda, Anio, Tipo_Psp, FF_ID, AF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID);
                        break;
                    case "PG": //obtenermos los datos de las partidas genericas de egresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Partidas_Genericas(Busqueda, Anio, Tipo_Psp, FF_ID, AF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID);
                        break;
                    case "P": //obtenermos los datos de las partidas especificas de egresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Partidas(Busqueda, Anio, Tipo_Psp, FF_ID, AF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID);
                        break;
                    #endregion

                    #region(Nivel Ingresos)
                    case "Nivel_FF_Ing": //obtenemos los datos del nivel de las fuentes de financiamientos de ingresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_FF_Ing(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID,
                                                          PP_Ing_ID, PP_IngF_ID, Rubro_F_ID, Tipo_F_ID, Clase_F_ID, Concepto_F_Ing_ID, SubConcepto_F_ID, Estado, Tot);
                        break;
                    case "Nivel_PP_Ing": //obtenemos los datos del nivel de los proyectos programas de ingresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_PP_Ing(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID,
                                                          PP_Ing_ID, PP_IngF_ID, Rubro_F_ID, Tipo_F_ID, Clase_F_ID, Concepto_F_Ing_ID, SubConcepto_F_ID, Estado, Tot);
                        break;
                    case "Nivel_Rubro":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_Rubro(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID,
                                                          PP_Ing_ID, PP_IngF_ID, Rubro_F_ID, Tipo_F_ID, Clase_F_ID, Concepto_F_Ing_ID, SubConcepto_F_ID, Estado, Tot);
                        break;
                    case "Nivel_Tipo":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_Tipos(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID,
                                                            PP_Ing_ID, PP_IngF_ID, Rubro_F_ID, Tipo_F_ID, Clase_F_ID, Concepto_F_Ing_ID, SubConcepto_F_ID, Estado, Tot);
                        break;
                    case "Nivel_Clase":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_Clases(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID,
                                                            PP_Ing_ID, PP_IngF_ID, Rubro_F_ID, Tipo_F_ID, Clase_F_ID, Concepto_F_Ing_ID, SubConcepto_F_ID, Estado, Tot);
                        break;
                    case "Nivel_Concepto_Ing":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_Conceptos_Ing(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID,
                                                            PP_Ing_ID, PP_IngF_ID, Rubro_F_ID, Tipo_F_ID, Clase_F_ID, Concepto_F_Ing_ID, SubConcepto_F_ID, Estado, Tot);
                        break;
                    case "Nivel_SubConcepto":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_SubConceptos(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID,
                                                            PP_Ing_ID, PP_IngF_ID, Rubro_F_ID, Tipo_F_ID, Clase_F_ID, Concepto_F_Ing_ID, SubConcepto_F_ID, Estado, Tot);
                        break;
                    #endregion

                    #region(Nivel Egresos)
                    case "Nivel_FF": //obtenemos los datos del nivel de las fuentes de financiamientos de egresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_FF(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID,
                            UR_F_ID, PP_F_ID, CapituloF_ID, ConceptoF_ID, Partida_GenF_ID, PartidaF_ID, AF_ID, Estado, Tot);
                        break;
                    case "Nivel_AF": //obtenemos los datos del nivel de las areas funcionales pertenecientes al presupuesto de egresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_AF(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID,
                            UR_F_ID, PP_F_ID, CapituloF_ID, ConceptoF_ID, Partida_GenF_ID, PartidaF_ID, AF_ID, Estado, Tot);
                        break;
                    case "Nivel_PP": //obtenemos los datos del nivel de los proyectos programas al presupuesto de egresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_PP(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID,
                            UR_F_ID, PP_F_ID, CapituloF_ID, ConceptoF_ID, Partida_GenF_ID, PartidaF_ID, AF_ID, Estado, Tot);
                        break;
                    case "Nivel_UR": //obtenemos los datos del nivel de las unidades responsables pertenecientes al presupuesto de egresos
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_UR(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID,
                            UR_F_ID, PP_F_ID, CapituloF_ID, ConceptoF_ID, Partida_GenF_ID, PartidaF_ID, AF_ID, Estado, Tot);
                        break;
                    case "Nivel_Capitulos":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_Capitulos(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID,
                            UR_F_ID, PP_F_ID, CapituloF_ID, ConceptoF_ID, Partida_GenF_ID, PartidaF_ID, AF_ID, Estado, Tot);
                        break;
                    case "Nivel_Concepto":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_Conceptos(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID,
                            UR_F_ID, PP_F_ID, CapituloF_ID, ConceptoF_ID, Partida_GenF_ID, PartidaF_ID, AF_ID, Estado, Tot);
                        break;
                    case "Nivel_Partida_Gen":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_Partida_Generica(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID,
                            UR_F_ID, PP_F_ID, CapituloF_ID, ConceptoF_ID, Partida_GenF_ID, PartidaF_ID, AF_ID, Estado, Tot);
                        break;
                    case "Nivel_Partida":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Nivel_Partida(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_ID, UR_ID, PP_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID,
                            UR_F_ID, PP_F_ID, CapituloF_ID, ConceptoF_ID, Partida_GenF_ID, PartidaF_ID, AF_ID, Estado, Tot);
                        break;
                    #endregion

                    #region(Detalles Ingresos)
                    case "Det_Mov_Ingresos":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Det_Mov_Ing(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID, PP_Ing_ID, Tipo_Det);
                        break;
                    case "Det_Recaudado_Ing":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Det_Recaudado_Ing(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID, PP_Ing_ID, Tipo_Det);
                        break;
                    #endregion

                    #region(Detalles Egresos)
                    case "Det_Mov_Egresos":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Det_Mov_Egr(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_ID, AF_ID, PP_ID, UR_ID,Capitulo_ID,Concepto_ID, Partida_Gen_ID, Partida_ID, Tipo_Det);
                        break;
                    case "Det_Mov_Con_Egr":
                        Exixte_Reporte = false;
                        Json_Cadena = Obtener_Det_Mov_Con_Egr(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_ID, AF_ID, PP_ID, UR_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID, Tipo_Det);
                        break;
                    #endregion

                    #region(Reporte)
                    case "Reporte":
                        Exixte_Reporte = true;
                        Json_Cadena = Obtener_Reporte(Anio, FF_ID, FF_Ing_ID, PP_ID, PP_F_ID, PP_Ing_ID, PP_IngF_ID, UR_ID, UR_F_ID,
                            AF_ID, Rubro_ID, Rubro_F_ID, Tipo_ID, Tipo_F_ID, Clase_ID, Clase_F_ID, Concepto_Ing_ID, Concepto_F_Ing_ID,
                            SubConcepto_ID, SubConcepto_F_ID, Capitulo_ID, CapituloF_ID, Concepto_ID, ConceptoF_ID, Partida_Gen_ID,
                            Partida_GenF_ID, Partida_ID, PartidaF_ID, Reporte, Tipo_Psp);
                        break;
                    case "Generar_Poliza":
                        Exixte_Reporte = true;
                        Json_Cadena = Obtener_Poliza(No_Poliza,Tipo_Poliza, Mes_Anio);
                        break;
                    case "Eliminar_Archivo":
                        Exixte_Reporte = true;

                        if (this.Request.QueryString["Ruta"] != null)
                        {
                            if (!String.IsNullOrEmpty(this.Request.QueryString["Ruta"].ToString().Trim()))
                            {
                                Json_Cadena = Eliminar_Archivo(this.Request.QueryString["Ruta"].ToString().Trim());
                            }
                        }
                        break;
                    case "Generar_Solicitud_Egr":
                        Exixte_Reporte = true;
                        Json_Cadena = Crear_Solicitud_Movimientos_Egresos(No_Modificacion, No_Solicitud, Tipo_Det, Anio); 
                        break;
                    case "Generar_Solicitud_Ing":
                        Exixte_Reporte = true;
                        Json_Cadena = Crear_Solicitud_Movimientos_Ingresos(Anio, No_Modificacion, No_Solicitud);
                        break;
                    case "Reporte_Mensual_Egresos":
                        Exixte_Reporte = true;
                        Json_Cadena = Obtener_Reporte_Mensual_Egresos(Anio, FF_ID, PP_ID, PP_F_ID, UR_ID, UR_F_ID,
                            AF_ID, Capitulo_ID, CapituloF_ID, Concepto_ID, ConceptoF_ID, Partida_Gen_ID,
                            Partida_GenF_ID, Partida_ID, PartidaF_ID);
                        break;
                    case "Reporte_Mensual_Ingresos":
                        Exixte_Reporte = true;
                        Json_Cadena = Obtener_Reporte_Mensual_Ingresos(Anio, FF_Ing_ID, PP_Ing_ID, PP_IngF_ID,
                            Rubro_ID, Rubro_F_ID, Tipo_ID, Tipo_F_ID, Clase_ID, Clase_F_ID, Concepto_Ing_ID, Concepto_F_Ing_ID,
                            SubConcepto_ID, SubConcepto_F_ID);
                        break;
                    #endregion

                    #region(Detalles Ingresos Reporte)
                    case "Det_Mov_Ingresos_Reporte":
                        Exixte_Reporte = true;
                        Json_Cadena = Obtener_Det_Mov_Ing_Reporte(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID, PP_Ing_ID, Tipo_Det);
                        break;
                    case "Det_Recaudado_Ing_Reporte":
                        Exixte_Reporte = true;
                        Json_Cadena = Obtener_Det_Recaudado_Ing_Reporte(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_Ing_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID, PP_Ing_ID, Tipo_Det);
                        break;
                    #endregion

                    #region(Detalles Egresos Reporte)
                    case "Det_Mov_Egresos_Reporte":
                        Exixte_Reporte = true;
                        Json_Cadena = Obtener_Det_Mov_Egr_Reporte(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_ID, AF_ID, PP_ID, UR_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID, Tipo_Det);
                        break;
                    case "Det_Mov_Con_Egr_Reporte":
                        Exixte_Reporte = true;
                        Json_Cadena = Obtener_Det_Mov_Con_Egr_Reporte(Anio, Mes, Fecha_Ini, Fecha_Fin, FF_ID, AF_ID, PP_ID, UR_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID, Tipo_Det);
                        break;
                    #endregion
                }
            }
        }
        if (!Exixte_Reporte)
        {
            Response.ContentType = "application/json";
            Response.Write(Json_Cadena);
            Response.Flush();
            Response.End();
        }
        else 
        {
            Response.ContentType = "text/html";
            Response.Write(HttpUtility.HtmlEncode(Json_Cadena));
            Response.Flush();
            Response.End();
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Obtener_Parametros
    ///DESCRIPCIÓN          : Metodo para obtener los parametros
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 25/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Obtener_Parametros() 
    {
        //Parametros de los combos
        String Anio = String.Empty;
        String Mes = String.Empty;
        String Fecha_Ini = String.Empty;
        String Fecha_Fin = String.Empty;
        String FF_ID = String.Empty;
        String FF_Ing_ID = String.Empty;
        String AF_ID = String.Empty;
        String UR_ID = String.Empty;
        String PP_ID = String.Empty;
        String PP_Ing_ID = String.Empty;
        String Capitulo_ID = String.Empty;
        String Concepto_ID = String.Empty;
        String Partida_Gen_ID = String.Empty;
        String Partida_ID = String.Empty;
        String Tipo_Psp = String.Empty;
        String Rubro_ID = String.Empty;
        String Tipo_ID = String.Empty;
        String Clase_ID = String.Empty;
        String Concepto_Ing_ID = String.Empty;
        String SubConcepto_ID = String.Empty;

        String UR_F_ID = String.Empty;
        String PP_F_ID = String.Empty;
        String PP_Ing_F_ID = String.Empty;
        String Capitulo_F_ID = String.Empty;
        String Concepto_F_ID = String.Empty;
        String Partida_Gen_F_ID = String.Empty;
        String Partida_F_ID = String.Empty;
        String Rubro_F_ID = String.Empty;
        String Tipo_F_ID = String.Empty;
        String Clase_F_ID = String.Empty;
        String Concepto_Ing_F_ID = String.Empty;
        String SubConcepto_F_ID = String.Empty;
        String Estado = String.Empty;
        String Tot = String.Empty;
        String Tipo_Det = String.Empty;

        String Busqueda = String.Empty;
        String Reporte = String.Empty;
        
        String No_Poliza = String.Empty;
        String Tipo_Poliza = String.Empty;
        String Mes_Anio = String.Empty;

        String No_Modificacion = String.Empty;
        String No_Solicitud = String.Empty;

        //obtenemos los parametros e los combos
        if (this.Request.QueryString["Tipo_Psp"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Tipo_Psp"].ToString().Trim()))
        {
            Tipo_Psp = this.Request.QueryString["Tipo_Psp"].ToString().Trim();
        }
        if (this.Request.QueryString["Anio"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Anio"].ToString().Trim()))
        {
            Anio = this.Request.QueryString["Anio"].ToString().Trim();
        }
        if (this.Request.QueryString["Mes"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Mes"].ToString().Trim()))
        {
            Mes = this.Request.QueryString["Mes"].ToString().Trim();
        }
        if (this.Request.QueryString["Fecha_Ini"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Fecha_Ini"].ToString().Trim()))
        {
            Fecha_Ini = this.Request.QueryString["Fecha_Ini"].ToString().Trim();
        }
        if (this.Request.QueryString["Fecha_Fin"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Fecha_Fin"].ToString().Trim()))
        {
            Fecha_Fin = this.Request.QueryString["Fecha_Fin"].ToString().Trim();
        }
        if (this.Request.QueryString["FF"] != null && !String.IsNullOrEmpty(this.Request.QueryString["FF"].ToString().Trim()))
        {
            FF_ID = this.Request.QueryString["FF"].ToString().Trim();
        }
        if (this.Request.QueryString["FF_Ing"] != null && !String.IsNullOrEmpty(this.Request.QueryString["FF_Ing"].ToString().Trim()))
        {
            FF_Ing_ID = this.Request.QueryString["FF_Ing"].ToString().Trim();
        }
        if (this.Request.QueryString["AF"] != null && !String.IsNullOrEmpty(this.Request.QueryString["AF"].ToString().Trim()))
        {
            AF_ID = this.Request.QueryString["AF"].ToString().Trim();
        }
        if (this.Request.QueryString["UR"] != null && !String.IsNullOrEmpty(this.Request.QueryString["UR"].ToString().Trim()))
        {
            UR_ID = this.Request.QueryString["UR"].ToString().Trim();
        }
        if (this.Request.QueryString["PP"] != null && !String.IsNullOrEmpty(this.Request.QueryString["PP"].ToString().Trim()))
        {
            PP_ID = this.Request.QueryString["PP"].ToString().Trim();
        }
        if (this.Request.QueryString["PP_Ing"] != null && !String.IsNullOrEmpty(this.Request.QueryString["PP_Ing"].ToString().Trim()))
        {
            PP_Ing_ID = this.Request.QueryString["PP_Ing"].ToString().Trim();
        }
        if (this.Request.QueryString["Cap"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Cap"].ToString().Trim()))
        {
            Capitulo_ID = this.Request.QueryString["Cap"].ToString().Trim();
        }
        if (this.Request.QueryString["Con"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Con"].ToString().Trim()))
        {
            Concepto_ID = this.Request.QueryString["Con"].ToString().Trim();
        }
        if (this.Request.QueryString["PG"] != null && !String.IsNullOrEmpty(this.Request.QueryString["PG"].ToString().Trim()))
        {
            Partida_Gen_ID = this.Request.QueryString["PG"].ToString().Trim();
        }
        if (this.Request.QueryString["P"] != null && !String.IsNullOrEmpty(this.Request.QueryString["P"].ToString().Trim()))
        {
            Partida_ID = this.Request.QueryString["P"].ToString().Trim();
        }
        if (this.Request.QueryString["R"] != null  && !String.IsNullOrEmpty(this.Request.QueryString["R"].ToString().Trim()))
        {
            Rubro_ID = this.Request.QueryString["R"].ToString().Trim();
        }
        if (this.Request.QueryString["T"] != null  && !String.IsNullOrEmpty(this.Request.QueryString["T"].ToString().Trim()))
        {
            Tipo_ID = this.Request.QueryString["T"].ToString().Trim();
        }
        if (this.Request.QueryString["Cl"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Cl"].ToString().Trim()))
        {
            Clase_ID = this.Request.QueryString["Cl"].ToString().Trim();
        }
        if (this.Request.QueryString["Con_Ing"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Con_Ing"].ToString().Trim()))
        {
            Concepto_Ing_ID = this.Request.QueryString["Con_Ing"].ToString().Trim();
        }
        if (this.Request.QueryString["SubCon"] != null && !String.IsNullOrEmpty(this.Request.QueryString["SubCon"].ToString().Trim()))
        {
            SubConcepto_ID = this.Request.QueryString["SubCon"].ToString().Trim();
        }
        if (this.Request["q"] != null  && !String.IsNullOrEmpty(this.Request["q"].ToString().Trim()))
        {
            Busqueda = this.Request["q"].ToString().Trim();
        }

   //obtenemos los parametros de los combos finales para obtener los rangos

        if (this.Request.QueryString["UR_F"] != null && !String.IsNullOrEmpty(this.Request.QueryString["UR_F"].ToString().Trim()))
        {
            UR_F_ID = this.Request.QueryString["UR_F"].ToString().Trim();
        }
        if (this.Request.QueryString["PP_F"] != null && !String.IsNullOrEmpty(this.Request.QueryString["PP_F"].ToString().Trim()))
        {
            PP_F_ID = this.Request.QueryString["PP_F"].ToString().Trim();
        }
        if (this.Request.QueryString["PP_Ing_F"] != null && !String.IsNullOrEmpty(this.Request.QueryString["PP_Ing_F"].ToString().Trim()))
        {
            PP_Ing_F_ID = this.Request.QueryString["PP_Ing_F"].ToString().Trim();
        }
        if (this.Request.QueryString["Cap_F"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Cap_F"].ToString().Trim()))
        {
            Capitulo_F_ID = this.Request.QueryString["Cap_F"].ToString().Trim();
        }
        if (this.Request.QueryString["Con_F"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Con_F"].ToString().Trim()))
        {
            Concepto_F_ID = this.Request.QueryString["Con_F"].ToString().Trim();
        }
        if (this.Request.QueryString["PG_F"] != null && !String.IsNullOrEmpty(this.Request.QueryString["PG_F"].ToString().Trim()))
        {
            Partida_Gen_F_ID = this.Request.QueryString["PG_F"].ToString().Trim();
        }
        if (this.Request.QueryString["P_F"] != null && !String.IsNullOrEmpty(this.Request.QueryString["P_F"].ToString().Trim()))
        {
            Partida_F_ID = this.Request.QueryString["P_F"].ToString().Trim();
        }
        if (this.Request.QueryString["R_F"] != null && !String.IsNullOrEmpty(this.Request.QueryString["R_F"].ToString().Trim()))
        {
            Rubro_F_ID = this.Request.QueryString["R_F"].ToString().Trim();
        }
        if (this.Request.QueryString["T_F"] != null && !String.IsNullOrEmpty(this.Request.QueryString["T_F"].ToString().Trim()))
        {
            Tipo_F_ID = this.Request.QueryString["T_F"].ToString().Trim();
        }
        if (this.Request.QueryString["Cl_F"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Cl_F"].ToString().Trim()))
        {
            Clase_F_ID = this.Request.QueryString["Cl_F"].ToString().Trim();
        }
        if (this.Request.QueryString["Con_Ing_F"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Con_Ing_F"].ToString().Trim()))
        {
            Concepto_Ing_F_ID = this.Request.QueryString["Con_Ing_F"].ToString().Trim();
        }
        if (this.Request.QueryString["SubCon_F"] != null && !String.IsNullOrEmpty(this.Request.QueryString["SubCon_F"].ToString().Trim()))
        {
            SubConcepto_F_ID = this.Request.QueryString["SubCon_F"].ToString().Trim();
        }


    //obtenemos los parametros del treegrid
        if (this.Request.QueryString["Param_FF_Ing"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_FF_Ing"].ToString().Trim()))
        {
            FF_Ing_ID = this.Request.QueryString["Param_FF_Ing"].ToString().Trim();
        }
        if (this.Request.QueryString["Param_PP_Ing"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_PP_Ing"].ToString().Trim()))
        {
            PP_Ing_ID = this.Request.QueryString["Param_PP_Ing"].ToString().Trim();
        }
        if (this.Request.QueryString["Param_Rubro"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_Rubro"].ToString().Trim()))
        {
            Rubro_ID = this.Request.QueryString["Param_Rubro"].ToString().Trim();
            Rubro_F_ID = String.Empty;
        }
        if (this.Request.QueryString["Param_Tipo"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_Tipo"].ToString().Trim()))
        {
            Tipo_ID= this.Request.QueryString["Param_Tipo"].ToString().Trim();
            Tipo_F_ID = String.Empty;
        }
        if (this.Request.QueryString["Param_Clase"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_Clase"].ToString().Trim()))
        {
            Clase_ID = this.Request.QueryString["Param_Clase"].ToString().Trim();
            Clase_F_ID = String.Empty;
        }
        if (this.Request.QueryString["Param_Concepto_Ing"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_Concepto_Ing"].ToString().Trim()))
        {
            Concepto_Ing_ID = this.Request.QueryString["Param_Concepto_Ing"].ToString().Trim();
            Concepto_Ing_F_ID = String.Empty;
        }
        if (this.Request.QueryString["Param_SubConcepto"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_SubConcepto"].ToString().Trim()))
        {
            SubConcepto_ID = this.Request.QueryString["Param_SubConcepto"].ToString().Trim();
            SubConcepto_F_ID = String.Empty;
        }

        //** EGRESOS**//
        if (this.Request.QueryString["Param_FF"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_FF"].ToString().Trim()))
        {
            FF_ID = this.Request.QueryString["Param_FF"].ToString().Trim();
        }
        if (this.Request.QueryString["Param_AF"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_AF"].ToString().Trim()))
        {
            AF_ID = this.Request.QueryString["Param_AF"].ToString().Trim();
        }
        if (this.Request.QueryString["Param_UR"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_UR"].ToString().Trim()))
        {
            UR_ID = this.Request.QueryString["Param_UR"].ToString().Trim();
            UR_F_ID = String.Empty;
        }
        if (this.Request.QueryString["Param_Progrm"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_Progrm"].ToString().Trim()))
        {
            PP_ID = this.Request.QueryString["Param_Progrm"].ToString().Trim();
            PP_F_ID = String.Empty;
        }
        if (this.Request.QueryString["Param_Cap"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_Cap"].ToString().Trim()))
        {
            Capitulo_ID = this.Request.QueryString["Param_Cap"].ToString().Trim();
            Capitulo_F_ID = String.Empty;
        }
        if (this.Request.QueryString["Param_Con"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_Con"].ToString().Trim()))
        {
            Concepto_ID = this.Request.QueryString["Param_Con"].ToString().Trim();
            Concepto_F_ID = String.Empty;
        }
        if (this.Request.QueryString["Param_Par_Gen"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_Par_Gen"].ToString().Trim()))
        {
            Partida_Gen_ID = this.Request.QueryString["Param_Par_Gen"].ToString().Trim();
            Partida_Gen_F_ID = String.Empty;
        }
        if (this.Request.QueryString["Param_Par"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Param_Par"].ToString().Trim()))
        {
            Partida_ID = this.Request.QueryString["Param_Par"].ToString().Trim();
            Partida_F_ID = String.Empty;
        }
        if (this.Request.QueryString["Estado"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Estado"].ToString().Trim()))
        {
            Estado = this.Request.QueryString["Estado"].ToString().Trim();
        }
        if (this.Request.QueryString["Tot"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Tot"].ToString().Trim()))
        {
            Tot = this.Request.QueryString["Tot"].ToString().Trim();
        }
        if (this.Request.QueryString["Tipo_Det"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Tipo_Det"].ToString().Trim()))
        {
            Tipo_Det = this.Request.QueryString["Tipo_Det"].ToString().Trim();
        }
        if (this.Request.QueryString["Reporte"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Reporte"].ToString().Trim()))
        {
            Reporte = this.Request.QueryString["Reporte"].ToString().Trim();
        }

        //datos de las polizas
        if (this.Request.QueryString["No_Poliza"] != null && !String.IsNullOrEmpty(this.Request.QueryString["No_Poliza"].ToString().Trim()))
        {
            No_Poliza = this.Request.QueryString["No_Poliza"].ToString().Trim();
        }
        if (this.Request.QueryString["Tipo_Poliza"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Tipo_Poliza"].ToString().Trim()))
        {
            Tipo_Poliza = this.Request.QueryString["Tipo_Poliza"].ToString().Trim();
        }
        if (this.Request.QueryString["Mes_Anio"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Mes_Anio"].ToString().Trim()))
        {
            Mes_Anio = this.Request.QueryString["Mes_Anio"].ToString().Trim();
        }

        if (this.Request.QueryString["No_Modificacion"] != null && !String.IsNullOrEmpty(this.Request.QueryString["No_Modificacion"].ToString().Trim()))
        {
            No_Modificacion = this.Request.QueryString["No_Modificacion"].ToString().Trim();
        }
        if (this.Request.QueryString["No_Solicitud"] != null && !String.IsNullOrEmpty(this.Request.QueryString["No_Solicitud"].ToString().Trim()))
        {
            No_Solicitud = this.Request.QueryString["No_Solicitud"].ToString().Trim();
        }

        Controlador_Inicio(Busqueda, Anio, Tipo_Psp, Mes, Fecha_Ini, Fecha_Fin, 
            FF_ID, AF_ID, UR_ID, UR_F_ID, PP_ID, PP_F_ID, PP_Ing_ID, PP_Ing_F_ID,
            Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID,
            Capitulo_F_ID, Concepto_F_ID, Partida_Gen_F_ID, Partida_F_ID,
            Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID, FF_Ing_ID,
            Rubro_F_ID, Tipo_F_ID, Clase_F_ID, Concepto_Ing_F_ID, SubConcepto_F_ID, 
            Estado, Tot, Tipo_Det, Reporte, No_Poliza, Tipo_Poliza, Mes_Anio, No_Modificacion, No_Solicitud);
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Obtener_Tipo_Usuario
    ///DESCRIPCIÓN          : Metodo para obtener el tipo de usuario
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 17/Agostp/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private String Obtener_Tipo_Usuario()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Mov_Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
        Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio_PSP = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
        String Administrador = String.Empty;
        DataTable Dt_Ramo33 = new DataTable();
        String Tipo_Usuario = String.Empty;
        String Gpo_Dep = "00000";
        DataTable Dt_Gpo_Dep = new DataTable();

        try
        {
            //verificar si el empleado que esta logueado tiene el rol de administrador del presupuesto municipal y estatal
            Negocio_PSP.P_Rol_ID = Cls_Sessiones.Rol_ID.Trim();
            Administrador = Negocio_PSP.Consultar_Rol();

            if (!String.IsNullOrEmpty(Administrador.Trim()))
            {
                if (Administrador.Trim().Equals("Presupuestos"))
                {
                    Tipo_Usuario = "Administrador";
                }
                else if (Administrador.Trim().Equals("Nomina")) 
                {
                    Tipo_Usuario = "Nomina";
                    //Tipo_Usuario = "Administrador";
                }
            }
            else
            {
                //verificamos si el usuario logueado es el administrador de ramo33
                Dt_Ramo33 = Mov_Negocio.Consultar_Usuario_Ramo33();
                if (Dt_Ramo33 != null && Dt_Ramo33.Rows.Count > 0)
                {
                    if (Dt_Ramo33.Rows[0][Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv].ToString().Trim().Equals(Cls_Sessiones.Empleado_ID.Trim())
                        || Dt_Ramo33.Rows[0][Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv_2].ToString().Trim().Equals(Cls_Sessiones.Empleado_ID.Trim()))
                    {
                        Tipo_Usuario = "Inversiones";
                    }
                    else
                    {
                        Tipo_Usuario = "Usuario";
                    }
                }
                else
                {
                    Tipo_Usuario = "Usuario";
                }

                if (Tipo_Usuario.Trim().Equals("Usuario"))
                {
                    //verificamos si no es algun coordinador administrativo de direccion
                    Negocio_PSP.P_Empleado_ID = Cls_Sessiones.Empleado_ID.Trim();
                    Dt_Gpo_Dep = Negocio_PSP.Obtener_Gpo_Dependencia();

                    if (Dt_Gpo_Dep != null && Dt_Gpo_Dep.Rows.Count > 0)
                    {
                        Gpo_Dep = Dt_Gpo_Dep.Rows[0][Cat_Dependencias.Campo_Grupo_Dependencia_ID].ToString().Trim();
                        Tipo_Usuario = "Coordinador";
                    }
                }
            }

            Tipo_Usuario = Tipo_Usuario.Trim() + "/" + Cls_Sessiones.Dependencia_ID_Empleado.Trim() + "/" + Gpo_Dep.Trim();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al obtener el tipo de usuario. Error[" + Ex.Message + "]");
        }
        return Tipo_Usuario;
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Obtener_Periodo
    ///DESCRIPCIÓN          : Metodo para obtener la fecha inicial y final de un mes
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 24/Agosto/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private String Obtener_Periodo(String Mes, String Anio, String Tipo)
    {
        String Fecha = String.Empty;
        int Dias = 1;
        try
        {
            if (Tipo.Trim().Equals("Inicial"))
            {
                Fecha =  "01/" + Mes + "/" + Anio; 
            }
            else 
            {
                Dias = DateTime.DaysInMonth(Convert.ToInt32(Anio), Convert.ToInt32(Mes));
                Fecha = Dias.ToString() + "/" + Mes + "/" + Anio; 
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al obtener el tipo de usuario. Error[" + Ex.Message + "]");
        }
        return Fecha;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Formatear_Fecha
    ///DESCRIPCIÓN          : consulta para formatear la fecha
    ///PARAMETROS           : 
    ///CREO                 : Leslie Gonzalez Vázquez
    ///FECHA_CREO           : 30/Marzo/2013 10:45 AM
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private String Formatear_Fecha(String Fecha)
    {
        String[] Fechas;
        String Mes = String.Empty;
        String Dia = String.Empty;
        String Anio = String.Empty;

        try
        {
            if (!String.IsNullOrEmpty(Fecha.Trim()))
            {
                Fechas = Fecha.Split('/');

                Mes = String.Format("{0:00}", Convert.ToInt32(Fechas[0].Trim()));
                Dia = String.Format("{0:00}", Convert.ToInt32(Fechas[1].Trim()));
                Anio = String.Format("{0:0000}", Convert.ToInt32(Fechas[2].Trim()));

                Fecha = Dia + "/" + Mes + "/" + Anio;
            }

        }
        catch (Exception Ex)
        {
            throw new Exception("Error al formatear la fecha Error[" + Ex.Message + "]");
        }

        return Fecha;
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Obtener_Det_Mov_Ing
    ///DESCRIPCIÓN          : Metodo para obtener el pie de pagina de un grid
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 25/Agosto/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private String Obtener_Pie_Pagina(String Json_Datos, DataTable Dt)
    {
        String Json = String.Empty;
        String Json_Total = String.Empty;

        try
        {
           Json_Total =  Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al obtener pie de pagina del grid. Error[" + Ex.Message + "]");
        }
        return Json;
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Obtener_Dependencias
    ///DESCRIPCIÓN          : Metodo para obtener los datos de las dependencias del usuario
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 14/Marzo/2013
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private String Obtener_Dependencias() 
    {
        Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
        DataTable Dt_Dependencias = new DataTable();
        String Dependencias = String.Empty;

        try
        {
            Negocio.P_Empleado_ID = Cls_Sessiones.Empleado_ID.Trim();
            Dt_Dependencias = Negocio.Consultar_Dependencias_Usuarios();

            if (Dt_Dependencias != null)
            {
                if (Dt_Dependencias.Rows.Count > 0)
                {
                    //obtenemos las dependencias
                    foreach (DataRow Dr_Ur in Dt_Dependencias.Rows)
                    {
                        Dependencias += "'" + Dr_Ur[Cat_Det_Empleado_UR.Campo_Dependencia_ID].ToString().Trim() + "',";
                    }
                    //quitamos la ultima coma
                    Dependencias = Dependencias.Substring(0, Dependencias.Length - 1);
                }
                else
                {
                    Dependencias = "'" + Cls_Sessiones.Dependencia_ID_Empleado.Trim() + "'";
                }
            }
            else
            {
                Dependencias = "'" + Cls_Sessiones.Dependencia_ID_Empleado.Trim() + "'";
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al obtener los datos de la dependencia del usuario. Error[" + Ex.Message + "]");
        }
        return Dependencias;
    }
    #endregion

    #region METODOS COMBOS JSON
        #region (GENERALES)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Anios
            ///DESCRIPCIÓN          : Metodo para obtener los años presupuestados aprobados
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Anios(String Busqueda, String Tipo_Psp)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json_Anios = string.Empty; //variable donde almacenaremos el string con los datos
                DataTable Dt_Anios = new DataTable(); //tabla para guardar los datos de los años
                String Seleccione = "[{\"ANIO\":\"SELECCIONE\",\"ANIO_ID\":\"\", \"selected\":true}]"; //cadena inicial del combo
                Json_Anios = Seleccione; //asignamos el inicio de la cadena

                try
                {
                    Negocio.P_Busqueda = Busqueda; //parametro por si el combo trae una busqueda
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp; //parametro del tipo de presupuesto
                    Dt_Anios = Negocio.Consultar_Anios(); //consultamos los años de los presupuestos
                    Dt_Anios.TableName = "anios"; //asignamos el nombre a la tabla
                    if (Dt_Anios != null) //validamos que no venga vacia la tabla
                    {
                        if (Dt_Anios.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim())) //si la busqueda es vacia agregamos el seleccione al combo
                            {
                                //obtenemos la cadena Json y agregamos el texto de seleccione al combo
                                Json_Anios = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt_Anios);
                                Json_Anios = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json_Anios.Substring(1);
                            }
                            else
                            {
                                //solo regresamos el valor de la busqueda
                                Json_Anios = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt_Anios);
                            }
                        }
                    }
                    return Json_Anios; //retornamos la cadena Json de los años
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Anios Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_FF
            ///DESCRIPCIÓN          : Metodo para obtener las fuentes de financiamiento
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_FF(String Busqueda, String Anio, String Tipo_Psp, String FF, String UR, String PP,
                 String Cap, String Con, String PG, String P, String R, String T, String Cl, String Con_Ing, String SubCon)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json_FF = string.Empty;
                DataTable Dt_FF = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"FUENTE_FINANCIAMIENTO_ID\":\"\", \"selected\":true}]";
                Json_FF = Seleccione;
                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        if (String.IsNullOrEmpty(UR))
                        {
                            Negocio.P_Dependencia_ID = Obtener_Dependencias();
                        }
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = FF_Nomina;
                    }

                    Dt_FF = Negocio.Consultar_FF();

                    if (Dt_FF != null)
                    {
                        if (Dt_FF.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json_FF = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt_FF);
                                Json_FF = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json_FF.Substring(1);
                            }
                            else
                            {
                                Json_FF = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt_FF);
                            }
                        }
                    }
                    return Json_FF;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Fuentes de financiamiento Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Programas
            ///DESCRIPCIÓN          : Metodo para obtener los proyectos programas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Programas(String Busqueda, String Anio, String Tipo_Psp, String FF,
                String AF, String UR, String PP, String Cap, String Con, String PG, String P)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json = string.Empty;
                DataTable Dt = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"PROYECTO_PROGRAMA_ID\":\"\", \"selected\":true}]";
                Json = Seleccione;

                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Area_Funcional_ID = AF;
                    Negocio.P_Fte_Financiamiento_Ing = FF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        if (String.IsNullOrEmpty(UR))
                        {
                            Negocio.P_Dependencia_ID = Obtener_Dependencias();
                        }
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = FF_Nomina;
                    }

                    Dt = Negocio.Consultar_Programa();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                                Json = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json.Substring(1);
                            }
                            else
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener los proyectos programas. Error[" + ex.Message + "]");
                }
            }
        #endregion

        #region (INGRESOS)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_FF
            ///DESCRIPCIÓN          : Metodo para obtener las fuentes de financiamiento
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Rubros(String Busqueda, String Anio, String Tipo_Psp, String FF,
                String PP, String R, String T, String Cl, String Con_Ing, String SubCon)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json = string.Empty;
                DataTable Dt = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"RUBRO_ID\":\"\", \"selected\":true}]";
                Json = Seleccione;

                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento_Ing = FF;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;

                    Dt = Negocio.Consultar_Rubros();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                                Json = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json.Substring(1);
                            }
                            else
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener los rubros Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Tipos
            ///DESCRIPCIÓN          : Metodo para obtener los tipos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Tipos(String Busqueda, String Anio, String Tipo_Psp, String FF,
                String PP, String R, String T, String Cl, String Con_Ing, String SubCon)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json = string.Empty;
                DataTable Dt = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"TIPO_ID\":\"\", \"selected\":true}]";
                Json = Seleccione;

                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento_Ing = FF;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;

                    Dt = Negocio.Consultar_Tipos();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                                Json = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json.Substring(1);
                            }
                            else
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener los tipos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Clases
            ///DESCRIPCIÓN          : Metodo para obtener las Clases
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Clases(String Busqueda, String Anio, String Tipo_Psp, String FF,
                String PP, String R, String T, String Cl, String Con_Ing, String SubCon)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json = string.Empty;
                DataTable Dt = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"CLASE_IN_ID\":\"\", \"selected\":true}]";
                Json = Seleccione;

                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento_Ing = FF;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;

                    Dt = Negocio.Consultar_Clases();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                                Json = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json.Substring(1);
                            }
                            else
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener las clases Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Conceptos_Ing
            ///DESCRIPCIÓN          : Metodo para obtener los conceptos de ingresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Conceptos_Ing(String Busqueda, String Anio, String Tipo_Psp, String FF,
                String PP, String R, String T, String Cl, String Con_Ing, String SubCon)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json = string.Empty;
                DataTable Dt = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"CONCEPTO_ING_ID\":\"\", \"selected\":true}]";
                Json = Seleccione;

                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento_Ing = FF;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;

                    Dt = Negocio.Consultar_Conceptos_Ing();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                                Json = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json.Substring(1);
                            }
                            else
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener los datos de los conceptos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_SubConceptos
            ///DESCRIPCIÓN          : Metodo para obtener las subconceptos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_SubConceptos(String Busqueda, String Anio, String Tipo_Psp, String FF,
                String PP, String R, String T, String Cl, String Con_Ing, String SubCon)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json = string.Empty;
                DataTable Dt = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"SUBCONCEPTO_ING_ID\":\"\", \"selected\":true}]";
                Json = Seleccione;
                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento_Ing = FF;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;

                    Dt = Negocio.Consultar_SubConceptos();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                                Json = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json.Substring(1);
                            }
                            else
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener los datos de los subconcepts Error[" + ex.Message + "]");
                }
            }
        #endregion

        #region (EGRESOS)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_AF
            ///DESCRIPCIÓN          : Metodo para obtener las areas funcionales
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 17/Agosto/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_AF(String Busqueda, String Anio, String Tipo_Psp, String FF, String AF,
                String UR, String PP, String Cap, String Con, String PG, String P)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json_AF = string.Empty;
                DataTable Dt_AF = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"AREA_FUNCIONAL_ID\":\"\", \"selected\":true}]";
                Json_AF = Seleccione;

                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Area_Funcional_ID = AF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        if (String.IsNullOrEmpty(UR))
                        {
                            Negocio.P_Dependencia_ID = Obtener_Dependencias();
                        }
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = FF_Nomina;
                    }

                    Dt_AF = Negocio.Consultar_AF();

                    if (Dt_AF != null)
                    {
                        if (Dt_AF.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json_AF = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt_AF);
                                Json_AF = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json_AF.Substring(1);
                            }
                            else
                            {
                                Json_AF = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt_AF);
                            }
                        }
                    }
                    return Json_AF;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al obtener las areas funcionales Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_UR
            ///DESCRIPCIÓN          : Metodo para obtener las dependencias
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_UR(String Busqueda, String Anio, String Tipo_Psp, String FF,
                String AF, String UR, String PP, String Cap, String Con, String PG, String P)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json = string.Empty;
                DataTable Dt = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"DEPENDENCIA_ID\":\"\", \"selected\":true}]";
                Json = Seleccione;

                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Area_Funcional_ID = AF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        if (String.IsNullOrEmpty(UR))
                        {
                            Negocio.P_Dependencia_ID = Obtener_Dependencias();
                        }
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = FF_Nomina;
                    }

                    Dt = Negocio.Consultar_UR();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                                Json = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json.Substring(1);
                            }
                            else
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener las unidades responsables Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Capitulos
            ///DESCRIPCIÓN          : Metodo para obtener los capitulos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Capitulos(String Busqueda, String Anio, String Tipo_Psp, String FF,
                String AF, String UR, String PP, String Cap, String Con, String PG, String P)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json = string.Empty;
                DataTable Dt = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"CAPITULO_ID\":\"\", \"selected\":true}]";
                Json = Seleccione;

                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Area_Funcional_ID = AF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        if (String.IsNullOrEmpty(UR))
                        {
                            Negocio.P_Dependencia_ID = Obtener_Dependencias();
                        }
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = FF_Nomina;
                    }

                    Dt = Negocio.Consultar_Capitulos();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                                Json = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json.Substring(1);
                            }
                            else
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener los capitulos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Conceptos
            ///DESCRIPCIÓN          : Metodo para obtener los conceptos de egresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Conceptos(String Busqueda, String Anio, String Tipo_Psp, String FF,
                String AF, String UR, String PP, String Cap, String Con, String PG, String P)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json = string.Empty;
                DataTable Dt = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"CONCEPTO_ID\":\"\", \"selected\":true}]";
                Json = Seleccione;

                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Area_Funcional_ID = AF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        if (String.IsNullOrEmpty(UR))
                        {
                            Negocio.P_Dependencia_ID = Obtener_Dependencias();
                        }
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = FF_Nomina;
                    }

                    Dt = Negocio.Consultar_Conceptos();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                                Json = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json.Substring(1);
                            }
                            else
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener los conceptos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Partidas_Genericas
            ///DESCRIPCIÓN          : Metodo para obtener las partidas genericas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Partidas_Genericas(String Busqueda, String Anio, String Tipo_Psp, String FF,
                String AF, String UR, String PP, String Cap, String Con, String PG, String P)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json = string.Empty;
                DataTable Dt = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"PARTIDA_GENERICA_ID\":\"\", \"selected\":true}]";
                Json = Seleccione;

                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Area_Funcional_ID = AF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        if (String.IsNullOrEmpty(UR))
                        {
                            Negocio.P_Dependencia_ID = Obtener_Dependencias();
                        }
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = FF_Nomina;
                    }

                    Dt = Negocio.Consultar_Partidas_Gen();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                                Json = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json.Substring(1);
                            }
                            else
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener las partidas genericas Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Partidas
            ///DESCRIPCIÓN          : Metodo para obtener las partidas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Partidas(String Busqueda, String Anio, String Tipo_Psp, String FF,
                String AF, String UR, String PP, String Cap, String Con, String PG, String P)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json = string.Empty;
                DataTable Dt = new DataTable();
                String Seleccione = "[{\"CLAVE_NOMBRE\":\"SELECCIONE\",\"PARTIDA_ID\":\"\", \"selected\":true}]";
                Json = Seleccione;

                try
                {
                    Negocio.P_Busqueda = Busqueda;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Tipo_Presupuesto = Tipo_Psp;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Area_Funcional_ID = AF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        if (String.IsNullOrEmpty(UR))
                        {
                            Negocio.P_Dependencia_ID = Obtener_Dependencias();
                        }
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = FF_Nomina;
                    }

                    Dt = Negocio.Consultar_Partidas();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            if (String.IsNullOrEmpty(Busqueda.Trim()))
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                                Json = Seleccione.Substring(0, Seleccione.Length - 1) + "," + Json.Substring(1);
                            }
                            else
                            {
                                Json = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener las partidas Error[" + ex.Message + "]");
                }
            }
        #endregion
    #endregion

    #region METODOS NIVELES JSON
        #region (INGRESOS)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_FF_Ing
            ///DESCRIPCIÓN          : Metodo para obtener las fuentes de financiamiento
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 22/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_FF_Ing(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
                String R, String T, String Cl, String Con_Ing, String SubCon, String PP, String PP_F,
                String R_F, String T_F, String Cl_F, String Con_Ing_F, String SubCon_F, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                String Json_Total = String.Empty;//variable para manejar la cadena json del total
                String Json_Tot = String.Empty; //variable para manejar la cadena json del total
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>(); //creamos una lista del objeto del nodo arbol
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid(); //Objeto de la clase de atributos
                // creamos un objeto de tipo JsonSerializerSettings para
                // serializar los datos que mostraremos en el nodo del árbol
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings(); 
                //controla cómo los valores nulos en. NET se manejan durante la serialización  
                //.aqui le estamos indicando que los pase por alto durante la serialización
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore; 
                
                try
                {   //obtenemos los parametros
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_ProgramaF_Ing_ID = PP_F;
                    Negocio.P_RubroF_ID = R_F;
                    Negocio.P_TipoF_ID = T_F;
                    Negocio.P_ClaseF_Ing_ID = Cl_F;
                    Negocio.P_ConceptoF_Ing_ID = Con_Ing_F;
                    Negocio.P_SubConceptoF_ID = SubCon_F;

                    Dt_Datos = Negocio.Consultar_Nivel_FF_Ing(); //obtenemos los datos del presupuesto de ingresos por fuente de financiamiento
                    if (!String.IsNullOrEmpty(Tot))
                    {   //obtenemos el total del presupuesto, para mostrarlo en el pie de pagina del treegrid
                        Json_Total = Obtener_Total_Ing(Dt_Datos);
                    }

                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {   //PASAMOS LOS DATOS CORRESPONDIENTES AL OBJETO DEL NODO ARBOL
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["ID"].ToString().Trim(); //ID DEL NODO
                                Nodo_Arbol.texto = Dr["CLAVE_NOMBRE"].ToString().Trim(); //NOMBRE DEL NODO
                                Nodo_Arbol.state = Estado; //ESTADO DEL NODO
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["ESTIMADO"]); //COLUMNAS DEL NODO
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["RECAUDADO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["DEV_REC"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["POR_RECAUDAR"]);
                                Nodo_Arbol.descripcion10 = String.Format("{0:n}", Dr["FF"]);
                                Nodo_Arbol.descripcion11 = String.Format("{0:n}", Dr["POR_RECAUDAR_ENE"]);
                                Nodo_Arbol.descripcion12 = String.Format("{0:n}", Dr["POR_RECAUDAR_FEB"]);
                                Nodo_Arbol.descripcion13 = String.Format("{0:n}", Dr["POR_RECAUDAR_MAR"]);
                                Nodo_Arbol.descripcion14 = String.Format("{0:n}", Dr["POR_RECAUDAR_ABR"]);
                                Nodo_Arbol.descripcion15 = String.Format("{0:n}", Dr["POR_RECAUDAR_MAY"]);
                                Nodo_Arbol.descripcion16 = String.Format("{0:n}", Dr["POR_RECAUDAR_JUN"]);
                                Nodo_Arbol.descripcion17 = String.Format("{0:n}", Dr["POR_RECAUDAR_JUL"]);
                                Nodo_Arbol.descripcion18 = String.Format("{0:n}", Dr["POR_RECAUDAR_AGO"]);
                                Nodo_Arbol.descripcion19 = String.Format("{0:n}", Dr["POR_RECAUDAR_SEP"]);
                                Nodo_Arbol.descripcion20 = String.Format("{0:n}", Dr["POR_RECAUDAR_OCT"]);
                                Nodo_Arbol.descripcion21 = String.Format("{0:n}", Dr["POR_RECAUDAR_NOV"]);
                                Nodo_Arbol.descripcion22 = String.Format("{0:n}", Dr["POR_RECAUDAR_DIC"]);
                                Nodo_Arbol.descripcion23 = String.Format("{0:n}", Dr["DEVENGADO_ENE"]);
                                Nodo_Arbol.descripcion24 = String.Format("{0:n}", Dr["DEVENGADO_FEB"]);
                                Nodo_Arbol.descripcion25 = String.Format("{0:n}", Dr["DEVENGADO_MAR"]);
                                Nodo_Arbol.descripcion26 = String.Format("{0:n}", Dr["DEVENGADO_ABR"]);
                                Nodo_Arbol.descripcion27 = String.Format("{0:n}", Dr["DEVENGADO_MAY"]);
                                Nodo_Arbol.descripcion28 = String.Format("{0:n}", Dr["DEVENGADO_JUN"]);
                                Nodo_Arbol.descripcion29 = String.Format("{0:n}", Dr["DEVENGADO_JUL"]);
                                Nodo_Arbol.descripcion30 = String.Format("{0:n}", Dr["DEVENGADO_AGO"]);
                                Nodo_Arbol.descripcion31 = String.Format("{0:n}", Dr["DEVENGADO_SEP"]);
                                Nodo_Arbol.descripcion32 = String.Format("{0:n}", Dr["DEVENGADO_OCT"]);
                                Nodo_Arbol.descripcion33 = String.Format("{0:n}", Dr["DEVENGADO_NOV"]);
                                Nodo_Arbol.descripcion34 = String.Format("{0:n}", Dr["DEVENGADO_DIC"]);
                                Nodo_Arbol.descripcion35 = String.Format("{0:n}", Dr["RECAUDADO_ENE"]);
                                Nodo_Arbol.descripcion36 = String.Format("{0:n}", Dr["RECAUDADO_FEB"]);
                                Nodo_Arbol.descripcion37 = String.Format("{0:n}", Dr["RECAUDADO_MAR"]);
                                Nodo_Arbol.descripcion38 = String.Format("{0:n}", Dr["RECAUDADO_ABR"]);
                                Nodo_Arbol.descripcion39 = String.Format("{0:n}", Dr["RECAUDADO_MAY"]);
                                Nodo_Arbol.descripcion40 = String.Format("{0:n}", Dr["RECAUDADO_JUN"]);
                                Nodo_Arbol.descripcion41 = String.Format("{0:n}", Dr["RECAUDADO_JUL"]);
                                Nodo_Arbol.descripcion42 = String.Format("{0:n}", Dr["RECAUDADO_AGO"]);
                                Nodo_Arbol.descripcion43 = String.Format("{0:n}", Dr["RECAUDADO_SEP"]);
                                Nodo_Arbol.descripcion44 = String.Format("{0:n}", Dr["RECAUDADO_OCT"]);
                                Nodo_Arbol.descripcion45 = String.Format("{0:n}", Dr["RECAUDADO_NOV"]);
                                Nodo_Arbol.descripcion46 = String.Format("{0:n}", Dr["RECAUDADO_DIC"]);
                                Nodo_Arbol.descripcion47 = String.Format("{0:n}", Dr["IMPORTE_ENE"]);
                                Nodo_Arbol.descripcion48 = String.Format("{0:n}", Dr["IMPORTE_FEB"]);
                                Nodo_Arbol.descripcion49 = String.Format("{0:n}", Dr["IMPORTE_MAR"]);
                                Nodo_Arbol.descripcion50 = String.Format("{0:n}", Dr["IMPORTE_ABR"]);
                                Nodo_Arbol.descripcion51 = String.Format("{0:n}", Dr["IMPORTE_MAY"]);
                                Nodo_Arbol.descripcion52 = String.Format("{0:n}", Dr["IMPORTE_JUN"]);
                                Nodo_Arbol.descripcion53 = String.Format("{0:n}", Dr["IMPORTE_JUL"]);
                                Nodo_Arbol.descripcion54 = String.Format("{0:n}", Dr["IMPORTE_AGO"]);
                                Nodo_Arbol.descripcion55 = String.Format("{0:n}", Dr["IMPORTE_SEP"]);
                                Nodo_Arbol.descripcion56 = String.Format("{0:n}", Dr["IMPORTE_OCT"]);
                                Nodo_Arbol.descripcion57 = String.Format("{0:n}", Dr["IMPORTE_NOV"]);
                                Nodo_Arbol.descripcion58 = String.Format("{0:n}", Dr["IMPORTE_DIC"]);
                                Nodo_Arbol.descripcion59 = String.Format("{0:n}", Dr["IMPORTE_TOTAL"]);

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid(); //ATRIBUTOS DEL NODO PARA IDENTIFICARLO
                                Atributos.valor1 = "Nivel_FF_Ing";
                                Atributos.valor2 = "";
                                Atributos.valor3 = "";
                                Atributos.valor4 = "";
                                Atributos.valor5 = "";
                                Atributos.valor6 = "";
                                Atributos.valor7 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor8 = "";

                                Nodo_Arbol.attributes = Atributos; //AGREGAMOS LOS ATRIBUTOS AL OBJETO DEL NODO ARBOL
                                Lista_Nodo_Arbol.Add(Nodo_Arbol); //AGREGAMOS A LA LISTA DEL NODO, EL OBJETO DEL NODO ARBOL
                            }

                            //UNA VEZ DE OBTENER LA LISTA CON TODOS LOS NODOS DEL ARBOL, PASAMOS A SERIALIZAR LA LISTA
                            //PARA GENERAR LA CADENA JSON CON LA QUE LLENAREMOS EL TREEGRID
                            //Formatting.Indented ES PARA INDICAR QUE QUEREMOS DEJAR UNA SANGRIA ENTRE NODOS HIJOS
                            Json_Datos = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            //ACOMODAMOS LA CADENA OBTENIDA PARA AGREGAR EL PIE DE PAGINA DEL TREEGRID CON EL TOTAL
                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt_Datos.Rows.Count + ", \"rows\":";
                                Json_Tot += Json_Datos;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json_Datos = Json_Tot;
                            }
                        }
                    }

                    return Json_Datos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Nivel_Fuente_Financiamiento_Ingresos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_PP_Ing
            ///DESCRIPCIÓN          : Metodo para obtener los proyectos programas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 22/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_PP_Ing(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
                String R, String T, String Cl, String Con_Ing, String SubCon, String PP, String PP_F,
                String R_F, String T_F, String Cl_F, String Con_Ing_F, String SubCon_F, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                String Json_Total = String.Empty;
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_ProgramaF_Ing_ID = PP_F;
                    Negocio.P_RubroF_ID = R_F;
                    Negocio.P_TipoF_ID = T_F;
                    Negocio.P_ClaseF_Ing_ID = Cl_F;
                    Negocio.P_ConceptoF_Ing_ID = Con_Ing_F;
                    Negocio.P_SubConceptoF_ID = SubCon_F;

                    Dt_Datos = Negocio.Consultar_Nivel_PP_Ing();
                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Ing(Dt_Datos);
                    }

                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["ID"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["CLAVE_NOMBRE"].ToString().Trim();
                                Nodo_Arbol.state = Estado;
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["ESTIMADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["RECAUDADO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["DEV_REC"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["POR_RECAUDAR"]);
                                Nodo_Arbol.descripcion10 = String.Format("{0:n}", Dr["FF"]);
                                Nodo_Arbol.descripcion11 = String.Format("{0:n}", Dr["POR_RECAUDAR_ENE"]);
                                Nodo_Arbol.descripcion12 = String.Format("{0:n}", Dr["POR_RECAUDAR_FEB"]);
                                Nodo_Arbol.descripcion13 = String.Format("{0:n}", Dr["POR_RECAUDAR_MAR"]);
                                Nodo_Arbol.descripcion14 = String.Format("{0:n}", Dr["POR_RECAUDAR_ABR"]);
                                Nodo_Arbol.descripcion15 = String.Format("{0:n}", Dr["POR_RECAUDAR_MAY"]);
                                Nodo_Arbol.descripcion16 = String.Format("{0:n}", Dr["POR_RECAUDAR_JUN"]);
                                Nodo_Arbol.descripcion17 = String.Format("{0:n}", Dr["POR_RECAUDAR_JUL"]);
                                Nodo_Arbol.descripcion18 = String.Format("{0:n}", Dr["POR_RECAUDAR_AGO"]);
                                Nodo_Arbol.descripcion19 = String.Format("{0:n}", Dr["POR_RECAUDAR_SEP"]);
                                Nodo_Arbol.descripcion20 = String.Format("{0:n}", Dr["POR_RECAUDAR_OCT"]);
                                Nodo_Arbol.descripcion21 = String.Format("{0:n}", Dr["POR_RECAUDAR_NOV"]);
                                Nodo_Arbol.descripcion22 = String.Format("{0:n}", Dr["POR_RECAUDAR_DIC"]);
                                Nodo_Arbol.descripcion23 = String.Format("{0:n}", Dr["DEVENGADO_ENE"]);
                                Nodo_Arbol.descripcion24 = String.Format("{0:n}", Dr["DEVENGADO_FEB"]);
                                Nodo_Arbol.descripcion25 = String.Format("{0:n}", Dr["DEVENGADO_MAR"]);
                                Nodo_Arbol.descripcion26 = String.Format("{0:n}", Dr["DEVENGADO_ABR"]);
                                Nodo_Arbol.descripcion27 = String.Format("{0:n}", Dr["DEVENGADO_MAY"]);
                                Nodo_Arbol.descripcion28 = String.Format("{0:n}", Dr["DEVENGADO_JUN"]);
                                Nodo_Arbol.descripcion29 = String.Format("{0:n}", Dr["DEVENGADO_JUL"]);
                                Nodo_Arbol.descripcion30 = String.Format("{0:n}", Dr["DEVENGADO_AGO"]);
                                Nodo_Arbol.descripcion31 = String.Format("{0:n}", Dr["DEVENGADO_SEP"]);
                                Nodo_Arbol.descripcion32 = String.Format("{0:n}", Dr["DEVENGADO_OCT"]);
                                Nodo_Arbol.descripcion33 = String.Format("{0:n}", Dr["DEVENGADO_NOV"]);
                                Nodo_Arbol.descripcion34 = String.Format("{0:n}", Dr["DEVENGADO_DIC"]);
                                Nodo_Arbol.descripcion35 = String.Format("{0:n}", Dr["RECAUDADO_ENE"]);
                                Nodo_Arbol.descripcion36 = String.Format("{0:n}", Dr["RECAUDADO_FEB"]);
                                Nodo_Arbol.descripcion37 = String.Format("{0:n}", Dr["RECAUDADO_MAR"]);
                                Nodo_Arbol.descripcion38 = String.Format("{0:n}", Dr["RECAUDADO_ABR"]);
                                Nodo_Arbol.descripcion39 = String.Format("{0:n}", Dr["RECAUDADO_MAY"]);
                                Nodo_Arbol.descripcion40 = String.Format("{0:n}", Dr["RECAUDADO_JUN"]);
                                Nodo_Arbol.descripcion41 = String.Format("{0:n}", Dr["RECAUDADO_JUL"]);
                                Nodo_Arbol.descripcion42 = String.Format("{0:n}", Dr["RECAUDADO_AGO"]);
                                Nodo_Arbol.descripcion43 = String.Format("{0:n}", Dr["RECAUDADO_SEP"]);
                                Nodo_Arbol.descripcion44 = String.Format("{0:n}", Dr["RECAUDADO_OCT"]);
                                Nodo_Arbol.descripcion45 = String.Format("{0:n}", Dr["RECAUDADO_NOV"]);
                                Nodo_Arbol.descripcion46 = String.Format("{0:n}", Dr["RECAUDADO_DIC"]);
                                Nodo_Arbol.descripcion47 = String.Format("{0:n}", Dr["IMPORTE_ENE"]);
                                Nodo_Arbol.descripcion48 = String.Format("{0:n}", Dr["IMPORTE_FEB"]);
                                Nodo_Arbol.descripcion49 = String.Format("{0:n}", Dr["IMPORTE_MAR"]);
                                Nodo_Arbol.descripcion50 = String.Format("{0:n}", Dr["IMPORTE_ABR"]);
                                Nodo_Arbol.descripcion51 = String.Format("{0:n}", Dr["IMPORTE_MAY"]);
                                Nodo_Arbol.descripcion52 = String.Format("{0:n}", Dr["IMPORTE_JUN"]);
                                Nodo_Arbol.descripcion53 = String.Format("{0:n}", Dr["IMPORTE_JUL"]);
                                Nodo_Arbol.descripcion54 = String.Format("{0:n}", Dr["IMPORTE_AGO"]);
                                Nodo_Arbol.descripcion55 = String.Format("{0:n}", Dr["IMPORTE_SEP"]);
                                Nodo_Arbol.descripcion56 = String.Format("{0:n}", Dr["IMPORTE_OCT"]);
                                Nodo_Arbol.descripcion57 = String.Format("{0:n}", Dr["IMPORTE_NOV"]);
                                Nodo_Arbol.descripcion58 = String.Format("{0:n}", Dr["IMPORTE_DIC"]);
                                Nodo_Arbol.descripcion59 = String.Format("{0:n}", Dr["IMPORTE_TOTAL"]);

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_PP_Ing";
                                Atributos.valor2 = "";
                                Atributos.valor3 = "";
                                Atributos.valor4 = "";
                                Atributos.valor5 = "";
                                Atributos.valor6 = "";
                                Atributos.valor7 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor8 = Dr["PP_ID"].ToString().Trim();

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Datos = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt_Datos.Rows.Count + ", \"rows\":";
                                Json_Tot += Json_Datos;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json_Datos = Json_Tot;
                            }
                        }
                    }

                    return Json_Datos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Nivel_Programas_Ingresos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_Rubro
            ///DESCRIPCIÓN          : Metodo para obtener los rubros
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 27/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_Rubro(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
                String R, String T, String Cl, String Con_Ing, String SubCon, String PP, String PP_F,
                String R_F, String T_F, String Cl_F, String Con_Ing_F, String SubCon_F, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                String Json_Total = String.Empty;
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_ProgramaF_Ing_ID = PP_F;
                    Negocio.P_RubroF_ID = R_F;
                    Negocio.P_TipoF_ID = T_F;
                    Negocio.P_ClaseF_Ing_ID = Cl_F;
                    Negocio.P_ConceptoF_Ing_ID = Con_Ing_F;
                    Negocio.P_SubConceptoF_ID = SubCon_F;

                    Dt_Datos = Negocio.Consultar_Nivel_Rubros();
                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Ing(Dt_Datos);
                    }

                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["ID"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["CLAVE_NOMBRE"].ToString().Trim();
                                Nodo_Arbol.state = Estado;
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["ESTIMADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["RECAUDADO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["DEV_REC"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["POR_RECAUDAR"]);
                                Nodo_Arbol.descripcion10 = String.Format("{0:n}", Dr["FF"]);
                                Nodo_Arbol.descripcion11 = String.Format("{0:n}", Dr["POR_RECAUDAR_ENE"]);
                                Nodo_Arbol.descripcion12 = String.Format("{0:n}", Dr["POR_RECAUDAR_FEB"]);
                                Nodo_Arbol.descripcion13 = String.Format("{0:n}", Dr["POR_RECAUDAR_MAR"]);
                                Nodo_Arbol.descripcion14 = String.Format("{0:n}", Dr["POR_RECAUDAR_ABR"]);
                                Nodo_Arbol.descripcion15 = String.Format("{0:n}", Dr["POR_RECAUDAR_MAY"]);
                                Nodo_Arbol.descripcion16 = String.Format("{0:n}", Dr["POR_RECAUDAR_JUN"]);
                                Nodo_Arbol.descripcion17 = String.Format("{0:n}", Dr["POR_RECAUDAR_JUL"]);
                                Nodo_Arbol.descripcion18 = String.Format("{0:n}", Dr["POR_RECAUDAR_AGO"]);
                                Nodo_Arbol.descripcion19 = String.Format("{0:n}", Dr["POR_RECAUDAR_SEP"]);
                                Nodo_Arbol.descripcion20 = String.Format("{0:n}", Dr["POR_RECAUDAR_OCT"]);
                                Nodo_Arbol.descripcion21 = String.Format("{0:n}", Dr["POR_RECAUDAR_NOV"]);
                                Nodo_Arbol.descripcion22 = String.Format("{0:n}", Dr["POR_RECAUDAR_DIC"]);
                                Nodo_Arbol.descripcion23 = String.Format("{0:n}", Dr["DEVENGADO_ENE"]);
                                Nodo_Arbol.descripcion24 = String.Format("{0:n}", Dr["DEVENGADO_FEB"]);
                                Nodo_Arbol.descripcion25 = String.Format("{0:n}", Dr["DEVENGADO_MAR"]);
                                Nodo_Arbol.descripcion26 = String.Format("{0:n}", Dr["DEVENGADO_ABR"]);
                                Nodo_Arbol.descripcion27 = String.Format("{0:n}", Dr["DEVENGADO_MAY"]);
                                Nodo_Arbol.descripcion28 = String.Format("{0:n}", Dr["DEVENGADO_JUN"]);
                                Nodo_Arbol.descripcion29 = String.Format("{0:n}", Dr["DEVENGADO_JUL"]);
                                Nodo_Arbol.descripcion30 = String.Format("{0:n}", Dr["DEVENGADO_AGO"]);
                                Nodo_Arbol.descripcion31 = String.Format("{0:n}", Dr["DEVENGADO_SEP"]);
                                Nodo_Arbol.descripcion32 = String.Format("{0:n}", Dr["DEVENGADO_OCT"]);
                                Nodo_Arbol.descripcion33 = String.Format("{0:n}", Dr["DEVENGADO_NOV"]);
                                Nodo_Arbol.descripcion34 = String.Format("{0:n}", Dr["DEVENGADO_DIC"]);
                                Nodo_Arbol.descripcion35 = String.Format("{0:n}", Dr["RECAUDADO_ENE"]);
                                Nodo_Arbol.descripcion36 = String.Format("{0:n}", Dr["RECAUDADO_FEB"]);
                                Nodo_Arbol.descripcion37 = String.Format("{0:n}", Dr["RECAUDADO_MAR"]);
                                Nodo_Arbol.descripcion38 = String.Format("{0:n}", Dr["RECAUDADO_ABR"]);
                                Nodo_Arbol.descripcion39 = String.Format("{0:n}", Dr["RECAUDADO_MAY"]);
                                Nodo_Arbol.descripcion40 = String.Format("{0:n}", Dr["RECAUDADO_JUN"]);
                                Nodo_Arbol.descripcion41 = String.Format("{0:n}", Dr["RECAUDADO_JUL"]);
                                Nodo_Arbol.descripcion42 = String.Format("{0:n}", Dr["RECAUDADO_AGO"]);
                                Nodo_Arbol.descripcion43 = String.Format("{0:n}", Dr["RECAUDADO_SEP"]);
                                Nodo_Arbol.descripcion44 = String.Format("{0:n}", Dr["RECAUDADO_OCT"]);
                                Nodo_Arbol.descripcion45 = String.Format("{0:n}", Dr["RECAUDADO_NOV"]);
                                Nodo_Arbol.descripcion46 = String.Format("{0:n}", Dr["RECAUDADO_DIC"]);
                                Nodo_Arbol.descripcion47 = String.Format("{0:n}", Dr["IMPORTE_ENE"]);
                                Nodo_Arbol.descripcion48 = String.Format("{0:n}", Dr["IMPORTE_FEB"]);
                                Nodo_Arbol.descripcion49 = String.Format("{0:n}", Dr["IMPORTE_MAR"]);
                                Nodo_Arbol.descripcion50 = String.Format("{0:n}", Dr["IMPORTE_ABR"]);
                                Nodo_Arbol.descripcion51 = String.Format("{0:n}", Dr["IMPORTE_MAY"]);
                                Nodo_Arbol.descripcion52 = String.Format("{0:n}", Dr["IMPORTE_JUN"]);
                                Nodo_Arbol.descripcion53 = String.Format("{0:n}", Dr["IMPORTE_JUL"]);
                                Nodo_Arbol.descripcion54 = String.Format("{0:n}", Dr["IMPORTE_AGO"]);
                                Nodo_Arbol.descripcion55 = String.Format("{0:n}", Dr["IMPORTE_SEP"]);
                                Nodo_Arbol.descripcion56 = String.Format("{0:n}", Dr["IMPORTE_OCT"]);
                                Nodo_Arbol.descripcion57 = String.Format("{0:n}", Dr["IMPORTE_NOV"]);
                                Nodo_Arbol.descripcion58 = String.Format("{0:n}", Dr["IMPORTE_DIC"]);
                                Nodo_Arbol.descripcion59 = String.Format("{0:n}", Dr["IMPORTE_TOTAL"]);
                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_Rubro";
                                Atributos.valor2 = Dr["RUBRO_ID"].ToString().Trim();
                                Atributos.valor3 = "";
                                Atributos.valor4 = "";
                                Atributos.valor5 = "";
                                Atributos.valor6 = "";
                                Atributos.valor7 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor8 = Dr["PP_ID"].ToString().Trim();

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Datos = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt_Datos.Rows.Count + ", \"rows\":";
                                Json_Tot += Json_Datos;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json_Datos = Json_Tot;
                            }
                        }
                    }

                    return Json_Datos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Nivel_Rubros Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_Tipos
            ///DESCRIPCIÓN          : Metodo para obtener los tipos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 27/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_Tipos(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
                String R, String T, String Cl, String Con_Ing, String SubCon, String PP, String PP_F,
                String R_F, String T_F, String Cl_F, String Con_Ing_F, String SubCon_F, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                String Json_Total = String.Empty;
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_ProgramaF_Ing_ID = PP_F;
                    Negocio.P_RubroF_ID = R_F;
                    Negocio.P_TipoF_ID = T_F;
                    Negocio.P_ClaseF_Ing_ID = Cl_F;
                    Negocio.P_ConceptoF_Ing_ID = Con_Ing_F;
                    Negocio.P_SubConceptoF_ID = SubCon_F;
                    Dt_Datos = Negocio.Consultar_Nivel_Tipos();
                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Ing(Dt_Datos);
                    }

                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["ID"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["CLAVE_NOMBRE"].ToString().Trim();
                                Nodo_Arbol.state = Estado;
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["ESTIMADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["RECAUDADO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["DEV_REC"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["POR_RECAUDAR"]);
                                Nodo_Arbol.descripcion10 = String.Format("{0:n}", Dr["FF"]);
                                Nodo_Arbol.descripcion11 = String.Format("{0:n}", Dr["POR_RECAUDAR_ENE"]);
                                Nodo_Arbol.descripcion12 = String.Format("{0:n}", Dr["POR_RECAUDAR_FEB"]);
                                Nodo_Arbol.descripcion13 = String.Format("{0:n}", Dr["POR_RECAUDAR_MAR"]);
                                Nodo_Arbol.descripcion14 = String.Format("{0:n}", Dr["POR_RECAUDAR_ABR"]);
                                Nodo_Arbol.descripcion15 = String.Format("{0:n}", Dr["POR_RECAUDAR_MAY"]);
                                Nodo_Arbol.descripcion16 = String.Format("{0:n}", Dr["POR_RECAUDAR_JUN"]);
                                Nodo_Arbol.descripcion17 = String.Format("{0:n}", Dr["POR_RECAUDAR_JUL"]);
                                Nodo_Arbol.descripcion18 = String.Format("{0:n}", Dr["POR_RECAUDAR_AGO"]);
                                Nodo_Arbol.descripcion19 = String.Format("{0:n}", Dr["POR_RECAUDAR_SEP"]);
                                Nodo_Arbol.descripcion20 = String.Format("{0:n}", Dr["POR_RECAUDAR_OCT"]);
                                Nodo_Arbol.descripcion21 = String.Format("{0:n}", Dr["POR_RECAUDAR_NOV"]);
                                Nodo_Arbol.descripcion22 = String.Format("{0:n}", Dr["POR_RECAUDAR_DIC"]);
                                Nodo_Arbol.descripcion23 = String.Format("{0:n}", Dr["DEVENGADO_ENE"]);
                                Nodo_Arbol.descripcion24 = String.Format("{0:n}", Dr["DEVENGADO_FEB"]);
                                Nodo_Arbol.descripcion25 = String.Format("{0:n}", Dr["DEVENGADO_MAR"]);
                                Nodo_Arbol.descripcion26 = String.Format("{0:n}", Dr["DEVENGADO_ABR"]);
                                Nodo_Arbol.descripcion27 = String.Format("{0:n}", Dr["DEVENGADO_MAY"]);
                                Nodo_Arbol.descripcion28 = String.Format("{0:n}", Dr["DEVENGADO_JUN"]);
                                Nodo_Arbol.descripcion29 = String.Format("{0:n}", Dr["DEVENGADO_JUL"]);
                                Nodo_Arbol.descripcion30 = String.Format("{0:n}", Dr["DEVENGADO_AGO"]);
                                Nodo_Arbol.descripcion31 = String.Format("{0:n}", Dr["DEVENGADO_SEP"]);
                                Nodo_Arbol.descripcion32 = String.Format("{0:n}", Dr["DEVENGADO_OCT"]);
                                Nodo_Arbol.descripcion33 = String.Format("{0:n}", Dr["DEVENGADO_NOV"]);
                                Nodo_Arbol.descripcion34 = String.Format("{0:n}", Dr["DEVENGADO_DIC"]);
                                Nodo_Arbol.descripcion35 = String.Format("{0:n}", Dr["RECAUDADO_ENE"]);
                                Nodo_Arbol.descripcion36 = String.Format("{0:n}", Dr["RECAUDADO_FEB"]);
                                Nodo_Arbol.descripcion37 = String.Format("{0:n}", Dr["RECAUDADO_MAR"]);
                                Nodo_Arbol.descripcion38 = String.Format("{0:n}", Dr["RECAUDADO_ABR"]);
                                Nodo_Arbol.descripcion39 = String.Format("{0:n}", Dr["RECAUDADO_MAY"]);
                                Nodo_Arbol.descripcion40 = String.Format("{0:n}", Dr["RECAUDADO_JUN"]);
                                Nodo_Arbol.descripcion41 = String.Format("{0:n}", Dr["RECAUDADO_JUL"]);
                                Nodo_Arbol.descripcion42 = String.Format("{0:n}", Dr["RECAUDADO_AGO"]);
                                Nodo_Arbol.descripcion43 = String.Format("{0:n}", Dr["RECAUDADO_SEP"]);
                                Nodo_Arbol.descripcion44 = String.Format("{0:n}", Dr["RECAUDADO_OCT"]);
                                Nodo_Arbol.descripcion45 = String.Format("{0:n}", Dr["RECAUDADO_NOV"]);
                                Nodo_Arbol.descripcion46 = String.Format("{0:n}", Dr["RECAUDADO_DIC"]);
                                Nodo_Arbol.descripcion47 = String.Format("{0:n}", Dr["IMPORTE_ENE"]);
                                Nodo_Arbol.descripcion48 = String.Format("{0:n}", Dr["IMPORTE_FEB"]);
                                Nodo_Arbol.descripcion49 = String.Format("{0:n}", Dr["IMPORTE_MAR"]);
                                Nodo_Arbol.descripcion50 = String.Format("{0:n}", Dr["IMPORTE_ABR"]);
                                Nodo_Arbol.descripcion51 = String.Format("{0:n}", Dr["IMPORTE_MAY"]);
                                Nodo_Arbol.descripcion52 = String.Format("{0:n}", Dr["IMPORTE_JUN"]);
                                Nodo_Arbol.descripcion53 = String.Format("{0:n}", Dr["IMPORTE_JUL"]);
                                Nodo_Arbol.descripcion54 = String.Format("{0:n}", Dr["IMPORTE_AGO"]);
                                Nodo_Arbol.descripcion55 = String.Format("{0:n}", Dr["IMPORTE_SEP"]);
                                Nodo_Arbol.descripcion56 = String.Format("{0:n}", Dr["IMPORTE_OCT"]);
                                Nodo_Arbol.descripcion57 = String.Format("{0:n}", Dr["IMPORTE_NOV"]);
                                Nodo_Arbol.descripcion58 = String.Format("{0:n}", Dr["IMPORTE_DIC"]);
                                Nodo_Arbol.descripcion59 = String.Format("{0:n}", Dr["IMPORTE_TOTAL"]);

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_Tipo";
                                Atributos.valor2 = Dr["RUBRO_ID"].ToString().Trim();
                                Atributos.valor3 = Dr["TIPO_ID"].ToString().Trim();
                                Atributos.valor4 = "";
                                Atributos.valor5 = "";
                                Atributos.valor6 = "";
                                Atributos.valor7 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor8 = Dr["PP_ID"].ToString().Trim();

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Datos = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt_Datos.Rows.Count + ", \"rows\":";
                                Json_Tot += Json_Datos;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json_Datos = Json_Tot;
                            }
                        }
                    }

                    return Json_Datos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Nivel_Tipo Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_Clases
            ///DESCRIPCIÓN          : Metodo para obtener las clases
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 27/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_Clases(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
                String R, String T, String Cl, String Con_Ing, String SubCon, String PP, String PP_F,
                String R_F, String T_F, String Cl_F, String Con_Ing_F, String SubCon_F, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                String Json_Total = String.Empty;
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_ProgramaF_Ing_ID = PP_F;
                    Negocio.P_RubroF_ID = R_F;
                    Negocio.P_TipoF_ID = T_F;
                    Negocio.P_ClaseF_Ing_ID = Cl_F;
                    Negocio.P_ConceptoF_Ing_ID = Con_Ing_F;
                    Negocio.P_SubConceptoF_ID = SubCon_F;
                    Dt_Datos = Negocio.Consultar_Nivel_Clases();

                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Ing(Dt_Datos);
                    }

                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["ID"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["CLAVE_NOMBRE"].ToString().Trim();
                                Nodo_Arbol.state = Estado;
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["ESTIMADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["RECAUDADO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["DEV_REC"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["POR_RECAUDAR"]);
                                Nodo_Arbol.descripcion10 = String.Format("{0:n}", Dr["FF"]);
                                Nodo_Arbol.descripcion11 = String.Format("{0:n}", Dr["POR_RECAUDAR_ENE"]);
                                Nodo_Arbol.descripcion12 = String.Format("{0:n}", Dr["POR_RECAUDAR_FEB"]);
                                Nodo_Arbol.descripcion13 = String.Format("{0:n}", Dr["POR_RECAUDAR_MAR"]);
                                Nodo_Arbol.descripcion14 = String.Format("{0:n}", Dr["POR_RECAUDAR_ABR"]);
                                Nodo_Arbol.descripcion15 = String.Format("{0:n}", Dr["POR_RECAUDAR_MAY"]);
                                Nodo_Arbol.descripcion16 = String.Format("{0:n}", Dr["POR_RECAUDAR_JUN"]);
                                Nodo_Arbol.descripcion17 = String.Format("{0:n}", Dr["POR_RECAUDAR_JUL"]);
                                Nodo_Arbol.descripcion18 = String.Format("{0:n}", Dr["POR_RECAUDAR_AGO"]);
                                Nodo_Arbol.descripcion19 = String.Format("{0:n}", Dr["POR_RECAUDAR_SEP"]);
                                Nodo_Arbol.descripcion20 = String.Format("{0:n}", Dr["POR_RECAUDAR_OCT"]);
                                Nodo_Arbol.descripcion21 = String.Format("{0:n}", Dr["POR_RECAUDAR_NOV"]);
                                Nodo_Arbol.descripcion22 = String.Format("{0:n}", Dr["POR_RECAUDAR_DIC"]);
                                Nodo_Arbol.descripcion23 = String.Format("{0:n}", Dr["DEVENGADO_ENE"]);
                                Nodo_Arbol.descripcion24 = String.Format("{0:n}", Dr["DEVENGADO_FEB"]);
                                Nodo_Arbol.descripcion25 = String.Format("{0:n}", Dr["DEVENGADO_MAR"]);
                                Nodo_Arbol.descripcion26 = String.Format("{0:n}", Dr["DEVENGADO_ABR"]);
                                Nodo_Arbol.descripcion27 = String.Format("{0:n}", Dr["DEVENGADO_MAY"]);
                                Nodo_Arbol.descripcion28 = String.Format("{0:n}", Dr["DEVENGADO_JUN"]);
                                Nodo_Arbol.descripcion29 = String.Format("{0:n}", Dr["DEVENGADO_JUL"]);
                                Nodo_Arbol.descripcion30 = String.Format("{0:n}", Dr["DEVENGADO_AGO"]);
                                Nodo_Arbol.descripcion31 = String.Format("{0:n}", Dr["DEVENGADO_SEP"]);
                                Nodo_Arbol.descripcion32 = String.Format("{0:n}", Dr["DEVENGADO_OCT"]);
                                Nodo_Arbol.descripcion33 = String.Format("{0:n}", Dr["DEVENGADO_NOV"]);
                                Nodo_Arbol.descripcion34 = String.Format("{0:n}", Dr["DEVENGADO_DIC"]);
                                Nodo_Arbol.descripcion35 = String.Format("{0:n}", Dr["RECAUDADO_ENE"]);
                                Nodo_Arbol.descripcion36 = String.Format("{0:n}", Dr["RECAUDADO_FEB"]);
                                Nodo_Arbol.descripcion37 = String.Format("{0:n}", Dr["RECAUDADO_MAR"]);
                                Nodo_Arbol.descripcion38 = String.Format("{0:n}", Dr["RECAUDADO_ABR"]);
                                Nodo_Arbol.descripcion39 = String.Format("{0:n}", Dr["RECAUDADO_MAY"]);
                                Nodo_Arbol.descripcion40 = String.Format("{0:n}", Dr["RECAUDADO_JUN"]);
                                Nodo_Arbol.descripcion41 = String.Format("{0:n}", Dr["RECAUDADO_JUL"]);
                                Nodo_Arbol.descripcion42 = String.Format("{0:n}", Dr["RECAUDADO_AGO"]);
                                Nodo_Arbol.descripcion43 = String.Format("{0:n}", Dr["RECAUDADO_SEP"]);
                                Nodo_Arbol.descripcion44 = String.Format("{0:n}", Dr["RECAUDADO_OCT"]);
                                Nodo_Arbol.descripcion45 = String.Format("{0:n}", Dr["RECAUDADO_NOV"]);
                                Nodo_Arbol.descripcion46 = String.Format("{0:n}", Dr["RECAUDADO_DIC"]);
                                Nodo_Arbol.descripcion47 = String.Format("{0:n}", Dr["IMPORTE_ENE"]);
                                Nodo_Arbol.descripcion48 = String.Format("{0:n}", Dr["IMPORTE_FEB"]);
                                Nodo_Arbol.descripcion49 = String.Format("{0:n}", Dr["IMPORTE_MAR"]);
                                Nodo_Arbol.descripcion50 = String.Format("{0:n}", Dr["IMPORTE_ABR"]);
                                Nodo_Arbol.descripcion51 = String.Format("{0:n}", Dr["IMPORTE_MAY"]);
                                Nodo_Arbol.descripcion52 = String.Format("{0:n}", Dr["IMPORTE_JUN"]);
                                Nodo_Arbol.descripcion53 = String.Format("{0:n}", Dr["IMPORTE_JUL"]);
                                Nodo_Arbol.descripcion54 = String.Format("{0:n}", Dr["IMPORTE_AGO"]);
                                Nodo_Arbol.descripcion55 = String.Format("{0:n}", Dr["IMPORTE_SEP"]);
                                Nodo_Arbol.descripcion56 = String.Format("{0:n}", Dr["IMPORTE_OCT"]);
                                Nodo_Arbol.descripcion57 = String.Format("{0:n}", Dr["IMPORTE_NOV"]);
                                Nodo_Arbol.descripcion58 = String.Format("{0:n}", Dr["IMPORTE_DIC"]);
                                Nodo_Arbol.descripcion59 = String.Format("{0:n}", Dr["IMPORTE_TOTAL"]);

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_Clase";
                                Atributos.valor2 = Dr["RUBRO_ID"].ToString().Trim();
                                Atributos.valor3 = Dr["TIPO_ID"].ToString().Trim();
                                Atributos.valor4 = Dr["CLASE_ING_ID"].ToString().Trim();
                                Atributos.valor5 = "";
                                Atributos.valor6 = "";
                                Atributos.valor7 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor8 = Dr["PP_ID"].ToString().Trim();

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Datos = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt_Datos.Rows.Count + ", \"rows\":";
                                Json_Tot += Json_Datos;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json_Datos = Json_Tot;
                            }
                        }
                    }

                    return Json_Datos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Nivel_Clases Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_Conceptos_Ing
            ///DESCRIPCIÓN          : Metodo para obtener los conceptos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 27/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_Conceptos_Ing(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
                String R, String T, String Cl, String Con_Ing, String SubCon, String PP, String PP_F,
                String R_F, String T_F, String Cl_F, String Con_Ing_F, String SubCon_F, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                String Json_Total = String.Empty;
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                DataTable Dt_Det = new DataTable();
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_ProgramaF_Ing_ID = PP_F;
                    Negocio.P_RubroF_ID = R_F;
                    Negocio.P_TipoF_ID = T_F;
                    Negocio.P_ClaseF_Ing_ID = Cl_F;
                    Negocio.P_ConceptoF_Ing_ID = Con_Ing_F;
                    Negocio.P_SubConceptoF_ID = SubCon_F;

                    Dt_Datos = Negocio.Consultar_Nivel_Conceptos_Ing();
                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Ing(Dt_Datos);
                    }

                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {
                                //obtenemos si el concepto tiene subconceptos
                                Negocio.P_Busqueda = String.Empty;
                                Negocio.P_Anio = Anio;
                                Negocio.P_Tipo_Presupuesto = "ING";
                                Negocio.P_Periodo_Inicial = String.Empty;
                                Negocio.P_Fecha_Inicial = String.Empty;
                                Negocio.P_Fecha_Final = String.Empty;
                                Negocio.P_Fte_Financiamiento_Ing = FF;
                                Negocio.P_Programa_Ing_ID = PP;
                                Negocio.P_Rubro_ID = R;
                                Negocio.P_Tipo_ID = T;
                                Negocio.P_Clase_Ing_ID = Cl;
                                Negocio.P_Concepto_Ing_ID = Dr["CONCEPTO_ING_ID"].ToString().Trim();
                                Negocio.P_SubConcepto_ID = String.Empty;

                                Dt_Det = Negocio.Consultar_SubConceptos();

                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["ID"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["CLAVE_NOMBRE"].ToString().Trim();
                                if (Dt_Det != null && Dt_Det.Rows.Count > 0)
                                {
                                    Nodo_Arbol.state = Estado;
                                }
                                else
                                {
                                    Nodo_Arbol.state = "opened";
                                }
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["ESTIMADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["RECAUDADO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["DEV_REC"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["POR_RECAUDAR"]);
                                Nodo_Arbol.descripcion10 = String.Format("{0:n}", Dr["FF"]);
                                Nodo_Arbol.descripcion11 = String.Format("{0:n}", Dr["POR_RECAUDAR_ENE"]);
                                Nodo_Arbol.descripcion12 = String.Format("{0:n}", Dr["POR_RECAUDAR_FEB"]);
                                Nodo_Arbol.descripcion13 = String.Format("{0:n}", Dr["POR_RECAUDAR_MAR"]);
                                Nodo_Arbol.descripcion14 = String.Format("{0:n}", Dr["POR_RECAUDAR_ABR"]);
                                Nodo_Arbol.descripcion15 = String.Format("{0:n}", Dr["POR_RECAUDAR_MAY"]);
                                Nodo_Arbol.descripcion16 = String.Format("{0:n}", Dr["POR_RECAUDAR_JUN"]);
                                Nodo_Arbol.descripcion17 = String.Format("{0:n}", Dr["POR_RECAUDAR_JUL"]);
                                Nodo_Arbol.descripcion18 = String.Format("{0:n}", Dr["POR_RECAUDAR_AGO"]);
                                Nodo_Arbol.descripcion19 = String.Format("{0:n}", Dr["POR_RECAUDAR_SEP"]);
                                Nodo_Arbol.descripcion20 = String.Format("{0:n}", Dr["POR_RECAUDAR_OCT"]);
                                Nodo_Arbol.descripcion21 = String.Format("{0:n}", Dr["POR_RECAUDAR_NOV"]);
                                Nodo_Arbol.descripcion22 = String.Format("{0:n}", Dr["POR_RECAUDAR_DIC"]);
                                Nodo_Arbol.descripcion23 = String.Format("{0:n}", Dr["DEVENGADO_ENE"]);
                                Nodo_Arbol.descripcion24 = String.Format("{0:n}", Dr["DEVENGADO_FEB"]);
                                Nodo_Arbol.descripcion25 = String.Format("{0:n}", Dr["DEVENGADO_MAR"]);
                                Nodo_Arbol.descripcion26 = String.Format("{0:n}", Dr["DEVENGADO_ABR"]);
                                Nodo_Arbol.descripcion27 = String.Format("{0:n}", Dr["DEVENGADO_MAY"]);
                                Nodo_Arbol.descripcion28 = String.Format("{0:n}", Dr["DEVENGADO_JUN"]);
                                Nodo_Arbol.descripcion29 = String.Format("{0:n}", Dr["DEVENGADO_JUL"]);
                                Nodo_Arbol.descripcion30 = String.Format("{0:n}", Dr["DEVENGADO_AGO"]);
                                Nodo_Arbol.descripcion31 = String.Format("{0:n}", Dr["DEVENGADO_SEP"]);
                                Nodo_Arbol.descripcion32 = String.Format("{0:n}", Dr["DEVENGADO_OCT"]);
                                Nodo_Arbol.descripcion33 = String.Format("{0:n}", Dr["DEVENGADO_NOV"]);
                                Nodo_Arbol.descripcion34 = String.Format("{0:n}", Dr["DEVENGADO_DIC"]);
                                Nodo_Arbol.descripcion35 = String.Format("{0:n}", Dr["RECAUDADO_ENE"]);
                                Nodo_Arbol.descripcion36 = String.Format("{0:n}", Dr["RECAUDADO_FEB"]);
                                Nodo_Arbol.descripcion37 = String.Format("{0:n}", Dr["RECAUDADO_MAR"]);
                                Nodo_Arbol.descripcion38 = String.Format("{0:n}", Dr["RECAUDADO_ABR"]);
                                Nodo_Arbol.descripcion39 = String.Format("{0:n}", Dr["RECAUDADO_MAY"]);
                                Nodo_Arbol.descripcion40 = String.Format("{0:n}", Dr["RECAUDADO_JUN"]);
                                Nodo_Arbol.descripcion41 = String.Format("{0:n}", Dr["RECAUDADO_JUL"]);
                                Nodo_Arbol.descripcion42 = String.Format("{0:n}", Dr["RECAUDADO_AGO"]);
                                Nodo_Arbol.descripcion43 = String.Format("{0:n}", Dr["RECAUDADO_SEP"]);
                                Nodo_Arbol.descripcion44 = String.Format("{0:n}", Dr["RECAUDADO_OCT"]);
                                Nodo_Arbol.descripcion45 = String.Format("{0:n}", Dr["RECAUDADO_NOV"]);
                                Nodo_Arbol.descripcion46 = String.Format("{0:n}", Dr["RECAUDADO_DIC"]);
                                Nodo_Arbol.descripcion47 = String.Format("{0:n}", Dr["IMPORTE_ENE"]);
                                Nodo_Arbol.descripcion48 = String.Format("{0:n}", Dr["IMPORTE_FEB"]);
                                Nodo_Arbol.descripcion49 = String.Format("{0:n}", Dr["IMPORTE_MAR"]);
                                Nodo_Arbol.descripcion50 = String.Format("{0:n}", Dr["IMPORTE_ABR"]);
                                Nodo_Arbol.descripcion51 = String.Format("{0:n}", Dr["IMPORTE_MAY"]);
                                Nodo_Arbol.descripcion52 = String.Format("{0:n}", Dr["IMPORTE_JUN"]);
                                Nodo_Arbol.descripcion53 = String.Format("{0:n}", Dr["IMPORTE_JUL"]);
                                Nodo_Arbol.descripcion54 = String.Format("{0:n}", Dr["IMPORTE_AGO"]);
                                Nodo_Arbol.descripcion55 = String.Format("{0:n}", Dr["IMPORTE_SEP"]);
                                Nodo_Arbol.descripcion56 = String.Format("{0:n}", Dr["IMPORTE_OCT"]);
                                Nodo_Arbol.descripcion57 = String.Format("{0:n}", Dr["IMPORTE_NOV"]);
                                Nodo_Arbol.descripcion58 = String.Format("{0:n}", Dr["IMPORTE_DIC"]);
                                Nodo_Arbol.descripcion59 = String.Format("{0:n}", Dr["IMPORTE_TOTAL"]);

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_Concepto_Ing";
                                Atributos.valor2 = Dr["RUBRO_ID"].ToString().Trim();
                                Atributos.valor3 = Dr["TIPO_ID"].ToString().Trim();
                                Atributos.valor4 = Dr["CLASE_ING_ID"].ToString().Trim();
                                Atributos.valor5 = Dr["CONCEPTO_ING_ID"].ToString().Trim();
                                Atributos.valor6 = "";
                                Atributos.valor7 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor8 = Dr["PP_ID"].ToString().Trim();

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Datos = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt_Datos.Rows.Count + ", \"rows\":";
                                Json_Tot += Json_Datos;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json_Datos = Json_Tot;
                            }
                        }
                    }

                    return Json_Datos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Nivel_Conceptos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_Conceptos
            ///DESCRIPCIÓN          : Metodo para obtener los conceptos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 27/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_SubConceptos(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
                String R, String T, String Cl, String Con_Ing, String SubCon, String PP, String PP_F,
                String R_F, String T_F, String Cl_F, String Con_Ing_F, String SubCon_F, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                String Json_Total = String.Empty;
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_ProgramaF_Ing_ID = PP_F;
                    Negocio.P_RubroF_ID = R_F;
                    Negocio.P_TipoF_ID = T_F;
                    Negocio.P_ClaseF_Ing_ID = Cl_F;
                    Negocio.P_ConceptoF_Ing_ID = Con_Ing_F;
                    Negocio.P_SubConceptoF_ID = SubCon_F;
                    Dt_Datos = Negocio.Consultar_Nivel_SubConceptos();

                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Ing(Dt_Datos);
                    }

                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["ID"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["CLAVE_NOMBRE"].ToString().Trim();
                                Nodo_Arbol.state = "opened";
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["ESTIMADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["RECAUDADO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["DEV_REC"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["POR_RECAUDAR"]);
                                Nodo_Arbol.descripcion10 = Dr["FF"].ToString().Trim();
                                Nodo_Arbol.descripcion11 = String.Format("{0:n}", Dr["POR_RECAUDAR_ENE"]);
                                Nodo_Arbol.descripcion12 = String.Format("{0:n}", Dr["POR_RECAUDAR_FEB"]);
                                Nodo_Arbol.descripcion13 = String.Format("{0:n}", Dr["POR_RECAUDAR_MAR"]);
                                Nodo_Arbol.descripcion14 = String.Format("{0:n}", Dr["POR_RECAUDAR_ABR"]);
                                Nodo_Arbol.descripcion15 = String.Format("{0:n}", Dr["POR_RECAUDAR_MAY"]);
                                Nodo_Arbol.descripcion16 = String.Format("{0:n}", Dr["POR_RECAUDAR_JUN"]);
                                Nodo_Arbol.descripcion17 = String.Format("{0:n}", Dr["POR_RECAUDAR_JUL"]);
                                Nodo_Arbol.descripcion18 = String.Format("{0:n}", Dr["POR_RECAUDAR_AGO"]);
                                Nodo_Arbol.descripcion19 = String.Format("{0:n}", Dr["POR_RECAUDAR_SEP"]);
                                Nodo_Arbol.descripcion20 = String.Format("{0:n}", Dr["POR_RECAUDAR_OCT"]);
                                Nodo_Arbol.descripcion21 = String.Format("{0:n}", Dr["POR_RECAUDAR_NOV"]);
                                Nodo_Arbol.descripcion22 = String.Format("{0:n}", Dr["POR_RECAUDAR_DIC"]);
                                Nodo_Arbol.descripcion23 = String.Format("{0:n}", Dr["DEVENGADO_ENE"]);
                                Nodo_Arbol.descripcion24 = String.Format("{0:n}", Dr["DEVENGADO_FEB"]);
                                Nodo_Arbol.descripcion25 = String.Format("{0:n}", Dr["DEVENGADO_MAR"]);
                                Nodo_Arbol.descripcion26 = String.Format("{0:n}", Dr["DEVENGADO_ABR"]);
                                Nodo_Arbol.descripcion27 = String.Format("{0:n}", Dr["DEVENGADO_MAY"]);
                                Nodo_Arbol.descripcion28 = String.Format("{0:n}", Dr["DEVENGADO_JUN"]);
                                Nodo_Arbol.descripcion29 = String.Format("{0:n}", Dr["DEVENGADO_JUL"]);
                                Nodo_Arbol.descripcion30 = String.Format("{0:n}", Dr["DEVENGADO_AGO"]);
                                Nodo_Arbol.descripcion31 = String.Format("{0:n}", Dr["DEVENGADO_SEP"]);
                                Nodo_Arbol.descripcion32 = String.Format("{0:n}", Dr["DEVENGADO_OCT"]);
                                Nodo_Arbol.descripcion33 = String.Format("{0:n}", Dr["DEVENGADO_NOV"]);
                                Nodo_Arbol.descripcion34 = String.Format("{0:n}", Dr["DEVENGADO_DIC"]);
                                Nodo_Arbol.descripcion35 = String.Format("{0:n}", Dr["RECAUDADO_ENE"]);
                                Nodo_Arbol.descripcion36 = String.Format("{0:n}", Dr["RECAUDADO_FEB"]);
                                Nodo_Arbol.descripcion37 = String.Format("{0:n}", Dr["RECAUDADO_MAR"]);
                                Nodo_Arbol.descripcion38 = String.Format("{0:n}", Dr["RECAUDADO_ABR"]);
                                Nodo_Arbol.descripcion39 = String.Format("{0:n}", Dr["RECAUDADO_MAY"]);
                                Nodo_Arbol.descripcion40 = String.Format("{0:n}", Dr["RECAUDADO_JUN"]);
                                Nodo_Arbol.descripcion41 = String.Format("{0:n}", Dr["RECAUDADO_JUL"]);
                                Nodo_Arbol.descripcion42 = String.Format("{0:n}", Dr["RECAUDADO_AGO"]);
                                Nodo_Arbol.descripcion43 = String.Format("{0:n}", Dr["RECAUDADO_SEP"]);
                                Nodo_Arbol.descripcion44 = String.Format("{0:n}", Dr["RECAUDADO_OCT"]);
                                Nodo_Arbol.descripcion45 = String.Format("{0:n}", Dr["RECAUDADO_NOV"]);
                                Nodo_Arbol.descripcion46 = String.Format("{0:n}", Dr["RECAUDADO_DIC"]);
                                Nodo_Arbol.descripcion47 = String.Format("{0:n}", Dr["IMPORTE_ENE"]);
                                Nodo_Arbol.descripcion48 = String.Format("{0:n}", Dr["IMPORTE_FEB"]);
                                Nodo_Arbol.descripcion49 = String.Format("{0:n}", Dr["IMPORTE_MAR"]);
                                Nodo_Arbol.descripcion50 = String.Format("{0:n}", Dr["IMPORTE_ABR"]);
                                Nodo_Arbol.descripcion51 = String.Format("{0:n}", Dr["IMPORTE_MAY"]);
                                Nodo_Arbol.descripcion52 = String.Format("{0:n}", Dr["IMPORTE_JUN"]);
                                Nodo_Arbol.descripcion53 = String.Format("{0:n}", Dr["IMPORTE_JUL"]);
                                Nodo_Arbol.descripcion54 = String.Format("{0:n}", Dr["IMPORTE_AGO"]);
                                Nodo_Arbol.descripcion55 = String.Format("{0:n}", Dr["IMPORTE_SEP"]);
                                Nodo_Arbol.descripcion56 = String.Format("{0:n}", Dr["IMPORTE_OCT"]);
                                Nodo_Arbol.descripcion57 = String.Format("{0:n}", Dr["IMPORTE_NOV"]);
                                Nodo_Arbol.descripcion58 = String.Format("{0:n}", Dr["IMPORTE_DIC"]);
                                Nodo_Arbol.descripcion59 = String.Format("{0:n}", Dr["IMPORTE_TOTAL"]);

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_SubConcepto";
                                Atributos.valor2 = Dr["RUBRO_ID"].ToString().Trim();
                                Atributos.valor3 = Dr["TIPO_ID"].ToString().Trim();
                                Atributos.valor4 = Dr["CLASE_ING_ID"].ToString().Trim();
                                Atributos.valor5 = Dr["CONCEPTO_ING_ID"].ToString().Trim();
                                Atributos.valor6 = Dr["SUBCONCEPTO_ING_ID"].ToString().Trim();
                                Atributos.valor7 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor8 = Dr["PP_ID"].ToString().Trim();

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Datos = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt_Datos.Rows.Count + ", \"rows\":";
                                Json_Tot += Json_Datos;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json_Datos = Json_Tot;
                            }
                        }
                    }

                    return Json_Datos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Nivel_Conceptos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Total_Egr
            ///DESCRIPCIÓN          : Metodo para obtener el total
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Total_Ing(DataTable Dt_Datos)
            {
                Double Aprobado = 0.00;
                Double Ampliacion = 0.00;
                Double Reduccion = 0.00;
                Double Modificado = 0.00;
                Double Devengado = 0.00;
                Double Pagado = 0.00;
                Double Dev_Pag = 0.00;
                Double Disponible = 0.00;
                Double Comprometido = 0.00;
                String Json_Total = string.Empty;
                Double Por_Recaudar_Ene = 0.00;
                Double Por_Recaudar_Feb = 0.00;
                Double Por_Recaudar_Mar = 0.00;
                Double Por_Recaudar_Abr = 0.00;
                Double Por_Recaudar_May = 0.00;
                Double Por_Recaudar_Jun = 0.00;
                Double Por_Recaudar_Jul = 0.00;
                Double Por_Recaudar_Ago = 0.00;
                Double Por_Recaudar_Sep = 0.00;
                Double Por_Recaudar_Oct = 0.00;
                Double Por_Recaudar_Nov = 0.00;
                Double Por_Recaudar_Dic = 0.00;
                Double Devengado_Ene = 0.00;
                Double Devengado_Feb = 0.00;
                Double Devengado_Mar = 0.00;
                Double Devengado_Abr = 0.00;
                Double Devengado_May = 0.00;
                Double Devengado_Jun = 0.00;
                Double Devengado_Jul = 0.00;
                Double Devengado_Ago = 0.00;
                Double Devengado_Sep = 0.00;
                Double Devengado_Oct = 0.00;
                Double Devengado_Nov = 0.00;
                Double Devengado_Dic = 0.00;
                Double Recaudado_Ene = 0.00;
                Double Recaudado_Feb = 0.00;
                Double Recaudado_Mar = 0.00;
                Double Recaudado_Abr = 0.00;
                Double Recaudado_May = 0.00;
                Double Recaudado_Jun = 0.00;
                Double Recaudado_Jul = 0.00;
                Double Recaudado_Ago = 0.00;
                Double Recaudado_Sep = 0.00;
                Double Recaudado_Oct = 0.00;
                Double Recaudado_Nov = 0.00;
                Double Recaudado_Dic = 0.00;
                Double Importe_Ene = 0.00;
                Double Importe_Feb = 0.00;
                Double Importe_Mar = 0.00;
                Double Importe_Abr = 0.00;
                Double Importe_May = 0.00;
                Double Importe_Jun = 0.00;
                Double Importe_Jul = 0.00;
                Double Importe_Ago = 0.00;
                Double Importe_Sep = 0.00;
                Double Importe_Oct = 0.00;
                Double Importe_Nov = 0.00;
                Double Importe_Dic = 0.00;
                Double Importe_Total = 0.00;


                Json_Total = "{[]}";
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                //String Json = String.Empty;

                try
                {
                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {
                                Aprobado += Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                Ampliacion += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                Reduccion += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                Modificado += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                Devengado += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                Pagado += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                Dev_Pag += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());
                                Comprometido += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                Disponible += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                Por_Recaudar_Ene += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_ENE"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_ENE"].ToString().Trim());
                                Por_Recaudar_Feb += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_FEB"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_FEB"].ToString().Trim());
                                Por_Recaudar_Mar += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_MAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_MAR"].ToString().Trim());
                                Por_Recaudar_Abr += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_ABR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_ABR"].ToString().Trim());
                                Por_Recaudar_May += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_MAY"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_MAY"].ToString().Trim());
                                Por_Recaudar_Jun += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_JUN"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_JUN"].ToString().Trim());
                                Por_Recaudar_Jul += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_JUL"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_JUL"].ToString().Trim());
                                Por_Recaudar_Ago += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_AGO"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_AGO"].ToString().Trim());
                                Por_Recaudar_Sep += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_SEP"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_SEP"].ToString().Trim());
                                Por_Recaudar_Oct += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_OCT"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_OCT"].ToString().Trim());
                                Por_Recaudar_Nov += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_NOV"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_NOV"].ToString().Trim());
                                Por_Recaudar_Dic += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_DIC"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_DIC"].ToString().Trim());
                                Devengado_Ene += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_ENE"].ToString().Trim()) ? "0" : Dr["DEVENGADO_ENE"].ToString().Trim());
                                Devengado_Feb += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_FEB"].ToString().Trim()) ? "0" : Dr["DEVENGADO_FEB"].ToString().Trim());
                                Devengado_Mar += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_MAR"].ToString().Trim()) ? "0" : Dr["DEVENGADO_MAR"].ToString().Trim());
                                Devengado_Abr += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_ABR"].ToString().Trim()) ? "0" : Dr["DEVENGADO_ABR"].ToString().Trim());
                                Devengado_May += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_MAY"].ToString().Trim()) ? "0" : Dr["DEVENGADO_MAY"].ToString().Trim());
                                Devengado_Jun += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_JUN"].ToString().Trim()) ? "0" : Dr["DEVENGADO_JUN"].ToString().Trim());
                                Devengado_Jul += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_JUL"].ToString().Trim()) ? "0" : Dr["DEVENGADO_JUL"].ToString().Trim());
                                Devengado_Ago += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_AGO"].ToString().Trim()) ? "0" : Dr["DEVENGADO_AGO"].ToString().Trim());
                                Devengado_Sep += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_SEP"].ToString().Trim()) ? "0" : Dr["DEVENGADO_SEP"].ToString().Trim());
                                Devengado_Oct += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_OCT"].ToString().Trim()) ? "0" : Dr["DEVENGADO_OCT"].ToString().Trim());
                                Devengado_Nov += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_NOV"].ToString().Trim()) ? "0" : Dr["DEVENGADO_NOV"].ToString().Trim());
                                Devengado_Dic += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_DIC"].ToString().Trim()) ? "0" : Dr["DEVENGADO_DIC"].ToString().Trim());
                                Recaudado_Ene += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_ENE"].ToString().Trim()) ? "0" : Dr["RECAUDADO_ENE"].ToString().Trim());
                                Recaudado_Feb += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_FEB"].ToString().Trim()) ? "0" : Dr["RECAUDADO_FEB"].ToString().Trim());
                                Recaudado_Mar += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_MAR"].ToString().Trim()) ? "0" : Dr["RECAUDADO_MAR"].ToString().Trim());
                                Recaudado_Abr += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_ABR"].ToString().Trim()) ? "0" : Dr["RECAUDADO_ABR"].ToString().Trim());
                                Recaudado_May += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_MAY"].ToString().Trim()) ? "0" : Dr["RECAUDADO_MAY"].ToString().Trim());
                                Recaudado_Jun += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_JUN"].ToString().Trim()) ? "0" : Dr["RECAUDADO_JUN"].ToString().Trim());
                                Recaudado_Jul += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_JUL"].ToString().Trim()) ? "0" : Dr["RECAUDADO_JUL"].ToString().Trim());
                                Recaudado_Ago += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_AGO"].ToString().Trim()) ? "0" : Dr["RECAUDADO_AGO"].ToString().Trim());
                                Recaudado_Sep += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_SEP"].ToString().Trim()) ? "0" : Dr["RECAUDADO_SEP"].ToString().Trim());
                                Recaudado_Oct += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_OCT"].ToString().Trim()) ? "0" : Dr["RECAUDADO_OCT"].ToString().Trim());
                                Recaudado_Nov += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_NOV"].ToString().Trim()) ? "0" : Dr["RECAUDADO_NOV"].ToString().Trim());
                                Recaudado_Dic += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_DIC"].ToString().Trim()) ? "0" : Dr["RECAUDADO_DIC"].ToString().Trim());
                                Importe_Ene += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_ENE"].ToString().Trim()) ? "0" : Dr["IMPORTE_ENE"].ToString().Trim());
                                Importe_Feb += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_FEB"].ToString().Trim()) ? "0" : Dr["IMPORTE_FEB"].ToString().Trim());
                                Importe_Mar += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_MAR"].ToString().Trim()) ? "0" : Dr["IMPORTE_MAR"].ToString().Trim());
                                Importe_Abr += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_ABR"].ToString().Trim()) ? "0" : Dr["IMPORTE_ABR"].ToString().Trim());
                                Importe_May += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_MAY"].ToString().Trim()) ? "0" : Dr["IMPORTE_MAY"].ToString().Trim());
                                Importe_Jun += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_JUN"].ToString().Trim()) ? "0" : Dr["IMPORTE_JUN"].ToString().Trim());
                                Importe_Jul += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_JUL"].ToString().Trim()) ? "0" : Dr["IMPORTE_JUL"].ToString().Trim());
                                Importe_Ago += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_AGO"].ToString().Trim()) ? "0" : Dr["IMPORTE_AGO"].ToString().Trim());
                                Importe_Sep += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_SEP"].ToString().Trim()) ? "0" : Dr["IMPORTE_SEP"].ToString().Trim());
                                Importe_Oct += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_OCT"].ToString().Trim()) ? "0" : Dr["IMPORTE_OCT"].ToString().Trim());
                                Importe_Nov += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_NOV"].ToString().Trim()) ? "0" : Dr["IMPORTE_NOV"].ToString().Trim());
                                Importe_Dic += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_DIC"].ToString().Trim()) ? "0" : Dr["IMPORTE_DIC"].ToString().Trim());
                                Importe_Total += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_TOTAL"].ToString().Trim()) ? "0" : Dr["IMPORTE_TOTAL"].ToString().Trim());
                            }

                            Nodo_Arbol = new Cls_Nodo_Arbol();

                            Nodo_Arbol.id = "";
                            Nodo_Arbol.texto = "Total";
                            Nodo_Arbol.descripcion1 = String.Format("{0:n}", Aprobado);
                            Nodo_Arbol.descripcion2 = String.Format("{0:n}", Ampliacion);
                            Nodo_Arbol.descripcion3 = String.Format("{0:n}", Reduccion);
                            Nodo_Arbol.descripcion4 = String.Format("{0:n}", Modificado);
                            Nodo_Arbol.descripcion5 = String.Format("{0:n}", Devengado);
                            Nodo_Arbol.descripcion6 = String.Format("{0:n}", Pagado);
                            Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dev_Pag);
                            Nodo_Arbol.descripcion8 = String.Format("{0:n}", Comprometido);
                            Nodo_Arbol.descripcion9 = String.Format("{0:n}", Disponible);
                            Nodo_Arbol.descripcion10 = "";
                            Nodo_Arbol.descripcion11 = String.Format("{0:n}", Por_Recaudar_Ene);
                            Nodo_Arbol.descripcion12 = String.Format("{0:n}", Por_Recaudar_Feb);
                            Nodo_Arbol.descripcion13 = String.Format("{0:n}", Por_Recaudar_Mar);
                            Nodo_Arbol.descripcion14 = String.Format("{0:n}", Por_Recaudar_Abr);
                            Nodo_Arbol.descripcion15 = String.Format("{0:n}", Por_Recaudar_May);
                            Nodo_Arbol.descripcion16 = String.Format("{0:n}", Por_Recaudar_Jun);
                            Nodo_Arbol.descripcion17 = String.Format("{0:n}", Por_Recaudar_Jul);
                            Nodo_Arbol.descripcion18 = String.Format("{0:n}", Por_Recaudar_Ago);
                            Nodo_Arbol.descripcion19 = String.Format("{0:n}", Por_Recaudar_Sep);
                            Nodo_Arbol.descripcion20 = String.Format("{0:n}", Por_Recaudar_Oct);
                            Nodo_Arbol.descripcion21 = String.Format("{0:n}", Por_Recaudar_Nov);
                            Nodo_Arbol.descripcion22 = String.Format("{0:n}", Por_Recaudar_Dic);
                            Nodo_Arbol.descripcion23 = String.Format("{0:n}", Devengado_Ene);
                            Nodo_Arbol.descripcion24 = String.Format("{0:n}", Devengado_Feb);
                            Nodo_Arbol.descripcion25 = String.Format("{0:n}", Devengado_Mar);
                            Nodo_Arbol.descripcion26 = String.Format("{0:n}", Devengado_Abr);
                            Nodo_Arbol.descripcion27 = String.Format("{0:n}", Devengado_May);
                            Nodo_Arbol.descripcion28 = String.Format("{0:n}", Devengado_Jun);
                            Nodo_Arbol.descripcion29 = String.Format("{0:n}", Devengado_Jul);
                            Nodo_Arbol.descripcion30 = String.Format("{0:n}", Devengado_Ago);
                            Nodo_Arbol.descripcion31 = String.Format("{0:n}", Devengado_Sep);
                            Nodo_Arbol.descripcion32 = String.Format("{0:n}", Devengado_Oct);
                            Nodo_Arbol.descripcion33 = String.Format("{0:n}", Devengado_Nov);
                            Nodo_Arbol.descripcion34 = String.Format("{0:n}", Devengado_Dic);
                            Nodo_Arbol.descripcion35 = String.Format("{0:n}", Recaudado_Ene);
                            Nodo_Arbol.descripcion36 = String.Format("{0:n}", Recaudado_Feb);
                            Nodo_Arbol.descripcion37 = String.Format("{0:n}", Recaudado_Mar);
                            Nodo_Arbol.descripcion38 = String.Format("{0:n}", Recaudado_Abr);
                            Nodo_Arbol.descripcion39 = String.Format("{0:n}", Recaudado_May);
                            Nodo_Arbol.descripcion40 = String.Format("{0:n}", Recaudado_Jun);
                            Nodo_Arbol.descripcion41 = String.Format("{0:n}", Recaudado_Jul);
                            Nodo_Arbol.descripcion42 = String.Format("{0:n}", Recaudado_Ago);
                            Nodo_Arbol.descripcion43 = String.Format("{0:n}", Recaudado_Sep);
                            Nodo_Arbol.descripcion44 = String.Format("{0:n}", Recaudado_Oct);
                            Nodo_Arbol.descripcion45 = String.Format("{0:n}", Recaudado_Nov);
                            Nodo_Arbol.descripcion46 = String.Format("{0:n}", Recaudado_Dic);
                            Nodo_Arbol.descripcion47 = String.Format("{0:n}", Importe_Ene);
                            Nodo_Arbol.descripcion48 = String.Format("{0:n}", Importe_Feb);
                            Nodo_Arbol.descripcion49 = String.Format("{0:n}", Importe_Mar);
                            Nodo_Arbol.descripcion50 = String.Format("{0:n}", Importe_Abr);
                            Nodo_Arbol.descripcion51 = String.Format("{0:n}", Importe_May);
                            Nodo_Arbol.descripcion52 = String.Format("{0:n}", Importe_Jun);
                            Nodo_Arbol.descripcion53 = String.Format("{0:n}", Importe_Jul);
                            Nodo_Arbol.descripcion54 = String.Format("{0:n}", Importe_Ago);
                            Nodo_Arbol.descripcion55 = String.Format("{0:n}", Importe_Sep);
                            Nodo_Arbol.descripcion56 = String.Format("{0:n}", Importe_Oct);
                            Nodo_Arbol.descripcion57 = String.Format("{0:n}", Importe_Nov);
                            Nodo_Arbol.descripcion58 = String.Format("{0:n}", Importe_Dic);
                            Nodo_Arbol.descripcion59 = String.Format("{0:n}", Importe_Dic);

                            //Agregamos los atributos
                            Atributos = new Cls_Atributos_TreeGrid();
                            Atributos.valor1 = "Total";
                            Atributos.valor2 = "";
                            Atributos.valor3 = "";
                            Atributos.valor4 = "";
                            Atributos.valor5 = "";
                            Atributos.valor6 = "";
                            Atributos.valor7 = "";
                            Atributos.valor8 = "";

                            Nodo_Arbol.attributes = Atributos;
                            Lista_Nodo_Arbol.Add(Nodo_Arbol);


                            Json_Total = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);
                        }
                    }
                    return Json_Total;
                }
                catch (Exception Ex)
                {
                    throw new Exception(" Error al tratar de Obtener_Total Error[" + Ex.Message + "]");
                }
            }
        #endregion

        #region (EGRESOS)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_FF
            ///DESCRIPCIÓN          : Metodo para obtener las fuentes de financiamiento de egresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_FF(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF, String UR, String PP,
                String Cap, String Con, String PG, String P, String UR_F, String PP_F, String Cap_F, String Con_F, String PG_F, String P_F,
                String AF, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json = "{[]}"; //aki almacenaremos el json de los datos
                DataTable Dt = new DataTable(); // dt donde guardaremos los datos
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Total = String.Empty;
                String Json_Tot = String.Empty;
                
                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Periodo_Inicial = Mes;
                    Negocio.P_Fecha_Inicial = Fecha_Ini;
                    Negocio.P_Fecha_Final = Fecha_Fin;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_DependenciaF_ID = UR_F;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_ProgramaF_ID = PP_F;
                    Negocio.P_CapituloF_ID = Cap_F;
                    Negocio.P_ConceptoF_ID = Con_F;
                    Negocio.P_Partida_GenericaF_ID = PG_F;
                    Negocio.P_PartidaF_ID = P_F;
                    Negocio.P_Area_Funcional_ID = AF;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        if (String.IsNullOrEmpty(UR))
                        {
                            Negocio.P_Dependencia_ID = Obtener_Dependencias();
                        }
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = FF_Nomina;
                    }

                    Dt = Negocio.Consultar_Nivel_FF();

                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Egr(Dt);
                    }

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["id"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["nombre"].ToString().Trim();
                                Nodo_Arbol.state = Estado;
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["APROBADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DISPONIBLE"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["EJERCIDO"]);
                                Nodo_Arbol.descripcion10 = String.Format("{0:n}", Dr["PAGADO"]);
                                Nodo_Arbol.descripcion11 = Dr["FF"].ToString().Trim();
                                Nodo_Arbol.descripcion12 = Dr["UR"].ToString().Trim();
                                Nodo_Arbol.descripcion13 = Dr["PP"].ToString().Trim();
                                Nodo_Arbol.descripcion14 = String.Format("{0:n}", Dr["DISPONIBLE_ENE"]);
                                Nodo_Arbol.descripcion15 = String.Format("{0:n}", Dr["DISPONIBLE_FEB"]);
                                Nodo_Arbol.descripcion16 = String.Format("{0:n}", Dr["DISPONIBLE_MAR"]);
                                Nodo_Arbol.descripcion17 = String.Format("{0:n}", Dr["DISPONIBLE_ABR"]);
                                Nodo_Arbol.descripcion18 = String.Format("{0:n}", Dr["DISPONIBLE_MAY"]);
                                Nodo_Arbol.descripcion19 = String.Format("{0:n}", Dr["DISPONIBLE_JUN"]);
                                Nodo_Arbol.descripcion20 = String.Format("{0:n}", Dr["DISPONIBLE_JUL"]);
                                Nodo_Arbol.descripcion21 = String.Format("{0:n}", Dr["DISPONIBLE_AGO"]);
                                Nodo_Arbol.descripcion22 = String.Format("{0:n}", Dr["DISPONIBLE_SEP"]);
                                Nodo_Arbol.descripcion23 = String.Format("{0:n}", Dr["DISPONIBLE_OCT"]);
                                Nodo_Arbol.descripcion24 = String.Format("{0:n}", Dr["DISPONIBLE_NOV"]);
                                Nodo_Arbol.descripcion25 = String.Format("{0:n}", Dr["DISPONIBLE_DIC"]);
                                Nodo_Arbol.descripcion26 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_ENE"]);
                                Nodo_Arbol.descripcion27 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_FEB"]);
                                Nodo_Arbol.descripcion28 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_MAR"]);
                                Nodo_Arbol.descripcion29 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_ABR"]);
                                Nodo_Arbol.descripcion30 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_MAY"]);
                                Nodo_Arbol.descripcion31 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_JUN"]);
                                Nodo_Arbol.descripcion32 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_JUL"]);
                                Nodo_Arbol.descripcion33 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_AGO"]);
                                Nodo_Arbol.descripcion34 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_SEP"]);
                                Nodo_Arbol.descripcion35 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_OCT"]);
                                Nodo_Arbol.descripcion36 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_NOV"]);
                                Nodo_Arbol.descripcion37 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_DIC"]);
                                Nodo_Arbol.descripcion38 = String.Format("{0:n}", Dr["COMPROMETIDO_ENE"]);
                                Nodo_Arbol.descripcion39 = String.Format("{0:n}", Dr["COMPROMETIDO_FEB"]);
                                Nodo_Arbol.descripcion40 = String.Format("{0:n}", Dr["COMPROMETIDO_MAR"]);
                                Nodo_Arbol.descripcion41 = String.Format("{0:n}", Dr["COMPROMETIDO_ABR"]);
                                Nodo_Arbol.descripcion42 = String.Format("{0:n}", Dr["COMPROMETIDO_MAY"]);
                                Nodo_Arbol.descripcion43 = String.Format("{0:n}", Dr["COMPROMETIDO_JUN"]);
                                Nodo_Arbol.descripcion44 = String.Format("{0:n}", Dr["COMPROMETIDO_JUL"]);
                                Nodo_Arbol.descripcion45 = String.Format("{0:n}", Dr["COMPROMETIDO_AGO"]);
                                Nodo_Arbol.descripcion46 = String.Format("{0:n}", Dr["COMPROMETIDO_SEP"]);
                                Nodo_Arbol.descripcion47 = String.Format("{0:n}", Dr["COMPROMETIDO_OCT"]);
                                Nodo_Arbol.descripcion48 = String.Format("{0:n}", Dr["COMPROMETIDO_NOV"]);
                                Nodo_Arbol.descripcion49 = String.Format("{0:n}", Dr["COMPROMETIDO_DIC"]);
                                Nodo_Arbol.descripcion50 = String.Format("{0:n}", Dr["DEVENGADO_ENE"]);
                                Nodo_Arbol.descripcion51 = String.Format("{0:n}", Dr["DEVENGADO_FEB"]);
                                Nodo_Arbol.descripcion52 = String.Format("{0:n}", Dr["DEVENGADO_MAR"]);
                                Nodo_Arbol.descripcion53 = String.Format("{0:n}", Dr["DEVENGADO_ABR"]);
                                Nodo_Arbol.descripcion54 = String.Format("{0:n}", Dr["DEVENGADO_MAY"]);
                                Nodo_Arbol.descripcion55 = String.Format("{0:n}", Dr["DEVENGADO_JUN"]);
                                Nodo_Arbol.descripcion56 = String.Format("{0:n}", Dr["DEVENGADO_JUL"]);
                                Nodo_Arbol.descripcion57 = String.Format("{0:n}", Dr["DEVENGADO_AGO"]);
                                Nodo_Arbol.descripcion58 = String.Format("{0:n}", Dr["DEVENGADO_SEP"]);
                                Nodo_Arbol.descripcion59 = String.Format("{0:n}", Dr["DEVENGADO_OCT"]);
                                Nodo_Arbol.descripcion60 = String.Format("{0:n}", Dr["DEVENGADO_NOV"]);
                                Nodo_Arbol.descripcion61 = String.Format("{0:n}", Dr["DEVENGADO_DIC"]);
                                Nodo_Arbol.descripcion62 = String.Format("{0:n}", Dr["EJERCIDO_ENE"]);
                                Nodo_Arbol.descripcion63 = String.Format("{0:n}", Dr["EJERCIDO_FEB"]);
                                Nodo_Arbol.descripcion64 = String.Format("{0:n}", Dr["EJERCIDO_MAR"]);
                                Nodo_Arbol.descripcion65 = String.Format("{0:n}", Dr["EJERCIDO_ABR"]);
                                Nodo_Arbol.descripcion66 = String.Format("{0:n}", Dr["EJERCIDO_MAY"]);
                                Nodo_Arbol.descripcion67 = String.Format("{0:n}", Dr["EJERCIDO_JUN"]);
                                Nodo_Arbol.descripcion68 = String.Format("{0:n}", Dr["EJERCIDO_JUL"]);
                                Nodo_Arbol.descripcion69 = String.Format("{0:n}", Dr["EJERCIDO_AGO"]);
                                Nodo_Arbol.descripcion70 = String.Format("{0:n}", Dr["EJERCIDO_SEP"]);
                                Nodo_Arbol.descripcion71 = String.Format("{0:n}", Dr["EJERCIDO_OCT"]);
                                Nodo_Arbol.descripcion72 = String.Format("{0:n}", Dr["EJERCIDO_NOV"]);
                                Nodo_Arbol.descripcion73 = String.Format("{0:n}", Dr["EJERCIDO_DIC"]);
                                Nodo_Arbol.descripcion74 = String.Format("{0:n}", Dr["PAGADO_ENE"]);
                                Nodo_Arbol.descripcion75 = String.Format("{0:n}", Dr["PAGADO_FEB"]);
                                Nodo_Arbol.descripcion76 = String.Format("{0:n}", Dr["PAGADO_MAR"]);
                                Nodo_Arbol.descripcion77 = String.Format("{0:n}", Dr["PAGADO_ABR"]);
                                Nodo_Arbol.descripcion78 = String.Format("{0:n}", Dr["PAGADO_MAY"]);
                                Nodo_Arbol.descripcion79 = String.Format("{0:n}", Dr["PAGADO_JUN"]);
                                Nodo_Arbol.descripcion80 = String.Format("{0:n}", Dr["PAGADO_JUL"]);
                                Nodo_Arbol.descripcion81 = String.Format("{0:n}", Dr["PAGADO_AGO"]);
                                Nodo_Arbol.descripcion82 = String.Format("{0:n}", Dr["PAGADO_SEP"]);
                                Nodo_Arbol.descripcion83 = String.Format("{0:n}", Dr["PAGADO_OCT"]);
                                Nodo_Arbol.descripcion84 = String.Format("{0:n}", Dr["PAGADO_NOV"]);
                                Nodo_Arbol.descripcion85 = String.Format("{0:n}", Dr["PAGADO_DIC"]);
                                Nodo_Arbol.descripcion86 = String.Format("{0:n}", Dr["IMPORTE_ENE"]);
                                Nodo_Arbol.descripcion87 = String.Format("{0:n}", Dr["IMPORTE_FEB"]);
                                Nodo_Arbol.descripcion88 = String.Format("{0:n}", Dr["IMPORTE_MAR"]);
                                Nodo_Arbol.descripcion89 = String.Format("{0:n}", Dr["IMPORTE_ABR"]);
                                Nodo_Arbol.descripcion90 = String.Format("{0:n}", Dr["IMPORTE_MAY"]);
                                Nodo_Arbol.descripcion91 = String.Format("{0:n}", Dr["IMPORTE_JUN"]);
                                Nodo_Arbol.descripcion92 = String.Format("{0:n}", Dr["IMPORTE_JUL"]);
                                Nodo_Arbol.descripcion93 = String.Format("{0:n}", Dr["IMPORTE_AGO"]);
                                Nodo_Arbol.descripcion94 = String.Format("{0:n}", Dr["IMPORTE_SEP"]);
                                Nodo_Arbol.descripcion95 = String.Format("{0:n}", Dr["IMPORTE_OCT"]);
                                Nodo_Arbol.descripcion96 = String.Format("{0:n}", Dr["IMPORTE_NOV"]);
                                Nodo_Arbol.descripcion97 = String.Format("{0:n}", Dr["IMPORTE_DIC"]);
                                Nodo_Arbol.descripcion98 = String.Format("{0:n}", Dr["IMPORTE_TOTAL"]);

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_FF";
                                Atributos.valor2 = "";
                                Atributos.valor3 = "";
                                Atributos.valor4 = "";
                                Atributos.valor5 = "";
                                Atributos.valor6 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor7 = "";
                                Atributos.valor8 = "";
                                Atributos.valor9 = "";

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt.Rows.Count + ", \"rows\":";
                                Json_Tot += Json;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json = Json_Tot;
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_fuente de financiamiento Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_AF
            ///DESCRIPCIÓN          : Metodo para obtener las areas funcionales de egresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_AF(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF, String UR, String PP,
                String Cap, String Con, String PG, String P, String UR_F, String PP_F, String Cap_F, String Con_F, String PG_F, String P_F,
                String AF, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json = "{[]}"; //aki almacenaremos el json de los datos
                DataTable Dt = new DataTable(); // dt donde guardaremos los datos
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Total = String.Empty;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Periodo_Inicial = Mes;
                    Negocio.P_Fecha_Inicial = Fecha_Ini;
                    Negocio.P_Fecha_Final = Fecha_Fin;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_DependenciaF_ID = UR_F;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_ProgramaF_ID = PP_F;
                    Negocio.P_CapituloF_ID = Cap_F;
                    Negocio.P_ConceptoF_ID = Con_F;
                    Negocio.P_Partida_GenericaF_ID = PG_F;
                    Negocio.P_PartidaF_ID = P_F;
                    Negocio.P_Area_Funcional_ID = AF;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        if (String.IsNullOrEmpty(UR))
                        {
                            Negocio.P_Dependencia_ID = Obtener_Dependencias();
                        }
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = FF_Nomina;
                    }

                    Dt = Negocio.Consultar_Nivel_AF();

                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Egr(Dt);
                    }

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["id"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["nombre"].ToString().Trim();
                                Nodo_Arbol.state = Estado;
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["APROBADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DISPONIBLE"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["EJERCIDO"]);
                                Nodo_Arbol.descripcion10 = String.Format("{0:n}", Dr["PAGADO"]);
                                Nodo_Arbol.descripcion11 = Dr["FF"].ToString().Trim();
                                Nodo_Arbol.descripcion12 = Dr["UR"].ToString().Trim();
                                Nodo_Arbol.descripcion13 = Dr["PP"].ToString().Trim();
                                Nodo_Arbol.descripcion14 = String.Format("{0:n}", Dr["DISPONIBLE_ENE"]);
                                Nodo_Arbol.descripcion15 = String.Format("{0:n}", Dr["DISPONIBLE_FEB"]);
                                Nodo_Arbol.descripcion16 = String.Format("{0:n}", Dr["DISPONIBLE_MAR"]);
                                Nodo_Arbol.descripcion17 = String.Format("{0:n}", Dr["DISPONIBLE_ABR"]);
                                Nodo_Arbol.descripcion18 = String.Format("{0:n}", Dr["DISPONIBLE_MAY"]);
                                Nodo_Arbol.descripcion19 = String.Format("{0:n}", Dr["DISPONIBLE_JUN"]);
                                Nodo_Arbol.descripcion20 = String.Format("{0:n}", Dr["DISPONIBLE_JUL"]);
                                Nodo_Arbol.descripcion21 = String.Format("{0:n}", Dr["DISPONIBLE_AGO"]);
                                Nodo_Arbol.descripcion22 = String.Format("{0:n}", Dr["DISPONIBLE_SEP"]);
                                Nodo_Arbol.descripcion23 = String.Format("{0:n}", Dr["DISPONIBLE_OCT"]);
                                Nodo_Arbol.descripcion24 = String.Format("{0:n}", Dr["DISPONIBLE_NOV"]);
                                Nodo_Arbol.descripcion25 = String.Format("{0:n}", Dr["DISPONIBLE_DIC"]);
                                Nodo_Arbol.descripcion26 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_ENE"]);
                                Nodo_Arbol.descripcion27 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_FEB"]);
                                Nodo_Arbol.descripcion28 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_MAR"]);
                                Nodo_Arbol.descripcion29 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_ABR"]);
                                Nodo_Arbol.descripcion30 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_MAY"]);
                                Nodo_Arbol.descripcion31 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_JUN"]);
                                Nodo_Arbol.descripcion32 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_JUL"]);
                                Nodo_Arbol.descripcion33 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_AGO"]);
                                Nodo_Arbol.descripcion34 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_SEP"]);
                                Nodo_Arbol.descripcion35 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_OCT"]);
                                Nodo_Arbol.descripcion36 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_NOV"]);
                                Nodo_Arbol.descripcion37 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_DIC"]);
                                Nodo_Arbol.descripcion38 = String.Format("{0:n}", Dr["COMPROMETIDO_ENE"]);
                                Nodo_Arbol.descripcion39 = String.Format("{0:n}", Dr["COMPROMETIDO_FEB"]);
                                Nodo_Arbol.descripcion40 = String.Format("{0:n}", Dr["COMPROMETIDO_MAR"]);
                                Nodo_Arbol.descripcion41 = String.Format("{0:n}", Dr["COMPROMETIDO_ABR"]);
                                Nodo_Arbol.descripcion42 = String.Format("{0:n}", Dr["COMPROMETIDO_MAY"]);
                                Nodo_Arbol.descripcion43 = String.Format("{0:n}", Dr["COMPROMETIDO_JUN"]);
                                Nodo_Arbol.descripcion44 = String.Format("{0:n}", Dr["COMPROMETIDO_JUL"]);
                                Nodo_Arbol.descripcion45 = String.Format("{0:n}", Dr["COMPROMETIDO_AGO"]);
                                Nodo_Arbol.descripcion46 = String.Format("{0:n}", Dr["COMPROMETIDO_SEP"]);
                                Nodo_Arbol.descripcion47 = String.Format("{0:n}", Dr["COMPROMETIDO_OCT"]);
                                Nodo_Arbol.descripcion48 = String.Format("{0:n}", Dr["COMPROMETIDO_NOV"]);
                                Nodo_Arbol.descripcion49 = String.Format("{0:n}", Dr["COMPROMETIDO_DIC"]);
                                Nodo_Arbol.descripcion50 = String.Format("{0:n}", Dr["DEVENGADO_ENE"]);
                                Nodo_Arbol.descripcion51 = String.Format("{0:n}", Dr["DEVENGADO_FEB"]);
                                Nodo_Arbol.descripcion52 = String.Format("{0:n}", Dr["DEVENGADO_MAR"]);
                                Nodo_Arbol.descripcion53 = String.Format("{0:n}", Dr["DEVENGADO_ABR"]);
                                Nodo_Arbol.descripcion54 = String.Format("{0:n}", Dr["DEVENGADO_MAY"]);
                                Nodo_Arbol.descripcion55 = String.Format("{0:n}", Dr["DEVENGADO_JUN"]);
                                Nodo_Arbol.descripcion56 = String.Format("{0:n}", Dr["DEVENGADO_JUL"]);
                                Nodo_Arbol.descripcion57 = String.Format("{0:n}", Dr["DEVENGADO_AGO"]);
                                Nodo_Arbol.descripcion58 = String.Format("{0:n}", Dr["DEVENGADO_SEP"]);
                                Nodo_Arbol.descripcion59 = String.Format("{0:n}", Dr["DEVENGADO_OCT"]);
                                Nodo_Arbol.descripcion60 = String.Format("{0:n}", Dr["DEVENGADO_NOV"]);
                                Nodo_Arbol.descripcion61 = String.Format("{0:n}", Dr["DEVENGADO_DIC"]);
                                Nodo_Arbol.descripcion62 = String.Format("{0:n}", Dr["EJERCIDO_ENE"]);
                                Nodo_Arbol.descripcion63 = String.Format("{0:n}", Dr["EJERCIDO_FEB"]);
                                Nodo_Arbol.descripcion64 = String.Format("{0:n}", Dr["EJERCIDO_MAR"]);
                                Nodo_Arbol.descripcion65 = String.Format("{0:n}", Dr["EJERCIDO_ABR"]);
                                Nodo_Arbol.descripcion66 = String.Format("{0:n}", Dr["EJERCIDO_MAY"]);
                                Nodo_Arbol.descripcion67 = String.Format("{0:n}", Dr["EJERCIDO_JUN"]);
                                Nodo_Arbol.descripcion68 = String.Format("{0:n}", Dr["EJERCIDO_JUL"]);
                                Nodo_Arbol.descripcion69 = String.Format("{0:n}", Dr["EJERCIDO_AGO"]);
                                Nodo_Arbol.descripcion70 = String.Format("{0:n}", Dr["EJERCIDO_SEP"]);
                                Nodo_Arbol.descripcion71 = String.Format("{0:n}", Dr["EJERCIDO_OCT"]);
                                Nodo_Arbol.descripcion72 = String.Format("{0:n}", Dr["EJERCIDO_NOV"]);
                                Nodo_Arbol.descripcion73 = String.Format("{0:n}", Dr["EJERCIDO_DIC"]);
                                Nodo_Arbol.descripcion74 = String.Format("{0:n}", Dr["PAGADO_ENE"]);
                                Nodo_Arbol.descripcion75 = String.Format("{0:n}", Dr["PAGADO_FEB"]);
                                Nodo_Arbol.descripcion76 = String.Format("{0:n}", Dr["PAGADO_MAR"]);
                                Nodo_Arbol.descripcion77 = String.Format("{0:n}", Dr["PAGADO_ABR"]);
                                Nodo_Arbol.descripcion78 = String.Format("{0:n}", Dr["PAGADO_MAY"]);
                                Nodo_Arbol.descripcion79 = String.Format("{0:n}", Dr["PAGADO_JUN"]);
                                Nodo_Arbol.descripcion80 = String.Format("{0:n}", Dr["PAGADO_JUL"]);
                                Nodo_Arbol.descripcion81 = String.Format("{0:n}", Dr["PAGADO_AGO"]);
                                Nodo_Arbol.descripcion82 = String.Format("{0:n}", Dr["PAGADO_SEP"]);
                                Nodo_Arbol.descripcion83 = String.Format("{0:n}", Dr["PAGADO_OCT"]);
                                Nodo_Arbol.descripcion84 = String.Format("{0:n}", Dr["PAGADO_NOV"]);
                                Nodo_Arbol.descripcion85 = String.Format("{0:n}", Dr["PAGADO_DIC"]);
                                Nodo_Arbol.descripcion86 = String.Format("{0:n}", Dr["IMPORTE_ENE"]);
                                Nodo_Arbol.descripcion87 = String.Format("{0:n}", Dr["IMPORTE_FEB"]);
                                Nodo_Arbol.descripcion88 = String.Format("{0:n}", Dr["IMPORTE_MAR"]);
                                Nodo_Arbol.descripcion89 = String.Format("{0:n}", Dr["IMPORTE_ABR"]);
                                Nodo_Arbol.descripcion90 = String.Format("{0:n}", Dr["IMPORTE_MAY"]);
                                Nodo_Arbol.descripcion91 = String.Format("{0:n}", Dr["IMPORTE_JUN"]);
                                Nodo_Arbol.descripcion92 = String.Format("{0:n}", Dr["IMPORTE_JUL"]);
                                Nodo_Arbol.descripcion93 = String.Format("{0:n}", Dr["IMPORTE_AGO"]);
                                Nodo_Arbol.descripcion94 = String.Format("{0:n}", Dr["IMPORTE_SEP"]);
                                Nodo_Arbol.descripcion95 = String.Format("{0:n}", Dr["IMPORTE_OCT"]);
                                Nodo_Arbol.descripcion96 = String.Format("{0:n}", Dr["IMPORTE_NOV"]);
                                Nodo_Arbol.descripcion97 = String.Format("{0:n}", Dr["IMPORTE_DIC"]);
                                Nodo_Arbol.descripcion98 = String.Format("{0:n}", Dr["IMPORTE_TOTAL"]);

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_AF";
                                Atributos.valor2 = "";
                                Atributos.valor3 = "";
                                Atributos.valor4 = "";
                                Atributos.valor5 = "";
                                Atributos.valor6 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor7 = Dr["AF_ID"].ToString().Trim();
                                Atributos.valor8 = "";
                                Atributos.valor9 = "";

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt.Rows.Count + ", \"rows\":";
                                Json_Tot += Json;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json = Json_Tot;
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener las areas funcionales Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_PP
            ///DESCRIPCIÓN          : Metodo para obtener los progamas de egresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_PP(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF, String UR, String PP,
                String Cap, String Con, String PG, String P, String UR_F, String PP_F, String Cap_F, String Con_F, String PG_F, String P_F,
                String AF, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json = "{[]}"; //aki almacenaremos el json de los datos
                DataTable Dt = new DataTable(); // dt donde guardaremos los datos
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Total = String.Empty;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Periodo_Inicial = Mes;
                    Negocio.P_Fecha_Inicial = Fecha_Ini;
                    Negocio.P_Fecha_Final = Fecha_Fin;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_DependenciaF_ID = UR_F;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_ProgramaF_ID = PP_F;
                    Negocio.P_CapituloF_ID = Cap_F;
                    Negocio.P_ConceptoF_ID = Con_F;
                    Negocio.P_Partida_GenericaF_ID = PG_F;
                    Negocio.P_PartidaF_ID = P_F;
                    Negocio.P_Area_Funcional_ID = AF;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        if (String.IsNullOrEmpty(UR))
                        {
                            Negocio.P_Dependencia_ID = Obtener_Dependencias();
                        }
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = FF_Nomina;
                    }

                    Dt = Negocio.Consultar_Nivel_PP();

                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Egr(Dt);
                    }

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["id"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["nombre"].ToString().Trim();
                                Nodo_Arbol.state = Estado;
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["APROBADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DISPONIBLE"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["EJERCIDO"]);
                                Nodo_Arbol.descripcion10 = String.Format("{0:n}", Dr["PAGADO"]);
                                Nodo_Arbol.descripcion11 = Dr["FF"].ToString().Trim();
                                Nodo_Arbol.descripcion12 = Dr["UR"].ToString().Trim();
                                Nodo_Arbol.descripcion13 = Dr["PP"].ToString().Trim();
                                Nodo_Arbol.descripcion14 = String.Format("{0:n}", Dr["DISPONIBLE_ENE"]);
                                Nodo_Arbol.descripcion15 = String.Format("{0:n}", Dr["DISPONIBLE_FEB"]);
                                Nodo_Arbol.descripcion16 = String.Format("{0:n}", Dr["DISPONIBLE_MAR"]);
                                Nodo_Arbol.descripcion17 = String.Format("{0:n}", Dr["DISPONIBLE_ABR"]);
                                Nodo_Arbol.descripcion18 = String.Format("{0:n}", Dr["DISPONIBLE_MAY"]);
                                Nodo_Arbol.descripcion19 = String.Format("{0:n}", Dr["DISPONIBLE_JUN"]);
                                Nodo_Arbol.descripcion20 = String.Format("{0:n}", Dr["DISPONIBLE_JUL"]);
                                Nodo_Arbol.descripcion21 = String.Format("{0:n}", Dr["DISPONIBLE_AGO"]);
                                Nodo_Arbol.descripcion22 = String.Format("{0:n}", Dr["DISPONIBLE_SEP"]);
                                Nodo_Arbol.descripcion23 = String.Format("{0:n}", Dr["DISPONIBLE_OCT"]);
                                Nodo_Arbol.descripcion24 = String.Format("{0:n}", Dr["DISPONIBLE_NOV"]);
                                Nodo_Arbol.descripcion25 = String.Format("{0:n}", Dr["DISPONIBLE_DIC"]);
                                Nodo_Arbol.descripcion26 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_ENE"]);
                                Nodo_Arbol.descripcion27 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_FEB"]);
                                Nodo_Arbol.descripcion28 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_MAR"]);
                                Nodo_Arbol.descripcion29 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_ABR"]);
                                Nodo_Arbol.descripcion30 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_MAY"]);
                                Nodo_Arbol.descripcion31 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_JUN"]);
                                Nodo_Arbol.descripcion32 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_JUL"]);
                                Nodo_Arbol.descripcion33 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_AGO"]);
                                Nodo_Arbol.descripcion34 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_SEP"]);
                                Nodo_Arbol.descripcion35 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_OCT"]);
                                Nodo_Arbol.descripcion36 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_NOV"]);
                                Nodo_Arbol.descripcion37 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_DIC"]);
                                Nodo_Arbol.descripcion38 = String.Format("{0:n}", Dr["COMPROMETIDO_ENE"]);
                                Nodo_Arbol.descripcion39 = String.Format("{0:n}", Dr["COMPROMETIDO_FEB"]);
                                Nodo_Arbol.descripcion40 = String.Format("{0:n}", Dr["COMPROMETIDO_MAR"]);
                                Nodo_Arbol.descripcion41 = String.Format("{0:n}", Dr["COMPROMETIDO_ABR"]);
                                Nodo_Arbol.descripcion42 = String.Format("{0:n}", Dr["COMPROMETIDO_MAY"]);
                                Nodo_Arbol.descripcion43 = String.Format("{0:n}", Dr["COMPROMETIDO_JUN"]);
                                Nodo_Arbol.descripcion44 = String.Format("{0:n}", Dr["COMPROMETIDO_JUL"]);
                                Nodo_Arbol.descripcion45 = String.Format("{0:n}", Dr["COMPROMETIDO_AGO"]);
                                Nodo_Arbol.descripcion46 = String.Format("{0:n}", Dr["COMPROMETIDO_SEP"]);
                                Nodo_Arbol.descripcion47 = String.Format("{0:n}", Dr["COMPROMETIDO_OCT"]);
                                Nodo_Arbol.descripcion48 = String.Format("{0:n}", Dr["COMPROMETIDO_NOV"]);
                                Nodo_Arbol.descripcion49 = String.Format("{0:n}", Dr["COMPROMETIDO_DIC"]);
                                Nodo_Arbol.descripcion50 = String.Format("{0:n}", Dr["DEVENGADO_ENE"]);
                                Nodo_Arbol.descripcion51 = String.Format("{0:n}", Dr["DEVENGADO_FEB"]);
                                Nodo_Arbol.descripcion52 = String.Format("{0:n}", Dr["DEVENGADO_MAR"]);
                                Nodo_Arbol.descripcion53 = String.Format("{0:n}", Dr["DEVENGADO_ABR"]);
                                Nodo_Arbol.descripcion54 = String.Format("{0:n}", Dr["DEVENGADO_MAY"]);
                                Nodo_Arbol.descripcion55 = String.Format("{0:n}", Dr["DEVENGADO_JUN"]);
                                Nodo_Arbol.descripcion56 = String.Format("{0:n}", Dr["DEVENGADO_JUL"]);
                                Nodo_Arbol.descripcion57 = String.Format("{0:n}", Dr["DEVENGADO_AGO"]);
                                Nodo_Arbol.descripcion58 = String.Format("{0:n}", Dr["DEVENGADO_SEP"]);
                                Nodo_Arbol.descripcion59 = String.Format("{0:n}", Dr["DEVENGADO_OCT"]);
                                Nodo_Arbol.descripcion60 = String.Format("{0:n}", Dr["DEVENGADO_NOV"]);
                                Nodo_Arbol.descripcion61 = String.Format("{0:n}", Dr["DEVENGADO_DIC"]);
                                Nodo_Arbol.descripcion62 = String.Format("{0:n}", Dr["EJERCIDO_ENE"]);
                                Nodo_Arbol.descripcion63 = String.Format("{0:n}", Dr["EJERCIDO_FEB"]);
                                Nodo_Arbol.descripcion64 = String.Format("{0:n}", Dr["EJERCIDO_MAR"]);
                                Nodo_Arbol.descripcion65 = String.Format("{0:n}", Dr["EJERCIDO_ABR"]);
                                Nodo_Arbol.descripcion66 = String.Format("{0:n}", Dr["EJERCIDO_MAY"]);
                                Nodo_Arbol.descripcion67 = String.Format("{0:n}", Dr["EJERCIDO_JUN"]);
                                Nodo_Arbol.descripcion68 = String.Format("{0:n}", Dr["EJERCIDO_JUL"]);
                                Nodo_Arbol.descripcion69 = String.Format("{0:n}", Dr["EJERCIDO_AGO"]);
                                Nodo_Arbol.descripcion70 = String.Format("{0:n}", Dr["EJERCIDO_SEP"]);
                                Nodo_Arbol.descripcion71 = String.Format("{0:n}", Dr["EJERCIDO_OCT"]);
                                Nodo_Arbol.descripcion72 = String.Format("{0:n}", Dr["EJERCIDO_NOV"]);
                                Nodo_Arbol.descripcion73 = String.Format("{0:n}", Dr["EJERCIDO_DIC"]);
                                Nodo_Arbol.descripcion74 = String.Format("{0:n}", Dr["PAGADO_ENE"]);
                                Nodo_Arbol.descripcion75 = String.Format("{0:n}", Dr["PAGADO_FEB"]);
                                Nodo_Arbol.descripcion76 = String.Format("{0:n}", Dr["PAGADO_MAR"]);
                                Nodo_Arbol.descripcion77 = String.Format("{0:n}", Dr["PAGADO_ABR"]);
                                Nodo_Arbol.descripcion78 = String.Format("{0:n}", Dr["PAGADO_MAY"]);
                                Nodo_Arbol.descripcion79 = String.Format("{0:n}", Dr["PAGADO_JUN"]);
                                Nodo_Arbol.descripcion80 = String.Format("{0:n}", Dr["PAGADO_JUL"]);
                                Nodo_Arbol.descripcion81 = String.Format("{0:n}", Dr["PAGADO_AGO"]);
                                Nodo_Arbol.descripcion82 = String.Format("{0:n}", Dr["PAGADO_SEP"]);
                                Nodo_Arbol.descripcion83 = String.Format("{0:n}", Dr["PAGADO_OCT"]);
                                Nodo_Arbol.descripcion84 = String.Format("{0:n}", Dr["PAGADO_NOV"]);
                                Nodo_Arbol.descripcion85 = String.Format("{0:n}", Dr["PAGADO_DIC"]);
                                Nodo_Arbol.descripcion86 = String.Format("{0:n}", Dr["IMPORTE_ENE"]);
                                Nodo_Arbol.descripcion87 = String.Format("{0:n}", Dr["IMPORTE_FEB"]);
                                Nodo_Arbol.descripcion88 = String.Format("{0:n}", Dr["IMPORTE_MAR"]);
                                Nodo_Arbol.descripcion89 = String.Format("{0:n}", Dr["IMPORTE_ABR"]);
                                Nodo_Arbol.descripcion90 = String.Format("{0:n}", Dr["IMPORTE_MAY"]);
                                Nodo_Arbol.descripcion91 = String.Format("{0:n}", Dr["IMPORTE_JUN"]);
                                Nodo_Arbol.descripcion92 = String.Format("{0:n}", Dr["IMPORTE_JUL"]);
                                Nodo_Arbol.descripcion93 = String.Format("{0:n}", Dr["IMPORTE_AGO"]);
                                Nodo_Arbol.descripcion94 = String.Format("{0:n}", Dr["IMPORTE_SEP"]);
                                Nodo_Arbol.descripcion95 = String.Format("{0:n}", Dr["IMPORTE_OCT"]);
                                Nodo_Arbol.descripcion96 = String.Format("{0:n}", Dr["IMPORTE_NOV"]);
                                Nodo_Arbol.descripcion97 = String.Format("{0:n}", Dr["IMPORTE_DIC"]);
                                Nodo_Arbol.descripcion98 = String.Format("{0:n}", Dr["IMPORTE_TOTAL"]);

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_PP";
                                Atributos.valor2 = "";
                                Atributos.valor3 = "";
                                Atributos.valor4 = "";
                                Atributos.valor5 = "";
                                Atributos.valor6 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor7 = Dr["AF_ID"].ToString().Trim();
                                Atributos.valor8 = Dr["PP_ID"].ToString().Trim();
                                Atributos.valor9 = "";

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt.Rows.Count + ", \"rows\":";
                                Json_Tot += Json;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json = Json_Tot;
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener los programas Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_UR
            ///DESCRIPCIÓN          : Metodo para obtener las unidades responsables de egresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_UR(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF, String UR, String PP,
                String Cap, String Con, String PG, String P, String UR_F, String PP_F, String Cap_F, String Con_F, String PG_F, String P_F,
                String AF, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json = "{[]}"; //aki almacenaremos el json de los datos
                DataTable Dt = new DataTable(); // dt donde guardaremos los datos
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Total = String.Empty;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Periodo_Inicial = Mes;
                    Negocio.P_Fecha_Inicial = Fecha_Ini;
                    Negocio.P_Fecha_Final = Fecha_Fin;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_DependenciaF_ID = UR_F;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_ProgramaF_ID = PP_F;
                    Negocio.P_CapituloF_ID = Cap_F;
                    Negocio.P_ConceptoF_ID = Con_F;
                    Negocio.P_Partida_GenericaF_ID = PG_F;
                    Negocio.P_PartidaF_ID = P_F;
                    Negocio.P_Area_Funcional_ID = AF;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        if (String.IsNullOrEmpty(UR))
                        {
                            Negocio.P_Dependencia_ID = Obtener_Dependencias();
                        }
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = FF_Nomina;
                    }

                    Dt = Negocio.Consultar_Nivel_UR();

                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Egr(Dt);
                    }

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["id"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["nombre"].ToString().Trim();
                                Nodo_Arbol.state = Estado;
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["APROBADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DISPONIBLE"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["EJERCIDO"]);
                                Nodo_Arbol.descripcion10 = String.Format("{0:n}", Dr["PAGADO"]);
                                Nodo_Arbol.descripcion11 = Dr["FF"].ToString().Trim();
                                Nodo_Arbol.descripcion12 = Dr["UR"].ToString().Trim();
                                Nodo_Arbol.descripcion13 = Dr["PP"].ToString().Trim();
                                Nodo_Arbol.descripcion14 = String.Format("{0:n}", Dr["DISPONIBLE_ENE"]);
                                Nodo_Arbol.descripcion15 = String.Format("{0:n}", Dr["DISPONIBLE_FEB"]);
                                Nodo_Arbol.descripcion16 = String.Format("{0:n}", Dr["DISPONIBLE_MAR"]);
                                Nodo_Arbol.descripcion17 = String.Format("{0:n}", Dr["DISPONIBLE_ABR"]);
                                Nodo_Arbol.descripcion18 = String.Format("{0:n}", Dr["DISPONIBLE_MAY"]);
                                Nodo_Arbol.descripcion19 = String.Format("{0:n}", Dr["DISPONIBLE_JUN"]);
                                Nodo_Arbol.descripcion20 = String.Format("{0:n}", Dr["DISPONIBLE_JUL"]);
                                Nodo_Arbol.descripcion21 = String.Format("{0:n}", Dr["DISPONIBLE_AGO"]);
                                Nodo_Arbol.descripcion22 = String.Format("{0:n}", Dr["DISPONIBLE_SEP"]);
                                Nodo_Arbol.descripcion23 = String.Format("{0:n}", Dr["DISPONIBLE_OCT"]);
                                Nodo_Arbol.descripcion24 = String.Format("{0:n}", Dr["DISPONIBLE_NOV"]);
                                Nodo_Arbol.descripcion25 = String.Format("{0:n}", Dr["DISPONIBLE_DIC"]);
                                Nodo_Arbol.descripcion26 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_ENE"]);
                                Nodo_Arbol.descripcion27 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_FEB"]);
                                Nodo_Arbol.descripcion28 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_MAR"]);
                                Nodo_Arbol.descripcion29 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_ABR"]);
                                Nodo_Arbol.descripcion30 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_MAY"]);
                                Nodo_Arbol.descripcion31 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_JUN"]);
                                Nodo_Arbol.descripcion32 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_JUL"]);
                                Nodo_Arbol.descripcion33 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_AGO"]);
                                Nodo_Arbol.descripcion34 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_SEP"]);
                                Nodo_Arbol.descripcion35 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_OCT"]);
                                Nodo_Arbol.descripcion36 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_NOV"]);
                                Nodo_Arbol.descripcion37 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_DIC"]);
                                Nodo_Arbol.descripcion38 = String.Format("{0:n}", Dr["COMPROMETIDO_ENE"]);
                                Nodo_Arbol.descripcion39 = String.Format("{0:n}", Dr["COMPROMETIDO_FEB"]);
                                Nodo_Arbol.descripcion40 = String.Format("{0:n}", Dr["COMPROMETIDO_MAR"]);
                                Nodo_Arbol.descripcion41 = String.Format("{0:n}", Dr["COMPROMETIDO_ABR"]);
                                Nodo_Arbol.descripcion42 = String.Format("{0:n}", Dr["COMPROMETIDO_MAY"]);
                                Nodo_Arbol.descripcion43 = String.Format("{0:n}", Dr["COMPROMETIDO_JUN"]);
                                Nodo_Arbol.descripcion44 = String.Format("{0:n}", Dr["COMPROMETIDO_JUL"]);
                                Nodo_Arbol.descripcion45 = String.Format("{0:n}", Dr["COMPROMETIDO_AGO"]);
                                Nodo_Arbol.descripcion46 = String.Format("{0:n}", Dr["COMPROMETIDO_SEP"]);
                                Nodo_Arbol.descripcion47 = String.Format("{0:n}", Dr["COMPROMETIDO_OCT"]);
                                Nodo_Arbol.descripcion48 = String.Format("{0:n}", Dr["COMPROMETIDO_NOV"]);
                                Nodo_Arbol.descripcion49 = String.Format("{0:n}", Dr["COMPROMETIDO_DIC"]);
                                Nodo_Arbol.descripcion50 = String.Format("{0:n}", Dr["DEVENGADO_ENE"]);
                                Nodo_Arbol.descripcion51 = String.Format("{0:n}", Dr["DEVENGADO_FEB"]);
                                Nodo_Arbol.descripcion52 = String.Format("{0:n}", Dr["DEVENGADO_MAR"]);
                                Nodo_Arbol.descripcion53 = String.Format("{0:n}", Dr["DEVENGADO_ABR"]);
                                Nodo_Arbol.descripcion54 = String.Format("{0:n}", Dr["DEVENGADO_MAY"]);
                                Nodo_Arbol.descripcion55 = String.Format("{0:n}", Dr["DEVENGADO_JUN"]);
                                Nodo_Arbol.descripcion56 = String.Format("{0:n}", Dr["DEVENGADO_JUL"]);
                                Nodo_Arbol.descripcion57 = String.Format("{0:n}", Dr["DEVENGADO_AGO"]);
                                Nodo_Arbol.descripcion58 = String.Format("{0:n}", Dr["DEVENGADO_SEP"]);
                                Nodo_Arbol.descripcion59 = String.Format("{0:n}", Dr["DEVENGADO_OCT"]);
                                Nodo_Arbol.descripcion60 = String.Format("{0:n}", Dr["DEVENGADO_NOV"]);
                                Nodo_Arbol.descripcion61 = String.Format("{0:n}", Dr["DEVENGADO_DIC"]);
                                Nodo_Arbol.descripcion62 = String.Format("{0:n}", Dr["EJERCIDO_ENE"]);
                                Nodo_Arbol.descripcion63 = String.Format("{0:n}", Dr["EJERCIDO_FEB"]);
                                Nodo_Arbol.descripcion64 = String.Format("{0:n}", Dr["EJERCIDO_MAR"]);
                                Nodo_Arbol.descripcion65 = String.Format("{0:n}", Dr["EJERCIDO_ABR"]);
                                Nodo_Arbol.descripcion66 = String.Format("{0:n}", Dr["EJERCIDO_MAY"]);
                                Nodo_Arbol.descripcion67 = String.Format("{0:n}", Dr["EJERCIDO_JUN"]);
                                Nodo_Arbol.descripcion68 = String.Format("{0:n}", Dr["EJERCIDO_JUL"]);
                                Nodo_Arbol.descripcion69 = String.Format("{0:n}", Dr["EJERCIDO_AGO"]);
                                Nodo_Arbol.descripcion70 = String.Format("{0:n}", Dr["EJERCIDO_SEP"]);
                                Nodo_Arbol.descripcion71 = String.Format("{0:n}", Dr["EJERCIDO_OCT"]);
                                Nodo_Arbol.descripcion72 = String.Format("{0:n}", Dr["EJERCIDO_NOV"]);
                                Nodo_Arbol.descripcion73 = String.Format("{0:n}", Dr["EJERCIDO_DIC"]);
                                Nodo_Arbol.descripcion74 = String.Format("{0:n}", Dr["PAGADO_ENE"]);
                                Nodo_Arbol.descripcion75 = String.Format("{0:n}", Dr["PAGADO_FEB"]);
                                Nodo_Arbol.descripcion76 = String.Format("{0:n}", Dr["PAGADO_MAR"]);
                                Nodo_Arbol.descripcion77 = String.Format("{0:n}", Dr["PAGADO_ABR"]);
                                Nodo_Arbol.descripcion78 = String.Format("{0:n}", Dr["PAGADO_MAY"]);
                                Nodo_Arbol.descripcion79 = String.Format("{0:n}", Dr["PAGADO_JUN"]);
                                Nodo_Arbol.descripcion80 = String.Format("{0:n}", Dr["PAGADO_JUL"]);
                                Nodo_Arbol.descripcion81 = String.Format("{0:n}", Dr["PAGADO_AGO"]);
                                Nodo_Arbol.descripcion82 = String.Format("{0:n}", Dr["PAGADO_SEP"]);
                                Nodo_Arbol.descripcion83 = String.Format("{0:n}", Dr["PAGADO_OCT"]);
                                Nodo_Arbol.descripcion84 = String.Format("{0:n}", Dr["PAGADO_NOV"]);
                                Nodo_Arbol.descripcion85 = String.Format("{0:n}", Dr["PAGADO_DIC"]);
                                Nodo_Arbol.descripcion86 = String.Format("{0:n}", Dr["IMPORTE_ENE"]);
                                Nodo_Arbol.descripcion87 = String.Format("{0:n}", Dr["IMPORTE_FEB"]);
                                Nodo_Arbol.descripcion88 = String.Format("{0:n}", Dr["IMPORTE_MAR"]);
                                Nodo_Arbol.descripcion89 = String.Format("{0:n}", Dr["IMPORTE_ABR"]);
                                Nodo_Arbol.descripcion90 = String.Format("{0:n}", Dr["IMPORTE_MAY"]);
                                Nodo_Arbol.descripcion91 = String.Format("{0:n}", Dr["IMPORTE_JUN"]);
                                Nodo_Arbol.descripcion92 = String.Format("{0:n}", Dr["IMPORTE_JUL"]);
                                Nodo_Arbol.descripcion93 = String.Format("{0:n}", Dr["IMPORTE_AGO"]);
                                Nodo_Arbol.descripcion94 = String.Format("{0:n}", Dr["IMPORTE_SEP"]);
                                Nodo_Arbol.descripcion95 = String.Format("{0:n}", Dr["IMPORTE_OCT"]);
                                Nodo_Arbol.descripcion96 = String.Format("{0:n}", Dr["IMPORTE_NOV"]);
                                Nodo_Arbol.descripcion97 = String.Format("{0:n}", Dr["IMPORTE_DIC"]);
                                Nodo_Arbol.descripcion98 = String.Format("{0:n}", Dr["IMPORTE_TOTAL"]);

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_UR";
                                Atributos.valor2 = "";
                                Atributos.valor3 = "";
                                Atributos.valor4 = "";
                                Atributos.valor5 = "";
                                Atributos.valor6 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor7 = Dr["AF_ID"].ToString().Trim();
                                Atributos.valor8 = Dr["PP_ID"].ToString().Trim();
                                Atributos.valor9 = Dr["UR_ID"].ToString().Trim();

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt.Rows.Count + ", \"rows\":";
                                Json_Tot += Json;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json = Json_Tot;
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener  las unidades responsables Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_Capitulos
            ///DESCRIPCIÓN          : Metodo para obtener los capitulos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_Capitulos(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF, String UR, String PP,
                String Cap, String Con, String PG, String P, String UR_F, String PP_F, String Cap_F, String Con_F, String PG_F, String P_F,
                String AF, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json_Capitulos = "{[]}"; //aki almacenaremos el json de los datos
                DataTable Dt_Capitulos = new DataTable(); // dt donde guardaremos los datos
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Total = String.Empty;
                String Json = string.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Periodo_Inicial = Mes;
                    Negocio.P_Fecha_Inicial = Fecha_Ini;
                    Negocio.P_Fecha_Final = Fecha_Fin;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_DependenciaF_ID = UR_F;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_ProgramaF_ID = PP_F;
                    Negocio.P_CapituloF_ID = Cap_F;
                    Negocio.P_ConceptoF_ID = Con_F;
                    Negocio.P_Partida_GenericaF_ID = PG_F;
                    Negocio.P_PartidaF_ID = P_F;

                    Negocio.P_Area_Funcional_ID = AF;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        if (String.IsNullOrEmpty(UR))
                        {
                            Negocio.P_Dependencia_ID = Obtener_Dependencias();
                        }
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = FF_Nomina;
                    }

                    Dt_Capitulos = Negocio.Consultar_Nivel_Capitulo();

                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Egr(Dt_Capitulos);
                    }

                    if (Dt_Capitulos != null)
                    {
                        if (Dt_Capitulos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Capitulos.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["id"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["nombre"].ToString().Trim();
                                Nodo_Arbol.state = Estado;
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["APROBADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DISPONIBLE"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["EJERCIDO"]);
                                Nodo_Arbol.descripcion10 = String.Format("{0:n}", Dr["PAGADO"]);
                                Nodo_Arbol.descripcion11 = Dr["FF"].ToString().Trim();
                                Nodo_Arbol.descripcion12 = Dr["UR"].ToString().Trim();
                                Nodo_Arbol.descripcion13 = Dr["PP"].ToString().Trim();
                                Nodo_Arbol.descripcion14 = String.Format("{0:n}", Dr["DISPONIBLE_ENE"]);
                                Nodo_Arbol.descripcion15 = String.Format("{0:n}", Dr["DISPONIBLE_FEB"]);
                                Nodo_Arbol.descripcion16 = String.Format("{0:n}", Dr["DISPONIBLE_MAR"]);
                                Nodo_Arbol.descripcion17 = String.Format("{0:n}", Dr["DISPONIBLE_ABR"]);
                                Nodo_Arbol.descripcion18 = String.Format("{0:n}", Dr["DISPONIBLE_MAY"]);
                                Nodo_Arbol.descripcion19 = String.Format("{0:n}", Dr["DISPONIBLE_JUN"]);
                                Nodo_Arbol.descripcion20 = String.Format("{0:n}", Dr["DISPONIBLE_JUL"]);
                                Nodo_Arbol.descripcion21 = String.Format("{0:n}", Dr["DISPONIBLE_AGO"]);
                                Nodo_Arbol.descripcion22 = String.Format("{0:n}", Dr["DISPONIBLE_SEP"]);
                                Nodo_Arbol.descripcion23 = String.Format("{0:n}", Dr["DISPONIBLE_OCT"]);
                                Nodo_Arbol.descripcion24 = String.Format("{0:n}", Dr["DISPONIBLE_NOV"]);
                                Nodo_Arbol.descripcion25 = String.Format("{0:n}", Dr["DISPONIBLE_DIC"]);
                                Nodo_Arbol.descripcion26 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_ENE"]);
                                Nodo_Arbol.descripcion27 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_FEB"]);
                                Nodo_Arbol.descripcion28 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_MAR"]);
                                Nodo_Arbol.descripcion29 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_ABR"]);
                                Nodo_Arbol.descripcion30 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_MAY"]);
                                Nodo_Arbol.descripcion31 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_JUN"]);
                                Nodo_Arbol.descripcion32 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_JUL"]);
                                Nodo_Arbol.descripcion33 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_AGO"]);
                                Nodo_Arbol.descripcion34 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_SEP"]);
                                Nodo_Arbol.descripcion35 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_OCT"]);
                                Nodo_Arbol.descripcion36 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_NOV"]);
                                Nodo_Arbol.descripcion37 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_DIC"]);
                                Nodo_Arbol.descripcion38 = String.Format("{0:n}", Dr["COMPROMETIDO_ENE"]);
                                Nodo_Arbol.descripcion39 = String.Format("{0:n}", Dr["COMPROMETIDO_FEB"]);
                                Nodo_Arbol.descripcion40 = String.Format("{0:n}", Dr["COMPROMETIDO_MAR"]);
                                Nodo_Arbol.descripcion41 = String.Format("{0:n}", Dr["COMPROMETIDO_ABR"]);
                                Nodo_Arbol.descripcion42 = String.Format("{0:n}", Dr["COMPROMETIDO_MAY"]);
                                Nodo_Arbol.descripcion43 = String.Format("{0:n}", Dr["COMPROMETIDO_JUN"]);
                                Nodo_Arbol.descripcion44 = String.Format("{0:n}", Dr["COMPROMETIDO_JUL"]);
                                Nodo_Arbol.descripcion45 = String.Format("{0:n}", Dr["COMPROMETIDO_AGO"]);
                                Nodo_Arbol.descripcion46 = String.Format("{0:n}", Dr["COMPROMETIDO_SEP"]);
                                Nodo_Arbol.descripcion47 = String.Format("{0:n}", Dr["COMPROMETIDO_OCT"]);
                                Nodo_Arbol.descripcion48 = String.Format("{0:n}", Dr["COMPROMETIDO_NOV"]);
                                Nodo_Arbol.descripcion49 = String.Format("{0:n}", Dr["COMPROMETIDO_DIC"]);
                                Nodo_Arbol.descripcion50 = String.Format("{0:n}", Dr["DEVENGADO_ENE"]);
                                Nodo_Arbol.descripcion51 = String.Format("{0:n}", Dr["DEVENGADO_FEB"]);
                                Nodo_Arbol.descripcion52 = String.Format("{0:n}", Dr["DEVENGADO_MAR"]);
                                Nodo_Arbol.descripcion53 = String.Format("{0:n}", Dr["DEVENGADO_ABR"]);
                                Nodo_Arbol.descripcion54 = String.Format("{0:n}", Dr["DEVENGADO_MAY"]);
                                Nodo_Arbol.descripcion55 = String.Format("{0:n}", Dr["DEVENGADO_JUN"]);
                                Nodo_Arbol.descripcion56 = String.Format("{0:n}", Dr["DEVENGADO_JUL"]);
                                Nodo_Arbol.descripcion57 = String.Format("{0:n}", Dr["DEVENGADO_AGO"]);
                                Nodo_Arbol.descripcion58 = String.Format("{0:n}", Dr["DEVENGADO_SEP"]);
                                Nodo_Arbol.descripcion59 = String.Format("{0:n}", Dr["DEVENGADO_OCT"]);
                                Nodo_Arbol.descripcion60 = String.Format("{0:n}", Dr["DEVENGADO_NOV"]);
                                Nodo_Arbol.descripcion61 = String.Format("{0:n}", Dr["DEVENGADO_DIC"]);
                                Nodo_Arbol.descripcion62 = String.Format("{0:n}", Dr["EJERCIDO_ENE"]);
                                Nodo_Arbol.descripcion63 = String.Format("{0:n}", Dr["EJERCIDO_FEB"]);
                                Nodo_Arbol.descripcion64 = String.Format("{0:n}", Dr["EJERCIDO_MAR"]);
                                Nodo_Arbol.descripcion65 = String.Format("{0:n}", Dr["EJERCIDO_ABR"]);
                                Nodo_Arbol.descripcion66 = String.Format("{0:n}", Dr["EJERCIDO_MAY"]);
                                Nodo_Arbol.descripcion67 = String.Format("{0:n}", Dr["EJERCIDO_JUN"]);
                                Nodo_Arbol.descripcion68 = String.Format("{0:n}", Dr["EJERCIDO_JUL"]);
                                Nodo_Arbol.descripcion69 = String.Format("{0:n}", Dr["EJERCIDO_AGO"]);
                                Nodo_Arbol.descripcion70 = String.Format("{0:n}", Dr["EJERCIDO_SEP"]);
                                Nodo_Arbol.descripcion71 = String.Format("{0:n}", Dr["EJERCIDO_OCT"]);
                                Nodo_Arbol.descripcion72 = String.Format("{0:n}", Dr["EJERCIDO_NOV"]);
                                Nodo_Arbol.descripcion73 = String.Format("{0:n}", Dr["EJERCIDO_DIC"]);
                                Nodo_Arbol.descripcion74 = String.Format("{0:n}", Dr["PAGADO_ENE"]);
                                Nodo_Arbol.descripcion75 = String.Format("{0:n}", Dr["PAGADO_FEB"]);
                                Nodo_Arbol.descripcion76 = String.Format("{0:n}", Dr["PAGADO_MAR"]);
                                Nodo_Arbol.descripcion77 = String.Format("{0:n}", Dr["PAGADO_ABR"]);
                                Nodo_Arbol.descripcion78 = String.Format("{0:n}", Dr["PAGADO_MAY"]);
                                Nodo_Arbol.descripcion79 = String.Format("{0:n}", Dr["PAGADO_JUN"]);
                                Nodo_Arbol.descripcion80 = String.Format("{0:n}", Dr["PAGADO_JUL"]);
                                Nodo_Arbol.descripcion81 = String.Format("{0:n}", Dr["PAGADO_AGO"]);
                                Nodo_Arbol.descripcion82 = String.Format("{0:n}", Dr["PAGADO_SEP"]);
                                Nodo_Arbol.descripcion83 = String.Format("{0:n}", Dr["PAGADO_OCT"]);
                                Nodo_Arbol.descripcion84 = String.Format("{0:n}", Dr["PAGADO_NOV"]);
                                Nodo_Arbol.descripcion85 = String.Format("{0:n}", Dr["PAGADO_DIC"]);
                                Nodo_Arbol.descripcion86 = String.Format("{0:n}", Dr["IMPORTE_ENE"]);
                                Nodo_Arbol.descripcion87 = String.Format("{0:n}", Dr["IMPORTE_FEB"]);
                                Nodo_Arbol.descripcion88 = String.Format("{0:n}", Dr["IMPORTE_MAR"]);
                                Nodo_Arbol.descripcion89 = String.Format("{0:n}", Dr["IMPORTE_ABR"]);
                                Nodo_Arbol.descripcion90 = String.Format("{0:n}", Dr["IMPORTE_MAY"]);
                                Nodo_Arbol.descripcion91 = String.Format("{0:n}", Dr["IMPORTE_JUN"]);
                                Nodo_Arbol.descripcion92 = String.Format("{0:n}", Dr["IMPORTE_JUL"]);
                                Nodo_Arbol.descripcion93 = String.Format("{0:n}", Dr["IMPORTE_AGO"]);
                                Nodo_Arbol.descripcion94 = String.Format("{0:n}", Dr["IMPORTE_SEP"]);
                                Nodo_Arbol.descripcion95 = String.Format("{0:n}", Dr["IMPORTE_OCT"]);
                                Nodo_Arbol.descripcion96 = String.Format("{0:n}", Dr["IMPORTE_NOV"]);
                                Nodo_Arbol.descripcion97 = String.Format("{0:n}", Dr["IMPORTE_DIC"]);
                                Nodo_Arbol.descripcion98 = String.Format("{0:n}", Dr["IMPORTE_TOTAL"]);

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_Capitulos";
                                Atributos.valor2 = Dr["Capitulo_id"].ToString().Trim();
                                Atributos.valor3 = "";
                                Atributos.valor4 = "";
                                Atributos.valor5 = "";
                                Atributos.valor6 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor7 = Dr["AF_ID"].ToString().Trim();
                                Atributos.valor8 = Dr["PP_ID"].ToString().Trim();
                                Atributos.valor9 = Dr["UR_ID"].ToString().Trim();

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Capitulos = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json = "{\"total\": " + Dt_Capitulos.Rows.Count + ", \"rows\":";
                                Json += Json_Capitulos;
                                Json += ", \"footer\": " + Json_Total + "}";
                            }
                            else
                            {
                                Json = Json_Capitulos;
                            }
                        }
                    }
                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Capitulos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_Conceptos
            ///DESCRIPCIÓN          : Metodo para obtener los conceptos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_Conceptos(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF, String UR, String PP,
                String Cap, String Con, String PG, String P, String UR_F, String PP_F, String Cap_F, String Con_F, String PG_F, String P_F,
                String AF, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json_Conceptos = "{[]}"; //aki almacenaremos el json de los datos
                DataTable Dt_Conceptos = new DataTable(); // dt donde guardaremos los datos
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Total = String.Empty;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Periodo_Inicial = Mes;
                    Negocio.P_Fecha_Inicial = Fecha_Ini;
                    Negocio.P_Fecha_Final = Fecha_Fin;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_DependenciaF_ID = UR_F;
                    Negocio.P_ProgramaF_ID = PP_F;
                    Negocio.P_CapituloF_ID = Cap_F;
                    Negocio.P_ConceptoF_ID = Con_F;
                    Negocio.P_Partida_GenericaF_ID = PG_F;
                    Negocio.P_PartidaF_ID = P_F;
                    Negocio.P_Area_Funcional_ID = AF;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        if (String.IsNullOrEmpty(UR))
                        {
                            Negocio.P_Dependencia_ID = Obtener_Dependencias();
                        }
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = FF_Nomina;
                    }

                    Dt_Conceptos = Negocio.Consultar_Nivel_Concepto();

                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Egr(Dt_Conceptos);
                    }

                    if (Dt_Conceptos != null)
                    {
                        if (Dt_Conceptos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Conceptos.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["id"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["nombre"].ToString().Trim();
                                Nodo_Arbol.state = Estado;
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["APROBADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DISPONIBLE"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["EJERCIDO"]);
                                Nodo_Arbol.descripcion10 = String.Format("{0:n}", Dr["PAGADO"]);
                                Nodo_Arbol.descripcion11 = Dr["FF"].ToString().Trim();
                                Nodo_Arbol.descripcion12 = Dr["UR"].ToString().Trim();
                                Nodo_Arbol.descripcion13 = Dr["PP"].ToString().Trim();
                                Nodo_Arbol.descripcion14 = String.Format("{0:n}", Dr["DISPONIBLE_ENE"]);
                                Nodo_Arbol.descripcion15 = String.Format("{0:n}", Dr["DISPONIBLE_FEB"]);
                                Nodo_Arbol.descripcion16 = String.Format("{0:n}", Dr["DISPONIBLE_MAR"]);
                                Nodo_Arbol.descripcion17 = String.Format("{0:n}", Dr["DISPONIBLE_ABR"]);
                                Nodo_Arbol.descripcion18 = String.Format("{0:n}", Dr["DISPONIBLE_MAY"]);
                                Nodo_Arbol.descripcion19 = String.Format("{0:n}", Dr["DISPONIBLE_JUN"]);
                                Nodo_Arbol.descripcion20 = String.Format("{0:n}", Dr["DISPONIBLE_JUL"]);
                                Nodo_Arbol.descripcion21 = String.Format("{0:n}", Dr["DISPONIBLE_AGO"]);
                                Nodo_Arbol.descripcion22 = String.Format("{0:n}", Dr["DISPONIBLE_SEP"]);
                                Nodo_Arbol.descripcion23 = String.Format("{0:n}", Dr["DISPONIBLE_OCT"]);
                                Nodo_Arbol.descripcion24 = String.Format("{0:n}", Dr["DISPONIBLE_NOV"]);
                                Nodo_Arbol.descripcion25 = String.Format("{0:n}", Dr["DISPONIBLE_DIC"]);
                                Nodo_Arbol.descripcion26 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_ENE"]);
                                Nodo_Arbol.descripcion27 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_FEB"]);
                                Nodo_Arbol.descripcion28 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_MAR"]);
                                Nodo_Arbol.descripcion29 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_ABR"]);
                                Nodo_Arbol.descripcion30 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_MAY"]);
                                Nodo_Arbol.descripcion31 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_JUN"]);
                                Nodo_Arbol.descripcion32 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_JUL"]);
                                Nodo_Arbol.descripcion33 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_AGO"]);
                                Nodo_Arbol.descripcion34 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_SEP"]);
                                Nodo_Arbol.descripcion35 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_OCT"]);
                                Nodo_Arbol.descripcion36 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_NOV"]);
                                Nodo_Arbol.descripcion37 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_DIC"]);
                                Nodo_Arbol.descripcion38 = String.Format("{0:n}", Dr["COMPROMETIDO_ENE"]);
                                Nodo_Arbol.descripcion39 = String.Format("{0:n}", Dr["COMPROMETIDO_FEB"]);
                                Nodo_Arbol.descripcion40 = String.Format("{0:n}", Dr["COMPROMETIDO_MAR"]);
                                Nodo_Arbol.descripcion41 = String.Format("{0:n}", Dr["COMPROMETIDO_ABR"]);
                                Nodo_Arbol.descripcion42 = String.Format("{0:n}", Dr["COMPROMETIDO_MAY"]);
                                Nodo_Arbol.descripcion43 = String.Format("{0:n}", Dr["COMPROMETIDO_JUN"]);
                                Nodo_Arbol.descripcion44 = String.Format("{0:n}", Dr["COMPROMETIDO_JUL"]);
                                Nodo_Arbol.descripcion45 = String.Format("{0:n}", Dr["COMPROMETIDO_AGO"]);
                                Nodo_Arbol.descripcion46 = String.Format("{0:n}", Dr["COMPROMETIDO_SEP"]);
                                Nodo_Arbol.descripcion47 = String.Format("{0:n}", Dr["COMPROMETIDO_OCT"]);
                                Nodo_Arbol.descripcion48 = String.Format("{0:n}", Dr["COMPROMETIDO_NOV"]);
                                Nodo_Arbol.descripcion49 = String.Format("{0:n}", Dr["COMPROMETIDO_DIC"]);
                                Nodo_Arbol.descripcion50 = String.Format("{0:n}", Dr["DEVENGADO_ENE"]);
                                Nodo_Arbol.descripcion51 = String.Format("{0:n}", Dr["DEVENGADO_FEB"]);
                                Nodo_Arbol.descripcion52 = String.Format("{0:n}", Dr["DEVENGADO_MAR"]);
                                Nodo_Arbol.descripcion53 = String.Format("{0:n}", Dr["DEVENGADO_ABR"]);
                                Nodo_Arbol.descripcion54 = String.Format("{0:n}", Dr["DEVENGADO_MAY"]);
                                Nodo_Arbol.descripcion55 = String.Format("{0:n}", Dr["DEVENGADO_JUN"]);
                                Nodo_Arbol.descripcion56 = String.Format("{0:n}", Dr["DEVENGADO_JUL"]);
                                Nodo_Arbol.descripcion57 = String.Format("{0:n}", Dr["DEVENGADO_AGO"]);
                                Nodo_Arbol.descripcion58 = String.Format("{0:n}", Dr["DEVENGADO_SEP"]);
                                Nodo_Arbol.descripcion59 = String.Format("{0:n}", Dr["DEVENGADO_OCT"]);
                                Nodo_Arbol.descripcion60 = String.Format("{0:n}", Dr["DEVENGADO_NOV"]);
                                Nodo_Arbol.descripcion61 = String.Format("{0:n}", Dr["DEVENGADO_DIC"]);
                                Nodo_Arbol.descripcion62 = String.Format("{0:n}", Dr["EJERCIDO_ENE"]);
                                Nodo_Arbol.descripcion63 = String.Format("{0:n}", Dr["EJERCIDO_FEB"]);
                                Nodo_Arbol.descripcion64 = String.Format("{0:n}", Dr["EJERCIDO_MAR"]);
                                Nodo_Arbol.descripcion65 = String.Format("{0:n}", Dr["EJERCIDO_ABR"]);
                                Nodo_Arbol.descripcion66 = String.Format("{0:n}", Dr["EJERCIDO_MAY"]);
                                Nodo_Arbol.descripcion67 = String.Format("{0:n}", Dr["EJERCIDO_JUN"]);
                                Nodo_Arbol.descripcion68 = String.Format("{0:n}", Dr["EJERCIDO_JUL"]);
                                Nodo_Arbol.descripcion69 = String.Format("{0:n}", Dr["EJERCIDO_AGO"]);
                                Nodo_Arbol.descripcion70 = String.Format("{0:n}", Dr["EJERCIDO_SEP"]);
                                Nodo_Arbol.descripcion71 = String.Format("{0:n}", Dr["EJERCIDO_OCT"]);
                                Nodo_Arbol.descripcion72 = String.Format("{0:n}", Dr["EJERCIDO_NOV"]);
                                Nodo_Arbol.descripcion73 = String.Format("{0:n}", Dr["EJERCIDO_DIC"]);
                                Nodo_Arbol.descripcion74 = String.Format("{0:n}", Dr["PAGADO_ENE"]);
                                Nodo_Arbol.descripcion75 = String.Format("{0:n}", Dr["PAGADO_FEB"]);
                                Nodo_Arbol.descripcion76 = String.Format("{0:n}", Dr["PAGADO_MAR"]);
                                Nodo_Arbol.descripcion77 = String.Format("{0:n}", Dr["PAGADO_ABR"]);
                                Nodo_Arbol.descripcion78 = String.Format("{0:n}", Dr["PAGADO_MAY"]);
                                Nodo_Arbol.descripcion79 = String.Format("{0:n}", Dr["PAGADO_JUN"]);
                                Nodo_Arbol.descripcion80 = String.Format("{0:n}", Dr["PAGADO_JUL"]);
                                Nodo_Arbol.descripcion81 = String.Format("{0:n}", Dr["PAGADO_AGO"]);
                                Nodo_Arbol.descripcion82 = String.Format("{0:n}", Dr["PAGADO_SEP"]);
                                Nodo_Arbol.descripcion83 = String.Format("{0:n}", Dr["PAGADO_OCT"]);
                                Nodo_Arbol.descripcion84 = String.Format("{0:n}", Dr["PAGADO_NOV"]);
                                Nodo_Arbol.descripcion85 = String.Format("{0:n}", Dr["PAGADO_DIC"]);
                                Nodo_Arbol.descripcion86 = String.Format("{0:n}", Dr["IMPORTE_ENE"]);
                                Nodo_Arbol.descripcion87 = String.Format("{0:n}", Dr["IMPORTE_FEB"]);
                                Nodo_Arbol.descripcion88 = String.Format("{0:n}", Dr["IMPORTE_MAR"]);
                                Nodo_Arbol.descripcion89 = String.Format("{0:n}", Dr["IMPORTE_ABR"]);
                                Nodo_Arbol.descripcion90 = String.Format("{0:n}", Dr["IMPORTE_MAY"]);
                                Nodo_Arbol.descripcion91 = String.Format("{0:n}", Dr["IMPORTE_JUN"]);
                                Nodo_Arbol.descripcion92 = String.Format("{0:n}", Dr["IMPORTE_JUL"]);
                                Nodo_Arbol.descripcion93 = String.Format("{0:n}", Dr["IMPORTE_AGO"]);
                                Nodo_Arbol.descripcion94 = String.Format("{0:n}", Dr["IMPORTE_SEP"]);
                                Nodo_Arbol.descripcion95 = String.Format("{0:n}", Dr["IMPORTE_OCT"]);
                                Nodo_Arbol.descripcion96 = String.Format("{0:n}", Dr["IMPORTE_NOV"]);
                                Nodo_Arbol.descripcion97 = String.Format("{0:n}", Dr["IMPORTE_DIC"]);
                                Nodo_Arbol.descripcion98 = String.Format("{0:n}", Dr["IMPORTE_TOTAL"]);

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_Concepto";
                                Atributos.valor2 = Dr["Capitulo_id"].ToString().Trim();
                                Atributos.valor3 = Dr["Concepto_id"].ToString().Trim();
                                Atributos.valor4 = "";
                                Atributos.valor5 = "";
                                Atributos.valor6 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor7 = Dr["AF_ID"].ToString().Trim();
                                Atributos.valor8 = Dr["PP_ID"].ToString().Trim();
                                Atributos.valor9 = Dr["UR_ID"].ToString().Trim();

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Conceptos = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);


                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt_Conceptos.Rows.Count + ", \"rows\":";
                                Json_Tot += Json_Conceptos;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json_Conceptos = Json_Tot;
                            }
                        }
                    }
                    return Json_Conceptos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Conceptos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_Partida_Generica
            ///DESCRIPCIÓN          : Metodo para obtener las partidas genericas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_Partida_Generica(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF, String UR,
                String PP, String Cap, String Con, String PG, String P, String UR_F, String PP_F, String Cap_F, String Con_F, String PG_F,
                String P_F, String AF, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json_Partida_Generica = "{[]}"; //aki almacenaremos el json de las Partidas Genericas
                DataTable Dt_Partida_Generica = new DataTable(); // dt donde guardaremos las Partidas Genericas
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Total = String.Empty;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Periodo_Inicial = Mes;
                    Negocio.P_Fecha_Inicial = Fecha_Ini;
                    Negocio.P_Fecha_Final = Fecha_Fin;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_DependenciaF_ID = UR_F;
                    Negocio.P_ProgramaF_ID = PP_F;
                    Negocio.P_CapituloF_ID = Cap_F;
                    Negocio.P_ConceptoF_ID = Con_F;
                    Negocio.P_Partida_GenericaF_ID = PG_F;
                    Negocio.P_PartidaF_ID = P_F;
                    Negocio.P_Area_Funcional_ID = AF;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        if (String.IsNullOrEmpty(UR))
                        {
                            Negocio.P_Dependencia_ID = Obtener_Dependencias();
                        }
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = FF_Nomina;
                    }

                    Dt_Partida_Generica = Negocio.Consultar_Nivel_Partida_Gen();

                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Egr(Dt_Partida_Generica);
                    }

                    if (Dt_Partida_Generica != null)
                    {
                        if (Dt_Partida_Generica.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Partida_Generica.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["id"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["nombre"].ToString().Trim();
                                Nodo_Arbol.state = Estado;
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["APROBADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DISPONIBLE"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["EJERCIDO"]);
                                Nodo_Arbol.descripcion10 = String.Format("{0:n}", Dr["PAGADO"]);
                                Nodo_Arbol.descripcion11 = Dr["FF"].ToString().Trim();
                                Nodo_Arbol.descripcion12 = Dr["UR"].ToString().Trim();
                                Nodo_Arbol.descripcion13 = Dr["PP"].ToString().Trim();
                                Nodo_Arbol.descripcion14 = String.Format("{0:n}", Dr["DISPONIBLE_ENE"]);
                                Nodo_Arbol.descripcion15 = String.Format("{0:n}", Dr["DISPONIBLE_FEB"]);
                                Nodo_Arbol.descripcion16 = String.Format("{0:n}", Dr["DISPONIBLE_MAR"]);
                                Nodo_Arbol.descripcion17 = String.Format("{0:n}", Dr["DISPONIBLE_ABR"]);
                                Nodo_Arbol.descripcion18 = String.Format("{0:n}", Dr["DISPONIBLE_MAY"]);
                                Nodo_Arbol.descripcion19 = String.Format("{0:n}", Dr["DISPONIBLE_JUN"]);
                                Nodo_Arbol.descripcion20 = String.Format("{0:n}", Dr["DISPONIBLE_JUL"]);
                                Nodo_Arbol.descripcion21 = String.Format("{0:n}", Dr["DISPONIBLE_AGO"]);
                                Nodo_Arbol.descripcion22 = String.Format("{0:n}", Dr["DISPONIBLE_SEP"]);
                                Nodo_Arbol.descripcion23 = String.Format("{0:n}", Dr["DISPONIBLE_OCT"]);
                                Nodo_Arbol.descripcion24 = String.Format("{0:n}", Dr["DISPONIBLE_NOV"]);
                                Nodo_Arbol.descripcion25 = String.Format("{0:n}", Dr["DISPONIBLE_DIC"]);
                                Nodo_Arbol.descripcion26 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_ENE"]);
                                Nodo_Arbol.descripcion27 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_FEB"]);
                                Nodo_Arbol.descripcion28 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_MAR"]);
                                Nodo_Arbol.descripcion29 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_ABR"]);
                                Nodo_Arbol.descripcion30 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_MAY"]);
                                Nodo_Arbol.descripcion31 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_JUN"]);
                                Nodo_Arbol.descripcion32 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_JUL"]);
                                Nodo_Arbol.descripcion33 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_AGO"]);
                                Nodo_Arbol.descripcion34 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_SEP"]);
                                Nodo_Arbol.descripcion35 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_OCT"]);
                                Nodo_Arbol.descripcion36 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_NOV"]);
                                Nodo_Arbol.descripcion37 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_DIC"]);
                                Nodo_Arbol.descripcion38 = String.Format("{0:n}", Dr["COMPROMETIDO_ENE"]);
                                Nodo_Arbol.descripcion39 = String.Format("{0:n}", Dr["COMPROMETIDO_FEB"]);
                                Nodo_Arbol.descripcion40 = String.Format("{0:n}", Dr["COMPROMETIDO_MAR"]);
                                Nodo_Arbol.descripcion41 = String.Format("{0:n}", Dr["COMPROMETIDO_ABR"]);
                                Nodo_Arbol.descripcion42 = String.Format("{0:n}", Dr["COMPROMETIDO_MAY"]);
                                Nodo_Arbol.descripcion43 = String.Format("{0:n}", Dr["COMPROMETIDO_JUN"]);
                                Nodo_Arbol.descripcion44 = String.Format("{0:n}", Dr["COMPROMETIDO_JUL"]);
                                Nodo_Arbol.descripcion45 = String.Format("{0:n}", Dr["COMPROMETIDO_AGO"]);
                                Nodo_Arbol.descripcion46 = String.Format("{0:n}", Dr["COMPROMETIDO_SEP"]);
                                Nodo_Arbol.descripcion47 = String.Format("{0:n}", Dr["COMPROMETIDO_OCT"]);
                                Nodo_Arbol.descripcion48 = String.Format("{0:n}", Dr["COMPROMETIDO_NOV"]);
                                Nodo_Arbol.descripcion49 = String.Format("{0:n}", Dr["COMPROMETIDO_DIC"]);
                                Nodo_Arbol.descripcion50 = String.Format("{0:n}", Dr["DEVENGADO_ENE"]);
                                Nodo_Arbol.descripcion51 = String.Format("{0:n}", Dr["DEVENGADO_FEB"]);
                                Nodo_Arbol.descripcion52 = String.Format("{0:n}", Dr["DEVENGADO_MAR"]);
                                Nodo_Arbol.descripcion53 = String.Format("{0:n}", Dr["DEVENGADO_ABR"]);
                                Nodo_Arbol.descripcion54 = String.Format("{0:n}", Dr["DEVENGADO_MAY"]);
                                Nodo_Arbol.descripcion55 = String.Format("{0:n}", Dr["DEVENGADO_JUN"]);
                                Nodo_Arbol.descripcion56 = String.Format("{0:n}", Dr["DEVENGADO_JUL"]);
                                Nodo_Arbol.descripcion57 = String.Format("{0:n}", Dr["DEVENGADO_AGO"]);
                                Nodo_Arbol.descripcion58 = String.Format("{0:n}", Dr["DEVENGADO_SEP"]);
                                Nodo_Arbol.descripcion59 = String.Format("{0:n}", Dr["DEVENGADO_OCT"]);
                                Nodo_Arbol.descripcion60 = String.Format("{0:n}", Dr["DEVENGADO_NOV"]);
                                Nodo_Arbol.descripcion61 = String.Format("{0:n}", Dr["DEVENGADO_DIC"]);
                                Nodo_Arbol.descripcion62 = String.Format("{0:n}", Dr["EJERCIDO_ENE"]);
                                Nodo_Arbol.descripcion63 = String.Format("{0:n}", Dr["EJERCIDO_FEB"]);
                                Nodo_Arbol.descripcion64 = String.Format("{0:n}", Dr["EJERCIDO_MAR"]);
                                Nodo_Arbol.descripcion65 = String.Format("{0:n}", Dr["EJERCIDO_ABR"]);
                                Nodo_Arbol.descripcion66 = String.Format("{0:n}", Dr["EJERCIDO_MAY"]);
                                Nodo_Arbol.descripcion67 = String.Format("{0:n}", Dr["EJERCIDO_JUN"]);
                                Nodo_Arbol.descripcion68 = String.Format("{0:n}", Dr["EJERCIDO_JUL"]);
                                Nodo_Arbol.descripcion69 = String.Format("{0:n}", Dr["EJERCIDO_AGO"]);
                                Nodo_Arbol.descripcion70 = String.Format("{0:n}", Dr["EJERCIDO_SEP"]);
                                Nodo_Arbol.descripcion71 = String.Format("{0:n}", Dr["EJERCIDO_OCT"]);
                                Nodo_Arbol.descripcion72 = String.Format("{0:n}", Dr["EJERCIDO_NOV"]);
                                Nodo_Arbol.descripcion73 = String.Format("{0:n}", Dr["EJERCIDO_DIC"]);
                                Nodo_Arbol.descripcion74 = String.Format("{0:n}", Dr["PAGADO_ENE"]);
                                Nodo_Arbol.descripcion75 = String.Format("{0:n}", Dr["PAGADO_FEB"]);
                                Nodo_Arbol.descripcion76 = String.Format("{0:n}", Dr["PAGADO_MAR"]);
                                Nodo_Arbol.descripcion77 = String.Format("{0:n}", Dr["PAGADO_ABR"]);
                                Nodo_Arbol.descripcion78 = String.Format("{0:n}", Dr["PAGADO_MAY"]);
                                Nodo_Arbol.descripcion79 = String.Format("{0:n}", Dr["PAGADO_JUN"]);
                                Nodo_Arbol.descripcion80 = String.Format("{0:n}", Dr["PAGADO_JUL"]);
                                Nodo_Arbol.descripcion81 = String.Format("{0:n}", Dr["PAGADO_AGO"]);
                                Nodo_Arbol.descripcion82 = String.Format("{0:n}", Dr["PAGADO_SEP"]);
                                Nodo_Arbol.descripcion83 = String.Format("{0:n}", Dr["PAGADO_OCT"]);
                                Nodo_Arbol.descripcion84 = String.Format("{0:n}", Dr["PAGADO_NOV"]);
                                Nodo_Arbol.descripcion85 = String.Format("{0:n}", Dr["PAGADO_DIC"]);
                                Nodo_Arbol.descripcion86 = String.Format("{0:n}", Dr["IMPORTE_ENE"]);
                                Nodo_Arbol.descripcion87 = String.Format("{0:n}", Dr["IMPORTE_FEB"]);
                                Nodo_Arbol.descripcion88 = String.Format("{0:n}", Dr["IMPORTE_MAR"]);
                                Nodo_Arbol.descripcion89 = String.Format("{0:n}", Dr["IMPORTE_ABR"]);
                                Nodo_Arbol.descripcion90 = String.Format("{0:n}", Dr["IMPORTE_MAY"]);
                                Nodo_Arbol.descripcion91 = String.Format("{0:n}", Dr["IMPORTE_JUN"]);
                                Nodo_Arbol.descripcion92 = String.Format("{0:n}", Dr["IMPORTE_JUL"]);
                                Nodo_Arbol.descripcion93 = String.Format("{0:n}", Dr["IMPORTE_AGO"]);
                                Nodo_Arbol.descripcion94 = String.Format("{0:n}", Dr["IMPORTE_SEP"]);
                                Nodo_Arbol.descripcion95 = String.Format("{0:n}", Dr["IMPORTE_OCT"]);
                                Nodo_Arbol.descripcion96 = String.Format("{0:n}", Dr["IMPORTE_NOV"]);
                                Nodo_Arbol.descripcion97 = String.Format("{0:n}", Dr["IMPORTE_DIC"]);
                                Nodo_Arbol.descripcion98 = String.Format("{0:n}", Dr["IMPORTE_TOTAL"]);

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_Partida_Gen";
                                Atributos.valor2 = Dr["Capitulo_id"].ToString().Trim();
                                Atributos.valor3 = Dr["Concepto_id"].ToString().Trim();
                                Atributos.valor4 = Dr["Partida_Generica_id"].ToString().Trim();
                                Atributos.valor5 = "";
                                Atributos.valor6 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor7 = Dr["AF_ID"].ToString().Trim();
                                Atributos.valor8 = Dr["PP_ID"].ToString().Trim();
                                Atributos.valor9 = Dr["UR_ID"].ToString().Trim();

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Partida_Generica = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt_Partida_Generica.Rows.Count + ", \"rows\":";
                                Json_Tot += Json_Partida_Generica;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json_Partida_Generica = Json_Tot;
                            }
                        }
                    }
                    return Json_Partida_Generica;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Partida_Generica Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Partida
            ///DESCRIPCIÓN          : Metodo para obtener las partidas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_Partida(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF, String UR, String PP,
                String Cap, String Con, String PG, String P, String UR_F, String PP_F, String Cap_F, String Con_F, String PG_F, String P_F,
                String AF, String Estado, String Tot)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json_Partida = "{[]}"; //aki almacenaremos el json de las partidas
                DataTable Dt_Partida = new DataTable(); // dt donde guardaremos las partidas
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Total = String.Empty;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Periodo_Inicial = Mes;
                    Negocio.P_Fecha_Inicial = Fecha_Ini;
                    Negocio.P_Fecha_Final = Fecha_Fin;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_DependenciaF_ID = UR_F;
                    Negocio.P_ProgramaF_ID = PP_F;
                    Negocio.P_CapituloF_ID = Cap_F;
                    Negocio.P_ConceptoF_ID = Con_F;
                    Negocio.P_Partida_GenericaF_ID = PG_F;
                    Negocio.P_PartidaF_ID = P_F;
                    Negocio.P_Area_Funcional_ID = AF;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        if (String.IsNullOrEmpty(UR))
                        {
                            Negocio.P_Dependencia_ID = Obtener_Dependencias();
                        }
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = FF_Nomina;
                    }

                    Dt_Partida = Negocio.Consultar_Nivel_Partidas();
                    if (!String.IsNullOrEmpty(Tot))
                    {
                        Json_Total = Obtener_Total_Egr(Dt_Partida);
                    }

                    if (Dt_Partida != null)
                    {
                        if (Dt_Partida.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Partida.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["id"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["nombre"].ToString().Trim();
                                Nodo_Arbol.descripcion1 = String.Format("{0:n}", Dr["APROBADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:n}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:n}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:n}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:n}", Dr["DISPONIBLE"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO"]);
                                Nodo_Arbol.descripcion7 = String.Format("{0:n}", Dr["COMPROMETIDO"]);
                                Nodo_Arbol.descripcion8 = String.Format("{0:n}", Dr["DEVENGADO"]);
                                Nodo_Arbol.descripcion9 = String.Format("{0:n}", Dr["EJERCIDO"]);
                                Nodo_Arbol.descripcion10 = String.Format("{0:n}", Dr["PAGADO"]);
                                Nodo_Arbol.descripcion11 = Dr["FF"].ToString().Trim();
                                Nodo_Arbol.descripcion12 = Dr["UR"].ToString().Trim();
                                Nodo_Arbol.descripcion13 = Dr["PP"].ToString().Trim();
                                Nodo_Arbol.descripcion14 = String.Format("{0:n}", Dr["DISPONIBLE_ENE"]);
                                Nodo_Arbol.descripcion15 = String.Format("{0:n}", Dr["DISPONIBLE_FEB"]);
                                Nodo_Arbol.descripcion16 = String.Format("{0:n}", Dr["DISPONIBLE_MAR"]);
                                Nodo_Arbol.descripcion17 = String.Format("{0:n}", Dr["DISPONIBLE_ABR"]);
                                Nodo_Arbol.descripcion18 = String.Format("{0:n}", Dr["DISPONIBLE_MAY"]);
                                Nodo_Arbol.descripcion19 = String.Format("{0:n}", Dr["DISPONIBLE_JUN"]);
                                Nodo_Arbol.descripcion20 = String.Format("{0:n}", Dr["DISPONIBLE_JUL"]);
                                Nodo_Arbol.descripcion21 = String.Format("{0:n}", Dr["DISPONIBLE_AGO"]);
                                Nodo_Arbol.descripcion22 = String.Format("{0:n}", Dr["DISPONIBLE_SEP"]);
                                Nodo_Arbol.descripcion23 = String.Format("{0:n}", Dr["DISPONIBLE_OCT"]);
                                Nodo_Arbol.descripcion24 = String.Format("{0:n}", Dr["DISPONIBLE_NOV"]);
                                Nodo_Arbol.descripcion25 = String.Format("{0:n}", Dr["DISPONIBLE_DIC"]);
                                Nodo_Arbol.descripcion26 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_ENE"]);
                                Nodo_Arbol.descripcion27 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_FEB"]);
                                Nodo_Arbol.descripcion28 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_MAR"]);
                                Nodo_Arbol.descripcion29 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_ABR"]);
                                Nodo_Arbol.descripcion30 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_MAY"]);
                                Nodo_Arbol.descripcion31 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_JUN"]);
                                Nodo_Arbol.descripcion32 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_JUL"]);
                                Nodo_Arbol.descripcion33 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_AGO"]);
                                Nodo_Arbol.descripcion34 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_SEP"]);
                                Nodo_Arbol.descripcion35 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_OCT"]);
                                Nodo_Arbol.descripcion36 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_NOV"]);
                                Nodo_Arbol.descripcion37 = String.Format("{0:n}", Dr["PRE_COMPROMETIDO_DIC"]);
                                Nodo_Arbol.descripcion38 = String.Format("{0:n}", Dr["COMPROMETIDO_ENE"]);
                                Nodo_Arbol.descripcion39 = String.Format("{0:n}", Dr["COMPROMETIDO_FEB"]);
                                Nodo_Arbol.descripcion40 = String.Format("{0:n}", Dr["COMPROMETIDO_MAR"]);
                                Nodo_Arbol.descripcion41 = String.Format("{0:n}", Dr["COMPROMETIDO_ABR"]);
                                Nodo_Arbol.descripcion42 = String.Format("{0:n}", Dr["COMPROMETIDO_MAY"]);
                                Nodo_Arbol.descripcion43 = String.Format("{0:n}", Dr["COMPROMETIDO_JUN"]);
                                Nodo_Arbol.descripcion44 = String.Format("{0:n}", Dr["COMPROMETIDO_JUL"]);
                                Nodo_Arbol.descripcion45 = String.Format("{0:n}", Dr["COMPROMETIDO_AGO"]);
                                Nodo_Arbol.descripcion46 = String.Format("{0:n}", Dr["COMPROMETIDO_SEP"]);
                                Nodo_Arbol.descripcion47 = String.Format("{0:n}", Dr["COMPROMETIDO_OCT"]);
                                Nodo_Arbol.descripcion48 = String.Format("{0:n}", Dr["COMPROMETIDO_NOV"]);
                                Nodo_Arbol.descripcion49 = String.Format("{0:n}", Dr["COMPROMETIDO_DIC"]);
                                Nodo_Arbol.descripcion50 = String.Format("{0:n}", Dr["DEVENGADO_ENE"]);
                                Nodo_Arbol.descripcion51 = String.Format("{0:n}", Dr["DEVENGADO_FEB"]);
                                Nodo_Arbol.descripcion52 = String.Format("{0:n}", Dr["DEVENGADO_MAR"]);
                                Nodo_Arbol.descripcion53 = String.Format("{0:n}", Dr["DEVENGADO_ABR"]);
                                Nodo_Arbol.descripcion54 = String.Format("{0:n}", Dr["DEVENGADO_MAY"]);
                                Nodo_Arbol.descripcion55 = String.Format("{0:n}", Dr["DEVENGADO_JUN"]);
                                Nodo_Arbol.descripcion56 = String.Format("{0:n}", Dr["DEVENGADO_JUL"]);
                                Nodo_Arbol.descripcion57 = String.Format("{0:n}", Dr["DEVENGADO_AGO"]);
                                Nodo_Arbol.descripcion58 = String.Format("{0:n}", Dr["DEVENGADO_SEP"]);
                                Nodo_Arbol.descripcion59 = String.Format("{0:n}", Dr["DEVENGADO_OCT"]);
                                Nodo_Arbol.descripcion60 = String.Format("{0:n}", Dr["DEVENGADO_NOV"]);
                                Nodo_Arbol.descripcion61 = String.Format("{0:n}", Dr["DEVENGADO_DIC"]);
                                Nodo_Arbol.descripcion62 = String.Format("{0:n}", Dr["EJERCIDO_ENE"]);
                                Nodo_Arbol.descripcion63 = String.Format("{0:n}", Dr["EJERCIDO_FEB"]);
                                Nodo_Arbol.descripcion64 = String.Format("{0:n}", Dr["EJERCIDO_MAR"]);
                                Nodo_Arbol.descripcion65 = String.Format("{0:n}", Dr["EJERCIDO_ABR"]);
                                Nodo_Arbol.descripcion66 = String.Format("{0:n}", Dr["EJERCIDO_MAY"]);
                                Nodo_Arbol.descripcion67 = String.Format("{0:n}", Dr["EJERCIDO_JUN"]);
                                Nodo_Arbol.descripcion68 = String.Format("{0:n}", Dr["EJERCIDO_JUL"]);
                                Nodo_Arbol.descripcion69 = String.Format("{0:n}", Dr["EJERCIDO_AGO"]);
                                Nodo_Arbol.descripcion70 = String.Format("{0:n}", Dr["EJERCIDO_SEP"]);
                                Nodo_Arbol.descripcion71 = String.Format("{0:n}", Dr["EJERCIDO_OCT"]);
                                Nodo_Arbol.descripcion72 = String.Format("{0:n}", Dr["EJERCIDO_NOV"]);
                                Nodo_Arbol.descripcion73 = String.Format("{0:n}", Dr["EJERCIDO_DIC"]);
                                Nodo_Arbol.descripcion74 = String.Format("{0:n}", Dr["PAGADO_ENE"]);
                                Nodo_Arbol.descripcion75 = String.Format("{0:n}", Dr["PAGADO_FEB"]);
                                Nodo_Arbol.descripcion76 = String.Format("{0:n}", Dr["PAGADO_MAR"]);
                                Nodo_Arbol.descripcion77 = String.Format("{0:n}", Dr["PAGADO_ABR"]);
                                Nodo_Arbol.descripcion78 = String.Format("{0:n}", Dr["PAGADO_MAY"]);
                                Nodo_Arbol.descripcion79 = String.Format("{0:n}", Dr["PAGADO_JUN"]);
                                Nodo_Arbol.descripcion80 = String.Format("{0:n}", Dr["PAGADO_JUL"]);
                                Nodo_Arbol.descripcion81 = String.Format("{0:n}", Dr["PAGADO_AGO"]);
                                Nodo_Arbol.descripcion82 = String.Format("{0:n}", Dr["PAGADO_SEP"]);
                                Nodo_Arbol.descripcion83 = String.Format("{0:n}", Dr["PAGADO_OCT"]);
                                Nodo_Arbol.descripcion84 = String.Format("{0:n}", Dr["PAGADO_NOV"]);
                                Nodo_Arbol.descripcion85 = String.Format("{0:n}", Dr["PAGADO_DIC"]);
                                Nodo_Arbol.descripcion86 = String.Format("{0:n}", Dr["IMPORTE_ENE"]);
                                Nodo_Arbol.descripcion87 = String.Format("{0:n}", Dr["IMPORTE_FEB"]);
                                Nodo_Arbol.descripcion88 = String.Format("{0:n}", Dr["IMPORTE_MAR"]);
                                Nodo_Arbol.descripcion89 = String.Format("{0:n}", Dr["IMPORTE_ABR"]);
                                Nodo_Arbol.descripcion90 = String.Format("{0:n}", Dr["IMPORTE_MAY"]);
                                Nodo_Arbol.descripcion91 = String.Format("{0:n}", Dr["IMPORTE_JUN"]);
                                Nodo_Arbol.descripcion92 = String.Format("{0:n}", Dr["IMPORTE_JUL"]);
                                Nodo_Arbol.descripcion93 = String.Format("{0:n}", Dr["IMPORTE_AGO"]);
                                Nodo_Arbol.descripcion94 = String.Format("{0:n}", Dr["IMPORTE_SEP"]);
                                Nodo_Arbol.descripcion95 = String.Format("{0:n}", Dr["IMPORTE_OCT"]);
                                Nodo_Arbol.descripcion96 = String.Format("{0:n}", Dr["IMPORTE_NOV"]);
                                Nodo_Arbol.descripcion97 = String.Format("{0:n}", Dr["IMPORTE_DIC"]);
                                Nodo_Arbol.descripcion98 = String.Format("{0:n}", Dr["IMPORTE_TOTAL"]);

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_Partida";
                                Atributos.valor2 = Dr["Capitulo_id"].ToString().Trim();
                                Atributos.valor3 = Dr["Concepto_id"].ToString().Trim();
                                Atributos.valor4 = Dr["Partida_Generica_id"].ToString().Trim();
                                Atributos.valor5 = Dr["Partida_id"].ToString().Trim();
                                Atributos.valor6 = Dr["FF_ID"].ToString().Trim();
                                Atributos.valor7 = Dr["AF_ID"].ToString().Trim();
                                Atributos.valor8 = Dr["PP_ID"].ToString().Trim();
                                Atributos.valor9 = Dr["UR_ID"].ToString().Trim();

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Partida = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json_Tot = "{\"total\": " + Dt_Partida.Rows.Count + ", \"rows\":";
                                Json_Tot += Json_Partida;
                                Json_Tot += ", \"footer\": " + Json_Total + "}";
                                Json_Partida = Json_Tot;
                            }
                        }
                    }
                    return Json_Partida;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Partida Error[" + ex.Message + "]");
                }
            }

            //********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Total_Egr
            ///DESCRIPCIÓN          : Metodo para obtener el total
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Total_Egr(DataTable Dt_Datos)
            {
                #region (Variables)
                    Double Aprobado = 0.00;
                    Double Ampliacion = 0.00;
                    Double Reduccion = 0.00;
                    Double Modificado = 0.00;
                    Double Devengado = 0.00;
                    Double Pagado = 0.00;
                    Double Ejercido = 0.00;
                    Double Disponible = 0.00;
                    Double Comprometido = 0.00;
                    Double Pre_Comprometido = 0.00;
                    Double Devengado_Ene = 0.00;
                    Double Pagado_Ene = 0.00;
                    Double Ejercido_Ene = 0.00;
                    Double Disponible_Ene = 0.00;
                    Double Comprometido_Ene = 0.00;
                    Double Pre_Comprometido_Ene = 0.00;
                    Double Devengado_Feb = 0.00;
                    Double Pagado_Feb = 0.00;
                    Double Ejercido_Feb = 0.00;
                    Double Disponible_Feb = 0.00;
                    Double Comprometido_Feb = 0.00;
                    Double Pre_Comprometido_Feb = 0.00;
                    Double Devengado_Mar = 0.00;
                    Double Pagado_Mar = 0.00;
                    Double Ejercido_Mar = 0.00;
                    Double Disponible_Mar = 0.00;
                    Double Comprometido_Mar = 0.00;
                    Double Pre_Comprometido_Mar = 0.00;
                    Double Devengado_Abr = 0.00;
                    Double Pagado_Abr = 0.00;
                    Double Ejercido_Abr = 0.00;
                    Double Disponible_Abr = 0.00;
                    Double Comprometido_Abr = 0.00;
                    Double Pre_Comprometido_Abr = 0.00;
                    Double Devengado_May = 0.00;
                    Double Pagado_May = 0.00;
                    Double Ejercido_May = 0.00;
                    Double Disponible_May = 0.00;
                    Double Comprometido_May = 0.00;
                    Double Pre_Comprometido_May = 0.00;
                    Double Devengado_Jun = 0.00;
                    Double Pagado_Jun = 0.00;
                    Double Ejercido_Jun = 0.00;
                    Double Disponible_Jun = 0.00;
                    Double Comprometido_Jun = 0.00;
                    Double Pre_Comprometido_Jun = 0.00;
                    Double Devengado_Jul = 0.00;
                    Double Pagado_Jul = 0.00;
                    Double Ejercido_Jul = 0.00;
                    Double Disponible_Jul = 0.00;
                    Double Comprometido_Jul = 0.00;
                    Double Pre_Comprometido_Jul = 0.00;
                    Double Devengado_Ago = 0.00;
                    Double Pagado_Ago = 0.00;
                    Double Ejercido_Ago = 0.00;
                    Double Disponible_Ago = 0.00;
                    Double Comprometido_Ago = 0.00;
                    Double Pre_Comprometido_Ago = 0.00;
                    Double Devengado_Sep = 0.00;
                    Double Pagado_Sep = 0.00;
                    Double Ejercido_Sep = 0.00;
                    Double Disponible_Sep = 0.00;
                    Double Comprometido_Sep = 0.00;
                    Double Pre_Comprometido_Sep = 0.00;
                    Double Devengado_Oct = 0.00;
                    Double Pagado_Oct = 0.00;
                    Double Ejercido_Oct = 0.00;
                    Double Disponible_Oct = 0.00;
                    Double Comprometido_Oct = 0.00;
                    Double Pre_Comprometido_Oct = 0.00;
                    Double Devengado_Nov = 0.00;
                    Double Pagado_Nov = 0.00;
                    Double Ejercido_Nov = 0.00;
                    Double Disponible_Nov = 0.00;
                    Double Comprometido_Nov = 0.00;
                    Double Pre_Comprometido_Nov = 0.00;
                    Double Devengado_Dic = 0.00;
                    Double Pagado_Dic = 0.00;
                    Double Ejercido_Dic = 0.00;
                    Double Disponible_Dic = 0.00;
                    Double Comprometido_Dic = 0.00;
                    Double Pre_Comprometido_Dic = 0.00;

                    Double Importe_Ene = 0.00;
                    Double Importe_Feb = 0.00;
                    Double Importe_Mar = 0.00;
                    Double Importe_Abr = 0.00;
                    Double Importe_May = 0.00;
                    Double Importe_Jun = 0.00;
                    Double Importe_Jul = 0.00;
                    Double Importe_Ago = 0.00;
                    Double Importe_Sep = 0.00;
                    Double Importe_Oct = 0.00;
                    Double Importe_Nov = 0.00;
                    Double Importe_Dic = 0.00;
                    Double Importe_Tot = 0.00;
                #endregion

                String Json_Total = string.Empty;
                Json_Total = "{[]}";
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;

                try
                {
                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            
                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {
                                Aprobado += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                Ampliacion += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                Reduccion += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                Modificado += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                Devengado += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                Pagado += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                Ejercido += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());
                                Disponible += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                Comprometido += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                Pre_Comprometido += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                Devengado_Ene += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_ENE"].ToString().Trim()) ? "0" : Dr["DEVENGADO_ENE"].ToString().Trim());
                                Pagado_Ene += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO_ENE"].ToString().Trim()) ? "0" : Dr["PAGADO_ENE"].ToString().Trim());
                                Ejercido_Ene += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO_ENE"].ToString().Trim()) ? "0" : Dr["EJERCIDO_ENE"].ToString().Trim());
                                Disponible_Ene += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_ENE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_ENE"].ToString().Trim());
                                Comprometido_Ene += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_ENE"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_ENE"].ToString().Trim());
                                Pre_Comprometido_Ene += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_ENE"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_ENE"].ToString().Trim());
                                Devengado_Feb += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_FEB"].ToString().Trim()) ? "0" : Dr["DEVENGADO_FEB"].ToString().Trim());
                                Pagado_Feb += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO_FEB"].ToString().Trim()) ? "0" : Dr["PAGADO_FEB"].ToString().Trim());
                                Ejercido_Feb += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO_FEB"].ToString().Trim()) ? "0" : Dr["EJERCIDO_FEB"].ToString().Trim());
                                Disponible_Feb += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_FEB"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_FEB"].ToString().Trim());
                                Comprometido_Feb += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_FEB"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_FEB"].ToString().Trim());
                                Pre_Comprometido_Feb += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_FEB"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_FEB"].ToString().Trim());
                                Devengado_Mar += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_MAR"].ToString().Trim()) ? "0" : Dr["DEVENGADO_MAR"].ToString().Trim());
                                Pagado_Mar += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO_MAR"].ToString().Trim()) ? "0" : Dr["PAGADO_MAR"].ToString().Trim());
                                Ejercido_Mar += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO_MAR"].ToString().Trim()) ? "0" : Dr["EJERCIDO_MAR"].ToString().Trim());
                                Disponible_Mar += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_MAR"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_MAR"].ToString().Trim());
                                Comprometido_Mar += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_MAR"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_MAR"].ToString().Trim());
                                Pre_Comprometido_Mar += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_MAR"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_MAR"].ToString().Trim());
                                Devengado_Abr += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_ABR"].ToString().Trim()) ? "0" : Dr["DEVENGADO_ABR"].ToString().Trim());
                                Pagado_Abr += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO_ABR"].ToString().Trim()) ? "0" : Dr["PAGADO_ABR"].ToString().Trim());
                                Ejercido_Abr += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO_ABR"].ToString().Trim()) ? "0" : Dr["EJERCIDO_ABR"].ToString().Trim());
                                Disponible_Abr += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_ABR"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_ABR"].ToString().Trim());
                                Comprometido_Abr += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_ABR"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_ABR"].ToString().Trim());
                                Pre_Comprometido_Abr += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_ABR"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_ABR"].ToString().Trim());
                                Devengado_May += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_MAY"].ToString().Trim()) ? "0" : Dr["DEVENGADO_MAY"].ToString().Trim());
                                Pagado_May += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO_MAY"].ToString().Trim()) ? "0" : Dr["PAGADO_MAY"].ToString().Trim());
                                Ejercido_May += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO_MAY"].ToString().Trim()) ? "0" : Dr["EJERCIDO_MAY"].ToString().Trim());
                                Disponible_May += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_MAY"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_MAY"].ToString().Trim());
                                Comprometido_May += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_MAY"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_MAY"].ToString().Trim());
                                Pre_Comprometido_May += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_MAY"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_MAY"].ToString().Trim());
                                Devengado_Jun += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_JUN"].ToString().Trim()) ? "0" : Dr["DEVENGADO_JUN"].ToString().Trim());
                                Pagado_Jun += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO_JUN"].ToString().Trim()) ? "0" : Dr["PAGADO_JUN"].ToString().Trim());
                                Ejercido_Jun += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO_JUN"].ToString().Trim()) ? "0" : Dr["EJERCIDO_JUN"].ToString().Trim());
                                Disponible_Jun += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_JUN"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_JUN"].ToString().Trim());
                                Comprometido_Jun += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_JUN"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_JUN"].ToString().Trim());
                                Pre_Comprometido_Jun += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_JUN"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_JUN"].ToString().Trim());
                                Devengado_Jul += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_JUL"].ToString().Trim()) ? "0" : Dr["DEVENGADO_JUL"].ToString().Trim());
                                Pagado_Jul += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO_JUL"].ToString().Trim()) ? "0" : Dr["PAGADO_JUL"].ToString().Trim());
                                Ejercido_Jul += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO_JUL"].ToString().Trim()) ? "0" : Dr["EJERCIDO_JUL"].ToString().Trim());
                                Disponible_Jul += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_JUL"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_JUL"].ToString().Trim());
                                Comprometido_Jul += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_JUL"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_JUL"].ToString().Trim());
                                Pre_Comprometido_Jul += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_JUL"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_JUL"].ToString().Trim());
                                Devengado_Ago += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_AGO"].ToString().Trim()) ? "0" : Dr["DEVENGADO_AGO"].ToString().Trim());
                                Pagado_Ago += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO_AGO"].ToString().Trim()) ? "0" : Dr["PAGADO_AGO"].ToString().Trim());
                                Ejercido_Ago += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO_AGO"].ToString().Trim()) ? "0" : Dr["EJERCIDO_AGO"].ToString().Trim());
                                Disponible_Ago += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_AGO"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_AGO"].ToString().Trim());
                                Comprometido_Ago += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_AGO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_AGO"].ToString().Trim());
                                Pre_Comprometido_Ago += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_AGO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_AGO"].ToString().Trim());
                                Devengado_Sep += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_SEP"].ToString().Trim()) ? "0" : Dr["DEVENGADO_SEP"].ToString().Trim());
                                Pagado_Sep += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO_SEP"].ToString().Trim()) ? "0" : Dr["PAGADO_SEP"].ToString().Trim());
                                Ejercido_Sep += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO_SEP"].ToString().Trim()) ? "0" : Dr["EJERCIDO_SEP"].ToString().Trim());
                                Disponible_Sep += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_SEP"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_SEP"].ToString().Trim());
                                Comprometido_Sep += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_SEP"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_SEP"].ToString().Trim());
                                Pre_Comprometido_Sep += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_SEP"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_SEP"].ToString().Trim());
                                Devengado_Oct += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_OCT"].ToString().Trim()) ? "0" : Dr["DEVENGADO_OCT"].ToString().Trim());
                                Pagado_Oct += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO_OCT"].ToString().Trim()) ? "0" : Dr["PAGADO_OCT"].ToString().Trim());
                                Ejercido_Oct += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO_OCT"].ToString().Trim()) ? "0" : Dr["EJERCIDO_OCT"].ToString().Trim());
                                Disponible_Oct += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_OCT"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_OCT"].ToString().Trim());
                                Comprometido_Oct += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_OCT"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_OCT"].ToString().Trim());
                                Pre_Comprometido_Oct += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_OCT"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_OCT"].ToString().Trim());
                                Devengado_Nov += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_NOV"].ToString().Trim()) ? "0" : Dr["DEVENGADO_NOV"].ToString().Trim());
                                Pagado_Nov += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO_NOV"].ToString().Trim()) ? "0" : Dr["PAGADO_NOV"].ToString().Trim());
                                Ejercido_Nov += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO_NOV"].ToString().Trim()) ? "0" : Dr["EJERCIDO_NOV"].ToString().Trim());
                                Disponible_Nov += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_NOV"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_NOV"].ToString().Trim());
                                Comprometido_Nov += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_NOV"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_NOV"].ToString().Trim());
                                Pre_Comprometido_Nov += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_NOV"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_NOV"].ToString().Trim());
                                Devengado_Dic += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_DIC"].ToString().Trim()) ? "0" : Dr["DEVENGADO_DIC"].ToString().Trim());
                                Pagado_Dic += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO_DIC"].ToString().Trim()) ? "0" : Dr["PAGADO_DIC"].ToString().Trim());
                                Ejercido_Dic += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO_DIC"].ToString().Trim()) ? "0" : Dr["EJERCIDO_DIC"].ToString().Trim());
                                Disponible_Dic += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_DIC"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_DIC"].ToString().Trim());
                                Comprometido_Dic += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_DIC"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_DIC"].ToString().Trim());
                                Pre_Comprometido_Dic += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_DIC"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_DIC"].ToString().Trim());
                                Importe_Ene += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_ENE"].ToString().Trim()) ? "0" : Dr["IMPORTE_ENE"].ToString().Trim());
                                Importe_Feb += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_FEB"].ToString().Trim()) ? "0" : Dr["IMPORTE_FEB"].ToString().Trim());
                                Importe_Mar += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_MAR"].ToString().Trim()) ? "0" : Dr["IMPORTE_MAR"].ToString().Trim());
                                Importe_Abr += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_ABR"].ToString().Trim()) ? "0" : Dr["IMPORTE_ABR"].ToString().Trim());
                                Importe_May += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_MAY"].ToString().Trim()) ? "0" : Dr["IMPORTE_MAY"].ToString().Trim());
                                Importe_Jun += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_JUN"].ToString().Trim()) ? "0" : Dr["IMPORTE_JUN"].ToString().Trim());
                                Importe_Jul += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_JUL"].ToString().Trim()) ? "0" : Dr["IMPORTE_JUL"].ToString().Trim());
                                Importe_Ago += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_AGO"].ToString().Trim()) ? "0" : Dr["IMPORTE_AGO"].ToString().Trim());
                                Importe_Sep += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_SEP"].ToString().Trim()) ? "0" : Dr["IMPORTE_SEP"].ToString().Trim());
                                Importe_Oct += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_OCT"].ToString().Trim()) ? "0" : Dr["IMPORTE_OCT"].ToString().Trim());
                                Importe_Nov += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_NOV"].ToString().Trim()) ? "0" : Dr["IMPORTE_NOV"].ToString().Trim());
                                Importe_Dic += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_DIC"].ToString().Trim()) ? "0" : Dr["IMPORTE_DIC"].ToString().Trim());
                                Importe_Tot += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_TOTAL"].ToString().Trim()) ? "0" : Dr["IMPORTE_TOTAL"].ToString().Trim());
                            }

                            Nodo_Arbol = new Cls_Nodo_Arbol();

                            Nodo_Arbol.id = "";
                            Nodo_Arbol.texto = "Total";
                            Nodo_Arbol.descripcion1 = String.Format("{0:n}", Aprobado);
                            Nodo_Arbol.descripcion2 = String.Format("{0:n}", Ampliacion);
                            Nodo_Arbol.descripcion3 = String.Format("{0:n}", Reduccion);
                            Nodo_Arbol.descripcion4 = String.Format("{0:n}", Modificado);
                            Nodo_Arbol.descripcion5 = String.Format("{0:n}", Disponible);
                            Nodo_Arbol.descripcion6 = String.Format("{0:n}", Pre_Comprometido);
                            Nodo_Arbol.descripcion7 = String.Format("{0:n}", Comprometido);
                            Nodo_Arbol.descripcion8 = String.Format("{0:n}", Devengado);
                            Nodo_Arbol.descripcion9 = String.Format("{0:n}", Ejercido);
                            Nodo_Arbol.descripcion10 = String.Format("{0:n}", Pagado);
                            Nodo_Arbol.descripcion11 = "";
                            Nodo_Arbol.descripcion12 = "";
                            Nodo_Arbol.descripcion13 = "";
                            Nodo_Arbol.descripcion14 = String.Format("{0:n}", Disponible_Ene);
                            Nodo_Arbol.descripcion15 = String.Format("{0:n}", Disponible_Feb);
                            Nodo_Arbol.descripcion16 = String.Format("{0:n}", Disponible_Mar);
                            Nodo_Arbol.descripcion17 = String.Format("{0:n}", Disponible_Abr);
                            Nodo_Arbol.descripcion18 = String.Format("{0:n}", Disponible_May);
                            Nodo_Arbol.descripcion19 = String.Format("{0:n}", Disponible_Jun);
                            Nodo_Arbol.descripcion20 = String.Format("{0:n}", Disponible_Jul);
                            Nodo_Arbol.descripcion21 = String.Format("{0:n}", Disponible_Ago);
                            Nodo_Arbol.descripcion22 = String.Format("{0:n}", Disponible_Sep);
                            Nodo_Arbol.descripcion23 = String.Format("{0:n}", Disponible_Oct);
                            Nodo_Arbol.descripcion24 = String.Format("{0:n}", Disponible_Nov);
                            Nodo_Arbol.descripcion25 = String.Format("{0:n}", Disponible_Dic);
                            Nodo_Arbol.descripcion26 = String.Format("{0:n}", Pre_Comprometido_Ene);
                            Nodo_Arbol.descripcion27 = String.Format("{0:n}", Pre_Comprometido_Feb);
                            Nodo_Arbol.descripcion28 = String.Format("{0:n}", Pre_Comprometido_Mar);
                            Nodo_Arbol.descripcion29 = String.Format("{0:n}", Pre_Comprometido_Abr);
                            Nodo_Arbol.descripcion30 = String.Format("{0:n}", Pre_Comprometido_May);
                            Nodo_Arbol.descripcion31 = String.Format("{0:n}", Pre_Comprometido_Jun);
                            Nodo_Arbol.descripcion32 = String.Format("{0:n}", Pre_Comprometido_Jul);
                            Nodo_Arbol.descripcion33 = String.Format("{0:n}", Pre_Comprometido_Ago);
                            Nodo_Arbol.descripcion34 = String.Format("{0:n}", Pre_Comprometido_Sep);
                            Nodo_Arbol.descripcion35 = String.Format("{0:n}", Pre_Comprometido_Oct);
                            Nodo_Arbol.descripcion36 = String.Format("{0:n}", Pre_Comprometido_Nov);
                            Nodo_Arbol.descripcion37 = String.Format("{0:n}", Pre_Comprometido_Dic);
                            Nodo_Arbol.descripcion38 = String.Format("{0:n}", Comprometido_Ene);
                            Nodo_Arbol.descripcion39 = String.Format("{0:n}", Comprometido_Feb);
                            Nodo_Arbol.descripcion40 = String.Format("{0:n}", Comprometido_Mar);
                            Nodo_Arbol.descripcion41 = String.Format("{0:n}", Comprometido_Abr);
                            Nodo_Arbol.descripcion42 = String.Format("{0:n}", Comprometido_May);
                            Nodo_Arbol.descripcion43 = String.Format("{0:n}", Comprometido_Jun);
                            Nodo_Arbol.descripcion44 = String.Format("{0:n}", Comprometido_Jul);
                            Nodo_Arbol.descripcion45 = String.Format("{0:n}", Comprometido_Ago);
                            Nodo_Arbol.descripcion46 = String.Format("{0:n}", Comprometido_Sep);
                            Nodo_Arbol.descripcion47 = String.Format("{0:n}", Comprometido_Oct);
                            Nodo_Arbol.descripcion48 = String.Format("{0:n}", Comprometido_Nov);
                            Nodo_Arbol.descripcion49 = String.Format("{0:n}", Comprometido_Dic);
                            Nodo_Arbol.descripcion50 = String.Format("{0:n}", Devengado_Ene);
                            Nodo_Arbol.descripcion51 = String.Format("{0:n}", Devengado_Feb);
                            Nodo_Arbol.descripcion52 = String.Format("{0:n}", Devengado_Mar);
                            Nodo_Arbol.descripcion53 = String.Format("{0:n}", Devengado_Abr);
                            Nodo_Arbol.descripcion54 = String.Format("{0:n}", Devengado_May);
                            Nodo_Arbol.descripcion55 = String.Format("{0:n}", Devengado_Jun);
                            Nodo_Arbol.descripcion56 = String.Format("{0:n}", Devengado_Jul);
                            Nodo_Arbol.descripcion57 = String.Format("{0:n}", Devengado_Ago);
                            Nodo_Arbol.descripcion58 = String.Format("{0:n}", Devengado_Sep);
                            Nodo_Arbol.descripcion59 = String.Format("{0:n}", Devengado_Oct);
                            Nodo_Arbol.descripcion60 = String.Format("{0:n}", Devengado_Nov);
                            Nodo_Arbol.descripcion61 = String.Format("{0:n}", Devengado_Dic);
                            Nodo_Arbol.descripcion62 = String.Format("{0:n}", Ejercido_Ene);
                            Nodo_Arbol.descripcion63 = String.Format("{0:n}", Ejercido_Feb);
                            Nodo_Arbol.descripcion64 = String.Format("{0:n}", Ejercido_Mar);
                            Nodo_Arbol.descripcion65 = String.Format("{0:n}", Ejercido_Abr);
                            Nodo_Arbol.descripcion66 = String.Format("{0:n}", Ejercido_May);
                            Nodo_Arbol.descripcion67 = String.Format("{0:n}", Ejercido_Jun);
                            Nodo_Arbol.descripcion68 = String.Format("{0:n}", Ejercido_Jul);
                            Nodo_Arbol.descripcion69 = String.Format("{0:n}", Ejercido_Ago);
                            Nodo_Arbol.descripcion70 = String.Format("{0:n}", Ejercido_Sep);
                            Nodo_Arbol.descripcion71 = String.Format("{0:n}", Ejercido_Oct);
                            Nodo_Arbol.descripcion72 = String.Format("{0:n}", Ejercido_Nov);
                            Nodo_Arbol.descripcion73 = String.Format("{0:n}", Ejercido_Dic);
                            Nodo_Arbol.descripcion74 = String.Format("{0:n}", Pagado_Ene);
                            Nodo_Arbol.descripcion75 = String.Format("{0:n}", Pagado_Feb);
                            Nodo_Arbol.descripcion76 = String.Format("{0:n}", Pagado_Mar);
                            Nodo_Arbol.descripcion77 = String.Format("{0:n}", Pagado_Abr);
                            Nodo_Arbol.descripcion78 = String.Format("{0:n}", Pagado_May);
                            Nodo_Arbol.descripcion79 = String.Format("{0:n}", Pagado_Jun);
                            Nodo_Arbol.descripcion80 = String.Format("{0:n}", Pagado_Jul);
                            Nodo_Arbol.descripcion81 = String.Format("{0:n}", Pagado_Ago);
                            Nodo_Arbol.descripcion82 = String.Format("{0:n}", Pagado_Sep);
                            Nodo_Arbol.descripcion83 = String.Format("{0:n}", Pagado_Oct);
                            Nodo_Arbol.descripcion84 = String.Format("{0:n}", Pagado_Nov);
                            Nodo_Arbol.descripcion85 = String.Format("{0:n}", Pagado_Dic);
                            Nodo_Arbol.descripcion86 = String.Format("{0:n}", Importe_Ene);
                            Nodo_Arbol.descripcion87 = String.Format("{0:n}", Importe_Feb);
                            Nodo_Arbol.descripcion88 = String.Format("{0:n}", Importe_Mar);
                            Nodo_Arbol.descripcion89 = String.Format("{0:n}", Importe_Abr);
                            Nodo_Arbol.descripcion90 = String.Format("{0:n}", Importe_May);
                            Nodo_Arbol.descripcion91 = String.Format("{0:n}", Importe_Jun);
                            Nodo_Arbol.descripcion92 = String.Format("{0:n}", Importe_Jul);
                            Nodo_Arbol.descripcion93 = String.Format("{0:n}", Importe_Ago);
                            Nodo_Arbol.descripcion94 = String.Format("{0:n}", Importe_Sep);
                            Nodo_Arbol.descripcion95 = String.Format("{0:n}", Importe_Oct);
                            Nodo_Arbol.descripcion96 = String.Format("{0:n}", Importe_Nov);
                            Nodo_Arbol.descripcion97 = String.Format("{0:n}", Importe_Dic);
                            Nodo_Arbol.descripcion98 = String.Format("{0:n}", Importe_Tot);

                            //Agregamos los atributos
                            Atributos = new Cls_Atributos_TreeGrid();
                            Atributos.valor1 = "Total";
                            Atributos.valor2 = "";
                            Atributos.valor3 = "";
                            Atributos.valor4 = "";
                            Atributos.valor5 = "";
                            Atributos.valor6 = "";
                            Atributos.valor7 = "";
                            Atributos.valor8 = "";
                            Atributos.valor9 = "";

                            Nodo_Arbol.attributes = Atributos;
                            Lista_Nodo_Arbol.Add(Nodo_Arbol);

                            Json_Total = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);
                        }
                    }
                    return Json_Total;
                }
                catch (Exception Ex)
                {
                    throw new Exception(" Error al tratar de Obtener_Total Error[" + Ex.Message + "]");
                }
            }
        #endregion
    #endregion

    #region METODOS DETALLES
        #region (INGRESOS)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Det_Mov_Ing
            ///DESCRIPCIÓN          : Metodo para obtener los detalles de los movimientos de ingresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 24/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Det_Mov_Ing(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
                String R, String T, String Cl, String Con_Ing, String SubCon, String PP, String Tipo_Det)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                String Json = ""; //aki almacenaremos el json de las fuentes de financiamiento
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                Double Importe = 0.00;
                DataTable Dt = new DataTable();
                DataRow Fila;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    if (!String.IsNullOrEmpty(Fecha_Ini.Trim()))
                    {
                        Negocio.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Formatear_Fecha(Fecha_Ini.Trim()));
                    }
                    else 
                    {
                        Negocio.P_Fecha_Inicial = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Fecha_Fin.Trim()))
                    {
                        Negocio.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Formatear_Fecha(Fecha_Fin.Trim()));
                    }
                    else 
                    {
                        Negocio.P_Fecha_Final = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Mes.Trim()))
                    {
                        Negocio.P_Periodo_Inicial = Obtener_Periodo(Mes, Anio, "Inicial");
                        Negocio.P_Periodo_Final = Obtener_Periodo(Mes, Anio, "Final");
                    }
                    else 
                    {
                        Negocio.P_Periodo_Inicial = String.Empty;
                        Negocio.P_Periodo_Final = String.Empty;
                    }
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_Tipo_Detalle = Tipo_Det;

                    Dt_Datos = Negocio.Consultar_Det_Modificacion_Ing();

                    if (Dt_Datos != null && Dt_Datos.Rows.Count > 0)
                    {
                        Dt_Datos.Columns.Add("NO_SOLICITUD_ING");
                        Dt_Datos.Columns.Add("IMPORTE");
                        Dt_Datos.Columns.Add("FECHA_INICIO");
                        Dt_Datos.Columns.Add("FECHA_AUTORIZO");
                        foreach (DataRow Dr in Dt_Datos.Rows)
                        {
                            Dr["NO_SOLICITUD_ING"] = "T" + String.Format("{0:00}", Dr["NO_MODIFICACION"]) + "-" + Dr["NO_MOVIMIENTO"].ToString().Trim() + "-" +Dr["ANIO"].ToString().Trim();
                            Dr["IMPORTE"] = String.Format("{0:n}", Dr["IMPORTE_TOTAL"]);
                            Dr["FECHA_INICIO"] = String.Format("{0:dd/MMM/yyyy}", Dr["FECHA_CREO"]);
                            Dr["FECHA_AUTORIZO"] = String.Format("{0:dd/MMM/yyyy}", Dr["FECHA_MODIFICO"]);
                            Importe += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_TOTAL"].ToString().Trim()) ? "0" : Dr["IMPORTE_TOTAL"]);
                        }

                        //ordenamos los detalles
                        Dt_Datos = Ordenar_Tabla("Ing_Mov", Dt_Datos);

                        Dt_Datos.TableName = "rows";
                        Json_Datos = Ayudante_JQuery.Crear_Tabla_Formato_JSON_DataGrid(Dt_Datos, Dt_Datos.Rows.Count);

                        //creamos el pie de pagina del  grid
                        Dt.Columns.Add("NO_MOVIMIENTO");
                        Dt.Columns.Add("TIPO_MOVIMIENTO");
                        Dt.Columns.Add("NO_SOLICITUD_ING");
                        Dt.Columns.Add("JUSTIFICACION");
                        Dt.Columns.Add("FECHA_INICIO");
                        Dt.Columns.Add("USUARIO_CREO");
                        Dt.Columns.Add("FECHA_AUTORIZO");
                        Dt.Columns.Add("USUARIO_MODIFICO");
                        Dt.Columns.Add("CONCEPTO");
                        Dt.Columns.Add("IMPORTE");
                        Dt.Columns.Add("NO_MODIFICACION");
                        Dt.Columns.Add("ANIO");

                        Fila = Dt.NewRow();
                        Fila["NO_MOVIMIENTO"] = 0;
                        Fila["TIPO_MOVIMIENTO"] = " ";
                        Fila["NO_SOLICITUD_ING"] = " ";
                        Fila["JUSTIFICACION"] = " ";
                        Fila["FECHA_INICIO"] = " ";
                        Fila["USUARIO_CREO"] = " ";
                        Fila["FECHA_AUTORIZO"] = " ";
                        Fila["USUARIO_MODIFICO"] = " ";
                        Fila["CONCEPTO"] = "Total";
                        Fila["IMPORTE"] = String.Format("{0:n}", Importe);
                        Fila["NO_MODIFICACION"] = 0;
                        Fila["ANIO"] = 0;
                        Dt.Rows.Add(Fila);

                        Json_Tot = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);

                        //juntamos las cadenas para formar el footer
                        if (!String.IsNullOrEmpty(Json_Datos) && !String.IsNullOrEmpty(Json_Tot))
                        {
                            Json = Json_Datos.Substring(0, Json_Datos.Length - 1);
                            Json += ", \"footer\": " + Json_Tot + "}";
                        }
                        else 
                        {
                            Json = Json_Datos;
                        }
                    }

                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al obtener los detalles de ingresos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Det_Recaudado_Ing
            ///DESCRIPCIÓN          : Metodo para obtener los detalles de los movimientos de ingresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 28/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Det_Recaudado_Ing(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
                String R, String T, String Cl, String Con_Ing, String SubCon, String PP, String Tipo_Det)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                String Json = ""; //aki almacenaremos el json de las fuentes de financiamiento
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                Double Importe = 0.00;
                DataTable Dt = new DataTable();
                DataRow Fila;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    if (!String.IsNullOrEmpty(Fecha_Ini.Trim()))
                    {
                        Negocio.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Formatear_Fecha(Fecha_Ini.Trim()));
                    }
                    else
                    {
                        Negocio.P_Fecha_Inicial = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Fecha_Fin.Trim()))
                    {
                        Negocio.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Formatear_Fecha(Fecha_Fin.Trim()));
                    }
                    else
                    {
                        Negocio.P_Fecha_Final = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Mes.Trim()))
                    {
                        Negocio.P_Periodo_Inicial = Obtener_Periodo(Mes, Anio, "Inicial");
                        Negocio.P_Periodo_Final = Obtener_Periodo(Mes, Anio, "Final");
                    }
                    else
                    {
                        Negocio.P_Periodo_Inicial = String.Empty;
                        Negocio.P_Periodo_Final = String.Empty;
                    }
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_SubConcepto_ID = SubCon;
                    Negocio.P_Programa_Ing_ID = PP;
                    Negocio.P_Tipo_Detalle = Tipo_Det;

                    Dt_Datos = Negocio.Consultar_Det_Recaudado_Ing();

                    if (Dt_Datos != null && Dt_Datos.Rows.Count > 0)
                    {
                        Dt_Datos.Columns.Add("IMPORTE_TOTAL");
                        Dt_Datos.Columns.Add("FECHA_INICIO");
                        foreach (DataRow Dr in Dt_Datos.Rows)
                        {
                            Dr["IMPORTE_TOTAL"] = String.Format("{0:n}", Dr["IMPORTE"]);
                            Dr["FECHA_INICIO"] = String.Format("{0:dd/MMM/yyyy}", Dr["FECHA"]);
                            Importe += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE"].ToString().Trim()) ? "0" : Dr["IMPORTE"]);
                        }

                        //ordenamos la tabla de los detalles
                        Dt_Datos = Ordenar_Tabla("Ing_Rec", Dt_Datos);

                        Dt_Datos.TableName = "rows";
                        Json_Datos = Ayudante_JQuery.Crear_Tabla_Formato_JSON_DataGrid(Dt_Datos, Dt_Datos.Rows.Count);

                        //creamos el pie de pagina del  grid
                        Dt.Columns.Add("NO_MOVIMIENTO");
                        Dt.Columns.Add("NO_POLIZA");
                        Dt.Columns.Add("TIPO_POLIZA_ID");
                        Dt.Columns.Add("MES_ANO");
                        Dt.Columns.Add("FECHA_INICIO");
                        Dt.Columns.Add("DESCRIPCION");
                        Dt.Columns.Add("REFERENCIA");
                        Dt.Columns.Add("IMPORTE_TOTAL");
                        Dt.Columns.Add("CONCEPTO");
                        Dt.Columns.Add("USUARIO_CREO");

                        Fila = Dt.NewRow();
                        Fila["NO_MOVIMIENTO"] = 0;
                        Fila["NO_POLIZA"] = " ";
                        Fila["TIPO_POLIZA_ID"] = " ";
                        Fila["MES_ANO"] = " ";
                        Fila["FECHA_INICIO"] = " ";
                        Fila["DESCRIPCION"] = " ";
                        Fila["REFERENCIA"] = " ";
                        Fila["IMPORTE_TOTAL"] = String.Format("{0:n}", Importe);
                        Fila["CONCEPTO"] = "Total";
                        Fila["USUARIO_CREO"] = " ";
                       
                        Dt.Rows.Add(Fila);

                        Json_Tot = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);

                        //juntamos las cadenas para formar el footer
                        if (!String.IsNullOrEmpty(Json_Datos) && !String.IsNullOrEmpty(Json_Tot))
                        {
                            Json = Json_Datos.Substring(0, Json_Datos.Length - 1);
                            Json += ", \"footer\": " + Json_Tot + "}";
                        }
                        else
                        {
                            Json = Json_Datos;
                        }
                    }

                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al obtener los detalles de ingresos Error[" + ex.Message + "]");
                }
            }
        #endregion

        #region (EGRESOS)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Det_Mov_Ing
            ///DESCRIPCIÓN          : Metodo para obtener los detalles de los movimientos de egresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 27/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Det_Mov_Egr(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
                String AF, String PP, String UR, String Cap, String Con, String PG, String P, String Tipo_Det)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                String Json = ""; //aki almacenaremos el json de las fuentes de financiamiento
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                Double Importe = 0.00;
                DataTable Dt = new DataTable();
                DataRow Fila;
                String Json_Tot = String.Empty;
                DataTable Dt_Temp = new DataTable();

                try
                {
                    Negocio.P_Anio = Anio;
                    if (!String.IsNullOrEmpty(Fecha_Ini.Trim()))
                    {
                        Negocio.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Formatear_Fecha(Fecha_Ini.Trim()));
                    }
                    else
                    {
                        Negocio.P_Fecha_Inicial = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Fecha_Fin.Trim()))
                    {
                        Negocio.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Formatear_Fecha(Fecha_Fin.Trim()));
                    }
                    else
                    {
                        Negocio.P_Fecha_Final = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Mes.Trim()))
                    {
                        Negocio.P_Periodo_Inicial = Obtener_Periodo(Mes, Anio, "Inicial");
                        Negocio.P_Periodo_Final = Obtener_Periodo(Mes, Anio, "Final");
                    }
                    else
                    {
                        Negocio.P_Periodo_Inicial = String.Empty;
                        Negocio.P_Periodo_Final = String.Empty;
                    }
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Area_Funcional_ID = AF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_Tipo_Detalle = Tipo_Det;

                    Dt_Datos = Negocio.Consultar_Det_Modificacion_Egr();

                    if (Dt_Datos != null && Dt_Datos.Rows.Count > 0)
                    {
                        if (!String.IsNullOrEmpty(Tipo_Det.Trim()) && Tipo_Det.Trim().Equals("AMPLIACION"))
                        {
                            Dt_Temp.Columns.Add("NO_MODIFICACION", typeof(System.Decimal));
                            Dt_Temp.Columns.Add("NO_MOVIMIENTO", typeof(System.Decimal));
                            Dt_Temp.Columns.Add("ANIO", typeof(System.Decimal));
                            Dt_Temp.Columns.Add("IMPORTE_TOTAL");
                            Dt_Temp.Columns.Add("TIPO_OPERACION");
                            Dt_Temp.Columns.Add("JUSTIFICACION");
                            Dt_Temp.Columns.Add("FECHA_CREO" , typeof(System.DateTime));
                            Dt_Temp.Columns.Add("USUARIO_CREO");
                            Dt_Temp.Columns.Add("FECHA_MODIFICO");
                            Dt_Temp.Columns.Add("USUARIO_MODIFICO");
                            Dt_Temp.Columns.Add("PARTIDA");
                            Dt_Temp.Columns.Add("NO_SOLICITUD_EGR");
                            Dt_Temp.Columns.Add("IMPORTE");
                            Dt_Temp.Columns.Add("FECHA_INICIO");
                            Dt_Temp.Columns.Add("FECHA_AUTORIZO");
                        }
                        else 
                        {
                            Dt_Datos.Columns.Add("NO_SOLICITUD_EGR");
                            Dt_Datos.Columns.Add("IMPORTE");
                            Dt_Datos.Columns.Add("FECHA_INICIO");
                            Dt_Datos.Columns.Add("FECHA_AUTORIZO");
                        }

                        foreach (DataRow Dr in Dt_Datos.Rows)
                        {
                            if (!String.IsNullOrEmpty(Tipo_Det.Trim()) && Tipo_Det.Trim().Equals("AMPLIACION"))
                            {
                                if ((Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Origen") && Dr["TIPO_OPERACION"].ToString().Trim().Equals("AMPLIACION"))
                                    || (Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Destino") && Dr["TIPO_OPERACION"].ToString().Trim().Equals("TRASPASO"))) 
                                {
                                    Fila = Dt_Temp.NewRow();
                                    Fila["NO_MODIFICACION"] = Dr["NO_MODIFICACION"];
                                    Fila["NO_MOVIMIENTO"] = Dr["NO_MOVIMIENTO"];
                                    Fila["ANIO"] = Dr["ANIO"];
                                    Fila["IMPORTE_TOTAL"] = Dr["IMPORTE_TOTAL"].ToString().Trim();
                                    Fila["TIPO_OPERACION"] = Dr["TIPO_OPERACION"].ToString().Trim();
                                    Fila["JUSTIFICACION"] = Dr["JUSTIFICACION"].ToString().Trim();
                                    Fila["FECHA_CREO"] = Dr["FECHA_CREO"];
                                    Fila["USUARIO_CREO"] = Dr["USUARIO_CREO"].ToString().Trim();
                                    Fila["FECHA_MODIFICO"] = Dr["FECHA_MODIFICO"].ToString().Trim();
                                    Fila["USUARIO_MODIFICO"] = Dr["USUARIO_MODIFICO"].ToString().Trim();
                                    Fila["PARTIDA"] = Dr["PARTIDA"].ToString().Trim();
                                    Fila["NO_SOLICITUD_EGR"] = Dr["NO_MODIFICACION"] + "a. Modificación - No Solicitud " + Dr["NO_MOVIMIENTO"].ToString().Trim() + " - " + Dr["ANIO"].ToString().Trim();
                                    Fila["IMPORTE"] = String.Format("{0:n}", Dr["IMPORTE_TOTAL"]);
                                    Fila["FECHA_INICIO"] = String.Format("{0:dd/MMM/yyyy}", Dr["FECHA_CREO"]);
                                    Fila["FECHA_AUTORIZO"] = String.Format("{0:dd/MMM/yyyy}", Dr["FECHA_MODIFICO"]);
                                    Dt_Temp.Rows.Add(Fila);

                                    Importe += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_TOTAL"].ToString().Trim()) ? "0" : Dr["IMPORTE_TOTAL"]);
                                }
                            }
                            else 
                            {
                                if (Convert.ToInt64(String.IsNullOrEmpty(Dr["NO_MODIFICACION"].ToString()) ? "0" : Dr["NO_MODIFICACION"])  <= 0)
                                {
                                    Dr["NO_SOLICITUD_EGR"] = "No Solicitud " + Dr["NO_MOVIMIENTO"].ToString().Trim() + " - " + Dr["ANIO"].ToString().Trim();
                                }
                                else{
                                    Dr["NO_SOLICITUD_EGR"] = Dr["NO_MODIFICACION"] + "a. Modificación - No Solicitud " + Dr["NO_MOVIMIENTO"].ToString().Trim() + " - " + Dr["ANIO"].ToString().Trim();
                                }
                                Dr["IMPORTE"] = String.Format("{0:n}", Dr["IMPORTE_TOTAL"]);
                                Dr["FECHA_INICIO"] = String.Format("{0:dd/MMM/yyyy}", Dr["FECHA_CREO"]);
                                Dr["FECHA_AUTORIZO"] = String.Format("{0:dd/MMM/yyyy}", Dr["FECHA_MODIFICO"]);
                                Importe += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_TOTAL"].ToString().Trim()) ? "0" : Dr["IMPORTE_TOTAL"]);
                            }
                        }

                        if (!String.IsNullOrEmpty(Tipo_Det.Trim()))
                        {
                            if (Tipo_Det.Trim() != "REDUCCION")
                            {
                                Dt_Datos = Dt_Temp;
                            }
                        }

                        //ordenamos el datatable
                        Dt_Datos = Ordenar_Tabla("Egr_Mov", Dt_Datos);

                        Dt_Datos.TableName = "rows";
                        Json_Datos = Ayudante_JQuery.Crear_Tabla_Formato_JSON_DataGrid(Dt_Datos, Dt_Datos.Rows.Count);

                        //creamos el pie de pagina del  grid
                        Dt.Columns.Add("NO_MOVIMIENTO");
                        Dt.Columns.Add("TIPO_OPERACION");
                        Dt.Columns.Add("NO_SOLICITUD_EGR");
                        Dt.Columns.Add("JUSTIFICACION");
                        Dt.Columns.Add("FECHA_INICIO");
                        Dt.Columns.Add("USUARIO_CREO");
                        Dt.Columns.Add("FECHA_AUTORIZO");
                        Dt.Columns.Add("USUARIO_MODIFICO");
                        Dt.Columns.Add("PARTIDA");
                        Dt.Columns.Add("IMPORTE");
                        Dt.Columns.Add("NO_MODIFICACION");
                        Dt.Columns.Add("ANIO");

                        Fila = Dt.NewRow();
                        Fila["NO_MOVIMIENTO"] = 0;
                        Fila["TIPO_OPERACION"] = " ";
                        Fila["NO_SOLICITUD_EGR"] = " ";
                        Fila["JUSTIFICACION"] = " ";
                        Fila["FECHA_INICIO"] = " ";
                        Fila["USUARIO_CREO"] = " ";
                        Fila["FECHA_AUTORIZO"] = " ";
                        Fila["USUARIO_MODIFICO"] = " ";
                        Fila["PARTIDA"] = "Total";
                        Fila["IMPORTE"] = String.Format("{0:n}", Importe);
                        Fila["NO_MODIFICACION"] = 0;
                        Fila["ANIO"] = 0;
                        Dt.Rows.Add(Fila);

                        Json_Tot = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);

                        //juntamos las cadenas para formar el footer
                        if (!String.IsNullOrEmpty(Json_Datos) && !String.IsNullOrEmpty(Json_Tot))
                        {
                            Json = Json_Datos.Substring(0, Json_Datos.Length - 1);
                            Json += ", \"footer\": " + Json_Tot + "}";
                        }
                        else
                        {
                            Json = Json_Datos;
                        }
                    }

                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al obtener los detalles de egresos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Det_Mov_Con_Egr
            ///DESCRIPCIÓN          : Metodo para obtener los detalles del devengado, pagado y comprometido de egresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 29/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Det_Mov_Con_Egr(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
                String AF, String PP, String UR, String Cap, String Con, String PG, String P, String Tipo_Det)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                String Json = ""; //aki almacenaremos el json de las fuentes de financiamiento
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                Double Importe = 0.00;
                DataTable Dt = new DataTable();
                DataRow Fila;
                String Json_Tot = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio;
                    if (!String.IsNullOrEmpty(Fecha_Ini.Trim()))
                    {
                        Negocio.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Formatear_Fecha(Fecha_Ini.Trim()));
                    }
                    else
                    {
                        Negocio.P_Fecha_Inicial = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Fecha_Fin.Trim()))
                    {
                        Negocio.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Formatear_Fecha(Fecha_Fin.Trim()));
                    }
                    else
                    {
                        Negocio.P_Fecha_Final = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Mes.Trim()))
                    {
                        Negocio.P_Periodo_Inicial = Obtener_Periodo(Mes, Anio, "Inicial");
                        Negocio.P_Periodo_Final = Obtener_Periodo(Mes, Anio, "Final");
                    }
                    else
                    {
                        Negocio.P_Periodo_Inicial = String.Empty;
                        Negocio.P_Periodo_Final = String.Empty;
                    }
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Area_Funcional_ID = AF;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_Concepto_ID = Con;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_Tipo_Detalle = Tipo_Det;

                    if (!String.IsNullOrEmpty(Tipo_Det.Trim()))
                    {
                        if (Tipo_Det.Trim().Equals(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido))
                        {
                            Dt_Datos = Negocio.Consultar_Det_Movimiento_Contable_Egr_Pre_Comprometido();
                        }
                        else if (Tipo_Det.Trim().Equals(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido))
                        {
                            Dt_Datos = Negocio.Consultar_Det_Movimiento_Contable_Egr_Comprometido();
                        }
                        else 
                        {
                            Dt_Datos = Negocio.Consultar_Det_Movimiento_Contable_Egr_Dev();
                        }
                    }

                    if (Dt_Datos != null && Dt_Datos.Rows.Count > 0)
                    {
                        Dt_Datos.Columns.Add("IMPORTE");
                        Dt_Datos.Columns.Add("FECHA");
                        foreach (DataRow Dr in Dt_Datos.Rows)
                        {
                            Importe += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_MOV"].ToString().Trim()) ? "0" : Dr["IMPORTE_MOV"]);

                            Dr["IMPORTE"] = String.Format("{0:n}", Dr["IMPORTE_MOV"]);
                            Dr["FECHA"] = String.Format("{0:dd/MMM/yyyy}", Dr["FECHA_MOV"]);

                            if (String.IsNullOrEmpty(Dr["BENEFICIARIO"].ToString()))
                            {
                                Dr["BENEFICIARIO"] = " ";
                            }
                        }
                        
                        //ordenamos el datatable
                        Dt_Datos = Ordenar_Tabla("Egr_Con", Dt_Datos);

                        Dt_Datos.TableName = "rows";
                        Json_Datos = Ayudante_JQuery.Crear_Tabla_Formato_JSON_DataGrid(Dt_Datos, Dt_Datos.Rows.Count);

                        //creamos el pie de pagina del  grid
                        Dt.Columns.Add("NO_RESERVA");
                        Dt.Columns.Add("NO_POLIZA");
                        Dt.Columns.Add("TIPO_POLIZA_ID");
                        Dt.Columns.Add("MES_ANO");
                        Dt.Columns.Add("IMPORTE");
                        Dt.Columns.Add("FECHA");
                        Dt.Columns.Add("USUARIO_MOV");
                        Dt.Columns.Add("ANIO");
                        Dt.Columns.Add("CONCEPTO");
                        Dt.Columns.Add("BENEFICIARIO");
                        Dt.Columns.Add("TIPO_SOLICITUD");
                        Dt.Columns.Add("DEPENDENCIA");
                        Dt.Columns.Add("PROGRAMA");
                        Dt.Columns.Add("FUENTE");
                        Dt.Columns.Add("PARTIDA");

                        Fila = Dt.NewRow();
                        Fila["NO_RESERVA"] = " ";
                        Fila["NO_POLIZA"] = " ";
                        Fila["TIPO_POLIZA_ID"] = " ";
                        Fila["MES_ANO"] = " ";
                        Fila["IMPORTE"] = String.Format("{0:n}", Importe);
                        Fila["FECHA"] = " ";
                        Fila["USUARIO_MOV"] = " ";
                        Fila["ANIO"] = " ";
                        Fila["CONCEPTO"] = " ";
                        Fila["BENEFICIARIO"] = " ";
                        Fila["TIPO_SOLICITUD"] = " ";
                        Fila["DEPENDENCIA"] = " ";
                        Fila["PROGRAMA"] = " ";
                        Fila["FUENTE"] = " ";
                        Fila["PARTIDA"] = "Total";
                        
                        Dt.Rows.Add(Fila);

                        Json_Tot = Ayudante_JQuery.Crear_Tabla_Formato_JSON_ComboBox(Dt);

                        //juntamos las cadenas para formar el footer
                        if (!String.IsNullOrEmpty(Json_Datos) && !String.IsNullOrEmpty(Json_Tot))
                        {
                            Json = Json_Datos.Substring(0, Json_Datos.Length - 1);
                            Json += ", \"footer\": " + Json_Tot + "}";
                        }
                        else
                        {
                            Json = Json_Datos;
                        }
                    }

                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al obtener los detalles de egresos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Ordenar_Tabla
            ///DESCRIPCIÓN          : Metodo para ordenar los detalles del presupuesto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 28/Agosto/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private DataTable Ordenar_Tabla(String Tipo, DataTable Dt_Datos) 
            {
                String[] Ordenar = null;
                String[] Param = null;
                String Parametro = String.Empty;
                String Orden = String.Empty;
                String Campo = String.Empty;

                try
                {
                    //obtenemos los parametros del campo a ordenar y el tipo de ordenamiento
                    if (this.Request.Form != null)
                    {
                        if (this.Request.Form.AllKeys.Length > 0)
                        {
                            Ordenar = this.Request.Form.ToString().Trim().Split('&');
                            if (Ordenar.Length > 0)
                            {
                                for (int i = 0; i < Ordenar.Length; i++)
                                {
                                    Param = Ordenar[i].Split('=');
                                    if (Param.Length > 0)
                                    {
                                        Parametro = Param[0].ToString().Trim();
                                        if (Parametro.Trim().Equals("order"))
                                        {
                                            Orden = Param[1].ToString().Trim();
                                        }
                                        else if (Parametro.Trim().Equals("sort"))
                                        {
                                            Campo = Param[1].ToString().Trim();
                                        }
                                    }
                                }
                            }


                            //ordenamos los datos
                            if (!String.IsNullOrEmpty(Orden))
                            {
                                switch (Orden)
                                {
                                    case "asc":
                                        if (Dt_Datos.Columns[Campo].DataType.ToString().Trim().Equals("System.Int32"))
                                        {
                                            Dt_Datos = (from Fila_ in Dt_Datos.AsEnumerable()
                                                        orderby Fila_.Field<Int32>(Campo) ascending
                                                        select Fila_).AsDataView().ToTable();
                                        }
                                        else
                                        {
                                            if (Campo.Trim().Contains("FECHA"))
                                            {
                                                if (Tipo.Trim().Equals("Egr_Con"))
                                                {
                                                    Dt_Datos = (from Fila_ in Dt_Datos.AsEnumerable()
                                                                orderby Fila_.Field<DateTime>("FECHA_MOV") ascending
                                                                select Fila_).AsDataView().ToTable();
                                                }
                                                else if (Tipo.Trim().Equals("Egr_Mov") || Tipo.Trim().Equals("Ing_Mov"))
                                                {
                                                    Dt_Datos = (from Fila_ in Dt_Datos.AsEnumerable()
                                                                orderby Fila_.Field<DateTime>("FECHA_CREO") ascending
                                                                select Fila_).AsDataView().ToTable();
                                                }
                                                else if (Tipo.Trim().Equals("Ing_Rec"))
                                                {
                                                    Dt_Datos = (from Fila_ in Dt_Datos.AsEnumerable()
                                                                orderby Fila_.Field<DateTime>("FECHA") ascending
                                                                select Fila_).AsDataView().ToTable();
                                                }
                                            }
                                            else if (Campo.Trim().Equals("NO_SOLICITUD_EGR"))
                                            {
                                                Dt_Datos = (from Fila_ in Dt_Datos.AsEnumerable()
                                                            orderby Fila_.Field<Decimal>("NO_MODIFICACION") ascending,
                                                            Fila_.Field<Decimal>("NO_MOVIMIENTO") ascending,
                                                            Fila_.Field<Decimal>("ANIO") ascending
                                                            select Fila_).AsDataView().ToTable();
                                            }
                                            else
                                            {
                                                if (Dt_Datos.Columns[Campo].DataType.ToString().Trim().Equals("System.Decimal"))
                                                {
                                                    Dt_Datos = (from Fila_ in Dt_Datos.AsEnumerable()
                                                                orderby Fila_.Field<Decimal>(Campo) ascending
                                                                select Fila_).AsDataView().ToTable();
                                                }
                                                else if (Dt_Datos.Columns[Campo].DataType.ToString().Trim().Equals("System.String"))
                                                {
                                                    Dt_Datos = (from Fila_ in Dt_Datos.AsEnumerable()
                                                                orderby Fila_.Field<String>(Campo) ascending
                                                                select Fila_).AsDataView().ToTable();
                                                }
                                            }
                                        }
                                        break;

                                    case "desc":
                                        if (Dt_Datos.Columns[Campo].DataType.ToString().Trim().Equals("System.Int32"))
                                        {
                                            Dt_Datos = (from Fila_ in Dt_Datos.AsEnumerable()
                                                        orderby Fila_.Field<Int32>(Campo) descending
                                                        select Fila_).AsDataView().ToTable();
                                        }
                                        else
                                        {
                                            if (Campo.Trim().Contains("FECHA"))
                                            {
                                                if (Tipo.Trim().Equals("Egr_Con"))
                                                {
                                                    Dt_Datos = (from Fila_ in Dt_Datos.AsEnumerable()
                                                                orderby Fila_.Field<DateTime>("FECHA_MOV") descending
                                                                select Fila_).AsDataView().ToTable();
                                                }
                                                else if (Tipo.Trim().Equals("Egr_Mov") || Tipo.Trim().Equals("Ing_Mov"))
                                                {
                                                    Dt_Datos = (from Fila_ in Dt_Datos.AsEnumerable()
                                                                orderby Fila_.Field<DateTime>("FECHA_CREO") descending
                                                                select Fila_).AsDataView().ToTable();
                                                }
                                                else if (Tipo.Trim().Equals("Ing_Rec"))
                                                {
                                                    Dt_Datos = (from Fila_ in Dt_Datos.AsEnumerable()
                                                                orderby Fila_.Field<DateTime>("FECHA") descending
                                                                select Fila_).AsDataView().ToTable();
                                                }
                                            }
                                            else if (Campo.Trim().Equals("NO_SOLICITUD_EGR"))
                                            {
                                                Dt_Datos = (from Fila_ in Dt_Datos.AsEnumerable()
                                                            orderby Fila_.Field<Decimal>("NO_MODIFICACION") descending,
                                                            Fila_.Field<Decimal>("NO_MOVIMIENTO") descending,
                                                            Fila_.Field<Decimal>("ANIO") descending
                                                            select Fila_).AsDataView().ToTable();
                                            }
                                            else
                                            {
                                                if (Dt_Datos.Columns[Campo].DataType.ToString().Trim().Equals("System.Decimal"))
                                                {
                                                    Dt_Datos = (from Fila_ in Dt_Datos.AsEnumerable()
                                                                orderby Fila_.Field<Decimal>(Campo) descending
                                                                select Fila_).AsDataView().ToTable();
                                                }
                                                else if (Dt_Datos.Columns[Campo].DataType.ToString().Trim().Equals("System.String"))
                                                {
                                                    Dt_Datos = (from Fila_ in Dt_Datos.AsEnumerable()
                                                                orderby Fila_.Field<String>(Campo) descending
                                                                select Fila_).AsDataView().ToTable();
                                                }
                                            }
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al ordernar los detalles. Error[" + Ex.Message + "]");
                }

                return Dt_Datos;
            }
        #endregion
    #endregion

    #region METODOS REPORTE
        #region (Reportes Psp Egresos Ingresos)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Reporte
            ///DESCRIPCIÓN          : Metodo para obtener los datos para los reportes
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 30/Agosto/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Reporte(String Anio, String FF, String FF_Ing, String PP, String PP_F, String PP_Ing, String PP_Ing_F,
                String UR, String UR_F, String AF, String R, String R_F, String T, String T_F, String Cl, String Cl_F, String Con_Ing,
                String Con_Ing_F, String SubCon, String SubCon_F, String Cap, String Cap_F, String Con, String Con_F, String PG, String PG_F,
                String P, String P_F, String Reporte, String Tipo_PSP)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json_Datos = "";
                DataTable Dt_Ing = new DataTable();
                DataTable Dt_Egr = new DataTable();
                DataTable Dt_Ing_Completo = new DataTable();
                DataTable Dt_Egr_Completo = new DataTable();
                String Json_Tot = String.Empty;

                try
                {
                    //parametros egresos
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Area_Funcional_ID = AF;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_ProgramaF_ID = PP_F;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_DependenciaF_ID = UR_F;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_CapituloF_ID = Cap_F;
                    Negocio.P_ConceptoF_ID = Con;
                    Negocio.P_Concepto_ID = Con_F;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_GenericaF_ID = PG_F;
                    Negocio.P_PartidaF_ID = P;
                    Negocio.P_Partida_ID = P_F;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        if (String.IsNullOrEmpty(UR))
                        {
                            Negocio.P_Dependencia_ID = Obtener_Dependencias();
                        }
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = FF_Nomina;
                    }

                    //parametros ingresos
                    Negocio.P_Fte_Financiamiento_Ing = FF_Ing;
                    Negocio.P_Programa_Ing_ID = PP_Ing;
                    Negocio.P_ProgramaF_Ing_ID = PP_Ing_F;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_RubroF_ID = R_F;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_TipoF_ID = T_F;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_ClaseF_Ing_ID = Cl_F;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_ConceptoF_Ing_ID = Con_Ing_F;
                    Negocio.P_SubConcepto_ID = SubCon;
                    Negocio.P_SubConceptoF_ID = SubCon_F;

                    if (Tipo_PSP.Trim().Equals("ING"))
                    {
                        Dt_Ing = Negocio.Consultar_Psp_Ing();
                    }
                    else if (Tipo_PSP.Trim().Equals("EGR"))
                    {
                        Dt_Egr = Negocio.Consultar_Psp_Egr();
                    }
                    else
                    {
                        Dt_Ing = Negocio.Consultar_Psp_Ing();
                        Dt_Egr = Negocio.Consultar_Psp_Egr();
                    }

                    if (Dt_Egr != null && Dt_Egr.Rows.Count > 0)
                    {
                        Dt_Egr_Completo = Generar_Dt_Psp_Egr(Dt_Egr);
                    }

                    if (Dt_Ing != null && Dt_Ing.Rows.Count > 0)
                    {
                        Dt_Ing_Completo = Generar_Dt_Psp_Ing(Dt_Ing);
                    }

                    Json_Datos = Obtener_Param_Niveles(Dt_Egr_Completo, Dt_Ing_Completo, Anio, Reporte);

                    return Json_Datos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Nivel_Fuente_Financiamiento_Ingresos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Param_Niveles
            ///DESCRIPCIÓN          : Metodo para obtener los parametros de los niveles del reporte
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 30/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Param_Niveles(DataTable Dt_Egr, DataTable Dt_Ing, String Anio, String Tipo_Reporte)
            {
                String Nom_Archivo = String.Empty;
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                //Parametros de los combos
                String Niv_FF = String.Empty;
                String Niv_PP = String.Empty;
                String Niv_AF = String.Empty;
                String Niv_UR = String.Empty;
                String Niv_Cap = String.Empty;
                String Niv_Con = String.Empty;
                String Niv_Par_Gen = String.Empty;
                String Niv_Par = String.Empty;

                String Niv_FF_Ing = String.Empty;
                String Niv_PP_Ing = String.Empty;
                String Niv_Rub = String.Empty;
                String Niv_Tip = String.Empty;
                String Niv_Cla = String.Empty;
                String Niv_Con_Ing = String.Empty;
                String Niv_SubCon = String.Empty;

                DataTable Dt_Niv_Psp_Ing = new DataTable();
                DataTable Dt_Niv_Psp_Egr = new DataTable();
                DataRow Fila;
                Boolean Insertar = false;
                DataSet Ds_Registros_Ing = null;
                DataTable Dt_UR = new DataTable();
                String Dependencia = String.Empty;
                String Gpo_Dep = String.Empty;

                try
                {
                    #region (Parametros)
                    //obtenemos los parametros e los combos
                    if (this.Request.QueryString["Niv_FF"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_FF"].ToString().Trim()))
                    {
                        Niv_FF = this.Request.QueryString["Niv_FF"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_PP"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_PP"].ToString().Trim()))
                    {
                        Niv_PP = this.Request.QueryString["Niv_PP"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_AF"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_AF"].ToString().Trim()))
                    {
                        Niv_AF = this.Request.QueryString["Niv_AF"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_UR"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_UR"].ToString().Trim()))
                    {
                        Niv_UR = this.Request.QueryString["Niv_UR"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_Cap"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_Cap"].ToString().Trim()))
                    {
                        Niv_Cap = this.Request.QueryString["Niv_Cap"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_Con"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_Con"].ToString().Trim()))
                    {
                        Niv_Con = this.Request.QueryString["Niv_Con"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_Par_Gen"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_Par_Gen"].ToString().Trim()))
                    {
                        Niv_Par_Gen = this.Request.QueryString["Niv_Par_Gen"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_Par"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_Par"].ToString().Trim()))
                    {
                        Niv_Par = this.Request.QueryString["Niv_Par"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_FF_Ing"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_FF_Ing"].ToString().Trim()))
                    {
                        Niv_FF_Ing = this.Request.QueryString["Niv_FF_Ing"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_PP_Ing"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_PP_Ing"].ToString().Trim()))
                    {
                        Niv_PP_Ing = this.Request.QueryString["Niv_PP_Ing"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_Rub"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_Rub"].ToString().Trim()))
                    {
                        Niv_Rub = this.Request.QueryString["Niv_Rub"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_Tip"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_Tip"].ToString().Trim()))
                    {
                        Niv_Tip = this.Request.QueryString["Niv_Tip"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_Cla"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_Cla"].ToString().Trim()))
                    {
                        Niv_Cla = this.Request.QueryString["Niv_Cla"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_Con_Ing"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_Con_Ing"].ToString().Trim()))
                    {
                        Niv_Con_Ing = this.Request.QueryString["Niv_Con_Ing"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_SubCon"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_SubCon"].ToString().Trim()))
                    {
                        Niv_SubCon = this.Request.QueryString["Niv_SubCon"].ToString().Trim();
                    }
                    #endregion

                    //obtenemos la dependencia del usuario y el grupo dependencia
                    Negocio.P_Busqueda = String.Empty;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = String.Empty;
                    Negocio.P_Area_Funcional_ID = String.Empty;
                    Negocio.P_Programa_ID = String.Empty;
                    Negocio.P_Tipo_Usuario = "Usuario";
                    Negocio.P_Dependencia_ID = "'" + Cls_Sessiones.Dependencia_ID_Empleado.Trim() + "'";

                    Dt_UR = Negocio.Consultar_UR();

                    if (Dt_UR != null && Dt_UR.Rows.Count > 0)
                    {
                        Dependencia = Dt_UR.Rows[0]["NOM_DEP"].ToString().Trim();
                        Gpo_Dep = Dt_UR.Rows[0]["NOM_GPO_DEP"].ToString().Trim();
                    }

                    if (Dt_Egr != null && Dt_Egr.Rows.Count > 0)
                    {
                        Dt_Niv_Psp_Egr.Columns.Add("Concepto", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Aprobado", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Ampliacion", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Reduccion", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Modificado", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pre_Comprometido", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Comprometido", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Devengado", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Ejercido", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pagado", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Disponible", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Tipo", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Elaboro", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Dependencia", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Anio", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Gpo_Dependencia", System.Type.GetType("System.String"));

                        foreach (DataRow Dr in Dt_Egr.Rows)
                        {
                            Insertar = false;

                            if (Dr["Tipo"].ToString().Trim().Equals("Tot"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("FF") && Niv_FF.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("AF") && Niv_AF.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("PP") && Niv_PP.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("UR") && Niv_UR.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("Cap") && Niv_Cap.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("Con") && Niv_Con.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("PG") && Niv_Par_Gen.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("P") && Niv_Par.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else
                            {
                                Insertar = false;
                            }

                            if (Insertar)
                            {
                                Fila = Dt_Niv_Psp_Egr.NewRow();
                                Fila["Concepto"] = Dr["Concepto"].ToString().Trim();
                                Fila["Aprobado"] = Dr["Aprobado"].ToString().Trim();
                                Fila["Ampliacion"] = Dr["Ampliacion"].ToString().Trim();
                                Fila["Reduccion"] = Dr["Reduccion"].ToString().Trim();
                                Fila["Modificado"] = Dr["Modificado"].ToString().Trim();
                                Fila["Pre_Comprometido"] = Dr["Pre_Comprometido"].ToString().Trim();
                                Fila["Comprometido"] = Dr["Comprometido"].ToString().Trim();
                                Fila["Devengado"] = Dr["Devengado"].ToString().Trim();
                                Fila["Ejercido"] = Dr["Ejercido"].ToString().Trim();
                                Fila["Pagado"] = Dr["Recaudado"].ToString().Trim();
                                Fila["Disponible"] = Dr["Por_Recaudar"].ToString().Trim();
                                Fila["Tipo"] = Dr["Tipo"].ToString().Trim();
                                Fila["Elaboro"] = Cls_Sessiones.Nombre_Empleado.Trim();
                                Fila["Dependencia"] = Dependencia.Trim();
                                Fila["Anio"] = "PRESUPUESTO DE EGRESOS EJERCICIO " + Anio.Trim();
                                Fila["Gpo_Dependencia"] = Gpo_Dep.Trim();
                                Dt_Niv_Psp_Egr.Rows.Add(Fila);
                            }
                        }

                        if (Tipo_Reporte.Trim().Equals("PDF"))
                        {
                            Ds_Registros_Ing = new DataSet();
                            Dt_Niv_Psp_Egr.TableName = "Dt_Psp_Egr";
                            Ds_Registros_Ing.Tables.Add(Dt_Niv_Psp_Egr.Copy());
                            Exportar_Reporte(Ds_Registros_Ing, "Cr_Rpt_Psp_Presupuesto_Egr.rpt", "Rpt_Presupuesto_Egresos_" + Session.SessionID,
                                "pdf", ExportFormatType.PortableDocFormat, "Presupuestos");
                            Nom_Archivo = "../../Reporte/Rpt_Presupuesto_Egresos_" + Session.SessionID + ".pdf     ";
                        }
                        else if (Tipo_Reporte.Trim().Equals("Excel"))
                        {
                            Ds_Registros_Ing = new DataSet();
                            Dt_Niv_Psp_Egr.TableName = "Dt_Psp_Egr";
                            Ds_Registros_Ing.Tables.Add(Dt_Niv_Psp_Egr.Copy());
                            Exportar_Reporte(Ds_Registros_Ing, "Cr_Rpt_Psp_Presupuesto_Egr.rpt", "Rpt_Presupuesto_Egresos_" + Session.SessionID,
                                "xls", ExportFormatType.Excel, "Presupuestos");
                            Nom_Archivo = "../../Reporte/Rpt_Presupuesto_Egresos_" + Session.SessionID + ".xls     ";
                        }
                        else if (Tipo_Reporte.Trim().Equals("Word"))
                        {
                            Ds_Registros_Ing = new DataSet();
                            Dt_Niv_Psp_Egr.TableName = "Dt_Psp_Egr";
                            Ds_Registros_Ing.Tables.Add(Dt_Niv_Psp_Egr.Copy());
                            Exportar_Reporte(Ds_Registros_Ing, "Cr_Rpt_Psp_Presupuesto_Egr.rpt", "Rpt_Presupuesto_Egresos_" + Session.SessionID,
                                "doc", ExportFormatType.WordForWindows, "Presupuestos");
                            Nom_Archivo = "../../Reporte/Rpt_Presupuesto_Egresos_" + Session.SessionID + ".doc     ";
                        }
                    }

                    if (Dt_Ing != null && Dt_Ing.Rows.Count > 0)
                    {
                        Dt_Niv_Psp_Ing.Columns.Add("Concepto", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Aprobado", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Ampliacion", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Reduccion", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Modificado", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Devengado", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Recaudado", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Dev_Rec", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Comprometido", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Por_Recaudar", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Tipo", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Elaboro", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Dependencia", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Anio", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Gpo_Dependencia", System.Type.GetType("System.String"));

                        foreach (DataRow Dr in Dt_Ing.Rows)
                        {
                            Insertar = false;

                            if (Dr["Tipo"].ToString().Trim().Equals("Tot"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("FF") && Niv_FF_Ing.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("PP") && Niv_PP_Ing.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("R") && Niv_Rub.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("T") && Niv_Tip.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("CL") && Niv_Cla.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("C") && Niv_Con_Ing.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("SC") && Niv_SubCon.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else
                            {
                                Insertar = false;
                            }

                            if (Insertar)
                            {
                                Fila = Dt_Niv_Psp_Ing.NewRow();
                                Fila["Concepto"] = Dr["Concepto"].ToString().Trim();
                                Fila["Aprobado"] = Dr["Aprobado"].ToString().Trim();
                                Fila["Ampliacion"] = Dr["Ampliacion"].ToString().Trim();
                                Fila["Reduccion"] = Dr["Reduccion"].ToString().Trim();
                                Fila["Modificado"] = Dr["Modificado"].ToString().Trim();
                                Fila["Devengado"] = Dr["Devengado"].ToString().Trim();
                                Fila["Recaudado"] = Dr["Recaudado"].ToString().Trim();
                                Fila["Dev_Rec"] = Dr["Dev_Rec"].ToString().Trim();
                                Fila["Comprometido"] = Dr["Comprometido"].ToString().Trim();
                                Fila["Por_Recaudar"] = Dr["Por_Recaudar"].ToString().Trim();
                                Fila["Tipo"] = Dr["Tipo"].ToString().Trim();
                                Fila["Elaboro"] = Cls_Sessiones.Nombre_Empleado.Trim();
                                Fila["Dependencia"] = Dependencia.Trim();
                                Fila["Anio"] = "PRESUPUESTO DE INGRESOS EJERCICIO " + Anio.Trim();
                                Fila["Gpo_Dependencia"] = Gpo_Dep.Trim();
                                Dt_Niv_Psp_Ing.Rows.Add(Fila);
                            }
                        }

                        if (Tipo_Reporte.Trim().Equals("PDF"))
                        {
                            Ds_Registros_Ing = new DataSet();
                            Dt_Niv_Psp_Ing.TableName = "Dt_Psp_Ing";
                            Ds_Registros_Ing.Tables.Add(Dt_Niv_Psp_Ing.Copy());
                            //Generar_Reporte(ref Ds_Registros_Ing, "Cr_Rpt_Psp_Presupuesto_Ing.rpt", "Rpt_Presupuesto_Ingresos" + Session.SessionID + ".pdf");
                            Exportar_Reporte(Ds_Registros_Ing, "Cr_Rpt_Psp_Presupuesto_Ing.rpt", "Rpt_Presupuesto_Ingresos_" + Session.SessionID,
                                "pdf", ExportFormatType.PortableDocFormat, "Presupuestos");
                            Nom_Archivo = "../../Reporte/Rpt_Presupuesto_Ingresos_" + Session.SessionID + ".pdf     ";
                        }
                        else if (Tipo_Reporte.Trim().Equals("Excel"))
                        {
                            Ds_Registros_Ing = new DataSet();
                            Dt_Niv_Psp_Ing.TableName = "Dt_Psp_Ing";
                            Ds_Registros_Ing.Tables.Add(Dt_Niv_Psp_Ing.Copy());
                            Exportar_Reporte(Ds_Registros_Ing, "Cr_Rpt_Psp_Presupuesto_Ing.rpt", "Rpt_Presupuesto_Ingresos_" + Session.SessionID,
                                "xls", ExportFormatType.Excel, "Presupuestos");
                            Nom_Archivo = "../../Reporte/Rpt_Presupuesto_Ingresos_" + Session.SessionID + ".xls    ";
                        }
                        else if (Tipo_Reporte.Trim().Equals("Word"))
                        {
                            Ds_Registros_Ing = new DataSet();
                            Dt_Niv_Psp_Ing.TableName = "Dt_Psp_Ing";
                            Ds_Registros_Ing.Tables.Add(Dt_Niv_Psp_Ing.Copy());
                            Exportar_Reporte(Ds_Registros_Ing, "Cr_Rpt_Psp_Presupuesto_Ing.rpt", "Rpt_Presupuesto_Ingresos_" + Session.SessionID,
                                "doc", ExportFormatType.WordForWindows, "Presupuestos");
                            Nom_Archivo = "../../Reporte/Rpt_Presupuesto_Ingresos_" + Session.SessionID + ".doc     ";
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al obtener los parametros. Error[" + ex.Message + "]");
                }
                return Nom_Archivo;
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Generar_Dt_Psp_Egr
            ///DESCRIPCIÓN          : Metodo para generar la tabla del presupuesto de egresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 30/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private DataTable Generar_Dt_Psp_Egr(DataTable Dt_Egr)
            {
                DataTable Dt_Psp = new DataTable();
                DataRow Fila;

                #region (variables)
                Double Tot_Est = 0.00;
                Double Tot_Amp = 0.00;
                Double Tot_Red = 0.00;
                Double Tot_Mod = 0.00;
                Double Tot_Dev = 0.00;
                Double Tot_Pag = 0.00;
                Double Tot_Dev_Rec = 0.00;
                Double Tot_Com = 0.00;
                Double Tot_xEje = 0.00;
                Double Tot_PreCom = 0.00;
                Double Tot_Ej = 0.00;

                Double FF_Est = 0.00;
                Double FF_Amp = 0.00;
                Double FF_Red = 0.00;
                Double FF_Mod = 0.00;
                Double FF_Dev = 0.00;
                Double FF_Pag = 0.00;
                Double FF_Dev_Rec = 0.00;
                Double FF_Com = 0.00;
                Double FF_xEje = 0.00;
                Double FF_PreCom = 0.00;
                Double FF_Ej = 0.00;

                Double AF_Est = 0.00;
                Double AF_Amp = 0.00;
                Double AF_Red = 0.00;
                Double AF_Mod = 0.00;
                Double AF_Dev = 0.00;
                Double AF_Pag = 0.00;
                Double AF_Dev_Rec = 0.00;
                Double AF_Com = 0.00;
                Double AF_xEje = 0.00;
                Double AF_PreCom = 0.00;
                Double AF_Ej = 0.00;

                Double PP_Est = 0.00;
                Double PP_Amp = 0.00;
                Double PP_Red = 0.00;
                Double PP_Mod = 0.00;
                Double PP_Dev = 0.00;
                Double PP_Pag = 0.00;
                Double PP_Dev_Rec = 0.00;
                Double PP_Com = 0.00;
                Double PP_xEje = 0.00;
                Double PP_PreCom = 0.00;
                Double PP_Ej = 0.00;

                Double UR_Est = 0.00;
                Double UR_Amp = 0.00;
                Double UR_Red = 0.00;
                Double UR_Mod = 0.00;
                Double UR_Dev = 0.00;
                Double UR_Pag = 0.00;
                Double UR_Dev_Rec = 0.00;
                Double UR_Com = 0.00;
                Double UR_xEje = 0.00;
                Double UR_PreCom = 0.00;
                Double UR_Ej = 0.00;

                Double Cap_Est = 0.00;
                Double Cap_Amp = 0.00;
                Double Cap_Red = 0.00;
                Double Cap_Mod = 0.00;
                Double Cap_Dev = 0.00;
                Double Cap_Pag = 0.00;
                Double Cap_Dev_Rec = 0.00;
                Double Cap_Com = 0.00;
                Double Cap_xEje = 0.00;
                Double Cap_PreCom = 0.00;
                Double Cap_Ej = 0.00;

                Double Con_Est = 0.00;
                Double Con_Amp = 0.00;
                Double Con_Red = 0.00;
                Double Con_Mod = 0.00;
                Double Con_Dev = 0.00;
                Double Con_Pag = 0.00;
                Double Con_Dev_Rec = 0.00;
                Double Con_Com = 0.00;
                Double Con_xEje = 0.00;
                Double Con_PreCom = 0.00;
                Double Con_Ej = 0.00;

                Double PG_Est = 0.00;
                Double PG_Amp = 0.00;
                Double PG_Red = 0.00;
                Double PG_Mod = 0.00;
                Double PG_Dev = 0.00;
                Double PG_Pag = 0.00;
                Double PG_Dev_Rec = 0.00;
                Double PG_Com = 0.00;
                Double PG_xEje = 0.00;
                Double PG_PreCom = 0.00;
                Double PG_Ej = 0.00;
                #endregion

                try
                {
                    #region(Tabla)
                    String FF_ID = Dt_Egr.Rows[0]["FF_ID"].ToString().Trim();
                    String AF_ID = Dt_Egr.Rows[0]["AF_ID"].ToString().Trim();
                    String PP_ID = Dt_Egr.Rows[0]["PP_ID"].ToString().Trim();
                    String UR_ID = Dt_Egr.Rows[0]["UR_ID"].ToString().Trim();
                    String Cap_ID = Dt_Egr.Rows[0]["CA_ID"].ToString().Trim();
                    String Con_ID = Dt_Egr.Rows[0]["CO_ID"].ToString().Trim();
                    String PG_ID = Dt_Egr.Rows[0]["PG_ID"].ToString().Trim();

                    String FF = "******* " + Dt_Egr.Rows[0]["CLAVE_NOM_FF"].ToString().Trim();
                    String AF = "****** " + Dt_Egr.Rows[0]["CLAVE_NOM_AF"].ToString().Trim();
                    String PP = "***** " + Dt_Egr.Rows[0]["CLAVE_NOM_PP"].ToString().Trim();
                    String UR = "**** " + Dt_Egr.Rows[0]["CLAVE_NOM_UR"].ToString().Trim();
                    String Cap = "*** " + Dt_Egr.Rows[0]["CLAVE_NOM_CAPITULO"].ToString().Trim();
                    String Con = "** " + Dt_Egr.Rows[0]["CLAVE_NOM_CONCEPTO"].ToString().Trim();
                    String PG = "* " + Dt_Egr.Rows[0]["CLAVE_NOM_PARTIDA_GEN"].ToString().Trim();

                    Dt_Psp.Columns.Add("Concepto", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Aprobado", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Ampliacion", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Reduccion", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Modificado", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Devengado", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Recaudado", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Dev_Rec", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Comprometido", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Por_Recaudar", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Tipo", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Pre_Comprometido", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Ejercido", System.Type.GetType("System.String"));

                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = "******** TOTAL";
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "Tot";
                    Fila["Pre_Comprometido"] = String.Empty;
                    Fila["Ejercido"] = String.Empty;
                    Dt_Psp.Rows.Add(Fila);

                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = FF.Trim();
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "FF_Tot";
                    Fila["Pre_Comprometido"] = String.Empty;
                    Fila["Ejercido"] = String.Empty;
                    Dt_Psp.Rows.Add(Fila);

                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = AF.Trim();
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "AF_Tot";
                    Fila["Pre_Comprometido"] = String.Empty;
                    Fila["Ejercido"] = String.Empty;
                    Dt_Psp.Rows.Add(Fila);


                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = PP.Trim();
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "PP_Tot";
                    Fila["Pre_Comprometido"] = String.Empty;
                    Fila["Ejercido"] = String.Empty;
                    Dt_Psp.Rows.Add(Fila);


                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = UR.Trim();
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "UR_Tot";
                    Fila["Pre_Comprometido"] = String.Empty;
                    Fila["Ejercido"] = String.Empty;
                    Dt_Psp.Rows.Add(Fila);

                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = Cap.Trim();
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "Cap_Tot";
                    Fila["Pre_Comprometido"] = String.Empty;
                    Fila["Ejercido"] = String.Empty;
                    Dt_Psp.Rows.Add(Fila);

                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = Con.Trim();
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "Con_Tot";
                    Fila["Pre_Comprometido"] = String.Empty;
                    Fila["Ejercido"] = String.Empty;
                    Dt_Psp.Rows.Add(Fila);

                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = PG.Trim();
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "PG_Tot";
                    Fila["Pre_Comprometido"] = String.Empty;
                    Fila["Ejercido"] = String.Empty;
                    Dt_Psp.Rows.Add(Fila);

                    #endregion

                    foreach (DataRow Dr in Dt_Egr.Rows)
                    {
                        Tot_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                        Tot_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                        Tot_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                        Tot_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                        Tot_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                        Tot_Pag += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                        Tot_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                        Tot_Com += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                        Tot_xEje += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                        Tot_PreCom += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                        Tot_Ej += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                        if (FF_ID.Trim().Equals(Dr["FF_ID"].ToString().Trim()))
                        {
                            //sumamos 
                            FF_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                            FF_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            FF_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            FF_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            FF_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            FF_Pag += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                            FF_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                            FF_Com += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                            FF_xEje += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                            FF_PreCom += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                            FF_Ej += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                            if (AF_ID.Trim().Equals(Dr["AF_ID"].ToString().Trim()))
                            {
                                //sumamos 
                                AF_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                AF_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                AF_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                AF_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                AF_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                AF_Pag += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                AF_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                AF_Com += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                AF_xEje += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                AF_PreCom += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                AF_Ej += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                if (PP_ID.Trim().Equals(Dr["PP_ID"].ToString().Trim()))
                                {
                                    //sumamos 
                                    PP_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                    PP_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                    PP_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                    PP_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                    PP_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                    PP_Pag += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                    PP_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                    PP_Com += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                    PP_xEje += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                    PP_PreCom += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                    PP_Ej += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                    if (UR_ID.Trim().Equals(Dr["UR_ID"].ToString().Trim()))
                                    {
                                        //sumamos 
                                        UR_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                        UR_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                        UR_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                        UR_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                        UR_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                        UR_Pag += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                        UR_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                        UR_Com += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                        UR_xEje += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                        UR_PreCom += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                        UR_Ej += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                        if (Cap_ID.Trim().Equals(Dr["CA_ID"].ToString().Trim()))
                                        {
                                            //sumamos 
                                            Cap_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                            Cap_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                            Cap_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                            Cap_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                            Cap_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                            Cap_Pag += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                            Cap_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                            Cap_Com += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                            Cap_xEje += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                            Cap_PreCom += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                            Cap_Ej += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                            if (Con_ID.Trim().Equals(Dr["CO_ID"].ToString().Trim()))
                                            {
                                                Con_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                                Con_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                                Con_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                                Con_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                                Con_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                                Con_Pag += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                                Con_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                                Con_Com += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                                Con_xEje += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                                Con_PreCom += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                                Con_Ej += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                                if (PG_ID.Trim().Equals(Dr["PG_ID"].ToString().Trim()))
                                                {
                                                    PG_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                                    PG_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                                    PG_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                                    PG_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                                    PG_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                                    PG_Pag += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                                    PG_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                                    PG_Com += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                                    PG_xEje += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                                    PG_PreCom += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                                    PG_Ej += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                                    Fila = Dt_Psp.NewRow();
                                                    Fila["Concepto"] = Dr["CLAVE_NOM_PARTIDA"].ToString().Trim();
                                                    Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                                                    Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                                    Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                                    Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                                    Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                                    Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim()));
                                                    Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim()));
                                                    Fila["Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                                                    Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                                                    Fila["Tipo"] = "P";
                                                    Fila["Pre_Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim()));
                                                    Fila["Ejercido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim()));
                                                    Dt_Psp.Rows.Add(Fila);
                                                }
                                                else
                                                {
                                                    #region (else PG)
                                                    foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                                                    {
                                                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("PG_Tot"))
                                                        {
                                                            Dr_Psp["Aprobado"] = String.Format("{0:n}", PG_Est);
                                                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", PG_Amp);
                                                            Dr_Psp["Reduccion"] = String.Format("{0:n}", PG_Red);
                                                            Dr_Psp["Modificado"] = String.Format("{0:n}", PG_Mod);
                                                            Dr_Psp["Devengado"] = String.Format("{0:n}", PG_Dev);
                                                            Dr_Psp["Recaudado"] = String.Format("{0:n}", PG_Pag);
                                                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PG_Dev_Rec);
                                                            Dr_Psp["Comprometido"] = String.Format("{0:n}", PG_Com);
                                                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PG_xEje);
                                                            Dr_Psp["Tipo"] = "PG";
                                                            Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", PG_PreCom);
                                                            Dr_Psp["Ejercido"] = String.Format("{0:n}", PG_Ej);
                                                        }
                                                    }//fin foreach

                                                    //limpiamos las variables
                                                    PG_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                                    PG_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                                    PG_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                                    PG_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                                    PG_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                                    PG_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                                    PG_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                                    PG_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                                    PG_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                                    PG_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                                    PG_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                                    PG_ID = Dr["PG_ID"].ToString().Trim();
                                                    PG = "* " + Dr["CLAVE_NOM_PARTIDA_GEN"].ToString().Trim();

                                                    Fila = Dt_Psp.NewRow();
                                                    Fila["Concepto"] = PG.Trim();
                                                    Fila["Aprobado"] = String.Empty;
                                                    Fila["Ampliacion"] = String.Empty;
                                                    Fila["Reduccion"] = String.Empty;
                                                    Fila["Modificado"] = String.Empty;
                                                    Fila["Devengado"] = String.Empty;
                                                    Fila["Recaudado"] = String.Empty;
                                                    Fila["Dev_Rec"] = String.Empty;
                                                    Fila["Comprometido"] = String.Empty;
                                                    Fila["Por_Recaudar"] = String.Empty;
                                                    Fila["Tipo"] = "PG_Tot";
                                                    Fila["Pre_Comprometido"] = String.Empty;
                                                    Fila["Ejercido"] = String.Empty;
                                                    Dt_Psp.Rows.Add(Fila);

                                                    Fila = Dt_Psp.NewRow();
                                                    Fila["Concepto"] = Dr["CLAVE_NOM_PARTIDA"].ToString().Trim();
                                                    Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                                                    Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                                    Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                                    Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                                    Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                                    Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim()));
                                                    Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim()));
                                                    Fila["Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                                                    Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                                                    Fila["Tipo"] = "P";
                                                    Fila["Pre_Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim()));
                                                    Fila["Ejercido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim()));
                                                    Dt_Psp.Rows.Add(Fila);
                                                    #endregion
                                                }
                                            }
                                            else
                                            {
                                                #region (else Con)
                                                foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                                                {
                                                    if (Dr_Psp["Tipo"].ToString().Trim().Equals("Con_Tot"))
                                                    {
                                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", Con_Est);
                                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", Con_Amp);
                                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", Con_Red);
                                                        Dr_Psp["Modificado"] = String.Format("{0:n}", Con_Mod);
                                                        Dr_Psp["Devengado"] = String.Format("{0:n}", Con_Dev);
                                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", Con_Pag);
                                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Con_Dev_Rec);
                                                        Dr_Psp["Comprometido"] = String.Format("{0:n}", Con_Com);
                                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Con_xEje);
                                                        Dr_Psp["Tipo"] = "Con";
                                                        Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", Con_PreCom);
                                                        Dr_Psp["Ejercido"] = String.Format("{0:n}", Con_Ej);
                                                    }
                                                    else if (Dr_Psp["Tipo"].ToString().Trim().Equals("PG_Tot"))
                                                    {
                                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", PG_Est);
                                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", PG_Amp);
                                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", PG_Red);
                                                        Dr_Psp["Modificado"] = String.Format("{0:n}", PG_Mod);
                                                        Dr_Psp["Devengado"] = String.Format("{0:n}", PG_Dev);
                                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", PG_Pag);
                                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PG_Dev_Rec);
                                                        Dr_Psp["Comprometido"] = String.Format("{0:n}", PG_Com);
                                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PG_xEje);
                                                        Dr_Psp["Tipo"] = "PG";
                                                        Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", PG_PreCom);
                                                        Dr_Psp["Ejercido"] = String.Format("{0:n}", PG_Ej);
                                                    }
                                                }//fin foreach

                                                //limpiamos las variables
                                                Con_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                                Con_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                                Con_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                                Con_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                                Con_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                                Con_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                                Con_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                                Con_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                                Con_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                                Con_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                                Con_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                                PG_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                                PG_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                                PG_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                                PG_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                                PG_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                                PG_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                                PG_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                                PG_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                                PG_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                                PG_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                                PG_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                                Con_ID = Dr["CO_ID"].ToString().Trim();
                                                Con = "** " + Dr["CLAVE_NOM_CONCEPTO"].ToString().Trim();
                                                PG_ID = Dr["PG_ID"].ToString().Trim();
                                                PG = "* " + Dr["CLAVE_NOM_PARTIDA_GEN"].ToString().Trim();

                                                Fila = Dt_Psp.NewRow();
                                                Fila["Concepto"] = Con.Trim();
                                                Fila["Aprobado"] = String.Empty;
                                                Fila["Ampliacion"] = String.Empty;
                                                Fila["Reduccion"] = String.Empty;
                                                Fila["Modificado"] = String.Empty;
                                                Fila["Devengado"] = String.Empty;
                                                Fila["Recaudado"] = String.Empty;
                                                Fila["Dev_Rec"] = String.Empty;
                                                Fila["Comprometido"] = String.Empty;
                                                Fila["Por_Recaudar"] = String.Empty;
                                                Fila["Tipo"] = "Con_Tot";
                                                Fila["Pre_Comprometido"] = String.Empty;
                                                Fila["Ejercido"] = String.Empty;
                                                Dt_Psp.Rows.Add(Fila);

                                                Fila = Dt_Psp.NewRow();
                                                Fila["Concepto"] = PG.Trim();
                                                Fila["Aprobado"] = String.Empty;
                                                Fila["Ampliacion"] = String.Empty;
                                                Fila["Reduccion"] = String.Empty;
                                                Fila["Modificado"] = String.Empty;
                                                Fila["Devengado"] = String.Empty;
                                                Fila["Recaudado"] = String.Empty;
                                                Fila["Dev_Rec"] = String.Empty;
                                                Fila["Comprometido"] = String.Empty;
                                                Fila["Por_Recaudar"] = String.Empty;
                                                Fila["Tipo"] = "PG_Tot";
                                                Fila["Pre_Comprometido"] = String.Empty;
                                                Fila["Ejercido"] = String.Empty;
                                                Dt_Psp.Rows.Add(Fila);

                                                Fila = Dt_Psp.NewRow();
                                                Fila["Concepto"] = Dr["CLAVE_NOM_PARTIDA"].ToString().Trim();
                                                Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                                                Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                                Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                                Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                                Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                                Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim()));
                                                Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim()));
                                                Fila["Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                                                Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                                                Fila["Tipo"] = "P";
                                                Fila["Pre_Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim()));
                                                Fila["Ejercido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim()));
                                                Dt_Psp.Rows.Add(Fila);
                                                #endregion
                                            }
                                        }
                                        else
                                        {
                                            #region (else Cap)
                                            foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                                            {
                                                if (Dr_Psp["Tipo"].ToString().Trim().Equals("Cap_Tot"))
                                                {
                                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", Cap_Est);
                                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", Cap_Amp);
                                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", Cap_Red);
                                                    Dr_Psp["Modificado"] = String.Format("{0:n}", Cap_Mod);
                                                    Dr_Psp["Devengado"] = String.Format("{0:n}", Cap_Dev);
                                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", Cap_Pag);
                                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Cap_Dev_Rec);
                                                    Dr_Psp["Comprometido"] = String.Format("{0:n}", Cap_Com);
                                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Cap_xEje);
                                                    Dr_Psp["Tipo"] = "Cap";
                                                    Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", Cap_PreCom);
                                                    Dr_Psp["Ejercido"] = String.Format("{0:n}", Cap_Ej);
                                                }
                                                else if (Dr_Psp["Tipo"].ToString().Trim().Equals("Con_Tot"))
                                                {
                                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", Con_Est);
                                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", Con_Amp);
                                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", Con_Red);
                                                    Dr_Psp["Modificado"] = String.Format("{0:n}", Con_Mod);
                                                    Dr_Psp["Devengado"] = String.Format("{0:n}", Con_Dev);
                                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", Con_Pag);
                                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Con_Dev_Rec);
                                                    Dr_Psp["Comprometido"] = String.Format("{0:n}", Con_Com);
                                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Con_xEje);
                                                    Dr_Psp["Tipo"] = "Con";
                                                    Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", Con_PreCom);
                                                    Dr_Psp["Ejercido"] = String.Format("{0:n}", Con_Ej);
                                                }
                                                else if (Dr_Psp["Tipo"].ToString().Trim().Equals("PG_Tot"))
                                                {
                                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", PG_Est);
                                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", PG_Amp);
                                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", PG_Red);
                                                    Dr_Psp["Modificado"] = String.Format("{0:n}", PG_Mod);
                                                    Dr_Psp["Devengado"] = String.Format("{0:n}", PG_Dev);
                                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", PG_Pag);
                                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PG_Dev_Rec);
                                                    Dr_Psp["Comprometido"] = String.Format("{0:n}", PG_Com);
                                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PG_xEje);
                                                    Dr_Psp["Tipo"] = "PG";
                                                    Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", PG_PreCom);
                                                    Dr_Psp["Ejercido"] = String.Format("{0:n}", PG_Ej);
                                                }

                                            }//fin foreach

                                            //limpiamos las variables
                                            Cap_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                            Cap_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                            Cap_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                            Cap_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                            Cap_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                            Cap_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                            Cap_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                            Cap_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                            Cap_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                            Cap_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                            Cap_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                            Con_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                            Con_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                            Con_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                            Con_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                            Con_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                            Con_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                            Con_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                            Con_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                            Con_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                            Con_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                            Con_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                            PG_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                            PG_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                            PG_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                            PG_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                            PG_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                            PG_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                            PG_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                            PG_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                            PG_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                            PG_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                            PG_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                            Cap_ID = Dr["CA_ID"].ToString().Trim();
                                            Cap = "*** " + Dr["CLAVE_NOM_CAPITULO"].ToString().Trim();
                                            Con_ID = Dr["CO_ID"].ToString().Trim();
                                            Con = "** " + Dr["CLAVE_NOM_CONCEPTO"].ToString().Trim();
                                            PG_ID = Dr["PG_ID"].ToString().Trim();
                                            PG = "* " + Dr["CLAVE_NOM_PARTIDA_GEN"].ToString().Trim();

                                            Fila = Dt_Psp.NewRow();
                                            Fila["Concepto"] = Cap.Trim();
                                            Fila["Aprobado"] = String.Empty;
                                            Fila["Ampliacion"] = String.Empty;
                                            Fila["Reduccion"] = String.Empty;
                                            Fila["Modificado"] = String.Empty;
                                            Fila["Devengado"] = String.Empty;
                                            Fila["Recaudado"] = String.Empty;
                                            Fila["Dev_Rec"] = String.Empty;
                                            Fila["Comprometido"] = String.Empty;
                                            Fila["Por_Recaudar"] = String.Empty;
                                            Fila["Tipo"] = "Cap_Tot";
                                            Fila["Pre_Comprometido"] = String.Empty;
                                            Fila["Ejercido"] = String.Empty;
                                            Dt_Psp.Rows.Add(Fila);

                                            Fila = Dt_Psp.NewRow();
                                            Fila["Concepto"] = Con.Trim();
                                            Fila["Aprobado"] = String.Empty;
                                            Fila["Ampliacion"] = String.Empty;
                                            Fila["Reduccion"] = String.Empty;
                                            Fila["Modificado"] = String.Empty;
                                            Fila["Devengado"] = String.Empty;
                                            Fila["Recaudado"] = String.Empty;
                                            Fila["Dev_Rec"] = String.Empty;
                                            Fila["Comprometido"] = String.Empty;
                                            Fila["Por_Recaudar"] = String.Empty;
                                            Fila["Tipo"] = "Con_Tot";
                                            Fila["Pre_Comprometido"] = String.Empty;
                                            Fila["Ejercido"] = String.Empty;
                                            Dt_Psp.Rows.Add(Fila);

                                            Fila = Dt_Psp.NewRow();
                                            Fila["Concepto"] = PG.Trim();
                                            Fila["Aprobado"] = String.Empty;
                                            Fila["Ampliacion"] = String.Empty;
                                            Fila["Reduccion"] = String.Empty;
                                            Fila["Modificado"] = String.Empty;
                                            Fila["Devengado"] = String.Empty;
                                            Fila["Recaudado"] = String.Empty;
                                            Fila["Dev_Rec"] = String.Empty;
                                            Fila["Comprometido"] = String.Empty;
                                            Fila["Por_Recaudar"] = String.Empty;
                                            Fila["Tipo"] = "PG_Tot";
                                            Fila["Pre_Comprometido"] = String.Empty;
                                            Fila["Ejercido"] = String.Empty;
                                            Dt_Psp.Rows.Add(Fila);

                                            Fila = Dt_Psp.NewRow();
                                            Fila["Concepto"] = Dr["CLAVE_NOM_PARTIDA"].ToString().Trim();
                                            Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                                            Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                            Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                            Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                            Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                            Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim()));
                                            Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim()));
                                            Fila["Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                                            Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                                            Fila["Tipo"] = "P";
                                            Fila["Pre_Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim()));
                                            Fila["Ejercido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim()));
                                            Dt_Psp.Rows.Add(Fila);
                                            #endregion
                                        }
                                    }
                                    else
                                    {
                                        #region (else ur)
                                        foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                                        {
                                            if (Dr_Psp["Tipo"].ToString().Trim().Equals("UR_Tot"))
                                            {
                                                Dr_Psp["Aprobado"] = String.Format("{0:n}", UR_Est);
                                                Dr_Psp["Ampliacion"] = String.Format("{0:n}", UR_Amp);
                                                Dr_Psp["Reduccion"] = String.Format("{0:n}", UR_Red);
                                                Dr_Psp["Modificado"] = String.Format("{0:n}", UR_Mod);
                                                Dr_Psp["Devengado"] = String.Format("{0:n}", UR_Dev);
                                                Dr_Psp["Recaudado"] = String.Format("{0:n}", UR_Pag);
                                                Dr_Psp["Dev_Rec"] = String.Format("{0:n}", UR_Dev_Rec);
                                                Dr_Psp["Comprometido"] = String.Format("{0:n}", UR_Com);
                                                Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", UR_xEje);
                                                Dr_Psp["Tipo"] = "UR";
                                                Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", UR_PreCom);
                                                Dr_Psp["Ejercido"] = String.Format("{0:n}", UR_Ej);
                                            }
                                            else if (Dr_Psp["Tipo"].ToString().Trim().Equals("Cap_Tot"))
                                            {
                                                Dr_Psp["Aprobado"] = String.Format("{0:n}", Cap_Est);
                                                Dr_Psp["Ampliacion"] = String.Format("{0:n}", Cap_Amp);
                                                Dr_Psp["Reduccion"] = String.Format("{0:n}", Cap_Red);
                                                Dr_Psp["Modificado"] = String.Format("{0:n}", Cap_Mod);
                                                Dr_Psp["Devengado"] = String.Format("{0:n}", Cap_Dev);
                                                Dr_Psp["Recaudado"] = String.Format("{0:n}", Cap_Pag);
                                                Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Cap_Dev_Rec);
                                                Dr_Psp["Comprometido"] = String.Format("{0:n}", Cap_Com);
                                                Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Cap_xEje);
                                                Dr_Psp["Tipo"] = "Cap";
                                                Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", Cap_PreCom);
                                                Dr_Psp["Ejercido"] = String.Format("{0:n}", Cap_Ej);
                                            }
                                            else if (Dr_Psp["Tipo"].ToString().Trim().Equals("Con_Tot"))
                                            {
                                                Dr_Psp["Aprobado"] = String.Format("{0:n}", Con_Est);
                                                Dr_Psp["Ampliacion"] = String.Format("{0:n}", Con_Amp);
                                                Dr_Psp["Reduccion"] = String.Format("{0:n}", Con_Red);
                                                Dr_Psp["Modificado"] = String.Format("{0:n}", Con_Mod);
                                                Dr_Psp["Devengado"] = String.Format("{0:n}", Con_Dev);
                                                Dr_Psp["Recaudado"] = String.Format("{0:n}", Con_Pag);
                                                Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Con_Dev_Rec);
                                                Dr_Psp["Comprometido"] = String.Format("{0:n}", Con_Com);
                                                Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Con_xEje);
                                                Dr_Psp["Tipo"] = "Con";
                                                Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", Con_PreCom);
                                                Dr_Psp["Ejercido"] = String.Format("{0:n}", Con_Ej);
                                            }
                                            else if (Dr_Psp["Tipo"].ToString().Trim().Equals("PG_Tot"))
                                            {
                                                Dr_Psp["Aprobado"] = String.Format("{0:n}", PG_Est);
                                                Dr_Psp["Ampliacion"] = String.Format("{0:n}", PG_Amp);
                                                Dr_Psp["Reduccion"] = String.Format("{0:n}", PG_Red);
                                                Dr_Psp["Modificado"] = String.Format("{0:n}", PG_Mod);
                                                Dr_Psp["Devengado"] = String.Format("{0:n}", PG_Dev);
                                                Dr_Psp["Recaudado"] = String.Format("{0:n}", PG_Pag);
                                                Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PG_Dev_Rec);
                                                Dr_Psp["Comprometido"] = String.Format("{0:n}", PG_Com);
                                                Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PG_xEje);
                                                Dr_Psp["Tipo"] = "PG";
                                                Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", PG_PreCom);
                                                Dr_Psp["Ejercido"] = String.Format("{0:n}", PG_Ej);
                                            }

                                        }//fin foreach

                                        //limpiamos las variables
                                        UR_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                        UR_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                        UR_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                        UR_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                        UR_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                        UR_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                        UR_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                        UR_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                        UR_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                        UR_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                        UR_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                        Cap_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                        Cap_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                        Cap_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                        Cap_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                        Cap_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                        Cap_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                        Cap_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                        Cap_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                        Cap_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                        Cap_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                        Cap_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                        Con_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                        Con_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                        Con_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                        Con_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                        Con_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                        Con_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                        Con_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                        Con_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                        Con_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                        Con_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                        Con_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                        PG_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                        PG_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                        PG_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                        PG_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                        PG_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                        PG_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                        PG_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                        PG_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                        PG_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                        PG_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                        PG_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                        UR_ID = Dr["UR_ID"].ToString().Trim();
                                        UR = "**** " + Dr["CLAVE_NOM_UR"].ToString().Trim();
                                        Cap_ID = Dr["CA_ID"].ToString().Trim();
                                        Cap = "*** " + Dr["CLAVE_NOM_CAPITULO"].ToString().Trim();
                                        Con_ID = Dr["CO_ID"].ToString().Trim();
                                        Con = "** " + Dr["CLAVE_NOM_CONCEPTO"].ToString().Trim();
                                        PG_ID = Dr["PG_ID"].ToString().Trim();
                                        PG = "* " + Dr["CLAVE_NOM_PARTIDA_GEN"].ToString().Trim();

                                        Fila = Dt_Psp.NewRow();
                                        Fila["Concepto"] = UR.Trim();
                                        Fila["Aprobado"] = String.Empty;
                                        Fila["Ampliacion"] = String.Empty;
                                        Fila["Reduccion"] = String.Empty;
                                        Fila["Modificado"] = String.Empty;
                                        Fila["Devengado"] = String.Empty;
                                        Fila["Recaudado"] = String.Empty;
                                        Fila["Dev_Rec"] = String.Empty;
                                        Fila["Comprometido"] = String.Empty;
                                        Fila["Por_Recaudar"] = String.Empty;
                                        Fila["Tipo"] = "UR_Tot";
                                        Fila["Pre_Comprometido"] = String.Empty;
                                        Fila["Ejercido"] = String.Empty;
                                        Dt_Psp.Rows.Add(Fila);

                                        Fila = Dt_Psp.NewRow();
                                        Fila["Concepto"] = Cap.Trim();
                                        Fila["Aprobado"] = String.Empty;
                                        Fila["Ampliacion"] = String.Empty;
                                        Fila["Reduccion"] = String.Empty;
                                        Fila["Modificado"] = String.Empty;
                                        Fila["Devengado"] = String.Empty;
                                        Fila["Recaudado"] = String.Empty;
                                        Fila["Dev_Rec"] = String.Empty;
                                        Fila["Comprometido"] = String.Empty;
                                        Fila["Por_Recaudar"] = String.Empty;
                                        Fila["Tipo"] = "Cap_Tot";
                                        Fila["Pre_Comprometido"] = String.Empty;
                                        Fila["Ejercido"] = String.Empty;
                                        Dt_Psp.Rows.Add(Fila);

                                        Fila = Dt_Psp.NewRow();
                                        Fila["Concepto"] = Con.Trim();
                                        Fila["Aprobado"] = String.Empty;
                                        Fila["Ampliacion"] = String.Empty;
                                        Fila["Reduccion"] = String.Empty;
                                        Fila["Modificado"] = String.Empty;
                                        Fila["Devengado"] = String.Empty;
                                        Fila["Recaudado"] = String.Empty;
                                        Fila["Dev_Rec"] = String.Empty;
                                        Fila["Comprometido"] = String.Empty;
                                        Fila["Por_Recaudar"] = String.Empty;
                                        Fila["Tipo"] = "Con_Tot";
                                        Fila["Pre_Comprometido"] = String.Empty;
                                        Fila["Ejercido"] = String.Empty;
                                        Dt_Psp.Rows.Add(Fila);

                                        Fila = Dt_Psp.NewRow();
                                        Fila["Concepto"] = PG.Trim();
                                        Fila["Aprobado"] = String.Empty;
                                        Fila["Ampliacion"] = String.Empty;
                                        Fila["Reduccion"] = String.Empty;
                                        Fila["Modificado"] = String.Empty;
                                        Fila["Devengado"] = String.Empty;
                                        Fila["Recaudado"] = String.Empty;
                                        Fila["Dev_Rec"] = String.Empty;
                                        Fila["Comprometido"] = String.Empty;
                                        Fila["Por_Recaudar"] = String.Empty;
                                        Fila["Tipo"] = "PG_Tot";
                                        Fila["Pre_Comprometido"] = String.Empty;
                                        Fila["Ejercido"] = String.Empty;
                                        Dt_Psp.Rows.Add(Fila);

                                        Fila = Dt_Psp.NewRow();
                                        Fila["Concepto"] = Dr["CLAVE_NOM_PARTIDA"].ToString().Trim();
                                        Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                                        Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                        Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                        Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                        Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                        Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim()));
                                        Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim()));
                                        Fila["Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                                        Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                                        Fila["Tipo"] = "P";
                                        Fila["Pre_Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim()));
                                        Fila["Ejercido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim()));
                                        Dt_Psp.Rows.Add(Fila);
                                        #endregion
                                    }
                                }
                                else
                                {
                                    #region (else pp)
                                    foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                                    {
                                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("PP_Tot"))
                                        {
                                            Dr_Psp["Aprobado"] = String.Format("{0:n}", PP_Est);
                                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", PP_Amp);
                                            Dr_Psp["Reduccion"] = String.Format("{0:n}", PP_Red);
                                            Dr_Psp["Modificado"] = String.Format("{0:n}", PP_Mod);
                                            Dr_Psp["Devengado"] = String.Format("{0:n}", PP_Dev);
                                            Dr_Psp["Recaudado"] = String.Format("{0:n}", PP_Pag);
                                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PP_Dev_Rec);
                                            Dr_Psp["Comprometido"] = String.Format("{0:n}", PP_Com);
                                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PP_xEje);
                                            Dr_Psp["Tipo"] = "PP";
                                            Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", PP_PreCom);
                                            Dr_Psp["Ejercido"] = String.Format("{0:n}", PP_Ej);
                                        }
                                        else if (Dr_Psp["Tipo"].ToString().Trim().Equals("UR_Tot"))
                                        {
                                            Dr_Psp["Aprobado"] = String.Format("{0:n}", UR_Est);
                                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", UR_Amp);
                                            Dr_Psp["Reduccion"] = String.Format("{0:n}", UR_Red);
                                            Dr_Psp["Modificado"] = String.Format("{0:n}", UR_Mod);
                                            Dr_Psp["Devengado"] = String.Format("{0:n}", UR_Dev);
                                            Dr_Psp["Recaudado"] = String.Format("{0:n}", UR_Pag);
                                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", UR_Dev_Rec);
                                            Dr_Psp["Comprometido"] = String.Format("{0:n}", UR_Com);
                                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", UR_xEje);
                                            Dr_Psp["Tipo"] = "UR";
                                            Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", UR_PreCom);
                                            Dr_Psp["Ejercido"] = String.Format("{0:n}", UR_Ej);
                                        }
                                        else if (Dr_Psp["Tipo"].ToString().Trim().Equals("Cap_Tot"))
                                        {
                                            Dr_Psp["Aprobado"] = String.Format("{0:n}", Cap_Est);
                                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", Cap_Amp);
                                            Dr_Psp["Reduccion"] = String.Format("{0:n}", Cap_Red);
                                            Dr_Psp["Modificado"] = String.Format("{0:n}", Cap_Mod);
                                            Dr_Psp["Devengado"] = String.Format("{0:n}", Cap_Dev);
                                            Dr_Psp["Recaudado"] = String.Format("{0:n}", Cap_Pag);
                                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Cap_Dev_Rec);
                                            Dr_Psp["Comprometido"] = String.Format("{0:n}", Cap_Com);
                                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Cap_xEje);
                                            Dr_Psp["Tipo"] = "Cap";
                                            Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", Cap_PreCom);
                                            Dr_Psp["Ejercido"] = String.Format("{0:n}", Cap_Ej);
                                        }
                                        else if (Dr_Psp["Tipo"].ToString().Trim().Equals("Con_Tot"))
                                        {
                                            Dr_Psp["Aprobado"] = String.Format("{0:n}", Con_Est);
                                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", Con_Amp);
                                            Dr_Psp["Reduccion"] = String.Format("{0:n}", Con_Red);
                                            Dr_Psp["Modificado"] = String.Format("{0:n}", Con_Mod);
                                            Dr_Psp["Devengado"] = String.Format("{0:n}", Con_Dev);
                                            Dr_Psp["Recaudado"] = String.Format("{0:n}", Con_Pag);
                                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Con_Dev_Rec);
                                            Dr_Psp["Comprometido"] = String.Format("{0:n}", Con_Com);
                                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Con_xEje);
                                            Dr_Psp["Tipo"] = "Con";
                                            Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", Con_PreCom);
                                            Dr_Psp["Ejercido"] = String.Format("{0:n}", Con_Ej);
                                        }
                                        else if (Dr_Psp["Tipo"].ToString().Trim().Equals("PG_Tot"))
                                        {
                                            Dr_Psp["Aprobado"] = String.Format("{0:n}", PG_Est);
                                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", PG_Amp);
                                            Dr_Psp["Reduccion"] = String.Format("{0:n}", PG_Red);
                                            Dr_Psp["Modificado"] = String.Format("{0:n}", PG_Mod);
                                            Dr_Psp["Devengado"] = String.Format("{0:n}", PG_Dev);
                                            Dr_Psp["Recaudado"] = String.Format("{0:n}", PG_Pag);
                                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PG_Dev_Rec);
                                            Dr_Psp["Comprometido"] = String.Format("{0:n}", PG_Com);
                                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PG_xEje);
                                            Dr_Psp["Tipo"] = "PG";
                                            Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", PG_PreCom);
                                            Dr_Psp["Ejercido"] = String.Format("{0:n}", PG_Ej);
                                        }

                                    }//fin foreach

                                    //limpiamos las variables
                                    PP_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                    PP_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                    PP_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                    PP_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                    PP_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                    PP_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                    PP_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                    PP_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                    PP_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                    PP_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                    PP_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                    UR_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                    UR_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                    UR_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                    UR_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                    UR_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                    UR_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                    UR_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                    UR_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                    UR_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                    UR_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                    UR_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                    Cap_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                    Cap_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                    Cap_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                    Cap_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                    Cap_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                    Cap_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                    Cap_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                    Cap_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                    Cap_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                    Cap_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                    Cap_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                    Con_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                    Con_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                    Con_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                    Con_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                    Con_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                    Con_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                    Con_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                    Con_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                    Con_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                    Con_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                    Con_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                    PG_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                    PG_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                    PG_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                    PG_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                    PG_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                    PG_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                    PG_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                    PG_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                    PG_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                    PG_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                    PG_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                    PP_ID = Dr["PP_ID"].ToString().Trim();
                                    PP = "***** " + Dr["CLAVE_NOM_PP"].ToString().Trim();
                                    UR_ID = Dr["UR_ID"].ToString().Trim();
                                    UR = "**** " + Dr["CLAVE_NOM_UR"].ToString().Trim();
                                    Cap_ID = Dr["CA_ID"].ToString().Trim();
                                    Cap = "*** " + Dr["CLAVE_NOM_CAPITULO"].ToString().Trim();
                                    Con_ID = Dr["CO_ID"].ToString().Trim();
                                    Con = "** " + Dr["CLAVE_NOM_CONCEPTO"].ToString().Trim();
                                    PG_ID = Dr["PG_ID"].ToString().Trim();
                                    PG = "* " + Dr["CLAVE_NOM_PARTIDA_GEN"].ToString().Trim();

                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = PP.Trim();
                                    Fila["Aprobado"] = String.Empty;
                                    Fila["Ampliacion"] = String.Empty;
                                    Fila["Reduccion"] = String.Empty;
                                    Fila["Modificado"] = String.Empty;
                                    Fila["Devengado"] = String.Empty;
                                    Fila["Recaudado"] = String.Empty;
                                    Fila["Dev_Rec"] = String.Empty;
                                    Fila["Comprometido"] = String.Empty;
                                    Fila["Por_Recaudar"] = String.Empty;
                                    Fila["Tipo"] = "PP_Tot";
                                    Fila["Pre_Comprometido"] = String.Empty;
                                    Fila["Ejercido"] = String.Empty;
                                    Dt_Psp.Rows.Add(Fila);

                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = UR.Trim();
                                    Fila["Aprobado"] = String.Empty;
                                    Fila["Ampliacion"] = String.Empty;
                                    Fila["Reduccion"] = String.Empty;
                                    Fila["Modificado"] = String.Empty;
                                    Fila["Devengado"] = String.Empty;
                                    Fila["Recaudado"] = String.Empty;
                                    Fila["Dev_Rec"] = String.Empty;
                                    Fila["Comprometido"] = String.Empty;
                                    Fila["Por_Recaudar"] = String.Empty;
                                    Fila["Tipo"] = "UR_Tot";
                                    Fila["Pre_Comprometido"] = String.Empty;
                                    Fila["Ejercido"] = String.Empty;
                                    Dt_Psp.Rows.Add(Fila);

                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = Cap.Trim();
                                    Fila["Aprobado"] = String.Empty;
                                    Fila["Ampliacion"] = String.Empty;
                                    Fila["Reduccion"] = String.Empty;
                                    Fila["Modificado"] = String.Empty;
                                    Fila["Devengado"] = String.Empty;
                                    Fila["Recaudado"] = String.Empty;
                                    Fila["Dev_Rec"] = String.Empty;
                                    Fila["Comprometido"] = String.Empty;
                                    Fila["Por_Recaudar"] = String.Empty;
                                    Fila["Tipo"] = "Cap_Tot";
                                    Fila["Pre_Comprometido"] = String.Empty;
                                    Fila["Ejercido"] = String.Empty;
                                    Dt_Psp.Rows.Add(Fila);

                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = Con.Trim();
                                    Fila["Aprobado"] = String.Empty;
                                    Fila["Ampliacion"] = String.Empty;
                                    Fila["Reduccion"] = String.Empty;
                                    Fila["Modificado"] = String.Empty;
                                    Fila["Devengado"] = String.Empty;
                                    Fila["Recaudado"] = String.Empty;
                                    Fila["Dev_Rec"] = String.Empty;
                                    Fila["Comprometido"] = String.Empty;
                                    Fila["Por_Recaudar"] = String.Empty;
                                    Fila["Tipo"] = "Con_Tot";
                                    Fila["Pre_Comprometido"] = String.Empty;
                                    Fila["Ejercido"] = String.Empty;
                                    Dt_Psp.Rows.Add(Fila);

                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = PG.Trim();
                                    Fila["Aprobado"] = String.Empty;
                                    Fila["Ampliacion"] = String.Empty;
                                    Fila["Reduccion"] = String.Empty;
                                    Fila["Modificado"] = String.Empty;
                                    Fila["Devengado"] = String.Empty;
                                    Fila["Recaudado"] = String.Empty;
                                    Fila["Dev_Rec"] = String.Empty;
                                    Fila["Comprometido"] = String.Empty;
                                    Fila["Por_Recaudar"] = String.Empty;
                                    Fila["Tipo"] = "PG_Tot";
                                    Fila["Pre_Comprometido"] = String.Empty;
                                    Fila["Ejercido"] = String.Empty;
                                    Dt_Psp.Rows.Add(Fila);

                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = Dr["CLAVE_NOM_PARTIDA"].ToString().Trim();
                                    Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                                    Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                    Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                    Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                    Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                    Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim()));
                                    Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim()));
                                    Fila["Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                                    Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                                    Fila["Tipo"] = "P";
                                    Fila["Pre_Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim()));
                                    Fila["Ejercido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim()));
                                    Dt_Psp.Rows.Add(Fila);
                                    #endregion
                                }
                            }
                            else
                            {
                                #region (else AF)
                                foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                                {
                                    if (Dr_Psp["Tipo"].ToString().Trim().Equals("AF_Tot"))
                                    {
                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", AF_Est);
                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", AF_Amp);
                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", AF_Red);
                                        Dr_Psp["Modificado"] = String.Format("{0:n}", AF_Mod);
                                        Dr_Psp["Devengado"] = String.Format("{0:n}", AF_Dev);
                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", AF_Pag);
                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", AF_Dev_Rec);
                                        Dr_Psp["Comprometido"] = String.Format("{0:n}", AF_Com);
                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", AF_xEje);
                                        Dr_Psp["Tipo"] = "AF";
                                        Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", AF_PreCom);
                                        Dr_Psp["Ejercido"] = String.Format("{0:n}", AF_Ej);
                                    }
                                    else if (Dr_Psp["Tipo"].ToString().Trim().Equals("PP_Tot"))
                                    {
                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", PP_Est);
                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", PP_Amp);
                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", PP_Red);
                                        Dr_Psp["Modificado"] = String.Format("{0:n}", PP_Mod);
                                        Dr_Psp["Devengado"] = String.Format("{0:n}", PP_Dev);
                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", PP_Pag);
                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PP_Dev_Rec);
                                        Dr_Psp["Comprometido"] = String.Format("{0:n}", PP_Com);
                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PP_xEje);
                                        Dr_Psp["Tipo"] = "PP";
                                        Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", PP_PreCom);
                                        Dr_Psp["Ejercido"] = String.Format("{0:n}", PP_Ej);
                                    }
                                    else if (Dr_Psp["Tipo"].ToString().Trim().Equals("UR_Tot"))
                                    {
                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", UR_Est);
                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", UR_Amp);
                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", UR_Red);
                                        Dr_Psp["Modificado"] = String.Format("{0:n}", UR_Mod);
                                        Dr_Psp["Devengado"] = String.Format("{0:n}", UR_Dev);
                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", UR_Pag);
                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", UR_Dev_Rec);
                                        Dr_Psp["Comprometido"] = String.Format("{0:n}", UR_Com);
                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", UR_xEje);
                                        Dr_Psp["Tipo"] = "UR";
                                        Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", UR_PreCom);
                                        Dr_Psp["Ejercido"] = String.Format("{0:n}", UR_Ej);
                                    }
                                    else if (Dr_Psp["Tipo"].ToString().Trim().Equals("Cap_Tot"))
                                    {
                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", Cap_Est);
                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", Cap_Amp);
                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", Cap_Red);
                                        Dr_Psp["Modificado"] = String.Format("{0:n}", Cap_Mod);
                                        Dr_Psp["Devengado"] = String.Format("{0:n}", Cap_Dev);
                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", Cap_Pag);
                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Cap_Dev_Rec);
                                        Dr_Psp["Comprometido"] = String.Format("{0:n}", Cap_Com);
                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Cap_xEje);
                                        Dr_Psp["Tipo"] = "Cap";
                                        Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", Cap_PreCom);
                                        Dr_Psp["Ejercido"] = String.Format("{0:n}", Cap_Ej);
                                    }
                                    else if (Dr_Psp["Tipo"].ToString().Trim().Equals("Con_Tot"))
                                    {
                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", Con_Est);
                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", Con_Amp);
                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", Con_Red);
                                        Dr_Psp["Modificado"] = String.Format("{0:n}", Con_Mod);
                                        Dr_Psp["Devengado"] = String.Format("{0:n}", Con_Dev);
                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", Con_Pag);
                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Con_Dev_Rec);
                                        Dr_Psp["Comprometido"] = String.Format("{0:n}", Con_Com);
                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Con_xEje);
                                        Dr_Psp["Tipo"] = "Con";
                                        Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", Con_PreCom);
                                        Dr_Psp["Ejercido"] = String.Format("{0:n}", Con_Ej);
                                    }
                                    else if (Dr_Psp["Tipo"].ToString().Trim().Equals("PG_Tot"))
                                    {
                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", PG_Est);
                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", PG_Amp);
                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", PG_Red);
                                        Dr_Psp["Modificado"] = String.Format("{0:n}", PG_Mod);
                                        Dr_Psp["Devengado"] = String.Format("{0:n}", PG_Dev);
                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", PG_Pag);
                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PG_Dev_Rec);
                                        Dr_Psp["Comprometido"] = String.Format("{0:n}", PG_Com);
                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PG_xEje);
                                        Dr_Psp["Tipo"] = "PG";
                                        Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", PG_PreCom);
                                        Dr_Psp["Ejercido"] = String.Format("{0:n}", PG_Ej);
                                    }

                                }//fin foreach

                                //limpiamos las variables
                                AF_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                AF_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                AF_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                AF_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                AF_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                AF_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                AF_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                AF_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                AF_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                AF_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                AF_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                PP_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                PP_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                PP_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                PP_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                PP_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                PP_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                PP_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                PP_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                PP_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                PP_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                PP_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                UR_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                UR_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                UR_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                UR_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                UR_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                UR_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                UR_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                UR_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                UR_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                UR_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                UR_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                Cap_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                Cap_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                Cap_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                Cap_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                Cap_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                Cap_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                Cap_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                Cap_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                Cap_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                Cap_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                Cap_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                Con_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                Con_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                Con_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                Con_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                Con_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                Con_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                Con_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                Con_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                Con_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                Con_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                Con_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                PG_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                PG_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                PG_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                PG_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                PG_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                PG_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                PG_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                                PG_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                PG_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                PG_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                                PG_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                                AF_ID = Dr["AF_ID"].ToString().Trim();
                                AF = "****** " + Dr["CLAVE_NOM_AF"].ToString().Trim();
                                PP_ID = Dr["PP_ID"].ToString().Trim();
                                PP = "***** " + Dr["CLAVE_NOM_PP"].ToString().Trim();
                                UR_ID = Dr["UR_ID"].ToString().Trim();
                                UR = "**** " + Dr["CLAVE_NOM_UR"].ToString().Trim();
                                Cap_ID = Dr["CA_ID"].ToString().Trim();
                                Cap = "*** " + Dr["CLAVE_NOM_CAPITULO"].ToString().Trim();
                                Con_ID = Dr["CO_ID"].ToString().Trim();
                                Con = "** " + Dr["CLAVE_NOM_CONCEPTO"].ToString().Trim();
                                PG_ID = Dr["PG_ID"].ToString().Trim();
                                PG = "* " + Dr["CLAVE_NOM_PARTIDA_GEN"].ToString().Trim();

                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = AF.Trim();
                                Fila["Aprobado"] = String.Empty;
                                Fila["Ampliacion"] = String.Empty;
                                Fila["Reduccion"] = String.Empty;
                                Fila["Modificado"] = String.Empty;
                                Fila["Devengado"] = String.Empty;
                                Fila["Recaudado"] = String.Empty;
                                Fila["Dev_Rec"] = String.Empty;
                                Fila["Comprometido"] = String.Empty;
                                Fila["Por_Recaudar"] = String.Empty;
                                Fila["Tipo"] = "AF_Tot";
                                Fila["Pre_Comprometido"] = String.Empty;
                                Fila["Ejercido"] = String.Empty;
                                Dt_Psp.Rows.Add(Fila);

                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = PP.Trim();
                                Fila["Aprobado"] = String.Empty;
                                Fila["Ampliacion"] = String.Empty;
                                Fila["Reduccion"] = String.Empty;
                                Fila["Modificado"] = String.Empty;
                                Fila["Devengado"] = String.Empty;
                                Fila["Recaudado"] = String.Empty;
                                Fila["Dev_Rec"] = String.Empty;
                                Fila["Comprometido"] = String.Empty;
                                Fila["Por_Recaudar"] = String.Empty;
                                Fila["Tipo"] = "PP_Tot";
                                Fila["Pre_Comprometido"] = String.Empty;
                                Fila["Ejercido"] = String.Empty;
                                Dt_Psp.Rows.Add(Fila);

                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = UR.Trim();
                                Fila["Aprobado"] = String.Empty;
                                Fila["Ampliacion"] = String.Empty;
                                Fila["Reduccion"] = String.Empty;
                                Fila["Modificado"] = String.Empty;
                                Fila["Devengado"] = String.Empty;
                                Fila["Recaudado"] = String.Empty;
                                Fila["Dev_Rec"] = String.Empty;
                                Fila["Comprometido"] = String.Empty;
                                Fila["Por_Recaudar"] = String.Empty;
                                Fila["Tipo"] = "UR_Tot";
                                Fila["Pre_Comprometido"] = String.Empty;
                                Fila["Ejercido"] = String.Empty;
                                Dt_Psp.Rows.Add(Fila);

                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = Cap.Trim();
                                Fila["Aprobado"] = String.Empty;
                                Fila["Ampliacion"] = String.Empty;
                                Fila["Reduccion"] = String.Empty;
                                Fila["Modificado"] = String.Empty;
                                Fila["Devengado"] = String.Empty;
                                Fila["Recaudado"] = String.Empty;
                                Fila["Dev_Rec"] = String.Empty;
                                Fila["Comprometido"] = String.Empty;
                                Fila["Por_Recaudar"] = String.Empty;
                                Fila["Tipo"] = "Cap_Tot";
                                Fila["Pre_Comprometido"] = String.Empty;
                                Fila["Ejercido"] = String.Empty;
                                Dt_Psp.Rows.Add(Fila);

                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = Con.Trim();
                                Fila["Aprobado"] = String.Empty;
                                Fila["Ampliacion"] = String.Empty;
                                Fila["Reduccion"] = String.Empty;
                                Fila["Modificado"] = String.Empty;
                                Fila["Devengado"] = String.Empty;
                                Fila["Recaudado"] = String.Empty;
                                Fila["Dev_Rec"] = String.Empty;
                                Fila["Comprometido"] = String.Empty;
                                Fila["Por_Recaudar"] = String.Empty;
                                Fila["Tipo"] = "Con_Tot";
                                Fila["Pre_Comprometido"] = String.Empty;
                                Fila["Ejercido"] = String.Empty;
                                Dt_Psp.Rows.Add(Fila);

                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = PG.Trim();
                                Fila["Aprobado"] = String.Empty;
                                Fila["Ampliacion"] = String.Empty;
                                Fila["Reduccion"] = String.Empty;
                                Fila["Modificado"] = String.Empty;
                                Fila["Devengado"] = String.Empty;
                                Fila["Recaudado"] = String.Empty;
                                Fila["Dev_Rec"] = String.Empty;
                                Fila["Comprometido"] = String.Empty;
                                Fila["Por_Recaudar"] = String.Empty;
                                Fila["Tipo"] = "PG_Tot";
                                Fila["Pre_Comprometido"] = String.Empty;
                                Fila["Ejercido"] = String.Empty;
                                Dt_Psp.Rows.Add(Fila);

                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = Dr["CLAVE_NOM_PARTIDA"].ToString().Trim();
                                Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                                Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim()));
                                Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim()));
                                Fila["Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                                Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                                Fila["Tipo"] = "P";
                                Fila["Pre_Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim()));
                                Fila["Ejercido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim()));
                                Dt_Psp.Rows.Add(Fila);
                                #endregion
                            }
                        }
                        else
                        {
                            #region (else FF)
                            foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                            {
                                if (Dr_Psp["Tipo"].ToString().Trim().Equals("FF_Tot"))
                                {
                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", FF_Est);
                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", FF_Amp);
                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", FF_Red);
                                    Dr_Psp["Modificado"] = String.Format("{0:n}", FF_Mod);
                                    Dr_Psp["Devengado"] = String.Format("{0:n}", FF_Dev);
                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", FF_Pag);
                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", FF_Dev_Rec);
                                    Dr_Psp["Comprometido"] = String.Format("{0:n}", FF_Com);
                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", FF_xEje);
                                    Dr_Psp["Tipo"] = "FF";
                                    Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", FF_PreCom);
                                    Dr_Psp["Ejercido"] = String.Format("{0:n}", FF_Ej);
                                }
                                else if (Dr_Psp["Tipo"].ToString().Trim().Equals("AF_Tot"))
                                {
                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", AF_Est);
                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", AF_Amp);
                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", AF_Red);
                                    Dr_Psp["Modificado"] = String.Format("{0:n}", AF_Mod);
                                    Dr_Psp["Devengado"] = String.Format("{0:n}", AF_Dev);
                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", AF_Pag);
                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", AF_Dev_Rec);
                                    Dr_Psp["Comprometido"] = String.Format("{0:n}", AF_Com);
                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", AF_xEje);
                                    Dr_Psp["Tipo"] = "AF";
                                    Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", AF_PreCom);
                                    Dr_Psp["Ejercido"] = String.Format("{0:n}", AF_Ej);
                                }
                                else if (Dr_Psp["Tipo"].ToString().Trim().Equals("PP_Tot"))
                                {
                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", PP_Est);
                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", PP_Amp);
                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", PP_Red);
                                    Dr_Psp["Modificado"] = String.Format("{0:n}", PP_Mod);
                                    Dr_Psp["Devengado"] = String.Format("{0:n}", PP_Dev);
                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", PP_Pag);
                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PP_Dev_Rec);
                                    Dr_Psp["Comprometido"] = String.Format("{0:n}", PP_Com);
                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PP_xEje);
                                    Dr_Psp["Tipo"] = "PP";
                                    Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", PP_PreCom);
                                    Dr_Psp["Ejercido"] = String.Format("{0:n}", PP_Ej);
                                }
                                else if (Dr_Psp["Tipo"].ToString().Trim().Equals("UR_Tot"))
                                {
                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", UR_Est);
                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", UR_Amp);
                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", UR_Red);
                                    Dr_Psp["Modificado"] = String.Format("{0:n}", UR_Mod);
                                    Dr_Psp["Devengado"] = String.Format("{0:n}", UR_Dev);
                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", UR_Pag);
                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", UR_Dev_Rec);
                                    Dr_Psp["Comprometido"] = String.Format("{0:n}", UR_Com);
                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", UR_xEje);
                                    Dr_Psp["Tipo"] = "UR";
                                    Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", UR_PreCom);
                                    Dr_Psp["Ejercido"] = String.Format("{0:n}", UR_Ej);
                                }
                                else if (Dr_Psp["Tipo"].ToString().Trim().Equals("Cap_Tot"))
                                {
                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", Cap_Est);
                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", Cap_Amp);
                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", Cap_Red);
                                    Dr_Psp["Modificado"] = String.Format("{0:n}", Cap_Mod);
                                    Dr_Psp["Devengado"] = String.Format("{0:n}", Cap_Dev);
                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", Cap_Pag);
                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Cap_Dev_Rec);
                                    Dr_Psp["Comprometido"] = String.Format("{0:n}", Cap_Com);
                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Cap_xEje);
                                    Dr_Psp["Tipo"] = "Cap";
                                    Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", Cap_PreCom);
                                    Dr_Psp["Ejercido"] = String.Format("{0:n}", Cap_Ej);
                                }
                                else if (Dr_Psp["Tipo"].ToString().Trim().Equals("Con_Tot"))
                                {
                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", Con_Est);
                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", Con_Amp);
                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", Con_Red);
                                    Dr_Psp["Modificado"] = String.Format("{0:n}", Con_Mod);
                                    Dr_Psp["Devengado"] = String.Format("{0:n}", Con_Dev);
                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", Con_Pag);
                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Con_Dev_Rec);
                                    Dr_Psp["Comprometido"] = String.Format("{0:n}", Con_Com);
                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Con_xEje);
                                    Dr_Psp["Tipo"] = "Con";
                                    Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", Con_PreCom);
                                    Dr_Psp["Ejercido"] = String.Format("{0:n}", Con_Ej);
                                }
                                else if (Dr_Psp["Tipo"].ToString().Trim().Equals("PG_Tot"))
                                {
                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", PG_Est);
                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", PG_Amp);
                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", PG_Red);
                                    Dr_Psp["Modificado"] = String.Format("{0:n}", PG_Mod);
                                    Dr_Psp["Devengado"] = String.Format("{0:n}", PG_Dev);
                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", PG_Pag);
                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PG_Dev_Rec);
                                    Dr_Psp["Comprometido"] = String.Format("{0:n}", PG_Com);
                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PG_xEje);
                                    Dr_Psp["Tipo"] = "PG";
                                    Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", PG_PreCom);
                                    Dr_Psp["Ejercido"] = String.Format("{0:n}", PG_Ej);
                                }

                            }//fin foreach

                            //limpiamos las variables
                            FF_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                            FF_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            FF_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            FF_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            FF_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            FF_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                            FF_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                            FF_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                            FF_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                            FF_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                            FF_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                            AF_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                            AF_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            AF_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            AF_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            AF_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            AF_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                            AF_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                            AF_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                            AF_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                            AF_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                            AF_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                            PP_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                            PP_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            PP_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            PP_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            PP_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            PP_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                            PP_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                            PP_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                            PP_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                            PP_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                            PP_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                            UR_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                            UR_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            UR_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            UR_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            UR_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            UR_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                            UR_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                            UR_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                            UR_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                            UR_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                            UR_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                            Cap_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                            Cap_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            Cap_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            Cap_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            Cap_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            Cap_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                            Cap_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                            Cap_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                            Cap_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                            Cap_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                            Cap_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                            Con_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                            Con_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            Con_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            Con_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            Con_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            Con_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                            Con_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                            Con_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                            Con_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                            Con_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                            Con_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                            PG_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                            PG_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            PG_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            PG_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            PG_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            PG_Pag = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                            PG_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim());
                            PG_Com = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                            PG_xEje = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                            PG_PreCom = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim());
                            PG_Ej = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());

                            FF_ID = Dr["FF_ID"].ToString().Trim();
                            FF = "******* " + Dr["CLAVE_NOM_FF"].ToString().Trim();
                            AF_ID = Dr["AF_ID"].ToString().Trim();
                            AF = "****** " + Dr["CLAVE_NOM_AF"].ToString().Trim();
                            PP_ID = Dr["PP_ID"].ToString().Trim();
                            PP = "***** " + Dr["CLAVE_NOM_PP"].ToString().Trim();
                            UR_ID = Dr["UR_ID"].ToString().Trim();
                            UR = "**** " + Dr["CLAVE_NOM_UR"].ToString().Trim();
                            Cap_ID = Dr["CA_ID"].ToString().Trim();
                            Cap = "*** " + Dr["CLAVE_NOM_CAPITULO"].ToString().Trim();
                            Con_ID = Dr["CO_ID"].ToString().Trim();
                            Con = "** " + Dr["CLAVE_NOM_CONCEPTO"].ToString().Trim();
                            PG_ID = Dr["PG_ID"].ToString().Trim();
                            PG = "* " + Dr["CLAVE_NOM_PARTIDA_GEN"].ToString().Trim();

                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = FF.Trim();
                            Fila["Aprobado"] = String.Empty;
                            Fila["Ampliacion"] = String.Empty;
                            Fila["Reduccion"] = String.Empty;
                            Fila["Modificado"] = String.Empty;
                            Fila["Devengado"] = String.Empty;
                            Fila["Recaudado"] = String.Empty;
                            Fila["Dev_Rec"] = String.Empty;
                            Fila["Comprometido"] = String.Empty;
                            Fila["Por_Recaudar"] = String.Empty;
                            Fila["Tipo"] = "FF_Tot";
                            Fila["Pre_Comprometido"] = String.Empty;
                            Fila["Ejercido"] = String.Empty;
                            Dt_Psp.Rows.Add(Fila);

                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = AF.Trim();
                            Fila["Aprobado"] = String.Empty;
                            Fila["Ampliacion"] = String.Empty;
                            Fila["Reduccion"] = String.Empty;
                            Fila["Modificado"] = String.Empty;
                            Fila["Devengado"] = String.Empty;
                            Fila["Recaudado"] = String.Empty;
                            Fila["Dev_Rec"] = String.Empty;
                            Fila["Comprometido"] = String.Empty;
                            Fila["Por_Recaudar"] = String.Empty;
                            Fila["Tipo"] = "AF_Tot";
                            Fila["Pre_Comprometido"] = String.Empty;
                            Fila["Ejercido"] = String.Empty;
                            Dt_Psp.Rows.Add(Fila);

                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = PP.Trim();
                            Fila["Aprobado"] = String.Empty;
                            Fila["Ampliacion"] = String.Empty;
                            Fila["Reduccion"] = String.Empty;
                            Fila["Modificado"] = String.Empty;
                            Fila["Devengado"] = String.Empty;
                            Fila["Recaudado"] = String.Empty;
                            Fila["Dev_Rec"] = String.Empty;
                            Fila["Comprometido"] = String.Empty;
                            Fila["Por_Recaudar"] = String.Empty;
                            Fila["Tipo"] = "PP_Tot";
                            Fila["Pre_Comprometido"] = String.Empty;
                            Fila["Ejercido"] = String.Empty;
                            Dt_Psp.Rows.Add(Fila);

                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = UR.Trim();
                            Fila["Aprobado"] = String.Empty;
                            Fila["Ampliacion"] = String.Empty;
                            Fila["Reduccion"] = String.Empty;
                            Fila["Modificado"] = String.Empty;
                            Fila["Devengado"] = String.Empty;
                            Fila["Recaudado"] = String.Empty;
                            Fila["Dev_Rec"] = String.Empty;
                            Fila["Comprometido"] = String.Empty;
                            Fila["Por_Recaudar"] = String.Empty;
                            Fila["Tipo"] = "UR_Tot";
                            Fila["Pre_Comprometido"] = String.Empty;
                            Fila["Ejercido"] = String.Empty;
                            Dt_Psp.Rows.Add(Fila);

                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = Cap.Trim();
                            Fila["Aprobado"] = String.Empty;
                            Fila["Ampliacion"] = String.Empty;
                            Fila["Reduccion"] = String.Empty;
                            Fila["Modificado"] = String.Empty;
                            Fila["Devengado"] = String.Empty;
                            Fila["Recaudado"] = String.Empty;
                            Fila["Dev_Rec"] = String.Empty;
                            Fila["Comprometido"] = String.Empty;
                            Fila["Por_Recaudar"] = String.Empty;
                            Fila["Tipo"] = "Cap_Tot";
                            Fila["Pre_Comprometido"] = String.Empty;
                            Fila["Ejercido"] = String.Empty;
                            Dt_Psp.Rows.Add(Fila);

                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = Con.Trim();
                            Fila["Aprobado"] = String.Empty;
                            Fila["Ampliacion"] = String.Empty;
                            Fila["Reduccion"] = String.Empty;
                            Fila["Modificado"] = String.Empty;
                            Fila["Devengado"] = String.Empty;
                            Fila["Recaudado"] = String.Empty;
                            Fila["Dev_Rec"] = String.Empty;
                            Fila["Comprometido"] = String.Empty;
                            Fila["Por_Recaudar"] = String.Empty;
                            Fila["Tipo"] = "Con_Tot";
                            Fila["Pre_Comprometido"] = String.Empty;
                            Fila["Ejercido"] = String.Empty;
                            Dt_Psp.Rows.Add(Fila);

                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = PG.Trim();
                            Fila["Aprobado"] = String.Empty;
                            Fila["Ampliacion"] = String.Empty;
                            Fila["Reduccion"] = String.Empty;
                            Fila["Modificado"] = String.Empty;
                            Fila["Devengado"] = String.Empty;
                            Fila["Recaudado"] = String.Empty;
                            Fila["Dev_Rec"] = String.Empty;
                            Fila["Comprometido"] = String.Empty;
                            Fila["Por_Recaudar"] = String.Empty;
                            Fila["Tipo"] = "PG_Tot";
                            Fila["Pre_Comprometido"] = String.Empty;
                            Fila["Ejercido"] = String.Empty;
                            Dt_Psp.Rows.Add(Fila);

                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = Dr["CLAVE_NOM_PARTIDA"].ToString().Trim();
                            Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                            Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                            Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                            Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                            Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                            Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim()));
                            Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_PAG"].ToString().Trim()) ? "0" : Dr["DEV_PAG"].ToString().Trim()));
                            Fila["Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                            Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                            Fila["Tipo"] = "P";
                            Fila["Pre_Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim()));
                            Fila["Ejercido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim()));
                            Dt_Psp.Rows.Add(Fila);
                            #endregion
                        }
                    }

                    #region (Fin Dt Psp)
                    foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                    {
                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", Tot_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", Tot_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", Tot_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", Tot_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", Tot_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", Tot_Pag);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Tot_Dev_Rec);
                            Dr_Psp["Comprometido"] = String.Format("{0:n}", Tot_Com);
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Tot_xEje);
                            Dr_Psp["Tipo"] = "Tot";
                            Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", Tot_PreCom);
                            Dr_Psp["Ejercido"] = String.Format("{0:n}", Tot_Ej);
                        }
                        else if (Dr_Psp["Tipo"].ToString().Trim().Equals("FF_Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", FF_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", FF_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", FF_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", FF_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", FF_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", FF_Pag);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", FF_Dev_Rec);
                            Dr_Psp["Comprometido"] = String.Format("{0:n}", FF_Com);
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", FF_xEje);
                            Dr_Psp["Tipo"] = "FF";
                            Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", FF_PreCom);
                            Dr_Psp["Ejercido"] = String.Format("{0:n}", FF_Ej);
                        }
                        else if (Dr_Psp["Tipo"].ToString().Trim().Equals("AF_Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", AF_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", AF_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", AF_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", AF_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", AF_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", AF_Pag);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", AF_Dev_Rec);
                            Dr_Psp["Comprometido"] = String.Format("{0:n}", AF_Com);
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", AF_xEje);
                            Dr_Psp["Tipo"] = "AF";
                            Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", AF_PreCom);
                            Dr_Psp["Ejercido"] = String.Format("{0:n}", AF_Ej);
                        }
                        else if (Dr_Psp["Tipo"].ToString().Trim().Equals("PP_Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", PP_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", PP_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", PP_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", PP_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", PP_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", PP_Pag);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PP_Dev_Rec);
                            Dr_Psp["Comprometido"] = String.Format("{0:n}", PP_Com);
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PP_xEje);
                            Dr_Psp["Tipo"] = "PP";
                            Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", PP_PreCom);
                            Dr_Psp["Ejercido"] = String.Format("{0:n}", PP_Ej);
                        }
                        else if (Dr_Psp["Tipo"].ToString().Trim().Equals("UR_Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", UR_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", UR_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", UR_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", UR_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", UR_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", UR_Pag);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", UR_Dev_Rec);
                            Dr_Psp["Comprometido"] = String.Format("{0:n}", UR_Com);
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", UR_xEje);
                            Dr_Psp["Tipo"] = "UR";
                            Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", UR_PreCom);
                            Dr_Psp["Ejercido"] = String.Format("{0:n}", UR_Ej);
                        }
                        else if (Dr_Psp["Tipo"].ToString().Trim().Equals("Cap_Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", Cap_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", Cap_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", Cap_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", Cap_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", Cap_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", Cap_Pag);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Cap_Dev_Rec);
                            Dr_Psp["Comprometido"] = String.Format("{0:n}", Cap_Com);
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Cap_xEje);
                            Dr_Psp["Tipo"] = "Cap";
                            Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", Cap_PreCom);
                            Dr_Psp["Ejercido"] = String.Format("{0:n}", Cap_Ej);
                        }
                        else if (Dr_Psp["Tipo"].ToString().Trim().Equals("Con_Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", Con_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", Con_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", Con_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", Con_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", Con_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", Con_Pag);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Con_Dev_Rec);
                            Dr_Psp["Comprometido"] = String.Format("{0:n}", Con_Com);
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Con_xEje);
                            Dr_Psp["Tipo"] = "Con";
                            Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", Con_PreCom);
                            Dr_Psp["Ejercido"] = String.Format("{0:n}", Con_Ej);
                        }
                        else if (Dr_Psp["Tipo"].ToString().Trim().Equals("PG_Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", PG_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", PG_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", PG_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", PG_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", PG_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", PG_Pag);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PG_Dev_Rec);
                            Dr_Psp["Comprometido"] = String.Format("{0:n}", PG_Com);
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PG_xEje);
                            Dr_Psp["Tipo"] = "PG";
                            Dr_Psp["Pre_Comprometido"] = String.Format("{0:n}", PG_PreCom);
                            Dr_Psp["Ejercido"] = String.Format("{0:n}", PG_Ej);
                        }

                    }//fin foreach
                    #endregion
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al generar el presupuesto de egresos. Error[" + ex.Message + "]");
                }
                return Dt_Psp;
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Generar_Dt_Psp_Igr
            ///DESCRIPCIÓN          : Metodo para generar la tabla del presupuesto de ingresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 30/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private DataTable Generar_Dt_Psp_Ing(DataTable Dt_Ing)
            {
                DataTable Dt_Psp = new DataTable();
                DataRow Fila;
                Boolean SubConcepto = false;
                Boolean Programa = false;

                #region (variables)
                Double Tot_Est = 0.00;
                Double Tot_Amp = 0.00;
                Double Tot_Red = 0.00;
                Double Tot_Mod = 0.00;
                Double Tot_Dev = 0.00;
                Double Tot_Dev_Rec = 0.00;
                Double Tot_Rec = 0.00;
                Double Tot_xRec = 0.00;

                Double FF_Est = 0.00;
                Double PP_Est = 0.00;
                Double R_Est = 0.00;
                Double T_Est = 0.00;
                Double CL_Est = 0.00;
                Double C_Est = 0.00;

                Double FF_Amp = 0.00;
                Double PP_Amp = 0.00;
                Double R_Amp = 0.00;
                Double T_Amp = 0.00;
                Double CL_Amp = 0.00;
                Double C_Amp = 0.00;

                Double FF_Red = 0.00;
                Double PP_Red = 0.00;
                Double R_Red = 0.00;
                Double T_Red = 0.00;
                Double CL_Red = 0.00;
                Double C_Red = 0.00;

                Double FF_Mod = 0.00;
                Double PP_Mod = 0.00;
                Double R_Mod = 0.00;
                Double T_Mod = 0.00;
                Double CL_Mod = 0.00;
                Double C_Mod = 0.00;

                Double FF_Dev = 0.00;
                Double PP_Dev = 0.00;
                Double R_Dev = 0.00;
                Double T_Dev = 0.00;
                Double CL_Dev = 0.00;
                Double C_Dev = 0.00;

                Double FF_Dev_Rec = 0.00;
                Double PP_Dev_Rec = 0.00;
                Double R_Dev_Rec = 0.00;
                Double T_Dev_Rec = 0.00;
                Double CL_Dev_Rec = 0.00;
                Double C_Dev_Rec = 0.00;


                Double FF_Rec = 0.00;
                Double PP_Rec = 0.00;
                Double R_Rec = 0.00;
                Double T_Rec = 0.00;
                Double CL_Rec = 0.00;
                Double C_Rec = 0.00;

                Double FF_xRec = 0.00;
                Double PP_xRec = 0.00;
                Double R_xRec = 0.00;
                Double T_xRec = 0.00;
                Double CL_xRec = 0.00;
                Double C_xRec = 0.00;
                #endregion

                try
                {
                    #region(Tabla)
                    String FF_ID = Dt_Ing.Rows[0]["FF_ID"].ToString().Trim();
                    String PP_ID = Dt_Ing.Rows[0]["PP_ID"].ToString().Trim();
                    String R_ID = Dt_Ing.Rows[0]["R_ID"].ToString().Trim();
                    String T_ID = Dt_Ing.Rows[0]["T_ID"].ToString().Trim();
                    String CL_ID = Dt_Ing.Rows[0]["CL_ID"].ToString().Trim();
                    String C_ID = Dt_Ing.Rows[0]["C_ID"].ToString().Trim();
                    String SC_ID = Dt_Ing.Rows[0]["SC_ID"].ToString().Trim();

                    String FF = "****** " + Dt_Ing.Rows[0]["CLAVE_NOM_FF"].ToString().Trim();
                    String PP = "***** " + Dt_Ing.Rows[0]["CLAVE_NOM_PP"].ToString().Trim();
                    String R = "**** " + Dt_Ing.Rows[0]["CLAVE_NOM_RUBRO"].ToString().Trim();
                    String T = "*** " + Dt_Ing.Rows[0]["CLAVE_NOM_TIPO"].ToString().Trim();
                    String CL = "** " + Dt_Ing.Rows[0]["CLAVE_NOM_CLASE"].ToString().Trim();
                    String C = "* " + Dt_Ing.Rows[0]["CLAVE_NOM_CON"].ToString().Trim();
                    String SC = Dt_Ing.Rows[0]["CLAVE_NOM_SUBCON"].ToString().Trim();

                    if (!String.IsNullOrEmpty(PP))
                    {
                        Programa = true;
                    }
                    if (!String.IsNullOrEmpty(SC))
                    {
                        SubConcepto = true;
                    }
                    else
                    {
                        C_ID = String.Empty;
                    }

                    Dt_Psp.Columns.Add("Concepto", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Aprobado", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Ampliacion", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Reduccion", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Modificado", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Devengado", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Recaudado", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Dev_Rec", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Comprometido", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Por_Recaudar", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Tipo", System.Type.GetType("System.String"));

                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = "******* TOTAL";
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "Tot";
                    Dt_Psp.Rows.Add(Fila);

                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = FF.Trim();
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "FF_Tot";
                    Dt_Psp.Rows.Add(Fila);

                    if (Programa)
                    {
                        Fila = Dt_Psp.NewRow();
                        Fila["Concepto"] = PP.Trim();
                        Fila["Aprobado"] = String.Empty;
                        Fila["Ampliacion"] = String.Empty;
                        Fila["Reduccion"] = String.Empty;
                        Fila["Modificado"] = String.Empty;
                        Fila["Devengado"] = String.Empty;
                        Fila["Recaudado"] = String.Empty;
                        Fila["Dev_Rec"] = String.Empty;
                        Fila["Comprometido"] = String.Empty;
                        Fila["Por_Recaudar"] = String.Empty;
                        Fila["Tipo"] = "PP_Tot";
                        Dt_Psp.Rows.Add(Fila);
                    }

                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = R.Trim();
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "R_Tot";
                    Dt_Psp.Rows.Add(Fila);

                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = T.Trim();
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "T_Tot";
                    Dt_Psp.Rows.Add(Fila);

                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = CL.Trim();
                    Fila["Aprobado"] = String.Empty;
                    Fila["Ampliacion"] = String.Empty;
                    Fila["Reduccion"] = String.Empty;
                    Fila["Modificado"] = String.Empty;
                    Fila["Devengado"] = String.Empty;
                    Fila["Recaudado"] = String.Empty;
                    Fila["Dev_Rec"] = String.Empty;
                    Fila["Comprometido"] = String.Empty;
                    Fila["Por_Recaudar"] = String.Empty;
                    Fila["Tipo"] = "CL_Tot";
                    Dt_Psp.Rows.Add(Fila);

                    if (SubConcepto)
                    {
                        Fila = Dt_Psp.NewRow();
                        Fila["Concepto"] = C.Trim();
                        Fila["Aprobado"] = String.Empty;
                        Fila["Ampliacion"] = String.Empty;
                        Fila["Reduccion"] = String.Empty;
                        Fila["Modificado"] = String.Empty;
                        Fila["Devengado"] = String.Empty;
                        Fila["Recaudado"] = String.Empty;
                        Fila["Dev_Rec"] = String.Empty;
                        Fila["Comprometido"] = String.Empty;
                        Fila["Por_Recaudar"] = String.Empty;
                        Fila["Tipo"] = "C_Tot";
                        Dt_Psp.Rows.Add(Fila);
                    }

                    #endregion

                    foreach (DataRow Dr in Dt_Ing.Rows)
                    {
                        Tot_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                        Tot_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                        Tot_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                        Tot_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                        Tot_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                        Tot_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());
                        Tot_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                        Tot_xRec += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());

                        if (String.IsNullOrEmpty(Dr["CLAVE_NOM_PP"].ToString().Trim()))
                        { Programa = false; }
                        else { Programa = true; }
                        if (String.IsNullOrEmpty(Dr["CLAVE_NOM_SUBCON"].ToString().Trim()))
                        { SubConcepto = false; }
                        else { SubConcepto = true; }

                        if (FF_ID.Trim().Equals(Dr["FF_ID"].ToString().Trim()))
                        {
                            //sumamos 
                            FF_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                            FF_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            FF_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            FF_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            FF_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            FF_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());
                            FF_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                            FF_xRec += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());

                            if (PP_ID.Trim().Equals(Dr["PP_ID"].ToString().Trim()))
                            {
                                //sumamos 
                                PP_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                PP_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                PP_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                PP_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                PP_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                PP_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());
                                PP_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                PP_xRec += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());

                                if (R_ID.Trim().Equals(Dr["R_ID"].ToString().Trim()))
                                {
                                    //sumamos 
                                    R_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                    R_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                    R_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                    R_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                    R_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                    R_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());
                                    R_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                    R_xRec += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());

                                    if (T_ID.Trim().Equals(Dr["T_ID"].ToString().Trim()))
                                    {
                                        //sumamos 
                                        T_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                        T_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                        T_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                        T_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                        T_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                        T_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());
                                        T_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                        T_xRec += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());

                                        if (CL_ID.Trim().Equals(Dr["CL_ID"].ToString().Trim()))
                                        {
                                            //sumamos 
                                            CL_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                            CL_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                            CL_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                            CL_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                            CL_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                            CL_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());
                                            CL_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                            CL_xRec += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());

                                            if (C_ID.Trim().Equals(Dr["C_ID"].ToString().Trim()))
                                            {
                                                C_Est += Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                                C_Amp += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                                C_Red += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                                C_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                                C_Dev += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                                C_Dev_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());
                                                C_Rec += Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                                C_xRec += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());

                                                if (SubConcepto)
                                                {
                                                    //sumamos 
                                                    Fila = Dt_Psp.NewRow();
                                                    Fila["Concepto"] = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();
                                                    Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                                    Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                                    Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                                    Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                                    Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                                    Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                                    Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                                    Fila["Comprometido"] = "0.00";
                                                    Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                                    Fila["Tipo"] = "SC";
                                                    Dt_Psp.Rows.Add(Fila);

                                                }
                                            }
                                            else
                                            {
                                                #region (else C)
                                                foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                                                {
                                                    if (SubConcepto)
                                                    {
                                                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("C_Tot"))
                                                        {
                                                            Dr_Psp["Aprobado"] = String.Format("{0:n}", C_Est);
                                                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", C_Amp);
                                                            Dr_Psp["Reduccion"] = String.Format("{0:n}", C_Red);
                                                            Dr_Psp["Modificado"] = String.Format("{0:n}", C_Mod);
                                                            Dr_Psp["Devengado"] = String.Format("{0:n}", C_Dev);
                                                            Dr_Psp["Recaudado"] = String.Format("{0:n}", C_Rec);
                                                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", C_Dev_Rec);
                                                            Dr_Psp["Comprometido"] = "0.00";
                                                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", C_xRec);
                                                            Dr_Psp["Tipo"] = "C";
                                                        }
                                                    }
                                                }//fin foreach

                                                //limpiamos las variables
                                                C_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                                C_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                                C_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                                C_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                                C_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                                C_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                                C_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                                C_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                                C_ID = Dr["C_ID"].ToString().Trim();
                                                SC_ID = Dr["SC_ID"].ToString().Trim();

                                                C = "* " + Dr["CLAVE_NOM_CON"].ToString().Trim();
                                                SC = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();

                                                if (!String.IsNullOrEmpty(SC))
                                                { SubConcepto = true; }
                                                else { SubConcepto = false; }

                                                if (SubConcepto)
                                                {
                                                    Fila = Dt_Psp.NewRow();
                                                    Fila["Concepto"] = C.Trim();
                                                    Fila["Aprobado"] = String.Empty;
                                                    Fila["Ampliacion"] = String.Empty;
                                                    Fila["Reduccion"] = String.Empty;
                                                    Fila["Modificado"] = String.Empty;
                                                    Fila["Devengado"] = String.Empty;
                                                    Fila["Recaudado"] = String.Empty;
                                                    Fila["Dev_Rec"] = String.Empty;
                                                    Fila["Comprometido"] = String.Empty;
                                                    Fila["Por_Recaudar"] = String.Empty;
                                                    Fila["Tipo"] = "C_Tot";
                                                    Dt_Psp.Rows.Add(Fila);
                                                }

                                                if (SubConcepto)
                                                {
                                                    Fila = Dt_Psp.NewRow();
                                                    Fila["Concepto"] = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();
                                                    Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                                    Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                                    Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                                    Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                                    Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                                    Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                                    Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                                    Fila["Comprometido"] = "0.00";
                                                    Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                                    Fila["Tipo"] = "SC";
                                                    Dt_Psp.Rows.Add(Fila);
                                                }
                                                else
                                                {
                                                    Fila = Dt_Psp.NewRow();
                                                    Fila["Concepto"] = Dr["CLAVE_NOM_CON"].ToString().Trim();
                                                    Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                                    Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                                    Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                                    Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                                    Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                                    Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                                    Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                                    Fila["Comprometido"] = "0.00";
                                                    Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                                    Fila["Tipo"] = "C";
                                                    Dt_Psp.Rows.Add(Fila);
                                                }
                                                #endregion
                                            }
                                        }
                                        else
                                        {
                                            #region (else CL)
                                            foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                                            {
                                                if (Dr_Psp["Tipo"].ToString().Trim().Equals("CL_Tot"))
                                                {
                                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", CL_Est);
                                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", CL_Amp);
                                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", CL_Red);
                                                    Dr_Psp["Modificado"] = String.Format("{0:n}", CL_Mod);
                                                    Dr_Psp["Devengado"] = String.Format("{0:n}", CL_Dev);
                                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", CL_Rec);
                                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", CL_Dev_Rec);
                                                    Dr_Psp["Comprometido"] = "0.00";
                                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", CL_xRec);
                                                    Dr_Psp["Tipo"] = "CL";
                                                }

                                                if (SubConcepto)
                                                {
                                                    if (Dr_Psp["Tipo"].ToString().Trim().Equals("C_Tot"))
                                                    {
                                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", C_Est);
                                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", C_Amp);
                                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", C_Red);
                                                        Dr_Psp["Modificado"] = String.Format("{0:n}", C_Mod);
                                                        Dr_Psp["Devengado"] = String.Format("{0:n}", CL_Dev);
                                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", C_Rec);
                                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", CL_Dev_Rec);
                                                        Dr_Psp["Comprometido"] = "0.00";
                                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", C_xRec);
                                                        Dr_Psp["Tipo"] = "C";
                                                    }
                                                }
                                            }//fin foreach

                                            //limpiamos las variables
                                            CL_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                            CL_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                            CL_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                            CL_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                            CL_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                            CL_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                            CL_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                            CL_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                            C_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                            C_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                            C_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                            C_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                            C_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                            C_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                            C_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                            C_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                            CL_ID = Dr["CL_ID"].ToString().Trim();
                                            C_ID = Dr["C_ID"].ToString().Trim();
                                            SC_ID = Dr["SC_ID"].ToString().Trim();

                                            CL = "** " + Dr["CLAVE_NOM_CLASE"].ToString().Trim();
                                            C = "* " + Dr["CLAVE_NOM_CON"].ToString().Trim();
                                            SC = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();

                                            if (!String.IsNullOrEmpty(SC))
                                            { SubConcepto = true; }
                                            else { SubConcepto = false; }

                                            Fila = Dt_Psp.NewRow();
                                            Fila["Concepto"] = CL.Trim();
                                            Fila["Aprobado"] = String.Empty;
                                            Fila["Ampliacion"] = String.Empty;
                                            Fila["Reduccion"] = String.Empty;
                                            Fila["Modificado"] = String.Empty;
                                            Fila["Devengado"] = String.Empty;
                                            Fila["Recaudado"] = String.Empty;
                                            Fila["Dev_Rec"] = String.Empty;
                                            Fila["Comprometido"] = String.Empty;
                                            Fila["Por_Recaudar"] = String.Empty;
                                            Fila["Tipo"] = "CL_Tot";
                                            Dt_Psp.Rows.Add(Fila);

                                            if (SubConcepto)
                                            {
                                                Fila = Dt_Psp.NewRow();
                                                Fila["Concepto"] = C.Trim();
                                                Fila["Aprobado"] = String.Empty;
                                                Fila["Ampliacion"] = String.Empty;
                                                Fila["Reduccion"] = String.Empty;
                                                Fila["Modificado"] = String.Empty;
                                                Fila["Devengado"] = String.Empty;
                                                Fila["Recaudado"] = String.Empty;
                                                Fila["Dev_Rec"] = String.Empty;
                                                Fila["Comprometido"] = String.Empty;
                                                Fila["Por_Recaudar"] = String.Empty;
                                                Fila["Tipo"] = "C_Tot";
                                                Dt_Psp.Rows.Add(Fila);
                                            }

                                            if (SubConcepto)
                                            {
                                                Fila = Dt_Psp.NewRow();
                                                Fila["Concepto"] = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();
                                                Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                                Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                                Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                                Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                                Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                                Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                                Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                                Fila["Comprometido"] = "0.00";
                                                Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                                Fila["Tipo"] = "SC";
                                                Dt_Psp.Rows.Add(Fila);
                                            }
                                            else
                                            {
                                                Fila = Dt_Psp.NewRow();
                                                Fila["Concepto"] = Dr["CLAVE_NOM_CON"].ToString().Trim();
                                                Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                                Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                                Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                                Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                                Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                                Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                                Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                                Fila["Comprometido"] = "0.00";
                                                Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                                Fila["Tipo"] = "C";
                                                Dt_Psp.Rows.Add(Fila);
                                            }
                                            #endregion
                                        }
                                    }
                                    else
                                    {
                                        #region (else T)
                                        foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                                        {
                                            if (Dr_Psp["Tipo"].ToString().Trim().Equals("T_Tot"))
                                            {
                                                Dr_Psp["Aprobado"] = String.Format("{0:n}", T_Est);
                                                Dr_Psp["Ampliacion"] = String.Format("{0:n}", T_Amp);
                                                Dr_Psp["Reduccion"] = String.Format("{0:n}", T_Red);
                                                Dr_Psp["Modificado"] = String.Format("{0:n}", T_Mod);
                                                Dr_Psp["Devengado"] = String.Format("{0:n}", T_Dev);
                                                Dr_Psp["Recaudado"] = String.Format("{0:n}", T_Rec);
                                                Dr_Psp["Dev_Rec"] = String.Format("{0:n}", T_Dev_Rec);
                                                Dr_Psp["Comprometido"] = "0.00";
                                                Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", T_xRec);
                                                Dr_Psp["Tipo"] = "T";
                                            }
                                            if (Dr_Psp["Tipo"].ToString().Trim().Equals("CL_Tot"))
                                            {
                                                Dr_Psp["Aprobado"] = String.Format("{0:n}", CL_Est);
                                                Dr_Psp["Ampliacion"] = String.Format("{0:n}", CL_Amp);
                                                Dr_Psp["Reduccion"] = String.Format("{0:n}", CL_Red);
                                                Dr_Psp["Modificado"] = String.Format("{0:n}", CL_Mod);
                                                Dr_Psp["Devengado"] = String.Format("{0:n}", CL_Dev);
                                                Dr_Psp["Recaudado"] = String.Format("{0:n}", CL_Rec);
                                                Dr_Psp["Dev_Rec"] = String.Format("{0:n}", CL_Dev_Rec);
                                                Dr_Psp["Comprometido"] = "0.00";
                                                Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", CL_xRec);
                                                Dr_Psp["Tipo"] = "CL";
                                            }

                                            if (SubConcepto)
                                            {
                                                if (Dr_Psp["Tipo"].ToString().Trim().Equals("C_Tot"))
                                                {
                                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", C_Est);
                                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", C_Amp);
                                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", C_Red);
                                                    Dr_Psp["Modificado"] = String.Format("{0:n}", C_Mod);
                                                    Dr_Psp["Devengado"] = String.Format("{0:n}", C_Dev);
                                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", C_Rec);
                                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", C_Dev_Rec);
                                                    Dr_Psp["Comprometido"] = "0.00";
                                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", C_xRec);
                                                    Dr_Psp["Tipo"] = "C";
                                                }
                                            }
                                        }//fin foreach

                                        //limpiamos las variables
                                        T_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                        T_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                        T_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                        T_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                        T_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                        T_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                        T_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                        T_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                        CL_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                        CL_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                        CL_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                        CL_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                        CL_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                        CL_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                        CL_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                        CL_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                        C_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                        C_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                        C_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                        C_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                        C_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                        C_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                        C_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                        C_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());


                                        T_ID = Dr["T_ID"].ToString().Trim();
                                        CL_ID = Dr["CL_ID"].ToString().Trim();
                                        C_ID = Dr["C_ID"].ToString().Trim();
                                        SC_ID = Dr["SC_ID"].ToString().Trim();

                                        T = "*** " + Dr["CLAVE_NOM_TIPO"].ToString().Trim();
                                        CL = "** " + Dr["CLAVE_NOM_CLASE"].ToString().Trim();
                                        C = "* " + Dr["CLAVE_NOM_CON"].ToString().Trim();
                                        SC = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();

                                        if (!String.IsNullOrEmpty(SC))
                                        { SubConcepto = true; }
                                        else { SubConcepto = false; }

                                        Fila = Dt_Psp.NewRow();
                                        Fila["Concepto"] = T.Trim();
                                        Fila["Aprobado"] = String.Empty;
                                        Fila["Ampliacion"] = String.Empty;
                                        Fila["Reduccion"] = String.Empty;
                                        Fila["Modificado"] = String.Empty;
                                        Fila["Devengado"] = String.Empty;
                                        Fila["Recaudado"] = String.Empty;
                                        Fila["Dev_Rec"] = String.Empty;
                                        Fila["Comprometido"] = String.Empty;
                                        Fila["Por_Recaudar"] = String.Empty;
                                        Fila["Tipo"] = "T_Tot";
                                        Dt_Psp.Rows.Add(Fila);

                                        Fila = Dt_Psp.NewRow();
                                        Fila["Concepto"] = CL.Trim();
                                        Fila["Aprobado"] = String.Empty;
                                        Fila["Ampliacion"] = String.Empty;
                                        Fila["Reduccion"] = String.Empty;
                                        Fila["Modificado"] = String.Empty;
                                        Fila["Devengado"] = String.Empty;
                                        Fila["Recaudado"] = String.Empty;
                                        Fila["Dev_Rec"] = String.Empty;
                                        Fila["Comprometido"] = String.Empty;
                                        Fila["Por_Recaudar"] = String.Empty;
                                        Fila["Tipo"] = "CL_Tot";
                                        Dt_Psp.Rows.Add(Fila);

                                        if (SubConcepto)
                                        {
                                            Fila = Dt_Psp.NewRow();
                                            Fila["Concepto"] = C.Trim();
                                            Fila["Aprobado"] = String.Empty;
                                            Fila["Ampliacion"] = String.Empty;
                                            Fila["Reduccion"] = String.Empty;
                                            Fila["Modificado"] = String.Empty;
                                            Fila["Devengado"] = String.Empty;
                                            Fila["Recaudado"] = String.Empty;
                                            Fila["Dev_Rec"] = String.Empty;
                                            Fila["Comprometido"] = String.Empty;
                                            Fila["Por_Recaudar"] = String.Empty;
                                            Fila["Tipo"] = "C_Tot";
                                            Dt_Psp.Rows.Add(Fila);
                                        }

                                        if (SubConcepto)
                                        {
                                            Fila = Dt_Psp.NewRow();
                                            Fila["Concepto"] = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();
                                            Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                            Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                            Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                            Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                            Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                            Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                            Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                            Fila["Comprometido"] = "0.00";
                                            Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                            Fila["Tipo"] = "SC";
                                            Dt_Psp.Rows.Add(Fila);
                                        }
                                        else
                                        {
                                            Fila = Dt_Psp.NewRow();
                                            Fila["Concepto"] = Dr["CLAVE_NOM_CON"].ToString().Trim();
                                            Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                            Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                            Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                            Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                            Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                            Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                            Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                            Fila["Comprometido"] = "0.00";
                                            Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                            Fila["Tipo"] = "C";
                                            Dt_Psp.Rows.Add(Fila);
                                        }
                                        #endregion
                                    }
                                }
                                else
                                {
                                    #region (else R)
                                    foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                                    {
                                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("R_Tot"))
                                        {
                                            Dr_Psp["Aprobado"] = String.Format("{0:n}", R_Est);
                                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", R_Amp);
                                            Dr_Psp["Reduccion"] = String.Format("{0:n}", R_Red);
                                            Dr_Psp["Modificado"] = String.Format("{0:n}", R_Mod);
                                            Dr_Psp["Devengado"] = String.Format("{0:n}", R_Dev);
                                            Dr_Psp["Recaudado"] = String.Format("{0:n}", R_Rec);
                                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", R_Dev_Rec);
                                            Dr_Psp["Comprometido"] = "0.00";
                                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", R_xRec);
                                            Dr_Psp["Tipo"] = "R";
                                        }
                                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("T_Tot"))
                                        {
                                            Dr_Psp["Aprobado"] = String.Format("{0:n}", T_Est);
                                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", T_Amp);
                                            Dr_Psp["Reduccion"] = String.Format("{0:n}", T_Red);
                                            Dr_Psp["Modificado"] = String.Format("{0:n}", T_Mod);
                                            Dr_Psp["Devengado"] = String.Format("{0:n}", T_Dev);
                                            Dr_Psp["Recaudado"] = String.Format("{0:n}", T_Rec);
                                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", T_Dev_Rec);
                                            Dr_Psp["Comprometido"] = "0.00";
                                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", T_xRec);
                                            Dr_Psp["Tipo"] = "T";
                                        }
                                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("CL_Tot"))
                                        {
                                            Dr_Psp["Aprobado"] = String.Format("{0:n}", CL_Est);
                                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", CL_Amp);
                                            Dr_Psp["Reduccion"] = String.Format("{0:n}", CL_Red);
                                            Dr_Psp["Modificado"] = String.Format("{0:n}", CL_Mod);
                                            Dr_Psp["Devengado"] = String.Format("{0:n}", CL_Dev);
                                            Dr_Psp["Recaudado"] = String.Format("{0:n}", CL_Rec);
                                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", CL_Dev_Rec);
                                            Dr_Psp["Comprometido"] = "0.00";
                                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", CL_xRec);
                                            Dr_Psp["Tipo"] = "CL";
                                        }

                                        if (SubConcepto)
                                        {
                                            if (Dr_Psp["Tipo"].ToString().Trim().Equals("C_Tot"))
                                            {
                                                Dr_Psp["Aprobado"] = String.Format("{0:n}", C_Est);
                                                Dr_Psp["Ampliacion"] = String.Format("{0:n}", C_Amp);
                                                Dr_Psp["Reduccion"] = String.Format("{0:n}", C_Red);
                                                Dr_Psp["Modificado"] = String.Format("{0:n}", C_Mod);
                                                Dr_Psp["Devengado"] = String.Format("{0:n}", CL_Dev);
                                                Dr_Psp["Recaudado"] = String.Format("{0:n}", C_Rec);
                                                Dr_Psp["Dev_Rec"] = String.Format("{0:n}", CL_Dev_Rec);
                                                Dr_Psp["Comprometido"] = "0.00";
                                                Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", C_xRec);
                                                Dr_Psp["Tipo"] = "C";
                                            }
                                        }
                                    }//fin foreach

                                    //limpiamos las variables
                                    R_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                    R_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                    R_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                    R_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                    R_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                    R_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                    R_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                    R_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                    //limpiamos las variables
                                    T_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                    T_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                    T_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                    T_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                    T_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                    T_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                    T_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                    T_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                    CL_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                    CL_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                    CL_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                    CL_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                    CL_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                    CL_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                    CL_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                    CL_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                    C_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                    C_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                    C_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                    C_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                    C_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                    C_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                    C_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                    C_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                    R_ID = Dr["R_ID"].ToString().Trim();
                                    T_ID = Dr["T_ID"].ToString().Trim();
                                    CL_ID = Dr["CL_ID"].ToString().Trim();
                                    C_ID = Dr["C_ID"].ToString().Trim();
                                    SC_ID = Dr["SC_ID"].ToString().Trim();

                                    R = "**** " + Dr["CLAVE_NOM_RUBRO"].ToString().Trim();
                                    T = "*** " + Dr["CLAVE_NOM_TIPO"].ToString().Trim();
                                    CL = "** " + Dr["CLAVE_NOM_CLASE"].ToString().Trim();
                                    C = "* " + Dr["CLAVE_NOM_CON"].ToString().Trim();
                                    SC = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();

                                    if (!String.IsNullOrEmpty(SC))
                                    { SubConcepto = true; }
                                    else { SubConcepto = false; }


                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = R.Trim();
                                    Fila["Aprobado"] = String.Empty;
                                    Fila["Ampliacion"] = String.Empty;
                                    Fila["Reduccion"] = String.Empty;
                                    Fila["Modificado"] = String.Empty;
                                    Fila["Devengado"] = String.Empty;
                                    Fila["Recaudado"] = String.Empty;
                                    Fila["Dev_Rec"] = String.Empty;
                                    Fila["Comprometido"] = String.Empty;
                                    Fila["Por_Recaudar"] = String.Empty;
                                    Fila["Tipo"] = "R_Tot";
                                    Dt_Psp.Rows.Add(Fila);

                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = T.Trim();
                                    Fila["Aprobado"] = String.Empty;
                                    Fila["Ampliacion"] = String.Empty;
                                    Fila["Reduccion"] = String.Empty;
                                    Fila["Modificado"] = String.Empty;
                                    Fila["Devengado"] = String.Empty;
                                    Fila["Recaudado"] = String.Empty;
                                    Fila["Dev_Rec"] = String.Empty;
                                    Fila["Comprometido"] = String.Empty;
                                    Fila["Por_Recaudar"] = String.Empty;
                                    Fila["Tipo"] = "T_Tot";
                                    Dt_Psp.Rows.Add(Fila);

                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = CL.Trim();
                                    Fila["Aprobado"] = String.Empty;
                                    Fila["Ampliacion"] = String.Empty;
                                    Fila["Reduccion"] = String.Empty;
                                    Fila["Modificado"] = String.Empty;
                                    Fila["Devengado"] = String.Empty;
                                    Fila["Recaudado"] = String.Empty;
                                    Fila["Dev_Rec"] = String.Empty;
                                    Fila["Comprometido"] = String.Empty;
                                    Fila["Por_Recaudar"] = String.Empty;
                                    Fila["Tipo"] = "CL_Tot";
                                    Dt_Psp.Rows.Add(Fila);

                                    if (SubConcepto)
                                    {
                                        Fila = Dt_Psp.NewRow();
                                        Fila["Concepto"] = C.Trim();
                                        Fila["Aprobado"] = String.Empty;
                                        Fila["Ampliacion"] = String.Empty;
                                        Fila["Reduccion"] = String.Empty;
                                        Fila["Modificado"] = String.Empty;
                                        Fila["Devengado"] = String.Empty;
                                        Fila["Recaudado"] = String.Empty;
                                        Fila["Dev_Rec"] = String.Empty;
                                        Fila["Comprometido"] = String.Empty;
                                        Fila["Por_Recaudar"] = String.Empty;
                                        Fila["Tipo"] = "C_Tot";
                                        Dt_Psp.Rows.Add(Fila);
                                    }


                                    if (SubConcepto)
                                    {
                                        Fila = Dt_Psp.NewRow();
                                        Fila["Concepto"] = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();
                                        Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                        Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                        Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                        Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                        Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                        Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                        Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                        Fila["Comprometido"] = "0.00";
                                        Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                        Fila["Tipo"] = "SC";
                                        Dt_Psp.Rows.Add(Fila);
                                    }
                                    else
                                    {
                                        Fila = Dt_Psp.NewRow();
                                        Fila["Concepto"] = Dr["CLAVE_NOM_CON"].ToString().Trim();
                                        Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                        Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                        Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                        Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                        Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                        Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                        Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                        Fila["Comprometido"] = "0.00";
                                        Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                        Fila["Tipo"] = "C";
                                        Dt_Psp.Rows.Add(Fila);
                                    }
                                    #endregion
                                }
                            }
                            else
                            {
                                #region (else PP)
                                foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                                {
                                    if (Programa)
                                    {
                                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("PP_Tot"))
                                        {
                                            Dr_Psp["Aprobado"] = String.Format("{0:n}", PP_Est);
                                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", PP_Amp);
                                            Dr_Psp["Reduccion"] = String.Format("{0:n}", PP_Red);
                                            Dr_Psp["Modificado"] = String.Format("{0:n}", PP_Mod);
                                            Dr_Psp["Devengado"] = String.Format("{0:n}", PP_Dev);
                                            Dr_Psp["Recaudado"] = String.Format("{0:n}", PP_Rec);
                                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PP_Dev_Rec);
                                            Dr_Psp["Comprometido"] = "0.00";
                                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PP_xRec);
                                            Dr_Psp["Tipo"] = "PP";
                                        }
                                    }

                                    if (Dr_Psp["Tipo"].ToString().Trim().Equals("R_Tot"))
                                    {
                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", R_Est);
                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", R_Amp);
                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", R_Red);
                                        Dr_Psp["Modificado"] = String.Format("{0:n}", R_Mod);
                                        Dr_Psp["Devengado"] = String.Format("{0:n}", R_Dev);
                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", R_Rec);
                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", R_Dev_Rec);
                                        Dr_Psp["Comprometido"] = "0.00";
                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", R_xRec);
                                        Dr_Psp["Tipo"] = "R";
                                    }
                                    if (Dr_Psp["Tipo"].ToString().Trim().Equals("T_Tot"))
                                    {
                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", T_Est);
                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", T_Amp);
                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", T_Red);
                                        Dr_Psp["Modificado"] = String.Format("{0:n}", T_Mod);
                                        Dr_Psp["Devengado"] = String.Format("{0:n}", T_Dev);
                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", T_Rec);
                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", T_Dev_Rec);
                                        Dr_Psp["Comprometido"] = "0.00";
                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", T_xRec);
                                        Dr_Psp["Tipo"] = "T";
                                    }
                                    if (Dr_Psp["Tipo"].ToString().Trim().Equals("CL_Tot"))
                                    {
                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", CL_Est);
                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", CL_Amp);
                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", CL_Red);
                                        Dr_Psp["Modificado"] = String.Format("{0:n}", CL_Mod);
                                        Dr_Psp["Devengado"] = String.Format("{0:n}", CL_Dev);
                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", CL_Rec);
                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", CL_Dev_Rec);
                                        Dr_Psp["Comprometido"] = "0.00";
                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", CL_xRec);
                                        Dr_Psp["Tipo"] = "CL";
                                    }

                                    if (SubConcepto)
                                    {
                                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("C_Tot"))
                                        {
                                            Dr_Psp["Aprobado"] = String.Format("{0:n}", C_Est);
                                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", C_Amp);
                                            Dr_Psp["Reduccion"] = String.Format("{0:n}", C_Red);
                                            Dr_Psp["Modificado"] = String.Format("{0:n}", C_Mod);
                                            Dr_Psp["Devengado"] = String.Format("{0:n}", C_Dev);
                                            Dr_Psp["Recaudado"] = String.Format("{0:n}", C_Rec);
                                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", C_Dev_Rec);
                                            Dr_Psp["Comprometido"] = "0.00";
                                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", C_xRec);
                                            Dr_Psp["Tipo"] = "C";
                                        }
                                    }
                                }//fin foreach

                                //limpiamos las variables
                                PP_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                PP_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                PP_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                PP_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                PP_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                PP_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                PP_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                PP_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                //limpiamos las variables
                                R_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                R_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                R_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                R_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                R_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                R_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                R_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                R_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                //limpiamos las variables
                                T_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                T_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                T_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                T_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                T_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                T_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                T_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                T_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                CL_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                CL_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                CL_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                CL_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                CL_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                CL_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                CL_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                CL_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                C_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                                C_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                C_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                C_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                C_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                                C_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                                C_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                                C_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                                PP_ID = Dr["PP_ID"].ToString().Trim();
                                R_ID = Dr["R_ID"].ToString().Trim();
                                T_ID = Dr["T_ID"].ToString().Trim();
                                CL_ID = Dr["CL_ID"].ToString().Trim();
                                C_ID = Dr["C_ID"].ToString().Trim();
                                SC_ID = Dr["SC_ID"].ToString().Trim();

                                PP = "***** " + Dr["CLAVE_NOM_PP"].ToString().Trim();
                                R = "**** " + Dr["CLAVE_NOM_RUBRO"].ToString().Trim();
                                T = "*** " + Dr["CLAVE_NOM_TIPO"].ToString().Trim();
                                CL = "** " + Dr["CLAVE_NOM_CLASE"].ToString().Trim();
                                C = "* " + Dr["CLAVE_NOM_CON"].ToString().Trim();
                                SC = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();

                                if (!String.IsNullOrEmpty(PP))
                                { Programa = true; }
                                else { Programa = false; }
                                if (!String.IsNullOrEmpty(SC))
                                { SubConcepto = true; }
                                else { SubConcepto = false; }

                                if (Programa)
                                {
                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = PP.Trim();
                                    Fila["Aprobado"] = String.Empty;
                                    Fila["Ampliacion"] = String.Empty;
                                    Fila["Reduccion"] = String.Empty;
                                    Fila["Modificado"] = String.Empty;
                                    Fila["Devengado"] = String.Empty;
                                    Fila["Recaudado"] = String.Empty;
                                    Fila["Dev_Rec"] = String.Empty;
                                    Fila["Comprometido"] = String.Empty;
                                    Fila["Por_Recaudar"] = String.Empty;
                                    Fila["Tipo"] = "PP_Tot";
                                    Dt_Psp.Rows.Add(Fila);
                                }

                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = R.Trim();
                                Fila["Aprobado"] = String.Empty;
                                Fila["Ampliacion"] = String.Empty;
                                Fila["Reduccion"] = String.Empty;
                                Fila["Modificado"] = String.Empty;
                                Fila["Devengado"] = String.Empty;
                                Fila["Recaudado"] = String.Empty;
                                Fila["Dev_Rec"] = String.Empty;
                                Fila["Comprometido"] = String.Empty;
                                Fila["Por_Recaudar"] = String.Empty;
                                Fila["Tipo"] = "R_Tot";
                                Dt_Psp.Rows.Add(Fila);

                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = T.Trim();
                                Fila["Aprobado"] = String.Empty;
                                Fila["Ampliacion"] = String.Empty;
                                Fila["Reduccion"] = String.Empty;
                                Fila["Modificado"] = String.Empty;
                                Fila["Devengado"] = String.Empty;
                                Fila["Recaudado"] = String.Empty;
                                Fila["Dev_Rec"] = String.Empty;
                                Fila["Comprometido"] = String.Empty;
                                Fila["Por_Recaudar"] = String.Empty;
                                Fila["Tipo"] = "T_Tot";
                                Dt_Psp.Rows.Add(Fila);

                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = CL.Trim();
                                Fila["Aprobado"] = String.Empty;
                                Fila["Ampliacion"] = String.Empty;
                                Fila["Reduccion"] = String.Empty;
                                Fila["Modificado"] = String.Empty;
                                Fila["Devengado"] = String.Empty;
                                Fila["Recaudado"] = String.Empty;
                                Fila["Dev_Rec"] = String.Empty;
                                Fila["Comprometido"] = String.Empty;
                                Fila["Por_Recaudar"] = String.Empty;
                                Fila["Tipo"] = "CL_Tot";
                                Dt_Psp.Rows.Add(Fila);

                                if (SubConcepto)
                                {
                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = C.Trim();
                                    Fila["Aprobado"] = String.Empty;
                                    Fila["Ampliacion"] = String.Empty;
                                    Fila["Reduccion"] = String.Empty;
                                    Fila["Modificado"] = String.Empty;
                                    Fila["Devengado"] = String.Empty;
                                    Fila["Recaudado"] = String.Empty;
                                    Fila["Dev_Rec"] = String.Empty;
                                    Fila["Comprometido"] = String.Empty;
                                    Fila["Por_Recaudar"] = String.Empty;
                                    Fila["Tipo"] = "C_Tot";
                                    Dt_Psp.Rows.Add(Fila);
                                }


                                if (SubConcepto)
                                {
                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();
                                    Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                    Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                    Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                    Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                    Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                    Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                    Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                    Fila["Comprometido"] = "0.00";
                                    Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                    Fila["Tipo"] = "SC";
                                    Dt_Psp.Rows.Add(Fila);
                                }
                                else
                                {
                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = Dr["CLAVE_NOM_CON"].ToString().Trim();
                                    Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                    Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                    Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                    Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                    Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                    Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                    Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                    Fila["Comprometido"] = "0.00";
                                    Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                    Fila["Tipo"] = "C";
                                    Dt_Psp.Rows.Add(Fila);
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            #region (else FF)
                            foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                            {
                                if (Dr_Psp["Tipo"].ToString().Trim().Equals("FF_Tot"))
                                {
                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", FF_Est);
                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", FF_Amp);
                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", FF_Red);
                                    Dr_Psp["Modificado"] = String.Format("{0:n}", FF_Mod);
                                    Dr_Psp["Devengado"] = String.Format("{0:n}", FF_Dev);
                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", FF_Rec);
                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", FF_Dev_Rec);
                                    Dr_Psp["Comprometido"] = "0.00";
                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", FF_xRec);
                                    Dr_Psp["Tipo"] = "FF";
                                }

                                if (Programa)
                                {
                                    if (Dr_Psp["Tipo"].ToString().Trim().Equals("PP_Tot"))
                                    {
                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", PP_Est);
                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", PP_Amp);
                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", PP_Red);
                                        Dr_Psp["Modificado"] = String.Format("{0:n}", PP_Mod);
                                        Dr_Psp["Devengado"] = String.Format("{0:n}", PP_Dev);
                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", PP_Rec);
                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PP_Dev_Rec);
                                        Dr_Psp["Comprometido"] = "0.00";
                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PP_xRec);
                                        Dr_Psp["Tipo"] = "PP";
                                    }
                                }

                                if (Dr_Psp["Tipo"].ToString().Trim().Equals("R_Tot"))
                                {
                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", R_Est);
                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", R_Amp);
                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", R_Red);
                                    Dr_Psp["Modificado"] = String.Format("{0:n}", R_Mod);
                                    Dr_Psp["Devengado"] = String.Format("{0:n}", R_Dev);
                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", R_Rec);
                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", R_Dev_Rec);
                                    Dr_Psp["Comprometido"] = "0.00";
                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", R_xRec);
                                    Dr_Psp["Tipo"] = "R";
                                }
                                if (Dr_Psp["Tipo"].ToString().Trim().Equals("T_Tot"))
                                {
                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", T_Est);
                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", T_Amp);
                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", T_Red);
                                    Dr_Psp["Modificado"] = String.Format("{0:n}", T_Mod);
                                    Dr_Psp["Devengado"] = String.Format("{0:n}", T_Dev);
                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", T_Rec);
                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", T_Dev_Rec);
                                    Dr_Psp["Comprometido"] = "0.00";
                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", T_xRec);
                                    Dr_Psp["Tipo"] = "T";
                                }
                                if (Dr_Psp["Tipo"].ToString().Trim().Equals("CL_Tot"))
                                {
                                    Dr_Psp["Aprobado"] = String.Format("{0:n}", CL_Est);
                                    Dr_Psp["Ampliacion"] = String.Format("{0:n}", CL_Amp);
                                    Dr_Psp["Reduccion"] = String.Format("{0:n}", CL_Red);
                                    Dr_Psp["Modificado"] = String.Format("{0:n}", CL_Mod);
                                    Dr_Psp["Devengado"] = String.Format("{0:n}", CL_Dev);
                                    Dr_Psp["Recaudado"] = String.Format("{0:n}", CL_Rec);
                                    Dr_Psp["Dev_Rec"] = String.Format("{0:n}", CL_Dev_Rec);
                                    Dr_Psp["Comprometido"] = "0.00";
                                    Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", CL_xRec);
                                    Dr_Psp["Tipo"] = "CL";
                                }

                                if (SubConcepto)
                                {
                                    if (Dr_Psp["Tipo"].ToString().Trim().Equals("C_Tot"))
                                    {
                                        Dr_Psp["Aprobado"] = String.Format("{0:n}", C_Est);
                                        Dr_Psp["Ampliacion"] = String.Format("{0:n}", C_Amp);
                                        Dr_Psp["Reduccion"] = String.Format("{0:n}", C_Red);
                                        Dr_Psp["Modificado"] = String.Format("{0:n}", C_Mod);
                                        Dr_Psp["Devengado"] = String.Format("{0:n}", C_Dev);
                                        Dr_Psp["Recaudado"] = String.Format("{0:n}", C_Rec);
                                        Dr_Psp["Dev_Rec"] = String.Format("{0:n}", C_Dev_Rec);
                                        Dr_Psp["Comprometido"] = "0.00";
                                        Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", C_xRec);
                                        Dr_Psp["Tipo"] = "C";
                                    }
                                }
                            }//fin foreach

                            //limpiamos las variables
                            FF_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                            FF_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            FF_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            FF_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            FF_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                            FF_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                            FF_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            FF_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                            //limpiamos las variables
                            PP_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                            PP_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            PP_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            PP_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            PP_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                            PP_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                            PP_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            PP_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                            //limpiamos las variables
                            R_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                            R_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            R_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            R_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            R_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                            R_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                            R_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            R_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                            //limpiamos las variables
                            T_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                            T_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            T_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            T_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            T_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                            T_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                            T_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            T_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                            CL_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                            CL_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            CL_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            CL_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            CL_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                            CL_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                            CL_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            CL_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                            C_Est = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                            C_Amp = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            C_Red = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            C_Mod = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            C_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim());
                            C_xRec = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                            C_Dev = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim());
                            C_Dev_Rec = Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim());

                            FF_ID = Dr["FF_ID"].ToString().Trim();
                            PP_ID = Dr["PP_ID"].ToString().Trim();
                            R_ID = Dr["R_ID"].ToString().Trim();
                            T_ID = Dr["T_ID"].ToString().Trim();
                            CL_ID = Dr["CL_ID"].ToString().Trim();
                            C_ID = Dr["C_ID"].ToString().Trim();
                            SC_ID = Dr["SC_ID"].ToString().Trim();

                            FF = "****** " + Dr["CLAVE_NOM_FF"].ToString().Trim();
                            PP = "***** " + Dr["CLAVE_NOM_PP"].ToString().Trim();
                            R = "**** " + Dr["CLAVE_NOM_RUBRO"].ToString().Trim();
                            T = "*** " + Dr["CLAVE_NOM_TIPO"].ToString().Trim();
                            CL = "** " + Dr["CLAVE_NOM_CLASE"].ToString().Trim();
                            C = "* " + Dr["CLAVE_NOM_CON"].ToString().Trim();
                            SC = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();

                            if (!String.IsNullOrEmpty(PP))
                            { Programa = true; }
                            else { Programa = false; }
                            if (!String.IsNullOrEmpty(SC))
                            { SubConcepto = true; }
                            else { SubConcepto = false; }

                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = FF.Trim();
                            Fila["Aprobado"] = String.Empty;
                            Fila["Ampliacion"] = String.Empty;
                            Fila["Reduccion"] = String.Empty;
                            Fila["Modificado"] = String.Empty;
                            Fila["Devengado"] = String.Empty;
                            Fila["Recaudado"] = String.Empty;
                            Fila["Dev_Rec"] = String.Empty;
                            Fila["Comprometido"] = String.Empty;
                            Fila["Por_Recaudar"] = String.Empty;
                            Fila["Tipo"] = "FF_Tot";
                            Dt_Psp.Rows.Add(Fila);

                            if (Programa)
                            {
                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = PP.Trim();
                                Fila["Aprobado"] = String.Empty;
                                Fila["Ampliacion"] = String.Empty;
                                Fila["Reduccion"] = String.Empty;
                                Fila["Modificado"] = String.Empty;
                                Fila["Devengado"] = String.Empty;
                                Fila["Recaudado"] = String.Empty;
                                Fila["Dev_Rec"] = String.Empty;
                                Fila["Comprometido"] = String.Empty;
                                Fila["Por_Recaudar"] = String.Empty;
                                Fila["Tipo"] = "PP_Tot";
                                Dt_Psp.Rows.Add(Fila);
                            }

                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = R.Trim();
                            Fila["Aprobado"] = String.Empty;
                            Fila["Ampliacion"] = String.Empty;
                            Fila["Reduccion"] = String.Empty;
                            Fila["Modificado"] = String.Empty;
                            Fila["Devengado"] = String.Empty;
                            Fila["Recaudado"] = String.Empty;
                            Fila["Dev_Rec"] = String.Empty;
                            Fila["Comprometido"] = String.Empty;
                            Fila["Por_Recaudar"] = String.Empty;
                            Fila["Tipo"] = "R_Tot";
                            Dt_Psp.Rows.Add(Fila);

                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = T.Trim();
                            Fila["Aprobado"] = String.Empty;
                            Fila["Ampliacion"] = String.Empty;
                            Fila["Reduccion"] = String.Empty;
                            Fila["Modificado"] = String.Empty;
                            Fila["Devengado"] = String.Empty;
                            Fila["Recaudado"] = String.Empty;
                            Fila["Dev_Rec"] = String.Empty;
                            Fila["Comprometido"] = String.Empty;
                            Fila["Por_Recaudar"] = String.Empty;
                            Fila["Tipo"] = "T_Tot";
                            Dt_Psp.Rows.Add(Fila);

                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = CL.Trim();
                            Fila["Aprobado"] = String.Empty;
                            Fila["Ampliacion"] = String.Empty;
                            Fila["Reduccion"] = String.Empty;
                            Fila["Modificado"] = String.Empty;
                            Fila["Devengado"] = String.Empty;
                            Fila["Recaudado"] = String.Empty;
                            Fila["Dev_Rec"] = String.Empty;
                            Fila["Comprometido"] = String.Empty;
                            Fila["Por_Recaudar"] = String.Empty;
                            Fila["Tipo"] = "CL_Tot";
                            Dt_Psp.Rows.Add(Fila);

                            if (SubConcepto)
                            {
                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = C.Trim();
                                Fila["Aprobado"] = String.Empty;
                                Fila["Ampliacion"] = String.Empty;
                                Fila["Reduccion"] = String.Empty;
                                Fila["Modificado"] = String.Empty;
                                Fila["Devengado"] = String.Empty;
                                Fila["Recaudado"] = String.Empty;
                                Fila["Dev_Rec"] = String.Empty;
                                Fila["Comprometido"] = String.Empty;
                                Fila["Por_Recaudar"] = String.Empty;
                                Fila["Tipo"] = "C_Tot";
                                Dt_Psp.Rows.Add(Fila);
                            }

                            if (SubConcepto)
                            {
                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();
                                Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                Fila["Comprometido"] = "0.00";
                                Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                Fila["Tipo"] = "SC";
                                Dt_Psp.Rows.Add(Fila);
                            }
                            else
                            {
                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = Dr["CLAVE_NOM_CON"].ToString().Trim();
                                Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                Fila["Dev_Rec"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEV_REC"].ToString().Trim()) ? "0" : Dr["DEV_REC"].ToString().Trim()));
                                Fila["Comprometido"] = "0.00";
                                Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                Fila["Tipo"] = "C";
                                Dt_Psp.Rows.Add(Fila);
                            }

                            #endregion
                        }
                    }

                    #region (Fin Dt Psp)
                    foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                    {
                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", Tot_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", Tot_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", Tot_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", Tot_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", Tot_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", Tot_Rec);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", Tot_Dev_Rec);
                            Dr_Psp["Comprometido"] = "0.00";
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", Tot_xRec);
                            Dr_Psp["Tipo"] = "Tot";
                        }

                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("FF_Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", FF_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", FF_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", FF_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", FF_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", FF_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", FF_Rec);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", FF_Dev_Rec);
                            Dr_Psp["Comprometido"] = "0.00";
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", FF_xRec);
                            Dr_Psp["Tipo"] = "FF";
                        }

                        if (Programa)
                        {
                            if (Dr_Psp["Tipo"].ToString().Trim().Equals("PP_Tot"))
                            {
                                Dr_Psp["Aprobado"] = String.Format("{0:n}", PP_Est);
                                Dr_Psp["Ampliacion"] = String.Format("{0:n}", PP_Amp);
                                Dr_Psp["Reduccion"] = String.Format("{0:n}", PP_Red);
                                Dr_Psp["Modificado"] = String.Format("{0:n}", PP_Mod);
                                Dr_Psp["Devengado"] = String.Format("{0:n}", PP_Dev);
                                Dr_Psp["Recaudado"] = String.Format("{0:n}", PP_Rec);
                                Dr_Psp["Dev_Rec"] = String.Format("{0:n}", PP_Dev_Rec);
                                Dr_Psp["Comprometido"] = "0.00";
                                Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", PP_xRec);
                                Dr_Psp["Tipo"] = "PP";
                            }
                        }

                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("R_Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", R_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", R_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", R_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", R_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", R_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", R_Rec);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", R_Dev_Rec);
                            Dr_Psp["Comprometido"] = "0.00";
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", R_xRec);
                            Dr_Psp["Tipo"] = "R";
                        }
                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("T_Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", T_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", T_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", T_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", T_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", T_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", T_Rec);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", T_Dev_Rec);
                            Dr_Psp["Comprometido"] = "0.00";
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", T_xRec);
                            Dr_Psp["Tipo"] = "T";
                        }
                        if (Dr_Psp["Tipo"].ToString().Trim().Equals("CL_Tot"))
                        {
                            Dr_Psp["Aprobado"] = String.Format("{0:n}", CL_Est);
                            Dr_Psp["Ampliacion"] = String.Format("{0:n}", CL_Amp);
                            Dr_Psp["Reduccion"] = String.Format("{0:n}", CL_Red);
                            Dr_Psp["Modificado"] = String.Format("{0:n}", CL_Mod);
                            Dr_Psp["Devengado"] = String.Format("{0:n}", CL_Dev);
                            Dr_Psp["Recaudado"] = String.Format("{0:n}", CL_Rec);
                            Dr_Psp["Dev_Rec"] = String.Format("{0:n}", CL_Dev_Rec);
                            Dr_Psp["Comprometido"] = "0.00";
                            Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", CL_xRec);
                            Dr_Psp["Tipo"] = "CL";
                        }

                        if (SubConcepto)
                        {
                            if (Dr_Psp["Tipo"].ToString().Trim().Equals("C_Tot"))
                            {
                                Dr_Psp["Aprobado"] = String.Format("{0:n}", C_Est);
                                Dr_Psp["Ampliacion"] = String.Format("{0:n}", C_Amp);
                                Dr_Psp["Reduccion"] = String.Format("{0:n}", C_Red);
                                Dr_Psp["Modificado"] = String.Format("{0:n}", C_Mod);
                                Dr_Psp["Devengado"] = String.Format("{0:n}", C_Dev);
                                Dr_Psp["Recaudado"] = String.Format("{0:n}", C_Rec);
                                Dr_Psp["Dev_Rec"] = String.Format("{0:n}", C_Dev_Rec);
                                Dr_Psp["Comprometido"] = "0.00";
                                Dr_Psp["Por_Recaudar"] = String.Format("{0:n}", C_xRec);
                                Dr_Psp["Tipo"] = "C";
                            }
                        }
                    }//fin foreach
                    #endregion
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al generar el presupuesto de ingresos. Error[" + ex.Message + "]");
                }
                return Dt_Psp;
            }
        #endregion

        #region (Generar Poliza)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Poliza
            ///DESCRIPCIÓN          : Metodo para obtener los datos para generar la poliza
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 04/Diciembre/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Poliza(String No_Poliza, String Tipo_Poliza, String Mes_Anio)
            {
                Cls_Ope_Con_Polizas_Negocio Negocio = new Cls_Ope_Con_Polizas_Negocio(); //conexion con la capa de negocios
                DataSet Ds_Polizas = new DataSet();
                DataTable Dt_Polizas = new DataTable();
                String Nom_Archivo = "";

                try
                {
                    if (!String.IsNullOrEmpty(No_Poliza.Trim()) && !String.IsNullOrEmpty(Tipo_Poliza.Trim())
                        && !String.IsNullOrEmpty(Mes_Anio.Trim()))
                    {
                        Negocio.P_No_Poliza = No_Poliza.Trim();
                        Negocio.P_Tipo_Poliza_ID = Tipo_Poliza.Trim();
                        Negocio.P_Mes_Ano = Mes_Anio.Trim();
                        Dt_Polizas = Negocio.Consulta_Detalle_Poliza();

                        if (Dt_Polizas != null)
                        {
                            if (Dt_Polizas.Rows.Count > 0)
                            {
                                Dt_Polizas.TableName = "Dt_Datos_Poliza";
                                Ds_Polizas.Tables.Add(Dt_Polizas.Copy());
                                Exportar_Reporte(Ds_Polizas, "Rpt_Con_Poliza.rpt", "Rpt_Poliza_" + Session.SessionID, "pdf",
                                    ExportFormatType.PortableDocFormat, "Contabilidad");
                                Nom_Archivo = "../../Reporte/Rpt_Poliza_" + Session.SessionID + ".pdf     ";
                            }
                        }
                    }

                    return Nom_Archivo;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Poliza Error[" + ex.Message + "]");
                }
            }
        #endregion

        #region (Generar Reporte Movimientos Egresos Ingresos)
            ///*****************************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Crear_Solicitud_Movimientos_Egresos
            ///DESCRIPCIÓN          : Metodo para crear el reporte de movimientos de egresos
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 01/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*****************************************************************************************************************
            public String Crear_Solicitud_Movimientos_Egresos(String No_Movimiento, String No_Solicitud, String Tipo_Movimiento, String Anio)
            {
                DataSet Ds_Registros = null;
                DataTable Dt_Encabezado = new DataTable();
                DataTable Dt_Mov_Justificacion = new DataTable();
                DataTable Dt_Mov_Origen = new DataTable();
                DataTable Dt_Mov_Destino = new DataTable();
                DataTable Dt_Mov_Origen_Det = new DataTable();
                DataTable Dt_Mov_Destino_Det = new DataTable();
                String Nom_Archivo = String.Empty;

                try
                {
                    if (Tipo_Movimiento.Trim() != "ADECUACION")
                    {
                        Ds_Registros = new DataSet();
                        Dt_Encabezado = Crear_Dt_Encabezado(No_Movimiento, No_Solicitud);
                        Dt_Mov_Origen = Crear_Dt_Movimientos("Origen", "Normal", No_Solicitud);
                        Dt_Mov_Destino = Crear_Dt_Movimientos("Destino", "Normal", No_Solicitud);
                        Dt_Mov_Origen_Det = Crear_Dt_Movimientos("Origen", "Det", No_Solicitud);
                        Dt_Mov_Destino_Det = Crear_Dt_Movimientos("Destino", "Det", No_Solicitud);
                        Dt_Mov_Justificacion = Crear_Dt_Movimientos("", "Jus", No_Solicitud);
                        Dt_Encabezado.TableName = "Dt_Encabezado";
                        Dt_Mov_Origen.TableName = "Dt_Mov_Origen";
                        Dt_Mov_Destino.TableName = "Dt_Mov_Destino";
                        Dt_Mov_Origen_Det.TableName = "Dt_Mov_Origen_Det";
                        Dt_Mov_Destino_Det.TableName = "Dt_Mov_Destino_Det";
                        Dt_Mov_Justificacion.TableName = "Dt_Mov_Justificacion";

                        Ds_Registros.Tables.Add(Dt_Encabezado.Copy());
                        Ds_Registros.Tables.Add(Dt_Mov_Origen.Copy());
                        Ds_Registros.Tables.Add(Dt_Mov_Destino.Copy());
                        Ds_Registros.Tables.Add(Dt_Mov_Origen_Det.Copy());
                        Ds_Registros.Tables.Add(Dt_Mov_Destino_Det.Copy());
                        Ds_Registros.Tables.Add(Dt_Mov_Justificacion.Copy());

                        if (Tipo_Movimiento.Trim().Equals("TRASPASO"))
                        {
                            Exportar_Reporte(Ds_Registros, "Cr_Ope_Psp_Movimiento_Presupuestal.rpt", "Rpt_Movimientos_" + Session.SessionID,
                                "pdf", ExportFormatType.PortableDocFormat, "Presupuestos");
                        }
                        else
                        {
                            Exportar_Reporte(Ds_Registros, "Cr_Rpt_Psp_Movimiento_Presupuestal.rpt", "Rpt_Movimientos_" + Session.SessionID,
                                "pdf", ExportFormatType.PortableDocFormat, "Presupuestos");
                        }
                    }
                    else 
                    {
                        Ds_Registros = new DataSet();
                        Dt_Encabezado = Crear_Dt_Encabezado_Adecuacion(No_Movimiento, No_Solicitud);
                        Dt_Mov_Origen = Crear_Dt_Movimientos_Adecuacion("Origen", "Normal", No_Solicitud);
                        Dt_Mov_Destino = Crear_Dt_Movimientos_Adecuacion("Destino", "Normal", No_Solicitud);
                        Dt_Mov_Origen_Det = Crear_Dt_Movimientos_Adecuacion("Origen", "Det", No_Solicitud);
                        Dt_Mov_Destino_Det = Crear_Dt_Movimientos_Adecuacion("Destino", "Det", No_Solicitud);
                        Dt_Mov_Justificacion = Crear_Dt_Movimientos_Adecuacion("", "Jus", No_Solicitud);
                        Dt_Encabezado.TableName = "Dt_Encabezado";
                        Dt_Mov_Origen.TableName = "Dt_Mov_Origen";
                        Dt_Mov_Destino.TableName = "Dt_Mov_Destino";
                        Dt_Mov_Origen_Det.TableName = "Dt_Mov_Origen_Det";
                        Dt_Mov_Destino_Det.TableName = "Dt_Mov_Destino_Det";
                        Dt_Mov_Justificacion.TableName = "Dt_Mov_Justificacion";

                        Ds_Registros.Tables.Add(Dt_Encabezado.Copy());
                        Ds_Registros.Tables.Add(Dt_Mov_Origen.Copy());
                        Ds_Registros.Tables.Add(Dt_Mov_Destino.Copy());
                        Ds_Registros.Tables.Add(Dt_Mov_Origen_Det.Copy());
                        Ds_Registros.Tables.Add(Dt_Mov_Destino_Det.Copy());
                        Ds_Registros.Tables.Add(Dt_Mov_Justificacion.Copy());

                        Exportar_Reporte(Ds_Registros, "Cr_Ope_Psp_Adecuacion_Presupuestal.rpt", "Rpt_Movimientos_" + Session.SessionID,
                               "pdf", ExportFormatType.PortableDocFormat, "Presupuestos");
                    }

                    
                    Nom_Archivo = "../../Reporte/Rpt_Movimientos_" + Session.SessionID + ".pdf     ";
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al generar el reporte. Error[" + ex.Message + "]");
                }
                return Nom_Archivo;
            }

            ///*****************************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Encabezado
            ///DESCRIPCIÓN          : METODO PARA CREAR EL REPORTE DE MOVIMIENTOS
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 01/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*****************************************************************************************************************
            public DataTable Crear_Dt_Encabezado(String No_Mov, String No_Solicitud)
            {
                Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
                DataTable Dt_Encabezado = new DataTable();
                DataRow Fila;
                String Texto = String.Empty;
                String UR = String.Empty;
                String Coordinador = String.Empty;
                String Puesto_Coordinador = String.Empty;
                String Puesto_Solicito = String.Empty;
                String Solicito = String.Empty;
                DataTable Dt = new DataTable();

                try
                {
                    Negocio.P_No_Solicitud = No_Solicitud;
                    Dt = Negocio.Consultar_Datos_Movimientos_Det();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            Solicito = Dt.Rows[0][Ope_Psp_Movimiento_Egr_Det.Campo_Nombre_Solicitante].ToString().Trim().ToUpper();
                            Puesto_Solicito = Dt.Rows[0][Ope_Psp_Movimiento_Egr_Det.Campo_Puesto_Solicitante].ToString().Trim().ToUpper();
                            Coordinador = Dt.Rows[0][Ope_Psp_Movimiento_Egr_Det.Campo_Nombre_Director].ToString().Trim().ToUpper();
                            Puesto_Coordinador = Dt.Rows[0][Ope_Psp_Movimiento_Egr_Det.Campo_Puesto_Director].ToString().Trim().ToUpper();
                            UR = Dt.Rows[0]["UR"].ToString().Trim().ToUpper();
                        }
                    }

                    Dt_Encabezado.Columns.Add("Nombre_UR");
                    Dt_Encabezado.Columns.Add("No_Modificacion");
                    Dt_Encabezado.Columns.Add("No_Solicitud");
                    Dt_Encabezado.Columns.Add("Anio");
                    Dt_Encabezado.Columns.Add("Fecha");
                    Dt_Encabezado.Columns.Add("Solicitante");
                    Dt_Encabezado.Columns.Add("Texto");
                    Dt_Encabezado.Columns.Add("Puesto_Solicitante");
                    Dt_Encabezado.Columns.Add("Dirigido");
                    Dt_Encabezado.Columns.Add("Puesto_Dirigido");

                    Texto = "Por medio del presente reciba un cordial saludo, a la vez que solicito su valioso apoyo";
                    Texto += " para que sean consideradas las modificaciones al presupuesto de la " + UR;
                    Texto += " de acuerdo al archivo anexo.";

                    Fila = Dt_Encabezado.NewRow();
                    Fila["Nombre_UR"] = UR.Trim();
                    Fila["No_Modificacion"] = No_Mov.Trim() + "a. Modificación - No. Solicitud " + No_Solicitud.Trim()
                            + " - " + String.Format("{0:yyyy}", DateTime.Now);
                    Fila["No_Solicitud"] = No_Solicitud.Trim();
                    Fila["Anio"] = String.Format("{0:yyyy}", DateTime.Now);
                    Fila["Fecha"] = Crear_Fecha(String.Format("{0:MM/dd/yyyy}", DateTime.Now));
                    Fila["Solicitante"] = Solicito.Trim();
                    Fila["Texto"] = Texto;
                    Fila["Puesto_Solicitante"] = Puesto_Solicito.Trim();
                    Fila["Dirigido"] = Coordinador.Trim();
                    Fila["Puesto_Dirigido"] = Puesto_Coordinador.Trim();
                    Dt_Encabezado.Rows.Add(Fila);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al generar el reporte. Error[" + ex.Message + "]");
                }
                return Dt_Encabezado;
            }

            ///*****************************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Mov_Origen
            ///DESCRIPCIÓN          : METODO PARA CREAR EL REPORTE DE MOVIMIENTOS
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 01/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*****************************************************************************************************************
            public DataTable Crear_Dt_Movimientos(String Tipo_Partida, String Tipo_Dt, String No_Solicitud)
            {
                Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
                DataTable Dt_Mov = new DataTable();
                DataRow Fila;
                DataTable Dt = new DataTable();
                Negocio.P_No_Solicitud = No_Solicitud;
                Dt = Negocio.Consultar_Datos_Movimientos_Det();
                String Codigo_Programatico = String.Empty;

                try
                {

                    if (Tipo_Dt.Trim().Equals("Det"))
                    {
                        Dt_Mov.Columns.Add("Anio", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("FF", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("AF", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("PP", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("UR", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("P", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("Imp_Ene", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Feb", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Mar", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Abr", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_May", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Jun", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Jul", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Ago", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Sep", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Oct", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Nov", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Dic", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Tot", System.Type.GetType("System.Double"));

                        foreach (DataRow Dr in Dt.Rows)
                        {
                            if (Dr["TIPO_PARTIDA"].ToString().Trim().Equals(Tipo_Partida.Trim()))
                            {
                                Codigo_Programatico = Dr["FF"].ToString().Trim() + "-" + Dr["AF"].ToString().Trim() + "-" + Dr["PP"].ToString().Trim() +
                                     "-" + Dr["UR"].ToString().Trim().Substring(0, Dr["UR"].ToString().Trim().IndexOf(" ")) +
                                     "-" + Dr["PARTIDA"].ToString().Trim().Substring(0, Dr["PARTIDA"].ToString().Trim().IndexOf(" "));

                                Fila = Dt_Mov.NewRow();
                                Fila["Anio"] = String.Format("{0:yyyy}", DateTime.Now);
                                Fila["FF"] = Codigo_Programatico.Trim();
                                Fila["AF"] = Dr["AF"].ToString().Trim();
                                Fila["PP"] = Dr["PP"].ToString().Trim();
                                Fila["UR"] = Dr["UR"].ToString().Trim().Substring(0, Dr["UR"].ToString().Trim().IndexOf(" "));
                                Fila["P"] = Dr["PARTIDA"].ToString().Trim().Substring(0, Dr["PARTIDA"].ToString().Trim().IndexOf(" "));
                                Fila["Imp_Ene"] = Dr["ENERO"];
                                Fila["Imp_Feb"] = Dr["FEBRERO"];
                                Fila["Imp_Mar"] = Dr["MARZO"];
                                Fila["Imp_Abr"] = Dr["ABRIL"];
                                Fila["Imp_May"] = Dr["MAYO"];
                                Fila["Imp_Jun"] = Dr["JUNIO"];
                                Fila["Imp_Jul"] = Dr["JULIO"];
                                Fila["Imp_Ago"] = Dr["AGOSTO"];
                                Fila["Imp_Sep"] = Dr["SEPTIEMBRE"];
                                Fila["Imp_Oct"] = Dr["OCTUBRE"];
                                Fila["Imp_Nov"] = Dr["NOVIEMBRE"];
                                Fila["Imp_Dic"] = Dr["DICIEMBRE"];
                                Fila["Imp_Tot"] = Dr["IMPORTE"];
                                Dt_Mov.Rows.Add(Fila);
                            }
                        }
                    }
                    else if (Tipo_Dt.Trim().Equals("Jus"))
                    {
                        Dt_Mov.Columns.Add("Anio", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("FF", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("AF", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("PP", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("UR", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("P", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("Partida", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("Justificacion", System.Type.GetType("System.String"));

                        foreach (DataRow Dr in Dt.Rows)
                        {
                            Codigo_Programatico = Dr["FF"].ToString().Trim() + "-" + Dr["AF"].ToString().Trim() + "-" + Dr["PP"].ToString().Trim() +
                                     "-" + Dr["UR"].ToString().Trim().Substring(0, Dr["UR"].ToString().Trim().IndexOf(" ")) +
                                     "-" + Dr["PARTIDA"].ToString().Trim().Substring(0, Dr["PARTIDA"].ToString().Trim().IndexOf(" "));

                            Fila = Dt_Mov.NewRow();
                            Fila["Anio"] = String.Format("{0:yyyy}", DateTime.Now);
                            Fila["FF"] = Codigo_Programatico.Trim();
                            Fila["AF"] = Dr["AF"].ToString().Trim();
                            Fila["PP"] = Dr["PP"].ToString().Trim();
                            Fila["UR"] = Dr["UR"].ToString().Trim().Substring(0, Dr["UR"].ToString().Trim().IndexOf(" "));
                            Fila["P"] = Dr["PARTIDA"].ToString().Trim().Substring(0, Dr["PARTIDA"].ToString().Trim().IndexOf(" "));
                            Fila["Partida"] = Dr["PARTIDA"].ToString().Trim().Substring(Dr["PARTIDA"].ToString().Trim().IndexOf(" "));
                            Fila["Justificacion"] = Dr["JUSTIFICACION"].ToString().Trim();
                            Dt_Mov.Rows.Add(Fila);
                        }
                    }
                    else
                    {
                        Dt_Mov.Columns.Add("Anio", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("FF", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("AF", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("PP", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("UR", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("P", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("Partida", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("Presupuesto_Aprobado", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Importe", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Presupuesto_Modificado", System.Type.GetType("System.Double"));

                        foreach (DataRow Dr in Dt.Rows)
                        {
                            if (Dr["TIPO_PARTIDA"].ToString().Trim().Equals(Tipo_Partida.Trim()))
                            {
                                Codigo_Programatico = Dr["FF"].ToString().Trim() + "-" + Dr["AF"].ToString().Trim() + "-" + Dr["PP"].ToString().Trim() +
                                     "-" + Dr["UR"].ToString().Trim().Substring(0, Dr["UR"].ToString().Trim().IndexOf(" ")) +
                                     "-" + Dr["PARTIDA"].ToString().Trim().Substring(0, Dr["PARTIDA"].ToString().Trim().IndexOf(" "));

                                Fila = Dt_Mov.NewRow();
                                Fila["Anio"] = String.Format("{0:yyyy}", DateTime.Now);
                                Fila["FF"] = Codigo_Programatico.Trim();
                                Fila["AF"] = Dr["AF"].ToString().Trim();
                                Fila["PP"] = Dr["PP"].ToString().Trim();
                                Fila["UR"] = Dr["UR"].ToString().Trim().Substring(0, Dr["UR"].ToString().Trim().IndexOf(" "));
                                Fila["P"] = Dr["PARTIDA"].ToString().Trim().Substring(0, Dr["PARTIDA"].ToString().Trim().IndexOf(" "));
                                Fila["Partida"] = Dr["PARTIDA"].ToString().Trim().Substring(Dr["PARTIDA"].ToString().Trim().IndexOf(" "));
                                Fila["Presupuesto_Aprobado"] = Dr["APROBADO"];
                                Fila["Importe"] = Dr["IMPORTE"];
                                Fila["Presupuesto_Modificado"] = Dr["MODIFICADO"];
                                Dt_Mov.Rows.Add(Fila);
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al generar el reporte. Error[" + ex.Message + "]");
                }
                return Dt_Mov;
            }

            //****************************************************************************************************
            //NOMBRE DE LA FUNCIÓN : Crear_Fecha
            //DESCRIPCIÓN          : Metodo para obtener la fecha en letras
            //PARAMETROS           1 Fecha: fecha a la cual le daremos formato
            //CREO                 : Leslie González Vázquez
            //FECHA_CREO           : 13/Diciembre/2011 
            //MODIFICO             :
            //FECHA_MODIFICO       :
            //CAUSA_MODIFICACIÓN   :
            //****************************************************************************************************
            internal static String Crear_Fecha(String Fecha)
            {
                String Fecha_Formateada = String.Empty;
                String Mes = String.Empty;
                String[] Fechas;

                try
                {
                    Fechas = Fecha.Split('/');
                    Mes = Fechas[0].ToString();
                    switch (Mes)
                    {
                        case "01":
                            Mes = "Enero";
                            break;
                        case "02":
                            Mes = "Febrero";
                            break;
                        case "03":
                            Mes = "Marzo";
                            break;
                        case "04":
                            Mes = "Abril";
                            break;
                        case "05":
                            Mes = "Mayo";
                            break;
                        case "06":
                            Mes = "Junio";
                            break;
                        case "07":
                            Mes = "Julio";
                            break;
                        case "08":
                            Mes = "Agosto";
                            break;
                        case "09":
                            Mes = "Septiembre";
                            break;
                        case "10":
                            Mes = "Octubre";
                            break;
                        case "11":
                            Mes = "Noviembre";
                            break;
                        default:
                            Mes = "Diciembre";
                            break;
                    }
                    Fecha_Formateada = "Irapuato, Guanajuato a " + Fechas[1].ToString() + " de " + Mes + " de " + Fechas[2].ToString();
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al crear el formato de fecha. Error: [" + Ex.Message + "]");
                }
                return Fecha_Formateada;
            }

            ///*****************************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Encabezado
            ///DESCRIPCIÓN          : METODO PARA CREAR EL REPORTE DE MOVIMIENTOS
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 01/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*****************************************************************************************************************
            public DataTable Crear_Dt_Encabezado_Adecuacion(String No_Mov, String No_Solicitud)
            {
                Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
                DataTable Dt_Encabezado = new DataTable();
                DataRow Fila;
                String Texto = String.Empty;
                String UR = String.Empty;
                String Coordinador = String.Empty;
                String Puesto_Coordinador = String.Empty;
                String Puesto_Solicito = String.Empty;
                String Solicito = String.Empty;
                DataTable Dt = new DataTable();

                try
                {
                    Negocio.P_No_Solicitud = No_Solicitud;
                    Dt = Negocio.Consultar_Datos_Movimientos_Det();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            Solicito = Dt.Rows[0][Ope_Psp_Movimiento_Egr_Det.Campo_Nombre_Solicitante].ToString().Trim().ToUpper();
                            Puesto_Solicito = Dt.Rows[0][Ope_Psp_Movimiento_Egr_Det.Campo_Puesto_Solicitante].ToString().Trim().ToUpper();
                            Coordinador = Dt.Rows[0][Ope_Psp_Movimiento_Egr_Det.Campo_Nombre_Director].ToString().Trim().ToUpper();
                            Puesto_Coordinador = Dt.Rows[0][Ope_Psp_Movimiento_Egr_Det.Campo_Puesto_Director].ToString().Trim().ToUpper();
                            UR = Dt.Rows[0]["UR"].ToString().Trim().ToUpper();
                        }
                    }

                    Dt_Encabezado.Columns.Add("Nombre_UR");
                    Dt_Encabezado.Columns.Add("No_Modificacion");
                    Dt_Encabezado.Columns.Add("No_Solicitud");
                    Dt_Encabezado.Columns.Add("Anio");
                    Dt_Encabezado.Columns.Add("Fecha");
                    Dt_Encabezado.Columns.Add("Solicitante");
                    Dt_Encabezado.Columns.Add("Texto");
                    Dt_Encabezado.Columns.Add("Puesto_Solicitante");
                    Dt_Encabezado.Columns.Add("Dirigido");
                    Dt_Encabezado.Columns.Add("Puesto_Dirigido");

                    Texto = "Por medio del presente reciba un cordial saludo, a la vez que solicito su valioso apoyo";
                    Texto += " para que sean consideradas las adecuaciones al presupuesto de la " + UR;
                    Texto += " de acuerdo al archivo anexo.";

                    Fila = Dt_Encabezado.NewRow();
                    Fila["Nombre_UR"] = UR.Trim();
                    Fila["No_Modificacion"] = "No. Solicitud " + No_Solicitud.Trim() + " - " + String.Format("{0:yyyy}", DateTime.Now);
                    Fila["No_Solicitud"] = No_Solicitud.Trim();
                    Fila["Anio"] = String.Format("{0:yyyy}", DateTime.Now);
                    Fila["Fecha"] = Crear_Fecha(String.Format("{0:MM/dd/yyyy}", DateTime.Now));
                    Fila["Solicitante"] = Solicito.Trim();
                    Fila["Texto"] = Texto;
                    Fila["Puesto_Solicitante"] = Puesto_Solicito.Trim();
                    Fila["Dirigido"] = Coordinador.Trim();
                    Fila["Puesto_Dirigido"] = Puesto_Coordinador.Trim();
                    Dt_Encabezado.Rows.Add(Fila);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al generar el reporte. Error[" + ex.Message + "]");
                }
                return Dt_Encabezado;
            }

            ///*****************************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Mov_Origen
            ///DESCRIPCIÓN          : METODO PARA CREAR EL REPORTE DE MOVIMIENTOS
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 01/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*****************************************************************************************************************
            public DataTable Crear_Dt_Movimientos_Adecuacion(String Tipo_Partida, String Tipo_Dt, String No_Solicitud)
            {
                Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
                DataTable Dt_Mov = new DataTable();
                DataTable Dt_Agrupado = new DataTable();
                DataRow Fila;
                DataTable Dt = new DataTable();
                Negocio.P_No_Solicitud = No_Solicitud;
                Dt = Negocio.Consultar_Datos_Movimientos_Det();
                String Codigo_Programatico = String.Empty;

                try
                {
                    if (Tipo_Dt.Trim().Equals("Det"))
                    {
                        Dt_Mov.Columns.Add("Anio", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("FF", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("AF", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("PP", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("UR", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("P", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("Imp_Ene", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Feb", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Mar", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Abr", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_May", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Jun", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Jul", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Ago", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Sep", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Oct", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Nov", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Dic", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Imp_Tot", System.Type.GetType("System.Double"));

                        foreach (DataRow Dr in Dt.Rows)
                        {
                            if (Dr["TIPO_PARTIDA"].ToString().Trim().Equals(Tipo_Partida.Trim()))
                            {
                                Codigo_Programatico = Dr["FF"].ToString().Trim() + "-" + Dr["AF"].ToString().Trim() + "-" + Dr["PP"].ToString().Trim() +
                                     "-" + Dr["UR"].ToString().Trim().Substring(0, Dr["UR"].ToString().Trim().IndexOf(" ")) +
                                     "-" + Dr["PARTIDA"].ToString().Trim().Substring(0, Dr["PARTIDA"].ToString().Trim().IndexOf(" "));

                                Fila = Dt_Mov.NewRow();
                                Fila["Anio"] = String.Format("{0:yyyy}", DateTime.Now);
                                Fila["FF"] = Codigo_Programatico.Trim();
                                Fila["AF"] = Dr["AF"].ToString().Trim();
                                Fila["PP"] = Dr["PP"].ToString().Trim();
                                Fila["UR"] = Dr["UR"].ToString().Trim().Substring(0, Dr["UR"].ToString().Trim().IndexOf(" "));
                                Fila["P"] = Dr["PARTIDA"].ToString().Trim().Substring(0, Dr["PARTIDA"].ToString().Trim().IndexOf(" "));
                                Fila["Imp_Ene"] = Dr["ENERO"];
                                Fila["Imp_Feb"] = Dr["FEBRERO"];
                                Fila["Imp_Mar"] = Dr["MARZO"];
                                Fila["Imp_Abr"] = Dr["ABRIL"];
                                Fila["Imp_May"] = Dr["MAYO"];
                                Fila["Imp_Jun"] = Dr["JUNIO"];
                                Fila["Imp_Jul"] = Dr["JULIO"];
                                Fila["Imp_Ago"] = Dr["AGOSTO"];
                                Fila["Imp_Sep"] = Dr["SEPTIEMBRE"];
                                Fila["Imp_Oct"] = Dr["OCTUBRE"];
                                Fila["Imp_Nov"] = Dr["NOVIEMBRE"];
                                Fila["Imp_Dic"] = Dr["DICIEMBRE"];
                                Fila["Imp_Tot"] = Dr["IMPORTE"];
                                Dt_Mov.Rows.Add(Fila);
                            }
                        }
                    }
                    else if (Tipo_Dt.Trim().Equals("Jus"))
                    {
                        Dt_Mov.Columns.Add("Anio", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("FF", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("AF", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("PP", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("UR", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("P", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("Partida", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("Justificacion", System.Type.GetType("System.String"));

                        foreach (DataRow Dr in Dt.Rows)
                        {
                            Codigo_Programatico = Dr["FF"].ToString().Trim() + "-" + Dr["AF"].ToString().Trim() + "-" + Dr["PP"].ToString().Trim() +
                                     "-" + Dr["UR"].ToString().Trim().Substring(0, Dr["UR"].ToString().Trim().IndexOf(" ")) +
                                     "-" + Dr["PARTIDA"].ToString().Trim().Substring(0, Dr["PARTIDA"].ToString().Trim().IndexOf(" "));

                            Fila = Dt_Mov.NewRow();
                            Fila["Anio"] = String.Format("{0:yyyy}", DateTime.Now);
                            Fila["FF"] = Codigo_Programatico.Trim();
                            Fila["AF"] = Dr["AF"].ToString().Trim();
                            Fila["PP"] = Dr["PP"].ToString().Trim();
                            Fila["UR"] = Dr["UR"].ToString().Trim().Substring(0, Dr["UR"].ToString().Trim().IndexOf(" "));
                            Fila["P"] = Dr["PARTIDA"].ToString().Trim().Substring(0, Dr["PARTIDA"].ToString().Trim().IndexOf(" "));
                            Fila["Partida"] = Dr["PARTIDA"].ToString().Trim().Substring(Dr["PARTIDA"].ToString().Trim().IndexOf(" "));
                            Fila["Justificacion"] = Dr["JUSTIFICACION"].ToString().Trim();
                            Dt_Mov.Rows.Add(Fila);
                        }

                        //FILTRAMOS QUE SEA UNA JUSTIFICACION POR CODIGO PROGRAMATICO
                        var Agrupacion = (from Fila_Just in Dt_Mov.AsEnumerable()
                                          group Fila_Just by new
                                          {
                                              Anio = Fila_Just.Field<String>("Anio"),
                                              FF = Fila_Just.Field<String>("FF"),
                                              AF = Fila_Just.Field<String>("AF"),
                                              PP = Fila_Just.Field<String>("PP"),
                                              UR = Fila_Just.Field<String>("UR"),
                                              PARTIDA = Fila_Just.Field<String>("Partida"),
                                              P = Fila_Just.Field<String>("P"),
                                              Just = Fila_Just.Field<String>("Justificacion")
                                          }
                                              into mov
                                              orderby mov.Key.FF
                                              select Dt_Mov.LoadDataRow(
                                                new object[]
                                        {
                                            mov.Key.Anio,
                                            mov.Key.FF,
                                            mov.Key.AF,
                                            mov.Key.PP,
                                            mov.Key.UR,
                                            mov.Key.PARTIDA,
                                            mov.Key.P,
                                            mov.Key.Just,
                                            
                                        }, LoadOption.PreserveChanges));

                        //obtenemos los datos agrupados en una tabla
                        Dt_Agrupado = Agrupacion.CopyToDataTable();
                        Dt_Mov = Dt_Agrupado;
                    }
                    else
                    {
                        Dt_Mov.Columns.Add("Anio", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("FF", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("AF", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("PP", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("UR", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("P", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("Partida", System.Type.GetType("System.String"));
                        Dt_Mov.Columns.Add("Presupuesto_Aprobado", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Importe", System.Type.GetType("System.Double"));
                        Dt_Mov.Columns.Add("Presupuesto_Modificado", System.Type.GetType("System.Double"));

                        foreach (DataRow Dr in Dt.Rows)
                        {
                            if (Dr["TIPO_PARTIDA"].ToString().Trim().Equals(Tipo_Partida.Trim()))
                            {
                                Codigo_Programatico = Dr["FF"].ToString().Trim() + "-" + Dr["AF"].ToString().Trim() + "-" + Dr["PP"].ToString().Trim() +
                                     "-" + Dr["UR"].ToString().Trim().Substring(0, Dr["UR"].ToString().Trim().IndexOf(" ")) +
                                     "-" + Dr["PARTIDA"].ToString().Trim().Substring(0, Dr["PARTIDA"].ToString().Trim().IndexOf(" "));

                                Fila = Dt_Mov.NewRow();
                                Fila["Anio"] = String.Format("{0:yyyy}", DateTime.Now);
                                Fila["FF"] = Codigo_Programatico.Trim();
                                Fila["AF"] = Dr["AF"].ToString().Trim();
                                Fila["PP"] = Dr["PP"].ToString().Trim();
                                Fila["UR"] = Dr["UR"].ToString().Trim().Substring(0, Dr["UR"].ToString().Trim().IndexOf(" "));
                                Fila["P"] = Dr["PARTIDA"].ToString().Trim().Substring(0, Dr["PARTIDA"].ToString().Trim().IndexOf(" "));
                                Fila["Partida"] = Dr["PARTIDA"].ToString().Trim().Substring(Dr["PARTIDA"].ToString().Trim().IndexOf(" "));
                                Fila["Presupuesto_Aprobado"] = Dr["APROBADO"];
                                Fila["Importe"] = Dr["IMPORTE"];
                                Fila["Presupuesto_Modificado"] = Dr["MODIFICADO"];
                                Dt_Mov.Rows.Add(Fila);
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al generar el reporte. Error[" + ex.Message + "]");
                }
                return Dt_Mov;
            }

            ///*****************************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Crear_Solicitud_Movimientos_Ingresos
            ///DESCRIPCIÓN          : Metodo para crear el reporte de movimientos de ingresos
            ///PARAMETROS           :  
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Noviembre/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*****************************************************************************************************************
            public String Crear_Solicitud_Movimientos_Ingresos(String Anio, String No_Modificacion, String Solicitud_ID)
            {
                Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio(); //conexion con la capa de negocios
                DataSet Ds_Registros = null;
                String Fecha = String.Empty;
                DataTable Dt_Movimientos = new DataTable();
                DataTable Dt_Mov = new DataTable();
                String Nom_Archivo = String.Empty;
                String No_Solicitud = String.Empty;

                try
                {
                    Negocio.P_Anio = Anio.Trim();
                    Negocio.P_No_Movimiento_Ing = No_Modificacion.Trim();
                    Negocio.P_Estatus = String.Empty;
                    Dt_Movimientos = Negocio.Consultar_Movimientos();

                    if (Dt_Movimientos != null)
                    {
                        if (Dt_Movimientos.Rows.Count > 0)
                        {
                            //obtenemos los movimientos de la solicitud
                            Dt_Mov = (from Fila in Dt_Movimientos.AsEnumerable()
                                      where Fila.Field<decimal>(Ope_Psp_Movimiento_Ing_Det.Campo_Movimiento_Ing_ID) == Convert.ToDecimal(Solicitud_ID.Trim())
                                             select Fila).AsDataView().ToTable();


                            //agregamos las filas de los datos complementarios para el reporte
                            Dt_Mov.Columns.Add("NO_SOLICITUD", System.Type.GetType("System.String"));
                            Dt_Mov.Columns.Add("FECHA", System.Type.GetType("System.String"));
                            Dt_Mov.Columns.Add("SOLICITANTE", System.Type.GetType("System.String"));
                            Dt_Mov.Columns.Add("PUESTO_SOLICITANTE", System.Type.GetType("System.String"));

                            Fecha = Crear_Fecha(String.Format("{0:MM/dd/yyyy}", DateTime.Now));

                            foreach (DataRow Dr in Dt_Mov.Rows)
                            {
                                No_Solicitud = String.Empty;
                                No_Solicitud = "MODIFICACIÓN AL PRESUPUESTO DE INGRESOS    T";
                                No_Solicitud += String.Format("{0:00}", Convert.ToInt32(Dr[Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing].ToString().Trim()));
                                No_Solicitud += " - " + Dr[Ope_Psp_Movimiento_Ing_Det.Campo_Anio].ToString().Trim();

                                Dr["NO_SOLICITUD"] = No_Solicitud;
                                Dr["FECHA"] = Fecha.Trim();
                                Dr["SOLICITANTE"] = "";
                                Dr["PUESTO_SOLICITANTE"] = "";

                                if (Dr[Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Movimiento].ToString().Trim().Equals("REDUCCION"))
                                {
                                    Dr["IMP_TOTAL"] = "-" + Dr["IMP_TOTAL"];
                                }
                            }

                            Ds_Registros = new DataSet();
                            Dt_Mov.TableName = "Dt_Movimientos_Ing";
                            Ds_Registros.Tables.Add(Dt_Mov.Copy());

                            Exportar_Reporte(Ds_Registros, "Cr_Ope_Psp_Movimiento_Presupuestal_Ingresos.rpt", "Rpt_Movimientos_" + Session.SessionID,
                            "pdf", ExportFormatType.PortableDocFormat, "Presupuestos");
                            Nom_Archivo = "../../Reporte/Rpt_Movimientos_" + Session.SessionID + ".pdf     ";
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al generar el reporte. Error[" + ex.Message + "]");
                }
                return Nom_Archivo;
            }
        #endregion

        #region (Reportes Psp Egresos Mensual)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Reporte_Mensual
            ///DESCRIPCIÓN          : Metodo para obtener los datos para los reportes mensual
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 12/Diciembre/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Reporte_Mensual_Egresos(String Anio, String FF, String PP, String PP_F, String UR, String UR_F,
                String AF, String Cap, String Cap_F, String Con, String Con_F, String PG, String PG_F, String P, String P_F)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json_Datos = "";
                DataTable Dt_Egr = new DataTable();
                DataTable Dt_Egr_Completo = new DataTable();
                String Json_Tot = String.Empty;

                try
                {
                    //parametros egresos
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF;
                    Negocio.P_Area_Funcional_ID = AF;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_ProgramaF_ID = PP_F;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_DependenciaF_ID = UR_F;
                    Negocio.P_Capitulo_ID = Cap;
                    Negocio.P_CapituloF_ID = Cap_F;
                    Negocio.P_ConceptoF_ID = Con;
                    Negocio.P_Concepto_ID = Con_F;
                    Negocio.P_Partida_Generica_ID = PG;
                    Negocio.P_Partida_GenericaF_ID = PG_F;
                    Negocio.P_PartidaF_ID = P;
                    Negocio.P_Partida_ID = P_F;

                    //obtenemos los datos del tipo de usuario
                    String Tipo_Usuario = Obtener_Tipo_Usuario();
                    String[] Usuario = Tipo_Usuario.Split('/');
                    Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                    Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                    if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        if (String.IsNullOrEmpty(UR))
                        {
                            Negocio.P_Dependencia_ID = Obtener_Dependencias();
                        }
                        Negocio.P_DependenciaF_ID = String.Empty;
                    }
                    else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                    {
                        Negocio.P_Fte_Financiamiento = FF_Nomina;
                    }

                    Dt_Egr = Negocio.Consultar_Psp_Egr();

                    if (Dt_Egr != null && Dt_Egr.Rows.Count > 0)
                    {
                        Dt_Egr_Completo = Generar_Dt_Psp_Egr_Mensual(Dt_Egr);
                    }

                    Json_Datos = Obtener_Param_Niveles_Mensual_Egresos(Dt_Egr_Completo, Anio);
                }
                catch (Exception ex)
                {
                    Json_Datos = "Error al Obtener_Reporte_Mensual_Egresos Error[" + ex.Message + "]";
                    throw new Exception("Error al Obtener_Reporte_Mensual_Egresos Error[" + ex.Message + "]");
                }
                return Json_Datos;
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Param_Niveles_Mensual
            ///DESCRIPCIÓN          : Metodo para obtener los parametros de los niveles del reporte
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 12/Diciembre/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Param_Niveles_Mensual_Egresos(DataTable Dt_Egr, String Anio)
            {
                String Nom_Archivo = String.Empty;
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                //Parametros de los combos
                String Niv_FF = String.Empty;
                String Niv_PP = String.Empty;
                String Niv_AF = String.Empty;
                String Niv_UR = String.Empty;
                String Niv_Cap = String.Empty;
                String Niv_Con = String.Empty;
                String Niv_Par_Gen = String.Empty;
                String Niv_Par = String.Empty;
                DataTable Dt_Niv_Psp_Egr = new DataTable();
                DataRow Fila;
                Boolean Insertar = false;
                DataTable Dt_UR = new DataTable();
                String Dependencia = String.Empty;
                String Gpo_Dep = String.Empty;

                try
                {
                    #region (Parametros)
                    //obtenemos los parametros e los combos
                    if (this.Request.QueryString["Niv_FF"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_FF"].ToString().Trim()))
                    {
                        Niv_FF = this.Request.QueryString["Niv_FF"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_PP"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_PP"].ToString().Trim()))
                    {
                        Niv_PP = this.Request.QueryString["Niv_PP"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_AF"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_AF"].ToString().Trim()))
                    {
                        Niv_AF = this.Request.QueryString["Niv_AF"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_UR"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_UR"].ToString().Trim()))
                    {
                        Niv_UR = this.Request.QueryString["Niv_UR"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_Cap"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_Cap"].ToString().Trim()))
                    {
                        Niv_Cap = this.Request.QueryString["Niv_Cap"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_Con"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_Con"].ToString().Trim()))
                    {
                        Niv_Con = this.Request.QueryString["Niv_Con"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_Par_Gen"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_Par_Gen"].ToString().Trim()))
                    {
                        Niv_Par_Gen = this.Request.QueryString["Niv_Par_Gen"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_Par"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_Par"].ToString().Trim()))
                    {
                        Niv_Par = this.Request.QueryString["Niv_Par"].ToString().Trim();
                    }
                    #endregion

                    //obtenemos la dependencia del usuario y el grupo dependencia
                    Negocio.P_Busqueda = String.Empty;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = String.Empty;
                    Negocio.P_Area_Funcional_ID = String.Empty;
                    Negocio.P_Programa_ID = String.Empty;
                    Negocio.P_Tipo_Usuario = "Usuario";
                    Negocio.P_Dependencia_ID = "'" + Cls_Sessiones.Dependencia_ID_Empleado.Trim() + "'";

                    Dt_UR = Negocio.Consultar_UR();

                    if (Dt_UR != null && Dt_UR.Rows.Count > 0)
                    {
                        Dependencia = Dt_UR.Rows[0]["NOM_DEP"].ToString().Trim();
                        Gpo_Dep = Dt_UR.Rows[0]["NOM_GPO_DEP"].ToString().Trim();
                    }

                    if (Dt_Egr != null && Dt_Egr.Rows.Count > 0)
                    {
                        Dt_Niv_Psp_Egr.Columns.Add("Concepto", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Aprobado", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Ampliacion", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Reduccion", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Modificado", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Disponible", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pre_Comprometido", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Comprometido", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Devengado", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Ejercido", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pagado", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Disponible_Ene", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Disponible_Feb", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Disponible_Mar", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Disponible_Abr", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Disponible_May", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Disponible_Jun", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Disponible_Jul", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Disponible_Ago", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Disponible_Sep", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Disponible_Oct", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Disponible_Nov", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Disponible_Dic", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pre_Comprometido_Ene", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pre_Comprometido_Feb", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pre_Comprometido_Mar", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pre_Comprometido_Abr", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pre_Comprometido_May", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pre_Comprometido_Jun", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pre_Comprometido_Jul", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pre_Comprometido_Ago", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pre_Comprometido_Sep", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pre_Comprometido_Oct", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pre_Comprometido_Nov", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pre_Comprometido_Dic", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Comprometido_Ene", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Comprometido_Feb", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Comprometido_Mar", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Comprometido_Abr", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Comprometido_May", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Comprometido_Jun", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Comprometido_Jul", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Comprometido_Ago", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Comprometido_Sep", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Comprometido_Oct", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Comprometido_Nov", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Comprometido_Dic", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Devengado_Ene", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Devengado_Feb", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Devengado_Mar", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Devengado_Abr", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Devengado_May", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Devengado_Jun", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Devengado_Jul", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Devengado_Ago", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Devengado_Sep", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Devengado_Oct", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Devengado_Nov", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Devengado_Dic", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Ejercido_Ene", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Ejercido_Feb", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Ejercido_Mar", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Ejercido_Abr", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Ejercido_May", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Ejercido_Jun", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Ejercido_Jul", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Ejercido_Ago", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Ejercido_Sep", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Ejercido_Oct", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Ejercido_Nov", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Ejercido_Dic", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pagado_Ene", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pagado_Feb", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pagado_Mar", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pagado_Abr", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pagado_May", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pagado_Jun", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pagado_Jul", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pagado_Ago", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pagado_Sep", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pagado_Oct", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pagado_Nov", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Pagado_Dic", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Tipo", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Elaboro", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Dependencia", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Anio", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Egr.Columns.Add("Gpo_Dependencia", System.Type.GetType("System.String"));

                        foreach (DataRow Dr in Dt_Egr.Rows)
                        {
                            Insertar = false;

                            if (Dr["Tipo"].ToString().Trim().Equals("Tot"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("FF") && Niv_FF.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("AF") && Niv_AF.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("PP") && Niv_PP.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("UR") && Niv_UR.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("Cap") && Niv_Cap.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("Con") && Niv_Con.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("PG") && Niv_Par_Gen.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("P") && Niv_Par.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else
                            {
                                Insertar = false;
                            }

                            if (Insertar)
                            {
                                Fila = Dt_Niv_Psp_Egr.NewRow();
                                Fila["Concepto"] = Dr["Concepto"].ToString().Trim();
                                Fila["Aprobado"] = Dr["Aprobado"].ToString().Trim();
                                Fila["Ampliacion"] = Dr["Ampliacion"].ToString().Trim();
                                Fila["Reduccion"] = Dr["Reduccion"].ToString().Trim();
                                Fila["Modificado"] = Dr["Modificado"].ToString().Trim();
                                Fila["Disponible"] = Dr["Por_Recaudar"].ToString().Trim();
                                Fila["Pre_Comprometido"] = Dr["Pre_Comprometido"].ToString().Trim();
                                Fila["Comprometido"] = Dr["Comprometido"].ToString().Trim();
                                Fila["Devengado"] = Dr["Devengado"].ToString().Trim();
                                Fila["Ejercido"] = Dr["Ejercido"].ToString().Trim();
                                Fila["Pagado"] = Dr["Recaudado"].ToString().Trim();
                                Fila["Disponible_Ene"] = Dr["Por_Recaudar_Ene"].ToString().Trim();
                                Fila["Disponible_Feb"] = Dr["Por_Recaudar_Feb"].ToString().Trim();
                                Fila["Disponible_Mar"] = Dr["Por_Recaudar_Mar"].ToString().Trim();
                                Fila["Disponible_Abr"] = Dr["Por_Recaudar_Abr"].ToString().Trim();
                                Fila["Disponible_May"] = Dr["Por_Recaudar_May"].ToString().Trim();
                                Fila["Disponible_Jun"] = Dr["Por_Recaudar_Jun"].ToString().Trim();
                                Fila["Disponible_Jul"] = Dr["Por_Recaudar_Jul"].ToString().Trim();
                                Fila["Disponible_Ago"] = Dr["Por_Recaudar_Ago"].ToString().Trim();
                                Fila["Disponible_Sep"] = Dr["Por_Recaudar_Sep"].ToString().Trim();
                                Fila["Disponible_Oct"] = Dr["Por_Recaudar_Oct"].ToString().Trim();
                                Fila["Disponible_Nov"] = Dr["Por_Recaudar_Nov"].ToString().Trim();
                                Fila["Disponible_Dic"] = Dr["Por_Recaudar_Dic"].ToString().Trim();
                                Fila["Pre_Comprometido_Ene"] = Dr["Pre_Comprometido_Ene"].ToString().Trim();
                                Fila["Pre_Comprometido_Feb"] = Dr["Pre_Comprometido_Feb"].ToString().Trim();
                                Fila["Pre_Comprometido_Mar"] = Dr["Pre_Comprometido_Mar"].ToString().Trim();
                                Fila["Pre_Comprometido_Abr"] = Dr["Pre_Comprometido_Abr"].ToString().Trim();
                                Fila["Pre_Comprometido_May"] = Dr["Pre_Comprometido_May"].ToString().Trim();
                                Fila["Pre_Comprometido_Jun"] = Dr["Pre_Comprometido_Jun"].ToString().Trim();
                                Fila["Pre_Comprometido_Jul"] = Dr["Pre_Comprometido_Jul"].ToString().Trim();
                                Fila["Pre_Comprometido_Ago"] = Dr["Pre_Comprometido_Ago"].ToString().Trim();
                                Fila["Pre_Comprometido_Sep"] = Dr["Pre_Comprometido_Sep"].ToString().Trim();
                                Fila["Pre_Comprometido_Oct"] = Dr["Pre_Comprometido_Oct"].ToString().Trim();
                                Fila["Pre_Comprometido_Nov"] = Dr["Pre_Comprometido_Nov"].ToString().Trim();
                                Fila["Pre_Comprometido_Dic"] = Dr["Pre_Comprometido_Dic"].ToString().Trim();
                                Fila["Comprometido_Ene"] = Dr["Comprometido_Ene"].ToString().Trim();
                                Fila["Comprometido_Feb"] = Dr["Comprometido_Feb"].ToString().Trim();
                                Fila["Comprometido_Mar"] = Dr["Comprometido_Mar"].ToString().Trim();
                                Fila["Comprometido_Abr"] = Dr["Comprometido_Abr"].ToString().Trim();
                                Fila["Comprometido_May"] = Dr["Comprometido_May"].ToString().Trim();
                                Fila["Comprometido_Jun"] = Dr["Comprometido_Jun"].ToString().Trim();
                                Fila["Comprometido_Jul"] = Dr["Comprometido_Jul"].ToString().Trim();
                                Fila["Comprometido_Ago"] = Dr["Comprometido_Ago"].ToString().Trim();
                                Fila["Comprometido_Sep"] = Dr["Comprometido_Sep"].ToString().Trim();
                                Fila["Comprometido_Oct"] = Dr["Comprometido_Oct"].ToString().Trim();
                                Fila["Comprometido_Nov"] = Dr["Comprometido_Nov"].ToString().Trim();
                                Fila["Comprometido_Dic"] = Dr["Comprometido_Dic"].ToString().Trim();
                                Fila["Devengado_Ene"] = Dr["Devengado_Ene"].ToString().Trim();
                                Fila["Devengado_Feb"] = Dr["Devengado_Feb"].ToString().Trim();
                                Fila["Devengado_Mar"] = Dr["Devengado_Mar"].ToString().Trim();
                                Fila["Devengado_Abr"] = Dr["Devengado_Abr"].ToString().Trim();
                                Fila["Devengado_May"] = Dr["Devengado_May"].ToString().Trim();
                                Fila["Devengado_Jun"] = Dr["Devengado_Jun"].ToString().Trim();
                                Fila["Devengado_Jul"] = Dr["Devengado_Jul"].ToString().Trim();
                                Fila["Devengado_Ago"] = Dr["Devengado_Ago"].ToString().Trim();
                                Fila["Devengado_Sep"] = Dr["Devengado_Sep"].ToString().Trim();
                                Fila["Devengado_Oct"] = Dr["Devengado_Oct"].ToString().Trim();
                                Fila["Devengado_Nov"] = Dr["Devengado_Nov"].ToString().Trim();
                                Fila["Devengado_Dic"] = Dr["Devengado_Dic"].ToString().Trim();
                                Fila["Ejercido_Ene"] = Dr["Ejercido_Ene"].ToString().Trim();
                                Fila["Ejercido_Feb"] = Dr["Ejercido_Feb"].ToString().Trim();
                                Fila["Ejercido_Mar"] = Dr["Ejercido_Mar"].ToString().Trim();
                                Fila["Ejercido_Abr"] = Dr["Ejercido_Abr"].ToString().Trim();
                                Fila["Ejercido_May"] = Dr["Ejercido_May"].ToString().Trim();
                                Fila["Ejercido_Jun"] = Dr["Ejercido_Jun"].ToString().Trim();
                                Fila["Ejercido_Jul"] = Dr["Ejercido_Jul"].ToString().Trim();
                                Fila["Ejercido_Ago"] = Dr["Ejercido_Ago"].ToString().Trim();
                                Fila["Ejercido_Sep"] = Dr["Ejercido_Sep"].ToString().Trim();
                                Fila["Ejercido_Oct"] = Dr["Ejercido_Oct"].ToString().Trim();
                                Fila["Ejercido_Nov"] = Dr["Ejercido_Nov"].ToString().Trim();
                                Fila["Ejercido_Dic"] = Dr["Ejercido_Dic"].ToString().Trim();
                                Fila["Pagado_Ene"] = Dr["Recaudado_Ene"].ToString().Trim();
                                Fila["Pagado_Feb"] = Dr["Recaudado_Feb"].ToString().Trim();
                                Fila["Pagado_Mar"] = Dr["Recaudado_Mar"].ToString().Trim();
                                Fila["Pagado_Abr"] = Dr["Recaudado_Abr"].ToString().Trim();
                                Fila["Pagado_May"] = Dr["Recaudado_May"].ToString().Trim();
                                Fila["Pagado_Jun"] = Dr["Recaudado_Jun"].ToString().Trim();
                                Fila["Pagado_Jul"] = Dr["Recaudado_Jul"].ToString().Trim();
                                Fila["Pagado_Ago"] = Dr["Recaudado_Ago"].ToString().Trim();
                                Fila["Pagado_Sep"] = Dr["Recaudado_Sep"].ToString().Trim();
                                Fila["Pagado_Oct"] = Dr["Recaudado_Oct"].ToString().Trim();
                                Fila["Pagado_Nov"] = Dr["Recaudado_Nov"].ToString().Trim();
                                Fila["Pagado_Dic"] = Dr["Recaudado_Dic"].ToString().Trim();
                                Fila["Tipo"] = Dr["Tipo"].ToString().Trim();
                                Fila["Elaboro"] = Cls_Sessiones.Nombre_Empleado.Trim();
                                Fila["Dependencia"] = Dependencia.Trim();
                                Fila["Anio"] = "PRESUPUESTO DE EGRESOS EJERCICIO " + Anio.Trim();
                                Fila["Gpo_Dependencia"] = Gpo_Dep.Trim();
                                Dt_Niv_Psp_Egr.Rows.Add(Fila);
                            }
                        }

                        if (Dt_Niv_Psp_Egr != null)
                        {
                            if (Dt_Niv_Psp_Egr.Rows.Count > 0)
                            { 
                               Nom_Archivo = Generar_Reporte_Psp_Egresos_Mensual(Dt_Niv_Psp_Egr, Anio.Trim());
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Nom_Archivo = "Error al obtener los parametros. Error[" + ex.Message + "]";
                    throw new Exception("Error al obtener los parametros. Error[" + ex.Message + "]");
                }
                return Nom_Archivo;
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Generar_Dt_Psp_Egr_Mensual
            ///DESCRIPCIÓN          : Metodo para generar la tabla del presupuesto de egresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 12/Diciembre/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private DataTable Generar_Dt_Psp_Egr_Mensual(DataTable Dt_Egr)
            {
                DataTable Dt_Psp = new DataTable();
                DataRow Fila;

                try
                {
                    
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al generar el presupuesto de egresos. Error[" + ex.Message + "]");
                }
                return Dt_Psp;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Generar_Reporte_Psp_Egresos_Mensula
            ///DESCRIPCIÓN          : metodo para generar el reporte en excel
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 13/Diciembre/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public String Generar_Reporte_Psp_Egresos_Mensual(DataTable Dt_Egr, String Anio)
            {
                Double Cantidad = 0.00;
                WorksheetCell Celda = new WorksheetCell();
                String Ruta_Archivo = "../../Archivos/Rpt_Presupuestos_Egreso_" + Anio + "_" + Session.SessionID + ".xls     ";
                String Nombre_Archivo = "Rpt_Presupuestos_Egreso_" + Anio + "_" + Session.SessionID + ".xls";

                try
                {
                    //Creamos el libro de Excel.
                    CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
                    //propiedades del libro
                    Libro.Properties.Title = "Reporte_Presupuesto";
                    Libro.Properties.Created = DateTime.Now;
                    Libro.Properties.Author = "JAPAMI";

                    #region Estilos
                    //Creamos el estilo cabecera para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
                    //Creamos el estilo cabecera 2 para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera2 = Libro.Styles.Add("HeaderStyle2");
                    //Creamos el estilo cabecera 3 para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera3 = Libro.Styles.Add("HeaderStyle3");
                    //Creamos el estilo contenido del concepto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Nombre_Partida = Libro.Styles.Add("Nombre_Partida");
                    //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_Partida = Libro.Styles.Add("Cant_Partida");
                    //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_Partida_Neg = Libro.Styles.Add("Cant_Partida_Neg");
                    //Creamos el estilo contenido del concepto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Nombre_Totales = Libro.Styles.Add("Nombre_Totales");
                    //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_Totales = Libro.Styles.Add("Cant_Totales");
                    //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_Totales_Neg = Libro.Styles.Add("Cant_Totales_Neg");
                    //Creamos el estilo contenido del concepto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Nombre_Total = Libro.Styles.Add("Nombre_Total");
                    //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_Total = Libro.Styles.Add("Cant_Total");
                    //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_Total_Neg = Libro.Styles.Add("Cant_Total_Neg");

                    //estilo para la cabecera
                    Estilo_Cabecera.Font.FontName = "Tahoma";
                    Estilo_Cabecera.Font.Size = 12;
                    Estilo_Cabecera.Font.Bold = true;
                    Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                    Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                    Estilo_Cabecera.Font.Color = "#FFFFFF";
                    Estilo_Cabecera.Interior.Color = "Gray";
                    Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para la cabecera2
                    Estilo_Cabecera2.Font.FontName = "Tahoma";
                    Estilo_Cabecera2.Font.Size = 10;
                    Estilo_Cabecera2.Font.Bold = true;
                    Estilo_Cabecera2.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                    Estilo_Cabecera2.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                    Estilo_Cabecera2.Font.Color = "#FFFFFF";
                    Estilo_Cabecera2.Interior.Color = "DarkGray";
                    Estilo_Cabecera2.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cabecera2.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para la cabecera3
                    Estilo_Cabecera3.Font.FontName = "Tahoma";
                    Estilo_Cabecera3.Font.Size = 12;
                    Estilo_Cabecera3.Font.Bold = true;
                    Estilo_Cabecera3.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                    Estilo_Cabecera3.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                    Estilo_Cabecera3.Font.Color = "#000000";
                    Estilo_Cabecera3.Interior.Color = "white";
                    Estilo_Cabecera3.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cabecera3.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera3.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera3.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera3.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el nombre de la partida
                    Estilo_Nombre_Partida.Font.FontName = "Tahoma";
                    Estilo_Nombre_Partida.Font.Size = 9;
                    Estilo_Nombre_Partida.Font.Bold = false;
                    Estilo_Nombre_Partida.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                    Estilo_Nombre_Partida.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                    Estilo_Nombre_Partida.Font.Color = "#000000";
                    Estilo_Nombre_Partida.Interior.Color = "White";
                    Estilo_Nombre_Partida.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Nombre_Partida.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Nombre_Partida.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Nombre_Partida.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Nombre_Partida.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el importe de la partida
                    Estilo_Cant_Partida.Font.FontName = "Tahoma";
                    Estilo_Cant_Partida.Font.Size = 9;
                    Estilo_Cant_Partida.Font.Bold = false;
                    Estilo_Cant_Partida.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                    Estilo_Cant_Partida.Alignment.Vertical = StyleVerticalAlignment.Center;
                    Estilo_Cant_Partida.Font.Color = "#000000";
                    Estilo_Cant_Partida.Interior.Color = "White";
                    Estilo_Cant_Partida.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cant_Partida.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Partida.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Partida.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Partida.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el importe negativo de la partida
                    Estilo_Cant_Partida_Neg.Font.FontName = "Tahoma";
                    Estilo_Cant_Partida_Neg.Font.Size = 9;
                    Estilo_Cant_Partida_Neg.Font.Bold = false;
                    Estilo_Cant_Partida_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                    Estilo_Cant_Partida_Neg.Alignment.Vertical = StyleVerticalAlignment.Center;
                    Estilo_Cant_Partida_Neg.Font.Color = "red";
                    Estilo_Cant_Partida_Neg.Interior.Color = "White";
                    Estilo_Cant_Partida_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cant_Partida_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Partida_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Partida_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Partida_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el nombre del total
                    Estilo_Nombre_Total.Font.FontName = "Tahoma";
                    Estilo_Nombre_Total.Font.Size = 9;
                    Estilo_Nombre_Total.Font.Bold = false;
                    Estilo_Nombre_Total.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                    Estilo_Nombre_Total.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                    Estilo_Nombre_Total.Font.Color = "#000000";
                    Estilo_Nombre_Total.Interior.Color = "#009933";
                    Estilo_Nombre_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Nombre_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Nombre_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Nombre_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Nombre_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el importe del total
                    Estilo_Cant_Total.Font.FontName = "Tahoma";
                    Estilo_Cant_Total.Font.Size = 9;
                    Estilo_Cant_Total.Font.Bold = false;
                    Estilo_Cant_Total.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                    Estilo_Cant_Total.Alignment.Vertical = StyleVerticalAlignment.Center;
                    Estilo_Cant_Total.Font.Color = "#000000";
                    Estilo_Cant_Total.Interior.Color = "#009933";
                    Estilo_Cant_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cant_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el importe negativo del total
                    Estilo_Cant_Total_Neg.Font.FontName = "Tahoma";
                    Estilo_Cant_Total_Neg.Font.Size = 9;
                    Estilo_Cant_Total_Neg.Font.Bold = false;
                    Estilo_Cant_Total_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                    Estilo_Cant_Total_Neg.Alignment.Vertical = StyleVerticalAlignment.Center;
                    Estilo_Cant_Total_Neg.Font.Color = "red";
                    Estilo_Cant_Total_Neg.Interior.Color = "#009933";
                    Estilo_Cant_Total_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cant_Total_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Total_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Total_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Total_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el nombre de los totales de fuente, area, programa, dependencia, partida generica, concepto y capitulo
                    Estilo_Nombre_Totales.Font.FontName = "Tahoma";
                    Estilo_Nombre_Totales.Font.Size = 9;
                    Estilo_Nombre_Totales.Font.Bold = false;
                    Estilo_Nombre_Totales.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                    Estilo_Nombre_Totales.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                    Estilo_Nombre_Totales.Font.Color = "#000000";
                    Estilo_Nombre_Totales.Interior.Color = "#33CC33";
                    Estilo_Nombre_Totales.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Nombre_Totales.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Nombre_Totales.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Nombre_Totales.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Nombre_Totales.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el importe de los totales de fuente, area, programa, dependencia, partida generica, concepto y capitulo
                    Estilo_Cant_Totales.Font.FontName = "Tahoma";
                    Estilo_Cant_Totales.Font.Size = 9;
                    Estilo_Cant_Totales.Font.Bold = false;
                    Estilo_Cant_Totales.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                    Estilo_Cant_Totales.Alignment.Vertical = StyleVerticalAlignment.Center;
                    Estilo_Cant_Totales.Font.Color = "#000000";
                    Estilo_Cant_Totales.Interior.Color = "#33CC33";
                    Estilo_Cant_Totales.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cant_Totales.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Totales.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Totales.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Totales.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el importe negativo de los totales de fuente, area, programa, dependencia, partida generica, concepto y capitulo
                    Estilo_Cant_Totales_Neg.Font.FontName = "Tahoma";
                    Estilo_Cant_Totales_Neg.Font.Size = 9;
                    Estilo_Cant_Totales_Neg.Font.Bold = false;
                    Estilo_Cant_Totales_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                    Estilo_Cant_Totales_Neg.Alignment.Vertical = StyleVerticalAlignment.Center;
                    Estilo_Cant_Totales_Neg.Font.Color = "red";
                    Estilo_Cant_Totales_Neg.Interior.Color = "#33CC33";
                    Estilo_Cant_Totales_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cant_Totales_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Totales_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Totales_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Totales_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                    #endregion

                    #region (COLUMNAS)
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja1 = Libro.Worksheets.Add("Presupuesto");
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon1 = Hoja1.Table.Rows.Add();
                    //Agregamos las columnas que tendrá la hoja de excel.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//Concepto.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//APROBADO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//AMPLIACION.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//REDUCCION.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//MODIFICADO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//EJERCIDO. 
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PAGADO. /*11*/
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE ENERO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE FEBRERO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE MARZO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE ABRIL.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE MAYO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE JUNIO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE JULIO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE AGOSTO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE SEPTIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE OCTUBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE NOVIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE DICIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO ENERO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO FEBRERO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO MARZO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO ABRIL.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO MAYO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO JUNIO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO JULIO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO AGOSTO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO SEPTIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO OCTUBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO NOVIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO DICIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO ENERO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO FEBRERO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO MARZO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO ABRIL.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO MAYO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO JUNIO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO JULIO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO AGOSTO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO SEPTIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO OCTUBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO NOVIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO DICIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO ENERO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO FEBRERO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO MARZO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO ABRIL.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO MAYO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO JUNIO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO JULIO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO AGOSTO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO SEPTIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO OCTUBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO NOVIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO DICIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//EJERCIDO ENERO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//EJERCIDO FEBRERO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//EJERCIDO MARZO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//EJERCIDO ABRIL.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//EJERCIDO MAYO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//EJERCIDO JUNIO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//EJERCIDO JULIO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//EJERCIDO AGOSTO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//EJERCIDO SEPTIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//EJERCIDO OCTUBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//EJERCIDO NOVIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//EJERCIDO DICIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PAGADO ENERO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PAGADO FEBRERO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PAGADO MARZO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PAGADO ABRIL.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PAGADO MAYO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PAGADO JUNIO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PAGADO JULIO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PAGADO AGOSTO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PAGADO SEPTIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PAGADO OCTUBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PAGADO NOVIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PAGADO DICIEMBRE.
                    #endregion

                    #region (Encabezado)
                    //se llena el encabezado principal
                    Renglon1 = Hoja1.Table.Rows.Add();
                    Celda = Renglon1.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                    Celda.MergeAcross = 82; 
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon1 = Hoja1.Table.Rows.Add();
                    Celda = Renglon1.Cells.Add(Dt_Egr.Rows[0]["Gpo_Dependencia"].ToString().Trim());
                    Celda.MergeAcross = 82; 
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon1 = Hoja1.Table.Rows.Add();
                    Celda = Renglon1.Cells.Add(Dt_Egr.Rows[0]["Dependencia"].ToString().Trim());
                    Celda.MergeAcross = 82; 
                    Celda.StyleID = "HeaderStyle";
                    //encabezado3
                    Renglon1 = Hoja1.Table.Rows.Add();//espacio entre el encabezado y el contenido
                    Renglon1 = Hoja1.Table.Rows.Add();
                    Celda = Renglon1.Cells.Add(Dt_Egr.Rows[0]["Anio"].ToString().Trim());
                    Celda.MergeAcross = 82; 
                    Celda.StyleID = "HeaderStyle3";

                    Renglon1 = Hoja1.Table.Rows.Add();
                    //para concepto
                    Celda = Renglon1.Cells.Add("CONCEPTO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el AMPROBADO
                    Celda = Renglon1.Cells.Add("AMPROBADO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el AMPLIACION
                    Celda = Renglon1.Cells.Add("AMPLIACIÓN");
                    Celda.StyleID = "HeaderStyle2";
                    //para el REDUCCION
                    Celda = Renglon1.Cells.Add("REDUCCIÓN");
                    Celda.StyleID = "HeaderStyle2";
                    //para el MODIFICADO
                    Celda = Renglon1.Cells.Add("MODIFICACIÓN");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DISPONIBLE
                    Celda = Renglon1.Cells.Add("DISPONIBLE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PRE COMPROMETIDO
                    Celda = Renglon1.Cells.Add("PRE COMPROMETIDO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el COMPROMETIDO
                    Celda = Renglon1.Cells.Add("COMPROMETIDO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO
                    Celda = Renglon1.Cells.Add("DEVENGADO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el EJERCIDO
                    Celda = Renglon1.Cells.Add("EJERCIDO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PAGADO
                    Celda = Renglon1.Cells.Add("PAGADO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DISPONIBLE DE ENERO
                    Celda = Renglon1.Cells.Add("DISPONIBLE ENERO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DISPONIBLE DE FEBRERO
                    Celda = Renglon1.Cells.Add("DISPONIBLE FEBRERO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DISPONIBLE DE MARZO
                    Celda = Renglon1.Cells.Add("DISPONIBLE MARZO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DISPONIBLE DE ABRIL
                    Celda = Renglon1.Cells.Add("DISPONIBLE ABRIL");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DISPONIBLE DE MAYO
                    Celda = Renglon1.Cells.Add("DISPONIBLE MAYO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DISPONIBLE DE JUNIO
                    Celda = Renglon1.Cells.Add("DISPONIBLE JUNIO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DISPONIBLE DE JULIO
                    Celda = Renglon1.Cells.Add("DISPONIBLE JULIO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DISPONIBLE DE AGOSTO
                    Celda = Renglon1.Cells.Add("DISPONIBLE AGOSTO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DISPONIBLE DE SEPTIEMBRE
                    Celda = Renglon1.Cells.Add("DISPONIBLE SEPTIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DISPONIBLE DE OCTUBRE
                    Celda = Renglon1.Cells.Add("DISPONIBLE OCTUBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DISPONIBLE DE NOVIEMBRE
                    Celda = Renglon1.Cells.Add("DISPONIBLE NOVIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DISPONIBLE DE DICIEMBRE
                    Celda = Renglon1.Cells.Add("DISPONIBLE DICIEMBRE");
                    Celda.StyleID = "HeaderStyle2";

                    //para el PRECOMPROMETIDO DE ENERO
                    Celda = Renglon1.Cells.Add("PRECOMPROMETIDO ENERO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PRECOMPROMETIDO DE FEBRERO
                    Celda = Renglon1.Cells.Add("PRECOMPROMETIDO FEBRERO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PRECOMPROMETIDO DE MARZO
                    Celda = Renglon1.Cells.Add("PRECOMPROMETIDO MARZO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PRECOMPROMETIDO DE ABRIL
                    Celda = Renglon1.Cells.Add("PRECOMPROMETIDO ABRIL");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PRECOMPROMETIDO DE MAYO
                    Celda = Renglon1.Cells.Add("PRECOMPROMETIDO MAYO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PRECOMPROMETIDO DE JUNIO
                    Celda = Renglon1.Cells.Add("PRECOMPROMETIDO JUNIO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PRECOMPROMETIDO DE JULIO
                    Celda = Renglon1.Cells.Add("PRECOMPROMETIDO JULIO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PRECOMPROMETIDO DE AGOSTO
                    Celda = Renglon1.Cells.Add("PRECOMPROMETIDO AGOSTO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PRECOMPROMETIDO DE SEPTIEMBRE
                    Celda = Renglon1.Cells.Add("PRECOMPROMETIDO SEPTIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PRECOMPROMETIDO DE OCTUBRE
                    Celda = Renglon1.Cells.Add("PRECOMPROMETIDO OCTUBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PRECOMPROMETIDO DE NOVIEMBRE
                    Celda = Renglon1.Cells.Add("PRECOMPROMETIDO NOVIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PRECOMPROMETIDO DE DICIEMBRE
                    Celda = Renglon1.Cells.Add("PRECOMPROMETIDO DICIEMBRE");
                    Celda.StyleID = "HeaderStyle2";

                    //para el COMPROMETIDO DE ENERO
                    Celda = Renglon1.Cells.Add("COMPROMETIDO ENERO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el COMPROMETIDO DE FEBRERO
                    Celda = Renglon1.Cells.Add("COMPROMETIDO FEBRERO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el COMPROMETIDO DE MARZO
                    Celda = Renglon1.Cells.Add("COMPROMETIDO MARZO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el COMPROMETIDO DE ABRIL
                    Celda = Renglon1.Cells.Add("COMPROMETIDO ABRIL");
                    Celda.StyleID = "HeaderStyle2";
                    //para el COMPROMETIDO DE MAYO
                    Celda = Renglon1.Cells.Add("COMPROMETIDO MAYO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el COMPROMETIDO DE JUNIO
                    Celda = Renglon1.Cells.Add("COMPROMETIDO JUNIO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el COMPROMETIDO DE JULIO
                    Celda = Renglon1.Cells.Add("COMPROMETIDO JULIO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el COMPROMETIDO DE AGOSTO
                    Celda = Renglon1.Cells.Add("COMPROMETIDO AGOSTO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el COMPROMETIDO DE SEPTIEMBRE
                    Celda = Renglon1.Cells.Add("COMPROMETIDO SEPTIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el COMPROMETIDO DE OCTUBRE
                    Celda = Renglon1.Cells.Add("COMPROMETIDO OCTUBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el COMPROMETIDO DE NOVIEMBRE
                    Celda = Renglon1.Cells.Add("COMPROMETIDO NOVIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el COMPROMETIDO DE DICIEMBRE
                    Celda = Renglon1.Cells.Add("COMPROMETIDO DICIEMBRE");
                    Celda.StyleID = "HeaderStyle2";

                    //para el DEVENGADO DE ENERO
                    Celda = Renglon1.Cells.Add("DEVENGADO ENERO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO DE FEBRERO
                    Celda = Renglon1.Cells.Add("DEVENGADO FEBRERO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO DE MARZO
                    Celda = Renglon1.Cells.Add("DEVENGADO MARZO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO DE ABRIL
                    Celda = Renglon1.Cells.Add("DEVENGADO ABRIL");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO DE MAYO
                    Celda = Renglon1.Cells.Add("DEVENGADO MAYO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO DE JUNIO
                    Celda = Renglon1.Cells.Add("DEVENGADO JUNIO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO DE JULIO
                    Celda = Renglon1.Cells.Add("DEVENGADO JULIO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO DE AGOSTO
                    Celda = Renglon1.Cells.Add("DEVENGADO AGOSTO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO DE SEPTIEMBRE
                    Celda = Renglon1.Cells.Add("DEVENGADO SEPTIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO DE OCTUBRE
                    Celda = Renglon1.Cells.Add("DEVENGADO OCTUBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO DE NOVIEMBRE
                    Celda = Renglon1.Cells.Add("DEVENGADO NOVIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO DE DICIEMBRE
                    Celda = Renglon1.Cells.Add("DEVENGADO DICIEMBRE");
                    Celda.StyleID = "HeaderStyle2";

                    //para el EJERCIDO DE ENERO
                    Celda = Renglon1.Cells.Add("EJERCIDO ENERO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el EJERCIDO DE FEBRERO
                    Celda = Renglon1.Cells.Add("EJERCIDO FEBRERO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el EJERCIDO DE MARZO
                    Celda = Renglon1.Cells.Add("EJERCIDO MARZO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el EJERCIDO DE ABRIL
                    Celda = Renglon1.Cells.Add("EJERCIDO ABRIL");
                    Celda.StyleID = "HeaderStyle2";
                    //para el EJERCIDO DE MAYO
                    Celda = Renglon1.Cells.Add("EJERCIDO MAYO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el EJERCIDO DE JUNIO
                    Celda = Renglon1.Cells.Add("EJERCIDO JUNIO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el EJERCIDO DE JULIO
                    Celda = Renglon1.Cells.Add("EJERCIDO JULIO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el EJERCIDO DE AGOSTO
                    Celda = Renglon1.Cells.Add("EJERCIDO AGOSTO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el EJERCIDO DE SEPTIEMBRE
                    Celda = Renglon1.Cells.Add("EJERCIDO SEPTIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el EJERCIDO DE OCTUBRE
                    Celda = Renglon1.Cells.Add("EJERCIDO OCTUBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el EJERCIDO DE NOVIEMBRE
                    Celda = Renglon1.Cells.Add("EJERCIDO NOVIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el EJERCIDO DE DICIEMBRE
                    Celda = Renglon1.Cells.Add("EJERCIDO DICIEMBRE");
                    Celda.StyleID = "HeaderStyle2";

                    //para el PAGADO DE ENERO
                    Celda = Renglon1.Cells.Add("PAGADO ENERO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PAGADO DE FEBRERO
                    Celda = Renglon1.Cells.Add("PAGADO FEBRERO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PAGADO DE MARZO
                    Celda = Renglon1.Cells.Add("PAGADO MARZO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PAGADO DE ABRIL
                    Celda = Renglon1.Cells.Add("PAGADO ABRIL");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PAGADO DE MAYO
                    Celda = Renglon1.Cells.Add("PAGADO MAYO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PAGADO DE JUNIO
                    Celda = Renglon1.Cells.Add("PAGADO JUNIO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PAGADO DE JULIO
                    Celda = Renglon1.Cells.Add("PAGADO JULIO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PAGADO DE AGOSTO
                    Celda = Renglon1.Cells.Add("PAGADO AGOSTO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PAGADO DE SEPTIEMBRE
                    Celda = Renglon1.Cells.Add("PAGADO SEPTIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PAGADO DE OCTUBRE
                    Celda = Renglon1.Cells.Add("PAGADO OCTUBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PAGADO DE NOVIEMBRE
                    Celda = Renglon1.Cells.Add("PAGADO NOVIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PAGADO DE DICIEMBRE
                    Celda = Renglon1.Cells.Add("PAGADO DICIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    #endregion

                    foreach (DataRow Dr in Dt_Egr.Rows)
                    {
                        Renglon1 = Hoja1.Table.Rows.Add();
                        foreach (DataColumn Columna in Dt_Egr.Columns)
                        {
                            if (Columna is DataColumn)
                            {
                                if (Dr["Tipo"].ToString().Equals("Tot"))
                                {
                                    if (Columna.ColumnName.Equals("Concepto"))
                                    {
                                        Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Nombre_Total"));
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName != "Tipo" && Columna.ColumnName != "Elaboro" && Columna.ColumnName != "Dependencia"
                                            && Columna.ColumnName != "Anio" && Columna.ColumnName != "Gpo_Dependencia")
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.String, "Cant_Total"));
                                            }
                                            else
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.String, "Cant_Total_Neg"));
                                            }
                                        }
                                    }
                                }
                                else if (Dr["Tipo"].ToString().Equals("P"))
                                {
                                    if (Columna.ColumnName.Equals("Concepto"))
                                    {
                                        Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Nombre_Partida"));
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName != "Tipo" && Columna.ColumnName != "Elaboro" && Columna.ColumnName != "Dependencia"
                                            && Columna.ColumnName != "Anio" && Columna.ColumnName != "Gpo_Dependencia")
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.String, "Cant_Partida"));
                                            }
                                            else
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.String, "Cant_Partida_Neg"));
                                            }
                                        }
                                    }
                                }
                                else 
                                {
                                    if (Columna.ColumnName.Equals("Concepto"))
                                    {
                                        Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Nombre_Totales"));
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName != "Tipo" && Columna.ColumnName != "Elaboro" && Columna.ColumnName != "Dependencia"
                                            && Columna.ColumnName != "Anio" && Columna.ColumnName != "Gpo_Dependencia")
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.String, "Cant_Totales"));
                                            }
                                            else
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.String, "Cant_Totales_Neg"));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                    Renglon1 = Hoja1.Table.Rows.Add();
                    Renglon1 = Hoja1.Table.Rows.Add();
                    Celda = Renglon1.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                    Celda.MergeAcross = 82;
                    Celda.StyleID = "Nombre_Partida";
                    Renglon1 = Hoja1.Table.Rows.Add();
                    Celda = Renglon1.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                    Celda.MergeAcross = 82;
                    Celda.StyleID = "Nombre_Partida";

                    Libro.Save(Server.MapPath(Ruta_Archivo.Trim()));

                    //Abre el archivo de excel
                    //Response.Clear();
                    //Response.Buffer = true;
                    //Response.ContentType = "application/vnd.ms-excel";
                    //Response.AddHeader("Content-Disposition", "attachment;filename=" + Nombre_Archivo);
                    //Response.Charset = "UTF-8";
                    //Response.ContentEncoding = Encoding.Default;
                    //Libro.Save(Response.OutputStream);
                    //Response.End();
                }
                catch (Exception Ex)
                {
                    Ruta_Archivo = "Error al generar el reporte mensual del presupuesto de egresos. Error[" + Ex.Message.ToString() + "]";
                   throw new Exception( "Error al generar el reporte mensual del presupuesto de egresos. Error[" + Ex.Message.ToString() + "]");
                }
                return Ruta_Archivo;
            }

        #endregion

        #region (Reportes Psp Ingresos Mensual)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Reporte_Mensual_Ingresos
            ///DESCRIPCIÓN          : Metodo para obtener los datos para el reporte de ingresos mensual
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 17/Diciembre/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Reporte_Mensual_Ingresos(String Anio, String FF_Ing, String PP_Ing, String PP_Ing_F, String R, 
                String R_F, String T, String T_F, String Cl, String Cl_F, String Con_Ing, String Con_Ing_F, String SubCon, String SubCon_F)
            {
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Json_Datos = "";
                DataTable Dt_Ing = new DataTable();
                DataTable Dt_Ing_Completo = new DataTable();
                String Json_Tot = String.Empty;

                try
                {
                    //parametros ingresos
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento_Ing = FF_Ing;
                    Negocio.P_Programa_Ing_ID = PP_Ing;
                    Negocio.P_ProgramaF_Ing_ID = PP_Ing_F;
                    Negocio.P_Rubro_ID = R;
                    Negocio.P_RubroF_ID = R_F;
                    Negocio.P_Tipo_ID = T;
                    Negocio.P_TipoF_ID = T_F;
                    Negocio.P_Clase_Ing_ID = Cl;
                    Negocio.P_ClaseF_Ing_ID = Cl_F;
                    Negocio.P_Concepto_Ing_ID = Con_Ing;
                    Negocio.P_ConceptoF_Ing_ID = Con_Ing_F;
                    Negocio.P_SubConcepto_ID = SubCon;
                    Negocio.P_SubConceptoF_ID = SubCon_F;

                    Dt_Ing = Negocio.Consultar_Psp_Ing();
                    Dt_Ing_Completo = Generar_Dt_Psp_Ing_Mensual(Dt_Ing);

                    Json_Datos = Obtener_Param_Niveles_Ingresos(Dt_Ing_Completo, Anio);

                    return Json_Datos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Nivel_Fuente_Financiamiento_Ingresos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Param_Niveles_Ingresos
            ///DESCRIPCIÓN          : Metodo para obtener los parametros de los niveles del reporte
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 17/Diciembre/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Param_Niveles_Ingresos(DataTable Dt_Ing, String Anio)
            {
                String Nom_Archivo = String.Empty;
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                //Parametros de los combos
                String Niv_FF_Ing = String.Empty;
                String Niv_PP_Ing = String.Empty;
                String Niv_Rub = String.Empty;
                String Niv_Tip = String.Empty;
                String Niv_Cla = String.Empty;
                String Niv_Con_Ing = String.Empty;
                String Niv_SubCon = String.Empty;

                DataTable Dt_Niv_Psp_Ing = new DataTable();
                DataRow Fila;
                Boolean Insertar = false;
                DataTable Dt_UR = new DataTable();
                String Dependencia = String.Empty;
                String Gpo_Dep = String.Empty;

                try
                {
                    #region (Parametros)
                    if (this.Request.QueryString["Niv_FF_Ing"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_FF_Ing"].ToString().Trim()))
                    {
                        Niv_FF_Ing = this.Request.QueryString["Niv_FF_Ing"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_PP_Ing"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_PP_Ing"].ToString().Trim()))
                    {
                        Niv_PP_Ing = this.Request.QueryString["Niv_PP_Ing"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_Rub"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_Rub"].ToString().Trim()))
                    {
                        Niv_Rub = this.Request.QueryString["Niv_Rub"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_Tip"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_Tip"].ToString().Trim()))
                    {
                        Niv_Tip = this.Request.QueryString["Niv_Tip"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_Cla"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_Cla"].ToString().Trim()))
                    {
                        Niv_Cla = this.Request.QueryString["Niv_Cla"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_Con_Ing"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_Con_Ing"].ToString().Trim()))
                    {
                        Niv_Con_Ing = this.Request.QueryString["Niv_Con_Ing"].ToString().Trim();
                    }
                    if (this.Request.QueryString["Niv_SubCon"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Niv_SubCon"].ToString().Trim()))
                    {
                        Niv_SubCon = this.Request.QueryString["Niv_SubCon"].ToString().Trim();
                    }
                    #endregion

                    //obtenemos la dependencia del usuario y el grupo dependencia
                    Negocio.P_Busqueda = String.Empty;
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = String.Empty;
                    Negocio.P_Area_Funcional_ID = String.Empty;
                    Negocio.P_Tipo_Usuario = "Usuario";
                    Negocio.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado.Trim();

                    Dt_UR = Negocio.Consultar_UR();

                    if (Dt_UR != null && Dt_UR.Rows.Count > 0)
                    {
                        Dependencia = Dt_UR.Rows[0]["NOM_DEP"].ToString().Trim();
                        Gpo_Dep = Dt_UR.Rows[0]["NOM_GPO_DEP"].ToString().Trim();
                    }

                    if (Dt_Ing != null && Dt_Ing.Rows.Count > 0)
                    {
                        Dt_Niv_Psp_Ing.Columns.Add("Concepto", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Aprobado", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Ampliacion", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Reduccion", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Modificado", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Por_Recaudar", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Devengado", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Recaudado", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Tipo", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Elaboro", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Dependencia", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Anio", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Gpo_Dependencia", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Por_Recaudar_Ene", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Por_Recaudar_Feb", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Por_Recaudar_Mar", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Por_Recaudar_Abr", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Por_Recaudar_May", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Por_Recaudar_Jun", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Por_Recaudar_Jul", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Por_Recaudar_Ago", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Por_Recaudar_Sep", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Por_Recaudar_Oct", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Por_Recaudar_Nov", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Por_Recaudar_Dic", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Devengado_Ene", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Devengado_Feb", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Devengado_Mar", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Devengado_Abr", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Devengado_May", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Devengado_Jun", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Devengado_Jul", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Devengado_Ago", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Devengado_Sep", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Devengado_Oct", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Devengado_Nov", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Devengado_Dic", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Recaudado_Ene", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Recaudado_Feb", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Recaudado_Mar", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Recaudado_Abr", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Recaudado_May", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Recaudado_Jun", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Recaudado_Jul", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Recaudado_Ago", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Recaudado_Sep", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Recaudado_Oct", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Recaudado_Nov", System.Type.GetType("System.String"));
                        Dt_Niv_Psp_Ing.Columns.Add("Recaudado_Dic", System.Type.GetType("System.String"));


                        foreach (DataRow Dr in Dt_Ing.Rows)
                        {
                            Insertar = false;

                            if (Dr["Tipo"].ToString().Trim().Equals("Tot"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("FF") && Niv_FF_Ing.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("PP") && Niv_PP_Ing.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("R") && Niv_Rub.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("T") && Niv_Tip.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("CL") && Niv_Cla.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("C") && Niv_Con_Ing.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else if (Dr["Tipo"].ToString().Trim().Equals("SC") && Niv_SubCon.Trim().Equals("S"))
                            {
                                Insertar = true;
                            }
                            else
                            {
                                Insertar = false;
                            }

                            if (Insertar)
                            {
                                Fila = Dt_Niv_Psp_Ing.NewRow();
                                Fila["Concepto"] = Dr["Concepto"].ToString().Trim();
                                Fila["Aprobado"] = Dr["Aprobado"].ToString().Trim();
                                Fila["Ampliacion"] = Dr["Ampliacion"].ToString().Trim();
                                Fila["Reduccion"] = Dr["Reduccion"].ToString().Trim();
                                Fila["Modificado"] = Dr["Modificado"].ToString().Trim();
                                Fila["Por_Recaudar"] = Dr["Por_Recaudar"].ToString().Trim();
                                Fila["Devengado"] = Dr["Devengado"].ToString().Trim();
                                Fila["Recaudado"] = Dr["Recaudado"].ToString().Trim();
                                Fila["Tipo"] = Dr["Tipo"].ToString().Trim();
                                Fila["Elaboro"] = Cls_Sessiones.Nombre_Empleado.Trim();
                                Fila["Dependencia"] = Dependencia.Trim();
                                Fila["Anio"] = "PRESUPUESTO DE INGRESOS EJERCICIO " + Anio.Trim();
                                Fila["Gpo_Dependencia"] = Gpo_Dep.Trim();
                                Fila["Por_Recaudar_Ene"] = Dr["Por_Recaudar_Ene"].ToString().Trim();
                                Fila["Por_Recaudar_Feb"] = Dr["Por_Recaudar_Feb"].ToString().Trim();
                                Fila["Por_Recaudar_Mar"] = Dr["Por_Recaudar_Mar"].ToString().Trim();
                                Fila["Por_Recaudar_Abr"] = Dr["Por_Recaudar_Abr"].ToString().Trim();
                                Fila["Por_Recaudar_May"] = Dr["Por_Recaudar_May"].ToString().Trim();
                                Fila["Por_Recaudar_Jun"] = Dr["Por_Recaudar_Jun"].ToString().Trim();
                                Fila["Por_Recaudar_Jul"] = Dr["Por_Recaudar_Jul"].ToString().Trim();
                                Fila["Por_Recaudar_Ago"] = Dr["Por_Recaudar_Ago"].ToString().Trim();
                                Fila["Por_Recaudar_Sep"] = Dr["Por_Recaudar_Sep"].ToString().Trim();
                                Fila["Por_Recaudar_Oct"] = Dr["Por_Recaudar_Oct"].ToString().Trim();
                                Fila["Por_Recaudar_Nov"] = Dr["Por_Recaudar_Nov"].ToString().Trim();
                                Fila["Por_Recaudar_Dic"] = Dr["Por_Recaudar_Dic"].ToString().Trim();
                                Fila["Devengado_Ene"] = Dr["Devengado_Ene"].ToString().Trim();
                                Fila["Devengado_Feb"] = Dr["Devengado_Feb"].ToString().Trim();
                                Fila["Devengado_Mar"] = Dr["Devengado_Mar"].ToString().Trim();
                                Fila["Devengado_Abr"] = Dr["Devengado_Abr"].ToString().Trim();
                                Fila["Devengado_May"] = Dr["Devengado_May"].ToString().Trim();
                                Fila["Devengado_Jun"] = Dr["Devengado_Jun"].ToString().Trim();
                                Fila["Devengado_Jul"] = Dr["Devengado_Jul"].ToString().Trim();
                                Fila["Devengado_Ago"] = Dr["Devengado_Ago"].ToString().Trim();
                                Fila["Devengado_Sep"] = Dr["Devengado_Sep"].ToString().Trim();
                                Fila["Devengado_Oct"] = Dr["Devengado_Oct"].ToString().Trim();
                                Fila["Devengado_Nov"] = Dr["Devengado_Nov"].ToString().Trim();
                                Fila["Devengado_Dic"] = Dr["Devengado_Dic"].ToString().Trim();
                                Fila["Recaudado_Ene"] = Dr["Recaudado_Ene"].ToString().Trim();
                                Fila["Recaudado_Feb"] = Dr["Recaudado_Feb"].ToString().Trim();
                                Fila["Recaudado_Mar"] = Dr["Recaudado_Mar"].ToString().Trim();
                                Fila["Recaudado_Abr"] = Dr["Recaudado_Abr"].ToString().Trim();
                                Fila["Recaudado_May"] = Dr["Recaudado_May"].ToString().Trim();
                                Fila["Recaudado_Jun"] = Dr["Recaudado_Jun"].ToString().Trim();
                                Fila["Recaudado_Jul"] = Dr["Recaudado_Jul"].ToString().Trim();
                                Fila["Recaudado_Ago"] = Dr["Recaudado_Ago"].ToString().Trim();
                                Fila["Recaudado_Sep"] = Dr["Recaudado_Sep"].ToString().Trim();
                                Fila["Recaudado_Oct"] = Dr["Recaudado_Oct"].ToString().Trim();
                                Fila["Recaudado_Nov"] = Dr["Recaudado_Nov"].ToString().Trim();
                                Fila["Recaudado_Dic"] = Dr["Recaudado_Dic"].ToString().Trim();
                                Dt_Niv_Psp_Ing.Rows.Add(Fila);
                            }
                        }

                        if (Dt_Niv_Psp_Ing != null)
                        {
                            if (Dt_Niv_Psp_Ing.Rows.Count > 0)
                            {
                                Nom_Archivo = Generar_Reporte_Psp_Ingresos_Mensual(Dt_Niv_Psp_Ing, Anio);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al obtener los datos del reporte mensual de ingresos. Error[" + ex.Message + "]");
                }
                return Nom_Archivo;
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Generar_Dt_Psp_Ing_Mensual
            ///DESCRIPCIÓN          : Metodo para generar la tabla del presupuesto de ingresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 17/Diciembre/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private DataTable Generar_Dt_Psp_Ing_Mensual(DataTable Dt_Ing)
            {
                DataTable Dt_Psp = new DataTable();
                DataRow Fila;
                Boolean SubConcepto = false;
                Boolean Programa = false;


                try
                {
                    
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al generar el presupuesto de ingresos. Error[" + ex.Message + "]");
                }
                return Dt_Psp;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Generar_Reporte_Psp_Ingresos_Mensual
            ///DESCRIPCIÓN          : metodo para generar el reporte en excel
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 17/Diciembre/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public String Generar_Reporte_Psp_Ingresos_Mensual(DataTable Dt_Ing, String Anio)
            {
                Double Cantidad = 0.00;
                WorksheetCell Celda = new WorksheetCell();
                String Ruta_Archivo = "../../Reporte/Rpt_Presupuestos_Ingreso_" + Anio + "_" + Session.SessionID + ".xls     ";

                try
                {
                    //Creamos el libro de Excel.
                    CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
                    //propiedades del libro
                    Libro.Properties.Title = "Reporte_Presupuesto";
                    Libro.Properties.Created = DateTime.Now;
                    Libro.Properties.Author = "JAPAMI";

                    #region Estilos
                    //Creamos el estilo cabecera para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
                    //Creamos el estilo cabecera 2 para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera2 = Libro.Styles.Add("HeaderStyle2");
                    //Creamos el estilo cabecera 3 para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera3 = Libro.Styles.Add("HeaderStyle3");
                    //Creamos el estilo contenido del concepto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Nombre_Partida = Libro.Styles.Add("Nombre_Partida");
                    //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_Partida = Libro.Styles.Add("Cant_Partida");
                    //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_Partida_Neg = Libro.Styles.Add("Cant_Partida_Neg");
                    //Creamos el estilo contenido del concepto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Nombre_Totales = Libro.Styles.Add("Nombre_Totales");
                    //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_Totales = Libro.Styles.Add("Cant_Totales");
                    //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_Totales_Neg = Libro.Styles.Add("Cant_Totales_Neg");
                    //Creamos el estilo contenido del concepto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Nombre_Total = Libro.Styles.Add("Nombre_Total");
                    //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_Total = Libro.Styles.Add("Cant_Total");
                    //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_Total_Neg = Libro.Styles.Add("Cant_Total_Neg");
                    //Creamos el estilo contenido del concepto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Nombre_SC = Libro.Styles.Add("Nombre_SC");
                    //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_SC = Libro.Styles.Add("Cant_SC");
                    //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_SC_Neg = Libro.Styles.Add("Cant_SC_Neg");
                    //Creamos el estilo contenido del concepto para la hoja de excel. 

                    //estilo para la cabecera
                    Estilo_Cabecera.Font.FontName = "Tahoma";
                    Estilo_Cabecera.Font.Size = 12;
                    Estilo_Cabecera.Font.Bold = true;
                    Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                    Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                    Estilo_Cabecera.Font.Color = "#FFFFFF";
                    Estilo_Cabecera.Interior.Color = "Gray";
                    Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para la cabecera2
                    Estilo_Cabecera2.Font.FontName = "Tahoma";
                    Estilo_Cabecera2.Font.Size = 10;
                    Estilo_Cabecera2.Font.Bold = true;
                    Estilo_Cabecera2.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                    Estilo_Cabecera2.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                    Estilo_Cabecera2.Font.Color = "#FFFFFF";
                    Estilo_Cabecera2.Interior.Color = "DarkGray";
                    Estilo_Cabecera2.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cabecera2.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para la cabecera3
                    Estilo_Cabecera3.Font.FontName = "Tahoma";
                    Estilo_Cabecera3.Font.Size = 12;
                    Estilo_Cabecera3.Font.Bold = true;
                    Estilo_Cabecera3.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                    Estilo_Cabecera3.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                    Estilo_Cabecera3.Font.Color = "#000000";
                    Estilo_Cabecera3.Interior.Color = "white";
                    Estilo_Cabecera3.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cabecera3.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera3.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera3.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera3.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el nombre de la partida
                    Estilo_Nombre_Partida.Font.FontName = "Tahoma";
                    Estilo_Nombre_Partida.Font.Size = 9;
                    Estilo_Nombre_Partida.Font.Bold = false;
                    Estilo_Nombre_Partida.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                    Estilo_Nombre_Partida.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                    Estilo_Nombre_Partida.Font.Color = "#000000";
                    Estilo_Nombre_Partida.Interior.Color = "#CCFFCC";
                    Estilo_Nombre_Partida.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Nombre_Partida.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Nombre_Partida.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Nombre_Partida.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Nombre_Partida.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el importe de la partida
                    Estilo_Cant_Partida.Font.FontName = "Tahoma";
                    Estilo_Cant_Partida.Font.Size = 9;
                    Estilo_Cant_Partida.Font.Bold = false;
                    Estilo_Cant_Partida.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                    Estilo_Cant_Partida.Alignment.Vertical = StyleVerticalAlignment.Center;
                    Estilo_Cant_Partida.Font.Color = "#000000";
                    Estilo_Cant_Partida.Interior.Color = "#CCFFCC";
                    Estilo_Cant_Partida.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cant_Partida.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Partida.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Partida.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Partida.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el importe negativo de la partida
                    Estilo_Cant_Partida_Neg.Font.FontName = "Tahoma";
                    Estilo_Cant_Partida_Neg.Font.Size = 9;
                    Estilo_Cant_Partida_Neg.Font.Bold = false;
                    Estilo_Cant_Partida_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                    Estilo_Cant_Partida_Neg.Alignment.Vertical = StyleVerticalAlignment.Center;
                    Estilo_Cant_Partida_Neg.Font.Color = "red";
                    Estilo_Cant_Partida_Neg.Interior.Color = "#CCFFCC";
                    Estilo_Cant_Partida_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cant_Partida_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Partida_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Partida_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Partida_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el nombre del total
                    Estilo_Nombre_Total.Font.FontName = "Tahoma";
                    Estilo_Nombre_Total.Font.Size = 9;
                    Estilo_Nombre_Total.Font.Bold = false;
                    Estilo_Nombre_Total.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                    Estilo_Nombre_Total.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                    Estilo_Nombre_Total.Font.Color = "#000000";
                    Estilo_Nombre_Total.Interior.Color = "#339999";
                    Estilo_Nombre_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Nombre_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Nombre_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Nombre_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Nombre_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el importe del total
                    Estilo_Cant_Total.Font.FontName = "Tahoma";
                    Estilo_Cant_Total.Font.Size = 9;
                    Estilo_Cant_Total.Font.Bold = false;
                    Estilo_Cant_Total.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                    Estilo_Cant_Total.Alignment.Vertical = StyleVerticalAlignment.Center;
                    Estilo_Cant_Total.Font.Color = "#000000";
                    Estilo_Cant_Total.Interior.Color = "#339999";
                    Estilo_Cant_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cant_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el importe negativo del total
                    Estilo_Cant_Total_Neg.Font.FontName = "Tahoma";
                    Estilo_Cant_Total_Neg.Font.Size = 9;
                    Estilo_Cant_Total_Neg.Font.Bold = false;
                    Estilo_Cant_Total_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                    Estilo_Cant_Total_Neg.Alignment.Vertical = StyleVerticalAlignment.Center;
                    Estilo_Cant_Total_Neg.Font.Color = "red";
                    Estilo_Cant_Total_Neg.Interior.Color = "#339999";
                    Estilo_Cant_Total_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cant_Total_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Total_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Total_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Total_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el nombre de los totales de fuente, area, programa, dependencia, partida generica, concepto y capitulo
                    Estilo_Nombre_Totales.Font.FontName = "Tahoma";
                    Estilo_Nombre_Totales.Font.Size = 9;
                    Estilo_Nombre_Totales.Font.Bold = false;
                    Estilo_Nombre_Totales.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                    Estilo_Nombre_Totales.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                    Estilo_Nombre_Totales.Font.Color = "#000000";
                    Estilo_Nombre_Totales.Interior.Color = "#66CCCC";
                    Estilo_Nombre_Totales.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Nombre_Totales.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Nombre_Totales.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Nombre_Totales.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Nombre_Totales.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el importe de los totales de fuente, area, programa, dependencia, partida generica, concepto y capitulo
                    Estilo_Cant_Totales.Font.FontName = "Tahoma";
                    Estilo_Cant_Totales.Font.Size = 9;
                    Estilo_Cant_Totales.Font.Bold = false;
                    Estilo_Cant_Totales.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                    Estilo_Cant_Totales.Alignment.Vertical = StyleVerticalAlignment.Center;
                    Estilo_Cant_Totales.Font.Color = "#000000";
                    Estilo_Cant_Totales.Interior.Color = "#66CCCC";
                    Estilo_Cant_Totales.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cant_Totales.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Totales.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Totales.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Totales.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el importe negativo de los totales de fuente, area, programa, dependencia, partida generica, concepto y capitulo
                    Estilo_Cant_Totales_Neg.Font.FontName = "Tahoma";
                    Estilo_Cant_Totales_Neg.Font.Size = 9;
                    Estilo_Cant_Totales_Neg.Font.Bold = false;
                    Estilo_Cant_Totales_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                    Estilo_Cant_Totales_Neg.Alignment.Vertical = StyleVerticalAlignment.Center;
                    Estilo_Cant_Totales_Neg.Font.Color = "red";
                    Estilo_Cant_Totales_Neg.Interior.Color = "#66CCCC";
                    Estilo_Cant_Totales_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cant_Totales_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Totales_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Totales_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_Totales_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el nombre de la partida
                    Estilo_Nombre_SC.Font.FontName = "Tahoma";
                    Estilo_Nombre_SC.Font.Size = 9;
                    Estilo_Nombre_SC.Font.Bold = false;
                    Estilo_Nombre_SC.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                    Estilo_Nombre_SC.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                    Estilo_Nombre_SC.Font.Color = "#000000";
                    Estilo_Nombre_SC.Interior.Color = "White";
                    Estilo_Nombre_SC.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Nombre_SC.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Nombre_SC.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Nombre_SC.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Nombre_SC.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el importe de la partida
                    Estilo_Cant_SC.Font.FontName = "Tahoma";
                    Estilo_Cant_SC.Font.Size = 9;
                    Estilo_Cant_SC.Font.Bold = false;
                    Estilo_Cant_SC.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                    Estilo_Cant_SC.Alignment.Vertical = StyleVerticalAlignment.Center;
                    Estilo_Cant_SC.Font.Color = "#000000";
                    Estilo_Cant_SC.Interior.Color = "White";
                    Estilo_Cant_SC.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cant_SC.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_SC.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_SC.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_SC.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el importe negativo de la partida
                    Estilo_Cant_SC_Neg.Font.FontName = "Tahoma";
                    Estilo_Cant_SC_Neg.Font.Size = 9;
                    Estilo_Cant_SC_Neg.Font.Bold = false;
                    Estilo_Cant_SC_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                    Estilo_Cant_SC_Neg.Alignment.Vertical = StyleVerticalAlignment.Center;
                    Estilo_Cant_SC_Neg.Font.Color = "red";
                    Estilo_Cant_SC_Neg.Interior.Color = "White";
                    Estilo_Cant_SC_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cant_SC_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_SC_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_SC_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cant_SC_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    #endregion

                    #region (COLUMNAS)
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja1 = Libro.Worksheets.Add("Presupuesto");
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon1 = Hoja1.Table.Rows.Add();
                    //Agregamos las columnas que tendrá la hoja de excel.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//Concepto.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//APROBADO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//AMPLIACION.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//REDUCCION.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//MODIFICADO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO. 
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR ENERO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR FEBRERO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR MARZO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR ABRIL.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR MAYO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR JUNIO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR JULIO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR AGOSTO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR SEPTIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR OCTUBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR NOVIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR DICIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO ENERO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO FEBRERO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO MARZO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO ABRIL.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO MAYO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO JUNIO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO JULIO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO AGOSTO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO SEPTIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO OCTUBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO NOVIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO DICIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO ENERO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO FEBRERO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO MARZO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO ABRIL.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO MAYO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO JUNIO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO JULIO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO AGOSTO.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO SEPTIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO OCTUBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO NOVIEMBRE.
                    Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO DICIEMBRE.
                    #endregion

                    #region (Encabezado)
                    //se llena el encabezado principal
                    Renglon1 = Hoja1.Table.Rows.Add();
                    Celda = Renglon1.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                    Celda.MergeAcross = 43;
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon1 = Hoja1.Table.Rows.Add();
                    Celda = Renglon1.Cells.Add(Dt_Ing.Rows[0]["Gpo_Dependencia"].ToString().Trim());
                    Celda.MergeAcross = 43;
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon1 = Hoja1.Table.Rows.Add();
                    Celda = Renglon1.Cells.Add(Dt_Ing.Rows[0]["Dependencia"].ToString().Trim());
                    Celda.MergeAcross = 43;
                    Celda.StyleID = "HeaderStyle";
                    //encabezado3
                    Renglon1 = Hoja1.Table.Rows.Add();//espacio entre el encabezado y el contenido
                    Renglon1 = Hoja1.Table.Rows.Add();
                    Celda = Renglon1.Cells.Add(Dt_Ing.Rows[0]["Anio"].ToString().Trim());
                    Celda.MergeAcross = 43;
                    Celda.StyleID = "HeaderStyle3";

                    Renglon1 = Hoja1.Table.Rows.Add();
                    //para concepto
                    Celda = Renglon1.Cells.Add("CONCEPTO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el AMPROBADO
                    Celda = Renglon1.Cells.Add("AMPROBADO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el AMPLIACION
                    Celda = Renglon1.Cells.Add("AMPLIACIÓN");
                    Celda.StyleID = "HeaderStyle2";
                    //para el REDUCCION
                    Celda = Renglon1.Cells.Add("REDUCCIÓN");
                    Celda.StyleID = "HeaderStyle2";
                    //para el MODIFICADO
                    Celda = Renglon1.Cells.Add("MODIFICACIÓN");
                    Celda.StyleID = "HeaderStyle2";
                    //para el POR RECAUDAR
                    Celda = Renglon1.Cells.Add("POR RECAUDAR");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO
                    Celda = Renglon1.Cells.Add("DEVENGADO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el RECAUDADO
                    Celda = Renglon1.Cells.Add("RECAUDADO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el POR RECAUDAR DE ENERO
                    Celda = Renglon1.Cells.Add("POR RECAUDAR ENERO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el POR RECAUDAR DE FEBRERO
                    Celda = Renglon1.Cells.Add("POR RECAUDAR FEBRERO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el POR RECAUDAR DE MARZO
                    Celda = Renglon1.Cells.Add("POR RECAUDAR MARZO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el POR RECAUDAR DE ABRIL
                    Celda = Renglon1.Cells.Add("POR RECAUDAR ABRIL");
                    Celda.StyleID = "HeaderStyle2";
                    //para el POR RECAUDAR DE MAYO
                    Celda = Renglon1.Cells.Add("POR RECAUDAR MAYO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el POR RECAUDAR DE JUNIO
                    Celda = Renglon1.Cells.Add("POR RECAUDAR JUNIO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el POR RECAUDAR DE JULIO
                    Celda = Renglon1.Cells.Add("POR RECAUDAR JULIO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el POR RECAUDAR DE AGOSTO
                    Celda = Renglon1.Cells.Add("POR RECAUDAR AGOSTO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el POR RECAUDAR DE SEPTIEMBRE
                    Celda = Renglon1.Cells.Add("POR RECAUDAR SEPTIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el POR RECAUDAR DE OCTUBRE
                    Celda = Renglon1.Cells.Add("POR RECAUDAR OCTUBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el POR RECAUDAR DE NOVIEMBRE
                    Celda = Renglon1.Cells.Add("POR RECAUDAR NOVIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el POR RECAUDAR DE DICIEMBRE
                    Celda = Renglon1.Cells.Add("POR RECAUDAR DICIEMBRE");
                    Celda.StyleID = "HeaderStyle2";

                    //para el DEVENGADO DE ENERO
                    Celda = Renglon1.Cells.Add("DEVENGADO ENERO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO DE FEBRERO
                    Celda = Renglon1.Cells.Add("DEVENGADO FEBRERO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO DE MARZO
                    Celda = Renglon1.Cells.Add("DEVENGADO MARZO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO DE ABRIL
                    Celda = Renglon1.Cells.Add("DEVENGADO ABRIL");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO DE MAYO
                    Celda = Renglon1.Cells.Add("DEVENGADO MAYO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO DE JUNIO
                    Celda = Renglon1.Cells.Add("DEVENGADO JUNIO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO DE JULIO
                    Celda = Renglon1.Cells.Add("DEVENGADO JULIO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO DE AGOSTO
                    Celda = Renglon1.Cells.Add("DEVENGADO AGOSTO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO DE SEPTIEMBRE
                    Celda = Renglon1.Cells.Add("DEVENGADO SEPTIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO DE OCTUBRE
                    Celda = Renglon1.Cells.Add("DEVENGADO OCTUBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO DE NOVIEMBRE
                    Celda = Renglon1.Cells.Add("DEVENGADO NOVIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO DE DICIEMBRE
                    Celda = Renglon1.Cells.Add("DEVENGADO DICIEMBRE");
                    Celda.StyleID = "HeaderStyle2";

                    //para el RECAUDADO DE ENERO
                    Celda = Renglon1.Cells.Add("RECAUDADO ENERO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el RECAUDADO DE FEBRERO
                    Celda = Renglon1.Cells.Add("RECAUDADO FEBRERO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el RECAUDADO DE MARZO
                    Celda = Renglon1.Cells.Add("RECAUDADO MARZO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el RECAUDADO DE ABRIL
                    Celda = Renglon1.Cells.Add("RECAUDADO ABRIL");
                    Celda.StyleID = "HeaderStyle2";
                    //para el RECAUDADO DE MAYO
                    Celda = Renglon1.Cells.Add("RECAUDADO MAYO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el RECAUDADO DE JUNIO
                    Celda = Renglon1.Cells.Add("RECAUDADO JUNIO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el RECAUDADO DE JULIO
                    Celda = Renglon1.Cells.Add("RECAUDADO JULIO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el RECAUDADO DE AGOSTO
                    Celda = Renglon1.Cells.Add("RECAUDADO AGOSTO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el RECAUDADO DE SEPTIEMBRE
                    Celda = Renglon1.Cells.Add("RECAUDADO SEPTIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el RECAUDADO DE OCTUBRE
                    Celda = Renglon1.Cells.Add("RECAUDADO OCTUBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el RECAUDADO DE NOVIEMBRE
                    Celda = Renglon1.Cells.Add("RECAUDADO NOVIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el RECAUDADO DE DICIEMBRE
                    Celda = Renglon1.Cells.Add("RECAUDADO DICIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    #endregion

                    foreach (DataRow Dr in Dt_Ing.Rows)
                    {
                        Renglon1 = Hoja1.Table.Rows.Add();
                        foreach (DataColumn Columna in Dt_Ing.Columns)
                        {
                            if (Columna is DataColumn)
                            {
                                if (Dr["Tipo"].ToString().Equals("Tot"))
                                {
                                    if (Columna.ColumnName.Equals("Concepto"))
                                    {
                                        Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Nombre_Total"));
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName != "Tipo" && Columna.ColumnName != "Elaboro" && Columna.ColumnName != "Dependencia"
                                            && Columna.ColumnName != "Anio" && Columna.ColumnName != "Gpo_Dependencia")
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.String, "Cant_Total"));
                                            }
                                            else
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.String, "Cant_Total_Neg"));
                                            }
                                        }
                                    }
                                }
                                else if (Dr["Tipo"].ToString().Equals("C"))
                                {
                                    if (Columna.ColumnName.Equals("Concepto"))
                                    {
                                        Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Nombre_Partida"));
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName != "Tipo" && Columna.ColumnName != "Elaboro" && Columna.ColumnName != "Dependencia"
                                            && Columna.ColumnName != "Anio" && Columna.ColumnName != "Gpo_Dependencia")
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.String, "Cant_Partida"));
                                            }
                                            else
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.String, "Cant_Partida_Neg"));
                                            }
                                        }
                                    }
                                }
                                else if (Dr["Tipo"].ToString().Equals("SC"))
                                {
                                    if (Columna.ColumnName.Equals("Concepto"))
                                    {
                                        Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Nombre_SC"));
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName != "Tipo" && Columna.ColumnName != "Elaboro" && Columna.ColumnName != "Dependencia"
                                            && Columna.ColumnName != "Anio" && Columna.ColumnName != "Gpo_Dependencia")
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.String, "Cant_SC"));
                                            }
                                            else
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.String, "Cant_SC_Neg"));
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (Columna.ColumnName.Equals("Concepto"))
                                    {
                                        Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Nombre_Totales"));
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName != "Tipo" && Columna.ColumnName != "Elaboro" && Columna.ColumnName != "Dependencia"
                                            && Columna.ColumnName != "Anio" && Columna.ColumnName != "Gpo_Dependencia")
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.String, "Cant_Totales"));
                                            }
                                            else
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.String, "Cant_Totales_Neg"));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                    Renglon1 = Hoja1.Table.Rows.Add();
                    Renglon1 = Hoja1.Table.Rows.Add();
                    Celda = Renglon1.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                    Celda.MergeAcross = 43;
                    Celda.StyleID = "Nombre_SC";
                    Renglon1 = Hoja1.Table.Rows.Add();
                    Celda = Renglon1.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                    Celda.MergeAcross = 43;
                    Celda.StyleID = "Nombre_SC";

                    Libro.Save(Server.MapPath(Ruta_Archivo.Trim()));
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al generar el reporte mensual del presupuesto de ingresos. Error[" + Ex.Message.ToString() + "]");
                }
                return Ruta_Archivo;
            }
        #endregion

        ///*******************************************************************************************************
        /// NOMBRE_FUNCIÓN: Exportar_Reporte
        /// DESCRIPCIÓN: Genera el reporte de Crystal con los datos proporcionados en el DataTable 
        /// PARÁMETROS:
        /// 		1. Ds_Reporte: Dataset con datos a imprimir
        /// 		2. Nombre_Reporte: Nombre del archivo de reporte .rpt
        /// 		3. Nombre_Archivo: Nombre del archivo a generar
        /// CREO: Roberto González Oseguera
        /// FECHA_CREO: 04-sep-2011
        /// MODIFICÓ: 
        /// FECHA_MODIFICÓ: 
        /// CAUSA_MODIFICACIÓN: 
        ///*******************************************************************************************************
        private void Exportar_Reporte(DataSet Ds_Reporte, String Nombre_Reporte, String Nombre_Archivo, 
            String Extension_Archivo, ExportFormatType Formato, String Nombre_Carpeta)
        {
            ReportDocument Reporte = new ReportDocument();
            String Ruta = Server.MapPath("../Rpt/" +Nombre_Carpeta + "/" + Nombre_Reporte);

            try
            {
                Reporte.Load(Ruta);
                Reporte.SetDataSource(Ds_Reporte);
            }
            catch
            {
                //Lbl_Mensaje_Error.Visible = true;
                //Lbl_Mensaje_Error.Text = "No se pudo cargar el reporte";
            }

            String Archivo_Reporte = Nombre_Archivo + "." + Extension_Archivo;  // formar el nombre del archivo a generar 
            try
            {
                ExportOptions Export_Options_Calculo = new ExportOptions();
                DiskFileDestinationOptions Disk_File_Destination_Options_Calculo = new DiskFileDestinationOptions();
                Disk_File_Destination_Options_Calculo.DiskFileName = Server.MapPath("../../Reporte/" + Archivo_Reporte);
                Export_Options_Calculo.ExportDestinationOptions = Disk_File_Destination_Options_Calculo;
                Export_Options_Calculo.ExportDestinationType = ExportDestinationType.DiskFile;
                Export_Options_Calculo.ExportFormatType = Formato;
                Reporte.Export(Export_Options_Calculo);
            }
            catch (Exception Ex)
            {
                //Lbl_Mensaje_Error.Visible = true;
                //Lbl_Mensaje_Error.Text = Ex.Message.ToString();
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Eliminar_Archivo
        ///DESCRIPCIÓN          : Metodo para eliminar un archivo
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 04/Diciembre/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private String Eliminar_Archivo(String Ruta) 
        {
            String Hecho = "NO";
            try
            {
                if (System.IO.File.Exists(Server.MapPath(Ruta)))
                {
                    System.IO.File.Delete(Server.MapPath(Ruta));
                }
            }
            catch (Exception Ex) 
            {
                throw new Exception("Error al Eliminar_Archivo Error[" + Ex.Message + "]");
            }

            return Hecho;
        }
    #endregion

    #region METODOS DETALLES REPORTES
        #region (INGRESOS)
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Det_Mov_Ing_Reporte
        ///DESCRIPCIÓN          : Metodo para obtener los detalles de los movimientos de ingresos y generar el reporte
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 03/Septiembre/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private String Obtener_Det_Mov_Ing_Reporte(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
            String R, String T, String Cl, String Con_Ing, String SubCon, String PP, String Tipo_Det)
        {
            Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
            String Json = ""; //aki almacenaremos el json de las fuentes de financiamiento
            DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
            DataTable Dt_Detalles = new DataTable(); // dt donde guardaremos los detalles
            DataRow Fila;

            try
            {
                Negocio.P_Anio = Anio;
                if (!String.IsNullOrEmpty(Fecha_Ini.Trim()))
                {
                    Negocio.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Formatear_Fecha(Fecha_Ini.Trim()));
                }
                else
                {
                    Negocio.P_Fecha_Inicial = String.Empty;
                }

                if (!String.IsNullOrEmpty(Fecha_Fin.Trim()))
                {
                    Negocio.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Formatear_Fecha(Fecha_Fin.Trim()));
                }
                else
                {
                    Negocio.P_Fecha_Final = String.Empty;
                }

                if (!String.IsNullOrEmpty(Mes.Trim()))
                {
                    Negocio.P_Periodo_Inicial = Obtener_Periodo(Mes, Anio, "Inicial");
                    Negocio.P_Periodo_Final = Obtener_Periodo(Mes, Anio, "Final");
                }
                else
                {
                    Negocio.P_Periodo_Inicial = String.Empty;
                    Negocio.P_Periodo_Final = String.Empty;
                }
                Negocio.P_Fte_Financiamiento = FF;
                Negocio.P_Rubro_ID = R;
                Negocio.P_Tipo_ID = T;
                Negocio.P_Clase_Ing_ID = Cl;
                Negocio.P_Concepto_Ing_ID = Con_Ing;
                Negocio.P_SubConcepto_ID = SubCon;
                Negocio.P_Programa_Ing_ID = PP;
                Negocio.P_Tipo_Detalle = Tipo_Det;

                Dt_Datos = Negocio.Consultar_Det_Modificacion_Ing();

                if (Dt_Datos != null && Dt_Datos.Rows.Count > 0)
                {
                    Dt_Detalles.Columns.Add("NO_SOLICITUD_ING", typeof(System.String));
                    Dt_Detalles.Columns.Add("CONCEPTO", typeof(System.String));
                    Dt_Detalles.Columns.Add("IMPORTE", typeof(System.Double));
                    Dt_Detalles.Columns.Add("FECHA", typeof(System.String));
                    Dt_Detalles.Columns.Add("CREO", typeof(System.String));
                    Dt_Detalles.Columns.Add("JUSTIFICACION", typeof(System.String));
                    Dt_Detalles.Columns.Add("TIPO", typeof(System.String));

                    foreach (DataRow Dr in Dt_Datos.Rows)
                    {
                        Fila = Dt_Detalles.NewRow();
                        Fila["NO_SOLICITUD_ING"] = "T" + String.Format("{0:00}", Dr["NO_MODIFICACION"]) + "-" + Dr["NO_MOVIMIENTO"].ToString().Trim() + "-" + Dr["ANIO"].ToString().Trim(); ;
                        Fila["CONCEPTO"] = Dr["CONCEPTO"].ToString().Trim();
                        Fila["IMPORTE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_TOTAL"].ToString()) ? "0" : Dr["IMPORTE_TOTAL"]);
                        Fila["FECHA"] = String.Format("{0:dd/MMM/yyyy}", Dr["FECHA_CREO"]);
                        Fila["CREO"] = Dr["USUARIO_CREO"].ToString().Trim();
                        Fila["JUSTIFICACION"] = Dr["JUSTIFICACION"].ToString().Trim();
                        Fila["TIPO"] = Dr[Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Movimiento].ToString().Trim();
                        Dt_Detalles.Rows.Add(Fila);
                    }

                    //GENERAMOS EL REPORTE
                    Json = Generar_Reporte_Detalles(Dt_Detalles, Anio, Tipo_Det);
                }

                return Json;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al obtener los detalles de ingresos Error[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Det_Recaudado_Ing_Reporte
        ///DESCRIPCIÓN          : Metodo para obtener los detalles de los movimientos de ingresos y generar el reporte
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 03/Septiembre/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private String Obtener_Det_Recaudado_Ing_Reporte(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
            String R, String T, String Cl, String Con_Ing, String SubCon, String PP, String Tipo_Det)
        {
            Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
            String Json = ""; //aki almacenaremos el json de las fuentes de financiamiento
            DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
            DataTable Dt = new DataTable(); // dt donde guardaremos los datos de los detalles
            DataRow Fila;

            try
            {
                Negocio.P_Anio = Anio;
                if (!String.IsNullOrEmpty(Fecha_Ini.Trim()))
                {
                    Negocio.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Formatear_Fecha(Fecha_Ini.Trim()));
                }
                else
                {
                    Negocio.P_Fecha_Inicial = String.Empty;
                }

                if (!String.IsNullOrEmpty(Fecha_Fin.Trim()))
                {
                    Negocio.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Formatear_Fecha(Fecha_Fin.Trim()));
                }
                else
                {
                    Negocio.P_Fecha_Final = String.Empty;
                }

                if (!String.IsNullOrEmpty(Mes.Trim()))
                {
                    Negocio.P_Periodo_Inicial = Obtener_Periodo(Mes, Anio, "Inicial");
                    Negocio.P_Periodo_Final = Obtener_Periodo(Mes, Anio, "Final");
                }
                else
                {
                    Negocio.P_Periodo_Inicial = String.Empty;
                    Negocio.P_Periodo_Final = String.Empty;
                }
                Negocio.P_Fte_Financiamiento = FF;
                Negocio.P_Rubro_ID = R;
                Negocio.P_Tipo_ID = T;
                Negocio.P_Clase_Ing_ID = Cl;
                Negocio.P_Concepto_Ing_ID = Con_Ing;
                Negocio.P_SubConcepto_ID = SubCon;
                Negocio.P_Programa_Ing_ID = PP;
                Negocio.P_Tipo_Detalle = Tipo_Det;

                Dt_Datos = Negocio.Consultar_Det_Recaudado_Ing();

                if (Dt_Datos != null && Dt_Datos.Rows.Count > 0)
                {
                    //creamos el pie de pagina del  grid
                    Dt.Columns.Add("NO", typeof(System.String));
                    Dt.Columns.Add("NO_POLIZA", typeof(System.String));
                    Dt.Columns.Add("CONCEPTO", typeof(System.String));
                    Dt.Columns.Add("IMPORTE", typeof(System.Double));
                    Dt.Columns.Add("FECHA", typeof(System.String));
                    Dt.Columns.Add("CREO", typeof(System.String));
                    Dt.Columns.Add("DESCRIPCION", typeof(System.String));
                    Dt.Columns.Add("REFERENCIA", typeof(System.String));

                    foreach (DataRow Dr in Dt_Datos.Rows)
                    {
                        Fila = Dt.NewRow();
                        Fila["NOO"] = Dr["NO_MOVIMIENTO"].ToString().Trim();
                        Fila["NO_POLIZA"] = Dr["NO_POLIZA"].ToString().Trim();
                        Fila["FECHA"] = String.Format("{0:dd/MMM/yyyy}", Dr["FECHA_INICIO"]);
                        Fila["DESCRIPCION"] = Dr["DESCRIPCION"].ToString().Trim();
                        Fila["REFERENCIA"] = Dr["REFERENCIA"].ToString().Trim();
                        Fila["IMPORTE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_TOTAL"].ToString()) ? "0" : Dr["IMPORTE_TOTAL"]);
                        Fila["CONCEPTO"] = Dr["CONCEPTO"].ToString().Trim();
                        Fila["CREO"] = Dr["USUARIO_CREO"].ToString().Trim();
                        Dt.Rows.Add(Fila);
                    }

                    Json = Generar_Reporte_Detalles(Dt, Anio, Tipo_Det);
                }

                return Json;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al obtener los detalles de ingresos Error[" + ex.Message + "]");
            }
        }
        #endregion

        #region (EGRESOS)
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Det_Mov_Ing_Reporte
        ///DESCRIPCIÓN          : Metodo para obtener los detalles de los movimientos de egresos y generar el reporte
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 03/Septiembre/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private String Obtener_Det_Mov_Egr_Reporte(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
            String AF, String PP, String UR, String Cap, String Con, String PG, String P, String Tipo_Det)
        {
            Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
            String Json = ""; //aki almacenaremos el json de las fuentes de financiamiento
            DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
            DataTable Dt = new DataTable();
            DataRow Fila;
            DataTable Dt_Temp = new DataTable();

            try
            {
                Negocio.P_Anio = Anio;
                if (!String.IsNullOrEmpty(Fecha_Ini.Trim()))
                {
                    Negocio.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Formatear_Fecha(Fecha_Ini.Trim()));
                }
                else
                {
                    Negocio.P_Fecha_Inicial = String.Empty;
                }

                if (!String.IsNullOrEmpty(Fecha_Fin.Trim()))
                {
                    Negocio.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Formatear_Fecha(Fecha_Fin.Trim()));
                }
                else
                {
                    Negocio.P_Fecha_Final = String.Empty;
                }

                if (!String.IsNullOrEmpty(Mes.Trim()))
                {
                    Negocio.P_Periodo_Inicial = Obtener_Periodo(Mes, Anio, "Inicial");
                    Negocio.P_Periodo_Final = Obtener_Periodo(Mes, Anio, "Final");
                }
                else
                {
                    Negocio.P_Periodo_Inicial = String.Empty;
                    Negocio.P_Periodo_Final = String.Empty;
                }
                Negocio.P_Fte_Financiamiento = FF;
                Negocio.P_Programa_ID = PP;
                Negocio.P_Area_Funcional_ID = AF;
                Negocio.P_Dependencia_ID = UR;
                Negocio.P_Capitulo_ID = Cap;
                Negocio.P_Concepto_ID = Con;
                Negocio.P_Partida_Generica_ID = PG;
                Negocio.P_Partida_ID = P;
                Negocio.P_Tipo_Detalle = Tipo_Det;

                Dt_Datos = Negocio.Consultar_Det_Modificacion_Egr();

                if (Dt_Datos != null && Dt_Datos.Rows.Count > 0)
                {
                    if (!String.IsNullOrEmpty(Tipo_Det.Trim()) && Tipo_Det.Trim().Equals("AMPLIACION"))
                    {
                        Dt_Temp.Columns.Add("NO_MODIFICACION", typeof(System.Decimal));
                        Dt_Temp.Columns.Add("NO_MOVIMIENTO", typeof(System.Decimal));
                        Dt_Temp.Columns.Add("ANIO", typeof(System.Decimal));
                        Dt_Temp.Columns.Add("IMPORTE_TOTAL");
                        Dt_Temp.Columns.Add("TIPO_OPERACION");
                        Dt_Temp.Columns.Add("JUSTIFICACION");
                        Dt_Temp.Columns.Add("FECHA_CREO", typeof(System.DateTime));
                        Dt_Temp.Columns.Add("USUARIO_CREO");
                        Dt_Temp.Columns.Add("FECHA_MODIFICO");
                        Dt_Temp.Columns.Add("USUARIO_MODIFICO");
                        Dt_Temp.Columns.Add("PARTIDA");
                        Dt_Temp.Columns.Add("NO_SOLICITUD_EGR");
                        Dt_Temp.Columns.Add("IMPORTE");
                        Dt_Temp.Columns.Add("FECHA_INICIO");
                        Dt_Temp.Columns.Add("FECHA_AUTORIZO");
                    }
                    else
                    {
                        Dt_Datos.Columns.Add("NO_SOLICITUD_EGR");
                        Dt_Datos.Columns.Add("IMPORTE");
                        Dt_Datos.Columns.Add("FECHA_INICIO");
                        Dt_Datos.Columns.Add("FECHA_AUTORIZO");
                    }

                    foreach (DataRow Dr in Dt_Datos.Rows)
                    {
                        if (!String.IsNullOrEmpty(Tipo_Det.Trim()) && Tipo_Det.Trim().Equals("AMPLIACION"))
                        {
                            if ((Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Origen") && Dr["TIPO_OPERACION"].ToString().Trim().Equals("AMPLIACION"))
                                || (Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Destino") && Dr["TIPO_OPERACION"].ToString().Trim().Equals("TRASPASO")))
                            {
                                Fila = Dt_Temp.NewRow();
                                Fila["NO_MODIFICACION"] = Dr["NO_MODIFICACION"];
                                Fila["NO_MOVIMIENTO"] = Dr["NO_MOVIMIENTO"];
                                Fila["ANIO"] = Dr["ANIO"];
                                Fila["IMPORTE_TOTAL"] = Dr["IMPORTE_TOTAL"].ToString().Trim();
                                Fila["TIPO_OPERACION"] = Dr["TIPO_OPERACION"].ToString().Trim();
                                Fila["JUSTIFICACION"] = Dr["JUSTIFICACION"].ToString().Trim();
                                Fila["FECHA_CREO"] = Dr["FECHA_CREO"];
                                Fila["USUARIO_CREO"] = Dr["USUARIO_CREO"].ToString().Trim();
                                Fila["FECHA_MODIFICO"] = Dr["FECHA_MODIFICO"].ToString().Trim();
                                Fila["USUARIO_MODIFICO"] = Dr["USUARIO_MODIFICO"].ToString().Trim();
                                Fila["PARTIDA"] = Dr["PARTIDA"].ToString().Trim();
                                Fila["NO_SOLICITUD_EGR"] = Dr["NO_MODIFICACION"] + "a. Modificación - No Solicitud " + Dr["NO_MOVIMIENTO"].ToString().Trim() + " - " + Dr["ANIO"].ToString().Trim();
                                Fila["IMPORTE"] = String.Format("{0:n}", Dr["IMPORTE_TOTAL"]);
                                Fila["FECHA_INICIO"] = String.Format("{0:dd/MMM/yyyy}", Dr["FECHA_CREO"]);
                                Fila["FECHA_AUTORIZO"] = String.Format("{0:dd/MMM/yyyy}", Dr["FECHA_MODIFICO"]);
                                Dt_Temp.Rows.Add(Fila);
                            }
                        }
                        else
                        {
                            if (Convert.ToInt64(String.IsNullOrEmpty(Dr["NO_MODIFICACION"].ToString()) ? "0" : Dr["NO_MODIFICACION"]) <= 0)
                            {
                                Dr["NO_SOLICITUD_EGR"] = "No Solicitud " + Dr["NO_MOVIMIENTO"].ToString().Trim() + " - " + Dr["ANIO"].ToString().Trim();
                            }
                            else
                            {
                                Dr["NO_SOLICITUD_EGR"] = Dr["NO_MODIFICACION"] + "a. Modificación - No Solicitud " + Dr["NO_MOVIMIENTO"].ToString().Trim() + " - " + Dr["ANIO"].ToString().Trim();
                            }
                            Dr["IMPORTE"] = String.Format("{0:n}", Dr["IMPORTE_TOTAL"]);
                            Dr["FECHA_INICIO"] = String.Format("{0:dd/MMM/yyyy}", Dr["FECHA_CREO"]);
                            Dr["FECHA_AUTORIZO"] = String.Format("{0:dd/MMM/yyyy}", Dr["FECHA_MODIFICO"]);
                        }
                    }

                    if (!String.IsNullOrEmpty(Tipo_Det.Trim()))
                    {
                        if (Tipo_Det.Trim() != "REDUCCION")
                        {
                            Dt_Datos = Dt_Temp;
                        }
                    }

                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            //creamos el pie de pagina del  grid
                            Dt.Columns.Add("NO_SOLICITUD", typeof(System.String));
                            Dt.Columns.Add("PARTIDA", typeof(System.String));
                            Dt.Columns.Add("IMPORTE", typeof(System.Double));
                            Dt.Columns.Add("FECHA", typeof(System.String));
                            Dt.Columns.Add("CREO", typeof(System.String));
                            Dt.Columns.Add("JUSTIFICACION", typeof(System.String));
                            Dt.Columns.Add("TIPO", typeof(System.String));

                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {
                                Fila = Dt.NewRow();
                                Fila["NO_SOLICITUD"] = Dr["NO_SOLICITUD_EGR"].ToString().Trim();
                                Fila["PARTIDA"] = Dr["PARTIDA"].ToString().Trim();
                                Fila["IMPORTE"] = Convert.ToDouble(string.IsNullOrEmpty(Dr["IMPORTE"].ToString()) ? "0" : Dr["IMPORTE"]);
                                Fila["FECHA"] = String.Format("{0:dd/MMM/yyyy}", Dr["FECHA_INICIO"]);
                                Fila["CREO"] = Dr["USUARIO_CREO"].ToString().Trim();
                                Fila["JUSTIFICACION"] = Dr["JUSTIFICACION"].ToString().Trim();
                                Fila["TIPO"] = Dr["TIPO_OPERACION"].ToString().Trim();
                                Dt.Rows.Add(Fila);
                            }

                            Json = Generar_Reporte_Detalles(Dt, Anio, Tipo_Det);
                        }
                    }
                }

                return Json;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al obtener los detalles de egresos Error[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Det_Mov_Con_Egr_Reporte
        ///DESCRIPCIÓN          : Metodo para obtener los detalles del devengado, pagado y comprometido de egresos y generar el reporte
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 03/Septiembre/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private String Obtener_Det_Mov_Con_Egr_Reporte(String Anio, String Mes, String Fecha_Ini, String Fecha_Fin, String FF,
            String AF, String PP, String UR, String Cap, String Con, String PG, String P, String Tipo_Det)
        {
            Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
            String Json = ""; //aki almacenaremos el json de las fuentes de financiamiento
            DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
            DataTable Dt = new DataTable();
            DataRow Fila;

            try
            {
                Negocio.P_Anio = Anio;
                if (!String.IsNullOrEmpty(Fecha_Ini.Trim()))
                {
                    Negocio.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Formatear_Fecha(Fecha_Ini.Trim()));
                }
                else
                {
                    Negocio.P_Fecha_Inicial = String.Empty;
                }

                if (!String.IsNullOrEmpty(Fecha_Fin.Trim()))
                {
                    Negocio.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Formatear_Fecha(Fecha_Fin.Trim()));
                }
                else
                {
                    Negocio.P_Fecha_Final = String.Empty;
                }

                if (!String.IsNullOrEmpty(Mes.Trim()))
                {
                    Negocio.P_Periodo_Inicial = Obtener_Periodo(Mes, Anio, "Inicial");
                    Negocio.P_Periodo_Final = Obtener_Periodo(Mes, Anio, "Final");
                }
                else
                {
                    Negocio.P_Periodo_Inicial = String.Empty;
                    Negocio.P_Periodo_Final = String.Empty;
                }
                Negocio.P_Fte_Financiamiento = FF;
                Negocio.P_Programa_ID = PP;
                Negocio.P_Area_Funcional_ID = AF;
                Negocio.P_Dependencia_ID = UR;
                Negocio.P_Capitulo_ID = Cap;
                Negocio.P_Concepto_ID = Con;
                Negocio.P_Partida_Generica_ID = PG;
                Negocio.P_Partida_ID = P;
                Negocio.P_Tipo_Detalle = Tipo_Det;

                if (!String.IsNullOrEmpty(Tipo_Det.Trim()))
                {
                    if (Tipo_Det.Trim().Equals(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido))
                    {
                        Dt_Datos = Negocio.Consultar_Det_Movimiento_Contable_Egr_Pre_Comprometido();
                    }
                    else if (Tipo_Det.Trim().Equals(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido))
                    {
                        Dt_Datos = Negocio.Consultar_Det_Movimiento_Contable_Egr_Comprometido();
                    }
                    else
                    {
                        Dt_Datos = Negocio.Consultar_Det_Movimiento_Contable_Egr_Dev();
                    }
                }

                if (Dt_Datos != null && Dt_Datos.Rows.Count > 0)
                {
                    //creamos el pie de pagina del  grid
                    Dt.Columns.Add("NO_RESERVA", typeof(System.String));
                    Dt.Columns.Add("NO_POLIZA", typeof(System.String));
                    Dt.Columns.Add("PARTIDA", typeof(System.String));
                    Dt.Columns.Add("IMPORTE", typeof(System.Double));
                    Dt.Columns.Add("FECHA", typeof(System.String));
                    Dt.Columns.Add("CREO", typeof(System.String));
                    Dt.Columns.Add("CONCEPTO", typeof(System.String));
                    Dt.Columns.Add("BENEFICIARIO", typeof(System.String));
                    Dt.Columns.Add("TIPO_SOLICITUD", typeof(System.String));

                    foreach (DataRow Dr in Dt_Datos.Rows)
                    {
                        Fila = Dt.NewRow();
                        Fila["NO_RESERVA"] = Dr["NO_RESERVA"].ToString().Trim();
                        Fila["NO_POLIZA"] = Dr["NO_POLIZA"].ToString().Trim();
                        Fila["IMPORTE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_MOV"].ToString()) ? "0" : Dr["IMPORTE_MOV"]);
                        Fila["FECHA"] = String.Format("{0:dd/MMM/yyyy}", Dr["FECHA_MOV"]);
                        Fila["CREO"] = Dr["USUARIO_MOV"].ToString().Trim();
                        Fila["CONCEPTO"] = Dr["CONCEPTO"].ToString().Trim();
                        Fila["BENEFICIARIO"] = Dr["BENEFICIARIO"].ToString().Trim();
                        Fila["TIPO_SOLICITUD"] = Dr["TIPO_SOLICITUD"].ToString().Trim();
                        Fila["PARTIDA"] = Dr["PARTIDA"].ToString().Trim();
                        Dt.Rows.Add(Fila);
                    }

                    Json = Generar_Reporte_Detalles(Dt, Anio, Tipo_Det);
                }

                return Json;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al obtener los detalles de egresos Error[" + ex.Message + "]");
            }
        }
        #endregion

        ///**************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Generar_Reporte_Detalles
        ///DESCRIPCIÓN          : metodo para generar el reporte en excel de los detalles del presupuesto
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 03/Septiembre/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///**************************************************************************************************
        public String Generar_Reporte_Detalles(DataTable Dt_Reporte, String Anio, String Tipo)
        {
            WorksheetCell Celda = new WorksheetCell();
            String Ruta_Archivo = "../../Reporte/Rpt_Detalles_Presupuestos_" + Tipo.Trim() + "_" + Anio.Trim() + "_" + Session.SessionID + ".xls     ";

            try
            {
                //Creamos el libro de Excel.
                CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
                //propiedades del libro
                Libro.Properties.Title = "Reporte_Detalle_Presupuesto";
                Libro.Properties.Created = DateTime.Now;
                Libro.Properties.Author = "JAPAMI";

                //Creamos una hoja que tendrá el libro.
                CarlosAg.ExcelXmlWriter.Worksheet Hoja = Libro.Worksheets.Add("Detalles");
                //Agregamos un renglón a la hoja de excel.
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon = Hoja.Table.Rows.Add();
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_Numero = Libro.Styles.Add("BodyStyle_Num");

                Estilo_Cabecera.Font.FontName = "Tahoma";
                Estilo_Cabecera.Font.Size = 8;
                Estilo_Cabecera.Font.Bold = true;
                Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera.Font.Color = "#FFFFFF";
                Estilo_Cabecera.Interior.Color = "#193d61";
                Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Contenido.Font.Bold = false;
                Estilo_Contenido.Font.FontName = "Courier New";
                Estilo_Contenido.Font.Size = 8;
                Estilo_Contenido.Font.Color = "#000000";
                Estilo_Contenido.Interior.Color = "#EEEEFF";
                Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Contenido.Alignment.Vertical = StyleVerticalAlignment.Justify;
                Estilo_Contenido.Alignment.WrapText = true;
                Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Dot, 1, "#CCCCCC");
                Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "#CCCCCC");
               
                Estilo_Contenido_Numero.Font.Bold = false;
                Estilo_Contenido_Numero.Font.FontName = "Courier New";
                Estilo_Contenido_Numero.Font.Size = 8;
                Estilo_Contenido_Numero.Font.Color = "#000000";
                Estilo_Contenido_Numero.Interior.Color = "#EEEEFF";
                Estilo_Contenido_Numero.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Contenido_Numero.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Contenido_Numero.Alignment.Vertical = StyleVerticalAlignment.Justify;
                Estilo_Contenido_Numero.Alignment.WrapText = true;
                Estilo_Contenido_Numero.Borders.Add(StylePosition.Bottom, LineStyleOption.Dot, 1, "#CCCCCC");
                Estilo_Contenido_Numero.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "#CCCCCC");
                Estilo_Contenido_Numero.NumberFormat = "#,###,##0.00";

                if (Dt_Reporte is System.Data.DataTable)
                {
                    if (Dt_Reporte.Rows.Count > 0)
                    {
                        foreach (System.Data.DataColumn COLUMNA in Dt_Reporte.Columns)
                        {
                            if (COLUMNA is System.Data.DataColumn)
                            {
                                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(COLUMNA.ColumnName, "HeaderStyle"));
                            }
                        }

                        foreach (System.Data.DataRow FILA in Dt_Reporte.Rows)
                        {
                            if (FILA is System.Data.DataRow)
                            {
                                Renglon = Hoja.Table.Rows.Add();

                                foreach (System.Data.DataColumn COLUMNA in Dt_Reporte.Columns)
                                {
                                    if (COLUMNA is System.Data.DataColumn)
                                    {
                                        if (COLUMNA.DataType.FullName.Equals("System.DateTime"))
                                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(string.Format("{0:dd/MM/yyyy}", FILA[COLUMNA.ColumnName]), DataType.String, "BodyStyle"));
                                        else if (COLUMNA.DataType.FullName.Equals("System.Decimal"))
                                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(string.Format("{0:n}", FILA[COLUMNA.ColumnName]), DataType.Number, "BodyStyle_Num"));
                                        else if (COLUMNA.DataType.FullName.Equals("System.Double"))
                                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(string.Format("{0:n}", FILA[COLUMNA.ColumnName]), DataType.Number, "BodyStyle_Num"));
                                        else
                                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(FILA[COLUMNA.ColumnName].ToString(), DataType.String, "BodyStyle"));
                                    }
                                }
                            }
                        }
                    }
                }

                Libro.Save(Server.MapPath(Ruta_Archivo.Trim()));
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar el reporte mensual del presupuesto de ingresos. Error[" + Ex.Message.ToString() + "]");
            }
            return Ruta_Archivo;
        }
    #endregion
    #endregion
}
