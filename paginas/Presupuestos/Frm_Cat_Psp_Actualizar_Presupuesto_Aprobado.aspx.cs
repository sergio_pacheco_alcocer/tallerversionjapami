﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.OleDb;
using AjaxControlToolkit;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Actualizar_Presupuesto_Aprobado.Negocios;

public partial class paginas_Presupuestos_Frm_Cat_Psp_Actualizar_Presupuesto_Aprobado : System.Web.UI.Page
{
    #region PAGE LOAD
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        if (!IsPostBack)
        {
            //Autorizar_Presupuesto_Inicio();
        }
    }
    #endregion

    #region  METODOS
    
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Mostrar_Ocultar_Error
    /// DESCRIPCION : Muestra u oculta los componentes de Error 
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 10/Septiembre/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public void Mostrar_Ocultar_Error(Boolean Estatus)
    {

        if (Estatus == false)
        {
            Lbl_Mensaje_Error.Text = String.Empty;
        }
        Lbl_Mensaje_Error.Visible = Estatus;
        Img_Error.Visible = Estatus;
    }
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Limpiar_Varibles_Sesion_Grid
    ///DESCRIPCIÓN          : Metodo limpia las varibles de sesion y limpia el grid
    ///PROPIEDADES          : 
    ///CREO                 : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO           : 12/Septiembre/2012 12:37p pm
    ///MODIFICO             : 
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...: 
    ///*********************************************************************************************************
    public void Limpiar_Varibles_Sesion_Grid() 
    {
        Session["Dt_Insertar"] = (DataTable) new DataTable();
        Session["Dt_Actualizar"] = (DataTable)new DataTable();
        Session["Dt_Presupuesto_Aprobado"] = (DataTable)new DataTable();
        Cargar_Grid_Presupuesto_Aprobado(0);
    }
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Validar_Columnas_Excel
    ///DESCRIPCIÓN          : Metodo que 
    ///PROPIEDADES          : Objeto DataTable, Del cual se va a nalizar las columnas que debe de conter el excel
    ///                       Para poder hacer la carga masiva
    ///CREO                 : Jennyfer Ivonne Ceja lemus
    ///FECHA_CREO           : 10/Septiembre/2012 12:34p pm
    ///MODIFICO             : 
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...: 
    ///*********************************************************************************************************
    public Boolean Validar_Columnas_Excel(DataTable Dt_Tabla)
    {
        Boolean Validacion = true;
        try
        {
            Mostrar_Ocultar_Error(false);
            if (!Dt_Tabla.Columns.Contains("FUENTE_FINANCIAMIENTO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna FUENTE_FINANCIAMIENTO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("AREA_FUNCIONAL"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna AREA_FUNCIONAL. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("UNIDAD_RESPONSABLE"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna UNIDAD_RESPONSABLE. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("PROGRAMA"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna PROGRAMA. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("CAPITULO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna CAPITULO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("PARTIDA"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna PARTIDA. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("SUBNIVEL"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna SUBNIVEL. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("ANIO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna ANIO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("APROBADO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna APROBADO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("AMPLIACION"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna AMPLIACION. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("REDUCCION"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna REDUCCION. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("MODIFICADO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna MODIFICADO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("DEVENGADO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna DEVENGADO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("EJERCIDO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna EJERCIDO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("PAGADO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna PAGADO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("PRE_COMPROMETIDO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna PRE_COMPROMETIDO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("COMPROMETIDO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna COMPROMETIDO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("DISPONIBLE"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna DISPONIBLE. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("SALDO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna SALDO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("ENERO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna ENERO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("FEBRERO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna FEBRERO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("MARZO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna MARZO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("ABRIL"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna ABRIL. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("MAYO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna MAYO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("JUNIO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna JUNIO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("JULIO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna JULIO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("AGOSTO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna AGOSTO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("SEPTIEMBRE"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna SEPTIEMBRE. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("OCTUBRE"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna OCTUBRE. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("NOVIEMBRE"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna NOVIEMBRE. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("DICIEMBRE"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna DICIEMBRE. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("TOTAL"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna TOTAL. <br />";
                Validacion = false;
            }
            return Validacion;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de validar los datos Error[" + ex.Message + "]");
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Datos_Obligatorios
    ///DESCRIPCIÓN: Valida que los datos obligatorios que se van a cargar no esten vacios 
    ///PARAMETROS: 
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 10/Septiembre/2010 18:06
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public Boolean Validar_Datos_Obligatorios()
    {
        DataTable Dt_Presupuesto = new DataTable();
        Dt_Presupuesto = (DataTable)Session["Dt_Presupuesto_Aprobado"];//Tabla que contiene todos los registros extraidos del documento de Excel
        Boolean Validar = true;
        try
        {
            Int32 Contador = 0;
            foreach (DataRow Registro in Dt_Presupuesto.Rows)
            {
                Contador++;
                if (String.IsNullOrEmpty(Registro["FUENTE_FINANCIAMIENTO"].ToString()))
                {
                    Lbl_Mensaje_Error.Text += "&nbsp; - El campo FUENTE_FINANCIAMIENTO del registro " + Contador + " no puede estar vacio <br>";
                    Validar = false;
                }
                if (String.IsNullOrEmpty(Registro["AREA_FUNCIONAL"].ToString()))
                {
                    Lbl_Mensaje_Error.Text += "&nbsp; - El campo AREA_FUNCIONAL del registro " + Contador + " no puede estar vacio <br>";
                    Validar = false;
                }
                if (String.IsNullOrEmpty(Registro["UNIDAD_RESPONSABLE"].ToString()))
                {
                    Lbl_Mensaje_Error.Text += "&nbsp; - El campo UNIDAD_RESPONSABLE del registro " + Contador + " no puede estar vacio <br>";
                    Validar = false;
                }
                if (String.IsNullOrEmpty(Registro["PROGRAMA"].ToString()))
                {
                    Lbl_Mensaje_Error.Text += "&nbsp; - El campo PROGRAMA del registro " + Contador + " no puede estar vacio <br>";
                    Validar = false;
                }
                if (String.IsNullOrEmpty(Registro["CAPITULO"].ToString()))
                {
                    Lbl_Mensaje_Error.Text += "&nbsp; - El campo CAPITULO del registro " + Contador + " no puede estar vacio <br>";
                    Validar = false;
                }
                if (String.IsNullOrEmpty(Registro["PARTIDA"].ToString()))
                {
                    Lbl_Mensaje_Error.Text += "&nbsp; - El campo PARTIDA del registro " + Contador + " no puede estar vacio <br>";
                    Validar = false;
                }
                //if (String.IsNullOrEmpty(Registro["SUBNIVEL"].ToString()))
                //{
                //    Lbl_Mensaje_Error.Text += "&nbsp; - El campo SUBNIVEL del registro " + Contador + " no puede estar vacio <br>";
                //    Validar = false;
                //}
                if (String.IsNullOrEmpty(Registro["ANIO"].ToString()))
                {
                    Lbl_Mensaje_Error.Text += "&nbsp; - El campo ANIO del registro " + Contador + " no puede estar vacio <br>";
                    Validar = false;
                }
                if (String.IsNullOrEmpty(Registro["APROBADO"].ToString()))
                {
                    Lbl_Mensaje_Error.Text += "&nbsp; - El campo APROBADO del registro " + Contador + " no puede estar vacio <br>";
                    Validar = false;
                }
                if (String.IsNullOrEmpty(Registro["DISPONIBLE"].ToString()))
                {
                    Lbl_Mensaje_Error.Text += "&nbsp; - El campo DISPONIBLE del registro " + Contador + " no puede estar vacio <br>";
                    Validar = false;
                }
                if (String.IsNullOrEmpty(Registro["SALDO"].ToString()))
                {
                    Lbl_Mensaje_Error.Text += "&nbsp; - El campo SALDO del registro " + Contador + " no puede estar vacio <br>";
                    Validar = false;
                }
                if (String.IsNullOrEmpty(Registro["ENERO"].ToString()))
                {
                    Lbl_Mensaje_Error.Text += "&nbsp; - El campo ENERO del registro " + Contador + " no puede estar vacio <br>";
                    Validar = false;
                }
                if (String.IsNullOrEmpty(Registro["FEBRERO"].ToString()))
                {
                    Lbl_Mensaje_Error.Text += "&nbsp; - El campo FEBRERO del registro " + Contador + " no puede estar vacio <br>";
                    Validar = false;
                }
                if (String.IsNullOrEmpty(Registro["MARZO"].ToString()))
                {
                    Lbl_Mensaje_Error.Text += "&nbsp; - El campo MARZO del registro " + Contador + " no puede estar vacio <br>";
                    Validar = false;
                }
                if (String.IsNullOrEmpty(Registro["ABRIL"].ToString()))
                {
                    Lbl_Mensaje_Error.Text += "&nbsp; - El campo ABRIL del registro " + Contador + " no puede estar vacio <br>";
                    Validar = false;
                }
                if (String.IsNullOrEmpty(Registro["MAYO"].ToString()))
                {
                    Lbl_Mensaje_Error.Text += "&nbsp; - El campo MAYO del registro " + Contador + " no puede estar vacio <br>";
                    Validar = false;
                }
                if (String.IsNullOrEmpty(Registro["JUNIO"].ToString()))
                {
                    Lbl_Mensaje_Error.Text += "&nbsp; - El campo JUNIO del registro " + Contador + " no puede estar vacio <br>";
                    Validar = false;
                }
                if (String.IsNullOrEmpty(Registro["JULIO"].ToString()))
                {
                    Lbl_Mensaje_Error.Text += "&nbsp; - El campo JULIO del registro " + Contador + " no puede estar vacio <br>";
                    Validar = false;
                }
                if (String.IsNullOrEmpty(Registro["AGOSTO"].ToString()))
                {
                    Lbl_Mensaje_Error.Text += "&nbsp; - El campo AGOSTO del registro " + Contador + " no puede estar vacio <br>";
                    Validar = false;
                }
                if (String.IsNullOrEmpty(Registro["SEPTIEMBRE"].ToString()))
                {
                    Lbl_Mensaje_Error.Text += "&nbsp; - El campo SEPTIEMBRE del registro " + Contador + " no puede estar vacio <br>";
                    Validar = false;
                }
                if (String.IsNullOrEmpty(Registro["OCTUBRE"].ToString()))
                {
                    Lbl_Mensaje_Error.Text += "&nbsp; - El campo OCTUBRE del registro " + Contador + " no puede estar vacio <br>";
                    Validar = false;
                }
                if (String.IsNullOrEmpty(Registro["NOVIEMBRE"].ToString()))
                {
                    Lbl_Mensaje_Error.Text += "&nbsp; - El campo NOVIEMBRE del registro " + Contador + " no puede estar vacio <br>";
                    Validar = false;
                }
                if (String.IsNullOrEmpty(Registro["DICIEMBRE"].ToString()))
                {
                    Lbl_Mensaje_Error.Text += "&nbsp; - El campo DICIEMBRE del registro " + Contador + " no puede estar vacio <br>";
                    Validar = false;
                }
                if (String.IsNullOrEmpty(Registro["TOTAL"].ToString()))
                {
                    Lbl_Mensaje_Error.Text += "&nbsp; - El campo TOTAL del registro " + Contador + " no puede estar vacio <br>";
                    Validar = false;
                }

            }
            return Validar;
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Error al validar Datos obligatorios[" + Ex.Message + "]");
        }

    }
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Presupuesto_Aprobado
    ///DESCRIPCIÓN          : Metodo para crear las columnas de las Data Table que guardaran los datos  a insertar y a actualizar
    ///PROPIEDADES          :
    ///CREO                 : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO           : 18/Septiembre/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Crear_Dt_Presupuesto_Aprobado()
    {
        DataTable Dt_Insertar = new DataTable();
        DataTable Dt_Actualizar = new DataTable();
        try
        {
            Dt_Insertar.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("AREA_FUNCIONAL_ID", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("PROYECTO_PTOGRAMA_ID", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("SUBNIVEL_ID", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("ANIO", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("NIVEL_PRESUPUESTAL", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("APROBADO", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("AMPLIACION", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("REDUCCION", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("MODIFICADO", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("DEVENGADO", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("EJERCIDO", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("PAGADO", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("PRE_COMPROMETIDO", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("COMPROMETIDO", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("DISPONIBLE", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("SALDO", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("ENERO", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("FEBRERO", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("MARZO", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("ABRIL", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("MAYO", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("JUNIO", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("JULIO", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("AGOSTO", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("SEPTIEMBRE", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("OCTUBRE", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("NOVIEMBRE", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("DICIEMBRE", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("TOTAL", System.Type.GetType("System.String"));


            Dt_Actualizar.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("AREA_FUNCIONAL_ID", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("PROYECTO_PTOGRAMA_ID", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("SUBNIVEL_ID", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("ANIO", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("NIVEL_PRESUPUESTAL", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("APROBADO", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("AMPLIACION", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("REDUCCION", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("MODIFICADO", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("DEVENGADO", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("EJERCIDO", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("PAGADO", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("PRE_COMPROMETIDO", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("COMPROMETIDO", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("DISPONIBLE", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("SALDO", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("ENERO", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("FEBRERO", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("MARZO", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("ABRIL", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("MAYO", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("JUNIO", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("JULIO", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("AGOSTO", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("SEPTIEMBRE", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("OCTUBRE", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("NOVIEMBRE", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("DICIEMBRE", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("TOTAL", System.Type.GetType("System.String"));

       

            Session["Dt_Insertar"] = (DataTable)Dt_Insertar;
            Session["Dt_Actualizar"] = (DataTable)Dt_Actualizar;

        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de crear las columnas de la tabla Error[" + ex.Message + "]");
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cargar_Grid
    ///DESCRIPCIÓN       : Realizar la consulta y llenar el grido con estos datos
    ///PARAMETROS        : 
    ///CREO              : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO        : 19/09/2012 10:55 am
    ///MODIFICO          : 
    ///FECHA_MODIFICO    : 
    ///CAUSA_MODIFICACION: 
    ///*******************************************************************************
    private void Cargar_Grid_Presupuesto_Aprobado(int Page_Index)
    {
        DataTable Dt_Presupuesto = new DataTable();
        try
        {
            Dt_Presupuesto = (DataTable)Session["Dt_Presupuesto_Aprobado"];
            if (Dt_Presupuesto != null && Dt_Presupuesto.Rows.Count > 0)
            {
                Grid_Presupuesto.PageIndex = Page_Index;
                Grid_Presupuesto.DataSource = Dt_Presupuesto;
                Grid_Presupuesto.DataBind();
            }
            else 
            {
                Dt_Presupuesto = new DataTable();
                Grid_Presupuesto.PageIndex = Page_Index;
                Grid_Presupuesto.DataSource = Dt_Presupuesto;
                Grid_Presupuesto.DataBind();
            }

        }
        catch (Exception Ex)
        {
            throw new Exception("Error al tratar de llena la grid Error[" + Ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Agregar_Filas_Dt_Proveedores
    ///DESCRIPCIÓN: Metodo que identifica a los proveedores que se van a actualizar y alos proveedores
    ///             que se van a  insertar 
    ///PARAMETROS: 
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 10/Septiembre/2010 17:45
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************

    public Boolean Agregar_Filas_Dt_Presupuesto()
    {
        Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Negocio Act_Presupuesto_Negocio = new Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Negocio(); // Clase de negocios

        DataTable Dt_Presupuestos = new DataTable();
        Dt_Presupuestos = (DataTable)Session["Dt_Presupuesto_Aprobado"];//Tabla que contiene todos los registros extraidos del documento de Excel

        DataTable Dt_Insertar = new DataTable();
        Dt_Insertar = (DataTable)Session["Dt_Insertar"];//Tabla que contendra los registros que se van a Insertar

        DataTable Dt_Actualizar = new DataTable();
        Dt_Actualizar = (DataTable)Session["Dt_Actualizar"];//Tabla que contendra los registros que se van a acutliazar

        DataTable Dt_Usos_Multiples = new DataTable();

        DataRow Fila;
        Mostrar_Ocultar_Error(false);
        Boolean Validacion = true;
        Int32 Contador = 0;
        String Conjunto_Partidas = String.Empty;
        try
        {
            if (Dt_Presupuestos is DataTable)
            {
                foreach (DataRow Registro in Dt_Presupuestos.Rows)
                {
                    if (Validacion == true)
                    {
                        Contador++;
    //                    Act_Prov_Negocio.P_Proveedor_ID = String.Format("{0:0000000000}", Convert.ToInt32(Registro["NO_PADRON_PROVEEDOR"].ToString()));
                        Act_Presupuesto_Negocio.P_Clave_Dependencia = Registro["UNIDAD_RESPONSABLE"].ToString();
                        Act_Presupuesto_Negocio.P_Clave_Fuente_Financiamiento = Registro["FUENTE_FINANCIAMIENTO"].ToString();
                        Act_Presupuesto_Negocio.P_Clave_Programa = Registro["PROGRAMA"].ToString();
                        Act_Presupuesto_Negocio.P_Clave_Partidaa = Registro["PARTIDA"].ToString();
                        Act_Presupuesto_Negocio.P_Anio = Registro["ANIO"].ToString();
                        if (!String.IsNullOrEmpty(Registro["SUBNIVEL"].ToString()))
                        Act_Presupuesto_Negocio.P_Clave_Subnivel = Registro["SUBNIVEL"].ToString();

                        Dt_Usos_Multiples = Act_Presupuesto_Negocio.Consultar_Presupuesto_Aprobado();
                        if (Dt_Usos_Multiples != null && Dt_Usos_Multiples.Rows.Count > 0) //Si existe el proveedor
                        {
                            Fila = Dt_Actualizar.NewRow(); //Crea un nuevo registro a la tabla Actualizar
                        }
                        else
                        {
                            Fila = Dt_Insertar.NewRow(); //Crea un nuevo registro a la tabla insertar
                        }
                        //Validar si la fuente de financiamiento esta registrada en la base de datos
                        Dt_Usos_Multiples = Act_Presupuesto_Negocio.Consultar_Fte_Financiamiento();
                        if (Dt_Usos_Multiples != null && Dt_Usos_Multiples.Rows.Count > 0)
                        {
                            Fila["FUENTE_FINANCIAMIENTO_ID"] = Dt_Usos_Multiples.Rows[0][Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID].ToString();
                        }else
                        {
                            Validacion = false;
                            Lbl_Mensaje_Error.Text += "&nbsp; - Revisar el archivo, la fuente de financiamiento del registro " + Contador + " No se encuentra registrado en la base de datos <br>";
                        }
                        //Validar si la unidad responsable esta registrada en la base de datos y si el area funcionales el que le corresponde
                        Act_Presupuesto_Negocio.P_Clave_Dependencia = Registro["UNIDAD_RESPONSABLE"].ToString();
                        Dt_Usos_Multiples = Act_Presupuesto_Negocio.Consultar_Dependencia();
                        if (Dt_Usos_Multiples != null && Dt_Usos_Multiples.Rows.Count > 0)
                        {
                            if (Dt_Usos_Multiples.Rows[0]["CLAVE_AREA_FUNCIONAL"].ToString().Equals(Registro["AREA_FUNCIONAL"].ToString()))
                            {
                                Fila["AREA_FUNCIONAL_ID"] = Dt_Usos_Multiples.Rows[0][Cat_Dependencias.Campo_Area_Funcional_ID].ToString();
                                Fila["DEPENDENCIA_ID"] = Dt_Usos_Multiples.Rows[0][Cat_Dependencias.Campo_Dependencia_ID].ToString();
                            }
                            else
                            {
                                Validacion = false;
                                Lbl_Mensaje_Error.Text += "&nbsp; - Revisar el archivo, el área funcional del registro " + Contador + " No pertenece a la Unidad Responsable <br>";
                            }
                        }
                        else 
                        {
                            Validacion = false;
                           Lbl_Mensaje_Error.Text += "&nbsp; - Revisar el archivo, el área funcional del registro " + Contador + " No pertenece a la Unidad Responsable <br>";
                        }
                        //Validar si el programa esta registrado en la base de datos
                        Act_Presupuesto_Negocio.P_Clave_Programa = Registro["PROGRAMA"].ToString();
                        Dt_Usos_Multiples = Act_Presupuesto_Negocio.Consultar_Programa();
                        if (Dt_Usos_Multiples != null && Dt_Usos_Multiples.Rows.Count > 0)
                        {
                            Act_Presupuesto_Negocio.P_Dependencia_ID = Fila["DEPENDENCIA_ID"].ToString();
                            Act_Presupuesto_Negocio.P_Programa_ID = Dt_Usos_Multiples.Rows[0][Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id].ToString();
                            Dt_Usos_Multiples = Act_Presupuesto_Negocio.Consultar_Relacion_Programa_Dependencia();
                            //verifica que el programa este relacionado con la unidad responsable
                            if (Dt_Usos_Multiples != null && Dt_Usos_Multiples.Rows.Count > 0)
                            {
                                Fila["PROYECTO_PTOGRAMA_ID"] = Dt_Usos_Multiples.Rows[0][Cat_SAP_Det_Prog_Dependencia.Campo_Proyecto_Programa_ID].ToString();
                            }
                            else
                            {
                                Validacion = false;
                                Lbl_Mensaje_Error.Text += "&nbsp; - Revisar el archivo, el programa del registro " + Contador + " No tiene relacion con la Unidad Responsable <br>";
                            }
                        }
                        else 
                        {
                            Validacion = false;
                            Lbl_Mensaje_Error.Text += "&nbsp; - Revisar el archivo, el programa del registro " + Contador + " No se encuentra registrado <br>";
                        }
                        //Validar que el capitulo este resgistrado en la base de datos
                        Act_Presupuesto_Negocio.P_Clave_Capitulo = Registro["CAPITULO"].ToString();
                        Dt_Usos_Multiples = Act_Presupuesto_Negocio.Consultar_Capitulo();
                        if (Dt_Usos_Multiples != null && Dt_Usos_Multiples.Rows.Count > 0)
                        {
                            Fila["CAPITULO_ID"] = Dt_Usos_Multiples.Rows[0][Cat_SAP_Capitulos.Campo_Capitulo_ID].ToString();
                        }
                        else
                        {
                            Validacion = false;
                            Lbl_Mensaje_Error.Text += "&nbsp; - Revisar el archivo, el capitulo del registro " + Contador + "puede estar inactivo o no esta registrado en el sistema <br>";
                        }
                        //Validar si la partida se encuentra registrada en la base de datos
                        Act_Presupuesto_Negocio.P_Clave_Partidaa = Registro["PARTIDA"].ToString();
                        Dt_Usos_Multiples = Act_Presupuesto_Negocio.Consultar_Partida();
                        if (Dt_Usos_Multiples != null && Dt_Usos_Multiples.Rows.Count > 0)
                        {
                            Act_Presupuesto_Negocio.P_Clave_Capitulo = Registro["CAPITULO"].ToString();
                            Dt_Usos_Multiples = Act_Presupuesto_Negocio.Consultar_Relacion_Partida_Capitulo();
                            if (Dt_Usos_Multiples != null && Dt_Usos_Multiples.Rows.Count > 0)
                            {
                                Fila["PARTIDA_ID"] = Dt_Usos_Multiples.Rows[0][Cat_Sap_Partidas_Especificas.Campo_Partida_ID].ToString();
                            }
                            else
                            {
                                Validacion = false;
                                Lbl_Mensaje_Error.Text += "&nbsp; - Revisar el archivo, la partida del registro " + Contador + " no pertenece al capitulo de este registro <br>";
                            }
                        }
                        else
                        {
                            Validacion = false;
                            Lbl_Mensaje_Error.Text += "&nbsp; - Revisar el archivo, la partida del registro " + Contador + "puede estar inactiva o no esta registrada en el sistema <br>";
                        }
                        if (!String.IsNullOrEmpty(Registro["SUBNIVEL"].ToString()))
                        {
                            Act_Presupuesto_Negocio.P_Clave_Fuente_Financiamiento = Registro["FUENTE_FINANCIAMIENTO"].ToString();
                            Act_Presupuesto_Negocio.P_Clave_Area_Funcional = Registro["AREA_FUNCIONAL"].ToString();
                            Act_Presupuesto_Negocio.P_Clave_Dependencia = Registro["UNIDAD_RESPONSABLE"].ToString();
                            Act_Presupuesto_Negocio.P_Clave_Programa = Registro["PROGRAMA"].ToString();
                            Act_Presupuesto_Negocio.P_Clave_Partidaa = Registro["PARTIDA"].ToString();
                            Act_Presupuesto_Negocio.P_Clave_Subnivel = Registro["SUBNIVEL"].ToString();
                            Dt_Usos_Multiples = Act_Presupuesto_Negocio.Consultar_Subnivel_Presupuestal();
                            if (Dt_Usos_Multiples != null && Dt_Usos_Multiples.Rows.Count > 0)
                            {
                                Fila["NIVEL_PRESUPUESTAL"] = Registro["SUBNIVEL"].ToString();
                                Fila["SUBNIVEL_ID"] = Dt_Usos_Multiples.Rows[0][Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal_ID].ToString();
                            }
                            else
                            {
                                Validacion = false;
                                Lbl_Mensaje_Error.Text += "&nbsp; - Revisar el archivo, la Subnivel del registro " + Contador + "puede no estar registrada en el sistema <br>";
                            }
                        }
                        else 
                        {
                            Act_Presupuesto_Negocio.P_Clave_Fuente_Financiamiento = Registro["FUENTE_FINANCIAMIENTO"].ToString();
                            Act_Presupuesto_Negocio.P_Clave_Area_Funcional = Registro["AREA_FUNCIONAL"].ToString();
                            Act_Presupuesto_Negocio.P_Clave_Dependencia = Registro["UNIDAD_RESPONSABLE"].ToString();
                            Act_Presupuesto_Negocio.P_Clave_Programa = Registro["PROGRAMA"].ToString();
                            Act_Presupuesto_Negocio.P_Clave_Partidaa = Registro["PARTIDA"].ToString();
                            Act_Presupuesto_Negocio.P_Clave_Subnivel = String.Empty;
                            Dt_Usos_Multiples = Act_Presupuesto_Negocio.Consultar_Subnivel_Presupuestal();
                            if (Dt_Usos_Multiples != null && Dt_Usos_Multiples.Rows.Count < 0)
                            {
                                Validacion = false;
                                Lbl_Mensaje_Error.Text += "&nbsp; - Revisar el archivo, el subnivel presupuestal del registro " + Contador + " no puede estar vacio <br>";
                            }
                        
                        }
                        Fila["ANIO"] = Registro["ANIO"].ToString();

                        Fila["APROBADO"] = Convert.ToDouble(String.IsNullOrEmpty(Registro["APROBADO"].ToString()) ? "0" : Registro["APROBADO"].ToString()).ToString();
                        Fila["AMPLIACION"] = Convert.ToDouble(String.IsNullOrEmpty(Registro["AMPLIACION"].ToString()) ? "0" : Registro["AMPLIACION"].ToString()).ToString();
                        Fila["REDUCCION"] = Convert.ToDouble(String.IsNullOrEmpty(Registro["REDUCCION"].ToString()) ? "0" : Registro["REDUCCION"].ToString()).ToString();
                        Fila["MODIFICADO"] = Convert.ToDouble(String.IsNullOrEmpty(Registro["MODIFICADO"].ToString()) ? "0" : Registro["MODIFICADO"].ToString());

                        Fila["DEVENGADO"] = Convert.ToDouble(String.IsNullOrEmpty(Registro["DEVENGADO"].ToString()) ? "0" : Registro["DEVENGADO"].ToString()).ToString().ToString();
                        Fila["EJERCIDO"] = Convert.ToDouble(String.IsNullOrEmpty(Registro["EJERCIDO"].ToString()) ? "0" : Registro["EJERCIDO"].ToString()).ToString();
                        Fila["PAGADO"] = Convert.ToDouble(String.IsNullOrEmpty(Registro["PAGADO"].ToString()) ? "0" : Registro["PAGADO"].ToString()).ToString();

                        Fila["PRE_COMPROMETIDO"] = Convert.ToDouble(String.IsNullOrEmpty(Registro["PRE_COMPROMETIDO"].ToString()) ? "0" : Registro["PRE_COMPROMETIDO"].ToString()).ToString();
                        Fila["COMPROMETIDO"] = Convert.ToDouble(String.IsNullOrEmpty(Registro["COMPROMETIDO"].ToString()) ? "0" : Registro["COMPROMETIDO"].ToString()).ToString();
                        Fila["DISPONIBLE"] = Convert.ToDouble(String.IsNullOrEmpty(Registro["DISPONIBLE"].ToString()) ? "0" : Registro["DISPONIBLE"].ToString()).ToString();
                        Fila["SALDO"] = Convert.ToDouble(String.IsNullOrEmpty(Registro["SALDO"].ToString()) ? "0" : Registro["SALDO"].ToString()).ToString();

                        Fila["ENERO"] = Convert.ToDouble(String.IsNullOrEmpty(Registro["ENERO"].ToString()) ? "0" : Registro["ENERO"].ToString()).ToString();
                        Fila["FEBRERO"] = Convert.ToDouble(String.IsNullOrEmpty(Registro["FEBRERO"].ToString()) ? "0" : Registro["FEBRERO"].ToString()).ToString();
                        Fila["MARZO"] = Convert.ToDouble(String.IsNullOrEmpty(Registro["MARZO"].ToString()) ? "0" : Registro["MARZO"].ToString()).ToString();
                        Fila["ABRIL"] = Convert.ToDouble(String.IsNullOrEmpty(Registro["ABRIL"].ToString()) ? "0" : Registro["ABRIL"].ToString()).ToString();
                        Fila["MAYO"] = Convert.ToDouble(String.IsNullOrEmpty(Registro["MAYO"].ToString()) ? "0" : Registro["MAYO"].ToString()).ToString();
                        Fila["JUNIO"] = Convert.ToDouble(String.IsNullOrEmpty(Registro["JUNIO"].ToString()) ? "0" : Registro["JUNIO"].ToString()).ToString();
                        Fila["JULIO"] = Convert.ToDouble(String.IsNullOrEmpty(Registro["JULIO"].ToString()) ? "0" : Registro["JULIO"].ToString()).ToString();
                        Fila["AGOSTO"] = Convert.ToDouble(String.IsNullOrEmpty(Registro["AGOSTO"].ToString()) ? "0" : Registro["AGOSTO"].ToString()).ToString();
                        Fila["SEPTIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Registro["SEPTIEMBRE"].ToString()) ? "0" : Registro["SEPTIEMBRE"].ToString()).ToString();
                        Fila["OCTUBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Registro["OCTUBRE"].ToString()) ? "0" : Registro["OCTUBRE"].ToString()).ToString();
                        Fila["NOVIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Registro["NOVIEMBRE"].ToString()) ? "0" : Registro["NOVIEMBRE"].ToString()).ToString();
                        Fila["DICIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Registro["DICIEMBRE"].ToString()) ? "0" : Registro["DICIEMBRE"].ToString()).ToString();
                        Fila["TOTAL"] = Convert.ToDouble(String.IsNullOrEmpty(Registro["TOTAL"].ToString()) ? "0" : Registro["TOTAL"].ToString()).ToString();
                        if (Validacion == true) 
                        {
                            Dt_Usos_Multiples = Act_Presupuesto_Negocio.Consultar_Presupuesto_Aprobado();
                            if (Dt_Usos_Multiples != null && Dt_Usos_Multiples.Rows.Count > 0) //Si existe la partida presupuestal
                            {
                                Dt_Actualizar.Rows.Add(Fila);
                            }
                            else
                            {
                                Dt_Insertar.Rows.Add(Fila);
                            }
                        }
                        

                    }//Fin del if Validacion ==  true
                    else { break; }
                }//Fin del for each
                if (Validacion == true)
                {
                    Session["Dt_Insertar"] = (DataTable)Dt_Insertar;
                    Session["Dt_Actualizar"] = (DataTable)Dt_Actualizar;
                }
            }
            return Validacion;

        }
        catch (FormatException e)
        {
            Validacion = false;
            Lbl_Mensaje_Error.Text += Lbl_Mensaje_Error.Text += "&nbsp; - El campo NO_PADRON_PROVEEDOR del registro " + Contador + " debe ser un número <br>";
            throw new Exception("Error al tratar de convertir el NO_PADRON_PROVEEDOR a número  Error[" + e.Message + "]");
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de crear las columnas de la tabla Error[" + ex.Message + "]");
        }
    }
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Convertir_Datos_Proveedores
    ///DESCRIPCIÓN          : Metodo que Actualiza o inserta los proveedores segun corresponda
    ///PROPIEDADES          : 
    ///CREO                 : Jennyfer Ivonne Ceja lemus
    ///FECHA_CREO           : 10/Septiembre/2012 12:03 p pm
    ///MODIFICO             : 
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...: 
    ///*********************************************************************************************************
    ///*******************************************************************************
    private void Convertir_Datos_Presuouesto_Aprobado(DataTable Dt_Proveedores_Detalles)
    {

        Session["Dt_Presupuesto_Aprobado"] = (DataTable)Dt_Proveedores_Detalles; //Guarda los datos en la varible de sesion;

        try
        {
            Mostrar_Ocultar_Error(false);
            if (Dt_Proveedores_Detalles.Rows.Count > 0)
            {
                if (Validar_Datos_Obligatorios())
                {
                    Crear_Dt_Presupuesto_Aprobado();
                    if (Agregar_Filas_Dt_Presupuesto())
                    {
                    Cargar_Grid_Presupuesto_Aprobado(0);
                    }
                    else
                    {
                        //Limpiar_Varibles_Sesion_Grid();
                        Mostrar_Ocultar_Error(true);
                    }
                }
                else
                {
                    //Limpiar_Varibles_Sesion_Grid();
                    Mostrar_Ocultar_Error(true);
                }

            }
            else
            {
                //Limpiar_Varibles_Sesion_Grid();
                Lbl_Mensaje_Error.Text += "&nbsp; - El arcivo de excel no contiene informacion <br />";
                Mostrar_Ocultar_Error(true);
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Convertir_Datos_Proveedores " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Interpretar_Excel
    /// DESCRIPCION : Interpreta los datos contenidos por el archivo de Excel.
    /// PARAMETROS  : 
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 13/Octubre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Interpretar_Excel()
    {
        OleDbConnection Conexion = new OleDbConnection();
        OleDbCommand Comando = new OleDbCommand();
        OleDbDataAdapter Adaptador = new OleDbDataAdapter();
        DataSet Ds_Informacion = new DataSet();
        String Query = String.Empty;
        DataSet Ds_Consulta = new DataSet();//Definimos el DataSet donde insertaremos los datos que leemos del excel
        String Tabla = "";
        String Rta = "";
        DataTable Dt_Presupuesto_Aprobado = null;    //Almacena los detalles de la poliza
        String Nombre_Archivo = String.Empty; //variable para el nombre del archivo

        //Colocar el nombre del archivo
        Nombre_Archivo = HttpContext.Current.Session["Nombre_Archivo_Excel"].ToString().Trim();

        int Caracter_Inicio = 0;    //Posicion del caracter de inicio del nombre del archivo.

        for (int Cont_Caracter = 0; Cont_Caracter < Nombre_Archivo.Length; Cont_Caracter++)
        {
            if (Nombre_Archivo[Cont_Caracter] == Convert.ToChar(92))
            {
                Caracter_Inicio = Cont_Caracter + 1;
            }
        }

        Rta = "C:\\Archivos\\" + Nombre_Archivo.Substring(Caracter_Inicio, Nombre_Archivo.Length - Caracter_Inicio);

        try
        {
            if (Rta.Contains(".xlsx"))       // Formar la cadena de conexion si el archivo es Exceml xml
            {
                Conexion.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                        "Data Source=" + Rta + ";" +
                        "Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";//Almacena si el archivo es 2007
            }
            else if (Rta.Contains(".xls"))   // Formar la cadena de conexion si el archivo es Exceml binario
            {
                Conexion.ConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                        "Data Source=" + Rta + ";" +
                        "Extended Properties=Excel 8.0;"; //Almacena si el archivo es 2003
            }
            // Get the name of the first worksheet:
            Conexion.Open();
            DataTable dbSchema = Conexion.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            if (dbSchema == null || dbSchema.Rows.Count < 1)
            {
                throw new Exception("Error: Could not determine the name of the first worksheet.");
            }
            String FirstSheetName = dbSchema.Rows[0]["TABLE_NAME"].ToString(); //Obtiene el nombre de la primera Hoja

            Tabla = "SELECT * FROM [" + FirstSheetName + "]";
            OleDbCommand cmd;   // Almacenara el comando a ejecutar.
            cmd = new OleDbCommand(Tabla, Conexion);    // Crea el nuevo adaptador Ole
            Adaptador.SelectCommand = cmd;
            Adaptador.Fill(Ds_Informacion); //Obtiene la informacion en un dataset
            Conexion.Close();

            Dt_Presupuesto_Aprobado = Ds_Informacion.Tables[0];    // Liga los datos con el DataTable

            if (Validar_Columnas_Excel(Dt_Presupuesto_Aprobado))
            {
                Convertir_Datos_Presuouesto_Aprobado(Dt_Presupuesto_Aprobado); //Convierte los datos extraidos para que sean visualizados de manera correcta.
            }
            else
            {
                Mostrar_Ocultar_Error(true);
            }
            if (System.IO.File.Exists("C:\\Archivos\\" + Nombre_Archivo.Substring(Caracter_Inicio, Nombre_Archivo.Length - Caracter_Inicio)))
            {
                System.IO.File.Delete("C:\\Archivos\\" + Nombre_Archivo.Substring(Caracter_Inicio, Nombre_Archivo.Length - Caracter_Inicio));
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error  Btn_Cargar_Click: " + ex.Message.ToString(), ex);
        }
        finally
        {
            //oledbConn.Close();  // Close connection
        }
    }
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Actualizar_Proveedores
    ///DESCRIPCIÓN          : Metodo que da de alta y actualiza los proveedores leidos del archivo de excel
    ///PROPIEDADES          : 
    ///CREO                 : Jennyfer Ivonne Ceja lemus
    ///FECHA_CREO           : 10/Septiembre/2012 1:35 p pm
    ///MODIFICO             : 
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...: 
    ///*********************************************************************************************************
    ///*******************************************************************************
    private void Actualizar_Presupuesto_Aprobado()
    {
        Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Negocio Negocio= new Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Negocio();
        DataTable Dt_Insertar = new DataTable();
        Dt_Insertar = (DataTable)Session["Dt_Insertar"];//Tabla que contendra los registros que se van a Insertar

        DataTable Dt_Actualizar = new DataTable();
        Dt_Actualizar = (DataTable)Session["Dt_Actualizar"];//Tabla que contendra los registros que se van a acutliazar
        String Mensaje = String.Empty;
        try
        {
            if ((Dt_Insertar != null && Dt_Insertar.Rows.Count > 0) || (Dt_Actualizar != null && Dt_Actualizar.Rows.Count > 0))
            {
                Negocio.P_Dt_Actualizar_Presupuesto = Dt_Actualizar;
                Negocio.P_Dt_Insertar_Presupuesto = Dt_Insertar;
                Mensaje = Negocio.Alta_Presupuesto_Aprobado();
                ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "Actualización Proveedores", "alert('" + Mensaje + "');", true);
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Convertir_Datos_Proveedores " + ex.Message.ToString(), ex);
        }
    }
    #endregion

    #region EVENTOS
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
    ///DESCRIPCIÓN          : Evento del boton Salir 
    ///PARAMETROS           :    
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 13/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        //Limpiar_Varibles_Sesion_Grid();
        Mostrar_Ocultar_Error(false);
        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");

    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Cargar_Presupuesto_Grid_Click
    /// DESCRIPCION : Evento del boton Cargar Presupuesto al dar click
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 14/Septiembre/2012 11:54 AM
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Cargar_Presupuesto_Grid_Click(object sender, EventArgs e)
    {
        try
        {
            if (!String.IsNullOrEmpty(HttpContext.Current.Session["Nombre_Archivo_Excel"].ToString().Trim()))
                Interpretar_Excel();    //Interpreta el archivo de Excel cargado.

            else
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - Ingrese el nombre del objeto que se cargara. <br />";
                Mostrar_Ocultar_Error(true);
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text += "&nbsp; - " + Ex.Message.ToString() + " <br />";
            Mostrar_Ocultar_Error(true);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Actualizar_Presupuesto_Click
    /// DESCRIPCION : Evento del boton Actualizar_Presupuesto al dar click
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 19/Septiembre/2012 11:31 AM
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Actualizar_Presupuesto_Click(object sender, EventArgs e)
    {
        try
        {
            Actualizar_Presupuesto_Aprobado();
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text += "&nbsp; - " + Ex.Message.ToString() + " <br />";
            Mostrar_Ocultar_Error(true);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: AFU_Archivo_Excel_UploadedComplete
    /// DESCRIPCION : CUANDO LA CARGA DEL ARCHIVO TERMINA LO GUARDA EN LA UBICACION PRESTABLECIDA
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 22/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void AFU_Archivo_Excel_UploadedComplete(object sender, AsyncFileUploadEventArgs e)
    {
        //****************************************
        //CUANDO LA CARGA DEL ARCHIVO TERMINA LO
        //GUARDA EN LA UBICACION PRESTABLECIDA
        //****************************************
        try
        {
            //Limpiar_Varibles_Sesion_Grid();
            Mostrar_Ocultar_Error(false);
            if (AFU_Archivo_Excel.HasFile)
            {

                string currentDateTime = String.Format("{0:yyyy-MM-dd_HH.mm.sstt}", DateTime.Now);
                string fileNameOnServer = System.IO.Path.GetFileName(AFU_Archivo_Excel.FileName).Replace(" ", "_");

                bool xls = Path.GetExtension(AFU_Archivo_Excel.PostedFile.FileName).Contains(".xls");
                bool xlsx = Path.GetExtension(AFU_Archivo_Excel.PostedFile.FileName).Contains(".xlsx");

                if (xls || xlsx)
                {
                    AFU_Archivo_Excel.SaveAs(@"C:/Archivos/" + fileNameOnServer);

                    //Colocar nombre en variable de sesion
                    HttpContext.Current.Session["Nombre_Archivo_Excel"] = fileNameOnServer;
                }
                else
                    return;
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Text += "&nbsp; - " + ex.Message.ToString() + " <br />";
            Mostrar_Ocultar_Error(true);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Cotizadores_PageIndexChanging
    ///DESCRIPCIÓN: Metodo para manejar la paginacion del Grid_Cotizadores
    ///PARAMETROS:   
    ///CREO: Jacqueline Ramìrez Sierra
    ///FECHA_CREO: 16/Febrero/2011  
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Presupuesto_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Grid_Presupuesto.SelectedIndex = (-1);
            Cargar_Grid_Presupuesto_Aprobado(e.NewPageIndex);
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text += " Error en la Paginacion [" + Ex.Message + "]";
            Mostrar_Ocultar_Error(true);
        }
    }
    #endregion

    
}
