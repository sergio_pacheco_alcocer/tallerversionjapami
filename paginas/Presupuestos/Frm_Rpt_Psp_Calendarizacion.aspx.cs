﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using JAPAMI.Presupuestos_Reporte_Calendarizacion.Negocio;

public partial class paginas_Presupuestos_Rpt_Psp_Calendarizacion : System.Web.UI.Page
{
    #region "Page Load"

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Page_Load
    ///DESCRIPCIÓN:          Evento que se Ejecuta al Cargar la Pagina
    ///PROPIEDADES:     
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Div_Contenedor_Msj_Error.Visible = false;
            if (Cls_Sessiones.No_Empleado == null || Cls_Sessiones.No_Empleado.Trim().Length == 0)
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            }
            if (!IsPostBack)
            {
                Llenar_Combo_Anio();
                Llenar_Combo_Unidad_Responsable();
                Llenar_Combo_Partida();
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    #endregion "Page Load"

    #region "Metodos"

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Filtros_Busqueda
    ///DESCRIPCIÓN:          Limpia los combos de los filtros.
    ///PROPIEDADES:     
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Limpiar_Filtros_Busqueda(Boolean Limpiar_Grid)
    {
        
        
        Cmb_Anio.SelectedIndex = 0;
        //Cmb_Unidad_Responsable.DataSource = new DataTable();
        //Cmb_Unidad_Responsable.DataBind();
        //Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("<-- HACE FALTA SELECCIONAR EL AÑO -->", ""));
        //Cmb_Partida.DataSource = new DataTable();
        //Cmb_Partida.DataBind();
        //Cmb_Partida.Items.Insert(0, new ListItem("<-- HACE FALTA SELECCIONAR UNA UNIDAD RESPONSABLE -->", ""));
        Cmb_Unidad_Responsable.SelectedIndex = 0;
        Cmb_Partida.SelectedIndex = 0;
        
        if (Limpiar_Grid)
        {
            Grid_Calendarizar.DataSource = new DataTable();
            Grid_Calendarizar.DataBind();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: llenar_Grid_Calendarizar()
    ///DESCRIPCIÓN:          Llena el grid de acuerdo a los filtros seleccionados
    ///PROPIEDADES:          
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Grid_Calendarizar()
    {
        try
        {
            
            Cls_Rpt_Psp_Calendarizacion_Negocio Reporte_Negocio = new Cls_Rpt_Psp_Calendarizacion_Negocio();
       
            if (Cmb_Anio.SelectedIndex > 0)
            {
                Reporte_Negocio.P_Anio = Cmb_Anio.SelectedItem.Value;
            }
            if (Cmb_Unidad_Responsable.SelectedIndex > 0)
            {
                Reporte_Negocio.P_Unidad_Responsable = Cmb_Unidad_Responsable.SelectedItem.Value;
            }
            if (Cmb_Partida.SelectedIndex > 0)
            {
                Reporte_Negocio.P_Partida = Cmb_Partida.SelectedItem.Value;
            }
        
            if (Reporte_Negocio.Consulta_Calendarizacion().Rows.Count != 0)
            {
                Grid_Calendarizar.DataSource = Reporte_Negocio.Consulta_Calendarizacion();
                Grid_Calendarizar.DataBind();
            }
            else
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No se encontraron Registros')", true);
    
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Anio
    ///DESCRIPCIÓN:          Llena el Combo de Año.
    ///PROPIEDADES:          
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combo_Anio()
    {
        try
        {
            Cls_Rpt_Psp_Calendarizacion_Negocio Reporte_Negocio = new Cls_Rpt_Psp_Calendarizacion_Negocio();
            DataTable Dt_Anio = Reporte_Negocio.Consultar_Anio();
            Cmb_Anio.DataSource = Dt_Anio;
            Cmb_Anio.DataTextField = "ANIO";
            Cmb_Anio.DataValueField = "ANIO";
            Cmb_Anio.DataBind();
            Cmb_Anio.Items.Insert(0, new ListItem("<-- TODAS -->", ""));
            
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Unidad_Responsable
    ///DESCRIPCIÓN:          Llena el Combo de Unidad_responsable.
    ///PROPIEDADES:          
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combo_Unidad_Responsable()
    {
        try
        {
            Cls_Rpt_Psp_Calendarizacion_Negocio Reporte_Negocio = new Cls_Rpt_Psp_Calendarizacion_Negocio();
            Reporte_Negocio.P_Anio = Cmb_Anio.SelectedItem.Value;
            Cmb_Unidad_Responsable.DataSource = Reporte_Negocio.Consultar_Unidad_Reponsable();
            Cmb_Unidad_Responsable.DataTextField = "DEPENDENCIA";
            Cmb_Unidad_Responsable.DataValueField = "CLAVE";
            Cmb_Unidad_Responsable.DataBind();
            Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("<-- TODAS -->", ""));
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Partida
    ///DESCRIPCIÓN:          Llena el Combo de Partida.
    ///PROPIEDADES:          
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combo_Partida()
    {
        try
        {
            Cls_Rpt_Psp_Calendarizacion_Negocio Reporte_Negocio = new Cls_Rpt_Psp_Calendarizacion_Negocio();
            Reporte_Negocio.P_Anio = Cmb_Anio.SelectedItem.Value;
            Reporte_Negocio.P_Unidad_Responsable = Cmb_Unidad_Responsable.SelectedItem.Value;
            Cmb_Partida.DataSource = Reporte_Negocio.Consultar_Partida();
            Cmb_Partida.DataTextField = "PARTIDA";
            Cmb_Partida.DataValueField = "CLAVE";
            Cmb_Partida.DataBind();
            Cmb_Partida.Items.Insert(0, new ListItem("<-- TODAS -->", ""));
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte_PDF
    ///DESCRIPCIÓN:          carga el data set fisico con el cual se genera el Reporte especificado
    ///PROPIEDADES:          1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
    ///                      2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
    ///                      3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Generar_Reporte_PDF(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte)
    {
        if (Data_Set_Consulta_DB.Tables[0].Rows.Count != 0)
        {
        ReportDocument Reporte = new ReportDocument();
        String File_Path = Server.MapPath("../Rpt/Presupuestos/" + Nombre_Reporte);
        Reporte.Load(File_Path);
        Ds_Reporte = Data_Set_Consulta_DB;
        Reporte.SetDataSource(Ds_Reporte);
        ExportOptions Export_Options = new ExportOptions();
        DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
        Disk_File_Destination_Options.DiskFileName = Server.MapPath("../../Reporte/Rpt_Psp_Calendarizacion.pdf");
        Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
        Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
        Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
        Reporte.Export(Export_Options);
        String Ruta = "../../Reporte/Rpt_Psp_Calendarizacion.pdf";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
        else
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No se encontraron Registros')", true);
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte_Excel
    ///DESCRIPCIÓN:          carga el data set fisico con el cual se genera el Reporte especificado
    ///PROPIEDADES:          1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
    ///                      2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
    ///                      3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    public void Generar_Reporte_Excel(DataSet Data_Set, DataSet Ds_Reporte, String Nombre_Reporte)
    {
        if (Data_Set.Tables[0].Rows.Count != 0)
        {
            ReportDocument Reporte = new ReportDocument();
            String File_Path = Server.MapPath("../Rpt/Presupuestos/" + Nombre_Reporte);
            Reporte.Load(File_Path);
            Ds_Reporte = Data_Set;
            Reporte.SetDataSource(Ds_Reporte);
            ExportOptions Export_Options = new ExportOptions();
            DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
            Disk_File_Destination_Options.DiskFileName = Server.MapPath("../../Reporte/Rpt_Psp_calendarizacion.xls");
            Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
            Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
            Export_Options.ExportFormatType = ExportFormatType.Excel;
            Reporte.Export(Export_Options);
            String Ruta = "../../Reporte/Rpt_Psp_Calendarizacion.xls";

            ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No se encontraron Registros')", true);

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
    ///DESCRIPCIÓN:          Genera los Datos para el Reporte y dependiendo de la Opción hace
    ///                      la exportación de los datos.
    ///PROPIEDADES:          1. Tipo.    Tipo de Reporte ya se PDF y Excel.
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    public void Generar_Reporte(String Tipo)
    {
        try
        {
            Cls_Rpt_Psp_Calendarizacion_Negocio Reporte_Negocio = new Cls_Rpt_Psp_Calendarizacion_Negocio();
           
            if (Cmb_Anio.SelectedIndex > 0)
            {
                Reporte_Negocio.P_Anio = Cmb_Anio.SelectedItem.Value;
            }
            if (Cmb_Unidad_Responsable.SelectedIndex > 0)
            {
                Reporte_Negocio.P_Unidad_Responsable = Cmb_Unidad_Responsable.SelectedItem.Value;
            }
            if (Cmb_Partida.SelectedIndex > 0)
            {
                Reporte_Negocio.P_Partida = Cmb_Partida.SelectedItem.Value;
            }
            
            DataTable Dt_Datos = Reporte_Negocio.Consulta_Calendarizacion();

            Ds_Rpt_Psp_Calendarizacion Ds_Reporte = new Ds_Rpt_Psp_Calendarizacion();
            DataSet Ds_Consulta = new DataSet();
            Dt_Datos.TableName = "DT_CALENDARIZACION";
            Ds_Consulta.Tables.Add(Dt_Datos.Copy());
            if (Tipo.Trim().Equals("PDF"))
            {
                Generar_Reporte_PDF(Ds_Consulta, Ds_Reporte, "Rpt_Psp_Calendarizacion.rpt");
            }
            else if (Tipo.Trim().Equals("EXCEL"))
            {
                Generar_Reporte_Excel(Ds_Consulta, Ds_Reporte, "Rpt_Psp_Calendarizacion.rpt");
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
    ///DESCRIPCIÓN:          Pasa un numero entero a Formato de ID.
    ///PARAMETROS:     
    ///                      1. Dato_ID. Dato que se desea pasar al Formato de ID.
    ///                      2. Longitud_ID. Longitud que tendra el ID. 
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO             : 
    ///FECHA_MODIFICO       : 
    ///CAUSA_MODIFICACIÓN   : 
    ///*******************************************************************************
    private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID)
    {
        String Retornar = "";
        String Dato = "" + Dato_ID;
        for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++)
        {
            Retornar = Retornar + "0";
        }
        Retornar = Retornar + Dato;
        return Retornar;
    }


    #endregion "Metodos"

    #region "Grids"

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Empleados_PageIndexChanging
    ///DESCRIPCIÓN: Maneja el Cambio de Pagina del Grid de Empleados.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 14/Julio/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Calendarizar_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Grid_Calendarizar.PageIndex = e.NewPageIndex;
            Llenar_Grid_Calendarizar();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Empleados_Resguardo_PageIndexChanging
    ///DESCRIPCIÓN: Maneja el evento de cambio de Página del GridView de Busqueda
    ///             de empleados.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 01/Diciembre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    //protected void Grid_Calendarizar_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    try
    //    {
    //        Grid_Calendarizar.PageIndex = e.NewPageIndex;
    //        Llenar_Grid_Busqueda_Empleados_Resguardo();
    //        MPE_Resguardante.Show();
    //    }
    //    catch (Exception Ex)
    //    {
    //        Lbl_Ecabezado_Mensaje.Text = Ex.Message;
    //        Lbl_Mensaje_Error.Text = "";
    //        Div_Contenedor_Msj_Error.Visible = true;
    //    }
    //}

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged
    ///DESCRIPCIÓN: Maneja el evento de cambio de Selección del GridView de Busqueda
    ///             de empleados.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 01/Diciembre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    //protected void Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        if (Grid_Busqueda_Empleados_Resguardo.SelectedIndex > (-1))
    //        {
    //            String Empleado_Seleccionado_ID = Grid_Busqueda_Empleados_Resguardo.SelectedRow.Cells[1].Text.Trim();
    //            Cls_Cat_Empleados_Negocios Empleado_Negocio = new Cls_Cat_Empleados_Negocios();
    //            Empleado_Negocio.P_Empleado_ID = Empleado_Seleccionado_ID.Trim();
    //            DataTable Dt_Datos_Empleado = Empleado_Negocio.Consulta_Empleados_General();
    //            String Dependencia_ID = (!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Dependencia_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Dependencia_ID].ToString().Trim() : null;
    //            Int32 Index_Combo = (-1);
    //            if (Dependencia_ID != null && Dependencia_ID.Trim().Length > 0)
    //            {
    //                Cmb_Busqueda_Dependencias.SelectedIndex = Cmb_Busqueda_Dependencias.Items.IndexOf(Cmb_Busqueda_Dependencias.Items.FindByValue(Dependencia_ID));
    //                Cmb_Busqueda_Dependencias_SelectedIndexChanged(Cmb_Busqueda_Dependencias, null);
    //                Cmb_Busqueda_Nombre_Empleado.SelectedIndex = Cmb_Busqueda_Nombre_Empleado.Items.IndexOf(Cmb_Busqueda_Nombre_Empleado.Items.FindByValue(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString().Trim()));
    //            }
    //            MPE_Resguardante.Hide();
    //            Grid_Busqueda_Empleados_Resguardo.SelectedIndex = (-1);
    //        }
    //    }
    //    catch (Exception Ex)
    //    {
    //        Lbl_Ecabezado_Mensaje.Text = Ex.Message;
    //        Lbl_Mensaje_Error.Text = "";
    //        Div_Contenedor_Msj_Error.Visible = true;
    //    }
    //}

    #endregion

    #region "Eventos"

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Limpiar_Filtros_Click
    ///DESCRIPCIÓN:          Maneja el Evento del Boton para realizar la Limpieza de los filtros
    ///                      para la busqueda.
    ///PARAMETROS:     
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Btn_Limpiar_Filtros_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Limpiar_Filtros_Busqueda(false);
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Generar_Listado_Click
    ///DESCRIPCIÓN:          Maneja el Evento del Boton para realizar la Busqueda con los filtros
    ///                      para la busqueda.
    ///PARAMETROS:     
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Btn_Generar_Listado_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Valida_Filtros())
            {
                Grid_Calendarizar.PageIndex = 0;
                Llenar_Grid_Calendarizar();       
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = " Favor de Seleccinar: ";
                Lbl_Mensaje_Error.Text = " El Año ";
                Div_Contenedor_Msj_Error.Visible = true;
            }

        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Busqueda_Dependencias_SelectedIndexChanged
    ///DESCRIPCIÓN:          Maneja el Evento del Cambio de Seleccion para el los Combos de
    ///                      Dependencias.
    ///PARAMETROS:     
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private Boolean Valida_Filtros() {
        
        if (Cmb_Anio.SelectedIndex > 0)
        {
            return true;
        }

        return false;
        
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Busqueda_Dependencias_SelectedIndexChanged
    ///DESCRIPCIÓN:          Maneja el Evento del Cambio de Seleccion para el los Combos de
    ///                      Dependencias.
    ///PARAMETROS:     
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Cmb_Anio_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Cmb_Anio.SelectedIndex > 0)
        {
            Llenar_Combo_Unidad_Responsable();
        }
        //else
        //{
            //Cmb_Unidad_Responsable.DataSource = new DataTable();
            //Cmb_Unidad_Responsable.DataBind();
        //    Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("<-- HACE FALTA SELECCIONAR EL AÑO -->", ""));
        //}
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Busqueda_Dependencias_SelectedIndexChanged
    ///DESCRIPCIÓN:          Maneja el Evento del Cambio de Seleccion para el los Combos de
    ///                      Dependencias.
    ///PARAMETROS:     
    ///CREO:                Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Cmb_Unidad_Responsable_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Cmb_Unidad_Responsable.SelectedIndex > 0)
        {
            Llenar_Combo_Partida();
        }
        //else
        //{
            //Cmb_Partida.DataSource = new DataTable();
            //Cmb_Partida.DataBind();
        //    Cmb_Partida.Items.Insert(0, new ListItem("<-- HACE FALTA SELECCIONAR UNA UNIDA RESPONSABLE -->", ""));
        //}
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN:          Sale del Formulario.
    ///PROPIEDADES:     
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Generar_Reporte_PDF_Click
    ///DESCRIPCIÓN:          Genera el Reporte en PDF.
    ///PROPIEDADES:     
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Generar_Reporte_PDF_Click(object sender, ImageClickEventArgs e)
    {
        if(Valida_Filtros())
            Generar_Reporte("PDF");
        else
        {
            Lbl_Ecabezado_Mensaje.Text = " Favor de Seleccionar: ";
            Lbl_Mensaje_Error.Text = " El Año ";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Generar_Reporte_Excel_Click
        ///DESCRIPCIÓN:      Genera el Reporte en Excel.
    ///PROPIEDADES:     
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Generar_Reporte_Excel_Click(object sender, ImageClickEventArgs e)
    {
        if(Valida_Filtros())
        Generar_Reporte("EXCEL");
        else
        {
            Lbl_Ecabezado_Mensaje.Text = " Favor de Seleccionar: ";
            Lbl_Mensaje_Error.Text = " El Año ";
            Div_Contenedor_Msj_Error.Visible = true;
        }

    }

    #endregion "Eventos"


}
