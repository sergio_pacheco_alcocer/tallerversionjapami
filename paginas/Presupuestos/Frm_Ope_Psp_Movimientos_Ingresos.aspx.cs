﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Cat_Psp_Rubros.Negocio;
using JAPAMI.Ope_Psp_Pronosticos_Ingresos.Negocio;
using JAPAMI.Ope_Psp_Movimientos_Ingresos.Negocio;
using AjaxControlToolkit;
using System.IO;
using JAPAMI.Ope_Con_Poliza_Ingresos.Datos;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class paginas_Presupuestos_Frm_Ope_Psp_Movimientos_Ingresos : System.Web.UI.Page
{
    #region (Page_Load)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Page_Load
        ///DESCRIPCIÓN          : Metodo de inicio de la pagina
        ///PARAMETROS           :
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 17/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            try
            {
                if (!IsPostBack)
                {
                    Mostar_Limpiar_Error(String.Empty, String.Empty, false);
                    Configuracion_Inicial();
                    ViewState["SortDirection"] = "DESC";
                    Lbl_Anexo.Visible = false;
                    Div_Anexos.Visible = false;
                }
            }
            catch (Exception Ex)
            {
                Mostar_Limpiar_Error(String.Empty, "Error al cargar la pagina de movimientos de ingresos. Error[" + Ex.Message + "]", true);
            }
        }
    #endregion

    #region (Metodos)
    #region (Generales)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Configuracion_Inicial
        ///DESCRIPCIÓN          : Metodo para cargar la configuracion inicial de la pagina
        ///PARAMETROS           1: Estatus: true o false para habilitar los controles
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 17/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Configuracion_Inicial()
        {
            try
            {
                Mostar_Limpiar_Error(String.Empty, String.Empty, false);
                Limpiar_Forma("Todo");
                Txt_Total_Modificado.Text = String.Empty;
                Habilitar_Forma(false);
                Estado_Botones("Inicial");
                Llenar_Combo_Estatus();
                Tr_Programa.Visible = false;
                Llenar_Grid_Movimientos();
                Div_Grid_Movimientos.Style.Add("display", "block");
                Div_Datos.Style.Add("display", "none");
                Tr_Chk_Autorizar.Style.Add("display", "none");
            }
            catch (Exception Ex)
            {
                Mostar_Limpiar_Error(String.Empty, "Error al cargar la pagina de Movimiento de ingresos. Error[" + Ex.Message + "]", true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Habilitar_Forma
        ///DESCRIPCIÓN          : Metodo para habilitar o deshabilitar los controles
        ///PARAMETROS           1: Estatus: true o false para habilitar los controles
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 17/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Habilitar_Forma(Boolean Estatus)
        {
            Txt_Importe.Enabled = Estatus;
            Txt_Enero.Enabled = Estatus;
            Txt_Febrero.Enabled = Estatus;
            Txt_Marzo.Enabled = Estatus;
            Txt_Abril.Enabled = Estatus;
            Txt_Mayo.Enabled = Estatus;
            Txt_Junio.Enabled = Estatus;
            Txt_Julio.Enabled = Estatus;
            Txt_Agosto.Enabled = Estatus;
            Txt_Septiembre.Enabled = Estatus;
            Txt_Octubre.Enabled = Estatus;
            Txt_Noviembre.Enabled = Estatus;
            Txt_Diciembre.Enabled = Estatus;
            Txt_Total.Enabled = false;
            Cmb_Rubro.Enabled = Estatus;
            Cmb_FF.Enabled = Estatus;
            Cmb_Tipo.Enabled = false;
            Cmb_Clase.Enabled = false;
            Cmb_Operacion.Enabled = Estatus;
            Cmb_Estatus.Enabled = false;
            Txt_Justificacion.Enabled = Estatus;
            Txt_Autorizo.Enabled = false;
            Txt_Comentario.Enabled = false;
            Txt_Fecha_Autorizo.Enabled = false;
            Btn_Agregar.Enabled = Estatus;
            Btn_Limpiar.Enabled = Estatus;
            
            if (Estatus)
            {
                Div_Anexos.Style.Add("display", "block");
                Div_Datos_Generales.Style.Add("display", "block");
                Div_CRI.Style.Add("display", "block");
            }
            else
            {
                Div_Anexos.Style.Add("display", "none");
                Div_Datos_Generales.Style.Add("display", "none");
                Div_CRI.Style.Add("display", "none");
            }
            
            Cmb_Programa.Enabled = false;
            Btn_Busqueda_Programa.Enabled = false;
            Txt_Busqueda_Programa.Enabled = false;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Limpiar_Forma
        ///DESCRIPCIÓN          : Metodo para limpiar los controles
        ///PARAMETROS           :
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 17/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Limpiar_Forma(String Tipo)
        {
            switch(Tipo)
            {
                case "Todo":
                    Txt_Importe.Text = String.Empty;
                    Txt_Enero.Text = String.Empty;
                    Txt_Febrero.Text = String.Empty;
                    Txt_Marzo.Text = String.Empty;
                    Txt_Abril.Text = String.Empty;
                    Txt_Mayo.Text = String.Empty;
                    Txt_Junio.Text = String.Empty;
                    Txt_Julio.Text = String.Empty;
                    Txt_Agosto.Text = String.Empty;
                    Txt_Septiembre.Text = String.Empty;
                    Txt_Octubre.Text = String.Empty;
                    Txt_Noviembre.Text = String.Empty;
                    Txt_Diciembre.Text = String.Empty;
                    Txt_Total.Text = String.Empty;
                    Cmb_Rubro.SelectedIndex = -1;
                    Cmb_FF.SelectedIndex = -1;
                    Cmb_Tipo.SelectedIndex = -1;
                    Cmb_Clase.SelectedIndex = -1;
                    Cmb_Operacion.SelectedIndex = -1;
                    Cmb_Estatus.SelectedIndex = -1;
                    Txt_Justificacion.Text = String.Empty;
                    Lbl_Disp_Ene.Text = String.Empty;
                    Lbl_Disp_Feb.Text = String.Empty;
                    Lbl_Disp_Mar.Text = String.Empty;
                    Lbl_Disp_Abr.Text = String.Empty;
                    Lbl_Disp_May.Text = String.Empty;
                    Lbl_Disp_Jun.Text = String.Empty;
                    Lbl_Disp_Jul.Text = String.Empty;
                    Lbl_Disp_Ago.Text = String.Empty;
                    Lbl_Disp_Sep.Text = String.Empty;
                    Lbl_Disp_Oct.Text = String.Empty;
                    Lbl_Disp_Nov.Text = String.Empty;
                    Lbl_Disp_Dic.Text = String.Empty;
                    Hf_Rubro_ID.Value = String.Empty;
                    Hf_Tipo_ID.Value = String.Empty;
                    Hf_Clase_ID.Value = String.Empty;
                    Hf_Concepto_ID.Value = String.Empty;
                    Hf_Modificado.Value = String.Empty;
                    Hf_Tipo_Concepto.Value = String.Empty;
                    Hf_SubConcepto_ID.Value = String.Empty;
                    Txt_Autorizo.Text = String.Empty;
                    Txt_Comentario.Text = String.Empty;
                    Txt_Fecha_Autorizo.Text = String.Empty;
                    Hf_Ampliacion.Value = String.Empty;
                    Hf_Reduccion.Value = String.Empty;
                    Hf_Estimado.Value = String.Empty;
                    Hf_Clave_Nom_Concepto.Value = String.Empty;
                    Grid_Conceptos.SelectedIndex = -1;
                    Hf_No_Rechazado.Value = string.Empty;
                    Txt_Total_Modificado.Text = String.Empty;
                    Cmb_Programa.SelectedIndex = -1;
                    Hf_Clave_Nom_Programa.Value = String.Empty;
                    Hf_Programa_ID.Value = String.Empty;
                    Hf_Importe_Programa.Value = String.Empty;
                    Lbl_Nombre_Concepto.Text = String.Empty;
                    break;
                case "Concepto":
                    Grid_Conceptos.SelectedIndex = -1;
                    Cmb_Tipo.SelectedIndex = -1;
                    Cmb_Clase.SelectedIndex = -1;
                    Lbl_Disp_Ene.Text = String.Empty;
                    Lbl_Disp_Feb.Text = String.Empty;
                    Lbl_Disp_Mar.Text = String.Empty;
                    Lbl_Disp_Abr.Text = String.Empty;
                    Lbl_Disp_May.Text = String.Empty;
                    Lbl_Disp_Jun.Text = String.Empty;
                    Lbl_Disp_Jul.Text = String.Empty;
                    Lbl_Disp_Ago.Text = String.Empty;
                    Lbl_Disp_Sep.Text = String.Empty;
                    Lbl_Disp_Oct.Text = String.Empty;
                    Lbl_Disp_Nov.Text = String.Empty;
                    Lbl_Disp_Dic.Text = String.Empty;
                    Txt_Importe.Text = String.Empty;
                    Txt_Enero.Text = String.Empty;
                    Txt_Febrero.Text = String.Empty;
                    Txt_Marzo.Text = String.Empty;
                    Txt_Abril.Text = String.Empty;
                    Txt_Mayo.Text = String.Empty;
                    Txt_Junio.Text = String.Empty;
                    Txt_Julio.Text = String.Empty;
                    Txt_Agosto.Text = String.Empty;
                    Txt_Septiembre.Text = String.Empty;
                    Txt_Octubre.Text = String.Empty;
                    Txt_Noviembre.Text = String.Empty;
                    Txt_Diciembre.Text = String.Empty;
                    Txt_Total.Text = String.Empty;
                    Hf_Rubro_ID.Value = String.Empty;
                    Hf_Tipo_ID.Value = String.Empty;
                    Hf_Clase_ID.Value = String.Empty;
                    Hf_Concepto_ID.Value = String.Empty;
                    Hf_Modificado.Value = String.Empty;
                    Hf_SubConcepto_ID.Value = String.Empty;
                    Hf_Ampliacion.Value = String.Empty;
                    Hf_Reduccion.Value = String.Empty;
                    Hf_Estimado.Value = String.Empty;
                    Hf_Clave_Nom_Concepto.Value = String.Empty;
                    Tabla_Meses.Visible = false;
                    
                    Txt_Importe.Text = String.Empty;
                    Lbl_Nombre_Concepto.Text = String.Empty;
                    
                    if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("SUPLEMENTO"))
                    {
                        Hf_Importe_Programa.Value = String.Empty;
                    }
                    break;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Limpiar_Forma
        ///DESCRIPCIÓN          : Metodo para limpiar los controles
        ///PARAMETROS           :
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 17/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Mostar_Limpiar_Error(String Encabezado_Error, String Mensaje_Error, Boolean Mostrar)
        {
            Lbl_Encabezado_Error.Text = Encabezado_Error;
            Lbl_Mensaje_Error.Text = Mensaje_Error;
            Lbl_Mensaje_Error.Visible = Mostrar;
            Td_Error.Visible = Mostrar;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Estado_Botones
        ///DESCRIPCIÓN          : metodo que muestra los botones de acuerdo al estado en el que se encuentre
        ///PARAMETROS           1: String Estado: El estado de los botones solo puede tomar 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 17/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public void Estado_Botones(String Estado)
        {
            switch (Estado)
            {
                case "Inicial":
                    //Boton Nuevo
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Nuevo.Enabled = true;
                    Btn_Nuevo.Visible = true;
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    //boton Modificar
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Modificar.Enabled = false;
                    Btn_Modificar.Visible = false;
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    //Boton Salir
                    Btn_Salir.ToolTip = "Inicio";
                    Btn_Salir.Enabled = true;
                    Btn_Salir.Visible = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    break;
                case "Nuevo":
                    //Boton Nuevo
                    Btn_Nuevo.ToolTip = "Guardar";
                    Btn_Nuevo.Enabled = true;
                    Btn_Nuevo.Visible = true;
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    //Boton Salir
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Salir.Enabled = true;
                    Btn_Salir.Visible = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    //boton Modificar
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Modificar.Enabled = false;
                    Btn_Modificar.Visible = false;
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    break;
                case "Actualizar":
                    //Boton Nuevo
                    Btn_Nuevo.ToolTip = "Actualizar";
                    Btn_Nuevo.Enabled = true;
                    Btn_Nuevo.Visible = true;
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    //Boton Salir
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Salir.Enabled = true;
                    Btn_Salir.Visible = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    //boton Modificar
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Modificar.Enabled = true;
                    Btn_Modificar.Visible = true;
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    break;
                case "Modificar":
                    //Boton Nuevo
                    Btn_Nuevo.Visible = false;
                    //Boton Modificar
                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Modificar.Enabled = true;
                    Btn_Modificar.Visible = true;
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                    //Boton Salir
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Salir.Enabled = true;
                    Btn_Salir.Visible = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    break;

                case "PreActualizar":
                    //Boton Nuevo
                    Btn_Nuevo.ToolTip = "Actualizar";
                    Btn_Nuevo.Enabled = true;
                    Btn_Nuevo.Visible = true;
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    //Boton Salir
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Salir.Enabled = true;
                    Btn_Salir.Visible = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    //boton Modificar
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Modificar.Enabled = false;
                    Btn_Modificar.Visible = false;
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    break;
            }//fin del switch
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Validar_Datos
        ///DESCRIPCIÓN          : metodo para validar los datos del formulario
        ///PARAMETROS           :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 17/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public Boolean Validar_Datos()
        {
            Mostar_Limpiar_Error(String.Empty, String.Empty, false);
            Cls_Cat_Psp_Rubros_Negocio Negocio = new Cls_Cat_Psp_Rubros_Negocio();
            String Mensaje_Encabezado = "Es necesario: ";
            String Mensaje_Error = String.Empty;
            Boolean Datos_Validos = true;
            Double Total = 0.00;
            Double Importe = 0.00;
            DataTable Dt_Mov_Conceptos = new DataTable();
            Dt_Mov_Conceptos = (DataTable)Session["Dt_Mov_Conceptos"];
            String Ruta = String.Empty;
            String Ruta_Destino = String.Empty;
            String Extension = String.Empty;
            String Archivo = String.Empty;
            Hf_Ruta_Doc.Value = String.Empty;
            Hf_Nombre_Doc.Value = String.Empty;

            try
            {
                if (String.IsNullOrEmpty(Txt_Importe.Text.Trim()))
                {
                    Mensaje_Error += "&nbsp;&nbsp;* Instroducir un importe. <br />";
                    Datos_Validos = false;
                }
                else
                {
                    if (!String.IsNullOrEmpty(Txt_Total.Text.Trim()))
                    {
                        Importe = Convert.ToDouble(String.IsNullOrEmpty(Txt_Importe.Text.Trim()) ? "0" : Txt_Importe.Text.Trim().Replace(",", ""));
                        Total = Convert.ToDouble(String.IsNullOrEmpty(Txt_Total.Text.Trim()) ? "0" : Txt_Total.Text.Trim().Replace(",", ""));

                        if (Importe > Total)
                        {
                            Mensaje_Error += "&nbsp;&nbsp;* Introducir un Total y un Importe iguales. <br />";
                            Datos_Validos = false;
                        }
                    }
                }


                if (Tr_Programa.Visible) 
                {
                    if (!String.IsNullOrEmpty(Cmb_Programa.SelectedItem.Value.Trim()))
                    {
                        Double Imp_Programa = Convert.ToDouble(String.IsNullOrEmpty(Hf_Importe_Programa.Value.Trim()) ? "0" : Hf_Importe_Programa.Value.Trim());
                        if (Imp_Programa > 0)
                        {
                            if (Importe > Imp_Programa)
                            {
                                Mensaje_Error += "&nbsp;&nbsp;* Favor de introducir otro importe, ya que el programa solo tiene: " + String.Format("{0:c}", Imp_Programa) + ". <br />";
                                Datos_Validos = false;
                            }
                            else if (Total < Imp_Programa)
                            {
                                Mensaje_Error += "&nbsp;&nbsp;* Favor de introducir el importe correcto, debe ampliar el importe de: " + String.Format("{0:c}", Imp_Programa) + ". <br />";
                                Datos_Validos = false;
                            }
                            else if (Importe < Imp_Programa)
                            {
                                Mensaje_Error += "&nbsp;&nbsp;* Favor de introducir el importe correcto, debe ampliar el importe de: " + String.Format("{0:c}", Imp_Programa) + ". <br />";
                                Datos_Validos = false;
                            }
                        }
                    }
                }

                if (String.IsNullOrEmpty(Txt_Justificacion.Text.Trim()))
                {
                    Mensaje_Error += "&nbsp;&nbsp;* Introducir una justificación. <br />";
                    Datos_Validos = false;
                }

                if (String.IsNullOrEmpty(Txt_Total.Text.Trim()))
                {
                    Mensaje_Error += "&nbsp;&nbsp;* Introducir un Total. <br />";
                    Datos_Validos = false;
                }

                if (String.IsNullOrEmpty(Hf_Concepto_ID.Value.Trim()))
                {
                    Mensaje_Error += "&nbsp;&nbsp;* Seleccionar un Concepto. <br />";
                    Datos_Validos = false;
                }

                if (Cmb_FF.SelectedIndex <= 0)
                {
                    Mensaje_Error += "&nbsp;&nbsp;* Seleccionar una fuente de financiamiento. <br />";
                    Datos_Validos = false;
                }

                if(Dt_Mov_Conceptos != null && Dt_Mov_Conceptos.Rows.Count > 0)
                {
                   if(Datos_Validos)
                   {
                       foreach (DataRow Dr in Dt_Mov_Conceptos.Rows)
                       {
                           if (Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim().Equals(Cmb_FF.SelectedItem.Value.Trim()) &&
                               Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim().Equals(Hf_Programa_ID.Value.Trim()) &&
                               Dr["CONCEPTO_ING_ID"].ToString().Trim().Equals(Hf_Concepto_ID.Value.Trim())
                               && Dr["SUBCONCEPTO_ING_ID"].ToString().Trim().Equals(Hf_SubConcepto_ID.Value.Trim()))
                           {
                               Mensaje_Error += "&nbsp;&nbsp;* Seleccionar otro concepto, puesto que este ya fue modificado. <br />";
                               Txt_Importe.Text = String.Empty;
                               Txt_Enero.Text = String.Empty;
                               Txt_Febrero.Text = String.Empty;
                               Txt_Marzo.Text = String.Empty;
                               Txt_Abril.Text = String.Empty;
                               Txt_Mayo.Text = String.Empty;
                               Txt_Junio.Text = String.Empty;
                               Txt_Julio.Text = String.Empty;
                               Txt_Agosto.Text = String.Empty;
                               Txt_Septiembre.Text = String.Empty;
                               Txt_Octubre.Text = String.Empty;
                               Txt_Noviembre.Text = String.Empty;
                               Txt_Diciembre.Text = String.Empty;
                               Txt_Total.Text = String.Empty;
                               Datos_Validos = false;
                               break;
                           }
                       }
                   }
                }

                if (AFU_Archivo.HasFile)
                {
                    Ruta_Destino = Server.MapPath("Archivos/Anexos_Ingresos/" + String.Format("{0:yyyy}", DateTime.Now) + "/");
                    Archivo = AFU_Archivo.FileName;
                    Extension = Archivo.Substring(Archivo.IndexOf(".") + 1);
                    if (Extension == "txt" || Extension == "doc" || Extension == "pdf" || Extension == "docx" || Extension == "jpg"
                        || Extension == "JPG" || Extension == "jpeg" || Extension == "JPEG" || Extension == "GIF" || Extension == "xls"
                        || Extension == "png" || Extension == "PNG" || Extension == "gif" || Extension == "xlsx" || Extension == "rar"
                        || Extension == "zip")
                    {
                        if (!System.IO.Directory.Exists(Ruta_Destino))
                        {
                            System.IO.Directory.CreateDirectory(Ruta_Destino);
                        }
                        
                        if (System.IO.File.Exists(Ruta_Destino + Archivo))
                        {
                            Mensaje_Error += "&nbsp;&nbsp;* Ya exixte un documento con este nombre, favor de cambiarlo <br />";
                            Datos_Validos = false;
                        }
                        else
                        {
                            AFU_Archivo.SaveAs(Ruta_Destino + Archivo);
                            Hf_Ruta_Doc.Value = "../Presupuestos/Archivos/Anexos_Ingresos/" + String.Format("{0:yyyy}", DateTime.Now) + "/" + Archivo.Trim();
                            Hf_Nombre_Doc.Value = Archivo.Trim().Substring(0, Archivo.IndexOf("."));
                        }
                    }
                    else 
                    {
                        Mensaje_Error += "&nbsp;&nbsp;* Solo se pueden cargar archivos con terminacion: pdf, txt, doc, docx, xls, png, gif, jpg <br />";
                        Datos_Validos = false;
                    }
                }

                if (!Datos_Validos) 
                {
                    Mostar_Limpiar_Error(Mensaje_Encabezado, Mensaje_Error, true);
                }

            }
            catch (Exception Ex)
            {
                Mostar_Limpiar_Error(String.Empty, "Error al validar los datos. Error[" + Ex.Message + "]", true);
            }
            return Datos_Validos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Sumar_Total
        ///DESCRIPCIÓN          : Metodo para sumar el total del movimiento
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 18/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Sumar_Total()
        {
            Double Total = 0.00;

            try
            {
                Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Enero.Text.Trim()) ? "0" : Txt_Enero.Text.Trim());
                Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Febrero.Text.Trim()) ? "0" : Txt_Febrero.Text.Trim());
                Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Marzo.Text.Trim()) ? "0" : Txt_Marzo.Text.Trim());
                Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Abril.Text.Trim()) ? "0" : Txt_Abril.Text.Trim());
                Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Mayo.Text.Trim()) ? "0" : Txt_Mayo.Text.Trim());
                Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Junio.Text.Trim()) ? "0" : Txt_Junio.Text.Trim());
                Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Julio.Text.Trim()) ? "0" : Txt_Julio.Text.Trim());
                Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Agosto.Text.Trim()) ? "0" : Txt_Agosto.Text.Trim());
                Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Septiembre.Text.Trim()) ? "0" : Txt_Septiembre.Text.Trim());
                Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Octubre.Text.Trim()) ? "0" : Txt_Octubre.Text.Trim());
                Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Noviembre.Text.Trim()) ? "0" : Txt_Noviembre.Text.Trim());
                Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Diciembre.Text.Trim()) ? "0" : Txt_Diciembre.Text.Trim());

                Txt_Total.Text = String.Format("{0:#,###,##0.00}", Total);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al sumar el total. Error[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Mov_Conceptos
        ///DESCRIPCIÓN          : Metodo para crear las csolumnas del datatable
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 10/Mayo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Crear_Dt_Mov_Conceptos()
        {
            DataTable Dt_Mov_Conceptos = new DataTable();
            try
            {
                Dt_Mov_Conceptos.Columns.Add("MOVIMIENTO_ING_ID", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("CLAVE_NOM_CONCEPTO", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("CLAVE_NOM_PROGRAMA", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("TIPO_MOVIMIENTO", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("IMP_TOTAL", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("ESTIMADO", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("AMPLIACION", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("REDUCCION", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("MODIFICADO", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("ESTATUS", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("RUBRO_ID", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("TIPO_ID", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("CLASE_ING_ID", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("CONCEPTO_ING_ID", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("SUBCONCEPTO_ING_ID", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("IMP_ENE", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("IMP_FEB", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("IMP_MAR", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("IMP_ABR", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("IMP_MAY", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("IMP_JUN", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("IMP_JUL", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("IMP_AGO", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("IMP_SEP", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("IMP_OCT", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("IMP_NOV", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("IMP_DIC", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("JUSTIFICACION", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("COMENTARIO", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("FECHA_MODIFICO", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("USUARIO_MODIFICO", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("TIPO_CONCEPTO", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("PROYECTO_PROGRAMA_ID", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("NOMBRE_DOC", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("RUTA_DOC", System.Type.GetType("System.String"));
                Dt_Mov_Conceptos.Columns.Add("EXTENSION_DOC", System.Type.GetType("System.String"));

                Session["Dt_Mov_Conceptos"] = (DataTable)Dt_Mov_Conceptos;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de crear las columnas de la tabla Error[" + ex.Message + "]");
            }
        }

        //********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Limpiar_Sessiones
        ///DESCRIPCIÓN          : Metodo para limpiar las sessiones del formulario
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 10/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Limpiar_Sessiones()
        {
            Session["Dt_Mov_Conceptos"] = null;
            Session["Dt_Mov_Conceptos"] = new DataTable();
            Grid_Mov_Conceptos.DataSource = new DataTable();
            Grid_Mov_Conceptos.DataBind();
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Agregar_Fila_Dt_Mov_Conceptos
        ///DESCRIPCIÓN          : Metodo para agregar los datos de los movimientos al datatable
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 10/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Agregar_Fila_Dt_Mov_Conceptos()
        {
            DataTable Dt_Mov_Conceptos = new DataTable();
            DataRow Fila;
            Dt_Mov_Conceptos = (DataTable)Session["Dt_Mov_Conceptos"];
            Double Importe = 0.00;
            Double Ampliacion = 0.00;
            Double Reduccion = 0.00;
            Double Modificado = 0.00;
            Double Estimado = 0.00;

            try
            {
                Estimado =  Convert.ToDouble(String.IsNullOrEmpty(Hf_Estimado.Value.Trim()) ? "0" : Hf_Estimado.Value.Trim());
                Ampliacion = Convert.ToDouble(String.IsNullOrEmpty(Hf_Ampliacion.Value.Trim()) ? "0" : Hf_Ampliacion.Value.Trim());
                Reduccion = Convert.ToDouble(String.IsNullOrEmpty(Hf_Reduccion.Value.Trim()) ? "0" : Hf_Reduccion.Value.Trim());
                Modificado =  Convert.ToDouble(String.IsNullOrEmpty(Hf_Modificado.Value.Trim()) ? "0" : Hf_Modificado.Value.Trim());
                Importe = Convert.ToDouble(String.IsNullOrEmpty(Txt_Importe.Text.Trim()) ? "0" : Txt_Importe.Text.Trim());

                Estimado = Estimado + Ampliacion - Reduccion;

                Ampliacion = 0.00;
                Reduccion = 0.00;

                if (Cmb_Operacion.SelectedItem.Text.Trim().Equals("AMPLIACION") || Cmb_Operacion.SelectedItem.Text.Trim().Equals("SUPLEMENTO"))
                {
                    //Ampliacion = Ampliacion + Importe;
                    Ampliacion = Importe;
                }
                else {
                    //Reduccion = Reduccion + Importe;
                    Reduccion = Importe;
                }

                Modificado = Estimado + Ampliacion - Reduccion;

                if (Tr_Programa.Visible)
                {
                    if (String.IsNullOrEmpty(Cmb_Programa.SelectedItem.Value.Trim()))
                    {
                        Hf_Programa_ID.Value = String.Empty;
                        Hf_Clave_Nom_Programa.Value = String.Empty;
                    }
                    else 
                    {
                        Hf_Programa_ID.Value = Cmb_Programa.SelectedItem.Value.Trim();
                        Hf_Clave_Nom_Programa.Value = Cmb_Programa.SelectedItem.Text.Trim();
                    }
                }

                Fila = Dt_Mov_Conceptos.NewRow();
                if (Btn_Nuevo.ToolTip.Equals("Guardar"))
                {
                    Fila["MOVIMIENTO_ING_ID"] = "";
                }
                if (Btn_Modificar.ToolTip.Equals("Actualizar"))
                {
                    Fila["MOVIMIENTO_ING_ID"] = Convert.ToInt32("0");
                }
                Fila["CLAVE_NOM_CONCEPTO"] = Hf_Clave_Nom_Concepto.Value.Trim();
                Fila["CLAVE_NOM_PROGRAMA"] = Hf_Clave_Nom_Programa.Value.Trim();
                if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("SUPLEMENTO"))
                {
                    Fila["TIPO_MOVIMIENTO"] = "AMPLIACION";
                }
                else 
                {
                    Fila["TIPO_MOVIMIENTO"] = Cmb_Operacion.SelectedItem.Value.Trim();
                }
                Fila["IMP_TOTAL"] = String.Format("{0:#,###,##0.00}", Importe);
                Fila["ESTIMADO"] = String.Format("{0:#,###,##0.00}", Estimado);
                Fila["AMPLIACION"] = String.Format("{0:#,###,##0.00}", Ampliacion);
                Fila["REDUCCION"] = String.Format("{0:#,###,##0.00}", Reduccion);
                Fila["MODIFICADO"] = String.Format("{0:#,###,##0.00}", Modificado);
                Fila["ESTATUS"] = Cmb_Estatus.SelectedItem.Text.Trim();
                Fila["RUBRO_ID"] = Hf_Rubro_ID.Value.Trim();
                Fila["TIPO_ID"] = Hf_Tipo_ID.Value.Trim();
                Fila["CLASE_ING_ID"] = Hf_Clase_ID.Value.Trim();
                Fila["CONCEPTO_ING_ID"] = Hf_Concepto_ID.Value.Trim();
                Fila["SUBCONCEPTO_ING_ID"] = Hf_SubConcepto_ID.Value.Trim();
                Fila["FUENTE_FINANCIAMIENTO_ID"] = Cmb_FF.SelectedItem.Value.Trim();
                Fila["IMP_ENE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Enero.Text.Trim()) ? "0" : Txt_Enero.Text.Trim()));
                Fila["IMP_FEB"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Febrero.Text.Trim()) ? "0" : Txt_Febrero.Text.Trim()));
                Fila["IMP_MAR"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Marzo.Text.Trim()) ? "0" : Txt_Marzo.Text.Trim()));
                Fila["IMP_ABR"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Abril.Text.Trim()) ? "0" : Txt_Abril.Text.Trim()));
                Fila["IMP_MAY"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Mayo.Text.Trim()) ? "0" : Txt_Mayo.Text.Trim()));
                Fila["IMP_JUN"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Junio.Text.Trim()) ? "0" : Txt_Junio.Text.Trim()));
                Fila["IMP_JUL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Julio.Text.Trim()) ? "0" : Txt_Julio.Text.Trim()));
                Fila["IMP_AGO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Agosto.Text.Trim()) ? "0" : Txt_Agosto.Text.Trim()));
                Fila["IMP_SEP"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Septiembre.Text.Trim()) ? "0" : Txt_Septiembre.Text.Trim()));
                Fila["IMP_OCT"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Octubre.Text.Trim()) ? "0" : Txt_Octubre.Text.Trim()));
                Fila["IMP_NOV"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Noviembre.Text.Trim()) ? "0" : Txt_Noviembre.Text.Trim()));
                Fila["IMP_DIC"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Diciembre.Text.Trim()) ? "0" : Txt_Diciembre.Text.Trim()));
                Fila["JUSTIFICACION"] = Txt_Justificacion.Text.Trim();
                Fila["COMENTARIO"] = "";
                if (Btn_Nuevo.ToolTip.Equals("Guardar"))
                {
                    Fila["FECHA_MODIFICO"] = "";
                }
                if (Btn_Modificar.ToolTip.Equals("Actualizar"))
                {
                    Fila["FECHA_MODIFICO"] = DateTime.Now;
                }
                Fila["USUARIO_MODIFICO"] = "";
                Fila["TIPO_CONCEPTO"] = Hf_Tipo_Concepto.Value.Trim();
                Fila["PROYECTO_PROGRAMA_ID"] = Hf_Programa_ID.Value.Trim();
                if (!String.IsNullOrEmpty(Hf_Nombre_Doc.Value.Trim()))
                {
                    Fila["NOMBRE_DOC"] = Hf_Nombre_Doc.Value.Trim();
                    Fila["RUTA_DOC"] = Hf_Ruta_Doc.Value.Trim();
                    Fila["EXTENSION_DOC"] = Hf_Ruta_Doc.Value.Substring(Hf_Ruta_Doc.Value.LastIndexOf(".") + 1);
                }
                else 
                {
                    Fila["NOMBRE_DOC"] = String.Empty;
                    Fila["RUTA_DOC"] = String.Empty;
                    Fila["EXTENSION_DOC"] = String.Empty;
                }
                Dt_Mov_Conceptos.Rows.Add(Fila);

                Session["Dt_Mov_Conceptos"] = (DataTable)Dt_Mov_Conceptos;
                Obtener_Consecutivo_ID();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de crear las columnas de la tabla Error[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Consecutivo_ID
        ///DESCRIPCIÓN          : Metodo para obtener el consecutivo del datatable 
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 10/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Obtener_Consecutivo_ID()
        {
            DataTable Dt_Mov_Conceptos = new DataTable();
            Dt_Mov_Conceptos = (DataTable)Session["Dt_Mov_Conceptos"];
            int Contador;
            try
            {
                if (Dt_Mov_Conceptos != null)
                {
                    if (Dt_Mov_Conceptos.Columns.Count > 0)
                    {
                        if (Dt_Mov_Conceptos.Rows.Count > 0)
                        {
                            Contador = 0;
                            foreach (DataRow Dr in Dt_Mov_Conceptos.Rows)
                            {
                                Contador++;
                                Dr["MOVIMIENTO_ING_ID"] = Contador.ToString().Trim();
                            }
                        }
                    }
                }
                Session["Dt_Mov_Conceptos"] = Dt_Mov_Conceptos;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al crear el datatable con consecutivo Erro[" + ex.Message + "]");
            }
        }

        //********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Calcular_Total_Modificado
        ///DESCRIPCIÓN          : Metodo para calcular el total que se lleva modificado
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 10/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Calcular_Total_Modificado()
        {
            DataTable Dt_Mov_Conceptos = new DataTable();
            Dt_Mov_Conceptos = (DataTable)Session["Dt_Mov_Conceptos"];
            Double Total = 0.00;

            try
            {
                if (Dt_Mov_Conceptos != null)
                {
                    if (Dt_Mov_Conceptos.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Mov_Conceptos.Rows)
                        {
                            Total = Total + Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_TOTAL"].ToString().Trim()) ? "0" : Dr["IMP_TOTAL"].ToString().Trim());
                        }
                    }
                }
                Txt_Total_Modificado.Text = "";
                Txt_Total_Modificado.Text = String.Format("{0:c}", Total);

             }
            catch (Exception ex)
            {
                throw new Exception("Error al calcular el total de modificado. Error[" + ex.Message + "]");
            }

        }

        //********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Atualizar_Datos
        ///DESCRIPCIÓN          : Metodo para actualizar los datos de autorizado o rechazado
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 11/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Actualizar_Datos()
        {
            Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio();
            Boolean Autorizado;
            Boolean Rechazado;
            Boolean Cancelado;
            Boolean Aceptado;
            String Autorizados = String.Empty;
            String Estatus = String.Empty;

            try
            {
                foreach (GridViewRow Renglon_Grid in Grid_Mov_Conceptos.Rows)
                {
                    if (Hf_Estatus.Value.Trim().Equals("PREAUTORIZADO"))
                    {
                        Estatus = "AUTORIZADO";
                        Autorizado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Autorizado")).Checked;
                        if (!Autorizado)
                        {
                            Rechazado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Rechazado")).Checked;
                            if (!Rechazado)
                            {
                                Lbl_Mensaje_Error.Text = "Favor de Autorizar o Rechazar todos los movimientos, ya que existen movimientos sin seleccionar";
                                Img_Error.Visible = true;
                                Lbl_Mensaje_Error.Visible = true;
                                return;
                            }
                        }
                        else
                        {
                            Rechazado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Rechazado")).Checked;
                            if (Rechazado)
                            {
                                Lbl_Mensaje_Error.Text = "Favor de solo Autorizar o Rechazar los movimientos, ya que existen movimientos que se seleccionaron ambas opciones";
                                Img_Error.Visible = true;
                                Lbl_Mensaje_Error.Visible = true;
                                return;
                            }
                        }
                    }
                    else
                    {
                        Estatus = "GENERADO";
                        Aceptado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Aceptar")).Checked;
                        if (Aceptado)
                        {
                            Cancelado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Cancelado")).Checked;
                            if (Cancelado)
                            {
                                Lbl_Mensaje_Error.Text = "Favor de solo Aceptar o Cancelar los movimientos, ya que existen movimientos que se seleccionaron ambas opciones";
                                Img_Error.Visible = true;
                                Lbl_Mensaje_Error.Visible = true;
                                return;
                            }
                        }

                        if (Chk_Autorizar.Checked)
                        {
                            Estatus = "PREAUTORIZADO";
                            Aceptado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Aceptar")).Checked;
                            if (!Aceptado)
                            {
                                Cancelado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Cancelado")).Checked;
                                if (!Cancelado)
                                {
                                    Lbl_Mensaje_Error.Text = "Favor de Aceptar o Cancelar todos los movimientos, ya que existen movimientos sin seleccionar";
                                    Img_Error.Visible = true;
                                    Lbl_Mensaje_Error.Visible = true;
                                    return;
                                }
                            }
                        }
                    }
                }

                Negocio.P_Estatus = Estatus.Trim();
                Negocio.P_Dt_Mov_Autorizados = Crear_Dt_Autorizados();
                Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                Negocio.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.Trim();
                Negocio.P_No_Movimiento_Ing = Hf_Movimiento_ID.Value.Trim();
                Negocio.P_Dt_Mov_Conceptos = (DataTable)Session["Dt_Mov_Conceptos"];

                Autorizados = Negocio.Modificar_Movimientos();
                if (Autorizados.Trim().Equals("SI"))
                {
                    Generar_Reporte_Modificaciones(Hf_Movimiento_ID.Value.Trim(), String.Format("{0:yyyy}", DateTime.Now), Estatus.Trim());

                    Configuracion_Inicial();
                    Grid_Movimiento.SelectedIndex = -1;
                    Limpiar_Sessiones();
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alta", "alert('Operacion Completa');", true);
                }
                else 
                {
                    Generar_Reporte_Modificaciones(Hf_Movimiento_ID.Value.Trim(), String.Format("{0:yyyy}", DateTime.Now), Estatus.Trim());

                    Configuracion_Inicial();
                    Grid_Movimiento.SelectedIndex = -1;
                    Limpiar_Sessiones();
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alta", "alert('" + Autorizados + "');", true);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al actualizar los datos. Error[" + ex.Message + "]");
            }
        }

        //********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Autorizados
        ///DESCRIPCIÓN          : Metodo para crear el datatable con los datos de la actualizacion
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 11/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private DataTable Crear_Dt_Autorizados()
        {
            DataTable Dt_Autorizados = new DataTable();
            Int32 Indice = 0;
            Boolean Autorizado;
            Boolean Rechazado;
            Boolean Cancelado;
            Boolean Aceptado;
            DataRow Fila;
            DataTable Dt_Comentarios = new DataTable();

            try
            {
                Dt_Comentarios = Obtener_Comentarios();
                Dt_Autorizados.Columns.Add("MOVIMIENTO_ING_ID");
                Dt_Autorizados.Columns.Add("ESTATUS");
                Dt_Autorizados.Columns.Add("COMENTARIO");

                foreach (GridViewRow Renglon_Grid in Grid_Mov_Conceptos.Rows)
                {
                    Indice++;
                    Grid_Mov_Conceptos.SelectedIndex = Indice;


                    if (Hf_Estatus.Value.Trim().Equals("PREAUTORIZADO"))
                    {
                        Autorizado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Autorizado")).Checked;
                        if (Autorizado)
                        {
                            Fila = Dt_Autorizados.NewRow();
                            Fila["MOVIMIENTO_ING_ID"] = Renglon_Grid.Cells[1].Text.Trim();
                            Fila["ESTATUS"] = "AUTORIZADO";
                            Fila["COMENTARIO"] = String.Empty;
                            Dt_Autorizados.Rows.Add(Fila);
                        }
                        else
                        {
                            Rechazado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Rechazado")).Checked;
                            if (Rechazado)
                            {
                                Fila = Dt_Autorizados.NewRow();
                                Fila["MOVIMIENTO_ING_ID"] = Renglon_Grid.Cells[1].Text.Trim();
                                Fila["ESTATUS"] = "RECHAZADO";
                                if (Dt_Comentarios != null && Dt_Comentarios.Rows.Count > 0)
                                {
                                    foreach (DataRow Dr_Com in Dt_Comentarios.Rows)
                                    {
                                        if (Dr_Com["MOVIMIENTO_ING_ID"].ToString().Trim().Equals(Renglon_Grid.Cells[1].Text.Trim()))
                                        {
                                            Fila["COMENTARIO"] = Dr_Com["COMENTARIO"].ToString().Trim();
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    Fila["COMENTARIO"] = String.Empty;
                                }
                                Dt_Autorizados.Rows.Add(Fila);
                            }
                        }
                    }
                    else
                    {
                        Aceptado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Aceptar")).Checked;
                        if (Aceptado)
                        {
                            Fila = Dt_Autorizados.NewRow();
                            Fila["MOVIMIENTO_ING_ID"] = Renglon_Grid.Cells[1].Text.Trim();
                            Fila["ESTATUS"] = "ACEPTADO";
                            Fila["COMENTARIO"] = String.Empty;
                            Dt_Autorizados.Rows.Add(Fila);
                        }
                        else
                        {
                            Cancelado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Cancelado")).Checked;
                            if (Cancelado)
                            {
                                Fila = Dt_Autorizados.NewRow();
                                Fila["MOVIMIENTO_ING_ID"] = Renglon_Grid.Cells[1].Text.Trim();
                                Fila["ESTATUS"] = "CANCELADO";
                                if (Dt_Comentarios != null && Dt_Comentarios.Rows.Count > 0)
                                {
                                    foreach (DataRow Dr_Com in Dt_Comentarios.Rows)
                                    {
                                        if (Dr_Com["MOVIMIENTO_ING_ID"].ToString().Trim().Equals(Renglon_Grid.Cells[1].Text.Trim()))
                                        {
                                            Fila["COMENTARIO"] = Dr_Com["COMENTARIO"].ToString().Trim();
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    Fila["COMENTARIO"] = String.Empty;
                                }
                                Dt_Autorizados.Rows.Add(Fila);
                            }
                        }
                    }
                }

                return Dt_Autorizados;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al actualizar los datos. Error[" + ex.Message + "]");
            }
        }

        //********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Comentarios
        ///DESCRIPCIÓN          : Metodo para crear el datatable con los comentarios
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 11/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private DataTable Obtener_Comentarios()
        {
            String Comentarios = String.Empty;
            DataTable Dt_Comentarios = new DataTable();
            DataRow Fila;
            String[] Coment;
            String[] Comenta;

            try
            {
                Comentarios = Hf_No_Rechazado.Value.Trim();
                Dt_Comentarios.Columns.Add("MOVIMIENTO_ING_ID");
                Dt_Comentarios.Columns.Add("COMENTARIO");

                if (!String.IsNullOrEmpty(Comentarios))
                {
                    Coment = Comentarios.Split('-');

                    for (int i = 0; i <= Coment.Length - 2; i++)
                    {
                        Comenta = Coment[i].Split(';');
                        Fila = Dt_Comentarios.NewRow();
                        Fila["MOVIMIENTO_ING_ID"] = Comenta[0].ToString().Trim();
                        Fila["COMENTARIO"] = Comenta[1].ToString().Trim();
                        Dt_Comentarios.Rows.Add(Fila);
                    }
                }

                return Dt_Comentarios;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al actualizar los datos. Error[" + ex.Message + "]");
            }
        }

        //********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Validar_Mov
        ///DESCRIPCIÓN          : Metodo para validar los consecutivos de los movimientos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 07/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private Boolean Validar_Mov()
        {
            Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio();
            Boolean Valido = true;
            DataTable Dt_Mov = new DataTable();
            String Estatus_Ing = String.Empty;
            String Estatus_Egr = String.Empty;
            String Mov_Ing = String.Empty;
            String Mov_Egr = String.Empty;

            try
            {
                Dt_Mov = Negocio.Consultar_Mov_Egresos();

                if(Dt_Mov != null && Dt_Mov.Rows.Count > 0)
                {
                    foreach (DataRow Dr in Dt_Mov.Rows)
                    {
                        if (Dr["TIPO"].ToString().Trim().Equals("INGRESOS"))
                        {
                            Estatus_Ing = Dr["ESTATUS"].ToString().Trim();
                            Mov_Ing = Dr["NO_MOVIMIENTO"].ToString().Trim();
                        }
                        else 
                        {
                            Estatus_Egr = Dr["ESTATUS"].ToString().Trim();
                            Mov_Egr = Dr["NO_MOVIMIENTO"].ToString().Trim();
                        }
                    }

                    if (Mov_Egr.Equals(Mov_Ing))
                    {
                        if (Estatus_Egr.Equals(Estatus_Ing))
                        {
                            Valido = true;
                        }
                    }
                    else 
                    {

                    }

                }

                return Valido;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al validar los datos. Error[" + ex.Message + "]");
            }
        }

        //********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Autorizar_Movimientos_Ramo33
        ///DESCRIPCIÓN          : Metodo para autorizar los movimientos de ramo 33
        ///PROPIEDADES          1 No_Modificacion: numero de la modificacion donde se guardaron los movimientos
        ///                     2 Anio: año de la modificación
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 23/Noviembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Autorizar_Movimientos_Ramo33(String No_Modificacion, String Anio)
        {
            Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio(); //conexion con la capa de negocios
            DataTable Dt_Mov_Conceptos = new DataTable();
            DataTable Dt_Mov_Ramo33 = new DataTable();

            try 
	        {
                //consultamos los datos de los movimientos
                Negocio.P_Anio = Anio.Trim();
                Negocio.P_No_Movimiento_Ing = No_Modificacion.Trim();
                Negocio.P_Estatus = "'GENERADO'";
                Dt_Mov_Conceptos = Negocio.Consultar_Movimientos();

                if (Dt_Mov_Conceptos != null)
                {
                    if (Dt_Mov_Conceptos.Rows.Count > 0)
                    {
                        //obtenemos los movimientos que fueron de dependencias de ramo 33
                        Dt_Mov_Ramo33 = (from Fila in Dt_Mov_Conceptos.AsEnumerable()
                                           where Fila.Field<string>(Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33) == "SI"
                                           select Fila).AsDataView().ToTable();

                        //validamos que se tengan registros para autorizar de ramo33
                        if (Dt_Mov_Ramo33 != null)
                        {
                            if (Dt_Mov_Ramo33.Rows.Count > 0)
                            {
                                //cambiamos el estatus de Generado a Autorizado
                                foreach (DataRow Dr in Dt_Mov_Ramo33.Rows)
                                {
                                    Dr["ESTATUS"] = "AUTORIZADO";
                                }

                                //ACTUALIZAMOS LOS DATOS DE LOS MOVIMIENTOS DE RAMO 33
                                Negocio.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.Trim();
                                Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                                Negocio.P_Dt_Mov_Conceptos = Dt_Mov_Ramo33;
                                Negocio.P_No_Movimiento_Ing = No_Modificacion.Trim();

                                Negocio.Alta_Movimientos_Ramo33();

                                Crear_Reporte(Dt_Mov_Ramo33);
                            }
                        }
                    }
                }

	        }
	        catch (Exception Ex)
	        {
		        throw new Exception("Error al querer autorizar los movimientos de Ramo 33. Error[" + Ex.Message + "]");
	        }
        }

        //********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Generar_Reporte_Modificaciones
        ///DESCRIPCIÓN          : Metodo para autorizar los movimientos de ramo 33
        ///PROPIEDADES          1 No_Modificacion: numero de la modificacion donde se guardaron los movimientos
        ///                     2 Anio: año de la modificación
        ///                     3 Estatus: estatus de los movimientos
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 24/Noviembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Generar_Reporte_Modificaciones(String No_Modificacion, String Anio, String Estatus)
        {
            Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio(); //conexion con la capa de negocios
            DataTable Dt_Mov_Conceptos = new DataTable();

            try
            {
                //consultamos los datos de los movimientos
                Negocio.P_Anio = Anio.Trim();
                Negocio.P_No_Movimiento_Ing = No_Modificacion.Trim();
                Negocio.P_Estatus = "'" + Estatus + "'";
                Dt_Mov_Conceptos = Negocio.Consultar_Movimientos();

                if (Dt_Mov_Conceptos != null)
                {
                    if (Dt_Mov_Conceptos.Rows.Count > 0)
                    {
                        Crear_Reporte(Dt_Mov_Conceptos);
                    }
                }

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar el reporte de los movimientos. Error[" + Ex.Message + "]");
            }
        }
    #endregion

    #region (Combos / Grid)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Estatus
        ///DESCRIPCIÓN          : Metodo para llenar el combo de estatus
        ///PARAMETROS           :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 17/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Llenar_Combo_Estatus()
        {
            Cmb_Estatus.Items.Clear();
            Cmb_Estatus.Items.Insert(0, new ListItem("GENERADO", "GENERADO"));
            Cmb_Estatus.Items.Insert(1, new ListItem("RECHAZADO", "RECHAZADO"));
            Cmb_Estatus.Items.Insert(2, new ListItem("AUTORIZADO", "AUTORIZADO"));
            Cmb_Estatus.DataBind();
            Cmb_Estatus.SelectedIndex = -1;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Tipo_Solicitud
        ///DESCRIPCIÓN          : Metodo para llenar el combo del tipo de solicitud
        ///PARAMETROS           :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Julio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Llenar_Combo_Tipo_Solicitud()
        {
            Cmb_Tipo_Solicitud.Items.Clear();
            Cmb_Tipo_Solicitud.Items.Insert(0, new ListItem("<--SELECCIONE-->", ""));
            Cmb_Tipo_Solicitud.Items.Insert(1, new ListItem("AMPLIACION", "AMPLIACION"));
            Cmb_Tipo_Solicitud.Items.Insert(2, new ListItem("REDUCCION", "REDUCCION"));
            Cmb_Tipo_Solicitud.DataBind();
            Cmb_Tipo_Solicitud.SelectedIndex = -1;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Movimientos
        ///DESCRIPCIÓN          : Metodo para llenar el grid de los movimientos
        ///PARAMETROS           :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 17/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Llenar_Grid_Movimientos()
        {
            Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio(); //conexion con la capa de negocios
            DataTable Dt_Movimienos = new DataTable();

            try
            {

                Dt_Movimienos = Negocio.Consultar_Modificaciones();
                if (Dt_Movimienos != null)
                {
                    Grid_Movimiento.Columns[6].Visible = true;
                    Grid_Movimiento.DataSource = Dt_Movimienos;
                    Grid_Movimiento.DataBind();
                    Grid_Movimiento.Columns[6].Visible = false;
                }
                else
                {
                    Grid_Movimiento.DataSource = new DataTable();
                }

                //PRUEBA PARA POLIZAS DE DEVENGADO Y RECAUDADO NORMAL
                //DataTable Dt_Psp = new DataTable();
                //DataRow Fila;
                //Dt_Psp.Columns.Add("Fte_Financiamiento_ID", System.Type.GetType("System.String"));
                //Dt_Psp.Columns.Add("Proyecto_Programa_ID", System.Type.GetType("System.String"));
                //Dt_Psp.Columns.Add("Concepto_Ing_ID", System.Type.GetType("System.String"));
                //Dt_Psp.Columns.Add("SubConcepto_Ing_ID", System.Type.GetType("System.String"));
                //Dt_Psp.Columns.Add("Anio", System.Type.GetType("System.String"));
                //Dt_Psp.Columns.Add("Importe", System.Type.GetType("System.String"));
               
                //los id el el servidor 83 son: para la fuente de financiamiento: 00022 y para el programa: 0000000727
                //en el servidor 206 son: para la fuente de financiamiento: 00022 y para el programa: 0000000654

                //*****************************MOVIMIENTO de devengado con dos conceptos********************************************//
                //Fila = Dt_Psp.NewRow();
                //Fila["Fte_Financiamiento_ID"] = "00022"; //id correspondiente a la fuente de financiamiento con clave CP12
                //Fila["Proyecto_Programa_ID"] = "0000000727"; //id correspondiente al programa con clave CP12
                //Fila["Concepto_Ing_ID"] = "0000000098"; //impuesto predial
                //Fila["SubConcepto_Ing_ID"] = "";//impuesto predial urbano corriente
                //Fila["Anio"] = "2012"; //anio que se afectara
                //Fila["Importe"] = "5000";
                //Dt_Psp.Rows.Add(Fila);

                //Cls_Ope_Con_Poliza_Ingresos_Datos.Alta_Movimientos_Presupuestales(Dt_Psp, null, "prueba leslie, xrec-dev ", "0000000001", "00001",
                //    "0912", Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar, Ope_Psp_Presupuesto_Ingresos.Campo_Devengado);

                ////**********************MOVIMIENTO de recaudado con los dos conceptos devengados anteriormente*************************//
                //Dt_Psp = new DataTable();
                //Dt_Psp.Columns.Add("Fte_Financiamiento_ID", System.Type.GetType("System.String"));
                //Dt_Psp.Columns.Add("Proyecto_Programa_ID", System.Type.GetType("System.String"));
                //Dt_Psp.Columns.Add("Concepto_Ing_ID", System.Type.GetType("System.String"));
                //Dt_Psp.Columns.Add("SubConcepto_Ing_ID", System.Type.GetType("System.String"));
                //Dt_Psp.Columns.Add("Anio", System.Type.GetType("System.String"));
                //Dt_Psp.Columns.Add("Importe", System.Type.GetType("System.String"));

                //Fila = Dt_Psp.NewRow();
                //Fila["Fte_Financiamiento_ID"] = "00022"; //id correspondiente a la fuente de financiamiento con clave CP12
                //Fila["Proyecto_Programa_ID"] = "0000000727"; //id correspondiente al programa con clave CP12
                //Fila["Concepto_Ing_ID"] = "0000000098"; //impuesto predial
                //Fila["SubConcepto_Ing_ID"] = "";//impuesto predial urbano corriente
                //Fila["Anio"] = "2012"; //anio que se afectara
                //Fila["Importe"] = "5000";
                //Dt_Psp.Rows.Add(Fila);


                //Cls_Ope_Con_Poliza_Ingresos_Datos.Alta_Movimientos_Presupuestales(Dt_Psp, null, "prueba leslie, dev_rec", "0000000001", "00001",
                //    "0912", Ope_Psp_Presupuesto_Ingresos.Campo_Devengado, Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado);

                //Cls_Ope_Con_Poliza_Ingresos_Datos.Alta_Movimientos_Presupuestales(Dt_Psp, null, "prueba leslie, xrec_rec", "0000000001", "00001",
                //    "0912", Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar, Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado);


                /************* MOVIMIENTO DE CANCELADO  DE DEVENGADO **************/
                //Dt_Psp = new DataTable();
                //Dt_Psp.Columns.Add("Fte_Financiamiento_ID", System.Type.GetType("System.String"));
                //Dt_Psp.Columns.Add("Proyecto_Programa_ID", System.Type.GetType("System.String"));
                //Dt_Psp.Columns.Add("Concepto_Ing_ID", System.Type.GetType("System.String"));
                //Dt_Psp.Columns.Add("SubConcepto_Ing_ID", System.Type.GetType("System.String"));
                //Dt_Psp.Columns.Add("Anio", System.Type.GetType("System.String"));
                //Dt_Psp.Columns.Add("Importe", System.Type.GetType("System.String"));

                //Fila = Dt_Psp.NewRow();
                //Fila["Fte_Financiamiento_ID"] = "00022"; //id correspondiente a la fuente de financiamiento con clave CP12
                //Fila["Proyecto_Programa_ID"] = "0000000727"; //id correspondiente al programa con clave CP12
                //Fila["Concepto_Ing_ID"] = "0000000098"; //impuesto predial
                //Fila["SubConcepto_Ing_ID"] = "";//impuesto predial urbano corriente
                //Fila["Anio"] = "2012"; //anio que se afectara
                //Fila["Importe"] = "5000";
                //Dt_Psp.Rows.Add(Fila);

                //Cls_Ope_Con_Poliza_Ingresos_Datos.Alta_Movimientos_Presupuestales(Dt_Psp, null, "prueba leslie, xrec-dev ", "0000000001", "00001",
                //    "0912", Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado, Ope_Psp_Presupuesto_Ingresos.Campo_Devengado);

                //**********************MOVIMIENTO de recaudado con los dos conceptos devengados anteriormente*************************//
                //Dt_Psp = new DataTable();
                //Dt_Psp.Columns.Add("Fte_Financiamiento_ID", System.Type.GetType("System.String"));
                //Dt_Psp.Columns.Add("Proyecto_Programa_ID", System.Type.GetType("System.String"));
                //Dt_Psp.Columns.Add("Concepto_Ing_ID", System.Type.GetType("System.String"));
                //Dt_Psp.Columns.Add("SubConcepto_Ing_ID", System.Type.GetType("System.String"));
                //Dt_Psp.Columns.Add("Anio", System.Type.GetType("System.String"));
                //Dt_Psp.Columns.Add("Importe", System.Type.GetType("System.String"));

                //Fila = Dt_Psp.NewRow();
                //Fila["Fte_Financiamiento_ID"] = "00022"; //id correspondiente a la fuente de financiamiento con clave CP12
                //Fila["Proyecto_Programa_ID"] = "0000000727"; //id correspondiente al programa con clave CP12
                //Fila["Concepto_Ing_ID"] = "0000000098"; //impuesto predial
                //Fila["SubConcepto_Ing_ID"] = "";//impuesto predial urbano corriente
                //Fila["Anio"] = "2012"; //anio que se afectara
                //Fila["Importe"] = "5000";
                //Dt_Psp.Rows.Add(Fila);

                //Cls_Ope_Con_Poliza_Ingresos_Datos.Alta_Movimientos_Presupuestales(Dt_Psp, null, "prueba leslie, dev_rec", "0000000001", "00001",
                //    "0912", Ope_Psp_Presupuesto_Ingresos.Campo_Devengado, Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar);

                //Cls_Ope_Con_Poliza_Ingresos_Datos.Alta_Movimientos_Presupuestales(Dt_Psp, null, "prueba leslie, xrec_rec", "0000000001", "00001",
                //    "0912", Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado, Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar);

            }
            catch (Exception Ex)
            {
                Mostar_Limpiar_Error(String.Empty, "Error al cargar la tabla de Movimientos. Error[" + Ex.Message + "]", true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Movimientos
        ///DESCRIPCIÓN          : Metodo para llenar el grid de los movimientos
        ///PARAMETROS           :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 17/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Llenar_Grid_Mov_Conceptos()
        {
            Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio(); //conexion con la capa de negocios
            DataTable Dt_Mov_Conceptos = new DataTable();
            String Estatus = String.Empty;

            Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
            if (!String.IsNullOrEmpty(Hf_Movimiento_ID.Value.Trim()))
            {
                Negocio.P_No_Movimiento_Ing = Hf_Movimiento_ID.Value.Trim();
                Estatus = Negocio.Consultar_Estatus_Mov_Ing();
            }

            try
            {
                Dt_Mov_Conceptos = (DataTable)Session["Dt_Mov_Conceptos"];
                if (Dt_Mov_Conceptos != null)
                {
                    Grid_Mov_Conceptos.Columns[0].Visible = true;
                    Grid_Mov_Conceptos.Columns[3].Visible = true;/*ocultar programa*/
                    Grid_Mov_Conceptos.Columns[10].Visible = true;
                    Grid_Mov_Conceptos.Columns[11].Visible = true;
                    Grid_Mov_Conceptos.Columns[12].Visible = true;
                    Grid_Mov_Conceptos.Columns[13].Visible = true;
                    Grid_Mov_Conceptos.Columns[14].Visible = true;
                    Grid_Mov_Conceptos.Columns[15].Visible = true;
                    Grid_Mov_Conceptos.Columns[16].Visible = true;
                    Grid_Mov_Conceptos.Columns[17].Visible = true;
                    Grid_Mov_Conceptos.Columns[18].Visible = true;
                    Grid_Mov_Conceptos.Columns[19].Visible = true;
                    Grid_Mov_Conceptos.Columns[20].Visible = true;
                    Grid_Mov_Conceptos.Columns[21].Visible = true;
                    Grid_Mov_Conceptos.Columns[22].Visible = true;
                    Grid_Mov_Conceptos.Columns[23].Visible = true;
                    Grid_Mov_Conceptos.Columns[24].Visible = true;
                    Grid_Mov_Conceptos.Columns[25].Visible = true;
                    Grid_Mov_Conceptos.Columns[26].Visible = true;
                    Grid_Mov_Conceptos.Columns[27].Visible = true;
                    Grid_Mov_Conceptos.Columns[28].Visible = true;
                    Grid_Mov_Conceptos.Columns[29].Visible = true;
                    Grid_Mov_Conceptos.Columns[30].Visible = true;
                    Grid_Mov_Conceptos.Columns[31].Visible = true;
                    Grid_Mov_Conceptos.Columns[32].Visible = true;
                    Grid_Mov_Conceptos.Columns[33].Visible = true;
                    Grid_Mov_Conceptos.Columns[34].Visible = true;
                    Grid_Mov_Conceptos.Columns[35].Visible = true;
                    Grid_Mov_Conceptos.Columns[36].Visible = true;
                    Grid_Mov_Conceptos.Columns[37].Visible = true;
                    Grid_Mov_Conceptos.Columns[38].Visible = true;
                    Grid_Mov_Conceptos.Columns[39].Visible = true;
                    Grid_Mov_Conceptos.Columns[40].Visible = true;
                    Grid_Mov_Conceptos.Columns[41].Visible = true;
                    Grid_Mov_Conceptos.Columns[42].Visible = true;
                    Grid_Mov_Conceptos.Columns[43].Visible = true;
                    Grid_Mov_Conceptos.DataSource = Dt_Mov_Conceptos;
                    Grid_Mov_Conceptos.DataBind();
                    Grid_Mov_Conceptos.Columns[3].Visible = false;/*ocultar programa*/
                    Grid_Mov_Conceptos.Columns[10].Visible = false;
                    Grid_Mov_Conceptos.Columns[11].Visible = false;
                    Grid_Mov_Conceptos.Columns[12].Visible = false;
                    Grid_Mov_Conceptos.Columns[13].Visible = false;
                    Grid_Mov_Conceptos.Columns[14].Visible = false;
                    Grid_Mov_Conceptos.Columns[15].Visible = false;
                    Grid_Mov_Conceptos.Columns[16].Visible = false;
                    Grid_Mov_Conceptos.Columns[17].Visible = false;
                    Grid_Mov_Conceptos.Columns[18].Visible = false;
                    Grid_Mov_Conceptos.Columns[19].Visible = false;
                    Grid_Mov_Conceptos.Columns[20].Visible = false;
                    Grid_Mov_Conceptos.Columns[21].Visible = false;
                    Grid_Mov_Conceptos.Columns[22].Visible = false;
                    Grid_Mov_Conceptos.Columns[23].Visible = false;
                    Grid_Mov_Conceptos.Columns[24].Visible = false;
                    Grid_Mov_Conceptos.Columns[25].Visible = false;
                    Grid_Mov_Conceptos.Columns[26].Visible = false;
                    Grid_Mov_Conceptos.Columns[27].Visible = false;
                    Grid_Mov_Conceptos.Columns[28].Visible = false;
                    Grid_Mov_Conceptos.Columns[29].Visible = false;
                    Grid_Mov_Conceptos.Columns[30].Visible = false;
                    Grid_Mov_Conceptos.Columns[31].Visible = false;
                    Grid_Mov_Conceptos.Columns[32].Visible = false;
                    Grid_Mov_Conceptos.Columns[33].Visible = false;
                    Grid_Mov_Conceptos.Columns[34].Visible = false;
                    Grid_Mov_Conceptos.Columns[36].Visible = false;
                    Grid_Mov_Conceptos.Columns[41].Visible = false;
                    Grid_Mov_Conceptos.Columns[42].Visible = false;
                    Grid_Mov_Conceptos.Columns[43].Visible = false;

                    if (Btn_Nuevo.ToolTip.Trim().Equals("Guardar") || Btn_Modificar.ToolTip.Trim().Equals("Actualizar")) 
                    {
                        Grid_Mov_Conceptos.Columns[0].Visible = false;
                        Grid_Mov_Conceptos.Columns[37].Visible = false;
                        Grid_Mov_Conceptos.Columns[38].Visible = false;
                        Grid_Mov_Conceptos.Columns[39].Visible = false;
                        Grid_Mov_Conceptos.Columns[40].Visible = false;
                    }
                    else  if (Btn_Nuevo.ToolTip.Trim().Equals("Actualizar"))
                    {
                        Grid_Mov_Conceptos.Columns[0].Visible = false;
                        Grid_Mov_Conceptos.Columns[35].Visible = false;
                        if (Estatus.Trim().Equals("GENERADO"))
                        {
                            Grid_Mov_Conceptos.Columns[39].Visible = false;
                            Grid_Mov_Conceptos.Columns[40].Visible = false;
                            Lbl_Autorizacion.Text = "Cierre Modificación";
                            Chk_Autorizar.Checked = false;
                            Chk_Aceptacion.Checked = false;
                            Chk_Aceptacion.Visible = true;
                            Lbl_Aceptar.Visible = true;

                            if (Dt_Mov_Conceptos is DataTable)
                            {
                                if (Dt_Mov_Conceptos.Rows.Count > 0)
                                {
                                    foreach (DataRow Dr in Dt_Mov_Conceptos.Rows)
                                    {
                                        if (Dr is DataRow)
                                        {
                                            if (!String.IsNullOrEmpty(Dr["ESTATUS"].ToString()))
                                            {
                                                if (Grid_Mov_Conceptos is GridView)
                                                {
                                                    if (Grid_Mov_Conceptos.Rows.Count > 0)
                                                    {
                                                        foreach (GridViewRow Gv_Mov in Grid_Mov_Conceptos.Rows)
                                                        {
                                                            if (Gv_Mov is GridViewRow)
                                                            {
                                                                if (!String.IsNullOrEmpty(Gv_Mov.Cells[10].Text))
                                                                {
                                                                    if (Gv_Mov.Cells[10].Text.Trim().Equals("ACEPTADO"))
                                                                    {
                                                                        ((CheckBox)Gv_Mov.Cells[37].FindControl("Chk_Aceptar")).Checked = true;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else 
                        {
                            Grid_Mov_Conceptos.Columns[37].Visible = false;
                            Grid_Mov_Conceptos.Columns[38].Visible = false;
                            Lbl_Autorizacion.Text = "Autorizar Todo";
                            Chk_Autorizar.Checked = false;
                            Chk_Aceptacion.Visible = false;
                            Lbl_Aceptar.Visible = false;
                        }
                    }
                    else 
                    {
                        Grid_Mov_Conceptos.Columns[0].Visible = false;
                        Grid_Mov_Conceptos.Columns[35].Visible = false;
                        Grid_Mov_Conceptos.Columns[37].Visible = false;
                        Grid_Mov_Conceptos.Columns[38].Visible = false;
                        Grid_Mov_Conceptos.Columns[39].Visible = false;
                        Grid_Mov_Conceptos.Columns[40].Visible = false;
                        Chk_Autorizar.Checked = false;
                        Chk_Aceptacion.Checked = false;
                    }

                }
                else
                {
                    Grid_Mov_Conceptos.DataSource = new DataTable();
                }
            }
            catch (Exception Ex)
            {
                Mostar_Limpiar_Error(String.Empty, "Error al cargar la tabla de Movimientos. Error[" + Ex.Message + "]", true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Rubros
        ///DESCRIPCIÓN          : Metodo para llenar el combo de los rubros
        ///PARAMETROS           :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 17/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Llenar_Combo_Rubros()
        {
            //Cls_Cat_Psp_Rubros_Negocio Negocio = new Cls_Cat_Psp_Rubros_Negocio(); //conexion con la capa de negocios
            Cls_Ope_Psp_Pronostico_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Pronostico_Ingresos_Negocio();
            DataTable Dt_Rubros = new DataTable();

            try
            {
                Cmb_Rubro.Items.Clear();
                Negocio.P_Estatus = "ACTIVO";
                Dt_Rubros = Negocio.Consultar_Rubros();
                if (Dt_Rubros != null)
                {
                    Cmb_Rubro.DataSource = Dt_Rubros;
                    Cmb_Rubro.DataTextField = "CLAVE_NOMBRE";
                    Cmb_Rubro.DataValueField = Cat_Psp_Rubro.Campo_Rubro_ID;
                    Cmb_Rubro.DataBind();
                    Cmb_Rubro.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                    Cmb_Rubro.SelectedIndex = -1;
                }
                else
                {
                    Cmb_Rubro.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                }
            }
            catch (Exception Ex)
            {
                Mostar_Limpiar_Error(String.Empty, "Error al cargar el combo de Rubros. Error[" + Ex.Message + "]", true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Tipos
        ///DESCRIPCIÓN          : Metodo para llenar el combo de los tipo
        ///PARAMETROS           :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 20/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Llenar_Combo_Tipos()
        {
            //Cls_Cat_Psp_Tipos_Negocio Negocio = new Cls_Cat_Psp_Tipos_Negocio(); //conexion con la capa de negocios
            Cls_Ope_Psp_Pronostico_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Pronostico_Ingresos_Negocio();
            DataTable Dt_Tipos = new DataTable();

            try
            {
                Cmb_Tipo.Items.Clear();
                Negocio.P_Estatus = "ACTIVO";
                Dt_Tipos = Negocio.Consultar_Tipos();
                if (Dt_Tipos != null)
                {
                    Cmb_Tipo.DataSource = Dt_Tipos;
                    Cmb_Tipo.DataTextField = "CLAVE_NOMBRE";
                    Cmb_Tipo.DataValueField = Cat_Psp_Tipo.Campo_Tipo_ID;
                    Cmb_Tipo.DataBind();
                    Cmb_Tipo.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                    Cmb_Tipo.SelectedIndex = -1;
                }
                else
                {
                    Cmb_Tipo.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                }
            }
            catch (Exception Ex)
            {
                Mostar_Limpiar_Error(String.Empty, "Error al cargar el combo de tipos. Error[" + Ex.Message + "]", true);
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Clases
        ///DESCRIPCIÓN          : Metodo para llenar el combo de los clases
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 27/Marzo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Llenar_Combo_Clases()
        {
            //Cls_Cat_Psp_Clase_Ing_Negocio Negocio = new Cls_Cat_Psp_Clase_Ing_Negocio();
            Cls_Ope_Psp_Pronostico_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Pronostico_Ingresos_Negocio();
            DataTable Dt_Clases = new DataTable();

            try
            {
                Negocio.P_Tipo_ID = Cmb_Tipo.SelectedItem.Value.Trim();
                Dt_Clases = Negocio.Consultar_Clase_Ing();

                Cmb_Clase.Items.Clear();

                if (Dt_Clases != null)
                {
                    if (Dt_Clases.Rows.Count > 0)
                    {
                        Cmb_Clase.DataValueField = Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID;
                        Cmb_Clase.DataTextField = "CLAVE_NOMBRE";
                        Cmb_Clase.DataSource = Dt_Clases;
                        Cmb_Clase.DataBind();
                        Cmb_Clase.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                        Tr_Clase.Visible = true;
                        Cmb_Clase.Enabled = true;
                    }
                    else
                    {
                        Tr_Clase.Visible = false;
                        Cmb_Clase.Enabled = false;
                    }
                }
                else
                {
                    Tr_Clase.Visible = false;
                    Cmb_Clase.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al Llenar_Combo_Clases ERROR[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Fte_Financiamiento
        ///DESCRIPCIÓN          : Metodo para llenar el combo de fuente de financiamiento
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 13/Abril/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Llenar_Combo_Fte_Financiamiento()
        {
            Cls_Ope_Psp_Movimientos_Ingresos_Negocio Ingresos_Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio();
            DataTable Dt_Fte_Financiamiento = new DataTable();

            try
            {
                Dt_Fte_Financiamiento = Ingresos_Negocio.Consulta_Fte_Financiamiento();

                Cmb_FF.Items.Clear();

                if (Dt_Fte_Financiamiento != null)
                {
                    if (Dt_Fte_Financiamiento.Rows.Count > 0)
                    {
                        Cmb_FF.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
                        Cmb_FF.DataTextField = "CLAVE_NOMBRE";
                        Cmb_FF.DataSource = Dt_Fte_Financiamiento;
                        Cmb_FF.DataBind();
                        Cmb_FF.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al Llenar_Combo_Fte_Financiamiento ERROR[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Rubros
        ///DESCRIPCIÓN          : Metodo para llenar el combo de los rubros del presupuesto de ingresos
        ///PARAMETROS           :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 17/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Llenar_Combo_Rubros_Ingresos()
        {
            Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio(); //conexion con la capa de negocios
            DataTable Dt_Rubros = new DataTable();

            try
            {
                Cmb_Rubro.Items.Clear();

                if (Cmb_FF.SelectedIndex > 0)
                {
                    Negocio.P_Fte_Financiamiento = Cmb_FF.SelectedItem.Value.Trim();
                }
                else 
                {
                    Negocio.P_Fte_Financiamiento = String.Empty;
                }

                Dt_Rubros = Negocio.Consultar_Rubros();
                if (Dt_Rubros != null)
                {
                    Cmb_Rubro.DataSource = Dt_Rubros;
                    Cmb_Rubro.DataTextField = "CLAVE_NOMBRE";
                    Cmb_Rubro.DataValueField = Cat_Psp_Rubro.Campo_Rubro_ID;
                    Cmb_Rubro.DataBind();
                    Cmb_Rubro.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                    Cmb_Rubro.SelectedIndex = -1;
                }
                else
                {
                    Cmb_Rubro.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                }
            }
            catch (Exception Ex)
            {
                Mostar_Limpiar_Error(String.Empty, "Error al cargar el combo de Rubros. Error[" + Ex.Message + "]", true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Fuente_Financiamiento_Ingresos
        ///DESCRIPCIÓN          : Metodo para llenar el combo de las fuentes de financiamiento del presupuesto de ingresos
        ///PARAMETROS           :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 17/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Llenar_Combo_Fuente_Financiamiento_Ingresos()
        {
            Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio(); //conexion con la capa de negocios
            DataTable Dt_FF = new DataTable();

            try
            {
                Cmb_FF.Items.Clear();
                Dt_FF = Negocio.Consultar_FF();
                if (Dt_FF != null)
                {
                    Cmb_FF.DataSource = Dt_FF;
                    Cmb_FF.DataTextField = "CLAVE_NOMBRE";
                    Cmb_FF.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
                    Cmb_FF.DataBind();
                    Cmb_FF.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));

                    if (Dt_FF.Rows.Count > 0)
                    { 
                        Cmb_FF.SelectedIndex = 1; 
                    }
                }
                else
                {
                    Cmb_FF.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                }
            }
            catch (Exception Ex)
            {
                Mostar_Limpiar_Error(String.Empty, "Error al cargar el combo de fuentes de financiamiento. Error[" + Ex.Message + "]", true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Conceptos
        ///DESCRIPCIÓN          : Metodo para llenar el grid de los conceptos
        ///PARAMETROS           :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 17/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Llenar_Grid_Conceptos()
        {
            Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio(); //conexion con la capa de negocios
            DataTable Dt_Conceptos = new DataTable();

            try
            {
                if (Cmb_Rubro.SelectedIndex > 0)
                {
                    Negocio.P_Rubro_ID = Cmb_Rubro.SelectedItem.Value.Trim();
                }
                else
                {
                    Negocio.P_Rubro_ID = String.Empty;
                }

                if (Cmb_Tipo.SelectedIndex > 0)
                {
                    Negocio.P_Tipo_ID = Cmb_Tipo.SelectedItem.Value.Trim();
                }
                else
                {
                    Negocio.P_Tipo_ID = String.Empty;
                }

                if (Cmb_Clase.SelectedIndex > 0)
                {
                    Negocio.P_Clase_ID = Cmb_Clase.SelectedItem.Value.Trim();
                }
                else
                {
                    Negocio.P_Clase_ID = String.Empty;
                }

                if (Cmb_FF.SelectedIndex > 0)
                {
                    Negocio.P_Fte_Financiamiento = Cmb_FF.SelectedItem.Value.Trim();
                }
                else
                {
                    Negocio.P_Fte_Financiamiento = String.Empty;
                }

                if (Tr_Programa.Visible)
                {
                    if (Cmb_Programa.SelectedIndex > 0)
                    {
                        Negocio.P_Programa_ID = Cmb_Programa.SelectedItem.Value.Trim();
                        Hf_Programa_ID.Value = Cmb_Programa.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocio.P_Programa_ID = String.Empty;
                        Hf_Programa_ID.Value = String.Empty;
                    }
                }
                else
                {
                    Negocio.P_Programa_ID = String.Empty;
                }

                Dt_Conceptos = Negocio.Consultar_Conceptos_Ing();

                if (Dt_Conceptos != null)
                {
                    Grid_Conceptos.Columns[0].Visible = true;
                    Grid_Conceptos.Columns[2].Visible = true;/*cambio ocultar programa*/
                    Grid_Conceptos.Columns[7].Visible = true;
                    Grid_Conceptos.Columns[8].Visible = true;
                    Grid_Conceptos.Columns[9].Visible = true;
                    Grid_Conceptos.Columns[10].Visible = true;
                    Grid_Conceptos.Columns[11].Visible = true;
                    Grid_Conceptos.Columns[12].Visible = true;
                    Grid_Conceptos.Columns[13].Visible = true;
                    Grid_Conceptos.Columns[14].Visible = true;
                    Grid_Conceptos.Columns[15].Visible = true;
                    Grid_Conceptos.Columns[16].Visible = true;
                    Grid_Conceptos.Columns[17].Visible = true;
                    Grid_Conceptos.Columns[18].Visible = true;
                    Grid_Conceptos.Columns[19].Visible = true;
                    Grid_Conceptos.Columns[20].Visible = true;
                    Grid_Conceptos.Columns[21].Visible = true;
                    Grid_Conceptos.Columns[22].Visible = true;
                    Grid_Conceptos.Columns[23].Visible = true;
                    Grid_Conceptos.Columns[24].Visible = true;
                    Grid_Conceptos.Columns[25].Visible = true;
                    Grid_Conceptos.Columns[26].Visible = true;
                    Grid_Conceptos.Columns[27].Visible = true;
                    Grid_Conceptos.DataSource = Dt_Conceptos;
                    Grid_Conceptos.DataBind();
                    Grid_Conceptos.Columns[2].Visible = false;/*cambio ocultar programa*/
                    Grid_Conceptos.Columns[7].Visible = false;
                    Grid_Conceptos.Columns[8].Visible = false;
                    Grid_Conceptos.Columns[9].Visible = false;
                    Grid_Conceptos.Columns[10].Visible = false;
                    Grid_Conceptos.Columns[11].Visible = false;
                    Grid_Conceptos.Columns[12].Visible = false;
                    Grid_Conceptos.Columns[13].Visible = false;
                    Grid_Conceptos.Columns[14].Visible = false;
                    Grid_Conceptos.Columns[15].Visible = false;
                    Grid_Conceptos.Columns[16].Visible = false;
                    Grid_Conceptos.Columns[17].Visible = false;
                    Grid_Conceptos.Columns[18].Visible = false;
                    Grid_Conceptos.Columns[19].Visible = false;
                    Grid_Conceptos.Columns[20].Visible = false;
                    Grid_Conceptos.Columns[21].Visible = false;
                    Grid_Conceptos.Columns[22].Visible = false;
                    Grid_Conceptos.Columns[23].Visible = false;
                    Grid_Conceptos.Columns[24].Visible = false;
                    Grid_Conceptos.Columns[25].Visible = false;
                    Grid_Conceptos.Columns[26].Visible = false;
                    Grid_Conceptos.Columns[27].Visible = false;
                }
                else
                {
                    Grid_Conceptos.DataSource = new DataTable();
                }
            }
            catch (Exception Ex)
            {
                Grid_Conceptos.DataSource = new DataTable();
                Mostar_Limpiar_Error(String.Empty, "Error al cargar la tabla de conceptos. Error[" + Ex.Message + "]", true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Conceptos_Ingresos
        ///DESCRIPCIÓN          : Metodo para llenar el grid de los conceptos
        ///PARAMETROS           :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 17/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Llenar_Grid_Conceptos_Ingresos()
        {
            Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio(); //conexion con la capa de negocios
            DataTable Dt_Conceptos = new DataTable();

            try
            {
                if (Cmb_FF.SelectedIndex > 0)
                {
                    Negocio.P_Fte_Financiamiento = Cmb_FF.SelectedItem.Value.Trim();
                }
                else
                {
                    Negocio.P_Fte_Financiamiento = String.Empty;
                }

                if (Tr_Programa.Visible)
                {
                    if (Cmb_Programa.SelectedIndex > 0)
                    {
                        Negocio.P_Programa_ID = Cmb_Programa.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocio.P_Programa_ID = String.Empty;
                    }
                }
                else 
                {
                    Negocio.P_Programa_ID = String.Empty;
                }

                if (Cmb_Rubro.SelectedIndex > 0)
                {
                    Negocio.P_Rubro_ID = Cmb_Rubro.SelectedItem.Value.Trim();
                }
                else
                {
                    Negocio.P_Rubro_ID = String.Empty;
                }

                if (!String.IsNullOrEmpty(Hf_Concepto_ID.Value.Trim()))
                {
                    Negocio.P_Concepto_ID = Hf_Concepto_ID.Value.Trim();
                }
                else
                {
                    Negocio.P_Concepto_ID = String.Empty;
                }

                Negocio.P_Busqueda = "CONCEPTO";
                Dt_Conceptos = Negocio.Consultar_Conceptos();

                if (Dt_Conceptos != null)
                {
                    Grid_Conceptos.Columns[0].Visible = true;
                    Grid_Conceptos.Columns[2].Visible = true;/*cambio ocultar programa*/
                    Grid_Conceptos.Columns[7].Visible = true;
                    Grid_Conceptos.Columns[8].Visible = true;
                    Grid_Conceptos.Columns[9].Visible = true;
                    Grid_Conceptos.Columns[10].Visible = true;
                    Grid_Conceptos.Columns[11].Visible = true;
                    Grid_Conceptos.Columns[12].Visible = true;
                    Grid_Conceptos.Columns[13].Visible = true;
                    Grid_Conceptos.Columns[14].Visible = true;
                    Grid_Conceptos.Columns[15].Visible = true;
                    Grid_Conceptos.Columns[16].Visible = true;
                    Grid_Conceptos.Columns[17].Visible = true;
                    Grid_Conceptos.Columns[18].Visible = true;
                    Grid_Conceptos.Columns[19].Visible = true;
                    Grid_Conceptos.Columns[20].Visible = true;
                    Grid_Conceptos.Columns[21].Visible = true;
                    Grid_Conceptos.Columns[22].Visible = true;
                    Grid_Conceptos.Columns[23].Visible = true;
                    Grid_Conceptos.Columns[24].Visible = true;
                    Grid_Conceptos.Columns[25].Visible = true;
                    Grid_Conceptos.Columns[26].Visible = true;
                    Grid_Conceptos.Columns[27].Visible = true;
                    Grid_Conceptos.DataSource = Dt_Conceptos;
                    Grid_Conceptos.DataBind();
                    Grid_Conceptos.Columns[2].Visible = false;/*cambio ocultar programa*/
                    Grid_Conceptos.Columns[7].Visible = false;
                    Grid_Conceptos.Columns[8].Visible = false;
                    Grid_Conceptos.Columns[9].Visible = false;
                    Grid_Conceptos.Columns[10].Visible = false;
                    Grid_Conceptos.Columns[11].Visible = false;
                    Grid_Conceptos.Columns[12].Visible = false;
                    Grid_Conceptos.Columns[13].Visible = false;
                    Grid_Conceptos.Columns[14].Visible = false;
                    Grid_Conceptos.Columns[15].Visible = false;
                    Grid_Conceptos.Columns[16].Visible = false;
                    Grid_Conceptos.Columns[17].Visible = false;
                    Grid_Conceptos.Columns[18].Visible = false;
                    Grid_Conceptos.Columns[19].Visible = false;
                    Grid_Conceptos.Columns[20].Visible = false;
                    Grid_Conceptos.Columns[21].Visible = false;
                    Grid_Conceptos.Columns[22].Visible = false;
                    Grid_Conceptos.Columns[23].Visible = false;
                    Grid_Conceptos.Columns[24].Visible = false;
                    Grid_Conceptos.Columns[25].Visible = false;
                    Grid_Conceptos.Columns[26].Visible = false;
                    Grid_Conceptos.Columns[27].Visible = false;
                }
                else
                {
                    Grid_Conceptos.DataSource = new DataTable();
                }
            }
            catch (Exception Ex)
            {
                Mostar_Limpiar_Error(String.Empty, "Error al cargar la tabla de conceptos. Error[" + Ex.Message + "]", true);
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Programas
        ///DESCRIPCIÓN          : Metodo para llenar el combo de los programas
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 16/Julio/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Llenar_Combo_Programas()
        {
            Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio(); //conexion con la capa de negocios
            DataTable Dt = new DataTable();

            try
            {
                Cmb_Programa.Items.Clear();

                if (Cmb_FF.SelectedIndex > 0)
                {
                    Negocio.P_Fte_Financiamiento = Cmb_FF.SelectedItem.Value.Trim();
                }
                else
                {
                    Negocio.P_Fte_Financiamiento = String.Empty;
                }

                if (!String.IsNullOrEmpty(Txt_Busqueda_Programa.Text.Trim()))
                {
                    Negocio.P_Busqueda = Txt_Busqueda_Programa.Text.Trim();
                }
                else 
                {
                    Negocio.P_Busqueda = String.Empty;
                }

                if (!String.IsNullOrEmpty(Hf_Concepto_ID.Value.Trim()))
                {
                    Negocio.P_Concepto_ID = Hf_Concepto_ID.Value.Trim();
                }
                else
                {
                    Negocio.P_Concepto_ID = String.Empty;
                }

                Negocio.P_Programa_ID = String.Empty;
                Dt = Negocio.Consultar_Programas();
                if (Dt != null && Dt.Rows.Count > 0)
                {
                    Cmb_Programa.DataTextField = "CLAVE_NOMBRE";
                    Cmb_Programa.DataValueField = Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id;
                    Cmb_Programa.DataSource = Dt;
                    Cmb_Programa.DataBind();
                    Cmb_Programa.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                    Cmb_Programa.SelectedIndex = -1;

                    Tr_Programa.Visible = false;/*cambio ocultar pograma*/
                    Cmb_Programa.Enabled = true;
                    Btn_Busqueda_Programa.Enabled = true;
                    Txt_Busqueda_Programa.Enabled = true;
                }
                else
                {
                    Cmb_Programa.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                    Tr_Programa.Visible = false;
                }
            }
            catch (Exception Ex)
            {
                Mostar_Limpiar_Error(String.Empty, "Error al cargar el combo de Programas. Error[" + Ex.Message + "]", true);
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Busqueda_Programa
        ///DESCRIPCIÓN          : Metodo para obtener la busqueda de los programas
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 17/julio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Busqueda_Programa(String Texto_Busqueda)
        {
            Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio(); //conexion con la capa de negocios
            DataTable Dt = new DataTable();

            try
            {
                Negocio.P_Anio = String.Empty;
                if (String.IsNullOrEmpty(Texto_Busqueda.Trim()))
                {
                    if (!String.IsNullOrEmpty(Hf_Concepto_ID.Value.Trim()))
                    {
                        Negocio.P_Concepto_ID = Hf_Concepto_ID.Value.Trim();
                    }
                    else
                    {
                        Negocio.P_Concepto_ID = String.Empty;
                    }

                    Negocio.P_Programa_ID = String.Empty;
                    Negocio.P_Busqueda = String.Empty;
                    Negocio.P_Fte_Financiamiento = Cmb_FF.SelectedItem.Value.Trim();
                    Dt = Negocio.Consultar_Programas();
                }
                else
                {
                    if (!String.IsNullOrEmpty(Hf_Concepto_ID.Value.Trim()))
                    {
                        Negocio.P_Concepto_ID = Hf_Concepto_ID.Value.Trim();
                    }
                    else
                    {
                        Negocio.P_Concepto_ID = String.Empty;
                    }

                    Negocio.P_Programa_ID = String.Empty;
                    Negocio.P_Busqueda = Texto_Busqueda.Trim();
                    Negocio.P_Fte_Financiamiento = Cmb_FF.SelectedItem.Value.Trim();
                    Dt = Negocio.Consultar_Programas();
                }

                Cmb_Programa.Items.Clear();
                if (Dt != null && Dt.Rows.Count > 0)
                {
                    Cmb_Programa.DataTextField = "CLAVE_NOMBRE";
                    Cmb_Programa.DataValueField = Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id;
                    Cmb_Programa.DataSource = Dt;
                    Cmb_Programa.DataBind();
                    Cmb_Programa.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                    Cmb_Programa.SelectedIndex = -1;
                }
                else
                {
                    Cmb_Programa.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error en la busqueda de los conceptos Erro[" + ex.Message + "]");
            }
        }
    #endregion
        
    #region (Subir Archivos)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Asy_Cargar_Archivo_Complete
        ///DESCRIPCIÓN          : Metodo para cargar los archivos anexos
        ///PARAMETROS           :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 05/Septiembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Asy_Cargar_Archivo_Complete(Object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            AsyncFileUpload Archivo;
            String Nombre_Archivo = String.Empty;
            String[] Archivos;
            Int32 No = 0;
            String Extension = String.Empty;
            String Ruta = String.Empty;

            try
            {
                Archivo = (AsyncFileUpload)sender;
                Nombre_Archivo = Archivo.FileName;

                if (!String.IsNullOrEmpty(Nombre_Archivo))
                {
                    Archivos = Nombre_Archivo.Split('\\');
                    No = Archivos.Length;
                    Ruta = Archivos[No - 1];
                    Extension = Ruta.Substring(Ruta.LastIndexOf(".") + 1);

                    if (Extension == "txt" || Extension == "doc" || Extension == "pdf" || Extension == "xls" || Extension == "zip" || Extension == "rar"
                        || Extension == "docx" || Extension == "jpg" || Extension == "JPG" || Extension == "jpeg" || Extension == "JPEG"
                        || Extension == "png" || Extension == "PNG" || Extension == "gif" || Extension == "GIF" || Extension == "xlsx")
                    {
                        if (Archivo.FileContent.Length > 2621440) {
                            return; 
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alta", "alert('La extension no es valida');", true);
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al validar los anexos. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : ClearSession_AsyncFileUpload
        ///DESCRIPCIÓN          : Metodo para limpiar el control de AsyncFileUpload
        ///PARAMETROS           :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 05/Septiembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void ClearSession_AsyncFileUpload(String ClientID)
        {
            HttpContext currentContext;
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
            {
                currentContext = HttpContext.Current;
            }
            else
            {
                currentContext = null;
            }
            if (currentContext != null)
            {
                foreach (String Key in currentContext.Session.Keys)
                {
                    if (Key.Contains(ClientID))
                    {
                        currentContext.Session.Remove(Key);
                        break;
                    }
                }
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Limpiar_Carpeta_Anexos
        ///DESCRIPCIÓN          : Metodo para limpiar la carpeta de anexos que ya no existen en la base de datos
        ///PARAMETROS           :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 06/Septiembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Limpiar_Carpeta_Anexos()
        {
            DataTable Dt = new DataTable();
            String Archivo = String.Empty;
            String Ruta_Destino = String.Empty;
            String Ruta_Temporal = String.Empty;
            String[] Archivos;

            try
            {
                Dt = (DataTable)Session["Dt_Mov_Conceptos"];

                if (Dt != null && Dt.Rows.Count > 0)
                {
                    Ruta_Destino = Server.MapPath("Archivos/Anexos_Ingresos/" + String.Format("{0:yyyy}", DateTime.Now) + "/");
                    Ruta_Temporal = Server.MapPath("Archivos/Anexos_Ingresos/" + String.Format("{0:yyyy}", DateTime.Now) + "/Otro/");

                    if (!System.IO.Directory.Exists(Ruta_Temporal))
                    {
                        System.IO.Directory.CreateDirectory(Ruta_Temporal);
                    }

                    if (System.IO.Directory.Exists(Ruta_Destino))
                    {
                        //copiamos los archivos de la carpeta a una temporal
                        Archivos = System.IO.Directory.GetFiles(Ruta_Destino);
                        foreach (String Arc in Archivos)
                        {
                            Archivo = System.IO.Path.GetFileName(Arc);
                            System.IO.File.Copy(Arc, Ruta_Temporal + Archivo, true);
                            System.IO.File.Delete(Arc);
                        }

                        //copiamos a la carpeta los archivos pertenecientes al movimiento solamente, por si se elimino algun registro
                        //que no existan archivos basura en las carpetas
                        foreach (DataRow Dr in Dt.Rows)
                        {
                            Archivo = Dr["NOMBRE_DOC"].ToString().Trim() + "." + Dr["EXTENSION_DOC"].ToString().Trim();
                            if (System.IO.File.Exists(Ruta_Temporal + Archivo))
                            {
                                System.IO.File.Copy(Ruta_Temporal + Archivo, Ruta_Destino + Archivo, true);
                                System.IO.File.Delete(Ruta_Temporal + Archivo);
                            }
                        }

                        //vaciamos el directorio temporal y lo eliminamos
                        Archivos = System.IO.Directory.GetFiles(Ruta_Temporal);
                        foreach (String Arc in Archivos)
                        {
                            System.IO.File.Delete(Arc);
                        }
                        System.IO.Directory.Delete(Ruta_Temporal);
                    }
                }
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = Ex.Message.ToString();
            }
        }
    #endregion

    #region (Reporte)
        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Crear_Reporte
        ///DESCRIPCIÓN          : Metodo para crear el reporte de movimientos
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 23/Noviembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        public void Crear_Reporte(DataTable Dt_Movimientos)
        {
            DataSet Ds_Registros = null;
            String Fecha = String.Empty;
            String No_Solicitud = String.Empty;

            try
            {
                if (Dt_Movimientos != null) 
                {
                    if (Dt_Movimientos.Rows.Count > 0)
                    {
                        //agregamos las filas de los datos complementarios para el reporte
                        Dt_Movimientos.Columns.Add("NO_SOLICITUD");
                        Dt_Movimientos.Columns.Add("FECHA");
                        Dt_Movimientos.Columns.Add("SOLICITANTE");
                        Dt_Movimientos.Columns.Add("PUESTO_SOLICITANTE");

                        Fecha = Crear_Fecha(String.Format("{0:MM/dd/yyyy}", DateTime.Now));

                        foreach (DataRow Dr in Dt_Movimientos.Rows)
                        {
                            No_Solicitud = String.Empty;
                            No_Solicitud = "MODIFICACIÓN AL PRONÓSTICO DE INGRESOS    T";
                            No_Solicitud += String.Format("{0:00}", Convert.ToInt32(Dr[Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing].ToString().Trim()));
                            No_Solicitud += " - " + Dr[Ope_Psp_Movimiento_Ing_Det.Campo_Anio].ToString().Trim(); 

                            Dr["NO_SOLICITUD"] = No_Solicitud;
                            Dr["FECHA"] = Fecha.Trim();
                            Dr["SOLICITANTE"] = "";
                            Dr["PUESTO_SOLICITANTE"] = "";

                            if (Dr[Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Movimiento].ToString().Trim().Equals("REDUCCION"))
                            {
                                Dr["IMP_TOTAL"] = "-" + Dr["IMP_TOTAL"];
                            }
                        }


                        Ds_Registros = new DataSet();
                        Dt_Movimientos.TableName = "Dt_Movimientos_Ing";
                        Ds_Registros.Tables.Add(Dt_Movimientos.Copy());

                        Generar_Reporte(ref Ds_Registros, "Cr_Ope_Psp_Movimiento_Presupuestal_Ingresos.rpt",
                                "Solicitud_Movimientos_Ingresos_" + Session.SessionID + ".pdf");
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Error al generar el reporte. Error[" + ex.Message + "]");
            }
        }

        //****************************************************************************************************
        //NOMBRE DE LA FUNCIÓN : Crear_Fecha
        //DESCRIPCIÓN          : Metodo para obtener la fecha en letras
        //PARAMETROS           1 Fecha: fecha a la cual le daremos formato
        //CREO                 : Leslie González Vázquez
        //FECHA_CREO           : 13/Diciembre/2011 
        //MODIFICO             :
        //FECHA_MODIFICO       :
        //CAUSA_MODIFICACIÓN   :
        //****************************************************************************************************
        internal static String Crear_Fecha(String Fecha)
        {
            String Fecha_Formateada = String.Empty;
            String Mes = String.Empty;
            String[] Fechas;

            try
            {
                Fechas = Fecha.Split('/');
                Mes = Fechas[0].ToString();
                switch (Mes)
                {
                    case "01":
                        Mes = "Enero";
                        break;
                    case "02":
                        Mes = "Febrero";
                        break;
                    case "03":
                        Mes = "Marzo";
                        break;
                    case "04":
                        Mes = "Abril";
                        break;
                    case "05":
                        Mes = "Mayo";
                        break;
                    case "06":
                        Mes = "Junio";
                        break;
                    case "07":
                        Mes = "Julio";
                        break;
                    case "08":
                        Mes = "Agosto";
                        break;
                    case "09":
                        Mes = "Septiembre";
                        break;
                    case "10":
                        Mes = "Octubre";
                        break;
                    case "11":
                        Mes = "Noviembre";
                        break;
                    default:
                        Mes = "Diciembre";
                        break;
                }
                Fecha_Formateada = "Irapuato, Guanajuato a " + Fechas[1].ToString() + " de " + Mes + " de " + Fechas[2].ToString();
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al crear el fromato de fecha. Error: [" + Ex.Message + "]");
            }
            return Fecha_Formateada;
        }

        /// *************************************************************************************
        /// NOMBRE: Generar_Reporte
        /// 
        /// DESCRIPCIÓN: Método que invoca la generación del reporte.
        ///              
        /// PARÁMETROS: Nombre_Plantilla_Reporte.- Nombre del archivo del Crystal Report.
        ///             Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
        /// 
        /// USUARIO CREO: Juan Alberto Hernández Negrete.
        /// FECHA CREO: 3/Mayo/2011 18:15 p.m.
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACIÓN:
        /// *************************************************************************************
        protected void Generar_Reporte(ref DataSet Ds_Datos, String Nombre_Plantilla_Reporte, String Nombre_Reporte_Generar)
        {
            ReportDocument Reporte = new ReportDocument();//Variable de tipo reporte.
            String Ruta = String.Empty;//Variable que almacenara la ruta del archivo del crystal report. 

            try
            {
                Ruta = @Server.MapPath("../Rpt/Presupuestos/" + Nombre_Plantilla_Reporte);
                Reporte.Load(Ruta);

                if (Ds_Datos is DataSet)
                {
                    if (Ds_Datos.Tables.Count > 0)
                    {
                        Reporte.SetDataSource(Ds_Datos);
                        Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar);
                        Mostrar_Reporte(Nombre_Reporte_Generar);
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
            }
        }

        /// *************************************************************************************
        /// NOMBRE: Exportar_Reporte_PDF
        /// 
        /// DESCRIPCIÓN: Método que guarda el reporte generado en formato PDF en la ruta
        ///              especificada.
        ///              
        /// PARÁMETROS: Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
        ///             Nombre_Reporte.- Nombre que se le dará al reporte.
        /// 
        /// USUARIO CREO: Juan Alberto Hernández Negrete.
        /// FECHA CREO: 3/Mayo/2011 18:19 p.m.
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACIÓN:
        /// *************************************************************************************
        protected void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte)
        {
            ExportOptions Opciones_Exportacion = new ExportOptions();
            DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
            PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

            try
            {
                if (Reporte is ReportDocument)
                {
                    Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte);
                    Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                    Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                    Reporte.Export(Opciones_Exportacion);
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
            }
        }

        /// *************************************************************************************
        /// NOMBRE: Mostrar_Reporte
        /// 
        /// DESCRIPCIÓN: Muestra el reporte en pantalla.
        ///              
        /// PARÁMETROS: Nombre_Reporte.- Nombre que tiene el reporte que se mostrara en pantalla.
        /// 
        /// USUARIO CREO: Juan Alberto Hernández Negrete.
        /// FECHA CREO: 3/Mayo/2011 18:20 p.m.
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACIÓN:
        /// *************************************************************************************
        protected void Mostrar_Reporte(String Nombre_Reporte)
        {
            String Pagina = "../../Reporte/";

            try
            {
                Pagina = Pagina + Nombre_Reporte;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt_Nominas_Negativas",
                    "window.open('" + Pagina + "', 'Busqueda_Empleados','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
        }
    #endregion

    #endregion

    #region (Eventos)
    #region (Botones)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Buscar_Click
        ///DESCRIPCIÓN          : Evento del boton Buscar 
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 17/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
        {
            Mostar_Limpiar_Error(String.Empty, String.Empty, false);
            try
            {
                Llenar_Grid_Movimientos();
            }
            catch (Exception Ex)
            {
                Mostar_Limpiar_Error(String.Empty, "Error en la busqueda de los movimientos. Error[" + Ex.Message + "]", true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
        ///DESCRIPCIÓN          : Evento del boton Salir 
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 17/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, EventArgs e)
        {
            Mostar_Limpiar_Error(String.Empty, String.Empty, false);
            switch (Btn_Salir.ToolTip)
            {
                case "Cancelar":
                    Configuracion_Inicial();
                    Grid_Movimiento.SelectedIndex = -1;
                    Limpiar_Sessiones();
                    break;

                case "Regresar":
                    Configuracion_Inicial();
                    Grid_Movimiento.SelectedIndex = -1;
                    Limpiar_Sessiones();
                    break;

                case "Inicio":
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    Limpiar_Sessiones();
                    break;
            }//fin del switch
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Nuevo_Click
        ///DESCRIPCIÓN          : Evento del boton Guardar
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 17/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Nuevo_Click(object sender, EventArgs e)
        {
            Mostar_Limpiar_Error(String.Empty, String.Empty, false);
            Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio();//Variable de conexion con la capa de negocios.
            DataTable Dt_Mov_Conceptos = new DataTable();
            Dt_Mov_Conceptos = (DataTable)Session["Dt_Mov_Conceptos"];
            String Estatus = String.Empty;
            String No_Modificacion = String.Empty;

            try
            {
                switch (Btn_Nuevo.ToolTip)
                {
                    case "Nuevo":
                        //consultamos el estatus de la ultima modificacion
                        Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                        Estatus = Negocio.Consultar_Estatus_Movimientos();

                        if (Estatus.Trim().Equals("AUTORIZADO"))
                        {
                            Estado_Botones("Nuevo");
                            Limpiar_Forma("Todo");
                            Habilitar_Forma(true);
                            Cmb_Estatus.Enabled = false;
                            Cmb_Estatus.SelectedIndex = 0;
                            Hf_Tipo_Concepto.Value = "Existente";
                            Llenar_Combo_Fuente_Financiamiento_Ingresos();
                            Llenar_Combo_Rubros_Ingresos();
                            Llenar_Grid_Conceptos_Ingresos();
                            Tabla_Meses.Visible = false;
                            Div_Grid_Comentarios.Visible = false;
                            Div_Grid_Movimientos.Style.Add("display", "none");
                            Div_Datos.Style.Add("display", "block");
                            Tr_Tipo.Visible = false;
                            Tr_Clase.Visible = false;
                            Tr_Estimado.Visible = true;
                            Tr_Estimado1.Visible = true;
                            Limpiar_Sessiones();
                            Tr_Chk_Autorizar.Style.Add("display", "none");
                        }
                        else
                        {
                            Mostar_Limpiar_Error("No se puede hacer una nueva modificación porque la anterior no se ha cerrado.", String.Empty, true);
                        }
                        break;
                    case "Guardar":
                        if (Dt_Mov_Conceptos != null && Dt_Mov_Conceptos.Rows.Count > 0)
                        {
                            Negocio.P_Total_Modificado = Txt_Total_Modificado.Text.Trim().Replace(",", "");
                            Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value.Trim();
                            Negocio.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado.Trim();
                            Negocio.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.Trim();
                            Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                            Negocio.P_Dt_Mov_Conceptos = Dt_Mov_Conceptos;

                            No_Modificacion = Negocio.Alta_Movimientos();

                            if (!String.IsNullOrEmpty(No_Modificacion.Trim()))
                            {

                                Autorizar_Movimientos_Ramo33(No_Modificacion.Trim(), String.Format("{0:yyyy}", DateTime.Now));  

                                Configuracion_Inicial();
                                Grid_Movimiento.SelectedIndex = -1;
                                Limpiar_Sessiones();
                                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alta", "alert('Operacion Completa');", true);
                            }
                        }
                        else 
                        {
                            Mostar_Limpiar_Error("Favor de agreagar movimientos a la modificación", String.Empty, true);
                        }
                        break;
                    case "Actualizar":
                        Actualizar_Datos();
                        break;
                }//fin del swirch
            }
            catch (Exception Ex)
            {
                Mostar_Limpiar_Error(String.Empty, "Error al guardar el movimiento. Error[" + Ex.Message + "]", true);
            }
        }//fin del boton Nuevo

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Nuevo_Click
    ///DESCRIPCIÓN          : Evento del boton agregar un movimiento a la modificacion
    ///PARAMETROS           :    
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 17/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Btn_Agregar_Click(object sender, EventArgs e) 
    {
        DataTable Dt_Mov_Conceptos = new DataTable();
        Dt_Mov_Conceptos = (DataTable)Session["Dt_Mov_Conceptos"];
        Mostar_Limpiar_Error(String.Empty, String.Empty, false);
        String Ruta = String.Empty;
        String Ruta_Destino = String.Empty;
        String Extension = String.Empty;
        String Archivo = String.Empty;

        try
        {
            Sumar_Total();
            
            if (Validar_Datos())
            {
                if (Dt_Mov_Conceptos == null || Dt_Mov_Conceptos.Columns.Count <= 0 )
                {
                    Crear_Dt_Mov_Conceptos();
                    Dt_Mov_Conceptos = (DataTable)Session["Dt_Mov_Conceptos"];
                }

                Agregar_Fila_Dt_Mov_Conceptos();
                Limpiar_Forma("Concepto");
                ClearSession_AsyncFileUpload(AFU_Archivo.ClientID);
                Hf_Nombre_Doc.Value = String.Empty;
                Hf_Ruta_Doc.Value = String.Empty;
                Llenar_Grid_Mov_Conceptos();
                Calcular_Total_Modificado();
                Txt_Justificacion.Text = String.Empty;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al agregar un movimiento. Error[" + ex.Message + "]");
        }         
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Limpiar_Click
    ///DESCRIPCIÓN          : Evento del boton de limpiar
    ///PARAMETROS           :    
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 10/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Btn_Limpiar_Click(object sender, EventArgs e)
    {
        try
        {
            Limpiar_Forma("Concepto");
        }
        catch (Exception ex)
        {
            throw new Exception("Error al limpiar los controles. Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Eliminar_Click
    ///DESCRIPCIÓN          : Evento del boton de eliminar un registro de un presupuesto de un producto
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 10/Mayo/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Eliminar_Click(object sender, EventArgs e)
    {
        Mostar_Limpiar_Error(String.Empty, String.Empty, false);
        DataTable Dt_Registros = new DataTable();
        ImageButton Btn_Eliminar = (ImageButton)sender;
        Int32 No_Fila = -1;
        String Id = String.Empty;
        String Archivo = String.Empty;
        String Ruta_Destino = String.Empty;

        try
        {
            Id = Btn_Eliminar.CommandArgument.ToString().Trim();
            Dt_Registros = (DataTable)Session["Dt_Mov_Conceptos"];
            if (Dt_Registros != null)
            {
                if (Dt_Registros.Rows.Count > 0)
                {
                    foreach (DataRow Dr in Dt_Registros.Rows)
                    {
                        No_Fila++;
                        if (Dr["MOVIMIENTO_ING_ID"].ToString().Trim().Equals(Id))
                        {
                            if (Btn_Nuevo.ToolTip.Trim().Equals("Guardar")){
                                //obtenmenos los datos del archivo para eliminarlo de la carpeta
                                Ruta_Destino = Server.MapPath("Archivos/Anexos_Ingresos/" + String.Format("{0:yyyy}", DateTime.Now) + "/");
                                Archivo = Dr["NOMBRE_DOC"].ToString().Trim() + "." + Dr["EXTENSION_DOC"].ToString().Trim();
                                if (System.IO.Directory.Exists(Ruta_Destino))
                                {
                                    if (System.IO.File.Exists(Ruta_Destino + Archivo))
                                    {
                                        System.IO.File.Delete(Ruta_Destino + Archivo);
                                    }
                                }
                            }
                            Dt_Registros.Rows.RemoveAt(No_Fila);
                            Session["Dt_Mov_Conceptos"] = Dt_Registros;
                            Llenar_Grid_Mov_Conceptos();
                            Calcular_Total_Modificado();
                            break;
                        }
                    }

                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al eliminar el registro el grid. Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Modificar_Click
    ///DESCRIPCIÓN          : Evento del boton Modificar
    ///PARAMETROS           :    
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 17/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Btn_Modificar_Click(object sender, EventArgs e)
    {
        Mostar_Limpiar_Error(String.Empty, String.Empty, false);
        Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio();//Variable de conexion con la capa de negocios.
        DataTable Dt_Mov_Conceptos = new DataTable();
        Dt_Mov_Conceptos = (DataTable)Session["Dt_Mov_Conceptos"];
        Mostar_Limpiar_Error(String.Empty, String.Empty, false);
        DataTable Dt_Movimientos = new DataTable();

        try
        {
            switch (Btn_Modificar.ToolTip)
            {
                case "Modificar":
                    Estado_Botones("Modificar");
                    Habilitar_Forma(true);
                    Cmb_Estatus.Enabled = false;
                    Cmb_Estatus.SelectedIndex = 0;
                    Hf_Tipo_Concepto.Value = "Existente";
                    Llenar_Combo_Fuente_Financiamiento_Ingresos();
                    Llenar_Combo_Rubros_Ingresos();
                    Llenar_Grid_Conceptos_Ingresos();
                    Tabla_Meses.Visible = false;
                    Div_Grid_Comentarios.Visible = false;
                    Div_Grid_Movimientos.Style.Add("display", "none");
                    Div_Datos.Style.Add("display", "block");
                    Tr_Tipo.Visible = false;
                    Tr_Clase.Visible = false;
                    Tr_Estimado.Visible = true;
                    Tr_Estimado1.Visible = true;
                    Tr_Chk_Autorizar.Style.Add("display", "none");

                    Negocio.P_No_Movimiento_Ing = Hf_Movimiento_ID.Value.Trim();
                    Negocio.P_Estatus = "'GENERADO'";
                    Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                    Dt_Movimientos = Negocio.Consultar_Movimientos();
                    if (Dt_Movimientos != null && Dt_Movimientos.Rows.Count > 0)
                    {
                        Session["Dt_Mov_Conceptos"] = Dt_Movimientos;
                        Llenar_Grid_Mov_Conceptos();
                        Calcular_Total_Modificado();
                    }
                    else 
                    {
                        Crear_Dt_Mov_Conceptos();
                        Llenar_Grid_Mov_Conceptos();
                        Calcular_Total_Modificado();
                    }

                    break;
                case "Actualizar":
                    if (Dt_Mov_Conceptos != null && Dt_Mov_Conceptos.Rows.Count > 0)
                    {
                        Negocio.P_Total_Modificado = Txt_Total_Modificado.Text.Trim().Replace(",", "").Replace("$","");
                        Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value.Trim();
                        Negocio.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado.Trim();
                        Negocio.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.Trim();
                        Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                        Negocio.P_Dt_Mov_Conceptos = Dt_Mov_Conceptos;
                        Negocio.P_No_Movimiento_Ing = Hf_Movimiento_ID.Value.Trim();


                        if (Negocio.Actualizar_Movimientos())
                        {
                            Autorizar_Movimientos_Ramo33(Hf_Movimiento_ID.Value.Trim(), String.Format("{0:yyyy}", DateTime.Now));

                            Hf_Movimiento_ID.Value = "";
                            Configuracion_Inicial();
                            Grid_Movimiento.SelectedIndex = -1;
                            Limpiar_Sessiones();
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alta", "alert('Operacion Completa');", true);
                        }
                    }
                    else
                    {
                        Mostar_Limpiar_Error("Favor de agreagar movimientos a la modificación", String.Empty, true);
                    }
                    break;
            }//fin del swirch
        }
        catch (Exception ex)
        {
            throw new Exception("Error al modificar los movimientos. Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Pdf_Click
    ///DESCRIPCIÓN          : Evento del boton de pdf para generar la solicitud
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 18/Diciembre/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Pdf_Click(object sender, EventArgs e)
    {
        Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocios = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio();
        DataTable Dt_Datos = new DataTable();
        ImageButton Btn_Pdf = (ImageButton)sender;
        String No_Movimiento_Ing = String.Empty;

        try
        {
            No_Movimiento_Ing = Btn_Pdf.CommandArgument.ToString().Trim();

            Negocios.P_No_Movimiento_Ing = No_Movimiento_Ing.Trim();
            Negocios.P_Estatus = "'GENERADO', 'ACEPTADO', 'AUTORIZADO', 'PREAUTORIZADO'";
            Negocios.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
            Dt_Datos = Negocios.Consultar_Movimientos();

            Crear_Reporte(Dt_Datos);
        }
        catch (Exception ex)
        {
            throw new Exception("Error al eliminar el registro el grid. Error[" + ex.Message + "]");
        }
    }

    #endregion

    #region (Grid)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Movimientos_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento de seleccion del grid
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 17/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Grid_Mov_Conceptos_SelectedIndexChanged(object sender, EventArgs e)
    {
        Mostar_Limpiar_Error(String.Empty, String.Empty, false);
        try
        {
            if (Grid_Mov_Conceptos.SelectedIndex > (-1))
            {
                Limpiar_Forma("Todo");

                //Txt_No_Solicitud.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[1].Text).ToString();
                Txt_Importe.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[4].Text).ToString();
                Txt_Justificacion.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[23].Text).ToString(); ;
                Txt_Importe.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[4].Text).ToString(); ;
                Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[5].Text).ToString()));
                Cmb_Operacion.SelectedIndex = Cmb_Operacion.Items.IndexOf(Cmb_Operacion.Items.FindByValue(HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[3].Text).ToString()));
                Llenar_Combo_Fuente_Financiamiento_Ingresos();
                Cmb_FF.SelectedIndex = Cmb_FF.Items.IndexOf(Cmb_FF.Items.FindByValue(HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[10].Text).ToString()));
                Llenar_Combo_Rubros_Ingresos();
                Cmb_Rubro.SelectedIndex = Cmb_Rubro.Items.IndexOf(Cmb_Rubro.Items.FindByValue(HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[6].Text).ToString()));

                Hf_Rubro_ID.Value = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[6].Text).ToString();
                Hf_Tipo_ID.Value = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[7].Text).ToString();
                Hf_Clase_ID.Value = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[8].Text).ToString();
                Hf_Concepto_ID.Value = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[9].Text).ToString();
                Hf_Programa_ID.Value = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[27].Text).ToString();
                Cmb_Programa.SelectedIndex = Cmb_Programa.Items.IndexOf(Cmb_Programa.Items.FindByValue(HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[27].Text).ToString()));

                Lbl_Nombre_Concepto.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[2].Text).ToString();

                if (!String.IsNullOrEmpty(Hf_Programa_ID.Value.Trim()))
                {
                    Tr_Programa.Visible = false;
                }
                else
                {
                    Tr_Programa.Visible = false;/*cambio ocultar programa*/
                    Llenar_Combo_Programas();
                }

                Llenar_Grid_Conceptos_Ingresos();

                Txt_Enero.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[11].Text).ToString();
                Txt_Febrero.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[12].Text).ToString();
                Txt_Marzo.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[13].Text).ToString();
                Txt_Abril.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[14].Text).ToString();
                Txt_Mayo.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[15].Text).ToString();
                Txt_Junio.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[16].Text).ToString();
                Txt_Julio.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[17].Text).ToString();
                Txt_Agosto.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[18].Text).ToString();
                Txt_Septiembre.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[19].Text).ToString();
                Txt_Octubre.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[20].Text).ToString();
                Txt_Noviembre.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[21].Text).ToString();
                Txt_Diciembre.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[22].Text).ToString();

                Sumar_Total();

                Estado_Botones("Inicial");
                Habilitar_Forma(false);
                Tr_Estimado.Visible = false;
                Tr_Estimado1.Visible = false;
                Tabla_Meses.Visible = true;
                Tr_Tipo.Visible = false;
                Tr_Clase.Visible = false;
                Div_Datos.Style.Add("display", "block");
                Div_Grid_Movimientos.Style.Add("display", "none");
                Grid_Conceptos.Columns[0].Visible = false;
                Div_Grid_Comentarios.Visible = false;
                Btn_Salir.ToolTip = "Regresar";
                if (!String.IsNullOrEmpty(HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[26].Text).ToString().Trim()))
                {
                    Div_Grid_Comentarios.Visible = true;
                    Txt_Fecha_Autorizo.Text = String.Format("{0:dd/MMM/yyyy}", Grid_Movimiento.SelectedRow.Cells[25].Text);
                    Txt_Autorizo.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[26].Text).ToString();
                    Txt_Comentario.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[24].Text).ToString(); 
                }
            }
        }
        catch (Exception Ex)
        {
            Mostar_Limpiar_Error(String.Empty, "Error al cargar los datos del movimientos. Error[" + Ex.Message + "]", true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Conceptos_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento de seleccion del grid
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 17/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Grid_Conceptos_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio();
        Mostar_Limpiar_Error(String.Empty, String.Empty, false);
        Boolean Repetido = false;
        DataTable Dt_Concepto = new DataTable();
        DataTable Dt = new DataTable();
        Double Estimado = 0.00;
        Double Ampliacion = 0.00;
        Double Reduccion = 0.00;
        Double Modificado = 0.00;

        try
        {
            if (Grid_Conceptos.SelectedIndex > (-1))
            {
                if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("SUPLEMENTO"))
                {
                    if (Cmb_FF.SelectedIndex > 0)
                    {
                        Negocio.P_Fte_Financiamiento = Cmb_FF.SelectedItem.Value.Trim();
                        Negocio.P_Rubro_ID = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[7].Text).ToString();
                        Negocio.P_Tipo_ID = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[8].Text).ToString();
                        Negocio.P_Clase_ID = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[9].Text).ToString();
                        Negocio.P_Concepto_ID = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[10].Text).ToString();
                        Negocio.P_SubConcepto_ID = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[11].Text).ToString().Trim();

                        if (!String.IsNullOrEmpty(Cmb_Programa.SelectedItem.Value.Trim()))
                        {
                            Negocio.P_Programa_ID = Cmb_Programa.SelectedItem.Value.Trim();
                        }
                        else 
                        {
                            Negocio.P_Programa_ID = String.Empty;
                        }

                        Negocio.P_Busqueda = "CON";
                        Dt_Concepto = Negocio.Consultar_Conceptos();

                        if (Dt_Concepto != null)
                        {
                            if (Dt_Concepto.Rows.Count > 0)
                            {
                                Mostar_Limpiar_Error("Favor de seleccionar otro concepto pues este ya se encuentra en el presupuesto de ingresos", String.Empty, true);
                                Repetido = true;
                            }
                        }
                    }
                    else 
                    {
                        Mostar_Limpiar_Error("Favor de seleccionar una Fuente de Financiamiento", String.Empty, true);
                        return;
                    }
                }

                if(!Repetido)
                {
                    Estimado = Convert.ToDouble(String.IsNullOrEmpty(HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[3].Text).ToString()) ? "0" : HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[3].Text).ToString());
                    Ampliacion = Convert.ToDouble(String.IsNullOrEmpty(HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[13].Text).ToString()) ? "0" : HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[13].Text).ToString());
                    Reduccion = Convert.ToDouble(String.IsNullOrEmpty(HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[14].Text).ToString()) ? "0" : HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[14].Text).ToString());

                    Modificado = Estimado + Ampliacion - Reduccion;

                    Hf_Clave_Nom_Concepto.Value = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[1].Text).ToString();
                    Hf_Estimado.Value = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[3].Text).ToString();
                    Hf_Modificado.Value = Modificado.ToString().Trim();
                    Hf_Rubro_ID.Value = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[7].Text).ToString();
                    Hf_Tipo_ID.Value = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[8].Text).ToString();
                    Hf_Clase_ID.Value = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[9].Text).ToString();
                    Hf_Concepto_ID.Value = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[10].Text).ToString();
                    Hf_SubConcepto_ID.Value = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[11].Text).ToString();
                    Hf_Ampliacion.Value = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[13].Text).ToString();
                    Hf_Reduccion.Value = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[14].Text).ToString();

                    Hf_Programa_ID.Value = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[27].Text).ToString();
                    Hf_Clave_Nom_Programa.Value = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[2].Text).ToString();
                    Cmb_Programa.SelectedIndex = Cmb_Programa.Items.IndexOf(Cmb_Programa.Items.FindByValue(HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[27].Text).ToString()));

                    Lbl_Disp_Ene.Text = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[15].Text).ToString();
                    Lbl_Disp_Feb.Text = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[16].Text).ToString();
                    Lbl_Disp_Mar.Text = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[17].Text).ToString();
                    Lbl_Disp_Abr.Text = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[18].Text).ToString();
                    Lbl_Disp_May.Text = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[19].Text).ToString();
                    Lbl_Disp_Jun.Text = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[20].Text).ToString();
                    Lbl_Disp_Jul.Text = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[21].Text).ToString();
                    Lbl_Disp_Ago.Text = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[22].Text).ToString();
                    Lbl_Disp_Sep.Text = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[23].Text).ToString();
                    Lbl_Disp_Oct.Text = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[24].Text).ToString();
                    Lbl_Disp_Nov.Text = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[25].Text).ToString();
                    Lbl_Disp_Dic.Text = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[26].Text).ToString();

                    Lbl_Nombre_Concepto.Text = HttpUtility.HtmlDecode(Grid_Conceptos.SelectedRow.Cells[1].Text).ToString();

                    Tabla_Meses.Visible = true;
                    Sumar_Total();

                    if (Tr_Programa.Visible)
                    {
                        if (!String.IsNullOrEmpty(Cmb_Programa.SelectedItem.Value.Trim()))
                        {
                            if (Cmb_FF.SelectedIndex > 0)
                            {
                                Negocio.P_Fte_Financiamiento = Cmb_FF.SelectedItem.Value.Trim();
                            }
                            else
                            {
                                Negocio.P_Fte_Financiamiento = String.Empty;
                            }

                            if (!String.IsNullOrEmpty(Txt_Busqueda_Programa.Text.Trim()))
                            {
                                Negocio.P_Busqueda = Txt_Busqueda_Programa.Text.Trim();
                            }
                            else
                            {
                                Negocio.P_Busqueda = String.Empty;
                            }

                            if (!String.IsNullOrEmpty(Hf_Concepto_ID.Value.Trim()))
                            {
                                Negocio.P_Concepto_ID = Hf_Concepto_ID.Value.Trim();
                            }
                            else
                            {
                                Negocio.P_Concepto_ID = String.Empty;
                            }

                            Negocio.P_Programa_ID = Cmb_Programa.SelectedItem.Value.Trim();
                            Dt = Negocio.Consultar_Programas();
                            if (Dt != null && Dt.Rows.Count > 0)
                            {
                                Hf_Importe_Programa.Value = Dt.Rows[0]["IMPORTE"].ToString().Trim();
                                Txt_Importe.Text = String.Format("{0:c}", Dt.Rows[0]["IMPORTE"]);
                            }
                            else
                            {
                                Hf_Importe_Programa.Value = "0.00";
                            }
                        }
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            Mostar_Limpiar_Error(String.Empty, "Error al cargar los datos de los conceptos. Error[" + Ex.Message + "]", true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Movimientos_Sorting
    ///DESCRIPCIÓN          : Evento de ordenar las columnas de los grids
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 17/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Grid_Movimiento_Sorting(object sender, GridViewSortEventArgs e)
    {
        Mostar_Limpiar_Error(String.Empty, String.Empty, false);
        DataTable Dt_Movimientos = new DataTable();

        try
        {
            Llenar_Grid_Movimientos();
            Dt_Movimientos = (DataTable)Grid_Movimiento.DataSource;
            if (Dt_Movimientos != null)
            {
                DataView Dv_Vista = new DataView(Dt_Movimientos);
                String Orden = ViewState["SortDirection"].ToString();
                if (Orden.Equals("ASC"))
                {
                    Dv_Vista.Sort = e.SortExpression + " DESC";
                    ViewState["SortDirection"] = "DESC";
                }
                else
                {
                    Dv_Vista.Sort = e.SortExpression + " ASC";
                    ViewState["SortDirection"] = "ASC";
                }

                Grid_Movimiento.Columns[6].Visible = true;
                Grid_Movimiento.DataSource = Dv_Vista;
                Grid_Movimiento.DataBind();
                Grid_Movimiento.Columns[6].Visible = false;
            }
        }
        catch (Exception Ex)
        {
            Mostar_Limpiar_Error(String.Empty, "Error al ordenar la tabla de Movimientos. Error[" + Ex.Message + "]", true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Movimiento_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento de seleccion del grid de movimientos
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 17/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Grid_Movimiento_SelectedIndexChanged(object sender, EventArgs e) 
    {
        Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocios = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio();
        DataTable Dt_Movimientos = new DataTable();
        Limpiar_Sessiones();
        Mostar_Limpiar_Error(String.Empty, String.Empty, false);
        String Estatus = String.Empty;

        try
        {
            Llenar_Combo_Tipo_Solicitud();
            Hf_Movimiento_ID.Value = "";
            Negocios.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
            Negocios.P_No_Movimiento_Ing = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[1].Text).ToString();
            Estatus = Negocios.Consultar_Estatus_Mov_Ing();
            Hf_Estatus.Value = Estatus.Trim();
            if (Estatus.Trim().Equals("GENERADO"))
            {
                Tr_Chk_Autorizar.Style.Add("display", "block");
                Limpiar_Forma("Todo");
                Estado_Botones("Actualizar");
                Habilitar_Forma(false);
                Div_Grid_Comentarios.Visible = false;
                Div_Grid_Movimientos.Style.Add("display", "none");
                Div_Datos.Style.Add("display", "block");

                Negocios.P_No_Movimiento_Ing = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[1].Text).ToString();
                Hf_Movimiento_ID.Value = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[1].Text).ToString();
                Negocios.P_Estatus = "'GENERADO', 'ACEPTADO'";
                Negocios.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                Dt_Movimientos = Negocios.Consultar_Movimientos();
                if (Dt_Movimientos != null && Dt_Movimientos.Rows.Count > 0)
                {
                    //consultamos el estatus de la ultima modificacion
                    Session["Dt_Mov_Conceptos"] = Dt_Movimientos;
                    Llenar_Grid_Mov_Conceptos();
                    Calcular_Total_Modificado();
                }
            }
            else if (Estatus.Trim().Equals("PREAUTORIZADO"))
            {
                Tr_Chk_Autorizar.Style.Add("display", "block");
                Limpiar_Forma("Todo");
                Estado_Botones("PreActualizar");
                Habilitar_Forma(false);
                Div_Grid_Comentarios.Visible = false;
                Div_Grid_Movimientos.Style.Add("display", "none");
                Div_Datos.Style.Add("display", "block");

                Negocios.P_No_Movimiento_Ing = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[1].Text).ToString();
                Hf_Movimiento_ID.Value = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[1].Text).ToString();
                Negocios.P_Estatus = "'PREAUTORIZADO'";
                Negocios.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                Dt_Movimientos = Negocios.Consultar_Movimientos();
                if (Dt_Movimientos != null && Dt_Movimientos.Rows.Count > 0)
                {
                    //consultamos el estatus de la ultima modificacion
                    Session["Dt_Mov_Conceptos"] = Dt_Movimientos;
                    Llenar_Grid_Mov_Conceptos();
                    Calcular_Total_Modificado();
                }
            }
            else 
            {
                Tr_Chk_Autorizar.Style.Add("display", "none");
                Limpiar_Forma("Todo");
                Estado_Botones("Inicial");
                Habilitar_Forma(false);
                Div_Grid_Comentarios.Visible = false;
                Div_Grid_Movimientos.Style.Add("display", "none");
                Div_Datos.Style.Add("display", "block");

                Negocios.P_No_Movimiento_Ing = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[1].Text).ToString();
                Hf_Movimiento_ID.Value = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[1].Text).ToString();
                Negocios.P_Estatus = "'AUTORIZADO'";
                Negocios.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                Dt_Movimientos = Negocios.Consultar_Movimientos();

                if (Dt_Movimientos != null && Dt_Movimientos.Rows.Count > 0)
                {
                    Session["Dt_Mov_Conceptos"] = Dt_Movimientos;
                    Llenar_Grid_Mov_Conceptos();

                    Calcular_Total_Modificado();
                }
                Btn_Salir.ToolTip = "Regresar";
            }
        }
        catch (Exception ex)
        {
            Mostar_Limpiar_Error("Error al obtener los datos de la modificación. Error["+ ex.Message +"]", String.Empty, true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Mov_Conceptos_RowDataBound
    ///DESCRIPCIÓN          : Evento del grid de los movimientos de los conceptos
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 05/Septiembre/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Grid_Mov_Conceptos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            HyperLink Hyp_Lnk_Ruta;

            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                Hyp_Lnk_Ruta = (HyperLink)e.Row.Cells[0].FindControl("Hyp_Lnk_Ruta");
                if (!String.IsNullOrEmpty(e.Row.Cells[42].Text.Trim()) && e.Row.Cells[42].Text.Trim() != "&nbsp;")
                {
                    Hyp_Lnk_Ruta.NavigateUrl = "../Contabilidad/Frm_Con_Mostrar_Archivos.aspx?Documento=" + e.Row.Cells[42].Text.Trim();
                    Hyp_Lnk_Ruta.Enabled = true;
                }
                else
                {
                    Hyp_Lnk_Ruta.NavigateUrl = "";
                    Hyp_Lnk_Ruta.Enabled = false;
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    #endregion

    #region EVENTOS COMBOS
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Rubro_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de rubros
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 18/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_FF_SelectedIndexChanged(object sender, EventArgs e)
        {
            Mostar_Limpiar_Error(String.Empty, String.Empty, false);
            try
            {
                Limpiar_Forma("Concepto");
                Tabla_Meses.Visible = false;
                if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("SUPLEMENTO"))
                {
                    if (Cmb_FF.SelectedIndex > 0)
                    {
                        Cmb_Rubro.Enabled = true;
                        Llenar_Combo_Rubros();
                        Llenar_Combo_Programas();
                    }
                    Llenar_Grid_Conceptos();
                    Tr_Tipo.Visible = false;
                    Tr_Clase.Visible = false;
                }
                else
                {
                    Tr_Tipo.Visible = false;
                    Tr_Clase.Visible = false;
                    Llenar_Combo_Rubros_Ingresos();
                    Llenar_Grid_Conceptos_Ingresos();
                }
                Sumar_Total();
            }
            catch (Exception ex)
            {
                Mostar_Limpiar_Error("Error en el evento del combo de fuente de financiamiento. Error[" + ex.Message + "]", String.Empty, true);
            }
        }
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Rubro_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de rubros
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 17/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_Rubro_SelectedIndexChanged(object sender, EventArgs e)
        {
            Mostar_Limpiar_Error(String.Empty, String.Empty, false);
            try
            {
                if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("SUPLEMENTO"))
                {
                    if (Cmb_Rubro.SelectedIndex > 0)
                    {
                        Cmb_Tipo.Enabled = false;
                        Tr_Clase.Visible = false;
                        Tr_Tipo.Visible = false;
                        Llenar_Combo_Tipos();
                    }
                    Llenar_Grid_Conceptos();
                }
                else 
                {
                    Tr_Tipo.Visible = false;
                    Tr_Clase.Visible = false;
                    Llenar_Grid_Conceptos_Ingresos();
                }
                Sumar_Total();
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del combo de rubros Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Tipo_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de tipos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 17/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_Tipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            Mostar_Limpiar_Error(String.Empty, String.Empty, false);
            try
            {
                if (Cmb_Tipo.SelectedIndex > 0)
                {
                    Llenar_Combo_Clases();
                    Tr_Clase.Visible = true;
                    Cmb_Clase.Enabled = true;
                }
                Llenar_Grid_Conceptos();
                Sumar_Total();
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del combo de tipos Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Clase_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de clases
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 17/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_Clase_SelectedIndexChanged(object sender, EventArgs e)
        {
            Mostar_Limpiar_Error(String.Empty,String.Empty, false);
            try
            {
                Llenar_Grid_Conceptos();
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del combo de clases Error[" + ex.Message + "]");
            }
            Sumar_Total();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Operacion_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de operacion
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 11/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_Operacion_SelectedIndexChanged(object sender, EventArgs e)
        {
            Mostar_Limpiar_Error(String.Empty, String.Empty, false);
            try
            {
                Txt_Justificacion.Text = String.Empty;
                Tr_Tipo.Visible = false;
                Tr_Clase.Visible = false;
                Tr_Programa.Visible = false;
                Cmb_Programa.Enabled = false;
                Btn_Busqueda_Programa.Enabled = false;
                Txt_Busqueda_Programa.Enabled = false;

                if (Cmb_Operacion.SelectedItem.Text.Trim().Equals("SUPLEMENTO"))
                {
                    Llenar_Combo_Fte_Financiamiento();
                    Llenar_Combo_Rubros();
                    Llenar_Grid_Conceptos();
                    Hf_Tipo_Concepto.Value = "Nuevo";
                }
                else 
                {
                    Tr_Tipo.Visible = false;
                    Tr_Clase.Visible = false;
                    Tr_Programa.Visible = false;
                    Llenar_Combo_Fuente_Financiamiento_Ingresos();
                    Llenar_Combo_Rubros_Ingresos();
                    Llenar_Grid_Conceptos_Ingresos();
                    Hf_Tipo_Concepto.Value = "Existente";
                }
                Sumar_Total();
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del combo de clases Error[" + ex.Message + "]");
            }
            Sumar_Total();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Tipo_Solicitud_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo del tipo de solicituf
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_Tipo_Solicitud_SelectedIndexChanged(object sender, EventArgs e)
        {
            Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocios = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio();
            DataTable Dt_Movimientos = new DataTable();
            Mostar_Limpiar_Error(String.Empty, String.Empty, false);
            DataView Dv_Datos = null;//Variable que almacena una vista que obtendra a partir de la búsqueda.
            String Expresion_Busqueda = String.Empty;//Variable que almacenara la expresion de búsqueda.
            Dt_Movimientos = (DataTable)Session["Dt_Mov_Conceptos"];
            String Texto_Buscar = Cmb_Tipo_Solicitud.SelectedItem.Value.Trim();
            Double Total = 0.00;

            try
            {
                Dv_Datos = new DataView(Dt_Movimientos);//Creamos el objeto que almacenara una vista de la tabla de roles.

                Expresion_Busqueda = String.Format("{0} '%{1}%'", Grid_Mov_Conceptos.SortExpression, Texto_Buscar);
                Dv_Datos.RowFilter = "TIPO_MOVIMIENTO like " + Expresion_Busqueda;

                Grid_Mov_Conceptos.Columns[0].Visible = true;
                Grid_Mov_Conceptos.Columns[3].Visible = true;/*ocultar programa*/
                Grid_Mov_Conceptos.Columns[10].Visible = true;
                Grid_Mov_Conceptos.Columns[11].Visible = true;
                Grid_Mov_Conceptos.Columns[12].Visible = true;
                Grid_Mov_Conceptos.Columns[13].Visible = true;
                Grid_Mov_Conceptos.Columns[14].Visible = true;
                Grid_Mov_Conceptos.Columns[15].Visible = true;
                Grid_Mov_Conceptos.Columns[16].Visible = true;
                Grid_Mov_Conceptos.Columns[17].Visible = true;
                Grid_Mov_Conceptos.Columns[18].Visible = true;
                Grid_Mov_Conceptos.Columns[19].Visible = true;
                Grid_Mov_Conceptos.Columns[20].Visible = true;
                Grid_Mov_Conceptos.Columns[21].Visible = true;
                Grid_Mov_Conceptos.Columns[22].Visible = true;
                Grid_Mov_Conceptos.Columns[23].Visible = true;
                Grid_Mov_Conceptos.Columns[24].Visible = true;
                Grid_Mov_Conceptos.Columns[25].Visible = true;
                Grid_Mov_Conceptos.Columns[26].Visible = true;
                Grid_Mov_Conceptos.Columns[27].Visible = true;
                Grid_Mov_Conceptos.Columns[28].Visible = true;
                Grid_Mov_Conceptos.Columns[29].Visible = true;
                Grid_Mov_Conceptos.Columns[30].Visible = true;
                Grid_Mov_Conceptos.Columns[31].Visible = true;
                Grid_Mov_Conceptos.Columns[32].Visible = true;
                Grid_Mov_Conceptos.Columns[33].Visible = true;
                Grid_Mov_Conceptos.Columns[34].Visible = true;
                Grid_Mov_Conceptos.Columns[35].Visible = true;
                Grid_Mov_Conceptos.Columns[36].Visible = true;
                Grid_Mov_Conceptos.Columns[37].Visible = true;
                Grid_Mov_Conceptos.Columns[38].Visible = true;
                Grid_Mov_Conceptos.Columns[39].Visible = true;
                Grid_Mov_Conceptos.Columns[40].Visible = true;
                Grid_Mov_Conceptos.Columns[41].Visible = true;
                Grid_Mov_Conceptos.Columns[42].Visible = true;
                Grid_Mov_Conceptos.Columns[43].Visible = true;
                Grid_Mov_Conceptos.DataSource = Dv_Datos;
                Grid_Mov_Conceptos.DataBind();
                Grid_Mov_Conceptos.Columns[3].Visible = false;/*ocultar programa*/
                Grid_Mov_Conceptos.Columns[10].Visible = false;
                Grid_Mov_Conceptos.Columns[11].Visible = false;
                Grid_Mov_Conceptos.Columns[12].Visible = false;
                Grid_Mov_Conceptos.Columns[13].Visible = false;
                Grid_Mov_Conceptos.Columns[14].Visible = false;
                Grid_Mov_Conceptos.Columns[15].Visible = false;
                Grid_Mov_Conceptos.Columns[16].Visible = false;
                Grid_Mov_Conceptos.Columns[17].Visible = false;
                Grid_Mov_Conceptos.Columns[18].Visible = false;
                Grid_Mov_Conceptos.Columns[19].Visible = false;
                Grid_Mov_Conceptos.Columns[20].Visible = false;
                Grid_Mov_Conceptos.Columns[21].Visible = false;
                Grid_Mov_Conceptos.Columns[22].Visible = false;
                Grid_Mov_Conceptos.Columns[23].Visible = false;
                Grid_Mov_Conceptos.Columns[24].Visible = false;
                Grid_Mov_Conceptos.Columns[25].Visible = false;
                Grid_Mov_Conceptos.Columns[26].Visible = false;
                Grid_Mov_Conceptos.Columns[27].Visible = false;
                Grid_Mov_Conceptos.Columns[28].Visible = false;
                Grid_Mov_Conceptos.Columns[29].Visible = false;
                Grid_Mov_Conceptos.Columns[30].Visible = false;
                Grid_Mov_Conceptos.Columns[31].Visible = false;
                Grid_Mov_Conceptos.Columns[32].Visible = false;
                Grid_Mov_Conceptos.Columns[33].Visible = false;
                Grid_Mov_Conceptos.Columns[34].Visible = false;
                Grid_Mov_Conceptos.Columns[36].Visible = false;
                Grid_Mov_Conceptos.Columns[41].Visible = false;
                Grid_Mov_Conceptos.Columns[42].Visible = false;
                Grid_Mov_Conceptos.Columns[43].Visible = false;

                if (Btn_Nuevo.ToolTip.Trim().Equals("Guardar") || Btn_Modificar.ToolTip.Trim().Equals("Actualizar"))
                {
                    Grid_Mov_Conceptos.Columns[0].Visible = false;
                    Grid_Mov_Conceptos.Columns[37].Visible = false;
                    Grid_Mov_Conceptos.Columns[38].Visible = false;
                    Grid_Mov_Conceptos.Columns[39].Visible = false;
                    Grid_Mov_Conceptos.Columns[40].Visible = false;
                }
                else if (Btn_Nuevo.ToolTip.Trim().Equals("Actualizar"))
                {
                    Grid_Mov_Conceptos.Columns[0].Visible = false;
                    Grid_Mov_Conceptos.Columns[35].Visible = false;
                    if (Hf_Estatus.Value.Trim().Equals("GENERADO"))
                    {
                        Grid_Mov_Conceptos.Columns[39].Visible = false;
                        Grid_Mov_Conceptos.Columns[40].Visible = false;
                        Lbl_Autorizacion.Text = "Cierre Modificación";
                        Chk_Autorizar.Checked = false;
                        Chk_Aceptacion.Checked = false;
                        Chk_Aceptacion.Visible = true;
                        Lbl_Aceptar.Visible = true;
                        if (Dv_Datos != null)
                        {
                            if (Dv_Datos.Count > 0)
                            {
                                foreach (DataRow Dr in Dv_Datos.Table.Rows)
                                {
                                    if (Dr is DataRow)
                                    {
                                        if (!String.IsNullOrEmpty(Dr["ESTATUS"].ToString()))
                                        {
                                            if (Grid_Mov_Conceptos is GridView)
                                            {
                                                if (Grid_Mov_Conceptos.Rows.Count > 0)
                                                {
                                                    foreach (GridViewRow Gv_Mov in Grid_Mov_Conceptos.Rows)
                                                    {
                                                        if (Gv_Mov is GridViewRow)
                                                        {
                                                            if (!String.IsNullOrEmpty(Gv_Mov.Cells[10].Text))
                                                            {
                                                                if (Gv_Mov.Cells[10].Text.Trim().Equals("ACEPTADO"))
                                                                {
                                                                    ((CheckBox)Gv_Mov.Cells[37].FindControl("Chk_Aceptar")).Checked = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                    }
                    else
                    {
                        Grid_Mov_Conceptos.Columns[37].Visible = false;
                        Grid_Mov_Conceptos.Columns[38].Visible = false;
                        Lbl_Autorizacion.Text = "Autorizar Todo";
                        Chk_Autorizar.Checked = false;
                        Chk_Aceptacion.Visible = false;
                        Lbl_Aceptar.Visible = false;
                    }
                }
                else
                {
                    Grid_Mov_Conceptos.Columns[0].Visible = false;
                    Grid_Mov_Conceptos.Columns[35].Visible = false;
                    Grid_Mov_Conceptos.Columns[37].Visible = false;
                    Grid_Mov_Conceptos.Columns[38].Visible = false;
                    Grid_Mov_Conceptos.Columns[39].Visible = false;
                    Grid_Mov_Conceptos.Columns[40].Visible = false;
                    Chk_Autorizar.Checked = false;
                    Chk_Aceptacion.Checked = false;
                }

                if (Dv_Datos != null && Dv_Datos.Count > 0)
                {
                    if (Dt_Movimientos != null)
                    {
                        if (Dt_Movimientos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Movimientos.Rows)
                            {
                                if (String.IsNullOrEmpty(Cmb_Tipo_Solicitud.SelectedItem.Value.Trim()))
                                {
                                    Total = Total + Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_TOTAL"].ToString().Trim()) ? "0" : Dr["IMP_TOTAL"].ToString().Trim());
                                }
                                else
                                {
                                    if (Cmb_Tipo_Solicitud.SelectedItem.Value.Trim().Equals(Dr["TIPO_MOVIMIENTO"].ToString().Trim()))
                                    {
                                        Total = Total + Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_TOTAL"].ToString().Trim()) ? "0" : Dr["IMP_TOTAL"].ToString().Trim());
                                    }
                                }
                            }
                        }
                    }
                }
                Txt_Total_Modificado.Text = "";
                Txt_Total_Modificado.Text = String.Format("{0:c}", Total);
            }
            catch (Exception ex)
            {
                Mostar_Limpiar_Error("Error en el evento del combo de Tipos. Error[" + ex.Message + "]", String.Empty, true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Programa_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de Programas
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 16/Julio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_Programa_SelectedIndexChanged(object sender, EventArgs e)
        {
            Mostar_Limpiar_Error(String.Empty, String.Empty, false);
            Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio(); //conexion con la capa de negocios
            DataTable Dt = new DataTable();

            try
            {
                Llenar_Grid_Conceptos();
            }
            catch (Exception ex)
            {
                Mostar_Limpiar_Error("Error en el evento del combo de programas. Error[" + ex.Message + "]", String.Empty, true);
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Busqueda_Programa_Click
        ///DESCRIPCIÓN          : Evento del boton de busqueda de programas
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 16/Julio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Btn_Busqueda_Programa_Click(object sender, EventArgs e)
        {
            Mostar_Limpiar_Error(String.Empty, String.Empty, false);
            try
            {
                Busqueda_Programa(Txt_Busqueda_Programa.Text.Trim());
            }
            catch (Exception ex)
            {
                Mostar_Limpiar_Error("Error al tratar de realizar la busqueda de programas. Error[" + ex.Message + "]", String.Empty, true);
            }
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Txt_Busueda_Programa_TextChanged
        ///DESCRIPCIÓN          : Evento de la caja de texto de busqueda de programas
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 16/Julio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Txt_Busueda_Programa_TextChanged(object sender, EventArgs e)
        {
            Mostar_Limpiar_Error(String.Empty, String.Empty, false);
            try
            {
                Busqueda_Programa(Txt_Busqueda_Programa.Text.Trim());
            }
            catch (Exception ex)
            {
                Mostar_Limpiar_Error("Error al tratar de realizar la busqueda de programas. Error[" + ex.Message + "]", String.Empty, true);
            }
        }

    #endregion
    #endregion
}
