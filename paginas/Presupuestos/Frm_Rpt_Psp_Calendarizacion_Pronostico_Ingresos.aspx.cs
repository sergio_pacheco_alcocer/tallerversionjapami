﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using CarlosAg.ExcelXmlWriter;
using System.Text;
using JAPAMI.Rpt_Psp_Estado_Analitico_Ingresos.Negocio;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using JAPAMI.Rpt_Psp_Movimeitnos_Presupuesto_Ingresos.Negocio;
using JAPAMI.Calendarizacion_Pronostico_Ingresos.Negocio;

public partial class paginas_Presupuestos_Frm_Rpt_Psp_Calendarizacion_Pronostico_Ingresos : System.Web.UI.Page
{
    #region (Page Load)

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Page_Load
        ///DESCRIPCIÓN          : Inicio de la pagina
        ///PARAMETROS           : 
        ///CREO                 : Ramón Baeza Yépez
        ///FECHA_CREO           : 11/Junio/2013
        ///*****************************************************************************************************************
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
                if (!IsPostBack)
                {
                    Reporte_Inicio();
                    ViewState["SortDirection"] = "DESC";
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error en el inicio del reporte analitico de ingresos. Error [" + Ex.Message + "]");
            }
        }
    #endregion

    #region METODOS
        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Reporte_Inicio
        ///DESCRIPCIÓN          : Metodo de inicio de la página
        ///PARAMETROS           : 
        ///CREO                 : Ramón Baeza Yépez
        ///FECHA_CREO           : 11/Junio/2013
        ///*****************************************************************************************************************
        private void Reporte_Inicio()
        {
            try
            {
                Div_Contenedor_Msj_Error.Visible = false;
                Limpiar_Controles("Todo");
                Cmb_Anio.SelectedIndex = -1;
                Llenar_Combo_Anios();                
            }
            catch (Exception Ex)
            {
                throw new Exception("Error en el inicio del reporte analitico de ingresos. Error [" + Ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Limpiar_Controles
        ///DESCRIPCIÓN          : Metodo para limpiar los controles del formulario
        ///PARAMETROS           1 Accion: para indicar que parte del codigo limpiara 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 02/Abril/2012
        ///*****************************************************************************************************************
        private void Limpiar_Controles(String Accion)
        {
            try
            {
                switch (Accion)
                {
                    case "Todo":
                        Lbl_Ecabezado_Mensaje.Text = "";
                        Lbl_Mensaje_Error.Text = "";
                        Cmb_Anio.SelectedIndex = -1;
                        break;
                    case "Error":
                        Lbl_Ecabezado_Mensaje.Text = "";
                        Lbl_Mensaje_Error.Text = "";
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al limpiar los controles Error [" + Ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Anios
        ///DESCRIPCIÓN          : Metodo para llenar el combo de los años
        ///PARAMETROS           : 
        ///CREO                 : Ramón Baeza Yépez
        ///FECHA_CREO           : 11/Junio/2013
        ///*****************************************************************************************************************
        private void Llenar_Combo_Anios()
        {
            Cls_Rpt_Psp_Estado_Analitico_Ingresos_Negocio Obj_Ingresos = new Cls_Rpt_Psp_Estado_Analitico_Ingresos_Negocio();
            DataTable Dt_Anios = new DataTable(); //Para almacenar los datos de los tipos de nominas

            try
            {
                Cmb_Anio.Items.Clear();

                Dt_Anios = Obj_Ingresos.Consultar_Anios();

                Cmb_Anio.DataValueField = Ope_Psp_Pronostico_Ingresos.Campo_Anio;
                Cmb_Anio.DataTextField = Ope_Psp_Pronostico_Ingresos.Campo_Anio;
                Cmb_Anio.DataSource = Dt_Anios;
                Cmb_Anio.DataBind();

                Cmb_Anio.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));

                Cmb_Anio.SelectedIndex = Cmb_Anio.Items.IndexOf(Cmb_Anio.Items.FindByValue(String.Format("{0:yyyy}", DateTime.Now)));
                Cmb_Mes.SelectedIndex = Cmb_Mes.Items.IndexOf(Cmb_Mes.Items.FindByValue(String.Format("{0:MM}", DateTime.Now)));
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al llenar el combo de anios: Error[" + Ex.Message + "]");
            }
        }
        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Validar_Campos
        ///DESCRIPCIÓN          : Metodo para validar los campos del formulario
        ///PARAMETROS           : 
        ///CREO                 : Ramón Baeza Yépez
        ///FECHA_CREO           : 02/Abril/2012
        ///*****************************************************************************************************************
        private Boolean Validar_Campos()
        {
            Boolean Datos_Validos = true;
            Limpiar_Controles("Error");
            Lbl_Ecabezado_Mensaje.Text = "Favor de:";
            try
            {
                if (Cmb_Anio.SelectedIndex <= 0)
                {
                    Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Seleccionar un año <br />";
                    Datos_Validos = false;
                }
                if (Cmb_Mes.SelectedIndex <= 0)
                {
                    Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Seleccionar un mes <br />";
                    Datos_Validos = false;
                }
                return Datos_Validos;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al valodar los campos del formulario Error [" + Ex.Message + "]");
            }
        }
        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Agrupar_Concepto
        ///DESCRIPCIÓN          : Agrupamos los datos de los Conceptos
        ///PARAMETROS           : 
        ///CREO                 : Ramón Baeza Yépez
        ///FECHA_CREO           : 02/Junio/2012
        ///*****************************************************************************************************************
        private DataTable Agrupar_Concepto(DataTable Dt_Datos)
        {
            DataRow Nueva_Linea;
            DataRow Linea;
            int Cont_Concepto = 0;
            String Concepto = String.Empty;
            String Clave_Concepto = String.Empty;
            String Clase = String.Empty;
            String Clave_Clase = String.Empty;
            Double Pronosticado = 0;
            Double Real = 0;
            Double Acumulado_Pronostico = 0;
            Double Acumulado_Real = 0;
            Double Diferencia = 0;
            Double Diferencia_Acumulado = 0;
            try
            {
                for (int Cont_Datos = 0; Cont_Datos < Dt_Datos.Rows.Count;Cont_Datos++ )
                {
                    Linea = Dt_Datos.Rows[Cont_Datos];
                    //inicializamos la clve
                    if (String.IsNullOrEmpty(Clave_Concepto))
                    {
                        Clave_Clase = Linea["CLAVE_CALSE"].ToString();
                        Clase = Linea["CLASE"].ToString();
                        Clave_Concepto = Linea["CLAVE_CONCEPTO"].ToString();
                        Concepto = Linea["CONCEPTO"].ToString();
                    }
                    //Cuando sea un nuevo rubro insertmaos los datos del rubro anterior al inicio de sus desgloses
                    if (!Linea["CLAVE_CONCEPTO"].ToString().Equals(Clave_Concepto))
                    {
                        Nueva_Linea = Dt_Datos.NewRow();
                        Nueva_Linea["CLAVE_CALSE"] = Clave_Clase;
                        Nueva_Linea["CLASE"] = Clase;
                        Nueva_Linea["CLAVE_CONCEPTO"] = Clave_Concepto;
                        Nueva_Linea["CONCEPTO"] = Concepto;
                        Nueva_Linea["PRONOSTICADO"] = Pronosticado;
                        Nueva_Linea["REAL"] = Real;
                        Nueva_Linea["DIFERENCIA"] = Diferencia;
                        Nueva_Linea["ACUMULADO_PRONOSTICO"] = Acumulado_Pronostico;
                        Nueva_Linea["ACUMULADO_REAL"] = Acumulado_Real;
                        Nueva_Linea["DIFERENCIA_ACUMULADO"] = Diferencia_Acumulado;

                        Dt_Datos.Rows.InsertAt(Nueva_Linea, Cont_Concepto);
                        Cont_Datos++;
                        Cont_Concepto = Cont_Datos;
                        Linea = Dt_Datos.Rows[Cont_Datos];
                        Clave_Clase = Linea["CLAVE_CALSE"].ToString();
                        Clase = Linea["CLASE"].ToString();
                        Clave_Concepto = Linea["CLAVE_CONCEPTO"].ToString();
                        Concepto = Linea["CONCEPTO"].ToString();
                        Pronosticado = 0;
                        Real = 0;
                        Acumulado_Pronostico = 0;
                        Acumulado_Real = 0;
                        Diferencia = 0;
                        Diferencia_Acumulado = 0;
                    }
                    if (!String.IsNullOrEmpty(Linea["PRONOSTICADO"].ToString()))
                        Pronosticado += Convert.ToDouble(Linea["PRONOSTICADO"]);
                    if (!String.IsNullOrEmpty(Linea["REAL"].ToString()))
                        Real += Convert.ToDouble(Linea["REAL"]);
                    if (!String.IsNullOrEmpty(Linea["ACUMULADO_PRONOSTICO"].ToString()))
                        Acumulado_Pronostico += Convert.ToDouble(Linea["ACUMULADO_PRONOSTICO"]);
                    if (!String.IsNullOrEmpty(Linea["ACUMULADO_REAL"].ToString()))
                        Acumulado_Real += Convert.ToDouble(Linea["ACUMULADO_REAL"]);
                    Diferencia = Real - Pronosticado;
                    Diferencia_Acumulado = Acumulado_Real - Acumulado_Pronostico;
                }
                Nueva_Linea = Dt_Datos.NewRow();
                Nueva_Linea["CLAVE_CALSE"] = Clave_Clase;
                Nueva_Linea["CLASE"] = Clase;
                Nueva_Linea["CLAVE_CONCEPTO"] = Clave_Concepto;
                Nueva_Linea["CONCEPTO"] = Concepto;
                Nueva_Linea["PRONOSTICADO"] = Pronosticado;
                Nueva_Linea["REAL"] = Real;
                Nueva_Linea["ACUMULADO_PRONOSTICO"] = Acumulado_Pronostico;
                Nueva_Linea["ACUMULADO_REAL"] = Acumulado_Real;
                Nueva_Linea["DIFERENCIA"] = Diferencia;
                Nueva_Linea["DIFERENCIA_ACUMULADO"] = Diferencia_Acumulado;
                Dt_Datos.Rows.InsertAt(Nueva_Linea, Cont_Concepto);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al Agrupar_Rubro Error [" + Ex.Message + "]");
            }
            return Dt_Datos;
        }
        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Agrupar_Clase
        ///DESCRIPCIÓN          : Agrupamos los datos de las Clases
        ///PARAMETROS           : 
        ///CREO                 : Ramón Baeza Yépez
        ///FECHA_CREO           : 02/Junio/2012
        ///*****************************************************************************************************************
        private DataTable Agrupar_Clase(DataTable Dt_Datos)
        {
            DataRow Nueva_Linea;
            DataRow Linea;
            String Clase = String.Empty;
            String Clave_Clase = String.Empty;
            Double Pronosticado = 0;
            Double Real = 0;
            Double Acumulado_Pronostico = 0;
            Double Acumulado_Real = 0;
            Double Diferencia = 0;
            Double Diferencia_Acumulado = 0;
            try
            {
                Dt_Datos.Columns.Add("DIFERENCIA", typeof(System.Double));
                Dt_Datos.Columns.Add("DIFERENCIA_ACUMULADO", typeof(System.Double));
                Dt_Datos.Columns.Add("%DIFERENCIA", typeof(System.Double));
                Dt_Datos.Columns.Add("%DIFERENCIA_ACUMULADO", typeof(System.Double));
                Dt_Datos = Agrupar_Concepto(Dt_Datos);
                for (int Cont_Datos = 0; Cont_Datos < Dt_Datos.Rows.Count; Cont_Datos++)
                {
                    Linea = Dt_Datos.Rows[Cont_Datos];
                    //inicializamos la clve
                    if (String.IsNullOrEmpty(Clave_Clase))
                    {
                        Clave_Clase = Linea["CLAVE_CALSE"].ToString();
                        Clase = Linea["CLASE"].ToString();
                    }
                    //Cuando sea un nuevo rubro insertmaos los datos del rubro anterior al inicio de sus desgloses
                    if (!Linea["CLAVE_CALSE"].ToString().Equals(Clave_Clase))
                    {
                        Nueva_Linea = Dt_Datos.NewRow();
                        Nueva_Linea["CLAVE_CALSE"] = Clave_Clase;
                        Nueva_Linea["CLASE"] = Clase;
                        Nueva_Linea["PRONOSTICADO"] = Pronosticado;
                        Nueva_Linea["REAL"] = Real;
                        Nueva_Linea["ACUMULADO_PRONOSTICO"] = Acumulado_Pronostico;
                        Nueva_Linea["ACUMULADO_REAL"] = Acumulado_Real;
                        Nueva_Linea["DIFERENCIA"] = Diferencia;
                        Nueva_Linea["DIFERENCIA_ACUMULADO"] =Diferencia_Acumulado;
                        Nueva_Linea["%DIFERENCIA"] = (Pronosticado != 0 ? Real / Pronosticado * 100 : 0.0);
                        Nueva_Linea["%DIFERENCIA_ACUMULADO"] = (Acumulado_Pronostico != 0 ? Acumulado_Real / Acumulado_Pronostico * 100 : 0.0);
                        Dt_Datos.Rows.InsertAt(Nueva_Linea, Cont_Datos);
                        Cont_Datos++;
                        Linea = Dt_Datos.Rows[Cont_Datos];
                        Clave_Clase = Linea["CLAVE_CALSE"].ToString();
                        Clase = Linea["CLASE"].ToString();
                        Pronosticado = 0;
                        Real = 0;
                        Acumulado_Pronostico = 0;
                        Acumulado_Real = 0;
                        Diferencia = 0;
                        Diferencia_Acumulado = 0;
                    }
                    if (!String.IsNullOrEmpty(Linea["CLAVE_SUBCONCEPTO"].ToString()))
                    {
                        if (!String.IsNullOrEmpty(Linea["PRONOSTICADO"].ToString()))
                            Pronosticado += Convert.ToDouble(Linea["PRONOSTICADO"]);
                        if (!String.IsNullOrEmpty(Linea["REAL"].ToString()))
                            Real += Convert.ToDouble(Linea["REAL"]);
                        if (!String.IsNullOrEmpty(Linea["ACUMULADO_PRONOSTICO"].ToString()))
                            Acumulado_Pronostico += Convert.ToDouble(Linea["ACUMULADO_PRONOSTICO"]);
                        if (!String.IsNullOrEmpty(Linea["ACUMULADO_REAL"].ToString()))
                            Acumulado_Real += Convert.ToDouble(Linea["ACUMULADO_REAL"]);
                        Diferencia = Real - Pronosticado;
                        Diferencia_Acumulado = Acumulado_Real - Acumulado_Pronostico;
                        if (!String.IsNullOrEmpty(Linea["PRONOSTICADO"].ToString()) && !String.IsNullOrEmpty(Linea["REAL"].ToString()))
                        {
                            Linea["%DIFERENCIA"] = (Convert.ToDouble(Linea["PRONOSTICADO"]) != 0 ? Convert.ToDouble(Linea["REAL"]) / Convert.ToDouble(Linea["PRONOSTICADO"]) * 100 : 0.0);
                            Linea["DIFERENCIA"] = Convert.ToDouble(Linea["REAL"]) - Convert.ToDouble(Linea["PRONOSTICADO"]);
                        }
                        else
                            Linea["%DIFERENCIA"] = 0.0;
                        if (!String.IsNullOrEmpty(Linea["ACUMULADO_PRONOSTICO"].ToString()) && !String.IsNullOrEmpty(Linea["ACUMULADO_REAL"].ToString()))
                        {
                            Linea["%DIFERENCIA_ACUMULADO"] = (Convert.ToDouble(Linea["ACUMULADO_PRONOSTICO"]) != 0 ? Convert.ToDouble(Linea["ACUMULADO_REAL"]) / Convert.ToDouble(Linea["ACUMULADO_PRONOSTICO"]) * 100 : 0.0);
                            Linea["DIFERENCIA_ACUMULADO"] = Convert.ToDouble(Linea["ACUMULADO_REAL"]) - Convert.ToDouble(Linea["ACUMULADO_PRONOSTICO"]);
                        }
                        else
                            Linea["%DIFERENCIA_ACUMULADO"] = 0.0;
                    }
                }
                Nueva_Linea = Dt_Datos.NewRow();
                Nueva_Linea["CLAVE_CALSE"] = Clave_Clase;
                Nueva_Linea["CLASE"] = Clase;
                Nueva_Linea["PRONOSTICADO"] = Pronosticado;
                Nueva_Linea["REAL"] = Real;
                Nueva_Linea["ACUMULADO_PRONOSTICO"] = Acumulado_Pronostico;
                Nueva_Linea["ACUMULADO_REAL"] = Acumulado_Real;
                Nueva_Linea["DIFERENCIA"] = Diferencia;
                Nueva_Linea["DIFERENCIA_ACUMULADO"] = Diferencia_Acumulado; 
                Nueva_Linea["%DIFERENCIA"] = (Pronosticado != 0 ? Real / Pronosticado * 100 : 0.0);
                Nueva_Linea["%DIFERENCIA_ACUMULADO"] = (Acumulado_Pronostico != 0 ? Acumulado_Real / Acumulado_Pronostico * 100 : 0.0);
                Dt_Datos.Rows.InsertAt(Nueva_Linea, Dt_Datos.Rows.Count);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al Agrupar_Rubro Error [" + Ex.Message + "]");
            }
            return Dt_Datos;
        }
    #endregion

    #region EVENTOS

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
        ///DESCRIPCIÓN          : Ejecuta la operacion de click en el boton de salir
        ///PARAMETROS           : 
        ///CREO                 : Ramón Baeza Yépez
        ///FECHA_CREO           : 11/Junio/2013
        ///*****************************************************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            if (Btn_Salir.ToolTip.Trim().Equals("Salir"))
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Generar_Reporte_Click
        ///DESCRIPCIÓN          : Ejecuta la operacion de click en el boton de generar
        ///PARAMETROS           : 
        ///CREO                 : Ramón Baeza Yépez
        ///FECHA_CREO           : 11/Junio/2013
        ///*****************************************************************************************************************
        protected void Btn_Generar_Reporte_Click(object sender, EventArgs e)
        {
            Cls_Rpt_Psp_Calendarizacion_Pronostico_Ingresos Negocio = new Cls_Rpt_Psp_Calendarizacion_Pronostico_Ingresos();
            Div_Contenedor_Msj_Error.Visible = false;
            Limpiar_Controles("Error");
            String Fecha;
            try
            {
                if (Validar_Campos())
                {
                    Negocio.Fecha = new DateTime(Convert.ToInt16(Cmb_Anio.SelectedItem.Text),Convert.ToInt16(Cmb_Mes.SelectedValue), 01);
                    Pasar_Datos_A_Excel(Agrupar_Clase(Negocio.Pronostico_Ingresos()));
                }
                else
                {
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del boton de generar el reporte. Error[" + ex.Message + "]");
            }
        }
    #endregion

    #region (Reportes)
        /// *************************************************************************************************************************
        /// Nombre: Pasar_DataTable_A_Excel
        /// Descripción: Pasa DataTable a Excel.
        /// Parámetros: Dt_Reporte.- DataTable que se pasara a excel.
        /// Usuario Creo: Ramón Baeza Yepez.
        /// Fecha Creó: 11/Junio/2013.
        /// *************************************************************************************************************************
        public void Pasar_Datos_A_Excel(DataTable Dt_Datos)
        {
            String Mes = Cmb_Mes.SelectedItem.Text;
            String Año = Cmb_Anio.SelectedItem.Text;
            String Nombre_Archivo = "Rpte Ingr Cal " + Mes + " " + Año + ".xls";
            DiskFileDestinationOptions m_crDiskFileDestinationOptions = new DiskFileDestinationOptions();
            ReportDocument Reporte = new ReportDocument();
            String Clase = String.Empty;
            try
            {
                //Creamos el libro de Excel.
                CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();

                Libro.Properties.Title = "Plantilla de Puestos";
                Libro.Properties.Created = DateTime.Now;
                Libro.Properties.Author = "Plantilla de Puestos - " + Cls_Sessiones.Nombre_Empleado;

                //Creamos una hoja que tendrá el libro.
                CarlosAg.ExcelXmlWriter.Worksheet Hoja;
                //Agregamos un renglón a la hoja de excel.
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon;

                //Creamos el estilo titulo para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Top = Libro.Styles.Add("Top");
                Estilo_Top.Font.FontName = "Arial";
                Estilo_Top.Font.Size = 14;
                Estilo_Top.Font.Bold = true;
                Estilo_Top.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Top.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Top.Font.Color = "blue";
                Estilo_Top.Interior.Color = "white";
                Estilo_Top.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Top.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                Estilo_Top.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Top.Alignment.WrapText = true;

                //Creamos el estilo titulo para la hoja de excel
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Header= Libro.Styles.Add("Header");
                Estilo_Header.Font.FontName = "Arial";
                Estilo_Header.Font.Size = 12;
                Estilo_Header.Font.Bold = true;
                Estilo_Header.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Header.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Header.Font.Color = "#000000";
                Estilo_Header.Interior.Color = "white";
                Estilo_Header.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Header.Alignment.WrapText = true;

                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Columna_1 = Libro.Styles.Add("Columna_1");
                Columna_1.Font.FontName = "Tahoma";
                Columna_1.Font.Size = 10;
                Columna_1.Font.Bold = true;
                Columna_1.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Columna_1.Alignment.Vertical = StyleVerticalAlignment.Center;
                Columna_1.Font.Color = "#000000";
                Columna_1.Interior.Color = "White";
                Columna_1.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Columna_1.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Columna_1.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Columna_1.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                Columna_1.Interior.Pattern = StyleInteriorPattern.Solid;
                Columna_1.Alignment.WrapText = true;

                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Columna_2 = Libro.Styles.Add("Columna_2");
                Columna_2.Font.FontName = "Tahoma";
                Columna_2.Font.Size = 10;
                Columna_2.Font.Bold = false;
                Columna_2.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Columna_2.Alignment.Vertical = StyleVerticalAlignment.Center;
                Columna_2.Font.Color = "#000000";
                Columna_2.Interior.Color = "#C0C0C0";
                Columna_2.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Columna_2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Columna_2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Columna_2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                Columna_2.Interior.Pattern = StyleInteriorPattern.Solid;
                Columna_2.Alignment.WrapText = true;

                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Columna_3 = Libro.Styles.Add("Columna_3");
                Columna_3.Font.FontName = "Tahoma";
                Columna_3.Font.Size = 10;
                Columna_3.Font.Bold = false;
                Columna_3.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Columna_3.Alignment.Vertical = StyleVerticalAlignment.Center;
                Columna_3.Font.Color = "#000000";
                Columna_3.Interior.Color = "#99CCFF";
                Columna_3.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Columna_3.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Columna_3.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Columna_3.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                Columna_3.Interior.Pattern = StyleInteriorPattern.Solid;
                Columna_3.Alignment.WrapText = true;

                //Creamos el estilo contenido para la hoja de fraccion. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Contenido_1 = Libro.Styles.Add("Contenido_1");
                Contenido_1.Font.FontName = "Arial";
                Contenido_1.Font.Size = 8;
                Contenido_1.Font.Bold = true;
                Contenido_1.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Contenido_1.Alignment.Vertical = StyleVerticalAlignment.Center;
                Contenido_1.Font.Color = "#000000";
                Contenido_1.Interior.Color = "#99CCFF";
                Contenido_1.Interior.Pattern = StyleInteriorPattern.Solid;
                Contenido_1.Alignment.WrapText = true;

                //Creamos el estilo contenido para la hoja de total. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Contenido_2 = Libro.Styles.Add("Contenido_2");
                Contenido_2.Font.FontName = "Arial";
                Contenido_2.Font.Size = 8;
                Contenido_2.Font.Bold = true;
                Contenido_2.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Contenido_2.Alignment.Vertical = StyleVerticalAlignment.Center;
                Contenido_2.Font.Color = "#000000";
                Contenido_2.Interior.Color = "#C0C0C0";
                Contenido_2.Interior.Pattern = StyleInteriorPattern.Solid;
                Contenido_2.Alignment.WrapText = true;

                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Contenido_3 = Libro.Styles.Add("Contenido_3");
                Contenido_3.Font.FontName = "Arial";
                Contenido_3.Font.Size = 8;
                Contenido_3.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Contenido_3.Alignment.Vertical = StyleVerticalAlignment.Center;
                Contenido_3.Font.Color = "#000000";
                Contenido_3.Interior.Color = "white";
                Contenido_3.Interior.Pattern = StyleInteriorPattern.Solid;
                Contenido_3.Alignment.WrapText = true;

                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Contenido_4 = Libro.Styles.Add("Contenido_4");
                Contenido_4.Font.FontName = "Arial";
                Contenido_4.Font.Size = 8;
                Contenido_4.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Contenido_4.Alignment.Vertical = StyleVerticalAlignment.Center;
                Contenido_4.Font.Color = "#000000";
                Contenido_4.Interior.Color = "white";
                Contenido_4.NumberFormat = "#,###,##0.00";
                Contenido_4.Interior.Pattern = StyleInteriorPattern.Solid;
                Contenido_4.Alignment.WrapText = true;

                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Contenido_5 = Libro.Styles.Add("Contenido_5");
                Contenido_5.Font.FontName = "Arial";
                Contenido_5.Font.Size = 8;
                Contenido_5.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Contenido_5.Alignment.Vertical = StyleVerticalAlignment.Center;
                Contenido_5.Font.Color = "#FF4141";
                Contenido_5.Interior.Color = "white";
                Contenido_5.NumberFormat = "#,###,##0.00";
                Contenido_5.Interior.Pattern = StyleInteriorPattern.Solid;
                Contenido_5.Alignment.WrapText = true;

                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Contenido_6 = Libro.Styles.Add("Contenido_6");
                Contenido_6.Font.FontName = "Arial";
                Contenido_6.Font.Size = 8;
                Contenido_6.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Contenido_6.Alignment.Vertical = StyleVerticalAlignment.Center;
                Contenido_6.Font.Color = "#000000";
                Contenido_6.Interior.Color = "#C0C0C0";
                Contenido_6.NumberFormat = "#,###,##0.00";
                Contenido_6.Interior.Pattern = StyleInteriorPattern.Solid;
                Contenido_6.Alignment.WrapText = true;

                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Contenido_7 = Libro.Styles.Add("Contenido_7");
                Contenido_7.Font.FontName = "Arial";
                Contenido_7.Font.Size = 8;
                Contenido_7.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Contenido_7.Alignment.Vertical = StyleVerticalAlignment.Center;
                Contenido_7.Font.Color = "#FF4141";
                Contenido_7.Interior.Color = "#C0C0C0";
                Contenido_7.NumberFormat = "#,###,##0.00";
                Contenido_7.Interior.Pattern = StyleInteriorPattern.Solid;
                Contenido_7.Alignment.WrapText = true;
                
                Hoja = Libro.Worksheets.Add(Cmb_Mes.SelectedItem.Text.Substring(0,3));
                for (Int32 Contador = 0; Contador < 11; Contador++)
                {
                    if (Contador < 2)
                        Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));
                    else if (Contador != 6)
                        Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(75));
                    else
                        Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(5));
                }
                Renglon = Hoja.Table.Rows.Add();
                Renglon.Cells.Clear();
                //SE CARGA LA CABECERA PRINCIPAL DEL ARCHIVO
                WorksheetCell cell;
                cell = Renglon.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.", DataType.String, "Top");
                cell.MergeAcross = 10;            // Merge two cells together
                Renglon.AutoFitHeight = true;
                Renglon = Hoja.Table.Rows.Add();
                cell = Renglon.Cells.Add("CALENDARIZACION PRONOSTICO DE INGRESOS CORRESPONDIENTE AL MES DE " + Mes + " DE " + Año, DataType.String, "Header");
                cell.MergeAcross = 9;            // Merge two cells together
                Renglon.AutoFitHeight = true;
                Renglon = Hoja.Table.Rows.Add();

                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("DESCRIPCION", DataType.String, "Columna_1"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CONCEPTO", DataType.String, "Columna_1"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Mes + " PRONOSTICADO", DataType.String, "Columna_2"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Mes + " REAL", DataType.String, "Columna_3"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("DIFERENCIA", DataType.String, "Columna_2"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("% DIFERENCIA", DataType.String, "Columna_2"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(" "));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("ACUM A " + Mes + " PRONOSTICADO", DataType.String, "Columna_2"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("ACUM A " + Mes + " REAL", DataType.String, "Columna_3"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("DIFERENCIA", DataType.String, "Columna_2"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("% DIFERENCIA", DataType.String, "Columna_2"));
                Renglon = Hoja.Table.Rows.Add();
                Renglon = Hoja.Table.Rows.Add();

                //SE CARGAN LOS DATOS...
                foreach (System.Data.DataRow Fila in Dt_Datos.Rows)
                {
                    if (!String.IsNullOrEmpty(Fila["CLAVE_SUBCONCEPTO"].ToString()))
                    {
                        Renglon.Cells.Add();
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Fila["SUBCONCEPTO"].ToString(), DataType.String, "Contenido_3"));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["PRONOSTICADO"])
                            , DataType.Number, "Contenido_4"));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["REAL"]), DataType.Number, "Contenido_4"));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["DIFERENCIA"]), DataType.Number, (
                            Convert.ToDouble(Fila["DIFERENCIA"]) >= 0 ? "Contenido_4" : "Contenido_5")));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["%DIFERENCIA"]) + "%", DataType.String, "Contenido_4"));
                        Renglon.Cells.Add();
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["ACUMULADO_PRONOSTICO"]), DataType.Number, "Contenido_4"));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["ACUMULADO_REAL"]), DataType.Number, "Contenido_4"));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["DIFERENCIA_ACUMULADO"]), DataType.Number, (
                            Convert.ToDouble(Fila["DIFERENCIA_ACUMULADO"]) >= 0 ? "Contenido_4" : "Contenido_5")));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["%DIFERENCIA_ACUMULADO"]) + "%", DataType.String, "Contenido_4"));
                        	
                        Renglon = Hoja.Table.Rows.Add();
                    }
                    else if (!String.IsNullOrEmpty(Fila["CLAVE_CONCEPTO"].ToString()))
                    {
                        if (String.IsNullOrEmpty(Clase) || !Fila["CLASE"].ToString().Equals(Clase))
                        {
                            Clase = Fila["CLASE"].ToString();
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Fila["CLASE"].ToString(), DataType.String, "Contenido_1"));
                        }
                        else
                            Renglon.Cells.Add("");
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Fila["CONCEPTO"].ToString(), DataType.String, "Contenido_1"));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", DataType.String, "Contenido_4"));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", DataType.String, "Contenido_4"));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", DataType.String, "Contenido_4"));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", DataType.String, "Contenido_4"));
                        Renglon.Cells.Add();
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", DataType.String, "Contenido_4"));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", DataType.String, "Contenido_4"));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", DataType.String, "Contenido_4"));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("", DataType.String, "Contenido_4"));
                        Renglon = Hoja.Table.Rows.Add();
                    }
                    else
                    {
                        cell = Renglon.Cells.Add("TOTAL DE " + Clase, DataType.String, "Contenido_2");
                        cell.MergeAcross = 1;            // Merge two cells together
                        Renglon.AutoFitHeight = true;
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["PRONOSTICADO"]), DataType.Number, "Contenido_6"));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["REAL"]), DataType.Number, "Contenido_6"));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["DIFERENCIA"]), DataType.Number, (
                            Convert.ToDouble(Fila["DIFERENCIA"]) >= 0 ? "Contenido_6" : "Contenido_7")));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["%DIFERENCIA"]) + "%", DataType.String, "Contenido_6"));
                        Renglon.Cells.Add();
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["ACUMULADO_PRONOSTICO"]), DataType.Number, "Contenido_6"));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["ACUMULADO_REAL"]), DataType.Number, "Contenido_6"));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["DIFERENCIA_ACUMULADO"]), DataType.Number, (
                            Convert.ToDouble(Fila["DIFERENCIA_ACUMULADO"]) >= 0 ? "Contenido_6" : "Contenido_7")));
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Fila["%DIFERENCIA_ACUMULADO"]) + "%", DataType.String, "Contenido_6"));
                        Renglon = Hoja.Table.Rows.Add();
                        Renglon = Hoja.Table.Rows.Add();
                    }
                }
                Renglon = Hoja.Table.Rows.Add();
                //Guardamos el archivo
                String Ruta_Archivo = @Server.MapPath("../../Reporte/");
                Libro.Save(Ruta_Archivo + Nombre_Archivo);
                Ruta_Archivo = "../../Reporte/" + Nombre_Archivo;
                Mostrar_Excel(Server.MapPath("../../Reporte/" + Nombre_Archivo), "application/vnd.ms-excel");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
            }
        }
        /// *************************************************************************************
        /// NOMBRE: Mostrar_Excel
        /// DESCRIPCIÓN: Muestra el reporte en excel.
        /// PARÁMETROS: No Aplica
        /// USUARIO CREO: Ramón Baeza Yépez
        /// *************************************************************************************
        private void Mostrar_Excel(string Ruta_Archivo, string Contenido)
        {
            try
            {
                System.IO.FileInfo ArchivoExcel = new System.IO.FileInfo(Ruta_Archivo);
                if (ArchivoExcel.Exists)
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ContentType = Contenido;
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + ArchivoExcel.Name);
                    Response.Charset = "UTF-8";
                    Response.ContentEncoding = Encoding.Default;
                    Response.WriteFile(ArchivoExcel.FullName);
                    Response.End();
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al mostrar el reporte en excel. Error: [" + Ex.Message + "]");
            }
        }
    #endregion
}
